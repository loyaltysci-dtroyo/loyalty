# Loyalty
## Resumen del Proyecto
Conjunto de proyectos y tecnologias para formar el programa de lealtad de ultima generacion Loyalty (Formalmente Affinity).
## Loyalty API Admin
[Ver detalles en el Readme](src/loyalty-api/README.md)
## Loyalty API Client
[Ver detalles en el Readme](src/loyalty-api-client/README.md)
## Loyalty API External
[Ver detalles en el Readme](src/loyalty-api-external/README.md)
## Loyalty API Internal
[Ver detalles en el Readme](src/loyalty-api-internal/README.md)
## Loyalty API Comercios
[Ver detalles en el Readme](src/loyalty-comercios/README.md)
## Loyalty Schedule Jobs
[Ver detalles en el Readme](src/loyalty-schedule-jobs/README.md)
## Loyalty Spark
[Ver detalles en el Readme](src/loyalty-spark/README.md)
## Loyalty Storm Trident
[Ver detalles en el Readme](src/loyalty-storm-trident/README.md)
## Loyalty Stress Tester
[Ver detalles en el Readme](src/loyalty-stress-tester/README.md)
## Loyalty Tracking
[Ver detalles en el Readme](src/loyalty-tracking/README.md)
## Loyalty Webpage Client
[Ver detalles en el Readme](src/loyalty-webpage/README.md)
## Loyalty Webpage Admin
[Ver detalles en el Readme](src/loyalty-admin/README.md)
