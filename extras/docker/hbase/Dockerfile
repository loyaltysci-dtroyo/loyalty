FROM java:openjdk-8

ARG __APACHE_MIRROR_SERVER=https://www-us.apache.org
ARG __HBASE_VERSION=1.3.1

RUN apt-get update \
	&& apt-get -y install curl wget bash \
	&& rm -rf /var/lib/apt/lists/*

ENV HBASE_DATA_DIR=/data \
    HBASE_HOME=/opt/hbase

RUN mkdir -p /opt \
    && mkdir -p "$HBASE_DATA_DIR" \
    && wget -q -O - ${__APACHE_MIRROR_SERVER}/dist/hbase/${__HBASE_VERSION}/hbase-${__HBASE_VERSION}-bin.tar.gz | tar -xzf - -C /opt \
    && mv /opt/hbase-${__HBASE_VERSION} /opt/hbase

COPY scripts /opt/hbase/docker-scripts

COPY conf /opt/hbase/conf

RUN chmod -R +x /opt/hbase/docker-scripts

VOLUME ["$HBASE_DATA_DIR"]

EXPOSE 2181 16000 16010 16020 16030

WORKDIR /opt/hbase

ENTRYPOINT ["/opt/hbase/docker-scripts/start.sh"]
CMD ["master"] 
