#!/usr/bin/env bash

if [ -z "${HBASE_HOME}" ]; then
  export HBASE_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

${HBASE_HOME}/bin/hbase regionserver start &
${HBASE_HOME}/bin/hbase master start --localRegionServers=0
