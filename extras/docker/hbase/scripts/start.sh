#!/bin/bash

if [[ "$1" = "master" ]]; then
  shift;
  /opt/hbase/docker-scripts/master.sh "$@"
elif [[ "$1" == "region" ]]; then
  shift;
  /opt/hbase/docker-scripts/region.sh "$@"
else 
  exec "$@"
fi
