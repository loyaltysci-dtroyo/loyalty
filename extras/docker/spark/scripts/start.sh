#!/bin/bash

if [[ "$1" = "master" ]]; then
  shift;
  /opt/spark/docker-scripts/master.sh "$@"
elif [[ "$1" == "worker" ]]; then
  shift;
  /opt/spark/docker-scripts/worker.sh "$@"
else 
  exec "$@"
fi
