#!/usr/bin/env bash

SPARK_EXECUTOR_MEMORY=2G

SPARK_WORKER_CORES=2

SPARK_WORKER_MEMORY=3g

SPARK_WORKER_INSTANCES=2

SPARK_WORKER_OPTS="-Dspark.worker.cleanup.enabled=true -Dspark.worker.cleanup.appDataTtl=3600 -Dspark.worker.cleanup.interval=180"

SPARK_JAVA_OPTS="-Djava.security.egd=file:///dev/urandom -Duser.timezone=America/Costa_Rica"
