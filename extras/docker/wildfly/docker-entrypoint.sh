#!/bin/bash
if [-z ${JBOSS_HOSTS+x}]
then
sed 's#<property name="initial_hosts"></property>#<property name="initial_hosts">'"$JBOSS_HOSTS"'</property>#' /opt/jboss/wildfly/standalone/configuration/standalone-ha.xml
fi

  if ["$DEPLOYMENTS"="internal"]
  then
    cp /interno/loyalty-api-internal.war "$JBOSS_HOME"/standalone/deployments/
    touch "$JBOSS_HOME"/standalone/deployments/loyalty-api-internal.war.dodeploy
    cp /interno/loyalty-schedule-jobs.jar "$JBOSS_HOME"/standalone/deployments/
    touch "$JBOSS_HOME"/standalone/deployments/loyalty-schedule-jobs.jar.dodeploy
  else
    cp /externo/loyalty-api.war "$JBOSS_HOME"/standalone/deployments/
    touch "$JBOSS_HOME"/standalone/deployments/loyalty-api.war.dodeploy
    cp /externo/loyalty-api-client.war "$JBOSS_HOME"/standalone/deployments/
    touch "$JBOSS_HOME"/standalone/deployments/loyalty-api-client.war.dodeploy
    cp /externo/loyalty-api-external.war "$JBOSS_HOME"/standalone/deployments/
    touch "$JBOSS_HOME"/standalone/deployments/loyalty-api-external.war.dodeploy
    cp /externo/loyalty-tracking.war "$JBOSS_HOME"/standalone/deployments/
    touch "$JBOSS_HOME"/standalone/deployments/loyalty-tracking.war.dodeploy
 fi
/opt/jboss/wildfly/bin/standalone.sh -b=0.0.0.0 -bmanagement=0.0.0.0 -server-config=standalone-ha.xml
