# Modulo de Apache HBase para Wildfly
## Descripcion
Modulo requerido para el uso del cliente de Apache HBase version 1.2.4 para aplicaciones en Wildfly 10.1.0
## Uso
Descomprima el contenido del [archivador](hbase-1.2.4-wildfly-10.1.0-module.tar) en la carpeta base de la instalacion de Wildfly. ejemplo: `tar -xvf hbase-1.2.4-wildfly-10.1.0-module.tar -C /opt/wildfly/10.1.0/`
