package com.flecharoja.loyalty.stress.tester.util;

import java.util.Map;

/**
 *
 * @author svargas
 */
public interface TestAPI extends AutoCloseable {

    public void postData(String nombre, String resourceTarget, Map<String, String> queryParams, String body) throws Exception;

    public void putData(String nombre, String resourceTarget, Map<String, String> queryParams, String body) throws Exception;

    public void patchData(String nombre, String resourceTarget, Map<String, String> queryParams, String body) throws Exception;

    public void getData(String nombre, String resourceTarget, Map<String, String> queryParams) throws Exception;

    public void deleteData(String nombre, String resourceTarget, Map<String, String> queryParams) throws Exception;
}
