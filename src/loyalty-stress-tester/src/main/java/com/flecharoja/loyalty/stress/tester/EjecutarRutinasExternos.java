package com.flecharoja.loyalty.stress.tester;

import com.flecharoja.loyalty.stress.tester.util.APIExterno;
import com.flecharoja.loyalty.stress.tester.util.TestAPI;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonString;

/**
 *
 * @author svargas
 */
public class EjecutarRutinasExternos {

    public static void main(String[] args) throws IOException, URISyntaxException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        try (Stream<Path> paths = Files.walk(Paths.get(EjecutarRutinasExternos.class.getClassLoader().getResource("pruebas_externo").toURI()))) {
            paths.filter(Files::isRegularFile).forEach((p) -> {
                try {
                    JsonObject data = Json.createReader(new StringReader(Files.readAllLines(p).stream().collect(Collectors.joining()))).readObject();

                    int iteraciones = data.getInt("numIteraciones");
                    boolean aleatorio = data.getBoolean("aleatorio");
                    JsonArray rutina = data.getJsonArray("rutina");

                    for (int i = 0; i < iteraciones; i++) {
                        if (aleatorio) {
                            Collections.shuffle(rutina);
                        }

                        try (TestAPI api = new APIExterno(p.getFileName().toString())) {
                            rutina.forEach((r) -> {
                                Map<String, String> parametrosPeticion;
                                if (((JsonObject) r).containsKey("parametrosPeticion")) {
                                    parametrosPeticion = ((JsonObject) r).getJsonObject("parametrosPeticion").entrySet().stream().collect(Collectors.toMap((t) -> t.getKey(), (t) -> ((JsonString)t.getValue()).getString()));
                                } else {
                                    parametrosPeticion = null;
                                }
                                executorService.execute(() -> {
                                    System.out.println("thread "+Thread.currentThread().getId()+" working");
                                    switch (((JsonObject) r).getString("metodo")) {
                                        case "POST": {
                                            try {
                                                api.postData(((JsonObject) r).getString("nombre"), ((JsonObject) r).getString("recurso"), parametrosPeticion, ((JsonObject) r).getJsonObject("cuerpo").toString());
                                            } catch (Exception ex) {
                                                Logger.getLogger(EjecutarRutinasExternos.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            break;
                                        }
                                        case "PUT": {
                                            try {
                                                api.putData(((JsonObject) r).getString("nombre"), ((JsonObject) r).getString("recurso"), parametrosPeticion, ((JsonObject) r).getJsonObject("cuerpo").toString());
                                            } catch (Exception ex) {
                                                Logger.getLogger(EjecutarRutinasExternos.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            break;
                                        }
                                        case "PATCH": {
                                            try {
                                                api.patchData(((JsonObject) r).getString("nombre"), ((JsonObject) r).getString("recurso"), parametrosPeticion, ((JsonObject) r).getJsonObject("cuerpo").toString());
                                            } catch (Exception ex) {
                                                Logger.getLogger(EjecutarRutinasExternos.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            break;
                                        }
                                        case "GET": {
                                            try {
                                                api.getData(((JsonObject) r).getString("nombre"), ((JsonObject) r).getString("recurso"), parametrosPeticion);
                                            } catch (Exception ex) {
                                                Logger.getLogger(EjecutarRutinasExternos.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            break;
                                        }
                                        case "DELETE": {
                                            try {
                                                api.deleteData(((JsonObject) r).getString("nombre"), ((JsonObject) r).getString("recurso"), parametrosPeticion);
                                            } catch (Exception ex) {
                                                Logger.getLogger(EjecutarRutinasExternos.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            break;
                                        }
                                    }
                                });
                            });
                        } catch (Exception e) {
                            Logger.getLogger(EjecutarRutinasExternos.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(EjecutarRutinasExternos.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }
        executorService.shutdown();
    }

}
