package com.flecharoja.loyalty.stress.tester.util;

import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author svargas
 */
public class APICliente implements TestAPI {

    private final String API_ENDPOINT = "https://api-ludis.loyaltysci.com/loyalty-api-client/webresources/v0";
    private final String IDP_ENDPOINT = "https://idp-ludis.loyaltysci.com/auth/realms/loyalty/protocol/openid-connect";

    private final String nombrePrueba;
    private final String username;
    private final String password;

    private JsonObject keycloakSession;

    public APICliente(String nombrePrueba, String username, String password) throws Exception {
        this.nombrePrueba = nombrePrueba;
        this.username = username;
        this.password = password;
        loginIDP();
    }

    private void loginIDP() throws Exception {
        Client client = ClientBuilder.newClient();
        try {
            WebTarget target = client.target(IDP_ENDPOINT).path("token");
            Invocation.Builder request = target.request();
            long start = System.nanoTime();
            Response response = request.post(Entity.entity("client_id=mobile"
                    + "&client_secret=389a046c-5967-4d53-a9c5-e7352aa67319"
                    + "&username=" + username
                    + "&password=" + password
                    + "&grant_type=password", MediaType.APPLICATION_FORM_URLENCODED_TYPE));
            long stop = System.nanoTime();
            if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
                writeResult(nombrePrueba, username, "LOGIN", "login to IDP", (response.getDate()==null?new Date():response.getDate()), "failed with status " + response.getStatus() + " - " + (response.hasEntity() ? response.readEntity(String.class) : "--"), (stop - start));
                throw new Exception("usuario no pudo autentificarse");
            }
            if (!response.getMediaType().equals(MediaType.APPLICATION_JSON_TYPE)) {
                writeResult(nombrePrueba, username, "LOGIN", "login to IDP", (response.getDate()==null?new Date():response.getDate()), "Response invalid", (stop - start));
                throw new Exception("datos de respuesta de autentificacion invalidos" + response.getMediaType());
            }
            keycloakSession = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();
            writeResult(nombrePrueba, username, "LOGIN", "login to IDP", (response.getDate()==null?new Date():response.getDate()), "login successful", (stop - start));
        } finally {
            client.close();
        }
    }

    @Override
    public void postData(String nombre, String resourceTarget, Map<String, String> queryParams, String body) throws Exception {
        Client client = ClientBuilder.newClient();
        try {
            WebTarget target = client.target(API_ENDPOINT).path(resourceTarget);
            if (queryParams!=null && !queryParams.isEmpty()) {
                for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                    target = target.queryParam(entry.getKey(), entry.getValue());
                }
            }
            Invocation.Builder requestCM = target.request();
            requestCM.header("Authorization", "bearer " + keycloakSession.getString("access_token"));

            long start = System.nanoTime();
            Response response = requestCM.post(Entity.entity(body, MediaType.APPLICATION_JSON_TYPE));
            long stop = System.nanoTime();

            switch (response.getStatusInfo().getFamily()) {
                case SUCCESSFUL: {
                    writeResult(nombrePrueba, username, "POST", nombre, (response.getDate()==null?new Date():response.getDate()), "ended with status " + response.getStatus() + " - " + (response.hasEntity() ? response.readEntity(String.class) : "--"), (stop - start));
                    break;
                }
                case CLIENT_ERROR: {
                    if (response.getStatus() == Response.Status.UNAUTHORIZED.getStatusCode()) {
                        loginIDP();
                        postData(nombre, resourceTarget, queryParams, body);
                        break;
                    }
                }
                default: {
                    writeResult(nombrePrueba, username, "POST", nombre, (response.getDate()==null?new Date():response.getDate()), "failed with status " + response.getStatus() + " - " + (response.hasEntity() ? response.readEntity(String.class) : "--"), (stop - start));
                }
            }
        } finally {
            client.close();
        }
    }

    @Override
    public void putData(String nombre, String resourceTarget, Map<String, String> queryParams, String body) throws Exception {
        Client client = ClientBuilder.newClient();
        try {
            WebTarget target = client.target(API_ENDPOINT).path(resourceTarget);
            if (queryParams!=null && !queryParams.isEmpty()) {
                for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                    target = target.queryParam(entry.getKey(), entry.getValue());
                }
            }
            Invocation.Builder requestCM = target.request();
            requestCM.header("Authorization", "bearer " + keycloakSession.getString("access_token"));

            long start = System.nanoTime();
            Response response = requestCM.put(Entity.entity(body, MediaType.APPLICATION_JSON_TYPE));
            long stop = System.nanoTime();

            switch (response.getStatusInfo().getFamily()) {
                case SUCCESSFUL: {
                    writeResult(nombrePrueba, username, "PUT", nombre, (response.getDate()==null?new Date():response.getDate()), "ended with status " + response.getStatus() + " - " + (response.hasEntity() ? response.readEntity(String.class) : "--"), (stop - start));
                    break;
                }
                case CLIENT_ERROR: {
                    if (response.getStatus() == Response.Status.UNAUTHORIZED.getStatusCode()) {
                        loginIDP();
                        putData(nombre, resourceTarget, queryParams, body);
                        break;
                    }
                }
                default: {
                    writeResult(nombrePrueba, username, "PUT", nombre, (response.getDate()==null?new Date():response.getDate()), "failed with status " + response.getStatus() + " - " + (response.hasEntity() ? response.readEntity(String.class) : "--"), (stop - start));
                }
            }
        } finally {
            client.close();
        }
    }

    @Override
    public void patchData(String nombre, String resourceTarget, Map<String, String> queryParams, String body) throws Exception {
        Client client = ClientBuilder.newClient();
        try {
            WebTarget target = client.target(API_ENDPOINT).path(resourceTarget);
            if (queryParams!=null && !queryParams.isEmpty()) {
                for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                    target = target.queryParam(entry.getKey(), entry.getValue());
                }
            }
            Invocation.Builder requestCM = target.request();
            requestCM.header("Authorization", "bearer " + keycloakSession.getString("access_token"));

            long start = System.nanoTime();
            Response response = requestCM.method("PATCH", Entity.entity(body, MediaType.APPLICATION_JSON_TYPE));
            long stop = System.nanoTime();

            switch (response.getStatusInfo().getFamily()) {
                case SUCCESSFUL: {
                    writeResult(nombrePrueba, username, "PATCH", nombre, (response.getDate()==null?new Date():response.getDate()), "ended with status " + response.getStatus() + " - " + (response.hasEntity() ? response.readEntity(String.class) : "--"), (stop - start));
                    break;
                }
                case CLIENT_ERROR: {
                    if (response.getStatus() == Response.Status.UNAUTHORIZED.getStatusCode()) {
                        loginIDP();
                        patchData(nombre, resourceTarget, queryParams, body);
                        break;
                    }
                }
                default: {
                    writeResult(nombrePrueba, username, "PATCH", nombre, (response.getDate()==null?new Date():response.getDate()), "failed with status " + response.getStatus() + " - " + (response.hasEntity() ? response.readEntity(String.class) : "--"), (stop - start));
                }
            }
        } finally {
            client.close();
        }
    }

    @Override
    public void getData(String nombre, String resourceTarget, Map<String, String> queryParams) throws Exception {
        Client client = ClientBuilder.newClient();
        try {
            WebTarget target = client.target(API_ENDPOINT).path(resourceTarget);
            if (queryParams!=null && !queryParams.isEmpty()) {
                for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                    target = target.queryParam(entry.getKey(), entry.getValue());
                }
            }
            Invocation.Builder requestCM = target.request();
            requestCM.header("Authorization", "bearer " + keycloakSession.getString("access_token"));

            long start = System.nanoTime();
            Response response = requestCM.get();
            long stop = System.nanoTime();

            switch (response.getStatusInfo().getFamily()) {
                case SUCCESSFUL: {
                    writeResult(nombrePrueba, username, "GET", nombre, (response.getDate()==null?new Date():response.getDate()), "ended with status " + response.getStatus() + " - " + (response.hasEntity() ? response.readEntity(String.class) : "--"), (stop - start));
                    break;
                }
                case CLIENT_ERROR: {
                    if (response.getStatus() == Response.Status.UNAUTHORIZED.getStatusCode()) {
                        loginIDP();
                        getData(nombre, resourceTarget, queryParams);
                        break;
                    }
                }
                default: {
                    writeResult(nombrePrueba, username, "GET", nombre, (response.getDate()==null?new Date():response.getDate()), "failed with status " + response.getStatus() + " - " + (response.hasEntity() ? response.readEntity(String.class) : "--"), (stop - start));
                }
            }
        } finally {
            client.close();
        }
    }

    @Override
    public void deleteData(String nombre, String resourceTarget, Map<String, String> queryParams) throws Exception {
        Client client = ClientBuilder.newClient();
        try {
            WebTarget target = client.target(API_ENDPOINT).path(resourceTarget);
            if (queryParams!=null && !queryParams.isEmpty()) {
                for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                    target = target.queryParam(entry.getKey(), entry.getValue());
                }
            }
            Invocation.Builder requestCM = target.request();
            requestCM.header("Authorization", "bearer " + keycloakSession.getString("access_token"));

            long start = System.nanoTime();
            Response response = requestCM.delete();
            long stop = System.nanoTime();

            switch (response.getStatusInfo().getFamily()) {
                case SUCCESSFUL: {
                    writeResult(nombrePrueba, username, "DELETE", nombre, (response.getDate()==null?new Date():response.getDate()), "ended with status " + response.getStatus() + " - " + (response.hasEntity() ? response.readEntity(String.class) : "--"), (stop - start));
                    break;
                }
                case CLIENT_ERROR: {
                    if (response.getStatus() == Response.Status.UNAUTHORIZED.getStatusCode()) {
                        loginIDP();
                        deleteData(nombre, resourceTarget, queryParams);
                        break;
                    }
                }
                default: {
                    writeResult(nombrePrueba, username, "DELETE", nombre, (response.getDate()==null?new Date():response.getDate()), "failed with status " + response.getStatus() + " - " + (response.hasEntity() ? response.readEntity(String.class) : "--"), (stop - start));
                }
            }
        } finally {
            client.close();
        }
    }

    @Override
    public void close() throws Exception {
        Client client = ClientBuilder.newClient();
        try {
            WebTarget target = client.target(IDP_ENDPOINT).path("logout");
            Invocation.Builder request = target.request();
            long start = System.nanoTime();
            Response response = request.post(Entity.entity("client_id=mobile"
                    + "&client_secret=389a046c-5967-4d53-a9c5-e7352aa67319"
                    + "&refresh_token=" + keycloakSession.getString("refresh_token"), MediaType.APPLICATION_FORM_URLENCODED_TYPE));
            long stop = System.nanoTime();
            if (response.getStatusInfo().getFamily()!=Response.Status.Family.SUCCESSFUL) {
                writeResult(nombrePrueba, username, "LOGOUT", "logout to IDP", (response.getDate()==null?new Date():response.getDate()), "failed with status " + response.getStatus() + " - " + (response.hasEntity() ? response.readEntity(String.class) : "--"), (stop - start));
            } else {
                writeResult(nombrePrueba, username, "LOGOUT", "logout to IDP", (response.getDate()==null?new Date():response.getDate()), "logout successful", (stop - start));
            }
        } finally {
            client.close();
        }
    }

    private void writeResult(String filename, String username, String httpMethod, String nombreTrabajo, Date fechaRespuesta, String respuesta, long tiempoNanoSeg) {
        String content = 
                "\'" + (username==null?"null":username) + "\',"
                + "\'" + httpMethod + "\',"
                + "\'" + nombreTrabajo + "\',"
                + "\'response date " + fechaRespuesta.toString() + "\',"
                + "\'" + respuesta.substring(0, respuesta.length()<100?respuesta.length():100) + "\',"
                + "\'" + tiempoNanoSeg + "ns\'" + System.lineSeparator();
        try {
            Files.write(Paths.get(APICliente.class.getProtectionDomain().getCodeSource().getLocation().toURI().resolve(filename + "-results.csv")), content.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (URISyntaxException | IOException ex) {
            Logger.getLogger(APICliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
