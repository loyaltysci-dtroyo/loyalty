package com.flecharoja.loyalty.stress.tester;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author svargas
 */
public class TestTransacciones {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(250);
        
        for (int i = 0; i < 10000; i++) {
            executorService.execute(() -> {
                long threadId = Thread.currentThread().getId();

                Client client = ClientBuilder.newClient();
                WebTarget target = client.target("https://idp.flecharoja.com/auth/realms/pruebas/protocol/openid-connect/token");
                Invocation.Builder request = target.request();
                Response response = request.post(Entity.entity("client_id=loyalty-api-external"
                        + "&client_secret=33c16c47-1657-4363-ad6b-bda1e5862280"
                        + "&grant_type=client_credentials", MediaType.APPLICATION_FORM_URLENCODED_TYPE));
                if (response.getStatus()!=200) {
                    return;
                }
                JsonObject keycloakSession = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();
                client.close();
            
                client = ClientBuilder.newClient();
                target = client.target("https://api-loyalty.flecharoja.com/loyalty-api-external/webresources/v0/transactions");
                request = target.request();
                request.header("Authorization", "bearer "+keycloakSession.getString("access_token"));
                
                String uuid = UUID.randomUUID().toString();
                
                JsonObjectBuilder builder = Json.createObjectBuilder();
                builder.add("idTransaction", ""+uuid);
                builder.add("idMember", "392553d6-25fd-4516-be52-19cb6e326513");
                builder.add("date", System.currentTimeMillis());
                builder.add("subtotal", 1000);
                builder.add("taxes", 500);
                builder.add("total", 1500);
                
                long start = System.nanoTime();
                response = request.post(Entity.entity(builder.build().toString(), MediaType.APPLICATION_JSON_TYPE));
                long stop = System.nanoTime();
                
                if (response.getStatus()==200) {
                    String content = (stop-start)+System.lineSeparator();
                    try {
                        Files.write(Paths.get(new URI("file:///home/flecharoja/times")), content.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                    } catch (URISyntaxException | IOException ex) {
                        Logger.getLogger(TestTransacciones.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    String content = "request "+System.currentTimeMillis()+" failed with status "+response.getStatus()+System.lineSeparator();
                    try {
                        Files.write(Paths.get(new URI("file:///home/flecharoja/errors")), content.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                    } catch (URISyntaxException | IOException ex) {
                        Logger.getLogger(TestTransacciones.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                client.close();
                
                client = ClientBuilder.newClient();
                target = client.target("https://idp.flecharoja.com/auth/realms/pruebas/protocol/openid-connect/logout");
                request = target.request();
                request.post(Entity.entity("client_id=loyalty-api-external"
                        + "&client_secret=33c16c47-1657-4363-ad6b-bda1e5862280"
                        + "&refresh_token="+keycloakSession.getString("refresh_token"), MediaType.APPLICATION_FORM_URLENCODED_TYPE));
                client.close();
            });
        }
        
        try {
            List<String> lines = Files.readAllLines(Paths.get(new URI("file:///home/flecharoja/times")));
            Long total = lines.stream().map((t) -> Long.parseLong(t)).reduce((t, u) -> u+t).get();
            String content = "total time: "+total+"ns - average time: "+(total/lines.size())+"ns"+System.lineSeparator();
            Files.write(Paths.get(new URI("file:///home/flecharoja/result")), content.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (URISyntaxException | IOException ex) {
            Logger.getLogger(TestTransacciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        executorService.shutdown();
    }
    
}
