package com.flecharoja.loyalty.stress.tester;

import com.flecharoja.loyalty.stress.tester.util.APICliente;
import com.flecharoja.loyalty.stress.tester.util.TestAPI;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonString;

/**
 *
 * @author svargas
 */
public class EjecutarRutinasClientes {

    public static void main(String[] args) throws IOException, URISyntaxException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        try (Stream<Path> paths = Files.walk(Paths.get(EjecutarRutinasClientes.class.getClassLoader().getResource("pruebas_cliente").toURI()))) {
            paths.filter(Files::isRegularFile).forEach((p) -> {
                try {
                    JsonObject data = Json.createReader(new StringReader(Files.readAllLines(p).stream().collect(Collectors.joining()))).readObject();

                    int iteraciones = data.getInt("numIteraciones");
                    boolean aleatorio = data.getBoolean("aleatorio");
                    JsonArray miembros = data.getJsonArray("miembros");
                    JsonArray rutina = data.getJsonArray("rutina");
                    
                    for (int i = 0; i < iteraciones; i++) {
                        miembros.forEach((m) -> {
                            executorService.execute(() -> {
                                System.out.println("thread "+Thread.currentThread().getId()+" working");
                                String username = ((JsonObject) m).getString("username");
                                String password = ((JsonObject) m).getString("password");

                                if (aleatorio) {
                                    Collections.shuffle(rutina);
                                }

                                try (TestAPI api = new APICliente(p.getFileName().toString(), username, password)) {
                                    rutina.forEach((r) -> {
                                        Map<String, String> parametrosPeticion;
                                        if (((JsonObject) r).containsKey("parametrosPeticion")) {
                                            parametrosPeticion = ((JsonObject) r).getJsonObject("parametrosPeticion").entrySet().stream().collect(Collectors.toMap((t) -> t.getKey(), (t) -> ((JsonString)t.getValue()).getString()));
                                        } else {
                                            parametrosPeticion = null;
                                        }
                                        
                                        switch (((JsonObject) r).getString("metodo")) {
                                            case "POST": {
                                                try {
                                                    api.postData(((JsonObject) r).getString("nombre"), ((JsonObject) r).getString("recurso"), parametrosPeticion, ((JsonObject) r).getJsonObject("cuerpo").toString());
                                                } catch (Exception ex) {
                                                    Logger.getLogger(EjecutarRutinasClientes.class.getName()).log(Level.SEVERE, null, ex);
                                                }
                                                break;
                                            }
                                            case "PUT": {
                                                try {
                                                    api.putData(((JsonObject) r).getString("nombre"), ((JsonObject) r).getString("recurso"), parametrosPeticion, ((JsonObject) r).getJsonObject("cuerpo").toString());
                                                } catch (Exception ex) {
                                                    Logger.getLogger(EjecutarRutinasClientes.class.getName()).log(Level.SEVERE, null, ex);
                                                }
                                                break;
                                            }
                                            case "PATCH": {
                                                try {
                                                    api.patchData(((JsonObject) r).getString("nombre"), ((JsonObject) r).getString("recurso"), parametrosPeticion, ((JsonObject) r).getJsonObject("cuerpo").toString());
                                                } catch (Exception ex) {
                                                    Logger.getLogger(EjecutarRutinasClientes.class.getName()).log(Level.SEVERE, null, ex);
                                                }
                                                break;
                                            }
                                            case "GET": {
                                                try {
                                                    api.getData(((JsonObject) r).getString("nombre"), ((JsonObject) r).getString("recurso"), parametrosPeticion);
                                                } catch (Exception ex) {
                                                    Logger.getLogger(EjecutarRutinasClientes.class.getName()).log(Level.SEVERE, null, ex);
                                                }
                                                break;
                                            }
                                            case "DELETE": {
                                                try {
                                                    api.deleteData(((JsonObject) r).getString("nombre"), ((JsonObject) r).getString("recurso"), parametrosPeticion);
                                                } catch (Exception ex) {
                                                    Logger.getLogger(EjecutarRutinasClientes.class.getName()).log(Level.SEVERE, null, ex);
                                                }
                                                break;
                                            }
                                        }
                                    });
                                } catch (Exception e) {
                                    Logger.getLogger(EjecutarRutinasClientes.class.getName()).log(Level.SEVERE, null, e);
                                }

                            });
                        });
                        try {
                            TimeUnit.SECONDS.sleep(3);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(EjecutarRutinasClientes.class.getName()).log(Level.INFO, null, ex);
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(EjecutarRutinasClientes.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }
        executorService.shutdown();
    }

}
