# Loyalty Spark
Este proyecto contiene varias aplicaciones para su ejecucion en un cluster de Apache Spark en modo standalone

## Prerequisitos
- Java 8
- Apache Maven 3.3.9
- Driver de Oracle 12c (12.1.0.2) instalado en el repositorio maven local (`mvn install:install-file -Dfile=<path-to-file> -DgroupId=com.oracle.jdbc -DartifactId=ojdbc7 -Dversion=12.1.0.2 -Dpackaging=jar`)
- Cluster Standalone de Apache Spark
- Cada trabajador del cluster debe tener acceso a bases de datos Oracle 12c definido en la variable "URL" dentro de la clase ["OracleDBData"](src/main/java/com/flecharoja/loyalty/data/OracleDBData.java) y a la bases de datos HBase definido en la variable "hbase.zookeeper.quorum" dentro de la configuracion en la clase ["HBaseDBData"](src/main/java/com/flecharoja/loyalty/data/HBaseDBData.java)

## Uso
- Compilar y empaquetar el proyecto
- Copiar el jar generado (loyalty-spark-1.0-SNAPSHOT.jar) en el directorio `/opt/spark/jars` de cada trabajador del cluster de spark
- Definir en el maestro del cluster la variable de ambiente "SPARK_HOME" con direccion al directorio de la instalacion de Apache Spark
- Ejecutar algun script dentro de la carpeta de ["scripts de ejecucion"](scripts) desde el maestro del cluster

## Scripts disponibles
### Calculo de miembros eligibles de todos los segmentos necesarios (que cumplan con estado y ultima fecha de actualizacion validas)
Ejecutar el script ["run_m_todos_segmentos"](scripts/run_m_todos_segmentos.sh)
### Calculo de miembros eligibles de un segmento
Ejecutar el script ["run_m_segmento"](scripts/run_m_segmento.sh) pasando como argumento el uuid del segmento
### Vencimiento de registros de metrica que ya han vencido y actualizacion de valores en billeteras
Ejecutar el script ["run_m_metricas"](scripts/run_m_metricas.sh)
