package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class VistasMisiones {
    public static final String NOMBRE_TABLA = "VISTAS_MISION";
    
    public enum ColumnasFamilia {
        DATA;
    }
    
    public enum ColumnasDATA {
        MISION;
    }
}
