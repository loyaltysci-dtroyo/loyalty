package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class Metrica {
    public static final String NOMBRE_TABLA = "METRICA";
    
    public enum NombreColumnas {
        ID_METRICA,
        IND_EXPIRACION,
        DIAS_VENCIMIENTO,
        GRUPO_NIVELES;
    }
    
    public enum ValoresIndExpiracion {
        TRUE("1"),
        FALSE("2");
        
        public final String value;

        private ValoresIndExpiracion(String value) {
            this.value = value;
        }
    }
}
