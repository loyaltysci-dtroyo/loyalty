package com.flecharoja.loyalty.data;

import com.flecharoja.loyalty.util.indicadores.EligibilidadMiembro;
import com.flecharoja.loyalty.util.indicadores.EligiblesSegmento;
import com.flecharoja.loyalty.util.indicadores.RegistrosMetrica;
import com.flecharoja.loyalty.util.indicadores.RegistrosMisiones;
import com.flecharoja.loyalty.util.indicadores.VistasMisiones;
import com.flecharoja.loyalty.util.indicadores.VistasPromociones;
import java.io.IOException;
import java.util.List;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapred.TableOutputFormat;
import org.apache.hadoop.hbase.mapreduce.TableInputFormat;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.mapreduce.Job;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * Clase encargada de la carga y subida de datos a HBase
 *
 * @author svargas
 */
public class HBaseDBData {
    private final JavaSparkContext javaContext;//contexto de spark necesario para la creacion de RDD's

    private final Configuration configuration;//configuracion de HBase

    public HBaseDBData(JavaSparkContext javaContext) {
        this.javaContext = javaContext;//contexto de spark
        
        this.configuration = new Configuration();
        this.configuration.addResource("conf/hbase/hbase-site.xml");
    }
    
    public void saveEligibilidadMiembro(JavaPairRDD<ImmutableBytesWritable, Put> registros) throws IOException {
        //establecimiento de parametros necesarios
        Job job = Job.getInstance(configuration);
        job.setOutputFormatClass(org.apache.hadoop.hbase.mapreduce.TableOutputFormat.class);
        job.getConfiguration().set(TableOutputFormat.OUTPUT_TABLE, configuration.get("hbase.namespace")+":"+EligibilidadMiembro.NOMBRE_TABLA);
        //ejecucion del guardado de eligibles en forma de tabla a HBase
        registros.saveAsNewAPIHadoopDataset(job.getConfiguration());
    }
    
    public JavaPairRDD<ImmutableBytesWritable, Result> getEligibilidadMiembros() {
        //establecimiento de parametros y nombre de la tabla
        configuration.set(TableInputFormat.INPUT_TABLE, configuration.get("hbase.namespace")+":"+EligibilidadMiembro.NOMBRE_TABLA);
        //obtencion de resultados
        JavaPairRDD<ImmutableBytesWritable, Result> registros = javaContext.newAPIHadoopRDD(configuration, TableInputFormat.class, ImmutableBytesWritable.class, Result.class);
        return registros;
    }

    /**
     * Metodo que devuelve la fila de miembros eligibles de un segmento
     *
     * @param idSegmento Identificador del segmento
     * @return Resultado con la informacion de la fila
     * @throws IOException
     */
    public Result getEligiblesSegmento(String idSegmento) throws IOException {
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), EligiblesSegmento.NOMBRE_TABLA));
            Get get = new Get(Bytes.toBytes(idSegmento));
            return table.get(get);
        }
    }

    /**
     * Metodo que salva una fila de miembros eligibles de un segmento
     *
     * @param eligibles Objeto con la informacion de los valores y columna de la
     * fila
     * @throws IOException
     */
    public void saveEligiblesSegmento(Put eligibles) throws IOException {
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), EligiblesSegmento.NOMBRE_TABLA));
            table.put(eligibles);
        }
    }
    
    public void saveEligiblesSegmento(List<Put> eligibles) throws IOException {
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), EligiblesSegmento.NOMBRE_TABLA));
            table.put(eligibles);
        }
    }

    /**
     * Metodo que salva una serie de filas de miembros eligibles en forma de rdd
     *
     * @param registros rdd de Put's con la informacion de cada fila
     * @throws IOException
     */
    public void saveEligiblesSegmento(JavaPairRDD<ImmutableBytesWritable, Put> registros) throws IOException {
        //establecimiento de parametros necesarios
        Job job = Job.getInstance(configuration);
        job.setOutputFormatClass(org.apache.hadoop.hbase.mapreduce.TableOutputFormat.class);
        job.getConfiguration().set(TableOutputFormat.OUTPUT_TABLE, configuration.get("hbase.namespace")+":"+EligiblesSegmento.NOMBRE_TABLA);
        //ejecucion del guardado de eligibles en forma de tabla a HBase
        registros.saveAsNewAPIHadoopDataset(job.getConfiguration());
    }

    /**
     * Metodo que guarda un rdd de spark con una serie de filas de registros de
     * metricas
     *
     * @param registros rdd con una serie de put de filas de registros de
     * metricas
     * @throws IOException
     */
    public void saveRegistrosMetricas(JavaPairRDD<ImmutableBytesWritable, Put> registros) throws IOException {
        //establecimiento de parametros necesarios
        Job job = Job.getInstance(configuration);
        job.setOutputFormatClass(org.apache.hadoop.hbase.mapreduce.TableOutputFormat.class);
        job.getConfiguration().set(TableOutputFormat.OUTPUT_TABLE, configuration.get("hbase.namespace")+":"+RegistrosMetrica.NOMBRE_TABLA);
        //ejecucion del guardado de eligibles en forma de tabla a HBase
        registros.saveAsNewAPIHadoopDataset(job.getConfiguration());
    }

    /**
     * Metodo que obtiene un rdd con todos los registros de metricas
     *
     * @return rdd de filas de registros de metricas
     */
    public JavaPairRDD<ImmutableBytesWritable, Result> getRegistroMetricas() {
        //establecimiento de parametros y nombre de la tabla
        configuration.set(TableInputFormat.INPUT_TABLE, configuration.get("hbase.namespace")+":"+RegistrosMetrica.NOMBRE_TABLA);

        //obtencion de resultados
        JavaPairRDD<ImmutableBytesWritable, Result> registros = javaContext.newAPIHadoopRDD(configuration, TableInputFormat.class, ImmutableBytesWritable.class, Result.class);

        return registros;
    }
    
    /**
     * Metodo que obtiene un rdd con todos los registros de vistas de misiones
     *
     * @return rdd de filas de vistas de misiones
     */
    public JavaPairRDD<ImmutableBytesWritable, Result> getVistasMisiones() {
        //establecimiento de parametros y nombre de la tabla
        configuration.set(TableInputFormat.INPUT_TABLE, configuration.get("hbase.namespace")+":"+VistasMisiones.NOMBRE_TABLA);

        //obtencion de resultados
        JavaPairRDD<ImmutableBytesWritable, Result> registros = javaContext.newAPIHadoopRDD(configuration, TableInputFormat.class, ImmutableBytesWritable.class, Result.class);

        return registros;
    }
    
    /**
     * Metodo que obtiene un rdd con todos los registros de vistas de promociones
     *
     * @return rdd de filas de vistas de promociones
     */
    public JavaPairRDD<ImmutableBytesWritable, Result> getVistasPromociones() {
        //establecimiento de parametros y nombre de la tabla
        configuration.set(TableInputFormat.INPUT_TABLE, configuration.get("hbase.namespace")+":"+VistasPromociones.NOMBRE_TABLA);

        //obtencion de resultados
        JavaPairRDD<ImmutableBytesWritable, Result> registros = javaContext.newAPIHadoopRDD(configuration, TableInputFormat.class, ImmutableBytesWritable.class, Result.class);

        return registros;
    }
    
    /**
     * Metodo que obtiene un rdd con todas los registros de respuestas de misiones por miembros
     *
     * @return rdd de filas de vistas de los registros de respuestas de misiones por miembros
     */
    public JavaPairRDD<ImmutableBytesWritable, Result> getRegistrosMisiones() {
        //establecimiento de parametros y nombre de la tabla
        configuration.set(TableInputFormat.INPUT_TABLE, configuration.get("hbase.namespace")+":"+RegistrosMisiones.NOMBRE_TABLA);

        //obtencion de resultados
        JavaPairRDD<ImmutableBytesWritable, Result> registros = javaContext.newAPIHadoopRDD(configuration, TableInputFormat.class, ImmutableBytesWritable.class, Result.class);

        return registros;
    }
}
