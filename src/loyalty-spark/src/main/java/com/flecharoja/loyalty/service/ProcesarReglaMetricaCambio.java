package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.data.HBaseDBData;
import com.flecharoja.loyalty.exception.ReglaException;
import com.flecharoja.loyalty.util.indicadores.RegistrosMetrica;
import com.flecharoja.loyalty.util.indicadores.ReglasSegmento;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;
import scala.Tuple2;

/**
 * Clase que se encarga de ejecutar una regla de tipo regla metrica cambio
 *
 * @author svargas
 */
public class ProcesarReglaMetricaCambio {

    private final SparkSession spark; //sesion de spark
    private final JavaSparkContext javaContext; //contexto de java spark
    private final StructType schema; //esquema del dataframe a usar para resultados

    private final HBaseDBData hdbd; //instancia encargada de la carga de datos desde HBase

    public ProcesarReglaMetricaCambio(SparkSession spark, StructType schema) {
        this.schema = schema;
        this.spark = spark;
        this.javaContext = new JavaSparkContext(spark.sparkContext());

        this.hdbd = new HBaseDBData(javaContext);
    }

    /**
     * Metodo que se encarga de procesar el metadata de una regla de metrica
     * cambio, para el calculo de miembros validos
     *
     * @param regla Informacion de una regla de metrica cambio
     * @param miembros lista opcional de miembros sobre cuales trabajar
     * @return dataframe de una serie de identificadores de miembros
     * @throws com.flecharoja.loyalty.exception.ReglaException
     */
    public Dataset<Row> inicio(Row regla, List<String> miembros) throws ReglaException {
        //obtencion del metadata de la regla
        String idMetrica = regla.getAs(ReglasSegmento.CambiosMetrica.NombreColumnas.ID_METRICA.toString());
        String indOperador = regla.getAs(ReglasSegmento.CambiosMetrica.NombreColumnas.IND_OPERADOR.toString());
        String indCambio = regla.getAs(ReglasSegmento.CambiosMetrica.NombreColumnas.IND_CAMBIO.toString());
        String indTipo = regla.getAs(ReglasSegmento.CambiosMetrica.NombreColumnas.IND_TIPO.toString());
        String indValor = regla.getAs(ReglasSegmento.CambiosMetrica.NombreColumnas.IND_VALOR.toString());
        Date fechaInicio = regla.getAs(ReglasSegmento.CambiosMetrica.NombreColumnas.FECHA_INICIO.toString());
        Date fechaFinal = regla.getAs(ReglasSegmento.CambiosMetrica.NombreColumnas.FECHA_FINAL.toString());
        Double valorComparacion = Double.parseDouble(regla.getAs(ReglasSegmento.CambiosMetrica.NombreColumnas.VALOR_COMPARACION.toString()));

        //se realiza un filtrado de registros de metrica por el identificador de metrica
        JavaPairRDD<ImmutableBytesWritable, Result> filter1 = hdbd.getRegistroMetricas().filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
            if (miembros == null || miembros.isEmpty() || miembros.contains(new String(t1._2.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.MIEMBRO.toString()))))) {
                String idMetricaRegistro = Bytes.toString(t1._2.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.METRICA.toString())));
                //solo si el identificador de metrica contenida en los registros de metrica es igual al identificador de metrica de la regla
                return idMetricaRegistro.equals(idMetrica);
            }
            return false;
        });

        //se realiza un filtrado de registros por fechas segun el indicador de tipo
        JavaPairRDD<ImmutableBytesWritable, Result> filter2;
        switch (ReglasSegmento.CambiosMetrica.ValoresIndTipo.get(indTipo)) {
            case ANTERIOR: {
                filter2 = filter1.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                    Calendar c0 = Calendar.getInstance();//fecha del registro de metrica
                    c0.setTimeInMillis(new Long(Bytes.toString(t1._2.getRow()).split("&")[0]));

                    Calendar c1 = Calendar.getInstance();//fecha actual

                    //segun el indicador de valor se realiza...
                    //comparacion del dia anterior
                    switch(ReglasSegmento.CambiosMetrica.ValoresIndValor.get(indValor)) {
                        case DIA: {
                            c1.add(Calendar.DATE, -1);
                            return DateUtils.isSameDay(c0, c1);
                        }
                        case MES: {
                            c1.add(Calendar.MONTH, -1);
                            return c0.get(Calendar.YEAR) == c1.get(Calendar.YEAR) && c0.get(Calendar.MONTH) == c1.get(Calendar.MONTH);
                        }
                        case ANO: {
                            c1.add(Calendar.YEAR, -1);
                            return c0.get(Calendar.YEAR) == c1.get(Calendar.YEAR);
                        }
                        default: {
                            throw new ReglaException("Indicador de valor no valido");
                        }
                    }
                });
                break;
            }
            case ULTIMO: {
                filter2 = filter1.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                    Calendar c0 = Calendar.getInstance();//fecha del registro de metrica
                    c0.setTimeInMillis(new Long(Bytes.toString(t1._2.getRow()).split("&")[0]));

                    Calendar c1 = Calendar.getInstance();//fecha 1
                    Calendar c2 = Calendar.getInstance();//fecha 2 (actual)
                    
                    //segun el indicador de valor...
                    //reduccion de la fecha 1 en un dia
                    switch(ReglasSegmento.CambiosMetrica.ValoresIndValor.get(indValor)) {
                        case DIA: {
                            c1.add(Calendar.DATE, -1);
                            break;
                        }
                        case MES: {
                            c1.add(Calendar.MONTH, -1);
                            break;
                        }
                        case ANO: {
                            c1.add(Calendar.YEAR, -1);
                            break;
                        }
                        default: {
                            throw new ReglaException("Indicador de valor no valido");
                        }
                    }

                    //comparacion de la fecha del registro con la fecha 1 para ver si esta es igual o posterior
                    //y comparacion de la fecha de registro con la fecha 2 (actual) para ver si esta es igual o antes
                    return c0.compareTo(c1) >= 0 && c0.compareTo(c2) <= 0;
                });
                break;
            }
            case ENTRE: {
                //filtrado de la fecha del registro este entre las fechas de inicio y final
                filter2 = filter1.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                    Date d0 = new Date(new Long(Bytes.toString(t1._2.getRow()).split("&")[0]));
                    return fechaInicio.compareTo(d0) <= 0 && fechaFinal.compareTo(d0) >= 0;
                });
                break;
            }
            case DESDE: {
                //filtrado de la fecha del registro sea igual o posterior a la fecha de inicio
                filter2 = filter1.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                    Date d0 = new Date(new Long(Bytes.toString(t1._2.getRow()).split("&")[0]));
                    return fechaInicio.compareTo(d0) <= 0;
                });
                break;
            }
            case HASTA: {
                //filtrado de la fecha del registro sea igual o anterior a la fecha final
                filter2 = filter1.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                    Date d0 = new Date(new Long(Bytes.toString(t1._2.getRow()).split("&")[0]));
                    return fechaFinal.compareTo(d0) >= 0;
                });
                break;
            }
            default: {
                throw new ReglaException("Indicador de tipo no valido");
            }
        }

        //se realiza un filtrado de registros por el indicador de cambio
        JavaPairRDD<ImmutableBytesWritable, Result> filter3;
        switch (ReglasSegmento.CambiosMetrica.ValoresIndCambio.get(indCambio)) {
            case ACUMULADO: {
                filter3 = filter2;
                break;
            }
            case DISPONIBLE: {
                filter3 = filter2.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                    return Bytes.toInt(t1._2.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.DISPONIBLE.toString())))>0;
                });
                break;
            }
            case REDIMIDO: {
                filter3 = filter2.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                    return Bytes.toInt(t1._2.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.DISPONIBLE.toString())))!=Bytes.toInt(t1._2.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.CANTIDAD.toString())));
                });
                break;
            }
            case VENCIDO: {
                filter3 = filter2.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                    return t1._2.containsColumn(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.VENCIDO.toString())) && Bytes.toBoolean(t1._2.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.VENCIDO.toString())));
                });
                break;
            }
            default: {
                throw new ReglaException("Indicador de cambio no valido");
            }
        }

        //se realiza la suma de registros por el identificador de miembro...
        //se mapea los registro en pares con el identificador de miembro como llave y la cantidad como valor
        JavaPairRDD<String, Double> filter4 = filter3.values().flatMapToPair((Result t) -> {
            Tuple2 tuple2;
            switch (ReglasSegmento.CambiosMetrica.ValoresIndCambio.get(indCambio)) {
                case ACUMULADO: {
                    tuple2 = new Tuple2(Bytes.toString(t.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.MIEMBRO.toString()))),
                            Bytes.toDouble(t.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.CANTIDAD.toString())))
                    );
                    break;
                }
                case DISPONIBLE: {
                    tuple2 = new Tuple2(Bytes.toString(t.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.MIEMBRO.toString()))),
                            Bytes.toDouble(t.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.DISPONIBLE.toString())))
                    );
                    break;
                }
                case REDIMIDO: {
                    tuple2 = new Tuple2(Bytes.toString(t.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.MIEMBRO.toString()))),
                            Bytes.toDouble(t.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.CANTIDAD.toString())))-Bytes.toInt(t.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.DISPONIBLE.toString())))
                    );
                    break;
                }
                case VENCIDO: {
                    tuple2 = new Tuple2(Bytes.toString(t.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.MIEMBRO.toString()))),
                            Bytes.toDouble(t.getValue(Bytes.toBytes(RegistrosMetrica.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMetrica.ColumnasDATA.DISPONIBLE.toString())))
                    );
                    break;
                }
                default: {
                    throw new ReglaException("Indicador de cambio no valido");
                }
            }
            List<Tuple2<String, Double>> list = new ArrayList<>();
            list.add(tuple2);
            return list.iterator();
        });
        //se reducen los pares por la llave sumando todos sus valores
        JavaPairRDD<String, Double> filter5 = filter4.reduceByKey((Double t1, Double t2) -> t1+t2);

        //se realiza una comparacion de registros
        JavaPairRDD<String, Double> filter6 = filter5.filter((Tuple2<String, Double> t1) -> {
            //segun el indicador de operador se realiza la operacion numerica entre el valor de comparacion con el valor sumado
            switch (ReglasSegmento.ValoresIndOperador.get(indOperador)) {
                case IGUAL: {
                    return t1._2.equals(valorComparacion);
                }
                case MAYOR: {
                    return t1._2 > valorComparacion;
                }
                case MAYOR_IGUAL: {
                    return t1._2 >= valorComparacion;
                }
                case MENOR: {
                    return t1._2 < valorComparacion;
                }
                case MENOR_IGUAL: {
                    return t1._2 <= valorComparacion;
                }
                default: {
                    throw new ReglaException("Indicador de operador no valido");
                }
            }
        });

        // se retorna un dataset con solo el identificador de miembro del resultado de los filtros
        return spark.createDataFrame(filter6.keys().map((String t1) -> RowFactory.create(t1)), schema);
    }
}
