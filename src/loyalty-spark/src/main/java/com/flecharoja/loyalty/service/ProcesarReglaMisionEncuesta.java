package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.data.HBaseDBData;
import com.flecharoja.loyalty.exception.ReglaException;
import com.flecharoja.loyalty.util.indicadores.RegistrosMisiones;
import com.flecharoja.loyalty.util.indicadores.ReglasSegmento;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import scala.Tuple2;

/**
 * Clase que se encarga de ejecutar una regla de tipo regla de respuesta de mision de encuesta
 *
 * @author svargas
 */
public class ProcesarReglaMisionEncuesta {

    private final SparkSession spark; //sesion de spark
    private final JavaSparkContext javaContext; //contexto de java spark
    private final StructType schema; //esquema del dataframe a usar para resultados

    private final HBaseDBData hdbd; //instancia encargada de la carga de datos desde HBase

    public ProcesarReglaMisionEncuesta(SparkSession spark, StructType schema) {
        this.schema = schema;
        this.spark = spark;
        this.javaContext = new JavaSparkContext(spark.sparkContext());

        this.hdbd = new HBaseDBData(javaContext);
    }

    /**
     * Metodo que se encarga de procesar el metadata de una regla de metrica
     * cambio, para el calculo de miembros validos
     *
     * @param regla Informacion de una regla de metrica cambio
     * @param miembros lista opcional de miembros sobre cuales trabajar
     * @return dataframe de una serie de identificadores de miembros
     * @throws com.flecharoja.loyalty.exception.ReglaException
     */
    public Dataset<Row> inicio(Row regla, List<String> miembros) throws ReglaException {
        //obtencion del metadata de la regla
        String idPregunta = regla.getAs(ReglasSegmento.MisionEncuesta.NombreColumnas.ID_PREGUNTA.toString());
        String indOperador = regla.getAs(ReglasSegmento.MisionEncuesta.NombreColumnas.IND_OPERADOR.toString());
        String valorComparacion = regla.getAs(ReglasSegmento.MisionEncuesta.NombreColumnas.VALOR_COMPARACION.toString());

        //se realiza un filtrado de registros de metrica por el identificador de metrica
        JavaPairRDD<ImmutableBytesWritable, Result> filter1 = hdbd.getRegistrosMisiones().mapToPair((t) -> {
            return t;
        }).filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
            if (miembros == null || miembros.isEmpty() || miembros.contains(Bytes.toString(t1._2.getRow()).split("&")[1])) {
                if (!t1._2.containsColumn(Bytes.toBytes(RegistrosMisiones.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMisiones.ColumnasDATA.RESPUESTA.toString()))) {
                    return false;
                }
                String respuesta = Bytes.toString(t1._2.getValue(Bytes.toBytes(RegistrosMisiones.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMisiones.ColumnasDATA.RESPUESTA.toString())));
                
                JsonNode jsonNode;
                try {
                    jsonNode = new ObjectMapper().readTree(respuesta);
                } catch (IOException e) {
                    return false;
                }
                
                if (!jsonNode.has("respuestaEncuesta")) {
                    return false;
                }
                
                for (Iterator<JsonNode> iterator = jsonNode.get("respuestaEncuesta").getElements(); iterator.hasNext();) {
                    JsonNode node = iterator.next();
                    if (node.get("idPregunta").asText().equals(idPregunta)) {
                        return true;
                    }
                }
                return false;
            }
            return false;
        });

        //se realiza un filtrado de registros por fechas segun el indicador de tipo
        JavaPairRDD<ImmutableBytesWritable, Result> filter2 = filter1.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
            String respuesta = Bytes.toString(t1._2.getValue(Bytes.toBytes(RegistrosMisiones.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMisiones.ColumnasDATA.RESPUESTA.toString())));
            JsonNode jsonNode = new ObjectMapper().readTree(respuesta);
            
            String valor = "";
            for (Iterator<JsonNode> iterator = jsonNode.get("respuestaEncuesta").getElements(); iterator.hasNext();) {
                JsonNode node = iterator.next();
                if (node.get("idPregunta").asText().equals(idPregunta)) {
                    valor = node.get("respuesta").asText();
                }
            }
            
            //segun el indicador de operador se realiza la operacion numerica entre el valor de comparacion con el valor sumado
            switch (ReglasSegmento.ValoresIndOperador.get(indOperador)) {
                case IGUAL: {
                    return Double.parseDouble(valor)==Double.parseDouble(valorComparacion);
                }
                case MAYOR: {
                    return Double.parseDouble(valor)>Double.parseDouble(valorComparacion);
                }
                case MAYOR_IGUAL: {
                    return Double.parseDouble(valor)>=Double.parseDouble(valorComparacion);
                }
                case MENOR: {
                    return Double.parseDouble(valor)<Double.parseDouble(valorComparacion);
                }
                case MENOR_IGUAL: {
                    return Double.parseDouble(valor)<=Double.parseDouble(valorComparacion);
                }
                case CONTIENE: {
                    return valor.toUpperCase().contains(valorComparacion.toUpperCase());
                }
                case ES: {
                    return valorComparacion.equalsIgnoreCase(valor);
                }
                case ES_UNO_DE: {
                    return valor.toUpperCase().contains(valorComparacion.toUpperCase());
                }
                case INICIA_CON: {
                    return valor.toUpperCase().startsWith(valorComparacion.toUpperCase());
                }
                case NO_CONTIENE: {
                    return !valor.toUpperCase().contains(valorComparacion.toUpperCase());
                }
                case NO_ES: {
                    return !valorComparacion.equalsIgnoreCase(valor);
                }
                case NO_ES_UNO_DE: {
                    return !valor.toUpperCase().contains(valorComparacion.toUpperCase());
                }
                case TERMINA_CON: {
                    return valor.toUpperCase().endsWith(valorComparacion.toUpperCase());
                }
                case OPCIONES_CUALQUIERA: {
                    for (String opcionValor : valor.split("\n")) {
                        for (String opcionComparacion : valorComparacion.split("\n")) {
                            if (opcionValor.equals(opcionComparacion)) {
                                return true;
                            }
                        }
                    }
                    return false;
                }
                case OPCIONES_SON: {
                    return Arrays.stream(valor.split("\n")).allMatch((opcionValor) -> {
                        for (String opcionComparacion : valorComparacion.split("\n")) {
                            if (opcionValor.equals(opcionComparacion)) {
                                return true;
                            }
                        }
                        return false;
                    });
                }
                default: {
                    throw new ReglaException("Indicador de operador no valido");
                }
            }
        });

        // se retorna un dataset con solo el identificador de miembro del resultado de los filtros
        return spark.createDataFrame(filter2.map((t1) -> Bytes.toString(t1._2.getRow()).split("&")[1]).map((String t1) -> RowFactory.create(t1)), schema);
    }
}
