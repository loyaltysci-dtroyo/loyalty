package com.flecharoja.loyalty.util.indicadores;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author svargas
 */
public class ListaReglasSegmento {
    public static final String NOMBRE_TABLA = "LISTA_REGLAS";
    
    public enum NombreColumnas {
        ID_LISTA,
        ID_SEGMENTO,
        ORDEN,
        IND_OPERADOR;
    }
    
    public enum ValoresIndOperador {
        AND("A"),
        OR("O"),
        NOT("N");
        
        public final String value;
        private static final Map<String, ValoresIndOperador> lookup = new HashMap<>();

        private ValoresIndOperador(String value) {
            this.value = value;
        }
        
        static {
                for (ValoresIndOperador tipo : values()) {
                    lookup.put(tipo.value, tipo);
                }
            }

            public static ValoresIndOperador get (String value) {
                return value==null?null:lookup.get(value);
            }
    }
}
