package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.data.MariaDBData;
import com.flecharoja.loyalty.exception.ReglaException;
import com.flecharoja.loyalty.util.indicadores.BalanceMetricaMiembro;
import com.flecharoja.loyalty.util.indicadores.ReglasSegmento;
import java.util.List;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * Clase que se encarga de ejecutar una regla del tipo metrica balance
 *
 * @author svargas
 */
public class ProcesarReglaMetricaBalance {

    private final SparkSession spark; //sesion de spark

    private final MariaDBData odbd; //instancia encargada de la carga de datos desde Oracle DB

    public ProcesarReglaMetricaBalance(SparkSession spark) {
        this.spark = spark;

        this.odbd = new MariaDBData(spark);
    }

    public Dataset<Row> inicio(Row regla, List<String> miembros) {
        String idMetrica = regla.getAs(ReglasSegmento.BalanceMetrica.NombreColumnas.ID_METRICA.toString());//obtencion del identificador de metrica de la regla
        String indOperador = regla.getAs(ReglasSegmento.BalanceMetrica.NombreColumnas.IND_OPERADOR.toString());//obtencion del indicador de operador de la regla
        String indBalance = regla.getAs(ReglasSegmento.BalanceMetrica.NombreColumnas.IND_BALANCE.toString());//obtencion del indicador de balance de la regla
        Double valorComparacion = Double.parseDouble(regla.getAs(ReglasSegmento.BalanceMetrica.NombreColumnas.VALOR_COMPARACION.toString()));//obtencion del valor a comparacion de la regla
        
        Dataset<Row> filter = odbd.getBalancesMetricaMiembro().where(new Column(BalanceMetricaMiembro.NombreColumnas.ID_METRICA.toString()).equalTo(idMetrica)).filter((row) -> {
            if (miembros == null || miembros.isEmpty() || miembros.contains((String)row.getAs(BalanceMetricaMiembro.NombreColumnas.ID_MIEMBRO.toString()))) {
                Double balance;//atributo para el almacenamiento del balance del miembro

                //segun el indicador de balance se obtiene el balance redimido, disponible, vencido
                switch (ReglasSegmento.BalanceMetrica.ValoresIndBalance.get(indBalance)) {
                    case ACUMULADO: {
                        balance = row.getAs(BalanceMetricaMiembro.NombreColumnas.TOTAL_ACUMULADO.toString());
                        break;
                    }
                    case REDIMIDO: {
                        balance = row.getAs(BalanceMetricaMiembro.NombreColumnas.REDIMIDO.toString());
                        break;
                    }
                    case DISPONIBLE: {
                        balance = row.getAs(BalanceMetricaMiembro.NombreColumnas.DISPONIBLE.toString());
                        break;
                    }
                    case VENCIDO: {
                        balance = row.getAs(BalanceMetricaMiembro.NombreColumnas.VENCIDO.toString());
                        break;
                    }
                    default: {
                        throw new ReglaException("Indicador de balance no valido");
                    }
                }

                //segun el indicador de operador se realiza la operacion numerica correspondiente entre el balance y el valor de comparacion
                switch (ReglasSegmento.ValoresIndOperador.get(indOperador)) {
                    case IGUAL: {
                        return balance.compareTo(valorComparacion) == 0;
                    }
                    case MAYOR: {
                        return balance.compareTo(valorComparacion) == 1;
                    }
                    case MAYOR_IGUAL: {
                        return balance.compareTo(valorComparacion) >= 0;
                    }
                    case MENOR: {
                        return balance.compareTo(valorComparacion) == -1;
                    }
                    case MENOR_IGUAL: {
                        return balance.compareTo(valorComparacion) <= 0;
                    }
                    default: {
                        throw new ReglaException("Indicador de operador no valido");
                    }
                }
            }
            return false;
        });

        return filter.select(new Column(BalanceMetricaMiembro.NombreColumnas.ID_MIEMBRO.toString()));
    }
}
