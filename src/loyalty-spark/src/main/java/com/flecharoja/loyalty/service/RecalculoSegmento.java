package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.data.HBaseDBData;
import com.flecharoja.loyalty.data.MariaDBData;
import com.flecharoja.loyalty.util.AccionSegmento;
import com.flecharoja.loyalty.util.indicadores.EligibilidadMiembro;
import com.flecharoja.loyalty.util.indicadores.EligiblesSegmento;
import com.flecharoja.loyalty.util.indicadores.ListaMiembrosSegmento;
import com.flecharoja.loyalty.util.indicadores.Miembro;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.util.LongAccumulator;
import scala.Tuple2;

/**
 * Clase que se encarga de realizar el mantenimiento de miembros eligibles de un
 * segmento por su identificador
 *
 * @author svargas
 */
public class RecalculoSegmento {

    public static void main(String[] args) {

        SparkSession spark; //sesion de spark
        //establecimiento de la sesion de spark e inicializacion de la aplicacion bajo un nombre
        spark = SparkSession.builder().appName("loyalty - mantenimiento del segmento " + args[0]).getOrCreate();
        JavaSparkContext javaContext; //contexto de java spark
        javaContext = new JavaSparkContext(spark.sparkContext());

        StructType schema; //esquema del dataframe a usar para resultados
        //el esquema contiene una unica columna que contrendra registros de identificadores de miembros
        schema = DataTypes.createStructType(new StructField[]{DataTypes.createStructField(Miembro.Atributos.ID_MIEMBRO.toString(), DataTypes.StringType, false)});

        HBaseDBData hdbd; //instancia encargada de la carga de datos desde HBase
        hdbd = new HBaseDBData(javaContext);
        MariaDBData odbd; //instancia encargada de la carga de datos desde Oracle DB
        odbd = new MariaDBData(spark);

        //acumuladores para el guardado de estadisticas
        LongAccumulator aReglasFallidas, aReglasProcesadas, aListasFallidas, aListasProcesadas;
        aReglasFallidas = javaContext.sc().longAccumulator();
        aReglasProcesadas = javaContext.sc().longAccumulator();
        aListasFallidas = javaContext.sc().longAccumulator();
        aListasProcesadas = javaContext.sc().longAccumulator();

        //obtencion del identificador de segmento
        String idSegmento = args[0];

        //obtencion de listados de miembros (solo uuids) eligibles por segmento (Row)
        Dataset<Row> resultado = new ProcesarSegmento(spark, schema, aReglasFallidas, aReglasProcesadas, aListasFallidas, aListasProcesadas).inicio(idSegmento, AccionSegmento.RECALCULAR);
        
        //obtencion de listas de miembros excluidos/incluidos y la seleccion de los incluidos
        Dataset<Row> datasetMiembrosI = odbd.getMiembrosIncluidosExcluidos(idSegmento).where(new Column(ListaMiembrosSegmento.NombreColumnas.IND_TIPO.toString()).equalTo(ListaMiembrosSegmento.ValoresIndTipo.INCLUIDO.value)).select(new Column(ListaMiembrosSegmento.NombreColumnas.ID_MIEMBRO.toString()));
        //agregado del resultado los miembros incluidos y la eliminacion de duplicados
        resultado = resultado.union(datasetMiembrosI).distinct();
        
        //obtencion de listas de miembros excluidos/incluidos y la seleccion de los excluidos
        Dataset<Row> datasetMiembrosE = odbd.getMiembrosIncluidosExcluidos(idSegmento).where(new Column(ListaMiembrosSegmento.NombreColumnas.IND_TIPO.toString()).equalTo(ListaMiembrosSegmento.ValoresIndTipo.EXCLUIDO.value)).select(new Column(ListaMiembrosSegmento.NombreColumnas.ID_MIEMBRO.toString()));
        //excluision del resultado los miembros excluidos
        resultado = resultado.except(datasetMiembrosE);
        
        JavaPairRDD<ImmutableBytesWritable, Result> eligibilidadMiembros = hdbd.getEligibilidadMiembros();
        Dataset<Row> oldMembers = spark.createDataFrame(
                eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.SEGMENTOS.toString()), Bytes.toBytes(idSegmento));
                    if (value == null) {
                        return false;
                    } else {
                        return Bytes.toBoolean(value);
                    }
                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                schema
        );
        
        resultado = resultado.withColumn("value", functions.lit(true))
                .union(oldMembers.except(resultado).withColumn("value", functions.lit(false)))
                .cache();
        
        JavaPairRDD<ImmutableBytesWritable, Put> resultadoSegmento = resultado.toJavaRDD()
                .mapToPair((Row t) -> {
                    Put put = new Put(Bytes.toBytes(t.getString(0)));
                    put.addColumn(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.SEGMENTOS.toString()), Bytes.toBytes(idSegmento), Bytes.toBytes(t.getBoolean(1)));
                    return new Tuple2<>(new ImmutableBytesWritable(), put);
                });
        long count = 0;
        try {
            Put put = new Put(Bytes.toBytes(idSegmento));//creacion de objeto de row a almacenar para hbase
            List<Row> miembros = resultado.collectAsList();//recoleccion de resultados de miembros a memoria local
            //recorrido de lista de miembros y el agregado del identificador de miembro en el row junto con el valor del uuid generado
            for (Row t : miembros) {
                String idMiembro = t.getAs(Miembro.Atributos.ID_MIEMBRO.toString());//uuid del miembro dentro de cada Row de eligibles
                boolean value = t.getAs("value");
                put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.MIEMBROS.toString()), Bytes.toBytes(idMiembro), Bytes.toBytes(value));
                if (value) {
                    count++;
                }
            }
            //agregado de valores adicionales
            put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.ULTIMA_ACTUALIZACION.toString()), Bytes.toBytes(Calendar.getInstance().getTimeInMillis()));
            put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.CANTIDAD_MIEMBROS.toString()), Bytes.toBytes(count));
            put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.LISTAS_PROCESADAS.toString()), Bytes.toBytes(aListasProcesadas.value()));
            put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.LISTAS_FALLIDAS.toString()), Bytes.toBytes(aListasFallidas.value()));
            put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.REGLAS_PROCESADAS.toString()), Bytes.toBytes(aReglasProcesadas.value()));
            put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.REGLAS_FALLIDAS.toString()), Bytes.toBytes(aReglasFallidas.value()));

            //almacenamiento de eligibles del segmento
            hdbd.saveEligibilidadMiembro(resultadoSegmento);
            hdbd.saveEligiblesSegmento(put);
            
//            hdbd.saveEligiblesSegmento(miembros.stream().map((t) -> {
//                Put put2 = new Put(Bytes.toBytes(t.getString(0)));
//                put2.addColumn(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.SEGMENTOS.toString()), Bytes.toBytes(idSegmento), Bytes.toBytes(t.getBoolean(1)));
//                return put2;
//            }).collect(Collectors.toList()));
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(-1);
        }
    }
}
