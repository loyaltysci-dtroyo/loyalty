package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class PremiosRedimidos {
    public static final String NOMBRE_TABLA = "PREMIO_MIEMBRO";
    
    public enum NombreColumnas {
        ID_PREMIO,
        ID_MIEMBRO,
        ESTADO,
        FECHA_ASIGNACION;
    }
    
    public enum Estados{
        REDIMIDO_SIN_RETIRAR('S'),
        REDIMIDO_RETIRADO('R'),
        ANULADO('A'),
        EXPIRADO('E');
        
        private final char value;

        private Estados(char value) {
            this.value = value;
        }

        public char getValue() {
            return value;
        }
    }
}
