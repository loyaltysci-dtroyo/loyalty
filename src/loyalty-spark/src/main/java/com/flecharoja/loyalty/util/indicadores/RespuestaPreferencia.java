package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class RespuestaPreferencia {
    public static final String NOMBRE_TABLA = "RESPUESTA_PREFERENCIA";
    
    public enum NombreColumnas {
        RESPUESTA,
        ID_MIEMBRO,
        ID_PREFERENCIA;
    }
}
