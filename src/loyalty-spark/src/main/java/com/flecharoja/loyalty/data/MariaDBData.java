package com.flecharoja.loyalty.data;

import com.flecharoja.loyalty.util.indicadores.AtributoDinamico;
import com.flecharoja.loyalty.util.indicadores.BalanceMetricaMiembro;
import com.flecharoja.loyalty.util.indicadores.ComprasProductos;
import com.flecharoja.loyalty.util.indicadores.GrupoNivelesMetrica;
import com.flecharoja.loyalty.util.indicadores.InstanciasNotificaciones;
import com.flecharoja.loyalty.util.indicadores.ListaMiembrosGrupoMiembro;
import com.flecharoja.loyalty.util.indicadores.ListaMiembrosSegmento;
import com.flecharoja.loyalty.util.indicadores.ListaReglasSegmento;
import com.flecharoja.loyalty.util.indicadores.MarcadoresPromociones;
import com.flecharoja.loyalty.util.indicadores.Metrica;
import com.flecharoja.loyalty.util.indicadores.Miembro;
import com.flecharoja.loyalty.util.indicadores.NivelInsigniaMiembro;
import com.flecharoja.loyalty.util.indicadores.NivelMetrica;
import com.flecharoja.loyalty.util.indicadores.NivelMetricaInicialMiembro;
import com.flecharoja.loyalty.util.indicadores.PosicionesGruposTabPos;
import com.flecharoja.loyalty.util.indicadores.PosicionesMiembrosTabPos;
import com.flecharoja.loyalty.util.indicadores.PremiosRedimidos;
import com.flecharoja.loyalty.util.indicadores.ReglasSegmento;
import com.flecharoja.loyalty.util.indicadores.RespuestaPreferencia;
import com.flecharoja.loyalty.util.indicadores.Segmento;
import com.flecharoja.loyalty.util.indicadores.TablaPosiciones;
import com.flecharoja.loyalty.util.indicadores.TransaccionesCompras;
import com.flecharoja.loyalty.util.indicadores.ValorAtbDinMiembro;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * Clase encargada de la lectura de datos de la base de datos relacional Oracle
 * 12c
 *
 * @author svargas
 */
public class MariaDBData {
    private final Properties properties;

    private final SparkSession spark; //variable con la sesion de spark

    public MariaDBData(SparkSession spark) {
        this.spark = spark;

        this.properties = new Properties(); //creacion de propiedades de la conexion con la bases de datos
        try {
            this.properties.load(this.getClass().getResourceAsStream("/conf/mariadb/connection.properties"));
        } catch (IOException ex) {
            Logger.getLogger(MariaDBData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para la obtencion de todos los segmentos
     *
     * @return Dataframe con la informacion de los segmentos
     */
    public Dataset<Row> getSegmentos() {
        return spark.read()
                .jdbc(properties.getProperty("url"), Segmento.NOMBRE_TABLA, properties);
    }

    /**
     * Metodo para la obtencion de todas las listas de reglas
     *
     * @return Dataframe de listas de reglas
     */
    public Dataset<Row> getListasReglas() {
        return spark.read()
                .jdbc(properties.getProperty("url"), ListaReglasSegmento.NOMBRE_TABLA, properties);
    }

    /**
     * Metodo para la obtencion de reglas de atributos de miembros
     *
     * @return Dataframe de reglas
     */
    public Dataset<Row> getReglasMiembroAtributo() {
        return spark.read()
                .jdbc(properties.getProperty("url"), ReglasSegmento.AtributosMiembro.NOMBRE_TABLA, properties);
    }

    /**
     * Metodo para la obtencion de reglas de balance de metrica
     *
     * @return Dataframe de reglas
     */
    public Dataset<Row> getReglasMetricaBalance() {
        return spark.read()
                .jdbc(properties.getProperty("url"), ReglasSegmento.BalanceMetrica.NOMBRE_TABLA, properties);
    }

    /**
     * Metodo para la obtencion de reglas de cambio de metricas
     *
     * @return Dataframe de reglas
     */
    public Dataset<Row> getReglasMetricaCambio() {
        return spark.read()
                .jdbc(properties.getProperty("url"), ReglasSegmento.CambiosMetrica.NOMBRE_TABLA, properties);
    }
    
    /**
     * Metodo para la obtencion de reglas avanzadas
     *
     * @return Dataframe de reglas
     */
    public Dataset<Row> getReglasAvanzada() {
        return spark.read()
                .jdbc(properties.getProperty("url"), ReglasSegmento.Avanzada.NOMBRE_TABLA, properties);
    }
    
    public Dataset<Row> getReglasEncuestaMision() {
        return spark.read()
                .jdbc(properties.getProperty("url"), ReglasSegmento.MisionEncuesta.NOMBRE_TABLA, properties);
    }
    
    public Dataset<Row> getReglasPreferencia() {
        return spark.read()
                .jdbc(properties.getProperty("url"), ReglasSegmento.Preferencia.NOMBRE_TABLA, properties);
    }

    /**
     * Metodo que obtiene los niveles de metrica
     *
     * @return Dataframe de los niveles de metrica
     */
    public Dataset<Row> getNivelesMetrica() {
        return spark.read()
                .jdbc(properties.getProperty("url"), NivelMetrica.NOMBRE_TABLA, properties);
    }

    /**
     * Metodo que obtiene los grupos de metricas
     *
     * @return Dataframe de los grupos de niveles de metrica
     */
    public Dataset<Row> getGrupoNivelesMetrica() {
        return spark.read()
                .jdbc(properties.getProperty("url"), GrupoNivelesMetrica.NOMBRE_TABLA, properties);
    }

    /**
     * Metodo que obtiene los atributos dinamicos
     *
     * @return Dataframe de los atributos dinamicos
     */
    public Dataset<Row> getAtributosDinamicos() {
        return spark.read()
                .jdbc(properties.getProperty("url"), AtributoDinamico.NOMBRE_TABLA, properties);
    }

    /**
     * Metodo que obtiene los atributos dinamicos asociados a un miembro
     *
     * @return Dataframe de los atributos dinamicos asociados a un miembro
     */
    public Dataset<Row> getMiembrosAtributosDinamicos() {
        return spark.read()
                .jdbc(properties.getProperty("url"), ValorAtbDinMiembro.NOMBRE_TABLA, properties);
    }

    /**
     * Metodo que obtiene todos los niveles iniciales de los miembros
     *
     * @return Dataframe de niveles iniciales de los miembros
     */
    public Dataset<Row> getMiembrosNivelesMetrica() {
        return spark.read()
                .jdbc(properties.getProperty("url"), NivelMetricaInicialMiembro.NOMBRE_TABLA, properties);
    }

    /**
     * Metodo que obtiene todos los miembros
     *
     * @return Dataframe con los miembros
     */
    public Dataset<Row> getMiembros() {
        return spark.read()
                .jdbc(properties.getProperty("url"), Miembro.NOMBRE_TABLA, properties);
    }
    
    /**
     * Metodo que obtiene los grupos de miembros asociados
     *
     * @return Dataframe de los grupos de miembros
     */
    public Dataset<Row> getGrupoMiembrosAsociados() {
        return spark.read()
                .jdbc(properties.getProperty("url"), ListaMiembrosGrupoMiembro.NOMBRE_TABLA, properties);
    }
    
    /**
     * Metodo que obtiene las insignias ganadas por miembros
     *
     * @return Dataframe de los grupos de miembros
     */
    public Dataset<Row> getInsigniasMiembrosGanadas() {
        return spark.read()
                .jdbc(properties.getProperty("url"), NivelInsigniaMiembro.NOMBRE_TABLA, properties);
    }

    /**
     * Metodo que obtiene el listado de miembros incluidos/excluidos de un
     * segmento por el identificador del segmento y seleccionando el
     * identificador de miembro y el indicador de tipo (incluido o excluido)
     *
     * @param idSegmento Identificador de un segmento
     * @return Dataframe de una parte de la informacion de los miembros
     * excluidos/incluidos de un segmento
     */
    public Dataset<Row> getMiembrosIncluidosExcluidos(String idSegmento) {
        return spark.read()
                .jdbc(properties.getProperty("url"), ListaMiembrosSegmento.NOMBRE_TABLA, properties)
                .where(new Column(ListaMiembrosSegmento.NombreColumnas.ID_SEGMENTO.toString()).equalTo(idSegmento));
    }
    
    /**
     * Metodo que obtiene los registros de las metricas
     *
     * @return Dataframe de una parte de la informacion de las metricas
     */
    public Dataset<Row> getMetricas() {
        return spark.read()
                .jdbc(properties.getProperty("url"), Metrica.NOMBRE_TABLA, properties);
    }
    
    /**
     * Metodo que obtiene los registros de tablas de posiciones
     *
     * @return Dataframe de una parte de la informacion de tablas de posiciones
     */
    public Dataset<Row> getTablasPosiciones() {
        return spark.read()
                .jdbc(properties.getProperty("url"), TablaPosiciones.NOMBRE_TABLA, properties);
    }
    
    /**
     * Metodo que obtiene los registros de acumulaciones de metrica de miembros a tablas de posicion
     *
     * @return Dataframe de una parte de la informacion de acumulaciones de metrica de miembros a tablas de posicion
     */
    public Dataset<Row> getMiembrosTablasPosiciones() {
        return spark.read()
                .jdbc(properties.getProperty("url"), PosicionesMiembrosTabPos.NOMBRE_TABLA, properties);
    }
    
    /**
     * Metodo que obtiene los registros de acumulaciones de metrica de grupos de miembros a tablas de posicion
     *
     * @return Dataframe de una parte de la informacion de acumulaciones de metrica de grupos de miembros a tablas de posicion
     */
    public Dataset<Row> getGruposMiembrosTablasPosiciones() {
        return spark.read()
                .jdbc(properties.getProperty("url"), PosicionesGruposTabPos.NOMBRE_TABLA, properties);
    }
    
    /**
     * Metodo que obtiene los balances de metricas de miembros
     *
     * @return Dataframe de una parte de la informacion de los balances de metricas de miembros
     */
    public Dataset<Row> getBalancesMetricaMiembro() {
        return spark.read()
                .jdbc(properties.getProperty("url"), BalanceMetricaMiembro.NOMBRE_TABLA, properties);
    }
    
    /**
     * Metodo que obtiene las instancias de notificaciones
     *
     * @return Dataframe de una parte de la informacion de las instancias de notificaciones
     */
    public Dataset<Row> getInstanciasNotificaciones() {
        return spark.read()
                .jdbc(properties.getProperty("url"), InstanciasNotificaciones.NOMBRE_TABLA, properties);
    }
    
    public Dataset<Row> getTransaccionesCompras() {
        return spark.read()
                .jdbc(properties.getProperty("url"), TransaccionesCompras.NOMBRE_TABLA, properties);
    }
    
    public Dataset<Row> getComprasProductos() {
        return spark.read()
                .jdbc(properties.getProperty("url"), ComprasProductos.NOMBRE_TABLA, properties);
    }
    
    public Dataset<Row> getMarcadoresPromociones() {
        return spark.read()
                .jdbc(properties.getProperty("url"), MarcadoresPromociones.NOMBRE_TABLA, properties);
    }
    
    public Dataset<Row> getPremiosRedimidos() {
        return spark.read()
                .jdbc(properties.getProperty("url"), PremiosRedimidos.NOMBRE_TABLA, properties);
    }
    
    public Dataset<Row> getRespuestasPreferencias() {
        return spark.read()
                .jdbc(properties.getProperty("url"), RespuestaPreferencia.NOMBRE_TABLA, properties);
    }
}
