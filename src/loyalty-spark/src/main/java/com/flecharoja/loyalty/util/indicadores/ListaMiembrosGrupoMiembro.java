package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class ListaMiembrosGrupoMiembro {
    public static final String NOMBRE_TABLA = "GRUPO_MIEMBRO";
    
    public enum NombreColumnas {
        ID_GRUPO,
        ID_MIEMBRO;
    }
}
