package com.flecharoja.loyalty.util.indicadores;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author svargas
 */
public class ReglasSegmento {
    
    public enum ValoresIndOperador {
        ES("A"),
        NO_ES("B"),
        ES_UNO_DE("C"),
        NO_ES_UNO_DE("D"),
        INICIA_CON("E"),
        TERMINA_CON("F"),
        CONTIENE("G"),
        NO_CONTIENE("H"),
        ESTA_VACIO("I"),
        NO_ESTA_VACIO("J"),
        IGUAL("V"),
        MAYOR("W"),
        MAYOR_IGUAL("X"),
        MENOR("Y"),
        MENOR_IGUAL("Z"),
        FECHA_ANTES_DE("O"),
        FECHA_A_PARTIR_DE("P"),
        FECHA_IGUAL("Q"),
        OPCIONES_SON("U"),
        OPCIONES_CUALQUIERA("K");
        
        public final String value;
        private static final Map<String, ValoresIndOperador> lookup = new HashMap<>();

        private ValoresIndOperador(String value) {
            this.value = value;
        }
        
        static {
            for (ValoresIndOperador valor : values()) {
                lookup.put(valor.value, valor);
            }
        }
        
        public static ValoresIndOperador get (String value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public static class BalanceMetrica {
        public static final String NOMBRE_TABLA = "REGLA_METRICA_BALANCE";
        
        public enum NombreColumnas {
            ID_REGLA,
            ID_LISTA,
            ID_METRICA,
            IND_BALANCE,
            IND_OPERADOR,
            VALOR_COMPARACION;
        }
        
        public enum ValoresIndBalance {
            ACUMULADO("A"),
            VENCIDO("B"),
            REDIMIDO("C"),
            DISPONIBLE("D");
            
            public final String value;
            private static final Map<String, ValoresIndBalance> lookup = new HashMap<>();

            private ValoresIndBalance(String value) {
                this.value = value;
            }
            
            static {
                for (ValoresIndBalance valor : values()) {
                    lookup.put(valor.value, valor);
                }
            }
            
            public static ValoresIndBalance get (String value) {
                return value==null?null:lookup.get(value);
            }
        }
    }
    
    public static class CambiosMetrica {
        public static final String NOMBRE_TABLA = "REGLA_METRICA_CAMBIO";
        
        public enum NombreColumnas {
            ID_REGLA,
            ID_LISTA,
            ID_METRICA,
            IND_CAMBIO,
            IND_TIPO,
            IND_VALOR,
            FECHA_INICIO,
            FECHA_FINAL,
            IND_OPERADOR,
            VALOR_COMPARACION;
        }
        
        public enum ValoresIndCambio {
            ACUMULADO("A"),
            VENCIDO("B"),
            REDIMIDO("C"),
            DISPONIBLE("D");
            
            public final String value;
            private static final Map<String, ValoresIndCambio> lookup = new HashMap<>();

            private ValoresIndCambio(String value) {
                this.value = value;
            }
            
            static {
                for (ValoresIndCambio valor : values()) {
                    lookup.put(valor.value, valor);
                }
            }
            
            public static ValoresIndCambio get (String value) {
                return value==null?null:lookup.get(value);
            }
        }
        
        public enum ValoresIndTipo {
            ULTIMO("B"),
            ANTERIOR("C"),
            DESDE("D"),
            ENTRE("E"),
            MES_ACTUAL("F"),
            ANO_ACTUAL("G"),
            HASTA("H");
            
            public final String value;
            private static final Map<String, ValoresIndTipo> lookup = new HashMap<>();

            private ValoresIndTipo(String value) {
                this.value = value;
            }
            
            static {
                for (ValoresIndTipo valor : values()) {
                    lookup.put(valor.value, valor);
                }
            }
            
            public static ValoresIndTipo get (String value) {
                return value==null?null:lookup.get(value);
            }
        }
        
        public enum ValoresIndValor {
            DIA("D"),
            MES("M"),
            ANO("A");
            
            public final String value;
            private static final Map<String, ValoresIndValor> lookup = new HashMap<>();

            private ValoresIndValor(String value) {
                this.value = value;
            }
            
            static {
                for (ValoresIndValor valor : values()) {
                    lookup.put(valor.value, valor);
                }
            }
            
            public static ValoresIndValor get (String value) {
                return value==null?null:lookup.get(value);
            }
        }
    }
    
    public static class AtributosMiembro {
        public static final String NOMBRE_TABLA = "REGLA_MIEMBRO_ATB";
        
        public enum NombreColumnas {
            ID_REGLA,
            ID_LISTA,
            IND_ATRIBUTO,
            ID_ATRIBUTO_DINAMICO,
            ID_METRICA,
            ID_NIVEL_METRICA,
            IND_OPERADOR,
            VALOR_COMPARACION;
        }
    }
    
    public static class Preferencia {
        public static final String NOMBRE_TABLA = "REGLA_PREFERENCIA";
        
        public enum NombreColumnas {
            ID_REGLA,
            ID_LISTA,
            ID_PREFERENCIA,
            IND_OPERADOR,
            VALOR_COMPARACION;
        }
    }
    
    public static class MisionEncuesta {
        public static final String NOMBRE_TABLA = "REGLA_ENCUESTA";
        
        public enum NombreColumnas {
            ID_REGLA,
            ID_LISTA,
            ID_PREGUNTA,
            IND_OPERADOR,
            VALOR_COMPARACION;
        }
    }
    
    public static class Avanzada {
        public static final String NOMBRE_TABLA = "REGLA_AVANZADA";
        
        public enum NombreColumnas {
            ID_REGLA,
            ID_LISTA,
            IND_TIPO_REGLA,
            REFERENCIA,
            IND_OPERADOR,
            VALOR_COMPARACION0,
            VALOR_COMPARACION1,
            FECHA_INICIO,
            FECHA_FINAL,
            FECHA_IND_ULTIMO,
            FECHA_CANTIDAD;
        }
        
        public enum TiposDispositivos {
            ANDROID("A"),
            IOS("I"),
            DESCONOCIDO("D");

            public final String value;
            private static final Map<String, TiposDispositivos> lookup = new HashMap<>();

            private TiposDispositivos(String value) {
                this.value = value;
            }

            static {
                for (TiposDispositivos tipo : values()) {
                    lookup.put(tipo.value, tipo);
                }
            }

            public static TiposDispositivos get (String value) {
                return value==null?null:lookup.get(value);
            }
        }

        public enum TiposUltimaFecha {
            DIA("D"),
            MES("M"),
            ANO("A");

            public final String value;
            private static final Map<String, TiposUltimaFecha> lookup = new HashMap<>();

            private TiposUltimaFecha(String value) {
                this.value = value;
            }

            static {
                for (TiposUltimaFecha tipo : values()) {
                    lookup.put(tipo.value, tipo);
                }
            }

            public static TiposUltimaFecha get (String value) {
                return value==null?null:lookup.get(value);
            }
        }

        public enum TiposRegla {
            PERTENECE_NIVEL("Z"),
            UNIERON_PROGRAMA("A"),
            RECIBIERON_MENSAJE("B"),
            ABRIERON_MENSAJE("C"),
            REDIMIERON_PREMIO("D"),
            GASTARON_DINERO("F"),
            ULTIMA_CONEXION("G"),
            REFERENCIAS_MIEMBROS("H"),
            TIPO_DISPOSITIVO("I"),
            POSICION_TABLA_POS("J"),
            CANTIDAD_COMPRAS_UBICACION("K"),
            REDIMIERON_PROMOCION("L"),
            VIERON_PROMOCION("M"),
            COMPRARON_PRODUCTO("N"),
            VIERON_MISION("O"),
            NO_APROBADAS_MISION("P"),
            GANARON_MISION("Q"),
            TIENE_INSIGNIA("R"),
            PERTENECEN_SEGMENTO("T"),
            PERTENECEN_GRUPO_MIE("S"),
            CAMBIARON_NIVEL("U");

            public final String value;
            private static final Map<String, TiposRegla> lookup = new HashMap<>();

            private TiposRegla(String value) {
                this.value = value;
            }

            static {
                for (TiposRegla tipo : TiposRegla.values()) {
                    lookup.put(tipo.value, tipo);
                }
            }

            public static TiposRegla get (String value) {
                return value==null?null:lookup.get(value);
            }
        }
    }
}
