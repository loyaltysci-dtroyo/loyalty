package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.data.HBaseDBData;
import com.flecharoja.loyalty.data.MariaDBData;
import com.flecharoja.loyalty.exception.ListaReglasException;
import com.flecharoja.loyalty.util.AccionSegmento;
import com.flecharoja.loyalty.util.TiposReglasSegmento;
import com.flecharoja.loyalty.util.indicadores.EligibilidadMiembro;
import com.flecharoja.loyalty.util.indicadores.ReglasSegmento;
import java.util.List;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.util.LongAccumulator;
import scala.Tuple2;

/**
 * Clase que se encarga de procesar las reglas de una lista de reglas por su
 * identificador
 *
 * @author svargas
 */
public class ProcesarListaReglas {

    private final SparkSession spark; //sesion de spark
    private final JavaSparkContext javaContext; //contexto de java spark
    private final StructType schema; //esquema del dataframe a usar para resultados

    private final MariaDBData odbd; //instancia encargada de la carga de datos desde Oracle DB
    private final HBaseDBData hbdbd;

    private final LongAccumulator aReglasFallidas, aReglasProcesadas;

    public ProcesarListaReglas(SparkSession spark, StructType schema, LongAccumulator aReglasFallidas, LongAccumulator aReglasProcesadas) {
        this.schema = schema;
        this.spark = spark;
        this.javaContext = new JavaSparkContext(spark.sparkContext());

        this.odbd = new MariaDBData(spark);
        this.hbdbd = new HBaseDBData(javaContext);

        this.aReglasFallidas = aReglasFallidas;
        this.aReglasProcesadas = aReglasProcesadas;
    }

    public Dataset<Row> inicio(String idLista, AccionSegmento accion) throws Exception {
        //lectura de reglas del tipo de atributo de miembros validas por lista de reglas en la bases de datos
        Dataset<Row> datasetReglaMiembroAtb = odbd.getReglasMiembroAtributo();
        //obtencion de reglas de atributos de miembros de una lista por el identificador de la lista de reglas y seleccionando el valor de comparacion y los indicadores de operador y atributo
        Dataset<Row> filter1ReglasMiembroAtb = datasetReglaMiembroAtb.where(new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.ID_LISTA.toString())
                .equalTo(idLista));

        //obtencion de las reglas de metrica balance
        Dataset<Row> datasetReglaMetricaBalance = odbd.getReglasMetricaBalance();
        //obtencion de reglas de balance de metrica de una lista por el identificador de la lista de reglas y seleccionando el identificador de metrica, el valor de comparacion y los indicadores de operador y balance
        Dataset<Row> filter1ReglasMetricaBalance = datasetReglaMetricaBalance.where(new Column(ReglasSegmento.BalanceMetrica.NombreColumnas.ID_LISTA.toString())
                .equalTo(idLista));

        //obtencion de las reglas de metrica cambio
        Dataset<Row> datasetReglaMetricaCambio = odbd.getReglasMetricaCambio();
        //obtencion de reglas de cambio de metricas de una lista por el identificador de la lista de reglas y seleccionando el identificador de metrica, el valor de comparacion, fecha de inicio, fecha final y los indicadores de operador, cambio, tipo y valor
        Dataset<Row> filter1ReglasMetricaCambio = datasetReglaMetricaCambio.where(new Column(ReglasSegmento.CambiosMetrica.NombreColumnas.ID_LISTA.toString())
                .equalTo(idLista));
        
        Dataset<Row> datasetReglaAvanzada = odbd.getReglasAvanzada();
        Dataset<Row> filter1ReglasAvanzada = datasetReglaAvanzada.where(new Column(ReglasSegmento.Avanzada.NombreColumnas.ID_LISTA.toString())
                .equalTo(idLista));
        
        Dataset<Row> datasetReglaEncuestaMision = odbd.getReglasEncuestaMision();
        Dataset<Row> filter1ReglasEncuestaMision = datasetReglaEncuestaMision.where(new Column(ReglasSegmento.MisionEncuesta.NombreColumnas.ID_LISTA.toString())
                .equalTo(idLista));
        
        Dataset<Row> datasetReglaPreferencia = odbd.getReglasPreferencia();
        Dataset<Row> filter1ReglasPreferencia = datasetReglaPreferencia.where(new Column(ReglasSegmento.Preferencia.NombreColumnas.ID_LISTA.toString())
                .equalTo(idLista));

        //definicion del esquema a usar en el data frame resultado de miembros eligibles (solo el uuid de miembros)
        Dataset<Row> resultado = spark.createDataFrame(javaContext.emptyRDD(), schema);
        
        JavaPairRDD<ImmutableBytesWritable, Result> eligibilidadMiembros = hbdbd.getEligibilidadMiembros();

        //recoleccion a memoria local del dataset como lista de "Row" y su recorrido
        List<Row> reglasMiembroAtb = filter1ReglasMiembroAtb.collectAsList();
        for (Row row : reglasMiembroAtb) {
            String idRegla = row.getAs(ReglasSegmento.AtributosMiembro.NombreColumnas.ID_REGLA.toString());
            try {
                Dataset<Row> miembros;
                switch(accion) {
                    case ACTUALIZAR: {
                        miembros = spark.createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.ATRIBUTO_MIEMBRO.getNombreColumna(idRegla)));
                                    if (value == null) {
                                        return false;
                                    } else {
                                        return Bytes.toBoolean(value);
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        break;
                    }
                    case RECALCULAR: {
                        miembros = new ProcesarReglaMiembroAtributo(spark, schema).inicio(row, null).cache();
                        
                        Dataset<Row> oldMembers = spark.createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.ATRIBUTO_MIEMBRO.getNombreColumna(idRegla)));
                                    if (value == null) {
                                        return false;
                                    } else {
                                        return Bytes.toBoolean(value);
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        JavaPairRDD<ImmutableBytesWritable, Put> resultadoRegla = miembros.withColumn("value", functions.lit(true))
                                .union(oldMembers.except(miembros).withColumn("value", functions.lit(false)))
                                .toJavaRDD()
                                .mapToPair((Row t) -> {
                                    Put put = new Put(Bytes.toBytes(t.getString(0)));
                                    put.add(new KeyValue(Bytes.toBytes(t.getString(0)), Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.ATRIBUTO_MIEMBRO.getNombreColumna(idRegla)), Bytes.toBytes(t.getBoolean(1))));
                                    return new Tuple2<>(new ImmutableBytesWritable(), put);
                                });
                        hbdbd.saveEligibilidadMiembro(resultadoRegla);
                        break;
                    }
                    default: {
                        throw new ListaReglasException("Accion no valida");
                    }
                }
                aReglasProcesadas.add(1);
                
                //la union directa de resultados del procesamiento de reglas y la eliminacion de duplicados
                resultado = resultado.union(miembros).distinct();
            } catch (Exception e) {
                System.out.println("Regla fallida: "+idRegla+"\npor la siguiente razon: "+e.getMessage());
                aReglasFallidas.add(1);
            }
        }
        List<Row> reglasMetricaBalance = filter1ReglasMetricaBalance.collectAsList();
        for (Row row : reglasMetricaBalance) {
            String idRegla = row.getAs(ReglasSegmento.BalanceMetrica.NombreColumnas.ID_REGLA.toString());
            try {
                Dataset<Row> miembros;
                switch(accion) {
                    case ACTUALIZAR: {
                        miembros = spark.createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.METRICA_BALANCE.getNombreColumna(idRegla)));
                                    if (value == null) {
                                        return false;
                                    } else {
                                        return Bytes.toBoolean(value);
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        break;
                    }
                    case RECALCULAR: {
                        miembros = new ProcesarReglaMetricaBalance(spark).inicio(row, null).cache();

                        Dataset<Row> oldMembers = spark.createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.METRICA_BALANCE.getNombreColumna(idRegla)));
                                    if (value == null) {
                                        return false;
                                    } else {
                                        return Bytes.toBoolean(value);
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        JavaPairRDD<ImmutableBytesWritable, Put> resultadoRegla = miembros.withColumn("value", functions.lit(true))
                                .union(oldMembers.except(miembros).withColumn("value", functions.lit(false)))
                                .toJavaRDD()
                                .mapToPair((Row t) -> {
                                    Put put = new Put(Bytes.toBytes(t.getString(0)));
                                    put.add(new KeyValue(Bytes.toBytes(t.getString(0)), Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.METRICA_BALANCE.getNombreColumna(idRegla)), Bytes.toBytes(t.getBoolean(1))));
                                    return new Tuple2<>(new ImmutableBytesWritable(), put);
                                });
                        hbdbd.saveEligibilidadMiembro(resultadoRegla);
                        break;
                    }
                    default: {
                        throw new ListaReglasException("Accion no valida");
                    }
                }
                aReglasProcesadas.add(1);
                
                //la union directa de resultados del procesamiento de reglas y la eliminacion de duplicados
                resultado = resultado.union(miembros).distinct();
            } catch (Exception e) {
                System.out.println("Regla fallida: "+idRegla+"\npor la siguiente razon: "+e.getMessage());
                aReglasFallidas.add(1);
            }
        }
        List<Row> reglasMetricaCambio = filter1ReglasMetricaCambio.collectAsList();
        for (Row row : reglasMetricaCambio) {
            String idRegla = row.getAs(ReglasSegmento.CambiosMetrica.NombreColumnas.ID_REGLA.toString());
            try {
                Dataset<Row> miembros;
                switch (accion) {
                    case ACTUALIZAR: {
                        miembros = spark.createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.METRICA_CAMBIO.getNombreColumna(idRegla)));
                                    if (value == null) {
                                        return false;
                                    } else {
                                        return Bytes.toBoolean(value);
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        break;
                    }
                    case RECALCULAR: {
                        miembros = new ProcesarReglaMetricaCambio(spark, schema).inicio(row, null).cache();
                
                        Dataset<Row> oldMembers = spark.createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.METRICA_CAMBIO.getNombreColumna(idRegla)));
                                    if (value == null) {
                                        return false;
                                    } else {
                                        return Bytes.toBoolean(value);
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        JavaPairRDD<ImmutableBytesWritable, Put> resultadoRegla = miembros.withColumn("value", functions.lit(true))
                                .union(oldMembers.except(miembros).withColumn("value", functions.lit(false)))
                                .toJavaRDD()
                                .mapToPair((Row t) -> {
                                    Put put = new Put(Bytes.toBytes(t.getString(0)));
                                    put.add(new KeyValue(Bytes.toBytes(t.getString(0)), Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.METRICA_CAMBIO.getNombreColumna(idRegla)), Bytes.toBytes(t.getBoolean(1))));
                                    return new Tuple2<>(new ImmutableBytesWritable(), put);
                                });
                        hbdbd.saveEligibilidadMiembro(resultadoRegla);
                        break;
                    }
                    default: {
                        throw new ListaReglasException("Accion no valida");
                    }
                }
                aReglasProcesadas.add(1);
                
                //la union directa de resultados del procesamiento de reglas y la eliminacion de duplicados
                resultado = resultado.union(miembros).distinct();
            } catch (Exception e) {
                System.out.println("Regla fallida: "+idRegla+"\npor la siguiente razon: "+e.getMessage());
                aReglasFallidas.add(1);
            }
        }
        List<Row> reglasAvanzada = filter1ReglasAvanzada.collectAsList();
        for (Row row : reglasAvanzada) {
            String idRegla = row.getAs(ReglasSegmento.Avanzada.NombreColumnas.ID_REGLA.toString());
            try {
                Dataset<Row> miembros;
                switch (accion) {
                    case ACTUALIZAR: {
                        miembros = spark.createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.AVANZADA.getNombreColumna(idRegla)));
                                    if (value == null) {
                                        return false;
                                    } else {
                                        return Bytes.toBoolean(value);
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        break;
                    }
                    case RECALCULAR: {
                        miembros = new ProcesarReglaAvanzada(spark, schema).inicio(row, null).cache();
                
                        Dataset<Row> oldMembers = spark.createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.AVANZADA.getNombreColumna(idRegla)));
                                    if (value == null) {
                                        return false;
                                    } else {
                                        return Bytes.toBoolean(value);
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        JavaPairRDD<ImmutableBytesWritable, Put> resultadoRegla = miembros.withColumn("value", functions.lit(true))
                                .union(oldMembers.except(miembros).withColumn("value", functions.lit(false)))
                                .toJavaRDD()
                                .mapToPair((Row t) -> {
                                    Put put = new Put(Bytes.toBytes(t.getString(0)));
                                    put.add(new KeyValue(Bytes.toBytes(t.getString(0)), Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.AVANZADA.getNombreColumna(idRegla)), Bytes.toBytes(t.getBoolean(1))));
                                    return new Tuple2<>(new ImmutableBytesWritable(), put);
                                });
                        hbdbd.saveEligibilidadMiembro(resultadoRegla);
                        break;
                    }
                    default: {
                        throw new ListaReglasException("Accion no valida");
                    }
                }
                aReglasProcesadas.add(1);
                
                //la union directa de resultados del procesamiento de reglas y la eliminacion de duplicados
                resultado = resultado.union(miembros).distinct();
            } catch (Exception e) {
                System.out.println("Regla fallida: "+idRegla+"\npor la siguiente razon: "+e.getMessage());
                aReglasFallidas.add(1);
            }
        }
        
        List<Row> reglasEncuestaMision = filter1ReglasEncuestaMision.collectAsList();
        for (Row row : reglasEncuestaMision) {
            String idRegla = row.getAs(ReglasSegmento.MisionEncuesta.NombreColumnas.ID_REGLA.toString());
            try {
                Dataset<Row> miembros;
                switch (accion) {
                    case ACTUALIZAR: {
                        miembros = spark.createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.ENCUESTA_MISION.getNombreColumna(idRegla)));
                                    if (value == null) {
                                        return false;
                                    } else {
                                        return Bytes.toBoolean(value);
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        break;
                    }
                    case RECALCULAR: {
                        miembros = new ProcesarReglaMisionEncuesta(spark, schema).inicio(row, null).cache();
                
                        Dataset<Row> oldMembers = spark.createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.ENCUESTA_MISION.getNombreColumna(idRegla)));
                                    if (value == null) {
                                        return false;
                                    } else {
                                        return Bytes.toBoolean(value);
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        JavaPairRDD<ImmutableBytesWritable, Put> resultadoRegla = miembros.withColumn("value", functions.lit(true))
                                .union(oldMembers.except(miembros).withColumn("value", functions.lit(false)))
                                .toJavaRDD()
                                .mapToPair((Row t) -> {
                                    Put put = new Put(Bytes.toBytes(t.getString(0)));
                                    put.add(new KeyValue(Bytes.toBytes(t.getString(0)), Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.ENCUESTA_MISION.getNombreColumna(idRegla)), Bytes.toBytes(t.getBoolean(1))));
                                    return new Tuple2<>(new ImmutableBytesWritable(), put);
                                });
                        hbdbd.saveEligibilidadMiembro(resultadoRegla);
                        break;
                    }
                    default: {
                        throw new ListaReglasException("Accion no valida");
                    }
                }
                aReglasProcesadas.add(1);
                
                //la union directa de resultados del procesamiento de reglas y la eliminacion de duplicados
                resultado = resultado.union(miembros).distinct();
            } catch (Exception e) {
                System.out.println("Regla fallida: "+idRegla+"\npor la siguiente razon: "+e.getMessage());
                aReglasFallidas.add(1);
            }
        }
        
        List<Row> reglasPreferencia = filter1ReglasPreferencia.collectAsList();
        for (Row row : reglasPreferencia) {
            String idRegla = row.getAs(ReglasSegmento.Preferencia.NombreColumnas.ID_REGLA.toString());
            try {
                Dataset<Row> miembros;
                switch (accion) {
                    case ACTUALIZAR: {
                        miembros = spark.createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.PREFERENCIA_MIEMBRO.getNombreColumna(idRegla)));
                                    if (value == null) {
                                        return false;
                                    } else {
                                        return Bytes.toBoolean(value);
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        break;
                    }
                    case RECALCULAR: {
                        miembros = new ProcesarReglaPreferencia(spark, schema).inicio(row, null).cache();
                
                        Dataset<Row> oldMembers = spark.createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.PREFERENCIA_MIEMBRO.getNombreColumna(idRegla)));
                                    if (value == null) {
                                        return false;
                                    } else {
                                        return Bytes.toBoolean(value);
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        JavaPairRDD<ImmutableBytesWritable, Put> resultadoRegla = miembros.withColumn("value", functions.lit(true))
                                .union(oldMembers.except(miembros).withColumn("value", functions.lit(false)))
                                .toJavaRDD()
                                .mapToPair((Row t) -> {
                                    Put put = new Put(Bytes.toBytes(t.getString(0)));
                                    put.add(new KeyValue(Bytes.toBytes(t.getString(0)), Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.PREFERENCIA_MIEMBRO.getNombreColumna(idRegla)), Bytes.toBytes(t.getBoolean(1))));
                                    return new Tuple2<>(new ImmutableBytesWritable(), put);
                                });
                        hbdbd.saveEligibilidadMiembro(resultadoRegla);
                        break;
                    }
                    default: {
                        throw new ListaReglasException("Accion no valida");
                    }
                }
                aReglasProcesadas.add(1);
                
                //la union directa de resultados del procesamiento de reglas y la eliminacion de duplicados
                resultado = resultado.union(miembros).distinct();
            } catch (Exception e) {
                System.out.println("Regla fallida: "+idRegla+"\npor la siguiente razon: "+e.getMessage());
                aReglasFallidas.add(1);
            }
        }
        
        //comprobacion de que la lista llegase a procesar al menos una regla
        if (reglasMetricaBalance.isEmpty() && reglasMetricaCambio.isEmpty() && reglasMiembroAtb.isEmpty() && reglasAvanzada.isEmpty() && reglasEncuestaMision.isEmpty() && reglasPreferencia.isEmpty()) {
            throw new ListaReglasException("Lista de reglas vacia");
        }
        
        return resultado;
    }
}
