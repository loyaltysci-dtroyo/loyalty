package com.flecharoja.loyalty.util;

/**
 *
 * @author svargas
 */
public enum TiposReglasSegmento {
    PREFERENCIA_MIEMBRO("F", "PRE_MIE_"),
    ATRIBUTO_MIEMBRO("C", "ATR_MIE_"),
    METRICA_CAMBIO("B", "MET_CAM_"),
    METRICA_BALANCE("A", "MET_BAL_"),
    AVANZADA("D", "AVA_"),
    ENCUESTA_MISION("E", "MIS_ENC_");

    private final String value, prefix;
    
    private TiposReglasSegmento(String value, String prefix) {
        this.value = value;
        this.prefix = prefix;
    }

    public String getValue() {
        return value;
    }

    public String getNombreColumna(String idRegla) {
        return prefix + idRegla;
    }
}
