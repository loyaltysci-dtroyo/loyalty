package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class ListaMiembrosSegmento {
    public static final String NOMBRE_TABLA = "SEGMENTO_LISTA_MIEMBRO";
    
    public enum NombreColumnas {
        ID_SEGMENTO,
        ID_MIEMBRO,
        IND_TIPO;
    }
    
    public enum ValoresIndTipo {
        EXCLUIDO("E"),
        INCLUIDO("I");
        
        public final String value;

        private ValoresIndTipo(String value) {
            this.value = value;
        }
    }
}
