package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.data.HBaseDBData;
import com.flecharoja.loyalty.data.MariaDBData;
import com.flecharoja.loyalty.util.TiposReglasSegmento;
import com.flecharoja.loyalty.util.indicadores.EligibilidadMiembro;
import com.flecharoja.loyalty.util.indicadores.ListaReglasSegmento;
import com.flecharoja.loyalty.util.indicadores.Miembro;
import com.flecharoja.loyalty.util.indicadores.ReglasSegmento;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import scala.Tuple2;

/**
 *
 * @author svargas
 */
public class AutoRecalculoReglaMiembroAtributo {

    public static void main(String[] args) throws InterruptedException, IOException {
        
        SparkConf sc = new SparkConf().setAppName("auto recalculo reglas miembros atributos").set("spark.cores.max", "2");
        JavaStreamingContext jsc = new JavaStreamingContext(sc, new Duration(15000));
        
        StructType schema; //esquema del dataframe a usar para resultados
        //el esquema contiene una unica columna que contrendra registros de identificadores de miembros
        schema = DataTypes.createStructType(new StructField[]{DataTypes.createStructField(Miembro.Atributos.ID_MIEMBRO.toString(), DataTypes.StringType, false)});
        
        MariaDBData odbd = new MariaDBData(SparkSession.builder().config(sc).getOrCreate());
        HBaseDBData hbdbd = new HBaseDBData(jsc.sparkContext());
        
        Map<String, Integer> topicMap = new HashMap<>();
        topicMap.put("calculo-reglas", 5);
        
        //propiedades de kafka
        Properties kpProperties = new Properties();
        Properties kcProperties = new Properties();
        kpProperties.load(AutoRecalculoReglaAvanzada.class.getResourceAsStream("/conf/kafka/kafka-producer.properties"));
        kcProperties.load(AutoRecalculoReglaAvanzada.class.getResourceAsStream("/conf/kafka/kafka-consumer.properties"));
        
        //lectura de mensajes desde kafka en los topics deseados
        JavaPairReceiverInputDStream<String, String> mensajes = KafkaUtils.createStream(jsc, kcProperties.getProperty("zookeeper.quorum"), "spark-streaming-AutoRecalculoReglaMiembroAtributo", topicMap);
        
        //subtraccion del contenido de los mensajes
        JavaDStream<String> contenidos = mensajes.map((Tuple2<String, String> t1) -> t1._2);
        
        try {
            contenidos.filter((String t1) -> {
                //filtrado de mensajes por peticiones de recalculo de reglas de atributos de miembro
                JsonNode json = new ObjectMapper().readTree(t1);
                String indTipoRegla = json.get("indTipoRegla").asText();
                return indTipoRegla.equals(TiposReglasSegmento.ATRIBUTO_MIEMBRO.getValue());
            }).mapToPair((String t) -> {
                //conversion a pares con lista de identificadores de miembros
                JsonNode json = new ObjectMapper().readTree(t);
                String idRegla = json.get("idRegla").asText();
                List<String> miembros = new ArrayList<>();
                JsonNode idMiembro = json.get("idMiembro");
                if (idMiembro!=null) {
                    miembros.add(idMiembro.asText());
                }
                return new Tuple2<>(idRegla, miembros);
            }).reduceByKey((List<String> t1, List<String> t2) -> {
                //reduccion de mensajes por identificador de regla
                if (t1.isEmpty() || t2.isEmpty()) {
                    return new ArrayList<>();
                }
                t1.addAll(t2);
                return t1;
            }).foreachRDD((JavaPairRDD<String, List<String>> t) -> {
                //obtencion de reglas de atributos de miembro
                Dataset<Row> reglasMiembroAtributoPorLista = odbd.getReglasMiembroAtributo();

                //obtencion de eligibilidad de miembros
                JavaPairRDD<ImmutableBytesWritable, Result> eligibilidadMiembros = hbdbd.getEligibilidadMiembros();

                //recoleccion de mensajes
                Map<String, List<String>> values = t.collectAsMap();

                if (!values.isEmpty()) {
                    //filtrado de reglas por reglas en los mensajes
                    Dataset<Row> filter1 = reglasMiembroAtributoPorLista.where(reglasMiembroAtributoPorLista.col(ReglasSegmento.AtributosMiembro.NombreColumnas.ID_REGLA.toString()).isin(values.keySet().stream().toArray(Object[]::new)));

                    for (Row row : filter1.collectAsList()) {
                        String idRegla = row.getAs(ReglasSegmento.AtributosMiembro.NombreColumnas.ID_REGLA.toString());

                        //procesamiento de regla
                        Dataset<Row> miembros = new ProcesarReglaMiembroAtributo(SparkSession.builder().config(sc).getOrCreate(), schema).inicio(row, values.get(idRegla).stream().distinct().collect(Collectors.toList()));

                        //obtencion de resultado de miembros pasados
                        Dataset<Row> oldMembers = SparkSession.builder().config(sc).getOrCreate().createDataFrame(eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                    if (values.get(idRegla).contains(Bytes.toString(t1._2.getRow()))) {
                                        byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.ATRIBUTO_MIEMBRO.getNombreColumna(idRegla)));
                                        if (value == null) {
                                            return false;
                                        } else {
                                            return Bytes.toBoolean(value);
                                        }
                                    } else {
                                        return false;
                                    }
                                }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                                schema
                        );
                        
                        //union de resultado actual con pasados (excluidos los actuales) con valores de true & false
                        JavaPairRDD<ImmutableBytesWritable, Put> resultadoRegla = miembros.withColumn("value", functions.lit(true))
                                .union(oldMembers.except(miembros).withColumn("value", functions.lit(false)))
                                .toJavaRDD()
                                .mapToPair((Row t1) -> {
                                    Put put = new Put(Bytes.toBytes(t1.getString(0)));
                                    put.add(new KeyValue(Bytes.toBytes(t1.getString(0)), Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.REGLAS.toString()), Bytes.toBytes(TiposReglasSegmento.ATRIBUTO_MIEMBRO.getNombreColumna(idRegla)), Bytes.toBytes(t1.getBoolean(1))));
                                    return new Tuple2<>(new ImmutableBytesWritable(), put);
                                });

                        //almacenamiento de eligibilidad de miembros
                        hbdbd.saveEligibilidadMiembro(resultadoRegla);

                        //producccion de mensaje para la actualizacion de segmento
                        try (KafkaProducer<Object, Object> kafkaProducer = new KafkaProducer<>(kpProperties)) {
                            odbd.getListasReglas().where(new Column(ListaReglasSegmento.NombreColumnas.ID_LISTA.toString()).equalTo(row.getAs(ReglasSegmento.AtributosMiembro.NombreColumnas.ID_LISTA.toString())))
                                    .collectAsList()
                                    .stream()
                                    .map((row1) -> row1.getAs(ListaReglasSegmento.NombreColumnas.ID_SEGMENTO.toString()))
                                    .forEachOrdered((idSegmento) -> {
                                        kafkaProducer.send(new ProducerRecord<>("actualizar-segmentos", idSegmento));
                                    });
                        }
                        
                        System.out.println("Regla Miembro Atributo recalculada = "+idRegla);
                    }
                }
            });
        } catch (Exception e) {
            System.out.println("error procesando el recalculo de regla... "+e.getMessage());
        }
        
        jsc.start();
        jsc.awaitTermination();
//        jsc.awaitTerminationOrTimeout(43200000);
        
    }
    
}
