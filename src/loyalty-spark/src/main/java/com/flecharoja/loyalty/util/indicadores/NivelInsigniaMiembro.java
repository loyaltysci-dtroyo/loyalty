package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class NivelInsigniaMiembro {
    public static final String NOMBRE_TABLA = "MIEMBRO_INSIGNIA_NIVEL";
    
    public enum NombreColumnas {
        ID_INSIGNIA,
        ID_NIVEL,
        ID_MIEMBRO;
    }
}
