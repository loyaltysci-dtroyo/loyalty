package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class PosicionesMiembrosTabPos {
    public static final String NOMBRE_TABLA = "TABPOS_MIEMBRO";
    
    public enum NombreColumnas {
        ID_TABLA,
        ID_MIEMBRO,
        ACUMULADO;
    }
}
