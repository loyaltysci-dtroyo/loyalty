package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class ComprasProductos {
    public static final String NOMBRE_TABLA = "COMPRA_PRODUCTO";
    
    public enum NombreColumnas {
        ID_TRANSACCION,
        ID_PRODUCTO,
        CANTIDAD;
    }
}
