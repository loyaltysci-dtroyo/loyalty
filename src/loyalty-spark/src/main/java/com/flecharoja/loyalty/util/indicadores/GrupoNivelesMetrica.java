package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class GrupoNivelesMetrica {
    public static final String NOMBRE_TABLA = "GRUPO_NIVELES";
    
    public enum NombreColumnas {
        ID_GRUPO_NIVEL;
    }
}
