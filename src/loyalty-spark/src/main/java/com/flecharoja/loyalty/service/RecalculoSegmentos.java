package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.data.HBaseDBData;
import com.flecharoja.loyalty.data.MariaDBData;
import com.flecharoja.loyalty.util.AccionSegmento;
import com.flecharoja.loyalty.util.indicadores.EligibilidadMiembro;
import com.flecharoja.loyalty.util.indicadores.EligiblesSegmento;
import com.flecharoja.loyalty.util.indicadores.ListaMiembrosSegmento;
import com.flecharoja.loyalty.util.indicadores.Miembro;
import com.flecharoja.loyalty.util.indicadores.Segmento;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.util.LongAccumulator;
import scala.Tuple2;

/**
 * Clase que se encarga de realizar el mantenimiento de miembros eligibles de
 * todos los segmentos eligibles por estado y ultima fecha de actualizacion
 *
 *
 * @author svargas
 */
public class RecalculoSegmentos {

    public static void main(String[] args) {

        SparkSession spark; //sesion de spark
        //establecimiento de la sesion de spark e inicializacion de la aplicacion bajo un nombre
        spark = SparkSession.builder().appName("loyalty - mantenimiento de todos los segmentos").getOrCreate();
        JavaSparkContext javaContext; //contexto de java spark
        javaContext = new JavaSparkContext(spark.sparkContext());

        StructType schema; //esquema del dataframe a usar para resultados
        //el esquema contiene una unica columna que contrendra registros de identificadores de miembros
        schema = DataTypes.createStructType(new StructField[]{DataTypes.createStructField(Miembro.Atributos.ID_MIEMBRO.toString(), DataTypes.StringType, false)});

        HBaseDBData hdbd; //instancia encargada de la carga de datos desde HBase
        hdbd = new HBaseDBData(javaContext);
        MariaDBData odbd; //instancia encargada de la carga de datos desde Oracle DB
        odbd = new MariaDBData(spark);

        LongAccumulator aReglasFallidas, aReglasProcesadas, aListasFallidas, aListasProcesadas;
        aReglasFallidas = javaContext.sc().longAccumulator();
        aReglasProcesadas = javaContext.sc().longAccumulator();
        aListasFallidas = javaContext.sc().longAccumulator();
        aListasProcesadas = javaContext.sc().longAccumulator();

        //lectura de registros de segmentos en la bases de datos
        Dataset<Row> datasetSegmentos = odbd.getSegmentos();

        //filtrado por indicadores de estatico (solo dinamicos) y de estado (solo activos) y la seleccion de solo los atributos de identificador y frecuencia de actualizacion
        Dataset<Row> filter1 = datasetSegmentos.where(new Column(Segmento.NombreColumnas.IND_ESTADO.toString()).equalTo(Segmento.ValoresIndEstado.Activo.value))
                .select(new Column(Segmento.NombreColumnas.ID_SEGMENTO.toString()), new Column(Segmento.NombreColumnas.FRECUENCIA_ACTUALIZACION.toString()));

        //recoleccion a memoria local del dataset como lista de "Row" y su recorrido
        for (Row row : filter1.collectAsList()) {
            String idSegmento = row.getAs(Segmento.NombreColumnas.ID_SEGMENTO.toString());//uuid del segmento dentro del Row

            //bandera para indicar si se debe recalcular el segmento (por defecto en verdadero)
            boolean flag = true;

            try {
                //obtencion de resultado de eligibles del segmento
                Result result = hdbd.getEligiblesSegmento(idSegmento);

                //se intenta obtener la fecha de actualizacion, de no estar presente se deja para que actualize los eligibles
                if (result.getValue(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.ULTIMA_ACTUALIZACION.toString())) != null) {
                    Calendar fechaActualizacion = Calendar.getInstance();//instancia de calendario para la fecha de actualizacion
                    fechaActualizacion.setTimeInMillis(Bytes.toLong(result.getValue(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.ULTIMA_ACTUALIZACION.toString()))));//establecimiento del valor de fecha de actualizacion

                    Long frecuencia = row.getAs(Segmento.NombreColumnas.FRECUENCIA_ACTUALIZACION.toString());//obtencion del valor de frecuencia de actualizacion en dias

                    Calendar fechaActual = Calendar.getInstance();//instancia de calendario con fecha actual

                    fechaActualizacion.add(Calendar.DATE, frecuencia.intValue());//agregado de numero de dias del valor de frecuencia a la fecha de ultima actualizacion

                    //si ambas fechas son en el mismo dia o la fecha actual sobrepasa a la fecha para actualizar... se marca a actualizar el segmento
                    flag = DateUtils.isSameDay(fechaActualizacion, fechaActual) || fechaActual.after(fechaActualizacion);
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
                System.exit(-1);
            }

            if (flag) {
                //obtencion de listados de miembros (solo uuids) eligibles por segmento (Row)
                Dataset<Row> resultado = new ProcesarSegmento(spark, schema, aReglasFallidas, aReglasProcesadas, aListasFallidas, aListasProcesadas).inicio(idSegmento, AccionSegmento.RECALCULAR);

                Dataset<Row> datasetMiembrosI = odbd.getMiembrosIncluidosExcluidos(idSegmento).where(new Column(ListaMiembrosSegmento.NombreColumnas.IND_TIPO.toString()).equalTo(ListaMiembrosSegmento.ValoresIndTipo.INCLUIDO.value)).select(new Column(ListaMiembrosSegmento.NombreColumnas.ID_MIEMBRO.toString()));
                resultado = resultado.union(datasetMiembrosI).distinct();

                Dataset<Row> datasetMiembrosE = odbd.getMiembrosIncluidosExcluidos(idSegmento).where(new Column(ListaMiembrosSegmento.NombreColumnas.IND_TIPO.toString()).equalTo(ListaMiembrosSegmento.ValoresIndTipo.EXCLUIDO.value)).select(new Column(ListaMiembrosSegmento.NombreColumnas.ID_MIEMBRO.toString()));
                resultado = resultado.except(datasetMiembrosE);

                JavaPairRDD<ImmutableBytesWritable, Result> eligibilidadMiembros = hdbd.getEligibilidadMiembros();
                Dataset<Row> oldMembers = spark.createDataFrame(
                        eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                            byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.SEGMENTOS.toString()), Bytes.toBytes(idSegmento));
                            if (value == null) {
                                return false;
                            } else {
                                return Bytes.toBoolean(value);
                            }
                        }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                        schema
                );
                
                resultado = resultado.withColumn("value", functions.lit(true))
                        .union(oldMembers.except(resultado).withColumn("value", functions.lit(false)))
                        .cache();
                
                JavaPairRDD<ImmutableBytesWritable, Put> resultadoSegmento = resultado.toJavaRDD()
                        .mapToPair((Row t) -> {
                            Put put = new Put(Bytes.toBytes(t.getString(0)));
                            put.addColumn(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.SEGMENTOS.toString()), Bytes.toBytes(idSegmento), Bytes.toBytes(t.getBoolean(1)));
                            return new Tuple2<>(new ImmutableBytesWritable(), put);
                        });
                
                List<Row> miembros = resultado.collectAsList();
                long count = 0;
                try {
                    Put put = new Put(Bytes.toBytes(idSegmento));
                    for (Row t : miembros) {
                        String idMiembro = t.getAs(Miembro.Atributos.ID_MIEMBRO.toString());//uuid del miembro dentro de cada Row de eligibles
                        boolean value = t.getAs("value");
                        put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.MIEMBROS.toString()), Bytes.toBytes(idMiembro), Bytes.toBytes(value));
                        if (value) {
                            count++;
                        }
                    }
                    //agregado de valores adicionales
                    put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.ULTIMA_ACTUALIZACION.toString()), Bytes.toBytes(Calendar.getInstance().getTimeInMillis()));
                    put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.CANTIDAD_MIEMBROS.toString()), Bytes.toBytes(count));
                    put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.LISTAS_PROCESADAS.toString()), Bytes.toBytes(aListasProcesadas.value()));
                    put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.LISTAS_FALLIDAS.toString()), Bytes.toBytes(aListasFallidas.value()));
                    put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.REGLAS_PROCESADAS.toString()), Bytes.toBytes(aReglasProcesadas.value()));
                    put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.REGLAS_FALLIDAS.toString()), Bytes.toBytes(aReglasFallidas.value()));

                    hdbd.saveEligibilidadMiembro(resultadoSegmento);
                    hdbd.saveEligiblesSegmento(put);
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    System.exit(-1);
                }

                aListasFallidas.reset();
                aListasProcesadas.reset();
                aReglasProcesadas.reset();
                aReglasFallidas.reset();
            }
        }
    }
}
