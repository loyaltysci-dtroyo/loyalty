package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class RegistrosMisiones {
    public static final String NOMBRE_TABLA = "REGISTRO_MISION";
    
    public enum ColumnasFamilia {
        DATA;
    }
    
    public enum ColumnasDATA {
        RESPUESTA,
        ESTADO;
    }
    
    public enum ValoresDATA_ESTADO {
        SIN_REVISION("S"),
        APROBADO("A"),
        RECHAZADO("R");
        
        public final String value;

        private ValoresDATA_ESTADO(String value) {
            this.value = value;
        }
    }
}
