package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class MarcadoresPromociones {
    public static final String NOMBRE_TABLA = "MARCADOR_PROMOCION";
    
    public enum NombreColumnas {
        ID_PROMOCION,
        ID_MIEMBRO,
        FECHA;
    }
}
