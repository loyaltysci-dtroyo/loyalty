package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class AtributoDinamico {
    public static final String NOMBRE_TABLA = "ATRIBUTO_DINAMICO";
    
    public enum NombreColumnas {
        ID_ATRIBUTO,
        VALOR_DEFECTO,
        IND_TIPO_DATO;
    }
    
    public enum ValoresIndTipoDato {
        TEXTO("T"),
        NUMERICO("N"),
        BOOLEANO("B"),
        FECHA("F");
        
        public final String value;

        private ValoresIndTipoDato(String value) {
            this.value = value;
        }
    }
}
