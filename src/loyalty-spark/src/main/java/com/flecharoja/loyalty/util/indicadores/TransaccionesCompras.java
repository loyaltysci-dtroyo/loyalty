package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class TransaccionesCompras {
    public static final String NOMBRE_TABLA = "TRANSACCION_COMPRA";
    
    public enum NombreColumnas {
        ID_TRANSACCION,
        ID_MIEMBRO,
        ID_UBICACION,
        FECHA,
        TOTAL;
    }
}
