package com.flecharoja.loyalty.util.indicadores;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author svargas
 */
public class TablaPosiciones {
    public static final String NOMBRE_TABLA = "TABLA_POSICIONES";
    
    public enum NombreColumnas {
        ID_TABLA,
        IND_TIPO;
    }
    
    public enum ValoresIndTipo {
        GLOBAL_INDIVIDUAL("U"),
        GLOBAL_GRUPAL("G"),
        GRUPO_MIEMBRO("E");
        
        public final String value;
        private static final Map<String, ValoresIndTipo> lookup = new HashMap<>();

        private ValoresIndTipo(String value) {
            this.value = value;
        }
        
        static {
            for (ValoresIndTipo valor : values()) {
                lookup.put(valor.value, valor);
            }
        }
        
        public static ValoresIndTipo get (String value) {
            return value==null?null:lookup.get(value);
        }
    }
}
