package com.flecharoja.loyalty.util;

import java.text.SimpleDateFormat;

/**
 *
 * @author svargas, wtencio
 */
public class Indicadores {
     
    public static final char INCLUIDO = 'I';
    public static final char EXCLUIDO = 'E';
    public static final char ASIGNADO = 'A';
    public static final char DISPONIBLES = 'D';
    
    public static final SimpleDateFormat ANGULAR_DATE_STRING_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
}
