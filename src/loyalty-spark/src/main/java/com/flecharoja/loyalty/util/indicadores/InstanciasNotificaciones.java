package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class InstanciasNotificaciones {
    public static final String NOMBRE_TABLA = "INSTANCIA_NOTIFICACION";
    
    public enum NombreColumnas {
        ID_MIEMBRO,
        ID_NOTIFICACION,
        FECHA_ENVIO,
        FECHA_ENTREGA,
        FECHA_VISTO;
    }
}
