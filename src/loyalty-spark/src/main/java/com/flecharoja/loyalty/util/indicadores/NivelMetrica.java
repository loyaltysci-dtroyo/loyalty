package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class NivelMetrica {
    public static final String NOMBRE_TABLA = "NIVEL_METRICA";
    
    public enum NombreColumnas {
        ID_NIVEL,
        GRUPO_NIVELES,
        METRICA_INICIAL;
    }
}
