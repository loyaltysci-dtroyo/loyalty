package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class Segmento {
    public static final String NOMBRE_TABLA = "SEGMENTO";
    
    public enum NombreColumnas {
        ID_SEGMENTO,
        IND_ESTATICO,
        IND_ESTADO,
        FRECUENCIA_ACTUALIZACION;
    }
    
    public enum ValoresIndEstado {
        Activo("AC");

        public final String value;
        
        private ValoresIndEstado(String value) {
            this.value = value;
        }
    }
    
    public enum ValoresIndEstatico {
        Dinamico("D");

        public final String value;
        
        private ValoresIndEstatico(String value) {
            this.value = value;
        }
    }
}
