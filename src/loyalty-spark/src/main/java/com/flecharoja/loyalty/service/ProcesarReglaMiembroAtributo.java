package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.data.MariaDBData;
import com.flecharoja.loyalty.exception.ReglaException;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.indicadores.AtributoDinamico;
import com.flecharoja.loyalty.util.indicadores.BalanceMetricaMiembro;
import com.flecharoja.loyalty.util.indicadores.GrupoNivelesMetrica;
import com.flecharoja.loyalty.util.indicadores.Metrica;
import com.flecharoja.loyalty.util.indicadores.Miembro;
import com.flecharoja.loyalty.util.indicadores.NivelMetrica;
import com.flecharoja.loyalty.util.indicadores.NivelMetricaInicialMiembro;
import com.flecharoja.loyalty.util.indicadores.ReglasSegmento;
import com.flecharoja.loyalty.util.indicadores.ValorAtbDinMiembro;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.StructType;

/**
 * Clase que realiza el procesamiento de una regla del tipo de atributo de
 * miembro
 *
 * @author svargas
 */
public class ProcesarReglaMiembroAtributo {

    private final SparkSession spark; //sesion de spark
    private final JavaSparkContext javaContext; //contexto de java spark
    private final StructType schema; //esquema del dataframe a usar para resultados

    private final MariaDBData odbd; //instancia encargada de la carga de datos desde Oracle DB

    public ProcesarReglaMiembroAtributo(SparkSession spark, StructType schema) {
        this.schema = schema;
        this.spark = spark;
        this.javaContext = new JavaSparkContext(spark.sparkContext());

        this.odbd = new MariaDBData(spark);
    }

    public Dataset<Row> inicio(Row regla, List<String> miembros) throws ReglaException {
        //obtencion de meta de la regla
        String indOperador = regla.getAs(ReglasSegmento.AtributosMiembro.NombreColumnas.IND_OPERADOR.toString());
        String indAtributo = regla.getAs(ReglasSegmento.AtributosMiembro.NombreColumnas.IND_ATRIBUTO.toString());
        String valorComparacion = regla.getAs(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString());
        String idNivelMetrica = regla.getAs(ReglasSegmento.AtributosMiembro.NombreColumnas.ID_NIVEL_METRICA.toString());
        String idMetrica = regla.getAs(ReglasSegmento.AtributosMiembro.NombreColumnas.ID_METRICA.toString());
        String idAtributoDinamico = regla.getAs(ReglasSegmento.AtributosMiembro.NombreColumnas.ID_ATRIBUTO_DINAMICO.toString());

        //si el atributo es referente al del nivel en progreso del miembro...
        if (Miembro.Atributos.get(indAtributo).equals(Miembro.Atributos.TIER_PROGRESO)) {
            //se obtienen los niveles de metrica
            Dataset<Row> datasetNiveles = odbd.getNivelesMetrica();
            //obtiene el nivel de una metrica (deberia ser uno) por el identificador de nivel y seleccionando el identificador de metrica y el valor de metrica inicial
            Dataset<Row> filter1 = datasetNiveles.where(new Column(NivelMetrica.NombreColumnas.ID_NIVEL.toString())
                    .equalTo(idNivelMetrica))
                    .select(new Column(NivelMetrica.NombreColumnas.METRICA_INICIAL.toString()));

            //se recolectan a memoria local y se recorren
            List<Row> collectAsList = filter1.collectAsList();
            if (!collectAsList.isEmpty()) {
                Double metricaInicial = collectAsList.get(0).getAs(NivelMetrica.NombreColumnas.METRICA_INICIAL.toString());//valor de la metrica inicial del nivel
                
                Dataset<Row> filter = odbd.getBalancesMetricaMiembro().where(new Column(BalanceMetricaMiembro.NombreColumnas.ID_METRICA.toString()).equalTo(idMetrica)).filter((row) -> {
                    if (miembros == null || miembros.isEmpty() || miembros.contains((String)row.getAs(BalanceMetricaMiembro.NombreColumnas.ID_MIEMBRO.toString()))) {
                        Double progreso = row.getAs(BalanceMetricaMiembro.NombreColumnas.PROGRESO_ACTUAL.toString());

                        //segun el indicador de operador se realiza una comparacion numerica de la metrica inicial con el de progreso del miembro
                        switch (ReglasSegmento.ValoresIndOperador.get(indOperador)) {
                            case IGUAL: {
                                return metricaInicial.compareTo(progreso) == 0;
                            }
                            case MAYOR: {
                                return progreso.compareTo(metricaInicial) == 1;
                            }
                            case MAYOR_IGUAL: {
                                return progreso.compareTo(metricaInicial) >= 0;
                            }
                            case MENOR: {
                                return progreso.compareTo(metricaInicial) == -1;
                            }
                            case MENOR_IGUAL: {
                                return progreso.compareTo(metricaInicial) <= 0;
                            }
                            default: {
                                throw new ReglaException("Indicador de operador no valido");
                            }
                        }
                    }
                    return false;
                });
                
                return filter.select(new Column(BalanceMetricaMiembro.NombreColumnas.ID_MIEMBRO.toString()));
            } else {
                //se retorna un dataframe vacio
                return spark.createDataFrame(javaContext.emptyRDD(), schema);
            }
        } else {
            //se obtienen los miembros
            Dataset<Row> datasetMiembros = odbd.getMiembros()
                    .filter((Row t) -> {
                        String idMiembro = t.getAs(Miembro.Atributos.ID_MIEMBRO.toString());
                        return miembros == null || miembros.isEmpty() || miembros.contains(idMiembro);
                    });

            //si el atributo es del nivel inicial de miembro
            switch (Miembro.Atributos.get(indAtributo)) {
                case TIER_INICIAL: {
                    
                    //se obtienen los niveles de metrica
                    Dataset<Row> datasetNiveles = odbd.getNivelesMetrica();
                    //obtiene el nivel de una metrica (deberia ser uno) por el identificador de nivel y seleccionando el identificador de metrica y el valor de metrica inicial
                    Dataset<Row> filter1 = datasetNiveles.where(new Column(NivelMetrica.NombreColumnas.ID_NIVEL.toString())
                            .equalTo(idNivelMetrica))
                            .select(new Column(NivelMetrica.NombreColumnas.METRICA_INICIAL.toString()));
                    List<Row> collectAsList = filter1.collectAsList();
                    
                    //memntras se haya encontrado el nivel de metrica
                    if (!collectAsList.isEmpty()) {
                        Double metricaInicial = collectAsList.get(0).getAs(NivelMetrica.NombreColumnas.METRICA_INICIAL.toString());//valor de la metrica inicial del nivel
                        
                        Dataset<Row> datasetMiembroNivelMetrica = odbd.getMiembrosNivelesMetrica();//obtencion de los niveles de metricas asignados a miembros
                        Dataset<Row> datasetNivelMetrica = odbd.getNivelesMetrica();//obtencion de los niveles de metrica
                        Dataset<Row> datasetGrupoNivelesMetrica = odbd.getGrupoNivelesMetrica();//obtencion de los grupos de niveles
                        Dataset<Row> datasetMetricas = odbd.getMetricas();//obtencion de las metricas
                        
                        //obtiene los niveles de una metrsica por el identificador de metrica y seleccionando el identificador de nivel y el valor de metrica inicial
                        String idGrupo = datasetMetricas
                                .where(new Column(Metrica.NombreColumnas.ID_METRICA.toString()).equalTo(idMetrica))
                                .join(datasetGrupoNivelesMetrica, datasetMetricas.col(Metrica.NombreColumnas.GRUPO_NIVELES.toString()).equalTo(datasetGrupoNivelesMetrica.col(GrupoNivelesMetrica.NombreColumnas.ID_GRUPO_NIVEL.toString())))
                                .select(datasetGrupoNivelesMetrica.col(GrupoNivelesMetrica.NombreColumnas.ID_GRUPO_NIVEL.toString()))
                                .collectAsList().get(0).getAs(GrupoNivelesMetrica.NombreColumnas.ID_GRUPO_NIVEL.toString());
                        
                        //seleccion del identificador de nivel y valor de metrica inicial donde el identificador de grupo sea igual al de la regla
                        Dataset<Row> filter2 = datasetNivelMetrica
                                .where(new Column(NivelMetrica.NombreColumnas.GRUPO_NIVELES.toString()).equalTo(idGrupo))
                                .select(datasetNivelMetrica.col(NivelMetrica.NombreColumnas.ID_NIVEL.toString()), new Column(NivelMetrica.NombreColumnas.METRICA_INICIAL.toString()));
                        
                        //join de los niveles de metrica asociados a un miembro (de la metrica de la regla) con el filtrado anterior
                        Dataset<Row> filter3 = datasetMiembroNivelMetrica
                                .where(new Column(NivelMetricaInicialMiembro.NombreColumnas.ID_METRICA.toString()).equalTo(idMetrica))
                                .join(filter2, datasetMiembroNivelMetrica.col(NivelMetricaInicialMiembro.NombreColumnas.ID_NIVEL.toString()).equalTo(filter2.col(NivelMetrica.NombreColumnas.ID_NIVEL.toString())), "inner")
                                .drop(filter2.col(NivelMetrica.NombreColumnas.ID_NIVEL.toString()));
                        
                        //join de los miebros con el filtrado anterior donde el identificador del miembro sea igual al del filtro (manteniendo los miembros sin registro encontrado)
                        Dataset<Row> filter4 = datasetMiembros
                                .select(new Column(Miembro.Atributos.ID_MIEMBRO.toString()))
                                .join(filter3, datasetMiembros.col(Miembro.Atributos.ID_MIEMBRO.toString()).equalTo(filter3.col(NivelMetricaInicialMiembro.NombreColumnas.ID_MIEMBRO.toString())), "left")
                                .drop(filter3.col(NivelMetricaInicialMiembro.NombreColumnas.ID_MIEMBRO.toString()));
                        
                        //llenado del valor de metrica inicial del filtrado anterior con 0 para los miembros sin la metrica inicial encontrada
                        filter4 = filter4.select(datasetMiembros.col(Miembro.Atributos.ID_MIEMBRO.toString()), new Column(NivelMetrica.NombreColumnas.METRICA_INICIAL.toString())).na().fill(0);
                        
                        //se filtra el resultado....
                        Dataset<Row> resultado = filter4.filter((Row t) -> {
                            Double metricaInicialMiembro = t.getAs(NivelMetrica.NombreColumnas.METRICA_INICIAL.toString());//se obtiene la metrica inicial del miembro
                            
                            //segun el indicador de operador se realiza la operacion numerico entre la metrica inicial del miembro con la metrica inicial del nivel de la regla
                            switch (ReglasSegmento.ValoresIndOperador.get(indOperador)) {
                                case IGUAL: {
                                    return metricaInicialMiembro.compareTo(metricaInicial) == 0;
                                }
                                case MAYOR: {
                                    return metricaInicialMiembro.compareTo(metricaInicial) == 1;
                                }
                                case MAYOR_IGUAL: {
                                    return metricaInicialMiembro.compareTo(metricaInicial) >= 0;
                                }
                                case MENOR: {
                                    return metricaInicialMiembro.compareTo(metricaInicial) == -1;
                                }
                                case MENOR_IGUAL: {
                                    return metricaInicialMiembro.compareTo(metricaInicial) <= 0;
                                }
                                default: {
                                    throw new ReglaException("Indicador de operador no valido");
                                }
                            }
                        });
                        
                        //se retorna la seleccion del identificador de miembro del resultado final
                        return resultado.select(resultado.col(Miembro.Atributos.ID_MIEMBRO.toString()));
                    } else {
                        //se retorna un dataset vacio
                        return spark.createDataFrame(javaContext.emptyRDD(), schema);
                    }
                    
                    //si el atributo es referente al de un atributo dinamico...
                }
                case ATRIBUTO_DINAMICO: {
                    Dataset<Row> datasetAtributosDinamicos = odbd.getAtributosDinamicos();//se obtiene los atributos dinamicos
                    //se obtiene los valores alterados del atributo dinamico por el miembro
                    Dataset<Row> datasetMiembrosAtributosDinamicos = odbd.getMiembrosAtributosDinamicos()
                            .select(new Column(ValorAtbDinMiembro.NombreColumnas.ID_MIEMBRO.toString()), new Column(ValorAtbDinMiembro.NombreColumnas.ID_ATRIBUTO.toString()), new Column(ValorAtbDinMiembro.NombreColumnas.VALOR.toString()))
                            .withColumnRenamed(ValorAtbDinMiembro.NombreColumnas.ID_MIEMBRO.toString(), ValorAtbDinMiembro.NombreColumnas.ID_MIEMBRO.toString().concat("2"))
                            .withColumnRenamed(ValorAtbDinMiembro.NombreColumnas.ID_ATRIBUTO.toString(), ValorAtbDinMiembro.NombreColumnas.ID_ATRIBUTO.toString().concat("2"));
                    
                    //union de los miembros (solo el id) con el atributo dinamico (por su id)
                    Dataset<Row> filter1 = datasetMiembros
                            .select(new Column(Miembro.Atributos.ID_MIEMBRO.toString()))
                            .crossJoin(datasetAtributosDinamicos.where(datasetAtributosDinamicos.col(AtributoDinamico.NombreColumnas.ID_ATRIBUTO.toString()).equalTo(idAtributoDinamico)))
                            .na().drop();
                    
                    //union con los atributos dinamicos resueltos por miembros, y seleccion de columnas requeridas
                    Dataset<Row> filter2 = filter1
                            .join(
                                    datasetMiembrosAtributosDinamicos,
                                    filter1.col(Miembro.Atributos.ID_MIEMBRO.toString()).equalTo(datasetMiembrosAtributosDinamicos.col(ValorAtbDinMiembro.NombreColumnas.ID_MIEMBRO.toString().concat("2")))
                                            .and(datasetMiembrosAtributosDinamicos.col(ValorAtbDinMiembro.NombreColumnas.ID_ATRIBUTO.toString().concat("2")).equalTo(idAtributoDinamico)),
                                    "left_outer"
                            ).select(
                                    filter1.col(ValorAtbDinMiembro.NombreColumnas.ID_MIEMBRO.toString()),
                                    filter1.col(AtributoDinamico.NombreColumnas.IND_TIPO_DATO.toString()),
                                    filter1.col(AtributoDinamico.NombreColumnas.VALOR_DEFECTO.toString()),
                                    datasetMiembrosAtributosDinamicos.col(ValorAtbDinMiembro.NombreColumnas.VALOR.toString())
                            );
                    
                    Dataset<Row> resultado = filter2.filter((Row t) -> {
                        String indTipoDato = t.getAs(AtributoDinamico.NombreColumnas.IND_TIPO_DATO.toString());
                        //se obtiene el valor a comparar como el valor por defecto si no esta presente el valor puesto por el miembro
                        String valor = t.getAs(ValorAtbDinMiembro.NombreColumnas.VALOR.toString()) == null ? t.getAs(AtributoDinamico.NombreColumnas.VALOR_DEFECTO.toString()) : t.getAs(ValorAtbDinMiembro.NombreColumnas.VALOR.toString());
                        
                        if (indTipoDato.equals(AtributoDinamico.ValoresIndTipoDato.FECHA.value)) {
                            Date fechaAtributo, fechaComparacion;
                            try {
                                fechaAtributo = new Date(Long.parseLong(valor));
                                fechaComparacion = Indicadores.ANGULAR_DATE_STRING_FORMAT.parse(valorComparacion);
                            } catch (ParseException | NumberFormatException e) {
                                throw new ReglaException("Valores de fechas incorrectos");
                            }
                            switch (ReglasSegmento.ValoresIndOperador.get(indOperador)) {
                                case FECHA_ANTES_DE: {
                                    return fechaAtributo.before(fechaComparacion) || fechaAtributo.equals(fechaComparacion);
                                }
                                case FECHA_A_PARTIR_DE: {
                                    return fechaAtributo.after(fechaComparacion) || fechaAtributo.equals(fechaComparacion);
                                }
                                case FECHA_IGUAL: {
                                    return DateUtils.isSameDay(fechaComparacion, fechaAtributo);
                                }
                                default: {
                                    throw new ReglaException("Indicador de operador no valido");
                                }
                            }
                        } else {
                            //segun el inicador de operador se realiza una operacion sobre el valor de comparacion
                            switch (ReglasSegmento.ValoresIndOperador.get(indOperador)) {
                                case ES: {
                                    return valor.toUpperCase().trim().equals(valorComparacion.toUpperCase().trim());
                                }
                                case NO_ES: {
                                    return !valor.toUpperCase().trim().equals(valorComparacion.toUpperCase().trim());
                                }
                                case INICIA_CON: {
                                    return valor.toUpperCase().trim().startsWith(valorComparacion.toUpperCase().trim());
                                }
                                case TERMINA_CON: {
                                    return valor.toUpperCase().trim().endsWith(valorComparacion.toUpperCase().trim());
                                }
                                case CONTIENE: {
                                    return valor.toUpperCase().trim().contains(valorComparacion.toUpperCase().trim());
                                }
                                case NO_CONTIENE: {
                                    return !valor.toUpperCase().trim().contains(valorComparacion.toUpperCase().trim());
                                }
                                case ESTA_VACIO: {
                                    return valor.toUpperCase().trim().equals(t.getAs(AtributoDinamico.NombreColumnas.VALOR_DEFECTO.toString().toUpperCase().trim()));
                                }
                                case NO_ESTA_VACIO: {
                                    return !valor.toUpperCase().trim().equals(t.getAs(AtributoDinamico.NombreColumnas.VALOR_DEFECTO.toString().toUpperCase().trim()));
                                }
                                case IGUAL: {
                                    try {
                                        return Double.parseDouble(valor)==Double.parseDouble(valorComparacion);
                                    } catch (NumberFormatException e) {
                                        return valor.equals(valorComparacion);
                                    }
                                }
                                case MAYOR: {
                                    try {
                                        return Double.parseDouble(valor)>Double.parseDouble(valorComparacion);
                                    } catch (NumberFormatException e) {
                                        return valor.compareTo(valorComparacion) == 1;
                                    }
                                }
                                case MAYOR_IGUAL: {
                                    try {
                                        return Double.parseDouble(valor)>=Double.parseDouble(valorComparacion);
                                    } catch (NumberFormatException e) {
                                        return valor.compareTo(valorComparacion) >= 0;
                                    }
                                }
                                case MENOR: {
                                    try {
                                        return Double.parseDouble(valor)<Double.parseDouble(valorComparacion);
                                    } catch (NumberFormatException e) {
                                        return valor.compareTo(valorComparacion) == -1;
                                    }
                                }
                                case MENOR_IGUAL: {
                                    try {
                                        return Double.parseDouble(valor)<=Double.parseDouble(valorComparacion);
                                    } catch (NumberFormatException e) {
                                        return valor.compareTo(valorComparacion) <= 0;
                                    }
                                }
                                default: {
                                    throw new ReglaException("Indicador de operador no valido");
                                }
                            }
                        }
                    }).select(new Column(Miembro.Atributos.ID_MIEMBRO.toString()));//seleccion del identificador de miembro
                    
                    //regreso del resultado final
                    return resultado;
                }
                case FECHA_INGRESO:
                case FECHA_NACIMIENTO: {
                    Dataset<Row> resultado = datasetMiembros.filter((row) -> {
                        Date fechaAtributo, fechaComparacion;
                        fechaAtributo = row.getAs(Miembro.Atributos.FECHA_NACIMIENTO.toString());
                        switch (ReglasSegmento.ValoresIndOperador.get(indOperador)) {
                            case FECHA_ANTES_DE: {
                                try {
                                    fechaComparacion = Indicadores.ANGULAR_DATE_STRING_FORMAT.parse(valorComparacion);
                                } catch (ParseException e) {
                                    throw new ReglaException("Valores de fechas incorrectos");
                                }
                                if (fechaAtributo!=null) {
                                    return fechaAtributo.before(fechaComparacion) || fechaAtributo.equals(fechaComparacion);
                                }
                                return false;
                            }
                            case FECHA_A_PARTIR_DE: {
                                try {
                                    fechaComparacion = Indicadores.ANGULAR_DATE_STRING_FORMAT.parse(valorComparacion);
                                } catch (ParseException e) {
                                    throw new ReglaException("Valores de fechas incorrectos");
                                }
                                if (fechaAtributo!=null) {
                                    return fechaAtributo.after(fechaComparacion) || fechaAtributo.equals(fechaComparacion);
                                }
                                return false;
                            }
                            case IGUAL:
                            case FECHA_IGUAL: {
                                try {
                                    fechaComparacion = Indicadores.ANGULAR_DATE_STRING_FORMAT.parse(valorComparacion);
                                } catch (ParseException e) {
                                    throw new ReglaException("Valores de fechas incorrectos");
                                }
                                if (fechaAtributo!=null) {
                                    return DateUtils.isSameDay(fechaComparacion, fechaAtributo);
                                }
                                return false;
                            }
                            case ESTA_VACIO: {
                                return fechaAtributo==null;
                            }
                            case NO_ESTA_VACIO: {
                                return fechaAtributo!=null;
                            }
                            default: {
                                throw new ReglaException("Indicador de operador no valido");
                            }
                        }
                    }).select(new Column(Miembro.Atributos.ID_MIEMBRO.toString())); //seleccion del identificador de miembro
                    
                    return resultado;
                }
                default: {
                    //si el indicador de operador es del tipo "es uno de"...
                    if (indOperador.equals(ReglasSegmento.ValoresIndOperador.ES_UNO_DE.value)) {
                        //se realiza la operacion directamente con los miembros
                        Dataset<Row> resultado = datasetMiembros.withColumn(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString(), functions.lit(valorComparacion)).filter(new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString()).isin(new Column(Miembro.Atributos.get(indAtributo).toString())));
                        return resultado.select(Miembro.Atributos.ID_MIEMBRO.toString());
                    }
                    //si el indicador de operador es del tipo "no es uno de"...
                    if (indOperador.equals(ReglasSegmento.ValoresIndOperador.NO_ES_UNO_DE.value)) {
                        //se realiza la operacion directamente con los miembros (el negativo del anterior)
                        Dataset<Row> resultado = datasetMiembros.withColumn(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString(), functions.lit(valorComparacion)).filter(functions.not(new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString()).isin(new Column(Miembro.Atributos.get(indAtributo).toString()))));
                        return resultado.select(Miembro.Atributos.ID_MIEMBRO.toString());
                    }
                    
                    //se forma la operacion a realizar del atributo y operador por sus indicadores
                    String operacion = getOperacion(indAtributo, indOperador); //variable que contiene la operacion a realizar al procesar los ind de atributo y operador
                    //si no se supo que operacion hacer...
                    if (operacion == null) {
                        //en el caso de que no se encuentre atributo u operacion a referir (NO DEBERIA PASAR)
                        throw new ReglaException("No se pudo formar operacion valida");
                    }
                    //en el caso de que el atributo sea acerca de valores obtenibles dentro de miembros
                    Dataset<Row> resultado = datasetMiembros.withColumn(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString(), functions.lit(valorComparacion)).where(operacion)
                            .select(Miembro.Atributos.ID_MIEMBRO.toString());
                    return resultado;
                }
            }
        }
    }

    //este metodo traduce indicadores de atributo y operador a una operacion valida SQL, usando como valor de comparacion de ser necesario una columna llamada "VALOR_COMPARACION"
    private String getOperacion(String indAtributo, String indOperador) {
        String atributo = Miembro.Atributos.get(indAtributo).toString();
        if (atributo == null) {
            return null;
        }
        //segun el inicador de operador se retorna una cadena con la operacion a realizar sobre el valor de comparacion
        switch (ReglasSegmento.ValoresIndOperador.get(indOperador)) {
            case ES: {
                return "UPPER(TRIM(" + atributo + ")) = UPPER(TRIM(" + new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString()) + "))";
            }
            case NO_ES: {
                return "UPPER(TRIM(" + atributo + ")) <> UPPER(TRIM(" + new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString()) + "))";
            }
            case INICIA_CON: {
                return "UPPER(TRIM(" + atributo + ")) like UPPER(TRIM( CONCAT(" + new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString()) + ",'%') ))";
            }
            case TERMINA_CON: {
                return "UPPER(TRIM(" + atributo + ")) like UPPER(TRIM( CONCAT('%'," + new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString()) + ") ))";
            }
            case CONTIENE: {
                return "UPPER(TRIM(" + atributo + ")) like UPPER(TRIM( CONCAT('%'," + new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString()) + ",'%') ))";
            }
            case NO_CONTIENE: {
                return "UPPER(TRIM(" + atributo + ")) not like UPPER(TRIM( CONCAT('%'," + new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString()) + ",'%') ))";
            }
            case ESTA_VACIO: {
                return atributo + " is null";
            }
            case NO_ESTA_VACIO: {
                return atributo + " is not null";
            }
            case IGUAL: {
                return "UPPER(TRIM(" + atributo + ")) = UPPER(TRIM(" + new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString()) + "))";
            }
            case MAYOR: {
                return atributo + " > " + new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString());
            }
            case MAYOR_IGUAL: {
                return atributo + " >= " + new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString());
            }
            case MENOR: {
                return atributo + " < " + new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString());
            }
            case MENOR_IGUAL: {
                return atributo + " <= " + new Column(ReglasSegmento.AtributosMiembro.NombreColumnas.VALOR_COMPARACION.toString());
            }
            default: {
                return null;
            }
        }
    }
}
