package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.data.HBaseDBData;
import com.flecharoja.loyalty.data.MariaDBData;
import com.flecharoja.loyalty.exception.ReglaException;
import com.flecharoja.loyalty.util.indicadores.BalanceMetricaMiembro;
import com.flecharoja.loyalty.util.indicadores.ComprasProductos;
import com.flecharoja.loyalty.util.indicadores.EligibilidadMiembro;
import com.flecharoja.loyalty.util.indicadores.InstanciasNotificaciones;
import com.flecharoja.loyalty.util.indicadores.ListaMiembrosGrupoMiembro;
import com.flecharoja.loyalty.util.indicadores.MarcadoresPromociones;
import com.flecharoja.loyalty.util.indicadores.Miembro;
import com.flecharoja.loyalty.util.indicadores.NivelInsigniaMiembro;
import com.flecharoja.loyalty.util.indicadores.NivelMetrica;
import com.flecharoja.loyalty.util.indicadores.PosicionesGruposTabPos;
import com.flecharoja.loyalty.util.indicadores.PosicionesMiembrosTabPos;
import com.flecharoja.loyalty.util.indicadores.PremiosRedimidos;
import com.flecharoja.loyalty.util.indicadores.RegistrosMisiones;
import com.flecharoja.loyalty.util.indicadores.ReglasSegmento;
import com.flecharoja.loyalty.util.indicadores.TablaPosiciones;
import com.flecharoja.loyalty.util.indicadores.TransaccionesCompras;
import com.flecharoja.loyalty.util.indicadores.VistasMisiones;
import com.flecharoja.loyalty.util.indicadores.VistasPromociones;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.Tuple2;

/**
 * Clase que realiza el procesamiento de una regla del tipo de atributo de
 * miembro
 *
 * @author svargas
 */
public class ProcesarReglaAvanzada {

    private final SparkSession spark; //sesion de spark
    private final JavaSparkContext javaContext; //contexto de java spark
    private final StructType schema; //esquema del dataframe a usar para resultados

    private final HBaseDBData hdbd; //instancia encargada de la carga de datos desde HBase
    private final MariaDBData odbd; //instancia encargada de la carga de datos desde Oracle DB

    public ProcesarReglaAvanzada(SparkSession spark, StructType schema) {
        this.schema = schema;
        this.spark = spark;
        this.javaContext = new JavaSparkContext(spark.sparkContext());

        this.hdbd = new HBaseDBData(javaContext);
        this.odbd = new MariaDBData(spark);
    }

    public Dataset<Row> inicio(Row regla, List<String> miembros) throws ReglaException, IOException {
        //obtencion de meta de la regla
        String indTipoRegla = regla.getAs(ReglasSegmento.Avanzada.NombreColumnas.IND_TIPO_REGLA.toString());
        String referencia = regla.getAs(ReglasSegmento.Avanzada.NombreColumnas.REFERENCIA.toString());
        String indOperador = regla.getAs(ReglasSegmento.Avanzada.NombreColumnas.IND_OPERADOR.toString());
        String valorComparacion0 = regla.getAs(ReglasSegmento.Avanzada.NombreColumnas.VALOR_COMPARACION0.toString());
        String valorComparacion1 = regla.getAs(ReglasSegmento.Avanzada.NombreColumnas.VALOR_COMPARACION1.toString());
        Date fechaInicio = regla.getAs(ReglasSegmento.Avanzada.NombreColumnas.FECHA_INICIO.toString());
        Date fechaFinal = regla.getAs(ReglasSegmento.Avanzada.NombreColumnas.FECHA_FINAL.toString());
        String indUltimo = regla.getAs(ReglasSegmento.Avanzada.NombreColumnas.FECHA_IND_ULTIMO.toString());
        Long fechaCantidad = regla.getAs(ReglasSegmento.Avanzada.NombreColumnas.FECHA_CANTIDAD.toString());

        ReglasSegmento.Avanzada.TiposRegla tipoRegla = ReglasSegmento.Avanzada.TiposRegla.get(indTipoRegla);
        
        switch (tipoRegla) {
            case ABRIERON_MENSAJE: {
                Dataset<Row> filtro1 = odbd.getInstanciasNotificaciones()
                        .filter((t) ->
                                miembros == null
                                || miembros.isEmpty()
                                || miembros.contains((String)t.getAs(InstanciasNotificaciones.NombreColumnas.ID_MIEMBRO.toString())))
                        .where(new Column(InstanciasNotificaciones.NombreColumnas.ID_NOTIFICACION.toString()).equalTo(referencia)
                                .and(new Column(InstanciasNotificaciones.NombreColumnas.FECHA_VISTO.toString()).isNotNull()));
                
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    filtro1 = filtro1.filter((t) -> {
                        Calendar fechaCreacion = Calendar.getInstance();
                        fechaCreacion.setTime(t.getAs(InstanciasNotificaciones.NombreColumnas.FECHA_VISTO.toString()));
                        
                        return DateUtils.isSameDay(fechaComparacion, fechaCreacion) || fechaComparacion.before(fechaCreacion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        filtro1 = filtro1.filter((t) -> {
                            Date fechaCreacion = t.getAs(InstanciasNotificaciones.NombreColumnas.FECHA_VISTO.toString());

                            return fechaCreacion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, fechaCreacion);
                        });
                    }
                    if (fechaFinal!=null) {
                        filtro1 = filtro1.filter((t) -> {
                            Date fechaCreacion = t.getAs(InstanciasNotificaciones.NombreColumnas.FECHA_VISTO.toString());

                            return fechaCreacion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, fechaCreacion);
                        });
                    }
                }
                
                return filtro1.select(new Column(InstanciasNotificaciones.NombreColumnas.ID_MIEMBRO.toString()));
            }
            case CAMBIARON_NIVEL: {
                throw new ReglaException("No soportada... esto es un evento");
            }
            case NO_APROBADAS_MISION: {
                JavaPairRDD<ImmutableBytesWritable, Result> resultado = hdbd.getRegistrosMisiones().filter((Tuple2<ImmutableBytesWritable, Result> t1) ->
                        miembros == null
                        || miembros.isEmpty()
                        || miembros.contains(Bytes.toString(t1._2.getRow()).split("&")[1])
                        && Bytes.toString(t1._2.getRow()).split("&")[0].equals(referencia)
                        && Bytes.toString(t1._2.getValue(Bytes.toBytes(RegistrosMisiones.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMisiones.ColumnasDATA.ESTADO.toString()))).equals(RegistrosMisiones.ValoresDATA_ESTADO.SIN_REVISION.value)
                        || Bytes.toString(t1._2.getValue(Bytes.toBytes(RegistrosMisiones.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMisiones.ColumnasDATA.ESTADO.toString()))).equals(RegistrosMisiones.ValoresDATA_ESTADO.RECHAZADO.value)
                );
                
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    resultado = resultado.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                        Calendar fechaCreacion = Calendar.getInstance();
                        fechaCreacion.setTimeInMillis(Long.parseLong(Bytes.toString(t1._2.getRow()).split("&")[2]));
                        
                        return DateUtils.isSameDay(fechaComparacion, fechaCreacion) || fechaComparacion.before(fechaCreacion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        resultado = resultado.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                            Date fechaCreacion = new Date(Long.parseLong(Bytes.toString(t1._2.getRow()).split("&")[2]));

                            return fechaCreacion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, fechaCreacion);
                        });
                    }
                    if (fechaFinal!=null) {
                        resultado = resultado.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                            Date fechaCreacion = new Date(Long.parseLong(Bytes.toString(t1._2.getRow()).split("&")[2]));

                            return fechaCreacion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, fechaCreacion);
                        });
                    }
                }
                
                return spark.createDataFrame(resultado.values().map((Result t1) -> RowFactory.create(Bytes.toString(t1.getRow()).split("&")[1])), schema).distinct();
            }
            case CANTIDAD_COMPRAS_UBICACION: {
                Dataset<Row> filtro1 = odbd.getTransaccionesCompras()
                        .filter((t) ->
                                miembros == null
                                || miembros.isEmpty()
                                || miembros.contains((String)t.getAs(TransaccionesCompras.NombreColumnas.ID_MIEMBRO.toString())));
                if (referencia!=null) {
                    filtro1 = filtro1.where(new Column(TransaccionesCompras.NombreColumnas.ID_UBICACION.toString()).equalTo(referencia));
                }
                
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    filtro1 = filtro1.filter((t) -> {
                        Calendar fechaCreacion = Calendar.getInstance();
                        fechaCreacion.setTime(t.getAs(TransaccionesCompras.NombreColumnas.FECHA.toString()));
                        
                        return DateUtils.isSameDay(fechaComparacion, fechaCreacion) || fechaComparacion.before(fechaCreacion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        filtro1 = filtro1.filter((t) -> {
                            Date fechaCreacion = t.getAs(TransaccionesCompras.NombreColumnas.FECHA.toString());

                            return fechaCreacion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, fechaCreacion);
                        });
                    }
                    if (fechaFinal!=null) {
                        filtro1 = filtro1.filter((t) -> {
                            Date fechaCreacion = t.getAs(TransaccionesCompras.NombreColumnas.FECHA.toString());

                            return fechaCreacion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, fechaCreacion);
                        });
                    }
                }
                
                filtro1.createOrReplaceTempView("transacciones");
                
                Dataset<Row> filtro2 = spark.sql("SELECT "+TransaccionesCompras.NombreColumnas.ID_MIEMBRO.toString()+", COUNT(*) AS CONTEO FROM transacciones"
                        + " GROUP BY "+TransaccionesCompras.NombreColumnas.ID_MIEMBRO.toString());
                
                filtro2 = filtro2.filter((t) -> {
                    Long conteo = t.getAs("CONTEO");
                    Long valorComparacion = Long.parseLong(valorComparacion0);
                    
                    switch (ReglasSegmento.ValoresIndOperador.get(indOperador)) {
                        case IGUAL: {
                            return valorComparacion.compareTo(conteo) == 0;
                        }
                        case MAYOR: {
                            return conteo.compareTo(valorComparacion) == 1;
                        }
                        case MAYOR_IGUAL: {
                            return conteo.compareTo(valorComparacion) >= 0;
                        }
                        case MENOR: {
                            return conteo.compareTo(valorComparacion) == -1;
                        }
                        case MENOR_IGUAL: {
                            return conteo.compareTo(valorComparacion) <= 0;
                        }
                        default: {
                            throw new ReglaException("Indicador de operador no valido");
                        }
                    }
                });

                return filtro2.select(new Column(TransaccionesCompras.NombreColumnas.ID_MIEMBRO.toString()));
            }
            case COMPRARON_PRODUCTO: {
                Dataset<Row> comprasProductos;
                if (referencia==null) {
                    comprasProductos = odbd.getComprasProductos();
                } else {
                    comprasProductos = odbd.getComprasProductos().where(new Column(ComprasProductos.NombreColumnas.ID_PRODUCTO.toString()).equalTo(referencia));
                }
                
                Dataset<Row> transacciones = odbd.getTransaccionesCompras();
                
                Dataset<Row> filter1 = comprasProductos.join(transacciones, comprasProductos.col(ComprasProductos.NombreColumnas.ID_TRANSACCION.toString()).equalTo(transacciones.col(TransaccionesCompras.NombreColumnas.ID_TRANSACCION.toString()))).dropDuplicates();
                
                Dataset<Row> filter2 = filter1;
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    filter2 = filter2.filter((t) -> {
                        Calendar fechaCreacion = Calendar.getInstance();
                        fechaCreacion.setTime(t.getAs(TransaccionesCompras.NombreColumnas.FECHA.toString()));
                        
                        return DateUtils.isSameDay(fechaComparacion, fechaCreacion) || fechaComparacion.before(fechaCreacion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        filter2 = filter2.filter((t) -> {
                            Date fechaCreacion = t.getAs(TransaccionesCompras.NombreColumnas.FECHA.toString());

                            return fechaCreacion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, fechaCreacion);
                        });
                    }
                    if (fechaFinal!=null) {
                        filter2 = filter2.filter((t) -> {
                            Date fechaCreacion = t.getAs(TransaccionesCompras.NombreColumnas.FECHA.toString());

                            return fechaCreacion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, fechaCreacion);
                        });
                    }
                }
                
                Dataset<Row> filter3 = filter2.select(new Column(TransaccionesCompras.NombreColumnas.ID_MIEMBRO.toString())).dropDuplicates().filter((t) ->
                        miembros == null
                        || miembros.isEmpty()
                        || miembros.contains((String)t.getAs(TransaccionesCompras.NombreColumnas.ID_MIEMBRO.toString())));
                
                return filter3;
            }
            case GANARON_MISION: {
                JavaPairRDD<ImmutableBytesWritable, Result> resultado = hdbd.getRegistrosMisiones().filter((Tuple2<ImmutableBytesWritable, Result> t1) ->
                        miembros==null
                        || miembros.isEmpty()
                        || miembros.contains(Bytes.toString(t1._2.getRow()).split("&")[1])
                        && Bytes.toString(t1._2.getRow()).split("&")[0].equals(referencia)
                        && Bytes.toString(t1._2.getValue(Bytes.toBytes(RegistrosMisiones.ColumnasFamilia.DATA.toString()), Bytes.toBytes(RegistrosMisiones.ColumnasDATA.ESTADO.toString()))).equals(RegistrosMisiones.ValoresDATA_ESTADO.APROBADO.value)
                );
                
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    resultado = resultado.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                        Calendar fechaCreacion = Calendar.getInstance();
                        fechaCreacion.setTimeInMillis(Long.parseLong(Bytes.toString(t1._2.getRow()).split("&")[2]));
                        
                        return DateUtils.isSameDay(fechaComparacion, fechaCreacion) || fechaComparacion.before(fechaCreacion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        resultado = resultado.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                            Date fechaCreacion = new Date(Long.parseLong(Bytes.toString(t1._2.getRow()).split("&")[2]));

                            return fechaCreacion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, fechaCreacion);
                        });
                    }
                    if (fechaFinal!=null) {
                        resultado = resultado.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                            Date fechaCreacion = new Date(Long.parseLong(Bytes.toString(t1._2.getRow()).split("&")[2]));

                            return fechaCreacion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, fechaCreacion);
                        });
                    }
                }
                
                return spark.createDataFrame(resultado.values().map((Result t1) -> RowFactory.create(Bytes.toString(t1.getRow()).split("&")[1])), schema).distinct();
            }
            case GASTARON_DINERO: {
                Dataset<Row> filtro1 = odbd.getTransaccionesCompras()
                        .filter((t) ->
                                miembros == null
                                || miembros.isEmpty()
                                || miembros.contains((String)t.getAs(TransaccionesCompras.NombreColumnas.ID_MIEMBRO.toString())));
                
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    filtro1 = filtro1.filter((t) -> {
                        Calendar fechaCreacion = Calendar.getInstance();
                        fechaCreacion.setTime(t.getAs(TransaccionesCompras.NombreColumnas.FECHA.toString()));
                        
                        return DateUtils.isSameDay(fechaComparacion, fechaCreacion) || fechaComparacion.before(fechaCreacion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        filtro1 = filtro1.filter((t) -> {
                            Date fechaCreacion = t.getAs(TransaccionesCompras.NombreColumnas.FECHA.toString());

                            return fechaCreacion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, fechaCreacion);
                        });
                    }
                    if (fechaFinal!=null) {
                        filtro1 = filtro1.filter((t) -> {
                            Date fechaCreacion = t.getAs(TransaccionesCompras.NombreColumnas.FECHA.toString());

                            return fechaCreacion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, fechaCreacion);
                        });
                    }
                }
                
                JavaPairRDD<String, Double> filtro2 = filtro1
                        .toJavaRDD()
                        .mapToPair((t) -> new Tuple2<>((String)t.getAs(TransaccionesCompras.NombreColumnas.ID_MIEMBRO.toString()), (Double)t.getAs(TransaccionesCompras.NombreColumnas.TOTAL.toString())))
                        .reduceByKey((t1, t2) -> t1+t2)
                        .filter((t1) -> {
                            Double valorComparacion = Double.parseDouble(valorComparacion0);

                            switch (ReglasSegmento.ValoresIndOperador.get(indOperador)) {
                                case IGUAL: {
                                    return valorComparacion.compareTo(t1._2) == 0;
                                }
                                case MAYOR: {
                                    return t1._2.compareTo(valorComparacion) == 1;
                                }
                                case MAYOR_IGUAL: {
                                    return t1._2.compareTo(valorComparacion) >= 0;
                                }
                                case MENOR: {
                                    return t1._2.compareTo(valorComparacion) == -1;
                                }
                                case MENOR_IGUAL: {
                                    return t1._2.compareTo(valorComparacion) <= 0;
                                }
                                default: {
                                    throw new ReglaException("Indicador de operador no valido");
                                }
                            }
                        });

                return spark.createDataFrame(filtro2.keys().map((t1) -> RowFactory.create(t1)), schema);
            }
            case PERTENECEN_GRUPO_MIE: {
                return odbd.getGrupoMiembrosAsociados()
                        .filter((t) ->
                                miembros == null
                                || miembros.isEmpty()
                                || miembros.contains((String)t.getAs(ListaMiembrosGrupoMiembro.NombreColumnas.ID_MIEMBRO.toString())))
                        .where(new Column(ListaMiembrosGrupoMiembro.NombreColumnas.ID_GRUPO.toString()).equalTo(referencia)).select(ListaMiembrosGrupoMiembro.NombreColumnas.ID_MIEMBRO.toString());
            }
            case PERTENECEN_SEGMENTO: {
                JavaPairRDD<ImmutableBytesWritable, Result> resultado = hdbd.getEligibilidadMiembros().filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                    if (miembros==null || miembros.isEmpty() || miembros.contains(Bytes.toString(t1._2.getRow())) && 
                            t1._2.containsColumn(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.SEGMENTOS.toString()), Bytes.toBytes(referencia))) {
                        return Bytes.toBoolean(t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.SEGMENTOS.toString()), Bytes.toBytes(referencia)));
                    }
                    return false;
                });
                return spark.createDataFrame(resultado.values().map((Result t1) -> RowFactory.create(Bytes.toString(t1.getRow()))), schema);
            }
            case POSICION_TABLA_POS: {
                String tipoTablaPosicion = odbd.getTablasPosiciones().where(new Column(TablaPosiciones.NombreColumnas.ID_TABLA.toString()).equalTo(referencia)).first().getAs(TablaPosiciones.NombreColumnas.IND_TIPO.toString());
                
                switch (TablaPosiciones.ValoresIndTipo.get(tipoTablaPosicion)) {
                    case GLOBAL_INDIVIDUAL:
                    case GRUPO_MIEMBRO: {
                        JavaPairRDD<Row, Long> resultado = odbd.getMiembrosTablasPosiciones()
                                .where(new Column(PosicionesMiembrosTabPos.NombreColumnas.ID_TABLA.toString()).equalTo(referencia))
                                .sort(new Column(PosicionesMiembrosTabPos.NombreColumnas.ACUMULADO.toString()).desc())
                                .toJavaRDD().zipWithIndex()
                                .filter((Tuple2<Row, Long> t1) -> t1._2>=Long.parseLong(valorComparacion0) && t1._2<=Long.parseLong(valorComparacion1));
                        return spark.createDataFrame(resultado.keys().map((Row t1) -> RowFactory.create(t1.getAs(PosicionesMiembrosTabPos.NombreColumnas.ID_MIEMBRO.toString()))), schema);
                    }
                    case GLOBAL_GRUPAL: {
                        JavaPairRDD<Row, Long> filtro1 = odbd.getGruposMiembrosTablasPosiciones()
                                .where(new Column(PosicionesMiembrosTabPos.NombreColumnas.ID_TABLA.toString()).equalTo(referencia))
                                .sort(new Column(PosicionesMiembrosTabPos.NombreColumnas.ACUMULADO.toString()).desc())
                                .toJavaRDD().zipWithIndex()
                                .filter((Tuple2<Row, Long> t1) -> t1._2>=Long.parseLong(valorComparacion0) && t1._2<=Long.parseLong(valorComparacion1));
                        
                        Dataset<Row> grupoMiembro = odbd.getGrupoMiembrosAsociados();
                        Dataset<Row> filtro2 = spark.createDataFrame(filtro1.keys(), DataTypes.createStructType(new StructField[]{
                            DataTypes.createStructField(PosicionesGruposTabPos.NombreColumnas.ID_TABLA.toString(), DataTypes.StringType, false),
                            DataTypes.createStructField(PosicionesGruposTabPos.NombreColumnas.ID_GRUPO.toString(), DataTypes.StringType, false),
                            DataTypes.createStructField(PosicionesGruposTabPos.NombreColumnas.ACUMULADO.toString(), DataTypes.LongType, false)}));
                        return filtro2.join(grupoMiembro, filtro2.col(PosicionesGruposTabPos.NombreColumnas.ID_GRUPO.toString()).equalTo(grupoMiembro.col(ListaMiembrosGrupoMiembro.NombreColumnas.ID_GRUPO.toString())), "left")
                                .select(grupoMiembro.col(ListaMiembrosGrupoMiembro.NombreColumnas.ID_MIEMBRO.toString())).distinct();
                    }
                }
                throw new ReglaException("Regla no se pudo procesar");
            }
            case RECIBIERON_MENSAJE: {
                Dataset<Row> filtro1 = odbd.getInstanciasNotificaciones()
                        .filter((t) ->
                                miembros == null
                                || miembros.isEmpty()
                                || miembros.contains((String)t.getAs(InstanciasNotificaciones.NombreColumnas.ID_MIEMBRO.toString())))
                        .where(new Column(InstanciasNotificaciones.NombreColumnas.ID_NOTIFICACION.toString()).equalTo(referencia)
                                .and(new Column(InstanciasNotificaciones.NombreColumnas.FECHA_ENTREGA.toString()).isNotNull()));
                
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    filtro1 = filtro1.filter((t) -> {
                        Calendar fechaCreacion = Calendar.getInstance();
                        fechaCreacion.setTime(t.getAs(InstanciasNotificaciones.NombreColumnas.FECHA_ENTREGA.toString()));
                        
                        return DateUtils.isSameDay(fechaComparacion, fechaCreacion) || fechaComparacion.before(fechaCreacion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        filtro1 = filtro1.filter((t) -> {
                            Date fechaCreacion = t.getAs(InstanciasNotificaciones.NombreColumnas.FECHA_ENTREGA.toString());

                            return fechaCreacion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, fechaCreacion);
                        });
                    }
                    if (fechaFinal!=null) {
                        filtro1 = filtro1.filter((t) -> {
                            Date fechaCreacion = t.getAs(InstanciasNotificaciones.NombreColumnas.FECHA_ENTREGA.toString());

                            return fechaCreacion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, fechaCreacion);
                        });
                    }
                }
                
                return filtro1.select(new Column(InstanciasNotificaciones.NombreColumnas.ID_MIEMBRO.toString()));
            }
            case REDIMIERON_PREMIO: {
                Dataset<Row> filter1 = odbd.getPremiosRedimidos()
                        .where(new Column(PremiosRedimidos.NombreColumnas.ESTADO.toString()).equalTo(PremiosRedimidos.Estados.REDIMIDO_SIN_RETIRAR.toString()))
                        .filter((t) -> miembros==null || miembros.isEmpty() || miembros.contains((String)t.getAs(PremiosRedimidos.NombreColumnas.ID_MIEMBRO.toString())))
                        .where(new Column(PremiosRedimidos.NombreColumnas.ID_PREMIO.toString()).equalTo(referencia));
                
                Dataset<Row> filter2 = filter1;
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    filter2 = filter2.filter((Row t) -> {
                        Calendar ultimaConexion = Calendar.getInstance();
                        ultimaConexion.setTime(t.getAs(PremiosRedimidos.NombreColumnas.FECHA_ASIGNACION.toString()));
                        
                        return DateUtils.isSameDay(fechaComparacion, ultimaConexion) || fechaComparacion.before(ultimaConexion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        filter2 = filter2.filter((Row t) -> {
                            Date ultimaConexion = t.getAs(PremiosRedimidos.NombreColumnas.FECHA_ASIGNACION.toString());

                            return ultimaConexion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, ultimaConexion);
                        });
                    }
                    if (fechaFinal!=null) {
                        filter2 = filter2.filter((Row t) -> {
                            Date ultimaConexion = t.getAs(PremiosRedimidos.NombreColumnas.FECHA_ASIGNACION.toString());

                            return ultimaConexion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, ultimaConexion);
                        });
                    }
                }
                
                return filter2.select(new Column(PremiosRedimidos.NombreColumnas.ID_MIEMBRO.toString())).dropDuplicates();
            }
            case REDIMIERON_PROMOCION: {
                Dataset<Row> filter1 = odbd.getMarcadoresPromociones()
                        .filter((t) -> miembros==null || miembros.isEmpty() || miembros.contains((String)t.getAs(MarcadoresPromociones.NombreColumnas.ID_MIEMBRO.toString())))
                        .where(new Column(MarcadoresPromociones.NombreColumnas.ID_PROMOCION.toString()).equalTo(referencia));
                
                Dataset<Row> filter2 = filter1;
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    filter2 = filter2.filter((Row t) -> {
                        Calendar ultimaConexion = Calendar.getInstance();
                        ultimaConexion.setTime(t.getAs(MarcadoresPromociones.NombreColumnas.FECHA.toString()));
                        
                        return DateUtils.isSameDay(fechaComparacion, ultimaConexion) || fechaComparacion.before(ultimaConexion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        filter2 = filter2.filter((Row t) -> {
                            Date ultimaConexion = t.getAs(MarcadoresPromociones.NombreColumnas.FECHA.toString());

                            return ultimaConexion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, ultimaConexion);
                        });
                    }
                    if (fechaFinal!=null) {
                        filter2 = filter2.filter((Row t) -> {
                            Date ultimaConexion = t.getAs(MarcadoresPromociones.NombreColumnas.FECHA.toString());

                            return ultimaConexion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, ultimaConexion);
                        });
                    }
                }
                
                return filter2.select(new Column(MarcadoresPromociones.NombreColumnas.ID_MIEMBRO.toString())).dropDuplicates();
            }
            case REFERENCIAS_MIEMBROS: {
                throw new ReglaException("No soportada");
            }
            case TIENE_INSIGNIA: {
                Dataset<Row> respuesta = odbd.getInsigniasMiembrosGanadas();
                
                respuesta = respuesta.filter((Row t) -> {
                    return miembros==null || miembros.isEmpty() || miembros.contains((String)t.getAs(Miembro.Atributos.ID_MIEMBRO.toString()));
                });
                
                respuesta = respuesta.where(new Column(NivelInsigniaMiembro.NombreColumnas.ID_INSIGNIA.toString()).equalTo(referencia));
                if (valorComparacion0!=null) {
                    respuesta = respuesta.where(new Column(NivelInsigniaMiembro.NombreColumnas.ID_NIVEL.toString()).equalTo(valorComparacion0));
                }
                
                return respuesta.select(new Column(NivelInsigniaMiembro.NombreColumnas.ID_MIEMBRO.toString()));
            }
            case TIPO_DISPOSITIVO: {
                Dataset<Row> respuesta = odbd.getMiembros().filter((Row t) -> {
                    return miembros==null || miembros.isEmpty() || miembros.contains((String)t.getAs(Miembro.Atributos.ID_MIEMBRO.toString()));
                });
                
                return respuesta.where(new Column(Miembro.Atributos.IND_TIPO_DISPOSITIVO.toString()).equalTo(valorComparacion0)).select(new Column(Miembro.Atributos.ID_MIEMBRO.toString()));
            }
            case ULTIMA_CONEXION: {
                Dataset<Row> respuesta = odbd.getMiembros().filter((Row t) -> {
                    return miembros==null || miembros.isEmpty() || miembros.contains((String)t.getAs(Miembro.Atributos.ID_MIEMBRO.toString()));
                });
                
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    respuesta = respuesta.filter((Row t) -> {
                        Calendar ultimaConexion = Calendar.getInstance();
                        ultimaConexion.setTime(t.getAs(Miembro.Atributos.FECHA_ULTIMA_CONEXION.toString()));
                        
                        return DateUtils.isSameDay(fechaComparacion, ultimaConexion) || fechaComparacion.before(ultimaConexion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        respuesta = respuesta.filter((Row t) -> {
                            Date ultimaConexion = t.getAs(Miembro.Atributos.FECHA_ULTIMA_CONEXION.toString());

                            return ultimaConexion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, ultimaConexion);
                        });
                    }
                    if (fechaFinal!=null) {
                        respuesta = respuesta.filter((Row t) -> {
                            Date ultimaConexion = t.getAs(Miembro.Atributos.FECHA_ULTIMA_CONEXION.toString());

                            return ultimaConexion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, ultimaConexion);
                        });
                    }
                }
                
                return respuesta.select(new Column(Miembro.Atributos.ID_MIEMBRO.toString()));
            }
            case UNIERON_PROGRAMA: {
                Dataset<Row> respuesta = odbd.getMiembros().filter((Row t) -> {
                    return miembros==null || miembros.isEmpty() || miembros.contains((String)t.getAs(Miembro.Atributos.ID_MIEMBRO.toString()));
                });
                
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    respuesta = respuesta.filter((Row t) -> {
                        Calendar fechaCreacion = Calendar.getInstance();
                        fechaCreacion.setTime(t.getAs(Miembro.Atributos.FECHA_INGRESO.toString()));
                        
                        return DateUtils.isSameDay(fechaComparacion, fechaCreacion) || fechaComparacion.before(fechaCreacion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        respuesta = respuesta.filter((Row t) -> {
                            Date fechaCreacion = t.getAs(Miembro.Atributos.FECHA_INGRESO.toString());

                            return fechaCreacion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, fechaCreacion);
                        });
                    }
                    if (fechaFinal!=null) {
                        respuesta = respuesta.filter((Row t) -> {
                            Date fechaCreacion = t.getAs(Miembro.Atributos.FECHA_INGRESO.toString());

                            return fechaCreacion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, fechaCreacion);
                        });
                    }
                }
                
                return respuesta.select(new Column(Miembro.Atributos.ID_MIEMBRO.toString()));
            }
            case VIERON_MISION: {
                JavaPairRDD<ImmutableBytesWritable, Result> resultado = hdbd.getVistasMisiones().filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                    if (miembros==null || miembros.isEmpty() || miembros.contains(Bytes.toString(t1._2.getRow()).split("&")[1])) {
                        return Bytes.toString(t1._2.getValue(Bytes.toBytes(VistasMisiones.ColumnasFamilia.DATA.toString()), Bytes.toBytes(VistasMisiones.ColumnasDATA.MISION.toString()))).equals(referencia);
                    }
                    return false;
                });
                
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    resultado.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                        Calendar fechaCreacion = Calendar.getInstance();
                        fechaCreacion.setTimeInMillis(Long.parseLong(Bytes.toString(t1._2.getRow()).split("&")[0]));
                        
                        return DateUtils.isSameDay(fechaCreacion, fechaComparacion) || fechaComparacion.before(fechaCreacion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        resultado.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                            Date fechaCreacion = new Date(Long.parseLong(Bytes.toString(t1._2.getRow()).split("&")[0]));
                            
                            return fechaCreacion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, fechaCreacion);
                        });
                    }
                    if (fechaFinal!=null) {
                        resultado.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                            Date fechaCreacion = new Date(Long.parseLong(Bytes.toString(t1._2.getRow()).split("&")[0]));
                            
                            return fechaCreacion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, fechaCreacion);
                        });
                    }
                }
                
                return spark.createDataFrame(resultado.values().map((Result t1) -> RowFactory.create(Bytes.toString(t1.getRow()).split("&")[1])), schema);
            }
            case VIERON_PROMOCION: {
                JavaPairRDD<ImmutableBytesWritable, Result> resultado = hdbd.getVistasPromociones().filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                    if (miembros==null || miembros.isEmpty() || miembros.contains(Bytes.toString(t1._2.getRow()).split("&")[1])) {
                        return Bytes.toString(t1._2.getValue(Bytes.toBytes(VistasPromociones.ColumnasFamilia.DATA.toString()), Bytes.toBytes(VistasPromociones.ColumnasDATA.PROMOCION.toString()))).equals(referencia);
                    }
                    return false;
                });
                
                if (indUltimo!=null) {
                    Calendar fechaComparacion = Calendar.getInstance();
                    switch (ReglasSegmento.Avanzada.TiposUltimaFecha.get(indUltimo)) {
                        case DIA: {
                            fechaComparacion.add(Calendar.DAY_OF_YEAR, -fechaCantidad.intValue());
                            break;
                        }
                        case MES: {
                            fechaComparacion.add(Calendar.MONTH, -fechaCantidad.intValue());
                            break;
                        }
                        case ANO: {
                            fechaComparacion.add(Calendar.YEAR, -fechaCantidad.intValue());
                            break;
                        }
                    }
                    resultado.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                        Calendar fechaCreacion = Calendar.getInstance();
                        fechaCreacion.setTimeInMillis(Long.parseLong(Bytes.toString(t1._2.getRow()).split("&")[0]));
                        
                        return DateUtils.isSameDay(fechaCreacion, fechaComparacion) || fechaComparacion.before(fechaCreacion);
                    });
                } else {
                    if (fechaInicio!=null) {
                        resultado.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                            Date fechaCreacion = new Date(Long.parseLong(Bytes.toString(t1._2.getRow()).split("&")[0]));
                            
                            return fechaCreacion.after(fechaInicio) || DateUtils.isSameDay(fechaInicio, fechaCreacion);
                        });
                    }
                    if (fechaFinal!=null) {
                        resultado.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                            Date fechaCreacion = new Date(Long.parseLong(Bytes.toString(t1._2.getRow()).split("&")[0]));
                            
                            return fechaCreacion.before(fechaFinal) || DateUtils.isSameDay(fechaFinal, fechaCreacion);
                        });
                    }
                }
                
                return spark.createDataFrame(resultado.values().map((Result t1) -> RowFactory.create(Bytes.toString(t1.getRow()).split("&")[1])), schema);
            }
            case PERTENECE_NIVEL: {
                Dataset<Row> nivelesMetrica = odbd.getNivelesMetrica().where(new Column(NivelMetrica.NombreColumnas.GRUPO_NIVELES.toString()).equalTo(valorComparacion1));
                double valorInicial = nivelesMetrica.where(new Column(NivelMetrica.NombreColumnas.ID_NIVEL.toString()).equalTo(valorComparacion0)).first().getAs(NivelMetrica.NombreColumnas.METRICA_INICIAL.toString());
                double valorFinal = nivelesMetrica.orderBy(new Column(NivelMetrica.NombreColumnas.METRICA_INICIAL.toString()).desc()).first().getAs(NivelMetrica.NombreColumnas.METRICA_INICIAL.toString());

                if (valorInicial>=valorFinal) {
                    return odbd.getBalancesMetricaMiembro().where(new Column(BalanceMetricaMiembro.NombreColumnas.ID_METRICA.toString()).equalTo(referencia)
                            .and(new Column(BalanceMetricaMiembro.NombreColumnas.PROGRESO_ACTUAL.toString()).geq(valorInicial)))
                            .select(new Column(BalanceMetricaMiembro.NombreColumnas.ID_MIEMBRO.toString()));
                } else {
                    return odbd.getBalancesMetricaMiembro().where(new Column(BalanceMetricaMiembro.NombreColumnas.ID_METRICA.toString()).equalTo(referencia)
                            .and(new Column(BalanceMetricaMiembro.NombreColumnas.PROGRESO_ACTUAL.toString()).between(valorInicial, valorFinal)))
                            .select(new Column(BalanceMetricaMiembro.NombreColumnas.ID_MIEMBRO.toString()));
                }
            }
            default: {
                throw new ReglaException("Tipo de regla desconocida");
            }
        }
    }
}
