package com.flecharoja.loyalty.exception;

/**
 *
 * @author svargas
 */
public class ListaReglasException extends Exception {

    /**
     * Constructs an instance of <code>ListaReglasException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ListaReglasException(String msg) {
        super(msg);
    }
}
