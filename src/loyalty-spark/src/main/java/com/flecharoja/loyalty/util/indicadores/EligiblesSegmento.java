package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class EligiblesSegmento {
    public static final String NOMBRE_TABLA = "ELEGIBLES_SEGMENTO";
    
    public enum ColumnasFamilia {
        MIEMBROS,
        DATA;
    }
    
    public enum ColumnasDATA {
        ULTIMA_ACTUALIZACION,
        LISTAS_PROCESADAS,
        LISTAS_FALLIDAS,
        REGLAS_PROCESADAS,
        REGLAS_FALLIDAS,
        CANTIDAD_MIEMBROS;
    }
}
