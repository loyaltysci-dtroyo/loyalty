package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class VistasPromociones {
    public static final String NOMBRE_TABLA = "VISTAS_PROMOCION";
    
    public enum ColumnasFamilia {
        DATA;
    }
    
    public enum ColumnasDATA {
        PROMOCION;
    }
}
