package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class NivelMetricaInicialMiembro {
    public static final String NOMBRE_TABLA = "MIEMBRO_NIVEL";
    
    public enum NombreColumnas {
        ID_MIEMBRO,
        ID_NIVEL,
        ID_METRICA;
    }
}
