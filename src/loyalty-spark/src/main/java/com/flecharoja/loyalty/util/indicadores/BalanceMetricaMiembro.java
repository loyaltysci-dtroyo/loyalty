package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class BalanceMetricaMiembro {
    public static final String NOMBRE_TABLA = "MIEMBRO_BALANCE_METRICA";
    
    public enum NombreColumnas {
        ID_MIEMBRO,
        ID_METRICA,
        PROGRESO_ACTUAL,
        TOTAL_ACUMULADO,
        DISPONIBLE,
        VENCIDO,
        REDIMIDO;
    }
}
