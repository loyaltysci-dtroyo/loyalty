package com.flecharoja.loyalty.util.indicadores;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author svargas
 */
public class Miembro {
    public static final String NOMBRE_TABLA = "MIEMBRO";
    
    public enum Atributos {
        IND_MIEMBRO_SISTEMA("G"),
        EMAIL("X"),
        ID_MIEMBRO(null),
        FECHA_NACIMIENTO("A"),
        IND_GENERO("B"),
        IND_ESTADO_CIVIL("C"),
        FRECUENCIA_COMPRA("D"),
        NOMBRE("E"),
        APELLIDO("F"),
        APELLIDO2("W"),
        IND_ESTADO_MIEMBRO("H"),
        TIER_INICIAL("I"),
        TIER_PROGRESO("J"),
        CIUDAD_RESIDENCIA("K"),
        ESTADO_RESIDENCIA("L"),
        PAIS_RESIDENCIA("M"),
        CODIGO_POSTAL("N"),
        IND_EDUCACION("O"),
        INGRESO_ECONOMICO("P"),
        IND_HIJOS("Q"),
        IND_CONTACTO_EMAIL("S"),
        IND_CONTACTO_NOTIFICACION("T"),
        IND_CONTACTO_SMS("U"),
        IND_CONTACTO_ESTADO("V"),
        ATRIBUTO_DINAMICO("R"),
        FECHA_INGRESO(null),
        IND_TIPO_DISPOSITIVO(null),
        FECHA_ULTIMA_CONEXION(null),
        MIEMBRO_REFERENTE(null);
        
        public final String value;
        private static final Map<String, Atributos> lookup = new HashMap<>();

        private Atributos(String value) {
            this.value = value;
        }
        
        static {
            for (Atributos atributo : values()) {
                lookup.put(atributo.value, atributo);
            }
        }
        
        public static Atributos get (String value) {
            return value==null?null:lookup.get(value);
        }
    }
}
