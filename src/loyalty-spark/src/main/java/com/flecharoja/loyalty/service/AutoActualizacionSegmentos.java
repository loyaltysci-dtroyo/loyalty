package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.data.HBaseDBData;
import com.flecharoja.loyalty.data.MariaDBData;
import com.flecharoja.loyalty.util.AccionSegmento;
import com.flecharoja.loyalty.util.indicadores.EligibilidadMiembro;
import com.flecharoja.loyalty.util.indicadores.EligiblesSegmento;
import com.flecharoja.loyalty.util.indicadores.ListaMiembrosSegmento;
import com.flecharoja.loyalty.util.indicadores.Miembro;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import org.apache.spark.util.LongAccumulator;
import scala.Tuple2;

/**
 *
 * @author svargas
 */
public class AutoActualizacionSegmentos {

    public static void main(String[] args) throws InterruptedException, IOException {
        
        SparkConf sc = new SparkConf().setAppName("auto actualizacion segmentos").set("spark.cores.max", "2");
        JavaStreamingContext jsc = new JavaStreamingContext(sc, new Duration(60000));
        
        StructType schema; //esquema del dataframe a usar para resultados
        //el esquema contiene una unica columna que contrendra registros de identificadores de miembros
        schema = DataTypes.createStructType(new StructField[]{DataTypes.createStructField(Miembro.Atributos.ID_MIEMBRO.toString(), DataTypes.StringType, false)});
        
        HBaseDBData hbdbd = new HBaseDBData(jsc.sparkContext());
        MariaDBData odbd = new MariaDBData(SparkSession.builder().config(sc).getOrCreate());
        
        Map<String, Integer> topicMap = new HashMap<>();
        topicMap.put("actualizar-segmentos", 5);
        
        Properties kcProperties = new Properties();
        kcProperties.load(AutoRecalculoReglaAvanzada.class.getResourceAsStream("/conf/kafka/kafka-consumer.properties"));
        
        //lectura de mensajes desde kafka en los topicos deseados
        JavaPairReceiverInputDStream<String, String> mensajes = KafkaUtils.createStream(jsc, kcProperties.getProperty("zookeeper.quorum"), "spark-streaming-AutoActualizacionSegmentos", topicMap);
        
        //subtraccion del contenido de los mensajes
        JavaDStream<String> contenidos = mensajes.map((Tuple2<String, String> t1) -> t1._2);
        
        try {
            contenidos.mapToPair((String t) -> {
                //conversion a pares con valores numericos
                return new Tuple2<>(t, 1);
            }).reduceByKey((Integer t1, Integer t2) -> {
                //reduccion de mensajes por contenido y conteo de cada identificador
                return t1+t2;
            }).foreachRDD((JavaPairRDD<String, Integer> t2) -> {
                //acumuladores para el guardado de estadisticas
                LongAccumulator aReglasFallidas, aReglasProcesadas, aListasFallidas, aListasProcesadas;

                aReglasFallidas = jsc.sparkContext().sc().longAccumulator();
                aReglasProcesadas = jsc.sparkContext().sc().longAccumulator();
                aListasFallidas = jsc.sparkContext().sc().longAccumulator();
                aListasProcesadas = jsc.sparkContext().sc().longAccumulator();

                JavaPairRDD<ImmutableBytesWritable, Result> eligibilidadMiembros = hbdbd.getEligibilidadMiembros();

                Set<String> idsSegmentos = t2.collectAsMap().keySet();
                for (String idSegmento : idsSegmentos) {
                    Dataset<Row> resultado = new ProcesarSegmento(SparkSession.builder().config(sc).getOrCreate(), schema, aReglasFallidas, aReglasProcesadas, aListasFallidas, aListasProcesadas)
                            .inicio(idSegmento, AccionSegmento.ACTUALIZAR);
                    
                    //obtencion de listas de miembros excluidos/incluidos y la seleccion de los incluidos
                    Dataset<Row> datasetMiembrosI = odbd.getMiembrosIncluidosExcluidos(idSegmento).where(new Column(ListaMiembrosSegmento.NombreColumnas.IND_TIPO.toString()).equalTo(ListaMiembrosSegmento.ValoresIndTipo.INCLUIDO.value)).select(new Column(ListaMiembrosSegmento.NombreColumnas.ID_MIEMBRO.toString()));
                    //agregado del resultado los miembros incluidos y la eliminacion de duplicados
                    resultado = resultado.union(datasetMiembrosI).distinct();

                    //obtencion de listas de miembros excluidos/incluidos y la seleccion de los excluidos
                    Dataset<Row> datasetMiembrosE = odbd.getMiembrosIncluidosExcluidos(idSegmento).where(new Column(ListaMiembrosSegmento.NombreColumnas.IND_TIPO.toString()).equalTo(ListaMiembrosSegmento.ValoresIndTipo.EXCLUIDO.value)).select(new Column(ListaMiembrosSegmento.NombreColumnas.ID_MIEMBRO.toString()));
                    //excluision del resultado los miembros excluidos
                    resultado = resultado.except(datasetMiembrosE);
                    
                    Dataset<Row> oldMembers = SparkSession.builder().config(sc).getOrCreate().createDataFrame(
                            eligibilidadMiembros.filter((Tuple2<ImmutableBytesWritable, Result> t1) -> {
                                byte[] value = t1._2.getValue(Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.SEGMENTOS.toString()), Bytes.toBytes(idSegmento));
                                if (value == null) {
                                    return false;
                                } else {
                                    return Bytes.toBoolean(value);
                                }
                            }).map((Tuple2<ImmutableBytesWritable, Result> t1) -> RowFactory.create(Bytes.toString(t1._2.getRow()))),
                            schema
                    );

                    //union del resultado con valores true (pertenecen) con el resultado viejo excluyendo el actual con valor false (no pertenencen)
                    resultado = resultado.withColumn("value", functions.lit(true))
                            .union(oldMembers.except(resultado).withColumn("value", functions.lit(false)))
                            .cache();
                    
                    //conversion a pares para su almacenamiento en HBase
                    JavaPairRDD<ImmutableBytesWritable, Put> resultadoSegmento = resultado.toJavaRDD()
                            .mapToPair((Row t) -> {
                                Put put = new Put(Bytes.toBytes(t.getString(0)));
                                put.add(new KeyValue(Bytes.toBytes(t.getString(0)), Bytes.toBytes(EligibilidadMiembro.ColumnasFamilia.SEGMENTOS.toString()), Bytes.toBytes(idSegmento), Bytes.toBytes(t.getBoolean(1))));
                                return new Tuple2<>(new ImmutableBytesWritable(), put);
                            });
                    long count = 0;
                    try {
                        Put put = new Put(Bytes.toBytes(idSegmento));//creacion de objeto de row a almacenar para hbase
                        List<Row> miembros = resultado.collectAsList();//recoleccion de resultados de miembros a memoria local
                        //recorrido de lista de miembros y el agregado del identificador de miembro en el row junto con el valor del uuid generado
                        for (Row t : miembros) {
                            String idMiembro = t.getAs(Miembro.Atributos.ID_MIEMBRO.toString());//uuid del miembro dentro de cada Row de eligibles
                            boolean value = t.getAs("value");
                            put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.MIEMBROS.toString()), Bytes.toBytes(idMiembro), Bytes.toBytes(value));
                            if (value) {
                                count++;
                            }
                        }
                        //agregado de valores adicionales
                        put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.ULTIMA_ACTUALIZACION.toString()), Bytes.toBytes(Calendar.getInstance().getTimeInMillis()));
                        put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.CANTIDAD_MIEMBROS.toString()), Bytes.toBytes(count));
                        put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.LISTAS_PROCESADAS.toString()), Bytes.toBytes(aListasProcesadas.value()));
                        put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.LISTAS_FALLIDAS.toString()), Bytes.toBytes(aListasFallidas.value()));
                        put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.REGLAS_PROCESADAS.toString()), Bytes.toBytes(aReglasProcesadas.value()));
                        put.addColumn(Bytes.toBytes(EligiblesSegmento.ColumnasFamilia.DATA.toString()), Bytes.toBytes(EligiblesSegmento.ColumnasDATA.REGLAS_FALLIDAS.toString()), Bytes.toBytes(aReglasFallidas.value()));

                        //almacenamiento de eligibles del segmento
                        hbdbd.saveEligibilidadMiembro(resultadoSegmento);
                        hbdbd.saveEligiblesSegmento(put);
                        
                        System.out.println("Segmento Actualizado = "+idSegmento);
                        
                        aListasFallidas.reset();
                        aListasProcesadas.reset();
                        aReglasFallidas.reset();
                        aReglasProcesadas.reset();
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                }
            });
            
        } catch (Exception e) {
            System.out.println("error procesando la actualizacion de segmento(s)... "+e.getMessage());
        }

        jsc.start();
        jsc.awaitTermination();
//        jsc.awaitTerminationOrTimeout(43200000);
    }
    
}
