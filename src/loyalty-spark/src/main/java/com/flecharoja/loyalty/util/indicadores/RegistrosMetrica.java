package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class RegistrosMetrica {
    public static final String NOMBRE_TABLA = "REGISTRO_METRICA";
    
    public enum ColumnasFamilia {
        DATA;
    }
    
    public enum ColumnasDATA {
        METRICA,
        MIEMBRO,
        CANTIDAD,
        DISPONIBLE,
        VENCIDO;
    }
}
