package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class ValorAtbDinMiembro {
    public static final String NOMBRE_TABLA = "MIEMBRO_ATRIBUTO";
    
    public enum NombreColumnas {
        ID_ATRIBUTO,
        ID_MIEMBRO,
        VALOR;
    }
}
