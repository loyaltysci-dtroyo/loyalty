package com.flecharoja.loyalty.exception;

/**
 *
 * @author svargas
 */
public class ReglaException extends Exception {

    /**
     * Constructs an instance of <code>ReglaException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public ReglaException(String msg) {
        super(msg);
    }
}
