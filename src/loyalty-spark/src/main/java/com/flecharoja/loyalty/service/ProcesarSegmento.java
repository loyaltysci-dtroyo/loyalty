package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.data.MariaDBData;
import com.flecharoja.loyalty.util.AccionSegmento;
import com.flecharoja.loyalty.util.indicadores.ListaReglasSegmento;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.util.LongAccumulator;

/**
 * Clase que se encarga el procesamiento de listas de reglas de un segmento por
 * su identificador
 *
 * @author svargas
 */
public class ProcesarSegmento {

    private final SparkSession spark; //sesion de spark
    private final JavaSparkContext javaContext; //contexto de java spark
    private final StructType schema; //esquema del dataframe a usar para resultados

    private final MariaDBData odbd; //instancia encargada de la carga de datos desde Oracle DB

    private final LongAccumulator aReglasFallidas, aReglasProcesadas, aListasFallidas, aListasProcesadas;

    public ProcesarSegmento(SparkSession spark, StructType schema, LongAccumulator aReglasFallidas, LongAccumulator aReglasProcesadas, LongAccumulator aListasFallidas, LongAccumulator aListasProcesadas) {
        this.schema = schema;
        this.spark = spark;
        this.javaContext = new JavaSparkContext(spark.sparkContext());

        this.odbd = new MariaDBData(spark);

        this.aReglasFallidas = aReglasFallidas;
        this.aReglasProcesadas = aReglasProcesadas;
        this.aListasFallidas = aListasFallidas;
        this.aListasProcesadas = aListasProcesadas;
    }

    public Dataset<Row> inicio(String idSegmento, AccionSegmento accion) {
        //lectura de registros de listas de reglas validas por segmento en la bases de datos
        Dataset<Row> datasetListasReglas = odbd.getListasReglas();

        //obtencion de listas de reglas de un segmento por el identificador del segmento, seleccionando el identificador de la lista, orden e indicador de operador y ordenado por el orden mismo
        Dataset<Row> filter1 = datasetListasReglas.where(new Column(ListaReglasSegmento.NombreColumnas.ID_SEGMENTO.toString())
                .equalTo(idSegmento))
                .select(new Column(ListaReglasSegmento.NombreColumnas.ID_LISTA.toString()), new Column(ListaReglasSegmento.NombreColumnas.ORDEN.toString()), new Column(ListaReglasSegmento.NombreColumnas.IND_OPERADOR.toString()))
                .orderBy(new Column(ListaReglasSegmento.NombreColumnas.ORDEN.toString()));

        //definicion del esquema a usar en el data frame resultado de miembros eligibles (solo el uuid de miembros)
        Dataset<Row> resultado = spark.createDataFrame(javaContext.emptyRDD(), schema);

        //recoleccion a memoria local del dataset como lista de "Row" y su recorrido
        for (Row row : filter1.collectAsList()) {
            Long orden = row.getAs(ListaReglasSegmento.NombreColumnas.ORDEN.toString()); //variable con el orden de las listas
            if (orden==1) {
                //en el caso de ser la primera lista, se procesa y se guarda el resultado directamente
                try {
                    resultado = new ProcesarListaReglas(spark, schema, aReglasFallidas, aReglasProcesadas).inicio(row.getAs(ListaReglasSegmento.NombreColumnas.ID_LISTA.toString()), accion);
                    aListasProcesadas.add(1);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    aListasFallidas.add(1);
                }
            } else {
                //se procesa la lista y se guarda el resultado en un dataframe temporal
                Dataset<Row> listaProcesada;
                try {
                    listaProcesada = new ProcesarListaReglas(spark, schema, aReglasFallidas, aReglasProcesadas).inicio(row.getAs(ListaReglasSegmento.NombreColumnas.ID_LISTA.toString()), accion);
                    aListasProcesadas.add(1);
                    
                    String indOperador = row.getAs(ListaReglasSegmento.NombreColumnas.IND_OPERADOR.toString()); //variable con el indicador del operador de la lista
                    switch (ListaReglasSegmento.ValoresIndOperador.get(indOperador)) {
                        case AND: {
                            //en el caso de un operador AND, se intersecta la lista resultado con la temporal
                            resultado = resultado.intersect(listaProcesada);
                            break;
                        }
                        case OR: {
                            //en el caso de un operador OR, se unen ambas listas y se eliminan duplicados
                            resultado = resultado.union(listaProcesada).distinct();
                            break;
                        }
                        case NOT: {
                            //en el caso de un operador NOT, se eliminan de la lista resultado, los resultados de la temporal
                            resultado = resultado.except(listaProcesada);
                            break;
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    aListasFallidas.add(1);
                }
            }
        }
        return resultado;
    }
}
