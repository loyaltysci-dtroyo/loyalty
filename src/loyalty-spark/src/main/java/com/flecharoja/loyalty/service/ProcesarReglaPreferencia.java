package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.data.MariaDBData;
import com.flecharoja.loyalty.exception.ReglaException;
import com.flecharoja.loyalty.util.indicadores.ReglasSegmento;
import com.flecharoja.loyalty.util.indicadores.RespuestaPreferencia;
import java.util.Arrays;
import java.util.List;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;

/**
 * Clase que se encarga de ejecutar una regla de tipo regla de respuesta de mision de encuesta
 *
 * @author svargas
 */
public class ProcesarReglaPreferencia {

    private final SparkSession spark; //sesion de spark
    private final JavaSparkContext javaContext; //contexto de java spark
    private final StructType schema; //esquema del dataframe a usar para resultados

    private final MariaDBData odbd; //instancia encargada de la carga de datos desde MariaDB

    public ProcesarReglaPreferencia(SparkSession spark, StructType schema) {
        this.schema = schema;
        this.spark = spark;
        this.javaContext = new JavaSparkContext(spark.sparkContext());

        this.odbd = new MariaDBData(spark);
    }

    /**
     * Metodo que se encarga de procesar el metadata de una regla de metrica
     * cambio, para el calculo de miembros validos
     *
     * @param regla Informacion de una regla de metrica cambio
     * @param miembros lista opcional de miembros sobre cuales trabajar
     * @return dataframe de una serie de identificadores de miembros
     * @throws com.flecharoja.loyalty.exception.ReglaException
     */
    public Dataset<Row> inicio(Row regla, List<String> miembros) throws ReglaException {
        //obtencion del metadata de la regla
        String idPreferencia = regla.getAs(ReglasSegmento.Preferencia.NombreColumnas.ID_PREFERENCIA.toString());
        String indOperador = regla.getAs(ReglasSegmento.Preferencia.NombreColumnas.IND_OPERADOR.toString());
        String valorComparacion = regla.getAs(ReglasSegmento.Preferencia.NombreColumnas.VALOR_COMPARACION.toString());

        Dataset<Row> filter = odbd.getRespuestasPreferencias()
                .filter((row) -> idPreferencia.equals(row.getAs(RespuestaPreferencia.NombreColumnas.ID_PREFERENCIA.toString())))
                .filter((row) -> {
                    String valor = row.getAs(RespuestaPreferencia.NombreColumnas.RESPUESTA.toString());
            
                    //segun el indicador de operador se realiza la operacion numerica entre el valor de comparacion con el valor sumado
                    switch (ReglasSegmento.ValoresIndOperador.get(indOperador)) {
                        case CONTIENE: {
                            return valor.toUpperCase().trim().contains(valorComparacion.toUpperCase().trim());
                        }
                        case ES: {
                            return valorComparacion.trim().equalsIgnoreCase(valor.trim());
                        }
                        case ES_UNO_DE: {
                            return valor.toUpperCase().trim().contains(valorComparacion.toUpperCase().trim());
                        }
                        case INICIA_CON: {
                            return valor.toUpperCase().trim().startsWith(valorComparacion.toUpperCase().trim());
                        }
                        case NO_CONTIENE: {
                            return !valor.toUpperCase().trim().contains(valorComparacion.toUpperCase().trim());
                        }
                        case NO_ES: {
                            return !valorComparacion.trim().equalsIgnoreCase(valor.trim());
                        }
                        case NO_ES_UNO_DE: {
                            return !valor.toUpperCase().trim().contains(valorComparacion.toUpperCase().trim());
                        }
                        case TERMINA_CON: {
                            return valor.toUpperCase().trim().endsWith(valorComparacion.toUpperCase().trim());
                        }
                        case OPCIONES_CUALQUIERA: {
                            for (String opcionValor : valor.split("\n")) {
                                for (String opcionComparacion : valorComparacion.split("\n")) {
                                    if (opcionValor.equals(opcionComparacion)) {
                                        return true;
                                    }
                                }
                            }
                            return false;
                        }
                        case OPCIONES_SON: {
                            return Arrays.stream(valor.split("\n")).allMatch((opcionValor) -> {
                                for (String opcionComparacion : valorComparacion.split("\n")) {
                                    if (opcionValor.equals(opcionComparacion)) {
                                        return true;
                                    }
                                }
                                return false;
                            });
                        }
                        default: {
                            throw new ReglaException("Indicador de operador no valido");
                        }
                    }
                });

        // se retorna un dataset con solo el identificador de miembro del resultado de los filtros
        return filter.select(new Column(RespuestaPreferencia.NombreColumnas.ID_MIEMBRO.toString()));
    }
}
