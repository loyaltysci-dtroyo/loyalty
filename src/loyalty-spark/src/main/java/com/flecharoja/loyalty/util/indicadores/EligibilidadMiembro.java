package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class EligibilidadMiembro {
    public static final String NOMBRE_TABLA = "ELEGIBILIDAD_MIEMBRO";
    
    public enum ColumnasFamilia {
        REGLAS,
        SEGMENTOS;
    }
}
