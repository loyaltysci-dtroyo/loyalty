package com.flecharoja.loyalty.util.indicadores;

/**
 *
 * @author svargas
 */
public class PosicionesGruposTabPos {
    public static final String NOMBRE_TABLA = "TABPOS_GRUPOMIEMBRO";
    
    public enum NombreColumnas {
        ID_TABLA,
        ID_GRUPO,
        ACUMULADO;
    }
}
