#!/bin/bash

#auto recalculo de reglas por eventos registrados en kafka
$SPARK_HOME/bin/spark-submit --class com.flecharoja.loyalty.service.AutoRecalculoReglas --master spark://spark.flecharoja.com:6066 --deploy-mode cluster --supervise /opt/spark/jars/loyalty-spark-1.0-SNAPSHOT.jar

#auto actualizacion de segmentos por eventos registrados en kafka
$SPARK_HOME/bin/spark-submit --class com.flecharoja.loyalty.service.AutoActualizacionSegmentos --master spark://spark.flecharoja.com:6066 --deploy-mode cluster --supervise /opt/spark/jars/loyalty-spark-1.0-SNAPSHOT.jar
