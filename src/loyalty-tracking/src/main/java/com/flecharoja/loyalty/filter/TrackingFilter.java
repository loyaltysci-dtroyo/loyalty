package com.flecharoja.loyalty.filter;

import com.flecharoja.loyalty.service.TrackingBean;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * Filtro que se aplica a la hora de la peticion de la imagen "carnada" repartida en correos electronicos para el rastreo del abrir de estos
 *
 * @author svargas
 */
public class TrackingFilter implements Filter{
    
    private FilterConfig filterConfig = null;
    
    @EJB
    TrackingBean bean;
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //se intentan obtener los parametros definidos en los enlaces repartidos
        String idInstancia = request.getParameter("idi");
        String idMiembro = request.getParameter("idm");
        //si ambos estan, se registra la vista del email
        if (idInstancia!=null && idMiembro!=null) {
            bean.registerVistaEmail(idInstancia, idMiembro);
        }
        
        //definicion de headers extras en la respuesta para evitar que la imagen se ponga en cache
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate, max-age=0, post-check=0, pre-check=0");
        httpResponse.setHeader("Pragma", "no-cache");
        httpResponse.setDateHeader("Expires", 0);
        httpResponse.setStatus(HttpServletResponse.SC_OK);
        
        //se sigue con la cadena
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
    
}
