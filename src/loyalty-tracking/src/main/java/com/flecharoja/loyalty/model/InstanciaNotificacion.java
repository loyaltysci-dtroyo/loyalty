package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Tabla que guarda las instancias referenciales de correos/push notificaciones creadas al ser enviadas
 *
 * @author svargas
 */
@Entity
@Table(name = "INSTANCIA_NOTIFICACION")
public class InstanciaNotificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InstanciaNotificacionPK instanciaNotificacionPK;
    
    @Column(name = "FECHA_VISTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVisto;

    public InstanciaNotificacion() {
    }

    public InstanciaNotificacionPK getInstanciaNotificacionPK() {
        return instanciaNotificacionPK;
    }

    public void setInstanciaNotificacionPK(InstanciaNotificacionPK instanciaNotificacionPK) {
        this.instanciaNotificacionPK = instanciaNotificacionPK;
    }

    public Date getFechaVisto() {
        return fechaVisto;
    }

    public void setFechaVisto(Date fechaVisto) {
        this.fechaVisto = fechaVisto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instanciaNotificacionPK != null ? instanciaNotificacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstanciaNotificacion)) {
            return false;
        }
        InstanciaNotificacion other = (InstanciaNotificacion) object;
        if ((this.instanciaNotificacionPK == null && other.instanciaNotificacionPK != null) || (this.instanciaNotificacionPK != null && !this.instanciaNotificacionPK.equals(other.instanciaNotificacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.InstanciaNotificacion[ instanciaNotificacionPK=" + instanciaNotificacionPK + " ]";
    }
    
}
