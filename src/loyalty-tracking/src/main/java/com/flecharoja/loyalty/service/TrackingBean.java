package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.InstanciaNotificacion;
import com.flecharoja.loyalty.model.InstanciaNotificacionPK;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Bean encargado del almacenamiento de vista de correo electronico
 *
 * @author svargas
 */
@Stateless
public class TrackingBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-tracking_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public void registerVistaEmail(String idInstancia, String idMiembro) {
        //se busca la instancia de envio de correo con el identificador sobre el miembro especificado
        InstanciaNotificacion in = em.find(InstanciaNotificacion.class, new InstanciaNotificacionPK(idInstancia, idMiembro));
        //mientras que exista y no se haya definido el campo de fecha de visto, se actualiza tal campo
        if (in!=null && in.getFechaVisto()==null) {
            in.setFechaVisto(new Date());
            em.merge(in);
        }
    }
}
