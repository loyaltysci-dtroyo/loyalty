package com.flecharoja.loyalty.util;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 *
 * @author svargas
 */
public class MyTinyUrlUtils implements AutoCloseable{
    
    private final Client client;

    public MyTinyUrlUtils() {
        this.client = ClientBuilder.newClient();
    }
    
    public String generateUrl(String url) {
        WebTarget target = client.target("https://tinyurl.com").path("api-create.php").queryParam("url", url);
        Response response = target.request().get();
        if (response.getStatusInfo().getFamily()==Response.Status.Family.SUCCESSFUL) {
            return response.readEntity(String.class).replaceFirst("http://", "https://");
        } else {
            return null;
        }
    }

    @Override
    public void close() throws Exception {
        this.client.close();
    }
    
}
