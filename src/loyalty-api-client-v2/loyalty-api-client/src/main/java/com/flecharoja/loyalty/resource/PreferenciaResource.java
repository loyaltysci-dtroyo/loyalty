package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.Preferencia;
import com.flecharoja.loyalty.model.PreferenciaMin;
import com.flecharoja.loyalty.service.PreferenciaBean;
import com.flecharoja.loyalty.util.Indicadores;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "Perfil del cliente")
@Path("perfil")
public class PreferenciaResource {
    
    @EJB
    PreferenciaBean preferenciaBean;
    
    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;
    
    
    /**
     * Método que obtiene las preferencias que existen
     * @return lista de preferencias
     */
    @ApiOperation(value = "Obtener las preferencias",
            responseContainer = "List",
            response = Preferencia.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("preferencia")
    @Produces(MediaType.APPLICATION_JSON)
    public List<PreferenciaMin> getPreferencias(){
        try{
            context.getUserPrincipal().getName();
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return preferenciaBean.getPreferencias(request.getLocale());
    }
    
     /**
     * Método que obtiene la información mas detallada de la preferencia
     * @param idPreferencia identificador de la preferencia
     * @return informacion de la preferencia
     */
    @ApiOperation(value = "Obtener detalle preferencia",
            response = Preferencia.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("preferencia/{idPreferencia}")
    @Produces(MediaType.APPLICATION_JSON)
    public Preferencia getPreferencia(
            @ApiParam(value = "Identificador de la preferencia", required = true)
            @PathParam("idPreferencia") String idPreferencia){
        String idMiembro;
        try{
            idMiembro = context.getUserPrincipal().getName();
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return preferenciaBean.getPreferencia(idPreferencia, idMiembro, request.getLocale());
    }
    
    
    /**
     * Método que actualiza o registra una respuesta a una preferencia por parte del miembro en sesión
     * @param idPreferencia identificador de la preferencia
     * @param valor valor a tomar
     */
    @ApiOperation(value = "Modificación o registro de una respuesta a una preferencia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Path("preferencia/{idPreferencia}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateRespuestaPreferencia(
            @ApiParam(value = "Identificador de la preferencia", required = true)
            @PathParam("idPreferencia") String idPreferencia,
            @ApiParam(value = "Valor que toma la respuesta" , required = true)
            String valor){
                
        String idMiembro;
        try{
            idMiembro = context.getUserPrincipal().getName();
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        preferenciaBean.updateRespuestaPreferencia(idMiembro,idPreferencia,valor, request.getLocale());
    }
    
}
