package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author svargas
 */
@Embeddable
public class WorkflowListaMiembroPK implements Serializable {

    @Column(name = "ID_MIEMBRO")
    private String idMiembro;
    
    @Column(name = "ID_WORKFLOW")
    private String idWorkflow;

    public WorkflowListaMiembroPK() {
    }

    public WorkflowListaMiembroPK(String idMiembro, String idWorkflow) {
        this.idMiembro = idMiembro;
        this.idWorkflow = idWorkflow;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIdWorkflow() {
        return idWorkflow;
    }

    public void setIdWorkflow(String idWorkflow) {
        this.idWorkflow = idWorkflow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        hash += (idWorkflow != null ? idWorkflow.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkflowListaMiembroPK)) {
            return false;
        }
        WorkflowListaMiembroPK other = (WorkflowListaMiembroPK) object;
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        if ((this.idWorkflow == null && other.idWorkflow != null) || (this.idWorkflow != null && !this.idWorkflow.equals(other.idWorkflow))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.WorkflowListaMiembroPK[ idMiembro=" + idMiembro + ", idWorkflow=" + idWorkflow + " ]";
    }
    
}
