package com.flecharoja.loyalty.util;

/**
 *
 * @author svargas
 */
public class Indicadores {
    public static final Character MIEMBRO_IND_ESTADO_ACTIVO = 'A';
    public static final Character MIEMBRO_IND_ESTADO_INACTIVO = 'I';
    
    public static final char INCLUIDO = 'I';
    public static final char EXCLUIDO = 'E';
    
    public static final Character TABLA_POSICIONES_IND_TIPO_MIEMBROS = 'U';
    public static final Character TABLA_POSICIONES_IND_TIPO_GRUPOS = 'G';
    public static final Character TABLA_POSICIONES_IND_TIPO_GRUPO_ESPECIFICO = 'E';
    
    public static final Character PREMIO_IND_TIPO_CERTIFICADO = 'C';
    public static final Character PREMIO_IND_TIPO_PRODUCTO = 'P';
    
    public static final String PAGINACION_RANGO_ACEPTADO = "Accept-Range";
    public static final int PAGINACION_RANGO_ACEPTADO_VALOR = 50;
    public static final String PAGINACION_RANGO_DEFECTO = "10";
    public static final String PAGINACION_RANGO_CONTENIDO = "Content-Range";

    public static final String URL_IMAGEN_PREDETERMINADA = "https://loyalty.flecharoja.com/resources/img/generic0.jpg";
    
    public static final int[] PUNTOS_JUEGO_MILLONARIO = {100,200,300,400,500,750,1000,1500,2000,3000,5000,7500,10000,15000,30000};

}
