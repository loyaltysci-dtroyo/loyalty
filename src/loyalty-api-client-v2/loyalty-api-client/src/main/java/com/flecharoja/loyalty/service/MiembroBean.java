/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.AtributoDinamico;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.IdentificacionMiembro;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.MiembroAtributo;
import com.flecharoja.loyalty.model.MiembroAtributoPK;
import com.flecharoja.loyalty.model.MiembroDataExtra;
import com.flecharoja.loyalty.model.NuevaPeticionCambioPassM;
import com.flecharoja.loyalty.model.PeticionCambioPassM;
import com.flecharoja.loyalty.model.PeticionEnvioNotificacionCustom;
import com.flecharoja.loyalty.model.PeticionReferirM;
import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.util.EnvioNotificacion;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.NotFoundException;
import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author wtencio
 */
@Stateless
public class MiembroBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;
    
    private final String URL_CLIENTE_RECUPERACION_PASS = "https://loyalty.clienteweb/recuperacion-credenciales";

    @EJB
    RegistroMetricaService registroMetricaService;

    @EJB
    BalanceBean balanceBean;

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public void updateDatosExtraMiembro(String idMiembro, MiembroDataExtra.TiposDispositivos td) {
        MiembroDataExtra miembro = em.find(MiembroDataExtra.class, idMiembro);
        if (miembro == null) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, Locale.US, "member_not_found");
        }
        miembro.setFechaUltimaConexion(new Date());
        miembro.setIndTipoDispositivo(td.getValue());
        em.merge(miembro);
    }

    public void setReferenciaMiembro(String idMiembro, PeticionReferirM data, Locale locale) {
        if (data == null || data.getIdMiembro()==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idMiembro");
        }
        String idMiembroReferente = data.getIdMiembro();
        
        if (idMiembroReferente == null || idMiembroReferente.equals(idMiembro)) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idMiembro");
        }
        MiembroDataExtra miembro = em.find(MiembroDataExtra.class, idMiembro);
        if (miembro == null) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, locale, "member_not_found");
        }
        Miembro miembroReferente = em.find(Miembro.class, idMiembroReferente);
        if (miembroReferente == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "member_not_found");
        }

        if (miembro.getMiembroReferente() != null) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "member_already_referenced");
        }

        miembro.setMiembroReferente(idMiembroReferente);
        em.merge(miembro);

        List<ConfiguracionGeneral> lista = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
        if (lista.isEmpty()) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "main_metric_not_found");
        }
        ConfiguracionGeneral configuracion = lista.get(0);
        //TODO cambiar el parametro de cantidad por el bono de referencia
        Date fechaRegistro = new Date();
        registroMetricaService.insertRegistroMetrica(new RegistroMetrica(fechaRegistro, configuracion.getIdMetricaInicial().getIdMetrica(), idMiembro, RegistroMetrica.TiposGane.REFERENCIA, configuracion.getBonoReferencia(), null));
        registroMetricaService.insertRegistroMetrica(new RegistroMetrica(fechaRegistro, configuracion.getIdMetricaInicial().getIdMetrica(), idMiembroReferente, RegistroMetrica.TiposGane.REFERENCIA, configuracion.getBonoReferencia(), null));
        
        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.MIEMBRO_REFERIDO, null, idMiembroReferente);
        } catch (Exception ex) {
            Logger.getLogger(MiembroBean.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    public void updateFCMTokenMiembro(String idMiembro, JsonObject data) {
        if (data == null || data.isEmpty() || !data.containsKey("token") || data.get("token").getValueType()!=JsonValue.ValueType.STRING) {
            System.err.println("Return nothing");
            return;
        }
        String token = data.getString("token");
        System.out.println("Token: " + token);
        
        System.out.println("ID Miembro: " + idMiembro);
        MiembroDataExtra miembro = em.find(MiembroDataExtra.class, idMiembro);
        
        if (miembro == null) {
            System.out.println("Miembro Null return nothing");
            return;
        }
        List<MiembroDataExtra> miembrosPasados = em.createNamedQuery("MiembroDataExtra.findByFcmTokenRegistro").setParameter("fcmTokenRegistro", token).getResultList();
        miembrosPasados.forEach((miembroPasado) -> {
            miembroPasado.setFcmTokenRegistro(null);
            em.merge(miembroPasado);
        });
        miembro.setFcmTokenRegistro(token);
        em.merge(miembro);
        System.out.println("Fin");
    }

    /**
     * Método que devuelve la informacion mas detallada del miembro en sesion
     *
     * @param idMiembro identificador del miembro
     * @param locale
     * @return miembro
     */
    public Miembro getMiembroDetalle(String idMiembro, Locale locale) {
        //SE RETORNA LA INFORMACION NO REQUERIDA Y REQUERIDA  DEL MIEMBRO QUE ESTA EN SESION
        Miembro miembro;
        try {
            miembro = em.find(Miembro.class, idMiembro);
            if (miembro == null) {
                throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, locale, "member_not_found");
            }
            //se busca la otra informacion en keycloak
            try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
                miembro.setNombreUsuario(keycloakUtils.getUsernameMember(idMiembro));
            }

            miembro.setBalance(balanceBean.getBalance(idMiembro, null, locale));
            
            miembro.setAtributoDinamicos(getAtributosDinamicosMiembro(idMiembro, locale));
        } catch (IllegalArgumentException | NotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }
        
        if (miembro.getIndCambioPass()==null) {
            miembro.setIndCambioPass(false);
        }
        
        if (miembro.getIndPolitica()==null) {
            miembro.setIndPolitica(false);
        }
        
        return miembro;
    }

    /**
     * Método que devuelve la informacion de los atributos dinamicos del miembro
     * en sesion
     *
     * @param idMiembro identificador del miembro
     * @return atributos dinamicos
     */
    private List<AtributoDinamico> getAtributosDinamicosMiembro(String idMiembro, Locale locale) {
        //SE BUSCAN LOS ATRIBUTOS DINAMICOS, SE TIENE QUE VERIFICAR QUE SI EXISTE ALGUNA RESPUESTA LA TRAIGA
        //Y SINO SE PONGA EL DEFAULT
        Map<String, Object> respuesta = new HashMap<>();

        List<AtributoDinamico> atributos = new ArrayList<>();
        List registros = new ArrayList();
        int total;

        try {
//            registros = em.createNamedQuery("AtributoDinamico.findAll").getResultList();

            atributos = em.createNamedQuery("AtributoDinamico.findActivosVisibles")
                    .setParameter("indEstado", AtributoDinamico.Estados.PUBLICADO.getValue())
                    .getResultList();

            List<MiembroAtributo> miembroAtributos = em.createNamedQuery("MiembroAtributo.findByIdMiembro")
                    .setParameter("idMiembro", idMiembro).getResultList();

            for (AtributoDinamico atributo : atributos) {
                for (MiembroAtributo miembroAtributo : miembroAtributos) {
                    if (atributo.getIdAtributo().equals(miembroAtributo.getMiembroAtributoPK().getIdAtributo())) {
                        atributo.setValorAtributoMiembro(miembroAtributo.getValor());
                    }
                }
            }
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "argument_invalid", "idMiembro");
        }

        return atributos;
    }

    /**
     * Método que actualiza la informacion de datos no requeridos de un miembro
     *
     * @param idMiembro identificador del miembro
     * @param idAtributo identificador del atributo
     * @param valor valor a tomar el atributo
     * @param locale
     */
    public void updateAtributoDinamico(String idMiembro, String idAtributo, String valor, Locale locale) {
        MiembroAtributo miembroAtributo = new MiembroAtributo();
        try {
            //obtiene el listado de atributos con una respuesta
            List<MiembroAtributo> atributos = em.createNamedQuery("MiembroAtributo.findByIdMiembro")
                    .setParameter("idMiembro", idMiembro).getResultList();

            //verificar que exista la relacion miembro atributo pr actualizar el valor
            Boolean existe = existsAsignacion(atributos, idAtributo);
            if (existe) {
                miembroAtributo = em.find(MiembroAtributo.class, new MiembroAtributoPK(idMiembro, idAtributo));
                if (valor == null || valor.equals("null") || "".equals(valor)) {
                    em.remove(miembroAtributo);
                    em.flush();
                } else {
                    miembroAtributo.setValor(valor);
                    em.merge(miembroAtributo);
                    em.flush();
                }

            } else if (valor == null || "null".equals(valor) || "".equals(valor)) {

            } else {
                miembroAtributo = new MiembroAtributo(idMiembro, idAtributo);
                miembroAtributo.setFechaCreacion(Calendar.getInstance().getTime());
                miembroAtributo.setValor(valor);
                em.merge(miembroAtributo);
                em.flush();
            }

        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "attribute_not_found");
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que verifica si existe una relacion de un atributo con un miembro
     *
     * @param atributos lista de atributos con respuesta del miembro
     * @param idAtributo identificador del atributo
     * @return false si no existe, true si existe
     */
    public boolean existsAsignacion(List<MiembroAtributo> atributos, String idAtributo) {
        for (MiembroAtributo atributo : atributos) {
            if (atributo.getMiembroAtributoPK().getIdAtributo().equals(idAtributo)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que actualiza la informacion del miembro
     *
     * @param miembro info del miembro
     * @param idMiembro identificador del miembro
     * @param locale
     */
    public void updateMiembro(Miembro miembro, String idMiembro, Locale locale) {
        if (miembro == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "member_empty");
        }
        Miembro idMiembroBase;
        try {
            idMiembroBase = em.find(Miembro.class, miembro.getIdMiembro());
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "argument_invalid", "idMiembro");
        }
        miembro.setIndCambioPass(idMiembroBase.getIndCambioPass());

        if (!idMiembro.equals(idMiembroBase.getIdMiembro())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "argument_invalid", "idMiembro");
        }
        if (miembro.getCorreo() == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "member_email_invalid");
        }

        if (miembro.getPaisResidencia() != null && !existsPais(miembro.getPaisResidencia())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "country_invalid");
        }

        if (!miembro.getDocIdentificacion().equals(idMiembroBase.getDocIdentificacion())) {
            if (existsIdentificador(miembro.getDocIdentificacion())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "id_already_exists");
            }
        }

        if (!miembro.getCorreo().equals(idMiembroBase.getCorreo())) {
            if (existsCorreo(miembro.getCorreo())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "email_already_exists");
            }
            try (MyKeycloakUtils mku = new MyKeycloakUtils(locale)) {
                mku.editEmail(idMiembro, miembro.getCorreo());
            }
        }
        
        if (miembro.getContrasena()!=null) {
            if (validatePassword(miembro.getContrasena())) {
                try (MyKeycloakUtils mku = new MyKeycloakUtils(locale)) {
                    mku.resetPasswordMember(idMiembro, miembro.getContrasena());
                }
                if (miembro.getIndCambioPass()!=null && miembro.getIndCambioPass()) {
                    miembro.setIndCambioPass(null);
                }
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "pass_invalid");
            }
        }

        //conversion de valores de atributos opcionales a su par en mayuscula
        miembro.setFechaIngreso(idMiembroBase.getFechaIngreso());
        miembro.setIndEstadoMiembro(idMiembroBase.getIndEstadoMiembro());
        miembro.setFechaCreacion(idMiembroBase.getFechaCreacion());
//        miembro.setFechaUltimaConexion(idMiembroBase.getFechaUltimaConexion());
//        miembro.setIndTipoDispositivo(idMiembroBase.getIndTipoDispositivo());
//        miembro.setFcmTokenRegistro(idMiembroBase.getFcmTokenRegistro());
        miembro.setComentarios(idMiembroBase.getComentarios());
        miembro.setFrecuenciaCompra(idMiembroBase.getFrecuenciaCompra());
        miembro.setIndContactoEstado(idMiembroBase.getIndContactoEstado());
        miembro.setFechaExpiracion(idMiembroBase.getFechaExpiracion());
        miembro.setIndEducacion(miembro.getIndEducacion() != null ? Character.toUpperCase(miembro.getIndEducacion()) : null);
        miembro.setIndEstadoCivil(miembro.getIndEstadoCivil() != null ? Character.toUpperCase(miembro.getIndEstadoCivil()) : null);
        miembro.setIndGenero(miembro.getIndGenero() != null ? Character.toUpperCase(miembro.getIndGenero()) : null);

        String currentUrl = (String) em.createNamedQuery("Miembro.getAvatarFromMiembro").setParameter("idMiembro", miembro.getIdMiembro()).getSingleResult();
        String newUrl = null;
        //si el atributo de imagen viene nula (no deberia)
        if (miembro.getAvatar() == null || miembro.getAvatar().trim().isEmpty()) {
            miembro.setAvatar(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else //ver si el valor en el atributo de imagen, difiere del almacenado
        {
            if (!currentUrl.equals(miembro.getAvatar())) {
                try {
                    miembro.setAvatar(new URL(miembro.getAvatar()).toURI().toString());
                } catch (MalformedURLException | URISyntaxException e) {
                    //se le intenta subir
                    try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                        newUrl = filesUtils.uploadImage(miembro.getAvatar(), MyAwsS3Utils.Folder.AVATAR_MIEMBRO, null);
                        miembro.setAvatar(newUrl);
                    } catch (Exception ex) {
                        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                        throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                    }
                }
            }
        }

        Date fecha = new Date();
        miembro.setFechaModificacion(fecha);
        if(idMiembroBase.getFechaPolitica()==null && miembro.getIndPolitica()){
            miembro.setFechaPolitica(fecha);
        }

        try {
            if (miembro.getAtributoDinamicos()!=null && !miembro.getAtributoDinamicos().isEmpty()) {
                for (AtributoDinamico atributo : miembro.getAtributoDinamicos()) {
                    if (atributo.getValorAtributoMiembro() != null) {
                        this.updateAtributoDinamico(idMiembro, atributo.getIdAtributo(), atributo.getValorAtributoMiembro(), locale);
                    }
                }
            }
            em.merge(miembro);
            em.flush();

        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (newUrl != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(newUrl);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }

            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }

        }
        
        if (newUrl != null && !currentUrl.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(currentUrl);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }

        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.ACTUALIZACION_PERFIL, null, idMiembro);
        } catch (Exception ex) {
            Logger.getLogger(MiembroBean.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    public void solicitarRecuperacionContrasena(NuevaPeticionCambioPassM cambioPass, Locale locale) {
        String idMiembro;
        try {
            idMiembro = (String) em.createNamedQuery("Miembro.getIdMiembroCorreo").setParameter("correo", cambioPass.getEmail()).setParameter("indEstado", Indicadores.MIEMBRO_IND_ESTADO_ACTIVO).getSingleResult();
        } catch (NoResultException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "email_not_valid");
        }
        
        String idPeticion = new BigInteger(1024, new SecureRandom()).toString(32);
        
        Date fechaExpiracion = DateUtils.addHours(new Date(), 10);
        
        PeticionCambioPassM peticionCambioPass = new PeticionCambioPassM(idMiembro, idPeticion, fechaExpiracion);
        
        em.merge(peticionCambioPass);
        
        
        PeticionEnvioNotificacionCustom peticionEnvioNotificacion = new PeticionEnvioNotificacionCustom();
        ResourceBundle customNotifications;
        try {
            customNotifications = ResourceBundle.getBundle("custom_notifications.forgotpass", locale);
        } catch (NullPointerException | MissingResourceException e) {
            customNotifications = ResourceBundle.getBundle("custom_notifications.forgotpass", Locale.US);
        }
        peticionEnvioNotificacion.setCuerpo(customNotifications.getString("body"));
        peticionEnvioNotificacion.setTitulo(customNotifications.getString("subject"));
        peticionEnvioNotificacion.getMiembros().add(idMiembro);
        peticionEnvioNotificacion.getParametros().put("url", URL_CLIENTE_RECUPERACION_PASS+"?idm="+idMiembro+"&idp="+idPeticion);
        peticionEnvioNotificacion.setTipoMsj(PeticionEnvioNotificacionCustom.Tipos.EMAIL.getValue());
        
        if (new EnvioNotificacion().enviarNotificacion(peticionEnvioNotificacion)) {
            em.flush();
        } else {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "email_cant_be_send");
        }
    }
    
    public IdentificacionMiembro verificarPeticionCambioContrasena(String idMiembro, String idPeticion, Locale locale) {
        if (idMiembro==null || idPeticion==null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "argument_invalid", "idMiembro, idPeticion");
        }
        
        PeticionCambioPassM peticionCambioPassM = em.find(PeticionCambioPassM.class, idMiembro);
        if (peticionCambioPassM==null || !peticionCambioPassM.getIdPeticion().equalsIgnoreCase(idPeticion)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "argument_invalid", "idMiembro, idPeticion");
        }
        
        if (peticionCambioPassM.getFechaExpiracion().before(new Date())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "request_has_expire");
        }
        
        Miembro miembro = em.find(Miembro.class, idMiembro);
        if (miembro.getIndEstadoMiembro().equals(Miembro.Estados.INACTIVO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "member_not_active");
        }
        
        IdentificacionMiembro identificacionMiembro = new IdentificacionMiembro();
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            identificacionMiembro.setUsername(keycloakUtils.getUsernameMember(idMiembro));
        }
        identificacionMiembro.setEmail(miembro.getCorreo());
        
        return identificacionMiembro;
    }
    
    public void completarCambioContrasena(PeticionCambioPassM data, Locale locale) {
        if (data.getIdMiembro()==null || data.getIdPeticion()==null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "argument_invalid", "idMiembro, idPeticion");
        }
        
        PeticionCambioPassM peticionCambioPassM = em.find(PeticionCambioPassM.class, data.getIdMiembro());
        if (peticionCambioPassM==null || !peticionCambioPassM.getIdPeticion().equalsIgnoreCase(data.getIdPeticion())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "argument_invalid", "idMiembro, idPeticion");
        }
        
        if (peticionCambioPassM.getFechaExpiracion().before(new Date())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "request_has_expire");
        }
        
        Miembro miembro = em.find(Miembro.class, data.getIdMiembro());
        if (miembro.getIndEstadoMiembro().equals(Miembro.Estados.INACTIVO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "member_not_active");
        }
        
        if (data.getContrasena()==null || data.getContrasena().trim().isEmpty() || !validatePassword(data.getContrasena())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "pass_invalid");
        }
        
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            keycloakUtils.resetPasswordMember(data.getIdMiembro(), data.getContrasena());
        }
        
        em.remove(peticionCambioPassM);
    }

    /**
     * Método que registra un nuevo miembro al sistema con los datos basicos y
     * requeridows
     *
     * @param miembro informaciòn requerida del miembro
     * @param locale
     */
    public void registroCuenta(Miembro miembro, Locale locale) {
        if (miembro == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "member_empty");
        }

        if (miembro.getContrasena() == null || miembro.getCorreo() == null || miembro.getDocIdentificacion() == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "member_basic_information_empty");
        }

        if (existsIdentificador(miembro.getDocIdentificacion())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "id_already_exists");
        }

        if (existsCorreo(miembro.getCorreo())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "email_already_exists");
        }

        if (existsUserName(miembro.getNombreUsuario(),locale)) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "member_username_exists");
        }
        if (!validatePassword(miembro.getContrasena())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "pass_invalid");
        }

        Miembro miembroComplete = new Miembro();
        miembroComplete.setNombre(miembro.getNombre());
        miembroComplete.setApellido(miembro.getApellido());
        miembroComplete.setDocIdentificacion(miembro.getDocIdentificacion());
        miembroComplete.setCorreo(miembro.getCorreo());
        miembroComplete.setFechaIngreso(Calendar.getInstance().getTime());
        miembroComplete.setNumVersion(new Long(1));
        miembroComplete.setIndEstadoMiembro(Miembro.Estados.ACTIVO.getValue());
        miembroComplete.setAvatar(Indicadores.URL_IMAGEN_PREDETERMINADA);
        miembroComplete.setIndCambioPass(null);

        String idMember;
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            idMember = keycloakUtils.createMember(miembro.getNombreUsuario(), miembro.getContrasena(),miembro.getCorreo());
            keycloakUtils.resetPasswordMember(idMember, miembro.getContrasena());
        } catch (Exception e) {
            if (e instanceof MyException) {
                throw e;
            } else {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "register_to_idp_failed");
            }
        }
        if (idMember == null) {
            throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "register_to_idp_failed");
        }

        miembroComplete.setIdMiembro(idMember);
        List<ConfiguracionGeneral> lista = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
        if (lista.isEmpty()) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "general_configuration_not_found");
        }
        ConfiguracionGeneral configuracion = lista.get(0);

        Date fecha = new Date();
        miembroComplete.setFechaCreacion(fecha);
        miembroComplete.setFechaModificacion(fecha);

        miembroComplete.setIndContactoEmail(miembro.getIndContactoEmail() == null ? true : miembro.getIndContactoEmail());
        miembroComplete.setIndContactoEstado(miembro.getIndContactoEstado() == null ? true : miembro.getIndContactoEstado());
        miembroComplete.setIndContactoNotificacion(miembro.getIndContactoNotificacion() == null ? true : miembro.getIndContactoNotificacion());
        miembroComplete.setIndContactoSms(miembro.getIndContactoSms() == null ? true : miembro.getIndContactoSms());

        try {
            em.persist(miembroComplete);
            em.flush();
        } catch (ConstraintViolationException e) {
            try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
                keycloakUtils.deleteMember(idMember);
            }

            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "argument_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));

        }

        registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), configuracion.getIdMetricaInicial().getIdMetrica(), idMember, RegistroMetrica.TiposGane.BIENVENIDA, configuracion.getBonoEntrada(), null));
        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.NUEVO_MIEMBRO, null, idMember);
        } catch (Exception ex) {
            Logger.getLogger(MiembroBean.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    /**
     * Método que genera una nueva contraseña para el miembro
     *
     * @return cadena de caracteres
     */
    public String getCadenaAlfanumAleatoria() {
        String cadenaAleatoria = "";
        long milis = new java.util.GregorianCalendar().getTimeInMillis();
        Random r = new Random(milis);
        int i = 0;
        while (i < 8) {
            char c = (char) r.nextInt(255);
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {
                cadenaAleatoria += c;
                i++;
            }
        }
        return cadenaAleatoria;
    }

    /**
     * verifica que si existe el pais
     *
     * @param paisResidencia
     * @return
     */
    private boolean existsPais(String paisResidencia) {
        return ((Long) em.createNamedQuery("Pais.countByAlfa3").setParameter("alfa3", paisResidencia).getSingleResult()) > 0;
    }

    /**
     * Verifica si existe el documento de identificacion
     *
     * @param docIdentificacion
     * @return
     */
    private boolean existsIdentificador(String docIdentificacion) {
        return ((Long) em.createNamedQuery("Miembro.countIdentificador").setParameter("docIdentificacion", docIdentificacion).getSingleResult()) > 0;
    }

    /**
     * Método que verifica si un correo electronico ya existe en algún otro
     * usuario
     *
     * @param correo correo a verificar
     * @return id del usuario si ya existe el correo, o null si no existe el
     * correo
     */
    public boolean existsCorreo(String correo) {
        return ((Long) em.createNamedQuery("Miembro.countCorreo").setParameter("correo", correo).getSingleResult()) > 0;
    }

    /**
     * Metodo que verifica si existe el nombre de usuario
     *
     * @param userName
     * @return Respuesta con un miembro con toda su informacion
     */
    public boolean existsUserName(String userName, Locale locale) {
        String idMiembro;
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            idMiembro = keycloakUtils.getIdMember(userName);
        }
        return idMiembro != null;

    }

    public boolean validatePassword(String password) {
        Pattern pswNamePtrn
                = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=.,_]).{6,100})");
        Matcher mtch = pswNamePtrn.matcher(password);
        return mtch.matches();
    }

}
