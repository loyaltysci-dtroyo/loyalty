package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author svargas
 */
@Embeddable
public class NoticiaListaSegmentoPK implements Serializable {

    @Column(name = "ID_SEGMENTO")
    private String idSegmento;

    @Column(name = "ID_NOTICIA")
    private String idNoticia;

    public NoticiaListaSegmentoPK() {
    }

    public NoticiaListaSegmentoPK(String idSegmento, String idNoticia) {
        this.idSegmento = idSegmento;
        this.idNoticia = idNoticia;
    }

    public String getIdSegmento() {
        return idSegmento;
    }

    public void setIdSegmento(String idSegmento) {
        this.idSegmento = idSegmento;
    }

    public String getIdNoticia() {
        return idNoticia;
    }

    public void setIdNoticia(String idNoticia) {
        this.idNoticia = idNoticia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSegmento != null ? idSegmento.hashCode() : 0);
        hash += (idNoticia != null ? idNoticia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NoticiaListaSegmentoPK)) {
            return false;
        }
        NoticiaListaSegmentoPK other = (NoticiaListaSegmentoPK) object;
        if ((this.idSegmento == null && other.idSegmento != null) || (this.idSegmento != null && !this.idSegmento.equals(other.idSegmento))) {
            return false;
        }
        if ((this.idNoticia == null && other.idNoticia != null) || (this.idNoticia != null && !this.idNoticia.equals(other.idNoticia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.NoticiaListaSegmentoPK[ idSegmento=" + idSegmento + ", idNoticia=" + idNoticia + " ]";
    }
    
}
