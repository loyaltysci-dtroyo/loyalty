package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NOTICIA_CATEGORIA_NOTICIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NoticiaCategoriaNoticia.findByIdCategoria", query = "SELECT n.noticia FROM NoticiaCategoriaNoticia n WHERE n.noticiaCategoriaNoticiaPK.idCategoria = :idCategoria and n.noticia.indEstado = :indEstado")
})
public class NoticiaCategoriaNoticia implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected NoticiaCategoriaNoticiaPK noticiaCategoriaNoticiaPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_CATEGORIA", referencedColumnName = "ID_CATEGORIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CategoriaNoticia categoria;
    
    @JoinColumn(name = "ID_NOTICIA", referencedColumnName = "ID_NOTICIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Noticia noticia;

    public NoticiaCategoriaNoticia() {
    }

    public NoticiaCategoriaNoticia(String idCategoria, String idNoticia, Date fechaCreacion, String usuarioCreacion) {
        this.noticiaCategoriaNoticiaPK = new NoticiaCategoriaNoticiaPK(idCategoria, idNoticia);
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public NoticiaCategoriaNoticiaPK getNoticiaCategoriaNoticiaPK() {
        return noticiaCategoriaNoticiaPK;
    }

    public void setNoticiaCategoriaNoticiaPK(NoticiaCategoriaNoticiaPK noticiaCategoriaNoticiaPK) {
        this.noticiaCategoriaNoticiaPK = noticiaCategoriaNoticiaPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public CategoriaNoticia getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriaNoticia categoria) {
        this.categoria = categoria;
    }

    public Noticia getNoticia() {
        return noticia;
    }

    public void setNoticia(Noticia noticia) {
        this.noticia = noticia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noticiaCategoriaNoticiaPK != null ? noticiaCategoriaNoticiaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NoticiaCategoriaNoticia)) {
            return false;
        }
        NoticiaCategoriaNoticia other = (NoticiaCategoriaNoticia) object;
        if ((this.noticiaCategoriaNoticiaPK == null && other.noticiaCategoriaNoticiaPK != null) || (this.noticiaCategoriaNoticiaPK != null && !this.noticiaCategoriaNoticiaPK.equals(other.noticiaCategoriaNoticiaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.NoticiaCategoriaNoticia[ noticiaCategoriaNoticiaPK=" + noticiaCategoriaNoticiaPK + " ]";
    }
    
}
