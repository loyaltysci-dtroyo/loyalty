/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Premio.findAll", query = "SELECT p FROM Premio p")
    ,
    @NamedQuery(name = "Premio.findByIdPremio", query = "SELECT p FROM Premio p WHERE p.idPremio = :idPremio")
})
public class Premio implements Serializable {

    public enum EstadosMiembro {
        REDIMIDO('R'),
        DISPONIBLE('D'), 
        EXPIRADO('E');

        private final char value;
        private static final Map<Character, EstadosMiembro> lookup = new HashMap<>();

        private EstadosMiembro(char value) {
            this.value = value;
        }

        static {
            for (EstadosMiembro estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static EstadosMiembro get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }
    
    public enum Tipo {
        CERTIFICADO('C'),
        PRODUCTO('P');

        private final char value;
        private static final Map<Character, Tipo> lookup = new HashMap<>();

        private Tipo(char value) {
            this.value = value;
        }

        static {
            for (Tipo tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static Tipo get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_PREMIO")
    @ApiModelProperty(value = "Identificador único del premio ", required = true)
    private String idPremio;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_PREMIO")
    @ApiModelProperty(value = "Indicador del tipo de premio", example = "Certificado, producto físico", required = true)
    private Character indTipoPremio;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ENVIO")
    @ApiModelProperty(value = "Indicador de envío del premio al miembro a domicilio", required = true)
    private Boolean indEnvio;

    @Basic(optional = false)
    @NotNull
    @Column(name = "VALOR_MONEDA")
    @ApiModelProperty(value = "Valor del premio en métrica", required = true)
    private Double valorMoneda;

    @Column(name = "VALOR_EFECTIVO")
    @ApiModelProperty(value = "Valor del premio en efectivo")
    private Double valorEfectivo;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN_ARTE")
    @ApiModelProperty(value = "Imagen representativa del premio", required = true)
    private String imagenArte;

    @Size(max = 100)
    @Column(name = "TRACKING_CODE")
    @ApiModelProperty(value = "Tracking code")
    private String trackingCode;

    @Size(max = 100)
    @Column(name = "SKU")
    @ApiModelProperty(value = "SKU del premio")
    private String sku;

    @Size(max = 150)
    @Column(name = "ENCABEZADO_ARTE")
    @ApiModelProperty(value = "Nombre del producto", required = true)
    private String encabezadoArte;

    @Size(max = 300)
    @Column(name = "SUBENCABEZADO_ARTE")
    @ApiModelProperty(value = "Información adicional del nombre del premio")
    private String subencabezadoArte;

    @Size(max = 500)
    @Column(name = "DETALLE_ARTE")
    @ApiModelProperty(value = "Descripción del premio", required = true)
    private String detalleArte;

    @Column(name = "CANT_MIN_ACUMULADO")
    @ApiModelProperty(value = "Cantidad minima de métrica que se tiene que tener acumulada para redimir el premio", required = true)
    private Double cantMinAcumulado;

    @Transient
    @ApiModelProperty(value = "Código de certificado si el miembro a redimido alguno")
    private String codigoCertificado;

    @Transient
    @ApiModelProperty(value = "Fecha en que se realizó la redención")
    private Date fechaRedencion;

    @Transient
    @ApiModelProperty(value = "Indicador del estado del premio para un miembro", example = "Redemido, disponible")
    private Character indEstado;

    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    @ApiModelProperty(value = "Información de la métrica de la cual se puede redimir el premio", required = true)
    private Metrica idMetrica;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_PREMIO")
    @ApiModelProperty(value = "Lista de códigos de certificados que tiene un premio")
    private List<CodigoCertificado> codigos;

    @Column(name = "PRECIO_PROMEDIO")
    private Double precioPromedio;

    @Column(name = "CANT_TOTAL")
    private Long cantTotal;
    
    @Column(name = "TOTAL_EXISTENCIAS")
    private Long totalExistencias;
    
    @Column(name = "CANT_DIAS_VENCIMIENTO")
    @ApiModelProperty(value = "Cantidad de dias que tiene un miembro para recoger un premio ganado")
    private Integer cantDiasVencimiento;
    
    @Transient
    @ApiModelProperty(value = "Fecha limite para reclamar el premio")
    private Date fechaExpiracion;

    public Premio() {
    }

    public Premio(String idPremio) {
        this.idPremio = idPremio;
    }

    public String getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(String idPremio) {
        this.idPremio = idPremio;
    }

    public Character getIndTipoPremio() {
        return indTipoPremio;
    }

    public void setIndTipoPremio(Character indTipoPremio) {
        this.indTipoPremio = indTipoPremio;
    }

    public Boolean getIndEnvio() {
        return indEnvio;
    }

    public void setIndEnvio(Boolean indEnvio) {
        this.indEnvio = indEnvio;
    }

    public Double getValorMoneda() {
        return valorMoneda;
    }

    public void setValorMoneda(Double valorMoneda) {
        this.valorMoneda = valorMoneda;
    }

    public Double getValorEfectivo() {
        return valorEfectivo;
    }

    public void setValorEfectivo(Double valorEfectivo) {
        this.valorEfectivo = valorEfectivo;
    }

    public String getImagenArte() {
        return imagenArte;
    }

    public void setImagenArte(String imagenArte) {
        this.imagenArte = imagenArte;
    }

    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getEncabezadoArte() {
        return encabezadoArte;
    }

    public void setEncabezadoArte(String encabezadoArte) {
        this.encabezadoArte = encabezadoArte;
    }

    public String getSubencabezadoArte() {
        return subencabezadoArte;
    }

    public void setSubencabezadoArte(String subencabezadoArte) {
        this.subencabezadoArte = subencabezadoArte;
    }

    public String getDetalleArte() {
        return detalleArte;
    }

    public void setDetalleArte(String detalleArte) {
        this.detalleArte = detalleArte;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }

    public Double getCantMinAcumulado() {
        return cantMinAcumulado;
    }

    public void setCantMinAcumulado(Double cantMinAcumulado) {
        this.cantMinAcumulado = cantMinAcumulado;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<CodigoCertificado> getCodigos() {
        return codigos;
    }

    public void setCodigos(List<CodigoCertificado> codigos) {
        this.codigos = codigos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPremio != null ? idPremio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Premio)) {
            return false;
        }
        Premio other = (Premio) object;
        if ((this.idPremio == null && other.idPremio != null) || (this.idPremio != null && !this.idPremio.equals(other.idPremio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.Premio[ idPremio=" + idPremio + " ]";
    }

    public String getCodigoCertificado() {
        return codigoCertificado;
    }

    public void setCodigoCertificado(String codigoCertificado) {
        this.codigoCertificado = codigoCertificado;
    }

    public Date getFechaRedencion() {
        return fechaRedencion;
    }

    public void setFechaRedencion(Date fechaRedencion) {
        this.fechaRedencion = fechaRedencion;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Double getPrecioPromedio() {
        return precioPromedio;
    }

    public void setPrecioPromedio(Double precioPromedio) {
        this.precioPromedio = precioPromedio;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Long getCantTotal() {
        return cantTotal;
    }

    public void setCantTotal(Long cantTotal) {
        this.cantTotal = cantTotal;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Long getTotalExistencias() {
        return totalExistencias;
    }

    public void setTotalExistencias(Long totalExistencias) {
        this.totalExistencias = totalExistencias;
    }

    public Integer getCantDiasVencimiento() {
        return cantDiasVencimiento;
    }

    public void setCantDiasVencimiento(Integer cantDiasVencimiento) {
        this.cantDiasVencimiento = cantDiasVencimiento;
    }
    
    

}
