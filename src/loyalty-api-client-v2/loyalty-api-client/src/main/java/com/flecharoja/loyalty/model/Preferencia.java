/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREFERENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Preferencia.findAll", query = "SELECT p FROM Preferencia p"),
    @NamedQuery(name = "Preferencia.findByIdPreferencia", query = "SELECT p FROM Preferencia p WHERE p.idPreferencia = :idPreferencia"),
    @NamedQuery(name = "Preferencia.countAll", query = "SELECT COUNT(p) FROM Preferencia p")
})
public class Preferencia implements Serializable {
    
    public enum Respuesta{
        SELECCION_UNICA("SU"),
        RESPTA_MULTIPLE("RM"),
        TEXTO("TX");
        
        private final String value;
        private static final Map<String, Respuesta> lookup =  new HashMap<>();

        private Respuesta(String value) {
            this.value = value;
        }
        
        static{
            for(Respuesta respuesta: values()){
                lookup.put(respuesta.value, respuesta);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static Respuesta get (String value){
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_PREFERENCIA")
    @ApiModelProperty(value = "Identificador de la preferencia",required = true)
    private String idPreferencia;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "PREGUNTA")
    @ApiModelProperty(value = "Pregunta a la cual se responde la preferencia",required = true)
    private String pregunta;
    
    @Size(max = 300)
    @Column(name = "RESPUESTAS")
    @ApiModelProperty(value = "Respuesta del miembro a la preferencia",required = true)
    private String respuestas;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "IND_TIPO_RESPUESTA")
    @ApiModelProperty(value = "Indicador del tipo de respuesta que tendrá la preferencia",required = true)
    private String indTipoRespuesta;
    
    @Transient
    @ApiModelProperty(value = "Respuesta del miembro a la preferencia")
    private String respuestaMiembro;

    public Preferencia() {
    }

    public Preferencia(String idPreferencia) {
        this.idPreferencia = idPreferencia;
    }

    public String getIdPreferencia() {
        return idPreferencia;
    }

    public void setIdPreferencia(String idPreferencia) {
        this.idPreferencia = idPreferencia;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(String respuestas) {
        this.respuestas = respuestas;
    }

    public String getIndTipoRespuesta() {
        return indTipoRespuesta;
    }

    public void setIndTipoRespuesta(String indTipoRespuesta) {
        this.indTipoRespuesta = indTipoRespuesta;
    }

    public String getRespuestaMiembro() {
        return respuestaMiembro;
    }

    public void setRespuestaMiembro(String respuestaMiembro) {
        this.respuestaMiembro = respuestaMiembro;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPreferencia != null ? idPreferencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Preferencia)) {
            return false;
        }
        Preferencia other = (Preferencia) object;
        if ((this.idPreferencia == null && other.idPreferencia != null) || (this.idPreferencia != null && !this.idPreferencia.equals(other.idPreferencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.Preferencia[ idPreferencia=" + idPreferencia + " ]";
    }
    
}
