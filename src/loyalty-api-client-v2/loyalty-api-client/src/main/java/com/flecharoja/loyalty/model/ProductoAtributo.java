/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PRODUCTO_ATRIBUTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoAtributo.findAll", query = "SELECT p FROM ProductoAtributo p")

})
public class ProductoAtributo implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoAtributoPK productoAtributoPK;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "VALOR")
    @ApiModelProperty(value = "Valor que tiene el atributo para el producto especifico",required = true)
    private String valor;
      
    @JoinColumn(name = "ID_ATRIBUTO", referencedColumnName = "ID_ATRIBUTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    @ApiModelProperty(value = "Información del atributo dinámico que tiene un producto",required = true)
    private AtributoDinamicoProducto atributoDinamicoProducto;
    
    @Column(name = "ID_PRODUCTO", insertable = false, updatable = false)
    @ApiModelProperty(value = "Información del producto que tiene el atributo dinámico",required = true)
    private String producto;

    public ProductoAtributo() {
    }

    public ProductoAtributo(String idProducto, String idAtributo) {
        this.productoAtributoPK = new ProductoAtributoPK(idProducto, idAtributo);
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public ProductoAtributoPK getProductoAtributoPK() {
        return productoAtributoPK;
    }

    public void setProductoAtributoPK(ProductoAtributoPK productoAtributoPK) {
        this.productoAtributoPK = productoAtributoPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public AtributoDinamicoProducto getAtributoDinamicoProducto() {
        return atributoDinamicoProducto;
    }

    public void setAtributoDinamicoProducto(AtributoDinamicoProducto atributoDinamicoProducto) {
        this.atributoDinamicoProducto = atributoDinamicoProducto;
    }

    public String getProducto() {
        return producto;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public void setProducto(String producto) {
        this.producto = producto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoAtributoPK != null ? productoAtributoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoAtributo)) {
            return false;
        }
        ProductoAtributo other = (ProductoAtributo) object;
        if ((this.productoAtributoPK == null && other.productoAtributoPK != null) || (this.productoAtributoPK != null && !this.productoAtributoPK.equals(other.productoAtributoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.ProductoAtributo[ productoAtributoPK=" + productoAtributoPK + " ]";
    }
    
}
