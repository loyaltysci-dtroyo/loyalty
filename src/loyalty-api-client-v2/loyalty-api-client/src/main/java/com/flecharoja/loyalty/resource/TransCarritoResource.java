package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.TransCarrito;
import com.flecharoja.loyalty.service.TransCarritoBean;
import com.flecharoja.loyalty.util.Indicadores;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "TransCarrito")
@Path("transCarrito")
public class TransCarritoResource {

    @EJB
    TransCarritoBean transCarritoBean;

    @Context
    SecurityContext context;

    @Context
    HttpServletRequest request;

    /**
     * Método que obtiene un listado de productos
     *
     * @param idCategoriaProducto
     * @return lista de productos
     */
    @ApiOperation(value = "Obtener los productos en las que el miembro en sesion es elegible",
            responseContainer = "List",
            response = TransCarrito.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TransCarrito> getTransCarrito(@ApiParam(value = "id de la categoría del carrito") @QueryParam("idCategoriaProducto") String idCategoriaProducto) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();

        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return transCarritoBean.getTransCarrito(idMiembro, idCategoriaProducto, request.getLocale());
    }
    
    
    /**
     * Método para pagar un carrito
     *
     * @param transCarrito 
     * @return Estado
     */
    @ApiOperation(value = "Pagar un carrito",
            responseContainer = "Boolean",
            response = Boolean.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Path("pagar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Boolean pagarTransCarrito(@ApiParam(value = "transCarrito con valores pagos") TransCarrito transCarrito) {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return transCarritoBean.pagarTransCarrito(transCarrito, request.getLocale());
    }
    

    /**
     * Método que obtiene los últimos 10 carritos.
     *
     * @return lista carritos
     */
    @ApiOperation(value = "Obtener los últimos 10 carritos",
            responseContainer = "List",
            response = TransCarrito.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("recientes")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TransCarrito> getTransCarritos() {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return transCarritoBean.getTransCarritos(idMiembro, request.getLocale());
    }
}
