/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "TRANSACCION_COMPRA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TransaccionCompra.findAll", query = "SELECT t FROM TransaccionCompra t"),
    @NamedQuery(name = "TransaccionCompra.findByIdTransaccion", query = "SELECT t FROM TransaccionCompra t WHERE t.idTransaccion = :idTransaccion"),
    @NamedQuery(name = "TransaccionCompra.findTransaccionInCompra", query = "SELECT MAX(t.fecha) FROM TransaccionCompra t WHERE t.idTransaccion IN :lista AND t.idMiembro.idMiembro = :idMiembro ORDER BY t.fecha DESC"),
    @NamedQuery(name = "TransaccionCompra.findTransaccionMiembro", query = "SELECT t.idTransaccion FROM TransaccionCompra t WHERE t.idMiembro.idMiembro = :idMiembro"),
    @NamedQuery(name = "TransaccionCompra.findTransaccionTiempoProducto", query = "SELECT t.idTransaccion FROM TransaccionCompra t WHERE t.fecha >= :fechaInicio AND t.fecha <= :fechaFin"),
    @NamedQuery(name = "TransaccionCompra.findTransaccionTiempoProductoMiembro", query = "SELECT t.idTransaccion FROM TransaccionCompra t WHERE t.idMiembro.idMiembro = :idMiembro AND t.fecha >= :fechaInicio AND t.fecha <= :fechaFin")
})
public class TransaccionCompra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "transaccion_uuid")
    @GenericGenerator(name = "transaccion_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_TRANSACCION")
    private String idTransaccion;
    
    @Size(max = 150)
    @Column(name = "ID_COMPRA")
    private String idCompra;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUBTOTAL")
    private Double subtotal;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IMPUESTO")
    private Double impuesto;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL")
    private Double total;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO")
    @ManyToOne(optional = false)
    private Miembro idMiembro;
    
    @JoinColumn(name = "ID_UBICACION", referencedColumnName = "ID_UBICACION")
    @ManyToOne
    private Ubicacion idUbicacion;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "transaccionCompra")
    private CompraPremio compraPremio;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "transaccionCompra")
    private List<CompraProducto> compraProductoList;

    public TransaccionCompra() {
    }

    public TransaccionCompra(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public TransaccionCompra(String idTransaccion, Date fecha, Double subtotal, Double impuesto, Double total) {
        this.idTransaccion = idTransaccion;
        this.fecha = fecha;
        this.subtotal = subtotal;
        this.impuesto = impuesto;
        this.total = total;
    }

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public String getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(String idCompra) {
        this.idCompra = idCompra;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(Double impuesto) {
        this.impuesto = impuesto;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Miembro getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(Miembro idMiembro) {
        this.idMiembro = idMiembro;
    }

    public Ubicacion getIdUbicacion() {
        return idUbicacion;
    }

    public void setIdUbicacion(Ubicacion idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    public CompraPremio getCompraPremio() {
        return compraPremio;
    }

    public void setCompraPremio(CompraPremio compraPremio) {
        this.compraPremio = compraPremio;
    }
    
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<CompraProducto> getCompraProductoList() {
        return compraProductoList;
    }

    public void setCompraProductoList(List<CompraProducto> compraProductoList) {
        this.compraProductoList = compraProductoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTransaccion != null ? idTransaccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransaccionCompra)) {
            return false;
        }
        TransaccionCompra other = (TransaccionCompra) object;
        if ((this.idTransaccion == null && other.idTransaccion != null) || (this.idTransaccion != null && !this.idTransaccion.equals(other.idTransaccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.TransaccionCompra[ idTransaccion=" + idTransaccion + " ]";
    }
    
}
