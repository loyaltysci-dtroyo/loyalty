package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@XmlRootElement
public class RespuestaMisionJuego {

    @ApiModelProperty(value = "Identificador del premio ganador", required = true)
    private String idMisionJuego;

    public RespuestaMisionJuego() {
    }

    public String getIdMisionJuego() {
        return idMisionJuego;
    }

    public void setIdMisionJuego(String idMisionJuego) {
        this.idMisionJuego = idMisionJuego;
    }

}
