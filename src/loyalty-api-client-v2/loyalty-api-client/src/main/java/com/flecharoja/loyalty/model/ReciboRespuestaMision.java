package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class ReciboRespuestaMision {
    public enum TiposPremios {
        METRICA('M'),
        AGRADECIMIENTO('G'),
        PREMIO('P');
        
        private final char value;

        private TiposPremios(char value) {
            this.value = value;
        }

        public char getValue() {
            return value;
        }
    }
    
    @ApiModelProperty(value = "Indicador de estado del recibido de respuesta",required = true)
    private String indEstado;
    
    @ApiModelProperty(value = "Mensaje del recibo de la respuesta de la misión",required = true)
    private String mensaje;
    
    @ApiModelProperty(value = "Indicador del tipo de premio ganado", example = "Métrica o premio",required = true)
    private Character indTipoPremio;
    
    @ApiModelProperty(value = "Referencia al identificador del premio, si la respuesta es tipo premio")
    private String referenciaPremio;

    public ReciboRespuestaMision() {
    }

    public String getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(String indEstado) {
        this.indEstado = indEstado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Character getIndTipoPremio() {
        return indTipoPremio;
    }

    public void setIndTipoPremio(Character indTipoPremio) {
        this.indTipoPremio = indTipoPremio;
    }

    public String getReferenciaPremio() {
        return referenciaPremio;
    }

    public void setReferenciaPremio(String referenciaPremio) {
        this.referenciaPremio = referenciaPremio;
    }
}
