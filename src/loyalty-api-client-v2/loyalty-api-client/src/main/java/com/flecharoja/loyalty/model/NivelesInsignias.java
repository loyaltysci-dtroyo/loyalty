/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "NIVELES_INSIGNIAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NivelesInsignias.findAll", query = "SELECT n FROM NivelesInsignias n"),
    @NamedQuery(name = "NivelesInsignias.findByIdNivel", query = "SELECT n FROM NivelesInsignias n WHERE n.idNivel = :idNivel")
})
public class NivelesInsignias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_NIVEL")
    @ApiModelProperty(value = "Identificador del nivel de insignia",required = true)
    private String idNivel;
    
    @Column(name = "NOMBRE_NIVEL")
    @ApiModelProperty(value = "Nombre del nivel de insignia",required = true)
    private String nombreNivel;
    
    @Column(name = "DESCRIPCION")
    @ApiModelProperty(value = "Descripción del nivel de insignia",required = true)
    private String descripcion;
    
    @Column(name = "IMAGEN")
    @ApiModelProperty(value = "Imagen representativa del nivel de insignia",required = true)
    private String imagen;
    
    @Transient
    @ApiModelProperty(value = "Indicador que dice si el nivel ha sido obtenido o no",required = true)
    private Boolean obtenido;

    @Column(name = "ID_INSIGNIA")
    @ApiModelProperty(value = "Identificador de la insignia a la que pertenece el nivel",required = true)
    private String idInsignia;

    public NivelesInsignias() {
    }

    public NivelesInsignias(String idNivel) {
        this.idNivel = idNivel;
    }

    public NivelesInsignias(String idNivel, String nombreNivel, String descripcion, String imagen) {
        this.idNivel = idNivel;
        this.nombreNivel = nombreNivel;
        this.descripcion = descripcion;
        this.imagen = imagen;
    }
    

    
    public String getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(String idNivel) {
        this.idNivel = idNivel;
    }

    public String getNombreNivel() {
        return nombreNivel;
    }

    public void setNombreNivel(String nombreNivel) {
        this.nombreNivel = nombreNivel;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    
    public String getIdInsignia() {
        return idInsignia;
    }

    public Boolean getObtenido() {
        return obtenido;
    }

    public void setObtenido(Boolean obtenido) {
        this.obtenido = obtenido;
    }
    

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public void setIdInsignia(String idInsignia) {
        this.idInsignia = idInsignia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivel != null ? idNivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NivelesInsignias)) {
            return false;
        }
        NivelesInsignias other = (NivelesInsignias) object;
        if ((this.idNivel == null && other.idNivel != null) || (this.idNivel != null && !this.idNivel.equals(other.idNivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.NivelesInsignias[ idNivel=" + idNivel + " ]";
    }
    
}
