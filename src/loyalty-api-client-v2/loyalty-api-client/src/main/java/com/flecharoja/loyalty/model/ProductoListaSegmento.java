/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PRODUCTO_LISTA_SEGMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoListaSegmento.countByIdProductoAndSegmentosInListaAndByIndTipo", query = "SELECT COUNT(s) FROM ProductoListaSegmento s WHERE s.productoListaSegmentoPK.idProducto = :idProducto AND s.productoListaSegmentoPK.idSegmento IN :lista AND s.indTipo = :indTipo"),
    @NamedQuery(name = "ProductoListaSegmento.countByIdProductoAndByIndTipo", query = "SELECT COUNT(s) FROM ProductoListaSegmento s WHERE s.productoListaSegmentoPK.idProducto = :idProducto AND s.indTipo = :indTipo")

})


public class ProductoListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoListaSegmentoPK productoListaSegmentoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    public ProductoListaSegmento() {
    }

    public ProductoListaSegmentoPK getProductoListaSegmentoPK() {
        return productoListaSegmentoPK;
    }

    public void setProductoListaSegmentoPK(ProductoListaSegmentoPK productoListaSegmentoPK) {
        this.productoListaSegmentoPK = productoListaSegmentoPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoListaSegmentoPK != null ? productoListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoListaSegmento)) {
            return false;
        }
        ProductoListaSegmento other = (ProductoListaSegmento) object;
        if ((this.productoListaSegmentoPK == null && other.productoListaSegmentoPK != null) || (this.productoListaSegmentoPK != null && !this.productoListaSegmentoPK.equals(other.productoListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ProductoListaSegmento[ productoListaSegmentoPK=" + productoListaSegmentoPK + " ]";
    }
    
}
