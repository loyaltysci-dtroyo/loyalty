package com.flecharoja.loyalty.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@ApiModel(value = "Respuesta")
@XmlRootElement
public class RespuestaRegistroEntidad {
    
    @ApiModelProperty(value = "Identificador de la entidad creada", readOnly = true)
    private String idEntidad;

    public RespuestaRegistroEntidad(String idEntidad) {
        this.idEntidad = idEntidad;
    }

    public String getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(String idEntidad) {
        this.idEntidad = idEntidad;
    }
    
}
