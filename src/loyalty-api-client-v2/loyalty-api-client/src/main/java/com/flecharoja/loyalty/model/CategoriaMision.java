package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "CATEGORIA_MISION")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "CategoriaMision.countAll", query = "SELECT COUNT(c) FROM CategoriaMision c"),
        @NamedQuery(name = "CategoriaMision.findAll", query = "SELECT c FROM CategoriaMision c")
})
public class CategoriaMision implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_CATEGORIA")
    @ApiModelProperty(value = "Identificador único y autogenerado de la categoría de misión",required = true)
    private String idCategoria;
    
    @Column(name = "NOMBRE")
    @ApiModelProperty(value = "Nombre de la categoría de misión",required = true)
    private String nombre;
    
    @Column(name = "IMAGEN")
    @ApiModelProperty(value = "Imagen representativa de la categoría de misión",required = true)
    private String imagen;
    
    @Column(name = "DESCRIPCION")
    @ApiModelProperty(value = "Descripción básica de la categoría de misión",required = true)
    private String descripcion;
    
    public CategoriaMision() {
    }
    
    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCategoria != null ? idCategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaMision)) {
            return false;
        }
        CategoriaMision other = (CategoriaMision) object;
        if ((this.idCategoria == null && other.idCategoria != null) || (this.idCategoria != null && !this.idCategoria.equals(other.idCategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CategoriaMision[ idCategoria=" + idCategoria + " ]";
    }
    
}
