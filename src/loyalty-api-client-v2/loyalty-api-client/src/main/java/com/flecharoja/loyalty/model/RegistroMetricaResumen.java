/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@XmlRootElement
public class RegistroMetricaResumen {
  
    @ApiModelProperty(value = "Fecha en que se registro la métrica ",required = true)
    private Date fecha;
    @ApiModelProperty(value = "Cantidad de métrica ganada",required = true)
    private double cantidadGanada;
    @ApiModelProperty(value = "Indicador de la forma en que fue ganada la métrica", example = "Misión o transacción",required = true)
    private String indFormaGanada;
    @ApiModelProperty(value = "Nombre de la misión si la métrica fue ganada por medio de misión")
    private String nombreMision;
    @ApiModelProperty(value = "Información de la transacción si la métrica fue ganada por medio de una transacción")
    private Transaccion transaccion;
    
    

    public RegistroMetricaResumen() {
    }

    public RegistroMetricaResumen(Date fecha, int cantidadGanada, String indFormaGanada, String nombreMision, Double totalComprado, String ubicacion) {
        this.fecha = fecha;
        this.cantidadGanada = cantidadGanada;
        this.indFormaGanada = indFormaGanada;
        this.nombreMision = nombreMision;
        this.transaccion = new Transaccion(totalComprado, ubicacion);
    }
    
     public RegistroMetricaResumen(Double totalComprado, String ubicacion) {
        this.transaccion = new Transaccion(totalComprado, ubicacion);
    }
    

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getCantidadGanada() {
        return cantidadGanada;
    }

    public void setCantidadGanada(double cantidadGanada) {
        this.cantidadGanada = cantidadGanada;
    }

    public String getIndFormaGanada() {
        return indFormaGanada;
    }

    public void setIndFormaGanada(String indFormaGanada) {
        this.indFormaGanada = indFormaGanada;
    }

    public String getNombreMision() {
        return nombreMision;
    }

    public void setNombreMision(String nombreMision) {
        this.nombreMision = nombreMision;
    }

    public Transaccion getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(Transaccion transaccion) {
        this.transaccion = transaccion;
    }
    
    @XmlRootElement
    public class Transaccion{
        @ApiModelProperty(value = "Total de compra que se realizó", required = true)
        private Double totalComprado;
        @ApiModelProperty(value = "Identificador del lugar donde se realizó la compra", required = true)
        private String lugarCompra;

        public Transaccion(Double totalComprado, String lugarCompra) {
            this.totalComprado = totalComprado;
            this.lugarCompra = lugarCompra;
        }

        public Double getTotalComprado() {
            return totalComprado;
        }

        public void setTotalComprado(Double totalComprado) {
            this.totalComprado = totalComprado;
        }

        public String getLugarCompra() {
            return lugarCompra;
        }

        public void setLugarCompra(String lugarCompra) {
            this.lugarCompra = lugarCompra;
        }
        
        
    }
    
    
    
}
