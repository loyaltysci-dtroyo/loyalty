package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "WORKFLOW_LISTA_SEGMENTO")
@NamedQueries({
    @NamedQuery(name = "WorkflowListaSegmento.countByIdWorkflowAndSegmentosInListaAndByIndTipo", query = "SELECT COUNT(s) FROM WorkflowListaSegmento s WHERE s.workflowListaSegmentoPK.idWorkflow = :idWorkflow AND s.workflowListaSegmentoPK.idSegmento IN :lista AND s.indTipo = :indTipo"),
    @NamedQuery(name = "WorkflowListaSegmento.countByIdWorkflowAndByIndTipo", query = "SELECT COUNT(s) FROM WorkflowListaSegmento s WHERE s.workflowListaSegmentoPK.idWorkflow = :idWorkflow AND s.indTipo = :indTipo")
})
public class WorkflowListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected WorkflowListaSegmentoPK workflowListaSegmentoPK;
    
    @Column(name = "IND_TIPO")
    private Character indTipo;

    public WorkflowListaSegmento() {
    }

    public WorkflowListaSegmentoPK getWorkflowListaSegmentoPK() {
        return workflowListaSegmentoPK;
    }

    public void setWorkflowListaSegmentoPK(WorkflowListaSegmentoPK workflowListaSegmentoPK) {
        this.workflowListaSegmentoPK = workflowListaSegmentoPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (workflowListaSegmentoPK != null ? workflowListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkflowListaSegmento)) {
            return false;
        }
        WorkflowListaSegmento other = (WorkflowListaSegmento) object;
        if ((this.workflowListaSegmentoPK == null && other.workflowListaSegmentoPK != null) || (this.workflowListaSegmentoPK != null && !this.workflowListaSegmentoPK.equals(other.workflowListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.WorkflowListaSegmento[ workflowListaSegmentoPK=" + workflowListaSegmentoPK + " ]";
    }
    
}
