package com.flecharoja.loyalty.filter;

import com.flecharoja.loyalty.model.MiembroDataExtra;
import com.flecharoja.loyalty.service.MiembroBean;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author svargas
 */
@Provider
public class GlobalRequestFilter implements ContainerRequestFilter {
    
    @EJB
    MiembroBean miembroBean;

    @Override
    public void filter(ContainerRequestContext request) {
        String idMiembro;
        try {
            idMiembro = request.getSecurityContext().getUserPrincipal().getName();
        } catch (NullPointerException e) {
            return;
        }
        
        String userAgent = request.getHeaderString("User-Agent");
        if (userAgent!=null) {
            if (Pattern.compile("\\b(ANDROID)").matcher(userAgent.toUpperCase()).find()) {
                miembroBean.updateDatosExtraMiembro(idMiembro, MiembroDataExtra.TiposDispositivos.ANDROID);
            } else {
                if (Pattern.compile("\\b(IOS|IPAD|IPHONE)").matcher(userAgent.toUpperCase()).find()) {
                    miembroBean.updateDatosExtraMiembro(idMiembro, MiembroDataExtra.TiposDispositivos.IOS);
                } else {
                    if (Pattern.compile("\\b(LINUX|WINDOWS|MACINTOSH)").matcher(userAgent.toUpperCase()).find()) {
                        miembroBean.updateDatosExtraMiembro(idMiembro, MiembroDataExtra.TiposDispositivos.WEB);
                    } else {
                        miembroBean.updateDatosExtraMiembro(idMiembro, MiembroDataExtra.TiposDispositivos.DESCONOCIDO);
                    }
                }
            }
        } else {
            miembroBean.updateDatosExtraMiembro(idMiembro, MiembroDataExtra.TiposDispositivos.DESCONOCIDO);
        }
    }
    
}
