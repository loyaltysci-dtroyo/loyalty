package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.TransProducto;
import com.flecharoja.loyalty.model.TransProductoPK;
import com.flecharoja.loyalty.service.TransProductoBean;
import com.flecharoja.loyalty.util.Indicadores;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "TransProducto")
@Path("transProducto")
public class TransProductoResource {

    @EJB
    TransProductoBean transProductoBean;

    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;
   
    /**
     * Método que obtiene un listado de productos
     *
     * @param numTransaction
     * @return lista de productos
     */
    @ApiOperation(value = "Obtener los productos en un carrito",
            responseContainer = "List",
            response = TransProducto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TransProducto> getProductos(@ApiParam(value = "id del carrito") @QueryParam("numTransaction") String numTransaction){
        try {
            context.getUserPrincipal().getName();
              
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return transProductoBean.getProductos(numTransaction, request.getLocale());
    }
    
    /**
     * Método que registra un producto al carrito
     *
     * @param idProducto
     * @param idCategoriaProducto
     * @param cantidad
     * @return lista de productos
     */
    @ApiOperation(value = "Registrar un producto a un carrito",
            response = TransProductoPK.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("{idProducto}/categoria/{idCategoriaProducto}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TransProductoPK insertTransProducto(
            @ApiParam(value = "id del producto que se insertará", required = true)
            @PathParam("idProducto") String idProducto,
            @ApiParam(value = "id de la categoria del producto", required = true)
            @PathParam("idCategoriaProducto") String idCategoriaProducto,
            @ApiParam(value = "Cantidad que se insertará de dicho producto", required = true) String cantidad
    ){
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
              
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return transProductoBean.insertTransProducto(idProducto, idMiembro, idCategoriaProducto, Integer.parseInt(cantidad), request.getLocale());
    }
    
    /**
     * Método que registra un producto al carrito
     *
     * @param numTransaccion
     * @param idProducto
     * @param cantidad
     * @return lista de productos
     */
    @ApiOperation(value = "Elimina un producto de un carrito",
            response = TransProducto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @DELETE
    @Path("{numTransaccion}/producto/{idProducto}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TransProducto deleteTransProducto(
            @ApiParam(value = "id del carrito", required = true)
            @PathParam("numTransaccion") String numTransaccion,
            @ApiParam(value = "id del producto que se eliminará", required = true)
            @PathParam("idProducto") String idProducto,
            @ApiParam(value = "Cantidad que se insertará de dicho producto", required = true) String cantidad
    ){
        try {
            context.getUserPrincipal().getName();
              
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return transProductoBean.deleteTransProducto(numTransaccion, idProducto, Integer.parseInt(cantidad), request.getLocale());
    }
}
