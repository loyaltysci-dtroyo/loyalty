/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CERTIFICADO_CATEGORIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CertificadoCategoria.findAll", query = "SELECT c FROM CertificadoCategoria c"),
    @NamedQuery(name = "CertificadoCategoria.CertificadoCategoria.findByCategoria", query = "SELECT c FROM CertificadoCategoria c WHERE c.categoriaProducto.idCategoria = :categoriaProducto"),
    @NamedQuery(name = "CertificadoCategoria.findAvailable", query = "SELECT c FROM CertificadoCategoria c WHERE c.categoriaProducto.idCategoria = :categoriaProducto AND c.estado = :estado"),
    @NamedQuery(name = "CertificadoCategoria.findByIdCertificadoCategoria", query = "SELECT c FROM CertificadoCategoria c WHERE c.idCertificado= :idCertificado")
})
public class CertificadoCategoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_CERTIFICADO")
    @ApiModelProperty(value = "Identificador único del carrito", required = true)
    private String idCertificado;
    
    @JoinColumn(name = "ID_CATEGORIA", referencedColumnName = "ID_CATEGORIA", insertable = false, updatable = true)
    @ManyToOne(optional = false)
    private CategoriaProducto categoriaProducto;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NUM_CERTIFICADO")
    @ApiModelProperty(value = "Número de certificado asignado por el restaurante", required = true)
    private String numCertificado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTADO")
    @ApiModelProperty(value = "Estado del certificado (A asignado D disponible)", required = true)
    private Character estado;
    
    //@Transient json si, DB no
    //@XmlTransient BD Si, json no
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Fecha de creación del certificado", required = true)
    private Date fechaCreacion;
    
    @Column(name = "FECHA_EXPIRACION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Fecha de expiración del certificado", required = false)
    private Date fechaExpiracion;

    public CertificadoCategoria() {
    }

    public CertificadoCategoria(String idCertificado) {
        this.idCertificado = idCertificado;
    }

    public CategoriaProducto getCategoriaProducto() {
        return categoriaProducto;
    }

    public Character getEstado() {
        return estado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public String getIdCertificado() {
        return idCertificado;
    }

    public String getNumCertificado() {
        return numCertificado;
    }

    public void setCategoriaProducto(CategoriaProducto categoriaProducto) {
        this.categoriaProducto = categoriaProducto;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public void setIdCertificado(String idCertificado) {
        this.idCertificado = idCertificado;
    }

    public void setNumCertificado(String numCertificado) {
        this.numCertificado = numCertificado;
    }   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCertificado != null ? idCertificado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CertificadoCategoria)) {
            return false;
        }
        CertificadoCategoria other = (CertificadoCategoria) object;
        if ((this.idCertificado == null && other.idCertificado != null) || (this.idCertificado != null && !this.idCertificado.equals(other.idCertificado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.CertificadoCategoria[ idCertificado=" + idCertificado + " ]";
    }

//    
    
    
}
