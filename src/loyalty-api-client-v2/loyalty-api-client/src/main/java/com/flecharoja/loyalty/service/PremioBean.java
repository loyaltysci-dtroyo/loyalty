/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.BalanceMetricaMensual;
import com.flecharoja.loyalty.model.BalanceMetricaMensualPK;
import com.flecharoja.loyalty.model.CategoriaPremio;
import com.flecharoja.loyalty.model.CodigoCertificado;
import com.flecharoja.loyalty.model.CompraPremio;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.ExistenciasUbicacion;
import com.flecharoja.loyalty.model.ExistenciasUbicacionPK;
import com.flecharoja.loyalty.model.HistoricoMovimientos;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.MiembroBalanceMetrica;
import com.flecharoja.loyalty.model.MiembroBalanceMetricaPK;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioControl;
import com.flecharoja.loyalty.model.PremioListaMiembro;
import com.flecharoja.loyalty.model.PremioListaMiembroPK;
import com.flecharoja.loyalty.model.PremioMiembro;
import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.model.TransaccionCompra;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.google.common.collect.Lists;
import com.google.common.primitives.Chars;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TemporalType;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author wtencio
 */
@Stateless
public class PremioBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;

    @EJB
    RegistroMetricaService registroMetricaService;
    
    @EJB
    SegmentoBean segmentoBean;

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Método que obtiene una lista de categorias de premios existentes
     *
     * @param locale
     * @return lista de categorias de premios
     */
    public List<CategoriaPremio> getCategoriasPremio(Locale locale) {
        return em.createNamedQuery("CategoriaPremio.findAll").getResultList();
    }

    /**
     * Método que devuelve una lista de premios la cual pertenece a la categoria
     * que se pasa por parametro y tambien que cumpla con que el miembro se
     * encuentre entre sus elegibles
     *
     * @param idCategoria identificador de la categoria de premio
     * @param idMiembro identificador del miembro
     * @param locale
     * @return lista de premios resultantes
     */
    public List<Premio> getPremiosPorCategoria(String idCategoria, String idMiembro, Locale locale) {
        List<Premio> resultado = new ArrayList<>();
        //obtencion de todas los premio dentro de la categoria de premio
        List<PremioControl> premios = em.createNamedQuery("PremioCategoriaPremio.findPremiosByIdCategoria").setParameter("idCategoria", idCategoria).getResultList();

        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);
        
        //filtrado de premios por su estado, elegibilidad por miembro
        //mapeo de control de premios a su par que contiene informacion basica
        resultado = premios.stream()
                .filter((PremioControl p) -> p.getIndEstado().equals(PremioControl.Estados.PUBLICADO.getValue()) && checkElegibilidadPremioMiembro(idMiembro, p.getIdPremio(), segmentosPertenecientes) && checkIntervalosRespuestaPremioMiembro(p, idMiembro) && checkCalendarizacionPremio(p))
                .map((PremioControl p) -> em.find(Premio.class, p.getIdPremio()))
                .collect(Collectors.toList());
        return resultado;
    }

    /**
     * Método que devuelve una lista de premios disponibles para el miembro
     *
     * @param idMiembro identificador del miembro
     * @param locale
     * @return lista de premios resultantes
     */
    public List<Premio> getPremios(String idMiembro, Locale locale) {

        //obtencion de todos los premios 
        List<PremioControl> premios = em.createNamedQuery("PremioControl.findAll").getResultList();
        List<Premio> resultado = new ArrayList<>();
        
        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);

        //filtrado y conversion de cada premio
        resultado = premios.stream()
                .filter((PremioControl c) -> c.getIndEstado().equals(PremioControl.Estados.PUBLICADO.getValue()) && checkElegibilidadPremioMiembro(idMiembro, c.getIdPremio(), segmentosPertenecientes) && checkIntervalosRespuestaPremioMiembro(c, idMiembro) && checkCalendarizacionPremio(c)
                && existsExitenciaUbicacion(c.getIdPremio(), locale)
                )
                .map((PremioControl c) -> {
                    Premio premio = em.find(Premio.class, c.getIdPremio());
                    premio.setIndEstado(Premio.EstadosMiembro.DISPONIBLE.getValue());
                    return premio;
                }).collect(Collectors.toList());

        return resultado;
    }

    /**
     * Método que devuelve una lista de premios redimidos para el miembro
     *
     * @param idMiembro identificador del miembro
     * @param locale
     * @return lista de premios resultantes
     */
    public List<Premio> getPremiosRedimidos(String idMiembro, Locale locale) {
        List<Premio> premios = new ArrayList<>();
        List<PremioMiembro> premioMiembros;

        try {
            premioMiembros = em.createNamedQuery("PremioMiembro.findByIdMiembro")
                    .setParameter("idMiembro", idMiembro)
                    .setParameter("estado", PremioMiembro.Estados.REDIMIDO_SIN_RETIRAR.getValue())
                    .getResultList();

            premios = premioMiembros.stream().map((p) -> {
                Premio premio = em.find(Premio.class, p.getIdPremio().getIdPremio());
                premio.setCodigoCertificado(p.getCodigoCertificado());
                premio.setFechaRedencion(p.getFechaAsignacion());
                premio.setIndEstado(Premio.EstadosMiembro.REDIMIDO.getValue());
                premio.setFechaExpiracion(p.getFechaExpiracion());
                return premio;
            }).collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "argument_invalid", "idMiembro");
        }

        return premios;
    }
    
    /**
     * Método que devuelve una lista de premios expirados para el miembro
     *
     * @param idMiembro identificador del miembro
     * @param locale
     * @return lista de premios resultantes
     */
    public List<Premio> getPremiosExpirados(String idMiembro, Locale locale) {
        List<Premio> premios = new ArrayList<>();
        List<PremioMiembro> premioMiembros = new ArrayList<>();

        try {
            premioMiembros = em.createNamedQuery("PremioMiembro.findByIdMiembro")
                    .setParameter("idMiembro", idMiembro)
                    .setParameter("estado", PremioMiembro.Estados.EXPIRADO.getValue())
                    .getResultList();

            premios = premioMiembros.stream().map((p) -> {
                Premio premio = em.find(Premio.class, p.getIdPremio().getIdPremio());
                premio.setCodigoCertificado(p.getCodigoCertificado());
                premio.setFechaRedencion(p.getFechaAsignacion());
                premio.setIndEstado(Premio.EstadosMiembro.EXPIRADO.getValue());
                premio.setFechaExpiracion(p.getFechaExpiracion());
                return premio;
            }).collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "argument_invalid", "idMiembro");
        }

        return premios;
    }

    /**
     * Método que permite a un miembro redimir un premio
     *
     * @param idPremio identificador del premio
     * @param idMiembro identificador del miembro
     * @param locale
     * @return
     */
    public String redimirPremio(String idPremio, String idMiembro, Locale locale) {
        List<RegistroMetrica> registrosActualizar = new ArrayList<>();
        CodigoCertificado codigoCertificado = null;
        //verificar el minimo que tiene de metrica el premio lo tenga el miembro
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }
        Miembro miembro = em.find(Miembro.class, idMiembro);
        if (miembro == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);
        
        //verificacion de la elegibilidad de redimir el premio
        //TODO verificar existencias y afectar existencias
        PremioControl premioControl = em.find(PremioControl.class, premio.getIdPremio());
        if (!(premioControl.getIndEstado().equals(PremioControl.Estados.PUBLICADO.getValue()) && checkElegibilidadPremioMiembro(idMiembro, premioControl.getIdPremio(), segmentosPertenecientes) && checkIntervalosRespuestaPremioMiembro(premioControl, idMiembro) && checkCalendarizacionPremio(premioControl) && existsExitenciaUbicacion(idPremio, locale))) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "reward_no_available");
        }

        //se obtiene la billetera del miembro
        MiembroBalanceMetrica balance = em.find(MiembroBalanceMetrica.class, new MiembroBalanceMetricaPK(idMiembro, premio.getIdMetrica().getIdMetrica()));
        if (balance == null) {
            balance = new MiembroBalanceMetrica(idMiembro, premio.getIdMetrica().getIdMetrica());
        }

        //verificacion del minimo de metrica y el valor del premio
        if (balance.getTotalAcumulado().compareTo(premio.getCantMinAcumulado()) >= 0 && balance.getDisponible().compareTo(premio.getValorMoneda()) >= 0) {
            //obtener la lista de registros de metrica de hbase
            List<RegistroMetrica> registrosMetrica = registroMetricaService.getRegistrosMetricasMiembroDisponibles(idMiembro, balance.getMetrica().getIdMetrica());

            //registro en premio lista
            PremioMiembro premioMiembro = new PremioMiembro();
            premioMiembro.setEstado(PremioMiembro.Estados.REDIMIDO_SIN_RETIRAR.getValue());
            premioMiembro.setFechaAsignacion(Calendar.getInstance().getTime());
            premioMiembro.setIdMiembro(miembro);
            premioMiembro.setIndMotivoAsignacion(PremioMiembro.Motivos.PREMIO.getValue());
            premioMiembro.setIdPremio(premio);
            premioMiembro.setFechaExpiracion(fechaVencimiento(new Date(), premio.getCantDiasVencimiento()));
            if (premio.getIndTipoPremio().equals(Indicadores.PREMIO_IND_TIPO_CERTIFICADO)) {
                List<CodigoCertificado> certificados = em.createNamedQuery("CodigoCertificado.findByIdPremio")
                        .setParameter("idPremio", premio.getIdPremio())
                        .setParameter("indEstado", CodigoCertificado.Estados.DISPONIBLE.getValue())
                        .getResultList();
                if (!certificados.isEmpty()) {
                    codigoCertificado = certificados.get(0);
                    premioMiembro.setCodigoCertificado(codigoCertificado.getCodigoCertificadoPK().getCodigo());
                    codigoCertificado.setIndEstado(CodigoCertificado.Estados.OCUPADO.getValue());
                } else {
                    throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "certificate_not_available");
                }
                em.merge(codigoCertificado);
            }
            //persisto
            em.persist(premioMiembro);
            em.flush();
            //si no ocurrio ningun error se actualiza la billetera del miembro
            balance.setDisponible(balance.getDisponible() - premio.getValorMoneda());
            balance.setRedimido(balance.getRedimido() - premio.getValorMoneda());
            //se actualiza existencias
            //actualizo
            em.merge(balance);
            this.afectarExistenciasHistorico(idPremio, idMiembro, premioMiembro.getIdPremioMiembro(), locale);

            //creo la lista de registros de metrica que tengo que actualizar
            double totalARedimir = premio.getValorMoneda();
            for (RegistroMetrica registro : registrosMetrica) {
                if (totalARedimir - registro.getDisponible() >= 0) {
                    //resto al total de redimir
                    totalARedimir -= registro.getDisponible();
                    registro.setDisponible(0);
                    registrosActualizar.add(registro);
                } else {
                    registro.setDisponible(registro.getDisponible() - totalARedimir);
                    totalARedimir -= totalARedimir;
                    registrosActualizar.add(registro);
                    break;
                }
            }

            //si el premio tiene un valor en efectivo se registra la transaccion
            if (premio.getValorEfectivo() != null && premio.getValorEfectivo().longValue() > 0) {
                //registro la transaccion
                TransaccionCompra transaccion = new TransaccionCompra();
                transaccion.setFecha(Calendar.getInstance().getTime());
                transaccion.setIdMiembro(miembro);
                transaccion.setSubtotal(premio.getValorEfectivo());
                transaccion.setTotal(premio.getValorEfectivo());
                transaccion.setImpuesto(0D);

                em.persist(transaccion);

                //se registra el detalle de la transaccion
                CompraPremio compraPremio = new CompraPremio();
                compraPremio.setIdTransaccion(transaccion.getIdTransaccion());
                compraPremio.setIdPremio(premio);
                compraPremio.setValorEfectivo(premio.getValorEfectivo());
                if (premio.getIndTipoPremio().equals(Indicadores.PREMIO_IND_TIPO_CERTIFICADO)) {
                    if (codigoCertificado != null) {
                        compraPremio.setCodigoCertificado(codigoCertificado.getCodigoCertificadoPK().getCodigo());
                    } else {
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "certificate_not_available");
                    }

                }

                em.persist(compraPremio);
            }
            
            RegistroMetrica registroMetrica = new RegistroMetrica(new Date(), balance.getMetrica().getIdMetrica(), idMiembro, RegistroMetrica.TiposGane.REDENCION, premio.getValorMoneda(), null);
            do {            
                registroMetrica.getRegistroMetricaPK().setIdInstancia(UUID.randomUUID().toString());
            } while (em.find(RegistroMetrica.class, registroMetrica.getRegistroMetricaPK())!=null);
            em.persist(registroMetrica);
            
            LocalDate localDate = LocalDate.now();
            BalanceMetricaMensual balanceMetricaMensual = em.find(BalanceMetricaMensual.class, new BalanceMetricaMensualPK(localDate.getYear(), localDate.getMonthValue(), registroMetrica.getIdMetrica()));
            if (balanceMetricaMensual == null) {
                balanceMetricaMensual = new BalanceMetricaMensual(localDate.getYear(), localDate.getMonthValue(), registroMetrica.getIdMetrica());
            }
            balanceMetricaMensual.setRedimido(balanceMetricaMensual.getRedimido()+registroMetrica.getCantidad());
            em.merge(balanceMetricaMensual);

            try {
                em.flush();
            } catch (PersistenceException e) {
                throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "redeem_failed");
            }

            //se actualiza los registros de hbase
            registroMetricaService.updateRegistrosMetricasMiembro(registrosActualizar);
            
            try {
                myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.REMIDIO_PREMIO, idPremio, idMiembro);
                myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.REDIME_METRICA, premio.getIdMetrica().getIdMetrica(), idMiembro);
            } catch (Exception e) {
                Logger.getLogger(MisionBean.class.getName()).log(Level.WARNING, null, e);
            }

        } else {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "insufficient_metrics");
        }

        //retorno
        if (codigoCertificado != null) {
            return codigoCertificado.getCodigoCertificadoPK().getCodigo();
        } else {
            return null;
        }

    }

    /**
     * filtrado por inclusion/exclusion
     */
    private boolean checkElegibilidadPremioMiembro(String idMiembro, String idPremio, List<String> segmentosPertenecientes) {
        //se consulta si exsite un registro de inclusion/exclusion del miembro para el premio
        PremioListaMiembro miembroIncluidoExcluido = em.find(PremioListaMiembro.class, new PremioListaMiembroPK(idMiembro, idPremio));
        if (miembroIncluidoExcluido != null) {
            //si existe... se filtra por su tipo... (inclusion o exclusion)
            switch (miembroIncluidoExcluido.getIndTipo()) {
                case Indicadores.INCLUIDO: {
                    return true;
                }
                case Indicadores.EXCLUIDO: {
                    return false;
                }
            }
        }

        if (!segmentosPertenecientes.isEmpty()) {
            List<List<String>> partitionsSegmentos;
            partitionsSegmentos = Lists.partition(segmentosPertenecientes, 999);
            //se verifica si existe algun segmento excluido en el premio que este dentro de los segmentos pertenecientes del miembro
            if (partitionsSegmentos.stream()
                    .anyMatch((l) -> ((long) em.createNamedQuery("PremioListaSegmento.countByIdPremioAndSegmentosInListaAndByIndTipo")
                    .setParameter("idPremio", idPremio)
                    .setParameter("lista", l)
                    .setParameter("indTipo", Indicadores.EXCLUIDO)
                    .getSingleResult() > 0))) {
                return false;
            }
            //se comprueba si no existe ningun segmento incluido para el premio (se asume que es para todos)
            if (partitionsSegmentos.stream()
                        .anyMatch((l) -> ((long) em.createNamedQuery("PremioListaSegmento.countByIdPremioAndSegmentosInListaAndByIndTipo")
                        .setParameter("idPremio", idPremio)
                        .setParameter("lista", l)
                        .setParameter("indTipo", Indicadores.INCLUIDO)
                        .getSingleResult() > 0))) {
                return true;
            }
        }
        
        return (long) em.createNamedQuery("PremioListaSegmento.countByIdPremioAndByIndTipo")
                    .setParameter("idPremio", idPremio)
                    .setParameter("indTipo", Indicadores.INCLUIDO)
                    .getSingleResult() == 0;
    }

    /**
     * filtrado de premio por tiempo transcurrido entre intervalos de respuestas
     */
    public boolean checkIntervalosRespuestaPremioMiembro(PremioControl premio, String idMiembro) {
        if (premio.getIndRespuesta() != null && premio.getIndRespuesta()) {
            PremioControl.IntervalosTiempoRespuesta intervalo;
            boolean flag = true;//bandera indicando si se aceptan nuevas rediciones (por defecto true)

            //verificacion de intervalos de respuesta totales
            intervalo = PremioControl.IntervalosTiempoRespuesta.get(premio.getIndIntervaloTotal());
            if (intervalo != null) {
                flag = checkIntervaloRespuestaPremio(null, premio.getIdPremio(), intervalo, premio.getCantTotal());
            }

            //verificacion de intervalos de respuesta totales por miembro..
            intervalo = PremioControl.IntervalosTiempoRespuesta.get(premio.getIndIntervaloMiembro());
            if (intervalo != null && flag) {
                flag = checkIntervaloRespuestaPremio(idMiembro, premio.getIdPremio(), intervalo, premio.getCantTotalMiembro());
            }

            //verificacion de tiempo entre respuestas del miembro...
            if (intervalo != null && flag) {
                Date fechaUltima = (Date) em.createNamedQuery("PremioMiembro.findByUltimaFechaAsignacion")
                        .setParameter("idMiembro", idMiembro)
                        .setParameter("idPremio", premio.getIdPremio())
                        .getSingleResult();
                if (fechaUltima != null) {
                    switch (intervalo) {
                        case POR_DIA: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.DAY_OF_YEAR, -premio.getCantIntervaloRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                        case POR_HORA: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.HOUR_OF_DAY, -premio.getCantIntervaloRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                        case POR_MINUTO: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.MINUTE, -premio.getCantIntervaloRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                    }
                }
            }
            return flag;
        }
        return true;
    }

    /**
     * true -> se aceptan nuevas respuestas false -> no se aceptan nuevas
     * respuestas
     */
    private boolean checkIntervaloRespuestaPremio(String idMiembro, String idPremio, PremioControl.IntervalosTiempoRespuesta intervalo, Long cantTotal) {
        /*
        Se obtienen la cantidad de respuestas de registros de respuestas de premio en rangos de tiempos de fecha
         */
        long cantidadRespuestas = 0;
        switch (intervalo) {
            case POR_SIEMPRE: {
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasPremio").setParameter("idPremio", idPremio).getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasPremioMiembro").setParameter("idPremio", idPremio).setParameter("idMiembro", idMiembro).getSingleResult();
                }
                break;
            }
            case POR_MES: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_SEMANA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.WEEK_OF_YEAR, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_DIA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_HORA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.HOUR_OF_DAY, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();

                }

                break;
            }
            case POR_MINUTO: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MINUTE, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
        }
        //mientras que la cantidad de respuestas siga siendo menor a la cantidad de respuestas totales aceptables..
        return cantidadRespuestas < cantTotal;
    }

    /**
     * Método que verifica que el premio este entre las fechas correctas
     */
    private boolean checkCalendarizacionPremio(PremioControl premio) {
        //se establece el indicador de respuesta como verdadero(todo es permitido hasta que sea negado)
        boolean flag = true;
        //se obtiene la fecha actual
        Calendar hoy = Calendar.getInstance();

        //si el premio es calendarizado
        if (premio.getEfectividadIndCalendarizado().equals(PremioControl.Efectividad.CALENDARIZADO.getValue())) {
            //si la fecha de inicio esta establecida, se comprueba que la fecha sea pasada a hoy o que sea el mismo dia
            if (premio.getEfectividadFechaInicio() != null) {
                flag = hoy.getTime().after(premio.getEfectividadFechaInicio()) || DateUtils.isSameDay(hoy.getTime(), premio.getEfectividadFechaInicio());
            }
            //mientras siga siendo verdadero...
            //si la fecha de fin esta establecida, se comprueba que la fecha sea posterior a hoy o que sea el mismo dia
            if (flag && premio.getEfectividadFechaFin() != null) {
                flag = hoy.getTime().before(premio.getEfectividadFechaFin()) || DateUtils.isSameDay(hoy.getTime(), premio.getEfectividadFechaFin());
            }

            //mientras siga siendo verdadero...
            if (flag && premio.getEfectividadIndDias() != null) {
                //se obtiene el numero de dia de hoy
                int hoyDia = hoy.get(Calendar.DAY_OF_WEEK);
                //se recorre los indicadores de dias de recurrencia en busca de que se cumpla al menos una condicion
                flag = Chars.asList(premio.getEfectividadIndDias().toCharArray()).stream().anyMatch((t) -> {
                    //segun el indicador de dia de recurrencia, se verifica si el numero de dia de hoy concuerda con el numero de dia asociado al indicador
                    switch (t) {
                        case 'L': {
                            return Calendar.MONDAY == hoyDia;
                        }
                        case 'K': {
                            return Calendar.TUESDAY == hoyDia;
                        }
                        case 'M': {
                            return Calendar.WEDNESDAY == hoyDia;
                        }
                        case 'J': {
                            return Calendar.THURSDAY == hoyDia;
                        }
                        case 'V': {
                            return Calendar.FRIDAY == hoyDia;
                        }
                        case 'S': {
                            return Calendar.SATURDAY == hoyDia;
                        }
                        case 'D': {
                            return Calendar.SUNDAY == hoyDia;
                        }
                        default: {
                            return false;
                        }
                    }
                });
            }
            if (flag && premio.getEfectividadIndSemana() != null) {
                long weeksPast = ChronoUnit.WEEKS.between(LocalDateTime.ofInstant(Instant.ofEpochMilli(premio.getEfectividadFechaInicio() == null ? premio.getFechaPublicacion().getTime() : premio.getEfectividadFechaInicio().getTime()), ZoneId.systemDefault()), LocalDateTime.now());
                if (weeksPast % premio.getEfectividadIndSemana().longValue() != 0) {
                    flag = false;
                }
            }
        }
        return flag;
    }

    /**
     * Método que dice si existen existencias del producto en la ubicacion
     * central
     *
     * @param idPremio identificador del premio
     * @param locale
     * @return
     */
    public boolean existsExitenciaUbicacion(String idPremio, Locale locale) {
        List<ConfiguracionGeneral> configuracion = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
        ConfiguracionGeneral config = configuracion.get(0);

        if (config.getIntegracionInventarioPremios()==null || config.getIntegracionInventarioPremios() == false) {
            return true;
        } else {
            if (config.getUbicacionPrincipal() == null) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "location_undefined");
            }
            String idUbicacion = config.getUbicacionPrincipal().getIdUbicacion();
            Premio premio = em.find(Premio.class, idPremio);
            if (premio.getIndTipoPremio().equals(Premio.Tipo.PRODUCTO.getValue())) {
                ExistenciasUbicacion existencias = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(idUbicacion, idPremio, config.getMes(), config.getPeriodo()));
                if (existencias == null) {
                    return false;
                }
                return existencias.getSaldoActual() >= 1;
            } else {
                long cantCertificados = (long) em.createNamedQuery("CodigoCertificado.countByIdPremio").setParameter("idPremio", idPremio).setParameter("indEstado", CodigoCertificado.Estados.DISPONIBLE.getValue()).getSingleResult();
                return cantCertificados >= 1;
            }
        }
    }

    public void afectarExistenciasHistorico(String idPremio, String idMiembro, String idPremioMiembro, Locale locale) {
        List<ConfiguracionGeneral> configuracion = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
        ConfiguracionGeneral config = configuracion.get(0);
        if (config.getIntegracionInventarioPremios() == true) {
            if (config.getUbicacionPrincipal() == null) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "location_undefined");
            }
            String idUbicacion = config.getUbicacionPrincipal().getIdUbicacion();
            HistoricoMovimientos historico;
            Premio premio = em.find(Premio.class, idPremio);
            if (premio == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
            }

            if (premio.getIndTipoPremio().equals(Premio.Tipo.PRODUCTO.getValue())) {
                ExistenciasUbicacion existencias = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(idUbicacion, idPremio, config.getMes(), config.getPeriodo()));
                if (existencias == null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "no_record_reward");
                }

                if (existencias.getSaldoActual() >= 1) {
                    existencias.setSalidas(existencias.getSalidas() + 1);
                    existencias.setSaldoActual((existencias.getSaldoInicial() + existencias.getEntradas()) - existencias.getSalidas());
                }
                try {
                    em.merge(existencias);
                    em.flush();
                } catch (ConstraintViolationException e) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "argument_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
                }
                historico = new HistoricoMovimientos(premio.getPrecioPromedio(), em.find(Miembro.class, idMiembro), premio, existencias.getUbicacion(), null, 1L, HistoricoMovimientos.Tipos.REDENCION_AUTO.getValue(), new Date(), HistoricoMovimientos.Status.REDIMIDO_SIN_RETIRAR.getValue(), idPremioMiembro);
                long totalPremios = (long) em.createNamedQuery("ExistenciasUbicacion.findByPremioByPeriodoByMes").setParameter("idPremio", idPremio).setParameter("mes", config.getMes()).setParameter("periodo", config.getPeriodo()).getSingleResult();
                premio.setTotalExistencias(totalPremios);
            } else {
                historico = new HistoricoMovimientos(null, em.find(Miembro.class, idMiembro), premio, config.getUbicacionPrincipal(), null, 1L, HistoricoMovimientos.Tipos.REDENCION_AUTO.getValue(), new Date(), HistoricoMovimientos.Status.REDIMIDO_SIN_RETIRAR.getValue(), idPremioMiembro);
                long totalCertificados = (long) em.createNamedQuery("CodigoCertificado.countByIdPremio").setParameter("idPremio", idPremio).setParameter("indEstado", CodigoCertificado.Estados.DISPONIBLE.getValue()).getSingleResult();
                premio.setTotalExistencias(totalCertificados);
            }

            try {
                em.persist(historico);
                em.merge(premio);
            } catch (ConstraintViolationException e) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "argument_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }
        }
    }

    public String getCodigoAleatorio() {
        String numero = "";
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            int num = random.nextInt(99);
            if (num < 10) {
                numero += "0" + num + "-";
            } else {
                numero += num + "-";
            }
            if (i < 4) {
                numero += "-";
            }
        }
        if (existsNumeroGenerado(numero)) {
            getCodigoAleatorio();
        }
        return numero;
    }

    public boolean existsNumeroGenerado(String numero) {
        return ((Long) em.createNamedQuery("PremioMiembro.countCodigoGenerado").setParameter("codigo", numero).getSingleResult()) > 0;
    }
    
    /**
     * Metodo para obtener la fecha de vencimiento
     * @param fecha
     * @param dias
     * @return 
     */
    public Date fechaVencimiento(Date fecha, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 58);
        return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos

    }

}
