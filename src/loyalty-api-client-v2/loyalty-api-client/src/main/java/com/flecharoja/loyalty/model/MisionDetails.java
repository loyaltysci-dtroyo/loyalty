package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION")
@XmlRootElement
public class MisionDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_MISION")
    @ApiModelProperty(value = "Identificador único y autogenerado de una misión", required = true)
    private String idMision;

    @Column(name = "IMAGEN")
    @ApiModelProperty(value = "Imagen representativa de la misión",required = true)
    private String imagenArte;
    
    @Column(name = "ENCABEZADO_ARTE")
    @ApiModelProperty(value = "Encabezado de la misión, representa el nombre de la misión ",required = true)
    private String encabezadoArte;
    
    @Column(name = "SUBENCABEZADO_ARTE")
    @ApiModelProperty(value = "Subencabezado de la misión, muestra un poco más de información de la misión")
    private String subencabezadoArte;
    
    @Column(name = "TEXTO_ARTE")
    @ApiModelProperty(value = "Descripción sencilla y representativa en que dice de que trata la misión",required = true)
    private String detalleArte;

    @Column(name = "CANT_METRICA")
    @ApiModelProperty(value = "Cantidad de métrica que se ganara el miembro por completar la misión",required = true)
    private Double cantMetrica;

    @Column(name = "IND_TIPO_MISION")
    @ApiModelProperty(value = "Tipo de misión", example = "Encuesta, Red Social...",required = true)
    private Character indTipoMision;

    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    @ApiModelProperty(value = "Información de la métrica a ganar por completar la misión",required = true)
    private Metrica metrica;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_MISION")
    @ApiModelProperty(value = "Información extra de la misión si es de tipo ver contenifo")
    private MisionVerContenido misionVerContenido;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_MISION")
    @ApiModelProperty(value = "Lista de atributos a actualizar, si la misión es de tipo actualizar perfil")
    private List<MisionPerfilAtributo> misionPerfilAtributos;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_MISION")
    @ApiModelProperty(value = "Lista de preguntas de una encuesta, si la misión es de tipo encuesta")
    private List<MisionEncuestaPregunta> misionEncuestaPreguntas;
    
    @Transient
    @ApiModelProperty(value = "Lista de preferencias de una encuesta, si la misión es de tipo encuesta de preferencias")
    private List<Preferencia> misionEncuestaPreferencias;
    
    @Transient
    @ApiModelProperty(value = "Información extra si la misión es de tipo juego")
    private Juego juego;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_MISION")
    @ApiModelProperty(value = "Información extra si la misión es de tipo red social")
    private MisionRedSocial misionRedSocial;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_MISION")
    @ApiModelProperty(value = "Información extra de la misión, si es de tipo subir contenido")
    private MisionSubirContenido misionSubirContenido;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_MISION")
    @ApiModelProperty(value = "Información extra de la misión, si es de tipo subir contenido")
    private MisionRealidadAumentada misionRealidadAumentada;
    

    public MisionDetails() {
    }

    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    public Double getCantMetrica() {
        return cantMetrica;
    }

    public void setCantMetrica(Double cantMetrica) {
        this.cantMetrica = cantMetrica;
    }

    public Character getIndTipoMision() {
        return indTipoMision;
    }

    public void setIndTipoMision(Character indTipoMision) {
        this.indTipoMision = indTipoMision;
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }
    
    public MisionVerContenido getMisionVerContenido() {
        return misionVerContenido;
    }

    public void setMisionVerContenido(MisionVerContenido misionVerContenido) {
        this.misionVerContenido = misionVerContenido;
    }
    
    public List<MisionPerfilAtributo> getMisionPerfilAtributos() {
        return misionPerfilAtributos;
    }

    public void setMisionPerfilAtributos(List<MisionPerfilAtributo> misionPerfilAtributos) {
        this.misionPerfilAtributos = misionPerfilAtributos;
    }
    
    public List<MisionEncuestaPregunta> getMisionEncuestaPreguntas() {
        return misionEncuestaPreguntas;
    }

    public void setMisionEncuestaPreguntas(List<MisionEncuestaPregunta> misionEncuestaPreguntas) {
        this.misionEncuestaPreguntas = misionEncuestaPreguntas;
    }

    public List<Preferencia> getMisionEncuestaPreferencias() {
        return misionEncuestaPreferencias;
    }

    public void setMisionEncuestaPreferencias(List<Preferencia> misionEncuestaPreferencias) {
        this.misionEncuestaPreferencias = misionEncuestaPreferencias;
    }

    public Juego getJuego() {
        return juego;
    }

    public void setJuego(Juego juego) {
        this.juego = juego;
    }
    
    public MisionRedSocial getMisionRedSocial() {
        return misionRedSocial;
    }

    public void setMisionRedSocial(MisionRedSocial misionRedSocial) {
        this.misionRedSocial = misionRedSocial;
    }
    
    public MisionSubirContenido getMisionSubirContenido() {
        return misionSubirContenido;
    }

    public void setMisionSubirContenido(MisionSubirContenido misionSubirContenido) {
        this.misionSubirContenido = misionSubirContenido;
    }

    public String getImagenArte() {
        return imagenArte;
    }

    public void setImagenArte(String imagenArte) {
        this.imagenArte = imagenArte;
    }

    public String getEncabezadoArte() {
        return encabezadoArte;
    }

    public void setEncabezadoArte(String encabezadoArte) {
        this.encabezadoArte = encabezadoArte;
    }

    public String getSubencabezadoArte() {
        return subencabezadoArte;
    }

    public void setSubencabezadoArte(String subencabezadoArte) {
        this.subencabezadoArte = subencabezadoArte;
    }

    public String getDetalleArte() {
        return detalleArte;
    }

    public void setDetalleArte(String detalleArte) {
        this.detalleArte = detalleArte;
    }

    public MisionRealidadAumentada getMisionRealidadAumentada() {
        return misionRealidadAumentada;
    }

    public void setMisionRealidadAumentada(MisionRealidadAumentada misionRealidadAumentada) {
        this.misionRealidadAumentada = misionRealidadAumentada;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMision != null ? idMision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionDetails)) {
            return false;
        }
        MisionDetails other = (MisionDetails) object;
        if ((this.idMision == null && other.idMision != null) || (this.idMision != null && !this.idMision.equals(other.idMision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Mision[ idMision=" + idMision + " ]";
    }

}
