package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.NewNoticia;
import com.flecharoja.loyalty.model.NewNoticiaComentario;
import com.flecharoja.loyalty.model.Noticia;
import com.flecharoja.loyalty.model.NoticiaComentario;
import com.flecharoja.loyalty.service.NoticiaBean;
import com.flecharoja.loyalty.util.RespuestaRegistroEntidad;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 * Recursos para el manejo de noticias/comentarios
 *
 * @author svargas
 */
@Api(value = "Noticias")
@Path("noticia")
public class NoticiaResource {
    
    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;
    
    @EJB
    NoticiaBean bean;
    
    @ApiOperation(value = "Obtener lista de noticias disponibles",
            responseContainer = "List",
            response = Noticia.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Noticia> getNoticias(
            @ApiParam(value = "Identificador de categoria de noticia", required = false) @QueryParam("idCategoria") String idCategoria) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return bean.getNoticias(idMiembro, idCategoria, request.getLocale());
    }

    @ApiOperation(value = "Registrar una nueva noticia",
            response = RespuestaRegistroEntidad.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaRegistroEntidad insertNoticia(
            @ApiParam(value = "Informacion de registro de noticia", required = true) NewNoticia noticia) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return bean.insertNoticia(noticia, idMiembro, request.getLocale());
    }
    
    @ApiOperation(value = "Editar una noticia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Path("{idNoticia}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void editNoticia(
            @ApiParam(value = "Identificador de la noticia", required = true) @PathParam("idNoticia") String idNoticia,
            @ApiParam(value = "Informacion de registro de noticia", required = true) NewNoticia noticia) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        bean.editNoticia(noticia, idNoticia, idMiembro, request.getLocale());
    }
    
    @ApiOperation(value = "Eliminar una noticia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @DELETE
    @Path("{idNoticia}")
    @Produces(MediaType.APPLICATION_JSON)
    public void removeNoticia(
            @ApiParam(value = "Identificador de la noticia", required = true) @PathParam("idNoticia") String idNoticia) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        bean.removeNoticia(idNoticia, idMiembro, request.getLocale());
    }
    
    @ApiOperation(value = "Marcar una noticia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Path("{idNoticia}/marcar-favorito")
    @Produces(MediaType.APPLICATION_JSON)
    public void marcarNoticia(
            @ApiParam(value = "Identificador de la noticia", required = true) @PathParam("idNoticia") String idNoticia) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        bean.marcarNoticia(idNoticia, idMiembro, request.getLocale());
    }
    
    @ApiOperation(value = "Obtener lista de comentarios por noticia",
            responseContainer = "List",
            response = NoticiaComentario.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("{idNoticia}/comentario")
    @Produces(MediaType.APPLICATION_JSON)
    public List<NoticiaComentario> getComentarios(
            @ApiParam(value = "Identificador de la noticia", required = true) @PathParam("idNoticia") String idNoticia,
            @ApiParam(value = "Fecha para la pagina siguiente") @QueryParam("since") long sinceDate,
            @ApiParam(value = "Fecha para la pagina anterior") @QueryParam("until") long untilDate) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return bean.getComentarios(idNoticia, null, idMiembro, request.getLocale(), sinceDate, untilDate);
    }
    
    @ApiOperation(value = "Obtener lista de comentarios por comentario en noticia",
            responseContainer = "List",
            response = NoticiaComentario.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("{idNoticia}/comentario/{idComentario}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<NoticiaComentario> getComentarios(
            @ApiParam(value = "Identificador de la noticia", required = true) @PathParam("idNoticia") String idNoticia,
            @ApiParam(value = "Identificador del comentario", required = true) @PathParam("idComentario") String idComentario,
            @ApiParam(value = "Fecha para la pagina siguiente") @QueryParam("since") long sinceDate,
            @ApiParam(value = "Fecha para la pagina anterior") @QueryParam("until") long untilDate) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return bean.getComentarios(idNoticia, idComentario, idMiembro, request.getLocale(), sinceDate, untilDate);
    }

    @ApiOperation(value = "Registrar un nuevo comentario",
            response = RespuestaRegistroEntidad.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("{idNoticia}/comentario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaRegistroEntidad insertComentario(
            @ApiParam(value = "Identificador de la noticia", required = true) @PathParam("idNoticia") String idNoticia,
            @ApiParam(value = "Informacion para el registro de comentario", required = true) NewNoticiaComentario comentario) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return bean.insertComentario(comentario, idNoticia, null, idMiembro, request.getLocale());
    }
    
    @ApiOperation(value = "Registrar un nuevo comentario sobre otro comentario en una noticia",
            response = RespuestaRegistroEntidad.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("{idNoticia}/comentario/{idComentario}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaRegistroEntidad insertComentario(
            @ApiParam(value = "Identificador de la noticia", required = true) @PathParam("idNoticia") String idNoticia,
            @ApiParam(value = "Identificador del comentario a referir", required = true) @PathParam("idComentario") String idComentarioReferente,
            @ApiParam(value = "Informacion para el registro de comentario", required = true) NewNoticiaComentario comentario) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return bean.insertComentario(comentario, idNoticia, idComentarioReferente, idMiembro, request.getLocale());
    }
    
    @ApiOperation(value = "Editar un comentario")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Path("{idNoticia}/comentario/{idComentario}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void editComentario(
            @ApiParam(value = "Identificador de la noticia", required = true) @PathParam("idNoticia") String idNoticia,
            @ApiParam(value = "Identificador del comentario", required = true) @PathParam("idComentario") String idComentario,
            @ApiParam(value = "Informacion para el registro de comentario", required = true) NewNoticiaComentario comentario) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        bean.editComentario(comentario, idNoticia, idComentario, idMiembro, request.getLocale());
    }
    
    @ApiOperation(value = "Eliminar un comentario")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @DELETE
    @Path("{idNoticia}/comentario/{idComentario}")
    @Produces(MediaType.APPLICATION_JSON)
    public void removeComentario(
            @ApiParam(value = "Identificador de la noticia", required = true) @PathParam("idNoticia") String idNoticia,
            @ApiParam(value = "Identificador del comentario", required = true) @PathParam("idComentario") String idComentario) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        bean.removeComentario(idNoticia, idComentario, idMiembro, request.getLocale());
    }
}
