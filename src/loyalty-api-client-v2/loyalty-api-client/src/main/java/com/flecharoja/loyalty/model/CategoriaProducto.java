/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CATEGORIA_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoriaProducto.findAll", query = "SELECT c FROM CategoriaProducto c"),
    @NamedQuery(name = "CategoriaProducto.countAll", query = "SELECT COUNT(c) FROM CategoriaProducto c"),
    @NamedQuery(name = "CategoriaProducto.findByIdCategoria", query = "SELECT c FROM CategoriaProducto c WHERE c.idCategoria = :idCategoria")
})
public class CategoriaProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_CATEGORIA")
    @ApiModelProperty(value = "Identificador único y autogenerado de la categoría de producto",required = true)
    private String idCategoria;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
     @ApiModelProperty(value = "Nombre de la categoría de producto",required = true)
    private String nombre;
    
    @Size(max = 300)
    @Column(name = "IMAGEN")
    @ApiModelProperty(value = "Imagen representativa de la categoría de producto")
    private String imagen;
    
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    @ApiModelProperty(value = "Descripción básica de la categoría de premio")
    private String descripcion;
    
    @Size(max = 300)
    @Column(name = "LOGIN_ID")
    @ApiModelProperty(value = "ID del comercio asociado a EVERTEC (U otro método de pago)")
    private String loginId;
       
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCategoria", fetch = FetchType.EAGER)
    @ApiModelProperty(value = "Lista de subcategorias pertenecientes a la categoria")
    private List<SubcategoriaProducto> subcategorias;

    public CategoriaProducto() {
    }

    public CategoriaProducto(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCategoria != null ? idCategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaProducto)) {
            return false;
        }
        CategoriaProducto other = (CategoriaProducto) object;
        return !((this.idCategoria == null && other.idCategoria != null) || (this.idCategoria != null && !this.idCategoria.equals(other.idCategoria)));
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.CategoriaProducto[ idCategoria=" + idCategoria + " ]";
    }

    public List<SubcategoriaProducto> getSubcategorias() {
        return subcategorias;
    }

    public void setSubcategoriaw(List<SubcategoriaProducto> subcategorias) {
        this.subcategorias = subcategorias;
    }
    
    
}
