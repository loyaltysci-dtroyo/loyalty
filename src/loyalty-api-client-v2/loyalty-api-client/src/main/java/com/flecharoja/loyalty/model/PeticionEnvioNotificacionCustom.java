package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author svargas
 */
public class PeticionEnvioNotificacionCustom {
    
    public enum Tipos {
        NOTIFICACION_PUSH("P"),
        EMAIL("E");
        
        private final String value;
        private static final Map<String, Tipos> lookup = new HashMap<>();

        private Tipos(String value) {
            this.value = value;
        }
        
        static {
            for (Tipos estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static Tipos get (String value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    @ApiModelProperty(value = "Remitente")
    private String sender;
    
    @ApiModelProperty(value = "Titulo de la notificacion")
    private String titulo;
    
    @ApiModelProperty(value = "Cuerpo o descripcion de la notificacion")
    private String cuerpo;
    
    @ApiModelProperty(value = "Miembros a los que va dirigida la notificacion")
    private List<String> miembros;
    
    @ApiModelProperty(value = "Parametros de la notificacion")
    private Map<String, String> parametros;
    
    @ApiModelProperty(value = "Tipo de mensaje de la notificacion")
    private String tipoMsj;

    public PeticionEnvioNotificacionCustom() {
        miembros = new ArrayList<>();
        parametros = new HashMap<>();
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public List<String> getMiembros() {
        return miembros;
    }

    public void setMiembros(List<String> miembros) {
        this.miembros = miembros;
    }

    public Map<String, String> getParametros() {
        return parametros;
    }

    public void setParametros(Map<String, String> parametros) {
        this.parametros = parametros;
    }

    public String getTipoMsj() {
        return tipoMsj;
    }

    public void setTipoMsj(String tipoMsj) {
        this.tipoMsj = tipoMsj;
    }
    
}
