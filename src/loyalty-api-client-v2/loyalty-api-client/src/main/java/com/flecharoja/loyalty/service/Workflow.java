package com.flecharoja.loyalty.service;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "WORKFLOW")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Workflow.findAll", query = "SELECT w FROM Workflow w")
    , @NamedQuery(name = "Workflow.findByIdWorkflow", query = "SELECT w FROM Workflow w WHERE w.idWorkflow = :idWorkflow")
         , @NamedQuery(name = "Workflow.findByIdNodo", query = "SELECT w FROM Workflow w WHERE w.idSucesor.idSucesor = :idNodo")
    , @NamedQuery(name = "Workflow.findByNombreWorkflow", query = "SELECT w FROM Workflow w WHERE w.nombreWorkflow = :nombreWorkflow")
    , @NamedQuery(name = "Workflow.findByNombreInternoWorkflow", query = "SELECT w FROM Workflow w WHERE w.nombreInternoWorkflow = :nombreInternoWorkflow")
    , @NamedQuery(name = "Workflow.findByIndDisparador", query = "SELECT w FROM Workflow w WHERE w.indDisparador = :indDisparador")
    , @NamedQuery(name = "Workflow.findByIndEstado", query = "SELECT w FROM Workflow w WHERE w.indEstado = :indEstado")
    , @NamedQuery(name = "Workflow.countByNombreInterno", query = "SELECT COUNT(w.idWorkflow) FROM Workflow w WHERE w.nombreInternoWorkflow = :nombreInterno")
    , @NamedQuery(name = "Workflow.findByNumVersion", query = "SELECT w FROM Workflow w WHERE w.numVersion = :numVersion")
    , @NamedQuery(name = "Workflow.findByReferenciaDisparador", query = "SELECT w FROM Workflow w WHERE w.referenciaDisparador = :referenciaDisparador")
    , @NamedQuery(name = "Workflow.findByUsuarioCreacion", query = "SELECT w FROM Workflow w WHERE w.usuarioCreacion = :usuarioCreacion")
    , @NamedQuery(name = "Workflow.findByUsuarioModificacion", query = "SELECT w FROM Workflow w WHERE w.usuarioModificacion = :usuarioModificacion")
    , @NamedQuery(name = "Workflow.findByFechaCreacion", query = "SELECT w FROM Workflow w WHERE w.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "Workflow.findByFechaModificacion", query = "SELECT w FROM Workflow w WHERE w.fechaModificacion = :fechaModificacion")
    , @NamedQuery(name = "Workflow.findAllByEspec", query = "SELECT w FROM Workflow w WHERE w.isWorkflowMisionEspec = true")})

public class Workflow implements Serializable {
    
    
      public enum Estados {
        ACTIVO('A'),
        ARCHIVADO('B'),
        BORRADOR('C');
        
        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public char getValue() {
            return value;
        }
        
        public static Estados get (char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }
      public enum Disparadores {
        MIE_REGISTRA_PROGRAMA(24),
        MIE_AGREGA_PROMO_BOOKMARK(20),
        MIE_APRUEBA_MISION(13),
        MIE_REDIMIO_PREMIO(18),
        MIE_COMPRA_PRODUCTO(19),
        MIE_CUMPLE_ANNOS(23),
        MIE_EVENTO_PERSONALIZADO(30);

        private final int value;
        private static final Map<Integer, Disparadores> lookup = new HashMap<>();

        private Disparadores(int value) {
            this.value = value;
        }

        static {
            for (Disparadores disparador : values()) {
                lookup.put(disparador.value, disparador);
            }
        }

        public int getValue() {
            return value;
        }

        public static Disparadores get(int value) {
            return value == Character.MIN_VALUE ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "workflow_uuid")
    @GenericGenerator(name = "workflow_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_WORKFLOW")
    private String idWorkflow;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE_WORKFLOW")
    private String nombreWorkflow;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE_INTERNO_WORKFLOW")
    private String nombreInternoWorkflow;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_DISPARADOR")
    private Character indDisparador;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    @Size(max = 40)
    @Column(name = "REFERENCIA_DISPARADOR")
    private String referenciaDisparador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @JoinColumn(name = "ID_SUCESOR", referencedColumnName = "ID_NODO")
    @ManyToOne
    private WorkflowNodo idSucesor;
    @Column(name = "IS_WORKFLOW_MISION_ESPEC")
    private boolean isWorkflowMisionEspec;


    public Workflow() {
    }

    public Workflow(String idWorkflow) {
        this.idWorkflow = idWorkflow;
    }

    public Workflow(String idWorkflow, String nombreWorkflow, String nombreInternoWorkflow, Character indDisparador, Character indEstado, Long numVersion, String usuarioCreacion, String usuarioModificacion, Date fechaCreacion, Date fechaModificacion) {
        this.idWorkflow = idWorkflow;
        this.nombreWorkflow = nombreWorkflow;
        this.nombreInternoWorkflow = nombreInternoWorkflow;
        this.indDisparador = indDisparador;
        this.indEstado = indEstado;
        this.numVersion = numVersion;
        this.usuarioCreacion = usuarioCreacion;
        this.usuarioModificacion = usuarioModificacion;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
    }

    public String getIdWorkflow() {
        return idWorkflow;
    }

    public void setIdWorkflow(String idWorkflow) {
        this.idWorkflow = idWorkflow;
    }

    public String getNombreWorkflow() {
        return nombreWorkflow;
    }

    public void setNombreWorkflow(String nombreWorkflow) {
        this.nombreWorkflow = nombreWorkflow;
    }

    public String getNombreInternoWorkflow() {
        return nombreInternoWorkflow;
    }

    public void setNombreInternoWorkflow(String nombreInternoWorkflow) {
        this.nombreInternoWorkflow = nombreInternoWorkflow;
    }

    public Character getIndDisparador() {
        return indDisparador;
    }

    public void setIndDisparador(Character indDisparador) {
        this.indDisparador = indDisparador;
    }

    public boolean isIsWorkflowMisionEspec() {
        return isWorkflowMisionEspec;
    }

    public void setIsWorkflowMisionEspec(boolean isWorkflowMisionEspec) {
        this.isWorkflowMisionEspec = isWorkflowMisionEspec;
    }
   
    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getReferenciaDisparador() {
        return referenciaDisparador;
    }

    public void setReferenciaDisparador(String referenciaDisparador) {
        this.referenciaDisparador = referenciaDisparador;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public WorkflowNodo getIdSucesor() {
        return idSucesor;
    }

    public void setIdSucesor(WorkflowNodo idSucesor) {
        this.idSucesor = idSucesor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idWorkflow != null ? idWorkflow.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Workflow)) {
            return false;
        }
        Workflow other = (Workflow) object;
        if ((this.idWorkflow == null && other.idWorkflow != null) || (this.idWorkflow != null && !this.idWorkflow.equals(other.idWorkflow))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Workflow[ idWorkflow=" + idWorkflow + " ]";
    }
    
}
