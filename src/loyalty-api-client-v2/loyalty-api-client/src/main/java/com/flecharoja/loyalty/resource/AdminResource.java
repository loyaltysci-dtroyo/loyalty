package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.IdentificacionMiembro;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.NuevaPeticionCambioPassM;
import com.flecharoja.loyalty.model.PeticionCambioPassM;
import com.flecharoja.loyalty.model.PeticionReferirM;
import com.flecharoja.loyalty.service.MiembroBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "Administracion")
@Path("admin")
public class AdminResource {

    @EJB
    MiembroBean miembroBean;//EJB con los metodo para el manejo del perfil del cliente

    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;
    
    /**
     * Método que acciona una tarea la cual realiza la peticion de cambiar 
     * la contraseña para ingresar al sistema
     * @param data al que llegara la nueva contraseña, tiene que ser el correo registrado
     */
    @ApiOperation(value = "Petición de envio de contraseña temporal a email")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("recuperar-contrasena")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void recuperarContrasena(@ApiParam(value = "Correo electronico donde se enviara la contraseña", required = true) NuevaPeticionCambioPassM data) {
        miembroBean.solicitarRecuperacionContrasena(data,request.getLocale());
    }
    
    @ApiOperation(value = "Verificacion de peticion de recuperacion de contraseña")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("recuperar-contrasena/verificar-peticion")
    @Produces(MediaType.APPLICATION_JSON)
    public IdentificacionMiembro verificarPeticionCambioContrasena(@ApiParam(value = "Identificador de miembro", required = true) @QueryParam("idMiembro") String idMiembro,
            @ApiParam(value = "Identificador de peticion", required = true) @QueryParam("idPeticion") String idPeticion) {
        return miembroBean.verificarPeticionCambioContrasena(idMiembro, idPeticion, request.getLocale());
    }
    
    @ApiOperation(value = "Cambio de la contraseña por peticion de recuperacion de contraseña")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("recuperar-contrasena/cambio-contrasena")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void cambioContrasena(@ApiParam(value = "Detalles de peticion de cambio de contraseña y cambio de contraseña", required = true) PeticionCambioPassM data) {
        miembroBean.completarCambioContrasena(data, request.getLocale());
    }

    
    /**
     * Método que acciona una tarea la cual realiza el registro de una nueva cuenta de
     * miembro en el sistema
     * @param miembro informacion del miembro
     */
    @ApiOperation(value = "Registro de un nuevo miembro al sistema")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("registro")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void registroCuenta(
            @ApiParam(value = "Información  del miembro a registrar, solo requerida", required = true) 
                    Miembro miembro) {
         
        miembroBean.registroCuenta(miembro,request.getLocale());
    }

    /**
     * Método que realiza la tarea de registrar la referencia de un miembro a otro
     * @param data informacion del miembro
     */
    @ApiOperation(value = "Registro de un nuevo miembro al sistema")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("referir")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void referirMiembro(@ApiParam(value = "Identificador del miembro referente", required = true)  PeticionReferirM data) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        
        miembroBean.setReferenciaMiembro(idMiembro, data, request.getLocale());
    }
}
