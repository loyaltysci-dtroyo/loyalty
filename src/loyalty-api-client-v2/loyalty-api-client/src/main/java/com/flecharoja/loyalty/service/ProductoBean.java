/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;


import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CategoriaProducto;
import com.flecharoja.loyalty.model.Producto;
import com.flecharoja.loyalty.model.ProductoControl;
import com.flecharoja.loyalty.model.ProductoListaMiembro;
import com.flecharoja.loyalty.model.ProductoListaMiembroPK;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import com.google.common.collect.Lists;
import com.google.common.primitives.Chars;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author wtencio
 */
@Stateless
public class ProductoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    SegmentoBean segmentoBean;

    /**
     * Método que obtiene una lista de categorias de productos existentes
     *
     * @return lista de categorias de productos
     */
    public List<CategoriaProducto> getCategoriasProductos() {
        List<CategoriaProducto> categorias = em.createNamedQuery("CategoriaProducto.findAll").getResultList();
        
        return categorias.stream()
                .filter((CategoriaProducto c) -> !c.getSubcategorias().isEmpty())
                .collect(Collectors.toList());
    }


    /**
     * Método que devuelve una lista de productos la cual pertenece a la
     * subcategoria que se pasa por parametro y tambien que cumpla con que el
     * miembro se encuentre entre sus elegibles
     *
     * @param idSubcategoria identificador de la subcategoria de producto
     * @param idMiembro identificador del miembro
     * @param locale
     * @return lista de productos resultantes
     */
    public List<Producto> getProductosPorSubcategoria(String idSubcategoria, String idMiembro, Locale locale) {
        //obtencion de todos los productos dentro de la subcategoria de producto
        List<ProductoControl> productos = em.createNamedQuery("ProductoCategSubcateg.findByIdSubcategoria").setParameter("idSubcategoria", idSubcategoria).getResultList();
        
        return  productos.stream()
                .filter((ProductoControl p) -> p.getIndEstado().equals(ProductoControl.Estados.PUBLICADO.getValue()) && checkElegibilidadMiembro(idMiembro, p.getIdProducto()) && checkIntervalosRespuestaProductoMiembro(p, idMiembro) && checkCalendarizacionProducto(p))
                .map((ProductoControl p) -> em.find(Producto.class, p.getIdProducto())).collect(Collectors.toList());
            
    }


    /**
     * Método que devuelve una lista de productos en las cuales el miembro es
     * elegible
     *
     * @param idMiembro identificador del miembro
     * @param locale
     * @return lista de productos resultantes
     */
    public List<Producto> getProductos(String idMiembro,Locale locale) {
        
        //obtencion de todas los productos 
        List<ProductoControl> productos = em.createNamedQuery("ProductoControl.findAll").getResultList();
        return productos.stream()
                .filter((ProductoControl p) -> p.getIndEstado().equals(ProductoControl.Estados.PUBLICADO.getValue()) && checkElegibilidadMiembro(idMiembro, p.getIdProducto()) && checkIntervalosRespuestaProductoMiembro(p, idMiembro) && checkCalendarizacionProducto(p))
                .map((ProductoControl p) -> em.find(Producto.class, p.getIdProducto())).collect(Collectors.toList());

    }
    
    /**
     * Metodo que obtiene la informacion de un producto por el identificador de
     * la misma
     *
     * @param idProducto Identificador del producto
     * @param locale
     * @return Respuesta con la informacion del producto encontrado
     */
    public Producto getProducto(String idProducto, Locale locale) {

        Producto producto = em.find(Producto.class, idProducto);
        //verificacion de que la entidad exista
        if (producto == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
        }

        return producto;
    }
    
    /**
     * Metodo que obtiene la informacion de un producto por el identificador de
     * la misma
     *
     * @param idProducto Identificador del producto
     * @param cantidad
     * @param locale
     * @return Respuesta con la informacion del producto encontrado
     */
    public Producto updateCantidadProducto(String idProducto, Integer cantidad, Locale locale) {
        Producto producto = em.find(Producto.class, idProducto);
        //verificacion de que la entidad exista
        if (producto == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
        }
        
        if (cantidad > 0) {
            if (producto.getSaldoDisponible() < cantidad) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "not_product_available");
            }
        } else {
            if ((producto.getSaldoDisponible() - cantidad) > producto.getSaldoInicial()) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "not_product_available");
            }
        }
        
        producto.setSaldoDisponible(producto.getSaldoDisponible() - cantidad);
        
        try {
            em.merge(producto);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//en caso de que alguna informacion no este valida o este "vieja"
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "error_merging");
        }

        return producto;
    }
    
    /**
     * Método que devuelve la lista de productos disponibles en las cuales el miembro es
     * elegible
     *
     * @param idMiembro identificador del miembro
     * @param locale
     * @return lista de productos resultantes
     */
    public List<Producto> getProductosDisponibles(String idMiembro,Locale locale) {
        
        //obtencion de todas los productos 
        List<ProductoControl> productos = em.createNamedQuery("Producto.findAvailable").getResultList();
        return productos.stream()
                .filter((ProductoControl p) -> p.getIndEstado().equals(ProductoControl.Estados.PUBLICADO.getValue()) && checkElegibilidadMiembro(idMiembro, p.getIdProducto()) && checkIntervalosRespuestaProductoMiembro(p, idMiembro) && checkCalendarizacionProducto(p))
                .map((ProductoControl p) -> em.find(Producto.class, p.getIdProducto())).collect(Collectors.toList());

    }

    /**
     * filtrado por inclusion/exclusion de miembros
     */
    private boolean checkElegibilidadMiembro(String idMiembro, String idProducto) {
        //se retraen los segmentos elegibles para el miembro y se particionan en listas de 1000(por limitante de numero de identificadores en clausula SQL "IN")
        List<List<String>> partitionsSegmentos;
        partitionsSegmentos = Lists.partition(segmentoBean.getSegmentosPertenecientesMiembro(idMiembro), 999);

        //se consulta si existe un registro de inclusion/exclusion del miembro para el producto
        ProductoListaMiembro miembroIncluidoExcluido = em.find(ProductoListaMiembro.class, new ProductoListaMiembroPK(idMiembro, idProducto));
        if (miembroIncluidoExcluido != null) {
            //si existe... se filtra por su tipo (inclusion o exclusion)
            switch (miembroIncluidoExcluido.getIndTipo()) {
                case Indicadores.INCLUIDO: {
                    return true;
                }
                case Indicadores.EXCLUIDO: {
                    return false;
                }
            }
        }

        //se verifica si existe algun segmento excluido en el producto que este dentro de los segmentos pertenecientes del miembro
        if (partitionsSegmentos.stream()
                .anyMatch((l) -> ((long) em.createNamedQuery("ProductoListaSegmento.countByIdProductoAndSegmentosInListaAndByIndTipo")
                        .setParameter("idProducto", idProducto)
                        .setParameter("lista", l)
                        .setParameter("indTipo", Indicadores.EXCLUIDO)
                        .getSingleResult() > 0))) {
            return false;
        }
        //se comprueba si no existe ningun segmento incluido para el producto (se asume que es dirigido hacia todos)
        if ((long) em.createNamedQuery("ProductoListaSegmento.countByIdProductoAndByIndTipo")
                .setParameter("idProducto", idProducto)
                .setParameter("indTipo", Indicadores.INCLUIDO)
                .getSingleResult() == 0) {
            return true;
        } else {
            //se verifica si existe algun segmento incluido en el producto que este dentro de los segmnetos pertenecientes del miembro
            return partitionsSegmentos.stream()
                    .anyMatch((l) -> ((long) em.createNamedQuery("ProductoListaSegmento.countByIdProductoAndSegmentosInListaAndByIndTipo")
                            .setParameter("idProducto", idProducto)
                            .setParameter("lista", l)
                            .setParameter("indTipo", Indicadores.INCLUIDO)
                            .getSingleResult() > 0));
        }
    }

    /*
    * filtrado de producto por tiempo transcurrido entre intervalos de respuestas
     */
    private boolean checkIntervalosRespuestaProductoMiembro(ProductoControl productoControl, String idMiembro) {

        if (productoControl.getLimiteIndRespuesta() != null && productoControl.getLimiteIndRespuesta()) {
            ProductoControl.IntervalosTiempoRespuesta intervalo;
            boolean flag = true;//bandera indicando si se aceptan nuevas rediciones (por defecto true) 

            //verificacion de intervalos de respuesta totales
            intervalo = ProductoControl.IntervalosTiempoRespuesta.get(productoControl.getLimiteIndIntervaloTotal());
            if (intervalo != null) {
                flag = checkIntervaloRespuestaProducto(null, productoControl.getIdProducto(), intervalo, productoControl.getLimiteCantTotal());
            }

            //verificacion de intervalos de respuesta totales por miembro...
            intervalo = ProductoControl.IntervalosTiempoRespuesta.get(productoControl.getLimiteIndIntervaloMiembro());
            if (intervalo != null && flag) {
                flag = checkIntervaloRespuestaProducto(idMiembro, productoControl.getIdProducto(), intervalo, productoControl.getLimiteCantTotalMiembro());
            }

            //verificacion de tiempo entre respuestas del miembro
            if (intervalo != null && flag) {
                List lista = em.createNamedQuery("CompraProducto.findByIdProducto").setParameter("idProducto", productoControl.getIdProducto()).getResultList();
                Date fechaUltima = null;
                if (!lista.isEmpty()) {
                    fechaUltima = (Date) em.createNamedQuery("TransaccionCompra.findTransaccionInCompra")
                            .setParameter("lista", lista)
                            .setParameter("idMiembro", idMiembro)
                            .getSingleResult();
                }
                if (fechaUltima != null) {
                    switch (intervalo) {
                        case POR_DIA: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.DAY_OF_YEAR, -productoControl.getLimiteCantInterRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                        case POR_HORA: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.HOUR_OF_DAY, -productoControl.getLimiteCantInterRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                        case POR_MINUTO: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.MINUTE, -productoControl.getLimiteCantInterRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                    }
                }
            }
            return flag;
        }
        return true;
    }

    /*
    *true-> se aceptan nuevas respuestas
    *false-> no se aceptan nuevas respuestas
     */
    private boolean checkIntervaloRespuestaProducto(String idMiembro, String idProducto, ProductoControl.IntervalosTiempoRespuesta intervalo, Long limiteCantTotal) {
        /*
        Se obtienen la cantidad de respuestas de registros de respuestas de premio en rangos de tiempos de fecha
         */
        long cantidadRespuestas = 0;
        List lista = new ArrayList();
        switch (intervalo) {
            case POR_SIEMPRE: {
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("CompraProducto.countRespuestasProducto").setParameter("idProducto", idProducto).getSingleResult();
                } else {
                    lista = em.createNamedQuery("TransaccionCompra.findTransaccionMiembro").setParameter("idMiembro", idMiembro).getResultList();
                    if (!lista.isEmpty()) {
                        cantidadRespuestas = (long) em.createNamedQuery("CompraProducto.countRespuestasProductoGenerico")
                                .setParameter("idProducto", idProducto)
                                .setParameter("lista", lista)
                                .getSingleResult();
                    } else {
                        cantidadRespuestas = 0;
                    }

                }
                break;
            }
            case POR_MES: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -1);
                if (idMiembro == null) {
                    lista = em.createNamedQuery("TransaccionCompra.findTransaccionTiempoProducto")
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getResultList();
                    if (!lista.isEmpty()) {
                        cantidadRespuestas = (long) em.createNamedQuery("CompraProducto.countRespuestasProductoGenerico")
                                .setParameter("idProducto", idProducto)
                                .setParameter("lista", lista)
                                .getSingleResult();
                    } else {
                        cantidadRespuestas = 0;
                    }
                } else {
                    lista = em.createNamedQuery("TransaccionCompra.findTransaccionTiempoProductoMiembro")
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getResultList();
                    if (!lista.isEmpty()) {
                        cantidadRespuestas = (long) em.createNamedQuery("CompraProducto.countRespuestasProductoGenerico")
                                .setParameter("idProducto", idProducto)
                                .setParameter("lista", lista)
                                .getSingleResult();
                    } else {
                        cantidadRespuestas = 0;
                    }

                }
                break;
            }
            case POR_SEMANA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.WEEK_OF_YEAR, -1);
                if (idMiembro == null) {
                    lista = em.createNamedQuery("TransaccionCompra.findTransaccionTiempoProducto")
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getResultList();
                    if (!lista.isEmpty()) {
                        cantidadRespuestas = (long) em.createNamedQuery("CompraProducto.countRespuestasProductoGenerico")
                                .setParameter("idProducto", idProducto)
                                .setParameter("lista", lista)
                                .getSingleResult();
                    } else {
                        cantidadRespuestas = 0;
                    }
                } else {
                    lista = em.createNamedQuery("TransaccionCompra.findTransaccionTiempoProductoMiembro")
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getResultList();
                    if (!lista.isEmpty()) {
                        cantidadRespuestas = (long) em.createNamedQuery("CompraProducto.countRespuestasProductoGenerico")
                                .setParameter("idProducto", idProducto)
                                .setParameter("lista", lista)
                                .getSingleResult();
                    } else {
                        cantidadRespuestas = 0;
                    }

                }
                break;
            }
            case POR_DIA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                if (idMiembro == null) {
                    lista = em.createNamedQuery("TransaccionCompra.findTransaccionTiempoProducto")
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getResultList();
                    if (!lista.isEmpty()) {
                        cantidadRespuestas = (long) em.createNamedQuery("CompraProducto.countRespuestasProductoGenerico")
                                .setParameter("idProducto", idProducto)
                                .setParameter("lista", lista)
                                .getSingleResult();
                    } else {
                        cantidadRespuestas = 0;
                    }
                } else {
                    lista = em.createNamedQuery("TransaccionCompra.findTransaccionTiempoProductoMiembro")
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getResultList();
                    if (!lista.isEmpty()) {
                        cantidadRespuestas = (long) em.createNamedQuery("CompraProducto.countRespuestasProductoGenerico")
                                .setParameter("idProducto", idProducto)
                                .setParameter("lista", lista)
                                .getSingleResult();
                    } else {
                        cantidadRespuestas = 0;
                    }
                }
                break;
            }
            case POR_HORA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.HOUR_OF_DAY, -1);
                if (idMiembro == null) {
                    lista = em.createNamedQuery("TransaccionCompra.findTransaccionTiempoProducto")
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getResultList();
                    if (!lista.isEmpty()) {
                        cantidadRespuestas = (long) em.createNamedQuery("CompraProducto.countRespuestasProductoGenerico")
                                .setParameter("idProducto", idProducto)
                                .setParameter("lista", lista)
                                .getSingleResult();
                    } else {
                        cantidadRespuestas = 0;
                    }
                } else {
                    lista = em.createNamedQuery("TransaccionCompra.findTransaccionTiempoProductoMiembro")
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getResultList();
                    if (!lista.isEmpty()) {
                        cantidadRespuestas = (long) em.createNamedQuery("CompraProducto.countRespuestasProductoGenerico")
                                .setParameter("idProducto", idProducto)
                                .setParameter("lista", lista)
                                .getSingleResult();
                    } else {
                        cantidadRespuestas = 0;
                    }
                }
                break;
            }
            case POR_MINUTO: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MINUTE, -1);
                if (idMiembro == null) {
                    lista = em.createNamedQuery("TransaccionCompra.findTransaccionTiempoProducto")
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getResultList();
                    if (!lista.isEmpty()) {
                        cantidadRespuestas = (long) em.createNamedQuery("CompraProducto.countRespuestasProductoGenerico")
                                .setParameter("idProducto", idProducto)
                                .setParameter("lista", lista)
                                .getSingleResult();
                    } else {
                        cantidadRespuestas = 0;
                    }
                } else {
                    lista = em.createNamedQuery("TransaccionCompra.findTransaccionTiempoProductoMiembro")
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getResultList();
                    if (!lista.isEmpty()) {
                        cantidadRespuestas = (long) em.createNamedQuery("CompraProducto.countRespuestasProductoGenerico")
                                .setParameter("idProducto", idProducto)
                                .setParameter("lista", lista)
                                .getSingleResult();
                    } else {
                        cantidadRespuestas = 0;
                    }

                }
                break;
            }
        }
        //mientras que la cantidad de respuestas siga siendo menor a la cantidad de respuestas totales aceptables...
        return cantidadRespuestas < limiteCantTotal;
    }

    private boolean checkCalendarizacionProducto(ProductoControl producto) {
        //se establece el indicador de respuesta como verdadero(todo es permitido hasta que sea negado)
        boolean flag = true;
        //se obtiene la fecha actual
        Calendar hoy = Calendar.getInstance();
        //si el producto es calendarizado
        if (producto.getEfectividadIndCalendarizado().equals(ProductoControl.Efectividad.CALENDARIZADO.getValue())) {
            //si la fecha de inicio esta establecida, se comprueba que la fecha sea pasada o igual a hoy
            if (producto.getEfectividadFechaInicio() != null) {
                flag = hoy.getTime().after(producto.getEfectividadFechaInicio()) || DateUtils.isSameDay(hoy.getTime(), producto.getEfectividadFechaInicio());
            }
            //mientras siga siendo verdadero
            //si la fecha de fin esta establecida, se comprueba que la fecha sea posterior o igual a hoy
            if (flag && producto.getEfectividadFechaFin() != null) {
                flag = hoy.getTime().before(producto.getEfectividadFechaFin()) || DateUtils.isSameDay(hoy.getTime(), producto.getEfectividadFechaFin());
            }
            //mientras siga siendo verdadero
            if (flag && producto.getEfectividadIndDias() != null) {
                //se obtiene el numero de dia de hoy
                int hoydia = hoy.get(Calendar.DAY_OF_WEEK);
                flag = Chars.asList(producto.getEfectividadIndDias().toCharArray()).stream().anyMatch((t) -> {
                    //segun el indicador de dia de recurrencia se verifica si el numero de dia de hoy concuerda con el numero de dia  asociado al indicador
                    switch (t) {
                        case 'L': {
                            return Calendar.MONDAY == hoydia;
                        }
                        case 'K': {
                            return Calendar.TUESDAY == hoydia;
                        }
                        case 'M': {
                            return Calendar.WEDNESDAY == hoydia;
                        }
                        case 'J': {
                            return Calendar.TUESDAY == hoydia;
                        }
                        case 'V': {
                            return Calendar.FRIDAY == hoydia;
                        }
                        case 'S': {
                            return Calendar.SATURDAY == hoydia;
                        }
                        case 'D': {
                            return Calendar.SUNDAY == hoydia;
                        }
                        default: {
                            return false;
                        }
                    }
                });
            }
            if (flag && producto.getEfectividadIndSemana() != null) {
                long weeksPast = ChronoUnit.WEEKS.between(LocalDateTime.ofInstant(Instant.ofEpochMilli(producto.getEfectividadFechaInicio() == null ? producto.getFechaPublicacion().getTime() : producto.getEfectividadFechaInicio().getTime()), ZoneId.systemDefault()), LocalDateTime.now());
                if (weeksPast % producto.getEfectividadIndSemana().longValue() != 0) {
                    flag = false;
                }
            }
        }
        return flag;
    }

}
