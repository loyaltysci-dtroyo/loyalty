package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.Metrica;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author wtencio, svargas
 */
@Stateless
public class MetricaBean {

    @EJB
    private RegistroMetricaService registroMetricaService;

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public MetricaBean() {
    }

    /**
     * Método que obtiene una lista de categorias de misiones existentes
     *
     * @param locale
     * @return lista de categorias de mision y su rango
     */
    public List<Metrica> getMetricas(Locale locale) {

        //lectura de categorias de mision en un rango de resultados
        return em.createNamedQuery("Metrica.findAll")
                .getResultList();
    }
}
