package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@ApiModel(value = "RegistroNoticia")
@XmlRootElement
public class NewNoticia implements Serializable {
    
    @ApiModelProperty(value = "Titulo de la noticia", required = true)
    private String titulo;
    
    @ApiModelProperty(value = "Texto de contenido de la noticia", required = true)
    private String contenido;
    
    @ApiModelProperty(value = "Base64 de la imagen adjunta")
    private String imagen;
    
    @ApiModelProperty(value = "Identificador de la categoria relacionada")
    private String idCategoria;

    public NewNoticia() {
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }
}
