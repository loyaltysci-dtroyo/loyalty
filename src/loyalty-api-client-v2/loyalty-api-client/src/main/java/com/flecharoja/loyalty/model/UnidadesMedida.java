/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "UNIDADES_MEDIDA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UnidadesMedida.findAll", query = "SELECT u FROM UnidadesMedida u")
    , @NamedQuery(name = "UnidadesMedida.findByIdUnidad", query = "SELECT u FROM UnidadesMedida u WHERE u.idUnidad = :idUnidad")
    , @NamedQuery(name = "UnidadesMedida.findByDescripcion", query = "SELECT u FROM UnidadesMedida u WHERE u.descripcion = :descripcion")
    , @NamedQuery(name = "UnidadesMedida.findByFechaCreacion", query = "SELECT u FROM UnidadesMedida u WHERE u.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "UnidadesMedida.findByUsuarioCreacion", query = "SELECT u FROM UnidadesMedida u WHERE u.usuarioCreacion = :usuarioCreacion")
    , @NamedQuery(name = "UnidadesMedida.findByFechaModificacion", query = "SELECT u FROM UnidadesMedida u WHERE u.fechaModificacion = :fechaModificacion")
    , @NamedQuery(name = "UnidadesMedida.findByUsuarioModificacion", query = "SELECT u FROM UnidadesMedida u WHERE u.usuarioModificacion = :usuarioModificacion")
    , @NamedQuery(name = "UnidadesMedida.findByNumVersion", query = "SELECT u FROM UnidadesMedida u WHERE u.numVersion = :numVersion")})
public class UnidadesMedida implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_UNIDAD")
    private String idUnidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;


    public UnidadesMedida() {
    }

    public UnidadesMedida(String idUnidad) {
        this.idUnidad = idUnidad;
    }

    public UnidadesMedida(String idUnidad, String descripcion, Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.idUnidad = idUnidad;
        this.descripcion = descripcion;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public String getIdUnidad() {
        return idUnidad;
    }

    public void setIdUnidad(String idUnidad) {
        this.idUnidad = idUnidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUnidad != null ? idUnidad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UnidadesMedida)) {
            return false;
        }
        UnidadesMedida other = (UnidadesMedida) object;
        if ((this.idUnidad == null && other.idUnidad != null) || (this.idUnidad != null && !this.idUnidad.equals(other.idUnidad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.UnidadesMedida[ idUnidad=" + idUnidad + " ]";
    }
    
}
