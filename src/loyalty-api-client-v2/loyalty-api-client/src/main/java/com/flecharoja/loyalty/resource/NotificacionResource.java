package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.service.MiembroBean;
import com.flecharoja.loyalty.service.NotificacionBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ejb.EJB;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

@Api(value = "Notificaciones")
@Path("notificacion")
public class NotificacionResource {
    
    @EJB
    NotificacionBean notificacionBean;
    
    @EJB
    MiembroBean miembroBean;
    
    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;
    
    @ApiOperation(value = "Obtener todas las notificaciones para el miembro",
            responseContainer = "List",
            response = Notificacion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Notificacion> getNotificaciones(@ApiParam(value = "Tipo de notificacion deseada (1=vistas, 0=no vistas, --=todas)") @QueryParam("tipo") String tipo){
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
       return notificacionBean.getNotificaciones(idMiembro, tipo);
    }
    
    @ApiOperation(value = "Obtener informacion de una notificacion en especifico")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("{idNotificacion}")
    @Produces(MediaType.APPLICATION_JSON)
    public Notificacion getNotificacion(
            @ApiParam(value = "Identificador de la notificacion" , required = true)
            @PathParam("idNotificacion") String idNotificacion){
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return notificacionBean.getNotificacion(idMiembro, idNotificacion,request.getLocale());
    }
    
    @ApiOperation(value = "Marca la notificacion como vista")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Path("{idNotificacion}/marcar-visto")
    public void setNotificacionVisto(
            @ApiParam(value = "Identificador de la notificacion" , required = true)
            @PathParam("idNotificacion") String idNotificacion){
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        notificacionBean.setNotificacionVisto(idMiembro, idNotificacion, request.getLocale());
    }

    @ApiOperation(value = "Obtener las notificaciones vistas por el miembro en las cuales es elegible",
            responseContainer = "List",
            response = Notificacion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("vistas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Notificacion> getNotificacionesVistas(){
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return notificacionBean.getNotificacionesVistas(idMiembro);
    }

    @ApiOperation(value = "Obtener las notificaciones nuevas por el miembro en las cuales es elegible",
            responseContainer = "List",
            response = Notificacion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("nuevas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Notificacion> getNotificacionesNuevas(){
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return notificacionBean.getNotificacionesNoVistas(idMiembro);
    }
    
    @ApiOperation(value = "Refrescar el token de registro de Firebase Cloud Messaging")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Path("refrescar-fcm-token")
    @Consumes(MediaType.APPLICATION_JSON)
    public void resetFCMTokenRegistro(
            @ApiParam(value = "Valor del token de registro de FCM" , required = true) JsonObject data){
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        System.out.println("FCM: " + data.toString());
        miembroBean.updateFCMTokenMiembro(idMiembro, data);
    }
}
