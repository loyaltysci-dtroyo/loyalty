package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.InstanciaMillonario;
import com.flecharoja.loyalty.service.InstanciaMillonarioBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio, svargas
 */
@Api(value = "Instancia millonario")
@Path("instancia-millonario")
public class InstanciaMillonarioResource {

    @EJB
    InstanciaMillonarioBean instanciaMillonarioBean;

    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;

    /**
     * Método que obtiene la informacion de una instancia
     *
     * @param idInstanciaMillonario
     * @return informacion de una instancia
     */
    @ApiOperation(value = "Obtener una instancia de juego millonario",
            response = InstanciaMillonario.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("{idInstanciaMillonario}")
    @Produces(MediaType.APPLICATION_JSON)
    public InstanciaMillonario getInstanciaMillonario(
        @ApiParam(value = "Identificador de la instancia")
        @PathParam("idInstanciaMillonario") String idInstanciaMillonario
    ) {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
       return instanciaMillonarioBean.getInstanciaMillonario(idInstanciaMillonario, request.getLocale());
    }
    
    /**
     * Método que obtiene la informacion de una instancia
     *
     * @param idMision
     * @return informacion de una instancia
     */
    @ApiOperation(value = "Obtener una instancia de juego millonario",
            response = InstanciaMillonario.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public InstanciaMillonario getInstanciaMillonarioPorMision(
        @ApiParam(value = "Identificador de la instancia")
        @QueryParam("idMision") String idMision
    ) {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
       return instanciaMillonarioBean.getInstanciaMillonarioPorMision(idMision, request.getLocale());
    }
}
