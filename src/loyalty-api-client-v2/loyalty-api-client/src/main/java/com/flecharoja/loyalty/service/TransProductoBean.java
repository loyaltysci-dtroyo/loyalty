/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;


import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CertificadoCategoria;
import com.flecharoja.loyalty.model.TransCarrito;
import com.flecharoja.loyalty.model.TransProducto;
import com.flecharoja.loyalty.model.TransProductoPK;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class TransProductoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    TransCarritoBean transCarritoBean;
    
    @EJB
    ProductoBean productoBean;
    
    @EJB
    CertificadoCategoriaBean certificadoCategoriaBean;

    /**
     * Método que obtiene un arrito disponible
     *
     * @param numTransaccion
     * @param locale
     * @return Carritos
     */
    public List<TransProducto> getProductos(String numTransaccion, Locale locale) {
        
        //obtencion de todas los carritos 
        return em.createNamedQuery("TransProducto.findByIdTransProducto").setParameter("numTransaccion", numTransaccion).getResultList();
    }
    
    /**
     * Método que inserta un nuevo carrito en la base de datos
     *
     * @param idProducto
     * @param cantidad
     * @param idCategoriaProducto
     * @param idMiembro identificador del usuario en sesion
     * @param locale
     * @return identificador del producto registrado
     */
    public TransProductoPK insertTransProducto(String idProducto, String idMiembro, String idCategoriaProducto, Integer cantidad, Locale locale) {
        productoBean.updateCantidadProducto(idProducto, cantidad, locale);
        List<TransCarrito> carritos = transCarritoBean.getTransCarrito(idMiembro, idCategoriaProducto, locale);
        String numTransaccion = null;
        CertificadoCategoria certificado = null;
        
        try {
            if (carritos.isEmpty()) {
                List<CertificadoCategoria> certificados = certificadoCategoriaBean.getCertificadosDisponibles(idCategoriaProducto);
                if (certificados.isEmpty()) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "not_certificate_available");
                }
                certificado = certificadoCategoriaBean.updateCertificadoCategoria(certificados.get(0), 'A');
                numTransaccion = transCarritoBean.insertTransCarrito(new TransCarrito(), idMiembro, idCategoriaProducto, certificado, locale);
            } else {
                numTransaccion = carritos.get(0).getNumTransaccion();
            }
            TransProductoPK transPK = new TransProductoPK(numTransaccion, idProducto);
            TransProducto transProducto = em.find(TransProducto.class, transPK);
            if (transProducto != null) {
                transProducto.setCantidad(transProducto.getCantidad() + cantidad);
                try {
                    em.merge(transProducto);
                    em.flush();
                } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
                }
            } else {
                transProducto = new TransProducto(transPK);
                transProducto.setCantidad(cantidad);
                transProducto.setUsuarioCreacion(idMiembro);
                transProducto.setNumVersion(1);
                transProducto.setPrecio(0.0);
                try {
                    em.persist(transProducto);
                    em.flush();
                } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
                }
            }
            return transProducto.getTransProductoPK();
            
        } catch(RuntimeException e) {
             productoBean.updateCantidadProducto(idProducto, cantidad * -1, locale);
            if (certificado != null) {
                certificadoCategoriaBean.updateCertificadoCategoria(certificado, 'D');
                if (numTransaccion != null) {
                    transCarritoBean.deleteTransCarrito(numTransaccion);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid");
        }
    }
    
    /**
     * Método que inserta un nuevo carrito en la base de datos
     *
     * @param numTransaccion
     * @param idProducto
     * @param cantidad
     * @param locale
     * @return identificador del producto registrado
     */
    public TransProducto deleteTransProducto(String numTransaccion, String idProducto, Integer cantidad, Locale locale) {
        TransProductoPK transPK = new TransProductoPK(numTransaccion, idProducto);
        TransProducto transProducto = em.find(TransProducto.class, transPK);
        if (transProducto == null || transProducto.getCantidad() < cantidad) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid");
        }
        productoBean.updateCantidadProducto(idProducto, -cantidad, locale);
        try {
            if (transProducto.getCantidad() > cantidad) {
                transProducto.setCantidad(transProducto.getCantidad() - cantidad);
                em.merge(transProducto);
                em.flush();
            } else {
                em.remove(transProducto);
                em.flush();
                transProducto.setCantidad(0);
            }
        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            productoBean.updateCantidadProducto(idProducto, cantidad, locale);
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        return transProducto;
    }
}
