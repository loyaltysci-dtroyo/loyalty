package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CategoriaPromocion;
import com.flecharoja.loyalty.model.MarcadorPromocion;
import com.flecharoja.loyalty.model.MarcadorPromocionPK;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.model.PromocionControl;
import com.flecharoja.loyalty.model.PromocionListaMiembro;
import com.flecharoja.loyalty.model.PromocionListaMiembroPK;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.google.common.collect.Lists;
import com.google.common.primitives.Chars;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author svargas
 */
@Stateless
public class PromocionBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @EJB
    SegmentoBean segmentoBean;
    
    private final Configuration hbaseConf;

    public PromocionBean() {
        this.hbaseConf = new Configuration();
        this.hbaseConf.addResource("conf/hbase/hbase-site.xml");
    }

    /**
     * Método que obtiene una lista de categorias de promociones existentes
     *
     * @param locale
     * @return lista de categorias de promocion
     */
    public List<CategoriaPromocion> getCategoriasPromocion(Locale locale) {
       return em.createNamedQuery("CategoriaPromocion.findAll").getResultList();
    }

    /**
     * Método que devuelve una lista de promociones la cual pertenece a la
     * categoria que se pasa por parametro y tambien que cumpla con que el
     * miembro se encuentre entre sus elegibles
     *
     * @param idCategoria identificador de la categoria de promocion
     * @param idMiembro identificador del miembro
     * @return lista de promociones resultantes
     */
    public List<Promocion> getPromocionesPorCategoria(String idCategoria, String idMiembro) {       
        List<PromocionControl> promociones = em.createNamedQuery("PromocionCategoriaPromo.findPromocionControlByIdCategoriaByPromocionIndEstado")
                .setParameter("idCategoria", idCategoria)
                .setParameter("indEstado", PromocionControl.Estados.PUBLICADO.getValue())
                .getResultList();
        
        //se retraen los segmentos eligibles para el miembro
        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);
        
        return promociones.stream()
                .filter((t) -> checkElegibilidadPromocionMiembro(t.getIdPromocion(), idMiembro, segmentosPertenecientes)&& checkCalendarizacionPromocion(t))
                .sorted((p1, p2) -> {
                    //obtencion de fecha a comparar segun el indicador de calendarizacion
                    Date fecha1 = p1.getIndCalendarizacion()==PromocionControl.IndicadoresCalendarizacion.CALENDARIZADO.getValue()?p1.getFechaInicio():p1.getFechaPublicacion();
                    Date fecha2 = p2.getIndCalendarizacion()==PromocionControl.IndicadoresCalendarizacion.CALENDARIZADO.getValue()?p2.getFechaInicio():p2.getFechaPublicacion();
                    //comparacion de fecha en orden descendente
                    return fecha2.compareTo(fecha1);
                })
                .map((t) -> {
                    Promocion promocion = em.find(Promocion.class, t.getIdPromocion());
                    promocion.setMarcada(em.find(MarcadorPromocion.class, new MarcadorPromocionPK(t.getIdPromocion(), idMiembro))!=null);
                    return promocion;
                })
                .collect(Collectors.toList());
    }

    /**
     * Método que la vista de la promocion
     *
     * @param idMiembro identificador del miembro
     * @param idPromocion identificador de la promocion
     * @param locale
     */
    public void registerVistaPromocion(String idMiembro, String idPromocion, Locale locale) {
        PromocionControl promocion = em.find(PromocionControl.class, idPromocion);
        
        if (promocion == null){
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        
        //se retraen los segmentos eligibles para el miembro
        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);
        
        if (!promocion.getIndEstado().equals(PromocionControl.Estados.PUBLICADO.getValue()) || !checkElegibilidadPromocionMiembro(idPromocion, idMiembro, segmentosPertenecientes)) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "promo_no_available");
        }
        
        insertRegistroVistaPromocion(idMiembro, idPromocion);
        
        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.VER_PROMOCION, idPromocion, idMiembro);
        } catch (Exception ex) {
            Logger.getLogger(PromocionBean.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    /**
     * Método que devuelve una lista de promociones en las cuales el miembro es
     * elegible
     *
     * @param idMiembro identificador del miembro
     * @return lista de misiones resultantes
     */
    public List<Promocion> getPromociones(String idMiembro) {
        List<PromocionControl> promociones = em.createNamedQuery("PromocionControl.findAllByIndEstado")
                .setParameter("indEstado", PromocionControl.Estados.PUBLICADO.getValue())
                .getResultList();
        
        //se retraen los segmentos eligibles para el miembro
        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);
        
        return promociones.stream()
                .filter((t) -> checkElegibilidadPromocionMiembro(t.getIdPromocion(), idMiembro, segmentosPertenecientes)&& checkCalendarizacionPromocion(t))
                .sorted((p1, p2) -> {
                    //obtencion de fecha a comparar segun el indicador de calendarizacion
                    Date fecha1 = p1.getIndCalendarizacion()==PromocionControl.IndicadoresCalendarizacion.CALENDARIZADO.getValue()?p1.getFechaInicio():p1.getFechaPublicacion();
                    Date fecha2 = p2.getIndCalendarizacion()==PromocionControl.IndicadoresCalendarizacion.CALENDARIZADO.getValue()?p2.getFechaInicio():p2.getFechaPublicacion();
                    //comparacion de fecha en orden descendente
                    return fecha2.compareTo(fecha1);
                })
                .map((t) -> {
                    Promocion promocion = em.find(Promocion.class, t.getIdPromocion());
                    promocion.setMarcada(em.find(MarcadorPromocion.class, new MarcadorPromocionPK(t.getIdPromocion(), idMiembro))!=null);
                    return promocion;
                })
                .collect(Collectors.toList());
    }
    
    /**
     * Método que marca una promocion como favorita
     * @param idMiembro identificador del miembro
     * @param idPromocion identificador de la promocion
     * @param locale
     */
    public void bookmarkPromocion(String idMiembro, String idPromocion, Locale locale) {
        PromocionControl promocion = em.find(PromocionControl.class, idPromocion);
        
        if (promocion == null){
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        
        //se retraen los segmentos eligibles para el miembro
        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);
        
        if (!promocion.getIndEstado().equals(PromocionControl.Estados.PUBLICADO.getValue()) || !checkElegibilidadPromocionMiembro(idPromocion, idMiembro, segmentosPertenecientes)) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "promo_no_available");
        }
        
        MarcadorPromocion marcadorPromocion = em.find(MarcadorPromocion.class, new MarcadorPromocionPK(idPromocion, idMiembro));
        
        if (marcadorPromocion==null) {
            em.persist(new MarcadorPromocion(idPromocion, idMiembro));
            
            try {
                myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.MARCO_PROMOCION, idPromocion, idMiembro);
            } catch (Exception ex) {
                Logger.getLogger(PromocionBean.class.getName()).log(Level.WARNING, null, ex);
            }
        } else {
            em.remove(marcadorPromocion);
        }
    }

    /**
     * Metodo que obtiene el listado de promociones favoritas para un miembro
     * @param idMiembro identificador del miembro
     * @return lista de promociones
     */
    public List<Promocion> getPromocionesFavoritas(String idMiembro) {
        List<PromocionControl> promociones = em.createNamedQuery("MarcadorPromocion.findPromocionControlByIdMiembroByIndEstado")
                .setParameter("idMiembro", idMiembro)
                .setParameter("indEstado", PromocionControl.Estados.PUBLICADO.getValue())
                .getResultList();
        
        //se retraen los segmentos eligibles para el miembro
        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);
        
        List<Promocion> resultado = promociones.stream()
                .filter((t) -> checkElegibilidadPromocionMiembro(t.getIdPromocion(), idMiembro, segmentosPertenecientes)&& checkCalendarizacionPromocion(t))
                .sorted((p1, p2) -> {
                    //obtencion de fecha a comparar segun el indicador de calendarizacion
                    Date fecha1 = p1.getIndCalendarizacion()==PromocionControl.IndicadoresCalendarizacion.CALENDARIZADO.getValue()?p1.getFechaInicio():p1.getFechaPublicacion();
                    Date fecha2 = p2.getIndCalendarizacion()==PromocionControl.IndicadoresCalendarizacion.CALENDARIZADO.getValue()?p2.getFechaInicio():p2.getFechaPublicacion();
                    //comparacion de fecha en orden descendente
                    return fecha2.compareTo(fecha1);
                })
                .map((t) -> {
                    Promocion promocion = em.find(Promocion.class, t.getIdPromocion());
                    promocion.setMarcada(true);
                    return promocion;
                })
                .collect(Collectors.toList());
        
        return resultado;
    }
    
    /**
     * Método que chekea la elegibilidad de un miembro en una promocion
     * @param idPromocion identificador de la promocion
     * @param idMiembro identificador del miembro
     * @return true or false
     */
    private boolean checkElegibilidadPromocionMiembro(String idPromocion, String idMiembro, List<String> segmentosPertenecientes) {
        PromocionListaMiembro miembroIncluidoExcluido = em.find(PromocionListaMiembro.class, new PromocionListaMiembroPK(idMiembro, idPromocion));
        if (miembroIncluidoExcluido!=null) {
            //si existe... se filtra por su tipo... (inclusion o exclusion)
            switch (miembroIncluidoExcluido.getIndTipo()) {
                case Indicadores.INCLUIDO: {
                    return true;
                }
                case Indicadores.EXCLUIDO: {
                    return false;
                }
            }
        }
        
        if (!segmentosPertenecientes.isEmpty()) {
            List<List<String>> partitionsSegmentos;
            partitionsSegmentos = Lists.partition(segmentosPertenecientes, 999);
            //los segmentos eligibles se particionan en listas de 1000 (por limitante de numero de identificadores en clausula SQL "IN")
            //se verifica si existe algun segmento excluido en la mision que este dentro de los segmentos pertenecientes del miembro
            if (partitionsSegmentos.stream()
                    .anyMatch((l) -> ((long)em.createNamedQuery("PromocionListaSegmento.countByIdPromocionAndSegmentosInListaAndByIndTipo")
                            .setParameter("idPromocion", idPromocion)
                            .setParameter("lista", l)
                            .setParameter("indTipo", Indicadores.EXCLUIDO)
                            .getSingleResult() > 0)))
                return false;
            //se verifica si existe algun segmento incluido en la mision que este dentro de los segmentos pertenecientes del miembro
            if (partitionsSegmentos.stream()
                    .anyMatch((l) -> ((long)em.createNamedQuery("PromocionListaSegmento.countByIdPromocionAndSegmentosInListaAndByIndTipo")
                            .setParameter("idPromocion", idPromocion)
                            .setParameter("lista", l)
                            .setParameter("indTipo", Indicadores.INCLUIDO)
                            .getSingleResult() > 0)))
                return true;
        }
        //se comprueba si no existe ningun segmento incluido para la mision (se asume que es dirigido hacia todos)
        return (long)em.createNamedQuery("PromocionListaSegmento.countByIdPromocionAndByIndTipo")
                .setParameter("idPromocion", idPromocion)
                .setParameter("indTipo", Indicadores.INCLUIDO)
                .getSingleResult() == 0;
    }
    
    /**
     * Método que chekea la calendarazacion de una promocion
     * @param promocion info de la promocion
     * @return true or false
     */
    private boolean checkCalendarizacionPromocion(PromocionControl promocion) {
        //se establece el indicador de respuesta como verdadero (todo es permitido hasta que se demuestre lo contrario)
        boolean flag = true;
        //se obtiene la fecha de hoy
        Calendar hoy = Calendar.getInstance();
        
        //si la calendarizacion es calendarizada...
        if (promocion.getIndCalendarizacion().equals(PromocionControl.IndicadoresCalendarizacion.CALENDARIZADO.getValue())) {
            //si la fecha de inicio esta establecida, se comprueba que la fecha sea pasada a hoy o que sea el mismo dia de hoy
            if (promocion.getFechaInicio()!=null) {
                flag = hoy.getTime().after(promocion.getFechaInicio()) || DateUtils.isSameDay(hoy.getTime(), promocion.getFechaInicio());
            }
            //mientras que siga siendo verdadero...
            //si la fecha de fin esta establecida, se comprueba que la fecha sea posterior a hoy o que sea el mismo dia de hoy
            if (flag && promocion.getFechaFin()!=null) {
                flag = hoy.getTime().before(promocion.getFechaFin()) || DateUtils.isSameDay(hoy.getTime(), promocion.getFechaFin());
            }
            //mientras que siga siendo verdadero...
            //si se establecieron dias de recurrencia...
            if (flag && promocion.getIndDiaRecurrencia()!=null) {
                //se obtiene el numero de dia de hoy
                int hoyDia = hoy.get(Calendar.DAY_OF_WEEK);
                
                //se recorre los indicadores de dias de recurrencia en busca de que se cumpla al menos una condicion...
                flag = Chars.asList(promocion.getIndDiaRecurrencia().toCharArray()).stream().anyMatch((t) -> {
                    //segun el indicador de dia de recurrencia, se verifica si el numero de dia de hoy concuerda con el numero de dia asociado al indicador
                    switch (t) {
                        case 'L': {
                            return Calendar.MONDAY == hoyDia;
                        }
                        case 'K': {
                            return Calendar.TUESDAY == hoyDia;
                        }
                        case 'M': {
                            return Calendar.WEDNESDAY == hoyDia;
                        }
                        case 'J': {
                            return Calendar.THURSDAY == hoyDia;
                        }
                        case 'V': {
                            return Calendar.FRIDAY == hoyDia;
                        }
                        case 'S': {
                            return Calendar.SATURDAY == hoyDia;
                        }
                        case 'D': {
                            return Calendar.SUNDAY == hoyDia;
                        }
                        default: {
                            return false;
                        }
                    }
                });
            }
            
            if (flag && promocion.getIndSemanaRecurrencia()!=null) {
                long weeksPast = ChronoUnit.WEEKS.between(LocalDateTime.ofInstant(Instant.ofEpochMilli(promocion.getFechaInicio() == null ? promocion.getFechaPublicacion().getTime() : promocion.getFechaInicio().getTime()), ZoneId.systemDefault()), LocalDateTime.now());
                if (weeksPast%promocion.getIndSemanaRecurrencia().longValue()!=0) {
                    flag = false;
                }
            }
        }
        
        return flag;
    }
    
    /**
     * registra un registro de vista de promocion
     *
     * @param idMiembro
     * @param idPromocion
     */
    public void insertRegistroVistaPromocion(String idMiembro, String idPromocion) {
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "VISTAS_PROMOCION"));

            //se establece el registro con la fecha actual
            byte[] rowkey = Bytes.toBytes(String.format("%013d", new Date().getTime()) + "&" + idMiembro);
            Put put = new Put(rowkey);
            put.add(new KeyValue(rowkey, Bytes.toBytes("DATA"), Bytes.toBytes("PROMOCION"), Bytes.toBytes(idPromocion)));
            //se almacena el registro
            table.put(put);
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, Locale.getDefault(), "database_connection_failed");
        }
    }
}
