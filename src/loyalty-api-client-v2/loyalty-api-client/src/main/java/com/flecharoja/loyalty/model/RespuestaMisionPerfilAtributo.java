package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaMisionPerfilAtributo {
    @ApiModelProperty(value = "Identificador del atributo a actualizar", required = true)
    private String idAtributo;
    @ApiModelProperty(value = "Respuesta a la actualización del atributo", required = true)
    private String respuesta;

    public RespuestaMisionPerfilAtributo() {
    }

    public String getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
}
