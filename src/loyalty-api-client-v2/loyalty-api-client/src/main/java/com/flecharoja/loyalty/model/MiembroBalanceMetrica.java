/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "MIEMBRO_BALANCE_METRICA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MiembroBalanceMetrica.findAll", query = "SELECT m FROM MiembroBalanceMetrica m"),
    @NamedQuery(name = "MiembroBalanceMetrica.findByIdMiembro", query = "SELECT m FROM MiembroBalanceMetrica m WHERE m.miembroBalanceMetricaPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroBalanceMetrica.findByIdMetrica", query = "SELECT m FROM MiembroBalanceMetrica m WHERE m.miembroBalanceMetricaPK.idMetrica = :idMetrica"),
    @NamedQuery(name = "MiembroBalanceMetrica.findByProgresoActual", query = "SELECT m FROM MiembroBalanceMetrica m WHERE m.progresoActual = :progresoActual"),
    @NamedQuery(name = "MiembroBalanceMetrica.findByTotalAcumulado", query = "SELECT m FROM MiembroBalanceMetrica m WHERE m.totalAcumulado = :totalAcumulado"),
    @NamedQuery(name = "MiembroBalanceMetrica.findByDisponible", query = "SELECT m FROM MiembroBalanceMetrica m WHERE m.disponible = :disponible"),
    @NamedQuery(name = "MiembroBalanceMetrica.findByVencido", query = "SELECT m FROM MiembroBalanceMetrica m WHERE m.vencido = :vencido"),
    @NamedQuery(name = "MiembroBalanceMetrica.findByRedimido", query = "SELECT m FROM MiembroBalanceMetrica m WHERE m.redimido = :redimido")
})
public class MiembroBalanceMetrica implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MiembroBalanceMetricaPK miembroBalanceMetricaPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "PROGRESO_ACTUAL")
    @ApiModelProperty(value = "Progreso que ha tenido el miembro en el transcurso de un tiempo",required = true)
    private Double progresoActual;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_ACUMULADO")
    @ApiModelProperty(value = "Total de una métrica especifica ganada en el transcurso de todo el tiempo de participación",required = true)
    private Double totalAcumulado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "DISPONIBLE")
    @ApiModelProperty(value = "Cantidad de métrica disponible para poder redimir premios",required = true)
    private Double disponible;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "VENCIDO")
    @ApiModelProperty(value = "Total de métrica que el miembro no ha podido utilizar y se ha vencido",required = true)
    private Double vencido;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "REDIMIDO")
    @ApiModelProperty(value = "Total de métrica que un miembro a podido redimir",required = true)
    private Double redimido;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    @ApiModelProperty(value = "Información de la métrica de la cual se obtiene el balance")
    private Metrica metrica;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;

    public MiembroBalanceMetrica() {
    }

    public MiembroBalanceMetrica(String idMiembro, String idMetrica) {
        this.miembroBalanceMetricaPK = new MiembroBalanceMetricaPK(idMiembro, idMetrica);
        this.progresoActual = Double.valueOf(0);
        this.totalAcumulado = Double.valueOf(0);
        this.disponible = Double.valueOf(0);
        this.vencido = Double.valueOf(0);
        this.redimido = Double.valueOf(0);
        this.numVersion = 1L;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public MiembroBalanceMetricaPK getMiembroBalanceMetricaPK() {
        return miembroBalanceMetricaPK;
    }

    public void setMiembroBalanceMetricaPK(MiembroBalanceMetricaPK miembroBalanceMetricaPK) {
        this.miembroBalanceMetricaPK = miembroBalanceMetricaPK;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getIdBalance() {
        return miembroBalanceMetricaPK.getIdMetrica();
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Double getProgresoActual() {
        return progresoActual;
    }

    public void setProgresoActual(Double progresoActual) {
        this.progresoActual = progresoActual;
    }

    public Double getTotalAcumulado() {
        return totalAcumulado;
    }

    public void setTotalAcumulado(Double totalAcumulado) {
        this.totalAcumulado = totalAcumulado;
    }

    public Double getDisponible() {
        return disponible;
    }

    public void setDisponible(Double disponible) {
        this.disponible = disponible;
    }

    public Double getVencido() {
        return vencido;
    }

    public void setVencido(Double vencido) {
        this.vencido = vencido;
    }

    public Double getRedimido() {
        return redimido;
    }

    public void setRedimido(Double redimido) {
        this.redimido = redimido;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Miembro getMiembroLower() {
        return miembro;
    }

    public void setMiembroLower(Miembro miembro) {
        this.miembro = miembro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (miembroBalanceMetricaPK != null ? miembroBalanceMetricaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiembroBalanceMetrica)) {
            return false;
        }
        MiembroBalanceMetrica other = (MiembroBalanceMetrica) object;
        if ((this.miembroBalanceMetricaPK == null && other.miembroBalanceMetricaPK != null) || (this.miembroBalanceMetricaPK != null && !this.miembroBalanceMetricaPK.equals(other.miembroBalanceMetricaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MiembroBalanceMetrica[ miembroBalanceMetricaPK=" + miembroBalanceMetricaPK + " ]";
    }
    
}
