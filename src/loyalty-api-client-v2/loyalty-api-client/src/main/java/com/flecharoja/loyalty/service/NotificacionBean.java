package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.InstanciaNotificacion;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.model.PremioControl;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author svargas
 */
@Stateless
public class NotificacionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    public List<Notificacion> getNotificaciones(String idMiembro, String indTipo) {
        if (indTipo!=null) {
            switch(indTipo) {
                case "0": {
                    return getNotificacionesNoVistas(idMiembro);
                }
                case "1": {
                    return getNotificacionesVistas(idMiembro);
                }
            }
        }
        List<InstanciaNotificacion> instanciasNoVistas;
        instanciasNoVistas = em.createNamedQuery("InstanciaNotificacion.findInstanciaByIdMiembro")
                .setParameter("idMiembro", idMiembro)
                .getResultList();

        List<Notificacion> resultado = instanciasNoVistas.stream().map((InstanciaNotificacion instancia) -> {
            Notificacion notificacion = em.find(Notificacion.class, instancia.getIdNotificacion());
            em.detach(notificacion);
            notificacion.setIdNotificacion(instancia.getIdInstancia());
            notificacion.setVisto(instancia.getFechaVisto()!=null);
            if (instancia.getTexto()!=null) {
                notificacion.setTexto(instancia.getTexto());
            }
            return notificacion;
        }).filter((t) -> t.getIndTipo() == Notificacion.Tipos.NOTIFICACION_PUSH.getValue()).collect(Collectors.toList());

        return resultado;
    }

    public List<Notificacion> getNotificacionesNoVistas(String idMiembro) {
        List<InstanciaNotificacion> instanciasNoVistas;
        instanciasNoVistas = em.createNamedQuery("InstanciaNotificacion.findInstanciaByIdMiembroWhereFechaVistoIsNull")
                .setParameter("idMiembro", idMiembro)
                .getResultList();

        List<Notificacion> resultado = instanciasNoVistas.stream().map((InstanciaNotificacion instancia) -> {
            Notificacion notificacion = em.find(Notificacion.class, instancia.getIdNotificacion());
            em.detach(notificacion);
            notificacion.setIdNotificacion(instancia.getIdInstancia());
            notificacion.setVisto(Boolean.FALSE);
            if (instancia.getTexto()!=null) {
                notificacion.setTexto(instancia.getTexto());
            }
            return notificacion;
        }).filter((t) -> t.getIndTipo() == Notificacion.Tipos.NOTIFICACION_PUSH.getValue()).collect(Collectors.toList());

        return resultado;
    }

    public List<Notificacion> getNotificacionesVistas(String idMiembro) {
        List<InstanciaNotificacion> instanciasNoVistas;
        instanciasNoVistas = em.createNamedQuery("InstanciaNotificacion.findInstanciaByIdMiembroWhereFechaVistoIsNotNull")
                .setParameter("idMiembro", idMiembro)
                .getResultList();

        List<Notificacion> resultado = instanciasNoVistas.stream().map((InstanciaNotificacion instancia) -> {
            Notificacion notificacion = em.find(Notificacion.class, instancia.getIdNotificacion());
            em.detach(notificacion);
            notificacion.setIdNotificacion(instancia.getIdInstancia());
            notificacion.setVisto(Boolean.TRUE);
            if (instancia.getTexto()!=null) {
                notificacion.setTexto(instancia.getTexto());
            }
            return notificacion;
        }).filter((t) -> t.getIndTipo() == Notificacion.Tipos.NOTIFICACION_PUSH.getValue()).collect(Collectors.toList());

        return resultado;
    }

    public Notificacion getNotificacion(String idMiembro, String idInstancia, Locale locale) {
        InstanciaNotificacion instancia;
        try {
            instancia = (InstanciaNotificacion) em.createNamedQuery("InstanciaNotificacion.findInstanciaByIdInstanciaIdMiembro")
                    .setParameter("idInstancia", idInstancia)
                    .setParameter("idMiembro", idMiembro)
                    .getSingleResult();
        } catch (NoResultException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }

//        if (instancia.getFechaEntrega()==null) {
//            instancia.setFechaEntrega(new Date());
//            em.merge(instancia);
//        }
        Notificacion notificacion = em.find(Notificacion.class, instancia.getIdNotificacion());

        if (notificacion.getIndTipo() != Notificacion.Tipos.NOTIFICACION_PUSH.getValue()) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "notification_type_invalid");
        }

        if (instancia.getFechaVisto() == null) {
            instancia.setFechaVisto(new Date());
            em.merge(instancia);
            em.flush();

            if (notificacion.getIndObjetivo() != null && Notificacion.Objectivos.get(notificacion.getIndObjetivo()) == Notificacion.Objectivos.RECOMPENSA) {
                PremioControl premio = em.find(PremioControl.class, notificacion.getPremio().getIdPremio());

                if (premio.getIndEstado().equals(PremioControl.Estados.PUBLICADO.getValue())) {
                    //TODO esperar que wendy termine las modificaciones necesarias al metodo de assignAwardMiembro en el admin api
                }
            }
        }

        em.detach(notificacion);
        notificacion.setIdNotificacion(idInstancia);
        
        notificacion.setVisto(Boolean.TRUE);
        
        if (instancia.getTexto()!=null) {
            notificacion.setTexto(instancia.getTexto());
        }

        return notificacion;
    }

    public void setNotificacionVisto(String idMiembro, String idInstancia, Locale locale) {
        InstanciaNotificacion instancia;
        try {
            instancia = (InstanciaNotificacion) em.createNamedQuery("InstanciaNotificacion.findInstanciaByIdInstanciaIdMiembro")
                    .setParameter("idInstancia", idInstancia)
                    .setParameter("idMiembro", idMiembro)
                    .getSingleResult();
        } catch (NoResultException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }

        Notificacion notificacion = em.find(Notificacion.class, instancia.getIdNotificacion());

        if (notificacion.getIndTipo() != Notificacion.Tipos.NOTIFICACION_PUSH.getValue()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "notification_type_invalid");
        }

        if (instancia.getFechaVisto() == null) {
            instancia.setFechaVisto(new Date());
            em.merge(instancia);
            em.flush();

            if (notificacion.getIndObjetivo() != null && Notificacion.Objectivos.get(notificacion.getIndObjetivo()) == Notificacion.Objectivos.RECOMPENSA) {
                PremioControl premio = em.find(PremioControl.class, notificacion.getPremio().getIdPremio());

                if (premio.getIndEstado().equals(PremioControl.Estados.PUBLICADO.getValue())) {
                    //TODO esperar que wendy termine las modificaciones necesarias al metodo de assignAwardMiembro en el admin api
                }
            }
        }
    }

//    public void resetFCMTokenRegistro(String idMiembro, String fcmToken) {
//        List<Miembro> miembrosPasados = em.createNamedQuery("Miembro.findByFcmTokenRegistro").setParameter("fcmTokenRegistro", fcmToken).getResultList();
//        miembrosPasados.forEach((miembroPasado) -> {
//            miembroPasado.setFcmTokenRegistro(null);
//            em.merge(miembroPasado);
//        });
//        Miembro miembro = em.find(Miembro.class, idMiembro);
//        miembro.setFcmTokenRegistro(fcmToken);
//        em.merge(miembro);
//    }
}
