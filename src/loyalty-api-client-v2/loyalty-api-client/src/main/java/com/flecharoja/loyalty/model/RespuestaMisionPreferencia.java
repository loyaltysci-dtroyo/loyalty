package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaMisionPreferencia {
    @ApiModelProperty(value = "Identificador de la preferencia", required = true)
    private String idPreferencia;
    
    @ApiModelProperty(value = "Respuesta a la preferencia", required = true)
    private String respuesta;

    public RespuestaMisionPreferencia() {
    }

    public String getIdPreferencia() {
        return idPreferencia;
    }

    public void setIdPreferencia(String idPreferencia) {
        this.idPreferencia = idPreferencia;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
}
