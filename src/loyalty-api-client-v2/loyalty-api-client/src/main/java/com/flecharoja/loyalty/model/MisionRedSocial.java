package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_RED_SOCIAL")
@XmlRootElement
public class MisionRedSocial implements Serializable {
    
    public enum Tipos {
        ENLACE('L'),
        MENSAJE('M'),
        ME_GUSTA('G'),
        IMAGEN('I');
        
        private final char value;
        private static final Map<Character, Tipos> lookup = new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }
        
        static {
            for (Tipos tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipos get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_MISION")
    @ApiModelProperty(value = "Identificador de la misión",required = true)
    private String idMision;
    
    @Column(name = "IND_TIPO")
    @ApiModelProperty(value = "Indicador del tipo de misión de red social se quiere completar",required = true)
    private Character indTipo;
    
    @Column(name = "MENSAJE")
    @ApiModelProperty(value = "Mensaje que se mostrará en la misión de red social",required = true)
    private String mensaje;
    
    @Column(name = "TITULO_URL")
    @ApiModelProperty(value = "Título de la misión de red social o url a compartir o ver",required = true)
    private String tituloUrl;
    
    @Column(name = "URL_OBJECTIVO")
    @ApiModelProperty(value = "Url objetivo de la misión red social",required = true)
    private String urlObjectivo;
    
    @Column(name = "URL_IMAGEN")
    @ApiModelProperty(value = "Url de la imagen de la misión de red social",required = true)
    private String urlImagen;

    public MisionRedSocial() {
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getTituloUrl() {
        return tituloUrl;
    }

    public void setTituloUrl(String tituloUrl) {
        this.tituloUrl = tituloUrl;
    }

    public String getUrlObjectivo() {
        return urlObjectivo;
    }

    public void setUrlObjectivo(String urlObjectivo) {
        this.urlObjectivo = urlObjectivo;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMision != null ? idMision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionRedSocial)) {
            return false;
        }
        MisionRedSocial other = (MisionRedSocial) object;
        if ((this.idMision == null && other.idMision != null) || (this.idMision != null && !this.idMision.equals(other.idMision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionRedSocial[ idMision=" + idMision + " ]";
    }
    
}
