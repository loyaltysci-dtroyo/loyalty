package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.service.MiembroBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio, svargas
 */
@Api(value = "Perfil del cliente")
@Path("perfil")
public class MiembroResource {

    @EJB
    MiembroBean miembroBean;

    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;

    /**
     * Método que obtiene la informacion basica y requerida del miembro en
     * sesion
     *
     * @return informacion del miembro
     */
    @ApiOperation(value = "Obtener el perfil del miembro en sesión",
            response = Miembro.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Miembro getPerfil() {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
       return miembroBean.getMiembroDetalle(idMiembro,request.getLocale());
    }


    /**
     * Método que actualiza solo la informacion basica y requerida de un miembro
     * del sistema
     *
     * @param miembro archivo json con los datos basicos a modificar
     */
    @ApiOperation(value = "Modificación del perfil del miembro en sesión",
            response = Miembro.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public void updatePerfil(
            @ApiParam(value = "Información a actualizar del miembro en sesión", required = true) Miembro miembro) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        miembroBean.updateMiembro(miembro, idMiembro,request.getLocale());
    }

//    /**
//     * Método que actualiza la contraseña actual del miembro que se encuentra en
//     * sesión
//     *
//     * @param contrasena nueva contraseña
//     * @return resultado de la operación
//     */
//    @ApiOperation(value = "Modificación de la contraseña de paso del miembro en sesión")
//    @ApiResponses(value = {
//        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
//    })
//    @PUT
//    @Path("cambiar-contrasena")
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response updateContrasenaMiembro(
//            @ApiParam(value = "Carácteres que conforman la contraseña", required = true) String contrasena) {
//        String idMiembro;
////        String miembroPrueba = "7a6325e2-1565-4693-a9bb-afc50ef27f84";
//
//        try {
//            idMiembro = context.getUserPrincipal().getName();
//            miembroBean.updateContrasena(contrasena, idMiembro);
//
//        } catch (NullPointerException e) {
//            throw new ErrorClienteException(MyException.Cliente.ERROR_AUTENTICACION);
//        } catch (Exception e) {
//            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
//            throw new ErrorSistemaException(MyException.ErrorSistema.ERROR_DESCONOCIDO);
//        }
//        return Response.ok().build();
//    }
}
