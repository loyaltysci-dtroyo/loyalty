package com.flecharoja.loyalty.exception;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlRootElement;

@ApiModel(value = "Excepcion")
@XmlRootElement
public class MyExceptionResponseBody {
    
    @ApiModelProperty(value = "Código de la excepción", readOnly = true)
    private int code;
    @ApiModelProperty(value = "Mensaje informativo de la excepción ocurrida", readOnly = true)
    private String message;
    @ApiModelProperty(value = "Lenguaje en el que se presenta el mensaje", readOnly = true)
    private String lang;

    public MyExceptionResponseBody() {
    }

    public MyExceptionResponseBody(int code, String message) {
        this.code = code;
        this.message = message;
        this.lang = "en";
    }

    public MyExceptionResponseBody(int code, String message, String lang) {
        this.code = code;
        this.message = message;
        this.lang = lang;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
    
}
