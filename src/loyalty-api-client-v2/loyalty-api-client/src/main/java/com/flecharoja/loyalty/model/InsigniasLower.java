/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "INSIGNIAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InsigniasLower.findAll", query = "SELECT i FROM Insignias i"),
    @NamedQuery(name = "InsigniasLower.findByIdInsignia", query = "SELECT i FROM Insignias i WHERE i.idInsignia = :idInsignia")
})
public class InsigniasLower implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_INSIGNIA")
    private String idInsignia;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INSIGNIA")
    private String nombreInsignia;
  
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    private String imagen;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN_2")
    private String imagen2;


    public InsigniasLower() {
    }

    public InsigniasLower(String idInsignia, String nombreInsignia, String imagen, String imagen2) {
        this.idInsignia = idInsignia;
        this.nombreInsignia = nombreInsignia;
        this.imagen = imagen;
        this.imagen2 = imagen2;
    }


    
  
    public String getIdInsignia() {
        return idInsignia;
    }

    public void setIdInsignia(String idInsignia) {
        this.idInsignia = idInsignia;
    }

    public String getNombreInsignia() {
        return nombreInsignia;
    }

    public void setNombreInsignia(String nombreInsignia) {
        this.nombreInsignia = nombreInsignia;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getImagen2() {
        return imagen2;
    }

    public void setImagen2(String imagen2) {
        this.imagen2 = imagen2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInsignia != null ? idInsignia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InsigniasLower)) {
            return false;
        }
        InsigniasLower other = (InsigniasLower) object;
        if ((this.idInsignia == null && other.idInsignia != null) || (this.idInsignia != null && !this.idInsignia.equals(other.idInsignia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Insignias[ idInsignia=" + idInsignia + " ]";
    }
}