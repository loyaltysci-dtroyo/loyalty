package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.TablaPosiciones;
import com.flecharoja.loyalty.model.TopTabPosiciones;
import com.flecharoja.loyalty.service.TablaPosicionesBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "Perfil del cliente")
@Path("perfil")
public class TablaPosicionesResource {

    @EJB
    TablaPosicionesBean tablaPosicionesBean;
    
    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;

    /**
     * Método que devuelve todas las tablas de posiciones en las que el miembro
     * en sesion tiene participacion
     * @return lista de tablas de posiciones
     */
    @ApiOperation(value = "Obtener leaderboard general",
            response = TablaPosiciones.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("leaderboards/general")
    @Produces(MediaType.APPLICATION_JSON)
    public TablaPosiciones getTablaGeneral() {
        try{
            context.getUserPrincipal().getName();
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return tablaPosicionesBean.getGeneral(request.getLocale());
    }
    
    
    /**
     * Método que devuelve todas las tablas de posiciones en las que el miembro
     * en sesion tiene participacion
     *
     * @param cantidad de registros maximos por pagina
     * @param registro por el cual se comienza la busqueda
     * @return lista de tablas de posiciones
//     */
//    @ApiOperation(value = "Obtener las tablas de posiciones",
//            responseContainer = "List",
//            response = TablaPosiciones.class,
//            responseHeaders = {
//                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
//                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
//            })
//
//    @ApiResponses(value = {
//        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
//    })
    @GET
    @Path("leaderboards")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TablaPosiciones> getTablaPosiciones(
            @ApiParam(value = "Cantidad de registros")
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        String idMiembro;
        try{
            idMiembro = context.getUserPrincipal().getName();
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return tablaPosicionesBean.getTablasPosiciones(idMiembro,request.getLocale());
    }

    /**
     * Método que devuelve una lista de miembros o grupos dependiendo del tipo de tabla 
     * en orden de los mejores acumulados
     *
     * @param idTabla identificador de la tabla de posiciones
     * @return lista de tablas de posiciones
     */
//    @ApiOperation(value = "Obtener las posiciones de grupos o miembros según sea el caso",
//            
//            responseHeaders = {
//                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
//                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
//            })
//
//    @ApiResponses(value = {
//        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
//    })
    @GET
    @Path("leaderboards/{idTabla}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TopTabPosiciones> getPosicionMiembrosOGrupos(
            @ApiParam(value = "Identificador de la tabla de posiciones")
            @PathParam("idTabla")String idTabla) {

        try{
            context.getUserPrincipal().getName();
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return tablaPosicionesBean.getPosicionMiembrosOGrupos(idTabla, request.getLocale());
    }
}
