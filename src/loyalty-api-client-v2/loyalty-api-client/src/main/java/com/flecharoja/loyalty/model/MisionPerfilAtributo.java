package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_PERFIL_ATRIBUTO")
@XmlRootElement
public class MisionPerfilAtributo implements Serializable {
    
    public enum Atributos {
        FECHA_DE_NACIMIENTO('A', "fechaNacimiento"),
        GENERO('B', "indGenero"),
        ESTADO_CIVIL('C', "indEstadoCivil"),
        FRECUENCIA_COMPRA('D', "frecuenciaCompra"),
        NOMBRE('E', "nombre"),
        PRIMER_APELLIDO('F', "apellido"),
        CIUDAD_DE_RESIDENCIA('K', "ciudadResidencia"),
        ESTADO_DE_RESIDENCIA('L', "estadoResidencia"),
        PAIS_DE_RESIDENCIA('M', "paisResidencia"),
        CODIGO_POSTAL('N', "codigoPostal"),
        TIPO_DE_EDUCACION('O', "indEducacion"),
        INGRESO_ECONOMICO('P', "ingresoEconomico"),
        HIJOS('Q', "indHijos"),
        ATRIBUTO_DINAMICO('R', null),
        SEGUNDO_APELLIDO('S', "apellido2");
        
        private final char value;
        private final String columnName;
        private static final Map<Character, Atributos> lookup = new HashMap<>();

        private Atributos(char value, String columnName) {
            this.value = value;
            this.columnName = columnName;
        }
        
        static {
            for (Atributos atributo : values()) {
                lookup.put(atributo.value, atributo);
            }
        }

        public char getValue() {
            return value;
        }

        public String getColumnName() {
            return columnName;
        }
        
        public static Atributos get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_ATRIBUTO")
    @ApiModelProperty(value = "Identificador único del atributo",required = true)
    private String idAtributo;
    
    @Transient
    @ApiModelProperty(value = "Respuesta que se da en el momento del cambio de valor",required = true)
    private String respuesta;
    
    @Column(name = "IND_REQUERIDO")
    @ApiModelProperty(value = "Indicador de obligatoriedad del atributo",required = true)
    private Boolean indRequerido;
    
    @Column(name = "IND_ATRIBUTO")
    @ApiModelProperty(value = "Indicador del atributo a actualizar",required = true)
    private Character indAtributo;
    
    @JoinColumn(name = "ID_ATRIBUTO_DINAMICO", referencedColumnName = "ID_ATRIBUTO")
    @ManyToOne
    @ApiModelProperty(value = "Información del atributo que se va actualizar",required = true)
    private AtributoDinamico atributoDinamico;
    
    @Column(name = "ID_MISION")
    @ApiModelProperty(value = "Identificador de la misión",required = true)
    private String idMision;

    public MisionPerfilAtributo() {
    }

    public String getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Boolean getIndRequerido() {
        return indRequerido;
    }

    public void setIndRequerido(Boolean indRequerido) {
        this.indRequerido = indRequerido;
    }

    public Character getIndAtributo() {
        return indAtributo;
    }

    public void setIndAtributo(Character indAtributo) {
        this.indAtributo = indAtributo == null ? null : Character.toUpperCase(indAtributo);
    }

    public AtributoDinamico getAtributoDinamico() {
        return atributoDinamico;
    }

    public void setAtributoDinamico(AtributoDinamico atributoDinamico) {
        this.atributoDinamico = atributoDinamico;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtributo != null ? idAtributo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionPerfilAtributo)) {
            return false;
        }
        MisionPerfilAtributo other = (MisionPerfilAtributo) object;
        if ((this.idAtributo == null && other.idAtributo != null) || (this.idAtributo != null && !this.idAtributo.equals(other.idAtributo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionPerfilAtributo[ idAtributo=" + idAtributo + " ]";
    }
    
}
