package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "WORKFLOW_LISTA_MIEMBRO")
public class WorkflowListaMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected WorkflowListaMiembroPK workflowListaMiembroPK;
    
    @Column(name = "IND_TIPO")
    private Character indTipo;

    public WorkflowListaMiembro() {
    }

    public WorkflowListaMiembroPK getWorkflowListaMiembroPK() {
        return workflowListaMiembroPK;
    }

    public void setWorkflowListaMiembroPK(WorkflowListaMiembroPK workflowListaMiembroPK) {
        this.workflowListaMiembroPK = workflowListaMiembroPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (workflowListaMiembroPK != null ? workflowListaMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkflowListaMiembro)) {
            return false;
        }
        WorkflowListaMiembro other = (WorkflowListaMiembro) object;
        if ((this.workflowListaMiembroPK == null && other.workflowListaMiembroPK != null) || (this.workflowListaMiembroPK != null && !this.workflowListaMiembroPK.equals(other.workflowListaMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.WorkflowListaMiembro[ workflowListaMiembroPK=" + workflowListaMiembroPK + " ]";
    }
    
}
