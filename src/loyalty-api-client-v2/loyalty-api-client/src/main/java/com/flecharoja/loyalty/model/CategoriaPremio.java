/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CATEGORIA_PREMIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoriaPremio.findAll", query = "SELECT c FROM CategoriaPremio c"),
    @NamedQuery(name = "CategoriaPremio.findByIdCategoria", query = "SELECT c FROM CategoriaPremio c WHERE c.idCategoria = :idCategoria"),
    @NamedQuery(name = "CategoriaPremio.countAll", query = "SELECT COUNT(c) FROM CategoriaPremio c")
})
public class CategoriaPremio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_CATEGORIA")
    @ApiModelProperty(value = "Identificador único y autogenerado de la categoría de premio",required = true)
    private String idCategoria;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    @ApiModelProperty(value = "Nombre de la categoría de premio",required = true)
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    @ApiModelProperty(value = "Imagen representativa de la categoría de premio",required = true)
    private String imagen;
    
    @Size(max = 200)
    @Column(name = "DESCRIPCION")
    @ApiModelProperty(value = "Descripción básica de la categoría de premio")
    private String descripcion;

    public CategoriaPremio() {
    }

    public CategoriaPremio(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCategoria != null ? idCategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaPremio)) {
            return false;
        }
        CategoriaPremio other = (CategoriaPremio) object;
        if ((this.idCategoria == null && other.idCategoria != null) || (this.idCategoria != null && !this.idCategoria.equals(other.idCategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.CategoriaPremio[ idCategoria=" + idCategoria + " ]";
    }
    
}
