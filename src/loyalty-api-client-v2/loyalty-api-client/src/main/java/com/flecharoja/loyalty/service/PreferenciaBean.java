/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;


import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Preferencia;
import com.flecharoja.loyalty.model.PreferenciaMin;
import com.flecharoja.loyalty.model.RespuestaPreferencia;
import com.flecharoja.loyalty.model.RespuestaPreferenciaPK;
import com.flecharoja.loyalty.util.Indicadores;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class PreferenciaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Método que devuelve una lista de preferencias que el miembro puede
     * contestar
     *
     * @param cantidad de registros maximos por pagina
     * @param registro por el cual se comienza la busqueda
     * @param locale
     * @return resultado de la operacion
     */
    public List<PreferenciaMin> getPreferencias(Locale locale) {
        List<Preferencia> preferencias = new ArrayList();
        List<PreferenciaMin> preferenciasMin =  new ArrayList<>();

        try {
            
            preferencias = em.createNamedQuery("Preferencia.findAll")
                    .getResultList();
            
            for (Preferencia preferencia : preferencias) {
                PreferenciaMin preferenciaMin = new PreferenciaMin(preferencia.getIdPreferencia(), preferencia.getPregunta());
                preferenciasMin.add(preferenciaMin);
            }
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }
        return preferenciasMin;
    }

    /**
     * Método que obtiene la información de la preferencia
     *
     * @param idPreferencia identificador de la preferencia
     * @param idMiembro identificador del nombre
     * @param locale
     * @return informacion detallada de la preferencia
     */
    public Preferencia getPreferencia(String idPreferencia, String idMiembro, Locale locale) {
        Preferencia preferencia;
        try {
            preferencia = em.find(Preferencia.class, idPreferencia);
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE,locale, "preference_not_found");
        }

        List<String> respuesta = null;
        try {
            respuesta =  em.createNamedQuery("RespuestaPreferencia.findByIdPreferenciaIdMiembro")
                    .setParameter("idPreferencia", idPreferencia)
                    .setParameter("idMiembro", idMiembro)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "argument_invalid", "idMiembro");
        }

        if (respuesta != null) {
            preferencia.setRespuestaMiembro(respuesta.get(0));
        }

        return preferencia;
    }

    /**
     * Método que actualiza o registra la respuesta de un miembro con una
     * preferencia
     *
     * @param idMiembro identificador del miembro
     * @param idPreferencia identificador de la preferencia
     * @param valor valor a tomar la respuesta de la preferencia
     * @param locale
     */
    public void updateRespuestaPreferencia(String idMiembro, String idPreferencia, String valor, Locale locale) {
        RespuestaPreferencia respuestaPreferencia = new RespuestaPreferencia();
        try {
            List<RespuestaPreferencia> respuestas = em.createNamedQuery("RespuestaPreferencia.findByIdMiembro").setParameter("idMiembro", idMiembro).getResultList();

            if(respuestas.isEmpty()){
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_no_preference");
            }
            
            //verifica qu exista una respuesta a la preferencia
            if (existsAsignacion(respuestas, idPreferencia)) {
                respuestaPreferencia = em.find(RespuestaPreferencia.class, new RespuestaPreferenciaPK(idMiembro, idPreferencia));

            } else {
                respuestaPreferencia = new RespuestaPreferencia(idMiembro, idPreferencia);
                respuestaPreferencia.setFechaCreacion(Calendar.getInstance().getTime());
            }
            //verifica su el valor viene nulo, no permita actualizar
            if (valor == null || valor.equals("null") || "".equals(valor)) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "argument_invalid", "valor");
            } else {
                respuestaPreferencia.setRespuesta(valor);
                em.merge(respuestaPreferencia);
                em.flush();
            }
         } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "argument_invalid", e.getConstraintViolations().stream().map((t)->t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que verifica la asignacion de una respuesta a una preferencia con un miembro
     * @param respuestas lista de respuestas registradas del miembro
     * @param idPreferencia identificador de la preferencia
     * @return true si existe la asignacion, false si no existe
     */
    private boolean existsAsignacion(List<RespuestaPreferencia> respuestas, String idPreferencia) {
        return respuestas.stream().anyMatch((respuesta) -> (respuesta.getRespuestaPreferenciaPK().getIdPreferencia().equals(idPreferencia)));
    }

}
