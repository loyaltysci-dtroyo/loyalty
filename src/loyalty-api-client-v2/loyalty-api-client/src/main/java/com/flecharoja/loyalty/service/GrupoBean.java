/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Grupo;
import com.flecharoja.loyalty.util.Indicadores;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;

/**
 *
 * @author wtencio
 */
@Stateless
public class GrupoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    /**
     * Método que obtiene un listado de grupos a los que pertenece el miembro
     *
     * @param idMiembro identificador del miembro
     * @param locale
     * @return lista de grupos
     */
    public List<Grupo> getGruposPertenecientes(String idMiembro, Locale locale) {
        List<Grupo> grupos = new ArrayList<>();
        try {
            grupos = em.createNamedQuery("GrupoMiembro.findByIdMiembro")
                    .setParameter("idMiembro", idMiembro)
                    .getResultList();
            
            grupos = grupos.stream().filter((Grupo g) -> g.getIndVisible().equals(Grupo.Visibilidad.VISIBLE.getValue()))
                    .collect(Collectors.toList());

        }catch(QueryTimeoutException e){
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }
        return grupos;
    }

    /**
     * Método que obtiene un listado de grupos a los que no pertenece el miembro
     *
     * @param idMiembro identificador del miembro
     * @param locale
     * @return lista de grupos
     */
    public List<Grupo> getGruposNoPertenecientes(String idMiembro, Locale locale) {
        
        List<Grupo> grupos = new ArrayList<>();
        try{
            
            
            grupos =  em.createNamedQuery("GrupoMiembro.findGruposNotInMiembro")
                    .setParameter("idMiembro", idMiembro)
                    .getResultList();
            grupos = grupos.stream().filter((Grupo g) -> g.getIndVisible().equals(Grupo.Visibilidad.VISIBLE.getValue()))
                    .collect(Collectors.toList());
        }catch(QueryTimeoutException e){
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }
        return grupos;
    }

//    /**
//     * Método que obtiene la información detallada del grupo
//     *
//     * @param idGrupo identificador del grupo
//     * @return informacion del grupo
//     */
//    public Grupo getDetalleGrupo(String idGrupo) {
//        Grupo grupo;
//        try{
//            grupo =  em.find(Grupo.class,idGrupo);
//            if(grupo.getIndVisible().equals(Grupo.Visibilidad.INVISIBLE.getValue())){
//                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, "");
//            }
//        }catch(IllegalArgumentException e){
//            throw new ErrorClienteException(MyException.ErrorPeticion.ENTIDAD_NO_EXISTENTE);
//        }
//        return grupo;
//    }

}
