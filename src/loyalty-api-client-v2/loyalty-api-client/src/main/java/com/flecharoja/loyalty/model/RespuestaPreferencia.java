/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "RESPUESTA_PREFERENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RespuestaPreferencia.getRespuestaByIdMiembroIdPreferencia", query = "SELECT r.respuesta FROM RespuestaPreferencia r WHERE r.respuestaPreferenciaPK.idMiembro = :idMiembro AND r.respuestaPreferenciaPK.idPreferencia = :idPreferencia"),
    @NamedQuery(name = "RespuestaPreferencia.findAll", query = "SELECT r FROM RespuestaPreferencia r"),
    @NamedQuery(name = "RespuestaPreferencia.findByIdMiembro", query = "SELECT r FROM RespuestaPreferencia r WHERE r.respuestaPreferenciaPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "RespuestaPreferencia.findByIdPreferenciaIdMiembro", query = "SELECT r.respuesta FROM RespuestaPreferencia r WHERE r.respuestaPreferenciaPK.idPreferencia = :idPreferencia AND r.respuestaPreferenciaPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "RespuestaPreferencia.findByRespuesta", query = "SELECT r FROM RespuestaPreferencia r WHERE r.respuesta = :respuesta"),
    @NamedQuery(name = "RespuestaPreferencia.findByFechaCreacion", query = "SELECT r FROM RespuestaPreferencia r WHERE r.fechaCreacion = :fechaCreacion")})
public class RespuestaPreferencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RespuestaPreferenciaPK respuestaPreferenciaPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "RESPUESTA")
    private String respuesta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;
    @JoinColumn(name = "ID_PREFERENCIA", referencedColumnName = "ID_PREFERENCIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Preferencia preferencia;

    public RespuestaPreferencia() {
    }

    public RespuestaPreferencia(String idMiembro, String idPreferencia, String respuesta, Date fechaCreacion) {
        this.respuestaPreferenciaPK = new RespuestaPreferenciaPK(idMiembro, idPreferencia);
        this.respuesta = respuesta;
        this.fechaCreacion = fechaCreacion;
    }

    public RespuestaPreferencia(String idMiembro, String idPreferencia) {
        this.respuestaPreferenciaPK = new RespuestaPreferenciaPK(idMiembro, idPreferencia);
    }

    public RespuestaPreferenciaPK getRespuestaPreferenciaPK() {
        return respuestaPreferenciaPK;
    }

    public void setRespuestaPreferenciaPK(RespuestaPreferenciaPK respuestaPreferenciaPK) {
        this.respuestaPreferenciaPK = respuestaPreferenciaPK;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public Preferencia getPreferencia() {
        return preferencia;
    }

    public void setPreferencia(Preferencia preferencia) {
        this.preferencia = preferencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (respuestaPreferenciaPK != null ? respuestaPreferenciaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaPreferencia)) {
            return false;
        }
        RespuestaPreferencia other = (RespuestaPreferencia) object;
        if ((this.respuestaPreferenciaPK == null && other.respuestaPreferenciaPK != null) || (this.respuestaPreferenciaPK != null && !this.respuestaPreferenciaPK.equals(other.respuestaPreferenciaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.RespuestaPreferencia[ respuestaPreferenciaPK=" + respuestaPreferenciaPK + " ]";
    }
    
}
