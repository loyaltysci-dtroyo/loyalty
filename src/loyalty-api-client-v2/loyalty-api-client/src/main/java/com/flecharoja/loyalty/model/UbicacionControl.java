/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "UBICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UbicacionControl.findAll", query = "SELECT u FROM UbicacionControl u"),
    @NamedQuery(name = "UbicacionControl.countAll", query = "SELECT COUNT(u.idUbicacion) FROM UbicacionControl u")
})
public class UbicacionControl implements Serializable {

    public enum Direccion{
        ESTADO("UE"),
        CIUDAD("UC");
        
        private final String value;
        private static final Map<String,Direccion> lookup = new HashMap<>();

        private Direccion(String value) {
            this.value = value;
        }
        static{
            for(Direccion direccion : values()){
                lookup.put(direccion.value, direccion);
            }
        }

        public String getValue() {
            return value;
        }
    }
    
    public enum Efectividad{
        PERMANENTE('P'),
        CALENDARIZADO('C'),
        RECURRENTE('R');
        
        private final char value;
        private static final Map<Character,Efectividad> lookup =  new HashMap<>();

        private Efectividad(char value) {
            this.value = value;
        }
        
        static{
            for(Efectividad efectividad : values()){
                lookup.put(efectividad.value, efectividad);
            }
        }

        public char getValue() {
            return value;
        }
        public static Efectividad get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Estados{
        DRAFT('D'),
        PUBLICADO('P'),
        ARCHIVADO('A');
        
        private final char value;
        private static final Map<Character,Estados> lookup= new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static{
            for(Estados estado : values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        public static Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
   

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "ubicacion_uuid")
    @GenericGenerator(name = "ubicacion_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_UBICACION")
    private String idUbicacion;

    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;

    @Column(name = "IND_CALENDARIZACION")
    private Character indCalendarizacion;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;

    @Size(max = 7)
    @Column(name = "IND_DIA_RECURRENCIA")
    private String indDiaRecurrencia;

    @Size(max = 4)
    @Column(name = "IND_SEMANA_RECURRENCIA")
    private Integer indSemanaRecurrencia;
    
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    public UbicacionControl() {
    }

    public String getIdUbicacion() {
        return idUbicacion;
    }

    public void setIdUbicacion(String idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null?null: Character.toUpperCase(indEstado);
    }

    public Character getIndCalendarizacion() {
        return indCalendarizacion;
    }

    public void setIndCalendarizacion(Character indCalendarizacion) {
        this.indCalendarizacion = indCalendarizacion == null ? null : Character.toUpperCase(indCalendarizacion);
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getIndDiaRecurrencia() {
        return indDiaRecurrencia;
    }

    public void setIndDiaRecurrencia(String indDiaRecurrencia) {
        this.indDiaRecurrencia = indDiaRecurrencia;
    }

    public Integer getIndSemanaRecurrencia() {
        return indSemanaRecurrencia;
    }

    public void setIndSemanaRecurrencia(Integer indSemanaRecurrencia) {
        this.indSemanaRecurrencia = indSemanaRecurrencia;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUbicacion != null ? idUbicacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UbicacionControl)) {
            return false;
        }
        UbicacionControl other = (UbicacionControl) object;
        if ((this.idUbicacion == null && other.idUbicacion != null) || (this.idUbicacion != null && !this.idUbicacion.equals(other.idUbicacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Ubicacion[ idUbicacion=" + idUbicacion + " ]";
    }
 
}
