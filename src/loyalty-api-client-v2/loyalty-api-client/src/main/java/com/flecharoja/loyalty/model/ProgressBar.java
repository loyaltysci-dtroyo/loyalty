package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author wtencio
 */
public class ProgressBar {
    
    @ApiModelProperty(value = "Cantidad de métrica que ha ganado",required = true)
    private Double progreso;
    @ApiModelProperty(value = "Cantidad de máxima de métrica para estar en máximo nivel",required = true)
    private Double tope;
    @ApiModelProperty(value = "Información de la métrica de la cual se obtiene el progreso",required = true)
    private Metrica metrica;
    @ApiModelProperty(value = "Lista de niveles que posee la métrica",required = true)
    private List<NivelMetrica> niveles;
    @ApiModelProperty(value = "Información del nivel actual en que se encuentra un miembro",required = true)
    private NivelMetrica nivelActual;

    public ProgressBar(Double progreso, Metrica metrica, List<NivelMetrica> niveles) {
        this.progreso = progreso;
        this.metrica = metrica;
        this.niveles = niveles;
    }

    public ProgressBar() {
    }

    public Double getProgreso() {
        return progreso;
    }

    public void setProgreso(Double progreso) {
        this.progreso = progreso;
    }

    public Double getTope() {
        return tope;
    }

    public void setTope(Double tope) {
        this.tope = tope;
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }

    public List<NivelMetrica> getNiveles() {
        return niveles;
    }

    public void setNiveles(List<NivelMetrica> niveles) {
        this.niveles = niveles;
    }

    public NivelMetrica getNivelActual() {
        return nivelActual;
    }

    public void setNivelActual(NivelMetrica nivelActual) {
        this.nivelActual = nivelActual;
    }

}
