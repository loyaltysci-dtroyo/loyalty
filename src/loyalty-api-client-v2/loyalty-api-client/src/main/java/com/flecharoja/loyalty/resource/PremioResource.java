package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.CategoriaPremio;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.service.PremioBean;
import com.flecharoja.loyalty.util.Indicadores;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "Premios")
@Path("premio")
public class PremioResource {

    @EJB
    PremioBean premioBean;

    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;

    /**
     * Método que obtiene un listado de categorias de premios
     *
     * @param cantidad de registros maximos por pagina
     * @param registro por el cual se inicia la busqueda
     * @return lista de categorias de premios
     */

    @GET
    @Path("categoria")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CategoriaPremio> getCategoriasPremio(
            @ApiParam(value = "Cantidad de registros")
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return premioBean.getCategoriasPremio(request.getLocale());
    }

    /**
     * Método que obtiene un listado de premios pertenecientes a una categoria
     *
     * @param idCategoria identificador de la categoria
     * @return lista de premios
     */
//    @ApiOperation(value = "Obtener los premios que pertenecen a una categoria especifica",
//            responseContainer = "List",
//            response = PremioMin.class,
//            responseHeaders = {
//                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
//                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
//            })
//
//    @ApiResponses(value = {
//        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
//        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
//    })
    @GET
    @Path("categoria/{idCategoria}/premios")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Premio> getPremiosPorCategoria(
            @ApiParam(value = "Identificador de la categoria", required = true)
            @PathParam("idCategoria") String idCategoria) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return premioBean.getPremiosPorCategoria(idCategoria, idMiembro, request.getLocale());
    }

   

    /**
     * Método que obtiene un listado de premios disponibles para el miembro
     *
     * @return lista de premios
     */
    @ApiOperation(value = "Obtener los premios disponibles en los que el miembro en sesion es elegible",
            responseContainer = "List",
            response = Premio.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })

    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Premio> getPremios() {
        String idMiembro;
        try {
           idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return premioBean.getPremios(idMiembro, request.getLocale());
    }
     /**
     * Método que obtiene un listado de premios redimidos por el miembro
     *
     * @return lista de premios
     */
    @ApiOperation(value = "Obtener los premios redimidos por el miembro en sesión",
            responseContainer = "List",
            response = Premio.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })

    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("redimidos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Premio> getPremiosRedimidos() {
        String idMiembro;
        try {
           idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return premioBean.getPremiosRedimidos(idMiembro, request.getLocale());
    }
    
     /**
     * Método que obtiene un listado de premios expirados para el miembro
     *
     * @return lista de premios
     */
    @ApiOperation(value = "Obtener los premios expirados para miembro en sesión",
            responseContainer = "List",
            response = Premio.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })

    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("expirados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Premio> getPremiosExpirados() {
        String idMiembro;
        try {
           idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return premioBean.getPremiosExpirados(idMiembro, request.getLocale());
    }
    

    /**
     * Método que permite redimir un premio
     *
     * @param idPremio identificador del premio
     * @return informacion del premio
     */
    @ApiOperation(value = "Redimir un premio")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Path("{idPremio}/redimir")
    @Produces(MediaType.APPLICATION_JSON)
    public String redimirPremio(
            @ApiParam(value = "Identificador del premio", required = true)
            @PathParam("idPremio") String idPremio) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return premioBean.redimirPremio(idPremio, idMiembro, request.getLocale());
    }
}
