package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaMision {
    
    public enum EstadosRespuesta {
        GANADO("G"),
        FALLIDO("F"),
        PENDIENTE("P");
        
        private final String value;
        private static final Map<String, EstadosRespuesta> lookup = new HashMap<>();

        private EstadosRespuesta(String value) {
            this.value = value;
        }
        
        static {
            for (EstadosRespuesta estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public String getValue() {
            return value;
        }
        
        public static EstadosRespuesta get (String value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    @ApiModelProperty(value = "Lista de respuestas para una encuesta")
    private List<RespuestaMisionEncuestaPregunta> respuestaEncuesta;
    
    @ApiModelProperty(value = "Lista de respuestas para una encuesta de preferencias")
    private List<RespuestaMisionPreferencia> respuestaPreferencias;
    
    @ApiModelProperty(value = "Lista de respuestas a actualizaciones de atributos del miembro")
    private List<RespuestaMisionPerfilAtributo> respuestaActualizarPerfil;
    
    @ApiModelProperty(value = "Respuesta a la misión subir contenido")
    private RespuestaMisionSubirContenido respuestaSubirContenido;
    
    @ApiModelProperty(value = "Respuesta a la misión de red social")
    private RespuestaMisionRedSocial respuestaRedSocial;
    
    @ApiModelProperty(value = "Respuesta a la misión ver contenido")
    private RespuestaMisionVerContenido respuestaVerContenido;
    
    @ApiModelProperty(value = "Respuesta a la misión de juego")
    private RespuestaMisionJuego respuestaJuego;
    
    @ApiModelProperty(value = "Id opcional de workflow")
    private String idWorkflow;

    public RespuestaMision() {
    }

    public List<RespuestaMisionEncuestaPregunta> getRespuestaEncuesta() {
        return respuestaEncuesta;
    }

    public void setRespuestaEncuesta(List<RespuestaMisionEncuestaPregunta> respuestaEncuesta) {
        this.respuestaEncuesta = respuestaEncuesta;
    }

    public List<RespuestaMisionPreferencia> getRespuestaPreferencias() {
        return respuestaPreferencias;
    }

    public void setRespuestaPreferencias(List<RespuestaMisionPreferencia> respuestaPreferencias) {
        this.respuestaPreferencias = respuestaPreferencias;
    }

    public List<RespuestaMisionPerfilAtributo> getRespuestaActualizarPerfil() {
        return respuestaActualizarPerfil;
    }

    public void setRespuestaActualizarPerfil(List<RespuestaMisionPerfilAtributo> respuestaActualizarPerfil) {
        this.respuestaActualizarPerfil = respuestaActualizarPerfil;
    }

    public RespuestaMisionSubirContenido getRespuestaSubirContenido() {
        return respuestaSubirContenido;
    }

    public void setRespuestaSubirContenido(RespuestaMisionSubirContenido respuestaSubirContenido) {
        this.respuestaSubirContenido = respuestaSubirContenido;
    }

    public RespuestaMisionRedSocial getRespuestaRedSocial() {
        return respuestaRedSocial;
    }

    public void setRespuestaRedSocial(RespuestaMisionRedSocial respuestaRedSocial) {
        this.respuestaRedSocial = respuestaRedSocial;
    }

    public RespuestaMisionVerContenido getRespuestaVerContenido() {
        return respuestaVerContenido;
    }

    public void setRespuestaVerContenido(RespuestaMisionVerContenido respuestaVerContenido) {
        this.respuestaVerContenido = respuestaVerContenido;
    }

    public RespuestaMisionJuego getRespuestaJuego() {
        return respuestaJuego;
    }

    public void setRespuestaJuego(RespuestaMisionJuego respuestaJuego) {
        this.respuestaJuego = respuestaJuego;
    }
    
    public String getIdWorkflow() {
        return idWorkflow;
}

    public void setIdWorkflow(String idWorkflow) {
        this.idWorkflow = idWorkflow;
    }
    
}
