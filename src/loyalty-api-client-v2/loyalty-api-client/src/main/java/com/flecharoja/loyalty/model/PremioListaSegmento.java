/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO_LISTA_SEGMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PremioListaSegmento.countByIdPremioAndSegmentosInListaAndByIndTipo", query = "SELECT COUNT(s) FROM PremioListaSegmento s WHERE s.premioListaSegmentoPK.idPremio = :idPremio AND s.premioListaSegmentoPK.idSegmento IN :lista AND s.indTipo = :indTipo"),
    @NamedQuery(name = "PremioListaSegmento.countByIdPremioAndByIndTipo", query = "SELECT COUNT(s) FROM PremioListaSegmento s WHERE s.premioListaSegmentoPK.idPremio = :idPremio AND s.indTipo = :indTipo")

})
public class PremioListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PremioListaSegmentoPK premioListaSegmentoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    public PremioListaSegmento() {
    }

    public PremioListaSegmentoPK getPremioListaSegmentoPK() {
        return premioListaSegmentoPK;
    }

    public void setPremioListaSegmentoPK(PremioListaSegmentoPK premioListaSegmentoPK) {
        this.premioListaSegmentoPK = premioListaSegmentoPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (premioListaSegmentoPK != null ? premioListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PremioListaSegmento)) {
            return false;
        }
        PremioListaSegmento other = (PremioListaSegmento) object;
        if ((this.premioListaSegmentoPK == null && other.premioListaSegmentoPK != null) || (this.premioListaSegmentoPK != null && !this.premioListaSegmentoPK.equals(other.premioListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.PremioListaSegmento[ premioListaSegmentoPK=" + premioListaSegmentoPK + " ]";
    }
    
}
