package com.flecharoja.loyalty.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faguilar
 */
@XmlRootElement
public class SecuenciaMision {
    private boolean isCompletada;
    private MisionDetails mision;

    public SecuenciaMision() {
    }

    public SecuenciaMision(boolean isCompletada, MisionDetails mision) {
        this.isCompletada = isCompletada;
        this.mision = mision;
    }

    public boolean isIsCompletada() {
        return isCompletada;
    }

    public void setIsCompletada(boolean isCompletada) {
        this.isCompletada = isCompletada;
    }

    public MisionDetails getMision() {
        return mision;
    }

    public void setMision(MisionDetails mision) {
        this.mision = mision;
    }
}
