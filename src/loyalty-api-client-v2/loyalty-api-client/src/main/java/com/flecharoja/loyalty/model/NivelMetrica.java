package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NIVEL_METRICA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NivelMetrica.findAll", query = "SELECT n FROM NivelMetrica n WHERE n.grupoNiveles = :grupo ORDER BY n.metricaInicial ASC")
})
public class NivelMetrica implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_NIVEL")
    @ApiModelProperty(value = "Identificador del nivel de métrica",required = true)
    private String idNivel;
    
    @Column(name = "NOMBRE")
    @ApiModelProperty(value = "Nombre del nivel de métrica",required = true)
    private String nombre;
    
    @Column(name = "DESCRIPCION")
    @ApiModelProperty(value = "Descripción del nivel de métrica")
    private String descripcion;
    
    @Column(name = "METRICA_INICIAL")
    @ApiModelProperty(value = "Cantidad de métrica inicial que tiene el nivel de métrica",required = true)
    private Double metricaInicial;

    @Column(name = "GRUPO_NIVELES")
    @ApiModelProperty(value = "Identificador del grupo de niveles al que pertenece el nivel de métrica",required = true)
    private String grupoNiveles;  
    
    @Column(name = "MULTIPLICADOR")
    private Double multiplicador;
    
  
    public NivelMetrica() {
    }

    public String getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(String idNivel) {
        this.idNivel = idNivel;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getMetricaInicial() {
        return metricaInicial;
    }

    public void setMetricaInicial(Double metricaInicial) {
        this.metricaInicial = metricaInicial;
    }
    
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getGrupoNiveles() {
        return grupoNiveles;
    }

    public void setGrupoNiveles(String grupoNiveles) {
        this.grupoNiveles = grupoNiveles;
    }

    public Double getMultiplicador() {
        return multiplicador;
    }

    public void setMultiplicador(Double multiplicador) {
        this.multiplicador = multiplicador;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivel != null ? idNivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NivelMetrica)) {
            return false;
        }
        NivelMetrica other = (NivelMetrica) object;
        if ((this.idNivel == null && other.idNivel != null) || (this.idNivel != null && !this.idNivel.equals(other.idNivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Nivel[ idNivel=" + idNivel + " ]";
    }
    
}
