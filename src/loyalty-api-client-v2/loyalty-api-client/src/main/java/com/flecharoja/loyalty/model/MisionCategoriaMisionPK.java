package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author svargas
 */
@Embeddable
public class MisionCategoriaMisionPK implements Serializable {

    @Column(name = "ID_CATEGORIA")
    private String idCategoria;
    
    @Column(name = "ID_MISION")
    private String idMision;

    public MisionCategoriaMisionPK() {
    }

    public MisionCategoriaMisionPK(String idCategoria, String idMision) {
        this.idCategoria = idCategoria;
        this.idMision = idMision;
    }

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCategoria != null ? idCategoria.hashCode() : 0);
        hash += (idMision != null ? idMision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionCategoriaMisionPK)) {
            return false;
        }
        MisionCategoriaMisionPK other = (MisionCategoriaMisionPK) object;
        if ((this.idCategoria == null && other.idCategoria != null) || (this.idCategoria != null && !this.idCategoria.equals(other.idCategoria))) {
            return false;
        }
        if ((this.idMision == null && other.idMision != null) || (this.idMision != null && !this.idMision.equals(other.idMision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionCategoriaMisionPK[ idCategoria=" + idCategoria + ", idMision=" + idMision + " ]";
    }
    
}
