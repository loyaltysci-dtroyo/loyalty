/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "GRUPO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grupo.findAll", query = "SELECT g FROM Grupo g"),
    @NamedQuery(name = "Grupo.findByIdGrupo", query = "SELECT g FROM Grupo g WHERE g.idGrupo = :idGrupo")

})
public class Grupo implements Serializable {
    
    public enum Visibilidad{
        VISIBLE('V'),
        INVISIBLE('I');
        
        private final char value;
        private static final Map<Character,Visibilidad> lookup = new HashMap<>();

        private Visibilidad(char value) {
            this.value = value;
        }
        
        static {
            for(Visibilidad visibilidad : values()){
                lookup.put(visibilidad.value, visibilidad);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Visibilidad get (Character value){
            return value==null?null:lookup.get(value);
        }
        
    }

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_GRUPO")
    @ApiModelProperty(value = "Identificador único y autogenerado de un grupo de miembros",required = true)
    private String idGrupo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
     @ApiModelProperty(value = "Nombre del grupo de miembros",required = true)
    private String nombre;
    
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
     @ApiModelProperty(value = "Descripción básica del grupo de miembros")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "AVATAR")
     @ApiModelProperty(value = "Imagen representativa del grupo de miembros",required = true)
    private String avatar;
   
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_VISIBLE")
     @ApiModelProperty(value = "Indicador de visibilidad del grupo de miembros para los miembros que lo componen",required = true)
    private Character indVisible;
    
    @Transient
    private BigInteger acumulado;

    public Grupo() {
    }

    public Grupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }

    
    public String getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public BigInteger getAcumulado() {
        return acumulado;
    }

    public void setAcumulado(BigInteger acumulado) {
        this.acumulado = acumulado;
    }

    public Character getIndVisible() {
        return indVisible;
    }

    public void setIndVisible(Character indVisible) {
        this.indVisible = indVisible;
    }
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGrupo != null ? idGrupo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupo)) {
            return false;
        }
        Grupo other = (Grupo) object;
        if ((this.idGrupo == null && other.idGrupo != null) || (this.idGrupo != null && !this.idGrupo.equals(other.idGrupo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Grupo[ idGrupo=" + idGrupo + " ]";
    }
    
}
