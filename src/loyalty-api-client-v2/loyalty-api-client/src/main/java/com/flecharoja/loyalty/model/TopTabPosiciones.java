/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author wtencio
 */
public class TopTabPosiciones {

    @ApiModelProperty(value = "Imagen representativa del miembro que se encuentra en el top de una tabla", required = true)
    private String avatar;
    @ApiModelProperty(value = "Nombre del miembro que se encuentra en el top de una tabla de posiciones", required = true)
    private String nombre;
    @ApiModelProperty(value = "Acumulado del miembro", required = true)
    private Double acumulado;

    public TopTabPosiciones(String avatar,String nombre, Double acumulado) {
        this.avatar = avatar;
        this.nombre = nombre;
        this.acumulado = acumulado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getAcumulado() {
        return acumulado;
    }

    public void setAcumulado(Double acumulado) {
        this.acumulado = acumulado;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    
    
}
