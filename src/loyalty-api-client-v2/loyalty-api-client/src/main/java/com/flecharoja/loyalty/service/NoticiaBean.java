package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.MarcadorNoticia;
import com.flecharoja.loyalty.model.MarcadorNoticiaPK;
import com.flecharoja.loyalty.model.NewNoticia;
import com.flecharoja.loyalty.model.NewNoticiaComentario;
import com.flecharoja.loyalty.model.Noticia;
import com.flecharoja.loyalty.model.NoticiaCategoriaNoticia;
import com.flecharoja.loyalty.model.NoticiaComentario;
import com.flecharoja.loyalty.model.Usuario;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import com.flecharoja.loyalty.util.RespuestaRegistroEntidad;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author svargas
 */
@Stateless
public class NoticiaBean {
    
    @EJB
    private SegmentoBean segmentoBean;
    
    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    public List<Noticia> getNoticias(String idMiembro, String idCategoria, Locale locale) {
        List<Noticia> noticias;
        if (idCategoria==null || idCategoria.isEmpty()) {
            noticias = em.createNamedQuery("Noticia.findByIndEstado").setParameter("indEstado", Noticia.Estados.PUBLICADO.getValue()).getResultList();
        } else {
            noticias = em.createNamedQuery("NoticiaCategoriaNoticia.findByIdCategoria").setParameter("idCategoria", idCategoria).setParameter("indEstado", Noticia.Estados.PUBLICADO.getValue()).getResultList();
        }
        
        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);
        
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            return noticias.stream()
                    .filter((noticia) -> checkEligibilidadNoticia(noticia.getIdNoticia(), segmentosPertenecientes))
                    .map((noticia) -> {
                        noticia.setCantComentarios(noticia.getComentarios().stream().filter((n) -> n.getComentarioReferente()==null).count());
                        noticia.setCantMarcas(noticia.getMarcas().stream().count());
                        noticia.setMarcado(em.find(MarcadorNoticia.class, new MarcadorNoticiaPK(noticia.getIdNoticia(), idMiembro))!=null);

                        if (noticia.getIndAutorAdmin()!=null && noticia.getIndAutorAdmin()) {
                            try {
                                Usuario admin = em.find(Usuario.class, noticia.getIdAutor());
                                noticia.setNombreAutor(admin.getNombrePublico());
                                noticia.setAvatarAutor(admin.getAvatar());
                            } catch (Exception e) {
                                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                            }
                        } else {
                            try {
                                noticia.setNombreAutor(keycloakUtils.getUsernameMember(noticia.getIdAutor()));
                                noticia.setAvatarAutor((String) em.createNamedQuery("Miembro.getAvatarFromMiembro").setParameter("idMiembro", noticia.getIdAutor()).getSingleResult());
                            } catch (Exception e) {
                                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                            }
                        }

                        return noticia;
                    })
                    .collect(Collectors.toList());
        }
    }
    
    public RespuestaRegistroEntidad insertNoticia(NewNoticia noticia, String idMiembro, Locale locale) {
        if (noticia==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "data_invalid");
        }
        
        Noticia newNoticia = new Noticia();
        newNoticia.setContenido(noticia.getContenido());
        Date fecha = new Date();
        newNoticia.setFechaCreacion(fecha);
        newNoticia.setFechaModificacion(fecha);
        newNoticia.setFechaPublicacion(fecha);
        newNoticia.setIdAutor(idMiembro);
        
        if (noticia.getImagen() == null || noticia.getImagen().trim().isEmpty()) {
            newNoticia.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                newNoticia.setImagen(filesUtils.uploadImage(noticia.getImagen(), MyAwsS3Utils.Folder.ARTE_NOTICIA, null));
            } catch (Exception e) {
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }
        
        newNoticia.setIndAutorAdmin(false);
        newNoticia.setIndEstado(Noticia.Estados.PUBLICADO.getValue());
        newNoticia.setNumVersion(1l);
        newNoticia.setTitulo(noticia.getTitulo());
        
        try {
            em.persist(newNoticia);
            em.flush();
            if (noticia.getIdCategoria()!=null && !noticia.getIdCategoria().isEmpty()) {
                em.persist(new NoticiaCategoriaNoticia(noticia.getIdCategoria(), newNoticia.getIdNoticia(), fecha, "null"));
            }
        } catch (Exception e) {
            if (!noticia.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(noticia.getImagen());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "argument_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t)-> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "unknown_error");
            }
        }
        
        return new RespuestaRegistroEntidad(newNoticia.getIdNoticia());
    }
    
    public void editNoticia(NewNoticia noticia, String idNoticia, String idMiembro, Locale locale) {
        if (noticia==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "data_invalid");
        }
        
        Noticia original = em.find(Noticia.class, idNoticia);
        if (original == null || original.getIndEstado().equals(Noticia.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "news_not_available");
        }
        
        if ((original.getIndAutorAdmin()!=null && original.getIndAutorAdmin()) || !original.getIdAutor().equals(idMiembro)) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "news_not_owned");
        }
        
        String imagenOriginal = original.getImagen();
        String imagenNueva = null;
        if (noticia.getImagen() == null || noticia.getImagen().trim().isEmpty()) {
            imagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            original.setImagen(imagenNueva);
        } else {
            if (!imagenOriginal.equals(noticia.getImagen())) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    imagenNueva = filesUtils.uploadImage(noticia.getImagen(), MyAwsS3Utils.Folder.ARTE_NOTICIA, null);
                    original.setImagen(imagenNueva);
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }
        
        original.setContenido(noticia.getContenido());
        original.setTitulo(noticia.getTitulo());
        
        original.setFechaModificacion(new Date());
        
        try {
            em.merge(original);
            em.flush();
        } catch (Exception e) {
            if (imagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(imagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "argument_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t)-> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "unknown_error");
            }
        }
        
        if (imagenNueva != null && !imagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(imagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }
    
    public void removeNoticia(String idNoticia, String idMiembro, Locale locale) {
        Noticia original = em.find(Noticia.class, idNoticia);
        if (original == null || original.getIndEstado().equals(Noticia.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "");
        }
        
        if ((original.getIndAutorAdmin()!=null && original.getIndAutorAdmin()) || !original.getIdAutor().equals(idMiembro)) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "");
        }
        
        original.setFechaModificacion(new Date());
        original.setIndEstado(Noticia.Estados.ARCHIVADO.getValue());
        
        em.merge(original);
    }
    
    public List<NoticiaComentario> getComentarios(String idNoticia, String idComentario, String idMiembro, Locale locale, long sinceDate, long untilDate) {
        Noticia noticia = em.find(Noticia.class, idNoticia);
        if (noticia==null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "referenced_entity_not_found", "noticia");
        }
        
        if (!checkEligibilidadNoticia(noticia.getIdNoticia(), segmentoBean.getSegmentosPertenecientesMiembro(idMiembro))) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "news_not_available");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<NoticiaComentario> query = cb.createQuery(NoticiaComentario.class);
        Root<NoticiaComentario> root = query.from(NoticiaComentario.class);
        
        if (idComentario==null) {
            query.where(cb.and(cb.equal(root.get("noticia"), noticia), cb.isNull(root.get("comentarioReferente"))));
        } else {
            NoticiaComentario comentarioReferente = em.find(NoticiaComentario.class, idComentario);
            if (comentarioReferente==null) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "referenced_entity_not_found", "comentario");
            }
            query.where(cb.and(cb.equal(root.get("noticia"), noticia), cb.equal(root.get("comentarioReferente"), comentarioReferente)));
        }
        
        //Se define el limite de registros elegibles segun la definicion de algunos de los atributos de fecha y el orden de estos
        if (sinceDate>0) {
            //registros nuevos de forma ascendente (pagina siguiente)
            query.where(query.getRestriction(), cb.greaterThan(root.<Date>get("fechaCreacion"), new Date(sinceDate))).orderBy(cb.asc(root.get("fechaCreacion")));
        } else {
            if (untilDate>0) {
                //registros anteriores
                query.where(query.getRestriction(), cb.lessThan(root.<Date>get("fechaCreacion"), new Date(untilDate)));
            }
            //si no se definio ninguna fecha, se trata de la pagina inicial (los registros a partir del mas reciente)
            //registros de forma descente para pagina anterior e inicial
            query.orderBy(cb.desc(root.get("fechaCreacion")));
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List<NoticiaComentario> resultado = em.createQuery(query.select(root))
                .setMaxResults(10)
                .getResultList();
        
        if (!resultado.isEmpty()) {
            NoticiaComentario ultimoComentario = resultado.get(resultado.size()-1);
            List<NoticiaComentario> resultList = em.createQuery(query.where(cb.and(cb.equal(root.get("fechaCreacion"), ultimoComentario.getFechaCreacion()), cb.not(root.in(resultado.stream().filter((t) -> t.getFechaCreacion().equals(ultimoComentario.getFechaCreacion())).collect(Collectors.toList())))))).getResultList();
            resultado.addAll(resultList);

            try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
                resultado.stream().forEach((t) -> {
                    t.setCantComentarios(t.getComentarios().stream().count());

                    if (t.getIndAutorAdmin()!=null && t.getIndAutorAdmin()) {
                        try {
                            Usuario admin = em.find(Usuario.class, t.getIdAutor());
                            t.setNombreAutor(admin.getNombrePublico());
                            t.setAvatarAutor(admin.getAvatar());
                        } catch (Exception e) {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                        }
                    } else {
                        try {
                            t.setNombreAutor(keycloakUtils.getUsernameMember(t.getIdAutor()));
                            t.setAvatarAutor((String) em.createNamedQuery("Miembro.getAvatarFromMiembro").setParameter("idMiembro", t.getIdAutor()).getSingleResult());
                        } catch (Exception e) {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                        }
                    }
                });
            }
            
            //invertir en caso de lista inicial de comentarios
            if (sinceDate==0 && untilDate==0) {
                resultado.sort((o1, o2) -> o1.getFechaCreacion().compareTo(o2.getFechaCreacion()));
            }
        }

        return resultado;
    }
    
    public RespuestaRegistroEntidad insertComentario(NewNoticiaComentario comentario, String idNoticia, String idComentarioReferente, String idMiembro, Locale locale) {
        if (comentario==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "data_invalid");
        }
        
        Noticia noticia = em.find(Noticia.class, idNoticia);
        if (noticia==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "news_not_available");
        }
        
        if (!checkEligibilidadNoticia(noticia.getIdNoticia(), segmentoBean.getSegmentosPertenecientesMiembro(idMiembro))) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "news_not_available");
        }
        
        NoticiaComentario newComentario = new NoticiaComentario();
        newComentario.setContenido(comentario.getContenido());
        Date fecha = new Date();
        newComentario.setFechaCreacion(fecha);
        newComentario.setFechaModificacion(fecha);
        newComentario.setIdAutor(idMiembro);
        if (idComentarioReferente!=null) {
            NoticiaComentario comentarioReferente = em.find(NoticiaComentario.class, idComentarioReferente);
            if (comentarioReferente==null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "comment_not_available");
            }
            newComentario.setComentarioReferente(comentarioReferente);
        }
        newComentario.setNoticia(noticia);
        newComentario.setNumVersion(1l);
        
        em.persist(newComentario);
        em.flush();
        
        return new RespuestaRegistroEntidad(newComentario.getIdComentario());
    }
    
    public void editComentario(NewNoticiaComentario comentario, String idNoticia, String idComentario, String idMiembro, Locale locale) {
        if (!checkEligibilidadNoticia(idNoticia, segmentoBean.getSegmentosPertenecientesMiembro(idMiembro))) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "news_not_available");
        }
        
        if (comentario==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "data_invalid");
        }
        
        NoticiaComentario original = em.find(NoticiaComentario.class, idComentario);
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "comment_not_available");
        }
        
        if ((original.getIndAutorAdmin()!=null && original.getIndAutorAdmin()) || !original.getIdAutor().equals(idMiembro)) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "comment_not_owned");
        }
        
        if (!original.getNoticia().getIdNoticia().equals(idNoticia)) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "comment_not_from_this_news");
        }
        
        //si el comentario tiene mas de 5 minutos de haberse creado, no puede ser editado
        if ((System.currentTimeMillis()-original.getFechaCreacion().getTime())>300000) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "comment_not_longer_editable");
        }
        
        original.setContenido(comentario.getContenido());
        
        original.setFechaModificacion(new Date());
        
        try {
            em.merge(original);
            em.flush();
        } catch (Exception e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "argument_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t)-> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "unknown_error");
            }
        }
    }
    
    public void removeComentario(String idNoticia, String idComentario, String idMiembro, Locale locale) {
        if (!checkEligibilidadNoticia(idNoticia, segmentoBean.getSegmentosPertenecientesMiembro(idMiembro))) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "news_not_available");
        }
        
        NoticiaComentario comentario = em.find(NoticiaComentario.class, idComentario);
        if (comentario == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "comment_not_available");
        }
        
        if ((comentario.getIndAutorAdmin()!=null && comentario.getIndAutorAdmin()) || !comentario.getIdAutor().equals(idMiembro)) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "comment_not_owned");
        }
        
        if (!comentario.getNoticia().getIdNoticia().equals(idNoticia)) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "comment_not_from_this_news");
        }
        
        em.remove(comentario);
    }
    
    public void marcarNoticia(String idNoticia, String idMiembro, Locale locale) {
        if (!checkEligibilidadNoticia(idNoticia, segmentoBean.getSegmentosPertenecientesMiembro(idMiembro))) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "news_not_available");
        }
        
        MarcadorNoticia marcadorNoticia = em.find(MarcadorNoticia.class, new MarcadorNoticiaPK(idNoticia, idMiembro));
        if (marcadorNoticia==null) {
            marcadorNoticia = new MarcadorNoticia(idNoticia, idMiembro);
            em.persist(marcadorNoticia);
        } else {
            em.remove(marcadorNoticia);
        }
    }
    
    /**
     * filtrado por inclusion/exclusion de miembro
     */
    private boolean checkEligibilidadNoticia(String idNoticia, List<String> segmentosPertenecientes) {
        if (!segmentosPertenecientes.isEmpty()) {
            List<List<String>> partitionsSegmentos;
            partitionsSegmentos = Lists.partition(segmentosPertenecientes, 999);
            if (partitionsSegmentos.stream()
                    .anyMatch((l) -> ((long) em.createNamedQuery("NoticiaListaSegmento.countByIdNoticiaAndSegmentosInListaAndByIndTipo")
                    .setParameter("idNoticia", idNoticia)
                    .setParameter("lista", l)
                    .setParameter("indTipo", Indicadores.EXCLUIDO)
                    .getSingleResult() > 0))) {
                return false;
            }
            if (partitionsSegmentos.stream()
                    .anyMatch((l) -> ((long) em.createNamedQuery("NoticiaListaSegmento.countByIdNoticiaAndSegmentosInListaAndByIndTipo")
                    .setParameter("idNoticia", idNoticia)
                    .setParameter("lista", l)
                    .setParameter("indTipo", Indicadores.INCLUIDO)
                    .getSingleResult() > 0))) {
                return true;
            }
        }
        return (long) em.createNamedQuery("NoticiaListaSegmento.countByIdNoticiaAndByIndTipo")
                .setParameter("idNoticia", idNoticia)
                .setParameter("indTipo", Indicadores.INCLUIDO)
                .getSingleResult() == 0;
    }
}
