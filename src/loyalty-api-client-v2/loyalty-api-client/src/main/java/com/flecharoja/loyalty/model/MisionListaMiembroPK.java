package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author svargas
 */
@Embeddable
public class MisionListaMiembroPK implements Serializable {

    @Column(name = "ID_MIEMBRO")
    private String idMiembro;
    
    @Column(name = "ID_MISION")
    private String idMision;

    public MisionListaMiembroPK() {
    }

    public MisionListaMiembroPK(String idMiembro, String idMision) {
        this.idMiembro = idMiembro;
        this.idMision = idMision;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        hash += (idMision != null ? idMision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionListaMiembroPK)) {
            return false;
        }
        MisionListaMiembroPK other = (MisionListaMiembroPK) object;
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        if ((this.idMision == null && other.idMision != null) || (this.idMision != null && !this.idMision.equals(other.idMision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionListaMiembroPK[ idMiembro=" + idMiembro + ", idMision=" + idMision + " ]";
    }
    
}
