package com.flecharoja.loyalty.model;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faguilar
 */
@XmlRootElement
public class SecuenciaMisiones {
    
    private String titulo;
    private String idWorkflow;
    private List<SecuenciaMision> misiones;

    public SecuenciaMisiones(String titulo, List<SecuenciaMision> misiones,String idWorkflow) {
        this.idWorkflow = idWorkflow;
        this.titulo = titulo;
        this.misiones = misiones;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<SecuenciaMision> getMisiones() {
        return misiones;
    }

    public void setMisiones(List<SecuenciaMision> misiones) {
        this.misiones = misiones;
    }

    public String getIdWorkflow() {
        return idWorkflow;
    }

    public void setIdWorkflow(String idWorkflow) {
        this.idWorkflow = idWorkflow;
    }
    
}
