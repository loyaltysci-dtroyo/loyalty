package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "BALANCE_METRICA_MENSUAL")
@XmlRootElement
public class BalanceMetricaMensual implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected BalanceMetricaMensualPK balanceMetricaMensualPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_ACUMULADO")
    private Double totalAcumulado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "VENCIDO")
    private Double vencido;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "REDIMIDO")
    private Double redimido;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Metrica metrica;

    public BalanceMetricaMensual() {
    }
    
    public BalanceMetricaMensual(Integer cantAno, Integer cantMes, String idMetrica) {
        this.balanceMetricaMensualPK = new BalanceMetricaMensualPK(cantAno, cantMes, idMetrica);
        this.totalAcumulado = Double.valueOf(0);
        this.vencido = Double.valueOf(0);
        this.redimido = Double.valueOf(0);
        this.numVersion = 1L;
    }

    @XmlTransient
    @ApiModelProperty(hidden = true)
    public BalanceMetricaMensualPK getBalanceMetricaMensualPK() {
        return balanceMetricaMensualPK;
    }

    public void setBalanceMetricaMensualPK(BalanceMetricaMensualPK balanceMetricaMensualPK) {
        this.balanceMetricaMensualPK = balanceMetricaMensualPK;
    }

    public Double getTotalAcumulado() {
        return totalAcumulado;
    }

    public void setTotalAcumulado(Double totalAcumulado) {
        this.totalAcumulado = totalAcumulado;
    }

    public Double getVencido() {
        return vencido;
    }

    public void setVencido(Double vencido) {
        this.vencido = vencido;
    }

    public Double getRedimido() {
        return redimido;
    }

    public void setRedimido(Double redimido) {
        this.redimido = redimido;
    }

    @XmlTransient
    @ApiModelProperty(hidden = true)
    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (balanceMetricaMensualPK != null ? balanceMetricaMensualPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BalanceMetricaMensual)) {
            return false;
        }
        BalanceMetricaMensual other = (BalanceMetricaMensual) object;
        if ((this.balanceMetricaMensualPK == null && other.balanceMetricaMensualPK != null) || (this.balanceMetricaMensualPK != null && !this.balanceMetricaMensualPK.equals(other.balanceMetricaMensualPK))) {
            return false;
        }
        return true;
    }
}
