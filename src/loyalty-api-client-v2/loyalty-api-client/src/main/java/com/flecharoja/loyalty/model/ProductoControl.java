package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoControl.findAll", query = "SELECT p FROM ProductoControl p"),
//    @NamedQuery(name = "ProductoControl.countByNombreInterno", query = "SELECT COUNT(p.idProducto) FROM ProductoControl p WHERE p.nombreInterno = :nombreInterno"),
    @NamedQuery(name = "ProductoControl.countAll", query = "SELECT COUNT(p.idProducto) FROM ProductoControl p"),
})
public class ProductoControl implements Serializable {
   
    public enum Estados {
        PUBLICADO('P'),
        ARCHIVADO('A'),
        BORRADOR('B');
        
        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();
        
        private Estados(char value){
            this.value = value;
        }
        
        static {
            for(Estados estado : values()){
                lookup.put(estado.value, estado);
            }
        }
        
        public char getValue(){
            return value;
        }
        
        public static Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum IntervalosTiempoRespuesta {
        POR_SIEMPRE('A'),
        POR_MINUTO('B'),
        POR_HORA('C'),
        POR_DIA('D'),
        POR_SEMANA('E'),
        POR_MES('F');
        
        private final char value;
        private static final Map<Character, IntervalosTiempoRespuesta> lookup = new HashMap<>();
        
        private IntervalosTiempoRespuesta(char value){
            this.value = value;
        }
        
        static {
            for (IntervalosTiempoRespuesta tipo : values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static IntervalosTiempoRespuesta get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
     public enum Efectividad{
        PERMANENTE('P'),
        CALENDARIZADO('C'),
        RECURRENTE('R');
        
        private final char value;
        private static final Map<Character,Efectividad> lookup = new HashMap<>();

        private Efectividad(char value) {
            this.value = value;
        }
        
        static{
            for(Efectividad efectividad : values()){
                lookup.put(efectividad.value, efectividad);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Efectividad get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_PRODUCTO")
    private String idProducto;
       
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "EFECTIVIDAD_IND_CALENDARIZADO")
    private Character efectividadIndCalendarizado;
    
    @Column(name = "EFECTIVIDAD_FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date efectividadFechaInicio;
    
    @Column(name = "EFECTIVIDAD_FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date efectividadFechaFin;
    
    @Size(max = 7)
    @Column(name = "EFECTIVIDAD_IND_DIAS")
    private String efectividadIndDias;
    
    @Column(name = "EFECTIVIDAD_IND_SEMANA")
    private Integer efectividadIndSemana;  
    
    @Column(name = "LIMITE_CANT_TOTAL")
    private Long limiteCantTotal;
    
    @Column(name = "LIMITE_IND_INTERVALO_TOTAL")
    private Character limiteIndIntervaloTotal;
    
    @Column(name = "LIMITE_CANT_TOTAL_MIEMBRO")
    private Long limiteCantTotalMiembro;
    
    @Column(name = "LIMITE_IND_INTERVALO_MIEMBRO")
    private Character limiteIndIntervaloMiembro;
    
    @Column(name = "LIMITE_IND_INTERVALO_RESPUESTA")
    private Character limiteIndIntervaloRespuesta;
    
    @NotNull
    @Column(name = "LIMITE_IND_RESPUESTA")
    private Boolean limiteIndRespuesta;
    
    @Column(name = "LIMITE_CANT_INTER_RESPUESTA")
    private Long limiteCantInterRespuesta;
    
    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;
        

    public ProductoControl() {
    }

    
    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }

    public Character getEfectividadIndCalendarizado() {
        return efectividadIndCalendarizado;
    }

    public void setEfectividadIndCalendarizado(Character efectividadIndCalendarizado) {
        this.efectividadIndCalendarizado = efectividadIndCalendarizado == null ? null : Character.toUpperCase(efectividadIndCalendarizado);
    }

    public Date getEfectividadFechaInicio() {
        return efectividadFechaInicio;
    }

    public void setEfectividadFechaInicio(Date efectividadFechaInicio) {
        this.efectividadFechaInicio = efectividadFechaInicio;
    }

    public Date getEfectividadFechaFin() {
        return efectividadFechaFin;
    }

    public void setEfectividadFechaFin(Date efectividadFechaFin) {
        this.efectividadFechaFin = efectividadFechaFin;
    }

    public String getEfectividadIndDias() {
        return efectividadIndDias;
    }

    public void setEfectividadIndDias(String efectividadIndDias) {
        this.efectividadIndDias = efectividadIndDias;
    }

    public Integer getEfectividadIndSemana() {
        return efectividadIndSemana;
    }

    public void setEfectividadIndSemana(Integer efectividadIndSemana) {
        this.efectividadIndSemana = efectividadIndSemana;
    }

    public Boolean getLimiteIndRespuesta() {
        return limiteIndRespuesta;
    }

    public void setLimiteIndRespuesta(Boolean limiteIndRespuesta) {
        this.limiteIndRespuesta = limiteIndRespuesta;
    }

    public Long getLimiteCantTotal() {
        return limiteCantTotal;
    }

    public void setLimiteCantTotal(Long limiteCantTotal) {
        this.limiteCantTotal = limiteCantTotal;
    }

    public Character getLimiteIndIntervaloTotal() {
        return limiteIndIntervaloTotal;
    }

    public void setLimiteIndIntervaloTotal(Character limiteIndIntervaloTotal) {
        this.limiteIndIntervaloTotal = limiteIndIntervaloTotal == null ? null : Character.toUpperCase(limiteIndIntervaloTotal);
    }

    public Long getLimiteCantTotalMiembro() {
        return limiteCantTotalMiembro;
    }

    public void setLimiteCantTotalMiembro(Long limiteCantTotalMiembro) {
        this.limiteCantTotalMiembro = limiteCantTotalMiembro;
    }

    public Character getLimiteIndIntervaloMiembro() {
        return limiteIndIntervaloMiembro;
    }

    public void setLimiteIndIntervaloMiembro(Character limiteIndIntervaloMiembro) {
        this.limiteIndIntervaloMiembro = limiteIndIntervaloMiembro == null ? null : Character.toUpperCase(limiteIndIntervaloMiembro);
    }

    public Character getLimiteIndIntervaloRespuesta() {
        return limiteIndIntervaloRespuesta;
    }

    public void setLimiteIndIntervaloRespuesta(Character limiteIndIntervaloRespuesta) {
        this.limiteIndIntervaloRespuesta = limiteIndIntervaloRespuesta == null ? null : Character.toUpperCase(limiteIndIntervaloRespuesta);
    }  
    
    
    public Long getLimiteCantInterRespuesta() {
        return limiteCantInterRespuesta;
    }

    public void setLimiteCantInterRespuesta(Long limiteCantInterRespuesta) {
        this.limiteCantInterRespuesta = limiteCantInterRespuesta;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoControl)) {
            return false;
        }
        ProductoControl other = (ProductoControl) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }
    

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Producto[ idProducto=" + idProducto + " ]";
    }

            
}
