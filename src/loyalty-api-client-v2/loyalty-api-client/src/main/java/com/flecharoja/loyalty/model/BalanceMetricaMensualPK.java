package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author svargas
 */
@Embeddable
public class BalanceMetricaMensualPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_ANO")
    private Integer numAno;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_MES")
    private Integer numMes;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_METRICA")
    private String idMetrica;

    public BalanceMetricaMensualPK() {
    }

    public BalanceMetricaMensualPK(Integer numAno, Integer numMes, String idMetrica) {
        this.numAno = numAno;
        this.numMes = numMes;
        this.idMetrica = idMetrica;
    }

    public Integer getNumAno() {
        return numAno;
    }

    public void setNumAno(Integer numAno) {
        this.numAno = numAno;
    }

    public Integer getNumMes() {
        return numMes;
    }

    public void setNumMes(Integer numMes) {
        this.numMes = numMes;
    }

    public String getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numAno != null ? numAno.hashCode() : 0);
        hash += (numMes != null ? numMes.hashCode() : 0);
        hash += (idMetrica != null ? idMetrica.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BalanceMetricaMensualPK)) {
            return false;
        }
        BalanceMetricaMensualPK other = (BalanceMetricaMensualPK) object;
        if ((this.numAno == null && other.numAno != null) || (this.numAno != null && !this.numAno.equals(other.numAno))) {
            return false;
        }
        if ((this.numMes == null && other.numMes != null) || (this.numMes != null && !this.numMes.equals(other.numMes))) {
            return false;
        }
        if ((this.idMetrica == null && other.idMetrica != null) || (this.idMetrica != null && !this.idMetrica.equals(other.idMetrica))) {
            return false;
        }
        return true;
    }
}
