/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "TABLA_POSICIONES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TablaPosiciones.findAll", query = "SELECT t FROM TablaPosiciones t"),
    @NamedQuery(name = "TablaPosiciones.findByIdTabla", query = "SELECT t FROM TablaPosiciones t WHERE t.idTabla = :idTabla"),
    @NamedQuery(name = "TablaPosiciones.findByIdMetrica", query = "SELECT t FROM TablaPosiciones t WHERE t.idMetrica.idMetrica = :idMetrica"),
    @NamedQuery(name = "TablaPosiciones.findGeneral", query = "SELECT t FROM TablaPosiciones t WHERE t.idMetrica.idMetrica = :idMetrica AND t.indTipo = :indTipo")
})
public class TablaPosiciones implements Serializable {
    
    public enum Calculo{
        SUMA('S'),
        PROMEDIO('P');
        
        private final char value;
        private static final Map<Character,Calculo> lookup = new HashMap<>();

        private Calculo(char value) {
            this.value = value;
        }
        
        static{
            for(Calculo calculo :values()){
                lookup.put(calculo.value, calculo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Calculo get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Tipo{
        MIEMBROS('U'),
        GRUPO('G'),
        GRUPO_ESPECIFICO('E');
        
        private final char value;
        private static final Map<Character, Tipo> lookup = new HashMap<>();

        private Tipo(char value) {
            this.value = value;
        }
        
        static{
            for(Tipo tipo:values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipo get(Character value){
            return value==null?null:lookup.get(value);
        }
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tablaPosiciones")
    private List<TabposMiembro> tabposMiembroList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tablaPosiciones")
    private List<TabposGrupomiembro> tabposGrupomiembroList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_TABLA")
    @ApiModelProperty(value = "Identificador de la tabla de posiciones", required = true)
    private String idTabla;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    @ApiModelProperty(value = "Nombre de la tabla de posiciones", required = true)
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    @ApiModelProperty(value = "Indicador del tipo de tabla de posiciones ",example = "Miembro, grupo o grupo especifico", required = true)
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "IND_FORMATO")
    @ApiModelProperty(value = "Indicador del formato de la tabla de posiciones",example = "Iniciales, nombre completo", required = true)
    private String indFormato;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "DESCRIPCION")
    @ApiModelProperty(value = "Descripción de la tabla de posiciones", required = true)
    private String descripcion;
    
    @Column(name = "IND_GRUPAL_FORMA_CALCULO")
    @ApiModelProperty(value = "Indicador de la forma de cálculo si la tabla es de tipo grupo", example = "Suma o promedio")
    private Character indGrupalFormaCalculo;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    @ApiModelProperty(value = "Imagen representativa de la tabla de posiciones", required = true)
    private String imagen;
    
    @JoinColumn(name = "ID_GRUPO", referencedColumnName = "ID_GRUPO")
    @ManyToOne
    @ApiModelProperty(value = "Identificador el grupo de miembros, si la tabla es de tipo grupo")
    private Grupo idGrupo;
    
    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    @ApiModelProperty(value = "Información de la métrica a la cual pertenece la tabla de posiciones", required = true)
    private Metrica idMetrica;
    
    @Transient
    private List<TopTabPosiciones> top;

    public TablaPosiciones() {
    }

    public TablaPosiciones(String idTabla) {
        this.idTabla = idTabla;
    }

    public String getIdTabla() {
        return idTabla;
    }

    public void setIdTabla(String idTabla) {
        this.idTabla = idTabla;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getIndFormato() {
        return indFormato;
    }

    public void setIndFormato(String indFormato) {
        this.indFormato = indFormato;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Character getIndGrupalFormaCalculo() {
        return indGrupalFormaCalculo;
    }

    public void setIndGrupalFormaCalculo(Character indGrupalFormaCalculo) {
        this.indGrupalFormaCalculo = indGrupalFormaCalculo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Grupo getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Grupo idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTabla != null ? idTabla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TablaPosiciones)) {
            return false;
        }
        TablaPosiciones other = (TablaPosiciones) object;
        if ((this.idTabla == null && other.idTabla != null) || (this.idTabla != null && !this.idTabla.equals(other.idTabla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.TablaPosiciones[ idTabla=" + idTabla + " ]";
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<TabposMiembro> getTabposMiembroList() {
        return tabposMiembroList;
    }

    public void setTabposMiembroList(List<TabposMiembro> tabposMiembroList) {
        this.tabposMiembroList = tabposMiembroList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<TabposGrupomiembro> getTabposGrupomiembroList() {
        return tabposGrupomiembroList;
    }

    public void setTabposGrupomiembroList(List<TabposGrupomiembro> tabposGrupomiembroList) {
        this.tabposGrupomiembroList = tabposGrupomiembroList;
    }

    public List<TopTabPosiciones> getTop() {
        return top;
    }

    public void setTop(List<TopTabPosiciones> top) {
        this.top = top;
    }
    
    
}
