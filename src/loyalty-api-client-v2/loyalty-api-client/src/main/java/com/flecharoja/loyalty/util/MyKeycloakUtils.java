package com.flecharoja.loyalty.util;

import com.flecharoja.loyalty.exception.MyException;
import java.io.StringReader;
import java.util.Locale;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author svargas, faguilar
 */
public class MyKeycloakUtils implements AutoCloseable{
    
    private final String CLIENT_SECRET = "26301e54-8492-42dd-8bc8-59faa43b2ba9";
    private final String CLIENT_ID = "loyalty-api";
    private final String ADMIN_URL = "https://idp-demos.loyaltysci.com/auth/admin/realms/loyalty";
    private final String TOKEN_ENDPOINT = "https://idp-demos.loyaltysci.com/auth/realms/loyalty/protocol/openid-connect/token";
    private final String LOGOUT_ENDPOINT = "https://idp-demos.loyaltysci.com/auth/realms/loyalty/protocol/openid-connect/logout";
    
    private final JsonObject keycloakSession;

    Locale locale;
    public MyKeycloakUtils(Locale locale) {
        this.locale = locale;
        Client client = ClientBuilder.newClient();
        try {
            String body = "client_id="+CLIENT_ID
                    +"&client_secret="+CLIENT_SECRET
                    +"&grant_type=client_credentials";
            Response response = client.target(TOKEN_ENDPOINT).request().post(Entity.entity(body, MediaType.APPLICATION_FORM_URLENCODED));
            if (response.getStatus()!=200) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }

            this.keycloakSession = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();
        } finally {
            client.close();
        }
    }
    
    public String createMember(String username, String password,String email) {
        if (username.trim().isEmpty() || password.trim().isEmpty()) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "invalid_username_password");
        }
        Client client = ClientBuilder.newClient();
        try {
            String body = Json.createObjectBuilder()
                    .add("username", username.trim())
                    .add("email", email)
                    .add("enabled", true)
                    .add("credentials", Json.createArrayBuilder()
                            .add(Json.createObjectBuilder()
                                    .add("type", "password")
                                    .add("temporary", false)
                                    .add("value", password.trim())))
                    .build().toString();
            Response response = client.target(ADMIN_URL+"/users").request().header("Authorization", "bearer "+keycloakSession.getString("access_token")).post(Entity.entity(body, MediaType.APPLICATION_JSON));
            if (response.getStatus()==409 || response.getStatus()==400) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "invalid_username_password");
            }
            if (response.getStatus()!=201) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
            String[] segments = response.getHeaderString("Location").split("/");
            return segments[segments.length-1];
        } finally {
            client.close();
        }
    }
    
    public void deleteMember(String id) {
        Client client = ClientBuilder.newClient();
        try {
            Response response = client.target(ADMIN_URL+"/users/"+id).request().header("Authorization", "bearer "+keycloakSession.getString("access_token")).delete();
            if (response.getStatus()!=204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }
    
    public void enableDisableMember(String id, boolean action) {
        Client client = ClientBuilder.newClient();
        try {
            String body = Json.createObjectBuilder()
                    .add("id", id)
                    .add("enabled", action)
                    .build().toString();
            Response response = client.target(ADMIN_URL+"/users/"+id).request().header("Authorization", "bearer "+keycloakSession.getString("access_token")).put(Entity.entity(body, MediaType.APPLICATION_JSON));
            if (response.getStatus()!=204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }
    
    public String getUsernameMember(String id) {
        Client client = ClientBuilder.newClient();
        try {
            Response response = client.target(ADMIN_URL+"/users/"+id).request().header("Authorization", "bearer "+keycloakSession.getString("access_token")).get();
            if (response.getStatus()!=200) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
            JsonObject data = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();
            return data.getString("username");
        } finally {
            client.close();
        }
    }
    
    public String getIdMember(String username) {
        Client client = ClientBuilder.newClient();
        try {
            Response response = client.target(ADMIN_URL+"/users").queryParam("username", username).request().header("Authorization", "bearer "+keycloakSession.getString("access_token")).get();
            String idMember = null;
            if (response.getStatus()==200) {
                for (JsonValue jsonValue : Json.createReader(new StringReader(response.readEntity(String.class))).readArray()) {
                    JsonObject data = (JsonObject) jsonValue;
                    if (data.getString("username").equalsIgnoreCase(username)) {
                        idMember = data.getString("id");
                    }
                }
            }
            return idMember;
        } finally {
            client.close();
        }
    }
    
    public void resetPasswordMember(String id, String password) {
        if (password.trim().isEmpty()) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "invalid_username_password");
        }
        Client client = ClientBuilder.newClient();
        try {
            String body = Json.createObjectBuilder()
                    .add("type", "password")
                    .add("temporary", false)
                    .add("value", password.trim())
                    .build().toString();
            Response response = client.target(ADMIN_URL + "/users/" + id+"/reset-password").request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).put(Entity.entity(body, MediaType.APPLICATION_JSON));
            if (response.getStatus() != 204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }
    
    public void editEmail(String id, String email) {
        Client client = ClientBuilder.newClient();
        try {
            String body = Json.createObjectBuilder()
                    .add("id", id)
                    .add("email", email)
                    .build().toString();
            Response response = client.target(ADMIN_URL + "/users/" + id).request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).put(Entity.entity(body, MediaType.APPLICATION_JSON));
            if (response.getStatus() != 204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }

    @Override
    public void close() {
        Client client = ClientBuilder.newClient();
        try {
            String body = "client_id="+CLIENT_ID
                    +"&client_secret="+CLIENT_SECRET
                    +"&refresh_token="+keycloakSession.getString("refresh_token");
            Response response = client.target(LOGOUT_ENDPOINT).request().post(Entity.entity(body, MediaType.APPLICATION_FORM_URLENCODED));
            if (response.getStatus()!=204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }
}
