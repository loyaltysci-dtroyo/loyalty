package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION")
@NamedQueries(
        @NamedQuery(name = "MisionControl.findAllByIndEstado", query = "SELECT m FROM MisionControl m WHERE m.indEstado = :indEstado")
)
public class MisionControl implements Serializable {
    
    public enum Estados {
        PUBLICADO('A'),
        ARCHIVADO('B'),
        BORRADOR('C');
        
        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public char getValue() {
            return value;
        }
        
        public static Estados get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum TiposMision {
        ENCUESTA('E'),
        PERFIL('P'),
        VER_CONTENIDO('V'),
        SUBIR_CONTENIDO('S'),
        RED_SOCIAL('R'),
        JUEGO('J'),
        REALIDAD_AUMENTADA('B'),
        PREFERENCIAS('A');
        
        private final char value;
        private static final Map<Character, TiposMision> lookup = new HashMap<>();

        private TiposMision(char value) {
            this.value = value;
        }
        
        static {
            for (TiposMision tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }
        
        public char getValue() {
            return value;
        }
        
        public static TiposMision get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum IntervalosTiempoRespuesta {
        POR_SIEMPRE('A'),
        POR_MINUTO('B'),
        POR_HORA('C'),
        POR_DIA('D'),
        POR_SEMANA('E'),
        POR_MES('F');
        
        private final char value;
        private static final Map<Character, IntervalosTiempoRespuesta> lookup = new HashMap<>();

        private IntervalosTiempoRespuesta(char value) {
            this.value = value;
        }
        
        static {
            for (IntervalosTiempoRespuesta tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }
        
        public char getValue() {
            return value;
        }
        
        public static IntervalosTiempoRespuesta get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum TiposAprobacion {
        AUTOMATICO('A'),
        MANUAL('M'),;
        
        private final char value;
        private static final Map<Character, TiposAprobacion> lookup = new HashMap<>();

        private TiposAprobacion(char value) {
            this.value = value;
        }
        
        static {
            for (TiposAprobacion estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public char getValue() {
            return value;
        }
        
        public static TiposAprobacion get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum IndicadoresCalendarizacion {
        PERMANENTE('P'),
        CALENDARIZADO('C');
        
        private final char value;

        private IndicadoresCalendarizacion(char value) {
            this.value = value;
        }

        public char getValue() {
            return value;
        }
    }

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_MISION")
    private String idMision;

    @Column(name = "IND_ESTADO")
    private Character indEstado;

    @Column(name = "CANT_METRICA")
    private Double cantMetrica;

    @Column(name = "IND_APROVACION")
    private Character indAprovacion;

    @Column(name = "IND_TIPO_MISION")
    private Character indTipoMision;

    @Column(name = "IND_RESPUESTA")
    private Boolean indRespuesta;

    @Column(name = "CANT_TOTAL")
    private Long cantTotal;

    @Column(name = "IND_INTERVALO_TOTAL")
    private Character indIntervaloTotal;

    @Column(name = "CANT_TOTAL_MIEMBRO")
    private Long cantTotalMiembro;

    @Column(name = "IND_INTERVALO_MIMEBRO")
    private Character indIntervaloMimebro;
    
    @Column(name = "CANT_INTERVALO")
    private Long cantIntervalo;

    @Column(name = "IND_INTERVALO")
    private Character indIntervalo;

    @Column(name = "ID_METRICA")
    private String idMetrica;
    
    @Column(name = "IND_CALENDARIZACION")
    private Character indCalendarizacion;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    
    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;

    @Column(name = "IND_DIA_RECURRENCIA")
    private String indDiaRecurrencia;

    @Column(name = "IND_SEMANA_RECURRENCIA")
    private Integer indSemanaRecurrencia;
    
    @Column(name = "IND_DEFECTO_NINGUNO")
    private Boolean indDefectoNinguno;

    public MisionControl() {
    }

    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    public Double getCantMetrica() {
        return cantMetrica;
    }

    public void setCantMetrica(Double cantMetrica) {
        this.cantMetrica = cantMetrica;
    }

    public Character getIndAprovacion() {
        return indAprovacion;
    }

    public void setIndAprovacion(Character indAprovacion) {
        this.indAprovacion = indAprovacion;
    }

    public Character getIndTipoMision() {
        return indTipoMision;
    }

    public void setIndTipoMision(Character indTipoMision) {
        this.indTipoMision = indTipoMision;
    }

    public Boolean getIndRespuesta() {
        return indRespuesta;
    }

    public void setIndRespuesta(Boolean indRespuesta) {
        this.indRespuesta = indRespuesta;
    }

    public Long getCantTotal() {
        return cantTotal;
    }

    public void setCantTotal(Long cantTotal) {
        this.cantTotal = cantTotal;
    }

    public Character getIndIntervaloTotal() {
        return indIntervaloTotal;
    }

    public void setIndIntervaloTotal(Character indIntervaloTotal) {
        this.indIntervaloTotal = indIntervaloTotal;
    }

    public Long getCantTotalMiembro() {
        return cantTotalMiembro;
    }

    public void setCantTotalMiembro(Long cantTotalMiembro) {
        this.cantTotalMiembro = cantTotalMiembro;
    }

    public Character getIndIntervaloMimebro() {
        return indIntervaloMimebro;
    }

    public void setIndIntervaloMimebro(Character indIntervaloMimebro) {
        this.indIntervaloMimebro = indIntervaloMimebro;
    }

    public Long getCantIntervalo() {
        return cantIntervalo;
    }

    public void setCantIntervalo(Long cantIntervalo) {
        this.cantIntervalo = cantIntervalo;
    }

    public Character getIndIntervalo() {
        return indIntervalo;
    }

    public void setIndIntervalo(Character indIntervalo) {
        this.indIntervalo = indIntervalo;
    }

    public String getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    public Character getIndCalendarizacion() {
        return indCalendarizacion;
    }

    public void setIndCalendarizacion(Character indCalendarizacion) {
        this.indCalendarizacion = indCalendarizacion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getIndDiaRecurrencia() {
        return indDiaRecurrencia;
    }

    public void setIndDiaRecurrencia(String indDiaRecurrencia) {
        this.indDiaRecurrencia = indDiaRecurrencia;
    }

    public Integer getIndSemanaRecurrencia() {
        return indSemanaRecurrencia;
    }

    public void setIndSemanaRecurrencia(Integer indSemanaRecurrencia) {
        this.indSemanaRecurrencia = indSemanaRecurrencia;
    }

    public Boolean getIndDefectoNinguno() {
        return indDefectoNinguno;
    }

    public void setIndDefectoNinguno(Boolean indDefectoNinguno) {
        this.indDefectoNinguno = indDefectoNinguno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMision != null ? idMision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionControl)) {
            return false;
        }
        MisionControl other = (MisionControl) object;
        if ((this.idMision == null && other.idMision != null) || (this.idMision != null && !this.idMision.equals(other.idMision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Mision[ idMision=" + idMision + " ]";
    }

}
