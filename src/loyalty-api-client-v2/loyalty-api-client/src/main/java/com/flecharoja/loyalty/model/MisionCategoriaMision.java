package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_CATEGORIA_MISION")
@NamedQueries(
        @NamedQuery(name = "MisionCategoriaMision.findMisionesByIdCategoria", query = "SELECT m.mision FROM MisionCategoriaMision m WHERE  m.misionCategoriaMisionPK.idCategoria = :idCategoria")
)
public class MisionCategoriaMision implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MisionCategoriaMisionPK misionCategoriaMisionPK;
    
    @JoinColumn(name = "ID_MISION", referencedColumnName = "ID_MISION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MisionControl mision;

    public MisionCategoriaMision() {
    }

    public MisionCategoriaMisionPK getMisionCategoriaMisionPK() {
        return misionCategoriaMisionPK;
    }

    public void setMisionCategoriaMisionPK(MisionCategoriaMisionPK misionCategoriaMisionPK) {
        this.misionCategoriaMisionPK = misionCategoriaMisionPK;
    }

    public MisionControl getMision() {
        return mision;
    }

    public void setMision(MisionControl mision) {
        this.mision = mision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (misionCategoriaMisionPK != null ? misionCategoriaMisionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionCategoriaMision)) {
            return false;
        }
        MisionCategoriaMision other = (MisionCategoriaMision) object;
        if ((this.misionCategoriaMisionPK == null && other.misionCategoriaMisionPK != null) || (this.misionCategoriaMisionPK != null && !this.misionCategoriaMisionPK.equals(other.misionCategoriaMisionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionCategoriaMision[ misionCategoriaMisionPK=" + misionCategoriaMisionPK + " ]";
    }
    
}
