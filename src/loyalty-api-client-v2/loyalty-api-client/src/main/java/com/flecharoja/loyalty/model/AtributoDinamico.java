package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio, svargas
 */
@Entity
@Table(name = "ATRIBUTO_DINAMICO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AtributoDinamico.findAll", query = "SELECT a FROM AtributoDinamico a"),
    @NamedQuery(name = "AtributoDinamico.findByIdAtributo", query = "SELECT a FROM AtributoDinamico a WHERE a.idAtributo = :idAtributo"),
    @NamedQuery(name = "AtributoDinamico.findActivosVisibles", query = "SELECT a FROM AtributoDinamico a WHERE a.indEstado = :indEstado AND a.indVisible = 1")

})
public class AtributoDinamico implements Serializable {

    public enum Estados {
        BORRADOR('D'),
        PUBLICADO('P'),
        ARCHIVADO('A');
        
        private final char value;
        private static final Map<Character,Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static{
            for(Estados estado: values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        public static final Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum TiposDato {
        FECHA('F'),
        TEXTO('T'),
        NUMERICO('N'),
        BOOLEANO('B');
        
        private final char value;
        private static final Map<Character,TiposDato> lookup = new HashMap<>();

        private TiposDato(char value) {
            this.value = value;
        }
        
        static{
            for(TiposDato estado: values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        public static final TiposDato get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Atributos {
        ID_ATRIBUTO("idAtributo"),
        VALOR("valorAtributoMiembro");
        
        private final String value;
        private static final Map<String,Atributos> lookup = new HashMap<>();

        private Atributos(String value) {
            this.value = value;
        }
        
        static{
            for(Atributos atributo: values()){
                lookup.put(atributo.value, atributo);
            }
        }

        public String getValue() {
            return value;
        }
        public static final Atributos get(String value){
            return value==null?null:lookup.get(value);
        }
    }
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @ApiModelProperty(value = "Identificador único y autogenerado del atributo dinámico", required = true)
    @Column(name = "ID_ATRIBUTO")
    private String idAtributo;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @ApiModelProperty(value = "Nombre que se le dara al atributo dinámico",required = true)
    @Column(name = "NOMBRE")
    private String nombre;

    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    @ApiModelProperty(value = "Descripción básica del atributo dinámico")
    private String descripcion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_DATO")
    @ApiModelProperty(value = "Indicador del tipo de dato que sera el atributo dinámico", example = "Texto, númerico, booleano, fecha...",required = true)
    private Character indTipoDato;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_VISIBLE")
    @ApiModelProperty(value = "Indicador de visibilidad del atributo hacia el miembro",required = true)
    private Boolean indVisible;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "VALOR_DEFECTO")
    @ApiModelProperty(value = "Valor por defecto que tendrá el atributo dinámico",required = true)
    private String valorDefecto;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_REQUERIDO")
    @ApiModelProperty(value = "Indica si el atributo será obligatorio",required = true)
    private Boolean indRequerido;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    @ApiModelProperty(value = "Indicador del estado del atributo dinámico", example = "Borrador, Publicado o Archivado",required = true)
    private Character indEstado;

    @Transient
    private String valorAtributoMiembro;


    public AtributoDinamico() {
    }

    public AtributoDinamico(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getIndTipoDato() {
        return indTipoDato;
    }

    public void setIndTipoDato(Character indTipoDato) {
        this.indTipoDato = indTipoDato;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Boolean getIndVisible() {
        return indVisible;
    }

    public void setIndVisible(Boolean indVisible) {
        this.indVisible = indVisible;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getValorDefecto() {
        return valorDefecto;
    }

    public void setValorDefecto(String valorDefecto) {
        this.valorDefecto = valorDefecto;
    }

    public Boolean getIndRequerido() {
        return indRequerido;
    }

    public void setIndRequerido(Boolean indRequerido) {
        this.indRequerido = indRequerido;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    public String getValorAtributoMiembro() {
        return valorAtributoMiembro;
    }

    public void setValorAtributoMiembro(String valorAtributoMiembro) {
        this.valorAtributoMiembro = valorAtributoMiembro;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtributo != null ? idAtributo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtributoDinamico)) {
            return false;
        }
        AtributoDinamico other = (AtributoDinamico) object;
        if ((this.idAtributo == null && other.idAtributo != null) || (this.idAtributo != null && !this.idAtributo.equals(other.idAtributo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AtributoDinamico{" + "idAtributo=" + idAtributo + ", nombre=" + nombre + ", descripcion=" + descripcion + ", indTipoDato=" + indTipoDato + ", indVisible=" + indVisible + ", valorDefecto=" + valorDefecto + ", indRequerido=" + indRequerido + ", indEstado=" + indEstado + ", valorAtributoMiembro=" + valorAtributoMiembro + '}';
    }

    

    
}
