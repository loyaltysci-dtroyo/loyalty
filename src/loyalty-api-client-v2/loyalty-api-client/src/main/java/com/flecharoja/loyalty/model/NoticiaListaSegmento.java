package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NOTICIA_LISTA_SEGMENTO")
@NamedQueries({
    @NamedQuery(name = "NoticiaListaSegmento.countByIdNoticiaAndSegmentosInListaAndByIndTipo", query = "SELECT COUNT(s) FROM NoticiaListaSegmento s WHERE s.noticiaListaSegmentoPK.idNoticia = :idNoticia AND s.noticiaListaSegmentoPK.idSegmento IN :lista AND s.indTipo = :indTipo"),
    @NamedQuery(name = "NoticiaListaSegmento.countByIdNoticiaAndByIndTipo", query = "SELECT COUNT(s) FROM NoticiaListaSegmento s WHERE s.noticiaListaSegmentoPK.idNoticia = :idNoticia AND s.indTipo = :indTipo")
})
public class NoticiaListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NoticiaListaSegmentoPK noticiaListaSegmentoPK;
    
    @Column(name = "IND_TIPO")
    private Character indTipo;

    public NoticiaListaSegmento() {
    }

    public NoticiaListaSegmentoPK getNoticiaListaSegmentoPK() {
        return noticiaListaSegmentoPK;
    }

    public void setNoticiaListaSegmentoPK(NoticiaListaSegmentoPK noticiaListaSegmentoPK) {
        this.noticiaListaSegmentoPK = noticiaListaSegmentoPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo!=null?Character.toUpperCase(indTipo):null;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noticiaListaSegmentoPK != null ? noticiaListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NoticiaListaSegmento)) {
            return false;
        }
        NoticiaListaSegmento other = (NoticiaListaSegmento) object;
        if ((this.noticiaListaSegmentoPK == null && other.noticiaListaSegmentoPK != null) || (this.noticiaListaSegmentoPK != null && !this.noticiaListaSegmentoPK.equals(other.noticiaListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.NoticiaListaSegmento[ noticiaListaSegmentoPK=" + noticiaListaSegmentoPK + " ]";
    }
    
}
