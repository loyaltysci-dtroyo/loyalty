/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "INSIGNIAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Insignias.findAllPublicadas", query = "SELECT i FROM Insignias i WHERE i.estado = :estado")
})
public class Insignias implements Serializable {
    
    public enum Estados{
       BORRADOR('B'),
       PUBLICADO('P'),
       ARCHIVADO('A');
       
       private final char value;
       private static final Map<Character,Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
       
       static{
           for(Estados estado : values()){
               lookup.put(estado.value, estado);
           }
       }

        public char getValue() {
            return value;
        }
       
        public static Estados get (Character value){
            return value==null?null:lookup.get(value);
        }
   }
   
   public enum Tipos{
       COLLECTOR('C'),
       STATUS('S');
       
       private final char value;
       private static final Map<Character,Tipos> lookup = new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }
       
       static{
           for(Tipos tipo : values()){
               lookup.put(tipo.value, tipo);
           }
       }

        public char getValue() {
            return value;
        }
       
        public static Tipos get(Character  value){
            return value==null?null:lookup.get(value);
        }
   }

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_INSIGNIA")
    @ApiModelProperty(value = "Identificador único y autogenerado de la insignia",required = true)
    private String idInsignia;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INSIGNIA")
    @ApiModelProperty(value = "Nombre de la insignia",required = true)
    private String nombreInsignia;
    
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    @ApiModelProperty(value = "Descripción básica de la insignia")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO")
    @ApiModelProperty(value = "Indicador del tipo de insignia", example = "Collector o Status",required = true)
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    @ApiModelProperty(value = "Imagen representativa de la insignia",required = true)
    private String imagen;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTADO")
    @ApiModelProperty(value = "Indicador de estado de la insignia", example = "Borrador, Publicado, Archivado",required = true)
    private Character estado;
    
    @Transient
    @ApiModelProperty(value = "Indicador booleano que permite saber si la insignia a sido obtenida por un miembro en particular")
    private Boolean obtenida;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInsignia" , fetch = FetchType.EAGER)
    @ApiModelProperty(value = "Lista de niveles que posee la insignia")
    private List<NivelesInsignias> niveles;

    public Insignias() {
    }

    public Insignias(String idInsignia) {
        this.idInsignia = idInsignia;
    }

  
    public String getIdInsignia() {
        return idInsignia;
    }

    public void setIdInsignia(String idInsignia) {
        this.idInsignia = idInsignia;
    }

    public String getNombreInsignia() {
        return nombreInsignia;
    }

    public void setNombreInsignia(String nombreInsignia) {
        this.nombreInsignia = nombreInsignia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }
    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

//    public List<NivelesInsignias> getNivelesInsigniasList() {
//        return niveles;
//    }
//
//    public void setNivelesInsigniasList(List<NivelesInsignias> nivelesInsigniasList) {
//        this.niveles = nivelesInsigniasList;
//    }

    public List<NivelesInsignias> getNiveles() {
        return niveles;
    }

    public void setNiveles(List<NivelesInsignias> niveles) {
        this.niveles = niveles;
    }
    
    

    public Character getEstado() {
        return estado;
    }

    public Boolean getObtenida() {
        return obtenida;
    }

    public void setObtenida(Boolean obtenida) {
        this.obtenida = obtenida;
    }
    
    

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public void setEstado(Character estado) {
        this.estado = estado;
    }   
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInsignia != null ? idInsignia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Insignias)) {
            return false;
        }
        Insignias other = (Insignias) object;
        if ((this.idInsignia == null && other.idInsignia != null) || (this.idInsignia != null && !this.idInsignia.equals(other.idInsignia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Insignias[ idInsignia=" + idInsignia + " ]";
    }
    
}
