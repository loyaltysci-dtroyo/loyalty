/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Grupo;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.TablaPosiciones;
import com.flecharoja.loyalty.model.TabposGrupomiembro;
import com.flecharoja.loyalty.model.TabposMiembro;
import com.flecharoja.loyalty.model.TopTabPosiciones;
import com.flecharoja.loyalty.util.Indicadores;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;

/**
 *
 * @author wtencio
 */
@Stateless
public class TablaPosicionesBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Método que obtiene el top 20 de la tabla de posiciones general
     *
     * @param locale
     * @return info del leaderboard con el top 20
     */
    public TablaPosiciones getGeneral(Locale locale) {
        //top maximo de 20 registros
        int topMax = 20;
        //obtengo la metrica principal del sistema
        List<String> metrica = em.createNamedQuery("ConfiguracionGeneral.findMetrica").getResultList();
        String idMetrica;
        if (!metrica.isEmpty()) {
            idMetrica = metrica.get(0);
        } else {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "main_metric_not_found");
        }
        //obtengo la informacion de la tabla de posiciones que corresponde
        List<TablaPosiciones> tablas = em.createNamedQuery("TablaPosiciones.findGeneral")
                .setParameter("idMetrica", idMetrica).setParameter("indTipo", TablaPosiciones.Tipo.MIEMBROS.getValue()).getResultList();
        if (tablas.isEmpty()) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "main_leaderboard_not_found");
        }
        TablaPosiciones tabla = tablas.get(0);
        //obtengo los miembros que hana acumulado en esa tabla ordenado de mayor a menor acumulado
        List<TabposMiembro> tabposMiembros = em.createNamedQuery("TabposMiembro.findByIdTabla")
                .setParameter("idTabla", tabla.getIdTabla()).setMaxResults(topMax).getResultList();
        List<TopTabPosiciones> top = new ArrayList<>();
        tabposMiembros.stream().forEach((tabposMiembro) -> {
            top.add(new TopTabPosiciones(tabposMiembro.getMiembro().getAvatar(), tabposMiembro.getMiembro().getNombre() + " " + tabposMiembro.getMiembro().getApellido(), tabposMiembro.getAcumulado()));
        });
        //seteo el top
        tabla.setTop(top);
        //retorno la tabla
        return tabla;
    }

    /**
     * Método que obtiene un listado de tablas de posiciones en las que aplica
     * el miembro
     *
     * @param miembro identificador del miembro
     * @param locale
     * @return listado de tablas de posiciones
     */
    public List<TablaPosiciones> getTablasPosiciones(String miembro,Locale locale) {
        return em.createNamedQuery("TabposMiembro.findByIdMiembro")
                        .setParameter("idMiembro", miembro)
                        .getResultList();
    }

    /**
     * Método que obtiene las posiciones de los miembros o grupos que conforman
     * un grupo
     *
     * @param idTabla identificador de la tabla
     * @param locale
     * @return listado de miembros o grupos
     */
    public List<TopTabPosiciones> getPosicionMiembrosOGrupos(String idTabla, Locale locale) {
        //OBTENER TODOS LOS MIEMBROS O GRUPOS QUE ESTAN EN LA TABLA ORDENADOS DE MEJOR PUNTAJE O PEOR PUNTAJE
        Map<String, Object> respuesta = new HashMap<>();
        List<TabposMiembro> tabposMiembros = new ArrayList<>();
        List<TopTabPosiciones> resultado = new ArrayList<>();
        TablaPosiciones tabla;
        int topMax = 20;

        tabla = em.find(TablaPosiciones.class, idTabla);
        if(tabla == null){
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "leaderboard_not_found");
        }
        //si la tabla es de tipo miembros, se presenta el top 20 de miembros 
        if (tabla.getIndTipo().equals(Indicadores.TABLA_POSICIONES_IND_TIPO_MIEMBROS)) {
            tabposMiembros = em.createNamedQuery("TabposMiembro.findByIdTabla").setParameter("idTabla", tabla.getIdTabla()).setMaxResults(topMax).getResultList();
            //se parsea la entidad a solo el nombre apellido y acumulado del miembro
            for (TabposMiembro tabposMiembro : tabposMiembros) {
                TopTabPosiciones ttp = new TopTabPosiciones(tabposMiembro.getMiembro().getAvatar(), tabposMiembro.getMiembro().getNombre() + " " + tabposMiembro.getMiembro().getApellido(), tabposMiembro.getAcumulado());
                resultado.add(ttp);
            }
            //si la tabla es de tipo grupos, se presenta el top 20 de grupos
        } else if (tabla.getIndTipo().equals(Indicadores.TABLA_POSICIONES_IND_TIPO_GRUPOS)) {
            //obtengo la lista de grupos de miembros con su acumulado ordenado de mayor a menor
            List<TabposGrupomiembro> tabposGrupomiembros = em.createNamedQuery("TabposGrupomiembro.findByIdTabla").setParameter("idTabla", tabla.getIdTabla()).setMaxResults(topMax).getResultList();
            //se parsea el grupo a nombre del grupo y acumulado total
            tabposGrupomiembros.stream().map(
                    (tabposGrupomiembro) -> new TopTabPosiciones(tabposGrupomiembro.getGrupo().getAvatar(), tabposGrupomiembro.getGrupo().getNombre(),
                            tabposGrupomiembro.getAcumulado())).forEach((ttp) -> {
                        resultado.add(ttp);
                    });
            //si la tabla es de tipo grupo especifico se busca el top 20 de miembros en ese grupo
        } else if (tabla.getIndTipo().equals(Indicadores.TABLA_POSICIONES_IND_TIPO_GRUPO_ESPECIFICO)) {
            //si no existe el grupo cae en una excepcion
            if (tabla.getIdGrupo() == null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "leaderboard_group_invalid");
            }
            //obtengo la info del grupo y los miembros que pertenecen a ese grupo
            Grupo grupo = em.find(Grupo.class, tabla.getIdGrupo().getIdGrupo());
            List<Miembro> miembros = em.createNamedQuery("GrupoMiembro.findByIdGrupo").setParameter("idGrupo", grupo.getIdGrupo()).getResultList();

            for (Miembro miembro : miembros) {
                //si existe un registro de acumulado para la tabla y el miembro se asigna a la lista
                if (existeMiembro(miembro.getIdMiembro(), tabla.getIdTabla())) {
                    tabposMiembros.add((TabposMiembro) em.createNamedQuery("TabposMiembro.findByIdMiembroTabla").setParameter("idMiembro", miembro.getIdMiembro()).setParameter("idTabla", tabla.getIdTabla()).getSingleResult());
                }
            }
            if (tabposMiembros == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "leaderboard_empty");
            }
            //acomodar la lista
            Collections.sort(tabposMiembros, (TabposMiembro p1, TabposMiembro p2) -> (p2.getAcumulado().intValue()-p1.getAcumulado().intValue()));
            //top maximo
            int contador = 0;
            for (TabposMiembro miembro : tabposMiembros) {
                if (contador <= topMax) {
                    TopTabPosiciones ttp = new TopTabPosiciones(miembro.getMiembro().getAvatar(), miembro.getMiembro().getNombre() + " " + miembro.getMiembro().getApellido(), miembro.getAcumulado());
                    resultado.add(ttp);
                    contador++;
                }
            }
        }
        return resultado;
    }

    /**
     * Método que verifica si un miembro existe en una tabla especifica
     *
     * @param idMiembro identificador del miembro
     * @param idTabla identificador de la tabla
     * @return true-> existe el miembro, false-> no existe el miembro
     */
    private boolean existeMiembro(String idMiembro, String idTabla) {
        return ((Long) em.createNamedQuery("TabposMiembro.countByIdMiembroTabla").setParameter("idMiembro", idMiembro).setParameter("idTabla", idTabla).getSingleResult()) > 0;
    }

}
