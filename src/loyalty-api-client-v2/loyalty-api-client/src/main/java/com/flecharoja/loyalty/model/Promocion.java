package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio, svargas
 */
@Entity
@Table(name = "PROMOCION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Promocion.findAll", query = "SELECT p FROM Promocion p"),
    @NamedQuery(name = "Promocion.countAll", query = "SELECT COUNT(p) FROM Promocion p"),
    @NamedQuery(name = "Promocion.findByIdPromocion", query = "SELECT p FROM Promocion p WHERE p.idPromocion = :idPromocion"),

})
public class Promocion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_PROMOCION")
    @ApiModelProperty(value = "Identificador único de la promoción",required = true)
    private String idPromocion;
    
    @Column(name = "IMAGEN_ARTE")
    @ApiModelProperty(value = "Imagen representativa de la promoción",required = true)
    private String imagenArte;
    
    @Column(name = "ENCABEZADO_ARTE")
    @ApiModelProperty(value = "Nombre de la promoción",required = true)
    private String encabezadoArte;
    
    @Column(name = "SUBENCABEZADO_ARTE")
    @ApiModelProperty(value = "Información adicional al nombre de la promoción")
    private String subencabezadoArte;
    
    @Column(name = "DETALLE_ARTE")
    @ApiModelProperty(value = "Descripción de la promoción",required = true)
    private String detalleArte;
    
    @Column(name = "IND_TIPO_ACCION")
    @ApiModelProperty(value = "Indicador del tipo de acción que tiene la promoción",required = true)
    private Character indTipoAccion;
    
    @Column(name = "URL_OR_CODIGO")
    @ApiModelProperty(value = "Url o código que identifica la promoción",required = true)
    private String urlOrCodigo;  
    
    @Transient
    @ApiModelProperty(value = "Indicador de marcado como favorita para un miembro especifico")
    private boolean marcada;
    
    public Promocion() {
    }

    public String getIdPromocion() {
        return idPromocion;
    }

    public void setIdPromocion(String idPromocion) {
        this.idPromocion = idPromocion;
    }

    public Character getIndTipoAccion() {
        return indTipoAccion;
    }

    public void setIndTipoAccion(Character indTipoAccion) {
        this.indTipoAccion = indTipoAccion;
    }

    public String getUrlOrCodigo() {
        return urlOrCodigo;
    }

    public void setUrlOrCodigo(String urlOrCodigo) {
        this.urlOrCodigo = urlOrCodigo;
    }

    public String getImagenArte() {
        return imagenArte;
    }

    public void setImagenArte(String imagenArte) {
        this.imagenArte = imagenArte;
    }

    public String getEncabezadoArte() {
        return encabezadoArte;
    }

    public void setEncabezadoArte(String encabezadoArte) {
        this.encabezadoArte = encabezadoArte;
    }

    public String getSubencabezadoArte() {
        return subencabezadoArte;
    }

    public void setSubencabezadoArte(String subencabezadoArte) {
        this.subencabezadoArte = subencabezadoArte;
    }

    public String getDetalleArte() {
        return detalleArte;
    }

    public void setDetalleArte(String detalleArte) {
        this.detalleArte = detalleArte;
    }

    public boolean isMarcada() {
        return marcada;
    }

    public void setMarcada(boolean marcada) {
        this.marcada = marcada;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPromocion != null ? idPromocion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Promocion)) {
            return false;
        }
        Promocion other = (Promocion) object;
        if ((this.idPromocion == null && other.idPromocion != null) || (this.idPromocion != null && !this.idPromocion.equals(other.idPromocion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Promocion[ idPromocion=" + idPromocion + " ]";
    }

    
}
