package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaMisionEncuestaPregunta {
    @ApiModelProperty(value = "Identificador de la pregunta", required = true)
    private String idPregunta;
    @ApiModelProperty(value = "Respuesta a la pregunta", required = true)
    private String respuesta;
    @ApiModelProperty(value = "Comentarios extra de la pregunta")
    private String comentarios;

    public RespuestaMisionEncuestaPregunta() {
    }

    public String getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(String idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
    
}
