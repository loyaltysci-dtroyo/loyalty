package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.AtributoDinamico;
import com.flecharoja.loyalty.model.CategoriaMision;
import com.flecharoja.loyalty.model.CodigoCertificado;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.ImagenesRompecabezas;
import com.flecharoja.loyalty.model.Juego;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.MiembroAtributo;
import com.flecharoja.loyalty.model.MiembroAtributoPK;
import com.flecharoja.loyalty.model.MisionControl;
import com.flecharoja.loyalty.model.MisionDetails;
import com.flecharoja.loyalty.model.MisionEncuestaPregunta;
import com.flecharoja.loyalty.model.MisionJuego;
import com.flecharoja.loyalty.model.MisionListaMiembro;
import com.flecharoja.loyalty.model.MisionListaMiembroPK;
import com.flecharoja.loyalty.model.MisionPerfilAtributo;
import com.flecharoja.loyalty.model.MisionSubirContenido;
import com.flecharoja.loyalty.model.Preferencia;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioControl;
import com.flecharoja.loyalty.model.PremioMiembro;
import com.flecharoja.loyalty.model.ReciboRespuestaMision;
import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.model.RegistroMetricaPK;
import com.flecharoja.loyalty.model.RespuestaMision;
import com.flecharoja.loyalty.model.RespuestaMisionEncuestaPregunta;
import com.flecharoja.loyalty.model.RespuestaMisionPerfilAtributo;
import com.flecharoja.loyalty.model.RespuestaMisionPreferencia;
import com.flecharoja.loyalty.model.RespuestaPreferencia;
import com.flecharoja.loyalty.model.RespuestaPreferenciaPK;
import com.flecharoja.loyalty.model.SecuenciaMision;
import com.flecharoja.loyalty.model.SecuenciaMisiones;
import com.flecharoja.loyalty.model.WorkflowListaMiembro;
import com.flecharoja.loyalty.model.WorkflowListaMiembroPK;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.google.common.collect.Lists;
import com.google.common.primitives.Chars;
import com.google.gson.Gson;
import com.j256.simplemagic.ContentInfo;
import com.j256.simplemagic.ContentInfoUtil;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Random;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TemporalType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.KeyOnlyFilter;
import org.apache.hadoop.hbase.filter.PrefixFilter;
import org.apache.hadoop.hbase.filter.RegexStringComparator;
import org.apache.hadoop.hbase.filter.RowFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author wtencio, svargas
 */
@Stateless
public class MisionBean {

    @EJB
    MyKafkaUtils myKafkaUtils;

    @EJB
    private SegmentoBean segmentoBean;

    @EJB
    PremioBean premioBean;

    @EJB
    private RegistroMetricaService registroMetricaService;

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    private final Configuration hbaseConf;

    public MisionBean() {
        this.hbaseConf = new Configuration();
        this.hbaseConf.addResource("conf/hbase/hbase-site.xml");
    }

    /**
     * Método que obtiene una lista de categorias de misiones existentes
     *
     * @param locale
     * @return lista de categorias de mision y su rango
     */
    public List<CategoriaMision> getCategoriasMision(Locale locale) {

        //lectura de categorias de mision en un rango de resultados
        return em.createNamedQuery("CategoriaMision.findAll")
                .getResultList();
    }

    /**
     * Método que devuelve una lista de misiones la cual pertenece a la
     * categoria que se pasa por parametro y tambien que cumpla con que el
     * miembro se encuentre entre sus elegibles
     *
     * @param idCategoria identificador de la categoria de mision
     * @param idMiembro identificador del miembro
     * @param locale
     * @return lista de misiones resultantes
     */
    public List<MisionDetails> getMisionesPorCategoria(String idCategoria, String idMiembro, Locale locale) {
        //obtencion de todas las misiones dentro de la categoria de mision
        List<MisionControl> misiones = em.createNamedQuery("MisionCategoriaMision.findMisionesByIdCategoria").setParameter("idCategoria", idCategoria).getResultList();

        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);

        //filtrado de misiones por su estado, elegibilidad por miembro e intervalo de respuesta entre respuestas de la mision
        //mapeo de control de misiones a su par que contiene informacion basica
        List<MisionDetails> resultado = misiones.stream()
                .filter((MisionControl m)
                        -> m.getIndEstado().equals(MisionControl.Estados.PUBLICADO.getValue())
                && checkEligibilidadMisionMiembro(idMiembro, m.getIdMision(), segmentosPertenecientes, m.getIndDefectoNinguno())
                && checkIntervalosRespuestaMisionMiembro(m, idMiembro)
                && checkCalendarizacionMision(m))
                .sorted((m1, m2) -> {
                    //obtencion de la fecha a comparar segun el indicador de calendarizacion
                    Date fecha1 = m1.getIndCalendarizacion() == MisionControl.IndicadoresCalendarizacion.CALENDARIZADO.getValue() ? m1.getFechaInicio() : m1.getFechaPublicacion();
                    Date fecha2 = m2.getIndCalendarizacion() == MisionControl.IndicadoresCalendarizacion.CALENDARIZADO.getValue() ? m2.getFechaInicio() : m2.getFechaPublicacion();
                    //comparacion de fecha en orden descendente
                    return fecha2.compareTo(fecha1);
                })
                .map((MisionControl m) -> {
                    MisionDetails detalle = em.find(MisionDetails.class, m.getIdMision());

                    switch (MisionControl.TiposMision.get(detalle.getIndTipoMision())) {
                        case PERFIL: {
                            Miembro miembro = em.find(Miembro.class, idMiembro);
                            for (MisionPerfilAtributo misionPerfilAtributo : detalle.getMisionPerfilAtributos()) {
                                MisionPerfilAtributo.Atributos tipoAtributo = MisionPerfilAtributo.Atributos.get(misionPerfilAtributo.getIndAtributo());
                                switch (tipoAtributo) {
                                    case ATRIBUTO_DINAMICO: {
                                        AtributoDinamico atributoDinamico = misionPerfilAtributo.getAtributoDinamico();
                                        MiembroAtributo miembroAtributo = em.find(MiembroAtributo.class, new MiembroAtributoPK(idMiembro, atributoDinamico.getIdAtributo()));
                                        if (miembroAtributo == null) {
                                            misionPerfilAtributo.setRespuesta(atributoDinamico.getValorDefecto());
                                        } else {
                                            misionPerfilAtributo.setRespuesta(miembroAtributo.getValor());
                                        }
                                        break;
                                    }
                                    case ESTADO_CIVIL: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIndEstadoCivil() == null ? null : miembro.getIndEstadoCivil().toString());
                                        break;
                                    }
                                    case GENERO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIndGenero() == null ? null : miembro.getIndGenero().toString());
                                        break;
                                    }
                                    case TIPO_DE_EDUCACION: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIndEducacion() == null ? null : miembro.getIndEducacion().toString());
                                        break;
                                    }
                                    case PAIS_DE_RESIDENCIA: {
                                        misionPerfilAtributo.setRespuesta(miembro.getPaisResidencia());
                                        break;
                                    }
                                    case FECHA_DE_NACIMIENTO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getFechaNacimiento() == null ? null : miembro.getFechaNacimiento().getTime() + "");
                                        break;
                                    }
                                    case INGRESO_ECONOMICO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIngresoEconomico() == null ? null : miembro.getIngresoEconomico().toString());
                                        break;
                                    }
                                    case HIJOS: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIndHijos() == null ? "FALSE" : miembro.getIndHijos() == true ? "TRUE" : "FALSE");
                                        break;
                                    }
                                    case CIUDAD_DE_RESIDENCIA: {
                                        misionPerfilAtributo.setRespuesta(miembro.getCiudadResidencia());
                                        break;
                                    }
                                    case CODIGO_POSTAL: {
                                        misionPerfilAtributo.setRespuesta(miembro.getCodigoPostal());
                                        break;
                                    }
                                    case ESTADO_DE_RESIDENCIA: {
                                        misionPerfilAtributo.setRespuesta(miembro.getEstadoResidencia());
                                        break;
                                    }
                                    case NOMBRE: {
                                        misionPerfilAtributo.setRespuesta(miembro.getNombre());
                                        break;
                                    }
                                    case PRIMER_APELLIDO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getApellido());
                                        break;
                                    }
                                    case SEGUNDO_APELLIDO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getApellido2());
                                        break;
                                    }
                                    case FRECUENCIA_COMPRA: {
                                        misionPerfilAtributo.setRespuesta(miembro.getFrecuenciaCompra());
                                        break;
                                    }
                                    default: {
                                        misionPerfilAtributo.setRespuesta(null);
                                    }
                                }
                            }
                            break;
                        }
                        case PREFERENCIAS: {
                            List<Preferencia> preferencias = em.createNamedQuery("MisionPreferencia.getPreferenciaByIdMision").setParameter("idMision", detalle.getIdMision()).getResultList();
                            preferencias.forEach((preferencia) -> {
                                try {
                                    preferencia.setRespuestaMiembro((String) em.createNamedQuery("RespuestaPreferencia.getRespuestaByIdMiembroIdPreferencia").setParameter("idMiembro", idMiembro).setParameter("idPreferencia", preferencia.getIdPreferencia()).getSingleResult());
                                } catch (NoResultException e) {
                                }
                            });
                            detalle.setMisionEncuestaPreferencias(preferencias);
                            break;
                        }
                        case JUEGO: {
                            Juego juego = em.find(Juego.class, detalle.getIdMision());
                            if (juego != null) {
                                List<MisionJuego> misionJuegos = em.createNamedQuery("MisionJuego.findByIdMision").setParameter("idMision", detalle.getIdMision()).getResultList();
                                if (juego.getTipo().equals(Juego.Tipo.RASPADITA.getValue())) {
                                    Juego.Estrategia estrategia = Juego.Estrategia.get(juego.getEstrategia());
                                    switch (estrategia) {
                                        case PROBABILIDAD: {
                                            MisionJuego misionGanar = this.getPremioGanadorProbabilidad(juego, idMiembro, locale);
                                            if (misionGanar == null) {
                                                boolean flag = false;
                                                for (MisionJuego misionJuego : misionJuegos) {
                                                    if (flag == false && misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                                        flag = true;
                                                        misionJuego.setIsGanar(Boolean.TRUE);
                                                    } else {
                                                        misionJuego.setIsGanar(Boolean.FALSE);
                                                    }
                                                }
                                            } else {
                                                misionJuegos.forEach((misionJuego) -> {
                                                    if (misionJuego.getIdMisionJuego().equals(misionGanar.getIdMisionJuego())) {
                                                        misionJuego.setIsGanar(Boolean.TRUE);
                                                    } else {
                                                        misionJuego.setIsGanar(Boolean.FALSE);
                                                    }
                                                });
                                            }
                                            break;
                                        }
                                        case RANDOM: {
                                            MisionJuego misionGanar = this.getPremioGanadorRandom(juego, idMiembro, locale);
                                            if (misionGanar == null) {
                                                misionJuegos.forEach((misionJuego) -> {
                                                    if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                                        misionJuego.setIsGanar(Boolean.TRUE);
                                                    } else {
                                                        misionJuego.setIsGanar(Boolean.FALSE);
                                                    }
                                                });
                                            } else {
                                                misionJuegos.forEach((misionJuego) -> {
                                                    if (misionJuego.getIdMisionJuego().equals(misionGanar.getIdMisionJuego())) {
                                                        misionJuego.setIsGanar(Boolean.TRUE);
                                                    } else {
                                                        misionJuego.setIsGanar(Boolean.FALSE);
                                                    }
                                                });
                                            }
                                            break;
                                        }
                                    }
                                } else if (juego.getTipo().equals(Juego.Tipo.ROMPECABEZAS.getValue())) {
                                    if (juego.getImagenesRompecabezas() != null) {
                                        String[] split = juego.getImagenesRompecabezas().split(",");
                                        List<ImagenesRompecabezas> lista = new ArrayList<>();
                                        for (String url : split) {
                                            lista.add(new ImagenesRompecabezas(url.trim()));
                                        }
                                        juego.setImagenes(lista);
                                    }
                                }
                                juego.setMisionJuego(misionJuegos);
                            }
                            detalle.setJuego(juego);
                            break;
                        }
                    }

                    return detalle;
                }).collect(Collectors.toList());

        return resultado;
    }

    /**
     * Retorna las secuencias de misiones para un usuario en especifico
     *
     * @param idMiembro el usuario para el cual se obtienen las secuencias
     * @param locale
     * @return secuencias de misiones para un usuario en especifico
     */
    public List<SecuenciaMisiones> getSecuenciasMisiones(String idMiembro, String indTipo, Locale locale) {
        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);
        //obtencion de todas las misiones dentro de la categoria de mision
        //List<MisionControl> misiones = em.createNamedQuery("MisionControl.findAllByIndEstado").setParameter("indEstado", MisionControl.Estados.PUBLICADO).getResultList();

        //Se obtienen todas las secuenciaciones de mision
        List<Workflow> workflows = em.createNamedQuery("Workflow.findAllByEspec").getResultList();
        //Se filtran las que sean elegibles parael usuario
        workflows=workflows.parallelStream().filter((w) -> {
            return w.isIsWorkflowMisionEspec() && checkEligibilidadWorkflow(idMiembro, w.getIdWorkflow(), segmentosPertenecientes);
        }).collect(Collectors.toList());
        //para cada secuenciacion se obtienen la lista de misiones 
        List<SecuenciaMisiones> result = workflows.parallelStream().map((w) -> {
            List<MisionControl> temp = new LinkedList<>();
            WorkflowNodo nodoActual = w.getIdSucesor();
            while (nodoActual != null) {
                temp.add(em.find(MisionControl.class, nodoActual.getReferencia()));
                nodoActual = nodoActual.getIdSucesor();
            }
            //se obtienen los detalles de mision segun el tipo
            List<MisionDetails> ldetails = temp.parallelStream().map((MisionControl m) -> {
                return getDetailsMisionSecuencia(m.getIdMision(), idMiembro, locale);
            }).collect(Collectors.toList());

            //finalmente se encapsulan las misiones y se establece el flag de mision completada
            List<SecuenciaMision> mlist = ldetails.parallelStream()
                    .map((m) -> new SecuenciaMision(isMisionCompletada(m.getIdMision(), idMiembro), m))
                    .collect(Collectors.toList());
            return new SecuenciaMisiones(w.getNombreWorkflow(), mlist,w.getIdWorkflow());
        }).collect(Collectors.toList());
        //se verifica si el miembro ya completo todas las misiones de la secuencia
        result= result.stream().filter((sm) -> {
            //primero se filtran las misiones no completadas de la secuencia
            Optional<SecuenciaMision> opt = sm.getMisiones().parallelStream().filter((s) -> {
                return !s.isIsCompletada();
            }).findAny();
            //si hay al menos una y el indicador es completadas, entonces se filtra la secuencia, de lo contrario se muestra
            return ("C".equals(indTipo)) ? !opt.isPresent() : ("D".equals(indTipo)) ? opt.isPresent() : false;
        }).collect(Collectors.toList());
        return result;
    }

    /**
     * Método que devuelve la informacion detallada de la mision
     *
     * @param idMision identificador de la misión
     * @param idMiembro identificador del miembro
     * @return informacion de la misión
     */
    public MisionDetails getMisionDetalle(String idMision, String idMiembro, Locale locale) {
        //busqueda de la mision
        MisionControl mision = em.find(MisionControl.class, idMision);
        if (mision == null) {
            //de no encontrarse se retorna una respuesta informando sobre el recurso no existente
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "challenge_not_found");
        }

        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);

        //se verifica que la mision este publicada y tenga elegibilidad para el miembro e intervalo de respuesta entre respuestas de la mision valido
        if (mision.getIndEstado().equals(MisionControl.Estados.PUBLICADO.getValue())
                && checkEligibilidadMisionMiembro(idMision, idMiembro, segmentosPertenecientes, mision.getIndDefectoNinguno())
                && checkIntervalosRespuestaMisionMiembro(mision, idMiembro)
                && checkCalendarizacionMision(mision)) {
            //se registra que el miembro vio la mision
            insertRegistroVistaMision(idMiembro, idMision);
            
            try {
                //registro de evento de vista de mision
                myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.VER_MISION, idMision, idMiembro);
            } catch (Exception ex) {
                Logger.getLogger(MisionBean.class.getName()).log(Level.WARNING, null, ex);
            }
            
            //se retorna el detalle de la mision
            return getMisionDetalleInternal(idMision, idMiembro, locale);
        }

        //si no cumplio con las condiciones anteriores se informa que el recurso no esta disponible
        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_unavailable");
    }
    
    /**
     * Obtencion de mision detalle sin verificacion alguna, uso interno
     * 
     * TODO: reducir metodo a lo requerido
     * 
     * @param idMision Id de mision requerido
     * @param idMiembro id miembro
     * @param locale locale
     * @return detalle de mision
     */
    private MisionDetails getMisionDetalleInternal(String idMision, String idMiembro, Locale locale) {
        //se retorna el detalle de la mision
        MisionDetails detalle = em.find(MisionDetails.class, idMision);

        switch (MisionControl.TiposMision.get(detalle.getIndTipoMision())) {
            case PERFIL: {
                Miembro miembro = em.find(Miembro.class, idMiembro);
                for (MisionPerfilAtributo misionPerfilAtributo : detalle.getMisionPerfilAtributos()) {
                    MisionPerfilAtributo.Atributos tipoAtributo = MisionPerfilAtributo.Atributos.get(misionPerfilAtributo.getIndAtributo());
                    switch (tipoAtributo) {
                        case ATRIBUTO_DINAMICO: {
                            AtributoDinamico atributoDinamico = misionPerfilAtributo.getAtributoDinamico();
                            MiembroAtributo miembroAtributo = em.find(MiembroAtributo.class, new MiembroAtributoPK(idMiembro, atributoDinamico.getIdAtributo()));
                            if (miembroAtributo == null) {
                                misionPerfilAtributo.setRespuesta(atributoDinamico.getValorDefecto());
                            } else {
                                misionPerfilAtributo.setRespuesta(miembroAtributo.getValor());
                            }
                            break;
                        }
                        case ESTADO_CIVIL: {
                            misionPerfilAtributo.setRespuesta(miembro.getIndEstadoCivil() == null ? null : miembro.getIndEstadoCivil().toString());
                            break;
                        }
                        case GENERO: {
                            misionPerfilAtributo.setRespuesta(miembro.getIndGenero() == null ? null : miembro.getIndGenero().toString());
                            break;
                        }
                        case TIPO_DE_EDUCACION: {
                            misionPerfilAtributo.setRespuesta(miembro.getIndEducacion() == null ? null : miembro.getIndEducacion().toString());
                            break;
                        }
                        case PAIS_DE_RESIDENCIA: {
                            misionPerfilAtributo.setRespuesta(miembro.getPaisResidencia());
                            break;
                        }
                        case FECHA_DE_NACIMIENTO: {
                            misionPerfilAtributo.setRespuesta(miembro.getFechaNacimiento() == null ? null : miembro.getFechaNacimiento().getTime() + "");
                            break;
                        }
                        case INGRESO_ECONOMICO: {
                            misionPerfilAtributo.setRespuesta(miembro.getIngresoEconomico() == null ? null : miembro.getIngresoEconomico().toString());
                            break;
                        }
                        case HIJOS: {
                            misionPerfilAtributo.setRespuesta(miembro.getIndHijos() == null ? "FALSE" : miembro.getIndHijos() == true ? "TRUE" : "FALSE");
                            break;
                        }
                        case CIUDAD_DE_RESIDENCIA: {
                            misionPerfilAtributo.setRespuesta(miembro.getCiudadResidencia());
                            break;
                        }
                        case CODIGO_POSTAL: {
                            misionPerfilAtributo.setRespuesta(miembro.getCodigoPostal());
                            break;
                        }
                        case ESTADO_DE_RESIDENCIA: {
                            misionPerfilAtributo.setRespuesta(miembro.getEstadoResidencia());
                            break;
                        }
                        case NOMBRE: {
                            misionPerfilAtributo.setRespuesta(miembro.getNombre());
                            break;
                        }
                        case PRIMER_APELLIDO: {
                            misionPerfilAtributo.setRespuesta(miembro.getApellido());
                            break;
                        }
                        case SEGUNDO_APELLIDO: {
                            misionPerfilAtributo.setRespuesta(miembro.getApellido2());
                            break;
                        }
                        case FRECUENCIA_COMPRA: {
                            misionPerfilAtributo.setRespuesta(miembro.getFrecuenciaCompra());
                            break;
                        }
                        default: {
                            misionPerfilAtributo.setRespuesta(null);
                        }
                    }
                }
                break;
            }
            case PREFERENCIAS: {
                List<Preferencia> preferencias = em.createNamedQuery("MisionPreferencia.getPreferenciaByIdMision").setParameter("idMision", detalle.getIdMision()).getResultList();
                preferencias.forEach((preferencia) -> {
                    try {
                        preferencia.setRespuestaMiembro((String) em.createNamedQuery("RespuestaPreferencia.getRespuestaByIdMiembroIdPreferencia").setParameter("idMiembro", idMiembro).setParameter("idPreferencia", preferencia.getIdPreferencia()).getSingleResult());
                    } catch (NoResultException e) {
                    }
                });
                detalle.setMisionEncuestaPreferencias(preferencias);
                break;
            }
            case JUEGO: {
                Juego juego = em.find(Juego.class, detalle.getIdMision());
                if (juego != null) {
                    List<MisionJuego> misionJuegos = em.createNamedQuery("MisionJuego.findByIdMision").setParameter("idMision", detalle.getIdMision()).getResultList();
                    if (juego.getTipo().equals(Juego.Tipo.RASPADITA.getValue())) {
                        Juego.Estrategia estrategia = Juego.Estrategia.get(juego.getEstrategia());
                        switch (estrategia) {
                            case PROBABILIDAD: {
                                MisionJuego misionGanar = this.getPremioGanadorProbabilidad(juego, idMiembro, locale);
                                if (misionGanar == null) {
                                    boolean flag = false;
                                    for (MisionJuego misionJuego : misionJuegos) {
                                        if (flag == false && misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                            flag = true;
                                            misionJuego.setIsGanar(Boolean.TRUE);
                                        } else {
                                            misionJuego.setIsGanar(Boolean.FALSE);
                                        }
                                    }
                                } else {
                                    misionJuegos.forEach((misionJuego) -> {
                                        if (misionJuego.getIdMisionJuego().equals(misionGanar.getIdMisionJuego())) {
                                            misionJuego.setIsGanar(Boolean.TRUE);
                                        } else {
                                            misionJuego.setIsGanar(Boolean.FALSE);
                                        }
                                    });
                                }
                                break;
                            }
                            case RANDOM: {
                                MisionJuego misionGanar = this.getPremioGanadorRandom(juego, idMiembro, locale);
                                if (misionGanar == null) {
                                    misionJuegos.forEach((misionJuego) -> {
                                        if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                            misionJuego.setIsGanar(Boolean.TRUE);
                                        } else {
                                            misionJuego.setIsGanar(Boolean.FALSE);
                                        }
                                    });
                                } else {
                                    misionJuegos.forEach((misionJuego) -> {
                                        if (misionJuego.getIdMisionJuego().equals(misionGanar.getIdMisionJuego())) {
                                            misionJuego.setIsGanar(Boolean.TRUE);
                                        } else {
                                            misionJuego.setIsGanar(Boolean.FALSE);
                                        }
                                    });
                                }
                                break;
                            }
                        }
                    } else if (juego.getTipo().equals(Juego.Tipo.ROMPECABEZAS.getValue())) {
                        if (juego.getImagenesRompecabezas() != null) {
                            String[] split = juego.getImagenesRompecabezas().split(",");
                            List<ImagenesRompecabezas> lista = new ArrayList<>();
                            for (String url : split) {
                                lista.add(new ImagenesRompecabezas(url.trim()));
                            }
                            juego.setImagenes(lista);
                        }
                    }
                    juego.setMisionJuego(misionJuegos);
                }
                detalle.setJuego(juego);
                break;
            }
        }

        return detalle;
    }

    /**
     * Método que registra la vista de la mision
     *
     * @param idMision identificador de la misión
     * @param idMiembro identificador del miembro
     * @param locale
     * @param idWorkflow identificador del workflow
     */
    public void registerVistaMision(String idMision, String idMiembro, Locale locale, String idWorkflow) {
        //busqueda de la mision
        MisionControl mision = em.find(MisionControl.class, idMision);
        if (mision == null) {
            //de no encontrarse se retorna una respuesta informando sobre el recurso no existente
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "challenge_not_found");
        }
        //se verifica que la mision este publicada
        if (!mision.getIndEstado().equals(MisionControl.Estados.PUBLICADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_unavailable");
        }

        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);

        //en caso de que la respuesta de mision sea sobre un workflow
        if (idWorkflow != null && !idWorkflow.isEmpty()) {
            Workflow w = em.find(Workflow.class, idWorkflow);
            if (w == null) {
                //si el workflow no se pudo encontrar
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_unavailable");
            }
            if (w.isIsWorkflowMisionEspec() && checkEligibilidadWorkflow(idMiembro, w.getIdWorkflow(), segmentosPertenecientes)) {
                WorkflowNodo sucesor = w.getIdSucesor();
                if (!sucesor.getReferencia().equals(idMision)) {
                    //si el primer nodo no es la mision objectivo (en caso contrario se deja responder sin restriccion alguna)
                    //busqueda de todos los sucesores de cada nodo
                    sucesor = sucesor.getIdSucesor();
                    boolean flag = false; //por defecto no se ha encontrado la mision
                    while (sucesor != null) {
                        if (sucesor.getReferencia().equals(idMision)) {
                            //de encontrarse la mision, solo se verifica la elegibilidad de la misma
                            flag = checkEligibilidadMisionMiembro(idMision, idMiembro, segmentosPertenecientes, mision.getIndDefectoNinguno());
                            break;
                        }
                        sucesor = sucesor.getIdSucesor();
                    }
                    if (!flag) {
                        //si no se encontro la mision o no es elegible para la misma
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_unavailable");
                    }
                }
            } else {
                //si el workflow no es del tipo mision especial o el miembro no es elegible para el workflow
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_unavailable");
            }
        } else {
            //se verifica la elegibilidad del miembro en la promocion y en el intervalo de tiempo de espera entre su ultima respuesta
            if (!(checkEligibilidadMisionMiembro(idMision, idMiembro, segmentosPertenecientes, mision.getIndDefectoNinguno())
                    && checkIntervalosRespuestaMisionMiembro(mision, idMiembro)
                    && checkCalendarizacionMision(mision))) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_unavailable");
            }
        }

        //se registra que el miembro vio la mision
        insertRegistroVistaMision(idMiembro, idMision);

        try {
            //registro de evento de vista de mision
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.VER_MISION, idMision, idMiembro);
        } catch (Exception ex) {
            Logger.getLogger(MisionBean.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    /**
     * Método que hace el registro de que una mision para un determinado miembro
     * a sido completada
     *
     * @param idMiembro identificador del miembro
     * @param idMision identificador de la mision
     * @param respuesta objeto con la respuesta de la misión
     * @param locale
     * @return recibo de la respuesta de mision
     */
    public ReciboRespuestaMision completarMision(String idMiembro, String idMision, RespuestaMision respuesta, Locale locale) {
        //busqueda de la mision
        MisionControl mControl = em.find(MisionControl.class, idMision);
        if (mControl == null) {
            //de no encontrarse se retorna una respuesta informando sobre el recurso no existente
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "challenge_not_found");
        }
        //se verifica que la mision este publicada
        if (!mControl.getIndEstado().equals(MisionControl.Estados.PUBLICADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_unavailable");
        }

        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);

        //en caso de que la respuesta de mision sea sobre un workflow
        if (respuesta.getIdWorkflow() != null) {
            Workflow w = em.find(Workflow.class, respuesta.getIdWorkflow());
            if (w == null) {
                //si el workflow no se pudo encontrar
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_unavailable");
            }
            if (w.isIsWorkflowMisionEspec() && checkEligibilidadWorkflow(idMiembro, w.getIdWorkflow(), segmentosPertenecientes)) {
                WorkflowNodo sucesor = w.getIdSucesor();
                if (!sucesor.getReferencia().equals(idMision)) {
                    //si el primer nodo no es la mision objectivo (en caso contrario se deja responder sin restriccion alguna)
                    //busqueda de todos los sucesores de cada nodo
                    sucesor = sucesor.getIdSucesor();
                    boolean flag = false; //por defecto no se ha encontrado la mision
                    while (sucesor != null) {
                        if (sucesor.getReferencia().equals(idMision)) {
                            //de encontrarse la mision, solo se verifica la elegibilidad de la misma
                            flag = checkEligibilidadMisionMiembro(idMision, idMiembro, segmentosPertenecientes, mControl.getIndDefectoNinguno());
                            break;
                        }
                        sucesor = sucesor.getIdSucesor();
                    }
                    if (!flag) {
                        //si no se encontro la mision o no es elegible para la misma
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_unavailable");
                    }
                }
            } else {
                //si el workflow no es del tipo mision especial o el miembro no es elegible para el workflow
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_unavailable");
            }
        } else {
            //se verifica la elegibilidad del miembro en la promocion y en el intervalo de tiempo de espera entre su ultima respuesta
            if (!(checkEligibilidadMisionMiembro(idMision, idMiembro, segmentosPertenecientes, mControl.getIndDefectoNinguno())
                    && checkIntervalosRespuestaMisionMiembro(mControl, idMiembro)
                    && checkCalendarizacionMision(mControl))) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_unavailable");
            }
        }

        //obtencion de los detalles de la mision
        MisionDetails dMision = getMisionDetalleInternal(idMision, idMiembro, locale);

        //validacion de respuesta y procesamiento de la misma
        switch (MisionControl.TiposMision.get(mControl.getIndTipoMision())) {
            case PREFERENCIAS: {
                if (respuesta.getRespuestaPreferencias() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answers_not_found");
                }

                for (Preferencia preferencia : dMision.getMisionEncuestaPreferencias()) {
                    Optional<RespuestaMisionPreferencia> respuestaPreferencia = respuesta.getRespuestaPreferencias().stream().filter((t) -> t.getIdPreferencia() != null && t.getIdPreferencia().equals(preferencia.getIdPreferencia())).findFirst();
                    if (respuestaPreferencia.isPresent() && respuestaPreferencia.get().getRespuesta() != null) {
                        switch (Preferencia.Respuesta.get(preferencia.getIndTipoRespuesta())) {
                            case RESPTA_MULTIPLE: {
                                Supplier<Stream<Integer>> respuestasSeleccionadas;
                                try {
                                    respuestasSeleccionadas = () -> Arrays.stream(respuestaPreferencia.get().getRespuesta().split("\n")).map((t) -> Integer.parseInt(t));
                                } catch (NumberFormatException e) {
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid", preferencia.getIdPreferencia());
                                }

                                if (respuestasSeleccionadas.get().count() > respuestasSeleccionadas.get().distinct().count()) {
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid_range", preferencia.getIdPreferencia());
                                }

                                int cantidadOpciones = Arrays.asList(preferencia.getRespuestas().split("\n")).size();
                                if (respuestasSeleccionadas.get().anyMatch((t) -> t > cantidadOpciones || t < 1)) {
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid_range", preferencia.getIdPreferencia());
                                }

                                break;
                            }
                            case SELECCION_UNICA: {
                                int valorRespuesta;
                                try {
                                    valorRespuesta = Integer.parseInt(respuestaPreferencia.get().getRespuesta());
                                } catch (NumberFormatException e) {
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid", preferencia.getIdPreferencia());
                                }
                                int cantOpciones = Arrays.asList(preferencia.getRespuestas().split("\n")).size();
                                if (valorRespuesta > cantOpciones || valorRespuesta < 1) {
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid_range", preferencia.getIdPreferencia());
                                }
                                break;
                            }
                        }
                    } else {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid", preferencia.getIdPreferencia());
                    }

                    RespuestaPreferencia preferenciaMiembro = em.find(RespuestaPreferencia.class, new RespuestaPreferenciaPK(idMiembro, preferencia.getIdPreferencia()));
                    if (preferenciaMiembro == null) {
                        preferenciaMiembro = new RespuestaPreferencia(idMiembro, preferencia.getIdPreferencia(), respuestaPreferencia.get().getRespuesta(), new Date());
                    } else {
                        preferenciaMiembro.setRespuesta(respuestaPreferencia.get().getRespuesta());
                    }
                    em.merge(preferenciaMiembro);
                }
                break;
            }
            case PERFIL: {
                //verificacion que en la respuesta este presente la respuesta de actualizacion de perfil
                if (respuesta.getRespuestaActualizarPerfil() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answers_not_found");
                }

                //formacion del criteria query para la actualizacion de perfil del miembro
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaUpdate<Miembro> cu = cb.createCriteriaUpdate(Miembro.class);
                Root<Miembro> root = cu.from(Miembro.class);

                boolean flag = false;//bandera para saber si ejecutar el update del criteria builder

                //verificacion de la lista de atributos requeridos, lista de atributos correctos y actualizacion del perfil de miembro
                //por cada atributo definido en la mision....
                for (MisionPerfilAtributo definicionPerfilAtributo : dMision.getMisionPerfilAtributos()) {
                    //se busca el atributo en la respuesta
                    Optional<RespuestaMisionPerfilAtributo> respuestaPerfilAtributo = respuesta.getRespuestaActualizarPerfil().stream().filter((t) -> t.getIdAtributo() != null && t.getIdAtributo().equals(definicionPerfilAtributo.getIdAtributo())).findFirst();
                    //se verifica que la respuesta del atributo este presente
                    if (respuestaPerfilAtributo.isPresent() && respuestaPerfilAtributo.get().getRespuesta() != null && !respuestaPerfilAtributo.get().getRespuesta().trim().isEmpty()) {
                        MisionPerfilAtributo.Atributos tipoAtributo = MisionPerfilAtributo.Atributos.get(definicionPerfilAtributo.getIndAtributo());
                        switch (tipoAtributo) {
                            case ATRIBUTO_DINAMICO: {
                                AtributoDinamico atributoDinamico = definicionPerfilAtributo.getAtributoDinamico();
                                MiembroAtributo miembroAtributo = em.find(MiembroAtributo.class, new MiembroAtributoPK(idMiembro, atributoDinamico.getIdAtributo()));
                                switch (AtributoDinamico.TiposDato.get(atributoDinamico.getIndTipoDato())) {
                                    case BOOLEANO: {
                                        if (respuestaPerfilAtributo.get().getRespuesta().equalsIgnoreCase("TRUE") || respuestaPerfilAtributo.get().getRespuesta().equalsIgnoreCase("FALSE")) {
                                            if (miembroAtributo == null) {
                                                miembroAtributo = new MiembroAtributo(idMiembro, atributoDinamico.getIdAtributo(), new Date(), respuestaPerfilAtributo.get().getRespuesta().toLowerCase());
                                            } else {
                                                miembroAtributo.setFechaCreacion(new Date());
                                                miembroAtributo.setValor(respuestaPerfilAtributo.get().getRespuesta().toLowerCase());
                                            }
                                        } else {
                                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", definicionPerfilAtributo.getIdAtributo());
                                        }
                                        break;
                                    }
                                    case FECHA: {
                                        Date fecha;
                                        try {
                                            fecha = new Date(Long.parseLong(respuestaPerfilAtributo.get().getRespuesta()));
                                        } catch (NumberFormatException e) {
                                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", definicionPerfilAtributo.getIdAtributo());
                                        }
                                        if (miembroAtributo == null) {
                                            miembroAtributo = new MiembroAtributo(idMiembro, atributoDinamico.getIdAtributo(), new Date(), String.valueOf(fecha.getTime()));
                                        } else {
                                            miembroAtributo.setFechaCreacion(new Date());
                                            miembroAtributo.setValor(String.valueOf(fecha.getTime()));
                                        }
                                        break;
                                    }
                                    case NUMERICO: {
                                        BigDecimal valor;
                                        try {
                                            valor = new BigDecimal(respuestaPerfilAtributo.get().getRespuesta());
                                        } catch (NumberFormatException e) {
                                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", definicionPerfilAtributo.getIdAtributo());
                                        }
                                        if (miembroAtributo == null) {
                                            miembroAtributo = new MiembroAtributo(idMiembro, atributoDinamico.getIdAtributo(), new Date(), valor.toString());
                                        } else {
                                            miembroAtributo.setFechaCreacion(new Date());
                                            miembroAtributo.setValor(valor.toString());
                                        }
                                        break;
                                    }
                                    case TEXTO: {
                                        if (miembroAtributo == null) {
                                            miembroAtributo = new MiembroAtributo(idMiembro, atributoDinamico.getIdAtributo(), new Date(), respuestaPerfilAtributo.get().getRespuesta());
                                        } else {
                                            miembroAtributo.setFechaCreacion(new Date());
                                            miembroAtributo.setValor(respuestaPerfilAtributo.get().getRespuesta());
                                        }
                                        break;
                                    }
                                }
                                em.merge(miembroAtributo);
                                break;
                            }
                            case ESTADO_CIVIL: {
                                if (respuestaPerfilAtributo.get().getRespuesta().length() == 1 && Miembro.ValoresIndEstadoCivil.get(respuestaPerfilAtributo.get().getRespuesta().charAt(0)) != null) {
                                    cu.set(tipoAtributo.getColumnName(), respuestaPerfilAtributo.get().getRespuesta().charAt(0));
                                } else {
                                    //en el caso que la respuesta no sea un valor correcto
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", definicionPerfilAtributo.getIdAtributo());
                                }
                                flag = true;
                                break;
                            }
                            case GENERO: {
                                if (respuestaPerfilAtributo.get().getRespuesta().length() == 1 && (respuestaPerfilAtributo.get().getRespuesta().charAt(0) == Miembro.ValoresIndGenero.FEMENINO.getValue()
                                        || respuestaPerfilAtributo.get().getRespuesta().charAt(0) == Miembro.ValoresIndGenero.MASCULINO.getValue())) {
                                    cu.set(tipoAtributo.getColumnName(), respuestaPerfilAtributo.get().getRespuesta().charAt(0));
                                } else {
                                    //en el caso que la respuesta no sea un valor correcto
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", definicionPerfilAtributo.getIdAtributo());
                                }
                                flag = true;
                                break;
                            }
                            case TIPO_DE_EDUCACION: {
                                if (respuestaPerfilAtributo.get().getRespuesta().length() == 1 && Miembro.ValoresIndEducacion.get(respuestaPerfilAtributo.get().getRespuesta().charAt(0)) != null) {
                                    cu.set(tipoAtributo.getColumnName(), respuestaPerfilAtributo.get().getRespuesta().charAt(0));
                                } else {
                                    //en el caso que la respuesta no sea un valor correcto
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", definicionPerfilAtributo.getIdAtributo());
                                }
                                flag = true;
                                break;
                            }
                            case PAIS_DE_RESIDENCIA: {
                                if ((long) em.createNamedQuery("Pais.countByAlfa3").setParameter("alfa3", respuestaPerfilAtributo.get().getRespuesta()).getSingleResult() != 1) {
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", definicionPerfilAtributo.getIdAtributo());
                                }
                                cu.set(tipoAtributo.getColumnName(), respuestaPerfilAtributo.get().getRespuesta());
                                flag = true;
                                break;
                            }
                            case FECHA_DE_NACIMIENTO: {
                                try {
                                    cu.set(tipoAtributo.getColumnName(), new Date(Long.parseLong(respuestaPerfilAtributo.get().getRespuesta())));
                                } catch (NumberFormatException e) {
                                    //en el caso que la respuesta no sea un valor correcto
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", definicionPerfilAtributo.getIdAtributo());
                                }
                                flag = true;
                                break;
                            }
                            case INGRESO_ECONOMICO: {
                                try {
                                    cu.set(tipoAtributo.getColumnName(), new BigInteger(respuestaPerfilAtributo.get().getRespuesta()));
                                } catch (NumberFormatException e) {
                                    //en el caso que la respuesta no sea un valor correcto
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", definicionPerfilAtributo.getIdAtributo());
                                }
                                flag = true;
                                break;
                            }
                            case HIJOS: {
                                if (respuestaPerfilAtributo.get().getRespuesta().equalsIgnoreCase("TRUE") || respuestaPerfilAtributo.get().getRespuesta().equalsIgnoreCase("FALSE")) {
                                    cu.set(tipoAtributo.getColumnName(), respuestaPerfilAtributo.get().getRespuesta().equalsIgnoreCase("TRUE"));
                                } else {
                                    //en el caso que la respuesta no sea un valor correcto
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", definicionPerfilAtributo.getIdAtributo());
                                }
                                flag = true;
                                break;
                            }
                            default: {
                                cu.set(tipoAtributo.getColumnName(), respuestaPerfilAtributo.get().getRespuesta());
                                flag = true;
                            }
                        }
                    } else {
                        //si la respuesta del atributo no esta presente y esta marcado como requerido, se marca la respuesta como no valida
                        if (definicionPerfilAtributo.getIndRequerido()) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_required", definicionPerfilAtributo.getIdAtributo());
                        }
                    }
                }

                if (flag) {
                    //se actualiza la informacion del miembro
                    cu.where(cb.equal(root.get("idMiembro"), idMiembro));
                    em.createQuery(cu).executeUpdate();
                }

                try {
                    em.flush();
                } catch (PersistenceException e) {
                    throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "data_member_cant_update");
                }

                try {
                    myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.ACTUALIZACION_PERFIL, null, idMiembro);
                } catch (Exception ex) {
                    Logger.getLogger(MisionBean.class.getName()).log(Level.WARNING, null, ex);
                }

                break;
            }
            case ENCUESTA: {
                //verificacion que la respuesta de la encuesta este presente
                if (respuesta.getRespuestaEncuesta() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answers_not_found");
                }

                //por cada pregunta definida en la encuesta
                for (MisionEncuestaPregunta definicionEncuestaPregunta : dMision.getMisionEncuestaPreguntas()) {
                    //busca la respuesta correspondiente a la pregunta
                    Optional<RespuestaMisionEncuestaPregunta> respuestaEncuestaPregunta = respuesta.getRespuestaEncuesta().stream().filter((t) -> t.getIdPregunta() != null && definicionEncuestaPregunta.getIdPregunta().equals(t.getIdPregunta())).findFirst();

                    //verifica que la respuesta este presente y no vacia
                    if (respuestaEncuestaPregunta.isPresent() && respuestaEncuestaPregunta.get().getRespuesta() != null) {
                        //segun el tipo de encuesta...
                        switch (MisionEncuestaPregunta.TiposPregunta.get(definicionEncuestaPregunta.getIndTipoPregunta())) {
                            case CALIFICACION: {
                                //en el caso de que sea una calificacion...
                                int valorRespuesta;
                                //verifica que la respuesta sea un valor numerico
                                try {
                                    valorRespuesta = Integer.parseInt(respuestaEncuestaPregunta.get().getRespuesta());
                                } catch (NumberFormatException e) {
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid", definicionEncuestaPregunta.getIdPregunta());
                                }
                                //verifica que la respuesta sea un valor entre 1 a 5
                                if (valorRespuesta > 5 || valorRespuesta < 1) {
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid_range", definicionEncuestaPregunta.getIdPregunta());
                                }
                                break;
                            }
                            case PREGUNTA: {
                                //en el caso de que sea una pregunta....

                                //segun el tipo de respuesta para la pregunta....
                                switch (MisionEncuestaPregunta.TiposRespuesta.get(definicionEncuestaPregunta.getIndTipoRespuesta())) {
                                    case RESPUESTA_ABIERTA_NUMERICO: {
                                        //en el caso de que sea una respuesta abierta con valor numerico
                                        //se verifica que la respuesta contenga un valor numerico
                                        try {
                                            BigDecimal valorRespuesta = new BigDecimal(respuestaEncuestaPregunta.get().getRespuesta());
                                        } catch (NumberFormatException e) {
                                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid", definicionEncuestaPregunta.getIdPregunta());
                                        }
                                        break;
                                    }
                                    case SELECCION_MULTIPLE: {
                                        //en el caso de que sea una respuesta de seleccion multiple

                                        //se obtienen las respuestas seleccionadas, se dividen en lineas y se convierten en numeros de opciones...
                                        Supplier<Stream<Integer>> respuestasSeleccionadas;
                                        try {
                                            respuestasSeleccionadas = () -> Arrays.stream(respuestaEncuestaPregunta.get().getRespuesta().split("\n")).map((t) -> Integer.parseInt(t));
                                        } catch (NumberFormatException e) {
                                            //en el caso de que los valores sobre las opciones seleccionadas no sean numericos (no validos)
                                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid", definicionEncuestaPregunta.getIdPregunta());
                                        }

                                        //se verifican que no existan ningun duplicado
                                        if (respuestasSeleccionadas.get().count() > respuestasSeleccionadas.get().distinct().count()) {
                                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid_range", definicionEncuestaPregunta.getIdPregunta());
                                        }

                                        //se obtiene la cantidad de opciones de respuestas de la mision
                                        int cantidadOpciones = Arrays.asList(definicionEncuestaPregunta.getRespuestas().split("\n")).size();
                                        //se verifica que los valores de respuesta seleccionada entre entre los rangos validos
                                        if (respuestasSeleccionadas.get().anyMatch((t) -> t > cantidadOpciones || t < 1)) {
                                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid_range", definicionEncuestaPregunta.getIdPregunta());
                                        }

                                        break;
                                    }
                                    case SELECCION_UNICA: {
                                        //en el caso de que sea una respuesta de seleccion unica

                                        //se verifica que sea un valor numerico
                                        int valorRespuesta;
                                        try {
                                            valorRespuesta = Integer.parseInt(respuestaEncuestaPregunta.get().getRespuesta());
                                        } catch (NumberFormatException e) {
                                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid", definicionEncuestaPregunta.getIdPregunta());
                                        }

                                        //se obtiene la cantidad de opciones de respuestas de la mision
                                        int cantidadOpciones = Arrays.asList(definicionEncuestaPregunta.getRespuestas().split("\n")).size();
                                        //se verifica que la opcion seleccionada este en el rango de opciones
                                        if (valorRespuesta > cantidadOpciones || valorRespuesta < 1) {
                                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answer_question_invalid_range", definicionEncuestaPregunta.getIdPregunta());
                                        }
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    } else {
                        //la respuesta para la calificacion/pregunta no esta presente
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_required", definicionEncuestaPregunta.getIdPregunta());
                    }
                }
                break;
            }
            case SUBIR_CONTENIDO: {
                //se verifica que la respuesta este presente con el contenido presente
                if (respuesta.getRespuestaSubirContenido() == null || respuesta.getRespuestaSubirContenido().getDatosContenido() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "answers_not_found");
                }

                //obtencion del tipo de archivo
                String fileType = null;
                byte[] bytes = Base64.getDecoder().decode(respuesta.getRespuestaSubirContenido().getDatosContenido());
                try {
                    ContentInfo contentInfo = new ContentInfoUtil().findMatch(bytes);
                    if (contentInfo != null) {
                        fileType = contentInfo.getMimeType();
                    }
                } catch (IllegalArgumentException e) {
                    //no se pudo saber el mimetype
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "data_invalid");
                }

                //segun el tipo de archivo solicitado por la mision...
                switch (MisionSubirContenido.Tipos.get(dMision.getMisionSubirContenido().getIndTipo())) {
                    case IMAGEN: {
                        //tamaño maximo permitido 5mb
                        if (bytes.length > 5e+6) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "data_too_large");
                        }
                        //se verifica que sea un tipo de imagen
                        if (fileType == null || !fileType.split("/")[0].equalsIgnoreCase("IMAGE")) {
                            //no es del tipo imagen
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "data_not_image");
                        }
                        break;
                    }
                    case VIDEO: {
                        //tamaño maximo permitido 5mb
                        if (bytes.length > 2.5e+7) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "data_too_large");
                        }
                        //se verifica que sea un tipo de video
                        if (fileType == null || !fileType.split("/")[0].equalsIgnoreCase("VIDEO")) {
                            //no es del tipo video
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "data_not_video");
                        }
                        break;
                    }
                    default: {
                        //no de algun tipo valido
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "data_invalid");
                    }
                }
                String url;
                try (MyAwsS3Utils fileUtils = new MyAwsS3Utils()) {
                    url = fileUtils.uploadMediaMision(respuesta.getRespuestaSubirContenido().getDatosContenido(), fileType);
                } catch (Exception ex) {
                    throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "answers_data_not_saved");
                }
                respuesta.getRespuestaSubirContenido().setDatosContenido(url);
            }
            case RED_SOCIAL: {
//                if (respuesta.getRespuestaRedSocial()==null) {
//                    throw new ErrorClienteException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA);
//                }
                //TODO ???
                break;
            }
            case VER_CONTENIDO: {
//                if (respuesta.getRespuestaVerContenido()==null) {
//                    throw new ErrorClienteException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA);
//                }
                //TODO ???
                break;
            }
        }

        //autoaprobacion/rechazo y registro de la respuesta de la mision
        if (mControl.getIndAprovacion().equals(MisionControl.TiposAprobacion.AUTOMATICO.getValue())) {
            //autoaprobacion o autorechazo de la respuesta de mision
            boolean aprobado = false;
            switch (MisionControl.TiposMision.get(mControl.getIndTipoMision())) {
                case ENCUESTA: {
                    //por cada pregunta definida en la encuesta, se verifica que cada pregunta/calificacion sea correcta
                    for (MisionEncuestaPregunta definicionEncuestaPregunta : dMision.getMisionEncuestaPreguntas()) {
                        //busca la respuesta correspondiente a la pregunta
                        RespuestaMisionEncuestaPregunta respuestaEncuestaPregunta = respuesta.getRespuestaEncuesta().stream().filter((t) -> t.getIdPregunta() != null && definicionEncuestaPregunta.getIdPregunta().equals(t.getIdPregunta())).findFirst().get();

                        //segun el tipo de pregunta...
                        switch (MisionEncuestaPregunta.TiposPregunta.get(definicionEncuestaPregunta.getIndTipoPregunta())) {
                            case CALIFICACION: {
                                //si es del tipo calificacion, segun el tipo de respuesta correcta...
                                switch (MisionEncuestaPregunta.TiposRespuestaCorrecta.get(definicionEncuestaPregunta.getIndRespuestaCorrecta())) {
                                    case TODAS: {
                                        //si la respuesta correcta es cualquier opcion, se marca como correcta
                                        aprobado = true;
                                        break;
                                    }
                                    case IGUAL: {
                                        //si la respuesta correcta es sobre un valor especifico, se comparan las respuestas de la misma
                                        aprobado = Integer.parseInt(definicionEncuestaPregunta.getRespuestasCorrectas()) == Integer.parseInt(respuestaEncuestaPregunta.getRespuesta());
                                        break;
                                    }
                                    case MAYOR_IGUAL: {
                                        //si la respuesta es igual o mayor al valor establecido, se compara con la respuesta dada
                                        aprobado = Integer.parseInt(definicionEncuestaPregunta.getRespuestasCorrectas()) <= Integer.parseInt(respuestaEncuestaPregunta.getRespuesta());
                                        break;
                                    }
                                    case MENOR_IGUAL: {
                                        //si la respuesta es igual o menor al valor establecido, se compara con la respuesta dada
                                        aprobado = Integer.parseInt(definicionEncuestaPregunta.getRespuestasCorrectas()) >= Integer.parseInt(respuestaEncuestaPregunta.getRespuesta());
                                        break;
                                    }
                                }
                                break;
                            }
                            case PREGUNTA: {
                                //si es del tipo pregunta, segun el tipo de respuesta de la pregunta...
                                switch (MisionEncuestaPregunta.TiposRespuesta.get(definicionEncuestaPregunta.getIndTipoRespuesta())) {
                                    case SELECCION_UNICA: {
                                        //si es de seleccion unica, segun el tipo de respuesta correcta...
                                        switch (MisionEncuestaPregunta.TiposRespuestaCorrecta.get(definicionEncuestaPregunta.getIndRespuestaCorrecta())) {
                                            case TODAS: {
                                                //si la respuesta correcta es cualquier opcion, se marca como correcta
                                                aprobado = true;
                                                break;
                                            }
                                            case ES: {
                                                //si la respuesta correcta es sobre un valor especifico, se compara la respuesta dada con el valor especificado
                                                aprobado = Integer.parseInt(definicionEncuestaPregunta.getRespuestasCorrectas()) == Integer.parseInt(respuestaEncuestaPregunta.getRespuesta());
                                                break;
                                            }
                                            case CUALQUIERA_DE: {
                                                //si la respuesta correcta es cualquiera de una serie de opciones seleccionadas, se compara la respuesta dada con las opciones marcadas en busca de al menos una seleccion correcta
                                                aprobado = Arrays.asList(definicionEncuestaPregunta.getRespuestasCorrectas().split("\n")).stream().anyMatch((t) -> {
                                                    return Integer.parseInt(respuestaEncuestaPregunta.getRespuesta()) == Integer.parseInt(t);
                                                });
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                    case SELECCION_MULTIPLE: {
                                        //si es de seleccion multiple, segun el tipo de respuestas correctas...
                                        switch (MisionEncuestaPregunta.TiposRespuestaCorrecta.get(definicionEncuestaPregunta.getIndRespuestaCorrecta())) {
                                            case TODAS: {
                                                //si las respuestas correctas son todas, se marca como correcta
                                                aprobado = true;
                                                break;
                                            }
                                            case SON: {
                                                //si las respuestas correctas son unas opciones especificas, se compara que cada opcion marcada en la respuesta concuerde con alguna de las opciones correctas
                                                aprobado = Arrays.asList(respuestaEncuestaPregunta.getRespuesta().split("\n")).stream().allMatch((t) -> {
                                                    return Arrays.asList(definicionEncuestaPregunta.getRespuestasCorrectas().split("\n")).stream().anyMatch((d) -> Integer.parseInt(d) == Integer.parseInt(t));
                                                });
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                    case RESPUESTA_ABIERTA_NUMERICO:
                                    case RESPUESTA_ABIERTA_TEXTO: {
                                        //si la respuesta es abierta, se marca como correcta
                                        aprobado = true;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
                case RED_SOCIAL: {
                    //TODO
                    //trabajo adicional para la auto aprobacion/rechazo de este tipo de mision sobre la respuesta dada
                    aprobado = true;
                    break;
                }
                case VER_CONTENIDO: {
                    //TODO
                    //trabajo adicional para la auto aprobacion/rechazo de este tipo de mision sobre la respuesta dada
                    aprobado = true;
                    break;
                }
                case JUEGO:
                case REALIDAD_AUMENTADA:
                case PERFIL:
                case PREFERENCIAS:
                case SUBIR_CONTENIDO: {
                    //NOTA: solo se requiere que la respuesta sea valida...
                    aprobado = true;
                    break;
                }
            }

            //respuesta sobre el recibo de respuesta de mision
            ReciboRespuestaMision recibo = new ReciboRespuestaMision();

            //en caso de que se haya aprobado....
            if (aprobado) {
                //se almacena la respuesta con un estado de ganado
                insertRegistroRespuestaMision(idMiembro, idMision, respuesta, RespuestaMision.EstadosRespuesta.GANADO);

                switch (MisionControl.TiposMision.get(mControl.getIndTipoMision())) {
                    //TODO cuando la mision es del tipo juego debe ver que tipo de premio es...
                    case JUEGO: {
                        MisionJuego misionJuego = null;
                        Juego juego = em.find(Juego.class, mControl.getIdMision());
                        Juego.Tipo tipoJuego = Juego.Tipo.get(juego.getTipo());

                        switch (tipoJuego) {
                            case RULETA: {
                                if (juego.getEstrategia().equals(Juego.Estrategia.RANDOM.getValue())) {
                                    misionJuego = this.getPremioGanadorRandom(juego, idMiembro, locale);
                                } else if (juego.getEstrategia().equals(Juego.Estrategia.PROBABILIDAD.getValue())) {
                                    misionJuego = this.getPremioGanadorProbabilidad(juego, idMiembro, locale);
                                }

                                if (misionJuego == null || misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                    //creo el recibo del gane de la mision
                                    recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                                    //los puntos acumulados son los del juego ganado
                                    recibo.setMensaje("Gracias por participar, suerte para la proxima");
                                    recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.AGRADECIMIENTO.getValue());
                                    //la referencia es de la metrica configurada en el premio
                                    recibo.setReferenciaPremio(null);
                                } else if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.METRICA.getValue())) {
                                    //creo el recibo del gane de la mision
                                    recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                                    //los puntos acumulados son los del juego ganado
                                    recibo.setMensaje("Mision ganada, has acumulado: " + misionJuego.getCantMetrica());
                                    recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.METRICA.getValue());
                                    //la referencia es de la metrica configurada en el premio
                                    recibo.setReferenciaPremio(misionJuego.getMetrica().getIdMetrica());
                                    //registro la metrica ganada
                                    registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), misionJuego.getMetrica().getIdMetrica(), idMiembro, RegistroMetrica.TiposGane.MISION, misionJuego.getCantMetrica(), idMision));

                                } else if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.PREMIO.getValue())) {
                                    String codigo = this.ganePremioConMision(misionJuego.getPremio().getIdPremio(), idMiembro, locale);
                                    //creo el recibo del gane de la mision
                                    recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                                    //los puntos acumulados son los del juego ganado
                                    recibo.setMensaje(codigo == null ? "Mision ganada, has ganado el premio: " + misionJuego.getPremio().getEncabezadoArte() : "Mision ganada, has ganado el certificado: " + codigo);
                                    recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.PREMIO.getValue());
                                    //la referencia es de la metrica configurada en el premio
                                    recibo.setReferenciaPremio(misionJuego.getPremio().getIdPremio());

                                }
                                break;
                            }

                            case RASPADITA: {
                                if (respuesta.getRespuestaJuego() == null) {
                                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idMisionJuego");
                                }
                                misionJuego = em.find(MisionJuego.class, respuesta.getRespuestaJuego().getIdMisionJuego());
                                if (misionJuego == null || misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                    //creo el recibo del gane de la mision
                                    recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                                    //los puntos acumulados son los del juego ganado
                                    recibo.setMensaje("Gracias por participar, suerte para la proxima");
                                    recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.AGRADECIMIENTO.getValue());
                                    //la referencia es de la metrica configurada en el premio
                                    recibo.setReferenciaPremio(null);
                                } else if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.METRICA.getValue())) {
                                    //creo el recibo del gane de la mision
                                    recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                                    //los puntos acumulados son los del juego ganado
                                    recibo.setMensaje("Mision ganada, has acumulado: " + misionJuego.getCantMetrica());
                                    recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.METRICA.getValue());
                                    //la referencia es de la metrica configurada en el premio
                                    recibo.setReferenciaPremio(misionJuego.getMetrica().getIdMetrica());
                                    //registro la metrica ganada
                                    registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), misionJuego.getMetrica().getIdMetrica(), idMiembro, RegistroMetrica.TiposGane.MISION, misionJuego.getCantMetrica(), idMision));

                                } else if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.PREMIO.getValue())) {
                                    String codigo = this.ganePremioConMision(misionJuego.getPremio().getIdPremio(), idMiembro, locale);
                                    //creo el recibo del gane de la mision
                                    recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                                    //los puntos acumulados son los del juego ganado
                                    recibo.setMensaje(codigo == null ? "Mision ganada, has ganado el premio: " + misionJuego.getPremio().getEncabezadoArte() : "Mision ganada, has ganado el certificado: " + codigo);
                                    recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.PREMIO.getValue());
                                    //la referencia es de la metrica configurada en el premio
                                    recibo.setReferenciaPremio(misionJuego.getPremio().getIdPremio());

                                }
                                break;
                            }
                            case ROMPECABEZAS: {
                                List<MisionJuego> misionesJuego = em.createNamedQuery("MisionJuego.findByIdMision").setParameter("idMision", idMision).getResultList();
                                if (misionesJuego.isEmpty()) {
                                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "no_rewards_win");
                                }
                                misionJuego = misionesJuego.get(0);

                                if (misionJuego == null || misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                    //creo el recibo del gane de la mision
                                    recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                                    //los puntos acumulados son los del juego ganado
                                    recibo.setMensaje("Gracias por participar, suerte para la proxima");
                                    recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.AGRADECIMIENTO.getValue());
                                    //la referencia es de la metrica configurada en el premio
                                    recibo.setReferenciaPremio(null);
                                } else if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.METRICA.getValue())) {
                                    //creo el recibo del gane de la mision
                                    recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                                    //los puntos acumulados son los del juego ganado
                                    recibo.setMensaje("Mision ganada, has acumulado: " + misionJuego.getCantMetrica());
                                    recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.METRICA.getValue());
                                    //la referencia es de la metrica configurada en el premio
                                    recibo.setReferenciaPremio(misionJuego.getMetrica().getIdMetrica());
                                    //registro la metrica ganada
                                    registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), misionJuego.getMetrica().getIdMetrica(), idMiembro, RegistroMetrica.TiposGane.MISION, misionJuego.getCantMetrica(), idMision));

                                } else if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.PREMIO.getValue())) {
                                    String codigo = this.ganePremioConMision(misionJuego.getPremio().getIdPremio(), idMiembro, locale);
                                    //creo el recibo del gane de la mision
                                    recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                                    //los puntos acumulados son los del juego ganado
                                    recibo.setMensaje(codigo == null ? "Mision ganada, has ganado el premio: " + misionJuego.getPremio().getEncabezadoArte() : "Mision ganada, has ganado el certificado: " + codigo);
                                    recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.PREMIO.getValue());
                                    //la referencia es de la metrica configurada en el premio
                                    recibo.setReferenciaPremio(misionJuego.getPremio().getIdPremio());

                                }
                                break;
                            }
                            case MILLONARIO: {
                                List<MisionJuego> misionesJuego = em.createNamedQuery("MisionJuego.findByIdMision").setParameter("idMision", idMision).getResultList();
                                if (misionesJuego.isEmpty()) {
                                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "no_rewards_win");
                                }
                                misionJuego = misionesJuego.get(0);

                                if (misionJuego == null || misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                    //creo el recibo del gane de la mision
                                    recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                                    //los puntos acumulados son los del juego ganado
                                    recibo.setMensaje("Gracias por participar, suerte para la proxima");
                                    recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.AGRADECIMIENTO.getValue());
                                    //la referencia es de la metrica configurada en el premio
                                    recibo.setReferenciaPremio(null);
                                } else if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.METRICA.getValue())) {
                                    //creo el recibo del gane de la mision
                                    recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                                    //los puntos acumulados son los del juego ganado
                                    recibo.setMensaje("Mision ganada, has acumulado: " + misionJuego.getCantMetrica());
                                    recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.METRICA.getValue());
                                    //la referencia es de la metrica configurada en el premio
                                    recibo.setReferenciaPremio(misionJuego.getMetrica().getIdMetrica());
                                    //registro la metrica ganada
                                    registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), misionJuego.getMetrica().getIdMetrica(), idMiembro, RegistroMetrica.TiposGane.MISION, misionJuego.getCantMetrica(), idMision));

                                } else if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.PREMIO.getValue())) {
                                    String codigo = this.ganePremioConMision(misionJuego.getPremio().getIdPremio(), idMiembro, locale);
                                    //creo el recibo del gane de la mision
                                    recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                                    //los puntos acumulados son los del juego ganado
                                    recibo.setMensaje(codigo == null ? "Mision ganada, has ganado el premio: " + misionJuego.getPremio().getEncabezadoArte() : "Mision ganada, has ganado el certificado: " + codigo);
                                    recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.PREMIO.getValue());
                                    //la referencia es de la metrica configurada en el premio
                                    recibo.setReferenciaPremio(misionJuego.getPremio().getIdPremio());

                                }
                                break;
                            }
                        }

                        break;
                    }
                    case REALIDAD_AUMENTADA:
                    case PREFERENCIAS:
                    case ENCUESTA:
                    case PERFIL:
                    case RED_SOCIAL:
                    case SUBIR_CONTENIDO:
                    case VER_CONTENIDO: {
                        recibo.setIndEstado(RespuestaMision.EstadosRespuesta.GANADO.getValue());
                        recibo.setMensaje("Mision ganada, has acumulado: " + mControl.getCantMetrica());
                        recibo.setIndTipoPremio(ReciboRespuestaMision.TiposPremios.METRICA.getValue());
                        recibo.setReferenciaPremio(mControl.getIdMetrica());

                        registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), mControl.getIdMetrica(), idMiembro, RegistroMetrica.TiposGane.MISION, mControl.getCantMetrica(), idMision));

                        break;
                    }
                }

                try {
                    myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.EJECUCION_MISION, idMision, idMiembro);
                    myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.APROBO_MISION, idMision, idMiembro);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            } else {
                //se almacena la respuesta con un estado de fallido
                insertRegistroRespuestaMision(idMiembro, idMision, respuesta, RespuestaMision.EstadosRespuesta.FALLIDO);

                recibo.setIndEstado(RespuestaMision.EstadosRespuesta.FALLIDO.getValue());
                recibo.setMensaje("Mision fallida");

                try {
                    myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.EJECUCION_MISION, idMision, idMiembro);
                    myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.REPROBO_MISION, idMision, idMiembro);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }

            return recibo;
        } else {
            //registro de la respuesta de mision con el estado de pendiente de revision y aprobacion/rechazo
            insertRegistroRespuestaMision(idMiembro, idMision, respuesta, RespuestaMision.EstadosRespuesta.PENDIENTE);

            try {
                myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.EJECUCION_MISION, idMision, idMiembro);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }

            ReciboRespuestaMision recibo = new ReciboRespuestaMision();
            recibo.setIndEstado(RespuestaMision.EstadosRespuesta.PENDIENTE.getValue());
            recibo.setMensaje("Mision aceptada, en espera de aprobacion");
            return recibo;
        }
    }

    /**
     * Método que devuelve una lista de misiones en las cuales el miembro es
     * elegible, ordenadas por el estado dando prioridad a las no completadas
     *
     * @param idMiembro identificador del miembro
     * @param locale
     * @return lista de misiones resultantes
     */
    public List<MisionDetails> getMisiones(String idMiembro, Locale locale) {
        //obtencion de todas las misiones dentro de la categoria de mision
        List<MisionControl> misiones = em.createNamedQuery("MisionControl.findAllByIndEstado").setParameter("indEstado", MisionControl.Estados.PUBLICADO.getValue()).getResultList();

        List<String> segmentosPertenecientes = segmentoBean.getSegmentosPertenecientesMiembro(idMiembro);

        //filtrado y conversion de cada mision....
        List<MisionDetails> resultado = misiones.stream()
                .filter((MisionControl m) -> checkEligibilidadMisionMiembro(m.getIdMision(), idMiembro, segmentosPertenecientes, m.getIndDefectoNinguno())
                && checkIntervalosRespuestaMisionMiembro(m, idMiembro)
                && checkCalendarizacionMision(m))
                .sorted((m1, m2) -> {
                    //obtencion de la fecha a comparar segun el indicador de calendarizacion
                    Date fecha1 = m1.getIndCalendarizacion() == MisionControl.IndicadoresCalendarizacion.CALENDARIZADO.getValue() ? m1.getFechaInicio() : m1.getFechaPublicacion();
                    Date fecha2 = m2.getIndCalendarizacion() == MisionControl.IndicadoresCalendarizacion.CALENDARIZADO.getValue() ? m2.getFechaInicio() : m2.getFechaPublicacion();
                    //comparacion de fecha en orden descendente
                    return fecha2.compareTo(fecha1);
                })
                .map((MisionControl m) -> {
                    MisionDetails detalle = em.find(MisionDetails.class, m.getIdMision());

                    switch (MisionControl.TiposMision.get(detalle.getIndTipoMision())) {
                        case PERFIL: {
                            Miembro miembro = em.find(Miembro.class, idMiembro);
                            for (MisionPerfilAtributo misionPerfilAtributo : detalle.getMisionPerfilAtributos()) {
                                MisionPerfilAtributo.Atributos tipoAtributo = MisionPerfilAtributo.Atributos.get(misionPerfilAtributo.getIndAtributo());
                                switch (tipoAtributo) {
                                    case ATRIBUTO_DINAMICO: {
                                        AtributoDinamico atributoDinamico = misionPerfilAtributo.getAtributoDinamico();
                                        MiembroAtributo miembroAtributo = em.find(MiembroAtributo.class, new MiembroAtributoPK(idMiembro, atributoDinamico.getIdAtributo()));
                                        if (miembroAtributo == null) {
                                            misionPerfilAtributo.setRespuesta(atributoDinamico.getValorDefecto());
                                        } else {
                                            misionPerfilAtributo.setRespuesta(miembroAtributo.getValor());
                                        }
                                        break;
                                    }
                                    case ESTADO_CIVIL: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIndEstadoCivil() == null ? null : miembro.getIndEstadoCivil().toString());
                                        break;
                                    }
                                    case GENERO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIndGenero() == null ? null : miembro.getIndGenero().toString());
                                        break;
                                    }
                                    case TIPO_DE_EDUCACION: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIndEducacion() == null ? null : miembro.getIndEducacion().toString());
                                        break;
                                    }
                                    case PAIS_DE_RESIDENCIA: {
                                        misionPerfilAtributo.setRespuesta(miembro.getPaisResidencia());
                                        break;
                                    }
                                    case FECHA_DE_NACIMIENTO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getFechaNacimiento() == null ? null : miembro.getFechaNacimiento().getTime() + "");
                                        break;
                                    }
                                    case INGRESO_ECONOMICO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIngresoEconomico() == null ? null : miembro.getIngresoEconomico().toString());
                                        break;
                                    }
                                    case HIJOS: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIndHijos() == null ? "FALSE" : miembro.getIndHijos() == true ? "TRUE" : "FALSE");
                                        break;
                                    }
                                    case CIUDAD_DE_RESIDENCIA: {
                                        misionPerfilAtributo.setRespuesta(miembro.getCiudadResidencia());
                                        break;
                                    }
                                    case CODIGO_POSTAL: {
                                        misionPerfilAtributo.setRespuesta(miembro.getCodigoPostal());
                                        break;
                                    }
                                    case ESTADO_DE_RESIDENCIA: {
                                        misionPerfilAtributo.setRespuesta(miembro.getEstadoResidencia());
                                        break;
                                    }
                                    case NOMBRE: {
                                        misionPerfilAtributo.setRespuesta(miembro.getNombre());
                                        break;
                                    }
                                    case PRIMER_APELLIDO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getApellido());
                                        break;
                                    }
                                    case SEGUNDO_APELLIDO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getApellido2());
                                        break;
                                    }
                                    case FRECUENCIA_COMPRA: {
                                        misionPerfilAtributo.setRespuesta(miembro.getFrecuenciaCompra());
                                        break;
                                    }
                                    default: {
                                        misionPerfilAtributo.setRespuesta(null);
                                    }
                                }
                            }
                            break;
                        }
                        case PREFERENCIAS: {
                            List<Preferencia> preferencias = em.createNamedQuery("MisionPreferencia.getPreferenciaByIdMision").setParameter("idMision", detalle.getIdMision()).getResultList();
                            preferencias.forEach((preferencia) -> {
                                try {
                                    preferencia.setRespuestaMiembro((String) em.createNamedQuery("RespuestaPreferencia.getRespuestaByIdMiembroIdPreferencia").setParameter("idMiembro", idMiembro).setParameter("idPreferencia", preferencia.getIdPreferencia()).getSingleResult());
                                } catch (NoResultException e) {
                                }
                            });
                            detalle.setMisionEncuestaPreferencias(preferencias);
                            break;
                        }
                        case JUEGO: {
                            Juego juego = em.find(Juego.class, detalle.getIdMision());
                            if (juego != null) {
                                List<MisionJuego> misionJuegos = em.createNamedQuery("MisionJuego.findByIdMision").setParameter("idMision", detalle.getIdMision()).getResultList();
                                if (juego.getTipo().equals(Juego.Tipo.RASPADITA.getValue())) {
                                    Juego.Estrategia estrategia = Juego.Estrategia.get(juego.getEstrategia());
                                    switch (estrategia) {
                                        case PROBABILIDAD: {
                                            MisionJuego misionGanar = this.getPremioGanadorProbabilidad(juego, idMiembro, locale);
                                            if (misionGanar == null) {
                                                boolean flag = false;
                                                for (MisionJuego misionJuego : misionJuegos) {
                                                    if (flag == false && misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                                        flag = true;
                                                        misionJuego.setIsGanar(Boolean.TRUE);
                                                    } else {
                                                        misionJuego.setIsGanar(Boolean.FALSE);
                                                    }
                                                }

                                            } else {
                                                misionJuegos.forEach((misionJuego) -> {
                                                    if (misionJuego.getIdMisionJuego().equals(misionGanar.getIdMisionJuego())) {
                                                        misionJuego.setIsGanar(Boolean.TRUE);
                                                    } else {
                                                        misionJuego.setIsGanar(Boolean.FALSE);
                                                    }
                                                });
                                            }
                                            break;
                                        }
                                        case RANDOM: {
                                            MisionJuego misionGanar = this.getPremioGanadorRandom(juego, idMiembro, locale);
                                            if (misionGanar == null) {
                                                misionJuegos.forEach((misionJuego) -> {
                                                    if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                                        misionJuego.setIsGanar(Boolean.TRUE);
                                                    } else {
                                                        misionJuego.setIsGanar(Boolean.FALSE);
                                                    }
                                                });
                                            } else {
                                                misionJuegos.forEach((misionJuego) -> {
                                                    if (misionJuego.getIdMisionJuego().equals(misionGanar.getIdMisionJuego())) {
                                                        misionJuego.setIsGanar(Boolean.TRUE);
                                                    } else {
                                                        misionJuego.setIsGanar(Boolean.FALSE);
                                                    }
                                                });
                                            }
                                            break;
                                        }
                                    }
                                } else if (juego.getTipo().equals(Juego.Tipo.ROMPECABEZAS.getValue())) {
                                    if (juego.getImagenesRompecabezas() != null) {
                                        String[] split = juego.getImagenesRompecabezas().split(",");
                                        List<ImagenesRompecabezas> lista = new ArrayList<>();
                                        for (String url : split) {
                                            lista.add(new ImagenesRompecabezas(url.trim()));
                                        }
                                        juego.setImagenes(lista);
                                    }
                                }
                                juego.setMisionJuego(misionJuegos);
                            }
                            detalle.setJuego(juego);
                            break;
                        }
                    }

                    return detalle;
                }).collect(Collectors.toList());

        return resultado;
    }

    /**
     * filtrado por inclusion/exclusion de miembro
     */
    private boolean checkEligibilidadMisionMiembro(String idMision, String idMiembro, List<String> segmentosPertenecientes, Boolean indDefectoNinguno) {
        //se consulta si existe un registro de inclusion/exclusion del miembro para la mision
        MisionListaMiembro miembroIncluidoExcluido = em.find(MisionListaMiembro.class, new MisionListaMiembroPK(idMiembro, idMision));
        if (miembroIncluidoExcluido != null) {
            //si existe... se filtra por su tipo... (inclusion o exclusion)
            switch (miembroIncluidoExcluido.getIndTipo()) {
                case Indicadores.INCLUIDO: {
                    return true;
                }
                case Indicadores.EXCLUIDO: {
                    return false;
                }
            }
        }

        if (!segmentosPertenecientes.isEmpty()) {
            List<List<String>> partitionsSegmentos;
            partitionsSegmentos = Lists.partition(segmentosPertenecientes, 999);
            if (partitionsSegmentos.stream()
                    .anyMatch((l) -> ((long) em.createNamedQuery("MisionListaSegmento.countByIdMisionAndSegmentosInListaAndByIndTipo")
                    .setParameter("idMision", idMision)
                    .setParameter("lista", l)
                    .setParameter("indTipo", Indicadores.EXCLUIDO)
                    .getSingleResult() > 0))) {
                return false;
            }
            if (partitionsSegmentos.stream()
                    .anyMatch((l) -> ((long) em.createNamedQuery("MisionListaSegmento.countByIdMisionAndSegmentosInListaAndByIndTipo")
                    .setParameter("idMision", idMision)
                    .setParameter("lista", l)
                    .setParameter("indTipo", Indicadores.INCLUIDO)
                    .getSingleResult() > 0))) {
                return true;
            }
        }
        return indDefectoNinguno == null || !indDefectoNinguno
                ? (long) em.createNamedQuery("MisionListaSegmento.countByIdMisionAndByIndTipo")
                        .setParameter("idMision", idMision)
                        .setParameter("indTipo", Indicadores.INCLUIDO)
                        .getSingleResult() == 0
                : false;
    }

    /**
     * filtrado de mision por tiempo trascurrido entre intervalos de respuestas
     */
    private boolean checkIntervalosRespuestaMisionMiembro(MisionControl mision, String idMiembro) {
        if (mision.getIndRespuesta() != null && mision.getIndRespuesta()) {
            MisionControl.IntervalosTiempoRespuesta intervalo;
            boolean flag = true;//bandera indicando si se aceptan nuevas respuestas (por defecto TRUE)

            //verificacion de intervalos de respuesta totales...
            intervalo = MisionControl.IntervalosTiempoRespuesta.get(mision.getIndIntervaloTotal());
            if (intervalo != null) {
                long cantidadRespuestas = 0;
                switch (intervalo) {
                    case POR_SIEMPRE: {
                        cantidadRespuestas = countRegistrosRespuestaMision(null, mision.getIdMision(), null, null);
                        break;
                    }
                    case POR_MES: {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.MONTH, -1);
                        cantidadRespuestas = countRegistrosRespuestaMision(null, mision.getIdMision(), calendar.getTime(), new Date());
                        break;
                    }
                    case POR_SEMANA: {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.WEEK_OF_YEAR, -1);
                        cantidadRespuestas = countRegistrosRespuestaMision(null, mision.getIdMision(), calendar.getTime(), new Date());
                        break;
                    }
                    case POR_DIA: {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.DAY_OF_YEAR, -1);
                        cantidadRespuestas = countRegistrosRespuestaMision(null, mision.getIdMision(), calendar.getTime(), new Date());
                        break;
                    }
                    case POR_HORA: {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.HOUR_OF_DAY, -1);
                        cantidadRespuestas = countRegistrosRespuestaMision(null, mision.getIdMision(), calendar.getTime(), new Date());
                        break;
                    }
                    case POR_MINUTO: {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.MINUTE, -1);
                        cantidadRespuestas = countRegistrosRespuestaMision(null, mision.getIdMision(), calendar.getTime(), new Date());
                        break;
                    }
                }

                //mientras que la cantidad de respuestas siga siendo menor a la cantidad de respuestas totales aceptables...
                flag = cantidadRespuestas < mision.getCantTotal();
            }

            //verificacion de intervalos de respuesta totales por miembro...
            intervalo = MisionControl.IntervalosTiempoRespuesta.get(mision.getIndIntervaloMimebro());
            if (intervalo != null && flag) {
                long cantidadRespuestas = 0;
                switch (intervalo) {
                    case POR_SIEMPRE: {
                        cantidadRespuestas = countRegistrosRespuestaMision(idMiembro, mision.getIdMision(), null, null);
                        break;
                    }
                    case POR_MES: {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.MONTH, -1);
                        cantidadRespuestas = countRegistrosRespuestaMision(idMiembro, mision.getIdMision(), calendar.getTime(), new Date());
                        break;
                    }
                    case POR_SEMANA: {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.WEEK_OF_YEAR, -1);
                        cantidadRespuestas = countRegistrosRespuestaMision(idMiembro, mision.getIdMision(), calendar.getTime(), new Date());
                        break;
                    }
                    case POR_DIA: {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.DAY_OF_YEAR, -1);
                        cantidadRespuestas = countRegistrosRespuestaMision(idMiembro, mision.getIdMision(), calendar.getTime(), new Date());
                        break;
                    }
                    case POR_HORA: {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.HOUR_OF_DAY, -1);
                        cantidadRespuestas = countRegistrosRespuestaMision(idMiembro, mision.getIdMision(), calendar.getTime(), new Date());
                        break;
                    }
                    case POR_MINUTO: {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.MINUTE, -1);
                        cantidadRespuestas = countRegistrosRespuestaMision(idMiembro, mision.getIdMision(), calendar.getTime(), new Date());
                        break;
                    }
                }

                //mientras que la cantidad de respuestas siga siendo menor a la cantidad de respuestas totales aceptables...
                flag = cantidadRespuestas < mision.getCantTotalMiembro();
            }

            //verificacion de tiempo entre respuestas del miembro...
            intervalo = MisionControl.IntervalosTiempoRespuesta.get(mision.getIndIntervalo());
            if (intervalo != null && flag) {
                Date fechaUltima = getFechaUltimoRegistroRespuestaMision(idMiembro, mision.getIdMision());
                if (fechaUltima != null) {
                    switch (intervalo) {
                        case POR_DIA: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.DAY_OF_YEAR, -mision.getCantIntervalo().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                        case POR_HORA: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.HOUR_OF_DAY, -mision.getCantIntervalo().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                        case POR_MINUTO: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.MINUTE, -mision.getCantIntervalo().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                    }
                }
            }

            return flag;
        }
        return true;
    }

    private boolean checkCalendarizacionMision(MisionControl mision) {
        if (mision.getIndCalendarizacion() == null) {
            return true;
        }

        //se establece el indicador de respuesta como verdadero (todo es permitido hasta que se demuestre lo contrario)
        boolean flag = true;
        //se obtiene la fecha de hoy
        Calendar hoy = Calendar.getInstance();

        //si la calendarizacion es calendarizada...
        if (mision.getIndCalendarizacion().equals(MisionControl.IndicadoresCalendarizacion.CALENDARIZADO.getValue())) {
            //si la fecha de inicio esta establecida, se comprueba que la fecha sea pasada a hoy o que sea el mismo dia de hoy
            if (mision.getFechaInicio() != null) {
                flag = hoy.getTime().after(mision.getFechaInicio());
            }
            //mientras que siga siendo verdadero...
            //si la fecha de fin esta establecida, se comprueba que la fecha sea posterior a hoy o que sea el mismo dia de hoy
            if (flag && mision.getFechaFin() != null) {
                flag = hoy.getTime().before(mision.getFechaFin());
            }
            //mientras que siga siendo verdadero...
            //si se establecieron dias de recurrencia...
            if (flag && mision.getIndDiaRecurrencia() != null) {
                //se obtiene el numero de dia de hoy
                int hoyDia = hoy.get(Calendar.DAY_OF_WEEK);

                //se recorre los indicadores de dias de recurrencia en busca de que se cumpla al menos una condicion...
                flag = Chars.asList(mision.getIndDiaRecurrencia().toCharArray()).stream().anyMatch((t) -> {
                    //segun el indicador de dia de recurrencia, se verifica si el numero de dia de hoy concuerda con el numero de dia asociado al indicador
                    switch (t) {
                        case 'L': {
                            return Calendar.MONDAY == hoyDia;
                        }
                        case 'K': {
                            return Calendar.TUESDAY == hoyDia;
                        }
                        case 'M': {
                            return Calendar.WEDNESDAY == hoyDia;
                        }
                        case 'J': {
                            return Calendar.THURSDAY == hoyDia;
                        }
                        case 'V': {
                            return Calendar.FRIDAY == hoyDia;
                        }
                        case 'S': {
                            return Calendar.SATURDAY == hoyDia;
                        }
                        case 'D': {
                            return Calendar.SUNDAY == hoyDia;
                        }
                        default: {
                            return false;
                        }
                    }
                });
            }

            if (flag && mision.getIndSemanaRecurrencia() != null) {
                long weeksPast = ChronoUnit.WEEKS.between(LocalDateTime.ofInstant(Instant.ofEpochMilli(mision.getFechaInicio() == null ? mision.getFechaPublicacion().getTime() : mision.getFechaInicio().getTime()), ZoneId.systemDefault()), LocalDateTime.now());
                if (weeksPast % mision.getIndSemanaRecurrencia().longValue() != 0) {
                    flag = false;
                }
            }
        }

        return flag;
    }

    /**
     * Método que escoge un premio ganador alazar para un juego
     *
     * @param juego
     * @return
     */
    private MisionJuego getPremioGanadorRandom(Juego juego, String idMiembro, Locale locale) {
        List<MisionJuego> misionJuegos = em.createNamedQuery("MisionJuego.findByIdMision").setParameter("idMision", juego.getIdMision()).getResultList();
        List<MisionJuego> resultado = new ArrayList<>();

        boolean flag = true;
        for (MisionJuego misionJuego : misionJuegos) {
            if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.PREMIO.getValue())) {
                flag = premioBean.existsExitenciaUbicacion(misionJuego.getPremio().getIdPremio(), locale);
            } else {
                //en el caso que el tipo sea metrica o gracias
                flag = true;
            }

            //si despues de verificar existencias flag sigue siendo true se verifica los limites del premio en el juego
            if (flag) {
                MisionJuego.Tipo tipo = MisionJuego.Tipo.get(misionJuego.getIndTipoPremio());
                switch (tipo) {
                    case PREMIO: {
                        flag = checkIntervalosRespuestaPremioMision(misionJuego, idMiembro);

                        //si flag sigue siendo true despues de verificar existencias y limites del premio, verificar los limites del premio
                        //?? vericar tambien calendarizacion y elegilibilidad?
                        if (flag) {
                            flag = premioBean.checkIntervalosRespuestaPremioMiembro(em.find(PremioControl.class, misionJuego.getPremio().getIdPremio()), idMiembro);
                        }
                        break;
                    }
                    case METRICA: {
                        flag = checkIntervalosGaneMetrica(misionJuego, idMiembro);
                        break;
                    }
                    case GRACIAS: {
                        flag = true;
                        break;
                    }
                }

            }

            //si flag es verdadero al final de todas las validaciones lo inserto en la lista 
            if (flag) {
                resultado.add(misionJuego);
            }
        }

        MisionJuego misionJuego;
        //segun el tamaño de la lista se manda la respuesta
        if (resultado.isEmpty()) {
            return null;
        } else if (resultado.size() == 1) {
            misionJuego = resultado.get(0);
        } else {
            Random rand = new Random();
            int n = rand.nextInt(resultado.size());
            misionJuego = resultado.get(n);
        }
        return misionJuego;
    }

    private MisionJuego getPremioGanadorProbabilidad(Juego juego, String idMiembro, Locale locale) {
        List<MisionJuego> misionJuegos = em.createNamedQuery("MisionJuego.findByIdMision").setParameter("idMision", juego.getIdMision()).getResultList();
        List<MisionJuego> resultado = new ArrayList<>();
        List<ConfiguracionGeneral> configuracion = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
        ConfiguracionGeneral config = configuracion.get(0);
        if (config == null) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "general_configuration_not_found");
        }
        boolean flag = true;
        for (MisionJuego misionJuego : misionJuegos) {
            if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.PREMIO.getValue())) {
                flag = premioBean.existsExitenciaUbicacion(misionJuego.getPremio().getIdPremio(), locale);
            } else {
                //en el caso que el tipo sea metrica o gracias
                flag = true;
            }

            //si despues de verificar existencias flag sigue siendo true se verifica los limites del premio en el juego
            if (flag) {
                MisionJuego.Tipo tipo = MisionJuego.Tipo.get(misionJuego.getIndTipoPremio());
                switch (tipo) {
                    case PREMIO: {
                        flag = checkIntervalosRespuestaPremioMision(misionJuego, idMiembro);
                        //si flag sigue siendo true despues de verificar existencias y limites del premio, verificar los limites del premio
                        //?? vericar tambien calendarizacion y elegilibilidad?
                        if (flag) {
                            flag = premioBean.checkIntervalosRespuestaPremioMiembro(em.find(PremioControl.class, misionJuego.getPremio().getIdPremio()), idMiembro);
                        }
                        break;
                    }
                    case METRICA: {
                        flag = checkIntervalosGaneMetrica(misionJuego, idMiembro);
                        break;
                    }
                    case GRACIAS: {
                        flag = true;
                        break;
                    }
                }

            }

            //si flag es verdadero al final de todas las validaciones lo inserto en la lista 
            if (flag) {
                resultado.add(misionJuego);
            }
        }

        MisionJuego misionJuego;
        //segun el tamaño de la lista se manda la respuesta
        if (resultado.isEmpty()) {
            return null;
        } else if (resultado.size() == 1) {
            misionJuego = resultado.get(0);
        } else {
            //creo el mapa con los valores de probabilidad
            List<MisionJuego> arregloPremios = new ArrayList<>();
            for (MisionJuego mision : resultado) {
                for (int i = 0; i < mision.getProbabilidad(); i++) {
                    arregloPremios.add(mision);
                }
            }
            Random rand = new Random();
            int n = rand.nextInt(arregloPremios.size());
            misionJuego = arregloPremios.get(n);
        }

        return misionJuego;
    }

    /**
     * filtrado de premio por tiempo transcurrido entre intervalos de respuestas
     */
    private boolean checkIntervalosRespuestaPremioMision(MisionJuego misionJuego, String idMiembro) {
        if (misionJuego.getIndRespuesta() != null && misionJuego.getIndRespuesta()) {
            MisionJuego.IntervalosTiempoRespuesta intervalo;
            boolean flag = true;//bandera indicando si se aceptan nuevas rediciones (por defecto true)

            //verificacion de intervalos de respuesta totales
            intervalo = MisionJuego.IntervalosTiempoRespuesta.get(misionJuego.getIndInterTotal());
            if (intervalo != null) {
                flag = checkIntervaloRespuestaPremioMision(null, misionJuego.getPremio().getIdPremio(), intervalo, misionJuego.getCantInterTotal());
            }

            //verificacion de intervalos de respuesta totales por miembro..
            intervalo = MisionJuego.IntervalosTiempoRespuesta.get(misionJuego.getIndInterMiembro());
            if (intervalo != null && flag) {
                flag = checkIntervaloRespuestaPremioMision(idMiembro, misionJuego.getPremio().getIdPremio(), intervalo, misionJuego.getCantInterMiembro());
            }
            return flag;
        }
        return true;
    }

    /**
     * true -> se aceptan nuevas respuestas false -> no se aceptan nuevas
     * respuestas
     */
    private boolean checkIntervaloRespuestaPremioMision(String idMiembro, String idPremio, MisionJuego.IntervalosTiempoRespuesta intervalo, Long cantTotal) {
        /*
        Se obtienen la cantidad de respuestas de registros de respuestas de premio en rangos de tiempos de fecha
         */
        long cantidadRespuestas = 0;
        switch (intervalo) {
            case POR_SIEMPRE: {
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasPremio").setParameter("idPremio", idPremio).getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasPremioMiembro").setParameter("idPremio", idPremio).setParameter("idMiembro", idMiembro).getSingleResult();
                }
                break;
            }
            case POR_MES: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_SEMANA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.WEEK_OF_YEAR, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_DIA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_HORA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.HOUR_OF_DAY, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();

                }

                break;
            }
            case POR_MINUTO: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MINUTE, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
        }
        //mientras que la cantidad de respuestas siga siendo menor a la cantidad de respuestas totales aceptables..
        return cantidadRespuestas < cantTotal;
    }

    private boolean checkIntervalosGaneMetrica(MisionJuego misionJuego, String idMiembro) {
        if (misionJuego.getIndRespuesta() != null && misionJuego.getIndRespuesta()) {
            MisionJuego.IntervalosTiempoRespuesta intervalo;
            boolean flag = true;//bandera indicando si se aceptan nuevas rediciones (por defecto true)

            //verificacion de intervalos de respuesta totales
            intervalo = MisionJuego.IntervalosTiempoRespuesta.get(misionJuego.getIndInterTotal());
            if (intervalo != null) {
                flag = checkIntervaloRespuestaGaneMision(null, misionJuego.getMetrica().getIdMetrica(), misionJuego.getIdJuego().getIdMision(), intervalo, misionJuego.getCantInterTotal());
            }

            //verificacion de intervalos de respuesta totales por miembro..
            intervalo = MisionJuego.IntervalosTiempoRespuesta.get(misionJuego.getIndInterMiembro());
            if (intervalo != null && flag) {
                flag = checkIntervaloRespuestaGaneMision(idMiembro, misionJuego.getMetrica().getIdMetrica(), misionJuego.getIdJuego().getIdMision(), intervalo, misionJuego.getCantInterMiembro());
            }
            return flag;
        }
        return true;
    }

    private boolean checkIntervaloRespuestaGaneMision(String idMiembro, String idMetrica, String idMision, MisionJuego.IntervalosTiempoRespuesta intervalo, Long cantTotal) {
        List<RegistroMetricaPK> registros = new ArrayList<>();
        if (idMiembro == null) {
            registros = registroMetricaService.getGaneMetricaMision(null, idMetrica, idMision);
        } else {
            registros = registroMetricaService.getGaneMetricaMision(idMiembro, idMetrica, idMision);
        }

        long cantidadRespuestas = 0;
        switch (intervalo) {
            case POR_SIEMPRE: {
                cantidadRespuestas = registros.size();
                break;
            }
            case POR_MES: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -1);
                cantidadRespuestas = registros.stream().filter(t -> ((t.getFecha().after(calendar.getTime()) || t.getFecha().equals(calendar.getTime()))
                        && (t.getFecha().before(new Date()) || t.getFecha().equals(new Date()))))
                        .collect(Collectors.toList()).size();
                break;
            }
            case POR_SEMANA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.WEEK_OF_YEAR, -1);
                cantidadRespuestas = registros.stream().filter(t -> ((t.getFecha().after(calendar.getTime()) || t.getFecha().equals(calendar.getTime()))
                        && (t.getFecha().before(new Date()) || t.getFecha().equals(new Date()))))
                        .collect(Collectors.toList()).size();
                break;
            }
            case POR_DIA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                cantidadRespuestas = registros.stream().filter(t -> ((t.getFecha().after(calendar.getTime()) || t.getFecha().equals(calendar.getTime()))
                        && (t.getFecha().before(new Date()) || t.getFecha().equals(new Date()))))
                        .collect(Collectors.toList()).size();
                break;
            }
            case POR_HORA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.HOUR_OF_DAY, -1);
                cantidadRespuestas = registros.stream().filter(t -> ((t.getFecha().after(calendar.getTime()) || t.getFecha().equals(calendar.getTime()))
                        && (t.getFecha().before(new Date()) || t.getFecha().equals(new Date()))))
                        .collect(Collectors.toList()).size();

                break;
            }
            case POR_MINUTO: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MINUTE, -1);
                cantidadRespuestas = registros.stream().filter(t -> ((t.getFecha().after(calendar.getTime()) || t.getFecha().equals(calendar.getTime()))
                        && (t.getFecha().before(new Date()) || t.getFecha().equals(new Date()))))
                        .collect(Collectors.toList()).size();
                break;
            }
        }
        //mientras que la cantidad de respuestas siga siendo menor a la cantidad de respuestas totales aceptables..
        return cantidadRespuestas < cantTotal;
    }

    private String ganePremioConMision(String idPremio, String idMiembro, Locale locale) {
        CodigoCertificado codigoCertificado = null;
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }
        Miembro miembro = em.find(Miembro.class, idMiembro);
        if (miembro == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        //verificacion de la elegibilidad de redimir el premio
        PremioControl premioControl = em.find(PremioControl.class, premio.getIdPremio());
        if (!(premioControl.getIndEstado().equals(PremioControl.Estados.PUBLICADO.getValue()))) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "reward_no_available");
        }

        PremioMiembro premioMiembro = new PremioMiembro();
        premioMiembro.setEstado(PremioMiembro.Estados.REDIMIDO_SIN_RETIRAR.getValue());
        premioMiembro.setFechaAsignacion(Calendar.getInstance().getTime());
        premioMiembro.setIdMiembro(miembro);
        premioMiembro.setIndMotivoAsignacion(PremioMiembro.Motivos.PREMIO_MISION.getValue());
        premioMiembro.setIdPremio(premio);
        if (premio.getIndTipoPremio().equals(Premio.Tipo.CERTIFICADO.getValue())) {
            List<CodigoCertificado> certificados = em.createNamedQuery("CodigoCertificado.findByIdPremio")
                    .setParameter("idPremio", premio.getIdPremio())
                    .setParameter("indEstado", CodigoCertificado.Estados.DISPONIBLE.getValue())
                    .getResultList();
            if (!certificados.isEmpty()) {
                codigoCertificado = certificados.get(0);
                premioMiembro.setCodigoCertificado(codigoCertificado.getCodigoCertificadoPK().getCodigo());
                codigoCertificado.setIndEstado(CodigoCertificado.Estados.OCUPADO.getValue());
            } else {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "certificate_not_available");
            }
            em.merge(codigoCertificado);
        }
        //persisto
        try {
            em.persist(premioMiembro);
            em.flush();
            premioBean.afectarExistenciasHistorico(idPremio, idMiembro, premioMiembro.getIdPremioMiembro(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        //retorno
        if (codigoCertificado != null) {
            return codigoCertificado.getCodigoCertificadoPK().getCodigo();
        } else {
            return null;
        }
    }

    public String getCodigoAleatorio() {
        String numero = "";
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            int num = random.nextInt(99);
            if (num < 10) {
                numero += "0" + num + "-";
            } else {
                numero += num + "-";
            }
            if (i < 4) {
                numero += "-";
            }
        }
        if (existsNumeroGenerado(numero)) {
            getCodigoAleatorio();
        }
        return numero;
    }

    public boolean existsNumeroGenerado(String numero) {
        return ((Long) em.createNamedQuery("PremioMiembro.countCodigoGenerado").setParameter("codigo", numero).getSingleResult()) > 0;
    }

    /**
     * metodo que obtiene la fecha del ultimo registro de respuesta de la mision
     *
     * @param idMiembro identificador del miembro
     * @param idMision identificador de la mision
     * @return fecha del ultimo registro
     */
    public Date getFechaUltimoRegistroRespuestaMision(String idMiembro, String idMision) {
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "REGISTRO_MISION"));

            //se define un escaner sobre files con un prefijo (regex) en el rowkey (que inicie con el identificador de mision + miembro), en un orden descendiente (se ordenan por el ultimo valor pendiente... fecha)
            Scan scan = new Scan();
            FilterList filterList = new FilterList(new RowFilter(CompareFilter.CompareOp.EQUAL, new RegexStringComparator(idMision + "&" + idMiembro + "&.*")), new KeyOnlyFilter());
            scan.setFilter(filterList);
            scan.setReversed(true);

            //se obtiene el primer resultado... si existe
            Result result;
            try (ResultScanner scanner = table.getScanner(scan)) {
                result = scanner.next();
                if (result == null || result.isEmpty()) {
                    return null;
                }
            }

            //se recupera la fecha del rowkey
            return new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[2]));
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, Locale.getDefault(), "database_connection_failed");
        }
    }

    /**
     * Verifica si el miembro ha completado una mision
     *
     * @param idMision
     * @param idMiembro
     * @return
     * @throws IOException
     */
    public boolean isMisionCompletada(String idMision, String idMiembro) {
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "REGISTRO_MISION"));

            //se define un escaner sobre files con un prefijo (regex) en el rowkey (que inicie con el identificador de mision + miembro), en un orden descendiente (se ordenan por el ultimo valor pendiente... fecha)
            Scan scan = new Scan();
            FilterList filterList = new FilterList(new RowFilter(CompareFilter.CompareOp.EQUAL, new RegexStringComparator(idMision + "&" + idMiembro + "&.*")), new KeyOnlyFilter());
            scan.setFilter(filterList);

            //se obtiene el primer resultado... si existe
            Result result;
            try (ResultScanner scanner = table.getScanner(scan)) {
                result = scanner.next();
                return !(result == null || result.isEmpty());
            }
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, Locale.getDefault(), "database_connection_failed");
        }
    }

    /**
     * metodo que cuenta los registros de respuesta de misiones
     *
     * @param idMiembro identificador opcional del miembro
     * @param idMision identificador de la mision
     * @param fechaInicio fecha opcional de registro inicial
     * @param fechaFinal fecha opcional de registro final
     * @return cantidad de registros
     */
    public long countRegistrosRespuestaMision(String idMiembro, String idMision, Date fechaInicio, Date fechaFinal) {
        //se establece el regex inicial a partir de los valores de identificador de miembro y mision
        String regex;
        if (idMiembro == null) {
            //de no estar presente el identificador de miembro...
            regex = idMision + "&.*&";
        } else {
            regex = idMision + "&" + idMiembro + "&";
        }

        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "REGISTRO_MISION"));

            //se establece el regex final a partir de los valores de fecha y el escaner en rango de registros si se amerita
            Scan scan;
            if (fechaInicio != null || fechaFinal != null) {
                //si la fecha de inicio y/o final se establecieron...
                if (fechaInicio != null && fechaFinal != null) {
                    //si ambas fechas se establecieron, se define el escaner en un rango de registros
                    scan = new Scan(Bytes.toBytes(regex + String.format("%013d", fechaInicio.getTime())), Bytes.toBytes(regex + String.format("%013d", fechaFinal.getTime() + 1)));
                    scan.setFilter(new KeyOnlyFilter());
                } else {
                    if (fechaInicio != null) {
                        //si la fecha de inicio fue la definida, se define el escaner a partir de un proximo registro
                        scan = new Scan(Bytes.toBytes(regex + String.format("%013d", fechaInicio.getTime())));
                        scan.setFilter(new KeyOnlyFilter());
                    } else {
                        //si la fecha final fue la definida, se define el escaner en un rango de registros (con el inicial en el valor minimo)
                        scan = new Scan(Bytes.toBytes(regex + String.format("%013d", new Date(0).getTime())), Bytes.toBytes(regex + String.format("%013d", fechaFinal.getTime() + 1)));
                        scan.setFilter(new KeyOnlyFilter());
                    }
                }
            } else {
                //si no se establecieron ninguna fecha, se establece un filtro sobre el escaner por prefijo del rowkey con el valor inicial del regex inicial
                scan = new Scan();
                FilterList filterList = new FilterList(new RowFilter(CompareFilter.CompareOp.EQUAL, new RegexStringComparator(regex)), new KeyOnlyFilter());
                scan.setFilter(filterList);
            }

            //se realiza el conteo de registros...
            long count = 0;
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    count++;
                }
            }

            return count;
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, Locale.getDefault(), "database_connection_failed");
        }
    }

    /**
     * registra un registro de respuesta de mision
     *
     * @param idMiembro identificador del miembro
     * @param idMision identificador de la mision
     * @param respuestaMision respuesta de la mision
     * @param estadoRespuesta estado de la respuesta de la mision
     */
    public void insertRegistroRespuestaMision(String idMiembro, String idMision, RespuestaMision respuestaMision, RespuestaMision.EstadosRespuesta estadoRespuesta) {
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "REGISTRO_MISION"));

            //se establece el registro a crear en la fecha/hora actual
            Put put = new Put(Bytes.toBytes(idMision + "&" + idMiembro + "&" + String.format("%013d", new Date().getTime())));
            put.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"), Bytes.toBytes(estadoRespuesta.getValue()));
            put.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("RESPUESTA"), Bytes.toBytes(new Gson().toJson(respuestaMision)));
            //se almacena...
            table.put(put);
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, Locale.getDefault(), "database_connection_failed");
        }
    }

    /**
     * registra un registro de vista de mision
     *
     * @param idMiembro identificador del miembro
     * @param idMision identificador de la mision
     */
    public void insertRegistroVistaMision(String idMiembro, String idMision) {
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "VISTAS_MISION"));

            //se establece el registro con la fecha actual
            byte[] rowkey = Bytes.toBytes(String.format("%013d", new Date().getTime()) + "&" + idMiembro);
            Put put = new Put(rowkey);
            put.add(new KeyValue(rowkey, Bytes.toBytes("DATA"), Bytes.toBytes("MISION"), Bytes.toBytes(idMision)));
            //se almacena el registro
            table.put(put);
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, Locale.getDefault(), "database_connection_failed");
        }
    }

    private boolean checkEligibilidadWorkflow(String idMiembro, String idWorkflow, List<String> segmentosPertenecientes) {
        //se consulta si existe un registro de inclusion/exclusion del miembro para workflow
        WorkflowListaMiembro miembroIncluidoExcluido = em.find(WorkflowListaMiembro.class, new WorkflowListaMiembroPK(idMiembro, idWorkflow));
        if (miembroIncluidoExcluido != null) {
            //si existe... se filtra por su tipo... (inclusion o exclusion)
            switch (miembroIncluidoExcluido.getIndTipo()) {
                case Indicadores.INCLUIDO: {
                    return true;
                }
                case Indicadores.EXCLUIDO: {
                    return false;
                }
            }
        }
      
        if (!segmentosPertenecientes.isEmpty()) {
            List<List<String>> partitionsSegmentos;
            partitionsSegmentos = Lists.partition(segmentosPertenecientes, 999);
            if (partitionsSegmentos.stream()
                    .anyMatch((l) -> ((long) em.createNamedQuery("WorkflowListaSegmento.countByIdWorkflowAndSegmentosInListaAndByIndTipo")
                    .setParameter("idWorkflow", idWorkflow)
                    .setParameter("lista", l)
                    .setParameter("indTipo", Indicadores.EXCLUIDO)
                    .getSingleResult() > 0))) {
                return false;
            }
            if (partitionsSegmentos.stream()
                    .anyMatch((l) -> ((long) em.createNamedQuery("WorkflowListaSegmento.countByIdWorkflowAndSegmentosInListaAndByIndTipo")
                    .setParameter("idWorkflow", idWorkflow)
                    .setParameter("lista", l)
                    .setParameter("indTipo", Indicadores.INCLUIDO)
                    .getSingleResult() > 0))) {
                return true;
            }
        }
        return (long) em.createNamedQuery("WorkflowListaSegmento.countByIdWorkflowAndByIndTipo")
                .setParameter("idWorkflow", idWorkflow)
                .setParameter("indTipo", Indicadores.INCLUIDO)
                .getSingleResult() == 0;
    }

    private MisionDetails getDetailsMisionSecuencia(String idMision, String idMiembro, Locale locale) {
        MisionDetails detalle = em.find(MisionDetails.class, idMision);
        switch (MisionControl.TiposMision.get(detalle.getIndTipoMision())) {
            case PERFIL: {
                Miembro miembro = em.find(Miembro.class, idMiembro);
                for (MisionPerfilAtributo misionPerfilAtributo : detalle.getMisionPerfilAtributos()) {
                    MisionPerfilAtributo.Atributos tipoAtributo = MisionPerfilAtributo.Atributos.get(misionPerfilAtributo.getIndAtributo());
                    switch (tipoAtributo) {
                        case ATRIBUTO_DINAMICO: {
                            AtributoDinamico atributoDinamico = misionPerfilAtributo.getAtributoDinamico();
                            MiembroAtributo miembroAtributo = em.find(MiembroAtributo.class, new MiembroAtributoPK(idMiembro, atributoDinamico.getIdAtributo()));
                            if (miembroAtributo == null) {
                                misionPerfilAtributo.setRespuesta(atributoDinamico.getValorDefecto());
                            } else {
                                misionPerfilAtributo.setRespuesta(miembroAtributo.getValor());
                            }
                            break;
                        }
                        case ESTADO_CIVIL: {
                            misionPerfilAtributo.setRespuesta(miembro.getIndEstadoCivil() == null ? null : miembro.getIndEstadoCivil().toString());
                            break;
                        }
                        case GENERO: {
                            misionPerfilAtributo.setRespuesta(miembro.getIndGenero() == null ? null : miembro.getIndGenero().toString());
                            break;
                        }
                        case TIPO_DE_EDUCACION: {
                            misionPerfilAtributo.setRespuesta(miembro.getIndEducacion() == null ? null : miembro.getIndEducacion().toString());
                            break;
                        }
                        case PAIS_DE_RESIDENCIA: {
                            misionPerfilAtributo.setRespuesta(miembro.getPaisResidencia());
                            break;
                        }
                        case FECHA_DE_NACIMIENTO: {
                            misionPerfilAtributo.setRespuesta(miembro.getFechaNacimiento() == null ? null : miembro.getFechaNacimiento().getTime() + "");
                            break;
                        }
                        case INGRESO_ECONOMICO: {
                            misionPerfilAtributo.setRespuesta(miembro.getIngresoEconomico() == null ? null : miembro.getIngresoEconomico().toString());
                            break;
                        }
                        case HIJOS: {
                            misionPerfilAtributo.setRespuesta(miembro.getIndHijos() == null ? "FALSE" : miembro.getIndHijos() == true ? "TRUE" : "FALSE");
                            break;
                        }
                        case CIUDAD_DE_RESIDENCIA: {
                            misionPerfilAtributo.setRespuesta(miembro.getCiudadResidencia());
                            break;
                        }
                        case CODIGO_POSTAL: {
                            misionPerfilAtributo.setRespuesta(miembro.getCodigoPostal());
                            break;
                        }
                        case ESTADO_DE_RESIDENCIA: {
                            misionPerfilAtributo.setRespuesta(miembro.getEstadoResidencia());
                            break;
                        }
                        case NOMBRE: {
                            misionPerfilAtributo.setRespuesta(miembro.getNombre());
                            break;
                        }
                        case PRIMER_APELLIDO: {
                            misionPerfilAtributo.setRespuesta(miembro.getApellido());
                            break;
                        }
                        case SEGUNDO_APELLIDO: {
                            misionPerfilAtributo.setRespuesta(miembro.getApellido2());
                            break;
                        }
                        case FRECUENCIA_COMPRA: {
                            misionPerfilAtributo.setRespuesta(miembro.getFrecuenciaCompra());
                            break;
                        }
                        default: {
                            misionPerfilAtributo.setRespuesta(null);
                        }
                    }
                }
                break;
            }
            case PREFERENCIAS: {
                List<Preferencia> preferencias = em.createNamedQuery("MisionPreferencia.getPreferenciaByIdMision").setParameter("idMision", detalle.getIdMision()).getResultList();
                preferencias.forEach((preferencia) -> {
                    try {
                        preferencia.setRespuestaMiembro((String) em.createNamedQuery("RespuestaPreferencia.getRespuestaByIdMiembroIdPreferencia").setParameter("idMiembro", idMiembro).setParameter("idPreferencia", preferencia.getIdPreferencia()).getSingleResult());
                    } catch (NoResultException e) {
                    }
                });
                detalle.setMisionEncuestaPreferencias(preferencias);
                break;
            }
            case JUEGO: {
                Juego juego = em.find(Juego.class, detalle.getIdMision());
                if (juego != null) {
                    List<MisionJuego> misionJuegos = em.createNamedQuery("MisionJuego.findByIdMision").setParameter("idMision", detalle.getIdMision()).getResultList();
                    if (juego.getTipo().equals(Juego.Tipo.RASPADITA.getValue())) {
                        Juego.Estrategia estrategia = Juego.Estrategia.get(juego.getEstrategia());
                        switch (estrategia) {
                            case PROBABILIDAD: {
                                MisionJuego misionGanar = this.getPremioGanadorProbabilidad(juego, idMiembro, locale);
                                if (misionGanar == null) {
                                    boolean flag = false;
                                    for (MisionJuego misionJuego : misionJuegos) {
                                        if (flag == false && misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                            flag = true;
                                            misionJuego.setIsGanar(Boolean.TRUE);
                                        } else {
                                            misionJuego.setIsGanar(Boolean.FALSE);
                                        }
                                    }
                                } else {
                                    misionJuegos.forEach((misionJuego) -> {
                                        if (misionJuego.getIdMisionJuego().equals(misionGanar.getIdMisionJuego())) {
                                            misionJuego.setIsGanar(Boolean.TRUE);
                                        } else {
                                            misionJuego.setIsGanar(Boolean.FALSE);
                                        }
                                    });
                                }
                                break;
                            }
                            case RANDOM: {
                                MisionJuego misionGanar = this.getPremioGanadorRandom(juego, idMiembro, locale);
                                if (misionGanar == null) {
                                    misionJuegos.forEach((misionJuego) -> {
                                        if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                            misionJuego.setIsGanar(Boolean.TRUE);
                                        } else {
                                            misionJuego.setIsGanar(Boolean.FALSE);
                                        }
                                    });
                                } else {
                                    misionJuegos.forEach((misionJuego) -> {
                                        if (misionJuego.getIdMisionJuego().equals(misionGanar.getIdMisionJuego())) {
                                            misionJuego.setIsGanar(Boolean.TRUE);
                                        } else {
                                            misionJuego.setIsGanar(Boolean.FALSE);
                                        }
                                    });
                                }
                                break;
                            }
                        }
                    } else if (juego.getTipo().equals(Juego.Tipo.ROMPECABEZAS.getValue())) {
                        if (juego.getImagenesRompecabezas() != null) {
                            String[] split = juego.getImagenesRompecabezas().split(",");
                            List<ImagenesRompecabezas> lista = new ArrayList<>();
                            for (String url : split) {
                                lista.add(new ImagenesRompecabezas(url.trim()));
                            }
                            juego.setImagenes(lista);
                        }
                    }
                    juego.setMisionJuego(misionJuegos);
                }
                detalle.setJuego(juego);
                break;
            }
        }
        return detalle;
    }
    
    
    /**
     * Método que devuelve una lista de misiones en las cuales el miembro es
     * elegible, ordenadas por el estado dando prioridad a las no completadas
     *
     * @param idMiembro identificador del miembro
     * @param locale
     * @param estado
     * @return lista de misiones resultantes
     */
    public List<MisionDetails> getMisionesPorEstado(String idMiembro, Locale locale, String estado) {
        //obtencion de todas las misiones dentro de la categoria de mision
        List<MisionControl> preMisiones = em.createNamedQuery("MisionControl.findAllByIndEstado").setParameter("indEstado", MisionControl.Estados.PUBLICADO.getValue()).getResultList();
        List<MisionControl> misiones;
        System.out.println("Estado: " + estado);
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            misiones = preMisiones.stream().filter((MisionControl m)
                -> {
                boolean exist = false;
                try {
                    Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "REGISTRO_MISION"));
                    Scan scan = new Scan();
                    //Put put = new Put(Bytes.toBytes(idMision + "&" + idMiembro + "&" + String.format("%013d", new Date().getTime())));
                    FilterList filterList = new FilterList(
                            new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes(estado)),
                            new PrefixFilter(Bytes.toBytes(m.getIdMision() + "&" + idMiembro))
                    );
                    System.out.println("BUSCAR: " + m.getIdMision() + "&" + idMiembro);
                    
                    scan.setFilter(filterList);
                    scan.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"));
                    scan.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("RESPUESTA"));
                    
                    try {
                        ResultScanner scanner = table.getScanner(scan);
                        for (Result result : scanner) {
                            System.out.println("result: " + m.getIdMision());
                            exist = true;
                        }
                    } catch (IOException ex) {
                        exist = false;
                    }
                } catch (IOException ex) {
                    System.out.println("Error with hbase: " + ex.getMessage());
                }
                return exist;
            }).collect(Collectors.toList());
        } catch (Exception e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }

        //filtrado y conversion de cada mision....
        List<MisionDetails> resultado = misiones.stream()
                .map((MisionControl m) -> {
                    MisionDetails detalle = em.find(MisionDetails.class, m.getIdMision());

                    switch (MisionControl.TiposMision.get(detalle.getIndTipoMision())) {
                        case PERFIL: {
                            Miembro miembro = em.find(Miembro.class, idMiembro);
                            for (MisionPerfilAtributo misionPerfilAtributo : detalle.getMisionPerfilAtributos()) {
                                MisionPerfilAtributo.Atributos tipoAtributo = MisionPerfilAtributo.Atributos.get(misionPerfilAtributo.getIndAtributo());
                                switch (tipoAtributo) {
                                    case ATRIBUTO_DINAMICO: {
                                        AtributoDinamico atributoDinamico = misionPerfilAtributo.getAtributoDinamico();
                                        MiembroAtributo miembroAtributo = em.find(MiembroAtributo.class, new MiembroAtributoPK(idMiembro, atributoDinamico.getIdAtributo()));
                                        if (miembroAtributo == null) {
                                            misionPerfilAtributo.setRespuesta(atributoDinamico.getValorDefecto());
                                        } else {
                                            misionPerfilAtributo.setRespuesta(miembroAtributo.getValor());
                                        }
                                        break;
                                    }
                                    case ESTADO_CIVIL: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIndEstadoCivil() == null ? null : miembro.getIndEstadoCivil().toString());
                                        break;
                                    }
                                    case GENERO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIndGenero() == null ? null : miembro.getIndGenero().toString());
                                        break;
                                    }
                                    case TIPO_DE_EDUCACION: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIndEducacion() == null ? null : miembro.getIndEducacion().toString());
                                        break;
                                    }
                                    case PAIS_DE_RESIDENCIA: {
                                        misionPerfilAtributo.setRespuesta(miembro.getPaisResidencia());
                                        break;
                                    }
                                    case FECHA_DE_NACIMIENTO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getFechaNacimiento() == null ? null : miembro.getFechaNacimiento().getTime() + "");
                                        break;
                                    }
                                    case INGRESO_ECONOMICO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIngresoEconomico() == null ? null : miembro.getIngresoEconomico().toString());
                                        break;
                                    }
                                    case HIJOS: {
                                        misionPerfilAtributo.setRespuesta(miembro.getIndHijos() == null ? "FALSE" : miembro.getIndHijos() == true ? "TRUE" : "FALSE");
                                        break;
                                    }
                                    case CIUDAD_DE_RESIDENCIA: {
                                        misionPerfilAtributo.setRespuesta(miembro.getCiudadResidencia());
                                        break;
                                    }
                                    case CODIGO_POSTAL: {
                                        misionPerfilAtributo.setRespuesta(miembro.getCodigoPostal());
                                        break;
                                    }
                                    case ESTADO_DE_RESIDENCIA: {
                                        misionPerfilAtributo.setRespuesta(miembro.getEstadoResidencia());
                                        break;
                                    }
                                    case NOMBRE: {
                                        misionPerfilAtributo.setRespuesta(miembro.getNombre());
                                        break;
                                    }
                                    case PRIMER_APELLIDO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getApellido());
                                        break;
                                    }
                                    case SEGUNDO_APELLIDO: {
                                        misionPerfilAtributo.setRespuesta(miembro.getApellido2());
                                        break;
                                    }
                                    case FRECUENCIA_COMPRA: {
                                        misionPerfilAtributo.setRespuesta(miembro.getFrecuenciaCompra());
                                        break;
                                    }
                                    default: {
                                        misionPerfilAtributo.setRespuesta(null);
                                    }
                                }
                            }
                            break;
                        }
                        case PREFERENCIAS: {
                            List<Preferencia> preferencias = em.createNamedQuery("MisionPreferencia.getPreferenciaByIdMision").setParameter("idMision", detalle.getIdMision()).getResultList();
                            preferencias.forEach((preferencia) -> {
                                try {
                                    preferencia.setRespuestaMiembro((String) em.createNamedQuery("RespuestaPreferencia.getRespuestaByIdMiembroIdPreferencia").setParameter("idMiembro", idMiembro).setParameter("idPreferencia", preferencia.getIdPreferencia()).getSingleResult());
                                } catch (NoResultException e) {
                                }
                            });
                            detalle.setMisionEncuestaPreferencias(preferencias);
                            break;
                        }
                        case JUEGO: {
                            Juego juego = em.find(Juego.class, detalle.getIdMision());
                            if (juego != null) {
                                List<MisionJuego> misionJuegos = em.createNamedQuery("MisionJuego.findByIdMision").setParameter("idMision", detalle.getIdMision()).getResultList();
                                if (juego.getTipo().equals(Juego.Tipo.RASPADITA.getValue())) {
                                    Juego.Estrategia estrategia = Juego.Estrategia.get(juego.getEstrategia());
                                    switch (estrategia) {
                                        case PROBABILIDAD: {
                                            MisionJuego misionGanar = this.getPremioGanadorProbabilidad(juego, idMiembro, locale);
                                            if (misionGanar == null) {
                                                boolean flag = false;
                                                for (MisionJuego misionJuego : misionJuegos) {
                                                    if (flag == false && misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                                        flag = true;
                                                        misionJuego.setIsGanar(Boolean.TRUE);
                                                    } else {
                                                        misionJuego.setIsGanar(Boolean.FALSE);
                                                    }
                                                }

                                            } else {
                                                misionJuegos.forEach((misionJuego) -> {
                                                    if (misionJuego.getIdMisionJuego().equals(misionGanar.getIdMisionJuego())) {
                                                        misionJuego.setIsGanar(Boolean.TRUE);
                                                    } else {
                                                        misionJuego.setIsGanar(Boolean.FALSE);
                                                    }
                                                });
                                            }
                                            break;
                                        }
                                        case RANDOM: {
                                            MisionJuego misionGanar = this.getPremioGanadorRandom(juego, idMiembro, locale);
                                            if (misionGanar == null) {
                                                misionJuegos.forEach((misionJuego) -> {
                                                    if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                                                        misionJuego.setIsGanar(Boolean.TRUE);
                                                    } else {
                                                        misionJuego.setIsGanar(Boolean.FALSE);
                                                    }
                                                });
                                            } else {
                                                misionJuegos.forEach((misionJuego) -> {
                                                    if (misionJuego.getIdMisionJuego().equals(misionGanar.getIdMisionJuego())) {
                                                        misionJuego.setIsGanar(Boolean.TRUE);
                                                    } else {
                                                        misionJuego.setIsGanar(Boolean.FALSE);
                                                    }
                                                });
                                            }
                                            break;
                                        }
                                    }
                                } else if (juego.getTipo().equals(Juego.Tipo.ROMPECABEZAS.getValue())) {
                                    if (juego.getImagenesRompecabezas() != null) {
                                        String[] split = juego.getImagenesRompecabezas().split(",");
                                        List<ImagenesRompecabezas> lista = new ArrayList<>();
                                        for (String url : split) {
                                            lista.add(new ImagenesRompecabezas(url.trim()));
                                        }
                                        juego.setImagenes(lista);
                                    }
                                }
                                juego.setMisionJuego(misionJuegos);
                            }
                            detalle.setJuego(juego);
                            break;
                        }
                    }

                    return detalle;
                }).collect(Collectors.toList());

        return resultado;
    }
}
