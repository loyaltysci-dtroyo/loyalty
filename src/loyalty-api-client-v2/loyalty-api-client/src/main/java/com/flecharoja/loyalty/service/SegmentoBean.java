package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author svargas
 */
@Stateless
public class SegmentoBean {

    private final Configuration hbaseConf;

    public SegmentoBean() {
        this.hbaseConf = new Configuration();
        this.hbaseConf.addResource("conf/hbase/hbase-site.xml");
    }
    
    /**
     * metodo que obtiene los segmentos donde un miembro es perteneciente
     *
     * @param idMiembro identificador del miembro
     * @return lista de identificadores de segmentos
     */
    public List<String> getSegmentosPertenecientesMiembro(String idMiembro) {
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "ELEGIBILIDAD_MIEMBRO"));

            //se obtiene la fila (solo la columna familia de segmentos) con el rowkey igual al valor del identificador del miembro
            Get get = new Get(Bytes.toBytes(idMiembro));
            get.addFamily(Bytes.toBytes("SEGMENTOS"));
            Result result = table.get(get);

            //en caso de que no se haya encontrado alguna fila...
            if (result.isEmpty()) {
                return new ArrayList<>();
            }

            //por cada entrada de segmento (columna & valor), se filtran los que indiquen que el miembro pertenece y se mapea al texto de los identificadores de segmento
            return result.getFamilyMap(Bytes.toBytes("SEGMENTOS"))
                    .entrySet()
                    .stream()
                    .filter((t) -> Bytes.toBoolean(t.getValue()))
                    .map((t) -> Bytes.toString(t.getKey()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, Locale.getDefault(), "database_connection_failed");
        }
    }
}
