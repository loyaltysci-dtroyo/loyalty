package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@ApiModel(value = "RegistroComentario")
@XmlRootElement
public class NewNoticiaComentario implements Serializable {

    @ApiModelProperty(value = "Texto contenido del comentario", required = true)
    private String contenido;

    public NewNoticiaComentario() {
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

}
