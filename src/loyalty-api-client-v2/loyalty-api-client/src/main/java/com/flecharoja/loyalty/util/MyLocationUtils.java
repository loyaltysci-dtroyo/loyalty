package com.flecharoja.loyalty.util;

/**
 *
 * @author svargas
 */
public class MyLocationUtils {

    public static double getDistanceFromPointsInMeters(double lat1, double lon1, double lat2, double lon2) {
        //formula de haversine, basado en http://www.movable-type.co.uk/scripts/latlong.html
        double R = 6371e3; //radio de la tierra en metros

        double dLat = (lat2 - lat1) * (Math.PI / 180);
        double dLon = (lon2 - lon1) * (Math.PI / 180);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(lat1 * (Math.PI / 180)) * Math.cos(lat2 * (Math.PI / 180))
                * Math.sin(dLon / 2) * Math.sin(dLon / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return R * c;
    }
}
