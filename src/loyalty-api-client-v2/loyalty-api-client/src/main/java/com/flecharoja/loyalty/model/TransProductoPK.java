package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author svargas
 */
@Embeddable
public class TransProductoPK implements Serializable {

    @Column(name = "NUM_TRANSACCION")
    private String numTransaccion;
    
    @Column(name = "ID_PRODUCTO")
    private String idProducto;

    public TransProductoPK() {
    }

    public TransProductoPK(String numTransaccion, String idProducto) {
        this.numTransaccion = numTransaccion;
        this.idProducto = idProducto;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public String getNumTransaccion() {
        return numTransaccion;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public void setNumTransaccion(String numTransaccion) {
        this.numTransaccion = numTransaccion;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numTransaccion != null ? numTransaccion.hashCode() : 0);
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransProductoPK)) {
            return false;
        }
        TransProductoPK other = (TransProductoPK) object;
        if ((this.numTransaccion == null && other.numTransaccion != null) || (this.numTransaccion != null && !this.numTransaccion.equals(other.numTransaccion))) {
            return false;
        }
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.TransProductoPK[ numTransaccion=" + numTransaccion + ", idProducto=" + idProducto + " ]";
    }
    
}
