package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.InstanciaPreguntaMillonario;
import com.flecharoja.loyalty.service.InstanciaPreguntaMillonarioBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio, svargas
 */
@Api(value = "Instancia Pregunta millonario")
@Path("instancia-pregunta-millonario")
public class InstanciaPreguntaMillonarioResource {

    @EJB
    InstanciaPreguntaMillonarioBean instanciaPreguntaMillonarioBean;

    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;

    /**
     * Método que obtiene la lista de preguntas de una instancia
     *
     * @param idInstanciaMillonario
     * @return informacion de una instancia
     */
    @ApiOperation(value = "Obtener las preguntas de una instancia de juego millonario",
            response = List.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("{idInstanciaMillonario}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<InstanciaPreguntaMillonario> getPreguntasInstanciaMillonario(
        @ApiParam(value = "Identificador de la instancia")
        @PathParam("idInstanciaMillonario") String idInstanciaMillonario
    ) {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
       return instanciaPreguntaMillonarioBean
               .getPreguntasInstanciaMillonario(idInstanciaMillonario, request.getLocale());
    }
    
    /**
     * Método que obtiene la lista de preguntas de una instancia de manera random
     *
     * @param idInstanciaMillonario
     * @return informacion de una instancia
     */
    @ApiOperation(value = "Obtener las preguntas de una instancia de juego millonario",
            response = List.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("random/{idInstanciaMillonario}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<InstanciaPreguntaMillonario> getPreguntasInstanciaMillonarioRandom(
        @ApiParam(value = "Identificador de la instancia")
        @PathParam("idInstanciaMillonario") String idInstanciaMillonario
    ) {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
       return instanciaPreguntaMillonarioBean
               .getPreguntasInstanciaMillonarioRandom(idInstanciaMillonario, request.getLocale());
    }
}
