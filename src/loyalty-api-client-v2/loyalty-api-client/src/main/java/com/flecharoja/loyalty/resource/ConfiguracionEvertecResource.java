package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.service.ConfiguracionEvertecBean;
import com.flecharoja.loyalty.util.Indicadores;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "ConfiguracionEvertec")
@Path("configuracionEvertec")
public class ConfiguracionEvertecResource {

    @EJB
    ConfiguracionEvertecBean configuracionEvertecBean;

    @Context
    SecurityContext context;

    @Context
    HttpServletRequest request;

    /**
     * Método que obtiene la configuración general para evertec
     *
     * @return configuración con la información de evertec
     */
    @ApiOperation(value = "Obtener la configuracion de evertec",
            responseContainer = "ConfiguracionGeneral",
            response = ConfiguracionGeneral.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ConfiguracionGeneral getConfiguracionEvertec() {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();

        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return configuracionEvertecBean.getConfiguracionEvertec(request.getLocale());
    }
}
