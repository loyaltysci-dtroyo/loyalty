/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;


import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CertificadoCategoria;
import com.flecharoja.loyalty.model.TransCarrito;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class TransCarritoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Método que obtiene un carrito disponible
     *
     * @param idMiembro
     * @param idCategoriaProducto
     * @param locale
     * @return Carritos
     */
    public List<TransCarrito> getTransCarrito(String idMiembro, String idCategoriaProducto, Locale locale) {
        
        //Obtener de todas los carritos 
        List<TransCarrito> carritos = em.createNamedQuery("TransCarrito.findAvailable").setParameter("idMiembro", idMiembro).setParameter("idCategoria", idCategoriaProducto).setParameter("estado", 'P').getResultList();
        if (carritos.isEmpty()) {
            return carritos;
        }
        CertificadoCategoria certificado = em.find(CertificadoCategoria.class, carritos.get(0).getIdCertificado());
        if (certificado != null) {
            carritos.get(0).setNumCertificado(certificado.getNumCertificado());
        }
        return carritos;
    }
    
    /**
     * Método que obtiene los últimos 10 carritos de 1 usuario
     *
     * @param idMiembro
     * @param locale
     * @return Carritos
     */
    public List<TransCarrito> getTransCarritos(String idMiembro, Locale locale) {
        
        //Obtener de todas los carritos 
        List <TransCarrito> carritos = em.createNamedQuery("TransCarrito.findRecently").setParameter("idMiembro", idMiembro).setParameter("estado", 'C').getResultList();
        int cantidad = carritos.size();
        cantidad = cantidad > 10 ? 10 : cantidad;
        List<TransCarrito> newCarritos = new ArrayList();
        for(int i = 0; i < cantidad; i++) {
            CertificadoCategoria certificado = em.find(CertificadoCategoria.class, carritos.get(i).getIdCertificado());
            if (certificado != null) {
                carritos.get(i).setNumCertificado(certificado.getNumCertificado());
            }
            newCarritos.add(carritos.get(i));
        }
        
        return newCarritos;
    }
    
    /**
     * Método que inserta un nuevo carrito en la base de datos
     *
     * @param transCarrito
     * @param idMiembro identificador del usuario en sesion
     * @param idCategoriaProducto
     * @param certificadoCategoria
     * @param locale
     * @return identificador del producto registrado
     */
    public String insertTransCarrito(TransCarrito transCarrito, String idMiembro, String idCategoriaProducto, CertificadoCategoria certificadoCategoria,  Locale locale) {
        transCarrito.setIdMiembro(idMiembro);
        transCarrito.setUsuarioCreacion(idMiembro);
        transCarrito.setNumVersion(1);
        transCarrito.setIdCategoriaProducto(idCategoriaProducto);
        transCarrito.setEstado('P');
        transCarrito.setNumTransaccion(new Date().toString());
        transCarrito.setDatetime(new Date());
        transCarrito.setIdCertificado(certificadoCategoria.getIdCertificado());
        transCarrito.setCardId("000");
        transCarrito.setTotal(0.0);
        transCarrito.setSubTotal(0.0);
        transCarrito.setImp(0.0);
        
        Calendar expiracion = Calendar.getInstance();
        expiracion.add(Calendar.MINUTE, 10);
        CertificadoCategoria certificado = em.find(CertificadoCategoria.class, certificadoCategoria.getIdCertificado());
        certificado.setFechaExpiracion(expiracion.getTime());
        
        
        try {
            em.persist(transCarrito);
            em.flush();
            em.merge(certificado);
            em.flush();
        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        return transCarrito.getNumTransaccion();
    }
    
    /**
     * Método que inserta un nuevo carrito en la base de datos
     *
     * @param transCarrito
     * @param locale
     * @return estado de la transacción
     */
    public Boolean pagarTransCarrito(TransCarrito transCarrito,  Locale locale) {
        if (transCarrito.getNumTransaccion() == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "numTransaccion_notFound");
        }
        if (transCarrito.getSubTotal() == null || transCarrito.getTotal() == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "totalNotFound");
        }
        
        if (transCarrito.getCardId() == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "cardIdNotFound");
        }
        
        TransCarrito original = em.find(TransCarrito.class, transCarrito.getNumTransaccion());
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "transCarrito_not_found");
        }
        
        if (original.getEstado() == 'C') {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "transCarrito_already_payed");
        }
        
        if (transCarrito.getImp() == null) {
            original.setImp(0.0);
        }
        
        original.setCardId(transCarrito.getCardId());
        original.setTotal(transCarrito.getTotal());
        original.setSubTotal(transCarrito.getSubTotal());
        
        original.setEstado('C');
        try {
            em.merge(original);
            em.flush();
            return true;
        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }
    
    /**
     * Método que inserta un nuevo carrito en la base de datos
     *
     * @param numTransaccion
     * @return identificador del producto registrado
     */
    public String deleteTransCarrito(String numTransaccion) {
        TransCarrito transCarrito = em.find(TransCarrito.class, numTransaccion);
        if (transCarrito != null) {
            try {
                em.remove(transCarrito);
                em.flush();
            } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }
        }
        return numTransaccion;
    }
}
