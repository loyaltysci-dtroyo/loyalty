/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.model.UbicacionControl;
import com.google.common.primitives.Chars;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author wtencio
 */
@Stateless
public class UbicacionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Método que devuelve la informacion detallada con las ubicaciones
     * incluidas que tiene una region
     *
     * @param locale
     * @return informacion detallada de la region
     */
    public List<Ubicacion> getUbicaciones(Locale locale) {
        List<UbicacionControl> ubicaciones = em.createNamedQuery("UbicacionControl.findAll").getResultList();
        //filtrado por estado y por calendarizacion
        return ubicaciones.stream()
                .filter((UbicacionControl u) -> u.getIndEstado().equals(UbicacionControl.Estados.PUBLICADO.getValue())&& checkCalendarizacionUbicacion(u))
                .map((UbicacionControl u) -> em.find(Ubicacion.class, u.getIdUbicacion()))
                .collect(Collectors.toList());
    }

    

    /**
     * Método que verifica la calendarizacion de la ubicacion para saber si puede ser expuesta
     * @param ubicacion info de la ubicacion
     * @return true si esta disponible, false lo contrario
     */
    private boolean checkCalendarizacionUbicacion(UbicacionControl ubicacion) {
        //se establece el indicador de respuesta como verdadero (todo es valido mientras no sea negado)
        boolean flag = true;
        //se obtiene la fecha actual
        Calendar hoy = Calendar.getInstance();

        //si la ubicacion es calendarizada
        if (ubicacion.getIndCalendarizacion().equals(UbicacionControl.Efectividad.CALENDARIZADO.getValue())) {
            //si la fecha de inicio esta establecida, se comprueba que sea pasada o igual a hoy
            if (ubicacion.getFechaInicio() != null) {
                flag = hoy.getTime().after(ubicacion.getFechaInicio()) || DateUtils.isSameDay(hoy.getTime(), ubicacion.getFechaInicio());
            }
            //mientras siga siendo verdadero..
            //si la fecha de fin esta establecida, se verifica que se posterior o igual a hoy
            if(flag && ubicacion.getFechaFin() !=null){
                flag = hoy.getTime().before(ubicacion.getFechaFin()) || DateUtils.isSameDay(hoy.getTime(), ubicacion.getFechaFin());
            }
            
            //mientras siga siendo verdadero
            if(flag && ubicacion.getIndDiaRecurrencia()!=null){
                //se obtiene el numero de dia de hoy
                int hoyDia = hoy.get(Calendar.DAY_OF_WEEK);
                //se recorre los indicadores de dias de recurrencia 
                flag = Chars.asList(ubicacion.getIndDiaRecurrencia().toCharArray()).stream().anyMatch((t) -> {
                    //segun el indicador de dia de recurrencia, se verifica si el numero de dia de hoy concuerda con el numero de dia asociado al indicador
                    switch(t) {
                        case 'L':{
                            return Calendar.MONDAY == hoyDia;
                        }
                        case 'K':{
                            return Calendar.TUESDAY==hoyDia;
                        }
                        case 'M':{
                            return Calendar.WEDNESDAY==hoyDia;
                        }
                        case 'J':{
                            return Calendar.TUESDAY == hoyDia;
                        }
                        case 'V':{
                            return Calendar.FRIDAY == hoyDia;
                        }
                        case 'S':{
                            return Calendar.SATURDAY==hoyDia;
                        }
                        case 'D':{
                            return Calendar.SUNDAY==hoyDia;
                        }
                        default:
                            return false;
                    }
                });
            }
            if(flag && ubicacion.getIndSemanaRecurrencia() !=null){
                long weeksPast = ChronoUnit.WEEKS.between(LocalDateTime.ofInstant(Instant.ofEpochMilli(ubicacion.getFechaInicio() == null ? ubicacion.getFechaModificacion().getTime() : ubicacion.getFechaInicio().getTime()), ZoneId.systemDefault()), LocalDateTime.now());
                if (weeksPast % ubicacion.getIndSemanaRecurrencia() != 0) {
                    flag = false;
                }
            }
        }
        return flag;
    }

}
