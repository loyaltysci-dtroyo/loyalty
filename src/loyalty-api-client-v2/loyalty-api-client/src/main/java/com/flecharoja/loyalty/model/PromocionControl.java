package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PROMOCION")
@NamedQueries({
    @NamedQuery(name = "PromocionControl.findAllByIndEstado", query = "SELECT p FROM PromocionControl p WHERE p.indEstado = :indEstado")
})
public class PromocionControl implements Serializable {
    
    public enum Estados {
        PUBLICADO('A'),
        ARCHIVADO('B'),
        BORRADOR('C');
        
        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public char getValue() {
            return value;
        }
        
        public static Estados get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum IndicadoresCalendarizacion {
        PERMANENTE('P'),
        CALENDARIZADO('C');
        
        private final char value;

        private IndicadoresCalendarizacion(char value) {
            this.value = value;
        }

        public char getValue() {
            return value;
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_PROMOCION")
    private String idPromocion;

    @Column(name = "IND_ESTADO")
    private Character indEstado;

    @Column(name = "IND_CALENDARIZACION")
    private Character indCalendarizacion;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    
    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;

    @Column(name = "IND_DIA_RECURRENCIA")
    private String indDiaRecurrencia;

    @Column(name = "IND_SEMANA_RECURRENCIA")
    private Integer indSemanaRecurrencia;

    public PromocionControl() {
    }

    public String getIdPromocion() {
        return idPromocion;
    }

    public void setIdPromocion(String idPromocion) {
        this.idPromocion = idPromocion;
    }
    
    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado != null ? Character.toUpperCase(indEstado) : null;
    }

    public Character getIndCalendarizacion() {
        return indCalendarizacion;
    }

    public void setIndCalendarizacion(Character indCalendarizacion) {
        this.indCalendarizacion = indCalendarizacion != null ? Character.toUpperCase(indCalendarizacion) : null;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getIndDiaRecurrencia() {
        return indDiaRecurrencia;
    }

    public void setIndDiaRecurrencia(String indDiaRecurrencia) {
        this.indDiaRecurrencia = indDiaRecurrencia != null ? indDiaRecurrencia.toUpperCase() : null;
    }

    public Integer getIndSemanaRecurrencia() {
        return indSemanaRecurrencia;
    }

    public void setIndSemanaRecurrencia(Integer indSemanaRecurrencia) {
        this.indSemanaRecurrencia = indSemanaRecurrencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPromocion != null ? idPromocion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromocionControl)) {
            return false;
        }
        PromocionControl other = (PromocionControl) object;
        if ((this.idPromocion == null && other.idPromocion != null) || (this.idPromocion != null && !this.idPromocion.equals(other.idPromocion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Promocion[ idPromocion=" + idPromocion + " ]";
    }

}
