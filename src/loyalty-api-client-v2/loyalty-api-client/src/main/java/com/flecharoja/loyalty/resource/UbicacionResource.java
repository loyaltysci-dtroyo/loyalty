package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.service.UbicacionBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "Ubicaciones")
@Path("ubicacion")
public class UbicacionResource {

    @EJB
    UbicacionBean ubicacionBean;
    
    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;

    /**
     * Método que muestra la informacion detallada de la region
     *
     * @return informacion de la region
     */
    @ApiOperation(value = "Obtener las ubicaciones que pertenecen a una determinada región",
            responseContainer = "List",
            response = Ubicacion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Ubicacion> getUbicaciones() {
        try{
            context.getUserPrincipal().getName();
        }catch(NullPointerException e){
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
       return ubicacionBean.getUbicaciones(request.getLocale());
    }
    
}
