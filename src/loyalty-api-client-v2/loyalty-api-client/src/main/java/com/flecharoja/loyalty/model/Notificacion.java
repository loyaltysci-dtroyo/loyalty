package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NOTIFICACION")
@XmlRootElement
public class Notificacion implements Serializable {
    
    public enum Objectivos {
        PROMOCION('P'),
        MISION('M'),
        RECOMPENSA('R');
        
        private final char value;
        private static final Map<Character, Objectivos> lookup = new HashMap<>();

        private Objectivos(char value) {
            this.value = value;
        }
        
        static {
            for (Objectivos objetivo : values()) {
                lookup.put(objetivo.value, objetivo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Objectivos get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Tipos {
        NOTIFICACION_PUSH('P'),
        EMAIL('E');
        
        private final char value;
        private static final Map<Character, Tipos> lookup = new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }
        
        static {
            for (Tipos estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipos get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_NOTIFICACION")
    @ApiModelProperty(value = "Identificador de la notificación",required = true)
    private String idNotificacion;
    
    @Column(name = "IMAGEN_ARTE")
    @ApiModelProperty(value = "Imagen representativa de la notificación",required = true)
    private String imagen;
    
    @Column(name = "ENCABEZADO_ARTE")
    @ApiModelProperty(value = "Información que representa el nombre de la notificación",required = true)
    private String encabezado;
    
    @Column(name = "TEXTO")
    @ApiModelProperty(value = "Descripción de la notificación",required = true)
    private String texto;
    
    @Column(name = "IND_OBJETIVO")
    @ApiModelProperty(value = "Indicador del objetivo de la notificación", example = "Misión, recompensa, promoción",required = true)
    private Character indObjetivo;
    
    @JoinColumn(name = "ID_ACTIVIDAD_OBJETIVO", referencedColumnName = "ID_MISION")
    @ManyToOne
    @ApiModelProperty(value = "Información de la misión si la notificación es de tipo misión ")
    private MisionDetails mision;
    
    @JoinColumn(name = "ID_RECOMPENSA_OBJETIVO", referencedColumnName = "ID_PREMIO")
    @ManyToOne
    @ApiModelProperty(value = "Información del premio si la notificación es de tipo premio ")
    private Premio premio;
    
    @JoinColumn(name = "ID_PROMO_OBJETIVO", referencedColumnName = "ID_PROMOCION")
    @ManyToOne
    @ApiModelProperty(value = "Información de la promoción si la notificación es de tipo promoción ")
    private Promocion promocion;
    
    @Column(name = "IND_TIPO")
    @ApiModelProperty(value = "Indicador del tipo de notificación", example = "Push, email",required = true)
    private Character indTipo;
    
    @Transient
    private Boolean visto;

    public Notificacion() {
    }

    public String getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(String idNotificacion) {
        this.idNotificacion = idNotificacion;
    }
    
    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getEncabezado() {
        return encabezado;
    }

    public void setEncabezado(String encabezado) {
        this.encabezado = encabezado;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Character getIndObjetivo() {
        return indObjetivo;
    }

    public void setIndObjetivo(Character indObjetivo) {
        this.indObjetivo = indObjetivo!=null?Character.toUpperCase(indObjetivo):null;
    }

    public MisionDetails getMision() {
        return mision;
    }

    public void setMision(MisionDetails mision) {
        this.mision = mision;
    }

    public Premio getPremio() {
        return premio;
    }

    public void setPremio(Premio premio) {
        this.premio = premio;
    }

    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }
    
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo!=null?Character.toUpperCase(indTipo):null;
    }

    public Boolean getVisto() {
        return visto;
    }

    public void setVisto(Boolean visto) {
        this.visto = visto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotificacion != null ? idNotificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notificacion)) {
            return false;
        }
        Notificacion other = (Notificacion) object;
        if ((this.idNotificacion == null && other.idNotificacion != null) || (this.idNotificacion != null && !this.idNotificacion.equals(other.idNotificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Notificacion[ idNotificacion=" + idNotificacion + " ]";
    }
    
}
