/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PROMOCION_CATEGORIA_PROMO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PromocionCategoriaPromo.findAll", query = "SELECT p FROM PromocionCategoriaPromo p"),
    @NamedQuery(name = "PromocionCategoriaPromo.findPromocionControlByIdCategoriaByPromocionIndEstado", query = "SELECT p.promocionControl FROM PromocionCategoriaPromo p WHERE p.promocionCategoriaPromoPK.idCategoria = :idCategoria AND p.promocionControl.indEstado = :indEstado"),
    @NamedQuery(name = "PromocionCategoriaPromo.findByIdCategoria", query = "SELECT p.promocion FROM PromocionCategoriaPromo p WHERE p.promocionCategoriaPromoPK.idCategoria = :idCategoria"),
    @NamedQuery(name = "PromocionCategoriaPromo.countByIdCategoria", query = "SELECT COUNT(p) FROM PromocionCategoriaPromo p WHERE p.promocionCategoriaPromoPK.idCategoria = :idCategoria"),
    @NamedQuery(name = "PromocionCategoriaPromo.findByIdPromocion", query = "SELECT p FROM PromocionCategoriaPromo p WHERE p.promocionCategoriaPromoPK.idPromocion = :idPromocion"),
    @NamedQuery(name = "PromocionCategoriaPromo.findByFechaCreacion", query = "SELECT p FROM PromocionCategoriaPromo p WHERE p.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "PromocionCategoriaPromo.findByUsuarioCreacion", query = "SELECT p FROM PromocionCategoriaPromo p WHERE p.usuarioCreacion = :usuarioCreacion")})
public class PromocionCategoriaPromo implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PromocionCategoriaPromoPK promocionCategoriaPromoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @JoinColumn(name = "ID_CATEGORIA", referencedColumnName = "ID_CATEGORIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CategoriaPromocion categoriaPromocion;
    @JoinColumn(name = "ID_PROMOCION", referencedColumnName = "ID_PROMOCION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Promocion promocion;
    
    @JoinColumn(name = "ID_PROMOCION", referencedColumnName = "ID_PROMOCION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private PromocionControl promocionControl;

    public PromocionCategoriaPromo() {
    }

    public PromocionCategoriaPromo(PromocionCategoriaPromoPK promocionCategoriaPromoPK) {
        this.promocionCategoriaPromoPK = promocionCategoriaPromoPK;
    }

    public PromocionCategoriaPromo(PromocionCategoriaPromoPK promocionCategoriaPromoPK, Date fechaCreacion, String usuarioCreacion) {
        this.promocionCategoriaPromoPK = promocionCategoriaPromoPK;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public PromocionCategoriaPromo(String idCategoria, String idPromocion) {
        this.promocionCategoriaPromoPK = new PromocionCategoriaPromoPK(idCategoria, idPromocion);
    }

    public PromocionCategoriaPromoPK getPromocionCategoriaPromoPK() {
        return promocionCategoriaPromoPK;
    }

    public void setPromocionCategoriaPromoPK(PromocionCategoriaPromoPK promocionCategoriaPromoPK) {
        this.promocionCategoriaPromoPK = promocionCategoriaPromoPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public CategoriaPromocion getCategoriaPromocion() {
        return categoriaPromocion;
    }

    public void setCategoriaPromocion(CategoriaPromocion categoriaPromocion) {
        this.categoriaPromocion = categoriaPromocion;
    }

    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

    public PromocionControl getPromocionControl() {
        return promocionControl;
    }

    public void setPromocionControl(PromocionControl promocionControl) {
        this.promocionControl = promocionControl;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (promocionCategoriaPromoPK != null ? promocionCategoriaPromoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromocionCategoriaPromo)) {
            return false;
        }
        PromocionCategoriaPromo other = (PromocionCategoriaPromo) object;
        if ((this.promocionCategoriaPromoPK == null && other.promocionCategoriaPromoPK != null) || (this.promocionCategoriaPromoPK != null && !this.promocionCategoriaPromoPK.equals(other.promocionCategoriaPromoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.PromocionCategoriaPromo[ promocionCategoriaPromoPK=" + promocionCategoriaPromoPK + " ]";
    }
    
}
