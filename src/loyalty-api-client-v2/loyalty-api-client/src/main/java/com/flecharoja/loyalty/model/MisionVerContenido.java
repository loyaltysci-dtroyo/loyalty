package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_VER_CONTENIDO")
@XmlRootElement
public class MisionVerContenido implements Serializable {
    
    public enum Tipos {
        VIDEO('V'),
        URL('U'),
        IMAGEN('I');
        
        private final char value;
        private static final Map<Character, Tipos> lookup = new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }
        
        static {
            for (Tipos pregunta : values()) {
                lookup.put(pregunta.value, pregunta);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipos get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_MISION")
    @ApiModelProperty(value = "Identificador de la misión",required = true)
    private String idMision;
    
    @Column(name = "IND_TIPO")
    @ApiModelProperty(value = "Indicador del tipo de ver contenido es la misión", example = "Video, url o imagen",required = true)
    private Character indTipo;
    
    @Column(name = "URL")
    @ApiModelProperty(value = "Url a ver si el tipo es de url",required = true)
    private String url;
    
    @Column(name = "TEXTO")
    @ApiModelProperty(value = "Texto informativo de la misión",required = true)
    private String texto;

    public MisionVerContenido() {
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo == null ? null : Character.toUpperCase(indTipo);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMision != null ? idMision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionVerContenido)) {
            return false;
        }
        MisionVerContenido other = (MisionVerContenido) object;
        if ((this.idMision == null && other.idMision != null) || (this.idMision != null && !this.idMision.equals(other.idMision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionVerContenido[ idMision=" + idMision + " ]";
    }
    
}
