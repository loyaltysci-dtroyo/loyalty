package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.CategoriaNoticia;
import java.util.List;
import java.util.Locale;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Clase encargada de proveer metodos para la lectura de categorias de noticias
 *
 * @author svargas
 */
@Stateless
public class CategoriaNoticiaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    //@PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Metodo que obtiene el listado de categorias de noticias almacenadas
     *
     * @param locale
     * @return Lista
     */
    public List<CategoriaNoticia> getCategoriasNoticias(Locale locale) {
        return em.createNamedQuery("CategoriaNoticia.findAll").getResultList();
    }

}
