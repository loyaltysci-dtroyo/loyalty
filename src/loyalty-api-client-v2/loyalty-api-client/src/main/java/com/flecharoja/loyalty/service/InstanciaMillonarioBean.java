/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.InstanciaMillonario;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Kevin Ramírez
 * LoyaltySCI
 */

@Stateless
public class InstanciaMillonarioBean {
    @EJB
    MyKafkaUtils myKafkaUtils;

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    /**
     * Método que obtiene un instancia millonario
     *
     * @param idInstanciaMillonario
     * @param locale
     * @return instancia millonario encontrada
     */
    public InstanciaMillonario getInstanciaMillonario(String idInstanciaMillonario, Locale locale) {
        return em.find(InstanciaMillonario.class, idInstanciaMillonario);
    }
    
    /**
     * Método que obtiene una lista de categorias de misiones existentes
     *
     * @param idMision
     * @param locale
     * @return lista de categorias de mision y su rango
     */
    public InstanciaMillonario getInstanciaMillonarioPorMision(String idMision, Locale locale) {

        //lectura de categorias de mision en un rango de resultados
        List<InstanciaMillonario> instancias = em.createNamedQuery("InstanciaMillonario.findByMision")
                .setParameter("idMision", idMision)
                .getResultList();
        
        if (instancias.isEmpty()) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "instancia_millonario_not_found");
        }
        
        return instancias.get(0);
    }
}
