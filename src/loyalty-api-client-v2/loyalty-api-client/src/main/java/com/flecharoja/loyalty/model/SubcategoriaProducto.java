/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "SUBCATEGORIA_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SubcategoriaProducto.findAll", query = "SELECT s FROM SubcategoriaProducto s"),
    @NamedQuery(name = "SubcategoriaProducto.findByIdSubcategoria", query = "SELECT s FROM SubcategoriaProducto s WHERE s.idSubcategoria = :idSubcategoria"),
    @NamedQuery(name = "SubcategoriaProducto.findByCategoria", query = "SELECT s FROM SubcategoriaProducto s WHERE s.idCategoria.idCategoria = :idCategoria"),
    @NamedQuery(name = "SubcategoriaProducto.countByCategoria", query = "SELECT COUNT(s) FROM SubcategoriaProducto s WHERE s.idCategoria.idCategoria = :idCategoria")
})
public class SubcategoriaProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_SUBCATEGORIA")
    @ApiModelProperty(value = "Identificador único de la subcategoría", required = true)
    private String idSubcategoria;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    @ApiModelProperty(value = "Nombre de la subcategoría", required = true)
    private String nombre;

    @Size(max = 300)
    @Column(name = "IMAGEN")
    @ApiModelProperty(value = "Imagen representativa de la subcategoría",required = true)
    private String imagen;
    
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    @ApiModelProperty(value = "Descripción de la subcategoría")
    private String descripcion;

    @Column(name = "ID_CATEGORIA")
    @ApiModelProperty(value = "Identificador de la categoría a la cual pertenece la subcategoría", required = true)
    private String idCategoria;

    public SubcategoriaProducto() {
    }

    public SubcategoriaProducto(String idSubcategoria) {
        this.idSubcategoria = idSubcategoria;
    }

    public String getIdSubcategoria() {
        return idSubcategoria;
    }

    public void setIdSubcategoria(String idSubcategoria) {
        this.idSubcategoria = idSubcategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

//    @ApiModelProperty(hidden = true)
//    @XmlTransient
    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSubcategoria != null ? idSubcategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubcategoriaProducto)) {
            return false;
        }
        SubcategoriaProducto other = (SubcategoriaProducto) object;
        if ((this.idSubcategoria == null && other.idSubcategoria != null) || (this.idSubcategoria != null && !this.idSubcategoria.equals(other.idSubcategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.SubcategoriaProducto[ idSubcategoria=" + idSubcategoria + " ]";
    }

}
