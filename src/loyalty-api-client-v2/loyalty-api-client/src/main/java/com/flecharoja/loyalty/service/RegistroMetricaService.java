package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.BalanceMetricaMensual;
import com.flecharoja.loyalty.model.BalanceMetricaMensualPK;
import com.flecharoja.loyalty.model.Grupo;
import com.flecharoja.loyalty.model.GrupoMiembro;
import com.flecharoja.loyalty.model.GrupoMiembroPK;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.MiembroBalanceMetrica;
import com.flecharoja.loyalty.model.MiembroBalanceMetricaPK;
import com.flecharoja.loyalty.model.NivelMetrica;
import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.model.RegistroMetricaPK;
import com.flecharoja.loyalty.model.TablaPosiciones;
import com.flecharoja.loyalty.model.TabposGrupomiembro;
import com.flecharoja.loyalty.model.TabposGrupomiembroPK;
import com.flecharoja.loyalty.model.TabposMiembro;
import com.flecharoja.loyalty.model.TabposMiembroPK;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author svargas
 */
@Stateless
public class RegistroMetricaService {
    
    @EJB
    MyKafkaUtils myKafkaUtils;
    
    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    private final Configuration hbaseConf;

    public RegistroMetricaService() {
        this.hbaseConf = new Configuration();
        this.hbaseConf.addResource("conf/hbase/hbase-site.xml");
    }

    public void insertRegistroMetrica(RegistroMetrica registroMetrica) {
        MiembroBalanceMetrica balanceMetrica = em.find(MiembroBalanceMetrica.class, new MiembroBalanceMetricaPK(registroMetrica.getIdMiembro(), registroMetrica.getIdMetrica()));
        if (balanceMetrica==null) {
            balanceMetrica = new MiembroBalanceMetrica(registroMetrica.getIdMiembro(), registroMetrica.getIdMetrica());
        }
        Double progresoActual = balanceMetrica.getProgresoActual();
        
        Metrica metrica = em.find(Metrica.class, registroMetrica.getIdMetrica());
        List<NivelMetrica> niveles = metrica.getGrupoNiveles().getNivelMetricaList();
        if (niveles != null) {
            Optional<NivelMetrica> nivelActual = niveles.stream().sorted((o1, o2) -> Double.compare(o2.getMetricaInicial(), o1.getMetricaInicial())).filter((t) -> t.getMetricaInicial()<=progresoActual).findFirst();
            if (nivelActual.isPresent() && nivelActual.get().getMultiplicador()!=null) {
                registroMetrica.setCantidad(registroMetrica.getCantidad()*nivelActual.get().getMultiplicador());
            }
            
            boolean nivelAlcanzado = niveles.stream()
                    .filter((NivelMetrica t) -> t.getMetricaInicial().compareTo(progresoActual)>=0)
                    .anyMatch((NivelMetrica t) -> t.getMetricaInicial().compareTo(progresoActual+registroMetrica.getCantidad())<=0);
            if (nivelAlcanzado) {
                try {
                    myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.ALCANZA_NIVEL_METRICA, registroMetrica.getIdMetrica(), registroMetrica.getIdMiembro());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
        }
        
        balanceMetrica.setDisponible(balanceMetrica.getDisponible()+registroMetrica.getCantidad());
        balanceMetrica.setTotalAcumulado(balanceMetrica.getTotalAcumulado()+registroMetrica.getCantidad());
        balanceMetrica.setProgresoActual(balanceMetrica.getProgresoActual()+registroMetrica.getCantidad());
        em.merge(balanceMetrica);
        
        registroMetrica.setDisponibleActual(balanceMetrica.getDisponible());
        
        LocalDate localDate = LocalDate.now();
        BalanceMetricaMensual balanceMetricaMensual = em.find(BalanceMetricaMensual.class, new BalanceMetricaMensualPK(localDate.getYear(), localDate.getMonthValue(), registroMetrica.getIdMetrica()));
        if (balanceMetricaMensual == null) {
            balanceMetricaMensual = new BalanceMetricaMensual(localDate.getYear(), localDate.getMonthValue(), registroMetrica.getIdMetrica());
        }
        balanceMetricaMensual.setTotalAcumulado(balanceMetricaMensual.getTotalAcumulado()+registroMetrica.getCantidad());
        em.merge(balanceMetricaMensual);
        
        List<TablaPosiciones> tablas = em.createNamedQuery("TablaPosiciones.findByIdMetrica").setParameter("idMetrica", registroMetrica.getIdMetrica()).getResultList();
        tablas.forEach((t) -> {
            switch(TablaPosiciones.Tipo.get(t.getIndTipo())) {
                case MIEMBROS: {
                    TabposMiembro tm = em.find(TabposMiembro.class, new TabposMiembroPK(t.getIdTabla(), registroMetrica.getIdMiembro()));
                    if (tm==null) {
                        tm = new TabposMiembro(t.getIdTabla(), registroMetrica.getIdMiembro());
                    }
                    tm.setAcumulado(tm.getAcumulado()+registroMetrica.getCantidad());
                    em.merge(tm);
                    break;
                }
                case GRUPO_ESPECIFICO: {
                    GrupoMiembro gm = em.find(GrupoMiembro.class, new GrupoMiembroPK(registroMetrica.getIdMiembro(), t.getIdGrupo().getIdGrupo()));
                    if (gm!=null) {
                        TabposMiembro tm = em.find(TabposMiembro.class, new TabposMiembroPK(t.getIdTabla(), registroMetrica.getIdMiembro()));
                        if (tm==null) {
                            tm = new TabposMiembro(t.getIdTabla(), registroMetrica.getIdMiembro());
                        }
                        tm.setAcumulado(tm.getAcumulado()+registroMetrica.getCantidad());
                        em.merge(tm);
                    }
                    break;
                }
                case GRUPO: {
                    List<Grupo> grupos = em.createNamedQuery("GrupoMiembro.findByIdMiembro").setParameter("idMiembro", registroMetrica.getIdMiembro()).getResultList();
                    grupos.forEach((g) -> {
                        TabposGrupomiembro tm = em.find(TabposGrupomiembro.class, new TabposGrupomiembroPK(t.getIdTabla(), g.getIdGrupo()));
                        if (tm==null) {
                            tm = new TabposGrupomiembro(t.getIdTabla(), g.getIdGrupo());
                        }
                        tm.setAcumulado(tm.getAcumulado()+registroMetrica.getCantidad());
                        em.merge(tm);
                    });
                    break;
                }
            }
        });
        
        //se verifica que no exista otro registro con el mismo valor de llave primaria y se recalcula el valor de instancia de ser necesario
        do {            
            registroMetrica.getRegistroMetricaPK().setIdInstancia(UUID.randomUUID().toString());
        } while (em.find(RegistroMetrica.class, registroMetrica.getRegistroMetricaPK())!=null);
        
        em.persist(registroMetrica);
        em.flush();
        
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "REGISTRO_METRICA"));

            //se crea el put con los valores del registro de metrica
            Put put = new Put(Bytes.toBytes(String.format("%013d", registroMetrica.getRegistroMetricaPK().getFecha().getTime())+"&"+registroMetrica.getRegistroMetricaPK().getIdInstancia()));

            put.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("METRICA"), Bytes.toBytes(registroMetrica.getIdMetrica()));
            put.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("MIEMBRO"), Bytes.toBytes(registroMetrica.getIdMiembro()));
            put.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("TIPO_GANE"), Bytes.toBytes(registroMetrica.getIndTipoGane()));
            put.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("CANTIDAD"), Bytes.toBytes(registroMetrica.getCantidad()));
            put.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("DISPONIBLE"), Bytes.toBytes(registroMetrica.getCantidad()));
            put.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("VENCIDO"), Bytes.toBytes(false));

            //detalles
            if (registroMetrica.getIdMision() != null) {
                put.addColumn(Bytes.toBytes("DETALLES"), Bytes.toBytes("MISION"), Bytes.toBytes(registroMetrica.getIdMision()));
            }
            if (registroMetrica.getIdTransaccion()!=null) {
                put.addColumn(Bytes.toBytes("DETALLES"), Bytes.toBytes("TRANSACCION"), Bytes.toBytes(registroMetrica.getIdTransaccion()));
            }

            //se almacena el registro
            table.put(put);
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, Locale.getDefault(), "database_connection_failed");
        }
        
        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.GANA_METRICA, registroMetrica.getIdMetrica(), registroMetrica.getIdMiembro());
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
        }
    }
    
    /**
     * metodo que actualiza una lista de registros de metrica
     *
     * @param registros lista de registros de metrica a actualizar
     */
    public void updateRegistrosMetricasMiembro(List<RegistroMetrica> registros) {
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "REGISTRO_METRICA"));

            //mapeo de registros a puts para su almacenamiento
            List<Put> puts = registros.stream().map((t) -> {
                Put put = new Put(Bytes.toBytes(String.format("%013d", t.getRegistroMetricaPK().getFecha().getTime()) + "&" + t.getRegistroMetricaPK().getIdInstancia()));
                put.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("DISPONIBLE"), Bytes.toBytes(t.getDisponible()));

                return put;
            }).collect(Collectors.toList());

            //se almacena los registros
            table.put(puts);
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, Locale.getDefault(), "database_connection_failed");
        }
    }
    
    /**
     * metodo para obtener los registros de metricas disponibles segun por -se
     * encuentre un valor mayor de 0 en la cantidad disponible -no se encuentre
     * con el indicador de vencido
     *
     * @param idMiembro identificador del miembro
     * @param idMetrica identificador de la metrica objetivo
     * @return listado de registros
     */
    public List<RegistroMetrica> getRegistrosMetricasMiembroDisponibles(String idMiembro, String idMetrica) {
        List<RegistroMetrica> registros = new ArrayList<>();
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "REGISTRO_METRICA"));

            //se establece el sacnner con una lista de filtros con condiciones a cumplir
            //que el el rowkey cumpla con el regex (que termine con el identificador del miembro), que el valor de la metrica sea el buscado,
            //que el indicador de vencido no sea verdadero y que el valor almacenado en la cantidad sea mayor a 0
            Scan scan = new Scan();
            FilterList filterList = new FilterList(
                    new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("MIEMBRO"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes(idMiembro)),
                    new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("METRICA"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes(idMetrica)),
                    new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("VENCIDO"), CompareFilter.CompareOp.NOT_EQUAL, Bytes.toBytes(true)),
                    new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("DISPONIBLE"), CompareFilter.CompareOp.GREATER, Bytes.toBytes(0d))
            );
            scan.setFilter(filterList);

            //por cada registro encontrado, se extraen los datos en terminos de registros y se guarda en la lista de registros
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    registros.add(new RegistroMetrica(
                            new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[0])),
                            Bytes.toString(result.getRow()).split("&")[1],
                            Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("DISPONIBLE")))));
                }
            }
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, Locale.getDefault(), "database_connection_failed");
        }

        //se retorna la lista de registros
        return registros;
    }
    
    /**
     * metodo para obtener los registros historicos de metricas del miembro en
     * un periodo establecido
     *
     * @param idMiembro identificador del miembro
     * @param idMetrica identificador de la metrica objetivo
     * @return listado de registros
     */
    public List<RegistroMetrica> getHistorialMetricasMiembro(String idMiembro, String idMetrica) {
        List<RegistroMetrica> registros = em.createNamedQuery("RegistroMetrica.findByIdMetricaIdMiembroUntilFecha")
                .setParameter("idMiembro", idMiembro)
                .setParameter("idMetrica", idMetrica)
                .setParameter("fecha", java.sql.Date.valueOf(LocalDate.now().minusMonths(2)))
                .getResultList();

        //se retorna la lista de registros
        return registros;
    }

    public List<RegistroMetricaPK> getGaneMetricaMision(String idMiembro, String idMetrica, String idMision) {
        //se leen las llaves primarias de los registros de metrica segun si el valor de idMiembro es null o no
        List<RegistroMetricaPK> registros = idMiembro==null?
                em.createNamedQuery("RegistroMetrica.getRegistroMetricaPKByIdMetricaIdMision").setParameter("idMetrica", idMetrica).setParameter("idMision", idMision).getResultList():
                em.createNamedQuery("RegistroMetrica.getRegistroMetricaPKByIdMetricaIdMisionIdMiembro").setParameter("idMetrica", idMetrica).setParameter("idMision", idMision).setParameter("idMiembro", idMiembro).getResultList();

        //se retorna la lista de registros
        return registros;
    }
}
