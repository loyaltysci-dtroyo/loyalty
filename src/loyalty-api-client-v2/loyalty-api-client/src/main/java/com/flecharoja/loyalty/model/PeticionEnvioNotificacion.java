package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author svargas
 */
public class PeticionEnvioNotificacion {
    
    @ApiModelProperty(value = "Remitente")
    private String sender;
    
    @ApiModelProperty(value = "Identificador de la notificacion")
    private String idNotificacion;
    
    @ApiModelProperty(value = "Lista de miembros a los que va dirigida la notificacion")
    private List<String> miembros;
    
    @ApiModelProperty(value = "Parametros de la notificacion")
    private Map<String, String> parametros;

    public PeticionEnvioNotificacion() {
        miembros = new ArrayList<>();
        parametros = new HashMap<>();
    }

    public String getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(String idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public List<String> getMiembros() {
        return miembros;
    }

    public void setMiembros(List<String> miembros) {
        this.miembros = miembros;
    }

    public Map<String, String> getParametros() {
        return parametros;
    }

    public void setParametros(Map<String, String> parametros) {
        this.parametros = parametros;
    }
    
}
