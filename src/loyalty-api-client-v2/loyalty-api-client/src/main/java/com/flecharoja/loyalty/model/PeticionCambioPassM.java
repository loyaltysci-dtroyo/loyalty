package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PETICION_CAMBIO_PASSM")
@XmlRootElement
public class PeticionCambioPassM implements Serializable {
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_MIEMBRO")
    @ApiModelProperty(value = "Identificador de un miembro",required = true)
    private String idMiembro;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PETICION")
    @ApiModelProperty(value = "Identificador de la peticion de recuperacion de contraseña",required = true)
    private String idPeticion;
    
    @Transient
    @ApiModelProperty(value = "Contraseña nueva de miembro",required = false)
    private String contrasena;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_EXPIRACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaExpiracion;

    public PeticionCambioPassM() {
    }

    public PeticionCambioPassM(String idMiembro, String idPeticion, Date fechaExpiracion) {
        this.idMiembro = idMiembro;
        this.idPeticion = idPeticion;
        this.fechaExpiracion = fechaExpiracion;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIdPeticion() {
        return idPeticion;
    }

    public void setIdPeticion(String idPeticion) {
        this.idPeticion = idPeticion;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    @XmlTransient
    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }
}
