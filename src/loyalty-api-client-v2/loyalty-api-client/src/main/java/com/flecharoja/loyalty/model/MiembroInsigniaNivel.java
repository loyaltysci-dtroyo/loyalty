/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "MIEMBRO_INSIGNIA_NIVEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MiembroInsigniaNivel.findAll", query = "SELECT m FROM MiembroInsigniaNivel m"),
    @NamedQuery(name = "MiembroInsigniaNivel.findByIdMiembro", query = "SELECT m.insignias FROM MiembroInsigniaNivel m WHERE m.miembroInsigniaNivelPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroInsigniaNivel.findByIdInsignia", query = "SELECT m FROM MiembroInsigniaNivel m WHERE m.miembroInsigniaNivelPK.idInsignia = :idInsignia"),
    @NamedQuery(name = "MiembroInsigniaNivel.findInsigniasNotInMiembro", query = "SELECT i FROM Insignias i WHERE  i.idInsignia NOT IN (SELECT s.miembroInsigniaNivelPK.idInsignia FROM MiembroInsigniaNivel s WHERE s.miembroInsigniaNivelPK.idMiembro = :idMiembro)"),
    @NamedQuery(name = "MiembroInsigniaNivel.findByIdInsigniaNivel", query = "SELECT m.idNivel FROM MiembroInsigniaNivel m WHERE m.miembroInsigniaNivelPK.idInsignia = :idInsignia"),
    @NamedQuery(name = "MiembroInsigniaNivel.findIdMiembroIdInsignia", query= "SELECT m.insignias FROM MiembroInsigniaNivel m WHERE m.miembroInsigniaNivelPK.idInsignia = :idInsignia AND m.miembroInsigniaNivelPK.idMiembro = :idMiembro")
})
public class MiembroInsigniaNivel implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MiembroInsigniaNivelPK miembroInsigniaNivelPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @JoinColumn(name = "ID_INSIGNIA", referencedColumnName = "ID_INSIGNIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Insignias insignias;
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MiembroMin miembroLower;
    @JoinColumn(name = "ID_NIVEL", referencedColumnName = "ID_NIVEL")
    @ManyToOne
    private NivelesInsignias idNivel;

    public MiembroInsigniaNivel() {
    }

    public MiembroInsigniaNivel(MiembroInsigniaNivelPK miembroInsigniaNivelPK) {
        this.miembroInsigniaNivelPK = miembroInsigniaNivelPK;
    }

    public MiembroInsigniaNivel(MiembroInsigniaNivelPK miembroInsigniaNivelPK, Date fechaCreacion, String usuarioCreacion) {
        this.miembroInsigniaNivelPK = miembroInsigniaNivelPK;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public MiembroInsigniaNivel(String idMiembro, String idInsignia) {
        this.miembroInsigniaNivelPK = new MiembroInsigniaNivelPK(idMiembro, idInsignia);
    }

    public MiembroInsigniaNivelPK getMiembroInsigniaNivelPK() {
        return miembroInsigniaNivelPK;
    }

    public void setMiembroInsigniaNivelPK(MiembroInsigniaNivelPK miembroInsigniaNivelPK) {
        this.miembroInsigniaNivelPK = miembroInsigniaNivelPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Insignias getInsignias() {
        return insignias;
    }

    public void setInsignias(Insignias insignias) {
        this.insignias = insignias;
    }

    public MiembroMin getMiembroLower() {
        return miembroLower;
    }

    public void setMiembroLower(MiembroMin miembroLower) {
        this.miembroLower = miembroLower;
    }

    public NivelesInsignias getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(NivelesInsignias idNivel) {
        this.idNivel = idNivel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (miembroInsigniaNivelPK != null ? miembroInsigniaNivelPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiembroInsigniaNivel)) {
            return false;
        }
        MiembroInsigniaNivel other = (MiembroInsigniaNivel) object;
        if ((this.miembroInsigniaNivelPK == null && other.miembroInsigniaNivelPK != null) || (this.miembroInsigniaNivelPK != null && !this.miembroInsigniaNivelPK.equals(other.miembroInsigniaNivelPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MiembroInsigniaNivel[ miembroInsigniaNivelPK=" + miembroInsigniaNivelPK + " ]";
    }
    
}
