package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.Grupo;
import com.flecharoja.loyalty.service.GrupoBean;
import com.flecharoja.loyalty.util.Indicadores;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "Grupos de miembros")
@Path("grupo-miembro")
public class GrupoResource {

    @EJB
    GrupoBean grupoBean;
    
    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;

    /**
     * Método que obtiene una lista de grupos a los que pertenece un miembro
     *
     * @return lista de grupos
     */
    @ApiOperation(value = "Obtener una lista de grupos a los que pertenece un miembro",
            responseContainer = "List",
            response = Grupo.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("pertenecientes")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Grupo> getGruposPertenecientes() {
        
        String idMiembro;
        try{
            idMiembro =  context.getUserPrincipal().getName();
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
                
        return grupoBean.getGruposPertenecientes(idMiembro,request.getLocale());
    }

    /**
     * Método que obtiene una lista de grupos a los que no pertenece un miembro
     *
     * @return lista de grupos
     */
    @ApiOperation(value = "Obtener una lista de grupos a los que no pertenece un miembro",
            responseContainer = "List",
            response = Grupo.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("otros")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Grupo> getGruposNoPertenecientes() {
        String idMiembro;
        try{
            idMiembro =  context.getUserPrincipal().getName();
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return grupoBean.getGruposNoPertenecientes(idMiembro,request.getLocale());          
    }
}
