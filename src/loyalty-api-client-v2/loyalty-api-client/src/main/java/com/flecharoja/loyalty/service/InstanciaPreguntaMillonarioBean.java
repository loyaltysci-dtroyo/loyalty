/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.InstanciaPreguntaMillonario;
import java.util.List;
import java.util.Locale;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;

/**
 *
 * @author kevin
 */

@Stateless
public class InstanciaPreguntaMillonarioBean {
   
    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    /**
     * Método que obtiene una lista de categorias de misiones existentes
     *
     * @param idInstanciaMillonario
     * @param locale
     * @return lista de preguntas de una instancia millonario
     */
    public List<InstanciaPreguntaMillonario> getPreguntasInstanciaMillonario(String idInstanciaMillonario, Locale locale) {

        //lectura de categorias de mision en un rango de resultados
        List<InstanciaPreguntaMillonario> pregunta = em.createNamedQuery("InstanciaPreguntaMillonario.findByInstanciaMillonario")
                .setParameter("idInstanciaJuego", idInstanciaMillonario)
                .getResultList();
        
        if (pregunta.isEmpty()) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "instancia_millonario_not_found");
        }
        
        return pregunta;
    }
    
    /**
     * Método que obtiene una lista de categorias de misiones existentes
     *
     * @param idInstanciaMillonario
     * @param locale
     * @return lista de preguntas de una instancia millonario
     */
    public List<InstanciaPreguntaMillonario> getPreguntasInstanciaMillonarioRandom(String idInstanciaMillonario, Locale locale) {

        //lectura de categorias de mision en un rango de resultados
        List<InstanciaPreguntaMillonario> preguntas = em.createNamedQuery("InstanciaPreguntaMillonario.findByInstanciaMillonario")
                .setParameter("idInstanciaJuego", idInstanciaMillonario)
                .getResultList();
        
        if (preguntas.isEmpty()) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "instancia_millonario_not_found");
        }
        List<InstanciaPreguntaMillonario> preguntasPorPuntos = new ArrayList<>();
        List<InstanciaPreguntaMillonario> preguntasReducidas = new ArrayList<>();
        
        int puntosActuales = 0;
        if (preguntas.size() > 0) {
            puntosActuales = preguntas.get(0).getPuntos();
        }
        
        //Las preguntas llegar ordenadas desde el query
        for (int i=0; i<preguntas.size(); i++) {
            if (preguntas.get(i).getPuntos() == puntosActuales) {
                preguntasPorPuntos.add(preguntas.get(i));
            } else {
                preguntasReducidas.add(
                        preguntasPorPuntos.get(
                                (int)(Math.random()*preguntasPorPuntos.size())
                        )
                );
                preguntasPorPuntos.clear();
                puntosActuales = preguntas.get(i).getPuntos();
                preguntasPorPuntos.add(preguntas.get(i));
            }
        }
        
        if (preguntasPorPuntos.size() > 0) {
            preguntasReducidas.add(
                    preguntasPorPuntos.get(
                            (int)(Math.random()*preguntasPorPuntos.size())
                    )
            );
        }
        
        return preguntasReducidas;
    }
}
