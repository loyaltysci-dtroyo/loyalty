package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MIEMBRO")
@NamedQueries({
    @NamedQuery(name = "MiembroDataExtra.findByFcmTokenRegistro", query = "SELECT m FROM MiembroDataExtra m WHERE m.fcmTokenRegistro = :fcmTokenRegistro"),
    @NamedQuery(name = "MiembroDataExtra.findByMiembroReferente", query = "SELECT m FROM MiembroDataExtra m WHERE m.miembroReferente = :miembroReferente")
})
public class MiembroDataExtra implements Serializable {
    
    public enum TiposDispositivos {
        ANDROID('A'),
        IOS('I'),
        WEB('W'),
        DESCONOCIDO('D');
        
        private final char value;
        private static final Map<Character, TiposDispositivos> lookup = new HashMap<>();

        private TiposDispositivos(char value) {
            this.value = value;
        }
        
        static {
            for (TiposDispositivos tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static TiposDispositivos get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_MIEMBRO")
    @ApiModelProperty(value = "Identificador del miembro")
    private String idMiembro;

    @Column(name = "IND_TIPO_DISPOSITIVO")
    @ApiModelProperty(value = "Indicador del tipo de dispositivo ultima vez conectado")
    private Character indTipoDispositivo;

    @Column(name = "FECHA_ULTIMA_CONEXION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Fecha de la ultima conexion")
    private Date fechaUltimaConexion;

    @Size(max = 255)
    @Column(name = "FCM_TOKEN_REGISTRO")
    @ApiModelProperty(value = "Token del miembro, actualizado cada cierto tiempo")
    private String fcmTokenRegistro;
    
    @Column(name = "MIEMBRO_REFERENTE")
    @ApiModelProperty(value = "Miembro que lo refirio, si existe la referencia")
    private String miembroReferente;

    public MiembroDataExtra() {
    }

    public MiembroDataExtra(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public Character getIndTipoDispositivo() {
        return indTipoDispositivo;
    }

    public void setIndTipoDispositivo(Character indTipoDispositivo) {
        this.indTipoDispositivo = indTipoDispositivo;
    }

    public Date getFechaUltimaConexion() {
        return fechaUltimaConexion;
    }

    public void setFechaUltimaConexion(Date fechaUltimaConexion) {
        this.fechaUltimaConexion = fechaUltimaConexion;
    }

    public String getFcmTokenRegistro() {
        return fcmTokenRegistro;
    }

    public void setFcmTokenRegistro(String fcmTokenRegistro) {
        this.fcmTokenRegistro = fcmTokenRegistro;
    }

    public String getMiembroReferente() {
        return miembroReferente;
    }

    public void setMiembroReferente(String miembroReferente) {
        this.miembroReferente = miembroReferente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiembroDataExtra)) {
            return false;
        }
        MiembroDataExtra other = (MiembroDataExtra) object;
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Miembro[ idMiembro=" + idMiembro + " ]";
    }

}
