/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;


import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CertificadoCategoria;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class CertificadoCategoriaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;


    /**
     * Método que obtiene una lista de certificados disponibles
     *
     * @param categoriaProducto
     * @return lista de certificados disponibles
     */
    public List<CertificadoCategoria> getCertificadosDisponibles(String categoriaProducto) {
        return em.createNamedQuery("CertificadoCategoria.findAvailable").setParameter("categoriaProducto", categoriaProducto).setParameter("estado", 'D').getResultList();
    }
    
    public CertificadoCategoria updateCertificadoCategoria(CertificadoCategoria certificado, Character estado) {
        certificado.setEstado(estado);
        try {
            em.persist(certificado);
            em.flush();

        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        return certificado;
    }
}
