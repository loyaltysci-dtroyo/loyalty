package com.flecharoja.loyalty.util;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author svargas
 */
@Stateless
public class MyKafkaUtils {
    
    public enum EventosSistema {
        GANA_METRICA(7),
        REDIME_METRICA(8),
        ALCANZA_NIVEL_METRICA(10),
        EJECUCION_MISION(12),
        APROBO_MISION(13),
        REPROBO_MISION(14),
        RECIBIO_PREMIO(17),
        REMIDIO_PREMIO(18),
        COMPRO_ALGUN_PRODUCTO(19),
        MARCO_PROMOCION(20),
        NUEVO_MIEMBRO(24),
        VER_PROMOCION(27),
        ACTUALIZACION_PERFIL(28),
        VER_MISION(29),
        COMPRA_PRODUCTO_X(31),
        MIEMBRO_REFERIDO(33);

        private final int value;

        private EventosSistema(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    private KafkaProducer<Object, Object> producer;
    
    private static int id = 0;

    @PostConstruct
    private void init() {
        try {
            Properties properties = new Properties();
            properties.put("client.id", "producer-"+id++);
            properties.load(MyKafkaUtils.class.getResourceAsStream("/conf/kafka/kafka-producer.properties"));
            producer = new KafkaProducer<>(properties);
        } catch (IOException ex) {
            Logger.getLogger(MyKafkaUtils.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }
    
    @Asynchronous
    public void notifyEvento (EventosSistema evento, String idElemento, String idMiembro) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("indEvento", evento.getValue());
        if (idElemento != null) {
            builder.add("idElemento", idElemento);
        }
        builder.add("idMiembro", idMiembro);
        producer.send(new ProducerRecord<>("eventos-sistema", builder.build().toString()));
    }
    
    @PreDestroy
    private void preDestroy() {
        producer.close();
    }
}
