package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_LISTA_UBICACION")
public class MisionListaUbicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MisionListaUbicacionPK misionListaUbicacionPK;
    
    @Column(name = "IND_TIPO")
    private Character indTipo;

    public MisionListaUbicacion() {
    }

    public MisionListaUbicacionPK getMisionListaUbicacionPK() {
        return misionListaUbicacionPK;
    }

    public void setMisionListaUbicacionPK(MisionListaUbicacionPK misionListaUbicacionPK) {
        this.misionListaUbicacionPK = misionListaUbicacionPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (misionListaUbicacionPK != null ? misionListaUbicacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionListaUbicacion)) {
            return false;
        }
        MisionListaUbicacion other = (MisionListaUbicacion) object;
        if ((this.misionListaUbicacionPK == null && other.misionListaUbicacionPK != null) || (this.misionListaUbicacionPK != null && !this.misionListaUbicacionPK.equals(other.misionListaUbicacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionListaUbicacion[ misionListaUbicacionPK=" + misionListaUbicacionPK + " ]";
    }
    
}
