/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Insignias;
import com.flecharoja.loyalty.model.MiembroInsigniaNivel;
import com.flecharoja.loyalty.model.MiembroInsigniaNivelPK;
import com.flecharoja.loyalty.model.NivelesInsignias;
import com.flecharoja.loyalty.util.Indicadores;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;

/**
 *
 * @author wtencio
 */
@Stateless
public class InsigniasBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Método que obtiene un listado de todas las insignias que han sido
     * obtenidas por el miembro en sesion
     *
     * @param idMiembro identificador del miembro
     * @param locale
     * @return lista de insignias obtenidas
     */
    public List<Insignias> getInsignias(String idMiembro, Locale locale) {

        List<Insignias> registros = new ArrayList();

        try {
            //obtener las insignias publicadas
            registros = em.createNamedQuery("Insignias.findAllPublicadas").setParameter("estado", Insignias.Estados.PUBLICADO.getValue()).getResultList();

            for (Insignias registro : registros) {
                //obtengo si existe el registro de la insignia ganada
                MiembroInsigniaNivel miembroInsignia = em.find(MiembroInsigniaNivel.class, new MiembroInsigniaNivelPK(idMiembro, registro.getIdInsignia()));
                if (miembroInsignia == null) {
                    registro.setObtenida(false);
                } else {
                    if (registro.getIndTipo().equals(Insignias.Tipos.STATUS.getValue()) && !registro.getNiveles().isEmpty()) {
                        List<NivelesInsignias> niveles = registro.getNiveles();
                        for (NivelesInsignias nivel : niveles) {
                            if (nivel.getIdNivel().equals(miembroInsignia.getIdNivel().getIdNivel())) {
                                nivel.setObtenido(true);
                            } else {
                                nivel.setObtenido(false);
                            }
                        }
                    } else {
                        registro.setObtenida(false);
                    }

                    if (registro.getIndTipo().equals(Insignias.Tipos.COLLECTOR.getValue())) {
                        registro.setObtenida(true);

                    }
                }

            }

        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }

        return registros;
    }

    /**
     * Método que obtiene un listado de las metricas que no han sido obtenidas
     * por el miembro en sesion
     *
     * @param idMiembro identificador del miembro
     * @param cantidad de registros maximos por pagina
     * @param registro por el cual se inicia la busqueda
     * @param locale
     * @return lista de insignias obtenidas
     */
    public Map<String, Object> getInsigniasNoObtenidas(String idMiembro, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        Map<String, Object> respuesta = new HashMap<>();
        List<Insignias> insignias = new ArrayList<>();
        List registros = new ArrayList();
        int total;

        try {
            registros = em.createNamedQuery("MiembroInsigniaNivel.findInsigniasNotInMiembro").setParameter("idMiembro", idMiembro).getResultList();
            total = registros.size();
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            insignias = em.createNamedQuery("MiembroInsigniaNivel.findInsigniasNotInMiembro")
                    .setParameter("idMiembro", idMiembro)
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();

        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }

        respuesta.put("insignias", insignias);
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        return respuesta;
    }

//   /**
//    * Método que devulve la informacion completa de una insignia especifica
//    * @param idInsignia identificador de la insignia
//    * @param miembro identificador del miembro
//    * @return informacion detallada de la insignia
//    */
//   public Map<String, Object> getDetalleInsignia(String idInsignia, String miembro){     
//       Map<String, Object> respuesta =  new HashMap<>();
//       Insignias insignia = (Insignias) em.createNamedQuery("MiembroInsigniaNivel.findIdMiembroIdInsignia").setParameter("idInsignia", idInsignia).setParameter("idMiembro", miembro).getResultList().get(0);
//               
//       if(insignia == null){
//           throw new ErrorClienteException(MyException.Cliente.ENTIDAD_NO_EXISTENTE);
//       }
//       List<NivelesInsignias> nivelesCompletos = new ArrayList<>();
//       List<NivelesInsignias> niveles = new ArrayList<>();
//       
//       if(insignia.getTipo().equals(Indicadores.INSIGNIAS_TIPO_STATUS)){
//           nivelesCompletos = em.createNamedQuery("NivelesInsignias.findNiveles").setParameter("idInsignia", insignia.getIdInsignia()).getResultList();
//           for (NivelesInsignias nivel : nivelesCompletos) {
//               NivelesInsignias nivelInsi = new NivelesInsignias(nivel.getIdNivel(), nivel.getNombreNivel(), nivel.getDescripcion(), nivel.getImagen());
//               niveles.add(nivelInsi);
//           }
//       }
//       
//       insignia.setNivelesInsigniasList(niveles);
//       
//       respuesta.put("resultado", insignia);
//       return respuesta;
//       
//  
//   }
}
