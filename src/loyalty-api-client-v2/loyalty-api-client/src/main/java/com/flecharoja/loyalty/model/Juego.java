/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "MISION_JUEGO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Juego.findAll", query = "SELECT j FROM Juego j"),
    @NamedQuery(name = "Juego.findIdMision", query = "SELECT j FROM Juego j WHERE j.idMision = :idMision")
})
public class Juego implements Serializable {

    @Column(name = "IMAGEN")
    private String imagen;

    @Column(name = "IMAGENES_ROMPECABEZAS")
    private String imagenesRompecabezas;
    
    @Column(name = "TIEMPO")
    private Integer tiempo;
    
    @Size(min = 36, max = 40)
    @Column(name = "ID_INSTANCIA_MILLONARIO")
    private String idInstanciaMillonario;
    
    @Size(min = 36, max = 40)
    @Column(name = "ID_JUEGO_MILLONARIO")
    private String idJuegoMillonario;

    public enum Tipo {
        RULETA('R'),
        RASPADITA('A'),
        ROMPECABEZAS('O'),
        MILLONARIO('M');

        private final char value;
        private static final Map<Character, Tipo> lookup = new HashMap<>();

        private Tipo(char value) {
            this.value = value;
        }

        static {
            for (Tipo tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static Tipo get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum Estrategia {
        RANDOM('R'),
        PROBABILIDAD('P');

        private final char value;
        private static final Map<Character, Estrategia> lookup = new HashMap<>();

        private Estrategia(char value) {
            this.value = value;
        }

        static {
            for (Estrategia estrategia : values()) {
                lookup.put(estrategia.value, estrategia);
            }
        }

        public char getValue() {
            return value;
        }

        public static Estrategia get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO")
    private Character tipo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTRATEGIA")
    private Character estrategia;

    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_MISION")
    private String idMision;

    @Transient
    private List<MisionJuego> misionJuego;
    
    @Transient
    private List<ImagenesRompecabezas> imagenes;

//    @JoinColumn(name = "ID_MISION", referencedColumnName = "ID_MISION", insertable = false, updatable = false)
//    @OneToOne(optional = false)
//    private Mision mision;
    public Juego() {
    }

    public Juego(String idMision) {
        this.idMision = idMision;
    }

    public Juego(String idMision, Character tipo, Character estrategia) {
        this.idMision = idMision;
        this.tipo = tipo;
        this.estrategia = estrategia;

    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public Character getEstrategia() {
        return estrategia;
    }

    public void setEstrategia(Character estrategia) {
        this.estrategia = estrategia;
    }

    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    public List<MisionJuego> getMisionJuego() {
        return misionJuego;
    }

    public void setMisionJuego(List<MisionJuego> misionJuego) {
        this.misionJuego = misionJuego;
    }

    public String getIdInstanciaMillonario() {
        return idInstanciaMillonario;
    }

    public void setIdInstanciaMillonario(String idInstanciaMillonario) {
        this.idInstanciaMillonario = idInstanciaMillonario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMision != null ? idMision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Juego)) {
            return false;
        }
        Juego other = (Juego) object;
        if ((this.idMision == null && other.idMision != null) || (this.idMision != null && !this.idMision.equals(other.idMision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Juego{" + "imagen=" + imagen + ", tipo=" + tipo + ", estrategia=" + estrategia + ", idMision=" + idMision + '}';
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getImagenesRompecabezas() {
        return imagenesRompecabezas;
    }

    public void setImagenesRompecabezas(String imagenesRompecabezas) {
        this.imagenesRompecabezas = imagenesRompecabezas;
    }

    public List<ImagenesRompecabezas> getImagenes() {
        return imagenes;
    }

    public void setImagenes(List<ImagenesRompecabezas> imagenes) {
        this.imagenes = imagenes;
    }

    public Integer getTiempo() {
        return tiempo;
    }

    public void setTiempo(Integer tiempo) {
        this.tiempo = tiempo;
    }

    public String getIdJuegoMillonario() {
        return idJuegoMillonario;
    }

    public void setIdJuegoMillonario(String idJuegoMillonario) {
        this.idJuegoMillonario = idJuegoMillonario;
    }
}
