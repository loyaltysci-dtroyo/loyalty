/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import java.util.List;
import java.util.Locale;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;

/**
 *
 * @author wtencio
 */
@Stateless
public class ConfiguracionEvertecBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Método que obtiene un listado de todas las insignias que han sido
     * obtenidas por el miembro en sesion
     *
     * @param locale
     * @return lista de insignias obtenidas
     */
    public ConfiguracionGeneral getConfiguracionEvertec(Locale locale) {
        ConfiguracionGeneral config = new ConfiguracionGeneral();
        try {
            List<ConfiguracionGeneral> configuraciones = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
            if (configuraciones.isEmpty()) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "configuracionGeneral_notFound");
            }
            ConfiguracionGeneral configuracionGeneral = configuraciones.get(0);
            config.setxFpSecuencia(configuracionGeneral.getxFpSecuencia());
            config.setIdProvider(configuracionGeneral.getIdProvider());
            config.setKeyEvertec(configuracionGeneral.getKeyEvertec());
            config.setIvEvertec(configuracionGeneral.getIvEvertec());
            config.setCurrencyCod(configuracionGeneral.getCurrencyCod());
            config.setUsuarioEver(configuracionGeneral.getUsuarioEver());
            config.setPasswordEver(configuracionGeneral.getPasswordEver());
            config.setInvoiceSeq(configuracionGeneral.getInvoiceSeq());
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }
        return config;
    }
}
