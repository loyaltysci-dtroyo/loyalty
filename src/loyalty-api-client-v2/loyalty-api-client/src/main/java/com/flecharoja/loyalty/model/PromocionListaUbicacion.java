package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PROMOCION_LISTA_UBICACION")
@XmlRootElement
public class PromocionListaUbicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PromocionListaUbicacionPK promocionListaUbicacionPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;

    public PromocionListaUbicacion() {
    }

    public PromocionListaUbicacionPK getPromocionListaUbicacionPK() {
        return promocionListaUbicacionPK;
    }

    public void setPromocionListaUbicacionPK(PromocionListaUbicacionPK promocionListaUbicacionPK) {
        this.promocionListaUbicacionPK = promocionListaUbicacionPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo!=null?Character.toUpperCase(indTipo):null;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (promocionListaUbicacionPK != null ? promocionListaUbicacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromocionListaUbicacion)) {
            return false;
        }
        PromocionListaUbicacion other = (PromocionListaUbicacion) object;
        if ((this.promocionListaUbicacionPK == null && other.promocionListaUbicacionPK != null) || (this.promocionListaUbicacionPK != null && !this.promocionListaUbicacionPK.equals(other.promocionListaUbicacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.PromocionListaUbicacion[ promocionListaUbicacionPK=" + promocionListaUbicacionPK + " ]";
    }
    
}
