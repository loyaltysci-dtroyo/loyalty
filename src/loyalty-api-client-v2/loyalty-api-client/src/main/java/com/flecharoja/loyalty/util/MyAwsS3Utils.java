package com.flecharoja.loyalty.util;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.util.Base64;
import java.util.Properties;
import java.util.UUID;

/**
 *
 * @author svargas
 */
public class MyAwsS3Utils implements AutoCloseable {
    
    public enum Folder {
        ARTE_NOTICIA("images/news"),
        MEDIA_RESPUESTA_MISION("media/answers-challenge"),
        AVATAR_MIEMBRO("images/client"),
        OTROS_IMAGENES("images/other");
        
        private final String path;

        private Folder(String path) {
            this.path = path;
        }

        public String getPath() {
            return path;
        }
    }
    
    private final Properties s3Properties;
    private final AmazonS3 s3Client;

    public MyAwsS3Utils() throws IOException, NullPointerException {
        this.s3Properties = new Properties();
        s3Properties.load(this.getClass().getResourceAsStream("/conf/aws/s3.properties"));
        
        AmazonS3ClientBuilder s3ClientBuilder = AmazonS3ClientBuilder.standard();
        s3ClientBuilder.setCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(s3Properties.getProperty("accessKey"), s3Properties.getProperty("secretKey"))));
        s3ClientBuilder.setRegion(s3Properties.getProperty("region"));
        this.s3Client = s3ClientBuilder.build();
    }
    
    /**
     * Metodo que almacena en AWS S3 una imagen codificada en base64 en el
     * bucket configurado bajo un nombre especifico (reemplaza archivos
     * existentes) en un directorio en especifico
     *
     * @param base64 datos del archivo a almacenar
     * @param folder si no es null, directorio en donde almacenar el archivo
     * @param filename nombre del archivo
     * @return enlace publico del archivo almacenado
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws NullPointerException
     * @throws SdkClientException
     * @throws AmazonServiceException
     */
    public String uploadImage(String base64, Folder folder, String filename) throws IOException, IllegalArgumentException, NullPointerException, SdkClientException, AmazonServiceException {
        byte[] fileBytes = Base64.getDecoder().decode(base64);
        
        String fileType = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(fileBytes));
        if(fileType==null || !fileType.split("/")[0].equalsIgnoreCase("IMAGE")) {
            //no es del tipo de imagen
            throw new IllegalArgumentException();
        }
        
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(fileBytes.length);
        objectMetadata.setContentType(fileType);
        
        if (folder==null) {
            folder = Folder.OTROS_IMAGENES;
        }
        if (filename==null) {
            do {
                filename = folder.path+"/"+UUID.randomUUID().toString();
            } while (s3Client.doesObjectExist(s3Properties.getProperty("bucket"), filename));
        } else {
            filename = folder.path+"/"+filename;
        }
        s3Client.putObject(new PutObjectRequest(s3Properties.getProperty("bucket"), filename, new ByteArrayInputStream(fileBytes), objectMetadata));
        s3Client.setObjectAcl(s3Properties.getProperty("bucket"), filename, CannedAccessControlList.PublicRead);
        
        return s3Client.getUrl(s3Properties.getProperty("bucket"), filename).toString();
    }
    
    /**
     * Metodo que intenta eliminar del bucket configurado un archivo segun su
     * enlace publico o ruta y nombre del archivo
     * 
     * @param filenameOrUrl ruta y nombre o enlace publico del archivo a eliminar
     * @throws NullPointerException
     * @throws SdkClientException
     * @throws AmazonServiceException 
     */
    public void deleteFile(String filenameOrUrl) throws NullPointerException, IllegalArgumentException, SdkClientException, AmazonServiceException {
        String bucket = s3Properties.getProperty("bucket");
        if (s3Client.doesObjectExist(bucket, filenameOrUrl)) {
            s3Client.deleteObject(bucket, filenameOrUrl);
        } else {
            String filename = new AmazonS3URI(filenameOrUrl).getKey();
            if (s3Client.doesObjectExist(bucket, filename)) {
                s3Client.deleteObject(bucket, filename);
            }
        }
    }
    
    public String uploadMediaMision(String base64, String fileType) throws IOException, IllegalArgumentException, NullPointerException, SdkClientException, AmazonServiceException {
        byte[] fileBytes = Base64.getDecoder().decode(base64);
        
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(fileBytes.length);
        objectMetadata.setContentType(fileType);
        
        String filename;
        do {
            filename = Folder.MEDIA_RESPUESTA_MISION.path+"/"+UUID.randomUUID().toString();
        } while (s3Client.doesObjectExist(s3Properties.getProperty("bucket"), filename));
        
        s3Client.putObject(new PutObjectRequest(s3Properties.getProperty("bucket"), filename, new ByteArrayInputStream(fileBytes), objectMetadata));
        s3Client.setObjectAcl(s3Properties.getProperty("bucket"), filename, CannedAccessControlList.PublicRead);
        
        return s3Client.getUrl(s3Properties.getProperty("bucket"), filename).toString();
    }

    @Override
    public void close() throws Exception {
        this.s3Properties.clear();
        this.s3Client.shutdown();
    }
}
