package com.flecharoja.loyalty.exception;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.jboss.resteasy.spi.DefaultOptionsMethodException;
import org.jboss.resteasy.spi.Failure;
import org.jboss.resteasy.spi.LoggableFailure;

/**
 *
 * @author svargas
 */
@Provider
public class UncaughtExceptionMapper implements ExceptionMapper<Throwable>{

    @Override
    public Response toResponse(Throwable exception) {
        //no sobreescritura de excepciones internas de resteasy
        if (exception instanceof DefaultOptionsMethodException) {
            return ((DefaultOptionsMethodException) exception).getResponse();
        }
        if (exception instanceof Failure) {
            return ((Failure) exception).getResponse();
        }
        if (exception instanceof LoggableFailure) {
            return ((LoggableFailure) exception).getResponse();
        }
        
        if (exception instanceof WebApplicationException) {
            return ((WebApplicationException) exception).getResponse();
        }
        
        if (exception instanceof MyException) {
            return ((MyException) exception).getResponse();
        }
        
        if (exception instanceof javax.json.JsonException
                || exception instanceof com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException
                || exception instanceof org.codehaus.jackson.map.exc.UnrecognizedPropertyException) {
            Logger.getLogger(exception.getClass().getName()).log(Level.SEVERE, null, exception);
            return new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, Locale.US, "data_format_invalid").getResponse();
        }
        
        Logger.getLogger(exception.getClass().getName()).log(Level.SEVERE, null, exception);
        return new MyException(MyException.ErrorSistema.ERROR_DESCONOCIDO, Locale.US, "unknown_error").getResponse();
    }
    
}
