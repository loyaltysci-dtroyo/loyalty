/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "ATRIBUTO_DINAMICO_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AtributoDinamicoProducto.findAll", query = "SELECT a FROM AtributoDinamicoProducto a"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByIdAtributo", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.idAtributo = :idAtributo")
})
public class AtributoDinamicoProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_ATRIBUTO")
    @ApiModelProperty(value = "Identificador único y autogenerado del atributo dinámico del producto",required = true)
    private String idAtributo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    @ApiModelProperty(value = "Nombre dado al atributo dinámico del producto",required = true)
    private String nombre;
    
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    @ApiModelProperty(value = "Descripción básica del atributo dinámico del producto")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    @ApiModelProperty(value = "Indicador del tipo de dato que será el atributo dinámico del producto", example = "Texto, númerico, opciones...",required = true)
    private Character indTipo;
    
    @Size(max = 300)
    @Column(name = "VALOR_OPCIONES")
    @ApiModelProperty(value = "Valores que podrá tener el atributo dinámico del producto si el tipo es de opciones", example = "Tipo=talla, Opciones = S,M,L,XL")
    private String valorOpciones;
    
   

    public AtributoDinamicoProducto() {
    }

    public AtributoDinamicoProducto(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public String getValorOpciones() {
        return valorOpciones;
    }

    public void setValorOpciones(String valorOpciones) {
        this.valorOpciones = valorOpciones;
    }

 
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtributo != null ? idAtributo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtributoDinamicoProducto)) {
            return false;
        }
        AtributoDinamicoProducto other = (AtributoDinamicoProducto) object;
        if ((this.idAtributo == null && other.idAtributo != null) || (this.idAtributo != null && !this.idAtributo.equals(other.idAtributo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.AtributoDinamicoProducto[ idAtributo=" + idAtributo + " ]";
    }
    
}
