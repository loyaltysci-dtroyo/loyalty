package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.MiembroBalanceMetrica;
import com.flecharoja.loyalty.model.MiembroBalanceMetricaPK;
import com.flecharoja.loyalty.model.MisionDetails;
import com.flecharoja.loyalty.model.NivelMetrica;
import com.flecharoja.loyalty.model.ProgressBar;
import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.model.RegistroMetricaResumen;
import com.flecharoja.loyalty.model.TransaccionCompra;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author wtencio, svargas
 */
@Stateless
public class BalanceBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @EJB
    RegistroMetricaService registroMetricaService;

    /**
     * Método que obtiene el balance actual del miembro en sesion
     * @param idMiembro identificador del miembro
     * @param idMetrica
     * @param locale
     * @return balance
     */
    public MiembroBalanceMetrica getBalance(String idMiembro, String idMetrica, Locale locale) {
        if (idMetrica == null) {
            List<String> metrica = em.createNamedQuery("ConfiguracionGeneral.findMetrica").getResultList();
            if (!metrica.isEmpty()) {
                idMetrica = metrica.get(0);
            } else {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "main_metric_not_found");
            }
        } else {
            Metrica metrica = em.find(Metrica.class, idMetrica);
            if (metrica == null) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "main_metric_not_found");
            }
        }

        MiembroBalanceMetrica balance = em.find(MiembroBalanceMetrica.class, new MiembroBalanceMetricaPK(idMiembro, idMetrica));
        
        if (balance==null) {
            balance = new MiembroBalanceMetrica(idMiembro, idMetrica);
            balance.setMetrica(em.find(Metrica.class, idMetrica));
        }
        
        return balance;
    }
    
    /**
     * Método que obtiene el balance actual del miembro en sesion
     * @param idMiembro identificador del miembro
     * @param locale
     * @return balance
     */
    public MiembroBalanceMetrica getBalanceSecundaria(String idMiembro, Locale locale) {
        List<String> metrica = em.createNamedQuery("ConfiguracionGeneral.findMetricaSecundaria").getResultList();
        String idMetrica;
        if (!metrica.isEmpty()) {
            idMetrica = metrica.get(0);
        } else {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "main_metric_not_found");
        }

        MiembroBalanceMetrica balance = em.find(MiembroBalanceMetrica.class, new MiembroBalanceMetricaPK(idMiembro, idMetrica));
        
        if (balance==null) {
            balance = new MiembroBalanceMetrica(idMiembro, idMetrica);
            balance.setMetrica(em.find(Metrica.class, idMetrica));
        }
        
        return balance;
    }

    /**
     * Método que obtiene el historial de registros de metricas
     * @param idMiembro identificador del miembro
     * @param idMetrica
     * @param locale
     * @return registros de metricas
     */
    public List<RegistroMetricaResumen> getHistorial(String idMiembro, String idMetrica, Locale locale) {
        List<RegistroMetricaResumen> resumen;
        if (idMetrica == null) {
            List<String> metrica = em.createNamedQuery("ConfiguracionGeneral.findMetrica").getResultList();
            if (!metrica.isEmpty()) {
                idMetrica = metrica.get(0);
            } else {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "main_metric_not_found");
            }
        } else {
            Metrica metrica = em.find(Metrica.class, idMetrica);
            if (metrica == null) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "main_metric_not_found");
            }
        }

        List<RegistroMetrica> registros = registroMetricaService.getHistorialMetricasMiembro(idMiembro, idMetrica);

        resumen = registros.stream().map((RegistroMetrica r) -> {
            RegistroMetricaResumen registro = new RegistroMetricaResumen();
            
            registro.setIndFormaGanada(r.getIndTipoGane());
            
            RegistroMetrica.TiposGane tipoGane = RegistroMetrica.TiposGane.get(r.getIndTipoGane());
            switch(tipoGane==null?RegistroMetrica.TiposGane.DESCONOCIDO:tipoGane) {
                case MISION: {
                    registro.setNombreMision(em.find(MisionDetails.class, r.getIdMision()).getEncabezadoArte());
                    break;
                }
                case TRANSACCION: {
                    TransaccionCompra transaccion = em.find(TransaccionCompra.class, r.getIdTransaccion());
                    registro.setTransaccion(new RegistroMetricaResumen(transaccion.getTotal(), transaccion.getIdUbicacion()!=null?transaccion.getIdUbicacion().getNombre():null).getTransaccion());
                    break;
                }
            }
            
            registro.setCantidadGanada(r.getCantidad());
            registro.setFecha(r.getRegistroMetricaPK().getFecha());
            return registro;
        }).collect(Collectors.toList());

        return resumen;
    }

    /**
     * Método que obtiene el progreso en niveles de la metrica principal del miembro en sesion
     * @param idMiembro identificador del miembro
     * @param idMetrica
     * @param locale
     * @return progreso
     */
    public ProgressBar getProgressBar(String idMiembro, String idMetrica, Locale locale) {
        ProgressBar progressBar = new ProgressBar();
        
        if (idMetrica == null) {
            List<String> metrica = em.createNamedQuery("ConfiguracionGeneral.findMetrica").getResultList();
            if (!metrica.isEmpty()) {
                idMetrica = metrica.get(0);
            } else {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "main_metric_not_found");
            }
        } else {
            Metrica metrica = em.find(Metrica.class, idMetrica);
            if (metrica == null) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "main_metric_not_found");
            }
        }
        
        //obtengo la metrica
        Metrica metrica = em.find(Metrica.class, idMetrica);
        //obtengo el grupo de nivel de metrica
        String grupo = metrica.getGrupoNiveles().getIdGrupoNivel();
        //obtengo la lista de niveles
        List<NivelMetrica> niveles = metrica.getGrupoNiveles().getNivelMetricaList();
        if (niveles == null) {
            niveles = new ArrayList<>();
        }
        niveles = niveles.stream().sorted((o1, o2) -> Double.compare(o1.getMetricaInicial(), o2.getMetricaInicial())).collect(Collectors.toList());
        //obtengo el balance del miembro con la metrica
        MiembroBalanceMetrica balance = em.find(MiembroBalanceMetrica.class, new MiembroBalanceMetricaPK(idMiembro, idMetrica));
        if (balance==null) {
            balance = new MiembroBalanceMetrica(idMiembro, idMetrica);
        }
        
        //seteo los valores
        progressBar.setMetrica(metrica);
        progressBar.setNiveles(niveles);
        progressBar.setProgreso(balance.getProgresoActual());
        progressBar.setTope(niveles.get(niveles.size()-1).getMetricaInicial());
        NivelMetrica nivelActual = null;
        for (NivelMetrica t : niveles) {
            if (t.getMetricaInicial().compareTo(balance.getProgresoActual())<=0) {
                nivelActual = t;
            } else {
                break;
            }
        }
        progressBar.setNivelActual(nivelActual);

        return progressBar;
    }

}
