/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO_MISION_JUEGO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MisionJuego.findAll", query = "SELECT m FROM MisionJuego m")
    , @NamedQuery(name = "MisionJuego.findByIdMision", query = "SELECT m FROM MisionJuego m WHERE m.idJuego.idMision = :idMision")
})
public class MisionJuego implements Serializable {

    public enum IntervalosTiempoRespuesta {
        POR_SIEMPRE('A'),
        POR_MINUTO('B'),
        POR_HORA('C'),
        POR_DIA('D'),
        POR_SEMANA('E'),
        POR_MES('F');

        private final char value;
        private static final Map<Character, IntervalosTiempoRespuesta> lookup = new HashMap<>();

        private IntervalosTiempoRespuesta(char value) {
            this.value = value;
        }

        static {
            for (IntervalosTiempoRespuesta tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static IntervalosTiempoRespuesta get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "PROBABILIDAD")
    @ApiModelProperty(value = "Probabilidad de ganar el juego un miembro", required = true)
    private Double probabilidad;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_RESPUESTA")
    @ApiModelProperty(value = "Indicador de respuesta, si es que se quieren restricciones para ganar", required = true)
    private Boolean indRespuesta;

    @JoinColumn(name = "METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne
    @ApiModelProperty(value = "Información de la métrica si el premio es metrica")
    private Metrica metrica;

    public enum Tipo {
        PREMIO('P'),
        METRICA('M'),
        GRACIAS('G');

        private final char value;
        private static final Map<Character, Tipo> lookup = new HashMap<>();

        private Tipo(char value) {
            this.value = value;
        }

        static {
            for (Tipo tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static Tipo get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "mision_juego_uuid")
    @GenericGenerator(name = "mision_juego_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_MISION_JUEGO")
    @ApiModelProperty(value = "Identificador de la mision juego")
    private String idMisionJuego;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_PREMIO")
    @ApiModelProperty(value = "Indicador del tipo de premio a ganar")
    private Character indTipoPremio;

    @Column(name = "CANT_INTER_TOTAL")
    @ApiModelProperty(value = "Cantidad de respuestas maximas en total para todos los miembros, si el indRespuesta es verdadero")
    private Long cantInterTotal;

    @Column(name = "IND_INTER_TOTAL")
    @ApiModelProperty(value = "Indicador de frecuencia de las respuestas totales para todos los miembros, si el indRespuesta es verdadero")
    private Character indInterTotal;

    @Column(name = "CANT_INTER_MIEMBRO")
    @ApiModelProperty(value = "Cantidad de respuestas maximas en total para un miembro, si el indRespuesta es verdadero")
    private Long cantInterMiembro;

    @Column(name = "IND_INTER_MIEMBRO")
    @ApiModelProperty(value = "Indicador de frecuencia de las respuestas totales para un miembro, si el indRespuesta es verdadero")
    private Character indInterMiembro;

    @Column(name = "CANT_METRICA")
    @ApiModelProperty(value = "Cantidad de metrica a ganar, si el premio es metrica")
    private Double cantMetrica;

    @Transient
    private Boolean isGanar;

    @JoinColumn(name = "ID_JUEGO", referencedColumnName = "ID_MISION")
    @ManyToOne(optional = false)
    @ApiModelProperty(value = "Identificador del juego al que pertenece la descripcion de mision juego", required = true)
    private Juego idJuego;

    @JoinColumn(name = "ID_PREMIO", referencedColumnName = "ID_PREMIO")
    @ManyToOne
    @ApiModelProperty(value = "Informacion del premio, si lo que se gana en el juego es un premio fisico o certificado")
    private Premio premio;

    public MisionJuego() {
    }

    public MisionJuego(String idMisionJuego) {
        this.idMisionJuego = idMisionJuego;
    }

    public MisionJuego(String idMisionJuego, Character indTipoPremio, Double probabilidad, Boolean indRespuesta) {
        this.idMisionJuego = idMisionJuego;
        this.indTipoPremio = indTipoPremio;
        this.probabilidad = probabilidad;
        this.indRespuesta = indRespuesta;

    }

    public String getIdMisionJuego() {
        return idMisionJuego;
    }

    public void setIdMisionJuego(String idMisionJuego) {
        this.idMisionJuego = idMisionJuego;
    }

    public Character getIndTipoPremio() {
        return indTipoPremio;
    }

    public void setIndTipoPremio(Character indTipoPremio) {
        this.indTipoPremio = indTipoPremio;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Boolean getIndRespuesta() {
        return indRespuesta;
    }

    public void setIndRespuesta(Boolean indRespuesta) {
        this.indRespuesta = indRespuesta;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Long getCantInterTotal() {
        return cantInterTotal;
    }

    public void setCantInterTotal(Long cantInterTotal) {
        this.cantInterTotal = cantInterTotal;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Character getIndInterTotal() {
        return indInterTotal;
    }

    public void setIndInterTotal(Character indInterTotal) {
        this.indInterTotal = indInterTotal;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Long getCantInterMiembro() {
        return cantInterMiembro;
    }

    public void setCantInterMiembro(Long cantInterMiembro) {
        this.cantInterMiembro = cantInterMiembro;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Character getIndInterMiembro() {
        return indInterMiembro;
    }

    public void setIndInterMiembro(Character indInterMiembro) {
        this.indInterMiembro = indInterMiembro;
    }

    public Double getCantMetrica() {
        return cantMetrica;
    }

    public void setCantMetrica(Double cantMetrica) {
        this.cantMetrica = cantMetrica;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Juego getIdJuego() {
        return idJuego;
    }

    public void setIdJuego(Juego idJuego) {
        this.idJuego = idJuego;
    }

    public Premio getPremio() {
        return premio;
    }

    public void setPremio(Premio idPremio) {
        this.premio = idPremio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMisionJuego != null ? idMisionJuego.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionJuego)) {
            return false;
        }
        MisionJuego other = (MisionJuego) object;
        if ((this.idMisionJuego == null && other.idMisionJuego != null) || (this.idMisionJuego != null && !this.idMisionJuego.equals(other.idMisionJuego))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MisionJuego{" + "probabilidad=" + probabilidad + ", indRespuesta=" + indRespuesta + ", metrica=" + metrica + ", idMisionJuego=" + idMisionJuego + ", indTipoPremio=" + indTipoPremio + ", cantInterTotal=" + cantInterTotal + ", indInterTotal=" + indInterTotal + ", cantInterMiembro=" + cantInterMiembro + ", indInterMiembro=" + indInterMiembro + ", cantMetrica=" + cantMetrica + ", idJuego=" + idJuego + ", idPremio=" + premio + '}';
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Double getProbabilidad() {
        return probabilidad;
    }

    public void setProbabilidad(Double probabilidad) {
        this.probabilidad = probabilidad;
    }

    public Boolean getIsGanar() {
        return isGanar;
    }

    public void setIsGanar(Boolean isGanar) {
        this.isGanar = isGanar;
    }
    
    

}
