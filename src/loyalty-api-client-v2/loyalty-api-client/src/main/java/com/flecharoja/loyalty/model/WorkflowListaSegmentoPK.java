package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author svargas
 */
@Embeddable
public class WorkflowListaSegmentoPK implements Serializable {

    @Column(name = "ID_SEGMENTO")
    private String idSegmento;
    
    @Column(name = "ID_WORKFLOW")
    private String idWorkflow;

    public WorkflowListaSegmentoPK() {
    }

    public WorkflowListaSegmentoPK(String idSegmento, String idWorkflow) {
        this.idSegmento = idSegmento;
        this.idWorkflow = idWorkflow;
    }

    public String getIdSegmento() {
        return idSegmento;
    }

    public void setIdSegmento(String idSegmento) {
        this.idSegmento = idSegmento;
    }

    public String getIdWorkflow() {
        return idWorkflow;
    }

    public void setIdWorkflow(String idWorkflow) {
        this.idWorkflow = idWorkflow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSegmento != null ? idSegmento.hashCode() : 0);
        hash += (idWorkflow != null ? idWorkflow.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkflowListaSegmentoPK)) {
            return false;
        }
        WorkflowListaSegmentoPK other = (WorkflowListaSegmentoPK) object;
        if ((this.idSegmento == null && other.idSegmento != null) || (this.idSegmento != null && !this.idSegmento.equals(other.idSegmento))) {
            return false;
        }
        if ((this.idWorkflow == null && other.idWorkflow != null) || (this.idWorkflow != null && !this.idWorkflow.equals(other.idWorkflow))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.WorkflowListaSegmentoPK[ idSegmento=" + idSegmento + ", idWorkflow=" + idWorkflow + " ]";
    }
    
}
