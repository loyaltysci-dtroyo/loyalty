/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CATALOGO_IDENTIFICACIONES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatalogoIdentificaciones.findAll", query = "SELECT c FROM CatalogoIdentificaciones c"),
    @NamedQuery(name = "CatalogoIdentificaciones.findByIdIdentificacion", query = "SELECT c FROM CatalogoIdentificaciones c WHERE c.idIdentificacion = :idIdentificacion")
})
public class CatalogoIdentificaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_IDENTIFICACION")
    private String idIdentificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IDENTIFICADOR")
    private Character identificador;

    public CatalogoIdentificaciones() {
    }

    public CatalogoIdentificaciones(String idIdentificacion) {
        this.idIdentificacion = idIdentificacion;
    }

    public String getIdIdentificacion() {
        return idIdentificacion;
    }

    public void setIdIdentificacion(String idIdentificacion) {
        this.idIdentificacion = idIdentificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Character getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Character identificador) {
        this.identificador = identificador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idIdentificacion != null ? idIdentificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatalogoIdentificaciones)) {
            return false;
        }
        CatalogoIdentificaciones other = (CatalogoIdentificaciones) object;
        if ((this.idIdentificacion == null && other.idIdentificacion != null) || (this.idIdentificacion != null && !this.idIdentificacion.equals(other.idIdentificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CatalogoIdentificaciones[ idIdentificacion=" + idIdentificacion + " ]";
    }

              
}
