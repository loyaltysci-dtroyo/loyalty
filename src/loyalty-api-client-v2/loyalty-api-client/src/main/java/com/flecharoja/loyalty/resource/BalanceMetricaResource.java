package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.MiembroBalanceMetrica;
import com.flecharoja.loyalty.model.ProgressBar;
import com.flecharoja.loyalty.model.RegistroMetricaResumen;
import com.flecharoja.loyalty.service.BalanceBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "Perfil del cliente")
@Path("perfil/metrica/{idMetrica}")
public class BalanceMetricaResource {
    
    @EJB
    BalanceBean balanceBean;
    
    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;
    
    /**
     * Identificador de premio
     */
    @PathParam("idMetrica")
    String idMetrica;
    
    /**
     * Método que obtiene el balance de métrica de un usuario en sesión
     *
     * @return informacion del miembro
     */
    @ApiOperation(value = "Obtener el balance de métrica del miembro en sesión",
            response = MiembroBalanceMetrica.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("balance")
    @Produces(MediaType.APPLICATION_JSON)
    public MiembroBalanceMetrica getBalance() {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        
       return balanceBean.getBalance(idMiembro, idMetrica,request.getLocale());
    }
    
     /**
     * Método que obtiene un historial del balance de metrica
     *
     * @return informacion del miembro
     */
    @ApiOperation(value = "Obtener historial del balance de métrica del miembro en sesión",
            responseContainer = "List",
            response = RegistroMetricaResumen.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("balance/historial")
    @Produces(MediaType.APPLICATION_JSON)
    public List<RegistroMetricaResumen> getHistorialBalance() {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        
       return balanceBean.getHistorial(idMiembro, idMetrica ,request.getLocale());
    }
    
    /**
     * Método que obtiene un historial del balance de metrica
     *
     * @return informacion del miembro
     */
    @ApiOperation(value = "Obtener historial del balance de métrica del miembro en sesión",
            response = ProgressBar.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("balance/progreso")
    @Produces(MediaType.APPLICATION_JSON)
    public ProgressBar getProgressBar() {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return balanceBean.getProgressBar(idMiembro, idMetrica, request.getLocale());
    }
}
