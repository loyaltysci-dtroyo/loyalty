package com.flecharoja.loyalty.util;

/*
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import javax.imageio.ImageIO;
import org.jboss.logging.Logger;
import org.jclouds.ContextBuilder;
import org.jclouds.io.Payloads;
import org.jclouds.io.payloads.ByteArrayPayload;
import org.jclouds.openstack.swift.v1.features.ObjectApi;
import org.jclouds.rackspace.cloudfiles.v1.CloudFilesApi;
import org.jclouds.rackspace.cloudfiles.v1.domain.CDNContainer;
*/

/**
 *
 * @author svargas
 */
public class MyCloudFilesUtils {

/*
    private static final String RACKSPACE_CDN_USERNAME = "codecr";
    private static final String RACKSPACE_CDN_APIKEY = "6f73754e82ef4c352e70feffe9daceac";
    private static final String RACKSPACE_PROVIDER = "rackspace-cloudfiles-us";
    private static final String RACKSPACE_REGION = "DFW";
    private static final String RACKSPACE_CONTENEDOR = "loyalty-files";
*/

    /**
     * Metodo que almacena un nuevo archivo basandose en su codificacion en base64 e suponiendo la extension del mismo, se retornara una cadena de texto
     * con el valor de una url publicamente accesible
     *
     * @param base64 Archivo codificado
     * @return URL del archivo almacenado
     * @throws CloudFilesException
     */
    public String uploadImagen(String base64) {
        return null;
/*
        if (base64==null || base64.trim().isEmpty()) {
            throw new CloudFilesException();
        }
        //decodificacion del string a bytes y verificacion de que sea un base64 valido
        byte[] fileBytes;
        try {
            fileBytes = Base64.getDecoder().decode(base64);
        } catch (IllegalArgumentException e) {
            //no es un base64 valido
            throw new CloudFilesException();
        }
        
        try (CloudFilesApi api = ContextBuilder.newBuilder(RACKSPACE_PROVIDER).credentials(RACKSPACE_CDN_USERNAME, RACKSPACE_CDN_APIKEY).buildApi(CloudFilesApi.class)) {
            //creacion del contenedor y activacion de CDN si no existe
            if (api.getContainerApi(RACKSPACE_REGION).get(RACKSPACE_CONTENEDOR)==null) {
                api.getContainerApi(RACKSPACE_REGION).create(RACKSPACE_CONTENEDOR);
                api.getCDNApi(RACKSPACE_REGION).enable(RACKSPACE_CONTENEDOR);
            }

            //obtencion del tipo de archivo
            String fileType;
            try {
                fileType = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(fileBytes));
            } catch (IOException e) {
                //no se pudo saber el mimetype
                throw new CloudFilesException();
            }
            if(fileType==null || !fileType.split("/")[0].equalsIgnoreCase("IMAGE")) {
                //no es del tipo de imagen
                throw new CloudFilesException();
            }

            //obtension de la extension a utilizar
            String fileExtension = fileType.split("/")[1];

            ByteArrayInputStream in = new ByteArrayInputStream(fileBytes);
            List<byte[]> subFilesBytes = new ArrayList<>();
            try {
                BufferedImage img = ImageIO.read(in);

                int[] heights = {200,400,800};

                for (int height : heights) {
                    int width = (height*img.getWidth())/img.getHeight();

                    Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                    BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                    imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0,0,0), null);

                    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

                    ImageIO.write(imageBuff, fileExtension, buffer);

                    subFilesBytes.add(buffer.toByteArray());
                }
            } catch (IOException e) {
                throw new CloudFilesException();
            }

            //creacion del nombre de archivo
            String filename = UUID.randomUUID().toString();

            //verificacion de que no exista otro archivo igual y regeneracion de nuevos nombres hasta encontrar uno valido
            //practicamente no pasa... pero no nunca
            while (api.getObjectApi(RACKSPACE_REGION, RACKSPACE_CONTENEDOR).getWithoutBody(filename.concat("."+fileExtension))!=null) {
                filename = UUID.randomUUID().toString();
            }

            //subida del archivo a cloudfiles
            ObjectApi objectApi = api.getObjectApi(RACKSPACE_REGION, RACKSPACE_CONTENEDOR);

            ByteArrayPayload payload = Payloads.newByteArrayPayload(fileBytes);
            objectApi.put(filename.concat("."+fileExtension), payload);
            payload = Payloads.newByteArrayPayload(subFilesBytes.get(0));
            objectApi.put(filename.concat("_small."+fileExtension), payload);
            payload = Payloads.newByteArrayPayload(subFilesBytes.get(1));
            objectApi.put(filename.concat("_medium."+fileExtension), payload);
            payload = Payloads.newByteArrayPayload(subFilesBytes.get(2));
            objectApi.put(filename.concat("_large."+fileExtension), payload);

            //verificacion de la existencia de los archivos
            if (objectApi.getWithoutBody(filename.concat("."+fileExtension))==null ||
                    objectApi.getWithoutBody(filename.concat("_small."+fileExtension))==null ||
                    objectApi.getWithoutBody(filename.concat("_medium."+fileExtension))==null ||
                    objectApi.getWithoutBody(filename.concat("_large."+fileExtension))==null) {
                //borrado de posibles residuos
                objectApi.delete(filename.concat("."+fileExtension));
                objectApi.delete(filename.concat("_small."+fileExtension));
                objectApi.delete(filename.concat("_medium."+fileExtension));
                objectApi.delete(filename.concat("_large."+fileExtension));
                throw new CloudFilesException();
            }

            //obtencion del enlace creado
            CDNContainer container = api.getCDNApi(RACKSPACE_REGION).get(RACKSPACE_CONTENEDOR);
            if (container!=null) {
                return container.getUri().toString()+"/"+filename.concat("."+fileExtension);
            } else {
                throw new CloudFilesException();
            }
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Logger.Level.WARN, e);
            throw new CloudFilesException();
        }
*/
    }
    
    /**
     * Metodo que intenta borrar del contenedor un recurso segun un url proveido (mientras que no sea la preterminada)
     *
     * @param oldUrl Valor del url del recurso a eliminar
     */
    public void deleteImagen(String oldUrl) {
/*
        if (oldUrl!=null && !oldUrl.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)){
            try (CloudFilesApi api = ContextBuilder.newBuilder(RACKSPACE_PROVIDER).credentials(RACKSPACE_CDN_USERNAME, RACKSPACE_CDN_APIKEY).buildApi(CloudFilesApi.class)) {
                ObjectApi objectApi = api.getObjectApi(RACKSPACE_REGION, RACKSPACE_CONTENEDOR);
                //borrado de archivos
                String filename = oldUrl.substring(oldUrl.lastIndexOf("/")+1, oldUrl.length());
                String[] temp = filename.split("\\.");
                objectApi.delete(filename);
                objectApi.delete(temp[0]+"_small."+temp[1]);
                objectApi.delete(temp[0]+"_medium."+temp[1]);
                objectApi.delete(temp[0]+"_large."+temp[1]);
            } catch (IOException | IllegalStateException e) {
                Logger.getLogger(this.getClass().getName()).log(Logger.Level.WARN, e);
            }
        }
*/
    }
}
