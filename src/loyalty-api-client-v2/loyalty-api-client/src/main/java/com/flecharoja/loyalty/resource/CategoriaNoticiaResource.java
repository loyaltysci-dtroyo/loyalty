package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.CategoriaNoticia;
import com.flecharoja.loyalty.service.CategoriaNoticiaBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 * Recursos para la lectura de categorias
 *
 * @author svargas
 */
@Api(value = "Noticias")
@Path("categoria-noticia")
public class CategoriaNoticiaResource {
    
    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;
    
    @EJB
    CategoriaNoticiaBean bean;
    
    @ApiOperation(value = "Obtener lista de categorias disponibles",
            responseContainer = "List",
            response = CategoriaNoticia.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<CategoriaNoticia> getCategorias() {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return bean.getCategoriasNoticias(request.getLocale());
    }

}
