/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

/**
 *
 * @author wtencio
 */
public class ImagenesRompecabezas {
    
    private String urlImagen;

    public ImagenesRompecabezas(String urlImagen) {
        this.urlImagen = urlImagen;
    }
    

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }
    
    
    
}
