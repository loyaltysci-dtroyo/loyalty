package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class NuevaPeticionCambioPassM implements Serializable {
    
    @ApiModelProperty(value = "Email de la cuenta de miembro",required = true)
    private String email;

    public NuevaPeticionCambioPassM() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
