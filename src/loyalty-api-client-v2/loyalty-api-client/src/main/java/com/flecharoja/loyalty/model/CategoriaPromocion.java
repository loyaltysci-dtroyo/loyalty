/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CATEGORIA_PROMOCION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoriaPromocion.findAll", query = "SELECT c FROM CategoriaPromocion c"),
    @NamedQuery(name = "CategoriaPromocion.findByIdCategoria", query = "SELECT c FROM CategoriaPromocion c WHERE c.idCategoria = :idCategoria"),
    @NamedQuery(name = "CategoriaPromocion.countAll", query = "SELECT COUNT(c.idCategoria) FROM CategoriaPromocion c")
})
public class CategoriaPromocion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_CATEGORIA")
    @ApiModelProperty(value = "Identificador único y autogenerado de la categoría de promoción",required = true)
    private String idCategoria;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
     @ApiModelProperty(value = "Nombre de la categoría de promoción",required = true)
    private String nombre;
    
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    @ApiModelProperty(value = "Descripción básica de la categoría de promoción")
    private String descripcion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    @ApiModelProperty(value = "Imagen representativa de la categoría de promoción",required = true)
    private String imagen;


    public CategoriaPromocion() {
    }

    public CategoriaPromocion(String idCategoria) {
        this.idCategoria = idCategoria;
    }


    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    } 

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCategoria != null ? idCategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaPromocion)) {
            return false;
        }
        CategoriaPromocion other = (CategoriaPromocion) object;
        if ((this.idCategoria == null && other.idCategoria != null) || (this.idCategoria != null && !this.idCategoria.equals(other.idCategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CategoriaPromocion[ idCategoria=" + idCategoria + " ]";
    }
    
}
