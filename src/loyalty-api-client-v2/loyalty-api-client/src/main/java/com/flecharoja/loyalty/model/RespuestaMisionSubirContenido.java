package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaMisionSubirContenido {
    @ApiModelProperty(value = "Datos para subir el contenido", required = true)
    private String datosContenido;

    public RespuestaMisionSubirContenido() {
    }

    public String getDatosContenido() {
        return datosContenido;
    }

    public void setDatosContenido(String datosContenido) {
        this.datosContenido = datosContenido;
    }
}
