package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio, svargas
 */
@Entity
@Table(name = "MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Miembro.getIdMiembroCorreo", query="SELECT m.idMiembro FROM Miembro m WHERE m.correo = :correo AND m.indEstadoMiembro = :indEstado"),
    @NamedQuery(name = "Miembro.findAll", query = "SELECT m FROM Miembro m"),
    @NamedQuery(name = "Miembro.countIdentificador", query="SELECT COUNT(m.docIdentificacion) FROM Miembro m WHERE m.docIdentificacion = :docIdentificacion"),
    @NamedQuery(name = "Miembro.countCorreo", query="SELECT COUNT(m.correo) FROM Miembro m WHERE m.correo = :correo"),
    @NamedQuery(name = "Miembro.findCorreo", query="SELECT m FROM Miembro m WHERE m.correo = :correo AND m.indEstadoMiembro = :indEstado"),
    @NamedQuery(name = "Miembro.findByIdMiembro", query = "SELECT m FROM Miembro m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "Miembro.getAvatarFromMiembro", query = "SELECT m.avatar FROM Miembro m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "Miembro.findByIndEstado", query = "SELECT m FROM Miembro m WHERE m.indEstadoMiembro = :indEstado")
})
public class Miembro implements Serializable {
    
    public enum Atributos {
        ID_MIEMBRO("idMiembro"),
        DOC_IDENTIFICACION("docIdentificacion"),
        DIRECCION("direccion"),
        CIUDAD_RESIDENCIA("ciudadResidencia"),
        ESTADO_RESIDENCIA("estadoResidencia"),
        PAIS_RESIDENCIA("paisResidencia"),
        CODIGO_POSTAL("codigoPostal"),
        TELEFONO_MOVIL("telefonoMovil"),
        FECHA_NACIMIENTO("fechaNacimiento"),
        IND_GENERO("indGenero"),
        IND_ESTADO_CIVIL("indEstadoCivil"),
        IND_EDUCACION("indEducacion"),
        INGRESO_ECONOMICO("ingresoEconomico"),
        AVATAR("avatar"),
        NOMBRE("nombre"),
        APELLIDO("apellido"),
        APELLIDO2("apellido2"),
        IND_CONTACTO_EMAIL("indContactoEmail"),
        IND_CONTACTO_SMS("indContactoSms"),
        IND_CONTACTO_NOTIFICACION("indContactoNotificacion"),
        IND_CONTACTO_ESTADO("indContactoEstado"),
        IND_HIJOS("indHijos"),
        CONTRASENA("contrasena"),
        CORREO("correo"),
        ATRIBUTO_DINAMICO("atributoDinamicos"),
        NUM_VERSION("numVersion");
        
        private final String value;
        private static final Map<String, Atributos> lookup = new HashMap<>();

        private Atributos(String value) {
            this.value = value;
        }
        
        static {
            for (Atributos atributo : values()) {
                lookup.put(atributo.value, atributo);
            }
        }

        public String getValue() {
            return value;
        }
        
        
        public static Atributos get (String value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum ValoresIndGenero {
        FEMENINO('F'),
        MASCULINO('M');
        
        private final char value;
        private static final Map<Character,ValoresIndGenero> lookup = new HashMap<>();
        private ValoresIndGenero(char value) {
            this.value = value;
        }
        static {
            for(ValoresIndGenero valor : values()){
                lookup.put(valor.value, valor);
            }
        }

        public char getValue() {
            return value;
        }
        public static ValoresIndGenero get(Character value){
            return value==null?null:lookup.get(value);
        }
        
        
    }
    
    public enum ValoresIndEstadoCivil {
        CASADO('C'),
        SOLTERO('S'),
        UNION_LIBRE('U'),
        NINGUNO('N');
        
        private final char value;
        private static final Map<Character, ValoresIndEstadoCivil> lookup = new HashMap<>();

        private ValoresIndEstadoCivil(char value) {
            this.value = value;
        }
        
        static {
            for (ValoresIndEstadoCivil valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static ValoresIndEstadoCivil get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum ValoresIndEducacion {
        PRIMARIA('P'),
        SECUNDARIA('S'),
        TECNICA('T'),
        UNIVERSITARIA('U'),
        GRADO('G'),
        POSTGRADO('R'),
        MASTER('M'),
        DOCTORADO('D');
        
        private final char value;
        private static final Map<Character, ValoresIndEducacion> lookup = new HashMap<>();

        private ValoresIndEducacion(char value) {
            this.value = value;
        }
        
        static {
            for (ValoresIndEducacion valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static ValoresIndEducacion get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Estados{
        ACTIVO('A'),
        INACTIVO('I');
        
        private final char value;
        private static final Map<Character,Estados> lookup =  new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static{
            for(Estados estado:values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        public static Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_MIEMBRO")
    @ApiModelProperty(value = "Identificador único y autogenerado de un miembro",required = true)
    private String idMiembro;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DOC_IDENTIFICACION")
    @ApiModelProperty(value = "Número de identificación de un miembro",required = true)
    private String docIdentificacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Fecha de registro de un miembro",required = true)
    private Date fechaIngreso;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO_MIEMBRO")
    @ApiModelProperty(value = "Indicador de estado de un miembro", example = "Activo, inactivo",required = true)
    private Character indEstadoMiembro;

    @Column(name = "FECHA_EXPIRACION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Fecha de expiración de cuenta de un miembro")
    private Date fechaExpiracion;

    @Size(max = 200)
    @Column(name = "DIRECCION")
    @ApiModelProperty(value = "Dirección de habitación de un miembro")
    private String direccion;

    @Size(max = 100)
    @Column(name = "CIUDAD_RESIDENCIA")
    @ApiModelProperty(value = "Ciudad de residencia de un miembro")
    private String ciudadResidencia;

    @Size(max = 100)
    @Column(name = "ESTADO_RESIDENCIA")
    @ApiModelProperty(value = "Estado de residencia de un miembro")
    private String estadoResidencia;

    @Size(max = 3)
    @Column(name = "PAIS_RESIDENCIA")
    @ApiModelProperty(value = "País de residencia de un miembro")
    private String paisResidencia;

    @Size(max = 10)
    @Column(name = "CODIGO_POSTAL")
    @ApiModelProperty(value = "Código postal que el miembro posee por lugar de habitación")
    private String codigoPostal;

    @Size(max = 15)
    @Column(name = "TELEFONO_MOVIL")
    @ApiModelProperty(value = "Teléfono móvil de un miembro")
    private String telefonoMovil;

    @Column(name = "FECHA_NACIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Fecha de nacimiento de un miembro")
    private Date fechaNacimiento;

    @Column(name = "IND_GENERO")
    @ApiModelProperty(value = "Indicador de género de un miembro", example = "Femenino o Masculino")
    private Character indGenero;

    @Column(name = "IND_CONTACTO_EMAIL")
    @ApiModelProperty(value = "Indicador de contacto por email de un miembro")
    private Boolean indContactoEmail;

    @Column(name = "IND_CONTACTO_SMS")
    @ApiModelProperty(value = "Indicador de contacto por sms de un miembro")
    private Boolean indContactoSms;

    @Column(name = "IND_CONTACTO_NOTIFICACION")
    @ApiModelProperty(value = "Indicador de contacto por notificación push de un miembro")
    private Boolean indContactoNotificacion;

    @Column(name = "IND_CONTACTO_ESTADO")
    private Boolean indContactoEstado;

    @Column(name = "IND_ESTADO_CIVIL")
    @ApiModelProperty(value = "Indicador estado civil de un miembro", example = "Casado, Soltero, Viudo...")
    private Character indEstadoCivil;

    @Size(max = 20)
    @Column(name = "FRECUENCIA_COMPRA")
    @ApiModelProperty(value = "Frecuencia de compra de un miembro")
    private String frecuenciaCompra;

    @Column(name = "IND_EDUCACION")
     @ApiModelProperty(value = "Indicador de educación de un miembro", example = "Primaria, Secundaria...")
    private Character indEducacion;

    @Column(name = "INGRESO_ECONOMICO")
     @ApiModelProperty(value = "Ingreso económico de un miembro")
    private Double ingresoEconomico;

    @Column(name = "IND_HIJOS")
     @ApiModelProperty(value = "Indicador si el miembro posee hijos")
    private Boolean indHijos;

    @Size(max = 500)
    @Column(name = "COMENTARIOS")
    @ApiModelProperty(value = "Comentarios de un miembro")
    private String comentarios;
    
    @Column(name = "IND_POLITICA")
    @ApiModelProperty(value = "Indicador de politicas")
    private Boolean indPolitica;
    
    @Column(name = "FECHA_POLITICA")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Fecha aceptacion de politicas")
    private Date fechaPolitica;
    
    @Version
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    @Size(max = 300)
    @Column(name = "AVATAR")
    @ApiModelProperty(value = "Imagen representiva de un miembro")
    private String avatar;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NOMBRE")
    @ApiModelProperty(value = "Nombre de un miembro",required = true)
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "APELLIDO")
    @ApiModelProperty(value = "Primer apellido de un miembro",required = true)
    private String apellido;

    @Size(max = 100)
    @Column(name = "APELLIDO2")
    @ApiModelProperty(value = "Segundo apellido de un miembro")
    private String apellido2;

    @Transient
    @ApiModelProperty(value = "Contraseña de la cuenta de un miembro, requerida para el registro e ingreso a la aplicación",required = true)
    private String contrasena;

    @Basic(optional = false)
    @NotNull
    @Column(name = "EMAIL")
    @ApiModelProperty(value = "Correo electrónico de un miembro", required = true)
    private String correo;

    @Transient
    @ApiModelProperty(value = "Nombre de usuario de un miembro, siempre es igual al correo",required = true)
    private String nombreUsuario;
    
    
    @Transient
    @ApiModelProperty(value = "Lista de atributos dinámicos para un miembro")
    private List<AtributoDinamico> atributoDinamicos;
    
    @Transient
    @ApiModelProperty(value = "Balance actual de un miembro, para la métrica general del sistema")
    private MiembroBalanceMetrica balance;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Column(name = "IND_CAMBIO_PASS")
    @ApiModelProperty(value = "Indicador sobre si es requerido el cambio de contraseña",required = true)
    private Boolean indCambioPass;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_MIEMBRO_SISTEMA")
    @ApiModelProperty(value = "Indicador sobre si el miembro es perteneciente completamente al sistema")
    private Boolean indMiembroSistema;

    public Miembro() {
        //todo miembro creado en el api cliente es automaticamente no miembro del sistema
        this.indMiembroSistema = false;
    }

    public Miembro(String idMiembro) {
        this();
        this.idMiembro = idMiembro;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getDocIdentificacion() {
        return docIdentificacion;
    }

    public void setDocIdentificacion(String docIdentificacion) {
        this.docIdentificacion = docIdentificacion;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Character getIndEstadoMiembro() {
        return indEstadoMiembro;
    }

    public void setIndEstadoMiembro(Character indEstadoMiembro) {
        this.indEstadoMiembro = indEstadoMiembro;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudadResidencia() {
        return ciudadResidencia;
    }

    public void setCiudadResidencia(String ciudadResidencia) {
        this.ciudadResidencia = ciudadResidencia;
    }

    public String getEstadoResidencia() {
        return estadoResidencia;
    }

    public void setEstadoResidencia(String estadoResidencia) {
        this.estadoResidencia = estadoResidencia;
    }

    public String getPaisResidencia() {
        return paisResidencia;
    }

    public void setPaisResidencia(String paisResidencia) {
        this.paisResidencia = paisResidencia;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Character getIndGenero() {
        return indGenero;
    }

    public void setIndGenero(Character indGenero) {
        this.indGenero = indGenero;
    }

    public Boolean getIndContactoEmail() {
        return indContactoEmail;
    }

    public void setIndContactoEmail(Boolean indContactoEmail) {
        this.indContactoEmail = indContactoEmail;
    }

    public Boolean getIndContactoSms() {
        return indContactoSms;
    }

    public void setIndContactoSms(Boolean indContactoSms) {
        this.indContactoSms = indContactoSms;
    }

    public Boolean getIndContactoNotificacion() {
        return indContactoNotificacion;
    }

    public void setIndContactoNotificacion(Boolean indContactoNotificacion) {
        this.indContactoNotificacion = indContactoNotificacion;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Boolean getIndContactoEstado() {
        return indContactoEstado;
    }

    public void setIndContactoEstado(Boolean indContactoEstado) {
        this.indContactoEstado = indContactoEstado;
    }

    public Character getIndEstadoCivil() {
        return indEstadoCivil;
    }

    public void setIndEstadoCivil(Character indEstadoCivil) {
        this.indEstadoCivil = indEstadoCivil;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getFrecuenciaCompra() {
        return frecuenciaCompra;
    }

    public void setFrecuenciaCompra(String frecuenciaCompra) {
        this.frecuenciaCompra = frecuenciaCompra;
    }

    public Character getIndEducacion() {
        return indEducacion;
    }

    public void setIndEducacion(Character indEducacion) {
        this.indEducacion = indEducacion;
    }

    public Double getIngresoEconomico() {
        return ingresoEconomico;
    }

    public void setIngresoEconomico(Double ingresoEconomico) {
        this.ingresoEconomico = ingresoEconomico;
    }

    public Boolean getIndHijos() {
        return indHijos;
    }

    public void setIndHijos(Boolean indHijos) {
        this.indHijos = indHijos;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

//    @ApiModelProperty(hidden = true)
//    @XmlTransient
    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }
    
    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public List<AtributoDinamico> getAtributoDinamicos() {
        return atributoDinamicos;
    }

    public void setAtributoDinamicos(List<AtributoDinamico> atributoDinamicos) {
        this.atributoDinamicos = atributoDinamicos;
    }

    public MiembroBalanceMetrica getBalance() {
        return balance;
    }

    public void setBalance(MiembroBalanceMetrica balance) {
        this.balance = balance;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Boolean getIndPolitica() {
        return indPolitica;
    }

    public void setIndPolitica(Boolean indPolitica) {
        this.indPolitica = indPolitica;
    }

    public Date getFechaPolitica() {
        return fechaPolitica;
    }

    public void setFechaPolitica(Date fechaPolitica) {
        this.fechaPolitica = fechaPolitica;
    }

    public Boolean getIndCambioPass() {
        return indCambioPass;
    }

    public void setIndCambioPass(Boolean indCambioPass) {
        this.indCambioPass = indCambioPass;
    }

    public Boolean getIndMiembroSistema() {
        return indMiembroSistema;
    }

    public void setIndMiembroSistema(Boolean indMiembroSistema) {
        this.indMiembroSistema = indMiembroSistema;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Miembro)) {
            return false;
        }
        Miembro other = (Miembro) object;
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Miembro[ idMiembro=" + idMiembro + " ]";
    }

}
