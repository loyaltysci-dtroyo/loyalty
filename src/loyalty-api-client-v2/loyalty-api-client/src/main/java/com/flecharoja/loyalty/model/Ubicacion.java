/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "UBICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ubicacion.findAll", query = "SELECT u FROM Ubicacion u"),
    @NamedQuery(name = "Ubicacion.findByIdUbicacion", query = "SELECT u FROM Ubicacion u WHERE u.idUbicacion = :idUbicacion"),
    @NamedQuery(name = "Ubicacion.countAll", query = "SELECT COUNT(u) FROM Ubicacion")
})
public class Ubicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_UBICACION")
    @ApiModelProperty(value = "Identificador único de la ubicación", required = true)
    private String idUbicacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    @ApiModelProperty(value = "Nombre de la ubicación", required = true)
    private String nombre;
  
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "IND_DIR_PAIS")
    @ApiModelProperty(value = "País en el que se encuentra la ubicación", required = true)
    private String indDirPais;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "IND_DIR_ESTADO")
    @ApiModelProperty(value = "Estado en el que se encuentra la ubicación", required = true)
    private String indDirEstado;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "IND_DIR_CIUDAD")
    @ApiModelProperty(value = "Ciudad en el que se encuentra la ubicación", required = true)
    private String indDirCiudad;
    
    @Size(max = 250)
    @Column(name = "DIRECCION")
    @ApiModelProperty(value = "Dirección exacta de la ubicación")
    private String direccion;
    
    @Size(max = 150)
    @Column(name = "HORARIO_ATENCION")
    @ApiModelProperty(value = "Horario de atención de la ubicación")
    private String horarioAtencion;
    
    @Size(max = 25)
    @Column(name = "TELEFONO")
    @ApiModelProperty(value = "Teléfono de la ubicación")
    private String telefono;

    @Basic(optional = false)
    @NotNull
    @Column(name = "DIR_LAT")
    @ApiModelProperty(value = "Latitud de la ubicación", required = true)
    private Double dirLat;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "DIR_LNG")
    @ApiModelProperty(value = "Longitud de la ubicación", required = true)
    private Double dirLng;
      
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUbicacion", fetch = FetchType.EAGER)
//    private List<Zona> zonaList;

    public Ubicacion() {
    }

    public Ubicacion(String idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

   
    public String getIdUbicacion() {
        return idUbicacion;
    }

    public void setIdUbicacion(String idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIndDirPais() {
        return indDirPais;
    }

    public void setIndDirPais(String indDirPais) {
        this.indDirPais = indDirPais;
    }

    public String getIndDirEstado() {
        return indDirEstado;
    }

    public void setIndDirEstado(String indDirEstado) {
        this.indDirEstado = indDirEstado;
    }

    public String getIndDirCiudad() {
        return indDirCiudad;
    }

    public void setIndDirCiudad(String indDirCiudad) {
        this.indDirCiudad = indDirCiudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    public String getHorarioAtencion() {
        return horarioAtencion;
    }

    public void setHorarioAtencion(String horarioAtencion) {
        this.horarioAtencion = horarioAtencion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Double getDirLat() {
        return dirLat;
    }

    public void setDirLat(Double dirLat) {
        this.dirLat = dirLat;
    }

    public Double getDirLng() {
        return dirLng;
    }

    public void setDirLng(Double dirLng) {
        this.dirLng = dirLng;
    }

//    public List<Zona> getZonaList() {
//        return zonaList;
//    }
//
//    public void setZonaList(List<Zona> zonaList) {
//        this.zonaList = zonaList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUbicacion != null ? idUbicacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ubicacion)) {
            return false;
        }
        Ubicacion other = (Ubicacion) object;
        if ((this.idUbicacion == null && other.idUbicacion != null) || (this.idUbicacion != null && !this.idUbicacion.equals(other.idUbicacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.Ubicacion[ idUbicacion=" + idUbicacion + " ]";
    }
    
}
