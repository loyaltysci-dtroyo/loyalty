package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_ENCUESTA_PREGUNTA")
@XmlRootElement
public class MisionEncuestaPregunta implements Serializable {
    
    public enum TiposPregunta {
        PREGUNTA('E'),
        CALIFICACION('C');
        
        private final char value;
        private static final Map<Character, TiposPregunta> lookup = new HashMap<>();

        private TiposPregunta(char value) {
            this.value = value;
        }
        
        static {
            for (TiposPregunta pregunta : values()) {
                lookup.put(pregunta.value, pregunta);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static TiposPregunta get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum TiposRespuesta {
        SELECCION_MULTIPLE("RM"),
        SELECCION_UNICA("RU"),
        RESPUESTA_ABIERTA_TEXTO("RT"),
        RESPUESTA_ABIERTA_NUMERICO("RN");
        
        private final String value;
        private static final Map<String, TiposRespuesta> lookup = new HashMap<>();

        private TiposRespuesta(String value) {
            this.value = value;
        }
        
        static {
            for (TiposRespuesta pregunta : values()) {
                lookup.put(pregunta.value, pregunta);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static TiposRespuesta get (String value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum TiposRespuestaCorrecta {
        TODAS('T'),
        ES('U'),
        CUALQUIERA_DE('C'),
        SON('G'),
        MENOR_IGUAL('D'),
        IGUAL('E'),
        MAYOR_IGUAL('F');
        
        private final char value;
        private static final Map<Character, TiposRespuestaCorrecta> lookup = new HashMap<>();

        private TiposRespuestaCorrecta(char value) {
            this.value = value;
        }
        
        static {
            for (TiposRespuestaCorrecta pregunta : values()) {
                lookup.put(pregunta.value, pregunta);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static TiposRespuestaCorrecta get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_PREGUNTA")
    @ApiModelProperty(value = "Identificador de la pregunta", required = true)
    private String idPregunta;
    
    @Column(name = "IND_TIPO_PREGUNTA")
    @ApiModelProperty(value = "Indicador del tipo de pregunta de la encuesta", required = true, example = "Pregunta o calificación")
    private Character indTipoPregunta;
    
    @Column(name = "PREGUNTA")
    @ApiModelProperty(value = "Pregunta de la encuesta", required = true)
    private String pregunta;
    
    @Column(name = "RESPUESTAS")
    @ApiModelProperty(value = "Respuestas que puede tener la pregunta")
    private String respuestas;
    
    @Column(name = "IND_TIPO_RESPUESTA")
    @ApiModelProperty(value = "Indicador del tipo de respuesta de la pregunta", required = true, example = "Selección múltiple, selección única...")
    private String indTipoRespuesta;
    
    @Column(name = "IMAGEN")
    @ApiModelProperty(value = "Imagen representativa de la pregunta")
    private String imagen;
    
    @Column(name = "IND_COMENTARIO")
    @ApiModelProperty(value = "Indicador si la respuesta puede tener comentarios", required = true)
    private Boolean indComentario;
    
    @Column(name = "IND_RESPUESTA_CORRECTA")
    @ApiModelProperty(value = "Indicador de la respuesta correcta", example = "Todas, cualquier, igual, mayor que...")
    private Character indRespuestaCorrecta;
    
    @Column(name = "RESPUESTAS_CORRECTAS")
    @ApiModelProperty(value = "Lista de respuestas correctas")
    private String respuestasCorrectas;
    
    @Column(name = "ID_MISION")
    @ApiModelProperty(value = "Identificador de la misión", required = true)
    private String idMision;

    public MisionEncuestaPregunta() {
    }

    public String getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(String idPregunta) {
        this.idPregunta = idPregunta;
    }

    public Character getIndTipoPregunta() {
        return indTipoPregunta;
    }

    public void setIndTipoPregunta(Character indTipoPregunta) {
        this.indTipoPregunta = indTipoPregunta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(String respuestas) {
        this.respuestas = respuestas;
    }

    public String getIndTipoRespuesta() {
        return indTipoRespuesta;
    }

    public void setIndTipoRespuesta(String indTipoRespuesta) {
        this.indTipoRespuesta = indTipoRespuesta;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Boolean getIndComentario() {
        return indComentario;
    }

    public void setIndComentario(Boolean indComentario) {
        this.indComentario = indComentario;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Character getIndRespuestaCorrecta() {
        return indRespuestaCorrecta;
    }

    public void setIndRespuestaCorrecta(Character indRespuestaCorrecta) {
        this.indRespuestaCorrecta = indRespuestaCorrecta;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getRespuestasCorrectas() {
        return respuestasCorrectas;
    }

    public void setRespuestasCorrectas(String respuestasCorrectas) {
        this.respuestasCorrectas = respuestasCorrectas;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPregunta != null ? idPregunta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionEncuestaPregunta)) {
            return false;
        }
        MisionEncuestaPregunta other = (MisionEncuestaPregunta) object;
        if ((this.idPregunta == null && other.idPregunta != null) || (this.idPregunta != null && !this.idPregunta.equals(other.idPregunta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionEncuestaPregunta[ idPregunta=" + idPregunta + " ]";
    }
    
}
