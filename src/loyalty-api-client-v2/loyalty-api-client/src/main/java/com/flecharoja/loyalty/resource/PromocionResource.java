package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.CategoriaPromocion;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.service.PromocionBean;
import com.flecharoja.loyalty.util.Indicadores;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio, svargas
 */
@Api(value = "Promociones")
@Path("promocion")
public class PromocionResource {

    @EJB
    PromocionBean promocionBean;

    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;

    /**
     * Método que obtiene un listado de categorias de promociones
     * @return lista de categorias de promociones
     */
    @ApiOperation(value = "Obtener las categorias de promociones",
            responseContainer = "List",
            response = CategoriaPromocion.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("categoria")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CategoriaPromocion> getCategoriasPromocion() {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return promocionBean.getCategoriasPromocion(request.getLocale());
    }

    /**
     * Método que obtiene un listado de promociones pertenecientes a una
     * categoria
     *
     * @param idCategoria identificador de la categoria
     * @return lista de promociones
     */
    @ApiOperation(value = "Obtener las promociones que pertenecen a una categoria especifica",
            responseContainer = "List",
            response = Promocion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("categoria/{idCategoria}/promociones")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Promocion> getPromocionesPorCategoria(@ApiParam(value = "Identificador de la categoria", required = true) @PathParam("idCategoria") String idCategoria) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return promocionBean.getPromocionesPorCategoria(idCategoria, idMiembro);
    }

    /**
     * Método que registra la accion de vista de una promocion
     *
     * @param idPromocion identificador de la promocion
     */
    @ApiOperation(value = "Registrar que una promocion fue vista")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("{idPromocion}/registrar-vista")
    @Produces(MediaType.APPLICATION_JSON)
    public void registerVistaPromocion(@ApiParam(value = "Identificador de la promocion", required = true) @PathParam("idPromocion") String idPromocion) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        promocionBean.registerVistaPromocion(idMiembro, idPromocion, request.getLocale());
    }

    /**
     * Método que obtiene un listado de promociones
     *
     * @return lista de promociones
     */
    @ApiOperation(value = "Obtener las promociones",
            responseContainer = "List",
            response = Promocion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Promocion> getPromociones() {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return promocionBean.getPromociones(idMiembro);
    }

    /**
     * Método que marca una promocion por el usuario
     *
     * @param idPromocion Identificador de la promocion
     */
    @ApiOperation(value = "Marca una promocion como favorita (pendiente)")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Path("{idPromocion}/marcar-favorito")
    @Produces(MediaType.APPLICATION_JSON)
    public void bookmarkPromocion(@ApiParam(value = "Identificador de la promocion", required = true) @PathParam("idPromocion") String idPromocion) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        promocionBean.bookmarkPromocion(idMiembro, idPromocion, request.getLocale());
    }

    /**
     * Método que obtiene un listado de promociones marcadas por el usuario
     * (favoritas)
     *
     * @return lista de promociones
     */
    @ApiOperation(value = "Obtener las promociones en las que el miembro en sesion tiene marcadas como favoritas (pendiente)",
            responseContainer = "List",
            response = Promocion.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("favoritas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Promocion> getPromocionesFavoritas() {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return promocionBean.getPromocionesFavoritas(idMiembro);
    }
}
