package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.CategoriaMision;
import com.flecharoja.loyalty.model.MisionDetails;
import com.flecharoja.loyalty.model.ReciboRespuestaMision;
import com.flecharoja.loyalty.model.RespuestaMision;
import com.flecharoja.loyalty.model.SecuenciaMisiones;
import com.flecharoja.loyalty.service.MisionBean;
import com.flecharoja.loyalty.util.Indicadores;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio, svargas
 */
@Api(value = "Misiones")
@Path("mision")
public class MisionResource {

    @EJB
    MisionBean misionBean;

    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;

    /**
     * Método que obtiene un listado de categorias de misiones
     *
     * @return lista de categorias de misiones
     */
    @ApiOperation(value = "Obtener las categorias de misiones",
            responseContainer = "List",
            response = CategoriaMision.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("categoria")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CategoriaMision> getCategoriasMision() {
        try {
             context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return misionBean.getCategoriasMision(request.getLocale());
    }

    /**
     * Método que obtiene un listado de misiones pertenecientes a una categoria
     *
     * @param idCategoria identificador de la categoria
     * @return lista de misiones
     */
    @ApiOperation(value = "Obtener las misiones que pertenecen a una categoria especifica",
            responseContainer = "List",
            response = MisionDetails.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("categoria/{idCategoria}/misiones")
    @Produces(MediaType.APPLICATION_JSON)
    public List<MisionDetails> getMisionesPorCategoria(@ApiParam(value = "Identificador de la categoria", required = true) @PathParam("idCategoria") String idCategoria) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return misionBean.getMisionesPorCategoria(idCategoria, idMiembro, request.getLocale());
    }

    /**
     * Método que muestra la informacion detallada de la misión
     *
     * @param idMision identificador de la mision
     * @param idWorkflow identificador del workflow
     */
    @ApiOperation(value = "Registrar que una mision fue vista")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("{idMision}/registrar-vista")
    @Produces(MediaType.APPLICATION_JSON)
    public void registerVistaMision(
            @ApiParam(value = "Identificador de la misión", required = true) @PathParam("idMision") String idMision,
            @ApiParam(value = "Identificador opcional del workflow") @QueryParam("idWorkflow") String idWorkflow
    ) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        misionBean.registerVistaMision(idMision, idMiembro,request.getLocale(), idWorkflow);
    }

    /**
     * Método que registra que una mision ha sido completada por un miembro
     *
     * @param idMision identificador de la mision
     * @param respuesta valor a tomar la respuesta de la mision
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Registro de la completitud de una misión por un miembro", response = ReciboRespuestaMision.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("{idMision}/completar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ReciboRespuestaMision completarMision(
            @ApiParam(value = "Identificador de la misión", required = true)
            @PathParam("idMision") String idMision,
            @ApiParam(value = "Respuesta de la mision", required = true) RespuestaMision respuesta) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }

       return misionBean.completarMision(idMiembro, idMision, respuesta, request.getLocale());
    }

    /**
     * Método que obtiene un listado de misiones
     *
     * @return lista de misiones
     */
    @ApiOperation(value = "Obtener la lista de misiones",
            responseContainer = "List",
            response = MisionDetails.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<MisionDetails> getMisiones() {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return misionBean.getMisiones(idMiembro, request.getLocale());
    }
    
     /**
     * Método que obtiene un listado de misiones
     *
     * @param indTipo
     * @return lista de misiones
     */
    @ApiOperation(value = "Obtener la lista de misiones",
            responseContainer = "List",
            response = MisionDetails.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("secuencias")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SecuenciaMisiones> getSecuenciasMisiones(
            @ApiParam(value = "Indicador de tipo (C=completado, D=disponible)",required = true)
            @QueryParam(value = "indTipo") String indTipo) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return misionBean.getSecuenciasMisiones(idMiembro,indTipo, request.getLocale());
    }
    
    /**
     * Método que obtiene un listado de misiones
     *
     * @param estado
     * @return lista de misiones
     */
    @ApiOperation(value = "Obtener la lista de misiones",
            responseContainer = "List",
            response = MisionDetails.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("{estado}/estado")
    @Produces(MediaType.APPLICATION_JSON)
    public List<MisionDetails> getMisionesPorEstado(@PathParam("estado") String estado) {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return misionBean.getMisionesPorEstado(idMiembro, request.getLocale(), estado);
    }
}
