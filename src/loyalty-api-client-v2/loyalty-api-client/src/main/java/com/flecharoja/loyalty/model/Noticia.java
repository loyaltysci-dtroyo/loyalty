package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@ApiModel(value = "Noticia")
@Entity
@Table(name = "NOTICIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Noticia.findByIndEstado", query = "SELECT n FROM Noticia n WHERE n.indEstado = :indEstado")
})
public class Noticia implements Serializable {
    
    public enum Estados {
        PUBLICADO('A'),
        ARCHIVADO('B');
        
        private final char value;
        
        private Estados(char value) {
            this.value = value;
        }
        
        public char getValue() {
            return value;
        }
    }

    private static final long serialVersionUID = 1L;
    
    @ApiModelProperty(value = "Identificador de la Noticia", readOnly = true)
    @Id
    @GeneratedValue(generator = "noticia_uuid")
    @GenericGenerator(name = "noticia_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_NOTICIA")
    private String idNoticia;
    
    @ApiModelProperty(value = "Titulo de la noticia", required = true)
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "TITULO")
    private String titulo;
    
    @ApiModelProperty(value = "Contenido de la noticia", required = true)
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "CONTENIDO")
    private String contenido;
    
    @ApiModelProperty(value = "Imagen de la noticia")
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    private String imagen;
    
    @ApiModelProperty(hidden = true)
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @ApiModelProperty(hidden = true)
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @ApiModelProperty(hidden = true)
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @ApiModelProperty(hidden = true)
    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;
    
    @ApiModelProperty(value = "Indicador si la noticia fue creada por un administrador", readOnly = true)
    @Column(name = "IND_AUTOR_ADMIN")
    private Boolean indAutorAdmin;
    
    @ApiModelProperty(value = "Identificador del autor de noticia", readOnly = true)
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "ID_AUTOR")
    private String idAutor;
    
    @ApiModelProperty(hidden = true)
    @Basic(optional = false)
    @NotNull
    @Version
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @ApiModelProperty(hidden = true)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "noticia")
    private List<NoticiaComentario> comentarios;
    
    @ApiModelProperty(hidden = true)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "noticia")
    private List<MarcadorNoticia> marcas;
    
    @ApiModelProperty(value = "Cantidad de comentarios", readOnly = true)
    @Transient
    private long cantComentarios;
    
    @ApiModelProperty(value = "Cantidad de marcas", readOnly = true)
    @Transient
    private long cantMarcas;
    
    @ApiModelProperty(value = "Indicador si la noticia fue marcada por el miembro", readOnly = true)
    @Transient
    private boolean marcado;
    
    @ApiModelProperty(value = "Nombre publico del autor/Nombre de usuario", readOnly = true)
    @Transient
    private String nombreAutor;
    
    @ApiModelProperty(value = "URL del avatar del autor", readOnly = true)
    @Transient
    private String avatarAutor;

    public Noticia() {
    }
    
    public String getIdNoticia() {
        return idNoticia;
    }

    public void setIdNoticia(String idNoticia) {
        this.idNoticia = idNoticia;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @XmlTransient
    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    @XmlTransient
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @XmlTransient
    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @XmlTransient
    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Boolean getIndAutorAdmin() {
        return indAutorAdmin==null?false:indAutorAdmin;
    }

    public void setIndAutorAdmin(Boolean indAutorAdmin) {
        this.indAutorAdmin = indAutorAdmin;
    }

    public String getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(String idAutor) {
        this.idAutor = idAutor;
    }

    @XmlTransient
    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @XmlTransient
    public List<NoticiaComentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<NoticiaComentario> comentarios) {
        this.comentarios = comentarios;
    }

    @XmlTransient
    public List<MarcadorNoticia> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<MarcadorNoticia> marcas) {
        this.marcas = marcas;
    }

    public long getCantComentarios() {
        return cantComentarios;
    }

    public void setCantComentarios(long cantComentarios) {
        this.cantComentarios = cantComentarios;
    }

    public long getCantMarcas() {
        return cantMarcas;
    }

    public void setCantMarcas(long cantMarcas) {
        this.cantMarcas = cantMarcas;
    }

    public boolean isMarcado() {
        return marcado;
    }

    public void setMarcado(boolean marcado) {
        this.marcado = marcado;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    public String getAvatarAutor() {
        return avatarAutor;
    }

    public void setAvatarAutor(String avatarAutor) {
        this.avatarAutor = avatarAutor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNoticia != null ? idNoticia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Noticia)) {
            return false;
        }
        Noticia other = (Noticia) object;
        if ((this.idNoticia == null && other.idNoticia != null) || (this.idNoticia != null && !this.idNoticia.equals(other.idNoticia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Noticia[ idNoticia=" + idNoticia + " ]";
    }
    
}
