/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PRODUCTO_CATEG_SUBCATEG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoCategSubcateg.findAll", query = "SELECT p FROM ProductoCategSubcateg p"),   
    @NamedQuery(name = "ProductoCategSubcateg.findByIdSubcategoria", query = "SELECT p.productoControl FROM ProductoCategSubcateg p WHERE p.productoCategSubcategPK.idSubcategoria = :idSubcategoria")
})
public class ProductoCategSubcateg implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoCategSubcategPK productoCategSubcategPK;
     
    @JoinColumn(name = "ID_SUBCATEGORIA", referencedColumnName = "ID_SUBCATEGORIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    @ApiModelProperty(value = "Subcategoria de una categoría que pertenece un producto",required = true)
    private SubcategoriaProducto subcategoriaProducto;
    
    @JoinColumn(name = "ID_PRODUCTO", referencedColumnName = "ID_PRODUCTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ProductoControl productoControl;
    
    

    public ProductoCategSubcateg() {
    }

    
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public ProductoCategSubcategPK getProductoCategSubcategPK() {
        return productoCategSubcategPK;
    }

    public void setProductoCategSubcategPK(ProductoCategSubcategPK productoCategSubcategPK) {
        this.productoCategSubcategPK = productoCategSubcategPK;
    }

    
    public SubcategoriaProducto getSubcategoriaProducto() {
        return subcategoriaProducto;
    }

    public void setSubcategoriaProducto(SubcategoriaProducto subcategoriaProducto) {
        this.subcategoriaProducto = subcategoriaProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoCategSubcategPK != null ? productoCategSubcategPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoCategSubcateg)) {
            return false;
        }
        ProductoCategSubcateg other = (ProductoCategSubcateg) object;
        if ((this.productoCategSubcategPK == null && other.productoCategSubcategPK != null) || (this.productoCategSubcategPK != null && !this.productoCategSubcategPK.equals(other.productoCategSubcategPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.ProductoCategSubcateg[ productoCategSubcategPK=" + productoCategSubcategPK + " ]";
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public ProductoControl getProducto() {
        return productoControl;
    }

    public void setProducto(ProductoControl productoControl) {
        this.productoControl = productoControl;
    }
    
    
    
}
