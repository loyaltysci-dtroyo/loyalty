package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio, svargas
 */
@Entity
@Table(name = "METRICA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Metrica.findAll", query = "SELECT m FROM Metrica m"),
    @NamedQuery(name = "Metrica.findByIdMetrica", query = "SELECT m FROM Metrica m WHERE m.idMetrica = :idMetrica")
})
public class Metrica implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_METRICA")
    @ApiModelProperty(value = "Identificador único y autogenerado de una métrica",required = true)
    private String idMetrica;
    
    @Column(name = "NOMBRE")
    @ApiModelProperty(value = "Nombre de una métrica",required = true)
    private String nombre;
    
    @Column(name = "MEDIDA")
    @ApiModelProperty(value = "Medida de una métrica", example = "Puntos, visitas, millas ...",required = true)
    private String medida;
    
    @Column(name = "IND_ESTADO")
    @ApiModelProperty(value = "Indicador de estado de una métrica", example = "Borrador, Publicado, Archivado",required = true)
    private Character indEstado;

    @JoinColumn(name = "GRUPO_NIVELES", referencedColumnName = "ID_GRUPO_NIVEL")
    @ManyToOne
    private GrupoNiveles grupoNiveles;

    public Metrica() {
    }

    public Metrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    public String getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMedida() {
        return medida;
    }

    public void setMedida(String medida) {
        this.medida = medida;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public GrupoNiveles getGrupoNiveles() {
        return grupoNiveles;
    }

    public void setGrupoNiveles(GrupoNiveles grupoNiveles) {
        this.grupoNiveles = grupoNiveles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMetrica != null ? idMetrica.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Metrica)) {
            return false;
        }
        Metrica other = (Metrica) object;
        if ((this.idMetrica == null && other.idMetrica != null) || (this.idMetrica != null && !this.idMetrica.equals(other.idMetrica))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Metrica[ idMetrica=" + idMetrica + " ]";
    }

}
