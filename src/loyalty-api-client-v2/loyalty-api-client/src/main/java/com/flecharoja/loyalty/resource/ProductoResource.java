package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.CategoriaProducto;
import com.flecharoja.loyalty.model.Producto;
import com.flecharoja.loyalty.service.ProductoBean;
import com.flecharoja.loyalty.util.Indicadores;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "Productos")
@Path("producto")
public class ProductoResource {

    @EJB
    ProductoBean productoBean;

    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;

    /**
     * Método que obtiene un listado de categorias de productos
     *
     * @return lista de categorias de productos
     */
    @ApiOperation(value = "Obtener las categorias de productos",
            responseContainer = "List",
            response = CategoriaProducto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("categoria")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CategoriaProducto> getCategoriasProducto() {
        try{
            context.getUserPrincipal().getName();
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return productoBean.getCategoriasProductos();
    }

    /**
     * Método que obtiene un listado de productos pertenecientes a una categoria
     * y subcategoria
     *
     * @param idSubcategoria identificador de la subcategoria
     * @return lista de productos
     */
    @ApiOperation(value = "Obtener los productos que pertenecen a una categoria y subcategoria especifica",
            responseContainer = "List",
            response = Producto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("categoria/{idSubcategoria}/productos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Producto> getProductosPorCategoriaSubcategoria(
            @ApiParam(value = "Identificador de la categoria", required = true)
            @PathParam("idSubcategoria") String idSubcategoria) {

        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return productoBean.getProductosPorSubcategoria(idSubcategoria, idMiembro, request.getLocale());
    }
   
    /**
     * Método que obtiene un listado de productos
     *
     * @return lista de productos
     */
    @ApiOperation(value = "Obtener los productos en las que el miembro en sesion es elegible",
            responseContainer = "List",
            response = Producto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Producto> getProductos() {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
              
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return productoBean.getProductos(idMiembro,request.getLocale());
    }
    
    /**
     * Método que obtiene un listado de productos disponibles
     *
     * @return lista de productos disponibles
     */
    @ApiOperation(value = "Obtener los productos disponibles en los que el miembro en sesion es elegible",
            responseContainer = "List",
            response = Producto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("available")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Producto> getProductosDisponibles() {
        String idMiembro;
        try {
            idMiembro = context.getUserPrincipal().getName();
              
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return productoBean.getProductosDisponibles(idMiembro, request.getLocale());
    }
}
