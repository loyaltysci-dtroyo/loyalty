package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MARCADOR_PROMOCION")
@NamedQueries(
    @NamedQuery(name = "MarcadorPromocion.findPromocionControlByIdMiembroByIndEstado", query = "SELECT m.promocionControl FROM MarcadorPromocion m WHERE m.marcadorPromocionPK.idMiembro = :idMiembro AND m.promocionControl.indEstado = :indEstado")
)
public class MarcadorPromocion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MarcadorPromocionPK marcadorPromocionPK;
    
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @JoinColumn(name = "ID_PROMOCION", referencedColumnName = "ID_PROMOCION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private PromocionControl promocionControl;

    public MarcadorPromocion() {
    }

    public MarcadorPromocion(String idPromocion, String idMiembro) {
        this.marcadorPromocionPK = new MarcadorPromocionPK(idPromocion, idMiembro);
        this.fecha = new Date();
    }

    public MarcadorPromocionPK getMarcadorPromocionPK() {
        return marcadorPromocionPK;
    }

    public void setMarcadorPromocionPK(MarcadorPromocionPK marcadorPromocionPK) {
        this.marcadorPromocionPK = marcadorPromocionPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (marcadorPromocionPK != null ? marcadorPromocionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarcadorPromocion)) {
            return false;
        }
        MarcadorPromocion other = (MarcadorPromocion) object;
        if ((this.marcadorPromocionPK == null && other.marcadorPromocionPK != null) || (this.marcadorPromocionPK != null && !this.marcadorPromocionPK.equals(other.marcadorPromocionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MarcadorPromocion[ marcadorPromocionPK=" + marcadorPromocionPK + " ]";
    }
    
}
