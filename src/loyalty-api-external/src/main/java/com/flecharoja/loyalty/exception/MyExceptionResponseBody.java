package com.flecharoja.loyalty.exception;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@ApiModel(value = "ExceptionResponse")
public class MyExceptionResponseBody {
    
    @ApiModelProperty(value = "Internal code value of the error ocurred")
    private int code;
    @ApiModelProperty(value = "Msj about the error")
    private String message;
    @ApiModelProperty(value = "Locale of the msj languaje")
    private String lang;

    public MyExceptionResponseBody() {
    }

    public MyExceptionResponseBody(int code, String message, String lang) {
        this.code = code;
        this.message = message;
        this.lang = lang;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    @Override
    public String toString() {
        return "{\"code\":" + code + ",\"message\":\"" + message + "\",\"lang\":\"" + lang + "\"}";
    }
}
