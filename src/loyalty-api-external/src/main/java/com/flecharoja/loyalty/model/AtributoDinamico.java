package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "ATRIBUTO_DINAMICO")
@NamedQueries({
        @NamedQuery(name = "AtributoDinamico.findByNombreInterno", query = "SELECT a FROM AtributoDinamico a WHERE a.nombreInterno = :nombreInterno"),
        @NamedQuery(name = "AtributoDinamico.getIndTipoDatoByIdAtributo", query = "SELECT a.indTipoDato FROM AtributoDinamico a WHERE a.idAtributo = :idAtributo")
})
public class AtributoDinamico implements Serializable {
    
    public enum Estados{
        BORRADOR('D'),
        PUBLICADO('P'),
        ARCHIVADO('A');
        
        private final char value;
        private static final Map<Character,Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static {
            for(Estados estado: values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum TiposDato {
        FECHA('F'),
        TEXTO('T'),
        NUMERICO('N'),
        BOOLEANO('B');
        
        private final char value;
        private static final Map<Character,TiposDato> lookup = new HashMap<>();

        private TiposDato(char value) {
            this.value = value;
        }
        
        static{
            for(TiposDato estado: values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        public static final TiposDato get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "atributo_uuid")
    @GenericGenerator(name = "atributo_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_ATRIBUTO")
    private String idAtributo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;
  
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_DATO")
    private Character indTipoDato;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "VALOR_DEFECTO")
    private String valorDefecto;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_VISIBLE")
    private Boolean indVisible;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_REQUERIDO")
    private Boolean indRequerido;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    public AtributoDinamico() {
    }

    public String getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }

    public Character getIndTipoDato() {
        return indTipoDato;
    }

    public void setIndTipoDato(Character indTipoDato) {
        this.indTipoDato = indTipoDato;
    }

    public Boolean getIndVisible() {
        return indVisible;
    }

    public void setIndVisible(Boolean indVisible) {
        this.indVisible = indVisible;
    }

    public String getValorDefecto() {
        return valorDefecto;
    }

    public void setValorDefecto(String valorDefecto) {
        this.valorDefecto = valorDefecto;
    }

    public Boolean getIndRequerido() {
        return indRequerido;
    }

    public void setIndRequerido(Boolean indRequerido) {
        this.indRequerido = indRequerido;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }
    
     
    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtributo != null ? idAtributo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtributoDinamico)) {
            return false;
        }
        AtributoDinamico other = (AtributoDinamico) object;
        if ((this.idAtributo == null && other.idAtributo != null) || (this.idAtributo != null && !this.idAtributo.equals(other.idAtributo))) {
            return false;
        }
        return true;
    }

}
