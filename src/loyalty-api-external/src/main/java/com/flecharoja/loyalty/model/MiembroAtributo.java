package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MIEMBRO_ATRIBUTO")
@NamedQueries({
    @NamedQuery(name = "MiembroAtributo.findByIdMiembro", query = "SELECT m FROM MiembroAtributo m WHERE m.miembroAtributoPK.idMiembro = :idMiembro")
})
public class MiembroAtributo implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MiembroAtributoPK miembroAtributoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Size(max = 100)
    @Column(name = "VALOR")
    private String valor;

    public MiembroAtributo() {
    }

    public MiembroAtributo(String idMiembro, String idAtributo, Date fechaCreacion, String valor) {
        this.miembroAtributoPK = new MiembroAtributoPK(idMiembro, idAtributo);
        this.fechaCreacion = fechaCreacion;
        this.valor = valor;
    }

    public MiembroAtributo(Date fechaCreacion, String valor) {
        this.fechaCreacion = fechaCreacion;
        this.valor = valor;
    }

    public MiembroAtributoPK getMiembroAtributoPK() {
        return miembroAtributoPK;
    }

    public void setMiembroAtributoPK(MiembroAtributoPK miembroAtributoPK) {
        this.miembroAtributoPK = miembroAtributoPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (miembroAtributoPK != null ? miembroAtributoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiembroAtributo)) {
            return false;
        }
        MiembroAtributo other = (MiembroAtributo) object;
        if ((this.miembroAtributoPK == null && other.miembroAtributoPK != null) || (this.miembroAtributoPK != null && !this.miembroAtributoPK.equals(other.miembroAtributoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MiembroAtributo[ miembroAtributoPK=" + miembroAtributoPK + " ]";
    }
    
}
