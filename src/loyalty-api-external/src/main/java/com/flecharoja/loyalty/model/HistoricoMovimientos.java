package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "HISTORICO_MOVIMIENTOS")
public class HistoricoMovimientos implements Serializable {

    public enum Tipos{
        REDENCION_AUTO('R'),
        COMPRA('C'),
        AJUSTE_MAS('A'),
        AJUSTE_MENOS('M'),
        INTER_UBICACION('I');
        
        public final char value;
        public static final Map<Character,Tipos> lookup= new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }
        
        static{
            for(Tipos tipo : values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipos get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Status{
        REDIMIDO_SIN_RETIRAR('S'),
        REDIMIDO_RETIRADO('R'),
        ANULADO('A');
        
        public final char value;
        public static final Map<Character,Status> lookup= new HashMap<>();

        private Status(char value) {
            this.value = value;
        }
        
        static{
            for(Status tipo : values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Status get(Character value){
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "historico_uuid")
    @GenericGenerator(name = "historico_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_HISTORICO")
    private String idHistorico;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANTIDAD")
    private Long cantidad;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO_MOVIMIENTO")
    private Character tipoMovimiento;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @Column(name = "STATUS")
    private Character status;
    
    @Column(name = "ID_PREMIO_MIEMBRO")
    private String idPremioMiembro;
    
    @Column(name = "PRECIO_PROMEDIO")
    private Double precioPromedio;
    
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;
    
    @Column(name = "COD_PREMIO")
    private String codPremio;
    
    @Column(name = "UBICACION_ORIGEN")
    private String ubicacionOrigen;
    
    @Column(name = "UBICACION_DESTINO")
    private String ubicacionDestino;

    public HistoricoMovimientos() {
    }
    
    public HistoricoMovimientos(Double precioPromedio, String idMiembro, String codPremio, String ubicacionOrigen, String ubicacionDestino, Long cantidad, Character tipoMovimiento, Date fecha, Character status,String idPremioMiembro) {
        this.precioPromedio = precioPromedio;
        this.idMiembro = idMiembro;
        this.codPremio = codPremio;
        this.ubicacionOrigen = ubicacionOrigen;
        this.ubicacionDestino = ubicacionDestino;
        this.cantidad = cantidad;
        this.tipoMovimiento = tipoMovimiento;
        this.fecha = fecha;
        this.status = status;
        this.idPremioMiembro = idPremioMiembro;
    }

    public String getIdHistorico() {
        return idHistorico;
    }

    public void setIdHistorico(String idHistorico) {
        this.idHistorico = idHistorico;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public Character getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(Character tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHistorico != null ? idHistorico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoricoMovimientos)) {
            return false;
        }
        HistoricoMovimientos other = (HistoricoMovimientos) object;
        if ((this.idHistorico == null && other.idHistorico != null) || (this.idHistorico != null && !this.idHistorico.equals(other.idHistorico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.HistoricoMovimientos[ idHistorico=" + idHistorico + " ]";
    }

    public Double getPrecioPromedio() {
        return precioPromedio;
    }

    public void setPrecioPromedio(Double precioPromedio) {
        this.precioPromedio = precioPromedio;
    }

    public String getIdPremioMiembro() {
        return idPremioMiembro;
    }

    public void setIdPremioMiembro(String idPremioMiembro) {
        this.idPremioMiembro = idPremioMiembro;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getCodPremio() {
        return codPremio;
    }

    public void setCodPremio(String codPremio) {
        this.codPremio = codPremio;
    }

    public String getUbicacionOrigen() {
        return ubicacionOrigen;
    }

    public void setUbicacionOrigen(String ubicacionOrigen) {
        this.ubicacionOrigen = ubicacionOrigen;
    }

    public String getUbicacionDestino() {
        return ubicacionDestino;
    }

    public void setUbicacionDestino(String ubicacionDestino) {
        this.ubicacionDestino = ubicacionDestino;
    }
}
