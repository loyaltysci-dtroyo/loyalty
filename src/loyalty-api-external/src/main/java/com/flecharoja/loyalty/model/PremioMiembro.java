package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PREMIO_MIEMBRO")
@NamedQueries({
    @NamedQuery(name = "PremioMiembro.findByUltimaFechaAsignacion", query = "SELECT MAX(p.fechaAsignacion) FROM PremioMiembro p WHERE p.idPremio = :idPremio AND p.idMiembro = :idMiembro ORDER BY p.fechaAsignacion DESC"),
    @NamedQuery(name = "PremioMiembro.countRespuestasPremio", query = "SELECT COUNT(p) FROM PremioMiembro p WHERE p.idPremio = :idPremio"),
    @NamedQuery(name = "PremioMiembro.countRespuestasPremioMiembro", query = "SELECT COUNT(p) FROM PremioMiembro p WHERE p.idPremio = :idPremio AND p.idMiembro = :idMiembro"),
    @NamedQuery(name = "PremioMiembro.countRespuestasTiempoPremio", query = "SELECT COUNT(p) FROM PremioMiembro p WHERE p.idPremio = :idPremio AND p.fechaAsignacion >= :fechaInicio AND p.fechaAsignacion <= :fechaFin"),
    @NamedQuery(name = "PremioMiembro.countRespuestasTiempoPremioMiembro", query = "SELECT COUNT(p) FROM PremioMiembro p WHERE p.idPremio = :idPremio AND p.idMiembro = :idMiembro AND p.fechaAsignacion >= :fechaInicio AND p.fechaAsignacion <= :fechaFin"),
    @NamedQuery(name = "PremioMiembro.findByIdPremioCodigoCertificado", query = "SELECT p FROM PremioMiembro p WHERE p.idPremio = :idPremio AND p.codigoCertificado = :codigoCertificado")
})
public class PremioMiembro implements Serializable {
    
    public enum Motivos {
        REWARD('R'),
        PREMIO('P'),
        PREMIO_MISION('M'),
        AWARD('A');

        private final char value;
        private static final Map<Character, Motivos> lookup = new HashMap<>();

        private Motivos(char value) {
            this.value = value;
        }

        static {
            for (Motivos motivo : values()) {
                lookup.put(motivo.value, motivo);
            }
        }

        public char getValue() {
            return value;
        }

        public static Motivos get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }
    
    public enum Estados{
        REDIMIDO_SIN_RETIRAR('S'),
        REDIMIDO_RETIRADO('R'),
        ANULADO('A');
        
        private final char value;
        private static final Map<Character,Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static{
            for(Estados estado : values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "metrica_uuid")
    @GenericGenerator(name = "metrica_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_PREMIO_MIEMBRO")
    private String idPremioMiembro;
    
    @Size(min = 1, max = 200)
    @Column(name = "MOTIVO_ASIGNACION")
    private String motivoAsignacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTADO")
    private Character estado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_ASIGNACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAsignacion;
    
    @Column(name = "FECHA_RECLAMO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaReclamo;
    
    @Column(name = "FECHA_CANCELACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCancelacion;
    
    @Size(max = 40)
    @Column(name = "USUARIO_ASIGNADOR")
    private String usuarioAsignador;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_MOTIVO_ASIGNACION")
    private Character indMotivoAsignacion;
    
    @Size(max = 50)
    @Column(name = "CODIGO_CERTIFICADO")
    private String codigoCertificado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PREMIO")
    private String idPremio;
    
    @Column(name = "FECHA_EXPIRACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaExpiracion;

    public PremioMiembro() {
    }

    public String getMotivoAsignacion() {
        return motivoAsignacion;
    }

    public void setMotivoAsignacion(String motivoAsignacion) {
        this.motivoAsignacion = motivoAsignacion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado == null ? null : Character.toUpperCase(estado);
    }

    public Date getFechaAsignacion() {
        return fechaAsignacion;
    }

    public void setFechaAsignacion(Date fechaAsignacion) {
        this.fechaAsignacion = fechaAsignacion;
    }

    public Date getFechaReclamo() {
        return fechaReclamo;
    }

    public void setFechaReclamo(Date fechaReclamo) {
        this.fechaReclamo = fechaReclamo;
    }

    public Date getFechaCancelacion() {
        return fechaCancelacion;
    }

    public void setFechaCancelacion(Date fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public String getUsuarioAsignador() {
        return usuarioAsignador;
    }

    public void setUsuarioAsignador(String usuarioAsignador) {
        this.usuarioAsignador = usuarioAsignador;
    }

    public Character getIndMotivoAsignacion() {
        return indMotivoAsignacion;
    }

    public void setIndMotivoAsignacion(Character indMotivoAsignacion) {
        this.indMotivoAsignacion = indMotivoAsignacion == null ? null : Character.toUpperCase(indMotivoAsignacion);
    }

    public String getCodigoCertificado() {
        return codigoCertificado;
    }

    public void setCodigoCertificado(String codigoCertificado) {
        this.codigoCertificado = codigoCertificado;
    }

    public String getIdPremioMiembro() {
        return idPremioMiembro;
    }

    public void setIdPremioMiembro(String idPremioMiembro) {
        this.idPremioMiembro = idPremioMiembro;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(String idPremio) {
        this.idPremio = idPremio;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPremioMiembro != null ? idPremioMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PremioMiembro)) {
            return false;
        }
        PremioMiembro other = (PremioMiembro) object;
        if ((this.idPremioMiembro == null && other.idPremioMiembro != null) || (this.idPremioMiembro != null && !this.idPremioMiembro.equals(other.idPremioMiembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PremioMiembro{" + "idPremioMiembro=" + idPremioMiembro + ", motivoAsignacion=" + motivoAsignacion + ", estado=" + estado + ", fechaAsignacion=" + fechaAsignacion + ", fechaReclamo=" + fechaReclamo + ", fechaCancelacion=" + fechaCancelacion + ", usuarioAsignador=" + usuarioAsignador + ", indMotivoAsignacion=" + indMotivoAsignacion + ", codigoCertificado=" + codigoCertificado + ", idMiembro=" + idMiembro + ", idPremio=" + idPremio + '}';
    }
    
}
