package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.AtributoDinamicoProducto;
import com.flecharoja.loyalty.model.ProductEntry;
import com.flecharoja.loyalty.model.ProductoAtributo;
import com.flecharoja.loyalty.model.ProductoAtributoPK;
import com.flecharoja.loyalty.model.RespuestaRegistroEntidad;
import com.flecharoja.loyalty.util.Indicadores;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author svargas
 */
@Stateless
public class ProductoService {
    
    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @EJB
    private AtributoDinamicoBean atributoDinamicoBean;
    
    private enum CamposFiltro {
        CODIGO_PRODUCTO("cod-product"),
        NOMBRE_INTERNO("internal-name"),
        UUID("internal-id");
        
        private final String value;
        private static final Map<String, CamposFiltro> lookup =  new HashMap<>();

        private CamposFiltro(String value) {
            this.value = value;
        }
        
        static {
            for (CamposFiltro genero : values()) {
                lookup.put(genero.value, genero);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static CamposFiltro get(String value) {
            return value==null?null:lookup.get(value.toLowerCase());
        }
    }
    
    private enum CamposListas {
        TODOS("all"),
        PUBLICADOS("published"),
        ARCHIVADOS("archived");
        
        private final String value;
        private static final Map<String, CamposListas> lookup =  new HashMap<>();

        private CamposListas(String value) {
            this.value = value;
        }
        
        static {
            for (CamposListas genero : values()) {
                lookup.put(genero.value, genero);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static CamposListas get(String value) {
            return value==null?null:lookup.get(value.toLowerCase());
        }
    }

    public RespuestaRegistroEntidad registrarProducto(JsonObject data, String enable, Locale locale) {
        if (data==null) {
            throw new MyException(MyException.ErrorPeticion.CUERPO_PETICION_INVALIDO, locale, data, "data_registration_not_send");
        }
        
        ProductEntry productEntry = new ProductEntry();
        
        if (enable==null) {
            productEntry.setIndEstado(ProductEntry.Estados.INACTIVO.getValue());
        } else {
            switch(enable) {
                case "0": {
                    productEntry.setIndEstado(ProductEntry.Estados.INACTIVO.getValue());
                    break;
                }
                case "1": {
                    productEntry.setIndEstado(ProductEntry.Estados.PUBLICADO.getValue());
                    break;
                }
                default: {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTO_PETICION_INVALIDO, locale, data, "query_parameter_invalid", "enable");
                }
            }
        }
        
        try {
            productEntry.setName(data.getString("name"));
            productEntry.setEncabezadoArte(productEntry.getName());
        } catch (ClassCastException | NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "name");
        }
        
        try {
            productEntry.setDescription(data.getString("description"));
            productEntry.setDetalleArte(productEntry.getDescription());
        } catch (ClassCastException | NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "description");
        }
        
        try {
            if (((long)em.createNamedQuery("Producto.countByInternalName").setParameter("internalName", data.getString("internalName")).getSingleResult())>1) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "entity_same_internal_name");
            }
            productEntry.setInternalName(data.getString("internalName"));
        } catch (ClassCastException | NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "internalName");
        }
        
        try {
            productEntry.setPrice(data.getJsonNumber("price").doubleValue());
        } catch (ClassCastException | NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "price");
        }
        
        try {
            productEntry.setDeliveryCost(data.getJsonNumber("deliveryCost").doubleValue());
        } catch (ClassCastException | NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "deliveryCost");
        }
        
        try {
            productEntry.setIndTaxes(data.getBoolean("indTaxes"));
        } catch (ClassCastException | NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "email");
        }
        
        try {
            productEntry.setIndDelivery(ProductEntry.Entrega.get(data.getString("indDelivery").charAt(0)).getValue());
        } catch (ClassCastException | NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "indDelivery");
        }
        
        Map<String, ProductoAtributo> map = new HashMap<>();
        
        data.entrySet().stream().distinct().forEach((atributo) -> {
            switch(atributo.getKey()) {
                case "internalProductId":
                case "name":
                case "description":
                case "internalName":
                case "price":
                case "deliveryCost":
                case "indTaxes":
                case "indDelivery": {
                    //manejo externo
                    break;
                }
                case "codProducto": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    if (((long)em.createNamedQuery("Producto.countByCodProduct").setParameter("codProduct", ((JsonString) atributo.getValue()).getString()).getSingleResult())>1) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "entity_same_cod_product");
                    }
                    productEntry.setCodProduct(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "idLocation": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    String idLocation;
                    try {
                        idLocation = (String) em.createNamedQuery("LocationEntry.getInternalLocationIdByInternalLocationId").setParameter("internalLocationId", ((JsonString) atributo.getValue()).getString()).getSingleResult();
                    } catch (NoResultException e) {
                        try {
                            idLocation = (String) em.createNamedQuery("LocationEntry.getInternalLocationIdByInternalCode").setParameter("internalCode", ((JsonString) atributo.getValue()).getString()).getSingleResult();
                        } catch (NoResultException | NonUniqueResultException e2) {
                            try {
                                idLocation = (String) em.createNamedQuery("LocationEntry.getInternalLocationIdByInternalName").setParameter("internalName", ((JsonString) atributo.getValue()).getString()).getSingleResult();
                            } catch (NoResultException | NonUniqueResultException e3) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                        }
                    }
                    productEntry.setIdLocation(idLocation);
                    break;
                }
                case "sku": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    productEntry.setSku(((JsonString) atributo.getValue()).getString());
                    break;
                }
                default: {
                    List<AtributoDinamicoProducto> list = em.createNamedQuery("AtributoDinamicoProducto.findByNombreInterno").setParameter("nombreInterno", atributo.getKey()).getResultList();
                    AtributoDinamicoProducto atributoDinamico;
                    if (list.isEmpty()) {
                        atributoDinamico = new AtributoDinamicoProducto();
                        atributoDinamico.setNombre(atributo.getKey());
                        atributoDinamico.setNombreInterno(atributo.getKey());
                        atributoDinamico.setFechaCreacion(new Date());
                        atributoDinamico.setFechaModificacion(new Date());
                        atributoDinamico.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
                        atributoDinamico.setIndEstado(AtributoDinamicoProducto.Estados.PUBLICADO.getValue());
                        atributoDinamico.setIndRequerido(false);
                        atributoDinamico.setNumVersion(1L);
                        switch(atributo.getValue().getValueType()) {
                            case NUMBER: {
                                atributoDinamico.setIndTipo(AtributoDinamicoProducto.Tipos.NUMERICO.getValue());
                                break;
                            }
                            case STRING: {
                                atributoDinamico.setIndTipo(AtributoDinamicoProducto.Tipos.TEXTO.getValue());
                                break;
                            }
                            case ARRAY: {
                                if (((JsonArray) atributo.getValue()).stream().anyMatch((jsonValue) -> jsonValue.getValueType()!=JsonValue.ValueType.STRING)) {
                                    throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                                }
                                atributoDinamico.setIndTipo(AtributoDinamicoProducto.Tipos.OPCIONES.getValue());
                            }
                            default: {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                        }
                        atributoDinamico.setIdAtributo(atributoDinamicoBean.insertAtributoDinamicoProducto(atributoDinamico));
                    } else {
                        atributoDinamico = list.get(0);
                    }
                    
                    AtributoDinamicoProducto.Tipos td = AtributoDinamicoProducto.Tipos.get(atributoDinamico.getIndTipo());
                    if (td==null) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    switch(td) {
                        case NUMERICO: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            map.put(atributoDinamico.getIdAtributo(), new ProductoAtributo(new Date(), ((JsonNumber) atributo.getValue()).bigDecimalValue().toPlainString()));
                            break;
                        }
                        case TEXTO: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            map.put(atributoDinamico.getIdAtributo(), new ProductoAtributo(new Date(), ((JsonString) atributo.getValue()).getString()));
                            break;
                        }
                        case OPCIONES: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.ARRAY) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            String value = ((JsonArray) atributo.getValue()).stream().map((jsonValue) -> {
                                if (jsonValue.getValueType()!=JsonValue.ValueType.STRING) {
                                    throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                                }
                                return ((JsonString) jsonValue).getString();
                            }).collect(Collectors.joining("&"));
                            map.put(atributoDinamico.getIdAtributo(), new ProductoAtributo(new Date(), value));
                            break;
                        }
                    }
                }
            }
        });
        
        productEntry.setEfectividadIndCalendarizado('P');
        productEntry.setLimiteIndRespuesta(true);
        productEntry.setImagenArte(Indicadores.URL_IMAGEN_PREDETERMINADA);
        Date fecha = new Date();
        productEntry.setFechaCreacion(fecha);
        productEntry.setFechaModificacion(fecha);
        productEntry.setFechaPublicacion(fecha);
        
        em.persist(productEntry);
        try {
            em.flush();
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        } catch (Exception e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, data, "could_not_be_persisted");
        }
        map.entrySet().forEach((entry) -> {
            ProductoAtributo productoAtributo = entry.getValue();
            productoAtributo.setProductoAtributoPK(new ProductoAtributoPK(productEntry.getInternalProductId(), entry.getKey()));
            em.merge(productoAtributo);
        });
        try {
            em.flush();
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        } catch (Exception e) {
            em.remove(productEntry);
            em.flush();
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, data, "could_not_be_persisted");
        }
        
        return new RespuestaRegistroEntidad(productEntry.getInternalProductId(), fecha);
    }

    public void editarProducto(JsonObject data, String field, String id, Locale locale) {
        if (data==null) {
            throw new MyException(MyException.ErrorPeticion.CUERPO_PETICION_INVALIDO, locale, data, "data_registration_not_send");
        }
        
        ProductEntry productEntry = searchProduct(field, id, locale);
        
        data.entrySet().stream().distinct().forEach((atributo) -> {
            switch(atributo.getKey()) {
                case "internalProductId": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    //no se puede alterar el internal id de miembro
                    if (!((JsonString) atributo.getValue()).getString().equalsIgnoreCase(productEntry.getInternalProductId())) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    break;
                }
                case "internalName": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    if (((long)em.createNamedQuery("Producto.countByInternalName").setParameter("internalName", ((JsonString) atributo.getValue()).getString()).getSingleResult())>1) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "entity_same_internal_name");
                    }
                    productEntry.setInternalName(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "name": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    productEntry.setName(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "description": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    productEntry.setDescription(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "price": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    productEntry.setPrice(((JsonNumber) atributo.getValue()).doubleValue());
                    break;
                }
                case "deliveryCost": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    productEntry.setDeliveryCost(((JsonNumber) atributo.getValue()).doubleValue());
                    break;
                }
                case "indTaxes": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.TRUE && atributo.getValue().getValueType()!=JsonValue.ValueType.FALSE) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    productEntry.setIndTaxes(atributo.getValue().getValueType()==JsonValue.ValueType.TRUE);
                    break;
                }
                case "indDelivery": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    ProductEntry.Entrega entrega = ProductEntry.Entrega.get(((JsonString) atributo.getValue()).getString().toUpperCase().charAt(0));
                    if (entrega==null) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    productEntry.setIndDelivery(entrega.getValue());
                    break;
                }
                case "codProducto": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    if (((long)em.createNamedQuery("Producto.countByCodProduct").setParameter("codProduct", ((JsonString) atributo.getValue()).getString()).getSingleResult())>1) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "entity_same_cod_product");
                    }
                    productEntry.setCodProduct(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "idLocation": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    String idLocation;
                    try {
                        idLocation = (String) em.createNamedQuery("LocationEntry.getInternalLocationIdByInternalLocationId").setParameter("internalLocationId", ((JsonString) atributo.getValue()).getString()).getSingleResult();
                    } catch (NoResultException e) {
                        try {
                            idLocation = (String) em.createNamedQuery("LocationEntry.getInternalLocationIdByInternalCode").setParameter("internalCode", ((JsonString) atributo.getValue()).getString()).getSingleResult();
                        } catch (NoResultException | NonUniqueResultException e2) {
                            try {
                                idLocation = (String) em.createNamedQuery("LocationEntry.getInternalLocationIdByInternalName").setParameter("internalName", ((JsonString) atributo.getValue()).getString()).getSingleResult();
                            } catch (NoResultException | NonUniqueResultException e3) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                        }
                    }
                    productEntry.setIdLocation(idLocation);
                    break;
                }
                case "sku": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    productEntry.setSku(((JsonString) atributo.getValue()).getString());
                    break;
                }
                default: {
                    List<AtributoDinamicoProducto> list = em.createNamedQuery("AtributoDinamicoProducto.findByNombreInterno").setParameter("nombreInterno", atributo.getKey()).getResultList();
                    AtributoDinamicoProducto atributoDinamico;
                    if (list.isEmpty()) {
                        atributoDinamico = new AtributoDinamicoProducto();
                        atributoDinamico.setNombre(atributo.getKey());
                        atributoDinamico.setNombreInterno(atributo.getKey());
                        atributoDinamico.setFechaCreacion(new Date());
                        atributoDinamico.setFechaModificacion(new Date());
                        atributoDinamico.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
                        atributoDinamico.setIndEstado(AtributoDinamicoProducto.Estados.PUBLICADO.getValue());
                        atributoDinamico.setIndRequerido(false);
                        atributoDinamico.setNumVersion(1L);
                        switch(atributo.getValue().getValueType()) {
                            case NUMBER: {
                                atributoDinamico.setIndTipo(AtributoDinamicoProducto.Tipos.NUMERICO.getValue());
                                break;
                            }
                            case STRING: {
                                atributoDinamico.setIndTipo(AtributoDinamicoProducto.Tipos.TEXTO.getValue());
                                break;
                            }
                            case ARRAY: {
                                if (((JsonArray) atributo.getValue()).stream().anyMatch((jsonValue) -> jsonValue.getValueType()!=JsonValue.ValueType.STRING)) {
                                    throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                                }
                                atributoDinamico.setIndTipo(AtributoDinamicoProducto.Tipos.OPCIONES.getValue());
                                break;
                            }
                            default: {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                        }
                        atributoDinamico.setIdAtributo(atributoDinamicoBean.insertAtributoDinamicoProducto(atributoDinamico));
                    } else {
                        atributoDinamico = list.get(0);
                    }
                    
                    AtributoDinamicoProducto.Tipos td = AtributoDinamicoProducto.Tipos.get(atributoDinamico.getIndTipo());
                    if (td==null) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    switch(td) {
                        case NUMERICO: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            em.merge(new ProductoAtributo(productEntry.getInternalProductId(), atributoDinamico.getIdAtributo(), new Date(), ((JsonNumber) atributo.getValue()).bigDecimalValue().toPlainString()));
                            break;
                        }
                        case TEXTO: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            em.merge(new ProductoAtributo(productEntry.getInternalProductId(), atributoDinamico.getIdAtributo(), new Date(), ((JsonString) atributo.getValue()).getString()));
                            break;
                        }
                        case OPCIONES: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.ARRAY) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            String value = ((JsonArray) atributo.getValue()).stream().map((jsonValue) -> {
                                if (jsonValue.getValueType()!=JsonValue.ValueType.STRING) {
                                    throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                                }
                                return ((JsonString) jsonValue).getString();
                            }).collect(Collectors.joining("&"));
                            em.merge(new ProductoAtributo(productEntry.getInternalProductId(), atributoDinamico.getIdAtributo(), new Date(), value));
                            break;
                        }
                    }
                }
            }
        });
        
        Date fecha = new Date();
        productEntry.setFechaModificacion(fecha);
        
        em.merge(productEntry);
        try {
            em.flush();
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        } catch (Exception e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, data, "could_not_be_persisted");
        }
    }

    public JsonObject obtenerProducto(String field, String id, Locale locale) {
        ProductEntry productEntry = searchProduct(field, id, locale);
        
        JsonObjectBuilder builder = Json.createObjectBuilder();
        try {
            BeanUtils.describe(productEntry).forEach((k, v) -> {
                String key = (String) k;
                String value = (String) v;
                switch(key) {
                    case "idLocation":
                    case "indDelivery":
                    case "codProduct":
                    case "sku":
                    case "description":
                    case "internalName":
                    case "name":
                    case "internalProductId": {
                        if (value!=null) {
                            builder.add(key, value);
                        } else {
                            builder.addNull(key);
                        }
                        break;
                    }
                    case "indEstado": {
                        switch(ProductEntry.Estados.get(value.charAt(0))) {
                            case INACTIVO: {
                                builder.add("enabled", false);
                                break;
                            }
                            case PUBLICADO: {
                                builder.add("enabled", true);
                                break;
                            }
                            case ARCHIVADO: {
                                builder.addNull("enabled");
                                break;
                            }
                        }
                        break;
                    }
                    case "deliveryCost":
                    case "price": {
                        if (value!=null) {
                            builder.add(key, Double.parseDouble(value));
                        } else {
                            builder.addNull(key);
                        }
                        break;
                    }
                    case "indTaxes":{
                        if (value!=null) {
                            builder.add(key, value.equalsIgnoreCase(Boolean.TRUE.toString()));
                        } else {
                            builder.addNull(key);
                        }
                        break;
                    }
                }
            });
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, null, "can_not_form_json");
        }
        
        List<ProductoAtributo> productoAtributos = em.createNamedQuery("ProductoAtributo.findByIdProducto").setParameter("idProducto", productEntry.getInternalProductId()).getResultList();
        productoAtributos.forEach((productoAtributo) -> {
            AtributoDinamicoProducto atributoDinamico = em.find(AtributoDinamicoProducto.class, productoAtributo.getProductoAtributoPK().getIdAtributo());
            if (atributoDinamico!=null && atributoDinamico.getNombreInterno()!=null) {
                AtributoDinamicoProducto.Tipos tipoDato = AtributoDinamicoProducto.Tipos.get(atributoDinamico.getIndTipo());
                if (tipoDato!=null) {
                    switch(tipoDato) {
                        case NUMERICO: {
                            builder.add(atributoDinamico.getNombreInterno(), Double.parseDouble(productoAtributo.getValor()));
                            break;
                        }
                        case OPCIONES: {
                            JsonArrayBuilder opciones = Json.createArrayBuilder();
                            for (String valor : productoAtributo.getValor().split("&")) {
                                opciones.add(valor);
                            }
                            builder.add(atributoDinamico.getNombreInterno(), opciones);
                            break;
                        }
                        case TEXTO: {
                            builder.add(atributoDinamico.getNombreInterno(), productoAtributo.getValor());
                            break;
                        }
                    }
                }
            }
        });
        
        return builder.build();
    }

    public JsonArray obtenerProductos(String listType, Locale locale) {
        CamposListas lista = CamposListas.get(listType);
        if (lista==null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTO_RUTA_INVALIDO, locale, null, "query_parameter_invalid", "list-type");
        }
        
        Set<String> result = new HashSet<>();
        switch(lista) {
            case TODOS: {
                result.addAll(em.createNamedQuery("Producto.getIdAll").getResultList());
                result.removeAll(em.createNamedQuery("Producto.getIdByIndEstado").setParameter("indEstado", ProductEntry.Estados.BORRADOR.getValue()).getResultList());
                break;
            }
            case PUBLICADOS: {
                result.addAll(em.createNamedQuery("Producto.getIdByIndEstado").setParameter("indEstado", ProductEntry.Estados.PUBLICADO.getValue()).getResultList());
                result.addAll(em.createNamedQuery("Producto.getIdByIndEstado").setParameter("indEstado", ProductEntry.Estados.INACTIVO.getValue()).getResultList());
                break;
            }
            case ARCHIVADOS: {
                result.addAll(em.createNamedQuery("Producto.getIdByIndEstado").setParameter("indEstado", ProductEntry.Estados.ARCHIVADO.getValue()).getResultList());
                break;
            }
        }
        
        JsonArrayBuilder builder = Json.createArrayBuilder();
        result.stream().map((id) -> obtenerProducto(CamposFiltro.UUID.value, id, locale)).forEach((jsonObject) -> builder.add(jsonObject));
        return builder.build();
    }
    
    private ProductEntry searchProduct(String field, String id, Locale locale) {
        CamposFiltro filtro = CamposFiltro.get(field);
        if (filtro==null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTO_RUTA_INVALIDO, locale, null, "query_parameter_invalid", "field");
        }
        ProductEntry product;
        switch (filtro) {
            case NOMBRE_INTERNO: {
                try {
                    product = (ProductEntry) em.createNamedQuery("Producto.findByInternalName").setParameter("internalName", id).getSingleResult();
                } catch (NoResultException e) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, null, "product_not_found");
                }
                break;
            }
            case CODIGO_PRODUCTO: {
                try {
                    product = (ProductEntry) em.createNamedQuery("Producto.findByCodProduct").setParameter("codProduct", id).getSingleResult();
                } catch (NoResultException | NonUniqueResultException e) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, null, "product_not_found");
                }
                break;
            }
            case UUID: {
                product = em.find(ProductEntry.class, id);
                break;
            }
            default: {
                product = null;
            }
        }
        if (product==null || product.getIndEstado()==ProductEntry.Estados.BORRADOR.getValue()) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, null, "product_not_found");
        }
        
        return product;
    }

}
