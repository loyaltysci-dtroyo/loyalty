package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "ATRIBUTO_DINAMICO_PRODUCTO")
@NamedQueries({
        @NamedQuery(name = "AtributoDinamicoProducto.findByNombreInterno", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.nombreInterno = :nombreInterno"),
        @NamedQuery(name = "AtributoDinamicoProducto.getIndTipoByIdAtributo", query = "SELECT a.indTipo FROM AtributoDinamicoProducto a WHERE a.idAtributo = :idAtributo")
})
public class AtributoDinamicoProducto implements Serializable {

    public enum Tipos {
        NUMERICO('N'),
        TEXTO('T'),
        OPCIONES('M');
        
        private final char value;
        private static final Map<Character,Tipos>  lookup = new HashMap<>();
        
        private Tipos(char value){
            this.value=value;
        }
        
        static{
            for(Tipos tipo : values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipos get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Estados{
        BORRADOR('B'),
        PUBLICADO('P'),
        ARCHIVADO('A');
        
        private final char value;
        private static final Map<Character,Estados> lookup = new HashMap<>();
        
        private Estados (char value){
            this.value = value;
        }
        
        static{
            for(Estados estado : values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "atributo_producto_uuid")
    @GenericGenerator(name = "atributo_producto_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_ATRIBUTO")
    private String idAtributo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_REQUERIDO")
    private Boolean indRequerido;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(max = 300)
    @Column(name = "IMAGEN")
    private String imagen;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    public AtributoDinamicoProducto() {
        this.indEstado = 'P';
    }

    public String getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo == null ? null : Character.toUpperCase(indTipo);
    }

    public Boolean getIndRequerido() {
        return indRequerido;
    }

    public void setIndRequerido(Boolean indRequerido) {
        this.indRequerido = indRequerido;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtributo != null ? idAtributo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtributoDinamicoProducto)) {
            return false;
        }
        AtributoDinamicoProducto other = (AtributoDinamicoProducto) object;
        if ((this.idAtributo == null && other.idAtributo != null) || (this.idAtributo != null && !this.idAtributo.equals(other.idAtributo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.AtributoDinamicoProducto[ idAtributo=" + idAtributo + " ]";
    }
    
}
