package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PRODUCTO")
@NamedQueries(
        @NamedQuery(name = "ProductoMin.getIdsProductosByNombreInterno", query = "SELECT p.idProducto FROM ProductoMin p WHERE p.nombreInterno = :nombreInterno")
)
public class ProductoMin implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_PRODUCTO")
    private String idProducto;
    
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;

    public ProductoMin() {
    }

    public ProductoMin(String idProducto) {
        this.idProducto = idProducto;
    }


    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoMin)) {
            return false;
        }
        ProductoMin other = (ProductoMin) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.Producto[ idProducto=" + idProducto + " ]";
    }
    
}
