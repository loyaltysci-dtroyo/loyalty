package com.flecharoja.loyalty.resource;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * @author svargas
 */
@Path("reward")
public class RewardResource {

    @GET
    @Path("{status}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPremios(@PathParam("status") String status) {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }
    
    @GET
    @Path("{status}/by-member-{field}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPremiosPorMiembro(@PathParam("status") String status, @PathParam("field") String field, @PathParam("id") String id) {
        throw new UnsupportedOperationException();
    }

    @PUT
    @Path("change-to-{status}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void putPremio(String premio) {
    }
}
