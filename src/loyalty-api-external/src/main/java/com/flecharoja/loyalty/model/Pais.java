package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * * * @author svargas
 */
@Entity
@Table(name = "PAIS")
@NamedQueries({
    @NamedQuery(name = "Pais.findByAlfa2", query = "SELECT p FROM Pais p WHERE p.alfa2 = :alfa2"),
    @NamedQuery(name = "Pais.findByAlfa3", query = "SELECT p FROM Pais p WHERE p.alfa3 = :alfa3")
})
public class Pais implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_PAIS")
    private BigDecimal idPais;
    @Column(name = "ALFA2")
    private String alfa2;
    @Column(name = "ALFA3")
    private String alfa3;

    public Pais() {
    }

    public BigDecimal getIdPais() {
        return idPais;
    }

    public void setIdPais(BigDecimal idPais) {
        this.idPais = idPais;
    }

    public String getAlfa2() {
        return alfa2;
    }

    public void setAlfa2(String alfa2) {
        this.alfa2 = alfa2;
    }

    public String getAlfa3() {
        return alfa3;
    }

    public void setAlfa3(String alfa3) {
        this.alfa3 = alfa3;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPais != null ? idPais.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set 
        if (!(object instanceof Pais)) {
            return false;
        }
        Pais other = (Pais) object;
        if ((this.idPais == null && other.idPais != null) || (this.idPais != null && !this.idPais.equals(other.idPais))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Pais[ idPais=" + idPais + " ]";
    }
}