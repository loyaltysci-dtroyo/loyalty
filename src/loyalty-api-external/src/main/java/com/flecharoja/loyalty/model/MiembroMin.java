package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author wtencio, svargas
 */
@Entity
@Table(name = "MIEMBRO")
@NamedQueries({
        @NamedQuery(name = "MiembroMin.findMiembrosByDocIdentificacion", query = "SELECT m FROM MiembroMin m WHERE m.docIdentificacion = :docIdentificacion"),
        @NamedQuery(name = "MiembroMin.findMiembrosByCorreo", query = "SELECT m FROM MiembroMin m WHERE m.correo = :correo")
})
public class MiembroMin implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;

    @Column(name = "DOC_IDENTIFICACION")
    private String docIdentificacion;
    
    @Column(name = "EMAIL")
    private String correo;

    public MiembroMin() {
    }

    public MiembroMin(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getDocIdentificacion() {
        return docIdentificacion;
    }

    public void setDocIdentificacion(String docIdentificacion) {
        this.docIdentificacion = docIdentificacion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiembroMin)) {
            return false;
        }
        MiembroMin other = (MiembroMin) object;
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Miembro[ idMiembro=" + idMiembro + " ]";
    }

}
