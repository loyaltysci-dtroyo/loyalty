
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PRODUCTO")
@NamedQueries({
    @NamedQuery(name = "Producto.findByInternalName", query = "SELECT p FROM ProductEntry p WHERE p.internalName = :internalName"),
    @NamedQuery(name = "Producto.findByCodProduct", query = "SELECT p FROM ProductEntry p WHERE p.codProduct = :codProduct"),
    @NamedQuery(name = "Producto.countByInternalName", query = "SELECT COUNT(p) FROM ProductEntry p WHERE p.internalName = :internalName"),
    @NamedQuery(name = "Producto.countByCodProduct", query = "SELECT COUNT(p) FROM ProductEntry p WHERE p.codProduct = :codProduct"),
    @NamedQuery(name = "Producto.getIdByIndEstado", query = "SELECT p.internalProductId FROM ProductEntry p WHERE p.indEstado = :indEstado"),
    @NamedQuery(name = "Producto.getIdAll", query = "SELECT p.internalProductId FROM ProductEntry p")
})
public class ProductEntry implements Serializable {
    
    public enum Efectividad{
        PERMANENTE('P'),
        CALENDARIZADO('C');
        
        private final char value;
        private static final Map<Character,Efectividad> lookup =  new HashMap<>();

        private Efectividad(char value) {
            this.value = value;
        }
        
        static{
            for(Efectividad efectividad : values()){
                lookup.put(efectividad.value, efectividad);
            }
        }

        public char getValue() {
            return value;
        }
        public static Efectividad get(Character value){
            return value == null ? null : lookup.get(value);
        }
    }
    
    public enum Estados{
        INACTIVO('I'),
        BORRADOR('B'),
        PUBLICADO('P'),
        ARCHIVADO('A');
        
        private final char value;
        private static final Map<Character,Estados> lookup =  new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static{
            for(Estados estados : values()){
                lookup.put(estados.value, estados);
            }
        }

        public char getValue() {
            return value;
        }
        public static Estados get(Character value){
            return value == null ? null : lookup.get(value);
        }
    }
    
    public enum Entrega{
        DOMICILIO('D'),
        UBICACION('U');
        
        private final char value;
        private static final Map<Character,Entrega> lookup =  new HashMap<>();

        private Entrega(char value) {
            this.value = value;
        }
        
        static{
            for(Entrega entrega : values()){
                lookup.put(entrega.value, entrega);
            }
        }

        public char getValue() {
            return value;
        }
        public static Entrega get(Character value){
            return value == null ? null : lookup.get(value);
        }
    }
    
    public enum IntervalosTiempoRespuesta{
        POR_SIEMPRE('A'),
        POR_MINUTO('B'),
        POR_HORA('C'),
        POR_DIA('D'),
        POR_SEMANA('E'),
        POR_MES('F');
        
        private final char value;
        private static final Map<Character,IntervalosTiempoRespuesta> lookup = new HashMap<>();

        private IntervalosTiempoRespuesta(char value) {
            this.value = value;
        }
        
        
        static{
            for(IntervalosTiempoRespuesta tipo : values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static IntervalosTiempoRespuesta get(Character value){
            return value == null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "producto_uuid")
    @GenericGenerator(name = "producto_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_PRODUCTO")
    @ApiModelProperty(hidden = true)
    private String internalProductId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    @ApiModelProperty(value = "Common name", required = true)
    private String name;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE_INTERNO")
    @ApiModelProperty(value = "Internal name", required = true)
    private String internalName;
    
    @Size(max = 300)
    @Column(name = "DESCRIPCION")
    @ApiModelProperty(value = "Description")
    private String description;
    
    @Size(max = 100)
    @Column(name = "SKU")
    @ApiModelProperty(value = "SKU code")
    private String sku;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRECIO")
    @ApiModelProperty(value = "Price", required = true)
    private Double price;
    
    @Column(name = "MONTO_ENTREGA")
    @ApiModelProperty(value = "Delivery cost")
    private Double deliveryCost;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    @ApiModelProperty(hidden = true)
    private Character indEstado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "EFECTIVIDAD_IND_CALENDARIZADO")
    @ApiModelProperty(hidden = true)
    private Character efectividadIndCalendarizado;
    
    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(hidden = true)
    private Date fechaPublicacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(hidden = true)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(hidden = true)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN_ARTE")
    @ApiModelProperty(hidden = true)
    private String imagenArte;
    
    @Basic(optional = false)
    @NotNull
    @Size(max = 150)
    @Column(name = "ENCABEZADO_ARTE")
    @ApiModelProperty(hidden = true)
    private String encabezadoArte;
    
    @Basic(optional = false)
    @NotNull
    @Size(max = 500)
    @Column(name = "DETALLE_ARTE")
    @ApiModelProperty(hidden = true)
    private String detalleArte;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_IMPUESTO")
    @ApiModelProperty(value = "Taxes", required = true)
    private Boolean indTaxes;
    
    @Column(name = "LIMITE_IND_RESPUESTA")
    @ApiModelProperty(hidden = true)
    private Boolean limiteIndRespuesta;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    @ApiModelProperty(hidden = true)
    private Long numVersion;
    
    @Size(max = 40)
    @Column(name = "COD_PRODUCTO")
    @ApiModelProperty(value = "Product code")
    private String codProduct;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ENTREGA")
    @ApiModelProperty(value = "Home delivery available", required = true)
    private Character indDelivery;
    
    @Column(name = "ID_UBICACION")
    @ApiModelProperty(value = "Location identification")
    private String idLocation;
    
    public ProductEntry() {
    }
    
    public String getInternalProductId() {
        return internalProductId;
    }

    public void setInternalProductId(String internalProductId) {
        this.internalProductId = internalProductId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.encabezadoArte = name;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        this.detalleArte = description;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getIndTaxes() {
        return indTaxes;
    }

    public void setIndTaxes(Boolean indTaxes) {
        this.indTaxes = indTaxes;
    }

    public Double getDeliveryCost() {
        return deliveryCost;
    }

    public void setDeliveryCost(Double deliveryCost) {
        this.deliveryCost = deliveryCost;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }

    public Character getEfectividadIndCalendarizado() {
        return efectividadIndCalendarizado;
    }

    public void setEfectividadIndCalendarizado(Character efectividadIndCalendarizado) {
        this.efectividadIndCalendarizado = efectividadIndCalendarizado == null ? null : Character.toUpperCase(efectividadIndCalendarizado);
    }

    public Character getIndDelivery() {
        return indDelivery;
    }

    public void setIndDelivery(Character indDelivery) {
        this.indDelivery = indDelivery == null ? null : Character.toUpperCase(indDelivery);
    }


    public Boolean getLimiteIndRespuesta() {
        return limiteIndRespuesta;
    }

    public void setLimiteIndRespuesta(Boolean limiteIndRespuesta) {
        this.limiteIndRespuesta = limiteIndRespuesta;
    }
    
    public String getImagenArte() {
        return imagenArte;
    }

    public void setImagenArte(String imagenArte) {
        this.imagenArte = imagenArte;
    }

    public String getEncabezadoArte() {
        return encabezadoArte;
    }

    public void setEncabezadoArte(String encabezadoArte) {
        this.encabezadoArte = encabezadoArte;
    }

    public String getDetalleArte() {
        return detalleArte;
    }

    public void setDetalleArte(String detalleArte) {
        this.detalleArte = detalleArte;
    }

    public String getIdLocation() {
        return idLocation;
    }

    public void setIdLocation(String idLocation) {
        this.idLocation = idLocation;
    }
    
    public String getCodProduct() {
        return codProduct;
    }

    public void setCodProduct(String codProduct) {
        this.codProduct = codProduct;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (internalProductId != null ? internalProductId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductEntry)) {
            return false;
        }
        ProductEntry other = (ProductEntry) object;
        if ((this.internalProductId == null && other.internalProductId != null) || (this.internalProductId != null && !this.internalProductId.equals(other.internalProductId))) {
            return false;
        }
        return true;
    }
   
}
