package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "REGLA_ASIGNACION_PREMIO")
@NamedQueries({
    @NamedQuery(name = "ReglaAsignacionPremio.findByIdPremioOrderByFechaCreacion", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.idPremio = :idPremio ORDER BY r.fechaCreacion DESC")
})
public class ReglaAsignacionPremio implements Serializable {

    public enum TiposRegla {
        FRECUENCIA_COMPRA("A"),
        MONTO_COMPRA("B"),
        ACUMULADO_COMPRA("C");

        private final String value;
        private static final Map<String, TiposRegla> lookup = new HashMap<>();

        private TiposRegla(String value) {
            this.value = value;
        }

        static {
            for (TiposRegla tipo : TiposRegla.values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public static TiposRegla get(String value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum TiposUltimaFecha {
        DIA("D"),
        MES("M"),
        ANO("A");

        private final String value;
        private static final Map<String, TiposUltimaFecha> lookup = new HashMap<>();

        private TiposUltimaFecha(String value) {
            this.value = value;
        }

        static {
            for (TiposUltimaFecha tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public static TiposUltimaFecha get(String value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum TiposFecha {
        ULTIMO("B"),
        ANTERIOR("C"),
        DESDE("D"),
        ENTRE("E"),
        MES_ACTUAL("F"),
        ANO_ACTUAL("G"),
        HASTA("H");

        private final String value;
        private static final Map<String, TiposFecha> lookup = new HashMap<>();

        private TiposFecha(String value) {
            this.value = value;
        }

        static {
            for (TiposFecha valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public static TiposFecha get(String value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum ValoresIndOperador {
        IGUAL("V"),
        MAYOR("W"),
        MAYOR_IGUAL("X"),
        MENOR("Y"),
        MENOR_IGUAL("Z");

        private final String value;
        private static final Map<String, ValoresIndOperador> lookup = new HashMap<>();

        private ValoresIndOperador(String value) {
            this.value = value;
        }

        static {
            for (ValoresIndOperador valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public static ValoresIndOperador get(String value) {
            return value == null ? null : lookup.get(value);
        }
    }
    
    public enum ValoresIndOperadorRegla {
        OR("O"),
        AND("A");

        private final String value;
        private static final Map<String, ValoresIndOperadorRegla> lookup = new HashMap<>();

        private ValoresIndOperadorRegla(String value) {
            this.value = value;
        }

        static {
            for (ValoresIndOperadorRegla valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public static ValoresIndOperadorRegla get(String value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_REGLA")
    private String idRegla;

    @Column(name = "IND_TIPO_REGLA")
    private String indTipoRegla;

    @Column(name = "IND_OPERADOR")
    private String indOperador;

    @Column(name = "VALOR_COMPARACION")
    private String valorComparacion;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "FECHA_FINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFinal;

    @Column(name = "FECHA_IND_ULTIMO")
    private String fechaIndUltimo;

    @Column(name = "FECHA_CANTIDAD")
    private String fechaCantidad;

    @Column(name = "FECHA_IND_TIPO")
    private String fechaIndTipo;// tipo de calendarizacion de la regla (TipoFecha)

    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "IND_OPERADOR_REGLA")
    private String indOperadorRegla;
    
    @JoinColumn(name = "TIER", referencedColumnName = "ID_NIVEL")
    @ManyToOne()
    private NivelMetrica tier;
    
    @Column(name = "PREMIO")
    private String idPremio;

    public ReglaAsignacionPremio() {
    }

    public String getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(String idRegla) {
        this.idRegla = idRegla;
    }

    public String getIndTipoRegla() {
        return indTipoRegla;
    }

    public void setIndTipoRegla(String indTipoRegla) {
        this.indTipoRegla = indTipoRegla;
    }

    public String getIndOperador() {
        return indOperador;
    }

    public void setIndOperador(String indOperador) {
        this.indOperador = indOperador;
    }

    public String getValorComparacion() {
        return valorComparacion;
    }

    public void setValorComparacion(String valorComparacion) {
        this.valorComparacion = valorComparacion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public String getFechaIndUltimo() {
        return fechaIndUltimo;
    }

    public void setFechaIndUltimo(String fechaIndUltimo) {
        this.fechaIndUltimo = fechaIndUltimo;
    }

    public String getFechaCantidad() {
        return fechaCantidad;
    }

    public void setFechaCantidad(String fechaCantidad) {
        this.fechaCantidad = fechaCantidad;
    }

    public String getFechaIndTipo() {
        return fechaIndTipo;
    }

    public void setFechaIndTipo(String fechaIndTipo) {
        this.fechaIndTipo = fechaIndTipo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIndOperadorRegla() {
        return indOperadorRegla;
    }

    public void setIndOperadorRegla(String indOperadorRegla) {
        this.indOperadorRegla = indOperadorRegla;
    }
    
    public NivelMetrica getTier() {
        return tier;
    }

    public void setTier(NivelMetrica tier) {
        this.tier = tier;
    }

    public String getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(String idPremio) {
        this.idPremio = idPremio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegla != null ? idRegla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReglaAsignacionPremio)) {
            return false;
        }
        ReglaAsignacionPremio other = (ReglaAsignacionPremio) object;
        if ((this.idRegla == null && other.idRegla != null) || (this.idRegla != null && !this.idRegla.equals(other.idRegla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ReglaAsignacionPremio[ idRegla=" + idRegla + " ]";
    }

}
