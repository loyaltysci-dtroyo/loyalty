package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CodigoCertificado;
import com.flecharoja.loyalty.model.CodigoCertificadoPK;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.ExistenciasUbicacion;
import com.flecharoja.loyalty.model.ExistenciasUbicacionPK;
import com.flecharoja.loyalty.model.HistoricoMovimientos;
import com.flecharoja.loyalty.model.MemberEntry;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioListaMiembro;
import com.flecharoja.loyalty.model.PremioListaMiembroPK;
import com.flecharoja.loyalty.model.PremioMiembro;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.google.common.collect.Lists;
import com.google.common.primitives.Chars;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TemporalType;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author wtencio, svargas
 */
@Stateless
public class PremioBean {

    @EJB
    MyKafkaUtils myKafkaUtils;

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    private final Configuration hbaseConf;

    public PremioBean() {
        this.hbaseConf = new Configuration();
        this.hbaseConf.addResource("conf/hbase/hbase-site.xml");
    }

    public void otorgarPremio(String idPremio, String idMiembro, Locale locale) {
        CodigoCertificado codigoCertificado = null;
        //verificar el minimo que tiene de metrica el premio lo tenga el miembro
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new IllegalArgumentException("reward_not_found");
        }
        MemberEntry miembro = em.find(MemberEntry.class, idMiembro);
        if (miembro == null) {
            throw new IllegalArgumentException("member_not_found");
        }

        //verificacion de la elegibilidad de redimir el premio
        if (!(premio.getIndEstado().equals(Premio.Estados.PUBLICADO.getValue()) && checkElegibilidadPremioMiembro(idMiembro, idPremio) && checkIntervalosRespuestaPremioMiembro(premio, idMiembro) && checkCalendarizacionPremio(premio) && existsExitenciaUbicacion(idPremio, locale))) {
            throw new IllegalStateException("reward_not_available");
        }

        //registro en premio lista
        PremioMiembro premioMiembro = new PremioMiembro();
        premioMiembro.setEstado(PremioMiembro.Estados.REDIMIDO_SIN_RETIRAR.getValue());
        premioMiembro.setFechaAsignacion(Calendar.getInstance().getTime());
        premioMiembro.setIdMiembro(idMiembro);
        premioMiembro.setIndMotivoAsignacion(PremioMiembro.Motivos.REWARD.getValue());
        premioMiembro.setIdPremio(idPremio);
        if (premio.getCantDiasVencimiento()!=null && premio.getCantDiasVencimiento()>0) {
            premioMiembro.setFechaExpiracion(fechaVencimiento(new Date(), premio.getCantDiasVencimiento()));
        }
        if (Premio.Tipo.get(premio.getIndTipoPremio())==Premio.Tipo.CERTIFICADO) {
            List<CodigoCertificado> certificados = em.createNamedQuery("CodigoCertificado.findByIdPremio")
                    .setParameter("idPremio", idPremio)
                    .setParameter("indEstado", CodigoCertificado.Estados.DISPONIBLE.getValue())
                    .getResultList();
            if (!certificados.isEmpty()) {
                codigoCertificado = certificados.get(0);
                premioMiembro.setCodigoCertificado(codigoCertificado.getCodigoCertificadoPK().getCodigo());
                codigoCertificado.setIndEstado(CodigoCertificado.Estados.OCUPADO.getValue());
            } else {
                throw new IllegalStateException("certificates_not_available");
            }
            em.merge(codigoCertificado);
        }
        //persisto
        em.persist(premioMiembro);

        afectarExistenciasHistorico(idPremio, idMiembro, premioMiembro.getIdPremioMiembro(), locale);

        try {
            em.flush();
        } catch (PersistenceException e) {
            throw new RuntimeException("redeem_failed");
        }

        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.REMIDIO_PREMIO, idPremio, idMiembro);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
        }
    }

    /**
     * filtrado por inclusion/exclusion
     */
    private boolean checkElegibilidadPremioMiembro(String idMiembro, String idPremio) {
        //se retraen los segmentos elegibles para el miembro y se particionan en listas de 1000(por limitante de numero de identificadores en clausula SQL "IN" )
        List<List<String>> partitionsSegmentos;
        partitionsSegmentos = Lists.partition(getSegmentosPertenecientesMiembro(idMiembro), 999);

        //se consulta si exsite un registro de inclusion/exclusion del miembro para el premio
        PremioListaMiembro miembroIncluidoExcluido = em.find(PremioListaMiembro.class, new PremioListaMiembroPK(idMiembro, idPremio));
        if (miembroIncluidoExcluido != null) {
            //si existe... se filtra por su tipo... (inclusion o exclusion)
            switch (miembroIncluidoExcluido.getIndTipo()) {
                case Indicadores.INCLUIDO: {
                    return true;
                }
                case Indicadores.EXCLUIDO: {
                    return false;
                }
            }
        }

        //se verifica si existe algun segmento excluido en el premio que este dentro de los segmentos pertenecientes del miembro
        if (partitionsSegmentos.stream()
                .anyMatch((l) -> ((long) em.createNamedQuery("PremioListaSegmento.countByIdPremioAndSegmentosInListaAndByIndTipo")
                .setParameter("idPremio", idPremio)
                .setParameter("lista", l)
                .setParameter("indTipo", Indicadores.EXCLUIDO)
                .getSingleResult() > 0))) {
            return false;
        }
        //se comprueba si no existe ningun segmento incluido para el premio (se asume que es para todos)
        if ((long) em.createNamedQuery("PremioListaSegmento.countByIdPremioAndByIndTipo")
                .setParameter("idPremio", idPremio)
                .setParameter("indTipo", Indicadores.INCLUIDO)
                .getSingleResult() == 0) {
            return true;
        } else {
            //se verifica si existe algun segmento incluido en el premio que este dentro de los segmentos pertenecientes del miembro
            return partitionsSegmentos.stream()
                    .anyMatch((l) -> ((long) em.createNamedQuery("PremioListaSegmento.countByIdPremioAndSegmentosInListaAndByIndTipo")
                    .setParameter("idPremio", idPremio)
                    .setParameter("lista", l)
                    .setParameter("indTipo", Indicadores.INCLUIDO)
                    .getSingleResult() > 0));
        }
    }

    /**
     * filtrado de premio por tiempo transcurrido entre intervalos de respuestas
     */
    private boolean checkIntervalosRespuestaPremioMiembro(Premio premio, String idMiembro) {
        if (premio.getIndRespuesta() != null && premio.getIndRespuesta()) {
            Premio.IntervalosTiempoRespuesta intervalo;
            boolean flag = true;//bandera indicando si se aceptan nuevas rediciones (por defecto true)

            //verificacion de intervalos de respuesta totales
            intervalo = Premio.IntervalosTiempoRespuesta.get(premio.getIndIntervaloTotal());
            if (intervalo != null) {
                flag = checkIntervaloRespuestaPremio(null, premio.getIdPremio(), intervalo, premio.getCantTotal());
            }

            //verificacion de intervalos de respuesta totales por miembro..
            intervalo = Premio.IntervalosTiempoRespuesta.get(premio.getIndIntervaloMiembro());
            if (intervalo != null && flag) {
                flag = checkIntervaloRespuestaPremio(idMiembro, premio.getIdPremio(), intervalo, premio.getCantTotalMiembro());
            }

            //verificacion de tiempo entre respuestas del miembro...
            if (intervalo != null && flag) {
                Date fechaUltima = (Date) em.createNamedQuery("PremioMiembro.findByUltimaFechaAsignacion")
                        .setParameter("idMiembro", idMiembro)
                        .setParameter("idPremio", premio.getIdPremio())
                        .getSingleResult();
                if (fechaUltima != null) {
                    switch (intervalo) {
                        case POR_DIA: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.DAY_OF_YEAR, -premio.getCantIntervaloRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                        case POR_HORA: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.HOUR_OF_DAY, -premio.getCantIntervaloRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                        case POR_MINUTO: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.MINUTE, -premio.getCantIntervaloRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                    }
                }
            }
            return flag;
        }
        return true;
    }

    /**
     * true -> se aceptan nuevas respuestas false -> no se aceptan nuevas
     * respuestas
     */
    private boolean checkIntervaloRespuestaPremio(String idMiembro, String idPremio, Premio.IntervalosTiempoRespuesta intervalo, Long cantTotal) {
        /*
        Se obtienen la cantidad de respuestas de registros de respuestas de premio en rangos de tiempos de fecha
         */
        long cantidadRespuestas = 0;
        switch (intervalo) {
            case POR_SIEMPRE: {
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasPremio").setParameter("idPremio", idPremio).getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasPremioMiembro").setParameter("idPremio", idPremio).setParameter("idMiembro", idMiembro).getSingleResult();
                }
                break;
            }
            case POR_MES: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_SEMANA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.WEEK_OF_YEAR, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_DIA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_HORA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.HOUR_OF_DAY, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();

                }

                break;
            }
            case POR_MINUTO: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MINUTE, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
        }
        //mientras que la cantidad de respuestas siga siendo menor a la cantidad de respuestas totales aceptables..
        return cantidadRespuestas < cantTotal;
    }

    /**
     * Método que verifica que el premio este entre las fechas correctas
     */
    private boolean checkCalendarizacionPremio(Premio premio) {
        //se establece el indicador de respuesta como verdadero(todo es permitido hasta que sea negado)
        boolean flag = true;
        //se obtiene la fecha actual
        Calendar hoy = Calendar.getInstance();

        //si el premio es calendarizado
        if (premio.getEfectividadIndCalendarizado().equals(Premio.Efectividad.CALENDARIZADO.getValue())) {
            //si la fecha de inicio esta establecida, se comprueba que la fecha sea pasada a hoy o que sea el mismo dia
            if (premio.getEfectividadFechaInicio() != null) {
                flag = hoy.getTime().after(premio.getEfectividadFechaInicio()) || DateUtils.isSameDay(hoy.getTime(), premio.getEfectividadFechaInicio());
            }
            //mientras siga siendo verdadero...
            //si la fecha de fin esta establecida, se comprueba que la fecha sea posterior a hoy o que sea el mismo dia
            if (flag && premio.getEfectividadFechaFin() != null) {
                flag = hoy.getTime().before(premio.getEfectividadFechaFin()) || DateUtils.isSameDay(hoy.getTime(), premio.getEfectividadFechaFin());
            }

            //mientras siga siendo verdadero...
            if (flag && premio.getEfectividadIndDias() != null) {
                //se obtiene el numero de dia de hoy
                int hoyDia = hoy.get(Calendar.DAY_OF_WEEK);
                //se recorre los indicadores de dias de recurrencia en busca de que se cumpla al menos una condicion
                flag = Chars.asList(premio.getEfectividadIndDias().toCharArray()).stream().anyMatch((t) -> {
                    //segun el indicador de dia de recurrencia, se verifica si el numero de dia de hoy concuerda con el numero de dia asociado al indicador
                    switch (t) {
                        case 'L': {
                            return Calendar.MONDAY == hoyDia;
                        }
                        case 'K': {
                            return Calendar.TUESDAY == hoyDia;
                        }
                        case 'M': {
                            return Calendar.WEDNESDAY == hoyDia;
                        }
                        case 'J': {
                            return Calendar.THURSDAY == hoyDia;
                        }
                        case 'V': {
                            return Calendar.FRIDAY == hoyDia;
                        }
                        case 'S': {
                            return Calendar.SATURDAY == hoyDia;
                        }
                        case 'D': {
                            return Calendar.SUNDAY == hoyDia;
                        }
                        default: {
                            return false;
                        }
                    }
                });
            }
            if (flag && premio.getEfectividadIndSemana() != null) {
                long weeksPast = ChronoUnit.WEEKS.between(LocalDate.ofEpochDay(premio.getEfectividadFechaInicio() == null ? premio.getFechaPublicacion().getTime() : premio.getEfectividadFechaInicio().getTime()), LocalDate.now());
                if (weeksPast % premio.getEfectividadIndSemana().longValue() != 0) {
                    flag = false;
                }
            }
        }
        return flag;
    }

    /**
     * Método que dice si existen existencias del producto en la ubicacion
     * central
     *
     * @param idPremio identificador del premio
     * @param locale
     * @return
     */
    private boolean existsExitenciaUbicacion(String idPremio, Locale locale) {
        ConfiguracionGeneral config = em.find(ConfiguracionGeneral.class, 0l);

        if (config.getIntegracionInventarioPremios() == null || config.getIntegracionInventarioPremios() == false) {
            return true;
        } else {
            if (config.getUbicacionPrincipal() == null) {
                throw new IllegalStateException("location_undefined");
            }
            String idUbicacion = config.getUbicacionPrincipal();
            Premio premio = em.find(Premio.class, idPremio);
            if (premio.getIndTipoPremio().equals(Premio.Tipo.PRODUCTO.getValue())) {
                ExistenciasUbicacion existencias = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(idUbicacion, idPremio, config.getMes(), config.getPeriodo()));
                if (existencias == null) {
                    return false;
                }
                return existencias.getSaldoActual() >= 1;
            } else {
                long cantCertificados = (long) em.createNamedQuery("CodigoCertificado.countByIdPremio").setParameter("idPremio", idPremio).setParameter("indEstado", CodigoCertificado.Estados.DISPONIBLE.getValue()).getSingleResult();
                return cantCertificados >= 1;
            }
        }
    }

    private void afectarExistenciasHistorico(String idPremio, String idMiembro, String idPremioMiembro, Locale locale) {
        ConfiguracionGeneral config = em.find(ConfiguracionGeneral.class, 0l);
        if (config.getIntegracionInventarioPremios() == true) {
            if (config.getUbicacionPrincipal() == null) {
                throw new IllegalStateException("location_undefined");
            }
            String idUbicacion = config.getUbicacionPrincipal();
            HistoricoMovimientos historico;
            Premio premio = em.find(Premio.class, idPremio);
            if (premio == null) {
                throw new IllegalStateException("reward_not_found");
            }

            if (premio.getIndTipoPremio().equals(Premio.Tipo.PRODUCTO.getValue())) {
                ExistenciasUbicacion existencias = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(idUbicacion, idPremio, config.getMes(), config.getPeriodo()));
                if (existencias == null) {
                    throw new IllegalStateException("no_record_reward");
                }

                if (existencias.getSaldoActual() >= 1) {
                    existencias.setSalidas(existencias.getSalidas() + 1);
                    existencias.setSaldoActual((existencias.getSaldoInicial() + existencias.getEntradas()) - existencias.getSalidas());
                }
                try {
                    em.merge(existencias);
                    em.flush();
                } catch (ConstraintViolationException e) {
                    throw new RuntimeException("constraints_violated");
                }
                historico = new HistoricoMovimientos(premio.getPrecioPromedio(), idMiembro, idPremio, existencias.getExistenciasUbicacionPK().getCodUbicacion(), null, 1L, HistoricoMovimientos.Tipos.REDENCION_AUTO.getValue(), new Date(), HistoricoMovimientos.Status.REDIMIDO_SIN_RETIRAR.getValue(), idPremioMiembro);
                long totalPremios = (long) em.createNamedQuery("ExistenciasUbicacion.findByPremioByPeriodoByMes").setParameter("idPremio", idPremio).setParameter("mes", config.getMes()).setParameter("periodo", config.getPeriodo()).getSingleResult();
                premio.setTotalExistencias(totalPremios);
            } else {
                historico = new HistoricoMovimientos(null, idMiembro, idPremio, config.getUbicacionPrincipal(), null, 1L, HistoricoMovimientos.Tipos.REDENCION_AUTO.getValue(), new Date(), HistoricoMovimientos.Status.REDIMIDO_SIN_RETIRAR.getValue(), idPremioMiembro);
                long totalCertificados = (long) em.createNamedQuery("CodigoCertificado.countByIdPremio").setParameter("idPremio", idPremio).setParameter("indEstado", CodigoCertificado.Estados.DISPONIBLE.getValue()).getSingleResult();
                premio.setTotalExistencias(totalCertificados);
            }

            try {
                em.persist(historico);
                em.merge(premio);
            } catch (ConstraintViolationException e) {
                throw new RuntimeException("constraints_violated");
            }
        }
    }

    /**
     * Metodo para obtener la fecha de vencimiento
     * @param fecha
     * @param dias
     * @return
     */
    private Date fechaVencimiento(Date fecha, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 58);
        return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos

    }

    private List<String> getSegmentosPertenecientesMiembro(String idMiembro) {
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "ELEGIBILIDAD_MIEMBRO"));

            //se obtiene la fila (solo la columna familia de segmentos) con el rowkey igual al valor del identificador del miembro
            Get get = new Get(Bytes.toBytes(idMiembro));
            get.addFamily(Bytes.toBytes("SEGMENTOS"));
            Result result = table.get(get);

            //en caso de que no se haya encontrado alguna fila...
            if (result.isEmpty()) {
                return new ArrayList<>();
            }

            //por cada entrada de segmento (columna & valor), se filtran los que indiquen que el miembro pertenece y se mapea al texto de los identificadores de segmento
            return result.getFamilyMap(Bytes.toBytes("SEGMENTOS"))
                    .entrySet()
                    .stream()
                    .filter((t) -> Bytes.toBoolean(t.getValue()))
                    .map((t) -> Bytes.toString(t.getKey()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            throw new IllegalStateException("database_connection_failed");
        }
    }

    /**
     * Metodo para la entrega de un certificado de premio como entregado, segun
     * el codigo del premio y el valor del certificado
     * 
     * @param codPremio Codigo del premio
     * @param codCertificado Codigo del certificado
     * @param locale Locale de la peticion
     */
    public void entregarPremioCertificado(String codPremio, String codCertificado, Locale locale) {
        //se busca los premios con el codigo de premio asignado
        List<Premio> premios = em.createNamedQuery("Premio.findByCodPremio").setParameter("codPremio", codPremio).getResultList();
        //si no se encontro alguno o hay mas de uno (no posible dado que es unico)
        if (premios.isEmpty() || premios.size() > 1) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, null, "prize_not_found");
        }
        //detalle de premio
        Premio premio = premios.get(0);

        //se obtiene el codigo de certificado segun el valor en csv y id premio
        CodigoCertificado codigoCertificado = em.find(CodigoCertificado.class, new CodigoCertificadoPK(codCertificado, premio.getIdPremio()));

        //si no se encontro codigo alguno, se hace log de no existente
        if (codigoCertificado == null) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, null, "certificate_not_found");
        }

        //segun el estado del codigo certificado....
        CodigoCertificado.Estados estadoCodigoCertificado = CodigoCertificado.Estados.get(codigoCertificado.getIndEstado());
        if (estadoCodigoCertificado == null) {
            //si no se encontro mapeado el estado del certificado, se hace log de estado desconocido
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, null, "certificate_status_invalid");
        }
        switch (estadoCodigoCertificado) {
            case DISPONIBLE: {
                //si el certificado esta disponible, se hace log de no asignado
                throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, null, "certificate_is_available");
            }
            case OCUPADO: {
                //se busca los premios miembros con este codigo y premio
                List<PremioMiembro> premiosMiembros = em.createNamedQuery("PremioMiembro.findByIdPremioCodigoCertificado").setParameter("idPremio", codigoCertificado.getCodigoCertificadoPK().getIdPremio()).setParameter("codigoCertificado", codigoCertificado.getCodigoCertificadoPK().getCodigo()).getResultList();

                //si no se encontro ninguno, se hace log de no asignado
                if (premiosMiembros.isEmpty()) {
                    throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, null, "certificate_is_available");
                }

                //se busca por premio miembro con estado sin retirar
                Optional<PremioMiembro> optional = premiosMiembros.stream().filter((p) -> p.getEstado().equals(PremioMiembro.Estados.REDIMIDO_SIN_RETIRAR.getValue())).findFirst();
                if (optional.isPresent()) {
                    //se marca premio miembro como retirado
                    PremioMiembro premioMiembro = optional.get();
                    premioMiembro.setEstado(PremioMiembro.Estados.REDIMIDO_RETIRADO.getValue());

                    try {
                        em.merge(premioMiembro);
                        em.flush();
                    } catch (Exception e) {
                        //error en el guardado... se hace log de error procesando
                        throw new MyException(MyException.ErrorSistema.ERROR_DESCONOCIDO, locale, null, "unknown_error");
                    }
                    break;
                }

                //si no, se busca por redimidos retirados
                optional = premiosMiembros.stream().filter((p) -> p.getEstado().equals(PremioMiembro.Estados.REDIMIDO_RETIRADO.getValue())).findFirst();
                //si hay presente, se hace log de confirmacion ya hecha, si no de estado desconocido de premio miembro
                if (optional.isPresent()) {
                    throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, null, "certificate_already_delivered");
                } else {
                    throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, null, "certificate_status_invalid");
                }
            }
            default: {
                //si no hay caso establecido segun el estado, se hace log de estado desconocido de certificado
                throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, null, "certificate_status_invalid");
            }
        }
    }
}
