package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.RespuestaRegistroEntidad;
import com.flecharoja.loyalty.service.TransaccionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.ejb.EJB;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author svargas
 */
@Api(value = "Transactions")
@Path("transactions")
public class TransactionResource {
    
    @Context
    HttpServletRequest request;
    
    @EJB
    TransaccionService bean;
    
    @ApiOperation(value = "Register a new product entry", response = RespuestaRegistroEntidad.class,
            notes = "The value of the query param \"enable\" can be 0 (the entry will be register with status disable) or 1 (the entry will be register with status enable)")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "data", value = "Transaction data to register", required = true, dataType = "com.flecharoja.loyalty.model.TransaccionCompra", paramType = "body"),
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaRegistroEntidad registrarTransaccion(JsonObject data) {
        return bean.registrarTransaccion(data, request.getLocale());
    }
}
