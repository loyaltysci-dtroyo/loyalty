package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MIEMBRO")
@NamedQueries({
    @NamedQuery(name = "Miembro.findByEmail", query = "SELECT m FROM MemberEntry m WHERE m.email = :email"),
    @NamedQuery(name = "Miembro.countByIdentificationDoc", query = "SELECT COUNT(m) FROM MemberEntry m WHERE m.identificationDoc = :identificationDoc"),
    @NamedQuery(name = "Miembro.countByEmail", query = "SELECT COUNT(m) FROM MemberEntry m WHERE m.email = :email")
})
@XmlRootElement
public class MemberEntry implements Serializable {

    public enum ValoresIndGenero {
        FEMENINO('F'),
        MASCULINO('M');
        
        private final char value;
        private static final Map<Character,ValoresIndGenero> lookup =  new HashMap<>();

        private ValoresIndGenero(char value) {
            this.value = value;
        }
        
        static {
            for (ValoresIndGenero genero : values()) {
                lookup.put(genero.value, genero);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static ValoresIndGenero get(Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum ValoresIndEstadoCivil {
        CASADO('C'),
        SOLTERO('S'),
        UNION_LIBRE('U'),
        NINGUNO('N');
        
        private final char value;
        private static final Map<Character,ValoresIndEstadoCivil> lookup =  new HashMap<>();

        private ValoresIndEstadoCivil(char value) {
            this.value = value;
        }
        
        static {
            for (ValoresIndEstadoCivil estadoCivil : values()) {
                lookup.put(estadoCivil.value, estadoCivil);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static ValoresIndEstadoCivil get(Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum ValoresIndEducacion {
        PRIMARIA('P'),
        SECUNDARIA('S'),
        TECNICA('T'),
        UNIVERSITARIA('U'),
        GRADO('G'),
        POSTGRADO('R'),
        MASTER('M'),
        DOCTORADO('D');
        
        private final char value;
        private static final Map<Character,ValoresIndEducacion> lookup =  new HashMap<>();

        private ValoresIndEducacion(char value) {
            this.value = value;
        }
        
        static {
            for (ValoresIndEducacion educacion : values()) {
                lookup.put(educacion.value, educacion);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static ValoresIndEducacion get(Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Estados{
        ACTIVO('A'),
        INACTIVO('I');
        
        private final char value;
        private static final Map<Character,Estados> lookup =  new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static{
            for(Estados estado:values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        public static Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }

    public enum Residencia{
        CIUDAD('C'),
        ESTADO('E');
         private final char value;
        private static final Map<Character,Residencia> lookup =  new HashMap<>();

        private Residencia(char value) {
            this.value = value;
        }
        
        static{
            for(Residencia residencia:values()){
                lookup.put(residencia.value, residencia);
            }
        }

        public char getValue() {
            return value;
        }
        public static Residencia get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_MIEMBRO")
    @ApiModelProperty(hidden = true)
    private String internalMemberId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DOC_IDENTIFICACION")
    @ApiModelProperty(value = "Identification document", required = true)
    private String identificationDoc;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Admission date", required = true, dataType = "long")
    private Date admissionDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO_MIEMBRO")
    @ApiModelProperty(hidden = true)
    private Character indEstadoMiembro;

    @Column(name = "FECHA_EXPIRACION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Expiration date", dataType = "long")
    private Date expirationDate;

    @Size(max = 200)
    @Column(name = "DIRECCION")
    @ApiModelProperty(value = "Resident address")
    private String residentAddress;

    @Size(max = 100)
    @Column(name = "CIUDAD_RESIDENCIA")
    @ApiModelProperty(value = "Resident address city")
    private String residentCity;

    @Size(max = 100)
    @Column(name = "ESTADO_RESIDENCIA")
    @ApiModelProperty(value = "Resident address state")
    private String residentState;

    @Size(max = 3)
    @Column(name = "PAIS_RESIDENCIA")
    @ApiModelProperty(value = "Resident address country code")
    private String residentCountryCode;

    @Size(max = 10)
    @Column(name = "CODIGO_POSTAL")
    @ApiModelProperty(value = "Resident address ZIP code")
    private String residentZipCode;

    @Size(max = 15)
    @Column(name = "TELEFONO_MOVIL")
    @ApiModelProperty(value = "Mobile number")
    private String mobileNumber;

    @Column(name = "FECHA_NACIMIENTO")
    @ApiModelProperty(value = "Birthdate", dataType = "long")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;

    @Column(name = "IND_GENERO")
    @ApiModelProperty(value = "Gender")
    private Character indGender;

    @Column(name = "IND_ESTADO_CIVIL")
    @ApiModelProperty(value = "Civil status")
    private Character indCivilStatus;

    @Column(name = "FECHA_SUSPENSION")
    @ApiModelProperty(value = "Disable date", dataType = "long")
    @Temporal(TemporalType.TIMESTAMP)
    private Date disableDate;

    @Size(max = 200)
    @Column(name = "CAUSA_SUSPENSION")
    @ApiModelProperty(value = "Disable cause")
    private String disableCause;

    @Column(name = "IND_EDUCACION")
    @ApiModelProperty(value = "Education type")
    private Character indEducationType;

    @Column(name = "INGRESO_ECONOMICO")
    @ApiModelProperty(value = "Income amount")
    private Long incomeAmount;

    @Size(max = 500)
    @Column(name = "COMENTARIOS")
    @ApiModelProperty(value = "Commentaries")
    private String commentaries;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(hidden = true)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(hidden = true)
    private Date fechaModificacion;

    @Size(max = 300)
    @Column(name = "AVATAR")
    @ApiModelProperty(hidden = true)
    private String avatar;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NOMBRE")
    @ApiModelProperty(value = "Name", required = true)
    private String name;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "APELLIDO")
    @ApiModelProperty(value = "LastName", required = true)
    private String lastName1;

    @Size(max = 100)
    @Column(name = "APELLIDO2")
    @ApiModelProperty(value = "Second lastname", required = true)
    private String lastName2;
    
    @Column(name = "IND_CONTACTO_EMAIL")
    @ApiModelProperty(value = "Contact by email")
    private Boolean indContactByEmail;
    
    @Column(name = "IND_CONTACTO_SMS")
    @ApiModelProperty(value = "Contact by sms")
    private Boolean indContactBySms;
    
    @Column(name = "IND_CONTACTO_NOTIFICACION")
    @ApiModelProperty(value = "Contact by push notifications")
    private Boolean indContactByPushNotification;
    
    @Column(name = "IND_HIJOS")
    @ApiModelProperty(value = "Has children")
    private Boolean indChildren;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    @ApiModelProperty(hidden = true)
    private Long numVersion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "EMAIL")
    @ApiModelProperty(value = "Email", required = true)
    private String email;

    @Transient
    @ApiModelProperty(value = "Username", required = true)
    private String username;
    
    @Transient
    @ApiModelProperty(value = "Password", required = true)
    private String password;
    
    @Transient
    @ApiModelProperty(hidden = true)
    private boolean newMember;
    
    @Column(name = "IND_CAMBIO_PASS")
    @ApiModelProperty(hidden = true)
    private Boolean indCambioPass;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_MIEMBRO_SISTEMA")
    @ApiModelProperty(value = "Indicator about if the member is full member of the system (by default true)")
    private Boolean indSystemMember;
    
    public MemberEntry() {
        //todo miembro creado en el api externo es automaticamente miembro del sistema a no ser que se especifique que no
        this.indSystemMember = true;
    }
    
    public MemberEntry(boolean newMember) {
        this();
        this.newMember = newMember;
    }

    public String getInternalMemberId() {
        return internalMemberId;
    }

    public void setInternalMemberId(String internalMemberId) {
        this.internalMemberId = internalMemberId;
    }

    public String getIdentificationDoc() {
        return identificationDoc;
    }

    public void setIdentificationDoc(String identificationDoc) {
        this.identificationDoc = identificationDoc;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
    }

    @XmlTransient
    public Character getIndEstadoMiembro() {
        return indEstadoMiembro;
    }

    public void setIndEstadoMiembro(Character indEstadoMiembro) {
        this.indEstadoMiembro = indEstadoMiembro != null ? Character.toUpperCase(indEstadoMiembro) : null;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getResidentAddress() {
        return residentAddress;
    }

    public void setResidentAddress(String residentAddress) {
        this.residentAddress = residentAddress;
    }

    public String getResidentCity() {
        return residentCity;
    }

    public void setResidentCity(String residentCity) {
        this.residentCity = residentCity;
    }

    public String getResidentState() {
        return residentState;
    }

    public void setResidentState(String residentState) {
        this.residentState = residentState;
    }

    public String getResidentCountryCode() {
        return residentCountryCode;
    }

    public void setResidentCountryCode(String residentCountryCode) {
        this.residentCountryCode = residentCountryCode;
    }

    public String getResidentZipCode() {
        return residentZipCode;
    }

    public void setResidentZipCode(String residentZipCode) {
        this.residentZipCode = residentZipCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Character getIndGender() {
        return indGender;
    }

    public void setIndGender(Character indGender) {
        this.indGender = indGender != null ? Character.toUpperCase(indGender) : null;
    }

    public Boolean getIndContactByEmail() {
        return indContactByEmail;
    }

    public void setIndContactByEmail(Boolean indContactByEmail) {
        this.indContactByEmail = indContactByEmail;
    }

    public Boolean getIndContactBySms() {
        return indContactBySms;
    }

    public void setIndContactBySms(Boolean indContactBySms) {
        this.indContactBySms = indContactBySms;
    }

    public Boolean getIndContactByPushNotification() {
        return indContactByPushNotification;
    }

    public void setIndContactByPushNotification(Boolean indContactByPushNotification) {
        this.indContactByPushNotification = indContactByPushNotification;
    }

    public Character getIndCivilStatus() {
        return indCivilStatus;
    }

    public void setIndCivilStatus(Character indCivilStatus) {
        this.indCivilStatus = indCivilStatus != null ? Character.toUpperCase(indCivilStatus) : null;
    }

    @XmlTransient
    public Date getDisableDate() {
        return disableDate;
    }

    public void setDisableDate(Date disableDate) {
        this.disableDate = disableDate;
    }

    @XmlTransient
    public String getDisableCause() {
        return disableCause;
    }

    public void setDisableCause(String disableCause) {
        this.disableCause = disableCause;
    }

    public Character getIndEducationType() {
        return indEducationType;
    }

    public void setIndEducationType(Character indEducationType) {
        this.indEducationType = indEducationType != null ? Character.toUpperCase(indEducationType) : null;
    }

    public Long getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(Long incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public Boolean getIndChildren() {
        return indChildren;
    }

    public void setIndChildren(Boolean indChildren) {
        this.indChildren = indChildren;
    }

    public String getCommentaries() {
        return commentaries;
    }

    public void setCommentaries(String commentaries) {
        this.commentaries = commentaries;
    }

    @XmlTransient
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @XmlTransient
    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @XmlTransient
    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName1() {
        return lastName1;
    }

    public void setLastName1(String lastName1) {
        this.lastName1 = lastName1;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isNewMember() {
        return newMember;
    }

    public void setNewMember(boolean newMember) {
        this.newMember = newMember;
    }

    public String getLastName2() {
        return lastName2;
    }

    public void setLastName2(String lastName2) {
        this.lastName2 = lastName2;
    }

    public Boolean getIndCambioPass() {
        return indCambioPass;
    }

    public void setIndCambioPass(Boolean indCambioPass) {
        this.indCambioPass = indCambioPass;
    }

    public Boolean getIndSystemMember() {
        return indSystemMember;
    }

    public void setIndSystemMember(Boolean indSystemMember) {
        this.indSystemMember = indSystemMember;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (internalMemberId != null ? internalMemberId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MemberEntry)) {
            return false;
        }
        MemberEntry other = (MemberEntry) object;
        if ((this.internalMemberId == null && other.internalMemberId != null) || (this.internalMemberId != null && !this.internalMemberId.equals(other.internalMemberId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Miembro[ idMiembro=" + internalMemberId + " ]";
    }
    
}
