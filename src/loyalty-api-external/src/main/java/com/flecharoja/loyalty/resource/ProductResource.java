package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.ProductEntry;
import com.flecharoja.loyalty.model.RespuestaRegistroEntidad;
import com.flecharoja.loyalty.patch.PATCH;
import com.flecharoja.loyalty.service.ProductoService;
import com.flecharoja.loyalty.service.TransaccionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ejb.EJB;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author svargas
 */
@Api(value = "Products")
@Path("product")
public class ProductResource {
    
    @Context
    HttpServletRequest request;
    
    @EJB
    ProductoService bean;
    
    @EJB
    TransaccionService beanTranstaction;
    
    @ApiOperation(value = "Register a new product entry", response = RespuestaRegistroEntidad.class,
            notes = "The value of the query param \"enable\" can be 0 (the entry will be register with status disable) or 1 (the entry will be register with status enable)")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "data", value = "Product data to register", required = true, dataType = "com.flecharoja.loyalty.model.ProductEntry", paramType = "body"),
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaRegistroEntidad registerProduct(JsonObject data,
            @ApiParam(value = "Indicator for enabling/disabling the product entry", required = true, defaultValue = "0") @QueryParam("enable") String enable) {
        return bean.registrarProducto(data, enable, request.getLocale());
    }

    @ApiOperation(value = "Patch an existing product entry",
            notes = "The path param \"field\" can have the following values:\n-\"cod-product\" (code of the product)\n-\"internal-name\" (internal name of the product)\n-\"internal-id\" (internal id of the product)")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "data", value = "Product data to patch", required = true, dataType = "com.flecharoja.loyalty.model.ProductEntry", paramType = "body"),
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("by-{field}/{id}")
    public void patchProduct(JsonObject data,
            @ApiParam(value = "Key field to search the product entry", required = true) @PathParam("field") String field,
            @ApiParam(value = "Field value for the product entry", required = true) @PathParam("id") String id) {
        bean.editarProducto(data, field, id, request.getLocale());
    }
    
    @ApiOperation(value = "Get an existing product entry", response = ProductEntry.class,
            notes = "The path param \"field\" can have the following values:\n-\"cod-product\" (code of the product)\n-\"internal-name\" (internal name of the product)\n-\"internal-id\" (internal id of the product)")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("by-{field}/{id}")
    public JsonObject getProduct(
            @ApiParam(value = "Key field to search the product entry", required = true) @PathParam("field") String field,
            @ApiParam(value = "Field value for the product entry", required = true) @PathParam("id") String id) {
        return bean.obtenerProducto(field, id, request.getLocale());
    }
    
    @ApiOperation(value = "Get a list of product entries", response = ProductEntry.class, responseContainer = "List",
            notes = "The path param \"list-type\" can have the following values:\n-\"all\" (all the products registered)\n-\"published\" (only the published products)\n-\"archived\" (only the archived products)")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("list-{list-type}")
    public List getProductList(@ApiParam(value = "Product entries list type to get", required = true) @PathParam("list-type") String listType) {
        return bean.obtenerProductos(listType, request.getLocale());
    }
}
