package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "EXISTENCIAS_UBICACION")
@NamedQueries({
    @NamedQuery(name = "ExistenciasUbicacion.findByPremioByPeriodoByMes", query = "SELECT SUM(e.saldoActual) FROM ExistenciasUbicacion e WHERE e.existenciasUbicacionPK.codPremio = :idPremio AND e.existenciasUbicacionPK.mes = :mes AND e.existenciasUbicacionPK.periodo = :periodo")
})
public class ExistenciasUbicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ExistenciasUbicacionPK existenciasUbicacionPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SALDO_INICIAL")
    private Integer saldoInicial;

    @Basic(optional = false)
    @NotNull
    @Column(name = "SALDO_ACTUAL")
    private Integer saldoActual;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ENTRADAS")
    private Integer entradas;

    @Basic(optional = false)
    @NotNull
    @Column(name = "SALIDAS")
    private Integer salidas;

    public ExistenciasUbicacion() {
    }

    public ExistenciasUbicacion(ExistenciasUbicacionPK existenciasUbicacionPK) {
        this.existenciasUbicacionPK = existenciasUbicacionPK;
    }

    public ExistenciasUbicacion(ExistenciasUbicacionPK existenciasUbicacionPK, Integer saldoInicial, Integer entradas, Integer salidas) {
        this.existenciasUbicacionPK = existenciasUbicacionPK;
        this.saldoInicial = saldoInicial;
        this.entradas = entradas;
        this.salidas = salidas;
    }

    public ExistenciasUbicacion(String codUbicacion, String codPremio, Integer mes, Integer periodo) {
        this.existenciasUbicacionPK = new ExistenciasUbicacionPK(codUbicacion, codPremio, mes, periodo);
    }

    public ExistenciasUbicacionPK getExistenciasUbicacionPK() {
        return existenciasUbicacionPK;
    }

    public void setExistenciasUbicacionPK(ExistenciasUbicacionPK existenciasUbicacionPK) {
        this.existenciasUbicacionPK = existenciasUbicacionPK;
    }

    public Integer getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(Integer saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public Integer getEntradas() {
        return entradas;
    }

    public void setEntradas(Integer entradas) {
        this.entradas = entradas;
    }

    public Integer getSalidas() {
        return salidas;
    }

    public void setSalidas(Integer salidas) {
        this.salidas = salidas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (existenciasUbicacionPK != null ? existenciasUbicacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExistenciasUbicacion)) {
            return false;
        }
        ExistenciasUbicacion other = (ExistenciasUbicacion) object;
        if ((this.existenciasUbicacionPK == null && other.existenciasUbicacionPK != null) || (this.existenciasUbicacionPK != null && !this.existenciasUbicacionPK.equals(other.existenciasUbicacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ExistenciasUbicacion[ existenciasUbicacionPK=" + existenciasUbicacionPK + " ]";
    }

    public Integer getSaldoActual() {
        return saldoActual;
    }

    public void setSaldoActual(Integer saldoActual) {
        this.saldoActual = saldoActual;
    }

    
}
