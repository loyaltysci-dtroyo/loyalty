package com.flecharoja.loyalty.resource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author svargas
 */

@ApplicationPath("webresources/v0")
public class JAXRSConfiguration extends Application {

}
