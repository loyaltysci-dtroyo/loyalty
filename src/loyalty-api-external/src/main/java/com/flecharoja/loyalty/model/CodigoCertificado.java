package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CODIGO_CERTIFICADO")
@NamedQueries({
    @NamedQuery(name = "CodigoCertificado.findByIdPremio", query = "SELECT c FROM CodigoCertificado c WHERE c.codigoCertificadoPK.idPremio = :idPremio AND c.indEstado = :indEstado"),
    @NamedQuery(name = "CodigoCertificado.countByIdPremio", query = "SELECT COUNT(c) FROM CodigoCertificado c WHERE c.codigoCertificadoPK.idPremio = :idPremio AND c.indEstado = :indEstado")
})
public class CodigoCertificado implements Serializable {

    public enum Estados {
        DISPONIBLE('D'),
        OCUPADO('O');

        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }

        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Estados get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CodigoCertificadoPK codigoCertificadoPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;

    public CodigoCertificado() {
    }

    public CodigoCertificado(CodigoCertificadoPK codigoCertificadoPK) {
        this.codigoCertificadoPK = codigoCertificadoPK;
    }

    public CodigoCertificado(String codigo, String idPremio) {
        this.codigoCertificadoPK = new CodigoCertificadoPK(codigo, idPremio);
    }

    public CodigoCertificadoPK getCodigoCertificadoPK() {
        return codigoCertificadoPK;
    }

    public void setCodigoCertificadoPK(CodigoCertificadoPK codigoCertificadoPK) {
        this.codigoCertificadoPK = codigoCertificadoPK;
    }
    
    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoCertificadoPK != null ? codigoCertificadoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CodigoCertificado)) {
            return false;
        }
        CodigoCertificado other = (CodigoCertificado) object;
        if ((this.codigoCertificadoPK == null && other.codigoCertificadoPK != null) || (this.codigoCertificadoPK != null && !this.codigoCertificadoPK.equals(other.codigoCertificadoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CodigoCertificado[ codigoCertificadoPK=" + codigoCertificadoPK + " ]";
    }

}
