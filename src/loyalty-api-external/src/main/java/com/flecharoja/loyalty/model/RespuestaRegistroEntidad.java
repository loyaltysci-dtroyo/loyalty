package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
@ApiModel(value = "RegisterCompleteResponse")
public class RespuestaRegistroEntidad {
    @ApiModelProperty(value = "Internal code of the entry created")
    private String internalIdCreated;
    @ApiModelProperty(value = "Date", dataType = "long")
    private Date dateCreation;

    public RespuestaRegistroEntidad() {
    }

    public RespuestaRegistroEntidad(String internalIdCreated, Date dateCreation) {
        this.internalIdCreated = internalIdCreated;
        this.dateCreation = dateCreation;
    }

    public String getInternalIdCreated() {
        return internalIdCreated;
    }

    public void setInternalIdCreated(String internalIdCreated) {
        this.internalIdCreated = internalIdCreated;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }
}
