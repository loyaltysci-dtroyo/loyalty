package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PRODUCTO_ATRIBUTO")
@NamedQuery(name = "ProductoAtributo.findByIdProducto", query = "SELECT p FROM ProductoAtributo p WHERE p.productoAtributoPK.idProducto = :idProducto")
public class ProductoAtributo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected ProductoAtributoPK productoAtributoPK;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "VALOR")
    private String valor;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    public ProductoAtributo() {
    }
    
    public ProductoAtributo(Date fechaCreacion, String valor) {
        this.fechaCreacion = fechaCreacion;
        this.valor = valor;
    }
    
    public ProductoAtributo(String idProducto, String idAtributo, Date fechaCreacion, String valor) {
        this.productoAtributoPK = new ProductoAtributoPK(idProducto, idAtributo);
        this.fechaCreacion = fechaCreacion;
        this.valor = valor;
    }

    public ProductoAtributoPK getProductoAtributoPK() {
        return productoAtributoPK;
    }

    public void setProductoAtributoPK(ProductoAtributoPK productoAtributoPK) {
        this.productoAtributoPK = productoAtributoPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoAtributoPK != null ? productoAtributoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoAtributo)) {
            return false;
        }
        ProductoAtributo other = (ProductoAtributo) object;
        if ((this.productoAtributoPK == null && other.productoAtributoPK != null) || (this.productoAtributoPK != null && !this.productoAtributoPK.equals(other.productoAtributoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ProductoAtributo[ productoAtributoPK=" + productoAtributoPK + " ]";
    }
    
}
