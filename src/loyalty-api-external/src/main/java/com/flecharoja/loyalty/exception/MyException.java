package com.flecharoja.loyalty.exception;

import com.google.gson.Gson;
import com.sun.tools.javac.resources.compiler;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ApplicationException;
import javax.json.JsonObject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author svargas
 */
@ApplicationException(rollback = true)
public class MyException extends WebApplicationException {
    
    public enum ErrorPeticion {
        CUERPO_PETICION_INVALIDO(100),
        ATRIBUTO_CUERPO_INVALIDO(101),
        ARGUMENTO_RUTA_INVALIDO(102),
        ARGUMENTO_PETICION_INVALIDO(103);

        private static final Response.Status status = Response.Status.BAD_REQUEST;
        private final int value;

        private ErrorPeticion(int value) {
            this.value = value;
        }
    }

    public enum ErrorAutenticacion {
        PARAMETROS_AUTENTIFICACION_INVALIDOS(105);

        private static final Response.Status status = Response.Status.UNAUTHORIZED;
        private final int value;

        private ErrorAutenticacion(int value) {
            this.value = value;
        }
    }
    
    public enum ErrorRecursoNoEncontrado {
        ENTIDAD_NO_EXISTENTE(106);

        private static final Response.Status status = Response.Status.NOT_FOUND;
        private final int value;

        private ErrorRecursoNoEncontrado(int value) {
            this.value = value;
        }
    }
    
    public enum ErrorAutorizacion {
        TRANSACCION_NO_DISPONIBLE(110),
        TRANSACCION_INMUTABLE(109),
        ENTIDAD_ARCHIVADA(107);

        private static final Response.Status status = Response.Status.FORBIDDEN;
        private final int value;

        private ErrorAutorizacion(int value) {
            this.value = value;
        }
    }
    
    public enum ErrorConflicto {
        IDENTIFICADOR_DUPLICADO(108);

        private static final Response.Status status = Response.Status.CONFLICT;
        private final int value;

        private ErrorConflicto(int value) {
            this.value = value;
        }
    }

    public enum ErrorSistema {
        KEYCLOAK_ERROR(104),
        TIEMPO_CONEXION_DB_AGOTADO(200),
        ENTIDAD_EXISTENTE(201),
        PROBLEMAS_HBASE(202),
        OPERACION_FALLIDA(203),
        ERROR_DESCONOCIDO(300);

        private final int value;
        private final static Response.Status status = Response.Status.INTERNAL_SERVER_ERROR;

        private ErrorSistema(int value) {
            this.value = value;
        }
    }
    
    private static final String BUNDLE_NAME = "messages.error";
    
    public MyException(ErrorPeticion codigo, Locale locale, JsonObject petition, String key, Object... args) {
        super(Response.status(ErrorPeticion.status)
                .entity(new MyExceptionResponseBody(codigo.value, getMessageString(key, locale, args), getLocaleLang(locale)))
                .build()
        );
        saveExceptionData(this.getResponse(), petition);
    }
    
    public MyException(ErrorAutenticacion codigo, Locale locale, JsonObject petition, String key, Object... args) {
        super(Response.status(ErrorAutenticacion.status)
                .entity(new MyExceptionResponseBody(codigo.value, getMessageString(key, locale, args), getLocaleLang(locale)))
                .build()
        );
        saveExceptionData(this.getResponse(), petition);
    }
    
    public MyException(ErrorRecursoNoEncontrado codigo, Locale locale, JsonObject petition, String key, Object... args) {
        super(Response.status(ErrorRecursoNoEncontrado.status)
                .entity(new MyExceptionResponseBody(codigo.value, getMessageString(key, locale, args), getLocaleLang(locale)))
                .build()
        );
        saveExceptionData(this.getResponse(), petition);
    }
    
    public MyException(ErrorAutorizacion codigo, Locale locale, JsonObject petition, String key, Object... args) {
        super(Response.status(ErrorAutorizacion.status)
                .entity(new MyExceptionResponseBody(codigo.value, getMessageString(key, locale, args), getLocaleLang(locale)))
                .build()
        );
        saveExceptionData(this.getResponse(), petition);
    }
    
    public MyException(ErrorConflicto codigo, Locale locale, JsonObject petition, String key, Object... args) {
        super(Response.status(ErrorConflicto.status)
                .entity(new MyExceptionResponseBody(codigo.value, getMessageString(key, locale, args), getLocaleLang(locale)))
                .build()
        );
        saveExceptionData(this.getResponse(), petition);
    }
    
    public MyException(ErrorSistema codigo, Locale locale, JsonObject petition, String key, Object... args) {
        super(Response.status(ErrorSistema.status)
                .entity(new MyExceptionResponseBody(codigo.value, getMessageString(key, locale, args), getLocaleLang(locale)))
                .build()
        );
        saveExceptionData(this.getResponse(), petition);
    }
    
    private static String getMessageString(String key, Locale locale, Object... params) {
        try {
            if (params.length>0) {
                return MessageFormat.format(ResourceBundle.getBundle(BUNDLE_NAME, locale).getString(key), params);
            } else {
                return ResourceBundle.getBundle(BUNDLE_NAME, locale).getString(key);
            }
        } catch (MissingResourceException | IllegalArgumentException e) {
            if (!locale.getLanguage().equals(Locale.US.getLanguage())) {
                return getMessageString(key, Locale.US, params);
            }
            return '!'+key+'!';
        }
    }
    
    private static String getLocaleLang(Locale locale) {
        String lang;
        try {
            lang = ResourceBundle.getBundle(BUNDLE_NAME, locale).getLocale().getLanguage();
        } catch (MissingResourceException e) {
            lang = Locale.US.getLanguage();
        }
        return lang;
    }
    
    private static void saveExceptionData(Response response, JsonObject petition) {
        Configuration configuration = new Configuration();
        configuration.addResource("conf/hbase/hbase-site.xml");
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "LOG_EXCEPTIONS_EXTERNAL"));
            
            Put put = new Put(Bytes.toBytes(String.format("%013d", System.currentTimeMillis())+"&"+UUID.randomUUID().toString()));
            
            put.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("HTTP_STATUS"), Bytes.toBytes(response.getStatusInfo().getReasonPhrase()));
            put.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("RESPONSE_BODY"), Bytes.toBytes(new Gson().toJson(response.getEntity())));
            if (petition!=null) {
                put.addColumn(Bytes.toBytes("DETAILS"), Bytes.toBytes("PETITION_BODY"), Bytes.toBytes(petition.toString()));
            }
            
            table.put(put);
        } catch (IOException e) {
            Logger.getLogger(MyException.class.getName()).log(Level.WARNING, null, e);
        }
    }
}
