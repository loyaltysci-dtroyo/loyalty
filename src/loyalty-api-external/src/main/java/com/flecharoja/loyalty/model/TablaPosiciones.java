package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "TABLA_POSICIONES")
@NamedQueries(
    @NamedQuery(name = "TablaPosiciones.findByIdMetrica", query = "SELECT t FROM TablaPosiciones t WHERE t.idMetrica = :idMetrica")
)
public class TablaPosiciones implements Serializable {
    
    public enum Tipo{
        MIEMBROS('U'),
        GRUPO('G'),
        GRUPO_ESPECIFICO('E');
        
        private final char value;
        private static final Map<Character, Tipo> lookup = new HashMap<>();

        private Tipo(char value) {
            this.value = value;
        }
        
        static{
            for(Tipo tipo:values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipo get(Character value){
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_TABLA")
    private String idTabla;
    
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Column(name = "ID_GRUPO")
    private String idGrupo;
    
    @Column(name = "ID_METRICA")
    private String idMetrica;

    public TablaPosiciones() {
    }

    public TablaPosiciones(String idTabla) {
        this.idTabla = idTabla;
    }

    public String getIdTabla() {
        return idTabla;
    }

    public void setIdTabla(String idTabla) {
        this.idTabla = idTabla;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public String getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTabla != null ? idTabla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TablaPosiciones)) {
            return false;
        }
        TablaPosiciones other = (TablaPosiciones) object;
        if ((this.idTabla == null && other.idTabla != null) || (this.idTabla != null && !this.idTabla.equals(other.idTabla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.TablaPosiciones[ idTabla=" + idTabla + " ]";
    }
    
}
