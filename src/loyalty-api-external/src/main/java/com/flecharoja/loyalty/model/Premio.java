package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PREMIO")
@NamedQueries({
    @NamedQuery(name = "Premio.findAllByIndEstado", query = "SELECT p FROM Premio p WHERE p.indEstado = :indEstado"),
    @NamedQuery(name = "Premio.findByCodPremio", query = "SELECT p FROM Premio p WHERE p.codPremio = :codPremio")
})
public class Premio implements Serializable {
    
    public enum Tipo {
        CERTIFICADO('C'),
        PRODUCTO('P');

        private final char value;
        private static final Map<Character, Tipo> lookup = new HashMap<>();

        private Tipo(char value) {
            this.value = value;
        }

        static {
            for (Tipo tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static Tipo get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum Efectividad {
        PERMANENTE('P'),
        CALENDARIZADO('C');

        private final char value;
        private static final Map<Character, Efectividad> lookup = new HashMap<>();

        private Efectividad(char value) {
            this.value = value;
        }

        static {
            for (Efectividad efectividad : values()) {
                lookup.put(efectividad.value, efectividad);
            }
        }

        public char getValue() {
            return value;
        }

        public static Efectividad get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum Estados {
        BORRADOR('B'),
        PUBLICADO('P'),
        ARCHIVADO('A');

        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }

        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Estados get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum IntervalosTiempoRespuesta {
        POR_SIEMPRE('A'),
        POR_MINUTO('B'),
        POR_HORA('C'),
        POR_DIA('D'),
        POR_SEMANA('E'),
        POR_MES('F');

        private final char value;
        private static final Map<Character, IntervalosTiempoRespuesta> lookup = new HashMap<>();

        private IntervalosTiempoRespuesta(char value) {
            this.value = value;
        }

        static {
            for (IntervalosTiempoRespuesta intervalo : values()) {
                lookup.put(intervalo.value, intervalo);
            }
        }

        public char getValue() {
            return value;
        }

        public static IntervalosTiempoRespuesta get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_PREMIO")
    private String idPremio;

    @Column(name = "IND_TIPO_PREMIO")
    private Character indTipoPremio;

    @Column(name = "TRACKING_CODE")
    private String trackingCode;

    @Column(name = "IND_ESTADO")
    private Character indEstado;

    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;

    @Column(name = "FECHA_ARCHIVADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaArchivado;

    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    private Metrica idMetrica;

    @Column(name = "SKU")
    private String sku;

    @Column(name = "CANT_TOTAL")
    private Long cantTotal;

    @Column(name = "IND_INTERVALO_TOTAL")
    private Character indIntervaloTotal;

    @Column(name = "CANT_TOTAL_MIEMBRO")
    private Long cantTotalMiembro;

    @Column(name = "IND_INTERVALO_MIEMBRO")
    private Character indIntervaloMiembro;

    @Column(name = "IND_INTERVALO_RESPUESTA")
    private Character indIntervaloRespuesta;
    
    @Column(name = "CANT_INTERVALO_RESPUESTA")
    private Long cantIntervaloRespuesta;

    @Column(name = "CANT_MIN_ACUMULADO")
    private Double cantMinAcumulado;

    @Column(name = "EFECTIVIDAD_IND_CALENDARIZADO")
    private Character efectividadIndCalendarizado;

    @Column(name = "EFECTIVIDAD_FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date efectividadFechaInicio;

    @Column(name = "EFECTIVIDAD_FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date efectividadFechaFin;

    @Column(name = "EFECTIVIDAD_IND_DIAS")
    private String efectividadIndDias;

    @Column(name = "EFECTIVIDAD_IND_SEMANA")
    private Integer efectividadIndSemana;
    
    @Column(name = "IND_ENVIO")
    private Boolean indEnvio;

    @Column(name = "IND_RESPUESTA")
    private Boolean indRespuesta;

    @Column(name = "PRECIO_PROMEDIO")
    private Double precioPromedio;

    @Column(name = "COD_PREMIO")
    private String codPremio;

    @Column(name = "TOTAL_EXISTENCIAS")
    private Long totalExistencias;
    
    @Column(name = "CANT_DIAS_VENCIMIENTO")
    private Integer cantDiasVencimiento;

    public Premio() {
    }

    public String getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(String idPremio) {
        this.idPremio = idPremio;
    }

    public Character getIndTipoPremio() {
        return indTipoPremio;
    }

    public void setIndTipoPremio(Character indTipoPremio) {
        this.indTipoPremio = indTipoPremio == null ? null : Character.toUpperCase(indTipoPremio);
    }

    public Boolean getIndEnvio() {
        return indEnvio;
    }

    public void setIndEnvio(Boolean indEnvio) {
        this.indEnvio = indEnvio;
    }

    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Date getFechaArchivado() {
        return fechaArchivado;
    }

    public void setFechaArchivado(Date fechaArchivado) {
        this.fechaArchivado = fechaArchivado;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }
    
    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Boolean getIndRespuesta() {
        return indRespuesta;
    }

    public void setIndRespuesta(Boolean indRespuesta) {
        this.indRespuesta = indRespuesta;
    }

    public Long getCantTotal() {
        return cantTotal;
    }

    public void setCantTotal(Long cantTotal) {
        this.cantTotal = cantTotal;
    }

    public Character getIndIntervaloTotal() {
        return indIntervaloTotal;
    }

    public void setIndIntervaloTotal(Character indIntervaloTotal) {
        this.indIntervaloTotal = indIntervaloTotal;
    }

    public Long getCantTotalMiembro() {
        return cantTotalMiembro;
    }

    public void setCantTotalMiembro(Long cantTotalMiembro) {
        this.cantTotalMiembro = cantTotalMiembro;
    }

    public Character getIndIntervaloMiembro() {
        return indIntervaloMiembro;
    }

    public void setIndIntervaloMiembro(Character indIntervaloMiembro) {
        this.indIntervaloMiembro = indIntervaloMiembro;
    }

    public Character getIndIntervaloRespuesta() {
        return indIntervaloRespuesta;
    }

    public void setIndIntervaloRespuesta(Character indIntervaloRespuesta) {
        this.indIntervaloRespuesta = indIntervaloRespuesta;
    }

    public Double getCantMinAcumulado() {
        return cantMinAcumulado;
    }

    public void setCantMinAcumulado(Double cantMinAcumulado) {
        this.cantMinAcumulado = cantMinAcumulado;
    }

    public Character getEfectividadIndCalendarizado() {
        return efectividadIndCalendarizado;
    }

    public void setEfectividadIndCalendarizado(Character efectividadIndCalendarizado) {
        this.efectividadIndCalendarizado = efectividadIndCalendarizado == null ? null : Character.toUpperCase(efectividadIndCalendarizado);
    }

    public Date getEfectividadFechaInicio() {
        return efectividadFechaInicio;
    }

    public void setEfectividadFechaInicio(Date efectividadFechaInicio) {
        this.efectividadFechaInicio = efectividadFechaInicio;
    }

    public Date getEfectividadFechaFin() {
        return efectividadFechaFin;
    }

    public void setEfectividadFechaFin(Date efectividadFechaFin) {
        this.efectividadFechaFin = efectividadFechaFin;
    }

    public String getEfectividadIndDias() {
        return efectividadIndDias;
    }

    public void setEfectividadIndDias(String efectividadIndDias) {
        this.efectividadIndDias = efectividadIndDias;
    }

    public Integer getEfectividadIndSemana() {
        return efectividadIndSemana;
    }

    public void setEfectividadIndSemana(Integer efectividadIndSemana) {
        this.efectividadIndSemana = efectividadIndSemana;
    }

    public Long getCantIntervaloRespuesta() {
        return cantIntervaloRespuesta;
    }

    public void setCantIntervaloRespuesta(Long cantIntervaloRespuesta) {
        this.cantIntervaloRespuesta = cantIntervaloRespuesta;
    }

    public String getCodPremio() {
        return codPremio;
    }

    public void setCodPremio(String codPremio) {
        this.codPremio = codPremio;
    }

    public Double getPrecioPromedio() {
        return precioPromedio;
    }

    public void setPrecioPromedio(Double precioPromedio) {
        this.precioPromedio = precioPromedio;
    }

    public Long getTotalExistencias() {
        return totalExistencias;
    }

    public void setTotalExistencias(Long totalExistencias) {
        this.totalExistencias = totalExistencias;
    }

    public Integer getCantDiasVencimiento() {
        return cantDiasVencimiento;
    }

    public void setCantDiasVencimiento(Integer cantDiasVencimiento) {
        this.cantDiasVencimiento = cantDiasVencimiento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPremio != null ? idPremio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Premio)) {
            return false;
        }
        Premio other = (Premio) object;
        if ((this.idPremio == null && other.idPremio != null) || (this.idPremio != null && !this.idPremio.equals(other.idPremio))) {
            return false;
        }
        return true;
    }

}
