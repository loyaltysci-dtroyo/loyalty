package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.AtributoDinamico;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.MemberEntry;
import com.flecharoja.loyalty.model.MiembroAtributo;
import com.flecharoja.loyalty.model.MiembroAtributoPK;
import com.flecharoja.loyalty.model.PeticionEnvioNotificacionCustom;
import com.flecharoja.loyalty.model.Pais;
import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.util.EnvioNotificacion;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author svargas
 */
@Stateless
public class MiembroService {
    
    @EJB
    MyKafkaUtils myKafkaUtils;
    
    private enum CamposFiltro {
        NOMBRE_USUARIO("username"),
        CORREO_ELECTRONICO("email"),
        UUID("internal-id");
        
        private final String value;
        private static final Map<String, CamposFiltro> lookup =  new HashMap<>();

        private CamposFiltro(String value) {
            this.value = value;
        }
        
        static {
            for (CamposFiltro genero : values()) {
                lookup.put(genero.value, genero);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static CamposFiltro get(String value) {
            return value==null?null:lookup.get(value.toLowerCase());
        }
    }

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @EJB
    private AtributoDinamicoBean atributoDinamicoBean;
    
    @EJB
    private RegistroMetricaService registroMetricaService;
    
    public MemberEntry registrarMiembro(JsonObject data, String enable, Locale locale) {
        if (data==null) {
            throw new MyException(MyException.ErrorPeticion.CUERPO_PETICION_INVALIDO, locale, data, "data_registration_not_send");
        }
        
        MemberEntry memberEntry = new MemberEntry(true);
        
        Date fecha = new Date();
        memberEntry.setFechaCreacion(fecha);
        memberEntry.setFechaModificacion(fecha);
        
        if (enable==null) {
            memberEntry.setIndEstadoMiembro(MemberEntry.Estados.INACTIVO.getValue());
        } else {
            switch(enable) {
                case "0": {
                    memberEntry.setIndEstadoMiembro(MemberEntry.Estados.INACTIVO.getValue());
                    memberEntry.setDisableDate(fecha);
                    break;
                }
                case "1": {
                    memberEntry.setIndEstadoMiembro(MemberEntry.Estados.ACTIVO.getValue());
                    break;
                }
                default: {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTO_PETICION_INVALIDO, locale, data, "query_parameter_invalid", "enable");
                }
            }
        }
        
        try {
            if (((long)em.createNamedQuery("Miembro.countByIdentificationDoc").setParameter("identificationDoc", data.getString("identificationDoc")).getSingleResult())>0) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "member_identification_doc_exists");
            }
            memberEntry.setIdentificationDoc(data.getString("identificationDoc"));
        } catch (ClassCastException | NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "identificationDoc");
        }
        
        try {
            memberEntry.setName(data.getString("name"));
        } catch (ClassCastException | NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "name");
        }
        
        try {
            memberEntry.setLastName1(data.getString("lastName1"));
        } catch (ClassCastException | NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "lastName1");
        }
        
        try {
            if (((long)em.createNamedQuery("Miembro.countByEmail").setParameter("email", data.getString("email")).getSingleResult())>0) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "member_email_exists");
            }
            memberEntry.setEmail(data.getString("email"));
        } catch (ClassCastException | NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "email");
        }
        
        try {
            memberEntry.setAvatar(new URL(data.getString("avatar")).toURI().toString());
        } catch (MalformedURLException | URISyntaxException | ClassCastException | NullPointerException e) {
            memberEntry.setAvatar(Indicadores.URL_IMAGEN_PREDETERMINADA);
        }
        
        try {
            memberEntry.setAdmissionDate(new Date(data.getJsonNumber("admissionDate").longValue()));
        } catch (ClassCastException | NullPointerException e) {
            memberEntry.setAdmissionDate(fecha);
        }
        
        Map<String, MiembroAtributo> map = new HashMap<>();
        
        data.entrySet().stream().distinct().forEach((atributo) -> {
            if (atributo.getValue().getValueType()==JsonValue.ValueType.ARRAY || atributo.getValue().getValueType()==JsonValue.ValueType.OBJECT) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
            }
            switch(atributo.getKey()) {
                case "disableCause":
                case "disableDate":
                case "identificationDoc":
                case "name":
                case "lastName1":
                case "email":
                case "avatar":
                case "username":
                case "admissionDate":
                case "password": {
                    //control externo
                    break;
                }
                case "expirationDate": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    Date date = new Date(((JsonNumber) atributo.getValue()).longValue());
                    memberEntry.setExpirationDate(date);
                    break;
                }
                case "residentAddress": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setResidentAddress(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "residentCity": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setResidentCity(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "residentState": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setResidentState(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "residentCountryCode": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    Pais pais;
                    try {
                        pais = (Pais) em.createNamedQuery("Pais.findByAlfa3").setParameter("alfa3", ((JsonString) atributo.getValue()).getString()).getSingleResult();
                    } catch (NoResultException | NonUniqueResultException ex) {
                        try {
                            pais = (Pais) em.createNamedQuery("Pais.findByAlfa2").setParameter("alfa2", ((JsonString) atributo.getValue()).getString()).getSingleResult();
                        } catch (NoResultException | NonUniqueResultException e) {
                            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "country_indicator_invalid");
                        }
                    }
                    memberEntry.setResidentCountryCode(pais.getAlfa3());
                    break;
                }
                case "residentZipCode": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setResidentZipCode(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "mobileNumber": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setMobileNumber(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "birthDate": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    Date date = new Date(((JsonNumber) atributo.getValue()).longValue());
                    memberEntry.setBirthDate(date);
                    break;
                }
                case "indGender": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    MemberEntry.ValoresIndGenero genero = MemberEntry.ValoresIndGenero.get(((JsonString) atributo.getValue()).getString().toUpperCase().charAt(0));
                    if (genero==null) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndGender(genero.getValue());
                    break;
                }
                case "indCivilStatus": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    MemberEntry.ValoresIndEstadoCivil estadoCivil = MemberEntry.ValoresIndEstadoCivil.get(((JsonString) atributo.getValue()).getString().toUpperCase().charAt(0));
                    if (estadoCivil==null) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndCivilStatus(estadoCivil.getValue());
                    break;
                }
                case "indEducationType": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    MemberEntry.ValoresIndEducacion educacion = MemberEntry.ValoresIndEducacion.get(((JsonString) atributo.getValue()).getString().toUpperCase().charAt(0));
                    if (educacion==null) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndEducationType(educacion.getValue());
                    break;
                }
                case "incomeAmount": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIncomeAmount(((JsonNumber) atributo.getValue()).longValue());
                    break;
                }
                case "commentaries": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setCommentaries(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "lastName2": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setLastName2(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "indContactByEmail": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.TRUE && atributo.getValue().getValueType()!=JsonValue.ValueType.FALSE) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndContactByEmail(atributo.getValue().getValueType()==JsonValue.ValueType.TRUE);
                    break;
                }
                case "indContactBySms": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.TRUE && atributo.getValue().getValueType()!=JsonValue.ValueType.FALSE) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndContactBySms(atributo.getValue().getValueType()==JsonValue.ValueType.TRUE);
                    break;
                }
                case "indContactByPushNotification": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.TRUE && atributo.getValue().getValueType()!=JsonValue.ValueType.FALSE) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndContactByPushNotification(atributo.getValue().getValueType()==JsonValue.ValueType.TRUE);
                    break;
                }
                case "indChildren": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.TRUE && atributo.getValue().getValueType()!=JsonValue.ValueType.FALSE) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndChildren(atributo.getValue().getValueType()==JsonValue.ValueType.TRUE);
                    break;
                }
                default: {
                    List<AtributoDinamico> list = em.createNamedQuery("AtributoDinamico.findByNombreInterno").setParameter("nombreInterno", atributo.getKey()).getResultList();
                    AtributoDinamico atributoDinamico;
                    if (list.isEmpty()) {
                        atributoDinamico = new AtributoDinamico();
                        atributoDinamico.setNombre(atributo.getKey());
                        atributoDinamico.setNombreInterno(atributo.getKey());
                        atributoDinamico.setFechaCreacion(new Date());
                        atributoDinamico.setFechaModificacion(new Date());
                        atributoDinamico.setIndEstado(AtributoDinamico.Estados.PUBLICADO.getValue());
                        atributoDinamico.setIndRequerido(false);
                        atributoDinamico.setIndVisible(true);
                        atributoDinamico.setNumVersion(1L);
                        switch(atributo.getValue().getValueType()) {
                            case FALSE:
                            case TRUE: {
                                atributoDinamico.setIndTipoDato(AtributoDinamico.TiposDato.BOOLEANO.getValue());
                                atributoDinamico.setValorDefecto("false");
                                break;
                            }
                            case NUMBER: {
                                atributoDinamico.setIndTipoDato(AtributoDinamico.TiposDato.NUMERICO.getValue());
                                atributoDinamico.setValorDefecto("0");
                                break;
                            }
                            case STRING: {
                                atributoDinamico.setIndTipoDato(AtributoDinamico.TiposDato.TEXTO.getValue());
                                atributoDinamico.setValorDefecto("null");
                                break;
                            }
                            default: {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                        }
                        atributoDinamico.setIdAtributo(atributoDinamicoBean.insertAtributoDinamicoMiembro(atributoDinamico));
                    } else {
                        atributoDinamico = list.get(0);
                    }
                    
                    AtributoDinamico.TiposDato td = AtributoDinamico.TiposDato.get(atributoDinamico.getIndTipoDato());
                    if (td==null) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    switch(td) {
                        case BOOLEANO: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.TRUE && atributo.getValue().getValueType()!=JsonValue.ValueType.FALSE) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            map.put(atributoDinamico.getIdAtributo(), new MiembroAtributo(new Date(), atributo.getValue().getValueType()==JsonValue.ValueType.TRUE?"true":"false"));
                            break;
                        }
                        case FECHA: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            Date date = new Date(((JsonNumber) atributo.getValue()).longValue());
                            map.put(atributoDinamico.getIdAtributo(), new MiembroAtributo(new Date(), ""+date.getTime()));
                            break;
                        }
                        case NUMERICO: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            map.put(atributoDinamico.getIdAtributo(), new MiembroAtributo(new Date(), ((JsonNumber) atributo.getValue()).bigDecimalValue().toPlainString()));
                            break;
                        }
                        case TEXTO: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            map.put(atributoDinamico.getIdAtributo(), new MiembroAtributo(new Date(), ((JsonString) atributo.getValue()).getString()));
                            break;
                        }
                    }
                }
            }
        });
        
        String username;
        String password;
        try {
            username = data.getString("username");
            password = data.getString("password");
        } catch (ClassCastException | NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "username, password");
        }
        
        if (!validatePassword(password)) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "password");
        }
        
        String idMember;
        try(MyKeycloakUtils keycloakUtils = new MyKeycloakUtils()) {
            idMember = keycloakUtils.createMember(username, password, memberEntry.getEmail());
        } catch (Exception e) {
            if (e instanceof MyException) {
                throw e;
            } else {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, data, "register_to_idp_failed");
            }
        }
        if (idMember==null) {
            throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, data, "register_to_idp_failed");
        }
        memberEntry.setInternalMemberId(idMember);
        memberEntry.setUsername(username);
        memberEntry.setPassword(password);
        
        memberEntry.setIndCambioPass(true);
        
        memberEntry.setIndContactByEmail(memberEntry.getIndContactByEmail()==null?true:memberEntry.getIndContactByEmail());
        memberEntry.setIndContactByPushNotification(memberEntry.getIndContactByPushNotification()==null?true:memberEntry.getIndContactByPushNotification());
        memberEntry.setIndContactBySms(memberEntry.getIndContactBySms()==null?true:memberEntry.getIndContactBySms());
        
        em.persist(memberEntry);
        
        map.entrySet().forEach((entry) -> {
            MiembroAtributo miembroAtributo = entry.getValue();
            miembroAtributo.setMiembroAtributoPK(new MiembroAtributoPK(idMember, entry.getKey()));
            em.merge(miembroAtributo);
        });
        try {
            em.flush();
        } catch (Exception e) {
            try(MyKeycloakUtils keycloakUtils = new MyKeycloakUtils()) {
                keycloakUtils.deleteMember(idMember);
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, data, "could_not_be_persisted");
            }
        }
        
        return memberEntry;
    }
    
    @Asynchronous
    public void postRegistroMiembro(MemberEntry memberEntry) {
        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.NUEVO_MIEMBRO, null, memberEntry.getInternalMemberId());
        } catch (Exception ex) {
            Logger.getLogger(MiembroService.class.getName()).log(Level.WARNING, null, ex);
        }
        
        ConfiguracionGeneral config = em.find(ConfiguracionGeneral.class, 0L);
        
        if (config.getBonoEntrada()!=null && config.getIdMetricaPrincipal()!=null && config.getBonoEntrada().compareTo(0d)>0) {
            RegistroMetrica registroMetrica = new RegistroMetrica(new Date(), config.getIdMetricaPrincipal(), memberEntry.getInternalMemberId(), RegistroMetrica.TiposGane.BIENVENIDA, config.getBonoEntrada(), null);
            
            try {
                registroMetricaService.insertRegistroMetrica(registroMetrica);
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
        }
        
        PeticionEnvioNotificacionCustom peticionEnvioNotificacion = new PeticionEnvioNotificacionCustom();
        ResourceBundle customNotifications;
        try {
            customNotifications = ResourceBundle.getBundle("custom_notifications.welcome_credentials", Locale.US);
        } catch (NullPointerException | MissingResourceException e) {
            customNotifications = ResourceBundle.getBundle("custom_notifications.welcome_credentials", Locale.US);
        }
        peticionEnvioNotificacion.setCuerpo(customNotifications.getString("body"));
        peticionEnvioNotificacion.setTitulo(customNotifications.getString("subject"));
        peticionEnvioNotificacion.getMiembros().add(memberEntry.getInternalMemberId());
        peticionEnvioNotificacion.getParametros().put("username", memberEntry.getUsername());
        peticionEnvioNotificacion.getParametros().put("password", memberEntry.getPassword());
        peticionEnvioNotificacion.setTipoMsj(PeticionEnvioNotificacionCustom.Tipos.EMAIL.getValue());
        
        new EnvioNotificacion().enviarNotificacion(peticionEnvioNotificacion);
    }
    
    public void editarMiembro(JsonObject data, String field, String id, Locale locale) {
        if (data==null) {
            throw new MyException(MyException.ErrorPeticion.CUERPO_PETICION_INVALIDO, locale, data, "data_registration_not_send");
        }
        
        MemberEntry memberEntry = searchMember(field, id, locale);
        
        boolean changeEmail = false;
        data.entrySet().stream().distinct().forEach((atributo) -> {
            if (atributo.getValue().getValueType()==JsonValue.ValueType.ARRAY || atributo.getValue().getValueType()==JsonValue.ValueType.OBJECT) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
            }
            switch(atributo.getKey()) {
                case "email":
                case "disableCause":
                case "disableDate":
                case "admissionDate":
                case "username":
                case "password": {
                    //control externo
                    break;
                }
                case "internalMemberId": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    //no se puede alterar el internal id de miembro
                    if (!((JsonString) atributo.getValue()).getString().equalsIgnoreCase(memberEntry.getInternalMemberId())) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    break;
                }
                case "identificationDoc": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    if (!memberEntry.getIdentificationDoc().equals(((JsonString) atributo.getValue()).getString())) {
                        if (((long)em.createNamedQuery("Miembro.countByIdentificationDoc").setParameter("identificationDoc", ((JsonString) atributo.getValue()).getString()).getSingleResult())>0) {
                            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "member_identification_doc_exists");
                        }
                        memberEntry.setIdentificationDoc(((JsonString) atributo.getValue()).getString());
                    }
                    break;
                }
                case "name": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setName(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "lastName1": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setLastName1(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "avatar": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    try {
                        memberEntry.setAvatar(new URL(data.getString("avatar")).toURI().toString());
                    } catch (MalformedURLException | URISyntaxException | ClassCastException | NullPointerException e) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    break;
                }
                case "expirationDate": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    Date date = new Date(((JsonNumber) atributo.getValue()).longValue());
                    memberEntry.setExpirationDate(date);
                    break;
                }
                case "residentAddress": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setResidentAddress(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "residentCity": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setResidentCity(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "residentState": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setResidentState(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "residentCountryCode": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    Pais pais;
                    try {
                        pais = (Pais) em.createNamedQuery("Pais.findByAlfa3").setParameter("alfa3", ((JsonString) atributo.getValue()).getString()).getSingleResult();
                    } catch (NoResultException | NonUniqueResultException ex) {
                        try {
                            pais = (Pais) em.createNamedQuery("Pais.findByAlfa2").setParameter("alfa2", ((JsonString) atributo.getValue()).getString()).getSingleResult();
                        } catch (NoResultException | NonUniqueResultException e) {
                            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "country_indicator_invalid");
                        }
                    }
                    memberEntry.setResidentCountryCode(pais.getAlfa3());
                    break;
                }
                case "residentZipCode": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setResidentZipCode(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "mobileNumber": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setMobileNumber(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "birthDate": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    Date date = new Date(((JsonNumber) atributo.getValue()).longValue());
                    memberEntry.setBirthDate(date);
                    break;
                }
                case "indGender": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    MemberEntry.ValoresIndGenero genero = MemberEntry.ValoresIndGenero.get(((JsonString) atributo.getValue()).getString().toUpperCase().charAt(0));
                    if (genero==null) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndGender(genero.getValue());
                    break;
                }
                case "indCivilStatus": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    MemberEntry.ValoresIndEstadoCivil estadoCivil = MemberEntry.ValoresIndEstadoCivil.get(((JsonString) atributo.getValue()).getString().toUpperCase().charAt(0));
                    if (estadoCivil==null) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndCivilStatus(estadoCivil.getValue());
                    break;
                }
                case "indEducationType": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    MemberEntry.ValoresIndEducacion educacion = MemberEntry.ValoresIndEducacion.get(((JsonString) atributo.getValue()).getString().toUpperCase().charAt(0));
                    if (educacion==null) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndEducationType(educacion.getValue());
                    break;
                }
                case "incomeAmount": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIncomeAmount(((JsonNumber) atributo.getValue()).longValue());
                    break;
                }
                case "commentaries": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setCommentaries(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "lastName2": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setLastName2(((JsonString) atributo.getValue()).getString());
                    break;
                }
                case "indContactByEmail": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.TRUE && atributo.getValue().getValueType()!=JsonValue.ValueType.FALSE) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndContactByEmail(atributo.getValue().getValueType()==JsonValue.ValueType.TRUE);
                    break;
                }
                case "indContactBySms": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.TRUE && atributo.getValue().getValueType()!=JsonValue.ValueType.FALSE) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndContactBySms(atributo.getValue().getValueType()==JsonValue.ValueType.TRUE);
                    break;
                }
                case "indContactByPushNotification": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.TRUE && atributo.getValue().getValueType()!=JsonValue.ValueType.FALSE) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndContactByPushNotification(atributo.getValue().getValueType()==JsonValue.ValueType.TRUE);
                    break;
                }
                case "indChildren": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.TRUE && atributo.getValue().getValueType()!=JsonValue.ValueType.FALSE) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    memberEntry.setIndChildren(atributo.getValue().getValueType()==JsonValue.ValueType.TRUE);
                    break;
                }
                default: {
                    List<AtributoDinamico> list = em.createNamedQuery("AtributoDinamico.findByNombreInterno").setParameter("nombreInterno", atributo.getKey()).getResultList();
                    AtributoDinamico atributoDinamico;
                    if (list.isEmpty()) {
                        atributoDinamico = new AtributoDinamico();
                        atributoDinamico.setNombre(atributo.getKey());
                        atributoDinamico.setNombreInterno(atributo.getKey());
                        atributoDinamico.setFechaCreacion(new Date());
                        atributoDinamico.setFechaModificacion(new Date());
                        atributoDinamico.setIndEstado(AtributoDinamico.Estados.PUBLICADO.getValue());
                        atributoDinamico.setIndRequerido(false);
                        atributoDinamico.setIndVisible(true);
                        atributoDinamico.setNumVersion(1L);
                        switch(atributo.getValue().getValueType()) {
                            case FALSE:
                            case TRUE: {
                                atributoDinamico.setIndTipoDato(AtributoDinamico.TiposDato.BOOLEANO.getValue());
                                atributoDinamico.setValorDefecto("false");
                                break;
                            }
                            case NUMBER: {
                                atributoDinamico.setIndTipoDato(AtributoDinamico.TiposDato.NUMERICO.getValue());
                                atributoDinamico.setValorDefecto("0");
                                break;
                            }
                            case STRING: {
                                atributoDinamico.setIndTipoDato(AtributoDinamico.TiposDato.TEXTO.getValue());
                                atributoDinamico.setValorDefecto("null");
                                break;
                            }
                            default: {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                        }
                        atributoDinamico.setIdAtributo(atributoDinamicoBean.insertAtributoDinamicoMiembro(atributoDinamico));
                    } else {
                        atributoDinamico = list.get(0);
                    }
                    
                    AtributoDinamico.TiposDato td = AtributoDinamico.TiposDato.get(atributoDinamico.getIndTipoDato());
                    if (td==null) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    switch(td) {
                        case BOOLEANO: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.TRUE && atributo.getValue().getValueType()!=JsonValue.ValueType.FALSE) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            em.merge(new MiembroAtributo(memberEntry.getInternalMemberId(), atributoDinamico.getIdAtributo(), new Date(), atributo.getValue().getValueType()==JsonValue.ValueType.TRUE?"true":"false"));
                            break;
                        }
                        case FECHA: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            Date date = new Date(((JsonNumber) atributo.getValue()).longValue());
                            em.merge(new MiembroAtributo(memberEntry.getInternalMemberId(), atributoDinamico.getIdAtributo(), new Date(), ""+date.getTime()));
                            break;
                        }
                        case NUMERICO: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            em.merge(new MiembroAtributo(memberEntry.getInternalMemberId(), atributoDinamico.getIdAtributo(), new Date(), ((JsonNumber) atributo.getValue()).bigDecimalValue().toPlainString()));
                            break;
                        }
                        case TEXTO: {
                            if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                            }
                            em.merge(new MiembroAtributo(memberEntry.getInternalMemberId(), atributoDinamico.getIdAtributo(), new Date(), ((JsonString) atributo.getValue()).getString()));
                            break;
                        }
                    }
                }
            }
        });
        
        //no se puede cambiar el username, por lo que si esta presente el valor, se comprueba que sea el mismo del miembro encontrado
        if (data.containsKey("username")) {
            String username;
            try {
                username = data.getString("username");
            } catch (ClassCastException e) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "username, password");
            }
            switch (CamposFiltro.get(field)) {
                case NOMBRE_USUARIO: {
                    if (!username.equalsIgnoreCase(id)) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "username, password");
                    }
                    break;
                }
                default: {
                    try(MyKeycloakUtils keycloakUtils = new MyKeycloakUtils()) {
                        if (!username.equalsIgnoreCase(keycloakUtils.getUsernameMember(memberEntry.getInternalMemberId()))) {
                            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "username, password");
                        }
                    }
                }
            }
        }
        
        if (data.containsKey("email")) {
            String email;
            try {
                email = data.getString("email");
                
            } catch (ClassCastException e) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "email");
            }
            if (!email.equals(memberEntry.getEmail())) {
                if (((long)em.createNamedQuery("Miembro.countByEmail").setParameter("email", email).getSingleResult())>0) {
                    throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "member_email_exists");
                }
                try(MyKeycloakUtils keycloakUtils = new MyKeycloakUtils()) {
                    keycloakUtils.changeEmailMember(id, email);
                }
                memberEntry.setEmail(email);
            }
        }
        
        if (data.containsKey("password")) {
            String password;
            try {
                password = data.getString("password");
            } catch (ClassCastException e) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "username, password");
            }
            
            if (!validatePassword(password)) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "password");
            }
            
            try(MyKeycloakUtils keycloakUtils = new MyKeycloakUtils()) {
                keycloakUtils.resetPasswordMember(memberEntry.getInternalMemberId(), password);
            }
            memberEntry.setIndCambioPass(true);
        }
        
        memberEntry.setFechaModificacion(new Date());
        
        em.merge(memberEntry);
        try {
            em.flush();
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        } catch (Exception e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, data, "could_not_be_persisted");
        }
        
        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.ACTUALIZACION_PERFIL, null, memberEntry.getInternalMemberId());
        } catch (Exception ex) {
            Logger.getLogger(MiembroService.class.getName()).log(Level.WARNING, null, ex);
        }
    }
    
    public JsonObject obtenerMiembro(String field, String id, Locale locale) {
        MemberEntry memberEntry = searchMember(field, id, locale);
        
        JsonObjectBuilder builder = Json.createObjectBuilder();
        
        try {
            BeanUtils.describe(memberEntry).forEach((k, v) -> {
                String key = (String) k;
                String value = (String) v;
                switch(key) {
                    case "disableCause":
                    case "indEducationType":
                    case "indCivilStatus":
                    case "indGender":
                    case "lastName2":
                    case "commentaries":
                    case "mobileNumber":
                    case "residentZipCode":
                    case "residentCountryCode":
                    case "residentState":
                    case "residentCity":
                    case "residentAddress":
                    case "avatar":
                    case "email":
                    case "lastName1":
                    case "name":
                    case "internalMemberId":
                    case "identificationDoc": {
                        if (value!=null) {
                            builder.add(key, value);
                        } else {
                            builder.addNull(key);
                        }
                        break;
                    }
                    case "incomeAmount": {
                        if (value!=null) {
                            builder.add(key, Long.parseLong(value));
                        } else {
                            builder.addNull(key);
                        }
                        break;
                    }
                    case "disableDate":
                    case "admissionDate":
                    case "birthDate":
                    case "expirationDate": {
                        if (value!=null) {
                            try {
                                builder.add(key, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(value).getTime());
                            } catch (ParseException ex) {
                                break;
                            }
                        } else {
                            builder.addNull(key);
                        }
                        break;
                    }
                    case "indChildren":
                    case "indContactByPushNotification":
                    case "indContactBySms":
                    case "indContactByEmail": {
                        if (value!=null) {
                            builder.add(key, value.equalsIgnoreCase("true"));
                        } else {
                            builder.addNull(key);
                        }
                        break;
                    }
                }
            });
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, null, "can_not_form_json");
        }
        
        if (field.equalsIgnoreCase(CamposFiltro.NOMBRE_USUARIO.value)) {
            builder.add("username", id);
        } else {
            try(MyKeycloakUtils keycloakUtils = new MyKeycloakUtils()) {
                builder.add("username", keycloakUtils.getUsernameMember(memberEntry.getInternalMemberId()));
            }
        }
        
        List<MiembroAtributo> miembroAtributos = em.createNamedQuery("MiembroAtributo.findByIdMiembro").setParameter("idMiembro", memberEntry.getInternalMemberId()).getResultList();
        miembroAtributos.forEach((miembroAtributo) -> {
            AtributoDinamico atributoDinamico = em.find(AtributoDinamico.class, miembroAtributo.getMiembroAtributoPK().getIdAtributo());
            if (atributoDinamico!=null && atributoDinamico.getNombreInterno()!=null) {
                AtributoDinamico.TiposDato tipoDato = AtributoDinamico.TiposDato.get(atributoDinamico.getIndTipoDato());
                if (tipoDato!=null) {
                    try {
                        switch (tipoDato) {
                            case BOOLEANO: {
                                builder.add(atributoDinamico.getNombreInterno(), miembroAtributo.getValor().equalsIgnoreCase("true"));
                                break;
                            }
                            case NUMERICO: {
                                builder.add(atributoDinamico.getNombreInterno(), Double.parseDouble(miembroAtributo.getValor()));
                                break;
                            }
                            case FECHA : {
                                builder.add(atributoDinamico.getNombreInterno(), Long.parseLong(miembroAtributo.getValor()));
                                break;
                            }
                            case TEXTO: {
                                builder.add(atributoDinamico.getNombreInterno(), miembroAtributo.getValor());
                                break;
                            }
                        }
                    } catch (NumberFormatException e) {
                        builder.add(atributoDinamico.getNombreInterno(), miembroAtributo.getValor());
                    } catch (NullPointerException e) {
                        builder.addNull(atributoDinamico.getNombreInterno());
                    }
                }
            }
        });
        
        return builder.build();
    }
    
    public void cambioEstadoMiembro(String field, String id, String action, JsonObject data, Locale locale) {
        if (data==null) {
            throw new MyException(MyException.ErrorPeticion.CUERPO_PETICION_INVALIDO, locale, data, "data_registration_not_send");
        }
        switch(action.toUpperCase()) {
            case "ENABLE": {
                MemberEntry memberEntry = searchMember(field, id, locale);
                
                memberEntry.setIndEstadoMiembro(MemberEntry.Estados.ACTIVO.getValue());
                memberEntry.setExpirationDate(null);
                memberEntry.setDisableCause(null);
                memberEntry.setDisableDate(null);
                
                em.merge(memberEntry);
                try {
                    em.flush();
                } catch (Exception e) {
                    throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, data, "could_not_be_persisted");
                }
                
                try(MyKeycloakUtils keycloakUtils = new MyKeycloakUtils()) {
                    keycloakUtils.enableDisableMember(memberEntry.getInternalMemberId(), true);
                }
                break;
            }
            case "DISABLE": {
                if (!data.containsKey("disableCause") || data.get("disableCause").getValueType()!=JsonValue.ValueType.STRING) {
                    throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_invalid", "disableCause");
                }
                String causaInhabilitacion = data.getString("disableCause");
                MemberEntry memberEntry = searchMember(field, id, locale);
                
                Date fecha = new Date();
                
                memberEntry.setExpirationDate(fecha);
                memberEntry.setDisableCause(causaInhabilitacion);
                memberEntry.setDisableDate(fecha);
                
                em.merge(memberEntry);
                try {
                    em.flush();
                } catch (Exception e) {
                    throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, data, "could_not_be_persisted");
                }
                
                try(MyKeycloakUtils keycloakUtils = new MyKeycloakUtils()) {
                    keycloakUtils.enableDisableMember(memberEntry.getInternalMemberId(), false);
                }
                break;
            }
        }
    }
    
    private MemberEntry searchMember(String field, String id, Locale locale) {
        CamposFiltro filtro = CamposFiltro.get(field);
        if (filtro==null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTO_PETICION_INVALIDO, locale, null, "query_parameter_invalid", "enable");
        }
        MemberEntry memberEntry;
        switch (filtro) {
            case CORREO_ELECTRONICO: {
                try {
                    memberEntry = (MemberEntry) em.createNamedQuery("Miembro.findByEmail").setParameter("email", id).getSingleResult();
                } catch (NoResultException e) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, null, "member_not_found");
                }
                break;
            }
            case NOMBRE_USUARIO: {
                String idMember;
                try(MyKeycloakUtils keycloakUtils = new MyKeycloakUtils()) {
                    idMember = keycloakUtils.getIdMember(id);
                }
                if (idMember==null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, null, "member_not_found");
                }
                memberEntry = em.find(MemberEntry.class, idMember);
                break;
            }
            case UUID: {
                memberEntry = em.find(MemberEntry.class, id);
                break;
            }
            default: {
                memberEntry = null;
            }
        }
        if (memberEntry==null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, null, "member_not_found");
        }
        
        return memberEntry;
    }
    
    public boolean validatePassword(String password) {
        Pattern pswNamePtrn
                = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=.,_]).{6,100})");
        Matcher mtch = pswNamePtrn.matcher(password);
        return mtch.matches();
    }
}
