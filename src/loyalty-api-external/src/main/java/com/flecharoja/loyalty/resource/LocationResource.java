package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.LocationEntry;
import com.flecharoja.loyalty.model.RespuestaRegistroEntidad;
import com.flecharoja.loyalty.patch.PATCH;
import com.flecharoja.loyalty.service.UbicacionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ejb.EJB;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author svargas
 */
@Api(value = "Locations")
@Path("location")
public class LocationResource {
    
    @Context
    HttpServletRequest request;
    
    @EJB
    UbicacionService bean;
    
    @ApiOperation(value = "Register a new location entry", response = RespuestaRegistroEntidad.class,
            notes = "The value of the query param \"enable\" can be 0 (the entry will be register with status disable) or 1 (the entry will be register with status enable)")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "data", value = "Location data to register", required = true, dataType = "com.flecharoja.loyalty.model.LocationEntry", paramType = "body"),
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaRegistroEntidad registerLocation(JsonObject data,
            @ApiParam(value = "Indicator for enabling/disabling the location entry", required = true, defaultValue = "0") @QueryParam("enable") String enable) {
        return bean.registrarUbicacion(data, enable, request.getLocale());
    }

    @ApiOperation(value = "Patch an existing location entry",
            notes = "The path param \"field\" can have the following values:\n-\"cod-location\" (code of the location)\n-\"internal-name\" (internal name of the location)\n-\"internal-id\" (internal id of the location)")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "data", value = "Location data to patch", required = true, dataType = "com.flecharoja.loyalty.model.LocationEntry", paramType = "body"),
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("by-{field}/{id}")
    public void patchLocation(JsonObject data,
            @ApiParam(value = "Key field to search the location entry", required = true) @PathParam("field") String field,
            @ApiParam(value = "Field value for the location entry", required = true) @PathParam("id") String id) {
        bean.editarUbicacion(data, field, id, request.getLocale());
    }
    
    @ApiOperation(value = "Get an existing location entry", response = LocationEntry.class,
            notes = "The path param \"field\" can have the following values:\n-\"internal-code\" (code of the location)\n-\"internal-name\" (internal name of the location)\n-\"internal-id\" (internal id of the location)")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("by-{field}/{id}")
    public LocationEntry getLocation(
            @ApiParam(value = "Key field to search the location entry", required = true) @PathParam("field") String field,
            @ApiParam(value = "Field value for the location entry", required = true) @PathParam("id") String id) {
        return bean.obtenerUbicacion(field, id, request.getLocale());
    }
    
    @ApiOperation(value = "Get a list of location entries", response = LocationEntry.class, responseContainer = "List",
            notes = "The path param \"list-type\" can have the following values:\n-\"all\" (all the locations registered)\n-\"published\" (only the published locations)\n-\"archived\" (only the archived locations)")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("list-{list-type}")
    public List getLocationsList(@ApiParam(value = "Location entries list type to get", required = true) @PathParam("list-type") String listType) {
        return bean.obtenerUbicaciones(listType, request.getLocale());
    }
}
