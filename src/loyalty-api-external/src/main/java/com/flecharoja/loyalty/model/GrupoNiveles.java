package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "GRUPO_NIVELES")
public class GrupoNiveles implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_GRUPO_NIVEL")
    private String idGrupoNivel;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "GRUPO_NIVELES")
    @OrderBy(value = "metricaInicial")
    private List<NivelMetrica> nivelMetricaList;
    
    public GrupoNiveles() {
    }
    
    public String getIdGrupoNivel() {
        return idGrupoNivel;
    }

    public void setIdGrupoNivel(String idGrupoNivel) {
        this.idGrupoNivel = idGrupoNivel;
    }

    public List<NivelMetrica> getNivelMetricaList() {
        return nivelMetricaList;
    }

    public void setNivelMetricaList(List<NivelMetrica> nivelMetricaList) {
        this.nivelMetricaList = nivelMetricaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGrupoNivel != null ? idGrupoNivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupoNiveles)) {
            return false;
        }
        GrupoNiveles other = (GrupoNiveles) object;
        if ((this.idGrupoNivel == null && other.idGrupoNivel != null) || (this.idGrupoNivel != null && !this.idGrupoNivel.equals(other.idGrupoNivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.GrupoNiveles[ idGrupoNivel=" + idGrupoNivel + " ]";
    }  

}
