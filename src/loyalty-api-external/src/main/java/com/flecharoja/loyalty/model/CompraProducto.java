package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "COMPRA_PRODUCTO")
@ApiModel(value = "TransactionProductEntry")
public class CompraProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    @ApiModelProperty(hidden = true)
    protected CompraProductoPK compraProductoPK;
    
    @Transient
    @ApiModelProperty(value = "Product ID/internal name", required = true)
    private String idProduct;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANTIDAD")
    @ApiModelProperty(value = "Quantity", required = true)
    private Long quantity;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "VALOR_UNITARIO")
    @ApiModelProperty(value = "Value per unit", required = true)
    private Double valuePerUnit;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL")
    @ApiModelProperty(value = "Total", required = true)
    private Double total;

    public CompraProducto() {
    }

    public CompraProducto(String idTransaccion, String idProduct, Long quantity, Double valuePerUnit, Double total) {
        this.compraProductoPK = new CompraProductoPK(idTransaccion, idProduct);
        this.quantity = quantity;
        this.valuePerUnit = valuePerUnit;
        this.total = total;
    }

    public CompraProductoPK getCompraProductoPK() {
        return compraProductoPK;
    }

    public void setCompraProductoPK(CompraProductoPK compraProductoPK) {
        this.compraProductoPK = compraProductoPK;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getValuePerUnit() {
        return valuePerUnit;
    }

    public void setValuePerUnit(Double valuePerUnit) {
        this.valuePerUnit = valuePerUnit;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (compraProductoPK != null ? compraProductoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompraProducto)) {
            return false;
        }
        CompraProducto other = (CompraProducto) object;
        if ((this.compraProductoPK == null && other.compraProductoPK != null) || (this.compraProductoPK != null && !this.compraProductoPK.equals(other.compraProductoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CompraProducto[ compraProductoPK=" + compraProductoPK + " ]";
    }
    
}
