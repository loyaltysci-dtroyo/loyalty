package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.MemberEntry;
import com.flecharoja.loyalty.model.RespuestaRegistroEntidad;
import com.flecharoja.loyalty.patch.PATCH;
import com.flecharoja.loyalty.service.MiembroService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.ejb.EJB;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author svargas
 */
@Api(value = "Members")
@Path("member")
public class MemberResource {
    
    @Context
    HttpServletRequest request;
    
    @EJB
    MiembroService bean;
    
    @ApiOperation(value = "Register a new member entry", response = RespuestaRegistroEntidad.class,
            notes = "The value of the query param \"enable\" can be 0 (the entry will be register with status disable) or 1 (the entry will be register with status enable)")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "data", value = "Member data to register", required = true, dataType = "com.flecharoja.loyalty.model.MemberEntry", paramType = "body"),
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaRegistroEntidad registerMember(JsonObject data,
            @ApiParam(value = "Indicator for enabling/disabling the member entry", required = true, defaultValue = "0") @QueryParam("enable") String enable) {
        MemberEntry memberEntry = bean.registrarMiembro(data, enable, request.getLocale());
        bean.postRegistroMiembro(memberEntry);
        return new RespuestaRegistroEntidad(memberEntry.getInternalMemberId(), memberEntry.getFechaCreacion());
    }

    @ApiOperation(value = "Patch an existing member entry",
            notes = "The path param \"field\" can have the following values:\n-\"username\" (username of the member account)\n-\"email\" (actual email of a member account)\n-\"internal-id\" (internal id of the member account)")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "data", value = "Member data to patch", required = true, dataType = "com.flecharoja.loyalty.model.MemberEntry", paramType = "body"),
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("by-{field}/{id}")
    public void patchMember(JsonObject data,
            @ApiParam(value = "Key field to search the member entry", required = true) @PathParam("field") String field,
            @ApiParam(value = "Field value for the member entry", required = true) @PathParam("id") String id) {
        bean.editarMiembro(data, field, id, request.getLocale());
    }
    
    @ApiOperation(value = "Get an existing member entry", response = MemberEntry.class,
            notes = "The path param \"field\" can have the following values:\n-\"username\" (username of the member account)\n-\"email\" (actual email of a member account)\n-\"internal-id\" (internal id of the member account)")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("by-{field}/{id}")
    public JsonObject getMember(
            @ApiParam(value = "Key field to search the member entry", required = true) @PathParam("field") String field,
            @ApiParam(value = "Field value for the member entry", required = true) @PathParam("id") String id) {
        return bean.obtenerMiembro(field, id, request.getLocale());
    }
    
    @ApiOperation(value = "Enable/Disable an existing member entry",
            notes = "The path param \"field\" can have the following values:\n-\"username\" (username of the member account)\n-\"email\" (actual email of a member account)\n-\"internal-id\" (internal id of the member account)\nThe path param \"action\" can have the following values:\n-\"enable\" (enable a member account)\n-\"disable\" (disable a member account)")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "data", value = "Extra data", required = true, dataType = "string", paramType = "body"),
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("by-{field}/{id}/{action}")
    public void disableEnableMember(
            @ApiParam(value = "Key field to search the location entry", required = true) @PathParam("field") String field,
            @ApiParam(value = "Field value for the location entry", required = true) @PathParam("id") String id,
            @ApiParam(value = "Action to trigger", required = true) @PathParam("action") String action,
            JsonObject data) {
        bean.cambioEstadoMiembro(field, id, action, data, request.getLocale());
    }
}
