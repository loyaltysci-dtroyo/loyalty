package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CompraProducto;
import com.flecharoja.loyalty.model.CompraProductoPK;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.MiembroMin;
import com.flecharoja.loyalty.model.ProductoMin;
import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.model.RespuestaRegistroEntidad;
import com.flecharoja.loyalty.model.TransaccionCompra;
import com.flecharoja.loyalty.model.UbicacionMin;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author svargas
 */
@Stateless
public class TransaccionService {
    
    @EJB
    MyKafkaUtils myKafkaUtils;
    
    @EJB
    private RegistroMetricaService registroMetricaService;
    
    @EJB
    private AutoAsignacionPremios autoAsignacionPremios;

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    public RespuestaRegistroEntidad registrarTransaccion(JsonObject data, Locale locale) {
        if (data==null) {
            throw new MyException(MyException.ErrorPeticion.CUERPO_PETICION_INVALIDO, locale, data, "data_registration_not_send");
        }
        String idMember;
        try {
            idMember = data.getString("idMember");
        } catch (NullPointerException | ClassCastException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "id_member_not_present");
        }
        
        TransaccionCompra tc = new TransaccionCompra();
        if (em.find(MiembroMin.class, idMember)==null) {
            String idMemberKC;
            try(MyKeycloakUtils keycloakUtils = new MyKeycloakUtils()) {
                idMemberKC = keycloakUtils.getIdMember(idMember);
            }
            if (idMemberKC!=null) {
                tc.setIdMember(idMemberKC);
            } else {
                List<MiembroMin> result = em.createNamedQuery("MiembroMin.findMiembrosByDocIdentificacion").setParameter("docIdentificacion", idMember).getResultList();
                if (!result.isEmpty() && result.size()==1) {
                    tc.setIdMember(result.get(0).getIdMiembro());
                } else {
                    result = em.createNamedQuery("MiembroMin.findMiembrosByCorreo").setParameter("correo", idMember).getResultList();
                    if (!result.isEmpty() && result.size()==1) {
                        tc.setIdMember(result.get(0).getIdMiembro());
                    } else {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "id_member_invalid");
                    }
                }
            }
        } else {
            tc.setIdMember(idMember);
        }
        
        try {
            tc.setIdTransaction(data.getString("idTransaction"));
        } catch (NullPointerException | ClassCastException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "id_transaction_not_present");
        }
        
        if (data.containsKey("idLocation")) {
            String idUbicacion;
            try {
                idUbicacion = data.getString("idLocation");
            } catch (ClassCastException e) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "id_location_invalid");
            }
            if (em.find(UbicacionMin.class, idUbicacion)==null) {
                List<UbicacionMin> resultUbicaciones = em.createNamedQuery("UbicacionMin.getUbicacionesByNombreInterno").setParameter("nombreInterno", idUbicacion).getResultList();
                if (resultUbicaciones.isEmpty() || resultUbicaciones.size()!=1) {
                    throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "id_location_invalid");
                } else {
                    tc.setIdLocation(resultUbicaciones.get(0).getIdUbicacion());
                }
            } else {
                tc.setIdLocation(idUbicacion);
            }
        }
        
        double subtotal;
        double taxes;
        double total;
        try {
            subtotal = data.getJsonNumber("subtotal").doubleValue();
            taxes = data.getJsonNumber("taxes").doubleValue();
            total = data.getJsonNumber("total").doubleValue();
        } catch (NullPointerException | ClassCastException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "transaction_details_invalid", "subTotal, taxes, total");
        }
        
        Date fecha = new Date();
        try {
            fecha.setTime(data.getJsonNumber("date").longValue());
        } catch (NullPointerException | ClassCastException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "transaction_details_invalid", "date");
        }
        tc.setDate(fecha);
        
        if ((subtotal+taxes)!=total) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "transaction_details_invalid", "subTotal, taxes, total");
        } else {
            tc.setSubtotal(subtotal);
            tc.setTaxes(taxes);
            tc.setTotal(total);
        }
        
//        List<CompraProducto> productsDetails = transaccionCompra.getProductsDetails();
        Map<String, CompraProducto> mapProductDetails;
        if (data.containsKey("productsDetails")) {
            mapProductDetails = data.getJsonArray("productsDetails").stream().map((t) -> {
                if (t.getValueType()!=JsonValue.ValueType.OBJECT) {
                    throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "products_definitions_invalid");
                }
                JsonObject jo = (JsonObject) t;
                return new CompraProducto(null, jo.getString("idProduct"), jo.getJsonNumber("quantity").longValue(), jo.getJsonNumber("valuePerUnit").doubleValue(), jo.getJsonNumber("total").doubleValue());
            }).collect(Collectors.toMap((t) -> t.getCompraProductoPK().getIdProducto(), (t) -> {
                if (t.getQuantity().compareTo(0l)<=0 || t.getTotal().compareTo(0d)<=0 || t.getValuePerUnit().compareTo(0d)<=0) {
                    return null;
                }
                if (em.find(ProductoMin.class, t.getCompraProductoPK().getIdProducto())==null) {
                    List<String> idsProductos = em.createNamedQuery("ProductoMin.getIdsProductosByNombreInterno").setParameter("nombreInterno", t.getCompraProductoPK().getIdProducto()).getResultList();
                    if (idsProductos.isEmpty() || idsProductos.size()!=1) {
                        return null;
                    } else {
                        t.getCompraProductoPK().setIdProducto(idsProductos.get(0));
                    }
                }
                return t;
            }));

            Supplier<Stream<String>> supplier = () -> mapProductDetails.entrySet().stream().filter((t) -> t.getValue()==null).map((t) -> t.getKey());
            if (supplier.get().count()>0) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "products_definitions_invalid", supplier.get().collect(Collectors.joining(", ")));
            }
            
            if (data.getJsonArray("productsDetails").size()!=mapProductDetails.values().stream().map((t) ->  t.getCompraProductoPK().getIdProducto()).distinct().count()) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "duplicates_ids_products");
            }
        } else {
            mapProductDetails = null;
        }
        
        try {
            em.persist(tc);
            em.flush();
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "transaction_details_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        
        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.COMPRO_ALGUN_PRODUCTO, tc.getIdTransaccionCompra(), tc.getIdMember());
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
        }
        
        if (mapProductDetails!=null) {
            mapProductDetails.values().stream().map((t) -> {
                t.setCompraProductoPK(new CompraProductoPK(tc.getIdTransaccionCompra(), t.getCompraProductoPK().getIdProducto()));
                return t;
            }).map((t) -> {
                em.persist(t);
                return t;
            }).forEachOrdered((t) -> {
                try {
                    myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.COMPRA_PRODUCTO_X, t.getCompraProductoPK().getIdProducto(), tc.getIdMember());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            });
        }
        
        if (data.containsKey("metricEntry")) {
            JsonObject metricEntry;
            try {
                metricEntry = data.getJsonObject("metricEntry");
            } catch (ClassCastException ex) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "transaction_details_invalid", "metricEntry");
            }
            
            double cantidad;
            try {
                cantidad = metricEntry.getJsonNumber("amount").doubleValue();
            } catch (ClassCastException | NullPointerException e) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "transaction_details_invalid", "metricEntry.amount");
            }
            
            if (cantidad != 0) {
                RegistroMetrica.TiposGane indTipoGane;
                try {
                    indTipoGane = RegistroMetrica.TiposGane.get(metricEntry.getString("indType"));
                    if (indTipoGane == null) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "transaction_details_invalid", "metricEntry.indType");
                    }
                } catch (NullPointerException ex) {
                    indTipoGane = RegistroMetrica.TiposGane.TRANSACCION;
                } catch (ClassCastException ex) {
                    throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "transaction_details_invalid", "metricEntry.indType");
                }
                
                //revision de casos de tipo negativo por deficion de cantidad en negativo o en caso contrario positivo
                switch(indTipoGane) {
                    case REVERSION: {
                        if (cantidad>0) {
                            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "the_metric_amount_incorrect", "negative");
                        }
                        cantidad *= -1;
                        break;
                    }
                    default: {
                        if (cantidad<0) {
                            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "the_metric_amount_incorrect", "positive");
                        }
                    }
                }
                
                String idMetrica;
                try {
                    idMetrica = metricEntry.getString("idMetric");
                    if (em.find(Metrica.class, idMetrica)==null) {
                        try {
                            idMetrica = (String) em.createNamedQuery("Metrica.getIdMetricaByNombreInterno").setParameter("nombreInterno", idMetrica).getSingleResult();
                        } catch (NoResultException ex) {
                            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "transaction_details_invalid", "metricEntry.idMetric");
                        } catch (NonUniqueResultException ex) {
                            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "metric_could_not_be_determinated");
                        }
                    }
                } catch (NullPointerException ex) {
                    idMetrica = em.find(ConfiguracionGeneral.class, 0L).getIdMetricaPrincipal();
                } catch (ClassCastException ex) {
                    throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "transaction_details_invalid", "metricEntry.idMetric");
                }
                
                //segun el tipo de gane se realiza la accion de reversion o de acumulacion
                switch(indTipoGane) {
                    case REVERSION: {
                        registroMetricaService.revertirMetrica(new RegistroMetrica(new Date(), idMetrica, idMember, indTipoGane, cantidad, tc.getIdTransaccionCompra()));
                        break;
                    }
                    default: {
                        registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), idMetrica, idMember, indTipoGane, cantidad, tc.getIdTransaccionCompra()));
                    }
                }
            }
        } else {
            ConfiguracionGeneral config = em.find(ConfiguracionGeneral.class, 0L);

            if (config.getPorcentajeCompra()!=null && config.getIdMetricaPrincipal()!=null && config.getPorcentajeCompra().compareTo(0l)>0) {
                double metricaGanada = tc.getTotal()*(config.getPorcentajeCompra()/100d);

                if (metricaGanada<0) {
                    registroMetricaService.revertirMetrica(new RegistroMetrica(new Date(), config.getIdMetricaPrincipal(), tc.getIdMember(), RegistroMetrica.TiposGane.REVERSION, metricaGanada*-1, tc.getIdTransaccionCompra()));
                } else {
                    registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), config.getIdMetricaPrincipal(), tc.getIdMember(), RegistroMetrica.TiposGane.TRANSACCION, metricaGanada, tc.getIdTransaccionCompra()));
                }
            }
        }

        autoAsignacionPremios.eventosCompra(tc);

        return new RespuestaRegistroEntidad(tc.getIdTransaccionCompra(), new Date());
    }
    
}
