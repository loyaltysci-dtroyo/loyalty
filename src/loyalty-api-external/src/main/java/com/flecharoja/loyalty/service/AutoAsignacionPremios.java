package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.MiembroBalanceMetrica;
import com.flecharoja.loyalty.model.MiembroBalanceMetricaPK;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.ReglaAsignacionPremio;
import com.flecharoja.loyalty.model.TransaccionCompra;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author svargas
 */
@Stateless
public class AutoAsignacionPremios {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    PremioBean premioBean;

    @TransactionAttribute(TransactionAttributeType.NEVER)
    @Asynchronous
    public void eventosCompra(TransaccionCompra transaccion) {
        List<Premio> premios = em.createNamedQuery("Premio.findAllByIndEstado").setParameter("indEstado", Premio.Estados.PUBLICADO.getValue()).getResultList();

        for (Iterator<Premio> iterator = premios.stream().filter((premio) -> {
            List<ReglaAsignacionPremio> reglas = em.createNamedQuery("ReglaAsignacionPremio.findByIdPremioOrderByFechaCreacion").setParameter("idPremio", premio.getIdPremio()).getResultList();
            boolean result = !reglas.isEmpty();
            try {
                for (ReglaAsignacionPremio regla : reglas) {
                    if (!result && ReglaAsignacionPremio.ValoresIndOperadorRegla.get(regla.getIndOperadorRegla())==ReglaAsignacionPremio.ValoresIndOperadorRegla.AND) {
                        break;
                    }
                    boolean resultRegla = false;
                    
                    if (regla.getTier()!=null) {
                        String idMetrica;
                        try {
                            idMetrica = ((ConfiguracionGeneral) em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList().get(0)).getIdMetricaPrincipal();
                        } catch (NullPointerException e) {
                            continue;
                        }
                        MiembroBalanceMetrica balanceMetrica = em.find(MiembroBalanceMetrica.class, new MiembroBalanceMetricaPK(transaccion.getIdMember(), idMetrica));
                        if (balanceMetrica==null) {
                            balanceMetrica = new MiembroBalanceMetrica(transaccion.getIdMember(), idMetrica);
                        }
                        if (regla.getTier().getMetricaInicial()>balanceMetrica.getProgresoActual()) {
                            continue;
                        }
                    }
                    
                    ReglaAsignacionPremio.TiposRegla tipoRegla = ReglaAsignacionPremio.TiposRegla.get(regla.getIndTipoRegla());
                    switch (tipoRegla) {
                        case MONTO_COMPRA: {
                            switch(ReglaAsignacionPremio.ValoresIndOperador.get(regla.getIndOperador())) {
                                case IGUAL: {
                                    resultRegla = transaccion.getTotal()==Double.parseDouble(regla.getValorComparacion());
                                    break;
                                }
                                case MAYOR: {
                                    resultRegla = transaccion.getTotal()>Double.parseDouble(regla.getValorComparacion());
                                    break;
                                }
                                case MAYOR_IGUAL: {
                                    resultRegla = transaccion.getTotal()>=Double.parseDouble(regla.getValorComparacion());
                                    break;
                                }
                                case MENOR: {
                                    resultRegla = transaccion.getTotal()<Double.parseDouble(regla.getValorComparacion());
                                    break;
                                }
                                case MENOR_IGUAL: {
                                    resultRegla = transaccion.getTotal()<=Double.parseDouble(regla.getValorComparacion());
                                    break;
                                }
                            }
                            break;
                        }
                        default: {
                            Stream<TransaccionCompra> transacciones = em.createNamedQuery("TransaccionCompra.findByIdMiembro").setParameter("idMember", transaccion.getIdMember()).getResultList().stream();
                            switch(ReglaAsignacionPremio.TiposFecha.get(regla.getFechaIndTipo())) {
                                case ANO_ACTUAL: {
                                    transacciones = transacciones.filter((transaccionMiembro) -> {
                                        return DateUtils.truncatedEquals(transaccion.getDate(), transaccionMiembro.getDate(), Calendar.YEAR);
                                    });
                                    break;
                                }
                                case ANTERIOR: {
                                    int cantidadFecha;
                                    try {
                                        cantidadFecha = regla.getFechaCantidad()!=null?Integer.parseInt(regla.getFechaCantidad()):1;
                                    } catch (NumberFormatException e) {
                                        cantidadFecha = 0;
                                    }
                                    switch(ReglaAsignacionPremio.TiposUltimaFecha.get(regla.getFechaIndUltimo())) {
                                        case ANO: {
                                            Date fecha = DateUtils.addYears(new Date(), -cantidadFecha);
                                            transacciones = transacciones.filter((transaccionMiembro) -> {
                                                boolean expr1 = DateUtils.truncate(fecha, Calendar.YEAR).equals(DateUtils.truncate(transaccionMiembro.getDate(), Calendar.YEAR));
                                                boolean expr2 = DateUtils.truncate(fecha, Calendar.YEAR).before(DateUtils.truncate(transaccionMiembro.getDate(), Calendar.YEAR));
                                                //si la transaccion ocurrio en el x # de annos anteriores pero no en el actual
                                                return !expr1 && expr2;
                                            });
                                            break;
                                        }
                                        case DIA: {
                                            Date fecha = DateUtils.addDays(new Date(), -cantidadFecha);
                                            transacciones = transacciones.filter((transaccionMiembro) -> {
                                                boolean expr1 = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH).equals(DateUtils.truncate(transaccionMiembro.getDate(), Calendar.DAY_OF_MONTH));
                                                boolean expr2 = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH).before(DateUtils.truncate(transaccionMiembro.getDate(), Calendar.DAY_OF_MONTH));
                                                //si la transaccion ocurrio en el x # de dias anteriores pero no en el actual
                                                return !expr1 && expr2;
                                            });
                                            break;
                                        }
                                        case MES: {
                                            Date fecha = DateUtils.addMonths(new Date(), -cantidadFecha);
                                            transacciones = transacciones.filter((transaccionMiembro) -> {
                                                boolean expr1 = DateUtils.truncate(fecha, Calendar.MONTH).equals(DateUtils.truncate(transaccionMiembro.getDate(), Calendar.MONTH));
                                                boolean expr2 = DateUtils.truncate(fecha, Calendar.MONTH).before(DateUtils.truncate(transaccionMiembro.getDate(), Calendar.MONTH));
                                                //si la transaccion ocurrio en el x # de dias anteriores pero no en el actual
                                                return !expr1 && expr2;
                                            });
                                            break;
                                        }
                                    }
                                    break;
                                }
                                case DESDE: {
                                    transacciones = transacciones.filter((transaccionMiembro) -> {
                                        return transaccionMiembro.getDate().compareTo(regla.getFechaInicio())>=0;
                                    });
                                    break;
                                }
                                case ENTRE: {
                                    transacciones = transacciones.filter((transaccionMiembro) -> {
                                        return transaccionMiembro.getDate().compareTo(regla.getFechaInicio())>=0 && transaccionMiembro.getDate().compareTo(regla.getFechaFinal())<=0;
                                    });
                                    break;
                                }
                                case HASTA: {
                                    transacciones = transacciones.filter((transaccionMiembro) -> {
                                        return transaccionMiembro.getDate().compareTo(regla.getFechaFinal())<=0;
                                    });
                                    break;
                                }
                                case MES_ACTUAL: {
                                    transacciones = transacciones.filter((transaccionMiembro) -> {
                                        return DateUtils.truncatedEquals(transaccion.getDate(), transaccionMiembro.getDate(), Calendar.YEAR) && DateUtils.truncatedEquals(transaccion.getDate(), transaccionMiembro.getDate(), Calendar.MONTH);
                                    });
                                    break;
                                }
                                case ULTIMO: {
                                    int cantidadFecha;
                                    try {
                                        cantidadFecha = regla.getFechaCantidad()!=null?Integer.parseInt(regla.getFechaCantidad()):1;
                                    } catch (NumberFormatException e) {
                                        cantidadFecha = 0;
                                    }
                                    switch(ReglaAsignacionPremio.TiposUltimaFecha.get(regla.getFechaIndUltimo())) {
                                        case ANO: {
                                            Date fecha = DateUtils.addYears(new Date(), -cantidadFecha);
                                            transacciones = transacciones.filter((transaccionMiembro) -> {
                                                return transaccionMiembro.getDate().compareTo(fecha)>=0;
                                            });
                                            break;
                                        }
                                        case DIA: {
                                            Date fecha = DateUtils.addDays(new Date(), -cantidadFecha);
                                            transacciones = transacciones.filter((transaccionMiembro) -> {
                                                return transaccionMiembro.getDate().compareTo(fecha)>=0;
                                            });
                                            break;
                                        }
                                        case MES: {
                                            Date fecha = DateUtils.addMonths(new Date(), -cantidadFecha);
                                            transacciones = transacciones.filter((transaccionMiembro) -> {
                                                return transaccionMiembro.getDate().compareTo(fecha)>=0;
                                            });
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            switch (tipoRegla) {
                                case ACUMULADO_COMPRA: {
                                    double resultado = transacciones.map((transaccionMiembro) -> transaccionMiembro.getTotal()).reduce(0d, (t, u) -> t+u);
                                    switch(ReglaAsignacionPremio.ValoresIndOperador.get(regla.getIndOperador())) {
                                        case IGUAL: {
                                            resultRegla = resultado==Double.parseDouble(regla.getValorComparacion());
                                            break;
                                        }
                                        case MAYOR: {
                                            resultRegla = resultado>Double.parseDouble(regla.getValorComparacion());
                                            break;
                                        }
                                        case MAYOR_IGUAL: {
                                            resultRegla = resultado>=Double.parseDouble(regla.getValorComparacion());
                                            break;
                                        }
                                        case MENOR: {
                                            resultRegla = resultado<Double.parseDouble(regla.getValorComparacion());
                                            break;
                                        }
                                        case MENOR_IGUAL: {
                                            resultRegla = resultado<=Double.parseDouble(regla.getValorComparacion());
                                            break;
                                        }
                                    }
                                    break;
                                }
                                case FRECUENCIA_COMPRA: {
                                    int resultado = transacciones.map((transaccionMiembro) -> transaccionMiembro.getTotal()>0?1:-1).reduce(0, (t, u) -> t+u);
                                    switch(ReglaAsignacionPremio.ValoresIndOperador.get(regla.getIndOperador())) {
                                        case IGUAL: {
                                            resultRegla = resultado==Integer.parseInt(regla.getValorComparacion());
                                            break;
                                        }
                                        case MAYOR: {
                                            resultRegla = resultado>Integer.parseInt(regla.getValorComparacion());
                                            break;
                                        }
                                        case MAYOR_IGUAL: {
                                            resultRegla = resultado>=Integer.parseInt(regla.getValorComparacion());
                                            break;
                                        }
                                        case MENOR: {
                                            resultRegla = resultado<Integer.parseInt(regla.getValorComparacion());
                                            break;
                                        }
                                        case MENOR_IGUAL: {
                                            resultRegla = resultado<=Integer.parseInt(regla.getValorComparacion());
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if (regla.equals(reglas.get(0))) {
                        result = resultRegla;
                    } else {
                        switch(ReglaAsignacionPremio.ValoresIndOperadorRegla.get(regla.getIndOperadorRegla())) {
                            case AND: {
                                result = resultRegla && result;
                                break;
                            }
                            case OR: {
                                result = resultRegla || result;
                                break;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Alguna regla del premio "+premio.getIdPremio()+" fallo...", e);
                return false;
            }
            return result;
        }).iterator(); iterator.hasNext();) {
            Premio premio = iterator.next();
            try {
                premioBean.otorgarPremio(premio.getIdPremio(), transaccion.getIdMember(), Locale.US);
                break;
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Algo fue muy mal...", ex);
            }
        }
    }
}
