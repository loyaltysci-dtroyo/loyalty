package com.flecharoja.loyalty.util;

/**
 *
 * @author svargas, wtencio
 */
public class Indicadores {
    public static final String URL_IMAGEN_PREDETERMINADA = "https://loyalty.flecharoja.com/resources/img/generic0.jpg";
    
    public static final char INCLUIDO = 'I';
    public static final char EXCLUIDO = 'E';
}
