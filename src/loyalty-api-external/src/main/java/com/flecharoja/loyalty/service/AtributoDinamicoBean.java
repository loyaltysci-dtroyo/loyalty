package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.AtributoDinamico;
import com.flecharoja.loyalty.model.AtributoDinamicoProducto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author svargas
 */
@Stateless
public class AtributoDinamicoBean {
    
    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    public String insertAtributoDinamicoMiembro(AtributoDinamico atributoDinamico) {
        em.persist(atributoDinamico);
        em.flush();
        return atributoDinamico.getIdAtributo();
    }
    
    public String insertAtributoDinamicoProducto(AtributoDinamicoProducto atributoDinamico) {
        em.persist(atributoDinamico);
        em.flush();
        return atributoDinamico.getIdAtributo();
    }
}
