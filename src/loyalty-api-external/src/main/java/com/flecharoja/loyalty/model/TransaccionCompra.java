package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "TRANSACCION_COMPRA")
@NamedQueries({
    @NamedQuery(name = "TransaccionCompra.findByIdTransaction", query = "SELECT t FROM TransaccionCompra t WHERE t.idTransaction = :idTransaction"),
    @NamedQuery(name = "TransaccionCompra.countByIdTransaction", query = "SELECT COUNT(t) FROM TransaccionCompra t WHERE t.idTransaction = :idTransaction"),
    @NamedQuery(name = "TransaccionCompra.findByIdMiembro", query = "SELECT t FROM TransaccionCompra t WHERE t.idMember = :idMember")
})
@ApiModel(value = "TransactionEntry")
public class TransaccionCompra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "transaccion_uuid")
    @GenericGenerator(name = "transaccion_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_TRANSACCION")
    @ApiModelProperty(hidden = true)
    private String idTransaccionCompra;
    
    @Basic(optional = false)
    @NotNull
    @Size(max = 150)
    @Column(name = "ID_COMPRA")
    @ApiModelProperty(value = "Transaction ID", required = true)
    private String idTransaction;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "ID_MIEMBRO")
    @ApiModelProperty(value = "Member ID/username/email/identification document", required = true)
    private String idMember;
    
    @Size(min = 36, max = 40)
    @Column(name = "ID_UBICACION")
    @ApiModelProperty(value = "Location ID/internal name/internal code")
    private String idLocation;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Date", required = true, dataType = "long")
    private Date date;
    
    @Transient
    @ApiModelProperty(value = "List of products entries")
    private List<CompraProducto> productsDetails;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUBTOTAL")
    @ApiModelProperty(value = "Subtotal", required = true)
    private Double subtotal;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IMPUESTO")
    @DecimalMin("0.00")
    @ApiModelProperty(value = "Taxes", required = true)
    private Double taxes;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL")
    @ApiModelProperty(value = "Total (subtotal+taxes)", required = true)
    private Double total;
    
    @Transient
    private RegistroMetrica metricEntry;
    
    @Transient
    private String idMetrica;
    
    @Transient
    private int cantMetrica;

    public TransaccionCompra() {
    }

    public String getIdTransaccionCompra() {
        return idTransaccionCompra;
    }

    public void setIdTransaccionCompra(String idTransaccionCompra) {
        this.idTransaccionCompra = idTransaccionCompra;
    }

    public int getCantMetrica() {
        return cantMetrica;
    }

    public String getIdMetrica() {
        return idMetrica;
    }

    public void setCantMetrica(int cantMetrica) {
        this.cantMetrica = cantMetrica;
    }

    public void setIdMetrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    public String getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(String idTransaction) {
        this.idTransaction = idTransaction;
    }

    public String getIdMember() {
        return idMember;
    }

    public void setIdMember(String idMember) {
        this.idMember = idMember;
    }

    public String getIdLocation() {
        return idLocation;
    }

    public void setIdLocation(String idLocation) {
        this.idLocation = idLocation;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<CompraProducto> getProductsDetails() {
        return productsDetails;
    }

    public void setProductsDetails(List<CompraProducto> productsDetails) {
        this.productsDetails = productsDetails;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getTaxes() {
        return taxes;
    }

    public void setTaxes(Double taxes) {
        this.taxes = taxes;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public RegistroMetrica getMetricEntry() {
        return metricEntry;
    }

    public void setMetricEntry(RegistroMetrica metricEntry) {
        this.metricEntry = metricEntry;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTransaccionCompra != null ? idTransaccionCompra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransaccionCompra)) {
            return false;
        }
        TransaccionCompra other = (TransaccionCompra) object;
        if ((this.idTransaccionCompra == null && other.idTransaccionCompra != null) || (this.idTransaccionCompra != null && !this.idTransaccionCompra.equals(other.idTransaccionCompra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.TransaccionCompra[ idTransaccion=" + idTransaccionCompra + " ]";
    }
    
}
