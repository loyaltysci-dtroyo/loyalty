package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "TABPOS_MIEMBRO")
public class TabposMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected TabposMiembroPK tabposMiembroPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACUMULADO")
    private Double acumulado;

    public TabposMiembro() {
    }

    public TabposMiembro(String idTabla, String idMiembro) {
        this.tabposMiembroPK = new TabposMiembroPK(idTabla, idMiembro);
        this.acumulado = 0d;
    }

    public TabposMiembroPK getTabposMiembroPK() {
        return tabposMiembroPK;
    }

    public void setTabposMiembroPK(TabposMiembroPK tabposMiembroPK) {
        this.tabposMiembroPK = tabposMiembroPK;
    }

    public Double getAcumulado() {
        return acumulado;
    }

    public void setAcumulado(Double acumulado) {
        this.acumulado = acumulado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tabposMiembroPK != null ? tabposMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TabposMiembro)) {
            return false;
        }
        TabposMiembro other = (TabposMiembro) object;
        if ((this.tabposMiembroPK == null && other.tabposMiembroPK != null) || (this.tabposMiembroPK != null && !this.tabposMiembroPK.equals(other.tabposMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.TabposMiembro[ tabposMiembroPK=" + tabposMiembroPK + " ]";
    }

}
