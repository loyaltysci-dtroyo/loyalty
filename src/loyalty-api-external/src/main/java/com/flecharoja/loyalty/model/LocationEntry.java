package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "UBICACION")
@NamedQueries({
    @NamedQuery(name = "LocationEntry.getIdAll", query = "SELECT l.internalLocationId FROM LocationEntry l"),
    @NamedQuery(name = "LocationEntry.getIdByIndEstado", query = "SELECT l.internalLocationId FROM LocationEntry l WHERE l.indEstado = :indEstado"),
    @NamedQuery(name = "LocationEntry.getByInternalCode", query = "SELECT l FROM LocationEntry l WHERE l.internalCode=:internalCode"),
    @NamedQuery(name = "LocationEntry.getByInternalName", query = "SELECT l FROM LocationEntry l WHERE l.internalName=:internalName"),
    @NamedQuery(name = "LocationEntry.countByInternalCode", query = "SELECT COUNT(l) FROM LocationEntry l WHERE l.internalCode=:internalCode"),
    @NamedQuery(name = "LocationEntry.countByInternalName", query = "SELECT COUNT(l) FROM LocationEntry l WHERE l.internalName=:internalName"),
    @NamedQuery(name = "LocationEntry.getInternalLocationIdByInternalLocationId", query = "SELECT l.internalLocationId FROM LocationEntry l WHERE l.internalLocationId=:internalLocationId"),
    @NamedQuery(name = "LocationEntry.getInternalLocationIdByInternalName", query = "SELECT l.internalLocationId FROM LocationEntry l WHERE l.internalName=:internalName"),
    @NamedQuery(name = "LocationEntry.getInternalLocationIdByInternalCode", query = "SELECT l.internalLocationId FROM LocationEntry l WHERE l.internalCode=:internalCode")
})
@XmlRootElement
public class LocationEntry implements Serializable {

    public enum Direccion {
        ESTADO("UE"),
        CIUDAD("UC");

        private final String value;
        private static final Map<String, Direccion> lookup = new HashMap<>();

        private Direccion(String value) {
            this.value = value;
        }

        static {
            for (Direccion direccion : values()) {
                lookup.put(direccion.value, direccion);
            }
        }

        public String getValue() {
            return value;
        }
    }

    public enum Efectividad {
        PERMANENTE('P'),
        CALENDARIZADO('C');

        private final char value;
        private static final Map<Character, Efectividad> lookup = new HashMap<>();

        private Efectividad(char value) {
            this.value = value;
        }

        static {
            for (Efectividad efectividad : values()) {
                lookup.put(efectividad.value, efectividad);
            }
        }

        public char getValue() {
            return value;
        }

        public static Efectividad get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum Estados {
        DRAFT('D'),
        PUBLICADO_ACTIVO('P'),
        PUBLICADO_INACTIVO('I'),
        ARCHIVADO('A');

        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }

        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Estados get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "ubicacion_uuid")
    @GenericGenerator(name = "ubicacion_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_UBICACION")
    @ApiModelProperty(hidden = true)
    private String internalLocationId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    @ApiModelProperty(value = "Common name", required = true)
    private String name;

    @Basic(optional = false)
    @NotNull
    @Size(max = 50)
    @Column(name = "NOMBRE_DESPLIEGUE")
    @ApiModelProperty(value = "Internal name", required = true)
    private String internalName;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    @ApiModelProperty(hidden = true)
    private Character indEstado;

    @Basic(optional = false)
    @NotNull
    @Size(max = 3)
    @Column(name = "IND_DIR_PAIS")
    @ApiModelProperty(value = "Address country code", required = true)
    private String addressCountryCode;

    @Basic(optional = false)
    @NotNull
    @Size(max = 100)
    @Column(name = "IND_DIR_ESTADO")
    @ApiModelProperty(value = "Address state name", required = true)
    private String addressState;

    @Basic(optional = false)
    @NotNull
    @Size(max = 100)
    @Column(name = "IND_DIR_CIUDAD")
    @ApiModelProperty(value = "Address city name", required = true)
    private String addressCity;

    @Size(max = 250)
    @Column(name = "DIRECCION")
    @ApiModelProperty(value = "Address")
    private String address;

    @Size(max = 150)
    @Column(name = "HORARIO_ATENCION")
    @ApiModelProperty(value = "Atention schedule string")
    private String atentionSchedule;

    @Size(max = 25)
    @Column(name = "TELEFONO")
    @ApiModelProperty(value = "Phone number")
    private String phoneNumber;

    @Column(name = "IND_CALENDARIZACION")
    @ApiModelProperty(hidden = true)
    private Character indCalendarizacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(hidden = true)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(hidden = true)
    private Date fechaModificacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "DIR_LAT")
    @ApiModelProperty(value = "Address latitude", required = true)
    private Double dirLat;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "DIR_LNG")
    @ApiModelProperty(value = "Address longitude", required = true)
    private Double dirLng;

    @Basic(optional = false)
    @NotNull
    @Column(name = "CODIGO_INTERNO")
    @ApiModelProperty(value = "Internal code", required = true)
    private String internalCode;
    
    @Version
    @Basic(optional = false)
    @NotNull()
    @Column(name = "NUM_VERSION")
    @ApiModelProperty(hidden = true)
    private Long numVersion;

    public LocationEntry() {
    }

    public String getInternalLocationId() {
        return internalLocationId;
    }

    public void setInternalLocationId(String internalLocationId) {
        this.internalLocationId = internalLocationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    @XmlTransient
    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }

    public String getAddressCountryCode() {
        return addressCountryCode;
    }

    public void setAddressCountryCode(String addressCountryCode) {
        this.addressCountryCode = addressCountryCode == null ? null : addressCountryCode.toUpperCase();
    }

    public String getAddressState() {
        return addressState;
    }

    public void setAddressState(String addressState) {
        this.addressState = addressState == null ? null : addressState.toUpperCase();
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAtentionSchedule() {
        return atentionSchedule;
    }

    public void setAtentionSchedule(String atentionSchedule) {
        this.atentionSchedule = atentionSchedule;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @XmlTransient
    public Character getIndCalendarizacion() {
        return indCalendarizacion;
    }

    public void setIndCalendarizacion(Character indCalendarizacion) {
        this.indCalendarizacion = indCalendarizacion == null ? null : Character.toUpperCase(indCalendarizacion);
    }

    @XmlTransient
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @XmlTransient
    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @XmlTransient
    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Double getDirLat() {
        return dirLat;
    }

    public void setDirLat(Double dirLat) {
        this.dirLat = dirLat;
    }

    public Double getDirLng() {
        return dirLng;
    }

    public void setDirLng(Double dirLng) {
        this.dirLng = dirLng;
    }
    
    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += ( internalLocationId != null ? internalLocationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocationEntry)) {
            return false;
        }
        LocationEntry other = (LocationEntry) object;
        if ((this.internalLocationId == null && other.internalLocationId != null) || (this.internalLocationId != null && !this.internalLocationId.equals(other.internalLocationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Ubicacion[ idUbicacion=" + internalLocationId + " ]";
    }
    
}
