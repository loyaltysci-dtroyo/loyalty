package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.LocationEntry;
import com.flecharoja.loyalty.model.RespuestaRegistroEntidad;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author svargas
 */
@Stateless
public class UbicacionService {
    
    private enum CamposFiltro {
        CODIGO_UBICACION("internal-code"),
        NOMBRE_INTERNO("internal-name"),
        UUID("internal-id");
        
        private final String value;
        private static final Map<String, CamposFiltro> lookup =  new HashMap<>();

        private CamposFiltro(String value) {
            this.value = value;
        }
        
        static {
            for (CamposFiltro genero : values()) {
                lookup.put(genero.value, genero);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static CamposFiltro get(String value) {
            return value==null?null:lookup.get(value.toLowerCase());
        }
    }
    
    private enum CamposListas {
        TODOS("all"),
        PUBLICADOS("published"),
        ARCHIVADOS("archived");
        
        private final String value;
        private static final Map<String, CamposListas> lookup =  new HashMap<>();

        private CamposListas(String value) {
            this.value = value;
        }
        
        static {
            for (CamposListas genero : values()) {
                lookup.put(genero.value, genero);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static CamposListas get(String value) {
            return value==null?null:lookup.get(value.toLowerCase());
        }
    }
    
    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public RespuestaRegistroEntidad registrarUbicacion(JsonObject data, String enable, Locale locale) {
        if (data==null) {
            throw new MyException(MyException.ErrorPeticion.CUERPO_PETICION_INVALIDO, locale, data, "data_registration_not_send");
        }
        
        LocationEntry locationEntry = new LocationEntry();
        
        if (enable==null) {
            locationEntry.setIndEstado(LocationEntry.Estados.PUBLICADO_INACTIVO.getValue());
        } else {
            switch(enable) {
                case "0": {
                    locationEntry.setIndEstado(LocationEntry.Estados.PUBLICADO_INACTIVO.getValue());
                    break;
                }
                case "1": {
                    locationEntry.setIndEstado(LocationEntry.Estados.PUBLICADO_ACTIVO.getValue());
                    break;
                }
                default: {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTO_PETICION_INVALIDO, locale, data, "query_parameter_invalid", "enable");
                }
            }
        }
        
        Date fecha = new Date();
        locationEntry.setFechaCreacion(fecha);
        locationEntry.setFechaModificacion(fecha);
        locationEntry.setIndCalendarizacion(LocationEntry.Efectividad.PERMANENTE.getValue());
        locationEntry.setNumVersion(1L);
        
        try {
            if (((long)em.createNamedQuery("LocationEntry.countByInternalCode").setParameter("internalCode", data.getString("internalCode")).getSingleResult())>0) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "duplicate_internal_code");
            }
            locationEntry.setInternalCode(data.getString("internalCode"));
        } catch (NullPointerException | ClassCastException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", "internalCode");
        }
        
        data.entrySet().stream().distinct().forEach((atributo) -> {
            switch(atributo.getKey()) {
                case "name": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setName(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "internalName": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setInternalName(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "addressCountryCode": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setAddressCountryCode(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "addressState": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setAddressState(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "addressCity": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setAddressCity(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "address": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setAddress(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "atentionSchedule": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setAtentionSchedule(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "phoneNumber": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setPhoneNumber(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "dirLat": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setDirLat(((JsonNumber)atributo.getValue()).doubleValue());
                    break;
                }
                case "dirLng": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setDirLng(((JsonNumber)atributo.getValue()).doubleValue());
                    break;
                }
            }
        });
        
        em.persist(data);
        try {
            em.flush();
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        } catch (Exception e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, data, "could_not_be_persisted");
        }
        
        return new RespuestaRegistroEntidad(locationEntry.getInternalLocationId(), fecha);
    }

    public void editarUbicacion(JsonObject data, String field, String id, Locale locale) {
        if (data==null) {
            throw new MyException(MyException.ErrorPeticion.CUERPO_PETICION_INVALIDO, locale, data, "data_registration_not_send");
        }
        
        LocationEntry locationEntry = searchLocation(field, id, locale);
        
        if (LocationEntry.Estados.get(locationEntry.getIndEstado())==LocationEntry.Estados.ARCHIVADO) {
            throw new MyException(MyException.ErrorAutorizacion.ENTIDAD_ARCHIVADA, locale, null, "location_archived");
        }
        
        Date fecha = new Date();
        locationEntry.setFechaModificacion(fecha);
        
        data.entrySet().stream().distinct().forEach((atributo) -> {
            switch(atributo.getKey()) {
                case "name": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setName(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "internalName": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setInternalName(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "addressCountryCode": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setAddressCountryCode(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "addressState": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setAddressState(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "addressCity": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setAddressCity(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "address": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setAddress(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "atentionSchedule": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setAtentionSchedule(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "phoneNumber": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.STRING) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setPhoneNumber(((JsonString)atributo.getValue()).getString());
                    break;
                }
                case "dirLat": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setDirLat(((JsonNumber)atributo.getValue()).doubleValue());
                    break;
                }
                case "dirLng": {
                    if (atributo.getValue().getValueType()!=JsonValue.ValueType.NUMBER) {
                        throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", atributo.getKey());
                    }
                    locationEntry.setDirLng(((JsonNumber)atributo.getValue()).doubleValue());
                    break;
                }
            }
        });
        
        em.merge(data);
        try {
            em.flush();
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, data, "attribute_value_type_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        } catch (Exception e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, data, "could_not_be_persisted");
        }
    }

    public LocationEntry obtenerUbicacion(String field, String id, Locale locale) {
        return searchLocation(field, id, locale);
    }

    public List<LocationEntry> obtenerUbicaciones(String listType, Locale locale) {
        CamposListas lista = CamposListas.get(listType);
        if (lista==null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTO_RUTA_INVALIDO, locale, null, "query_parameter_invalid", "list-type");
        }
        
        Set<String> result = new HashSet<>();
        switch(lista) {
            case TODOS: {
                result.addAll(em.createNamedQuery("LocationEntry.getIdAll").getResultList());
                result.removeAll(em.createNamedQuery("LocationEntry.getIdByIndEstado").setParameter("indEstado", LocationEntry.Estados.DRAFT.getValue()).getResultList());
                break;
            }
            case PUBLICADOS: {
                result.addAll(em.createNamedQuery("LocationEntry.getIdByIndEstado").setParameter("indEstado", LocationEntry.Estados.PUBLICADO_ACTIVO.getValue()).getResultList());
                result.addAll(em.createNamedQuery("LocationEntry.getIdByIndEstado").setParameter("indEstado", LocationEntry.Estados.PUBLICADO_INACTIVO.getValue()).getResultList());
                break;
            }
            case ARCHIVADOS: {
                result.addAll(em.createNamedQuery("LocationEntry.getIdByIndEstado").setParameter("indEstado", LocationEntry.Estados.ARCHIVADO.getValue()).getResultList());
                break;
            }
        }
        
        return result.stream().map((id) -> obtenerUbicacion(CamposFiltro.UUID.value, id, locale)).collect(Collectors.toList());
    }
    
    private LocationEntry searchLocation(String field, String id, Locale locale) {
        CamposFiltro filtro = CamposFiltro.get(field);
        if (filtro==null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTO_PETICION_INVALIDO, locale, null, "query_parameter_invalid", "enable");
        }
        LocationEntry locationEntry;
        switch (filtro) {
            case NOMBRE_INTERNO: {
                try {
                    locationEntry = (LocationEntry) em.createNamedQuery("LocationEntry.getByInternalName").setParameter("internalName", id).getSingleResult();
                } catch (NoResultException e) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, null, "member_not_found");
                }
                break;
            }
            case CODIGO_UBICACION: {
               try {
                    locationEntry = (LocationEntry) em.createNamedQuery("LocationEntry.getByInternalCode").setParameter("internalCode", id).getSingleResult();
                } catch (NoResultException e) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, null, "member_not_found");
                }
                break;
            }
            case UUID: {
                locationEntry = em.find(LocationEntry.class, id);
                break;
            }
            default: {
                locationEntry = null;
            }
        }
        if (locationEntry==null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, null, "member_not_found");
        }
        
        return locationEntry;
    }
}
