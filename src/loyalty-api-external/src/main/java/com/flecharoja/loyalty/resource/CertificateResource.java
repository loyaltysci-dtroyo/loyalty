package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.service.PremioBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 * @author svargas
 */
@Api(value = "Prize")
@Path("prize/{codePrize}/certificate/{codeCertificate}")
public class CertificateResource {
    
    @Context
    HttpServletRequest request;
    
    @EJB
    PremioBean bean;
    
    @PathParam("codePrize")
    String codePrize;
    
    @PathParam("codeCertificate")
    String codeCertificate;

    @ApiOperation(value = "Set as delivered a certificate from a prize")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "codePrize", value = "Prize code", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "codeCertificate", value = "Certificate code", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Path("set-as-delivered")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void setAsDelivered() {
        bean.entregarPremioCertificado(codePrize, codeCertificate, request.getLocale());
    }
}
