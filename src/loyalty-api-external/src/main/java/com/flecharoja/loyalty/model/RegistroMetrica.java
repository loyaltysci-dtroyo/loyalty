package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "REGISTRO_METRICA")
@ApiModel(value = "metricEntry")
public class RegistroMetrica implements Serializable {
    
    public enum TiposGane {
        BONIFICACION_EXTRA("E"),
        BIENVENIDA("B"),
        TRANSACCION("T"),
        REVERSION("V");
        
        private final String value;
        private static final Map<String, TiposGane> lookup = new HashMap<>();

        private TiposGane(String value) {
            this.value = value;
        }
        
        static {
            //puesta estatica de Tipos de gane elegibles
            lookup.put(BONIFICACION_EXTRA.value, BONIFICACION_EXTRA);
            lookup.put(TRANSACCION.value, TRANSACCION);
            lookup.put(REVERSION.value, REVERSION);
        }
        
        public static TiposGane get(String value) {
            return value == null ? null : lookup.get(value);
        }
    }
    
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    @ApiModelProperty(hidden = true)
    protected RegistroMetricaPK registroMetricaPK;
    
    @Column(name = "ID_METRICA")
    @ApiModelProperty(name = "idMetric", value = "Metric identification, if it is not specified... the main metric of the system is used")
    private String idMetrica;
    
    @Column(name = "ID_MIEMBRO")
    @ApiModelProperty(hidden = true)
    private String idMiembro;
    
    @Column(name = "IND_TIPO_GANE")
    @ApiModelProperty(name = "indType", value = "Indicator about the type of entry, if it is not specified... the type TRANSACTION will be used")
    private String indTipoGane;

    @Column(name = "CANTIDAD")
    @ApiModelProperty(name = "amount", required = true, value = "The amount to sum (if positive) or substract (if negative)")
    private Double cantidad;
    
    @Column(name = "ID_TRANSACCION")
    @ApiModelProperty(hidden = true)
    private String idTransaccion;
    
    @Column(name = "DISPONIBLE_ACTUAL")
    @ApiModelProperty(hidden = true)
    private Double disponibleActual;

    public RegistroMetrica() {
    }

    public RegistroMetrica(Date fecha, String idMetrica, String idMiembro, TiposGane tipoGane, Double cantidad, String idTransaccion) {
        this.registroMetricaPK = new RegistroMetricaPK(fecha, null);
        this.idMetrica = idMetrica;
        this.idMiembro = idMiembro;
        this.indTipoGane = tipoGane.value;
        this.cantidad = cantidad;
        this.idTransaccion = idTransaccion;
    }

    public RegistroMetricaPK getRegistroMetricaPK() {
        return registroMetricaPK;
    }

    public void setRegistroMetricaPK(RegistroMetricaPK registroMetricaPK) {
        this.registroMetricaPK = registroMetricaPK;
    }

    public String getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIndTipoGane() {
        return indTipoGane;
    }

    public void setIndTipoGane(String indTipoGane) {
        this.indTipoGane = indTipoGane;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Double getDisponibleActual() {
        return disponibleActual;
    }

    public void setDisponibleActual(Double disponibleActual) {
        this.disponibleActual = disponibleActual;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (registroMetricaPK != null ? registroMetricaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegistroMetrica)) {
            return false;
        }
        RegistroMetrica other = (RegistroMetrica) object;
        if ((this.registroMetricaPK == null && other.registroMetricaPK != null) || (this.registroMetricaPK != null && !this.registroMetricaPK.equals(other.registroMetricaPK))) {
            return false;
        }
        return true;
    }
}