package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "GRUPO_MIEMBRO")
@NamedQueries(
        @NamedQuery(name = "GrupoMiembro.getIdsGrupoByIdMiembro", query = "SELECT g.grupoMiembroPK.idGrupo FROM GrupoMiembro g WHERE g.grupoMiembroPK.idMiembro = :idMiembro")
)
public class GrupoMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GrupoMiembroPK grupoMiembroPK;

    public GrupoMiembro() {
    }

    public GrupoMiembroPK getGrupoMiembroPK() {
        return grupoMiembroPK;
    }

    public void setGrupoMiembroPK(GrupoMiembroPK grupoMiembroPK) {
        this.grupoMiembroPK = grupoMiembroPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (grupoMiembroPK != null ? grupoMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupoMiembro)) {
            return false;
        }
        GrupoMiembro other = (GrupoMiembro) object;
        if ((this.grupoMiembroPK == null && other.grupoMiembroPK != null) || (this.grupoMiembroPK != null && !this.grupoMiembroPK.equals(other.grupoMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.GrupoMiembro[ grupoMiembroPK=" + grupoMiembroPK + " ]";
    }
    
}
