package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO_LISTA_MIEMBRO")
public class PremioListaMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PremioListaMiembroPK premioListaMiembroPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
   

    public PremioListaMiembro() {
    }
    
    public PremioListaMiembroPK getPremioListaMiembroPK() {
        return premioListaMiembroPK;
    }

    public void setPremioListaMiembroPK(PremioListaMiembroPK premioListaMiembroPK) {
        this.premioListaMiembroPK = premioListaMiembroPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (premioListaMiembroPK != null ? premioListaMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PremioListaMiembro)) {
            return false;
        }
        PremioListaMiembro other = (PremioListaMiembro) object;
        if ((this.premioListaMiembroPK == null && other.premioListaMiembroPK != null) || (this.premioListaMiembroPK != null && !this.premioListaMiembroPK.equals(other.premioListaMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.PremioListaMiembro[ premioListaMiembroPK=" + premioListaMiembroPK + " ]";
    }
    
}
