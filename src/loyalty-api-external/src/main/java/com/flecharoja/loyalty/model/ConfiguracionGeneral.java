package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "CONFIGURACION_GENERAL")
@NamedQueries({
    @NamedQuery(name = "ConfiguracionGeneral.findAll", query = "SELECT c FROM ConfiguracionGeneral c")
})
public class ConfiguracionGeneral implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID")
    private Long id;
    
    @Column(name = "PORCENTAJE_COMPRA")
    private Long porcentajeCompra;
    
    @Column(name = "BONO_ENTRADA")
    private Double bonoEntrada;
    
    @Column(name = "ID_METRICA_INICIAL")
    private String idMetricaPrincipal;
    
    @Column(name = "UBICACION_PRINCIPAL")
    private String ubicacionPrincipal;
    
    @Column(name = "INTEGRACION_INVENTARIO_PREMIO")
    private Boolean integracionInventarioPremios;
    
    @Column(name = "PERIODO")
    private Integer periodo;
    
    @Column(name = "MES")
    private Integer mes;

    public ConfiguracionGeneral() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPorcentajeCompra() {
        return porcentajeCompra;
    }

    public void setPorcentajeCompra(Long porcentajeCompra) {
        this.porcentajeCompra = porcentajeCompra;
    }

    public Double getBonoEntrada() {
        return bonoEntrada;
    }

    public void setBonoEntrada(Double bonoEntrada) {
        this.bonoEntrada = bonoEntrada;
    }

    public String getIdMetricaPrincipal() {
        return idMetricaPrincipal;
    }

    public void setIdMetricaPrincipal(String idMetricaPrincipal) {
        this.idMetricaPrincipal = idMetricaPrincipal;
    }

    public String getUbicacionPrincipal() {
        return ubicacionPrincipal;
    }

    public void setUbicacionPrincipal(String ubicacionPrincipal) {
        this.ubicacionPrincipal = ubicacionPrincipal;
    }

    public Boolean getIntegracionInventarioPremios() {
        return integracionInventarioPremios;
    }

    public void setIntegracionInventarioPremios(Boolean integracionInventarioPremios) {
        this.integracionInventarioPremios = integracionInventarioPremios;
    }

    public Integer getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Integer periodo) {
        this.periodo = periodo;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfiguracionGeneral)) {
            return false;
        }
        ConfiguracionGeneral other = (ConfiguracionGeneral) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ConfiguracionGeneral[ id=" + id + " ]";
    }
}
