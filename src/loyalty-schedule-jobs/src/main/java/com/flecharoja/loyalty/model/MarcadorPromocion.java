package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MARCADOR_PROMOCION")
@NamedQueries({
    @NamedQuery(name = "MarcadorPromocion.countAllByIdPromocion", query = "SELECT COUNT(m) FROM MarcadorPromocion m WHERE m.marcadorPromocionPK.idPromocion = :idPromocion")
})
public class MarcadorPromocion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MarcadorPromocionPK marcadorPromocionPK;

    public MarcadorPromocion() {
    }

    public MarcadorPromocion(String idPromocion, String idMiembro) {
        this.marcadorPromocionPK = new MarcadorPromocionPK(idPromocion, idMiembro);
    }

    public MarcadorPromocionPK getMarcadorPromocionPK() {
        return marcadorPromocionPK;
    }

    public void setMarcadorPromocionPK(MarcadorPromocionPK marcadorPromocionPK) {
        this.marcadorPromocionPK = marcadorPromocionPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (marcadorPromocionPK != null ? marcadorPromocionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarcadorPromocion)) {
            return false;
        }
        MarcadorPromocion other = (MarcadorPromocion) object;
        if ((this.marcadorPromocionPK == null && other.marcadorPromocionPK != null) || (this.marcadorPromocionPK != null && !this.marcadorPromocionPK.equals(other.marcadorPromocionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MarcadorPromocion[ marcadorPromocionPK=" + marcadorPromocionPK + " ]";
    }
    
}
