package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "DATA_MISIONES_DIARIO")
@NamedQueries({
    @NamedQuery(name = "DataMisionesDiario.deleteAllByMes", query = "DELETE FROM DataMisionesDiario d WHERE d.dataMisionesDiarioPK.mes = :mes")
})
public class DataMisionesDiario implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DataMisionesDiarioPK dataMisionesDiarioPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANT_ELEGIBLES")
    private Long cantElegibles;

    public DataMisionesDiario() {
    }

    public DataMisionesDiario(Integer dia, Integer mes, String idMision) {
        this.dataMisionesDiarioPK = new DataMisionesDiarioPK(dia, mes, idMision);
    }

    public DataMisionesDiarioPK getDataMisionesDiarioPK() {
        return dataMisionesDiarioPK;
    }

    public void setDataMisionesDiarioPK(DataMisionesDiarioPK dataMisionesDiarioPK) {
        this.dataMisionesDiarioPK = dataMisionesDiarioPK;
    }

    public Long getCantElegibles() {
        return cantElegibles;
    }

    public void setCantElegibles(Long cantElegibles) {
        this.cantElegibles = cantElegibles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dataMisionesDiarioPK != null ? dataMisionesDiarioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DataMisionesDiario)) {
            return false;
        }
        DataMisionesDiario other = (DataMisionesDiario) object;
        if ((this.dataMisionesDiarioPK == null && other.dataMisionesDiarioPK != null) || (this.dataMisionesDiarioPK != null && !this.dataMisionesDiarioPK.equals(other.dataMisionesDiarioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.DataMisionesDiario[ dataMisionesDiarioPK=" + dataMisionesDiarioPK + " ]";
    }
    
}
