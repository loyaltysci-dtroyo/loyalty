package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION")
@NamedQueries({
    @NamedQuery(name = "Mision.findAll", query = "SELECT m FROM Mision m"),
    @NamedQuery(name = "Mision.getIdMisionByIndEstado", query = "SELECT m.idMision FROM Mision m WHERE m.indEstado = :indEstado")
})
public class Mision implements Serializable {
    
    public enum Tipos {
        ENCUESTA('E'),
        PERFIL('P'),
        VER_CONTENIDO('V'),
        SUBIR_CONTENIDO('S'),
        RED_SOCIAL('R'),
        JUEGO_PREMIO('J'),
        PREFERENCIAS('A');

        private final char value;
        private static final Map<Character, Tipos> lookup = new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }

        static {
            for (Tipos tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static Tipos get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }
    public enum Estados {
        PUBLICADO('A'),
        ARCHIVADO('B'),
        BORRADOR('C'),
        INACTIVO('D');

        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }

        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Estados get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "mision_uuid")
    @GenericGenerator(name = "mision_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_MISION")
    private String idMision;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_MISION")
    private Character indTipoMision;

    public Mision() {
    }

    public String getIdMision() {
        return idMision;
    }

    public Character getIndTipoMision() {
        return indTipoMision;
    }

    public void setIndTipoMision(Character indTipoMision) {
        this.indTipoMision = indTipoMision;
    }
    
    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMision != null ? idMision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mision)) {
            return false;
        }
        Mision other = (Mision) object;
        if ((this.idMision == null && other.idMision != null) || (this.idMision != null && !this.idMision.equals(other.idMision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Mision[ idMision=" + idMision + " ]";
    }

}
