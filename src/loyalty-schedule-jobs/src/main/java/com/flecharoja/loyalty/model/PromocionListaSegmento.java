package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PROMOCION_LISTA_SEGMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PromocionListaSegmento.findAllIdSegmentoByIdPromocionIndTipo", query = "SELECT p.promocionListaSegmentoPK.idSegmento FROM PromocionListaSegmento p WHERE p.promocionListaSegmentoPK.idPromocion = :idPromocion AND p.indTipo = :indTipo"),
})
public class PromocionListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PromocionListaSegmentoPK promocionListaSegmentoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;

    public PromocionListaSegmento() {
    }

    public PromocionListaSegmentoPK getPromocionListaSegmentoPK() {
        return promocionListaSegmentoPK;
    }

    public void setPromocionListaSegmentoPK(PromocionListaSegmentoPK promocionListaSegmentoPK) {
        this.promocionListaSegmentoPK = promocionListaSegmentoPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo!=null?Character.toUpperCase(indTipo):null;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (promocionListaSegmentoPK != null ? promocionListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromocionListaSegmento)) {
            return false;
        }
        PromocionListaSegmento other = (PromocionListaSegmento) object;
        if ((this.promocionListaSegmentoPK == null && other.promocionListaSegmentoPK != null) || (this.promocionListaSegmentoPK != null && !this.promocionListaSegmentoPK.equals(other.promocionListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.PromocionListaSegmento[ promocionListaSegmentoPK=" + promocionListaSegmentoPK + " ]";
    }
    
}
