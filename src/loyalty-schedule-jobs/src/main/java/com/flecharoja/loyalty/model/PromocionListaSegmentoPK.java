/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author wtencio
 */
@Embeddable
public class PromocionListaSegmentoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "ID_SEGMENTO")
    private String idSegmento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "ID_PROMOCION")
    private String idPromocion;

    public PromocionListaSegmentoPK() {
    }

    public PromocionListaSegmentoPK(String idSegmento, String idPromocion) {
        this.idSegmento = idSegmento;
        this.idPromocion = idPromocion;
    }

    public String getIdSegmento() {
        return idSegmento;
    }

    public void setIdSegmento(String idSegmento) {
        this.idSegmento = idSegmento;
    }

    public String getIdPromocion() {
        return idPromocion;
    }

    public void setIdPromocion(String idPromocion) {
        this.idPromocion = idPromocion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSegmento != null ? idSegmento.hashCode() : 0);
        hash += (idPromocion != null ? idPromocion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromocionListaSegmentoPK)) {
            return false;
        }
        PromocionListaSegmentoPK other = (PromocionListaSegmentoPK) object;
        if ((this.idSegmento == null && other.idSegmento != null) || (this.idSegmento != null && !this.idSegmento.equals(other.idSegmento))) {
            return false;
        }
        if ((this.idPromocion == null && other.idPromocion != null) || (this.idPromocion != null && !this.idPromocion.equals(other.idPromocion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.PromocionListaSegmentoPK[ idSegmento=" + idSegmento + ", idPromocion=" + idPromocion + " ]";
    }
    
}
