package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "DATA_PROMOS_DIARIO")
@NamedQueries({
    @NamedQuery(name = "DataPromosDiario.deleteAllByMes", query = "DELETE FROM DataPromosDiario d WHERE d.dataPromosDiarioPK.mes = :mes")
})
public class DataPromosDiario implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DataPromosDiarioPK dataPromosDiarioPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANT_MARCAS")
    private Long cantMarcas;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANT_ELEGIBLES")
    private Long cantElegibles;

    public DataPromosDiario() {
    }

    public DataPromosDiario(Integer dia, Integer mes, String idPromocion) {
        this.dataPromosDiarioPK = new DataPromosDiarioPK(dia, mes, idPromocion);
    }

    public DataPromosDiarioPK getDataPromosDiarioPK() {
        return dataPromosDiarioPK;
    }

    public void setDataPromosDiarioPK(DataPromosDiarioPK dataPromosDiarioPK) {
        this.dataPromosDiarioPK = dataPromosDiarioPK;
    }

    public Long getCantMarcas() {
        return cantMarcas;
    }

    public void setCantMarcas(Long cantMarcas) {
        this.cantMarcas = cantMarcas;
    }

    public Long getCantElegibles() {
        return cantElegibles;
    }

    public void setCantElegibles(Long cantElegibles) {
        this.cantElegibles = cantElegibles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dataPromosDiarioPK != null ? dataPromosDiarioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DataPromosDiario)) {
            return false;
        }
        DataPromosDiario other = (DataPromosDiario) object;
        if ((this.dataPromosDiarioPK == null && other.dataPromosDiarioPK != null) || (this.dataPromosDiarioPK != null && !this.dataPromosDiarioPK.equals(other.dataPromosDiarioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.DataPromosDiario[ dataPromosDiarioPK=" + dataPromosDiarioPK + " ]";
    }
    
}
