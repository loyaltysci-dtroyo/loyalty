package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_LISTA_SEGMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MisionListaSegmento.getAllIdSegmentosByIdMisionIndTipo", query = "SELECT m.misionListaSegmentoPK.idSegmento FROM MisionListaSegmento m WHERE m.misionListaSegmentoPK.idMision = :idMision AND m.indTipo = :indTipo")
})
public class MisionListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MisionListaSegmentoPK misionListaSegmentoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;

    public MisionListaSegmento() {
    }

    public MisionListaSegmentoPK getMisionListaSegmentoPK() {
        return misionListaSegmentoPK;
    }

    public void setMisionListaSegmentoPK(MisionListaSegmentoPK misionListaSegmentoPK) {
        this.misionListaSegmentoPK = misionListaSegmentoPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo == null ? null : Character.toUpperCase(indTipo);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (misionListaSegmentoPK != null ? misionListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionListaSegmento)) {
            return false;
        }
        MisionListaSegmento other = (MisionListaSegmento) object;
        if ((this.misionListaSegmentoPK == null && other.misionListaSegmentoPK != null) || (this.misionListaSegmentoPK != null && !this.misionListaSegmentoPK.equals(other.misionListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionListaSegmento[ misionListaSegmentoPK=" + misionListaSegmentoPK + " ]";
    }
    
}
