package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NIVEL_METRICA")
@NamedQueries({
    @NamedQuery(name = "NivelMetrica.getIdNivelMetricaInicialByIdGrupoNivel", query = "SELECT n.idNivel, n.metricaInicial FROM NivelMetrica n WHERE n.idGrupoNivel = :idGrupoNivel")
})
public class NivelMetrica implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_NIVEL")
    private String idNivel;
    
    @Column(name = "METRICA_INICIAL")
    private Double metricaInicial;
    
    @Column(name = "GRUPO_NIVELES")
    private String idGrupoNivel;  
  
    public NivelMetrica() {
    }

    public String getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(String idNivel) {
        this.idNivel = idNivel;
    }

    public Double getMetricaInicial() {
        return metricaInicial;
    }

    public void setMetricaInicial(Double metricaInicial) {
        if (metricaInicial==null) {
            this.metricaInicial = 0d;
        } else {
            this.metricaInicial = metricaInicial;
        }
    }

    public String getIdGrupoNivel() {
        return idGrupoNivel;
    }

    public void setIdGrupoNivel(String idGrupoNivel) {
        this.idGrupoNivel = idGrupoNivel;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivel != null ? idNivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NivelMetrica)) {
            return false;
        }
        NivelMetrica other = (NivelMetrica) object;
        if ((this.idNivel == null && other.idNivel != null) || (this.idNivel != null && !this.idNivel.equals(other.idNivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NivelMetrica{ idNivel=" + idNivel + '}';
    }
    
}
