package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author wtencio, svargas
 */
@Entity
@Table(name = "MIEMBRO")
@NamedQueries({
    @NamedQuery(name = "Miembro.countByMiembroReferenteIsNotNull", query = "SELECT COUNT(m) FROM Miembro m WHERE m.idMiembroReferente IS NOT NULL"),
    @NamedQuery(name = "Miembro.findAllIdMiembro", query = "SELECT m.idMiembro FROM Miembro m"),
    @NamedQuery(name = "Miembro.findAll", query = "SELECT m FROM Miembro m"),
    @NamedQuery(name = "Miembro.findAllByIndEstado", query = "SELECT m FROM Miembro m WHERE m.indEstadoMiembro = :indEstado")
})
public class Miembro implements Serializable {

    public enum Estados {
        ACTIVO('A'),
        INACTIVO('I');

        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }

        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Estados get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }
    
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO_MIEMBRO")
    private Character indEstadoMiembro;
    
    @Column(name = "FECHA_NACIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;
    
    @Column(name = "FECHA_EXPIRACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaExpiracion;
    
    @Column(name = "MIEMBRO_REFERENTE")
    private String idMiembroReferente;

    public Miembro() {
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Character getIndEstadoMiembro() {
        return indEstadoMiembro;
    }

    public void setIndEstadoMiembro(Character indEstadoMiembro) {
        this.indEstadoMiembro = indEstadoMiembro != null ? Character.toUpperCase(indEstadoMiembro) : null;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public String getIdMiembroReferente() {
        return idMiembroReferente;
    }

    public void setIdMiembroReferente(String idMiembroReferente) {
        this.idMiembroReferente = idMiembroReferente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Miembro)) {
            return false;
        }
        Miembro other = (Miembro) object;
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Miembro[ idMiembro=" + idMiembro + " ]";
    }
}
