/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "ESTADISTICA_SEGMENTO_GENERAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadisticaSegmentoGeneral.findAll", query = "SELECT e FROM EstadisticaSegmentoGeneral e")
    , @NamedQuery(name = "EstadisticaSegmentoGeneral.findByCantMiembrosSegmentados", query = "SELECT e FROM EstadisticaSegmentoGeneral e WHERE e.cantMiembrosSegmentados = :cantMiembrosSegmentados")
    , @NamedQuery(name = "EstadisticaSegmentoGeneral.findByCantMiembrosNoSegmentados", query = "SELECT e FROM EstadisticaSegmentoGeneral e WHERE e.cantMiembrosNoSegmentados = :cantMiembrosNoSegmentados")
    , @NamedQuery(name = "EstadisticaSegmentoGeneral.findById", query = "SELECT e FROM EstadisticaSegmentoGeneral e WHERE e.id = :id")})
public class EstadisticaSegmentoGeneral implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "CANT_MIEMBROS_SEGMENTADOS")
    private Integer cantMiembrosSegmentados;
    @Column(name = "CANT_MIEMBROS_NO_SEGMENTADOS")
    private Integer cantMiembrosNoSegmentados;

    public EstadisticaSegmentoGeneral() {
    }

    public EstadisticaSegmentoGeneral(Integer id) {
        this.id = id;
    }

    public Integer getCantMiembrosSegmentados() {
        return cantMiembrosSegmentados;
    }

    public void setCantMiembrosSegmentados(Integer cantMiembrosSegmentados) {
        this.cantMiembrosSegmentados = cantMiembrosSegmentados;
    }

    public Integer getCantMiembrosNoSegmentados() {
        return cantMiembrosNoSegmentados;
    }

    public void setCantMiembrosNoSegmentados(Integer cantMiembrosNoSegmentados) {
        this.cantMiembrosNoSegmentados = cantMiembrosNoSegmentados;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadisticaSegmentoGeneral)) {
            return false;
        }
        EstadisticaSegmentoGeneral other = (EstadisticaSegmentoGeneral) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.EstadisticaSegmentoGeneral[ id=" + id + " ]";
    }

}
