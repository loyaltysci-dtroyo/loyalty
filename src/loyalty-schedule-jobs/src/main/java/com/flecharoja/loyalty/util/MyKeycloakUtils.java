package com.flecharoja.loyalty.util;

import java.io.StringReader;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author svargas, faguilar
 */
public class MyKeycloakUtils implements AutoCloseable{
    
    private final String CLIENT_SECRET = "158a44a9-3555-4278-8c38-8f805619791b";
    private final String CLIENT_ID = "loyalty-external";
    private final String ADMIN_URL = "https://idp-demos.loyaltysci.com/auth/admin/realms/loyalty";
    private final String TOKEN_ENDPOINT = "https://idp-demos.loyaltysci.com/auth/realms/loyalty/protocol/openid-connect/token";
    private final String LOGOUT_ENDPOINT = "https://idp-demos.loyaltysci.com/auth/realms/loyalty/protocol/openid-connect/logout";
    
    private final JsonObject keycloakSession;
    Locale locale;
    public MyKeycloakUtils() {
        Client client = ClientBuilder.newClient();
        try {
            String body = "client_id="+CLIENT_ID
                    +"&client_secret="+CLIENT_SECRET
                    +"&grant_type=client_credentials";
            Response response = client.target(TOKEN_ENDPOINT).request().post(Entity.entity(body, MediaType.APPLICATION_FORM_URLENCODED));
            if (response.getStatus()!=200) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, response.getStatus());
            }

            this.keycloakSession = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();
        } finally {
            client.close();
        }
    }
 
    public void enableDisableMember(String id, boolean action) {
        Client client = ClientBuilder.newClient();
        try {
            String body = Json.createObjectBuilder()
                    .add("id", id)
                    .add("enabled", action)
                    .build().toString();
            Response response = client.target(ADMIN_URL+"/users/"+id).request().header("Authorization", "bearer "+keycloakSession.getString("access_token")).put(Entity.entity(body, MediaType.APPLICATION_JSON));
            if (response.getStatus()!=204) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, response.getStatus());
            }
        } finally {
            client.close();
        }
    }

    @Override
    public void close() {
        Client client = ClientBuilder.newClient();
        try {
            String body = "client_id="+CLIENT_ID
                    +"&client_secret="+CLIENT_SECRET
                    +"&refresh_token="+keycloakSession.getString("refresh_token");
            Response response = client.target(LOGOUT_ENDPOINT).request().post(Entity.entity(body, MediaType.APPLICATION_FORM_URLENCODED));
            if (response.getStatus()!=204) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, response.getStatus());
            }
        } finally {
            client.close();
        }
    }
}
