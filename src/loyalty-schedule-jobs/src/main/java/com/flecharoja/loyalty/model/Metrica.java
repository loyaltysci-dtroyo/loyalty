package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas, wtencio
 */
@Entity
@Table(name = "METRICA")
@NamedQueries({
    @NamedQuery(name = "Metrica.getIdMetricaIdGrupoNivelByIndEstado", query = "SELECT m.idMetrica, m.idGrupoNivel FROM Metrica m WHERE m.indEstado = :indEstado AND m.idGrupoNivel IS NOT NULL"),
    @NamedQuery(name = "Metrica.findAllByIndExpiracion", query = "SELECT m FROM Metrica m WHERE m.indExpiracion = :indExpiracion"),
})
public class Metrica implements Serializable {
    
    public enum Estados {
        PUBLICADO('P'),
        ARCHIVADO('A'),
        BORRADOR('B');

        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }

        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Estados get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_METRICA")
    private String idMetrica;
    
    @Column(name = "IND_EXPIRACION")
    private Boolean indExpiracion;

    @Column(name = "DIAS_VENCIMIENTO")
    private Long diasVencimiento;
    
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Column(name = "GRUPO_NIVELES")
    private String idGrupoNivel;

    public Metrica() {
    }

    public String getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    public Long getDiasVencimiento() {
        return diasVencimiento;
    }

    public void setDiasVencimiento(Long diasVencimiento) {
        this.diasVencimiento = diasVencimiento;
    }

    public Boolean getIndExpiracion() {
        return indExpiracion;
    }

    public void setIndExpiracion(Boolean indExpiracion) {
        this.indExpiracion = indExpiracion;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    public String getIdGrupoNivel() {
        return idGrupoNivel;
    }

    public void setIdGrupoNivel(String idGrupoNivel) {
        this.idGrupoNivel = idGrupoNivel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMetrica != null ? idMetrica.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Metrica)) {
            return false;
        }
        Metrica other = (Metrica) object;
        if ((this.idMetrica == null && other.idMetrica != null) || (this.idMetrica != null && !this.idMetrica.equals(other.idMetrica))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Metrica[ idMetrica=" + idMetrica + " ]";
    }
    
}
