package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.BalanceMetricaMensual;
import com.flecharoja.loyalty.model.BalanceMetricaMensualPK;
import com.flecharoja.loyalty.model.CertificadoCategoria;
import com.flecharoja.loyalty.model.CodigoCertificado;
import com.flecharoja.loyalty.model.CodigoCertificadoPK;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.ExistenciasUbicacion;
import com.flecharoja.loyalty.model.ExistenciasUbicacionPK;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.MiembroBalanceMetrica;
import com.flecharoja.loyalty.model.MiembroBalanceMetricaPK;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioMiembro;
import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.model.TransCarrito;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Schedule;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import servicesUtils.TransCarritoBean;
import servicesUtils.TransProductoBean;

/**
 *
 * @author svargas
 */
@Singleton
public class EventsBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;
    
    @EJB
    TransProductoBean transProductoBean;
    
    @EJB
    TransCarritoBean transCarritoBean;

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    private final Configuration hbaseConf;

    private final Properties sparkConf;

    public EventsBean() throws IOException {
        this.hbaseConf = new Configuration();
        this.hbaseConf.addResource("conf/hbase/hbase-site.xml");

        this.sparkConf = new Properties();
        this.sparkConf.load(EventsBean.class.getResourceAsStream("/conf/spark/spark-cluster.properties"));
    }

    /**
     * Verificacion de eventos diarios para la detonacion de los mismos
     * 
     * @throws IOException 
     */
    @Schedule(hour = "12", info = "EVENTOS_FECHA")
    public void checkEventosFecha() throws IOException {
        Calendar fecha = Calendar.getInstance();

        //lectura de miembros activos
        List<Miembro> miembros = em.createNamedQuery("Miembro.findAllByIndEstado").setParameter("indEstado", Miembro.Estados.ACTIVO.getValue()).getResultList();
        //filtrado de miembros que cumplan años
        miembros.stream().filter((miembro) -> {
            if (miembro.getFechaNacimiento() == null) {
                return false;
            }
            Calendar cumple = Calendar.getInstance();
            cumple.setTime(miembro.getFechaNacimiento());
            if ((cumple.get(Calendar.MONTH) == Calendar.FEBRUARY && cumple.get(Calendar.DAY_OF_MONTH) == 29) && (fecha.get(Calendar.MONTH) == Calendar.FEBRUARY && fecha.get(Calendar.DAY_OF_MONTH) == 28)) {
                return true;
            } else {
                return fecha.get(Calendar.MONTH) == cumple.get(Calendar.MONTH) && fecha.get(Calendar.DAY_OF_MONTH) == cumple.get(Calendar.DAY_OF_MONTH);
            }
        }).forEach((miembro) -> {
            //detone de eventos
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.CUMPLEANOS, null, miembro.getIdMiembro());
        });

        //filtrado de miembros con aniversario
        miembros.stream().filter((miembro) -> {
            Calendar ingreso = Calendar.getInstance();
            ingreso.setTime(miembro.getFechaIngreso());
            return fecha.get(Calendar.DAY_OF_YEAR) == ingreso.get(Calendar.DAY_OF_YEAR);
        }).forEach((miembro) -> {
            //detone de evento
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.ANIVERSARIO_PROGRAMA, null, miembro.getIdMiembro());
        });
    }

    /**
     * expiracion de metrica
     */
    @Schedule(info = "EXPIRACION_METRICA")
    public void expireMetrica() {
        //obtencion de metricas con indicador de expiracion en verdadero
        List<Metrica> metricas = em.createNamedQuery("Metrica.findAllByIndExpiracion").setParameter("indExpiracion", true).getResultList();

        //busqueda en registros de HBASE
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "REGISTRO_METRICA"));
            for (Metrica metrica : metricas) {
                //definicion de fecha final de busqueda de registros
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -metrica.getDiasVencimiento().intValue());
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
                calendar.set(Calendar.MILLISECOND, 999);

                Scan scan = new Scan();
                //filtrado de registros que no esten marcados como vencidos
                scan.setFilter(new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("VENCIDO"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes(false)));
                scan.setStopRow(Bytes.toBytes(String.format("%013d", calendar.getTimeInMillis()) + "&ffffffff-ffff-ffff-ffff-ffffffffffff"));

                //actualizacion de balance de metrica y registros
                List<Put> registrosVencidos = new ArrayList<>();
                Map<String, Double> temp = new HashMap<>();
                for (Result result : table.getScanner(scan)) {
                    registrosVencidos.add(new Put(result.getRow()).addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("VENCIDO"), Bytes.toBytes(true)));
                    String idMiembro = Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("MIEMBRO")));
                    if (Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("DISPONIBLE"))) > 0) {
                        MiembroBalanceMetrica balanceMiembro = em.find(MiembroBalanceMetrica.class, new MiembroBalanceMetricaPK(idMiembro, metrica.getIdMetrica()));
                        balanceMiembro.setDisponible(balanceMiembro.getDisponible() - Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("DISPONIBLE"))));
                        balanceMiembro.setVencido(balanceMiembro.getVencido() + Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("DISPONIBLE"))));
                        em.merge(balanceMiembro);
                        
                        LocalDate localDate = LocalDate.now();
                        BalanceMetricaMensual balanceMetricaMensual = em.find(BalanceMetricaMensual.class, new BalanceMetricaMensualPK(localDate.getYear(), localDate.getMonthValue(), metrica.getIdMetrica()));
                        if (balanceMetricaMensual == null) {
                            balanceMetricaMensual = new BalanceMetricaMensual(localDate.getYear(), localDate.getMonthValue(), metrica.getIdMetrica());
                        }
                        balanceMetricaMensual.setVencido(balanceMetricaMensual.getVencido()+Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("DISPONIBLE"))));
                        em.merge(balanceMetricaMensual);
                        
                        temp.merge(metrica.getIdMetrica()+"&"+idMiembro, Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("DISPONIBLE"))), Double::sum);
                    }
                }
                if (!registrosVencidos.isEmpty()) {
                    temp.entrySet().stream().forEach((entry) -> {
                        em.merge(new RegistroMetrica(entry.getKey().split("&")[0], entry.getKey().split("&")[1], RegistroMetrica.TiposGane.VENCIMIENTO, entry.getValue()));
                    });
                    
                    em.flush();
                    table.put(registrosVencidos);
                }
            }
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
        }
    }

    /**
     * actualizacion de segmentos
     */
    @Schedule(info = "ACTUALIZACION_SEGMENTOS")
    public void triggerActualizacionSegmentos() {
        //invocacion de API REST del cluster standalone de Apache Spark
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://" + sparkConf.getProperty("masterRestHost"))
                .path("v1/submissions/create");
        Invocation.Builder request = target.request();

        //definicion de peticion de detone de trabajo
        JsonObjectBuilder body = Json.createObjectBuilder();
        body.add("action", "CreateSubmissionRequest");
        body.add("appArgs", Json.createArrayBuilder());
        body.add("appResource", "file:" + sparkConf.getProperty("loyaltyJarDir"));
        body.add("clientSparkVersion", "2.1.0");
        body.add("environmentVariables", Json.createObjectBuilder().add("SPARK_ENV_LOADED", "1"));
        body.add("mainClass", "com.flecharoja.loyalty.service.RecalculoSegmentos");
        body.add("sparkProperties", Json.createObjectBuilder()
                .add("spark.app.name", "")
                .add("spark.submit.deployMode", "cluster")
                .add("spark.jars", "file:" + sparkConf.getProperty("loyaltyJarDir"))
                .add("spark.driver.supervise", "false")
                .add("spark.eventLog.enabled", "false")
                .add("spark.master", "spark://" + sparkConf.getProperty("masterRestHost"))
        );

        try {
            request.post(Entity.entity(body.build(), MediaType.APPLICATION_JSON));
        } catch (ResponseProcessingException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
        }
    }

    /**
     * expiracion de miembros por fecha de expiracion definida
     */
    @Schedule(info = "EXPIRACION_MIEMBROS")
    public void expireMiembros() {
        //obtencion de miembros expirados
        List<Miembro> miembros = em.createNamedQuery("Miembro.findAllByIndEstado").setParameter("indEstado", Miembro.Estados.ACTIVO.getValue()).getResultList();
        Calendar calendar = Calendar.getInstance();
        Calendar expiracionMiembro = Calendar.getInstance();
        for (Miembro miembro : miembros) {
            //verificacion de fecha de expiracion de cada miembro
            if (miembro.getFechaExpiracion() != null) {
                expiracionMiembro.setTime(miembro.getFechaExpiracion());
                if (calendar.get(Calendar.YEAR) == expiracionMiembro.get(Calendar.YEAR)
                        && calendar.get(Calendar.MONTH) == expiracionMiembro.get(Calendar.MONTH)
                        && calendar.get(Calendar.DAY_OF_MONTH) == expiracionMiembro.get(Calendar.DAY_OF_MONTH)) {
                    miembro.setIndEstadoMiembro(Miembro.Estados.INACTIVO.getValue());
                    try (MyKeycloakUtils keycloak = new MyKeycloakUtils()) {
                        keycloak.enableDisableMember(miembro.getIdMiembro(), Boolean.FALSE);
                    }
                    em.merge(miembro);
                }
            }
        }
    }
    
    /**
     * expiracion de certificados por fecha de expiración
     */
    @Schedule(hour = "*", minute = "0,5,10,15,20,25,30,35,40,45,50,55" , info = "EXPIRACION_CERTIFICADOS")
    public void expireCertificados() {
        Calendar fechaInicio = Calendar.getInstance();
        fechaInicio.add(Calendar.DAY_OF_MONTH, -1);
        Date fechaInicioFiltro = fechaInicio.getTime();
        //obtencion de certificados expirados
        List<CertificadoCategoria> certificados = em.createNamedQuery("CertificadoCategoria.findAllAsigned").setParameter("estado", 'A').setParameter("fechaInicioFiltro", fechaInicioFiltro).getResultList();
        
        Calendar calendar = Calendar.getInstance();
        Calendar expiracionCertificado = Calendar.getInstance();
        for (CertificadoCategoria certificado : certificados) {
            //verificacion de fecha de expiracion de cada miembro
            if (certificado.getFechaExpiracion() != null) {
                expiracionCertificado.setTime(certificado.getFechaExpiracion());
                if (calendar.getTimeInMillis() >= expiracionCertificado.getTimeInMillis()) {
                    TransCarrito carrito = transCarritoBean.getTransCarritoByCertificado(certificado.getIdCertificado(), certificado.getIdCategoria());
                    //System.out.print(carrito.getEstado() + " - " + (carrito.getEstado() == 'P'));
                    if (carrito.getEstado() == 'P') {
                        transProductoBean.deleteTransProductos(carrito.getNumTransaccion());
                        transCarritoBean.deleteTransCarrito(carrito.getNumTransaccion());
                        certificado.setEstado('D');
                        em.merge(certificado);
                    }
                }
            }
        }
    }

    /**
     * expiracion de premios entregados
     */
    @Schedule(info = "EXPIRACION_PREMIOS_ENTREGADOS")
    public void expirePremiosRedimidos() {
        //obtener todos los premios miembros que la fecha de expiracion sea igual al dia de hoy
        List<PremioMiembro> premiosMiembros = em.createNamedQuery("PremioMiembro.findFechaExpiracion")
                .setParameter("fechaExpiracion", new Date())
                .setParameter("estado", PremioMiembro.Estados.REDIMIDO_SIN_RETIRAR.getValue()).getResultList();

        //por cada premio miembro...
        try {
            premiosMiembros.forEach((premioMiembro) -> {
                Premio premio = premioMiembro.getIdPremio();
                //si el premio es certificado...
                if (premio.getIndTipoPremio().equals(Premio.Tipo.CERTIFICADO.getValue())) {
                    //... se regresa a los certificados disponibles
                    CodigoCertificado codigo = em.find(CodigoCertificado.class, new CodigoCertificadoPK(premioMiembro.getCodigoCertificado(), premio.getIdPremio()));
                    codigo.setIndEstado(CodigoCertificado.Estados.DISPONIBLE.getValue());
                    premio.setTotalExistencias(premio.getTotalExistencias() + 1);
                    premioMiembro.setEstado(PremioMiembro.Estados.EXPIRADO.getValue());
                    em.merge(codigo);
                    em.merge(premio);
                    em.merge(premioMiembro);
                } else {
                    ConfiguracionGeneral confi = em.find(ConfiguracionGeneral.class, 0L);
                    if (confi.getIntegracionInventarioPremios()) {
                        //... se regresa a las existencias de la ubicacion central
                        ExistenciasUbicacion existencias = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(confi.getUbicacionPrincipal().getIdUbicacion(), premioMiembro.getIdPremio().getIdPremio(), confi.getMes(), confi.getPeriodo()));
                        existencias.setSalidas(existencias.getSalidas() - 1);
                        existencias.setSaldoActual((existencias.getSaldoInicial() + existencias.getEntradas()) - existencias.getSalidas());

                        em.merge(existencias);
                        em.flush();
                        premio.setTotalExistencias(Long.valueOf(existencias.getSaldoActual()));
                        em.merge(premio);
                    }
                    premioMiembro.setEstado(PremioMiembro.Estados.EXPIRADO.getValue());
                    em.merge(premioMiembro);
                }
            });

        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
        }
    }
}
