package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author svargas
 */
@Embeddable
public class DataPremiosDiarioPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "DIA")
    private Integer dia;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "MES")
    private Integer mes;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_PREMIO")
    private String idPremio;

    public DataPremiosDiarioPK() {
    }

    public DataPremiosDiarioPK(Integer dia, Integer mes, String idMision) {
        this.dia = dia;
        this.mes = mes;
        this.idPremio = idMision;
    }

    public Integer getDia() {
        return dia;
    }

    public void setDia(Integer dia) {
        this.dia = dia;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public String getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(String idPremio) {
        this.idPremio = idPremio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dia != null ? dia.hashCode() : 0);
        hash += (mes != null ? mes.hashCode() : 0);
        hash += (idPremio != null ? idPremio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DataPremiosDiarioPK)) {
            return false;
        }
        DataPremiosDiarioPK other = (DataPremiosDiarioPK) object;
        if ((this.dia == null && other.dia != null) || (this.dia != null && !this.dia.equals(other.dia))) {
            return false;
        }
        if ((this.mes == null && other.mes != null) || (this.mes != null && !this.mes.equals(other.mes))) {
            return false;
        }
        if ((this.idPremio == null && other.idPremio != null) || (this.idPremio != null && !this.idPremio.equals(other.idPremio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.DataPremiosDiarioPK[ dia=" + dia + ", mes=" + mes + ", idMision=" + idPremio + " ]";
    }
    
}
