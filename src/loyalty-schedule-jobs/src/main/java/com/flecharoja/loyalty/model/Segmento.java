package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "SEGMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Segmento.countByNombreDespliegue", query = "SELECT COUNT(s.idSegmento) FROM Segmento s WHERE s.nombreDespliegue = :nombreDespliegue"),
    @NamedQuery(name = "Segmento.findByIdSegmentoInLista", query = "SELECT s FROM Segmento s WHERE s.idSegmento IN :lista ORDER BY s.fechaModificacion DESC")
})
public class Segmento implements Serializable {
    
    public enum Estados {
        ACTIVO("AC"),
        ARCHIVADO("AR"),
        BORRADOR("BO");
        
        private final String value;
        private static final Map<String, Estados> lookup = new HashMap<>();
        
        private Estados(String value) {
            this.value = value;
        }
        
        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public String getValue() {
            return value;
        }
        
        public static Estados get (String value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Tipos {
        DINAMICO('D'),
        ESTATICO('E');
        
        private final Character value;
        private static final Map<Character, Tipos> lookup = new HashMap<>();
        
        private Tipos(Character value) {
            this.value = value;
        }
        
        static {
            for (Tipos estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public Character getValue() {
            return value;
        }
        
        public static Tipos get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "segmento_uuid")
    @GenericGenerator(name = "segmento_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_SEGMENTO")
    private String idSegmento;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_DESPLIEGUE")
    private String nombreDespliegue;
    
    @Size(min = 1, max = 255)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTATICO")
    private Character indEstatico;
    
    @Column(name = "FRECUENCIA_ACTUALIZACION")
    private Long frecuenciaActualizacion;
    
    @Column(name = "ULTIMA_ACTUALIZACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimaActualizacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private String indEstado;
    
    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;
    
    @Column(name = "FECHA_ARCHIVADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaArchivado;
    
    @Size(min = 1, max = 500)
    @Column(name = "TAGS")
    private String tags;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    

    public Segmento() {
    }

    public String getIdSegmento() {
        return idSegmento;
    }

    public void setIdSegmento(String idSegmento) {
        this.idSegmento = idSegmento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreDespliegue() {
        return nombreDespliegue;
    }

    public void setNombreDespliegue(String nombreDespliegue) {
        this.nombreDespliegue = nombreDespliegue;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getIndEstatico() {
        return indEstatico;
    }

    public void setIndEstatico(Character indEstatico) {
        this.indEstatico = indEstatico!=null?Character.toUpperCase(indEstatico):null;
    }

    public Long getFrecuenciaActualizacion() {
        return frecuenciaActualizacion;
    }

    public void setFrecuenciaActualizacion(Long frecuenciaActualizacion) {
        this.frecuenciaActualizacion = frecuenciaActualizacion;
    }

    public Date getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public void setUltimaActualizacion(Date ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    public String getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(String indEstado) {
        this.indEstado = indEstado!=null?indEstado.toUpperCase():null;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Date getFechaArchivado() {
        return fechaArchivado;
    }

    public void setFechaArchivado(Date fechaArchivado) {
        this.fechaArchivado = fechaArchivado;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSegmento != null ? idSegmento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Segmento)) {
            return false;
        }
        Segmento other = (Segmento) object;
        if ((this.idSegmento == null && other.idSegmento != null) || (this.idSegmento != null && !this.idSegmento.equals(other.idSegmento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Segmento[ idSegmento=" + idSegmento + " ]";
    }

}
