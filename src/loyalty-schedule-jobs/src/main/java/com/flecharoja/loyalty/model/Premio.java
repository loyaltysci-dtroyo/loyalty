package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO")
@NamedQueries({
    @NamedQuery(name = "Premio.getIdPremioByIndEstado", query = "SELECT p.idPremio FROM Premio p WHERE p.indEstado = :indEstado")
})
public class Premio implements Serializable {

    public enum Estados {
        BORRADOR('B'),
        PUBLICADO('P'),
        ARCHIVADO('A');

        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }

        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Estados get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "premio_uuid")
    @GenericGenerator(name = "premio_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_PREMIO")
    private String idPremio;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_PREMIO")
    private Character indTipoPremio;

    @Column(name = "TOTAL_EXISTENCIAS")
    private Long totalExistencias;

    public Premio() {
    }

    public String getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(String idPremio) {
        this.idPremio = idPremio;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }

    public Character getIndTipoPremio() {
        return indTipoPremio;
    }

    public void setIndTipoPremio(Character indTipoPremio) {
        this.indTipoPremio = indTipoPremio;
    }

    public Long getTotalExistencias() {
        return totalExistencias;
    }

    public void setTotalExistencias(Long totalExistencias) {
        this.totalExistencias = totalExistencias;
    }
    
    

    public enum Tipo {
        CERTIFICADO('C'),
        PRODUCTO('P');

        private final char value;
        private static final Map<Character, Tipo> lookup = new HashMap<>();

        private Tipo(char value) {
            this.value = value;
        }

        static {
            for (Tipo tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static Tipo get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPremio != null ? idPremio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Premio)) {
            return false;
        }
        Premio other = (Premio) object;
        if ((this.idPremio == null && other.idPremio != null) || (this.idPremio != null && !this.idPremio.equals(other.idPremio))) {
            return false;
        }
        return true;
    }

}
