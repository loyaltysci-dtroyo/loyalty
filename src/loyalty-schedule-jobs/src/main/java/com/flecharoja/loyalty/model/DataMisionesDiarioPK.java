package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author svargas
 */
@Embeddable
public class DataMisionesDiarioPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "DIA")
    private Integer dia;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "MES")
    private Integer mes;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_MISION")
    private String idMision;

    public DataMisionesDiarioPK() {
    }

    public DataMisionesDiarioPK(Integer dia, Integer mes, String idMision) {
        this.dia = dia;
        this.mes = mes;
        this.idMision = idMision;
    }

    public Integer getDia() {
        return dia;
    }

    public void setDia(Integer dia) {
        this.dia = dia;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dia != null ? dia.hashCode() : 0);
        hash += (mes != null ? mes.hashCode() : 0);
        hash += (idMision != null ? idMision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DataMisionesDiarioPK)) {
            return false;
        }
        DataMisionesDiarioPK other = (DataMisionesDiarioPK) object;
        if ((this.dia == null && other.dia != null) || (this.dia != null && !this.dia.equals(other.dia))) {
            return false;
        }
        if ((this.mes == null && other.mes != null) || (this.mes != null && !this.mes.equals(other.mes))) {
            return false;
        }
        if ((this.idMision == null && other.idMision != null) || (this.idMision != null && !this.idMision.equals(other.idMision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.DataMisionesDiarioPK[ dia=" + dia + ", mes=" + mes + ", idMision=" + idMision + " ]";
    }
    
}
