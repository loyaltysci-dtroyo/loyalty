/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "TRANS_CARRITO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TransCarrito.findAll", query = "SELECT t FROM TransCarrito t"),
    @NamedQuery(name = "TransCarrito.findAvailable", query = "SELECT t FROM TransCarrito t WHERE t.idMiembro = :idMiembro AND t.idCategoriaProducto = :idCategoria AND t.estado = :estado"),
    @NamedQuery(name = "TransCarrito.findByIdTransCarrito", query = "SELECT t FROM TransCarrito t WHERE t.numTransaccion= :numTransaccion"),
    @NamedQuery(name = "TransCarrito.findByCertificado", query = "SELECT t FROM TransCarrito t WHERE t.idCertificado = :idCertificado AND t.idCategoriaProducto = :idCategoria")
})
public class TransCarrito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "NUM_TRANSACCION")
    private String numTransaccion;
    
    @Column(name = "FECHA_CARRITO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datetime;
    
    @Size(min = 1, max = 40)
    @Column(name = "MIEMBRO")
    private String idMiembro;
    
    @Size(min = 1, max = 40)
    @Column(name = "CATEGORIA")
    private String idCategoriaProducto;
    
    @Size(min = 1, max = 40)
    @Column(name = "CERTIFICADO")
    private String idCertificado;
    
    @Transient
    @Size(min = 1, max = 100)
    @Column(name = "NUM_CERTIFICADO")
    private String numCertificado;
    
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Column(name = "NUM_VERSION")
    private Integer numVersion;
    
    /*@JoinColumn(name = "CATEGORIA", referencedColumnName = "ID_CATEGORIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CategoriaProducto categoriaProducto;
    
    @JoinColumn(name = "CERTIFICADO", referencedColumnName = "ID_CERTIFICADO", insertable = false, updatable = true)
    @OneToOne(optional = false)
    private CertificadoCategoria certificadoCategoria;
    */
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTADO")
    private Character estado;
    
    @Size(min = 1, max = 20)
    @Column(name = "CARD_ID")
    private String cardId;
    
    @Column(name = "SUB_TOTAL")
    private Double subTotal;
    
    @Column(name = "IMP")
    private Double imp;
    
    @Column(name = "TOTAL")
    private Double total;

    public TransCarrito() {
    }

    public TransCarrito(String numTransaccion) {
        this.numTransaccion = numTransaccion;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Double getImp() {
        return imp;
    }

    public void setImp(Double imp) {
        this.imp = imp;
    }

    public String getNumTransaccion() {
        return numTransaccion;
    }

    public void setNumTransaccion(String numTransaccion) {
        this.numTransaccion = numTransaccion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public String getIdCategoriaProducto() {
        return idCategoriaProducto;
    }

    public String getIdCertificado() {
        return idCertificado;
    }

    public void setIdCategoriaProducto(String idCategoriaProducto) {
        this.idCategoriaProducto = idCategoriaProducto;
    }

    public void setIdCertificado(String idCertificado) {
        this.idCertificado = idCertificado;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public Integer getNumVersion() {
        return numVersion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setNumVersion(Integer numVersion) {
        this.numVersion = numVersion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getNumCertificado() {
        return numCertificado;
    }

    public void setNumCertificado(String numCertificado) {
        this.numCertificado = numCertificado;
    }
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numTransaccion != null ? numTransaccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransCarrito)) {
            return false;
        }
        TransCarrito other = (TransCarrito) object;
        if ((this.numTransaccion == null && other.numTransaccion != null) || (this.numTransaccion != null && !this.numTransaccion.equals(other.numTransaccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.TransCarrito[ numTransaccion=" + numTransaccion + " ]";
    }

//    
    
    
}
