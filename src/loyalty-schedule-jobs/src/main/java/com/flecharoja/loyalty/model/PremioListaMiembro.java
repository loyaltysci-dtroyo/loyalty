/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO_LISTA_MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PremioListaMiembro.findAll", query = "SELECT p FROM PremioListaMiembro p"),
    @NamedQuery(name = "PremioListaMiembro.findByIdMiembro", query = "SELECT p FROM PremioListaMiembro p WHERE p.premioListaMiembroPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "PremioListaMiembro.findByIdPremio", query = "SELECT p FROM PremioListaMiembro p WHERE p.premioListaMiembroPK.idPremio = :idPremio"),
    @NamedQuery(name = "PremioListaMiembro.findByIndTipo", query = "SELECT p FROM PremioListaMiembro p WHERE p.indTipo = :indTipo"),
    @NamedQuery(name = "PremioListaMiembro.countByIdPremio", query = "SELECT COUNT(p) FROM PremioListaMiembro p WHERE p.premioListaMiembroPK.idPremio = :idPremio"),
    @NamedQuery(name = "PremioListaMiembro.findMiembrosByIdPremio", query = "SELECT p.miembro FROM PremioListaMiembro p WHERE p.premioListaMiembroPK.idPremio = :idPremio"),
    @NamedQuery(name = "PremioListaMiembro.countMiembrosNotInPremioListaMiembro", query = "SELECT COUNT(m) FROM Miembro m WHERE  m.indEstadoMiembro = :estado AND m.idMiembro NOT IN (SELECT p.premioListaMiembroPK.idMiembro FROM PremioListaMiembro p WHERE p.premioListaMiembroPK.idPremio = :idPremio)"),
    @NamedQuery(name = "PremioListaMiembro.findMiembrosNotInPremioListaMiembro", query = "SELECT m FROM Miembro m WHERE m.indEstadoMiembro = :estado AND m.idMiembro NOT IN (SELECT p.premioListaMiembroPK.idMiembro FROM PremioListaMiembro p WHERE p.premioListaMiembroPK.idPremio = :idPremio)"),
    @NamedQuery(name = "PremioListaMiembro.countByIdPremioIndTipo", query = "SELECT COUNT(p) FROM PremioListaMiembro p WHERE p.premioListaMiembroPK.idPremio = :idPremio AND p.indTipo = :indTipo"),
    @NamedQuery(name = "PremioListaMiembro.findMiembrosByIdPremioIndTipo", query = "SELECT p.miembro FROM PremioListaMiembro p WHERE p.premioListaMiembroPK.idPremio = :idPremio AND p.indTipo = :indTipo"),
    @NamedQuery(name = "PremioListaMiembro.getIdMiembroByIdPremioIndTipo", query = "SELECT p.premioListaMiembroPK.idMiembro FROM PremioListaMiembro p WHERE p.premioListaMiembroPK.idPremio = :idPremio AND p.indTipo = :indTipo")
})
public class PremioListaMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PremioListaMiembroPK premioListaMiembroPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;
    
    @JoinColumn(name = "ID_PREMIO", referencedColumnName = "ID_PREMIO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Premio premio;

    public PremioListaMiembro() {
    }

    public PremioListaMiembro(PremioListaMiembroPK premioListaMiembroPK) {
        this.premioListaMiembroPK = premioListaMiembroPK;
    }

    public PremioListaMiembro(String idMiembro, String idPremio, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.premioListaMiembroPK = new PremioListaMiembroPK(idMiembro, idPremio);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public PremioListaMiembro(String idMiembro, String idPremio) {
        this.premioListaMiembroPK = new PremioListaMiembroPK(idMiembro, idPremio);
    }

    public PremioListaMiembroPK getPremioListaMiembroPK() {
        return premioListaMiembroPK;
    }

    public void setPremioListaMiembroPK(PremioListaMiembroPK premioListaMiembroPK) {
        this.premioListaMiembroPK = premioListaMiembroPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public Premio getPremio() {
        return premio;
    }

    public void setPremio(Premio premio) {
        this.premio = premio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (premioListaMiembroPK != null ? premioListaMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PremioListaMiembro)) {
            return false;
        }
        PremioListaMiembro other = (PremioListaMiembro) object;
        if ((this.premioListaMiembroPK == null && other.premioListaMiembroPK != null) || (this.premioListaMiembroPK != null && !this.premioListaMiembroPK.equals(other.premioListaMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.PremioListaMiembro[ premioListaMiembroPK=" + premioListaMiembroPK + " ]";
    }
    
}
