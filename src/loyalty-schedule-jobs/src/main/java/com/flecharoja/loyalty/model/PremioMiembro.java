/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO_MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PremioMiembro.findAll", query = "SELECT p FROM PremioMiembro p"),
    @NamedQuery(name = "PremioMiembro.countByIdMiembro", query = "SELECT COUNT(p) FROM PremioMiembro p WHERE p.idMiembro.idMiembro = :idMiembro AND p.estado = :estado"),
    @NamedQuery(name = "PremioMiembro.findByIdMiembroAndEstado", query = "SELECT p FROM PremioMiembro p WHERE p.idMiembro.idMiembro = :idMiembro AND p.estado = :estado ORDER BY p.fechaAsignacion DESC"),
    @NamedQuery(name = "PremioMiembro.findByIdMiembro", query = "SELECT p FROM PremioMiembro p WHERE p.idMiembro.idMiembro = :idMiembro"),
    @NamedQuery(name = "PremioMiembro.findByIdPremio", query = "SELECT p FROM PremioMiembro p WHERE p.idPremio.idPremio = :idPremio"),
    @NamedQuery(name = "PremioMiembro.findByEstado", query = "SELECT p FROM PremioMiembro p WHERE p.estado = :estado"),
    @NamedQuery(name = "PremioMiembro.findByFechaAsignacion", query = "SELECT p FROM PremioMiembro p WHERE p.fechaAsignacion = :fechaAsignacion"),
    @NamedQuery(name = "PremioMiembro.findByFechaReclamo", query = "SELECT p FROM PremioMiembro p WHERE p.fechaReclamo = :fechaReclamo"),
    @NamedQuery(name = "PremioMiembro.findByFechaCancelacion", query = "SELECT p FROM PremioMiembro p WHERE p.fechaCancelacion = :fechaCancelacion"),
    @NamedQuery(name = "PremioMiembro.findByUsuarioAsignador", query = "SELECT p FROM PremioMiembro p WHERE p.usuarioAsignador = :usuarioAsignador"),
    @NamedQuery(name = "PremioMiembro.findByIndMotivoAsignacion", query = "SELECT p FROM PremioMiembro p WHERE p.indMotivoAsignacion = :indMotivoAsignacion"),
    @NamedQuery(name = "PremioMiembro.findByCodigoCertificado", query = "SELECT p FROM PremioMiembro p WHERE p.codigoCertificado = :codigoCertificado"),
    @NamedQuery(name = "PremioMiembro.findByIdPremioMiembro", query = "SELECT p FROM PremioMiembro p WHERE p.idPremioMiembro = :idPremioMiembro"),
    @NamedQuery(name = "PremioMiembro.findByIdPremioIndMotivoAsignacion", query = "SELECT p FROM PremioMiembro p WHERE p.idPremio.idPremio = :idPremio AND p.indMotivoAsignacion = :indMotivoAsignacion"),
    @NamedQuery(name = "PremioMiembro.findFechaExpiracion", query = "SELECT p FROM PremioMiembro p WHERE p.estado = :estado  AND p.fechaExpiracion < :fechaExpiracion")

})
public class PremioMiembro implements Serializable {

    public enum Asignacion{
        AWARD('A'),
        MISION('M'),
        PREMIO('P');
        
        private final char value;
        private static final Map<Character,Asignacion> lookup = new HashMap<>();

        private Asignacion(char value) {
            this.value = value;
        }
        
        static{
            for(Asignacion asignacion : values()){
                lookup.put(asignacion.value, asignacion);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Asignacion get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    public enum Estados{
        REDIMIDO_SIN_RETIRAR('S'),
        REDIMIDO_RETIRADO('R'),
        ANULADO('A'),
        EXPIRADO('E');
        
        private final char value;
        private static final Map<Character,Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static{
            for(Estados estado : values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "metrica_uuid")
    @GenericGenerator(name = "metrica_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_PREMIO_MIEMBRO")
    private String idPremioMiembro;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTADO")
    private Character estado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_ASIGNACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAsignacion;
    
    @Column(name = "FECHA_RECLAMO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaReclamo;
    
    @Column(name = "FECHA_CANCELACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCancelacion;
    
    @Size(max = 40)
    @Column(name = "USUARIO_ASIGNADOR")
    private String usuarioAsignador;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_MOTIVO_ASIGNACION")
    private Character indMotivoAsignacion;
    
    @Size(max = 50)
    @Column(name = "CODIGO_CERTIFICADO")
    private String codigoCertificado;
    
       
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO")
    @ManyToOne(optional = false)
    private Miembro idMiembro;
    
    @JoinColumn(name = "ID_PREMIO", referencedColumnName = "ID_PREMIO")
    @ManyToOne(optional = false)
    private Premio idPremio;
    
    @Column(name = "FECHA_EXPIRACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaExpiracion;

    public PremioMiembro() {
    }

    public PremioMiembro(String idPremioMiembro) {
        this.idPremioMiembro = idPremioMiembro;
    }

    public PremioMiembro(String idPremioMiembro, Character estado, Date fechaAsignacion, Character indMotivoAsignacion) {
        this.idPremioMiembro = idPremioMiembro;
        this.estado = estado;
        this.fechaAsignacion = fechaAsignacion;
        this.indMotivoAsignacion = indMotivoAsignacion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado == null ? null : Character.toUpperCase(estado);
    }

    public Date getFechaAsignacion() {
        return fechaAsignacion;
    }

    public void setFechaAsignacion(Date fechaAsignacion) {
        this.fechaAsignacion = fechaAsignacion;
    }

    public Date getFechaReclamo() {
        return fechaReclamo;
    }

    public void setFechaReclamo(Date fechaReclamo) {
        this.fechaReclamo = fechaReclamo;
    }

    public Date getFechaCancelacion() {
        return fechaCancelacion;
    }

    public void setFechaCancelacion(Date fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public String getUsuarioAsignador() {
        return usuarioAsignador;
    }

    public void setUsuarioAsignador(String usuarioAsignador) {
        this.usuarioAsignador = usuarioAsignador;
    }

    public Character getIndMotivoAsignacion() {
        return indMotivoAsignacion;
    }

    public void setIndMotivoAsignacion(Character indMotivoAsignacion) {
        this.indMotivoAsignacion = indMotivoAsignacion == null ? null : Character.toUpperCase(indMotivoAsignacion);
    }

    public String getCodigoCertificado() {
        return codigoCertificado;
    }

    public void setCodigoCertificado(String codigoCertificado) {
        this.codigoCertificado = codigoCertificado;
    }

    public String getIdPremioMiembro() {
        return idPremioMiembro;
    }

    public void setIdPremioMiembro(String idPremioMiembro) {
        this.idPremioMiembro = idPremioMiembro;
    }

    public Miembro getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(Miembro idMiembro) {
        this.idMiembro = idMiembro;
    }

    public Premio getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(Premio idPremio) {
        this.idPremio = idPremio;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPremioMiembro != null ? idPremioMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PremioMiembro)) {
            return false;
        }
        PremioMiembro other = (PremioMiembro) object;
        if ((this.idPremioMiembro == null && other.idPremioMiembro != null) || (this.idPremioMiembro != null && !this.idPremioMiembro.equals(other.idPremioMiembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PremioMiembro{" + "idPremioMiembro=" + idPremioMiembro + ", estado=" + estado + ", fechaAsignacion=" + fechaAsignacion + ", fechaReclamo=" + fechaReclamo + ", fechaCancelacion=" + fechaCancelacion + ", usuarioAsignador=" + usuarioAsignador + ", indMotivoAsignacion=" + indMotivoAsignacion + ", codigoCertificado=" + codigoCertificado + ", idMiembro=" + idMiembro + ", idPremio=" + idPremio + '}';
    }
    
}
