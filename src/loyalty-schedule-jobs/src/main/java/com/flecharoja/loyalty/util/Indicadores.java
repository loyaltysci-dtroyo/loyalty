package com.flecharoja.loyalty.util;

/**
 *
 * @author svargas, wtencio
 */
public class Indicadores {
    
    public static final char INCLUIDO = 'I';
    public static final char EXCLUIDO = 'E';
    public static final char ASIGNADO = 'A';
    public static final char DISPONIBLES = 'D';
}
