package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CONFIGURACION_GENERAL")
@NamedQueries({
    @NamedQuery(name = "ConfiguracionGeneral.findAll", query = "SELECT c FROM ConfiguracionGeneral c"),
    @NamedQuery(name = "ConfiguracionGeneral.findMetrica" , query="SELECT c.idMetricaInicial.idMetrica FROM ConfiguracionGeneral c")
})
public class ConfiguracionGeneral implements Serializable {

    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    
    @JoinColumn(name = "ID_METRICA_INICIAL", referencedColumnName = "ID_METRICA")
    @ManyToOne
    private Metrica idMetricaInicial;
    
    @JoinColumn(name = "UBICACION_PRINCIPAL", referencedColumnName = "ID_UBICACION")
    @ManyToOne
    private Ubicacion ubicacionPrincipal;
 
    @Column(name = "PERIODO")
    private int periodo;
    
    @Column(name = "MES")
    private int mes;
    
    @Column(name = "INTEGRACION_INVENTARIO_PREMIO")
    private Boolean integracionInventarioPremios;
    

    public ConfiguracionGeneral() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    

    public Metrica getIdMetricaInicial() {
        return idMetricaInicial;
    }

    public void setIdMetricaInicial(Metrica idMetricaInicial) {
        this.idMetricaInicial = idMetricaInicial;
    }
    
    public Ubicacion getUbicacionPrincipal() {
        return ubicacionPrincipal;
    }

    public void setUbicacionPrincipal(Ubicacion ubicacionPrincipal) {
        this.ubicacionPrincipal = ubicacionPrincipal;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfiguracionGeneral)) {
            return false;
        }
        ConfiguracionGeneral other = (ConfiguracionGeneral) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ConfiguracionGeneral[ id=" + id + " ]";
    }

    public Boolean getIntegracionInventarioPremios() {
        return integracionInventarioPremios;
    }

    public void setIntegracionInventarioPremios(Boolean integracionInventarioPremios) {
        this.integracionInventarioPremios = integracionInventarioPremios;
    }
    
    
    
}
