/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p"),
    @NamedQuery(name = "Producto.findAvailable", query = "SELECT p FROM Producto p WHERE p.saldoDisponible > 0"),
    @NamedQuery(name = "Producto.findByIdProducto", query = "SELECT p FROM Producto p WHERE p.idProducto = :idProducto")
})
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_PRODUCTO")
    private String idProducto;
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN_ARTE")
    private String imagenArte;
    
    @Size(max = 100)
    @Column(name = "SKU")
    private String sku;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRECIO")
    private Double precio;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_IMPUESTO")
    private Character indImpuesto;
    
    @Column(name = "MONTO_ENTREGA")
    private Double montoEntrega;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ENTREGA")
    private Character indEntrega;
    
    @Column(name = "FECHA_ARCHIVADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaArchivado;
    
    
    @Size(max = 150)
    @Column(name = "ENCABEZADO_ARTE")
    private String encabezadoArte;
    
    @Size(max = 300)
    @Column(name = "SUBENCABEZADO_ARTE")
    private String subencabezadoArte;
    
    
    @Size(max = 500)
    @Column(name = "DETALLE_ARTE")
    private String detalleArte;
    
    @Column(name = "CANT_MIN_METRICA")
    private Double cantMinMetrica;
    
    @Column(name = "SALDO_INICIAL")
    private Integer saldoInicial;
    
    @Column(name = "SALDO_DISPONIBLE")
    private Integer saldoDisponible;

    public Producto() {
    }

    public Producto(String idProducto) {
        this.idProducto = idProducto;
    }


    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getImagenArte() {
        return imagenArte;
    }

    public void setImagenArte(String imagenArte) {
        this.imagenArte = imagenArte;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Character getIndImpuesto() {
        return indImpuesto;
    }

    public void setIndImpuesto(Character indImpuesto) {
        this.indImpuesto = indImpuesto;
    }

    public Double getMontoEntrega() {
        return montoEntrega;
    }

    public void setMontoEntrega(Double montoEntrega) {
        this.montoEntrega = montoEntrega;
    }

    public Character getIndEntrega() {
        return indEntrega;
    }

    public void setIndEntrega(Character indEntrega) {
        this.indEntrega = indEntrega;
    }

    public Date getFechaArchivado() {
        return fechaArchivado;
    }

    public void setFechaArchivado(Date fechaArchivado) {
        this.fechaArchivado = fechaArchivado;
    }

    public String getEncabezadoArte() {
        return encabezadoArte;
    }

    public void setEncabezadoArte(String encabezadoArte) {
        this.encabezadoArte = encabezadoArte;
    }

    public String getSubencabezadoArte() {
        return subencabezadoArte;
    }

    public void setSubencabezadoArte(String subencabezadoArte) {
        this.subencabezadoArte = subencabezadoArte;
    }

    public String getDetalleArte() {
        return detalleArte;
    }

    public void setDetalleArte(String detalleArte) {
        this.detalleArte = detalleArte;
    }

    public Double getCantMinMetrica() {
        return cantMinMetrica;
    }

    public void setCantMinMetrica(Double cantMinMetrica) {
        this.cantMinMetrica = cantMinMetrica;
    }

    public Integer getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(Integer saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public Integer getSaldoDisponible() {
        return saldoDisponible;
    }

    public void setSaldoDisponible(Integer saldoDisponible) {
        this.saldoDisponible = saldoDisponible;
    } 

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.Producto[ idProducto=" + idProducto + " ]";
    }
    
}
