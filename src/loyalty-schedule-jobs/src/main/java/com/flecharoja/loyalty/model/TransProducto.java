/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "TRANS_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TransProducto.findAll", query = "SELECT t FROM TransProducto t"),
    @NamedQuery(name = "TransProducto.findByIdTransProducto", query = "SELECT t FROM TransProducto t WHERE t.transProductoPK.numTransaccion= :numTransaccion")
})
public class TransProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TransProductoPK transProductoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANTIDAD")
    private Integer cantidad;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRECIO")
    private Double Precio;

    
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Column(name = "NUM_VERSION")
    private Integer numVersion;
    
    public TransProducto() {
    }

    public TransProducto(TransProductoPK transProductoPK) {
        this.transProductoPK = transProductoPK;
    }

    public void setTransProductoPK(TransProductoPK transProductoPK) {
        this.transProductoPK = transProductoPK;
    }

    public TransProductoPK getTransProductoPK() {
        return transProductoPK;
    }

    public void setPrecio(Double Precio) {
        this.Precio = Precio;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecio() {
        return Precio;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public Integer getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Integer numVersion) {
        this.numVersion = numVersion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transProductoPK != null ? transProductoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransProducto)) {
            return false;
        }
        TransProducto other = (TransProducto) object;
        if ((this.transProductoPK == null && other.transProductoPK != null) || (this.transProductoPK != null && !this.transProductoPK.equals(other.transProductoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.TransProducto[ transProductoPK=" + transProductoPK + " ]";
    }

//    
    
    
}
