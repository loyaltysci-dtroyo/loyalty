/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CODIGO_CERTIFICADO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CodigoCertificado.findAll", query = "SELECT c FROM CodigoCertificado c ORDER BY c.fechaCreacion DESC"),
    @NamedQuery(name = "CodigoCertificado.countIdPremio", query = "SELECT COUNT(c.codigoCertificadoPK.codigo) FROM CodigoCertificado c WHERE c.codigoCertificadoPK.idPremio = :idPremio"),
    @NamedQuery(name = "CodigoCertificado.findByCodigo", query = "SELECT c FROM CodigoCertificado c WHERE c.codigoCertificadoPK.codigo = :codigo ORDER BY c.fechaCreacion DESC"),
    @NamedQuery(name = "CodigoCertificado.findByIdPremio", query = "SELECT c FROM CodigoCertificado c WHERE c.codigoCertificadoPK.idPremio = :idPremio ORDER BY c.fechaCreacion DESC"),
    @NamedQuery(name = "CodigoCertificado.countByCodigo", query = "SELECT COUNT(c) FROM CodigoCertificado c WHERE c.codigoCertificadoPK.codigo = :codigo"),
   
})
public class CodigoCertificado implements Serializable {


    public enum Estados{
        DISPONIBLE('D'),
        OCUPADO('O'),
        ANULADO('A');
        
        private final char value;
        private static final Map<Character,Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static{
            for(Estados estado : values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }



    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CodigoCertificadoPK codigoCertificadoPK;
     
    
    @JoinColumn(name = "ID_PREMIO", referencedColumnName = "ID_PREMIO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Premio premio;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    public CodigoCertificado() {
    }

    public CodigoCertificado(CodigoCertificadoPK codigoCertificadoPK) {
        this.codigoCertificadoPK = codigoCertificadoPK;
    }

    public CodigoCertificado(String codigo, String idPremio) {
        this.codigoCertificadoPK = new CodigoCertificadoPK(codigo, idPremio);
    }

    public CodigoCertificadoPK getCodigoCertificadoPK() {
        return codigoCertificadoPK;
    }

    public void setCodigoCertificadoPK(CodigoCertificadoPK codigoCertificadoPK) {
        this.codigoCertificadoPK = codigoCertificadoPK;
    }
  

    public Premio getPremio() {
        return premio;
    }

    public void setPremio(Premio premio) {
        this.premio = premio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoCertificadoPK != null ? codigoCertificadoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CodigoCertificado)) {
            return false;
        }
        CodigoCertificado other = (CodigoCertificado) object;
        if ((this.codigoCertificadoPK == null && other.codigoCertificadoPK != null) || (this.codigoCertificadoPK != null && !this.codigoCertificadoPK.equals(other.codigoCertificadoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CodigoCertificado[ codigoCertificadoPK=" + codigoCertificadoPK + " ]";
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }    

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }
}
