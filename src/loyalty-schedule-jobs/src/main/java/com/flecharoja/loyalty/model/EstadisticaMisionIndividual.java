/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "ESTADISTICA_MISION_INDIVIDUAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadisticaMisionIndividual.findAll", query = "SELECT e FROM EstadisticaMisionIndividual e")
    , @NamedQuery(name = "EstadisticaMisionIndividual.findByIdMiembro", query = "SELECT e FROM EstadisticaMisionIndividual e WHERE e.miembro.idMiembro = :idMiembro")
    , @NamedQuery(name = "EstadisticaMisionIndividual.findByCantMisionesEncuesta", query = "SELECT e FROM EstadisticaMisionIndividual e WHERE e.cantMisionesEncuesta = :cantMisionesEncuesta")
    , @NamedQuery(name = "EstadisticaMisionIndividual.findByCantMisionesJuego", query = "SELECT e FROM EstadisticaMisionIndividual e WHERE e.cantMisionesJuego = :cantMisionesJuego")
    , @NamedQuery(name = "EstadisticaMisionIndividual.findByCantMisionesPerfil", query = "SELECT e FROM EstadisticaMisionIndividual e WHERE e.cantMisionesPerfil = :cantMisionesPerfil")
    , @NamedQuery(name = "EstadisticaMisionIndividual.findByCantMisionesPreferencia", query = "SELECT e FROM EstadisticaMisionIndividual e WHERE e.cantMisionesPreferencia = :cantMisionesPreferencia")
    , @NamedQuery(name = "EstadisticaMisionIndividual.findByCantMisionesVerContenido", query = "SELECT e FROM EstadisticaMisionIndividual e WHERE e.cantMisionesVerContenido = :cantMisionesVerContenido")
    , @NamedQuery(name = "EstadisticaMisionIndividual.findByCantMisionesSubirContenido", query = "SELECT e FROM EstadisticaMisionIndividual e WHERE e.cantMisionesSubirContenido = :cantMisionesSubirContenido")
    , @NamedQuery(name = "EstadisticaMisionIndividual.findByCantMisionesSocial", query = "SELECT e FROM EstadisticaMisionIndividual e WHERE e.cantMisionesSocial = :cantMisionesSocial")})
public class EstadisticaMisionIndividual implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO")
    @OneToOne(optional = false)
    private Miembro miembro;
    @Column(name = "CANT_MISIONES_ENCUESTA")
    private Integer cantMisionesEncuesta;
    @Column(name = "CANT_MISIONES_JUEGO")
    private Integer cantMisionesJuego;
    @Column(name = "CANT_MISIONES_PERFIL")
    private Integer cantMisionesPerfil;
    @Column(name = "CANT_MISIONES_PREFERENCIA")
    private Integer cantMisionesPreferencia;
    @Column(name = "CANT_MISIONES_VER_CONTENIDO")
    private Integer cantMisionesVerContenido;
    @Column(name = "CANT_MISIONES_SUBIR_CONTENIDO")
    private Integer cantMisionesSubirContenido;
    @Column(name = "CANT_MISIONES_SOCIAL")
    private Integer cantMisionesSocial;

    public EstadisticaMisionIndividual() {
    }

    public Integer getCantMisionesEncuesta() {
        return cantMisionesEncuesta;
    }

    public void setCantMisionesEncuesta(Integer cantMisionesEncuesta) {
        this.cantMisionesEncuesta = cantMisionesEncuesta;
    }

    public Integer getCantMisionesJuego() {
        return cantMisionesJuego;
    }

    public void setCantMisionesJuego(Integer cantMisionesJuego) {
        this.cantMisionesJuego = cantMisionesJuego;
    }

    public Integer getCantMisionesPerfil() {
        return cantMisionesPerfil;
    }

    public void setCantMisionesPerfil(Integer cantMisionesPerfil) {
        this.cantMisionesPerfil = cantMisionesPerfil;
    }

    public Integer getCantMisionesPreferencia() {
        return cantMisionesPreferencia;
    }

    public void setCantMisionesPreferencia(Integer cantMisionesPreferencia) {
        this.cantMisionesPreferencia = cantMisionesPreferencia;
    }

    public Integer getCantMisionesVerContenido() {
        return cantMisionesVerContenido;
    }

    public void setCantMisionesVerContenido(Integer cantMisionesVerContenido) {
        this.cantMisionesVerContenido = cantMisionesVerContenido;
    }

    public Integer getCantMisionesSubirContenido() {
        return cantMisionesSubirContenido;
    }

    public void setCantMisionesSubirContenido(Integer cantMisionesSubirContenido) {
        this.cantMisionesSubirContenido = cantMisionesSubirContenido;
    }

    public Integer getCantMisionesSocial() {
        return cantMisionesSocial;
    }

    public void setCantMisionesSocial(Integer cantMisionesSocial) {
        this.cantMisionesSocial = cantMisionesSocial;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (miembro != null ? miembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadisticaMisionIndividual)) {
            return false;
        }
        EstadisticaMisionIndividual other = (EstadisticaMisionIndividual) object;
        if ((this.miembro == null && other.miembro != null) || (this.miembro != null && !this.miembro.equals(other.miembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.EstadisticaMisionIndividual[ idMiembro=" + miembro.toString() + " ]";
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

}
