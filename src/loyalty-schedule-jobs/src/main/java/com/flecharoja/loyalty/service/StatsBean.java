package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.DataPromosDiario;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.model.DataMisionesDiario;
import com.flecharoja.loyalty.model.DataPremiosDiario;
import com.flecharoja.loyalty.model.EstadisticaIndividualMetricaNivel;
import com.flecharoja.loyalty.model.EstadisticaMiembroGeneral;
import com.flecharoja.loyalty.model.EstadisticaMiembroPremio;
import com.flecharoja.loyalty.model.EstadisticaMisionGeneral;
import com.flecharoja.loyalty.model.EstadisticaMisionIndividual;
import com.flecharoja.loyalty.model.EstadisticaSegmentoGeneral;
import com.flecharoja.loyalty.model.EstadisticaSegmentoIndividual;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.MiembroBalanceMetrica;
import com.flecharoja.loyalty.model.MiembroBalanceMetricaPK;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.util.Indicadores;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.KeyOnlyFilter;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author svargas
 */
@Singleton
public class StatsBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    private final Configuration configuration;

    public StatsBean() {
        this.configuration = new Configuration();
        this.configuration.addResource("conf/hbase/hbase-site.xml");
    }

    /**
     * Rejunta de estadisticas diarias de promociones
     */
    @Schedule(hour = "00", minute = "15", info = "STATS_PROMO")
    public void saveEstadisticasDiariasPromociones() {
        LocalDate fecha = LocalDate.now();
        //busqueda de promociones en estado publicado
        List<String> promocion = em.createNamedQuery("Promocion.getIdPromocionByIndEstado").setParameter("indEstado", Promocion.Estados.PUBLICADO.getValue()).getResultList();
        //por cada promocion...
        promocion.forEach((idPromocion) -> {
            DataPromosDiario dataPromosDiario = new DataPromosDiario(fecha.getDayOfMonth(), fecha.getMonthValue()-1, idPromocion);
            //obtencion de marcas en promocion
            dataPromosDiario.setCantMarcas((long) em.createNamedQuery("MarcadorPromocion.countAllByIdPromocion").setParameter("idPromocion", idPromocion).getSingleResult());
            //obtencion de elegibles actuales de promocion
            List<String> miembros = em.createNamedQuery("PromocionListaMiembro.getIdMiembroByIdPromocionIndTipo").setParameter("idPromocion", idPromocion).setParameter("indTipo", Indicadores.INCLUIDO).getResultList();
            List<String> miembrosExcluidos = em.createNamedQuery("PromocionListaMiembro.getIdMiembroByIdPromocionIndTipo").setParameter("idPromocion", idPromocion).setParameter("indTipo", Indicadores.EXCLUIDO).getResultList();
            try (Connection connection = ConnectionFactory.createConnection(configuration)) {
                Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "ELEGIBLES_SEGMENTO"));
                List<String> segmentosIncluidos = em.createNamedQuery("PromocionListaSegmento.findAllIdSegmentoByIdPromocionIndTipo").setParameter("idPromocion", idPromocion).setParameter("indTipo", Indicadores.INCLUIDO).getResultList();
                if (segmentosIncluidos.isEmpty() && miembros.isEmpty()) {
                    miembros = em.createNamedQuery("Miembro.findAllIdMiembro").getResultList();
                }
                for (String idSegmento : segmentosIncluidos) {
                    Get get = new Get(Bytes.toBytes(idSegmento));
                    get.addFamily(Bytes.toBytes("MIEMBROS"));
                    Result result = table.get(get);
                    if (table.exists(get)) {
                        miembros.addAll(result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                                .filter((t) -> Bytes.toBoolean(t.getValue()))
                                .map((t) -> Bytes.toString(t.getKey()))
                                .collect(Collectors.toList()));
                    }
                }
                miembros = miembros.stream().distinct().collect(Collectors.toList());
                for (Object idSegmento : em.createNamedQuery("PromocionListaSegmento.findAllIdSegmentoByIdPromocionIndTipo").setParameter("idPromocion", idPromocion).setParameter("indTipo", Indicadores.EXCLUIDO).getResultList()) {
                    Get get = new Get(Bytes.toBytes((String) idSegmento));
                    get.addFamily(Bytes.toBytes("MIEMBROS"));
                    Result result = table.get(get);
                    if (table.exists(get)) {
                        miembros.addAll(result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                                .filter((t) -> Bytes.toBoolean(t.getValue()))
                                .map((t) -> Bytes.toString(t.getKey()))
                                .collect(Collectors.toList()));
                    }
                }
                dataPromosDiario.setCantElegibles(miembros.stream().filter((idMiembro) -> !miembrosExcluidos.contains(idMiembro)).count());
            } catch (IOException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
            }

            em.merge(dataPromosDiario);
        });

        //eliminacion de registro de estadisticas con 5 meses
        if (fecha.getDayOfMonth() == 1) {
            em.createNamedQuery("DataPromosDiario.deleteAllByMes").setParameter("mes", fecha.minusMonths(5).getMonthValue()-1).executeUpdate();
        }
    }

    /**
     * Rejunta diaria de estadisticas de misiones
     */
    @Schedule(hour = "00", minute = "30", info = "STATS_MISION")
    public void saveEstadisticasDiariasMisiones() {
        LocalDate fecha = LocalDate.now();
        //obtencion de misiones en estado publicado
        List<String> misiones = em.createNamedQuery("Mision.getIdMisionByIndEstado").setParameter("indEstado", Mision.Estados.PUBLICADO.getValue()).getResultList();
        //por cada mision...
        misiones.forEach((idMision) -> {
            DataMisionesDiario dataMisionesDiario = new DataMisionesDiario(fecha.getDayOfMonth(), fecha.getMonthValue()-1, idMision);
            //obtencion de elegibles de mision
            List<String> miembros = em.createNamedQuery("MisionListaMiembro.getIdMiembroByIdMisionIndTipo").setParameter("idMision", idMision).setParameter("indTipo", Indicadores.INCLUIDO).getResultList();
            List<String> miembrosExcluidos = em.createNamedQuery("MisionListaMiembro.getIdMiembroByIdMisionIndTipo").setParameter("idMision", idMision).setParameter("indTipo", Indicadores.EXCLUIDO).getResultList();
            try (Connection connection = ConnectionFactory.createConnection(configuration)) {
                Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "ELEGIBLES_SEGMENTO"));
                List<String> segmentosIncluidos = em.createNamedQuery("MisionListaSegmento.getAllIdSegmentosByIdMisionIndTipo").setParameter("idMision", idMision).setParameter("indTipo", Indicadores.INCLUIDO).getResultList();
                if (segmentosIncluidos.isEmpty() && miembros.isEmpty()) {
                    miembros = em.createNamedQuery("Miembro.findAllIdMiembro").getResultList();
                }
                for (String idSegmento : segmentosIncluidos) {
                    Get get = new Get(Bytes.toBytes(idSegmento));
                    get.addFamily(Bytes.toBytes("MIEMBROS"));
                    Result result = table.get(get);
                    if (table.exists(get)) {
                        miembros.addAll(result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                                .filter((t) -> Bytes.toBoolean(t.getValue()))
                                .map((t) -> Bytes.toString(t.getKey()))
                                .collect(Collectors.toList()));
                    }
                }
                miembros = miembros.stream().distinct().collect(Collectors.toList());
                for (Object idSegmento : em.createNamedQuery("MisionListaSegmento.getAllIdSegmentosByIdMisionIndTipo").setParameter("idMision", idMision).setParameter("indTipo", Indicadores.EXCLUIDO).getResultList()) {
                    Get get = new Get(Bytes.toBytes((String) idSegmento));
                    get.addFamily(Bytes.toBytes("MIEMBROS"));
                    Result result = table.get(get);
                    if (table.exists(get)) {
                        miembros.removeAll(result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                                .filter((t) -> Bytes.toBoolean(t.getValue()))
                                .map((t) -> Bytes.toString(t.getKey()))
                                .collect(Collectors.toList()));
                    }
                }
                dataMisionesDiario.setCantElegibles(miembros.stream().filter((idMiembro) -> !miembrosExcluidos.contains(idMiembro)).count());

            } catch (IOException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
            }

            em.merge(dataMisionesDiario);
        });

        //eliminacion de registro de estadisticas con 5 meses
        if (fecha.getDayOfMonth() == 1) {
            em.createNamedQuery("DataMisionesDiario.deleteAllByMes").setParameter("mes", fecha.minusMonths(5).getMonthValue()-1).executeUpdate();
        }

    }

    /**
     * rejunta de estadisticas diarias de actividades de mision
     */
    @Schedule(hour = "00", minute = "45", info = "STATS_ACTIVIDAD_MISION")
    public void generarEstadisicasActividadMision() {
        List<Mision> misiones = em.createNamedQuery("Mision.findAll").getResultList();
        Map<String, Integer> estadisticaMisionEncuesta = new HashMap<>();
        Map<String, Integer> estadisticaMisionJuego = new HashMap<>();
        Map<String, Integer> estadisticaMisionPerfil = new HashMap<>();
        Map<String, Integer> estadisticaMisionPreferencia = new HashMap<>();
        Map<String, Integer> estadisticaMisionRedSocial = new HashMap<>();
        Map<String, Integer> estadisticaMisionSubirContenido = new HashMap<>();
        Map<String, Integer> estadisticaMisionVerContenido = new HashMap<>();
        Map<String, Integer> totales = new HashMap<>();
        
        AtomicInteger cantMisionEncuesta= new AtomicInteger(0);
        AtomicInteger cantMisionJuego= new AtomicInteger(0);
        AtomicInteger cantMisionPerfil= new AtomicInteger(0);
        AtomicInteger cantMisionPreferencia= new AtomicInteger(0);
        AtomicInteger cantMisionRedSocial= new AtomicInteger(0);
        AtomicInteger cantMisionSubirContenido= new AtomicInteger(0);
        AtomicInteger cantMisionVerContenido= new AtomicInteger(0);
        
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "REGISTRO_MISION"));
            Scan scan = new Scan();
            Filter filter = new KeyOnlyFilter();
            scan.setFilter(filter);

            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result = scanner.next(); result != null; result = scanner.next()) {
                    String key = Bytes.toString(result.getRow());
                    String[] row = key.split("&");
                    try {
                        Mision misionActual = misiones.stream().filter((mision) -> {
                            return ((row[0] != null) && (mision.getIdMision().equals(row[0])));
                        }).findFirst().get();
                        System.out.println(row[0] + " " + row[1] + " " + row[2]);
                        if (misionActual != null) {
                            switch (Mision.Tipos.get(misionActual.getIndTipoMision())) {
                                case ENCUESTA: {
                                    if (estadisticaMisionEncuesta.containsKey(row[1])) {
                                        estadisticaMisionEncuesta.put(row[1], estadisticaMisionEncuesta.get(row[1]) + 1);
                                        totales.put(row[1], totales.get(row[1]) + 1);
                                    } else {
                                        estadisticaMisionEncuesta.put(row[1], 1);
                                        totales.put(row[1], 1);
                                    }
                                    cantMisionEncuesta.getAndIncrement();
                                    break;
                                }
                                case JUEGO_PREMIO: {
                                    if (estadisticaMisionJuego.containsKey(row[1])) {
                                        estadisticaMisionJuego.put(row[1], estadisticaMisionJuego.get(row[1]) + 1);
                                        totales.put(row[1], totales.get(row[1]) + 1);
                                    } else {
                                        estadisticaMisionJuego.put(row[1], 1);
                                        totales.put(row[1], 1);
                                    }
                                    cantMisionJuego.getAndIncrement();
                                    break;
                                }
                                case PREFERENCIAS: {
                                    if (estadisticaMisionPreferencia.containsKey(row[1])) {
                                        estadisticaMisionPreferencia.put(row[1], estadisticaMisionPreferencia.get(row[1]) + 1);
                                        totales.put(row[1], totales.get(row[1]) + 1);
                                    } else {
                                        estadisticaMisionPreferencia.put(row[1], 1);
                                        totales.put(row[1], 1);
                                    }
                                    cantMisionPreferencia.getAndIncrement();
                                    break;
                                }
                                case PERFIL: {
                                    if (estadisticaMisionPerfil.containsKey(row[1])) {
                                        estadisticaMisionPerfil.put(row[1], estadisticaMisionPerfil.get(row[1]) + 1);
                                        totales.put(row[1], totales.get(row[1]) + 1);
                                    } else {
                                        estadisticaMisionPerfil.put(row[1], 1);
                                        totales.put(row[1], 1);
                                    }
                                    cantMisionPerfil.getAndIncrement();
                                    break;
                                }
                                case RED_SOCIAL: {
                                    if (estadisticaMisionRedSocial.containsKey(row[1])) {
                                        estadisticaMisionRedSocial.put(row[1], estadisticaMisionRedSocial.get(row[1]) + 1);
                                        totales.put(row[1], totales.get(row[1]) + 1);
                                    } else {
                                        estadisticaMisionRedSocial.put(row[1], 1);
                                        totales.put(row[1], 1);
                                    }
                                    cantMisionRedSocial.getAndIncrement();
                                    break;
                                }
                                case SUBIR_CONTENIDO: {
                                    if (estadisticaMisionSubirContenido.containsKey(row[1])) {
                                        estadisticaMisionSubirContenido.put(row[1], estadisticaMisionSubirContenido.get(row[1]) + 1);
                                        totales.put(row[1], totales.get(row[1]) + 1);
                                    } else {
                                        estadisticaMisionSubirContenido.put(row[1], 1);
                                        totales.put(row[1], 1);
                                    }
                                    cantMisionSubirContenido.getAndIncrement();
                                    break;
                                }
                                case VER_CONTENIDO: {
                                    if (estadisticaMisionVerContenido.containsKey(row[1])) {
                                        estadisticaMisionVerContenido.put(row[1], estadisticaMisionVerContenido.get(row[1]) + 1);
                                        totales.put(row[1], totales.get(row[1]) + 1);
                                    } else {
                                        estadisticaMisionVerContenido.put(row[1], 1);
                                        totales.put(row[1], 1);
                                    }
                                    cantMisionVerContenido.getAndIncrement();
                                    break;
                                }
                            }
                        }
                    } catch (java.util.NoSuchElementException ex) {
                        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "No se encontró Misión para el id: {0}", row[0]);
                    }
                }
                em.createQuery("DELETE FROM EstadisticaMisionIndividual").executeUpdate();
                em.createQuery("DELETE FROM EstadisticaMisionGeneral").executeUpdate();
                totales.entrySet().stream().sorted(Map.Entry.comparingByValue((o1, o2) -> {
                    return o2 - o1;
                })).limit(20).forEach((element) -> {
                    EstadisticaMisionIndividual emi = new EstadisticaMisionIndividual();
                    emi.setMiembro(em.find(Miembro.class, element.getKey()));
                    emi.setCantMisionesEncuesta(estadisticaMisionEncuesta.get(element.getKey()));
                    emi.setCantMisionesJuego(estadisticaMisionJuego.get(element.getKey()));
                    emi.setCantMisionesPerfil(estadisticaMisionPerfil.get(element.getKey()));
                    emi.setCantMisionesPreferencia(estadisticaMisionPreferencia.get(element.getKey()));
                    emi.setCantMisionesSocial(estadisticaMisionRedSocial.get(element.getKey()));
                    emi.setCantMisionesSubirContenido(estadisticaMisionSubirContenido.get(element.getKey()));
                    em.persist(emi);
                });
                
                EstadisticaMisionGeneral emg = new EstadisticaMisionGeneral(0);
                emg.setCantMisionesEncuesta(cantMisionEncuesta.get());
                emg.setCantMisionesJuego(cantMisionJuego.get());
                emg.setCantMisionesPerfil(cantMisionPerfil.get());
                emg.setCantMisionesPreferencia(cantMisionPreferencia.get());
                emg.setCantMisionesSocial(cantMisionRedSocial.get());
                emg.setCantMisionesSubirContenido(cantMisionSubirContenido.get());
                emg.setCantMisionesVerContenido(cantMisionVerContenido.get());
                em.persist(emg);
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
            }
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
        }
    }
    
    /**
     * rejunta diaria de estadisticas de premios
     */
    @Schedule(hour = "01", minute = "15", info = "STATS_PREMIO")
    public void saveEstadisticasDiariasPremios() {
        LocalDate fecha = LocalDate.now();

        //obtencion de premios en estado publicado
        List<String> premios = em.createNamedQuery("Premio.getIdPremioByIndEstado").setParameter("indEstado", Premio.Estados.PUBLICADO.getValue()).getResultList();
        premios.forEach((idPremio) -> {
            DataPremiosDiario dataPremiosDiario = new DataPremiosDiario(fecha.getDayOfMonth(), fecha.getMonthValue()-1, idPremio);

            //obtencion de elegibles
            List<String> miembros = em.createNamedQuery("PremioListaMiembro.getIdMiembroByIdPremioIndTipo").setParameter("idPremio", idPremio).setParameter("indTipo", Indicadores.INCLUIDO).getResultList();
            List<String> miembrosExcluidos = em.createNamedQuery("PremioListaMiembro.getIdMiembroByIdPremioIndTipo").setParameter("idPremio", idPremio).setParameter("indTipo", Indicadores.EXCLUIDO).getResultList();
            try (Connection connection = ConnectionFactory.createConnection(configuration)) {
                Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "ELEGIBLES_SEGMENTO"));
                List<String> segmentosIncluidos = em.createNamedQuery("PremioListaSegmento.getIdSegmentoByIdPremioIndTipo").setParameter("idPremio", idPremio).setParameter("indTipo", Indicadores.INCLUIDO).getResultList();
                if (segmentosIncluidos.isEmpty() && miembros.isEmpty()) {
                    miembros = em.createNamedQuery("Miembro.findAllIdMiembro").getResultList();
                }
                for (String idSegmento : segmentosIncluidos) {
                    Get get = new Get(Bytes.toBytes(idSegmento));
                    get.addFamily(Bytes.toBytes("MIEMBROS"));
                    Result result = table.get(get);
                    if (table.exists(get)) {
                        miembros.addAll(result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                                .filter((t) -> Bytes.toBoolean(t.getValue()))
                                .map((t) -> Bytes.toString(t.getKey()))
                                .collect(Collectors.toList()));
                    }
                }
                miembros = miembros.stream().distinct().collect(Collectors.toList());
                for (Object idSegmento : em.createNamedQuery("PremioListaSegmento.getIdSegmentoByIdPremioIndTipo").setParameter("idPremio", idPremio).setParameter("indTipo", Indicadores.EXCLUIDO).getResultList()) {
                    Get get = new Get(Bytes.toBytes((String) idSegmento));
                    get.addFamily(Bytes.toBytes("MIEMBROS"));
                    Result result = table.get(get);
                    if (table.exists(get)) {
                        miembros.removeAll(result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                                .filter((t) -> Bytes.toBoolean(t.getValue()))
                                .map((t) -> Bytes.toString(t.getKey()))
                                .collect(Collectors.toList()));
                    }
                }
                dataPremiosDiario.setCantElegibles(miembros.stream().filter((idMiembro) -> !miembrosExcluidos.contains(idMiembro)).count());
            } catch (IOException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
            }

            em.merge(dataPremiosDiario);
        });

        //eliminacion de registro de estadisticas con 5 meses
        if (fecha.getDayOfMonth() == 1) {
            em.createNamedQuery("DataPremiosDiario.deleteAllByMes").setParameter("mes", fecha.minusMonths(5).getMonthValue()-1).executeUpdate();
        }
    }
    
    /**
     * rejunta diaria de top ganadores de premios
     */
    @Schedule(hour = "01", minute = "15", info = "STATS_TOP_GANADORES_PREMIO")
    public void saveEstadisticasTopGanadoresPremios() {
        //Estadistica de miembros mas ganadores
        //TODO Persistir en BD
        try {
            List<Object[]> premiosRedimidos = em.createQuery("SELECT pm.idMiembro.idMiembro, COUNT(pm) AS TOTAL FROM PremioMiembro pm GROUP BY pm.idMiembro ORDER BY TOTAL DESC")
                    .setMaxResults(20)
                    .getResultList();
            //se eliminan las estadisticas antiguas
            em.createQuery("DELETE FROM EstadisticaMiembroPremio").executeUpdate();
            premiosRedimidos.forEach((t) -> {
                //cantidad de premios redimidos
                EstadisticaMiembroPremio emp = new EstadisticaMiembroPremio();
                emp.setIdMiembro((String) t[0]);
                emp.setCantPremiosRedimidos((int)((long)t[1]));
                em.merge(emp);
            });
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, e.getMessage(), e);
        }
    }
    
    /**
     * estadisticas de miembros nuevos y cantidad de miembros segmentados
     */
    @Schedule(hour = "01", minute = "30", info = "STATS_SEGMENTO")
    public void saveEstadisticasDiariasSegmentos() {
        AtomicInteger cantidadMiembrosSegmentados = new AtomicInteger(0);
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "ELEGIBILIDAD_MIEMBRO"));
            //se obtiene la fila (solo la columna familia de segmentos)
            Scan scan = new Scan();
            // scan.setFilter(new PageFilter(20));
            scan.addFamily(Bytes.toBytes("SEGMENTOS"));
            try (ResultScanner scanner = table.getScanner(scan)) {
                //itera los miembros
                AtomicInteger perteneceSegmento = new AtomicInteger(0);
                Map<String, Integer> segmentos = new HashMap<>();
                for (Result result = scanner.next(); result != null; result = scanner.next()) {
                   
                    //itera las columnas de mieembro por segmento
                    result.listCells().stream().forEach((cell) -> {
                        String key = Bytes.toString(Bytes.copy(cell.getQualifierArray(), cell.getQualifierOffset(), cell.getQualifierLength()));
                        if (!segmentos.containsKey(key)) {
                            segmentos.put(key, 0);//inicializa el valor si el id 
                        }
                        if (LocalDateTime.ofInstant(Instant.ofEpochMilli(cell.getTimestamp()), ZoneId.systemDefault()).isAfter(LocalDateTime.now().minusMonths(1))) {
                            segmentos.put(key, segmentos.get(key) + 1);
                        }
                        if (Bytes.toBoolean(Bytes.copy(cell.getValueArray(), cell.getValueOffset(),
                                cell.getValueLength()))) {
                            perteneceSegmento.getAndIncrement();
                        }
                    });
                    if (perteneceSegmento.get() > 0) {
                        cantidadMiembrosSegmentados.getAndIncrement();
                    }
                    perteneceSegmento.set(0);
                }
                EstadisticaSegmentoGeneral estad = em.find(EstadisticaSegmentoGeneral.class, 0);
                if(estad==null){
                    estad=new EstadisticaSegmentoGeneral();
                }
                estad.setId(0);
                estad.setCantMiembrosSegmentados(cantidadMiembrosSegmentados.get());
                estad.setCantMiembrosNoSegmentados(((Long)em.createQuery("select count(*) from Miembro").getSingleResult()).intValue() - cantidadMiembrosSegmentados.get());
                em.merge(estad);
                
                em.createQuery("DELETE FROM EstadisticaSegmentoIndividual").executeUpdate();
                segmentos.keySet().forEach((element) -> {
                    EstadisticaSegmentoIndividual esi = new EstadisticaSegmentoIndividual();
                    esi.setSegmento(em.find(Segmento.class, element));
                    esi.setMiembrosNuevos(segmentos.get(element));
                    em.persist(esi);
                });
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Estadisticas de miembro por rango de edades
     */
    @Schedule(hour = "01", minute = "45", info = "STATS_MIEMBRO")
    public void saveEstadisticasDiariasMiembros() {
        AtomicInteger cantMiembros1825 = new AtomicInteger(0);
        AtomicInteger cantMiembros2535 = new AtomicInteger(0);
        AtomicInteger cantMiembros3545 = new AtomicInteger(0);
        AtomicInteger cantMiembros4555 = new AtomicInteger(0);
        AtomicInteger cantMiembrosMayor55 = new AtomicInteger(0);
        //actualizar las estadisticas de rango de edades 
        em.createNamedQuery("Miembro.findAll", Miembro.class).getResultList().stream().forEach((Miembro miembro) -> {
            //count seconds between dates
            try{
            long years = ChronoUnit.YEARS.between(LocalDateTime.ofInstant(Instant.ofEpochMilli(miembro.getFechaNacimiento().getTime()), ZoneId.systemDefault()), LocalDateTime.now());
            
            
            if (!(years < 18)) {
                if (18 <= years && years <= 25) {
                    cantMiembros1825.getAndIncrement();
                } else if (years <= 35) {
                    cantMiembros2535.getAndIncrement();
                } else if (years <= 45) {
                    cantMiembros3545.getAndIncrement();
                } else if (years <= 55) {
                    cantMiembros4555.getAndIncrement();
                } else {
                    cantMiembrosMayor55.getAndIncrement();
                }
            }else{
//             Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Miembro fuera de rango de edades: {0} ", miembro.getIdMiembro());
            }
            }catch(NullPointerException ex){
//                Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Miembro con fecha de nacimiento nula: {0} ", miembro.getIdMiembro());
            }
        });
        em.createQuery("DELETE FROM EstadisticaMiembroGeneral").executeUpdate();
        em.flush();
        EstadisticaMiembroGeneral estadistica = new EstadisticaMiembroGeneral();
        estadistica.setId(0);
        estadistica.setCantMiembrosEdad1825(cantMiembros1825.get());
        estadistica.setCantMiembrosEdad2535(cantMiembros2535.get());
        estadistica.setCantMiembrosEdad3545(cantMiembros3545.get());
        estadistica.setCantMiembrosEdad4555(cantMiembros4555.get());
        estadistica.setCantMiembrosEdadMayor55(cantMiembrosMayor55.get());
        
        estadistica.setCantMiembrosReferidos((Long) em.createNamedQuery("Miembro.countByMiembroReferenteIsNotNull").getSingleResult());
        
        em.persist(estadistica);
    }

    /**
     * rejunta diaria de estadisticas de nivel de metrica
     */
    @Schedule(info = "STATS_METRICA_NIVEL")
    public void saveEstadisticasMetricaNivel() {
        em.createNamedQuery("Metrica.getIdMetricaIdGrupoNivelByIndEstado")
                .setParameter("indEstado", Metrica.Estados.PUBLICADO.getValue())
                .getResultList()
                .stream()
                .forEach((t) -> {
                    String idMetrica = (String) ((Object[]) t)[0];
                    String idGrupoNivel = (String) ((Object[]) t)[1];
                    List<String> miembros = em.createNamedQuery("Miembro.findAllIdMiembro").getResultList();
                    List<Double> progresosActuales = miembros.stream().map((idMiembro) -> {
                        MiembroBalanceMetrica balanceMetrica = em.find(MiembroBalanceMetrica.class, new MiembroBalanceMetricaPK(idMiembro, idMetrica));
                        if (balanceMetrica == null) {
                            return 0d;
                        } else {
                            return balanceMetrica.getProgresoActual();
                        }
                    }).collect(Collectors.toList());
                    List<Object[]> dataNivelMetrica = em.createNamedQuery("NivelMetrica.getIdNivelMetricaInicialByIdGrupoNivel").setParameter("idGrupoNivel", idGrupoNivel).getResultList();
                    
                    List<EstadisticaIndividualMetricaNivel> estadisticas = dataNivelMetrica.stream().map((n) -> {
                        return new EstadisticaIndividualMetricaNivel((String) n[0], idMetrica, progresosActuales.stream().filter((p) -> p>=(Double) n[1]).count());
                    }).collect(Collectors.toList());

                    if (!estadisticas.isEmpty()) {
                        long ajuste = estadisticas.stream().min((o1, o2) -> Long.compare(o1.getCantMiembros(), o2.getCantMiembros())).get().getCantMiembros();

                        estadisticas.forEach((e) -> {
                            if (e.getCantMiembros()>ajuste) {
                                e.setCantMiembros(e.getCantMiembros()-ajuste);
                            }
                        });
                    }
                    
                    em.createNamedQuery("EstadisticaIndividualMetricaNivel.deleteByIdMetrica").setParameter("idMetrica", idMetrica).executeUpdate();
                    estadisticas.forEach((e) -> em.persist(e));
                });
    }
}
