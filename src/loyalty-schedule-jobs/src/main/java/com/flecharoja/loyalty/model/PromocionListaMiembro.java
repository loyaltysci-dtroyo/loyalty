package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PROMOCION_LISTA_MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PromocionListaMiembro.getIdMiembroByIdPromocionIndTipo", query = "SELECT p.promocionListaMiembroPK.idMiembro FROM PromocionListaMiembro p WHERE p.promocionListaMiembroPK.idPromocion = :idPromocion AND p.indTipo = :indTipo")
})
public class PromocionListaMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PromocionListaMiembroPK promocionListaMiembroPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;

    public PromocionListaMiembro() {
    }

    public PromocionListaMiembroPK getPromocionListaMiembroPK() {
        return promocionListaMiembroPK;
    }

    public void setPromocionListaMiembroPK(PromocionListaMiembroPK promocionListaMiembroPK) {
        this.promocionListaMiembroPK = promocionListaMiembroPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo!=null?Character.toUpperCase(indTipo):null;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (promocionListaMiembroPK != null ? promocionListaMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromocionListaMiembro)) {
            return false;
        }
        PromocionListaMiembro other = (PromocionListaMiembro) object;
        if ((this.promocionListaMiembroPK == null && other.promocionListaMiembroPK != null) || (this.promocionListaMiembroPK != null && !this.promocionListaMiembroPK.equals(other.promocionListaMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.PromocionListaMiembro[ promocionListaMiembroPK=" + promocionListaMiembroPK + " ]";
    }
    
}
