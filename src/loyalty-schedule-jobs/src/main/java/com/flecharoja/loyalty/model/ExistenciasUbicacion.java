/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "EXISTENCIAS_UBICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExistenciasUbicacion.findAll", query = "SELECT e FROM ExistenciasUbicacion e"),
    @NamedQuery(name = "ExistenciasUbicacion.findByPremioByPeriodoByMes", query = "SELECT SUM(e.saldoActual) FROM ExistenciasUbicacion e WHERE e.premio.idPremio = :idPremio AND e.existenciasUbicacionPK.mes = :mes AND e.existenciasUbicacionPK.periodo = :periodo"),
    @NamedQuery(name = "ExistenciasUbicacion.findPremiosByUbicacion", query = "SELECT e.premio FROM ExistenciasUbicacion e WHERE e.existenciasUbicacionPK.codUbicacion = :ubicacion AND e.existenciasUbicacionPK.mes = :mes and e.existenciasUbicacionPK.periodo = :periodo"),
    @NamedQuery(name = "ExistenciasUbicacion.findByUbicacion", query = "SELECT e FROM ExistenciasUbicacion e WHERE e.existenciasUbicacionPK.codUbicacion = :idUbicacion AND e.existenciasUbicacionPK.mes = :mes AND e.existenciasUbicacionPK.periodo = :periodo"),
    @NamedQuery(name = "ExistenciasUbicacion.findByMesPeriodo", query = "SELECT e FROM ExistenciasUbicacion e WHERE e.existenciasUbicacionPK.mes = :mes AND e.existenciasUbicacionPK.periodo = :periodo"),
    @NamedQuery(name = "ExistenciasUbicacion.findByUbicacionCantidad", query = "SELECT e FROM ExistenciasUbicacion e WHERE e.existenciasUbicacionPK.codUbicacion = :idUbicacion AND e.existenciasUbicacionPK.mes = :mes AND e.existenciasUbicacionPK.periodo = :periodo AND e.saldoActual >=1"),
    @NamedQuery(name = "ExistenciasUbicacion.countPremiosByUbicacion", query = "SELECT COUNT(e.existenciasUbicacionPK.codPremio) FROM ExistenciasUbicacion e WHERE e.existenciasUbicacionPK.codUbicacion = :ubicacion AND e.existenciasUbicacionPK.mes = :mes AND e.existenciasUbicacionPK.periodo = :periodo")

})
public class ExistenciasUbicacion implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "SALDO_ACTUAL")
    private Integer saldoActual;

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ExistenciasUbicacionPK existenciasUbicacionPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "SALDO_INICIAL")
    private Integer saldoInicial;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ENTRADAS")
    private Integer entradas;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "SALIDAS")
    private Integer salidas;
    
    @JoinColumn(name = "COD_PREMIO", referencedColumnName = "ID_PREMIO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Premio premio;
    
    @JoinColumn(name = "COD_UBICACION", referencedColumnName = "ID_UBICACION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ubicacion ubicacion;
    
    @Transient
    private String codPremio;

    public ExistenciasUbicacion() {
    }

    public ExistenciasUbicacion(ExistenciasUbicacionPK existenciasUbicacionPK) {
        this.existenciasUbicacionPK = existenciasUbicacionPK;
    }

    public ExistenciasUbicacion(ExistenciasUbicacionPK existenciasUbicacionPK, Integer saldoInicial, Integer entradas, Integer salidas, Integer saldoActual) {
        this.existenciasUbicacionPK = existenciasUbicacionPK;
        this.saldoInicial = saldoInicial;
        this.entradas = entradas;
        this.salidas = salidas;
        this.saldoActual = saldoActual;
    }

    public ExistenciasUbicacion(String codUbicacion, String codPremio, Integer mes, Integer periodo) {
        this.existenciasUbicacionPK = new ExistenciasUbicacionPK(codUbicacion, codPremio, mes, periodo);
    }

    public ExistenciasUbicacionPK getExistenciasUbicacionPK() {
        return existenciasUbicacionPK;
    }

    public void setExistenciasUbicacionPK(ExistenciasUbicacionPK existenciasUbicacionPK) {
        this.existenciasUbicacionPK = existenciasUbicacionPK;
    }

    public Integer getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(Integer saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public Integer getEntradas() {
        return entradas;
    }

    public void setEntradas(Integer entradas) {
        this.entradas = entradas;
    }

    public Integer getSalidas() {
        return salidas;
    }

    public void setSalidas(Integer salidas) {
        this.salidas = salidas;
    }

    public Premio getPremio() {
        return premio;
    }

    public void setPremio(Premio premio) {
        this.premio = premio;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (existenciasUbicacionPK != null ? existenciasUbicacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExistenciasUbicacion)) {
            return false;
        }
        ExistenciasUbicacion other = (ExistenciasUbicacion) object;
        if ((this.existenciasUbicacionPK == null && other.existenciasUbicacionPK != null) || (this.existenciasUbicacionPK != null && !this.existenciasUbicacionPK.equals(other.existenciasUbicacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ExistenciasUbicacion[ existenciasUbicacionPK=" + existenciasUbicacionPK + " ]";
    }

    public Integer getSaldoActual() {
        return saldoActual;
    }

    public void setSaldoActual(Integer saldoActual) {
        this.saldoActual = saldoActual;
    }

    public String getCodPremio() {
        return codPremio;
    }

    public void setCodPremio(String codPremio) {
        this.codPremio = codPremio;
    }   
    
}
