package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_LISTA_MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MisionListaMiembro.getIdMiembroByIdMisionIndTipo", query = "SELECT m.misionListaMiembroPK.idMiembro FROM MisionListaMiembro m WHERE m.misionListaMiembroPK.idMision = :idMision AND m.indTipo = :indTipo")
})
public class MisionListaMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MisionListaMiembroPK misionListaMiembroPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;

    public MisionListaMiembro() {
    }

    public MisionListaMiembroPK getMisionListaMiembroPK() {
        return misionListaMiembroPK;
    }

    public void setMisionListaMiembroPK(MisionListaMiembroPK misionListaMiembroPK) {
        this.misionListaMiembroPK = misionListaMiembroPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo == null ? null : Character.toUpperCase(indTipo);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (misionListaMiembroPK != null ? misionListaMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionListaMiembro)) {
            return false;
        }
        MisionListaMiembro other = (MisionListaMiembro) object;
        if ((this.misionListaMiembroPK == null && other.misionListaMiembroPK != null) || (this.misionListaMiembroPK != null && !this.misionListaMiembroPK.equals(other.misionListaMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionListaMiembro[ misionListaMiembroPK=" + misionListaMiembroPK + " ]";
    }
    
}
