/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author wtencio
 */
@Embeddable
public class ExistenciasUbicacionPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "COD_UBICACION")
    private String codUbicacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "COD_PREMIO")
    private String codPremio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MES")
    private Integer mes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PERIODO")
    private Integer periodo;

    public ExistenciasUbicacionPK() {
    }

    public ExistenciasUbicacionPK(String codUbicacion, String codPremio, Integer mes, Integer periodo) {
        this.codUbicacion = codUbicacion;
        this.codPremio = codPremio;
        this.mes = mes;
        this.periodo = periodo;
    }

    public String getCodUbicacion() {
        return codUbicacion;
    }

    public void setCodUbicacion(String codUbicacion) {
        this.codUbicacion = codUbicacion;
    }

    public String getCodPremio() {
        return codPremio;
    }

    public void setCodPremio(String codPremio) {
        this.codPremio = codPremio;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Integer getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Integer periodo) {
        this.periodo = periodo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codUbicacion != null ? codUbicacion.hashCode() : 0);
        hash += (codPremio != null ? codPremio.hashCode() : 0);
        hash += (mes != null ? mes.hashCode() : 0);
        hash += (periodo != null ? periodo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExistenciasUbicacionPK)) {
            return false;
        }
        ExistenciasUbicacionPK other = (ExistenciasUbicacionPK) object;
        if ((this.codUbicacion == null && other.codUbicacion != null) || (this.codUbicacion != null && !this.codUbicacion.equals(other.codUbicacion))) {
            return false;
        }
        if ((this.codPremio == null && other.codPremio != null) || (this.codPremio != null && !this.codPremio.equals(other.codPremio))) {
            return false;
        }
        if ((this.mes == null && other.mes != null) || (this.mes != null && !this.mes.equals(other.mes))) {
            return false;
        }
        if ((this.periodo == null && other.periodo != null) || (this.periodo != null && !this.periodo.equals(other.periodo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ExistenciasUbicacionPK[ codUbicacion=" + codUbicacion + ", codPremio=" + codPremio + ", mes=" + mes + ", periodo=" + periodo + " ]";
    }
    
}
