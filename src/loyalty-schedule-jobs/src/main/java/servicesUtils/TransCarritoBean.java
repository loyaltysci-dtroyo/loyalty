/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicesUtils;


import com.flecharoja.loyalty.model.CertificadoCategoria;
import com.flecharoja.loyalty.model.TransCarrito;
import java.util.List;
import java.util.Locale;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class TransCarritoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Método que obtiene un carrito disponible
     *
     * @param idMiembro
     * @param idCategoriaProducto
     * @param locale
     * @return Carritos
     */
    public List<TransCarrito> getTransCarrito(String idMiembro, String idCategoriaProducto, Locale locale) {
        
        //Obtener de todas los carritos 
        List<TransCarrito> carritos = em.createNamedQuery("TransCarrito.findAvailable").setParameter("idMiembro", idMiembro).setParameter("idCategoria", idCategoriaProducto).setParameter("estado", 'P').getResultList();
        if (carritos.isEmpty()) {
            return carritos;
        }
        CertificadoCategoria certificado = em.find(CertificadoCategoria.class, carritos.get(0).getIdCertificado());
        if (certificado != null) {
            carritos.get(0).setNumCertificado(certificado.getNumCertificado());
        }
        return carritos;
    }
    
    /**
     * Método que obtiene un carrito usando el id certificado
     *
     * @param idCertificado
     * @param idCategoria
     * @return Carrito
     */
    public TransCarrito getTransCarritoByCertificado(String idCertificado, String idCategoria) {
        List<TransCarrito> carritos = em.createNamedQuery("TransCarrito.findByCertificado").setParameter("idCertificado", idCertificado).setParameter("idCategoria", idCategoria).getResultList();
        return carritos.get(0);
    }
    
    
    
    /**
     * Método que inserta un nuevo carrito en la base de datos
     *
     * @param numTransaccion
     * @return identificador del producto registrado
     */
    public String deleteTransCarrito(String numTransaccion) {
        TransCarrito transCarrito = em.find(TransCarrito.class, numTransaccion);
        if (transCarrito != null) {
            try {
                em.remove(transCarrito);
                em.flush();
            } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
                return null;
            }
        }
        return numTransaccion;
    }
}
