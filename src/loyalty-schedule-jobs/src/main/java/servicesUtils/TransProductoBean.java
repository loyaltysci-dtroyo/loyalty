/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicesUtils;


import com.flecharoja.loyalty.model.TransProducto;
import com.flecharoja.loyalty.model.TransProductoPK;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class TransProductoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    TransCarritoBean transCarritoBean;
    
    @EJB
    ProductoBean productoBean;
    
    /**
     * Método que obtiene un arrito disponible
     *
     * @param numTransaccion
     * @return Carritos
     */
    public List<TransProducto> getProductos(String numTransaccion) {
        
        //obtencion de todas los carritos 
        return em.createNamedQuery("TransProducto.findByIdTransProducto").setParameter("numTransaccion", numTransaccion).getResultList();
    }
    
    /**
     * Método que inserta un nuevo carrito en la base de datos
     *
     * @param numTransaccion
     */
    public void deleteTransProductos(String numTransaccion) {
        List<TransProducto> productos = getProductos(numTransaccion);
        for(TransProducto producto : productos) {
            TransProductoPK transPK = new TransProductoPK(numTransaccion, producto.getTransProductoPK().getIdProducto());
            TransProducto transProducto = em.find(TransProducto.class, transPK);
            if (transProducto == null || transProducto.getCantidad() < producto.getCantidad()) {
                return;
            }
            productoBean.updateCantidadProducto(producto.getTransProductoPK().getIdProducto(), -producto.getCantidad());
            try {
                em.remove(transProducto);
                em.flush();
                transProducto.setCantidad(0);
            } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
                productoBean.updateCantidadProducto(producto.getTransProductoPK().getIdProducto(), producto.getCantidad());
            }
        }
    }
}
