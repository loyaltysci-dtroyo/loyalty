/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicesUtils;


import com.flecharoja.loyalty.model.Producto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class ProductoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Metodo que obtiene la informacion de un producto por el identificador de
     * la misma
     *
     * @param idProducto Identificador del producto
     * @param locale
     * @return Respuesta con la informacion del producto encontrado
     */
    public Producto getProducto(String idProducto) {

        Producto producto = em.find(Producto.class, idProducto);
        return producto;
    }
    
    /**
     * Metodo que obtiene la informacion de un producto por el identificador de
     * la misma
     *
     * @param idProducto Identificador del producto
     * @param cantidad
     * @return Respuesta con la informacion del producto encontrado
     */
    public Producto updateCantidadProducto(String idProducto, Integer cantidad) {
        Producto producto = em.find(Producto.class, idProducto);
        //verificacion de que la entidad exista
        if (producto == null) {
            return null;
        }
        
        if (cantidad > 0) {
            if (producto.getSaldoDisponible() < cantidad) {
                return null;
            }
        } else {
            if ((producto.getSaldoDisponible() - cantidad) > producto.getSaldoInicial()) {
                return null;
            }
        }
        
        producto.setSaldoDisponible(producto.getSaldoDisponible() - cantidad);
        
        try {
            em.merge(producto);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//en caso de que alguna informacion no este valida o este "vieja"
            return null;
        }

        return producto;
    }
}
