import { LoyaltyComerciosPage } from './app.po';

describe('loyalty-comercios App', () => {
  let page: LoyaltyComerciosPage;

  beforeEach(() => {
    page = new LoyaltyComerciosPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
