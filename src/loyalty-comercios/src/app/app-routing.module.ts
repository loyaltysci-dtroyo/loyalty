import { ReportesComponent } from './reportes/reportes.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


export const routes: Routes = [
    { path: '', redirectTo: '/reportes', pathMatch: 'full' },
    {
        path: 'reportes', component: ReportesComponent
    }
];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
