import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { KeycloakService } from "./app/service/keycloak.service"

if (environment.production) {
  enableProdMode();
}

if (KeycloakService.init()) {
  platformBrowserDynamic().bootstrapModule(AppModule, { providers: [KeycloakService] });
} else {
  //REDIRECT O RELOAD
}

