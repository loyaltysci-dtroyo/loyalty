import { LoyaltyWebpagePage } from './app.po';

describe('loyalty-webpage App', () => {
  let page: LoyaltyWebpagePage;

  beforeEach(() => {
    page = new LoyaltyWebpagePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
