import { KeycloakService } from './app/service';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}
KeycloakService.init().then(
  (status) => {
    if (status === 'OK' || status === 'NOAUTH') {
      doBoobstrap();
    }
  },
  () => {
    // window.location.reload();
  }
);
//workaround para evitar falso positivo en deteccion del bootstrap de angularCLI
//TODO: quitar cuando arreglen el bug

function doBoobstrap() {
  platformBrowserDynamic().bootstrapModule((AppModule));
}
