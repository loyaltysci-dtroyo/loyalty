import { MessagesService } from './service/messages.service';
import { ErrorHandlersService } from './service/index';
import { Component, OnInit } from '@angular/core';
import { KeycloakService } from './service/index';

import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ErrorHandlersService, MessagesService]
})
export class AppComponent implements OnInit {
  title = 'app works!';
  constructor(private kc: KeycloakService,
    private router: Router) {

  }
  ngOnInit() {
    
  }

}
