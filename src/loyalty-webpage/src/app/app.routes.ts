import { AuthGuard } from './service/auth.guard';
import { Routes } from '@angular/router';
import * as  Components from './index';
export const appRoutes: Routes = [
  // ruta inicial (index)
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
  { path: 'passwordreset', component: Components.RequestPasswordResetComponent },
  { path: 'passwordresetconfirm', component: Components.ResetPasswordComponent },
  // rutas publicas
  {
    path: 'landing', component: Components.LandingComponent,
    children: [
      { path: '', redirectTo: 'dash', pathMatch: 'full' },
      { path: 'dash', component: Components.DashComponent },
      { path: 'register', component: Components.RegisterComponent },
      { path: 'info', component: Components.InfoComponent },
      { path: 'contact', component: Components.ContactComponent }
    ]
  },
  // rutas protegidas
  {
    path: 'home', component: Components.HomeComponent,
    children: [
      { path: '', redirectTo: 'mission', pathMatch: 'full' },
      { path: 'complete-mission', component: Components.CompleteMisssionComponent, canActivate: [AuthGuard] },
      { path: 'account', component: Components.AccountComponent, canActivate: [AuthGuard] },
      { path: 'activity', component: Components.ActivityComponent, canActivate: [AuthGuard] },
      { path: 'mission', component: Components.MissionComponent, canActivate: [AuthGuard] },
      { path: 'profile', component: Components.ProfileComponent, canActivate: [AuthGuard] },
      { path: 'contact', component: Components.ContactComponent, canActivate: [AuthGuard] },
      { path: 'offer-detail', component: Components.OfferDetailComponent, canActivate: [AuthGuard] },
      { path: 'rewards', component: Components.RewardsComponent, canActivate: [AuthGuard] },
      { path: 'reward-detail/:id', component: Components.RewardsDetailComponent, canActivate: [AuthGuard] },
      
    ]
  },
  { path: 'accerror', component: Components.AccountErrorComponent },
  // ruta no encontrada
  { path: '**', component: Components.NotFoundComponent }
];
