import { MemberBalance, MemberBalanceProgress } from '../../model/index';
import { MemberService, ErrorHandlersService, MessagesService } from '../../service/index';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  providers: [MemberService]
})
export class AccountComponent implements OnInit {
  public file: any;
  public usuario: any;
  public file_src: string;
  public balance: MemberBalance;
  public progress: any;
  public porcentajeCompletado: number;
  public pointsToGo: number;
  public lastLevel: boolean;

  constructor(
    private memberService: MemberService,
    private messageService: MessagesService,
    private errorHandlerService: ErrorHandlersService
  ) {
    this.porcentajeCompletado = 0;
    this.file_src = '';
    this.progress = new MemberBalanceProgress();
    this.balance = new MemberBalance();
    this.lastLevel = false;
  }

  ngOnInit() {
    this.getBalance();
    this.getProgress();
  }

  getBalance() {
    let sub = this.memberService.getBalance().subscribe(
      (balance: MemberBalance) => {
        this.balance = balance;
        this.updateToGo();
      },
      (err) => { this.handleError(err); },
      () => { sub.unsubscribe(); }
    );
  }
  getProgress() {
    let sub = this.memberService.getProgress().subscribe(
      (progress: MemberBalanceProgress) => {
        this.progress = progress;
        if (this.progress.niveles[this.progress.niveles.length - 1].idNivel === this.progress.nivelActual.idNivel) {
          this.porcentajeCompletado = 100;
          this.lastLevel = true;
        } else {
          this.updateToGo();
        }
      },
      (err) => { this.handleError(err); },
      () => { sub.unsubscribe(); }
    );
  }


  updateToGo() {
    if (this.balance && this.progress) {
      this.porcentajeCompletado = Math.round((this.progress.progreso * 100)/this.progress.tope );
      let togo = Math.round(this.progress.tope - this.progress.progreso);
      if (togo > 0) {
        this.pointsToGo = togo;
      }else{
        this.pointsToGo = 0;
      }
    }
  }
  private handleError(response: Response) {
    this.messageService.mostrarError(this.errorHandlerService.manejaErrorGrowl(response));
  }



}
