import { KeycloakService } from '../../service';
import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'error-account',
    templateUrl: 'error-account.component.html'
})
export class AccountErrorComponent implements OnInit {
    constructor(private kc:KeycloakService) { }

    ngOnInit() { }
     logout(){
        this.kc.logout();
    }
}