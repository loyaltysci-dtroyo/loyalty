import { KeycloakService } from '../../service';
import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'not-found',
    templateUrl: 'not-found.component.html'
})
export class NotFoundComponent implements OnInit {
    constructor(private kc:KeycloakService) { }

    ngOnInit() { }

   
}