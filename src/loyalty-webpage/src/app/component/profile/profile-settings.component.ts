import { ErrorHandlersService } from '../../service/error-handler.service';
import { MessagesService } from '../../service/messages.service';
import { Miembro } from '../../model/miembro';
import { Component, OnInit } from '@angular/core';
import { MemberService, KeycloakService } from '../../service/index';
import { Response } from '@angular/http';
@Component({
    moduleId: module.id,
    selector: 'profile-settings',
    templateUrl: 'profile-settings.component.html'
})
export class ProfileSettingsComponent implements OnInit {

    constructor(public miembro: Miembro,
        private miembroService: MemberService,
        private ehs: ErrorHandlersService,
        private msgService: MessagesService) {

    }
    ngOnInit() {

    }
    getUser() {
        this.miembroService.getMember().subscribe(
            (success) => {
            }, (err) => { this.manejaError(err) }, () => { },
        );
    }
    updateUser() {
        this.miembroService.editMember(this.miembro).subscribe(
            (success) => {
                this.getUser();
                this.msgService.mostrarSuccess({header:'Success', body:'Your profile has been updated'});
            }, (err) => { this.manejaError(err) }, () => { },
        );
    }

    manejaError(err: Response) {
        this.msgService.mostrarError(this.ehs.manejaErrorGrowl(err));
    }
}