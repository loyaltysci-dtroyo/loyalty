import { ErrorHandlersService } from '../../service/error-handler.service';
import { MessagesService } from '../../service/messages.service';
import { Miembro } from '../../model/miembro';
import { Component, OnInit } from '@angular/core';
import { MemberService, KeycloakService } from '../../service/index';
import { Response } from '@angular/http';
import { DatepickerConfig } from 'ng2-bootstrap';
declare var $: any;
@Component({
    moduleId: module.id,
    selector: 'profile-additional',
    templateUrl: 'profile-additional.component.html',
    providers: [DatepickerConfig]
})

export class ProfileAdditionalComponent implements OnInit {

    constructor(public miembro: Miembro,
        private miembroService: MemberService,
        private ehs: ErrorHandlersService,
        private msgService: MessagesService) {

    }
    ngOnInit() {
       

    }
    getUser() {
        this.miembroService.getMember().subscribe(
            (success) => {
            }, (err) => {
                this.manejaError(err);
            }, () => { },
        );
    }
    updateUser() {
        this.miembroService.editMember(this.miembro).subscribe(
            (success) => {
                this.getUser();
                this.msgService.mostrarSuccess({header:'Success', body:'Your profile has been updated'});
            }, (err) => {
                this.manejaError(err);
            }, () => { },
        );
    }

    manejaError(err: Response) {
        this.msgService.mostrarError(this.ehs.manejaErrorGrowl(err));
    }
}