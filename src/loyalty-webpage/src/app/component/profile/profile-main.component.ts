import { ErrorHandlersService } from '../../service/error-handler.service';
import { MessagesService } from '../../service/messages.service';
import { Miembro } from '../../model/miembro';
import { Component, OnInit } from '@angular/core';
import { MemberService } from '../../service/index';
import { Response } from '@angular/http';
@Component({
    moduleId: module.id,
    selector: 'profile-main',
    templateUrl: 'profile-main.component.html'
})
export class ProfileMainComponent implements OnInit {
    file: any;
    public usuario: any;
    file_src: string = "";
    resetPassword:any;
    constructor(public miembro: Miembro,
        private miembroService: MemberService,
        private ehs: ErrorHandlersService,
        private msgService: MessagesService) {

    }
    ngOnInit() {
        this.file_src = this.miembro.avatar;

    }
    getUser() {
        this.miembroService.getMember().subscribe(
            (success) => {
            }, (err) => { this.manejaError(err) }, () => { },
        );
    }
    updateUser() {
        this.miembroService.editMember(this.miembro).subscribe(
            (success) => {
                this.getUser();
                this.msgService.mostrarSuccess({header:'Success', body:'Your profile has been updated'});
            }, (err) => { this.manejaError(err) }, () => { },
        );
    }
    readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            // reader.onload = function (e) {
            //   $('#img-upload').attr('src', e.target.result);
            // }

            reader.readAsDataURL(input.files[0]);
        }
    }
    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {
        this.file = input.files[0];
        console.log(this.file);
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        let reader: any
        let target: EventTarget;
        reader = new FileReader(); // Crear un FileReader

        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            img.src = event.target.result; // Obtener el (base64 de la imagen)
            this.miembro.avatar = img.src.split(',')[1];
            this.file_src = img.src; // Redimensionar la imagen
        }, false);
        reader.readAsDataURL(input.files[0]);
    }

    manejaError(err: Response) {
        this.msgService.mostrarError    (this.ehs.manejaErrorGrowl(err));
    }
}