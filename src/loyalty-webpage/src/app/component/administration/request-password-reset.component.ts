import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { AdministracionService, MessagesService, ErrorHandlersService } from '../../service/index';
/**
 * Componente para solicitar un proceso de cambio de contraseña
 */
@Component({
    selector: 'request-passowrd-reset',
    templateUrl: 'request-password-reset.component.html',
    providers: [AdministracionService]
})
export class RequestPasswordResetComponent implements OnInit {
    public response: any;
    public request: any;
    public respuestaok: boolean;
    constructor(
        public messageService: MessagesService,
        public errorHandlerService: ErrorHandlersService,
        private administracionService: AdministracionService
    ) {
        this.respuestaok = false;
    }

    ngOnInit() {
        this.request = { email: "" };
        this.response = {};
    }

    resetPassword() {
        let sub = this.administracionService.requestResetPassword(this.request).subscribe(
            (response: Response) => { if (response.status == 204) { this.respuestaok = true; } },
            (error) => { this.handleError(error); },
            () => { sub.unsubscribe(); }
        );
    }

    /**
     * Handles errors thrown during http requests
     * @param response object representing the server response
     */
    public handleError(response: Response) {
        this.messageService.mostrarError(this.errorHandlerService.manejaErrorGrowl(response));
    }
}