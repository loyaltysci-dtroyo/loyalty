import { Response } from '@angular/http';
import { AdministracionService, MessagesService, ErrorHandlersService } from '../../service/index';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
@Component({
    selector: 'reset-password',
    templateUrl: 'reset-password.component.html',
    providers: [AdministracionService]
})

export class ResetPasswordComponent implements OnInit {
    public response: any;
    public request: any;
    public form: FormGroup;
    public idMiembro: string;
    public idPeticion: string;
    public respuestaok: boolean;
    public pwdconfirm: string;
    public pwd: string;

    constructor(
        public messageService: MessagesService,
        public errorHandlerService: ErrorHandlersService,
        public administracionService: AdministracionService,
        public activatedRoute: ActivatedRoute,
        public fb: FormBuilder
    ) {
        this.respuestaok = false;
        this.form = fb.group({
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required]
        },
            {
                validator: PasswordValidation.MatchPassword // your validation method
            });
    }

    ngOnInit() {
        this.request = { contrasena: '', idMiembro: '', idPeticion: '' };
        // subscribe to router event
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            console.log(params);
            this.request.idMiembro = params["idM"];
            this.request.idPeticion = params["idP"];
        });

        this.form.statusChanges.subscribe((e) => { console.log(e); });
    }

    requestPasswordReset() {
        if (this.pwdconfirm == this.pwd) {
            this.request.contrasena = this.pwd;
            let sub = this.administracionService.resetPassword(this.request).subscribe(
                (response: Response) => {
                    if (response.status == 204) {
                        this.respuestaok = true;
                    }
                },
                (error) => { this.handleError(error) },
                () => { sub.unsubscribe(); }
            );
        }
    }

    /**
     * Handles errors thrown during http requests
     * @param response object representing the server response
     */
    public handleError(response: Response) {
        this.messageService.mostrarError(this.errorHandlerService.manejaErrorGrowl(response));
    }
}
import { AbstractControl } from '@angular/forms';
export class PasswordValidation {

    static MatchPassword(AC: AbstractControl) {
        let password = AC.get('password').value; // to get value in input tag
        let confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
        if (password != confirmPassword) {
            console.log('false');
            AC.get('confirmPassword').setErrors({ MatchPassword: true })
        } else {
            console.log('true');
            return null;
        }
    }
}