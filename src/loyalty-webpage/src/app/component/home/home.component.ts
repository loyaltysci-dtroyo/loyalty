import { Offer } from '../../model/offer';
import { OfferService } from '../../service/offer.service';
import { MessagesService } from '../../service/messages.service';
import { ErrorHandlersService } from '../../service/error-handler.service';
import { Miembro } from '../../model/miembro';
import { KeycloakService, MisionService } from '../../service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MemberService } from '../../service/index';
import { Response } from '@angular/http';
import { Mision } from '../../model/';
declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ErrorHandlersService, MessagesService, Mision, MisionService, Offer, OfferService]
})
export class HomeComponent implements OnInit {

  constructor(private kc: KeycloakService,
    private msgService: MessagesService,
    private errHandlerSvc: ErrorHandlersService,
    private router: Router,
    public miembro: Miembro,
    private memberService: MemberService) {

  }
  copyValuesOf(objDestino, objFuente) {
    for (var property in objFuente) {
      objDestino[property] = objFuente[property];
    }
  }
  ngOnInit() {
    let sub = this.memberService.getMember().subscribe(
      (data: Miembro) => {
        this.copyValuesOf(this.miembro, data);
      },
      (error) => {
        this.router.navigate(['/accerror']);
        KeycloakService.auth.accError = true;
      },
      () => { sub.unsubscribe(); }
    );
  }

  logout() {
    this.kc.logout();
  }

  /**
   * Maneja error a nvel de componente
   */
  manejaError(error: Response) {
    this.msgService.mostrarError(error);
  }
}