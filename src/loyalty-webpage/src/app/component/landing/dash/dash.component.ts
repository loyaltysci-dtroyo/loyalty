import { LandingComponent } from '../../index';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css']
})
export class DashComponent implements OnInit {

  constructor(private landingComponent:LandingComponent) { }

  ngOnInit() {
    
  }
  mostarModalSignIn(){
      this.landingComponent.mostarModalSignIn();
  }


}
