import { Component, OnInit } from '@angular/core';
import { KeycloakService } from '../../service/index';
import { Router } from '@angular/router';

declare var $:any;
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  title = 'app works!';
  constructor(private kc: KeycloakService,
    private router: Router) {

  }

  ngOnInit() {
    if(KeycloakService.auth.loggedIn){
      this.router.navigate(['/home/']);
    }
  }
  initKCService() {

  }

  mostarModalSignIn(){
    // console.log("asdasdf");
     $('#modalSignIn').modal();
  }
  gotoRegister(){
      $('#modalSignIn').modal('hide');
    this.router.navigate(['/landing/register']);
  }

  login() {
    KeycloakService.login().then((status) => {
      this.router.navigate(['/home']);
    });
  }
}



