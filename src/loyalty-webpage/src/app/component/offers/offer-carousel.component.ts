import { Offer } from '../../model/offer';
import { OfferService } from '../../service/offer.service';
import { ErrorHandlersService, MessagesService } from '../../service/index';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { Router } from '@angular/router';

declare var $: any;
@Component({
    moduleId: module.id,
    selector: 'offer-carousel',
    templateUrl: 'offer-carousel.component.html',
    providers: [Offer, OfferService]
})
export class OffersCarouselComponent implements OnInit {
    public offers: Offer[];
    constructor(
        private router: Router,
        private messageService: MessagesService,
        private errorHandlerService: ErrorHandlersService,
        private offerService: OfferService,
        public selectedOffer: Offer
    ) { }


    ngOnInit() {
        this.getOffers();
    }
    gotoDetail(offer: Offer) {
        for(let prop in offer){
            this.selectedOffer[prop] = offer[prop];
        }
        
        $('#portfolioModal').modal('show');
        // this.router.navigate(['/home/offer-detail/',offer.idPromocion ]);

    }
    getOffers() {
        let sub = this.offerService.getOffers().subscribe(
            (data: Offer[]) => {
                this.offers = data;
            },
            (err) => {
                this.handleError(err);
            },
            () => {
                sub.unsubscribe();
            }
        );
    }
    private handleError(response: Response) {
        this.messageService.mostrarError(this.errorHandlerService.manejaErrorGrowl(response));
    }
}