import { OfferService } from '../../service/offer.service';
import { Offer } from '../../model/offer';
import { Component, OnInit,Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'offer-detail',
    templateUrl: 'offer-detail.component.html'
})
export class OfferDetailComponent implements OnInit {
    @Input() offer:Offer;
    constructor(
        private offerService: OfferService
    ) { }

    ngOnInit() { }

}