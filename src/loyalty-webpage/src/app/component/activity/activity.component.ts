import { Offer } from '../../model/offer';
import { OfferService } from '../../service/offer.service';
import {RewardService} from '../../service/';
import { Reward } from '../../model/';
import { MemberHistory } from '../../model/member-history';
import { MemberBalance, MemberBalanceProgress } from '../../model/index';
import { MemberService, ErrorHandlersService, MessagesService } from '../../service/index';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css'],
  providers: [RewardService]
})
export class ActivityComponent implements OnInit {

  public history: MemberHistory[];
  public redeemHistory: Reward[];

  constructor(
    public memberService: MemberService,
    public messageService: MessagesService,
    public errorHandlerService: ErrorHandlersService,
    public premioService: RewardService,
    public offerService: OfferService,
    public offer: Offer
  ) {
  }

  ngOnInit() {
    this.getHistory();
    this.getRedeemHistory();
  }
 /**
  * Gets the Main metric's history
  */
  getHistory() {
    let sub = this.memberService.getHistory().subscribe(
      (data: MemberHistory[]) => {
        this.history = data;
      },
      (err) => { this.handleError(err) },
      () => { sub.unsubscribe(); }
    );
  }

/**
 * Gets the Reward redeem history
 */
  getRedeemHistory() {
    let sub = this.premioService.getRewardsHistory().subscribe(
      (data: Reward[]) => {
        this.redeemHistory = data;
      },
      (err) => { this.handleError(err) },
      () => { sub.unsubscribe(); }
    );
  }
/**
 * Handles errors thrown during http requests
 * @param response object representing the server response
 */
  public handleError(response: Response) {
    this.messageService.mostrarError(this.errorHandlerService.manejaErrorGrowl(response));
  }
}

