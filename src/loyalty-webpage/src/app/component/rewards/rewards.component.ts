import { MessagesService } from '../../service/messages.service';
import { ErrorHandlersService } from '../../service/error-handler.service';
import { RewardService } from '../../service/';
import { Component, OnInit } from '@angular/core';
import { Reward } from '../../model/';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-rewards',
  templateUrl: './rewards.component.html',
  providers: [Reward, RewardService]
})
export class RewardsComponent implements OnInit {
  public rowOffset: number;
  public rowsPerPage: number;
  public statusRedeem: any;
  public listaRewards: Reward[];
  constructor(
    private rewardService: RewardService,
    private errorHandlerService: ErrorHandlersService,
    private messageService: MessagesService,
    private router: Router,
    public reward: Reward
  ) {
    
  }
  
  ngOnInit() {
   
    this.getRewards();
  }
  /**
   * clones an object
   * @param fuente source object
   * @param destino target object
   */
  setValues(fuente: any, destino: any) {
    for (let prop in fuente) {
      destino[prop] = fuente[prop];
    }
  }
  /**
   * Gets the rewards lsetSelectedRewardist
   */
  getRewards() {
    let sub = this.rewardService.getRewards().subscribe(
      (data: Reward[]) => {
        this.listaRewards = data;
      }, (err) => {
        this.handleError(err);
      }, () => {
        sub.unsubscribe();
      });
  }
 
//  TODO rutas para rewards
  gotoDetail(idReward: string) {
    this.router.navigate(['home/reward-detail', this.reward.idPremio]);
  }

  setSelectedReward(reward: Reward) {
    this.setValues(reward, this.reward);
  }


  private handleError(response: Response) {
    this.messageService.mostrarError(this.errorHandlerService.manejaErrorGrowl(response));
  }
 

}
