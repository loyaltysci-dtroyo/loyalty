import { MessagesService } from '../../service/messages.service';
import { ErrorHandlersService } from '../../service/error-handler.service';
import { RewardService } from '../../service/';
import { Component, OnInit, Input } from '@angular/core';
import { Reward } from '../../model/';
import { Response } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';

declare var $: any;
@Component({
    moduleId: module.id,
    selector: 'rewards-detail',
    templateUrl: 'rewards-detail.component.html',
    providers: [RewardService, Reward]
})
export class RewardsDetailComponent implements OnInit {
    public statusRedeem: any;

    @Input() reward: Reward;

    constructor(
        private router: Router,
        private rewardService: RewardService,
        private messageService: MessagesService,
        private errorHandlerService: ErrorHandlersService,
        private activatedroute: ActivatedRoute) {

    }
    /**
    * Redeems a reward
    */
    redeemReward() {
        let sub = this.rewardService.redeemReward(this.reward.idPremio).subscribe(
            (response: Response) => {
                // this.statusRedeem = { idCert: response.text(), status: response.status };
                this.statusRedeem = response.text();
                
                $('#modalStatus').modal('show');
            },
            (error: Response) => {
                this.handleError(error);
            },
            () => {
                 // show thw dialog so the user can see the result
                sub.unsubscribe();
            }
        );
    }
    /**
   * Gets the rewards lsetSelectedRewardist
   */
    ngOnInit() {
        this.activatedroute.params.subscribe((params) => {
            // console.log(param);
            if (params['id']) {
                this.findReward(params['id']);
            } else {
                this.router.navigate(['/home/rewards']);
            }
        });
    }

    findReward(idReward: string) {
        let sub = this.rewardService.getRewards().subscribe(
            (data: Reward[]) => {
                this.reward = data.find(element => element.idPremio === idReward);
                if (!this.reward) {
                    this.router.navigate(['/home/rewards']);
                }
            }, (err) => {
                this.handleError(err);
            }, () => {
                sub.unsubscribe();
            });
    }

    gotoRewards(){
        this.router.navigate(['/home/rewards']);
    }

    private handleError(response: Response) {
        this.messageService.mostrarError(this.errorHandlerService.manejaErrorGrowl(response));
    }
}
