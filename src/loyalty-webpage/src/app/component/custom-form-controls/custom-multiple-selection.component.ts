import { MisionEncuestaPregunta } from '../../model';
import { Component, OnInit, AfterViewInit, Input, forwardRef } from '@angular/core';
import {
    ControlValueAccessor,
    NG_VALUE_ACCESSOR,
    NG_VALIDATORS,
    FormControl,
    Validator, FormGroup
} from '@angular/forms';

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'multiple-selection',
    template: `
    <div *ngIf="question.indTipoRespuesta=='RU'">
        <div class="checkbox" *ngFor="let item of answers;let i=index;">
            <label>
                <input [id]="item" type="checkbox" (change)="onChange($event,i);clearChecks(i)">{{item}}
            </label>
        </div>
    </div>
    <div *ngIf="question.indTipoRespuesta=='RM'">
        <div class="checkbox" *ngFor="let item of answers;let i=index;" >
            <label>
                <input [id]="item" type="checkbox" (change)="onChange($event,i);">{{item}}
            </label>
        </div>
    </div>
    <div *ngIf="question.indTipoRespuesta=='RT'">
        <input [id]="item" class="form-control" [(ngModel)]="textValue"
         type="text" (keyup)="onChange($event)" (blur)="onChange($event)" (click)="onChange($event)">
    </div>
    <div *ngIf="question.indTipoRespuesta=='RN'">
        <input [id]="item" class="form-control" [(ngModel)]="numberValue"
         type="number"  (keyup)="onChange($event)" (blur)="onChange($event)" (click)="onChange($event)">
    </div>
    `,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => MultipleSelectionComponent),
            multi: true,
        }, {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => MultipleSelectionComponent),
            multi: true,
        }]
})
export class MultipleSelectionComponent implements OnInit, AfterViewInit, ControlValueAccessor, Validator {

    private isEmpty: boolean; // flags for validation
    private isValid: boolean; // flags for validation
    public values: boolean[] = []; // flag array representing the selected options (in case of single or multiple selection)
    public textValue: string; // value of the field (in case of text)
    public numberValue: number; // value of the field (in case of number)
    public answers: string[] = []; // question's answers (in case of single or multiple selection)
    @Input() question: MisionEncuestaPregunta; // the question itself
    private response: {
        idPregunta: string,
        respuesta: string,
        comentarios: string
    };//response for the question

    constructor() {
    }
    /**
     * Component's initializer
     */
    ngOnInit() {
        this.response = {
            idPregunta: this.question.idPregunta,
            respuesta: "",
            comentarios: ""
        };
    }
    ngAfterViewInit() {
        this.values = [];
        this.answers = [];
        this.textValue = '';
        this.numberValue = 0;
        if (this.question.indTipoRespuesta == 'RM' || this.question.indTipoRespuesta == 'RU') {
            this.question.respuestas.trim().split('\n').forEach((element, i) => {
                this.answers.push(element);
                this.values.push(false);
                // console.log(this.answers[i], this.values[i]);
            });
        }

    }
    clearChecks(i: number) {
        this.values.forEach((element, index) => {
            if (index != i) {
                this.values[i] = false;
            }
        });
    }

    /**
     * Registers a change in the control state
     */
    onChange($event, i?: number) {
        // console.log(this.numberValue, this.textValue);
        /**
         * 
            Selección múltiple - RM
            Selección única - RU
            Texto abierto - RT
            Numérico abierto - RN
         */
        switch (this.question.indTipoRespuesta) {
            case 'RM': {
                this.values[i] = !this.values[i];
                if (this.values.filter(element => element).length > 0) {
                    this.isValid = true;
                } else {
                    this.isValid = false;
                }

                break;
            }

            case 'RU': {
                this.values[i] = !this.values[i];
                if (this.values.filter(element => element).length === 1) {
                    this.isValid = true;
                } else {
                    this.isValid = false;
                }

                break;

            }

            case 'RT': {
                if (this.textValue !== '') {
                    this.isValid = true;
                } else {
                    this.isValid = false;
                }

                break;
            }

            case 'RN': {
                try {
                    if (parseFloat(this.numberValue + "")) {
                        this.isValid = true;
                    } else {
                        this.isValid = false;
                    }
                } catch (e) {
                    this.isValid = false;
                }
                break;
            }

            default: {
                //console.log('default');
                break;
            }
        }
        // console.log("change: ", this.isValid);
        this.propagateChange(this.encodeAnswers());
    }


    encodeAnswers() {
        switch (this.question.indTipoRespuesta) {
            case 'RM': {
                if (this.answers) {
                    this.response.respuesta = this.answers
                        .filter((element, index) => this.values[index])
                        .map((value) => { return this.answers.findIndex((ans) => { return value === ans; })+1})
                        .join('\n');
                } else {
                    this.response.respuesta = undefined;
                }
                break;
            }

            case 'RU': {
                if (this.answers) {
                    this.response.respuesta = this.answers
                        .filter((element, index) => this.values[index])
                        .map((value) => { return this.answers.findIndex((ans) => { return value === ans; })+1})
                        .join('\n');
                } else {
                    this.response.respuesta = undefined;
                }
                break;
            }

            case 'RT': {
                if (this.textValue) {
                    this.response.respuesta = this.textValue;
                } else {
                    this.response.respuesta = undefined;
                }
                break;
            }

            case 'RN': {
                if (this.numberValue) {
                    this.response.respuesta = this.numberValue + "";
                } else {
                    this.response.respuesta = undefined;
                }
                break;
            }

            default: {
                break;
            }
        }
        return this.response;

    }

    // returns null when valid, or else the validation object (error descriptor)
    public validate(c: FormControl) {
        // console.log("validate: ", this.isValid);
        return (this.isValid) ? null : {
            selectionError: {
                valid: false,
            },
        };
    }
    public writeValue(obj: any) {
        this.values = obj;
        //console.log('change value', obj);
    }
    public registerOnChange(fn: any) {
        this.propagateChange = fn;
    }
    // not used, used for touch input
    public registerOnTouched() {
    }

    // set in registerOnChange,this is a placeholder for a method that takes one parameter
    private propagateChange = (_: any) => { };

}