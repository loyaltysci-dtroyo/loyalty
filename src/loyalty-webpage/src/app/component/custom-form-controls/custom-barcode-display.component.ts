import { AppComponent } from '../../app.component';
import { Component, OnInit, Input } from '@angular/core';

declare var $: any;
declare var JsBarcode: any;
@Component({
    moduleId: module.id,
    selector: 'barcode',
    template: `<svg id="barcode">
</svg>`
})
export class CustomBarcodeDisplayComponent implements OnInit {
    @Input() value: string; // value to show
    @Input() format: string; // code's format
    // @Input() options: any;
    @Input() options: {
        font: string,
        height: number,
        displayValue: boolean,
        format: string,
        text: string,
        width: number,
        fontOptions: string,
        textAlign: string,
        textPosition: string,
        textMargin: number,
        fontSize: number,
        background: string,
        lineColor: string,
        margin: number,
        marginTop: number,
        marginBottom: number,
        marginLeft: number,
        marginRight: number,
        valid: Function
    };
    constructor() {
       
    }

    ngOnInit() {

         if (this.options && !this.options.font) {
            this.options.font = "monospace";
        }
        if (this.options && !this.options.height) {
            this.options.height = 100;
        }
        if (this.options && !this.options.displayValue) {
            this.options.displayValue = true;
        }
        if (this.options && !this.options.format) {
            this.options.format = "auto";
        }
        if (this.options && !this.options.text) {
            this.options.text = undefined;
        }
        if (this.options && !this.options.width) {
            this.options.width = 2;
        }
        if (this.options && !this.options.fontOptions) {
            this.options.fontOptions = "";
        }
        if (this.options && !this.options.textAlign) {
            this.options.textAlign = "center";
        }
        if (this.options && !this.options.textPosition) {
            this.options.textPosition = "bottom";
        }
        if (this.options && !this.options.textMargin) {
            this.options.textMargin = 2;
        }
        if (this.options && !this.options.fontSize) {
            this.options.fontSize = 20;
        }
        if (this.options && !this.options.background) {
            this.options.background = "#000000";
        }
        if (this.options && !this.options.lineColor) {
            this.options.lineColor = "#FFFFFF";
        }
        if (this.options && !this.options.margin) {
            this.options.margin = 10;
        }
        if (this.options && !this.options.marginTop) {
            this.options.marginTop = undefined;
        }
        if (this.options && !this.options.marginBottom) {
            this.options.marginBottom = undefined;
        }
        if (this.options && !this.options.marginLeft) {
            this.options.marginLeft = undefined;
        }
        if (this.options && !this.options.marginRight) {
            this.options.marginRight = undefined;
        }
        if (this.options && !this.options.valid) {
            this.options.valid = function (valid) { };
        }
        try {
            switch (this.format) {
                default: {
                    JsBarcode("#barcode")
                        .options(this.options) // Will affect all barcodes
                        .CODE128(this.value, { fontSize: 18, textMargin: 0 })
                        .render();
                }
            }
        } catch (e) {
            console.error(e);
        }
    }
}

/**
** List of default options **
* Option	        Default value	    Type
 format  	    "auto" (CODE128)	String
 width	        2	                Number
 height	        100	                Number
 displayValue	true	            Boolean
 text	        undefined	        String
 fontOptions 	""              	String
 font	        "monospace"	        String
 textAlign	    "center"	        String
 textPosition	"bottom"	        String
 textMargin  	2	                Number
 fontSize	    20	                Number
 background	    "#ffffff"	        String (CSS color)
 lineColor	    "#000000"	        String (CSS color)
 margin	        10	                Number
 marginTop	    undefined	        Number
 marginBottom	undefined	        Number
 marginLeft	    undefined	        Number
 marginRight	    undefined	        Number
 valid	        function(valid){}	Function
*/

