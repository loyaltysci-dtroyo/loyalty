import { MisionEncuestaPregunta } from '../../model';
import { Component, AfterViewInit, forwardRef, Input } from '@angular/core';
import {
    ControlValueAccessor,
    NG_VALUE_ACCESSOR,
    NG_VALIDATORS,
    FormControl,
    Validator
} from '@angular/forms';
declare var $: any;
@Component({
    moduleId: module.id,
    selector: 'star-rating',
    template: `<input id="input-id" type="text" class="rating" showcaption="false">`
    , providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => StarRatingComponent),
        multi: true,
    }, {
        provide: NG_VALIDATORS,
        useExisting: forwardRef(() => StarRatingComponent),
        multi: true,
    }]
})
export class StarRatingComponent implements AfterViewInit, ControlValueAccessor, Validator {
    public value: number;
    public data: number;
    public valid: boolean;
    @Input() question: MisionEncuestaPregunta;
    constructor() {
        this.valid = false;
        // console.log("const");
    }
    ngAfterViewInit() {
        $('#input-id').rating({
            step: 1, showCaption: false, size: 'xs'
        }).on("rating.change", (event, value) => { this.onChange(event, value) });

    }
    onChange(event, value) {
        // console.log(event,value);
        try {
            this.data = this.value = parseInt(value);
            this.valid = this.value >= 0 && this.value <= 5;
            if (this.question) {
                this.propagateChange({ respuesta: this.data, idPregunta: this.question.idPregunta });
            } else {
                this.propagateChange(this.data);
            }
        } catch (e) {
            this.valid = false;
        }
    }

    // set in registerOnChange,this is a placeholder for a method that takes one parameter
    private propagateChange = (_: any) => { };

    writeValue(obj: any): void {
        this.value = obj | 0;
        this.valid = false;
        // console.log(this.value);

    }
    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }
    registerOnTouched(fn: any): void {
        // nothing
    }
    setDisabledState(isDisabled: boolean): void {
        // nothing
    }

    // returns null when valid, or else the validation object (error descriptor)
    public validate(c: FormControl) {
        return (this.valid) ? null : {
            dateParseError: {
                valid: false,
            },
        };
    }
}
