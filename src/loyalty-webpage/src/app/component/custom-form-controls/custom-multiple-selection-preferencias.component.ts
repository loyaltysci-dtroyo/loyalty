import { MisionEncuestaPregunta, MisionPreferencia } from '../../model';
import { Component, OnInit, AfterViewInit, Input, forwardRef } from '@angular/core';
import {
    ControlValueAccessor,
    NG_VALUE_ACCESSOR,
    NG_VALIDATORS,
    FormControl,
    Validator, FormGroup
} from '@angular/forms';

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'multiple-selection-preference',
    template: `
    <div *ngIf="question.indTipoRespuesta=='SU'">
        <div class="radio" *ngFor="let item of answers;let i=index;">
            <label>
                <input [id]="item" type="radio" [(ngModel)]="selectedru"  [value]="i" (change)="onChange($event,i);"/>{{item}}
            </label>
        </div>
    </div>
    <div *ngIf="question.indTipoRespuesta=='RM'">
        <div class="checkbox" *ngFor="let item of answers;let i=index;" >
            <label>
                <input [id]="item" type="checkbox"  [checked]="values[i]" (change)="onChange($event,i);">{{item}}
            </label>
        </div>
    </div>
    <div *ngIf="question.indTipoRespuesta=='RT'">
        <input [id]="item" class="form-control" [(ngModel)]="textValue"
         type="text" (keyup)="onChange($event)" (blur)="onChange($event)" (click)="onChange($event)">
    </div>
    <div *ngIf="question.indTipoRespuesta=='RN'">
        <input [id]="item" class="form-control" [(ngModel)]="numberValue"
         type="number"  (keyup)="onChange($event)" (blur)="onChange($event)" (click)="onChange($event)">
    </div>
    `,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => MultipleSelectionPreferencesComponent),
            multi: true,
        }, {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => MultipleSelectionPreferencesComponent),
            multi: true,
        }]
})
export class MultipleSelectionPreferencesComponent implements OnInit, AfterViewInit, ControlValueAccessor, Validator {

    private isEmpty: boolean; // flags for validation
    private isValid: boolean; // flags for validation
    public selectedru: number;
    public values: boolean[] = []; // flag array representing the selected options (in case of single or multiple selection)
    public textValue: string; // value of the field (in case of text)
    public numberValue: number; // value of the field (in case of number)
    public answers: string[] = []; // question's answers (in case of single or multiple selection)
    @Input() question: MisionPreferencia; // the question itself
    private response: {
        idPreferencia: string,
        respuestaMiembro: string
    };//response for the question

    constructor() {
    }
    /**
     * Component's initializer
     */
    ngOnInit() {
        this.response = {
            idPreferencia: this.question.idPreferencia,
            respuestaMiembro: ""
        };
    }
    ngAfterViewInit() {
        this.values = [];

        this.answers = [];
        this.textValue = '';
        this.numberValue = 0;
        if (this.question.indTipoRespuesta == 'RM' || this.question.indTipoRespuesta == 'SU') {
            this.question.respuestas.trim().split('\n').forEach((element, i) => {
                this.answers.push(element);
                this.values.push(false);
                // 
            });
        }

    }
    clearChecks(i: number) {
        
        this.values.forEach((element, index) => {
            if (index != i) {
                
                this.values[index] = false;
            }
        });
        
    }

    /**
     * Registers a change in the control state
     */
    onChange($event, i?: number) {
        // 
        /**
         * 
            Selección múltiple - RM
            Selección única - SU
            Texto abierto - RT
            Numérico abierto - RN
         */
        switch (this.question.indTipoRespuesta) {
            case 'RM': {
                 this.values[i] = !this.values[i];
                if (this.values.filter(element => element).length > 0) {
                    this.isValid = true;
                } else {
                    this.isValid = false;
                }

                break;
            }

            case 'SU': {
                // this.values[i] = !this.values[i];
                
                if (this.selectedru>=0) {
                    this.isValid = true;
                } else {
                    this.isValid = false;
                }

                break;

            }

            case 'RT': {
                if (this.textValue !== '') {
                    this.isValid = true;
                } else {
                    this.isValid = false;
                }

                break;
            }

            case 'RN': {
                try {
                    if (parseFloat(this.numberValue + "")) {
                        this.isValid = true;
                    } else {
                        this.isValid = false;
                    }
                } catch (e) {
                    this.isValid = false;
                }
                break;
            }

            default: {
                //
                break;
            }
        }
        
        this.propagateChange(this.encodeAnswers());
    }


    encodeAnswers() {
        switch (this.question.indTipoRespuesta) {
            case 'RM': {
                if (this.answers) {
                    this.response.respuestaMiembro = this.answers
                        .filter((element, index) => this.values[index])
                        .map((value) => { return this.answers.findIndex((ans) => { return value === ans; })+1})
                        .join('\n');
                } else {
                    this.response.respuestaMiembro = undefined;
                }
                break;
            }

            case 'SU': {
                if (this.answers) {
                    this.response.respuestaMiembro = this.selectedru+1+'';
                } else {
                    this.response.respuestaMiembro = undefined;
                }
                break;
            }

            case 'RT': {
                if (this.textValue) {
                    this.response.respuestaMiembro = this.textValue;
                } else {
                    this.response.respuestaMiembro = undefined;
                }
                break;
            }

            case 'RN': {
                if (this.numberValue) {
                    this.response.respuestaMiembro = this.numberValue + "";
                } else {
                    this.response.respuestaMiembro = undefined;
                }
                break;
            }

            default: {
                break;
            }
        }
        return this.response.respuestaMiembro;

    }

    // returns null when valid, or else the validation object (error descriptor)
    public validate(c: FormControl) {
        // 
        return (this.isValid) ? null : {
            selectionError: {
                valid: false,
            },
        };
    }
    public writeValue(obj: any) {
        this.values = obj;
        //
    }
    public registerOnChange(fn: any) {
        this.propagateChange = fn;
    }
    // not used, used for touch input
    public registerOnTouched() {
    }

    // set in registerOnChange,this is a placeholder for a method that takes one parameter
    private propagateChange = (_: any) => { };

}