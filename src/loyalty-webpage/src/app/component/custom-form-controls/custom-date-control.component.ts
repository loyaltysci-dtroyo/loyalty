import { Component, AfterViewInit, forwardRef, Input } from '@angular/core';
import {
    ControlValueAccessor,
    NG_VALUE_ACCESSOR,
    NG_VALIDATORS,
    FormControl,
    Validator
} from '@angular/forms';
import 'moment/locale/pt-br';
import 'moment/locale/es';
declare var $: any;
@Component({
    moduleId: module.id,
    selector: 'datepicker',
    template: `<input id='datepicker-control' style='border: none;' type='text' [value]='value' 
        (change)='onChange($event)' (blur)='onChange($event)'>`,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DatePickerComponent),
            multi: true,
        }, {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => DatePickerComponent),
            multi: true,
        }]
})
export class DatePickerComponent implements AfterViewInit, ControlValueAccessor, Validator {
    public value: string; // value que tendra elemento html
    private formatError: boolean;
    private data: number; // unix timestamp,  valor que se reporta al api de forms y por ende al model
    @Input() locale: string;

    constructor() {
        this.data = new Date().getTime();
    }

    ngAfterViewInit() {
        $('#datepicker-control').datepicker({
            endDate: new Date(),
            orientation: 'bottom auto',
        });

    }

    // change events from the textarea
    onChange(event) {
        // get value from text area
        let newValue = event.target.value;
        try {
            this.data = new Date(newValue).getTime();
            this.formatError = false;
        } catch (ex) {
            this.data = new Date().getTime();
            this.formatError = true;
        }
        this.propagateChange(this.data);
    }
    // returns null when valid else the validation object
    // in this case we're checking if the json parsing has
    // passed or failed from the onChange method
    public validate(c: FormControl) {
        return (!this.formatError) ? null : {
            dateParseError: {
                valid: false,
            },
        };
    }

    // set in registerOnChange,this is a placeholder for a method that takes one parameter
    private propagateChange = (_: any) => { };

    // this is the initial value set to the component
    public writeValue(obj: any) {
        if (obj) {
            let modDate = new Date(parseInt(obj));

            this.data = modDate.getTime();//value para el model
            this.value = '' + modDate.toLocaleDateString();//value para el display html
        } else {
            let modDate = new Date();
            this.data = modDate.getTime();//value para el model
            this.value = '' + modDate.toLocaleDateString();
        }
    }
    // registers 'fn' that will be fired when changes are made
    // this is how we emit the changes back to the form
    public registerOnChange(fn: any) {
        this.propagateChange = fn;
    }
    // not used, used for touch input
    public registerOnTouched() { }

}
