import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
@Component({
    moduleId: module.id,
    selector: 'mission-status',
    templateUrl: 'mission-status.component.html'
})
export class MissionStatusComponent implements OnInit {
    public submissionStatus:any;
    @Input() status: any;
    @Input() set visible(visible: boolean) {
        if (visible) {
            this.showModal();
        } else {
            this.hideModal();
        }
    }
    /**
     * shows the modal with the transaction's info
     */
    showModal() {

    }
    /**
     * closes the modal with the transaction's info
     */
    hideModal() {

    }
    constructor() { }

    ngOnInit() { }
}