import { MessagesService } from '../../service/messages.service';
import { ErrorHandlersService } from '../../service/error-handler.service';
import { MisionService } from '../../service/mision.service';
import { Component, OnInit } from '@angular/core';
import { Mision } from '../../model/';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-mission',
  templateUrl: './mission.component.html',
  styleUrls: ['./mission.component.css'],
})
export class MissionComponent implements OnInit {
  public rowOffset: number;
  public rowsPerPage: number;
  public listaMisiones: Mision[];
  constructor(
    private misionService: MisionService,
    private errorHandlerService: ErrorHandlersService,
    private messageService: MessagesService,
    private router: Router,
    public mision: Mision
  ) {

  }

  ngOnInit() {
    this.getMisiones();
  }
  setValues(fuente: any, destino: any) {
    for (let prop in fuente) {
      destino[prop] = fuente[prop];
    }
  }

  getMisiones() {
    let sub = this.misionService.getMisiones().subscribe(
      (data: Mision[]) => {
        this.listaMisiones = data.filter((e) => e.indTipoMision != 'J');
      }, (err) => {
        this.handleError(err);
      }, () => {
        sub.unsubscribe();
      });
  }

  public handleError(response: Response) {
    this.messageService.mostrarError(this.errorHandlerService.manejaErrorGrowl(response));
  }

  gotoDetail(idMision: string) {
    this.router.navigate(['home/complete-mission']);
  }

  setSelectedMission(mision: Mision) {
    this.setValues(mision, this.mision);
  }

}
