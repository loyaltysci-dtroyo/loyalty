import { Component, OnInit } from '@angular/core';
import { Mision, MisionEncuestaPregunta } from '../../../model/';
import { MisionService, RewardService } from '../../../service/';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Response } from '@angular/http';

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'complete-mission-survey',
    templateUrl: 'complete-mission-survey.component.html',
    providers: [RewardService]
})

export class CompleteMissionSurveyComponent implements OnInit {
    public questionList: MisionEncuestaPregunta[];
    public form: FormGroup;
    public submitting: boolean;
    public response: {
        idPregunta: string,
        respuesta: string,
        comentarios: string
    }[] = []; // responses for all of the questions

    public submissionStatus: {
        indEstado: string,
        mensaje: string,
        indTipoPremio: string,
        referenciaPremio: string
    };
    constructor(
        private router: Router,
        public mision: Mision,
        private missionService: MisionService,
        private rewardsService: RewardService,

    ) {
        this.submitting = false;
        this.mision.misionEncuestaPreguntas.forEach((element) => {
            this.response.push({
                idPregunta: '',
                respuesta: '',
                comentarios: ''
            });
        });
    }

    ngOnInit() {
        // 
        this.form = this.toFormGroup(this.mision.misionEncuestaPreguntas);
        // this.form.valueChanges.subscribe(
        //     (element) => {
        //         
        //     }
        // );
    }

    /**
     * Generates the form group for the dynamic form
     * @param questions
     */
    toFormGroup(questions: MisionEncuestaPregunta[]) {
        let group: any = {};
        questions.forEach(question => {
            group[question.idPregunta] = new FormControl({}, Validators.required);
            if (question.indComentario) {
                group[question.idPregunta + '-comment'] = new FormControl('');
            }
        });
        // 
        return new FormGroup(group);
    }

    completeMission() {
        let tmp = {
            respuestaEncuesta: this.response,
            respuestaActualizarPerfil: undefined,
            respuestaSubirContenido: undefined,
            respuestaRedSocial: undefined,
            respuestaVerContenido: undefined
        };
        this.submitting = true;
        let sub = this.missionService.completeMission(this.mision.idMision, tmp).subscribe(
            (response: Response) => {
                this.submissionStatus = response.json();
                // 
                $('#modalStatus').modal('show'); // show thw dialog so the user can see the result
            },
            (error: Response) => {
                this.handleError(error);
            },
            () => {
                sub.unsubscribe();
                this.submitting = false;
            });

    }

    gotoMissions() {
        this.router.navigate(['/home/mission']);
    }

    handleError(error: Response) {
        $('#modalStatus').modal('show');
    }
}