import { Component, OnInit } from '@angular/core';
import { Mision, MisionPerfilAtributo } from '../../../model/';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ErrorHandlersService, MisionService } from '../../../service/';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
declare var $: any;
@Component({
    moduleId: module.id,
    selector: 'complete-mission-atributo',
    templateUrl: 'complete-mission-atributo.component.html'
})
export class CompleteMissionAtributoComponent implements OnInit {
    public request: {
        respuestaActualizarPerfil: {
            idAtributo: string,
            respuesta: string
        }[]
    };
    public responseMap: Map<string, any>;
    form: FormGroup;
    public submissionStatus: any;
    public submitting: boolean = false;
    constructor(public mision: Mision,
        private router: Router,
        private missionService: MisionService) {

    }
    ngOnInit() {
        this.responseMap = new Map<string, any>();
        this.form = this.toFormGroup(this.mision); 
    }

    /**
     * Sends a request to the API in order to complete a mission
     */
    completeMission() {
        let tmp: {
            idAtributo: string,
            respuesta: string
        }[] = [];

        for (let prop in this.form.value) {
            tmp.push({ idAtributo: prop, respuesta: this.form.value[prop] });
        }
        this.request = { respuestaActualizarPerfil: tmp }
        this.submitting = true;
        let sub = this.missionService.completeMission(this.mision.idMision, this.request).subscribe(
            (response: Response) => {
                this.submissionStatus = response.json();
                $('#modalStatus').modal('show'); // show thw dialog so the user can see the result
            },
            (error: Response) => {
                this.handleError(error);
                this.submitting = false;
            },
            () => {
                sub.unsubscribe();
                this.submitting = false;
            }
        );
    }

    /**
     * Generates a form group for the component's form
     * @param mision the mission to accomplish
     */
    toFormGroup(mision: Mision) {
        let group: any = {};
        if (mision.misionPerfilAtributos) {
            mision.misionPerfilAtributos.forEach((attr) => {
                this.responseMap.set(attr.idAtributo, undefined);
                if (attr.indRequerido) {
                    group[attr.idAtributo] = new FormControl(attr.respuesta, Validators.required);
                } else {
                    group[attr.idAtributo] = new FormControl(attr.respuesta);
                }

            });
        }
        return new FormGroup(group);
    }
    /**
     * Invoked after the mission is completed
     */
    gotoMissions() {
        this.router.navigate(['/home/mission']);
    }
    /**
     *  Handles the error, in this case it displays a modal with info
     * @param error
     */
    handleError(error: Response) {
        $('#modalStatus').modal('show');
    }
}
