import { Component, OnInit } from '@angular/core';
import { Mision, MisionPerfilAtributo } from '../../../model/';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ErrorHandlersService, MisionService } from '../../../service/';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
declare var $: any;

@Component({
    selector: 'complete-mision-preferencia',
    templateUrl: 'complete-mission-preferencia.component.html'
})

export class CompleteMisionPreferenciaComponent implements OnInit {
    public request: {
        respuestaPreferencias: {
            idPreferencia: string,
            respuesta: string
        }[]
    };
    public responseMap: Map<string, any>;
    form: FormGroup;
    public submissionStatus: {
        indEstado: string,
        mensaje: string,
        indTipoPremio: string,
        referenciaPremio: string
    };
    public submitting: boolean = false;
    constructor(public mision: Mision,
        private router: Router,
        private missionService: MisionService) {

    }
    ngOnInit() {
        this.responseMap = new Map<string, any>();
        this.form = this.toFormGroup(this.mision);
    }

    /**
     * Sends a request to the API in order to complete a mission
     */
    completeMission() {
        let tmp: {
            idPreferencia: string,
            respuesta: string
        }[] = [];

        for (const prop in this.form.value) {
            tmp.push({ idPreferencia: prop, respuesta: this.form.value[prop] });
        }
        this.request = { respuestaPreferencias: tmp }
        this.submitting = true;
        let sub = this.missionService.completeMission(this.mision.idMision, this.request).subscribe(
            (response: Response) => {
                this.submissionStatus = response.json();
                $('#modalStatus').modal('show'); // show thw dialog so the user can see the result
            },
            (error: Response) => {
                this.handleError(error);
                this.submitting = false;
            },
            () => {
                sub.unsubscribe();
                this.submitting = false;
            }
        );
    }

    /**
     * Generates a form group for the component's form
     * @param mision the mission to accomplish
     */
    toFormGroup(mision: Mision) {
        let group: any = {};
        if (mision.misionEncuestaPreferencias) {
            mision.misionEncuestaPreferencias.forEach((pref) => {
                this.responseMap.set(pref.idPreferencia, undefined);
                // if (pref.indRequerido) {
                group[pref.idPreferencia] = new FormControl(pref.respuestas, Validators.required);
                // } else {
                //     group[pref.idAtributo] = new FormControl(pref.respuesta);
                // }

            });
        }
        return new FormGroup(group);
    }
    /**
     * Invoked after the mission is completed
     */
    gotoMissions() {
        this.router.navigate(['/home/mission']);
    }
    /**
     *  Handles the error, in this case it displays a modal with info
     * @param error
     */
    handleError(error: Response) {
        $('#modalStatus').modal('show');
    }
}