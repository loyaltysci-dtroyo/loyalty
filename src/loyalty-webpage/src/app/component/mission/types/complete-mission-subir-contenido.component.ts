import { Component, OnInit } from '@angular/core';
import { Mision, MisionPerfilAtributo, Miembro } from '../../../model/';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ErrorHandlersService, MisionService, MessagesService } from '../../../service/';
import { Response } from '@angular/http';
import { Router } from '@angular/router';

declare var $: any;
const IMAGE = 'I';
const VIDEO = 'V';
const OTHER = 'O';

@Component({
    moduleId: module.id,
    selector: 'complete-mision-subir-contenido',
    templateUrl: 'complete-mission-subir-contenido.component.html'
})
export class CompleteMisionSubirContenidoComponent implements OnInit {

    // modelo de la respuesta de subir contenido, contiene string con el base 64
    public request: any;
    public inputFile: any;
    public submissionStatus: any;
    public submitting: boolean;
    public file: any;
    public file_src: any;
    public dataType: string;
    public isValid: boolean;

    constructor(
        public mision: Mision,
        private router: Router,
        private miembro: Miembro,
        private missionService: MisionService,
        private messageService: MessagesService
    ) {
        this.submitting = false;
        this.isValid = false;
    }

    ngOnInit() { }

    completeMission() {
        let tmp = {
            respuestaEncuesta: undefined,
            respuestaActualizarPerfil: undefined,
            respuestaSubirContenido: this.request,
            respuestaRedSocial: undefined,
            respuestaVerContenido: undefined
        };
        this.submitting = true;
        let sub = this.missionService.completeMission(this.mision.idMision, tmp).subscribe(
            (response: Response) => {
                this.submissionStatus = response.json();
                // 
                $('#modalStatus').modal('show'); // show thw dialog so the user can see the result
            },
            (error: Response) => {
                this.handleError(error);
            },
            () => {
                sub.unsubscribe();
                this.submitting = false;
            });
    }

    readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
        }
    }
    detectContentType(dataURL: string): boolean {
        if (dataURL) {
            let dataType = dataURL.split(';')[0];

            if (this.mision && this.mision.misionSubirContenido.indTipo == 'I') {
                return dataType.startsWith('data:image');
            } else if (this.mision && this.mision.misionSubirContenido.indTipo == 'V') {
                return dataType.startsWith('data:video');
            }
        } else {
            return false;
        }
    }

    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {
        this.file = input.files[0];
        var img = document.createElement('img');
        img.src = window.URL.createObjectURL(input.files[0]);
        let reader: any
        let target: EventTarget;
        reader = new FileReader(); // Crear un FileReader

        // Agregar un event listener para el cambio
        reader.addEventListener('load', (event) => {
            
            this.isValid = this.detectContentType(event.target.result);
            // 
            if (this.isValid) {
                this.request = { "datosContenido": event.target.result.split(',')[1] };
            } else {
                this.messageService.mostrarError({ body: "The data type is invalid", header: "Error" });
            }
            img.src = event.target.result; // Obtener el (base64 de la imagen)
            this.file_src = img.src; // Redimensionar la imagen
        }, false);
        reader.readAsDataURL(input.files[0]);
    }
    /**
      * Invoked after the mission is completed
      */
    gotoMissions() {
        this.router.navigate(['/home/mission']);
    }
    /**
     *  Handles the error, in this case it displays a modal with info
     * @param error
     */
    handleError(error: Response) {
       
    }


}