import { Component, OnInit } from '@angular/core';
import { Mision } from '../../../model/';
import { MisionService, ErrorHandlersService, MessagesService } from '../../../service';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
declare var YT: any;
declare var $: any;
@Component({
    moduleId: module.id,
    selector: 'complete-mission-contenido',
    templateUrl: 'complete-mission-ver-contenido.component.html'
})
export class CompleteMissionVerContenidoComponent implements OnInit {

    public done = false;
    public player: any;
    public videoURL: string;
    public submissionStatus: any;

    constructor(public mision: Mision,
        private MissionService: MisionService,
        private errorHandlerService: ErrorHandlersService,
        private messagesService: MessagesService,
        private missionService: MisionService,
        private router: Router) {
        console.log(this.mision);
    }

    ngOnInit() {
        if (this.mision.misionVerContenido) {
            switch (this.mision.misionVerContenido.indTipo) {
                case 'V': {
                    this.videoURL = this.mision.misionVerContenido.url;
                    this.player = new YT.Player('player', {
                        height: '360',
                        width: '640',
                        videoId: this.videoURL.split('?v=')[1],
                        events: {
                            'onReady': this.onPlayerReady,
                            'onStateChange': (event) => { if (event.data === YT.PlayerState.PLAYING) { window.setTimeout(() => {this.done = true; }, 15000); } }
                        }
                    });
                    break;
                }
                case 'U': {
                    break;
                }
                case 'I': {
                    window.setTimeout(() => { console.log("done"); this.done = true; }, 7000);
                    break;
                }
            }

        }
    }

    onPlayerReady(event) {
        event.target.playVideo();
    }
    stopVideo() {
        this.player.stopVideo();
    }

    onPlayerStateChange(event) {
        if (event.data === YT.PlayerState.PLAYING) {
            console.log(event);
            this.done = true;
            window.setTimeout(() => { console.log(this.done = true); this.done = true; }, 1000);
        }
    }

    completeMission() {
        let sub = this.missionService.completeMission(this.mision.idMision, undefined).subscribe(
            (response: Response) => {
                this.submissionStatus = response.json();
                $('#modalStatus').modal('show'); // show thw dialog so the user can see the result
            },
            (error: Response) => {
                this.handleError(error);
            },
            () => {
                sub.unsubscribe();
            });
    }


    gotoUrl(href) {
        var win = window.open(href, '_blank');
        // window.location.href = href;
        this.done = true;
    }

    gotoMissions() {
        this.router.navigate(['/home/mission']);
    }

    handleError(error: Response) {
        $('#modalStatus').modal('show');
    }
}