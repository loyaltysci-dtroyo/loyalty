import { Component, OnInit, OnDestroy } from '@angular/core';
import { Mision, Miembro, MisionRedSocial } from '../../../model/';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ErrorHandlersService, MisionService, MessagesService, FacebookService } from '../../../service/';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
declare var $: any;
@Component({
    moduleId: module.id,
    providers: [],
    selector: 'complete-mission-social',
    templateUrl: 'complete-mission-red-social.component.html'
})
export class CompleteMissionSocialComponent implements OnInit {
    public loading: boolean;
    public statusMision: any;
    public submissionStatus: {
        indEstado: string,
        mensaje: string,
        indTipoPremio: string,
        referenciaPremio: string
    };
    private submitting: boolean;
    constructor(
        public mision: Mision,
        private router: Router,
        private miembro: Miembro,
        private missionService: MisionService,
        private messageService: MessagesService,
        private fb: FacebookService) { }
    ngOnInit() {
        // console.log(this.mision);
        this.submissionStatus = { indEstado: "", indTipoPremio: "", mensaje: "", referenciaPremio: "" };
        FacebookService.updateButon();
        // this.mision.misionRedSocial.urlObjectivo = "http://google.com";
    }

    //realiza un share usando el api de FB
    share(url): Observable<boolean> {
        return FacebookService.share(url);
    }
    subscribeLikeEvent(): Observable<boolean> {
        return FacebookService.subscribeLike();
    }

    completeMission() {
        switch (this.mision.misionRedSocial.indTipo) {
            case MisionRedSocial.ENLACE: {
                this.share(this.mision.misionRedSocial.urlObjectivo).subscribe((result: boolean) => {
                    if (result) {
                        this.confirmCompletion();
                    } else {
                        console.log("miembro no realizo share");
                    }
                });
            }
                break;
            case MisionRedSocial.IMAGEN: {
                this.share(this.mision.misionRedSocial.urlObjectivo).subscribe((result: boolean) => {
                    if (result) {
                        this.confirmCompletion();
                    } else {
                        console.log("miembro no realizo share");
                    }
                });
            }
                break;
            case MisionRedSocial.ME_GUSTA: {
                this.subscribeLikeEvent().subscribe((result: boolean) => {
                    if (result) {
                        this.confirmCompletion();
                    } else {
                        console.log("miembro no realizo like");
                    }
                });
            }
                break;
            case MisionRedSocial.MENSAJE: {
                this.share(this.mision.misionRedSocial.urlObjectivo).subscribe((result: boolean) => {
                    if (result) {
                        this.confirmCompletion();
                    } else {
                        console.log("miembro no realizo share");
                    }
                });
            }
                break;
            default: {
                this.statusMision = 'error';
            }
        }
    }
    private confirmCompletion() {
        let sub = this.missionService.completeMission(this.mision.idMision, undefined).subscribe(
            (response: Response) => {
                // console.log(this.submissionStatus);
                this.submissionStatus = response.json();
                $('#modalStatus').modal('show'); // show thw dialog so the user can see the result
            },
            (error: Response) => {
                this.handleError(error);
            },
            () => {
                sub.unsubscribe();
                this.submitting = false;
            });
    }

    /**
      * Invoked after the mission is completed
      */
    gotoMissions() {
        this.router.navigate(['/home/mission']);
    }

    /**
    *  Handles the error, in this case it displays a modal with info
    * @param error
    */
    handleError(error: Response) {
        $('#modalStatus').modal('show');
    }
}
