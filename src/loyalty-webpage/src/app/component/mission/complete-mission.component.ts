import { MisionService } from '../../service/mision.service';
import { Component, OnInit } from '@angular/core';
import { Mision } from '../../model/index';
import { Response } from '@angular/http';
import { ActivatedRoute } from '@angular/router'
import {Router} from '@angular/router';
@Component({
    moduleId: module.id,
    selector: 'complete-mission',
    templateUrl: 'complete-mission.component.html'
})
export class CompleteMisssionComponent implements OnInit {
    public rowOffset: number;
    public rowsPerPage: number;
    public listaMisiones: Mision[];
    constructor(
         private router:Router,
        public mision: Mision,
        private misionService: MisionService,
        private activatedRoute: ActivatedRoute
    ) {
        this.listaMisiones = [];
    }
    ngOnInit() {
        if (this.mision.misionEncuestaPreguntas == undefined) {
            this.router.navigate(['/home/mission']);
        }
        
    }

    private manejaError(error: Response) {
        // return error;
    }
}