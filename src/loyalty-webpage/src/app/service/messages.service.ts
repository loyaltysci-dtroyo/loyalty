import { Injectable } from '@angular/core';
declare var $: any;
@Injectable()
export class MessagesService {

    public msgs: any[];
    public life: number;
    public sticky = "false";

    constructor() {
        this.msgs = [];
        this.life = 2000;
    }

    /**
     * Muestra un growl de info
     */
    mostrarInfo(obj:any) {
        $.notify({
            title: '<strong>' + obj.header + '</strong>',
            message: obj.body
        });
    }
    /**
     * Muestra un growl de error 
     */
    mostrarError(obj:any) {
        $.notify({
            title: '<strong>' + obj.header + '</strong>',
            message: obj.body
        }, {
                type: 'danger'
            });
    }

      /**
     * Muestra un growl de error 
     */
    mostrarErrorBody(obj:{code:number,message:string,lang:any}) {
        $.notify({
            title: '<strong>' + obj.code + '</strong>',
            message: obj.message
        }, {
                type: 'danger'
            });
    }


    /**
     * Muestra un growl de warning
     */
    mostrarWarning(obj:any) {
        $.notify({
            title:'<strong>' + obj.header + '</strong>',
            message: obj.body
        }, {
                type: 'warning'
            });
    }
    /**
     * Muestra un growl de success
     */
    mostrarSuccess(obj:any) {
        $.notify({
            title:'<strong>' + obj.header + '</strong>',
            message: obj.body
        }, {
                type: 'success'
            });
    }
    /**
        * Limpia los mensajes existentes
        */
    clearMessages() {
        this.msgs = []
    }
}