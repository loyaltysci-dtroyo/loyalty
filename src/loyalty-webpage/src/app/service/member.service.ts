import { MemberHistory, MemberBalanceProgress, MemberBalance } from '../model/index';
import { KeycloakService } from './index';
import { Miembro } from '../model/miembro';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { Router } from "@angular/router";
import { AppConfig } from "../app.config";
@Injectable()
export class MemberService {
    private baseURL: string;
    private headers: Headers;
    constructor(private http: Http,
        private router: Router, private kc: KeycloakService) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.set('Authorization', '');
        this.baseURL = AppConfig.API_ENDPOINT;
    }
    /**
     * Obtiene el perfil del miembro para un id de miembro determinado
     */
    public getMember(): Observable<Miembro> {
        return Observable.create((observer: Observer<Miembro>) => {
            this.kc.getToken().then((tkn) => {
                this.headers.set('Authorization', 'bearer ' + tkn);
                this.http.get(this.baseURL + '/perfil/', { headers: this.headers })
                    .map((response: Response) => { return <Miembro>response.json(); })
                    .subscribe(
                    (miembro: Miembro) => { observer.next(miembro); },
                    (err) => {
                        this.router.navigate(['/accerror']);
                        observer.error(this.handleError(err));
                    },
                    () => { observer.complete(); }
                    );
            }).catch(
                () => {
                    observer.error('ERR-KC');
                }
                );
        });
    }
    /**
    * Registers a new member
    */
    public registerMember(member: Miembro): Observable<Response> {
        return Observable.create((observer: Observer<Response>) => {
            this.http.post(this.baseURL + '/admin/registro', JSON.stringify(member), { headers: this.headers })
                .subscribe((data) => {
                    observer.next(data);
                    observer.complete();
                });
        });
    }
    /**
    * Gets the metric balance for the main metric
    */
    public getBalance(): Observable<MemberBalance> {
        return Observable.create((observer: Observer<MemberBalance>) => {
            this.kc.getToken().then((tkn) => {
                this.headers.set('Authorization', 'Bearer ' + tkn);
                this.http.get(this.baseURL + '/perfil/balance', { headers: this.headers })
                    .map((element) => { return <MemberBalance>element.json() })
                    .subscribe((data) => {
                        observer.next(data);
                        observer.complete();
                    });
            }).catch(
                () => {
                    observer.error('ERR-KC');
                    observer.complete();
                }
                );
        });
    }


    /**
       * Gets the order history, only the last 20 orders from within 3 months
       */
    public getHistory(): Observable<MemberHistory[]> {
        return Observable.create((observer: Observer<MemberHistory[]>) => {
            this.kc.getToken().then((tkn) => {
                this.headers.set('Authorization', 'Bearer ' + tkn);
                this.http.get(this.baseURL + '/perfil/balance/historial', { headers: this.headers })
                    .map((element) => { return <MemberHistory[]>element.json() })
                    .subscribe((data: MemberHistory[]) => {
                        observer.next(data);
                        observer.complete();
                    });
            }).catch(
                () => {
                    observer.error('ERR-KC');
                    observer.complete();
                }
                );
        });
    }


    /**
       * Gets the order history, only the last 20 orders from within 3 months
       */
    public getProgress(): Observable<MemberBalanceProgress> {
        return Observable.create((observer: Observer<MemberBalanceProgress>) => {
            this.kc.getToken().then((tkn) => {
                this.headers.set('Authorization', 'Bearer ' + tkn);
                this.http.get(this.baseURL + '/perfil/balance/progreso', { headers: this.headers })
                    .map((element) => { return <MemberBalanceProgress>element.json() })
                    .subscribe((data) => {
                        observer.next(data);
                        observer.complete();
                    });
            }).catch(
                () => {
                    observer.error('ERR-KC');
                    observer.complete();
                }
                );
        });
    }



    /**
       * Edits the member's profile
       */
    public editMember(member: Miembro): Observable<Response> {
        return Observable.create((observer: Observer<Response>) => {
            this.kc.getToken().then((tkn) => {
                this.headers.set('Authorization', 'Bearer ' + tkn);
                this.http.put(this.baseURL + '/perfil', JSON.stringify(member), { headers: this.headers })
                    .subscribe((data) => {
                        observer.next(data);
                        observer.complete();
                    });
            }).catch(
                () => {
                    observer.error('ERR-KC');
                    observer.complete();
                }
                );
        });
    }




    /**
     * Maneja error a nivel  de componente para request http
     */
    private handleError(error: Response | string): Observable<any> {
        return Observable.throw(error);
    }

}