import { KeycloakService } from './';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { AppConfig } from '../app.config';
import { Reward } from '../model/index';
@Injectable()
export class RewardService {
    private url: string;
    private headers: Headers;
    constructor(
        private http: Http,
        private kc: KeycloakService) {
        this.url = AppConfig.API_ENDPOINT + "/premio";
        this.headers = new Headers();
        this.headers.append('Authorization', 'null');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');

    }
    public getRewardsHistory(): Observable<Reward[]> {
        return Observable.create((observer: Observer<Reward[]>) => {
            this.kc.getToken().then(
                (tkn) => {
                    this.headers.set('Authorization', 'bearer ' + tkn);
                    this.http.get(this.url + '/redimidos',
                        { headers: this.headers }).map((element) => { return <Reward[]>element.json(); }).subscribe(
                        (response: Reward[]) => {
                            observer.next(response);
                        }, (err) => {
                            observer.error(err);
                            this.manejaError(err);
                        }, () => {
                            observer.complete();
                        }
                        );
                },
                (err) => { this.manejaError(err); }
            );
        });
    }
    public getRewards(): Observable<Reward[]> {
        return Observable.create((observer: Observer<Reward[]>) => {
            this.kc.getToken().then(
                (tkn) => {
                    this.headers.set('Authorization', 'bearer ' + tkn);
                    this.http.get(this.url ,
                        { headers: this.headers }).map((element) => { return <Reward[]>element.json(); }).subscribe(
                        (response: Reward[]) => {
                            observer.next(response);
                        }, (err) => {
                            observer.error(err);
                            this.manejaError(err);
                        }, () => {
                            observer.complete();
                        }
                        );
                },
                (err) => { this.manejaError(err); }
            );
        });
    }
    /**
     * Redeems a reward through the rest api
     * PUT /premio/{idPremio}/redimir
     * @param reward - string containing the reward's id
     */
    public redeemReward(idReward: string): Observable<Response> {
        return Observable.create((observer: Observer<Response>) => {
            this.kc.getToken().then(
                (tkn) => {
                    this.headers.set('Authorization', 'bearer ' + tkn);
                    this.http.put(this.url + '/' + idReward + '/redimir','', { headers: this.headers }).subscribe(
                        (response: Response) => {
                            observer.next(response);
                        }, (err:Response) => {
                            observer.error(err);
                            this.manejaError(err);
                        }, () => {
                            observer.complete();
                        }
                    );
                },
                (err) => { this.manejaError(err); }
            );
        });
    }
    private manejaError(error: Response) {
        console.log(error);
        // return error;
    }
}