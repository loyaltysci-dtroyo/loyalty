import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs/Rx';
declare var window: any;
declare var FB: any;
@Injectable()
export class FacebookService {
    public static userInfo;
    public static button;
    /**
     * Inicializa el servicio de Facebook
     */
    public static init() {
        window.fbAsyncInit = function () {
            FB.init({
                appId: '116294165644042',
                status: true,
                cookie: true,
                xfbml: true,
                oauth: true,
                version: 'v2.9'
            });
        };
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
            console.log("fbinit")
        }(document, 'script', 'facebook-jssdk'));

    }
    /**
     * actualiza el contenido del boton de login/logout 
     * @param response respuesta del api de facebook al solicitar el status de sesion
     */

    private static updateButton(response) {
        FacebookService.button = document.getElementById('fb-auth');
        FacebookService.userInfo = document.getElementById('user-info');
        FB.Event.subscribe('auth.statusChange', FacebookService.updateButton);
        if (response.authResponse) {
            console.log("if")
            // user is already logged in and connected
            FB.api('/me', function (info) {
                FacebookService.login(response, info);
            });
            FacebookService.button.onclick = function () {
                FB.logout(function (response) {
                    FacebookService.logout(response);
                });
            };
        } else {
            console.log("else")
            FacebookService.showLoader(false);
            // user is not connected to your app or logged out
            FacebookService.button.innerHTML = '<i class="fa fa-facebook-official" aria-hidden="true"></i> Login';
            FacebookService.button.onclick = function () {
                FacebookService.showLoader(true);
                FB.login(function (response) {
                    if (response.authResponse) {
                        FB.api('/me', function (info) {
                            FacebookService.login(response, info);
                        });
                    } else {
                        // user cancelled login or did not grant authorization
                        FacebookService.showLoader(false);
                    }
                }, { scope: 'publish_actions,email,user_birthday,user_about_me' });
            };
        } 
    }
    /**
     * provoca la actualizacion del contenido del boton
     */
    public static updateButon(){
        FB.getLoginStatus(FacebookService.updateButton);
    }

    /**
     * Inicia sesion en el app usando facebook
     */
    public static login(response, info) {
        var accessToken = response.authResponse.accessToken;
        FacebookService.userInfo.innerHTML = `
            <img id="profile-img" class="profile-img-card" src="https://graph.facebook.com/` + info.id + `/picture?type=large">
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin">
                <!-- <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus> -->
                <label> Logged in as:</label>
                <div class="row col-xs-12" style="text-align: center;padding-bottom: 13px;text-transform: inherit;font-family: "Roboto Slab", "Helvetica Neue", Helvetica, Arial, sans-serif;">`+ info.name + `</div>
                <!-- <input type="password" id="inputPassword" class="form-control" placeholder="Password" required> -->
            </form>`;
        FacebookService.button.innerHTML = '<i class="fa fa-sign-out" aria-hidden="true"></i> Logout';
        FacebookService.showLoader(false);
        document.getElementById('other').style.display = "block";

    }
    /**Cierra sesión de la app */
    public static logout(response) {
        FacebookService.userInfo.innerHTML = `<div style="text-align:center; margin:25 px;" class="row col-lg-12">
        <p i18n >You need to log in with facebook in order to complete this challenge</p>
</div>`;
        document.getElementById('debug').innerHTML = "";
        document.getElementById('other').style.display = "none";
        FacebookService.showLoader(false);
        FacebookService.updateButon();
    }
    public static loginStatus() {
        return FB.getLoginStatus();

    }
    public static showLoader(status) {
        if (status) {
            document.getElementById('loader').style.display = 'block';
        } else {
            document.getElementById('loader').style.display = 'none';
        }
    }
    /**
     * Realiza una publicacion en Facebook a nombre del usuario en sesion
      * @param hrefLink el link a incluir en la publicacion
     */
    public static post(hrefLink: string): Observable<boolean> {
        return new Observable((observer: Observer<boolean>) => {
            FacebookService.showLoader(true);
            let post = {
                method: 'feed',
                link: hrefLink,
                display: 'popup'
            };
            FB.ui(
                post,
                function (response) {
                    if (response && response.post_id) {
                        observer.next(true);
                    } else {
                        observer.next(false);
                    }
                    observer.complete();
                    FacebookService.showLoader(false);
                }
            );
        });
    }
    /**
     * Realiza un compartir en Facebook a nombre del usuario en sesion
     */
    public static share(href: string, msg?: string): Observable<boolean> {
        return new Observable((observer: Observer<boolean>) => {
            FacebookService.showLoader(true);
            let share = {
                method: 'share',
                href: href,
                display: 'popup'
            };
            FB.ui(share, function (response) {
                if (response && response.post_id) {
                    observer.next(true);
                } else {
                    observer.next(false);
                }
                observer.complete();
                FacebookService.showLoader(false);
            });
        });
    }

    public static subscribeLike(): Observable<boolean> {
        return new Observable((observer: Observer<boolean>) => {
            FB.Event.subscribe('edge.create', function (href, widget) {
                observer.next(true);
            });
        });
    }
}