import { Miembro } from '../model/miembro';
import { MemberService } from './member.service';
//  import { MessagesService } from '../../utils/index';
import { KeycloakService } from '../service/index';
import { Injectable, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, UrlSegment, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, Observer } from 'rxjs/Rx';

@Injectable()
/**
* Flecha Roja Technologies oct-2016
* Fernando Aguilar
* Encargada de realizar operaciones de autorización
* Dependencias: KeycloakService
*/
export class AuthGuard implements CanActivate {

    constructor(protected router: Router,
        private miembro: Miembro,
        private memberService: MemberService,
        public keycloakService: KeycloakService) {

    }

    /**
     * Metodo que determina si un usario puede activar una ruta determiada
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        if(KeycloakService.auth.accError){
            this.router.navigate(['/accerror']);
            return false;
        }
        return Observable.create((observer: Observer<boolean>) => {
            let ruta: UrlSegment[][] = route.pathFromRoot
                .filter((element) => { return element.url.length > 0; })
                .map((element) => { return element.url; });
            if (ruta[0][0].path === 'home' && KeycloakService.auth.loggedIn) {
                observer.next(true);
            } else {
                KeycloakService.login().then((status) => {
                    // this.router.navigate(['/home']);
                }).catch(()=>{
                     this.router.navigate(['/accerror']);
                });
                observer.next(false);
            }
            observer.complete();
        });
    }
}