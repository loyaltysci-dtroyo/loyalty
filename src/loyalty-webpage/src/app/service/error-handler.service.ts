import { environment } from '../../environments/environment.prod';
import { Response } from '@angular/http';
import { Injectable, ErrorHandler } from '@angular/core';
@Injectable()
/**
 * Flecha Roja Technologies 30-set-2016
 * Fernando Aguilar
 * Service encargado de retornar mensajes de error a partir de 
 * responses con errores (el campo de error en los observables)
 */
export class ErrorHandlersService implements ErrorHandler {

    handleError(error) {
        // do something with the exception
        // console.log('Error:');
        console.log(error);
        if (error ) {
        console.log(error.message);
        }
        if (error && error.originalStack) {
        console.log(error.originalStack);
        }
        // return
    }
    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un objeto que proporciona información sobre la transaccion
   */
    manejaErrorGrowl(error: Response) {
        try {
            if (error instanceof ProgressEvent) {
                return { header: 'Error de conexión', body: 'Verifica tu conectividad a la red e intentalo de nuevo'};
            }else{
                let msgJSON=error.json();
                return { header: "Error", body: msgJSON.message};
            }
        } catch (e) {
            console.log(e);
        }
    }

    constructor() {

    }
}