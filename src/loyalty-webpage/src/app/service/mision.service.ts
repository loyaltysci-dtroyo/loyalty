import { KeycloakService } from './';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { AppConfig } from '../app.config';
import { Mision } from '../model/index';
@Injectable()
export class MisionService {
    private url: string;
    private headers: Headers;
    constructor(
        private http: Http,
        private kc: KeycloakService) {
        this.url = AppConfig.API_ENDPOINT + '/mision';
        this.headers = new Headers();
        this.headers.append('Authorization', 'null');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');

    }
    public getMisiones(): Observable<Mision[]> {
        return Observable.create((observer: Observer<Mision[]>) => {
            this.kc.getToken().then(
                (tkn) => {
                    this.headers.set('Authorization', 'bearer ' + tkn);
                    this.http.get(this.url, { headers: this.headers })
                        .map((element) => { return <Mision[]>element.json(); }).subscribe(
                        (response: Mision[]) => {
                            observer.next(response);
                        }, (err) => {
                            observer.error(err);
                            this.manejaError(err);
                        }, () => {
                            observer.complete();
                        }
                        );
                },
                (err) => { this.manejaError(err); }
            );
        });
    }
    public completeMission(missionId: string, response: any): Observable<Response> {
        return Observable.create((observer: Observer<Response>) => {
            this.kc.getToken().then(
                (tkn) => {
                    this.headers.set('Authorization', 'bearer ' + tkn);
                    this.http.post(this.url + '/' + missionId + '/completar', JSON.stringify(response), { headers: this.headers })
                       .subscribe(
                        (resp: Response) => {
                            observer.next(resp);
                        }, (err) => {
                            observer.error(err);
                            this.manejaError(err);
                        }, () => {
                            observer.complete();
                        }
                        );
                },
                (err) => { this.manejaError(err); }
            );
        });
    }


    private manejaError(error: Response) {
        console.log(error);
        // return error;
    }
}