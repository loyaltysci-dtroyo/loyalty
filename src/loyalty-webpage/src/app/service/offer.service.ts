import { KeycloakService } from './';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { AppConfig } from '../app.config';
import { Offer } from '../model/index';
@Injectable()
export class OfferService {
    private url: string;
    private headers: Headers;
    constructor(
        private http: Http,
        private kc: KeycloakService) {
        this.url = AppConfig.API_ENDPOINT + "/promocion";
        this.headers = new Headers();
        this.headers.append('Authorization', 'null');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');

    }
    public getOffers(): Observable<Offer[]> {
        return Observable.create((observer: Observer<Offer[]>) => {
            this.kc.getToken().then(
                (tkn) => {
                    this.headers.set('Authorization', 'bearer '+ tkn);
                    this.http.get(this.url, { headers: this.headers })
                    .map((element) => { return <Offer[]>element.json();}).subscribe(
                        (response: Offer[]) => {
                            observer.next(response);
                        }, (err) => {
                            observer.error(err);
                            this.manejaError(err);
                        }, () => {
                            observer.complete();
                        }
                    );
                },
                (err) => { this.manejaError(err); }
            );
        });
    }

    private manejaError(error: Response) {
        console.log(error);
        // return error;
    }
}