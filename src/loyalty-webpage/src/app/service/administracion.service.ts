import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs/Rx';
import { KeycloakService } from './keycloak.service';
import { Http, Headers } from '@angular/http';
import {Response} from '@angular/http';
import { AppConfig } from '../app.config';
/**
 * Clase encargada de realizar labores de administración de cuentas, como reset de contrasena
 */
@Injectable()
export class AdministracionService {
    private url: string;
    private headers: Headers;
    constructor(
        private http: Http,
        private kc: KeycloakService) {
        this.url = AppConfig.API_ENDPOINT + "/admin";
        this.headers = new Headers();
        this.headers.append('Authorization', 'null');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');

    }
    /**
     * Envia una peticion de cambio de contrasena al api
     * POST /admin/recuperar-contrasena
     */
    public requestResetPassword(request:any): Observable<Response> {
        return Observable.create((observer: Observer<Response>) => {
            this.kc.getToken().then(
                (tkn) => {
                    console.log(request);
                    this.headers.set('Authorization', 'bearer ' + tkn);
                    this.http.post(this.url+ '/recuperar-contrasena',JSON.stringify(request), { headers: this.headers })
                    .subscribe(
                        (response: Response) => {
                            observer.next(response);
                        }, (err) => {
                            observer.error(err);
                            this.manejaError(err);
                        }, () => {
                            observer.complete();
                        }
                    );
                },
                (err) => { this.manejaError(err); }
            );  
        });
    }

    /**
     * Cambio de la contraseña por peticion de recuperacion de contraseña
     * POST /admin/recuperar-contrasena/cambio-contrasena
    */
    public resetPassword(request:any): Observable<Response> {
        return Observable.create((observer: Observer<Response>) => {
            this.kc.getToken().then(
                (tkn) => {
                    this.headers.set('Authorization', 'bearer ' + tkn);
                    this.http.post(this.url+'/recuperar-contrasena/cambio-contrasena',JSON.stringify(request),
                     { headers: this.headers }).subscribe(
                        (response: Response) => {
                            observer.next(response);
                        }, (err) => {
                            observer.error(err);
                            this.manejaError(err);
                        }, () => {
                            observer.complete();
                        }
                    );
                },
                (err) => { this.manejaError(err); }
            );
        });
    }
    /**
     * Verificacion de peticion de recuperacion de contraseña
     * GET /admin/recuperar-contrasena/verificar-peticion
     */

    public verifiyResetStatus(): Observable<any[]> {
        return Observable.create((observer: Observer<any[]>) => {
            this.kc.getToken().then(
                (tkn) => {
                    this.headers.set('Authorization', 'bearer ' + tkn);
                    this.http.get(this.url+'/admin/recuperar-contrasena/verificar-peticion', { headers: this.headers }).map((element) => { return element.json(); }).subscribe(
                        (response: any[]) => {
                            observer.next(response);
                        }, (err) => {
                            observer.error(err);
                            this.manejaError(err);
                        }, () => {
                            observer.complete();
                        }
                    );
                },
                (err) => { this.manejaError(err); }
            );
        });
    }


    /**
     * Registro de un nuevo miembro al sistema
     * POST /admin/registro
     */
    public registerAccount(): Observable<any[]> {
        return Observable.create((observer: Observer<any[]>) => {
            this.kc.getToken().then(
                (tkn) => {
                    this.headers.set('Authorization', 'bearer ' + tkn);
                    this.http.get(this.url+'/registro', { headers: this.headers }).map((element) => { return element.json(); }).subscribe(
                        (response: any[]) => {
                            observer.next(response);
                        }, (err) => {
                            observer.error(err);
                            this.manejaError(err);
                        }, () => {
                            observer.complete();
                        }
                    );
                },
                (err) => { this.manejaError(err); }
            );
        });
    }

    private manejaError(error: Response) {
        console.log(error);
        // return error;
    }

}