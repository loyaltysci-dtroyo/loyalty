import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Router} from '@angular/router';
import {AppConfig} from "../app.config";
declare var Keycloak: any;
// declare var KeycloakAuthorization: any;

@Injectable()
/**
* Flecha Roja Technologies 
* Fernando Aguilar
* Servicio encargado de administrar la autenticacion y autorizacion con el identity provider 
*/
export class KeycloakService {

    static auth: any = {};
    // static authz: any = {};
    // static clientID = 'loyalty-api';
    // static rpt: string;
    /** 
     * Metodo que inicializa el runtime de keycloak, es llamado desde antes de hacer el boostraping de la aplicacion
     * y no debería ser llamado en ningun otro lado de la aplicacion ya que para eso se expone este servicio desde el 
     * app module
    */
    static init(): Promise<any> {
        let keycloakAuth = new Keycloak(AppConfig.KC_CONF);
        KeycloakService.auth.loggedIn = false;
        KeycloakService.auth.accError = false;
        return new Promise((resolve, reject) => {
            keycloakAuth.init({ onLoad: 'check-sso' })
                .success(() => {
                    KeycloakService.auth.isInitialized = true;
                    KeycloakService.auth.auth = keycloakAuth;
                    // KeycloakService.authz = new KeycloakAuthorization(keycloakAuth);
                    KeycloakService.auth.loggedIn = false;
                    KeycloakService.auth.auth.updateToken(5).success(function (refreshed: any) {
                        // KeycloakService.authz.authorize();
                        // KeycloakService.authz.entitlement(KeycloakService.clientID).then((rpt: any) => KeycloakService.rpt = rpt);
                        KeycloakService.auth.loggedIn = true;
                        resolve('OK');
                    }).error(function () {
                        KeycloakService.auth.loggedIn = false;
                        resolve('NOAUTH');
                    });
                })
                .error(() => {
                    KeycloakService.auth.loggedIn = false;
                    KeycloakService.auth.isInitialized = true;
                    reject('ERR');
                });
        });
    }

    /** 
     * Metodo que inicializa el runtime de keycloak, es llamado desde antes de hacer el boostraping de la aplicacion
     * y no debería ser llamado en ningun otro lado de la aplicacion ya que para eso se expone este servicio desde el 
     * app module
    */
    static login(): Promise<any> {
        let keycloakAuth = new Keycloak(AppConfig.KC_CONF);
        KeycloakService.auth.loggedIn = false;
        KeycloakService.auth.accError = false;
        return new Promise((resolve, reject) => {
            keycloakAuth.init({ onLoad: 'login-required' })
                .success(() => {
                    KeycloakService.auth.isInitialized = true;
                    KeycloakService.auth.loggedIn = true;
                    KeycloakService.auth.auth = keycloakAuth;
                    // KeycloakService.authz = new KeycloakAuthorization(keycloakAuth);
                    KeycloakService.auth.auth.updateToken(5).success(function (refreshed: any) {
                        // KeycloakService.authz.authorize();
                        // KeycloakService.authz.entitlement(KeycloakService.clientID).then((rpt: any) => KeycloakService.rpt = rpt);
                        resolve('OK');
                    }).error(function () {
                        reject('ERR');
                    });
                })
                .error(() => {
                    KeycloakService.auth.isInitialized = true;
                    reject('ERR');
                });
        });
    }


    constructor(private http: Http ) {
    }


    /**Metodo encargado de cerrar la sesion de un usuario */
    logout(): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            // KeycloakService.authz = null;
           KeycloakService.auth.auth.logout({redirectUri: window.location.protocol+'//'+window.location.hostname+':'+window.location.port});
                // this.http.get(KeycloakService.auth.auth.createLogoutUrl()).subscribe();
                KeycloakService.auth.auth = null;
                KeycloakService.auth.loggedIn = false;
        });


    }
    /**Metodo encargado de crear un URL para redireccionar al usuario 
     *  a la pagina de amdinistracion de su cuneta enkeycloak donde puede 
     * cambiar su clave entre otros parametros */
    accountMAnagement() {
        return KeycloakService.auth.auth.createAccountUrl();
    }

    /** 
    * Metodo encagado de generar un access token para el usuario logueado en ese momento 
    *El token de acceso tiene una vida util finita, por lo tanto necefsitamos refrescar el 
    *token de acceso si este esta expirado antes de enviar requests al API, de locontrario el API
    *va a rechazar todo request
    */
    getToken(): Promise<string> {
        return new Promise<string>((resolver, rechazar) => {
            if (KeycloakService.auth.auth.isTokenExpired(5)) {
                KeycloakService.auth.auth.updateToken(5)
                    .success(
                    (refreshed: any) => {
                        resolver(<string>KeycloakService.auth.auth.token);
                    })
                    .error(
                    () => {
                        rechazar('Error al refrescar el token');
                    });
            } else {
                //no es necesario renovar token
                resolver(<string>KeycloakService.auth.auth.token);
            }
        });
    }
    /** 
    * Metodo encagado de generar un access token para el usuario logueado en ese momento 
    *El token de acceso tiene una vida util finita, por lo tanto necefsitamos refrescar el 
    *token de acceso si este esta expirado antes de enviar requests al API, de locontrario el API
    *va a rechazar todo request
    */
    getRefreshToken(): Promise<string> {
        return new Promise<string>((resolver, rechazar) => {
            if (KeycloakService.auth.auth.isTokenExpired(5)) {
                KeycloakService.auth.auth.updateToken(5)
                    .success(
                    (refreshed: any) => {
                        resolver(<string>KeycloakService.auth.auth.refreshToken);
                    })
                    .error(
                    () => {
                        rechazar('Error al refrescar el token');
                    });
            } else {
                //no es necesario renovar token
                resolver(<string>KeycloakService.auth.auth.token);
            }
        });
    }
    /*
    *Metodo que se encarga de obtener el id del usuario que se encuentra actualmente en sesion 
    */
    getUsuario() {
        return new Promise<string>((resolve, reject) => {
            KeycloakService.auth.auth.updateToken(5)
                .success(
                () => {
                    resolve(<string>KeycloakService.auth.auth.subject)
                }
                )
                .error(
                () => reject('Failed ')
                );
        });

    }
    getUserProfile() {
        KeycloakService.auth.loadUserProfile();
    }
}