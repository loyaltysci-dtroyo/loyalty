import { Injectable } from '@angular/core';

@Injectable()
export class MisionVerContenido {
    indTipo: string;
    url: string;
    texto: string;
    constructor() { }
}