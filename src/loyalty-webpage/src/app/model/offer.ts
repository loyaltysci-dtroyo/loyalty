import { Injectable } from '@angular/core';

@Injectable()
export class Offer {
    idPromocion: string;
    imagenArte: string;
    encabezadoArte: string;
    subencabezadoArte: string;
    detalleArte: string;
    indTipoAccion: string;
    urlOrCodigo: string;
    marcada: boolean;
    constructor() { }
}