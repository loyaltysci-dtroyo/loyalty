import { Injectable } from '@angular/core';

@Injectable()
export class MemberHistory {
    fecha: Date;
    cantidadGanada: number;
    indFormaGanada: string;
    nombreMision: string;
    transaccion: {
        totalComprado: number;
        lugarCompra: string;
    };
    constructor() { }
}