import { Injectable } from '@angular/core';

@Injectable()
export class MisionEncuestaPregunta {
        idPregunta: string;
        indTipoPregunta: string;
        pregunta: string;
        respuestas: string;
        indTipoRespuesta: string;
        imagen: string;
        indComentario: boolean;
    constructor() { }
}