import { AtributoDinamico } from './atributo-dinamico';
import { Injectable } from '@angular/core';

@Injectable()
export class MisionPerfilAtributo {
    idAtributo: string;
    indRequerido: false;
    respuesta:string;
    indAtributo: string;
    atributoDinamico: AtributoDinamico;
    constructor() { }
}