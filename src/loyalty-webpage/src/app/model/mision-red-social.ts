import { Injectable } from '@angular/core';

@Injectable()
export class MisionRedSocial {
    public static get ENLACE() { return 'L' };
    public static get MENSAJE() { return 'M' };
    public static get ME_GUSTA() { return 'G' };
    public static get IMAGEN() { return 'I' };

    indTipo: string;
    mensaje: string;
    tituloUrl: string;
    urlObjectivo: string;
    urlImagen: string;
    constructor() { }
}