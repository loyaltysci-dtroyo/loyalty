import { Injectable } from '@angular/core';

@Injectable()
export class MemberBalanceProgress {
    progreso: number;
    porcentajeCompletado: number;
    metrica: {
        idMetrica: string;
        nombre: string;
        medida: string
    };
    niveles: [
        {
            idNivel: string;
            nombre: string;
            descripcion: string;
            metricaInicial: number;
        }
    ]
    constructor(){
        this.progreso=this.porcentajeCompletado=0;
    }
}