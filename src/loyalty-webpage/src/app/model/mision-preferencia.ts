import { Injectable } from '@angular/core';
@Injectable()
export class MisionPreferencia {

    public idPreferencia: string;
    public indTipoRespuesta: string;
    public pregunta: string;
    public respuestaMiembro: string;
    public respuestas: string;
    constructor() { }
}