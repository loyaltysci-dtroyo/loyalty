import { Injectable } from '@angular/core';

@Injectable()
export class Miembro {
    idMiembro: string;//auto gen Min. Length:1Max. Length:100,
    //MAIN
    avatar: string; // (string, optional)stringMax. Length:300,
    nombre: string; // (string)stringMin. Length:1Max. Length:30,
    apellido: string; // apellido (string)stringMin. Length:1Max. Length:100,
    apellido2: string; // (string, optional)stringMax. Length:100
    correo: string; // correo (string, optional)
    nombreUsuario: string; // (string, optional)
    direccion: string; // Max. Length:200
    codigoPostal: string; // Max. Length:10
    contrasena: string; // 8-20 characters long, letters and numbers, not spaces

   //PREFS
    indGenero: string; // (string, optional),
    indContactoEmail: boolean; // (boolean, optional)booleanDefault: false,
    indContactoSms: boolean; // (boolean, optional)booleanDefault: false,
    indContactoNotificacion: boolean; // (boolean, optional)

   // ADDITIONAL
    docIdentificacion: string; // Min. Length:1 Max. Length:100,
    fechaNacimiento: Date; // (string, optional)
    telefonoMovil: string; // (string, optional)stringMax. Length: 15,
    indContactoEstado: boolean; // (boolean, optional)booleanDefault: false,
    indEstadoCivil: string; // (string, optional),
    indEducacion: string; // (string, optional),
    ingresoEconomico: number; // (integer, optional),
    indHijos: boolean; // (boolean, optional)booleanDefault: false,

    ciudadResidencia: string; // (string, optional)stringMax. Length:100,
    estadoResidencia: string; // (string, optional)stringMax. Length:100,
    paisResidencia: string; // (string, optional)stringMax. Length:3,

    constructor() {
        this.avatar = './assets/img/user.png';
        this.fechaNacimiento = new Date();
    }
}