import { Injectable } from '@angular/core';

@Injectable()
export class AtributoDinamico {
    idAtributo: string;
    nombre: string;
    descripcion: string;
    indTipoDato: string;
    indRequerido: false;
    valorAtributoMiembro: string
    constructor() { }
}