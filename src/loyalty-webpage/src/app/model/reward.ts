import { Injectable } from '@angular/core';

@Injectable()
export class Reward {
    idPremio: string;
    nombre: string;
    indTipoPremio: string;
    indEnvio: boolean;
    descripcion: string;
    valorMoneda: number;
    valorEfectivo: number;
    imagenArte: string;
    trackingCode: string;
    sku: string;
    encabezadoArte: string;
    subencabezadoArte: string;
    detalleArte: string;
    cantMinAcumulado: number;
    codigoCertificado: string;
    fechaRedencion: Date;
    indEstado: string;
    idMetrica: {
      idMetrica: string;
      nombre: string;
      medida: string
    };
    constructor() { }
}