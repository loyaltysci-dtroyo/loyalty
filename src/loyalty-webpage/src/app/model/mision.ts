import { MisionSubirContenido } from './mision-subir-contenido';
import { MisionRedSocial } from './mision-red-social';
import { MisionPreferencia } from './mision-preferencia';
import { MisionEncuestaPregunta } from './mision-encuesta';
import { MisionPerfilAtributo } from './mision-perfil-atributo';
import { MisionVerContenido } from './mision-ver-contenido';
import { Metrica } from './metrica';
import { Injectable } from '@angular/core';

@Injectable()
export class Mision {
    idMision: string;
    imagenArte: string;
    encabezadoArte: string;
    subencabezadoArte: string;
    detalleArte: string;
    cantMetrica: number;
    indTipoMision: string;
    metrica: Metrica;
    misionVerContenido: MisionVerContenido;
    misionPerfilAtributos: MisionPerfilAtributo[];
    misionEncuestaPreguntas: MisionEncuestaPregunta[];
    misionRedSocial: MisionRedSocial;
    misionSubirContenido: MisionSubirContenido;
    misionEncuestaPreferencias: MisionPreferencia[];
    constructor() {
    }
}