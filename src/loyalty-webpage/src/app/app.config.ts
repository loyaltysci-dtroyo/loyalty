import { Injectable } from '@angular/core';

@Injectable()
export class AppConfig {
    public static get BACK_DOMAIN(): string { return 'https://api-demos.flecharoja.com'; }
    public static get IDP_DOMAIN(): string { return 'https://idp-demos.loyaltysci.com'; }
    // public static get AUTHZ_API_CLIENT(): string { return 'loyalty-api' }
    public static get AUTHZ_API_CLIENT(): string { return 'loyalty' }

    // // usado para obtener el json de internacionalizacion
    // public static get LOCAL_DOMAIN(): string { return AppConfig.FRONT_DOMAIN; }
    // usado para cargar los elementos de internacionalizacion
    public static get LOCALE(): string { /*return navigator.language;*/ return "es" }
    // usado para realizar llamados al backend
    public static get TRANS_API_ENDPOINT(): string { return AppConfig.BACK_DOMAIN + '/loyalty-api-client-test-movida/webresources/v0'; }
    // usado para realizar llamados al backend
    public static get API_ENDPOINT(): string { return AppConfig.BACK_DOMAIN + '/loyalty-api-client-test-movida/webresources/v0'; }
    // usado para setear la imagen por default
    public static get DEFAULT_IMG_URL(): string {
        return 'http://580dfadb669a3d46adbc-6dc2c0f932578b1114beed9c144d3102.r58.cf1.rackcdn.com/ee5cf60a-7d8e-4062-8c40-c600d52d4294.jpeg';
    }

    public static get KC_CONF() {
        return {
            url: AppConfig.IDP_DOMAIN + '/auth',
            realm: 'loyalty',
            clientId: 'loyalty-webpage'
        }
    }
}