import { MissionStatusComponent } from './component/mission/mission-status.component';
import { ErrorHandlersService } from './service/error-handler.service';
import {ErrorHandler} from '@angular/core';
import { MemberService } from './service/member.service';
import { Miembro } from './model/miembro';
import { AuthGuard, KeycloakService } from './service/index';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { appRoutes } from './app.routes';
import * as Components from './index';

@NgModule({
  declarations: [
    AppComponent,
    Components.ActivityComponent,
    Components.InfoComponent,
    Components.MissionComponent,
    Components.ProfileComponent,
    Components.ProfileAdditionalComponent,
    Components.ProfileMainComponent,
    Components.ProfileSettingsComponent,
    Components.LandingComponent,
    Components.AccountComponent,
    Components.HomeComponent,
    Components.AccountErrorComponent,
    Components.NotFoundComponent,
    Components.ContactComponent,
    Components.DashComponent,
    Components.RegisterComponent,
    Components.DatePickerComponent,
    Components.CompleteMisssionComponent,
    Components.CompleteMisionSubirContenidoComponent,
    Components.CompleteMissionAtributoComponent,
    Components.CompleteMissionSocialComponent,
    Components.CompleteMissionVerContenidoComponent,
    Components.CompleteMissionSurveyComponent,
    Components.OffersCarouselComponent,
    Components.OfferDetailComponent,
    Components.RewardsComponent,
    Components.CustomBarcodeDisplayComponent,
    Components.StarRatingComponent,
    Components.MultipleSelectionComponent,
    Components.RewardsDetailComponent,
    Components.CompleteMisionPreferenciaComponent,
    Components.MultipleSelectionPreferencesComponent,
    Components.RequestPasswordResetComponent,
    Components.ResetPasswordComponent,
    MissionStatusComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,ReactiveFormsModule,
    HttpModule
  ],
  providers: [
     KeycloakService, AuthGuard, Miembro, MemberService],
  bootstrap: [AppComponent]
})
export class AppModule { }
