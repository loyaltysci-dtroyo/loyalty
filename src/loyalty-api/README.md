# Loyalty API
## Estructura y métodos disponibles de los servicios web:
```
webresources
├───usuario                           GET, POST, PUT
|   ├───rol/{nombreRol}               GET
|   ├───buscar                        GET
|   |   └───{busqueda}                GET    
|   ├───{idUsuario}                   GET
|   ├───exists/{nombre}               GET
|   ├───eliminar/{idUsuario}          PUT
|   └───activar/{idUsuario}           PUT
|
├───rolKeycloak                       GET, POST, PUT
|   └───{id}                          GET, DELETE
|
├───atributo                          GET, POST, PUT
|   ├───buscar                        GET
|   |   └───{busqueda}                GET 
|   └───{idAtributo}                  GET, DELETE
|       └───miembro                   GET
|           └───{idMiembro}           POST, DELETE, GET
|
├───categoria-promocion               GET, POST, PUT
|   ├───{idCategoria}                 GET, DELETE
|   |   └───promocion                 GET
|   |       └───{idPromocion}         POST, DELETE
│   └───exists/{nombre}               GET
|
├───metrica                           GET, POST, PUT
│   ├───exists/{nombre}               GET
|   └───{idMetrica}                   GET, DELETE
|       └───nivel                     GET, POST, PUT
|           └───{idNivel}             GET, DELETE
|
├───miembro                           GET, POST, PUT
|   ├───buscar                        GET
|   |   └───{busqueda}                GET
|   └───{idMiembro}                   GET, DELETE
|       └───nivel                     GET
|           └───{idNivel}             POST, DELETE
|
├───promocion                         GET, PUT, POST
|   ├───buscar                        GET
|   |   └───{busqueda}                GET
│   ├───exists/{nombre}               GET
|   └───{idPromocion}                 GET, DELETE
|       ├───segmento                  GET
|       |   └───{idSegmento}          POST, DELETE
|       ├───miembro                   GET
|       |   └───{idMiembro}           POST, DELETE
|       └───ubicacion                 GET
|           └───{idUbicacion}         POST, DELETE
|
├───segmento                          GET, PUT, POST
|   ├───buscar                        GET
|   |   └───{busqueda}                GET
│   ├───exists/{nombre}               GET
|   └───{idSegmento}                  GET, DELETE
|       ├───reglas                    GET, POST, PUT
|       |   ├───intercambiar          PUT
|       |   └───{idLista}             GET, DELETE
|       |       ├───metricaBalance    GET, POST
|       |       |   └───{idRegla}     DELETE
|       |       ├───metricaCambio     GET, POST
|       |       |   └───{idRegla}     DELETE
|       |       └───miembroAtributo   GET, POST
|       |           └───{idRegla}     DELETE
|       ├───miembro                   GET
|       |   └───{idMiembro}           POST, DELETE
|       └───eligibles                 ---
|           ├───info                  GET
|           └───miembros              GET
│
├───tipoMiembro                       GET
|   └───{idTipo}                      GET,POST, DELETE
|
├───ubicacion                         POST, PUT, GET
|   ├───{idUbicacion}                 GET
|   ├───cambiar-estado/{idUbicacion}  PUT             
|   └───buscar                        GET
|       └───{busqueda}                GET
|
├───preferencia                       POST, PUT, GET
|   ├───buscar                        GET
|   |   └───{busqueda}                GET
|   └───{idPreferencia}               GET, DELETE
|       └───miembro                   GET
|           └───{idMiembro}           POST, DELETE, PUT, GET
|
|
├───grupo                             POST, PUT, GET
|   ├───buscar                        GET
|   |   └───{busqueda}                GET
|   ├───visible/{idGrupo}             PUT
|   └───{idGrupo}                     GET, DELETE
|       └───miembro                   GET
|           └───{idMiembro}           POST, DELETE. GET
|
├───tabla-posiciones                  POST, PUT, GET
|   ├───buscar                        GET
|   |   └───{busqueda}                GET
|   └───{idTabla}                     GET, DELETE
|       └───miembros                  GET
|       └───grupos                    GET   
|
├───util                              
|   ├───paises                        GET
|   ├───sugerencias/{busqueda}        GET
|

```
---
## Detalles de los métodos web:

| Ruta | Metodo | Parametros entrada | Respuesta |
| :--- | :---: | :--- | :--- |
| webresources/usuario | GET | uery: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de usuarios en formato JSON |
| webresources/usuario | POST | body: información del usuario en formato JSON | |
| webresources/usuario | PUT | body: información del usuario en formato JSON | |
| webresources/usuario/buscar | GET | query: estado - indicadores opcionales sobre los estados de los usuarios deseados <br /> uery: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de los usuarios encontrados en formato JSON |
| webresources/usuario/buscar/{busqueda} | GET | path: busqueda - cadena de texto con palabras claves <br /> query: estado - indicadores opcionales sobre los estados de los usuarios deseados <br /> query: pagina - indicador opcional de la pagina actual de resultados (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de los usuarios encontrados en formato JSON |
| webresources/usuario/{idUsuario} | GET | path: idUsuario - identificador del usuario | Información del atributo en formato JSON |
| webresources/usuario/rol/{nombreRol} | GET | path: nombreRol - nombre del rol de los usuarios que se quieren buscar <br /> query: pagina - indicador opcional de la pagina actual de resultados (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de los usuarios encontrados en formato JSON |
| webresources/usuario/exists/{nombre} | GET | path: nombre - nombre del usuario |Obtiene si el userName ya existe (true), o no (false) en algun usuario |
| webresources/usuario/eliminar/{idUsuario} | PUT |  path: idUsuario - identificador del usuario | |
| webresources/usuario/activar/{idUsuario} | PUT |  path: idUsuario - identificador del usuario | |
| webresources/rolKeycloak | GET | | listado de roles en formato JSON |
| webresources/rolKeycloak/{nombreRol} | GET | path: nombreRol - identificador único del rol | Información del rol en formato JSON |
| webresources/rolKeycloak | POST | body: información del rol en formato JSON | |
| webresources/rolKeycloak | PUT | body: información del rol en formato JSON | |
| webresources/rolKeycloak/{nombreRol} | DELETE | path: nombre -identificador único del rol | |
| webresources/atributo | GET | query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | Obtiene la lista de todos los atributos en formato JSON |
| webresources/atributo | POST | body: información del atributo en formato JSON | |
| webresources/atributo | PUT | body: información del atributo en formato JSON | |
| webresources/atributo/buscar | GET | query: dato - indicadores opcionales sobre los tipos de datos de la preferencia deseada <br /> query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de atributos encontrados en formato JSON |
| webresources/atributo/buscar/{busqueda} | GET | path: busqueda - cadena de texto con palabras claves <br />  query: dato - indicadores opcionales sobre los tipos de datos de la preferencia deseada <br /> query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de los atributos encontrados en formato JSON |
| webresources/atributo/{idAtributo} | GET | path: idAtributo- identificador único del atributo a buscar | Información del atributo en formato JSON |
| webresources/atributo/{idAtributo} | DELETE | path: idAtributo - identificador único del atributo | |
| webresources/atributo/{idAtributo}/miembro | GET | path: idAtributo - identificador del atributo | listado de los miembros en formato JSON |
| webresources/atributo/{idAtributo}/miembro/{idMiembro} | POST | path: idMiembro - identificador del miembro  <br /> path:idAtributo- identificador único del atributo | |
| webresources/atributo/{idAtributo}/miembro/{idMiembro} | DELETE | path: idMiembro - identificador del miembro <br /> path:idAtributo- identificador único del atributo | |
| webresources/atributo/{idAtributo}/miembro/{idMiembro} | GET | path: idMiembro - identificador del miembro <br /> path:idAtributo- identificador único del atributo | Obtiene la lista de atributos que tiene un miembro en formato JSON |
| webresources/categoria-promocion | GET | query: pagina - indicador opcional de la pagina actual de resultados (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de categoria de promocion en formato JSON |
| webresources/categoria-promocion | POST | body: informacion de la categoria de promocion en formato JSON | identificador generado de la categoria de promocion |
| webresources/categoria-promocion | PUT | body: informacion de la categoria de promocion en formato JSON | |
| webresources/categoria-promocion/{idCategoria} | GET | path: idCategoria - identificador de la categoria | informacion de la categoria de promocion en formato JSON |
| webresources/categoria-promocion/{idCategoria} | DELETE | path: idCategoria - identificador de la categoria | |
| webresources/categoria-promocion/{idCategoria}/promocion | GET | path: idCategoria - identificador de la categoria <br/> query: pagina - indicador opcional de la pagina actual de resultados (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de promociones en la categoria en formato JSON |
| webresources/categoria-promocion/{idCategoria}/promocion/{idPromocion} | POST | path: idCategoria - identificador de la categoria <br/> path: idPromocion - identificador de la promocion | |
| webresources/categoria-promocion/{idCategoria}/promocion/{idPromocion} | DELETE | path: idCategoria - identificador de la categoria <br/> path: idPromocion - identificador de la promocion | |
| webresources/metrica | GET | query: estado - indicadores opcionales de estados deseados <br /> query: pagina - indicador opcional de la pagina actual de resultados (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de las metricas en formato JSON |
| webresources/metrica | POST | body: informacion de la metrica en formato JSON | |
| webresources/metrica | PUT | body: informacion de la metrica en formato JSON | |
| webresources/metrica/exists/{nombre} | GET | path: nombre - texto del nombre interno de la metrica a consultar | true/false en formato JSON |
| webresources/metrica/{idMetrica} | GET | path: idMetrica - identificador de la metrica | informacion de la metrica en formato JSON |
| webresources/metrica/{idMetrica} | DELETE | path: idMetrica - identificador de la metrica | |
| webresources/metrica/{idMetrica}/nivel | GET | path: idMetrica - identificador de la metrica | listado de todos los niveles de metrica en formato JSON |
| webresources/metrica/{idMetrica}/nivel | POST | path: idMetrica - identificador de la metrica | |
| webresources/metrica/{idMetrica}/nivel | PUT | path: idMetrica - identificador de la metrica | |
| webresources/metrica/{idMetrica}/nivel/{idNivel} | GET | path: idMetrica - identificador de la metrica <br /> path: idNivel - identificador del nivel de metrica | informacion del nivel de metrica en formato JSON |
| webresources/metrica/{idMetrica}/nivel/{idNivel} | DELETE | path: idMetrica - identificador de la metrica <br /> path: idNivel - identificador del nivel de metrica | |
| webresources/miembro | GET | query: estado - indicadores opcionales de estados deseados <br /> query: pagina - indicador opcional de la pagina actual de resultados (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de los miembros en formato JSON |
| webresources/miembro | POST | body: informacion del miembro en formato JSON | |
| webresources/miembro | PUT | body: informacion del miembro en formato JSON | |
| webresources/miembro/buscar | GET | query: estado - indicadores opcionales de estados deseados <br /> query: pagina - indicador opcional de la pagina actual de resultados (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de los miembros en formato JSON |
| webresources/miembro/buscar/{busqueda} | GET | path: busqueda - cadena de texto con las palabras claves <br /> query: estado - indicadores opcionales de estados deseados <br /> query: pagina - indicador opcional de la pagina actual de resultados (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de miembros en formato JSON |
| webresources/miembro/{idMiembro} | GET | path: idMiembro - identificador del miembro | informacion del miembro en formato JSON |
| webresources/miembro/{idMiembro} | DELETE | path: idMiembro - identificador del miembro | |
| webresources/miembro/{idMiembro}/nivel | GET | path: idMiembro - identificador del miembro | listado de niveles iniciales de un miembro en formato JSON |
| webresources/miembro/{idMiembro}/nivel/{idNivel} | POST | path: idMiembro - identificador del miembro <br/> path: idNivel - identificador del nivel | |
| webresources/miembro/{idMiembro}/nivel/{idNivel} | DELETE | path: idMiembro - identificador del miembro <br/> path: idNivel - identificador del nivel | |
| webresources/promocion | GET | query: pagina <br/> query: cantidad <br/> query: estado | listado de todas las promociones en formato JSON |
| webresources/promocion | PUT | body: informacion de la promocion en formato JSON | |
| webresources/promocion | POST | body: informacion de la promocion en formato JSON | UUID generado para la promocion |
| webresources/promocion/buscar | GET | query: pagina <br/> query: cantidad <br/> query: estado | listado de promociones en formato JSON |
| webresources/promocion/buscar/{busqueda} | GET | path: busqueda <br/> query: pagina <br/> query: cantidad <br/> query: estado | listado de promociones en formato JSON |
| webresources/promocion/exists/{nombre} | GET | path: nombre | true/false |
| webresources/promocion/{idPromocion} | GET | path: idPromocion | informacion sobre la promocion en formato JSON |
| webresources/promocion/{idPromocion} | DELETE | path: idPromocion | |
| webresources/promocion/{idPromocion}/segmento | GET | path: idPromocion <br/> query: pagina <br/> query: cantidad <br/> query: accion | listado de segmentos en formato JSON |
| webresources/promocion/{idPromocion}/segmento/{idSegmento} | POST | path: idPromocion <br/> query: accion | |
| webresources/promocion/{idPromocion}/segmento/{idSegmento} | DELETE | path: idPromocion | |
| webresources/promocion/{idPromocion}/miembro | GET | path: idPromocion <br/> query: pagina <br/> query: cantidad <br/> query: accion | listado de miembros en formato JSON |
| webresources/promocion/{idPromocion}/miembro/{idMiembro} | POST | path: idPromocion <br/> query: accion | |
| webresources/promocion/{idPromocion}/miembro/{idMiembro} | DELETE | path: idPromocion | |
| webresources/promocion/{idPromocion}/ubicacion | GET | path: idPromocion <br/> query: pagina <br/> query: cantidad <br/> query: accion | listado de ubicaciones en formato JSON |
| webresources/promocion/{idPromocion}/ubicacion/{idUbicacion} | POST | path: idPromocion <br/> query: accion | |
| webresources/promocion/{idPromocion}/ubicacion/{idUbicacion} | DELETE | path: idPromocion | |
| webresources/segmento | GET | query: estado - indicadores opcionales sobre los estados de los segmentos deseados | listado de los segmentos encontrados en formato JSON |
| webresources/segmento | POST | body: informacion del segmento en formato JSON | UUID generado para el segmento |
| webresources/segmento | PUT | body: informacion del segmento en formato JSON | |
| webresources/segmento/buscar | GET | query: estado - indicadores opcionales sobre los estados de los segmentos deseados | listado de los segmentos encontrados en formato JSON |
| webresources/segmento/buscar/{busqueda} | GET | path: busqueda - cadena de texto con palabras claves <br /> query: estado - indicadores opcionales sobre los estados de los segmentos deseados | listado de los segmentos encontrados en formato JSON |
| webresources/segmento/exists/{nombre} | GET | | |
| webresources/segmento/{idSegmento} | DELETE | | |
| webresources/segmento/{idSegmento} | GET | | |
| webresources/segmento/{idSegmento}/reglas | POST | | |
| webresources/segmento/{idSegmento}/reglas | PUT | | |
| webresources/segmento/{idSegmento}/reglas | GET | | |
| webresources/segmento/{idSegmento}/reglas/intercambiar | PUT | | |
| webresources/segmento/{idSegmento}/reglas/{idLista} | GET | | |
| webresources/segmento/{idSegmento}/reglas/{idLista} | DELETE | | |
| webresources/segmento/{idSegmento}/reglas/{idLista}/metricaBalance | GET | | |
| webresources/segmento/{idSegmento}/reglas/{idLista}/metricaBalance | POST | | |
| webresources/segmento/{idSegmento}/reglas/{idLista}/metricaBalance/{idRegla} | DELETE | | |
| webresources/segmento/{idSegmento}/reglas/{idLista}/metricaCambio | GET | | |
| webresources/segmento/{idSegmento}/reglas/{idLista}/metricaCambio | POST | | |
| webresources/segmento/{idSegmento}/reglas/{idLista}/metricaCambio/{idRegla} | DELETE | | |
| webresources/segmento/{idSegmento}/reglas/{idLista}/miembroAtributo | GET | | |
| webresources/segmento/{idSegmento}/reglas/{idLista}/miembroAtributo | POST | | |
| webresources/segmento/{idSegmento}/reglas/{idLista}/miembroAtributo/{idRegla} | DELETE | | |
| webresources/segmento/{idSegmento}/miembro | GET | path: idSegmento - identificador del segmento <br/> query: accion - indicador de exclusion o inclusion | listado de miembros en formato JSON |
| webresources/segmento/{idSegmento}/miembro/{idMiembro} | POST | path: idSegmento - identificador del segmento <br/> path: idMiembro - identificador del miembro <br/> query: accion - indicador de exclusion o inclusion | |
| webresources/segmento/{idSegmento}/miembro/{idMiembro} | DELETE | path: idSegmento - identificador del segmento <br/> path: idMiembro - identificador del miembro | |
| webresources/segmento/{idSegmento}/eligibles/info | GET | path: idSegmento - identificador del segmento | JSON con informacion adicional almacenada en registro de eligibles |
| webresources/segmento/{idSegmento}/eligibles/miembros | GET | path: idSegmento - identificador del segmento | listado de miembros en formato JSON |
| webresources/tipoMiembro | GET | | listado de todos los tipos de miembros encontrados en formato JSON |
| webresources/tipoMiembro/{idTipo} | GET | path: idTipo - identificador del tipo de miembro | informacion sobre un tipo de miembro en formato JSON |
| webresources/tipoMiembro/{idTipo} | DELETE | path: idTipo - identificador del tipo de miembro | |
| webresources/tipoMiembro/{nombre} | POST | path: nombre - texto sobre el nombre del tipo de miembro a insertar | |
| webresources/ubicacion | GET | query: estado - indicadores opcionales sobre los estados de las ubicaciones deseadas <br />query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de las ubicaciones encontrados en formato JSON |
| webresources/ubicacion | POST | body: información de la nueva ubicacion en formato JSON | |
| webresources/ubicacion | PUT | body: información de la ubicacion en formato JSON | |
| webresources/ubicacion/{idUbicacion} | GET | path: idUbicacion - identificador de la ubicacion | Obtiene la ubicacion en formato JSON |
| webresources/ubicacion/cambiar-estado/{idUbicación} | PUT | path: idUbicacion - identificador de la ubicación | |
| webresources/ubicacion/buscarUbicacion | GET | query: estado - indicadores opcionales sobre los estados de las ubicaciones deseadas <br />uery: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de las ubicaciones encontrados en formato JSON |
| webresources/ubicacion/buscarUbicacion/{busqueda} | GET | path: busqueda - cadena de texto con palabras claves <br /> query: estado - indicadores opcionales sobre los estados de las ubicaciones deseadas <br /> query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de las ubicaciones encontradas en formato JSON |
| webresources/preferencia | GET | query: respuesta- indicador del tipo de respuesta de la preferencia a buscar <br /> query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de preferencias en formato JSON   |
| webresources/preferencia | POST | body: información de la nueva preferencia en formato JSON | |
| webresources/preferencia | PUT | body: informaciónn de la preferencia en formato JSON | |
| webresources/preferencia/{idPreferencia} | GET | path: idPreferencia - identificador único de la preferencia | Obtiene la preferencia en formato JSON |
| webresources/preferencia/{idPreferencia} | DELETE | path: idPreferencia -identificador único de la preferencia | |
| webresoources/preferencia/buscar | GET | query: respuesta- indicadores opcionales sobre los tipos de respuesta de las preferencias deseadas <br /> query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de las preferencias encontradas en formato JSON |
| webresources/preferencia/buscar/{busqueda} | GET | path: busqueda - cadena de texto con palabras claves <br /> query: respuesta - indicadores opcionales sobre el tipo de respuesta de las preferencias deseadas <br />query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de las preferencias encontradas en formato JSON |
| webresources/preferencia/{idPreferencia}/miembro | GET | path: idPreferencia- identificador único de la preferencia <br /> query: pagina - indicador opcional de la pagina actual de resultados (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | Listado de todas las respuestas a preferencias en formato JSON |
| webresources/preferencia/{idPreferencia}/miembro/{idMiembro} | POST | path: idPreferencia- identificador único de la preferencia <br /> path: idMiembro- identificador único del miembro al que se desea ingresar una respuesta a una preferencia |  |
| webresources/preferencia/{idPreferencia}/miembro/{idMiembro} | DELETE | path: idPreferencia - identificador único de la preferencia <br /> path: idMiembro- identificador único al cual se eliminara una preferencia | |
| webresources/preferencia/{idPreferencia}/mimebro/{idMiembro} | PUT | path: idPreferencia- identificador único de la preferencia <br /> path: idMiembro- identificador único del miembro | |
| webresources/preferencia/{idPreferencia}/miembro/{idMiembro} | GET | path: idPreferencia- identificador único de la preferencia <br /> path: idMiembro- identificador único del miembro <br />query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | Lista de respuestas de un miembro especifico en formato JSON |
| webresources/grupo | POST | body: información del grupo en formato JSON | |
| webresources/grupo | PUT | body: información del grupo en formato JSON | |
| webresources/grupo | GET | query: visible- indicador de visibilidad del grupo <br /> query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | Listado de grupos en formato JSON |
| webresources/grupo/visible/{idGrupo} | PUT | path: idGrupo- identificador del grupo <br /> query: visible- indicador de visibilidad | |
| webresoources/grupo/buscar | GET | query: respuesta- indicadores opcionales sobre la visibilidad de los grupos deseados <br /> query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de los grupos encontrados en formato JSON |
| webresources/grupo/buscar/{busqueda} | GET | path: busqueda - cadena de texto con palabras claves <br /> query: visible - indicadores opcionales sobre el tipo de visibilidad de los grupos deseados <br />query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de los grupos encontrados en formato JSON |
| webresources/grupo/{idGrupo} | GET | path: idGrupo- identificador del grupo | Información del grupo en formato JSON |
| webresources/grupo/{idGrupo} | DELETE | path: idGrupo- identificador del grupo | |
| webresources/grupo/{idGrupo}/miembro | GET | path: idGrupo- identificador del grupo <br />query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0)   | Listado de miembros asociados a un grupo en formato JSON |
| webresources/grupo/{idGrupo}/miembro/{idMiembro} | POST | path: idGrupo- identificador del grupo <br /> path: idMiembro-identificador del miembro | |
| webresources/grupo/{idGrupo}/miembro/{idMiembro} | DELETE | path: idGrupo- identificador del grupo <br /> path:idMiembro-identificador del miembro | |
| webresources/grupo/{idGrupo}/miembro/{idMiembro} | GET | path: idGrupo- identificador del grupo <br /> path:idMiembro-identificador del miembro <br />query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | Listado de grupos de un miembro en formato JSON|
| webresources/tabla-posiciones | POST | body: información de la tabla en formato JSON | |
| webresources/tabla-posiciones | PUT | body: información de la tabla en formato JSON | |
| webresources/tabla-posiciones | GET | query: tipo- indicador opcional del tipo de tabla <br /> query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | Listado de tablas en formato JSON |
| webresources/tabla-posiciones/buscar | GET | query: tipo- indicadores opcionales sobre el tipo de tabla deseadas <br /> query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de las tablas encontradas en formato JSON |
| webresources/tabla-posiciones/buscar/{busqueda} | GET | path: busqueda - cadena de texto con palabras claves <br /> query: tipo - indicadores opcionales sobre el tipo de tabla  deseados <br />query: registro - indicador opcional del registro por el cual se desea iniciar la busqueda (>=1) <br /> query: cantidad - cantidad opcional de resultados deseados por pagina (>=0) | listado de los grupos encontrados en formato JSON |
| webresources/tabla-posiciones/{idTabla} | GET | path: idTabla- identificador de la tabla | Información de la tabla en formato JSON |
| webresources/tabla-posiciones/{idTabla} | DELETE | path: idTabla - identificador de la tabla | |
| webresources/tabla-posiciones/{idTabla}/miembros | GET | path: idTabla- identificador único de la tabla | Lista ordenada de mayor a menor puntaje acumulado de los miembros en formato JSON |
| webresources/tabla-posiciones/{idTabla}/grupos | GET | path: idTabla- identificador único de la tabla | Lista ordenada de mayor a menor puntaje acumulado por los miembros de los grupos en formato JSON |
| webresources/util/paises | GET | | Listado de todos los paises del mundo registrados en la base de datos |
| webresources/util/sugerencias/{busqueda} | GET | path: busqueda- cadena de texto con palabras claves <br /> query: entidad- identificador de la entidad en la que se desea buscar <br /> query: atributo- identificador del atributo de la entidad en la que se desea buscar | Listado de las cuatro primeras palabras sugeridas que quizo escribir |
