package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.AtributoDinamico;
import com.flecharoja.loyalty.model.CategoriaProducto;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.DataImportacionMiembroExtendido;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Pais;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.TrabajosInternos;
import com.flecharoja.loyalty.util.Indicadores;
import com.google.common.collect.Lists;
import com.opencsv.CSVReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author wtencio, svargas
 */
@Stateless
public class ImportarBean {
    
    /**
     * Formato de la fecha a usar
     */
    private static final String dateFormat = null;
    
    /**
     * Listado de los atributos regulares elegibles para la importacion de
     * miembro
     */
    private enum AtributosElegiblesImportacionMiembro {
        NOMBRE_USUARIO("A"),
        DOC_IDENTIFICACION("B"),
        DIRECCION("C"),
        CIUDAD_RESIDENCIA("D"),
        ESTADO_RESIDENCIA("E"),
        PAIS_RESIDENCIA("F"),
        CODIGO_POSTAL("G"),
        TELEFONO_MOVIL("H"),
        IND_ESTADO_MIEMBRO("I"),
        IND_ESTADO_SISTEMA("J"),
        FECHA_INGRESO("K"),
        FECHA_EXPIRACION("L"),
        FECHA_NACIMIENTO("M"),
        IND_GENERO("N"),
        IND_ESTADO_CIVIL("O"),
        IND_EDUCACION("P"),
        INGRESO_ECONOMICO("Q"),
        NOMBRE("R"),
        APELLIDO("S"),
        APELLIDO2("T"),
        IND_CONTACTO_EMAIL("U"),
        IND_CONTACTO_SMS("V"),
        IND_CONTACTO_NOTIFICACION("W"),
        IND_CONTACTO_ESTADO("X"),
        IND_HIJOS("Y"),
        COMENTARIOS("Z"),
        AVATAR("0"),
        CONTRASENA("1"),
        CORREO("2");

        private final String value;
        private static final Map<String, AtributosElegiblesImportacionMiembro> lookup = new HashMap<>();

        private AtributosElegiblesImportacionMiembro(String value) {
            this.value = value;
        }

        static {
            for (AtributosElegiblesImportacionMiembro atributo : values()) {
                lookup.put(atributo.value, atributo);
            }
        }

        public String getValue() {
            return value;
        }

        public static AtributosElegiblesImportacionMiembro get(String value) {
            return value == null ? null : lookup.get(value);
        }
    }

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    SegmentoListaMiembroBean segmentoListaMiembroBean;

    @EJB
    TrabajoBean trabajoBean;

    @EJB
    MiembroBean miembroBean;

    @EJB
    AtributoMiembroBean atributoMiembroBean;

    @EJB
    CodigoCertificadoBean codigoCertificadoBean;
    
    @EJB
    CertificadoCategoriaBean certificadoCategoriaBean;

    /**
     * Método que realiza el trabajo de importar de un archivo en base 64
     * miembros a un segmento
     *
     * @param base64 archivo con los ids o doc de identificaciones de los
     * miembros a registrar en el segmento
     * @param atributo atributo clave que trae el archivo(id del miembro o doc
     * de identificacion)
     * @param idSegmento identificador del segmento al que se va integrar los
     * miembros
     * @param idTrabajo identificador del trabajo
     * @param accion a realizar con los miembros(incluir /excluir)
     * @param usuario
     * @param locale
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    @Asynchronous
    public void importarMiembroSegmento(String base64, Miembro.Atributos atributo, String idSegmento, String idTrabajo, Character accion, String usuario, Locale locale) {
        if (atributo==null) {
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "Atributo id miembro no valido", 0d);
        }
        //ARCHIVO BASE 64
        //verificacion del archivo base64
        if (base64 == null || base64.trim().isEmpty()) {
            try {
                trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "file_invalid", Double.valueOf(0));
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
        }
        //decodificacion del string a bytes y verificacion de que sea un base64 valido
        byte[] fileBytes = null;
        try {
            fileBytes = Base64.getDecoder().decode(base64);
        } catch (IllegalArgumentException e) {
            //no es un base64 valido
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "file_invalid", Double.valueOf(0));
        }

        //Tratamiento del archivo
        ByteArrayInputStream in = new ByteArrayInputStream(fileBytes);
        CSVReader reader = new CSVReader(new InputStreamReader(in), ',');
        List<String> usuarioNoAgregados = new ArrayList<>();
        List<String> miembrosElegibles = new ArrayList<>();
        double completado = 0;
        // read line by line
        try {
            //primera linea es del encabezado, por lo que se ignora
            List<String[]> lista = reader.readAll();
            lista.remove(0);
            reader.close();
            //se recorre linea x linea para obtener la información del archivo
            List<List<String[]>> partitions = Lists.partition(lista, 100);
            for (List<String[]> list : partitions) {
                list.stream().forEach((record) -> {
                    String idMiembro = record[0];

                    switch (atributo) {
                        case DOC_IDENTIFICACION: {
                            List<String> miembro = em.createNamedQuery("Miembro.findIdentificador").setParameter("docIdentificacion", idMiembro).getResultList();
                            if (miembro == null) {
                                usuarioNoAgregados.add(idMiembro);
                            } else {
                                miembrosElegibles.add(miembro.get(0));
                            }
                            break;
                        }

                        case ID_MIEMBRO: {
                            Miembro miembro = em.find(Miembro.class, idMiembro);
                            if (miembro == null) {
                                usuarioNoAgregados.add(idMiembro);
                            } else {
                                miembrosElegibles.add(idMiembro);
                            }
                            break;
                        }
                    }
                });
                if (!miembrosElegibles.isEmpty()) {
                    try {
                        segmentoListaMiembroBean.includeExcludeBatchMiembro(idSegmento, miembrosElegibles, accion, usuario, locale);
                        completado += (100 / partitions.size());
                        miembrosElegibles.clear();
                        if (completado < 100) {
                            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.PROCESANDO.getValue(), "Procesado " + completado + "% del archivo", completado);
                        }
                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    }
                }
            }
            try {
                trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.COMPLETADO.getValue(), "Miembros no registrados : " + usuarioNoAgregados.stream().collect(Collectors.joining(", ")), Double.valueOf(100));
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
        } catch (IOException | ArrayIndexOutOfBoundsException e) {
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "file_invalid", Double.valueOf(0));
        }
    }

    /**
     * Método que realiza la importacion de miembros
     *
     * @param base64 archivo con la informacion del miembro
     * @param idTrabajo identificador del trabajo
     * @param usuario en sesion
     * @param locale
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    @Asynchronous
    public void importarMiembro(String base64, String idTrabajo, String usuario, Locale locale) {
        //verificacion del archivo base64
        //verificacion del archivo base64
        if (base64 == null || base64.trim().isEmpty()) {
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), base64 == null ? "nulo" : "vacio", Double.valueOf(0));
        }
        //decodificacion del string a bytes y verificacion de que sea un base64 valido
        byte[] fileBytes = null;
        try {
            fileBytes = Base64.getDecoder().decode(base64);
        } catch (IllegalArgumentException e) {
            //no es un base64 valido
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "invalid base64", Double.valueOf(0));
        }
        //Tratamiento del archivo
        ByteArrayInputStream in = new ByteArrayInputStream(fileBytes);
        CSVReader reader = new CSVReader(new InputStreamReader(in), ',');
        List<String> miembrosNoIngresados = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        //obtencion de la configuracion general la metrica general y el bono de entrada
        List<ConfiguracionGeneral> confi = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
        if (confi.isEmpty()) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "main_metric_not_found");
        }
        ConfiguracionGeneral configuracion = confi.get(0);
        String idMetricaInicial = configuracion.getIdMetricaInicial().getIdMetrica();
        int bonoEntrada = configuracion.getBonoEntrada().intValue();
        Date date = Calendar.getInstance().getTime();
        try {
            List<String[]> readAll = reader.readAll();
            reader.close();
            if (readAll.isEmpty()) {
                trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "No existen registros", Double.valueOf(0));
            } else {
                double completado = 0;
                //formato-> docIdentificacion, fechaNacimiento, nombre, apellido1,apellido2, genero, correo, contraseña, telefono, ciudad, fechaIngreso
                readAll.remove(0);
                List<List<String[]>> partitions = Lists.partition(readAll, 100);
                for (List<String[]> partition : partitions) {
                    for (String[] read : partition) {
                        if (read[0] == null || read[2] == null || read[3] == null || read[4] == null) {
                            miembrosNoIngresados.add(Arrays.toString(read));
                            continue;
                        }
                        String docIdentificacion = read[0];
                        try {
                            if (miembroBean.existsIdentificador(docIdentificacion)) {
                                miembrosNoIngresados.add(Arrays.toString(read));
                                continue;
                            }
                        } catch (Exception e) {
                            miembrosNoIngresados.add(Arrays.toString(read));
                            continue;
                        }
                        String fechaNacimiento = read[1];
                        Date fechaNacimientoDate = null;
                        if (!fechaNacimiento.isEmpty()) {
                            try {
                                fechaNacimientoDate = formatter.parse(fechaNacimiento);
                            } catch (ParseException e) {
                                miembrosNoIngresados.add(Arrays.toString(read));
                                continue;
                            }
                        }

                        String genero = read[5];
                        Character generoChar;
                        if (genero.equals("1")) {
                            generoChar = Miembro.ValoresIndGenero.MASCULINO.getValue();
                        } else {
                            generoChar = Miembro.ValoresIndGenero.FEMENINO.getValue();
                        }
                        String correo = read[6];
                        try {
                            if (miembroBean.existsCorreo(correo)) {
                                miembrosNoIngresados.add(Arrays.toString(read));
                                continue;
                            }
                        } catch (Exception e) {
                            miembrosNoIngresados.add(Arrays.toString(read));
                            continue;
                        }

                        String contrasena = read[7];
                        try {
                            if (contrasena.isEmpty()|| contrasena.equals("") || contrasena == null || contrasena.equals("null")) {
                                contrasena = Indicadores.CONTRASENA_TEMPORAL_PREDETERMINADA;
                            } else if (!miembroBean.validatePassword(contrasena)) {
                                miembrosNoIngresados.add(Arrays.toString(read));
                                continue;
                            }
                        } catch (Exception e) {
                            miembrosNoIngresados.add(Arrays.toString(read));
                            continue;
                        }

                        String fechaIngreso = read[10];
                        Date fechaIngresoDate = null;
                        if (!fechaIngreso.isEmpty() || !fechaIngreso.equals("")|| !fechaIngreso.equals("null") || fechaIngreso!=null) {
                            try {
                                fechaIngresoDate = formatter.parse(fechaIngreso);
                            } catch (ParseException e) {
                                miembrosNoIngresados.add(Arrays.toString(read));
                                continue;
                            }
                        }
                        try {
                            miembroBean.insertMiembroImport(new Miembro(docIdentificacion, fechaIngresoDate, Miembro.Estados.ACTIVO.getValue(), read[9], read[8], fechaNacimientoDate, generoChar, date, usuario, date, usuario, Indicadores.URL_IMAGEN_PREDETERMINADA, read[2], read[3], read[4], 1L, contrasena, correo, correo), idMetricaInicial, bonoEntrada, locale);
                        } catch (Exception e) {
                            miembrosNoIngresados.add(Arrays.toString(read));
                        }

                    }
                    completado += (100 / partitions.size());
                    if (completado < 100) {
                        try {
                            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.PROCESANDO.getValue(), "Procesado " + completado + "% del archivo", completado);
                        } catch (Exception e) {
                            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        }
                    }
                }
                try {
                    trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.COMPLETADO.getValue(), "Información no actualizada : " + miembrosNoIngresados.stream().collect(Collectors.joining(", ")), Double.valueOf(100));
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                }
            }
        } catch (IOException | ArrayIndexOutOfBoundsException ex) {
            if (ex instanceof IOException) {
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "file_invalid", Double.valueOf(0));
            }else{
                trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "file_invalid", Double.valueOf(0));
            }
        }
    }

    /**
     * Método que actualiza la informacion ya sea de un atributo estatico o
     * dinamico
     *
     * @param base64
     * @param idAtributo identificador del atributo o el nombre del atributo a
     * actualizar
     * @param tipoAtributo D-> dinamico, E-> estatico
     * @param tipoIdentificacion si el archivo viene con el doc de
     * identificacion o la cedula
     * @param idTrabajo
     * @param usuario
     * @param locale
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    @Asynchronous
    public void actualizaInfoMiembro(String base64, String idAtributo,
            Character tipoAtributo, String tipoIdentificacion,
            String idTrabajo, String usuario,
            Locale locale
    ) {
        //verificacion del archivo base64
        if (base64 == null || base64.trim().isEmpty()) {
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), base64 == null ? "nulo" : "vacio", Double.valueOf(0));
        }
        //decodificacion del string a bytes y verificacion de que sea un base64 valido
        byte[] fileBytes = null;
        try {
            fileBytes = Base64.getDecoder().decode(base64);
        } catch (IllegalArgumentException e) {
            //no es un base64 valido
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), base64, Double.valueOf(0));
        }
        //Tratamiento del archivo
        ByteArrayInputStream in = new ByteArrayInputStream(fileBytes);
        CSVReader reader = new CSVReader(new InputStreamReader(in), ',');
        List<String> miembrosNoIngresados = new ArrayList<>();

        // read line by line
        try {
            //primera linea es del encabezado, por lo que se ignora
            List<String[]> lista = reader.readAll();
            reader.close();
            lista.remove(0);
            //si la lista esta vacia se pone el trabajo como fallido
            if (lista.isEmpty()) {
                try {
                    trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "No existe información para actualizar", Double.valueOf(0));
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                }
            } else if (!lista.isEmpty()) {
                // si la lista no esta vacia, se particiona la lista en  pedazos de 1000
                double completado = 0;
                List<List<String[]>> partitions = Lists.partition(lista, 1000);
                for (List<String[]> partition : partitions) {
                    for (String[] record : partition) {
                        //si alguno de los datos viene nulo no se ingresa 
                        if (record[0] == null || record[0].isEmpty() || record[1] == null || record[1].isEmpty()) {
                            miembrosNoIngresados.add(Arrays.toString(record));
                        } else {
                            String idMiembro = null;
                            //si el tipo de identificacion es doc de identificacion, se busca el miembro y el id del miembro
                            if (tipoIdentificacion.equals(Miembro.Atributos.DOC_IDENTIFICACION.getValue())) {
                                List<String> miembro = em.createNamedQuery("Miembro.findIdentificador").setParameter("docIdentificacion", record[0]).getResultList();
                                if (miembro.isEmpty()) {
                                    miembrosNoIngresados.add(Arrays.toString(record));
                                } else {
                                    idMiembro = miembro.get(0);
                                }
                                //si el valor es de id del miembro, se obtiene de una vez
                            } else if (tipoIdentificacion.equals(Miembro.Atributos.ID_MIEMBRO.getValue())) {
                                idMiembro = record[0];
                            }
                            //switch para el tipo de atributo que se desea actualizar E-> estatico, D->dinamico
                            switch (tipoAtributo) {
                                case 'E': {
                                    try {
                                        String id = miembroBean.actualizarAtributoMiembro(Miembro.Atributos.get(idAtributo), record[1], idMiembro, usuario, locale);
                                        if (id != null) {
                                            miembrosNoIngresados.add(id);
                                        }
                                    } catch (Exception e) {
                                        miembrosNoIngresados.add(Arrays.toString(record));
                                    }
                                    break;
                                }
                                case 'D': {
                                    try {
                                        atributoMiembroBean.actualizarInfoMiembro(idAtributo, idMiembro, record[1], usuario, locale);
                                    } catch (Exception e) {
                                        miembrosNoIngresados.add(Arrays.toString(record));
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    completado += (100 / partitions.size());
                    if (completado < 100) {
                        try {
                            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.PROCESANDO.getValue(), "Procesado " + completado + "% del archivo", completado);
                        } catch (Exception e) {
                            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        }
                    }
                }
                try {
                    trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.COMPLETADO.getValue(), "Información no actualizada : " + miembrosNoIngresados.stream().collect(Collectors.joining(", ")), Double.valueOf(100));
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                }
            }
        } catch (IOException | ArrayIndexOutOfBoundsException e) {
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "file_invalid", Double.valueOf(0));
        }
    }

    /**
     * importacion de certificados a premio
     * 
     * @param base64 data
     * @param idPremio id de premio
     * @param idTrabajo id de trabajo asignado
     * @param usuario id admin
     * @param locale locale
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    @Asynchronous
    public void importarCodigoCertificado(String base64, String idPremio,
            String idTrabajo, String usuario,
            Locale locale
    ) {
        //ARCHIVO BASE 64
        //verificacion del archivo base64
        if (base64 == null || base64.trim().isEmpty()) {
            try {
                trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "file_invalid", Double.valueOf(0));
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
        }
        //decodificacion del string a bytes y verificacion de que sea un base64 valido
        byte[] fileBytes = null;
        try {
            fileBytes = Base64.getDecoder().decode(base64);
        } catch (IllegalArgumentException e) {
            //no es un base64 valido
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "file_invalid", Double.valueOf(0));
        }
        //Tratamiento del archivo
        ByteArrayInputStream in = new ByteArrayInputStream(fileBytes);
        CSVReader reader = new CSVReader(new InputStreamReader(in), ',');
        List<String> codigosNoRegistrados = new ArrayList<>();
        // read line by line
        try {
            //primera linea es del encabezado, por lo que se ignora
            Premio premio = em.find(Premio.class, idPremio);
            if (premio == null || !premio.getIndTipoPremio().equals(Premio.Tipo.CERTIFICADO.getValue())) {
                try {
                    trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "Premio no existe o no es de tipo certificado", Double.valueOf(0));
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                }
            }
            List<String[]> lista = reader.readAll();
            reader.close();
            lista.remove(0);
            if (lista.isEmpty()) {
                trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "No existe información para insertar", Double.valueOf(0));
            } else if (!lista.isEmpty()) {
                double completado = 0;
                List<List<String[]>> partitions = Lists.partition(lista, 1000);
                for (List<String[]> partition : partitions) {
                    for (String[] record : partition) {
                        if (record[0] == null || record[0].isEmpty()) {
                            codigosNoRegistrados.add(Arrays.toString(record));
                        }else if(codigoCertificadoBean.existsCodigo(record[0])){
                            codigosNoRegistrados.add(Arrays.toString(record));
                        } else {
                            try {
                                String codigo = record[0];
                                codigoCertificadoBean.insertCodigo(idPremio, codigo, usuario, locale);
                            } catch (Exception e) {
                                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                            }
                        }
                    }
                    completado += (100 / partitions.size());
                    if (completado < 100) {
                        try {
                            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.PROCESANDO.getValue(), "Procesado " + completado + "% del archivo", completado);
                        } catch (Exception e) {
                            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        }
                    }
                }
                try {
                    trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.COMPLETADO.getValue(), "Codigos no registrados : " + codigosNoRegistrados.stream().collect(Collectors.joining(", ")), Double.valueOf(100));
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                }
            }
        } catch (IOException | ArrayIndexOutOfBoundsException e) {
            
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "file_invalid", Double.valueOf(0));
        }
    }
    
    /**
     * importacion de certificados a premio
     * 
     * @param base64 data
     * @param idCategoriaProducto id de la categoria
     * @param idTrabajo id de trabajo asignado
     * @param usuario id admin
     * @param locale locale
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    @Asynchronous
    public void importarCertificadoCategoriaProducto(String base64, String idCategoriaProducto,
            String idTrabajo, String usuario,
            Locale locale
    ) {
        //ARCHIVO BASE 64
        //verificacion del archivo base64
        if (base64 == null || base64.trim().isEmpty()) {
            try {
                trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "file_invalid", Double.valueOf(0));
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
        }
        //decodificacion del string a bytes y verificacion de que sea un base64 valido
        byte[] fileBytes = null;
        try {
            fileBytes = Base64.getDecoder().decode(base64);
        } catch (IllegalArgumentException e) {
            //no es un base64 valido
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "file_invalid", Double.valueOf(0));
        }
        //Tratamiento del archivo
        ByteArrayInputStream in = new ByteArrayInputStream(fileBytes);
        CSVReader reader = new CSVReader(new InputStreamReader(in), ',');
        List<String> codigosNoRegistrados = new ArrayList<>();
        // read line by line
        try {
            //primera linea es del encabezado, por lo que se ignora
            CategoriaProducto categoria = em.find(CategoriaProducto.class, idCategoriaProducto);
            List<String[]> lista = reader.readAll();
            reader.close();
            lista.remove(0);
            if (lista.isEmpty()) {
                trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "No existe información para insertar", Double.valueOf(0));
            } else if (!lista.isEmpty()) {
                double completado = 0;
                List<List<String[]>> partitions = Lists.partition(lista, 1000);
                for (List<String[]> partition : partitions) {
                    for (String[] record : partition) {
                        if (record[0] == null || record[0].isEmpty()) {
                            codigosNoRegistrados.add(Arrays.toString(record));
                        }else if(certificadoCategoriaBean.existsCertificado(idCategoriaProducto, record[0])){
                            codigosNoRegistrados.add(Arrays.toString(record));
                        } else {
                            try {
                                String numCertificado = record[0];
                                certificadoCategoriaBean.insertarCertificadoCategoria(idCategoriaProducto, numCertificado, usuario, Locale.US);
                            } catch (Exception e) {
                                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                            }
                        }
                    }
                    completado += (100 / partitions.size());
                    if (completado < 100) {
                        try {
                            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.PROCESANDO.getValue(), "Procesado " + completado + "% del archivo", completado);
                        } catch (Exception e) {
                            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        }
                    }
                }
                try {
                    trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.COMPLETADO.getValue(), "Codigos no registrados : " + codigosNoRegistrados.stream().collect(Collectors.joining(", ")), Double.valueOf(100));
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                }
            }
        } catch (IOException | ArrayIndexOutOfBoundsException e) {
            
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "file_invalid", Double.valueOf(0));
        }
    }
    
    /**
     * Trabajo async de la importacion de miembros+atributos dinamicos a partir
     * de un archivo csv y la definicion de atributo por columna del archivo
     * 
     * @param data Datos con la informacion del archivo csv en base64 y la
     * definicion de atributos por columnas
     * @param idTrabajo Identificador del trabajo interno asignado
     * @param usuario Identificador del usuario administrativo detonador del
     * trabajo
     * @param locale Locale de la peticion
     * @param doPostInsertActions Indicador sobre si se realiza acciones post
     * registro (bonus de bienvenida, email de bienvenida con credenciales, etc)
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    @Asynchronous
    public void importarMiembro(DataImportacionMiembroExtendido data, String idTrabajo, String usuario, Locale locale, boolean doPostInsertActions) {
        //ARCHIVO BASE 64
        //verificacion del archivo base64
        if (data.getData() == null || data.getData().trim().isEmpty()) {
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), getMessageError("file_invalid", locale));
            return;
        }
        //decodificacion del string a bytes y verificacion de que sea un base64 valido
        byte[] fileBytes = null;
        try {
            fileBytes = Base64.getDecoder().decode(data.getData());
        } catch (IllegalArgumentException e) {
            //no es un base64 valido
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), getMessageError("file_invalid", locale));
            return;
        }
        
        //variable con la unidad de progreso por fila del csv
        double unidadProgreso;

        //Tratamiento del archivo
        ByteArrayInputStream in = new ByteArrayInputStream(fileBytes);
        List<CSVRecord> records;
        try {
            records = CSVParser.parse(in, Charset.defaultCharset(), CSVFormat.DEFAULT).getRecords();
        } catch (IOException ex) {
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), getMessageError("file_invalid", locale));
            return;
        }
        
        //calculo de la unidad de progreso del trabajo interno
        unidadProgreso = 100d/records.size();

        Stream<Object[]> resultados = records.stream().map((record) -> {
            Object[] resultado = new Object[3];
            resultado[0] = record.getRecordNumber();

            try {
                Miembro miembro = new Miembro();
                Map<String, String> atributosDinamicos = new HashMap<>();
                for (int i = 0; i < record.size(); i++) {
                    String atributo = data.getAtributos().get(i);
                    if (atributo!=null) {
                        AtributosElegiblesImportacionMiembro miembroAtributo = AtributosElegiblesImportacionMiembro.get(atributo);
                        if (miembroAtributo!=null) {
                            switch (miembroAtributo) {
                                case APELLIDO: {
                                    miembro.setApellido(record.get(i));
                                    break;
                                }
                                case APELLIDO2: {
                                    miembro.setApellido2(record.get(i));
                                    break;
                                }
                                case CIUDAD_RESIDENCIA: {
                                    miembro.setCiudadResidencia(record.get(i));
                                    break;
                                }
                                case CODIGO_POSTAL: {
                                    miembro.setCodigoPostal(record.get(i));
                                    break;
                                }
                                case CONTRASENA: {
                                    miembro.setContrasena(record.get(i));
                                    break;
                                }
                                case CORREO: {
                                    miembro.setCorreo(record.get(i));
                                    break;
                                }
                                case DIRECCION: {
                                    miembro.setDireccion(record.get(i));
                                    break;
                                }
                                case DOC_IDENTIFICACION: {
                                    miembro.setDocIdentificacion(record.get(i));
                                    break;
                                }
                                case ESTADO_RESIDENCIA: {
                                    miembro.setEstadoResidencia(record.get(i));
                                    break;
                                }
                                case FECHA_NACIMIENTO: {
                                    Date fecha;
                                    try {
                                        fecha = new Date(Long.parseLong(record.get(i)));
                                    } catch (NumberFormatException ex) {
                                        try {
                                            if (dateFormat==null) {
                                                fecha = new SimpleDateFormat().parse(record.get(i));
                                            } else {
                                                fecha = new SimpleDateFormat(dateFormat).parse(record.get(i));
                                            }
                                        } catch (ParseException ex1) {
                                            throw new IllegalStateException(getMessageError("date_column_invalid", locale, (i+1)));
                                        }
                                    }
                                    miembro.setFechaNacimiento(fecha);
                                    break;
                                }
                                case IND_CONTACTO_EMAIL: {
                                    boolean flag;
                                    switch (record.get(i).toUpperCase()) {
                                        case "TRUE":
                                        case "1": {
                                            flag = true;
                                            break;
                                        }
                                        case "FALSE":
                                        case "0": {
                                            flag = false;
                                            break;
                                        }
                                        default: {
                                            throw new IllegalStateException(getMessageError("boolean_column_invalid", locale, (i+1)));
                                        }
                                    }
                                    miembro.setIndContactoEmail(flag);
                                    break;
                                }
                                case IND_CONTACTO_ESTADO: {
                                    boolean flag;
                                    switch (record.get(i).toUpperCase()) {
                                        case "TRUE":
                                        case "1": {
                                            flag = true;
                                            break;
                                        }
                                        case "FALSE":
                                        case "0": {
                                            flag = false;
                                            break;
                                        }
                                        default: {
                                            throw new IllegalStateException(getMessageError("boolean_column_invalid", locale, (i+1)));
                                        }
                                    }
                                    miembro.setIndContactoEstado(flag);
                                    break;
                                }
                                case IND_CONTACTO_NOTIFICACION: {
                                    boolean flag;
                                    switch (record.get(i).toUpperCase()) {
                                        case "TRUE":
                                        case "1": {
                                            flag = true;
                                            break;
                                        }
                                        case "FALSE":
                                        case "0": {
                                            flag = false;
                                            break;
                                        }
                                        default: {
                                            throw new IllegalStateException(getMessageError("boolean_column_invalid", locale, (i+1)));
                                        }
                                    }
                                    miembro.setIndContactoNotificacion(flag);
                                    break;
                                }
                                case IND_CONTACTO_SMS: {
                                    boolean flag;
                                    switch (record.get(i).toUpperCase()) {
                                        case "TRUE":
                                        case "1": {
                                            flag = true;
                                            break;
                                        }
                                        case "FALSE":
                                        case "0": {
                                            flag = false;
                                            break;
                                        }
                                        default: {
                                            throw new IllegalStateException(getMessageError("boolean_column_invalid", locale, (i+1)));
                                        }
                                    }
                                    miembro.setIndContactoSms(flag);
                                    break;
                                }
                                case IND_EDUCACION: {
                                    Miembro.ValoresIndEducacion indEducacion = Miembro.ValoresIndEducacion.get(record.get(i).length()!=1?null:record.get(i).charAt(0));
                                    if (indEducacion==null) {
                                        throw new IllegalStateException(getMessageError("indicator_column_invalid", locale, (i+1)));
                                    }
                                    miembro.setIndEducacion(indEducacion.getValue());
                                    break;
                                }
                                case IND_ESTADO_CIVIL: {
                                    Miembro.ValoresIndEstadoCivil indEstadoCivil = Miembro.ValoresIndEstadoCivil.get(record.get(i).length()!=1?null:record.get(i).charAt(0));
                                    if (indEstadoCivil==null) {
                                        throw new IllegalStateException(getMessageError("indicator_column_invalid", locale, (i+1)));
                                    }
                                    miembro.setIndEstadoCivil(indEstadoCivil.getValue());
                                    break;
                                }
                                case IND_GENERO: {
                                    Miembro.ValoresIndGenero indGenero = Miembro.ValoresIndGenero.get(record.get(i).length()!=1?null:record.get(i).charAt(0));
                                    if (indGenero==null) {
                                        throw new IllegalStateException(getMessageError("indicator_column_invalid", locale, (i+1)));
                                    }
                                    miembro.setIndGenero(indGenero.getValue());
                                    break;
                                }
                                case IND_HIJOS: {
                                    boolean flag;
                                    switch (record.get(i).toUpperCase()) {
                                        case "TRUE":
                                        case "1": {
                                            flag = true;
                                            break;
                                        }
                                        case "FALSE":
                                        case "0": {
                                            flag = false;
                                            break;
                                        }
                                        default: {
                                            throw new IllegalStateException(getMessageError("boolean_column_invalid", locale, (i+1)));
                                        }
                                    }
                                    miembro.setIndHijos(flag);
                                    break;
                                }
                                case INGRESO_ECONOMICO: {
                                    double valor;
                                    try {
                                        valor = Double.parseDouble(record.get(i));
                                    } catch (NumberFormatException ex) {
                                        throw new IllegalStateException(getMessageError("number_column_invalid", locale, (i+1)));
                                    }
                                    miembro.setIngresoEconomico(valor);
                                    break;
                                }
                                case NOMBRE: {
                                    miembro.setNombre(record.get(i));
                                    break;
                                }
                                case PAIS_RESIDENCIA: {
                                    String codPais;
                                    try {
                                        codPais = ((Pais) em.createNamedQuery("Pais.findByAlfa2").setParameter("alfa2", record.get(i)).getSingleResult()).getAlfa3();
                                    } catch (NoResultException | NonUniqueResultException ex) {
                                        try {
                                            codPais = ((Pais) em.createNamedQuery("Pais.findByAlfa3").setParameter("alfa3", record.get(i)).getSingleResult()).getAlfa3();
                                        } catch (NoResultException | NonUniqueResultException ex1) {
                                            try {
                                                codPais = ((Pais) em.createNamedQuery("Pais.findByNombrePais").setParameter("nombrePais", record.get(i)).getSingleResult()).getAlfa3();
                                            } catch (NoResultException | NonUniqueResultException ex2) {
                                                throw new IllegalStateException(getMessageError("indicator_column_invalid", locale, (i+1)));
                                            }
                                        }
                                    }
                                    miembro.setPaisResidencia(codPais);
                                    break;
                                }
                                case TELEFONO_MOVIL: {
                                    miembro.setTelefonoMovil(record.get(i));
                                    break;
                                }
                                case AVATAR: {
                                    miembro.setAvatar(record.get(i));
                                    break;
                                }
                                case COMENTARIOS: {
                                    miembro.setComentarios(record.get(i));
                                    break;
                                }
                                case FECHA_EXPIRACION: {
                                    Date fecha;
                                    try {
                                        fecha = new Date(Long.parseLong(record.get(i)));
                                    } catch (NumberFormatException ex) {
                                        try {
                                            if (dateFormat==null) {
                                                fecha = new SimpleDateFormat().parse(record.get(i));
                                            } else {
                                                fecha = new SimpleDateFormat(dateFormat).parse(record.get(i));
                                            }
                                        } catch (ParseException ex1) {
                                            throw new IllegalStateException(getMessageError("date_column_invalid", locale, (i+1)));
                                        }
                                    }
                                    miembro.setFechaExpiracion(fecha);
                                    break;
                                }
                                case FECHA_INGRESO: {
                                    Date fecha;
                                    try {
                                        fecha = new Date(Long.parseLong(record.get(i)));
                                    } catch (NumberFormatException ex) {
                                        try {
                                            if (dateFormat==null) {
                                                fecha = new SimpleDateFormat().parse(record.get(i));
                                            } else {
                                                fecha = new SimpleDateFormat(dateFormat).parse(record.get(i));
                                            }
                                        } catch (ParseException ex1) {
                                            throw new IllegalStateException(getMessageError("date_column_invalid", locale, (i+1)));
                                        }
                                    }
                                    miembro.setFechaIngreso(fecha);
                                    break;
                                }
                                case IND_ESTADO_MIEMBRO: {
                                    Miembro.Estados indEstadoMiembro = Miembro.Estados.get(record.get(i).length()!=1?null:record.get(i).charAt(0));
                                    if (indEstadoMiembro==null) {
                                        throw new IllegalStateException(getMessageError("indicator_column_invalid", locale, (i+1)));
                                    }
                                    miembro.setIndEstadoMiembro(indEstadoMiembro.getValue());
                                    break;
                                }
                                case IND_ESTADO_SISTEMA: {
                                    boolean flag;
                                    switch (record.get(i).toUpperCase()) {
                                        case "TRUE":
                                        case "1": {
                                            flag = true;
                                            break;
                                        }
                                        case "FALSE":
                                        case "0": {
                                            flag = false;
                                            break;
                                        }
                                        default: {
                                            throw new IllegalStateException(getMessageError("boolean_column_invalid", locale, (i+1)));
                                        }
                                    }
                                    miembro.setIndMiembroSistema(flag);
                                    break;
                                }
                                case NOMBRE_USUARIO: {
                                    miembro.setNombreUsuario(record.get(i));
                                    break;
                                }
                            }
                        } else {
                            throw new IllegalStateException(getMessageError("attribute_not_found", locale, atributo));
                        }
                    } else {
                        atributo = data.getAtributosDinamicos().get(i);
                        if (atributo == null) {
                            throw new IllegalStateException(getMessageError("column_undefined", locale, (i+1)));
                        }
                        
                        AtributoDinamico atributoDinamico = em.find(AtributoDinamico.class, atributo);
                        if (atributoDinamico==null) {
                            throw new IllegalStateException(getMessageError("dynamic_attribute_not_found", locale, atributo));
                        }

                        switch(AtributoDinamico.TiposDato.get(atributoDinamico.getIndTipoDato())) {
                            case BOOLEANO: {
                                boolean flag;
                                switch (record.get(i).toUpperCase()) {
                                    case "TRUE":
                                    case "1": {
                                        flag = true;
                                        break;
                                    }
                                    case "FALSE":
                                    case "0": {
                                        flag = false;
                                        break;
                                    }
                                    default: {
                                        throw new IllegalStateException(getMessageError("boolean_column_invalid", locale, (i+1)));
                                    }
                                }
                                atributosDinamicos.put(atributoDinamico.getIdAtributo(), Boolean.toString(flag));
                                break;
                            }
                            case FECHA: {
                                Date fecha;
                                try {
                                    fecha = new Date(Long.parseLong(record.get(i)));
                                } catch (NumberFormatException ex) {
                                    try {
                                        if (dateFormat==null) {
                                            fecha = new SimpleDateFormat().parse(record.get(i));
                                        } else {
                                            fecha = new SimpleDateFormat(dateFormat).parse(record.get(i));
                                        }
                                    } catch (ParseException ex1) {
                                        throw new IllegalStateException(getMessageError("date_column_invalid", locale, (i+1)));
                                    }
                                }
                                atributosDinamicos.put(atributoDinamico.getIdAtributo(), String.valueOf(fecha.getTime()));
                                break;
                            }
                            case NUMERICO: {
                                BigDecimal valor;
                                try {
                                    valor = new BigDecimal(record.get(i));
                                } catch (NumberFormatException ex) {
                                    throw new IllegalStateException(getMessageError("number_column_invalid", locale, (i+1)));
                                }
                                atributosDinamicos.put(atributoDinamico.getIdAtributo(), valor.toPlainString());
                                break;
                            }
                            case TEXTO: {
                                atributosDinamicos.put(atributoDinamico.getIdAtributo(), record.get(i));
                            }
                        }
                    }
                }

                //insertar
                try {
                    String idMiembro = miembroBean.insertMiembroExtendido(miembro, atributosDinamicos, locale);
                    resultado[1] = true;
                    resultado[2] = idMiembro;
                    
                    if (doPostInsertActions) {
                        miembroBean.postRegistroMiembro(miembro, locale);
                    }
                } catch (MyException ex) {
                    resultado[1] = false;
                    resultado[2] = ((MyExceptionResponseBody) ex.getResponse().getEntity()).getMessage();
                }

                trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.PROCESANDO.getValue(), null, unidadProgreso*record.getRecordNumber());
            } catch (Exception ex) {
                resultado[1] = false;
                resultado[2] = ex.getMessage();
            }

            return resultado;
        });
            
        JsonArrayBuilder builder = Json.createArrayBuilder();
        resultados.forEach((t) -> builder.add(Json.createObjectBuilder().add("row", (long) t[0]).add("inserted", (boolean) t[1]).add("idMemberOrError", (String) t[2])));
        
        trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.COMPLETADO.getValue(), builder.build().toString(), 100d);
    }
    
    //bundle con msjs de error de importacion
    private static final String BUNDLE_NAME = "messages.import_error";
    
    /**
     * metodo para la obtencion del msj de error segun un key
     * 
     * @param key llave del msj
     * @param locale locale
     * @param params parametros del msj
     * @return texto final
     */
    private static String getMessageError(String key, Locale locale, Object... params) {
        try {
            if (params.length>0) {
                return MessageFormat.format(ResourceBundle.getBundle(BUNDLE_NAME, locale).getString(key), params);
            } else {
                return ResourceBundle.getBundle(BUNDLE_NAME, locale).getString(key);
            }
        } catch (MissingResourceException | IllegalArgumentException e) {
            if (!locale.getLanguage().equals(Locale.US.getLanguage())) {
                return getMessageError(key, Locale.US, params);
            }
            return '!'+key+'!';
        }
    }
}
