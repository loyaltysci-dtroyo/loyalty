package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.ReglasSegmento;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.KeyOnlyFilter;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * EJB encargado del manejo de subrecursos para el manejo de segmentos
 *
 * @author svargas
 */
@Stateless
public class ElegibilidadMiembroBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    private final Configuration configuration;
    
    public ElegibilidadMiembroBean() {
        this.configuration = new Configuration();
        this.configuration.addResource("conf/hbase/hbase-site.xml");
    }
    
    /**
     * Metodo que obtiene la lista de segmentos donde un miembro forma parte de
     * sus eligibles
     *
     * @param idMiembro Identificador de miembro
     * @param registro Numero de registro inicial
     * @param cantidad Cantidad de registros en la respuesta
     * @param locale
     * @return Respuesta con el listado de segmentos en un rango valido
     */
    public Map<String, Object> getSegmentosPorMiembroEligible(String idMiembro, int registro, int cantidad,Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        if (registro < 0 || cantidad < 0) {
           throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        Map<String, Object> respuesta = new HashMap<>();

        List<Segmento> resultado;
        long total;
        
        List<String> idsSegmentos;
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "ELEGIBILIDAD_MIEMBRO"));

            //se obtiene la fila (solo la columna familia de segmentos) con el rowkey igual al valor del identificador del miembro
            Get get = new Get(Bytes.toBytes(idMiembro));
            get.addFamily(Bytes.toBytes("SEGMENTOS"));
            Result result = table.get(get);

            //en caso de que no se haya encontrado alguna fila...
            if (result.isEmpty()) {
                idsSegmentos = new ArrayList<>();
            } else {
                //por cada entrada de segmento (columna & valor), se filtran los que indiquen que el miembro pertenece y se mapea al texto de los identificadores de segmento
                idsSegmentos = result.getFamilyMap(Bytes.toBytes("SEGMENTOS"))
                        .entrySet()
                        .stream()
                        .filter((t) -> Bytes.toBoolean(t.getValue()))
                        .map((t) -> Bytes.toString(t.getKey()))
                        .collect(Collectors.toList());
            }
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
        
        if (idsSegmentos.isEmpty()) {
            resultado = new ArrayList<>();
            total = 0;
            registro = 0;
        } else {
            total = idsSegmentos.size();
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            List<String> idsRequeridos = idsSegmentos.subList(registro, registro + cantidad > idsSegmentos.size() ? idsSegmentos.size() : registro + cantidad);

            if (idsRequeridos.isEmpty()) {
                resultado = new ArrayList();
            } else {
                resultado = em.createNamedQuery("Segmento.findByIdSegmentoInLista").setParameter("lista", idsRequeridos).getResultList();
            }
        }
        
        respuesta.put("resultado", resultado);
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);

        return respuesta;
    }
    
    /**
     * borrado de datos sobre resultados de elegibilidad de una regla en hbase
     * @param tipoRegla indicador del tipod de regla
     * @param idRegla identificador de la regla
     * @param locale locale
     */
    public void deleteResultadoElegibilidadRegla(ReglasSegmento.TiposReglasSegmento tipoRegla, String idRegla, Locale locale) {
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "ELEGIBILIDAD_MIEMBRO"));
            
            Scan scan = new Scan();
            FilterList filterList = new FilterList(
                    new QualifierFilter(CompareFilter.CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(tipoRegla.getNombreColumna(idRegla)))),
                    new KeyOnlyFilter()
            );
            scan.setFilter(filterList);
            
            try (ResultScanner scanner = table.getScanner(scan)) {
                List<Delete> deletes = new ArrayList<>();
                for (Result result : scanner) {
                    deletes.add(new Delete(result.getRow()).addColumn(Bytes.toBytes("REGLAS"), Bytes.toBytes(tipoRegla.getNombreColumna(idRegla))));
                }
                table.delete(deletes);
            }
        } catch (IOException ex) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
    }
}
