/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "HISTORICO_MOVIMIENTOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistoricoMovimientos.findAll", query = "SELECT h FROM HistoricoMovimientos h"),
    @NamedQuery(name = "HistoricoMovimientos.findByIdHistorico", query = "SELECT h FROM HistoricoMovimientos h WHERE h.idHistorico = :idHistorico"),
    @NamedQuery(name = "HistoricoMovimientos.findByCodPremio", query = "SELECT h FROM HistoricoMovimientos h WHERE h.codPremio = :codPremio"),
    @NamedQuery(name = "HistoricoMovimientos.findByIdMiembro", query = "SELECT h FROM HistoricoMovimientos h WHERE h.idMiembro = :idMiembro"),
    @NamedQuery(name = "HistoricoMovimientos.findByCantidad", query = "SELECT h FROM HistoricoMovimientos h WHERE h.cantidad = :cantidad"),
    @NamedQuery(name = "HistoricoMovimientos.findByTipoMovimiento", query = "SELECT h FROM HistoricoMovimientos h WHERE h.tipoMovimiento = :tipoMovimiento"),
    @NamedQuery(name = "HistoricoMovimientos.findByFecha", query = "SELECT h FROM HistoricoMovimientos h WHERE h.fecha = :fecha"),
    @NamedQuery(name = "HistoricoMovimientos.findByUbicacionOrigen", query = "SELECT h FROM HistoricoMovimientos h WHERE h.ubicacionOrigen = :ubicacionOrigen"),
    @NamedQuery(name = "HistoricoMovimientos.findByUbicacionDestino", query = "SELECT h FROM HistoricoMovimientos h WHERE h.ubicacionDestino = :ubicacionDestino")
})
public class HistoricoMovimientos implements Serializable {

    @Column(name = "PRECIO_PROMEDIO")
    private Double precioPromedio;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO")
    @ManyToOne
    private Miembro idMiembro;
    
    @JoinColumn(name = "COD_PREMIO", referencedColumnName = "ID_PREMIO")
    @ManyToOne(optional = false)
    private Premio codPremio;
    
    @JoinColumn(name = "UBICACION_ORIGEN", referencedColumnName = "ID_UBICACION")
    @ManyToOne(optional = false)
    private Ubicacion ubicacionOrigen;
    
    @JoinColumn(name = "UBICACION_DESTINO", referencedColumnName = "ID_UBICACION")
    @ManyToOne
    private Ubicacion ubicacionDestino;

    public enum Tipos{
        REDENCION_AUTO('R'),
        COMPRA('C'),
        AJUSTE_MAS('A'),
        AJUSTE_MENOS('M'),
        INTER_UBICACION('I');
        
        public final char value;
        public static final Map<Character,Tipos> lookup= new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }
        
        static{
            for(Tipos tipo : values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipos get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Status{
        REDIMIDO_SIN_RETIRAR('S'),
        REDIMIDO_RETIRADO('R'),
        ANULADO('A');
        
        public final char value;
        public static final Map<Character,Status> lookup= new HashMap<>();

        private Status(char value) {
            this.value = value;
        }
        
        static{
            for(Status tipo : values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Status get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "historico_uuid")
    @GenericGenerator(name = "historico_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_HISTORICO")
    private String idHistorico;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANTIDAD")
    private Long cantidad;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO_MOVIMIENTO")
    private Character tipoMovimiento;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @Column(name = "STATUS")
    private Character status;
    
    @JoinColumn(name = "NUM_DOCUMENTO", referencedColumnName = "NUM_DOCUMENTO")
    @ManyToOne(optional = false)
    private DocumentoInventario numDocumento;
    
    @Column(name = "ID_PREMIO_MIEMBRO")
    private String idPremioMiembro;
    

    public HistoricoMovimientos() {
    }

    public HistoricoMovimientos(Double precioPromedio, Miembro idMiembro, Premio codPremio, Ubicacion ubicacionOrigen, Ubicacion ubicacionDestino, Long cantidad, Character tipoMovimiento, Date fecha, Character status, DocumentoInventario numDocumento, String premioMiembro) {
        this.precioPromedio = precioPromedio;
        this.idMiembro = idMiembro;
        this.codPremio = codPremio;
        this.ubicacionOrigen = ubicacionOrigen;
        this.ubicacionDestino = ubicacionDestino;
        this.cantidad = cantidad;
        this.tipoMovimiento = tipoMovimiento;
        this.fecha = fecha;
        this.status = status;
        this.numDocumento = numDocumento;
        this.idPremioMiembro = premioMiembro;
    }
    
    public HistoricoMovimientos(Double precioPromedio, Miembro idMiembro, Premio codPremio, Ubicacion ubicacionOrigen, Ubicacion ubicacionDestino, Long cantidad, Character tipoMovimiento, Date fecha, Character status,String idPremioMiembro) {
        this.precioPromedio = precioPromedio;
        this.idMiembro = idMiembro;
        this.codPremio = codPremio;
        this.ubicacionOrigen = ubicacionOrigen;
        this.ubicacionDestino = ubicacionDestino;
        this.cantidad = cantidad;
        this.tipoMovimiento = tipoMovimiento;
        this.fecha = fecha;
        this.status = status;
        this.idPremioMiembro = idPremioMiembro;
    }
    
    public HistoricoMovimientos(String idHistorico) {
        this.idHistorico = idHistorico;
    }

    
    public String getIdHistorico() {
        return idHistorico;
    }

    public void setIdHistorico(String idHistorico) {
        this.idHistorico = idHistorico;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public Character getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(Character tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

   
    public DocumentoInventario getNumDocumento() {
        return numDocumento;
    }

    public void setNumDocumento(DocumentoInventario numDocumento) {
        this.numDocumento = numDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHistorico != null ? idHistorico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoricoMovimientos)) {
            return false;
        }
        HistoricoMovimientos other = (HistoricoMovimientos) object;
        if ((this.idHistorico == null && other.idHistorico != null) || (this.idHistorico != null && !this.idHistorico.equals(other.idHistorico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.HistoricoMovimientos[ idHistorico=" + idHistorico + " ]";
    }

    public Double getPrecioPromedio() {
        return precioPromedio;
    }

    public void setPrecioPromedio(Double precioPromedio) {
        this.precioPromedio = precioPromedio;
    }

    public Miembro getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(Miembro idMiembro) {
        this.idMiembro = idMiembro;
    }

    public Premio getCodPremio() {
        return codPremio;
    }

    public void setCodPremio(Premio codPremio) {
        this.codPremio = codPremio;
    }

    public Ubicacion getUbicacionOrigen() {
        return ubicacionOrigen;
    }

    public void setUbicacionOrigen(Ubicacion ubicacionOrigen) {
        this.ubicacionOrigen = ubicacionOrigen;
    }

    public Ubicacion getUbicacionDestino() {
        return ubicacionDestino;
    }

    public void setUbicacionDestino(Ubicacion ubicacionDestino) {
        this.ubicacionDestino = ubicacionDestino;
    }

    public String getIdPremioMiembro() {
        return idPremioMiembro;
    }

    public void setIdPremioMiembro(String idPremioMiembro) {
        this.idPremioMiembro = idPremioMiembro;
    }
    
    
    
}
