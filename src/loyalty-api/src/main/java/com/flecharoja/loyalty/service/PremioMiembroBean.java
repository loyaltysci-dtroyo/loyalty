/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioMiembro;

import com.flecharoja.loyalty.util.Indicadores;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author wtencio
 */
@Stateless
public class PremioMiembroBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Método que devuelve una lista de premios redimidos para el miembro
     *
     * @param idMiembro identificador del miembro
     * @param cantidad de registros maximos por pagina
     * @param registro por el cual se comienza la busqueda
     * @param locale
     * @return lista de premios resultantes
     */
    public Map<String, Object> getPremiosRedimidos(String idMiembro, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        long total;
        List<Premio> premios = new ArrayList<>();
        List<PremioMiembro> premioMiembros = new ArrayList<>();

        try {
            total = (Long) em.createNamedQuery("PremioMiembro.countByIdMiembro").setParameter("idMiembro", idMiembro)
                    .setParameter("estado", PremioMiembro.Estados.REDIMIDO_RETIRADO.getValue()).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;

            }

            premioMiembros = em.createNamedQuery("PremioMiembro.findByIdMiembroAndEstado")
                    .setParameter("idMiembro", idMiembro)
                    .setParameter("estado", PremioMiembro.Estados.REDIMIDO_RETIRADO.getValue())
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();

            premios = premioMiembros.stream().filter((p) -> p.getIdMiembro().getIdMiembro().equals(idMiembro))
                    .map((p) -> {
                        Premio premio = em.find(Premio.class, p.getIdPremio().getIdPremio());
                        premio.setCodigoCertificado(p.getCodigoCertificado());
                        premio.setFechaRedencion(p.getFechaAsignacion());
                        return premio;
                    }).collect(Collectors.toList());
             

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        respuesta.put("resultado", premios);
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        return respuesta;
    }

}
