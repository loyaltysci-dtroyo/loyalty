package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.CategoriaNoticia;
import com.flecharoja.loyalty.model.Noticia;
import com.flecharoja.loyalty.service.CategoriaNoticiaBean;
import com.flecharoja.loyalty.service.NoticiaCategoriaNoticiaBean;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase de recursos http disponibles para el manejo de las categorias de
 * noticias y la asignacion de noticias a categorias.
 *
 * @author svargas
 */
@Api(value = "Categoria de Noticias")
@Path("categoria-noticia")
public class CategoriaNoticiaResource {

    @EJB
    CategoriaNoticiaBean categoriaBean;

    @EJB
    NoticiaCategoriaNoticiaBean noticiaCategoriaBean;

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión
    
    @Context
    HttpServletRequest request;

    /**
     * Metodo que obtiene un listado de categorias de noticias por un rango
     * establecido usando los parametros de cantidad y registro bajo un estado
     * de OK(200) ademas de encabezados indicando del rango aceptable maximo y
     * el rango actual de resultados, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param busqueda
     * @param ordenTipo
     * @param ordenCampo
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta OK con el listado de categorias de noticia y los
     * encabezados de rango
     */
    @ApiOperation(value = "Obtener categorias de noticias",
            responseContainer = "List",
            response = CategoriaNoticia.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategoriasNoticias(
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = categoriaBean.getCategoriasNoticias(busqueda, ordenTipo, ordenCampo, registro, cantidad, request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }

    /**
     * Metodo que obtiene la informacion de una categoria de noticia segun su
     * identificador bajo un estado de OK(200), de ocurrir un error se retornara
     * una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param idCategoria Identificador de la categoria de noticia deseada
     * @return Respuesta OK con la informacion de la categoria de noticia
     * encontrada
     */
    @ApiOperation(value = "Obtener una categoria de noticia por identificador",
            response = CategoriaNoticia.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("{idCategoria}")
    @Produces(MediaType.APPLICATION_JSON)
    public CategoriaNoticia getCategoriaNoticia(
            @ApiParam(value = "Identificador de la categoria de noticia", required = true)
            @PathParam("idCategoria") String idCategoria) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return categoriaBean.getCategoriaNoticia(idCategoria,  request.getLocale());
    }

    /**
     * Metodo que registra la informacion de una nueva categoria de noticia y
     * retorna el identificador creado de la misma bajo un estado de OK(200).
     *
     * @param categoria Objeto con la informacion necesaria de una categoria de
     * noticia para registrar
     * @return Respuesta OK con el identificador de la categoria de noticia
     * creada
     */
    @ApiOperation(value = "Registrar categorias de promocion",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertCategoriaNoticia(
            @ApiParam(value = "Informacion de la categoria de noticia", required = true) CategoriaNoticia categoria) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return categoriaBean.insertCategoriaNoticia(categoria, usuarioContext,  request.getLocale());
    }

    /**
     * Metodo que modifica la informacion de una categoria de noticia
     * existente.
     *
     * @param categoria Objeto con la informacion necesaria de una categoria de
     * noticia para registrar
     */
    @ApiOperation(value = "Modificacion de categoria de noticia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void editCategoriaNoticia(
            @ApiParam(value = "Informacion de una categoria de noticia", required = true) CategoriaNoticia categoria) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        categoriaBean.editCategoriaNoticia(categoria, usuarioContext,  request.getLocale());
    }

    /**
     * Metodo que elimina un registro de categoria de noticia existente.
     *
     * @param idCategoria Identificador de la categoria de noticia deseada
     */
    @ApiOperation(value = "Eliminacion de categoria de noticia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @DELETE
    @Path("{idCategoria}")
    public void removeCategoriaNoticia(
            @ApiParam(value = "Identificador de una noticia", required = true)
            @PathParam("idCategoria") String idCategoria) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        categoriaBean.removeCategoriaNoticia(idCategoria, usuarioContext,  request.getLocale());
    }

    /**
     * Metodo que obtiene un listado de noticias asignadas a una categoria de
     * promocion segun el identificador de la categoria.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param indAccion
     * @param busqueda
     * @param filtros
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param idCategoria Identificador de la categoria de noticia
     * @return Respuesta OK con el listado de noticias de una categoria
     */
    @ApiOperation(value = "Obtencion de noticias de una categoria de noticia",
            responseContainer = "List",
            response = Noticia.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("{idCategoria}/noticia")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNoticias(
            @ApiParam(value = "Identificador de categoria de noticia", required = true) @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Indicador sobre el tipo de lista por categoria") @QueryParam("ind-accion-lista") String indAccion,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Filtros sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = noticiaCategoriaBean.getNoticiasPorIdCategoria(idCategoria, indAccion, null, busqueda, filtros, cantidad, registro, Busquedas.TiposOrden.DESCENDIENTE.getValue(), Busquedas.CamposOrden.FECHA_MODIFICACION.getValue(), request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }

    /**
     * Metodo que asigna una lista de identificadores de noticia a una
     * categoria de noticia por su identificador.
     *
     * @param idCategoria Identificador de la categoria de noticia
     * @param listaIdNoticia Lista de identificadores de noticia
     */
    @ApiOperation(value = "Asignacion de listas de noticias a categoria de promocion")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{idCategoria}/noticia")
    public void assignBatchCategoriaNoticia(
            @ApiParam(value = "Identificador de categoria de noticia", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Lista de identificadores de noticias", required = true)
            List<String> listaIdNoticia) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        noticiaCategoriaBean.assignBatchCategoriaMision(idCategoria, listaIdNoticia, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que asigna una lista de identificadores de noticia a una
     * categoria de noticia por su identificador.
     *
     * @param idCategoria Identificador de la categoria de noticia
     * @param listaIdNoticia Lista de identificadores de noticia
     */
    @ApiOperation(value = "Eliminacion de la asignacion de una lista de noticias a categoria de noticia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{idCategoria}/noticia/remover-lista")
    public void unassignBatchCategoriaNoticia(
            @ApiParam(value = "Identificador de categoria de noticia", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Lista de identificadores de noticia", required = true)
            List<String> listaIdNoticia) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        noticiaCategoriaBean.unassignBatchCategoriaMision(idCategoria, listaIdNoticia, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que asigna una noticia a una categoria de noticia usando sus
     * identificadores.
     *
     * @param idCategoria Identificador de la categoria de noticia
     * @param idNoticia Identificador de la noticia
     */
    @ApiOperation(value = "Asignacion de una noticia con una categoria de noticia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("{idCategoria}/noticia/{idNoticia}")
    public void assignCategoriaNoticia(
            @ApiParam(value = "Identificador de categoria de noticia", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Identificador de noticia", required = true)
            @PathParam("idNoticia") String idNoticia) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        noticiaCategoriaBean.assignCategoriaNoticia(idCategoria, idNoticia, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que remueve la asignacion de una noticia a una categoria de
     * noticia usando sus identificadores.
     *
     * @param idCategoria Identificador de la categoria de noticia
     * @param idNoticia Identificador de la noticia
     */
    @ApiOperation(value = "Eliminacion de la asignacion de una noticia con una categoria de noticia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @DELETE
    @Path("{idCategoria}/noticia/{idNoticia}")
    public void unassignCategoriaNoticia(
            @ApiParam(value = "Identificador de categoria de noticia", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Identificador de noticia", required = true)
            @PathParam("idNoticia") String idNoticia) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        noticiaCategoriaBean.unassignCategoriaNoticia(idCategoria, idNoticia, usuarioContext, request.getLocale());
    }

}
