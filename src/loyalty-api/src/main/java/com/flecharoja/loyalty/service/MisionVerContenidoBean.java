package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionVerContenido;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado del manejo de datos para el mantenimiento de misiones de tipo
 * ver contenido
 *
 * @author svargas
 */
@Stateless
public class MisionVerContenidoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    @EJB
    UtilBean utilBean;

    /**
     * Metodo que obtiene la informacion de ver contenido de una mision
     *
     * @param idMision Identificador de mision
     * @param locale
     * @return Informacion de la definicion de ver contenido
     */
    public MisionVerContenido getVerContenidoMision(String idMision,Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.VER_CONTENIDO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        MisionVerContenido verContenido = em.find(MisionVerContenido.class, idMision);
        if (verContenido == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_view_content_not_found");
        }

        return verContenido;
    }

    /**
     * Metodo que registra/edita la definicion de ver contenido de una mision
     *
     * @param idMision Identificador de la mision
     * @param verContenido Informacion de ver contenido
     * @param idUsuario Identificador del usuario creador/modificador
     * @param locale
     */
    public void createEditVerContenidoMision(String idMision, MisionVerContenido verContenido, String idUsuario,Locale locale) {
        if (verContenido==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "MisionVerContenido");
        }
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.VER_CONTENIDO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        MisionVerContenido original = em.find(MisionVerContenido.class, idMision);

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }

        Date fecha = new Date();
        if (original == null) {
            verContenido.setFechaCreacion(fecha);
            verContenido.setUsuarioCreacion(idUsuario);
            verContenido.setNumVersion(new Long(1));
        }
        verContenido.setIdMision(idMision);
        verContenido.setFechaModificacion(fecha);
        verContenido.setUsuarioModificacion(idUsuario);

        String urlImagenOriginal = original != null ? original.getUrl() : Indicadores.URL_IMAGEN_PREDETERMINADA;
        String urlImagenNueva = null;
        MisionVerContenido.Tipos tipoContenido = MisionVerContenido.Tipos.get(verContenido.getIndTipo());
        if (tipoContenido==null) {
             throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indTipo");
        }
        switch (tipoContenido) {
            case IMAGEN: {
                if (verContenido.getUrl() == null || verContenido.getUrl().trim().isEmpty()) {
                    verContenido.setUrl(Indicadores.URL_IMAGEN_PREDETERMINADA);
                } else if (!urlImagenOriginal.equals(verContenido.getUrl())) {
                    //si no, se le intenta subir
                    try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                        urlImagenNueva = filesUtils.uploadImage(verContenido.getUrl(), MyAwsS3Utils.Folder.IMAGEN_MISION_VER_CONTENIDO, null);
                        verContenido.setUrl(urlImagenNueva);
                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                        throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                    }
                }
                break;
            }
            case URL:
            case VIDEO: {
                try {
                    new URL(verContenido.getUrl()).toURI();
                } catch (MalformedURLException | URISyntaxException e) {
                     throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "url");
                }
                break;
            }
        }

        try {
            em.merge(verContenido);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//en caso de que la entidad no sea valida
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
        
        if (urlImagenNueva!=null && urlImagenOriginal!=null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }

        bitacoraBean.logAccion(MisionVerContenido.class, idMision, idUsuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * Metodo que elimina la informacion de ver contenido de una mision
     *
     * @param idMision Identificador de la mision
     * @param idUsuario Identificador del usuario modificador
     * @param locale
     */
    public void removeVerContenidoMision(String idMision, String idUsuario,Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        MisionVerContenido verContenido = em.find(MisionVerContenido.class, idMision);
        if (mision == null) {
           throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (verContenido==null) {
            return;
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.VER_CONTENIDO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }
        
        String url = null;
        if (MisionVerContenido.Tipos.get(verContenido.getIndTipo())==MisionVerContenido.Tipos.IMAGEN) {
            url = verContenido.getUrl();
        }

        try {
            em.remove(verContenido);
            em.flush();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_view_content_not_found");
        }
        
        if (url!=null && !url.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(url);
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
            }
        }

        bitacoraBean.logAccion(MisionVerContenido.class, idMision, idUsuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }
}
