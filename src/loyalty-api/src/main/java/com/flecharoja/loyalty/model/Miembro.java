package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio, svargas
 */
@Entity
@Table(name = "MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Miembro.findAllIdMiembro", query = "SELECT m.idMiembro FROM Miembro m"),
    @NamedQuery(name = "Miembro.getNombreApellidosByIdMiembro", query = "SELECT m.nombre, m.apellido, m.apellido2 FROM Miembro m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "Miembro.findAll", query = "SELECT m FROM Miembro m ORDER BY m.fechaCreacion DESC"),
    @NamedQuery(name = "Miembro.countIdentificador", query = "SELECT COUNT(m.docIdentificacion) FROM Miembro m WHERE m.docIdentificacion = :docIdentificacion"),
    @NamedQuery(name = "Miembro.countCorreo", query = "SELECT COUNT(m.correo) FROM Miembro m WHERE m.correo = :correo"),
    @NamedQuery(name = "Miembro.findIdentificador", query = "SELECT m.idMiembro FROM Miembro m WHERE m.docIdentificacion = :docIdentificacion"),
    @NamedQuery(name = "Miembro.findMiembroByDocIdentificacion", query = "SELECT m FROM Miembro m WHERE m.docIdentificacion = :docIdentificacion"),
    @NamedQuery(name = "Miembro.findMiembroByCorreo", query = "SELECT m FROM Miembro m WHERE m.correo = :correo"),
    @NamedQuery(name = "Miembro.countGenero", query = "SELECT COUNT(m) FROM Miembro m WHERE m.indGenero = :indGenero"),
    @NamedQuery(name = "Miembro.countGeneroEstado", query = "SELECT COUNT(m) FROM Miembro m WHERE m.indGenero = :indGenero AND m.indEstadoMiembro = :indEstado"),
    @NamedQuery(name = "Miembro.getAvatarFromMiembro", query = "SELECT m.avatar FROM Miembro m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "Miembro.findAllActivos", query = "SELECT m FROM Miembro m WHERE m.indEstadoMiembro = 'A' ORDER BY m.fechaCreacion DESC"),
    @NamedQuery(name = "Miembro.countAll", query = "SELECT COUNT(m.idMiembro) FROM Miembro m"),
    @NamedQuery(name = "Miembro.countAllByIndEstadoMiembro", query = "SELECT COUNT(m.idMiembro) FROM Miembro m WHERE m.indEstadoMiembro = :indEstadoMiembro"),
    @NamedQuery(name = "Miembro.countAllActivos", query = "SELECT COUNT(m.idMiembro) FROM Miembro m WHERE m.indEstadoMiembro = 'A' "),
    @NamedQuery(name = "Miembro.findAllByIdMiembro", query = "SELECT m FROM Miembro m WHERE m.idMiembro IN :lista ORDER BY m.fechaCreacion DESC"),
    @NamedQuery(name = "Miembro.findByIdMiembro", query = "SELECT m FROM Miembro m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "Miembro.findByIndEstadoMiembro", query = "SELECT m FROM Miembro m WHERE m.indEstadoMiembro = :indEstadoMiembro ORDER BY m.fechaCreacion DESC"),
    @NamedQuery(name = "Miembro.countMiembroReferente", query = "SELECT COUNT(m) FROM Miembro m WHERE m.miembroReferente.idMiembro = :idMiembro"),
    @NamedQuery(name = "Miembro.findMiembroReferente", query = "SELECT m FROM Miembro m WHERE m.miembroReferente.idMiembro = :idMiembro"),
    @NamedQuery(name = "Miembro.countMiembroEstado", query = "SELECT COUNT(m) FROM Miembro m WHERE m.indEstadoMiembro = :indEstado"),
    @NamedQuery(name = "Miembro.countMiembrosNuevos", query = "SELECT COUNT(m) FROM Miembro m WHERE m.fechaIngreso > :fecha")
    
})
public class Miembro implements Serializable, Comparable<Miembro> {

    public enum ValoresIndGenero {
        FEMENINO('F'),
        MASCULINO('M');

        private final char value;
        private static final Map<Character, ValoresIndGenero> lookup = new HashMap<>(); 

        private ValoresIndGenero(char value) {
            this.value = value;
        }

        static {
            for (ValoresIndGenero genero : values()) {
                lookup.put(genero.value, genero);
            }
        }

        public char getValue() {
            return value;
        }

        public static ValoresIndGenero get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum ValoresIndEstadoCivil {
        CASADO('C'),
        SOLTERO('S'),
        UNION_LIBRE('U'),
        NINGUNO('N');

        private final char value;
        private static final Map<Character, ValoresIndEstadoCivil> lookup = new HashMap<>();

        private ValoresIndEstadoCivil(char value) {
            this.value = value;
        }

        static {
            for (ValoresIndEstadoCivil estadoCivil : values()) {
                lookup.put(estadoCivil.value, estadoCivil);
            }
        }

        public char getValue() {
            return value;
        }

        public static ValoresIndEstadoCivil get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum ValoresIndEducacion {
        PRIMARIA('P'),
        SECUNDARIA('S'),
        TECNICA('T'),
        UNIVERSITARIA('U'),
        GRADO('G'),
        POSTGRADO('R'),
        MASTER('M'),
        DOCTORADO('D');

        private final char value;
        private static final Map<Character, ValoresIndEducacion> lookup = new HashMap<>();

        private ValoresIndEducacion(char value) {
            this.value = value;
        }

        static {
            for (ValoresIndEducacion educacion : values()) {
                lookup.put(educacion.value, educacion);
            }
        }

        public char getValue() {
            return value;
        }

        public static ValoresIndEducacion get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum Estados {
        ACTIVO('A'),
        INACTIVO('I');

        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }

        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Estados get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum Residencia {
        CIUDAD('C'),
        ESTADO('E');
        private final char value;
        private static final Map<Character, Residencia> lookup = new HashMap<>();

        private Residencia(char value) {
            this.value = value;
        }

        static {
            for (Residencia residencia : values()) {
                lookup.put(residencia.value, residencia);
            }
        }

        public char getValue() {
            return value;
        }

        public static Residencia get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum Atributos {
        ID_MIEMBRO("idMiembro"),
        DOC_IDENTIFICACION("docIdentificacion"),
        DIRECCION("direccion"),
        CIUDAD_RESIDENCIA("ciudadResidencia"),
        ESTADO_RESIDENCIA("estadoResidencia"),
        PAIS_RESIDENCIA("paisResidencia"),
        CODIGO_POSTAL("codigoPostal"),
        TELEFONO_MOVIL("telefonoMovil"),
        FECHA_NACIMIENTO("fechaNacimiento"),
        IND_GENERO("indGenero"),
        IND_ESTADO_CIVIL("indEstadoCivil"),
        IND_EDUCACION("indEducacion"),
        INGRESO_ECONOMICO("ingresoEconomico"),
        NOMBRE("nombre"),
        APELLIDO("apellido"),
        APELLIDO2("apellido2"),
        IND_CONTACTO_EMAIL("indContactoEmail"),
        IND_CONTACTO_SMS("indContactoSms"),
        IND_CONTACTO_NOTIFICACION("indContactoNotificacion"),
        IND_CONTACTO_ESTADO("indContactoEstado"),
        IND_HIJOS("indHijos"),
        CONTRASENA("contrasena"),
        CORREO("correo");

        private final String value;
        private static final Map<String, Atributos> lookup = new HashMap<>();

        private Atributos(String value) {
            this.value = value;
        }

        static {
            for (Atributos atributo : values()) {
                lookup.put(atributo.value, atributo);
            }
        }

        public String getValue() {
            return value;
        }

        public static Atributos get(String value) {
            return value == null ? null : lookup.get(value);
        }
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMiembro")
    private List<PremioMiembro> premioMiembroList;

    @OneToMany(mappedBy = "idMiembro")
    private List<HistoricoMovimientos> historicoMovimientosList;

    @JoinColumn(name = "MIEMBRO_REFERENTE", referencedColumnName = "ID_MIEMBRO")
    @ManyToOne
    private Miembro miembroReferente;
    
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DOC_IDENTIFICACION")
    private String docIdentificacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO_MIEMBRO")
    private Character indEstadoMiembro;

    @Column(name = "FECHA_EXPIRACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaExpiracion;

    @Size(max = 200)
    @Column(name = "DIRECCION")
    private String direccion;

    @Size(max = 100)
    @Column(name = "CIUDAD_RESIDENCIA")
    private String ciudadResidencia;

    @Size(max = 100)
    @Column(name = "ESTADO_RESIDENCIA")
    private String estadoResidencia;

    @Size(max = 3)
    @Column(name = "PAIS_RESIDENCIA")
    private String paisResidencia;

    @Size(max = 10)
    @Column(name = "CODIGO_POSTAL")
    private String codigoPostal;

    @Size(max = 15)
    @Column(name = "TELEFONO_MOVIL")
    private String telefonoMovil;

    @Column(name = "FECHA_NACIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;

    @Column(name = "IND_GENERO")
    private Character indGenero;

    @Column(name = "IND_ESTADO_CIVIL")
    private Character indEstadoCivil;

    @Size(max = 20)
    @Column(name = "FRECUENCIA_COMPRA")
    private String frecuenciaCompra;

    @Column(name = "FECHA_SUSPENSION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSuspension;

    @Size(max = 200)
    @Column(name = "CAUSA_SUSPENSION")
    private String causaSuspension;

    @Column(name = "IND_EDUCACION")
    private Character indEducacion;

    @Column(name = "INGRESO_ECONOMICO")
    private Double ingresoEconomico;

    @Size(max = 500)
    @Column(name = "COMENTARIOS")
    private String comentarios;
    
    @Column(name = "IND_POLITICA")
    private Boolean indPolitica;
    
    @Column(name = "FECHA_POLITICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPolitica;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    @Size(max = 300)
    @Column(name = "AVATAR")
    private String avatar;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NOMBRE")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "APELLIDO")
    private String apellido;

    @Size(max = 100)
    @Column(name = "APELLIDO2")
    private String apellido2;

    @Column(name = "IND_CONTACTO_EMAIL")
    private Boolean indContactoEmail;
    
    @Column(name = "IND_CONTACTO_SMS")
    private Boolean indContactoSms;
    
    @Column(name = "IND_CONTACTO_NOTIFICACION")
    private Boolean indContactoNotificacion;
    
    @Column(name = "IND_CONTACTO_ESTADO")
    private Boolean indContactoEstado;
    
    @Column(name = "IND_HIJOS")
    private Boolean indHijos;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "miembro")
    private List<PremioListaMiembro> premioListaMiembroList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "miembro")
    private List<MiembroInsigniaNivel> miembroInsigniaNivelList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "miembro")
    private List<MiembroMetricaNivel> miembroMetricaNivelList;

    @Transient
    private String contrasena;

    @Basic(optional = false)
    @NotNull
    @Column(name = "EMAIL")
    private String correo;

    @Transient
    private String nombreUsuario;

    @Transient
    private BigDecimal totalAcumulado;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "miembro")
    private List<TransaccionCompra> transaccionCompraList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "miembro")
    private List<ProductoListaMiembro> productoListaMiembroList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "miembro")
    private List<NotificacionListaMiembro> notificacionListaMiembroList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "miembro")
    private List<MiembroAtributo> miembroAtributoList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "miembro")
    private List<MisionListaMiembro> misionListaMiembroList;
    
    @Column(name = "IND_CAMBIO_PASS")
    private Boolean indCambioPass;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_MIEMBRO_SISTEMA")
    private Boolean indMiembroSistema;

    public Miembro() {
        //todo miembro creado en el api admin es automaticamente miembro del sistema a no ser que se especifique que no
        this.indMiembroSistema = true;
    }

    public Miembro(String docIdentificacion, Date fechaIngreso, Character indEstadoMiembro, String ciudadResidencia, String telefonoMovil, Date fechaNacimiento, Character indGenero, Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, String avatar, String nombre, String apellido, String apellido2, Long numVersion, String contrasena, String correo, String nombreUsuario) {
        this();
        this.docIdentificacion = docIdentificacion;
        this.fechaIngreso = fechaIngreso;
        this.indEstadoMiembro = indEstadoMiembro;
        this.ciudadResidencia = ciudadResidencia;
        this.telefonoMovil = telefonoMovil;
        this.fechaNacimiento = fechaNacimiento;
        this.indGenero = indGenero;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.avatar = avatar;
        this.nombre = nombre;
        this.apellido = apellido;
        this.apellido2 = apellido2;
        this.numVersion = numVersion;
        this.contrasena = contrasena;
        this.correo = correo;
        this.nombreUsuario = nombreUsuario;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getDocIdentificacion() {
        return docIdentificacion;
    }

    public void setDocIdentificacion(String docIdentificacion) {
        this.docIdentificacion = docIdentificacion;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Character getIndEstadoMiembro() {
        return indEstadoMiembro;
    }

    public void setIndEstadoMiembro(Character indEstadoMiembro) {
        this.indEstadoMiembro = indEstadoMiembro != null ? Character.toUpperCase(indEstadoMiembro) : null;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudadResidencia() {
        return ciudadResidencia;
    }

    public void setCiudadResidencia(String ciudadResidencia) {
        this.ciudadResidencia = ciudadResidencia;
    }

    public String getEstadoResidencia() {
        return estadoResidencia;
    }

    public void setEstadoResidencia(String estadoResidencia) {
        this.estadoResidencia = estadoResidencia;
    }

    public String getPaisResidencia() {
        return paisResidencia;
    }

    public void setPaisResidencia(String paisResidencia) {
        this.paisResidencia = paisResidencia;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Character getIndGenero() {
        return indGenero;
    }

    public void setIndGenero(Character indGenero) {
        this.indGenero = indGenero != null ? Character.toUpperCase(indGenero) : null;
    }

    public Boolean getIndContactoEmail() {
        return indContactoEmail;
    }

    public void setIndContactoEmail(Boolean indContactoEmail) {
        this.indContactoEmail = indContactoEmail;
    }

    public Boolean getIndContactoSms() {
        return indContactoSms;
    }

    public void setIndContactoSms(Boolean indContactoSms) {
        this.indContactoSms = indContactoSms;
    }

    public Boolean getIndContactoNotificacion() {
        return indContactoNotificacion;
    }

    public void setIndContactoNotificacion(Boolean indContactoNotificacion) {
        this.indContactoNotificacion = indContactoNotificacion;
    }

    public Boolean getIndContactoEstado() {
        return indContactoEstado;
    }

    public void setIndContactoEstado(Boolean indContactoEstado) {
        this.indContactoEstado = indContactoEstado;
    }

    public Character getIndEstadoCivil() {
        return indEstadoCivil;
    }

    public void setIndEstadoCivil(Character indEstadoCivil) {
        this.indEstadoCivil = indEstadoCivil != null ? Character.toUpperCase(indEstadoCivil) : null;
    }

    public String getFrecuenciaCompra() {
        return frecuenciaCompra;
    }

    public void setFrecuenciaCompra(String frecuenciaCompra) {
        this.frecuenciaCompra = frecuenciaCompra;
    }

    public Date getFechaSuspension() {
        return fechaSuspension;
    }

    public void setFechaSuspension(Date fechaSuspension) {
        this.fechaSuspension = fechaSuspension;
    }

    public String getCausaSuspension() {
        return causaSuspension;
    }

    public void setCausaSuspension(String causaSuspension) {
        this.causaSuspension = causaSuspension;
    }

    public Character getIndEducacion() {
        return indEducacion;
    }

    public void setIndEducacion(Character indEducacion) {
        this.indEducacion = indEducacion != null ? Character.toUpperCase(indEducacion) : null;
    }

    public Double getIngresoEconomico() {
        return ingresoEconomico;
    }

    public void setIngresoEconomico(Double ingresoEconomico) {
        this.ingresoEconomico = ingresoEconomico;
    }

    public Boolean getIndHijos() {
        return indHijos;
    }

    public void setIndHijos(Boolean indHijos) {
        this.indHijos = indHijos;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public BigDecimal getTotalAcumulado() {
        return totalAcumulado;
    }

    public void setTotalAcumulado(BigDecimal totalAcumulado) {
        this.totalAcumulado = totalAcumulado;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MiembroAtributo> getMiembroAtributoList() {
        return miembroAtributoList;
    }

    public void setMiembroAtributoList(List<MiembroAtributo> miembroAtributoList) {
        this.miembroAtributoList = miembroAtributoList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<NotificacionListaMiembro> getNotificacionListaMiembroList() {
        return notificacionListaMiembroList;
    }

    public void setNotificacionListaMiembroList(List<NotificacionListaMiembro> notificacionListaMiembroList) {
        this.notificacionListaMiembroList = notificacionListaMiembroList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MiembroMetricaNivel> getMiembroMetricaNivelList() {
        return miembroMetricaNivelList;
    }

    public void setMiembroMetricaNivelList(List<MiembroMetricaNivel> miembroMetricaNivelList) {
        this.miembroMetricaNivelList = miembroMetricaNivelList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MisionListaMiembro> getMisionListaMiembroList() {
        return misionListaMiembroList;
    }

    public void setMisionListaMiembroList(List<MisionListaMiembro> misionListaMiembroList) {
        this.misionListaMiembroList = misionListaMiembroList;
    }

    @Override
    public int compareTo(Miembro miembro) {

        return this.totalAcumulado.compareTo(miembro.getTotalAcumulado());

    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Miembro)) {
            return false;
        }
        Miembro other = (Miembro) object;
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        return true;
    }

//    @Override
//    public String toString() {
//        return "com.flecharoja.loyalty.model.Miembro[ idMiembro=" + idMiembro + " ]";
//    }
    @Override
    public String toString() {
        return idMiembro + ", " + docIdentificacion + ", " + fechaIngreso + ", " + indEstadoMiembro + ", " + fechaExpiracion + ", " + direccion + ", " + ciudadResidencia + ", " + estadoResidencia + ", " + paisResidencia + ", " + codigoPostal + ", " + telefonoMovil + ", " + fechaNacimiento + ", " + indGenero + ", " + indEstadoCivil + ", " + frecuenciaCompra + ", " + fechaSuspension + ", " + causaSuspension + ", " + indEducacion + ", " + ingresoEconomico + ", " + comentarios + ", " + fechaCreacion + ", " + usuarioCreacion + ", " + fechaModificacion + ", " + usuarioModificacion + ", " + avatar + ", " + nombre + ", " + apellido + ", " + apellido2 + ", " + contrasena + ", " + correo + ", " + nombreUsuario + ", " + indContactoEmail + ", " + indContactoSms + ", " + indContactoNotificacion + ", " + indContactoEstado + ", " + indHijos + "\n";
    }
    public String toString2(){
        return idMiembro+", "+nombre+", "+apellido+", "+indGenero+", "+ciudadResidencia+", "+estadoResidencia+", "+fechaIngreso+", "+fechaNacimiento+", ";
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MiembroInsigniaNivel> getMiembroInsigniaNivelList() {
        return miembroInsigniaNivelList;
    }

    public void setMiembroInsigniaNivelList(List<MiembroInsigniaNivel> miembroInsigniaNivelList) {
        this.miembroInsigniaNivelList = miembroInsigniaNivelList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<PremioListaMiembro> getPremioListaMiembroList() {
        return premioListaMiembroList;
    }

    public void setPremioListaMiembroList(List<PremioListaMiembro> premioListaMiembroList) {
        this.premioListaMiembroList = premioListaMiembroList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ProductoListaMiembro> getProductoListaMiembroList() {
        return productoListaMiembroList;
    }

    public void setProductoListaMiembroList(List<ProductoListaMiembro> productoListaMiembroList) {
        this.productoListaMiembroList = productoListaMiembroList;
    }

    public Miembro getMiembroReferente() {
        return miembroReferente;
    }

    public void setMiembroReferente(Miembro miembroReferente) {
        this.miembroReferente = miembroReferente;
    }
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<PremioMiembro> getPremioMiembroList() {
        return premioMiembroList;
    }

    public void setPremioMiembroList(List<PremioMiembro> premioMiembroList) {
        this.premioMiembroList = premioMiembroList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<TransaccionCompra> getTransaccionCompraList() {
        return transaccionCompraList;
    }

    public void setTransaccionCompraList(List<TransaccionCompra> transaccionCompraList) {
        this.transaccionCompraList = transaccionCompraList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<HistoricoMovimientos> getHistoricoMovimientosList() {
        return historicoMovimientosList;
    }

    public void setHistoricoMovimientosList(List<HistoricoMovimientos> historicoMovimientosList) {
        this.historicoMovimientosList = historicoMovimientosList;
    }

    public Boolean getIndPolitica() {
        return indPolitica;
    }

    public void setIndPolitica(Boolean indPolitica) {
        this.indPolitica = indPolitica;
    }

    public Date getFechaPolitica() {
        return fechaPolitica;
    }

    public void setFechaPolitica(Date fechaPolitica) {
        this.fechaPolitica = fechaPolitica;
    }

    public Boolean getIndCambioPass() {
        return indCambioPass;
    }

    public void setIndCambioPass(Boolean indCambioPass) {
        this.indCambioPass = indCambioPass;
    }

    public Boolean getIndMiembroSistema() {
        return indMiembroSistema;
    }

    public void setIndMiembroSistema(Boolean indMiembroSistema) {
        this.indMiembroSistema = indMiembroSistema;
    }
    
}
