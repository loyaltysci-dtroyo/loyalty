/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "REGLA_ASIGNACION_PREMIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReglaAsignacionPremio.findByIdPremioOrderByFechaCreacion", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.premio.idPremio = :idPremio ORDER BY r.fechaCreacion  DESC"),
    @NamedQuery(name = "ReglaAsignacionPremio.findAll", query = "SELECT r FROM ReglaAsignacionPremio r")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByIdRegla", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.idRegla = :idRegla")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByIndTipoRegla", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.indTipoRegla = :indTipoRegla")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByIndOperador", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.indOperador = :indOperador")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByValorComparacion", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.valorComparacion = :valorComparacion")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByFechaInicio", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.fechaInicio = :fechaInicio")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByFechaFinal", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.fechaFinal = :fechaFinal")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByFechaIndUltimo", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.fechaIndUltimo = :fechaIndUltimo")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByFechaCantidad", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.fechaCantidad = :fechaCantidad")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByFechaIndTipo", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.fechaIndTipo = :fechaIndTipo")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByUsuarioCreacion", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.usuarioCreacion = :usuarioCreacion")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByUsuarioModificacion", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.usuarioModificacion = :usuarioModificacion")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByFechaCreacion", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByFechaModificacion", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.fechaModificacion = :fechaModificacion")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByPremio", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.premio.idPremio = :idPremio")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByIndOperadorRegla", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.indOperadorRegla = :indOperadorRegla")
    , @NamedQuery(name = "ReglaAsignacionPremio.findByIdTier", query = "SELECT r FROM ReglaAsignacionPremio r WHERE r.tier.idNivel = :idTier")})
public class ReglaAsignacionPremio implements Serializable {

    public enum TiposRegla {
        FRECUENCIA_COMPRA('A'),
        MONTO_COMPRA('B'),
        ACUMULADO_COMPRA('C');

        private final char value;
        private static final Map<Character, TiposRegla> lookup = new HashMap<>();

        private TiposRegla(char value) {
            this.value = value;
        }

        static {
            for (TiposRegla tipo : TiposRegla.values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static TiposRegla get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum TiposUltimaFecha {
        DIA('D'),
        MES('M'),
        ANO('A');

        private final char value;
        private static final Map<Character, TiposUltimaFecha> lookup = new HashMap<>();

        private TiposUltimaFecha(char value) {
            this.value = value;
        }

        static {
            for (TiposUltimaFecha tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static TiposUltimaFecha get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum TiposFecha {
        ULTIMO('B'),
        ANTERIOR('C'),
        DESDE('D'),
        ENTRE('E'),
        MES_ACTUAL('F'),
        ANO_ACTUAL('G'),
        HASTA('H');

        public final Character value;
        private static final Map<Character, TiposFecha> lookup = new HashMap<>();

        private TiposFecha(char value) {
            this.value = value;
        }

        static {
            for (TiposFecha valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public static TiposFecha get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum ValoresIndOperador {
        IGUAL("V"),
        MAYOR("W"),
        MAYOR_IGUAL("X"),
        MENOR("Y"),
        MENOR_IGUAL("Z");

        public final String value;
        private static final Map<String, ValoresIndOperador> lookup = new HashMap<>();

        private ValoresIndOperador(String value) {
            this.value = value;
        }

        static {
            for (ValoresIndOperador valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public static ValoresIndOperador get(String value) {
            return value == null ? null : lookup.get(value);
        }
    }
    
    public enum ValoresIndOperadorRegla {
        OR("O"),
        AND("A");

        private final String value;
        private static final Map<String, ValoresIndOperadorRegla> lookup = new HashMap<>();

        private ValoresIndOperadorRegla(String value) {
            this.value = value;
        }

        static {
            for (ValoresIndOperadorRegla valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public static ValoresIndOperadorRegla get(String value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "metrica_uuid")
    @GenericGenerator(name = "metrica_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_REGLA")
    private String idRegla;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "IND_TIPO_REGLA")
    private String indTipoRegla;

    @Basic(optional = false)
    @NotNull
    @Size(max = 1)
    @Column(name = "IND_OPERADOR")
    private String indOperador;

    @Size(max = 40)
    @Column(name = "VALOR_COMPARACION")
    private String valorComparacion;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "FECHA_FINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFinal;

    @Size(max = 1)
    @Column(name = "FECHA_IND_ULTIMO")
    private String fechaIndUltimo;

    @Size(max = 10)
    @Column(name = "FECHA_CANTIDAD")
    private String fechaCantidad;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "FECHA_IND_TIPO")
    private String fechaIndTipo;// tipo de calendarizacion de la regla (TipoFecha)

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "IND_OPERADOR_REGLA")
    private String indOperadorRegla;
    
    @JoinColumn(name = "TIER", referencedColumnName = "ID_NIVEL")
    @ManyToOne()
    private NivelMetrica tier;
    
    @JoinColumn(name = "PREMIO", referencedColumnName = "ID_PREMIO")
    @ManyToOne()
    private Premio premio;
    
    @Version
    @Basic(optional = false)
    @NotNull()
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    public ReglaAsignacionPremio() {
    }

    public ReglaAsignacionPremio(String idRegla) {
        this.idRegla = idRegla;
    }

    public ReglaAsignacionPremio(String idRegla, String indTipoRegla, String fechaIndTipo, String usuarioCreacion, String usuarioModificacion, Date fechaCreacion, Date fechaModificacion, String indOperadorRegla) {
        this.idRegla = idRegla;
        this.indTipoRegla = indTipoRegla;
        this.fechaIndTipo = fechaIndTipo;
        this.usuarioCreacion = usuarioCreacion;
        this.usuarioModificacion = usuarioModificacion;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.indOperadorRegla = indOperadorRegla;
    }

    public String getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(String idRegla) {
        this.idRegla = idRegla;
    }

    public String getIndTipoRegla() {
        return indTipoRegla;
    }

    public void setIndTipoRegla(String indTipoRegla) {
        this.indTipoRegla = indTipoRegla;
    }

    public String getIndOperador() {
        return indOperador;
    }

    public void setIndOperador(String indOperador) {
        this.indOperador = indOperador;
    }

    public String getValorComparacion() {
        return valorComparacion;
    }

    public void setValorComparacion(String valorComparacion) {
        this.valorComparacion = valorComparacion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public String getFechaIndUltimo() {
        return fechaIndUltimo;
    }

    public void setFechaIndUltimo(String fechaIndUltimo) {
        this.fechaIndUltimo = fechaIndUltimo;
    }

    public String getFechaCantidad() {
        return fechaCantidad;
    }

    public void setFechaCantidad(String fechaCantidad) {
        this.fechaCantidad = fechaCantidad;
    }

    public String getFechaIndTipo() {
        return fechaIndTipo;
    }

    public void setFechaIndTipo(String fechaIndTipo) {
        this.fechaIndTipo = fechaIndTipo;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getIndOperadorRegla() {
        return indOperadorRegla;
    }

    public void setIndOperadorRegla(String indOperadorRegla) {
        this.indOperadorRegla = indOperadorRegla;
    }
    
    public NivelMetrica getTier() {
        return tier;
    }

    public void setTier(NivelMetrica tier) {
        this.tier = tier;
    }

    @XmlTransient
    public Premio getPremio() {
        return premio;
    }

    @XmlTransient
    public void setPremio(Premio premio) {
        this.premio = premio;
    }
    
    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegla != null ? idRegla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReglaAsignacionPremio)) {
            return false;
        }
        ReglaAsignacionPremio other = (ReglaAsignacionPremio) object;
        if ((this.idRegla == null && other.idRegla != null) || (this.idRegla != null && !this.idRegla.equals(other.idRegla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ReglaAsignacionPremio[ idRegla=" + idRegla + " ]";
    }

}
