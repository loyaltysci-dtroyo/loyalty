/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CategoriaProducto;
import com.flecharoja.loyalty.model.SubcategoriaProducto;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * Clase con los metodos de negocio para el manejo de subcategorias de productos
 *
 * @author wtencio
 */
@Stateless
public class SubCategoriaProductoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;

    @EJB
    UtilBean utilBean;

    /**
     * Método que almacena la informacion de una nueva subcategoria de productos
     *
     * @param subCategoria informacion de la subcategoria
     * @param usuario usuario en sesion
     * @param categoriaPerteneciente identificador de la categoria a la que
     * pertenece
     * @param locale
     * @return identificador de la categoria
     */
    public String insertSubCategoriaProducto(SubcategoriaProducto subCategoria, String usuario, String categoriaPerteneciente, Locale locale) {
        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (subCategoria.getImagen() == null || subCategoria.getImagen().trim().isEmpty()) {
            subCategoria.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                subCategoria.setImagen(filesUtils.uploadImage(subCategoria.getImagen(), MyAwsS3Utils.Folder.ARTE_SUBCATEGORIA_PRODUCTO, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }
        CategoriaProducto categoria;
        try {
            categoria = em.find(CategoriaProducto.class, categoriaPerteneciente);

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "categoriaPerteneciente");
        }
        subCategoria.setIdCategoria(categoria);
        subCategoria.setNombreInterno(subCategoria.getNombreInterno().toUpperCase());
        subCategoria.setFechaCreacion(Calendar.getInstance().getTime());
        subCategoria.setUsuarioCreacion(usuario);
        subCategoria.setFechaModificacion(Calendar.getInstance().getTime());
        subCategoria.setUsuarioModificacion(usuario);
        subCategoria.setNumVersion(new Long(1));

        try {
            em.persist(subCategoria);
            em.flush();
        } catch (ConstraintViolationException e) {
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (!subCategoria.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(subCategoria.getImagen());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        bitacoraBean.logAccion(SubcategoriaProducto.class, subCategoria.getIdSubcategoria(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        return subCategoria.getIdSubcategoria();
    }

    /**
     * Método que actualiza la informacion de una subcategoria de producto
     *
     * @param subCategoria informacion de la subcategoria de producto
     * @param usuario usuario en sesion
     * @param idCategoria identificador de la categoria perteneciente
     * @param locale
     */
    public void editCategoriaProducto(SubcategoriaProducto subCategoria, String usuario, String idCategoria, Locale locale) {

        //verificacion que la entidad exista
        SubcategoriaProducto original = (SubcategoriaProducto) em.createNamedQuery("SubcategoriaProducto.findByIdSubcategoria").setParameter("idSubcategoria", subCategoria.getIdSubcategoria()).getSingleResult();
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_subcategory_not_found");
        }

        String urlImagenOriginal = original.getImagen();//url imagen original
        String urlImagenNueva = null;//url imagen nueva
        //si el atributo de imagen viene nula (no deberia)
        if (subCategoria.getImagen() == null || subCategoria.getImagen().trim().isEmpty()) {
            //establecimiento de la imagen por defecto
            urlImagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            subCategoria.setImagen(urlImagenNueva);
        } else //ver si el valor en el atributo de imagen, difiere del almacenado
        {
            if (!urlImagenOriginal.equals(subCategoria.getImagen())) {//si la imagen es diferente a la almacenada
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    urlImagenNueva = filesUtils.uploadImage(subCategoria.getImagen(), MyAwsS3Utils.Folder.ARTE_SUBCATEGORIA_PRODUCTO, null);
                    subCategoria.setImagen(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }

        //establecimiento de atributos por defecto
        subCategoria.setUsuarioModificacion(usuario);
        subCategoria.setFechaModificacion(Calendar.getInstance().getTime());
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, subCategoria.toString());
        try {
            //se modifica la entidad y se registra la accion en bitacora
            em.merge(subCategoria);
            em.flush();//se manda a guardar a la bases de datos para la verificacion en el momento de la entidad, etc
        } catch (ConstraintViolationException | OptimisticLockException e) {//en el caso que la informacion no sea valida
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(SubcategoriaProducto.class, subCategoria.getIdSubcategoria(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);

        if (urlImagenNueva != null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Método que elimina una categoria de producto
     *
     * @param idSubCategoria identificador de la subcategoria
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeSubCategoriaProducto(String idSubCategoria, String usuario, Locale locale) {
        SubcategoriaProducto subCategoria = em.find(SubcategoriaProducto.class, idSubCategoria);//se busca la entidad
        if (subCategoria == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_subcategory_not_found");
        }

        //se obtiene el url de la imagen almacenada de la entidad y borra
        String url = subCategoria.getImagen();
        if (!url.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(url);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
        //se manda a borrar la entidad y se registra la accion en la bitacora
        em.remove(subCategoria);

        bitacoraBean.logAccion(SubcategoriaProducto.class, idSubCategoria, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

    /**
     * Método que obtiene la informacion de una categoria de producto
     *
     * @param idSubCategoria identificador de categoria
     * @param idCategoria identificador de la categoria perteneciente
     * @param locale
     * @return informacion de la subcategoria
     */
    public SubcategoriaProducto getSubCategoriaProductoPorId(String idSubCategoria, String idCategoria, Locale locale) {
        SubcategoriaProducto subcategoria = em.find(SubcategoriaProducto.class, idSubCategoria);
        
        //verifica que la entidad no sea nula (no se encontro)
        if (subcategoria == null) {
             throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_subcategory_not_found");
        }
        if (!subcategoria.getIdCategoria().getIdCategoria().equals(idCategoria)) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "subcategory_not_belog_category");
        }
        return subcategoria;
    }

    /**
     * Metodo que verifica si existe algun registro de subcategoria de producto
     * con un valor en la columna de nombreInterno con el mismo valor del
     * parametro nombre
     *
     * @param nombre Nombre interno a verificar
     * @return Existencia del nombre interno
     */
    public Boolean existsNombreInterno(String nombre) {
        return ((Long) em.createNamedQuery("SubcategoriaProducto.countByNombreInterno").setParameter("nombreInterno", nombre.toUpperCase()).getSingleResult()) > 0;
    }

    /**
     * Metodo que obtiene el listado de subcategorias de productos almacenadas
     * en un rango definido por parametros
     *
     * @param idCategoria identificador de la categoria
     * @param registro Numero de registro inicial
     * @param cantidad Cantidad de resultados en la lista
     * @param locale
     * @return Lista y encabezados acerca del rango
     */
    public Map<String, Object> getSubCategorias(String idCategoria, int registro, int cantidad, Locale locale) {
        //verifica que la cantidad en la peticion sea menor a la aceptada, de no ser asi se retorna una respuesta informando acerca de ello, junto con el encabezado de rango maximo
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        Long total;//almacena el total de registros
        List resultado;//almacena el resultado

        try {
            total = ((Long) em.createNamedQuery("SubcategoriaProducto.countAll").setParameter("idCategoria", idCategoria).getSingleResult());//se obtiene el total de registros
            total -= total - 1 < 0 ? 0 : 1;//reduce el total en uno (mientras que no sea <0) para que concuerde con el numero de ultimo registro

            //ve que si en el caso que el registro sea mayor que el total, este se reduce por cantidad (mientras que no sea <0) hasta que este entre un valor de 0 a total
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            //se obtiene el listado usando los parametros de registro y cantidad para limitar el rango de respuesta
            resultado = em.createNamedQuery("SubcategoriaProducto.findAll").setParameter("idCategoria", idCategoria)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();
        } catch (IllegalArgumentException e) {//posiblemente pueda ocurrir al establecer los parametros de registro y cantidad erroneos (ejm: menores a 0)
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        //se establece la respuesta encapsulando el listado y estableciendo los encabezados de rango de la respuesta y cantidad maximo aceptado
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * Metodo que realiza una busqueda en base a palabras claves sobre una serie
     * de atributos de la entidad subcategoria de productos, filtrado por un
     * rango de tuplas que puede devolver
     *
     * @param idCategoria identificador de la categoria
     * @param busqueda Cadena de texto con las palabras claves
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return Listado de todos las subcategorias de producto encontrados
     */
    public Map<String, Object> searchSubCategoriasProducto(String idCategoria, String busqueda, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        String[] palabrasClaves = busqueda.split(" ");
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<SubcategoriaProducto> root = query.from(SubcategoriaProducto.class);

            List<Predicate> predicates2 = new ArrayList<>();

            CategoriaProducto categoria = em.find(CategoriaProducto.class, idCategoria);
            predicates2.add(cb.equal(root.get("idCategoria"), categoria));

            List<Predicate> predicates = new ArrayList<>();
            for (String palabraClave : palabrasClaves) {
                predicates.add(cb.like(cb.lower(root.get("nombre")), "%" + palabraClave.toLowerCase() + "%"));
                predicates.add(cb.like(cb.lower(root.get("descripcion")), "%" + palabraClave.toLowerCase() + "%"));
                predicates.add(cb.like(cb.lower(root.get("nombreInterno")), "%" + palabraClave.toUpperCase() + "%"));

            }

            query.where(cb.or(predicates2.toArray(new Predicate[predicates2.size()])), cb.or(predicates.toArray(new Predicate[predicates.size()])));
            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            resultado = em.createQuery(query.select(root))
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        } catch (QueryTimeoutException e) {
           throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO,locale, "database_connection_failed");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }
}
