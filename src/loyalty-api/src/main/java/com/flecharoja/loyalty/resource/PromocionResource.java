package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CategoriaPromocion;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.service.PromocionBean;
import com.flecharoja.loyalty.service.PromocionCategoriaPromocionBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase de recursos http disponibles para el manejo de promociones.
 *
 * @author svargas
 */
@Api(value = "Promocion")
@Path("promocion")
public class PromocionResource {
    
    @EJB
    PromocionBean promocionBean;

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @EJB
    PromocionCategoriaPromocionBean promocionCategoriaPromocionBean;
    
    @Context
    HttpServletRequest request;
    

    /**
     * Metodo que obtiene un listado de promociones por un rango establecido
     * usando los parametros de cantidad y registro y opcionalmente filtrados
     * por indicadores y terminos de busqueda aplicados sobre una serie de
     * campos de filtros ademas de encabezados indicando del rango aceptable maximo
     * y el rango actual de resultados, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param estados Listado de indicadores de estado de promociones a filtrar
     * @param acciones Listado de indicadores de acciones de promociones a filtrar
     * @param calendarizaciones Listado de indicadores de calendarizacion de promociones a filtrar
     * @param busqueda Terminos de busqueda a aplicar
     * @param filtros Listado de campos de busqueda hacia donde aplicar la busqueda
     * @param ordenTipo
     * @param ordenCampo
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta con el listado de las promociones en un rango valido
     * junto con encabezados sobre el rango actual
     */
    @ApiOperation(value = "Obtener lista de promociones",
            responseContainer = "List",
            response = Promocion.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPromociones(
            @ApiParam(value = "Indicadores de estado de promociones") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de tipos de acciones de promocion") @QueryParam("accion") List<String> acciones,
            @ApiParam(value = "Indicadores de tipos de calendarizacion de promocion") @QueryParam("calendarizacion") List<String> calendarizaciones,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Filtros sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PROMOCIONES_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        //tratamiento temporal a estados...
        //TODO BORRAR despues de cambios en el front end
        List<String> estadosTemp = new ArrayList<>();
        estados.forEach((estado) -> {
            estadosTemp.addAll(Arrays.asList(estado.split("-")));
        });

        Map<String, Object> respuesta = promocionBean.getPromociones(estadosTemp, acciones, calendarizaciones, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }

    /**
     * Metodo que registra la informacion de una nueva promocion y retorna el
     * identificador de la promocion creada, de ocurrir un error se retornara
     * una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param promocion Objecto con la informacion de una promocion requerida
     * para su registro
     * @return Respuesta con el identificador de la promocion creada
     */
    @ApiOperation(value = "Registrar promocion", response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertPromocion(
            @ApiParam(value = "Informacion de la promocion", required = true) Promocion promocion) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PROMOCIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return promocionBean.insertPromocion(promocion, usuario,request.getLocale());
    }

    /**
     * Metodo que modifica la informacion de una promocion existente, de ocurrir
     * un error se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param promocion Objecto con la informacion actualizada de una promocion
     * existente para su registro
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Modificacion de promocion")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void editPromocion(
            @ApiParam(value = "Informacion de la promocion", required = true) Promocion promocion) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PROMOCIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        promocionBean.editPromocion(promocion, usuario,request.getLocale());
    }

    //TODO BORRAR /buscar/*
    
    @GET
    @Path("/buscar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchPromociones(
            @QueryParam("estado") String estado,
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @QueryParam("registro") int registro) {
        return this.getPromociones(estado==null?null:Arrays.asList(estado.split("-")), null, null, null, null, null, null, cantidad, registro);
    }

    @GET
    @Path("/buscar/{busqueda}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchPromociones(
            @PathParam("busqueda") String busqueda,
            @QueryParam("estado") String estado,
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @QueryParam("registro") int registro) {
        return this.getPromociones(estado==null?null:Arrays.asList(estado.split("-")), null, null, busqueda, null, null, null, cantidad, registro);
    }

    /**
     * Metodo que obtiene la informacion de una promocion segun su identificador
     * en el parametro de idPromocion, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param idPromocion Identificador de la promocion.
     * @return Respuesta con la informacion de la promocion deseada.
     */
    @ApiOperation(value = "Obtener una promocion por identificador",
            response = Promocion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idPromocion}")
    @Produces(MediaType.APPLICATION_JSON)
    public Promocion getPromocionPorId(
            @ApiParam(value = "Identificador de promocion", required = true)
            @PathParam("idPromocion") String idPromocion) {
        String token;
        try {
            context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PROMOCIONES_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return promocionBean.getPromocionPorId(idPromocion,request.getLocale());
    }

    /**
     * Metodo que elimina o archiva una promocion segun ciertas condiciones, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idPromocion Identificador de la promocion
     */
    @ApiOperation(value = "Eliminacion/Archivado de promocion")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idPromocion}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deletePromocion(
            @ApiParam(value = "Identificador de promocion", required = true)
            @PathParam("idPromocion") String idPromocion) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PROMOCIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        promocionBean.removePromocion(idPromocion, usuario,request.getLocale());
    }

    /**
     * Metodo que obtiene en termino de true/false la existencia de un nombre
     * interno de una promocion en algun registro de promocion ya existente, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param nombreInterno Paramatro con el nombre interno a verificar
     * @return Respuesta con el valor booleano de la existencia del nombre
     * interno
     */
    @ApiOperation(value = "Verificacion de existencia de nombre interno en promociones",
            response = Boolean.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("/comprobar-nombre/{nombreInterno}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean existsNombreInterno(@ApiParam(value = "Nombre interno de notificacion", required = true) @PathParam("nombreInterno") String nombreInterno) {
        String token;
        try {
            context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PROMOCIONES_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return promocionBean.existsNombreInterno(nombreInterno);
    }
    
    /**
     * Nota: Subrecurso a eliminar
     *
     * @param nombre Valor del nombre interno a comprobar su existencia
     * @return Respuesta OK con un indicador booleano
     */
    @GET
    @Path("exists/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean oldExistsNombreInterno(@PathParam("nombre") String nombre) {
        return this.existsNombreInterno(nombre);
    }

    /**
     * Metodo que obtiene una lista de categorias de promocion a la cual la
     * promocion pertenece en un rango segun los parametros de registro y
     * cantidad, de ocurrir un error se retornara una respuesta con un diferente
     * estado junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param idPromocion Identificador de la promocion
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta con el listado de las categorias de promocion en un
     * rango valido junto con encabezados sobre el rango actual
     */
    @ApiOperation(value = "Obtener lista de categorias de promocion pertenecentes a promocion",responseContainer = "List",
            response = CategoriaPromocion.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idPromocion}/categoria")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategorias(
            @ApiParam(value = "Identificador de promocion", required = true)
            @PathParam("idPromocion") String idPromocion,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PROMOCIONES_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
         Map<String, Object> respuesta = promocionCategoriaPromocionBean.getCategoriasPorIdPromocion(idPromocion, registro, cantidad,request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }
    
    /**
     * Metodo que clona una promocion por su identificador, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idPromocion Identificador de la promocion
     * @return Respuesta con el identificador de la promocion clonada
     */
    @ApiOperation(value = "Clonado de promocion")
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idPromocion}/clonar")
    @Produces(MediaType.APPLICATION_JSON)
    public String clonePromocion(
            @ApiParam(value = "Identificador de promocion", required = true)
            @PathParam("idPromocion") String idPromocion) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PROMOCIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return promocionBean.clonePromocion(idPromocion, usuario,request.getLocale());
    }
}
