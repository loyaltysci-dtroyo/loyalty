/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "DOCUMENTO_DETALLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentoDetalle.findAll", query = "SELECT d FROM DocumentoDetalle d"),
    @NamedQuery(name = "DocumentoDetalle.countByNumDocumento", query = "SELECT COUNT(d) FROM DocumentoDetalle d WHERE d.numDocumento.numDocumento = :numDocumento"),
    @NamedQuery(name = "DocumentoDetalle.findByNumDocumento", query = "SELECT d FROM DocumentoDetalle d WHERE d.numDocumento.numDocumento = :numDocumento"),
    @NamedQuery(name = "DocumentoDetalle.findByNumDocumentoIdPremio", query = "SELECT COUNT(d) FROM DocumentoDetalle d WHERE d.numDocumento.numDocumento = :numDocumento AND d.codPremio.idPremio = :codPremio")
})
public class DocumentoDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "detalle_uuid")
    @GenericGenerator(name = "detalle_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "NUM_DETALLE_DOCUMENTO")
    private String numDetalleDocumento;

    @Basic(optional = false)
    @NotNull
    @Column(name = "CANTIDAD")
    private Long cantidad;

    @Column(name = "STATUS")
    private Character status;

    @Size(max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Size(max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    @Column(name = "PRECIO")
    private Double precio;

    @Column(name = "PROMEDIO")
    private Double promedio;

    @JoinColumn(name = "NUM_DOCUMENTO", referencedColumnName = "NUM_DOCUMENTO")
    @ManyToOne(optional = false)
    private DocumentoInventario numDocumento;

    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO")
    @ManyToOne
    private Miembro idMiembro;

    @JoinColumn(name = "COD_PREMIO", referencedColumnName = "ID_PREMIO")
    @ManyToOne(optional = false)
    private Premio codPremio;

    public DocumentoDetalle() {
    }

    public DocumentoDetalle(String numDetalleDocumento) {
        this.numDetalleDocumento = numDetalleDocumento;
    }

    public DocumentoDetalle(String numDetalleDocumento, Long cantidad, Long numVersion) {
        this.numDetalleDocumento = numDetalleDocumento;
        this.cantidad = cantidad;
        this.numVersion = numVersion;
    }

    public String getNumDetalleDocumento() {
        return numDetalleDocumento;
    }

    public void setNumDetalleDocumento(String numDetalleDocumento) {
        this.numDetalleDocumento = numDetalleDocumento;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Double getPromedio() {
        return promedio;
    }

    public void setPromedio(Double promedio) {
        this.promedio = promedio;
    }

    public DocumentoInventario getNumDocumento() {
        return numDocumento;
    }

    public void setNumDocumento(DocumentoInventario numDocumento) {
        this.numDocumento = numDocumento;
    }

    public Miembro getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(Miembro idMiembro) {
        this.idMiembro = idMiembro;
    }

    public Premio getCodPremio() {
        return codPremio;
    }

    public void setCodPremio(Premio codPremio) {
        this.codPremio = codPremio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numDetalleDocumento != null ? numDetalleDocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentoDetalle)) {
            return false;
        }
        DocumentoDetalle other = (DocumentoDetalle) object;
        if ((this.numDetalleDocumento == null && other.numDetalleDocumento != null) || (this.numDetalleDocumento != null && !this.numDetalleDocumento.equals(other.numDetalleDocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.DocumentoDetalle[ numDetalleDocumento=" + numDetalleDocumento + " ]";
    }

}
