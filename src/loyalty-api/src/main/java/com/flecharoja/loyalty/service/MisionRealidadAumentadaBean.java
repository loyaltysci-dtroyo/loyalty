/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionRealidadAumentada;
import java.util.Date;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author faguilar
 */
@Stateless
public class MisionRealidadAumentadaBean {
    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    @EJB
    UtilBean utilBean;

    /**
     * obtencion de detalle de realidad aumentada en una mision
     * 
     * @param idMision identificador de mision
     * @param locale locale
     * @return detalle de realidad aumentada
     */
    public MisionRealidadAumentada getRealidadAumentadaMision(String idMision, Locale locale) {
         Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.REALIDAD_AUMENTADA.getValue())) {
             throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        MisionRealidadAumentada realidadAumentada = em.find(MisionRealidadAumentada.class, idMision);
        if (realidadAumentada == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_social_network_not_found");
        }

        return realidadAumentada;
    }

    /**
     * edicion de detalle de realidad aumentada a una mision
     * 
     * @param idMision identificador de mision
     * @param realidadAumentada data
     * @param idUsuario id admin
     * @param locale locale
     */
    public void createEditRealidadAumentadaMision(String idMision, MisionRealidadAumentada realidadAumentada, String idUsuario, Locale locale) {
        if (realidadAumentada==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "MisionRealidadAumentada");
        }
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.REALIDAD_AUMENTADA.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        MisionRealidadAumentada original = em.find(MisionRealidadAumentada.class, idMision);

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }

        Date fecha = new Date();
        if (original == null) {
            realidadAumentada.setFechaCreacion(fecha);
            realidadAumentada.setUsuarioCreacion(idUsuario);
            realidadAumentada.setNumVersion(new Long(1));
        }
        realidadAumentada.setIdMision(idMision);
        realidadAumentada.setFechaModificacion(fecha);
        realidadAumentada.setUsuarioModificacion(idUsuario);
        
        if(!(realidadAumentada.getIdBlippar()>=0&&realidadAumentada.getIdBlippar()<999999)){
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idBlippar");
        }
        try {
            em.merge(realidadAumentada);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//en caso de que la entidad no sea valida
            
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
        
        bitacoraBean.logAccion(MisionRealidadAumentada.class, idMision, idUsuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * eliminacion de detalle de realidad aumentada en una mision
     * 
     * @param idMision id de mision
     * @param idUsuario id admin
     * @param locale locale
     */
    public void removeRealidadAumentadaMision(String idMision, String idUsuario, Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        MisionRealidadAumentada misionRealidadAumentada = em.find(MisionRealidadAumentada.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (misionRealidadAumentada==null) {
            return;
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.REALIDAD_AUMENTADA.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }
        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }
        em.remove(misionRealidadAumentada);
        em.flush();
        bitacoraBean.logAccion(MisionRealidadAumentada.class, idMision, idUsuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }
    
}
