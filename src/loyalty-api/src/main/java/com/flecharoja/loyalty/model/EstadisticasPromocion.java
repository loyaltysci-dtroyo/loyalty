package com.flecharoja.loyalty.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class EstadisticasPromocion {
    
    @XmlRootElement
    public class Periodo {
        private String mes;
        private long cantMarcasPromedio;
        private long cantElegiblesPromedio;
        private long cantVistasTotales;
        private long cantVistasUnicas;

        public Periodo(String mes, long cantMarcasPromedio, long cantElegiblesPromedio, long cantVistasTotales, long cantVistasUnicas) {
            this.mes = mes;
            this.cantMarcasPromedio = cantMarcasPromedio;
            this.cantElegiblesPromedio = cantElegiblesPromedio;
            this.cantVistasTotales = cantVistasTotales;
            this.cantVistasUnicas = cantVistasUnicas;
        }

        public String getMes() {
            return mes;
        }

        public void setMes(String mes) {
            this.mes = mes;
        }

        public long getCantMarcasPromedio() {
            return cantMarcasPromedio;
        }

        public void setCantMarcasPromedio(long cantMarcasPromedio) {
            this.cantMarcasPromedio = cantMarcasPromedio;
        }

        public long getCantElegiblesPromedio() {
            return cantElegiblesPromedio;
        }

        public void setCantElegiblesPromedio(long cantElegiblesPromedio) {
            this.cantElegiblesPromedio = cantElegiblesPromedio;
        }

        public long getCantVistasTotales() {
            return cantVistasTotales;
        }

        public void setCantVistasTotales(long cantVistasTotales) {
            this.cantVistasTotales = cantVistasTotales;
        }

        public long getCantVistasUnicas() {
            return cantVistasUnicas;
        }

        public void setCantVistasUnicas(long cantVistasUnicas) {
            this.cantVistasUnicas = cantVistasUnicas;
        }
    }
    
    private String idPromocion;
    private String nombrePromocion;
    private String nombreInternoPromocion;
    
    private long cantMiembrosElegibles;
    private long cantVistasUnicasElegibles;
    private long cantVistasTotales;
    
    private List<Periodo> periodos;

    public EstadisticasPromocion() {
        periodos = new ArrayList<>();
    }

    public String getIdPromocion() {
        return idPromocion;
    }

    public void setIdPromocion(String idPromocion) {
        this.idPromocion = idPromocion;
    }

    public String getNombrePromocion() {
        return nombrePromocion;
    }

    public void setNombrePromocion(String nombrePromocion) {
        this.nombrePromocion = nombrePromocion;
    }

    public String getNombreInternoPromocion() {
        return nombreInternoPromocion;
    }

    public void setNombreInternoPromocion(String nombreInternoPromocion) {
        this.nombreInternoPromocion = nombreInternoPromocion;
    }

    public long getCantMiembrosElegibles() {
        return cantMiembrosElegibles;
    }

    public void setCantMiembrosElegibles(long cantMiembrosElegibles) {
        this.cantMiembrosElegibles = cantMiembrosElegibles;
    }

    public long getCantVistasUnicasElegibles() {
        return cantVistasUnicasElegibles;
    }

    public void setCantVistasUnicasElegibles(long cantVistasUnicasElegibles) {
        this.cantVistasUnicasElegibles = cantVistasUnicasElegibles;
    }

    public long getCantVistasTotales() {
        return cantVistasTotales;
    }

    public void setCantVistasTotales(long cantVistasTotales) {
        this.cantVistasTotales = cantVistasTotales;
    }

    public List<Periodo> getPeriodos() {
        return periodos;
    }

    public void setPeriodos(List<Periodo> periodos) {
        this.periodos = periodos;
    }
    
    public void addPeriodo(String mes, long cantMarcasPromedio, long cantElegiblesPromedio, long cantVistasTotales, long cantVistasUnicas) {
        this.periodos.add(new Periodo(mes, cantMarcasPromedio, cantElegiblesPromedio, cantVistasTotales, cantVistasUnicas));
    }
}
