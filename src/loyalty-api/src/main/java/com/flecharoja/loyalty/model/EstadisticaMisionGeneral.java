/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "ESTADISTICA_MISION_GENERAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadisticaMisionGeneral.findAll", query = "SELECT e FROM EstadisticaMisionGeneral e")
    , @NamedQuery(name = "EstadisticaMisionGeneral.findByCantMisionesEncuesta", query = "SELECT e FROM EstadisticaMisionGeneral e WHERE e.cantMisionesEncuesta = :cantMisionesEncuesta")
    , @NamedQuery(name = "EstadisticaMisionGeneral.findByCantMisionesJuego", query = "SELECT e FROM EstadisticaMisionGeneral e WHERE e.cantMisionesJuego = :cantMisionesJuego")
    , @NamedQuery(name = "EstadisticaMisionGeneral.findByCantMisionesPerfil", query = "SELECT e FROM EstadisticaMisionGeneral e WHERE e.cantMisionesPerfil = :cantMisionesPerfil")
    , @NamedQuery(name = "EstadisticaMisionGeneral.findByCantMisionesPreferencia", query = "SELECT e FROM EstadisticaMisionGeneral e WHERE e.cantMisionesPreferencia = :cantMisionesPreferencia")
    , @NamedQuery(name = "EstadisticaMisionGeneral.findByCantMisionesVerContenido", query = "SELECT e FROM EstadisticaMisionGeneral e WHERE e.cantMisionesVerContenido = :cantMisionesVerContenido")
    , @NamedQuery(name = "EstadisticaMisionGeneral.findByCantMisionesSubirContenido", query = "SELECT e FROM EstadisticaMisionGeneral e WHERE e.cantMisionesSubirContenido = :cantMisionesSubirContenido")
    , @NamedQuery(name = "EstadisticaMisionGeneral.findByCantMisionesSocial", query = "SELECT e FROM EstadisticaMisionGeneral e WHERE e.cantMisionesSocial = :cantMisionesSocial")
    , @NamedQuery(name = "EstadisticaMisionGeneral.findById", query = "SELECT e FROM EstadisticaMisionGeneral e WHERE e.id = :id")})
public class EstadisticaMisionGeneral implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "CANT_MISIONES_ENCUESTA")
    private Integer cantMisionesEncuesta;
    @Column(name = "CANT_MISIONES_JUEGO")
    private Integer cantMisionesJuego;
    @Column(name = "CANT_MISIONES_PERFIL")
    private Integer cantMisionesPerfil;
    @Column(name = "CANT_MISIONES_PREFERENCIA")
    private Integer cantMisionesPreferencia;
    @Column(name = "CANT_MISIONES_VER_CONTENIDO")
    private Integer cantMisionesVerContenido;
    @Column(name = "CANT_MISIONES_SUBIR_CONTENIDO")
    private Integer cantMisionesSubirContenido;
    @Column(name = "CANT_MISIONES_SOCIAL")
    private Integer cantMisionesSocial;

    public EstadisticaMisionGeneral() {
    }

    public EstadisticaMisionGeneral(Integer id) {
        this.id = id;
    }

    public Integer getCantMisionesEncuesta() {
        return cantMisionesEncuesta;
    }

    public void setCantMisionesEncuesta(Integer cantMisionesEncuesta) {
        this.cantMisionesEncuesta = cantMisionesEncuesta;
    }

    public Integer getCantMisionesJuego() {
        return cantMisionesJuego;
    }

    public void setCantMisionesJuego(Integer cantMisionesJuego) {
        this.cantMisionesJuego = cantMisionesJuego;
    }

    public Integer getCantMisionesPerfil() {
        return cantMisionesPerfil;
    }

    public void setCantMisionesPerfil(Integer cantMisionesPerfil) {
        this.cantMisionesPerfil = cantMisionesPerfil;
    }

    public Integer getCantMisionesPreferencia() {
        return cantMisionesPreferencia;
    }

    public void setCantMisionesPreferencia(Integer cantMisionesPreferencia) {
        this.cantMisionesPreferencia = cantMisionesPreferencia;
    }

    public Integer getCantMisionesVerContenido() {
        return cantMisionesVerContenido;
    }

    public void setCantMisionesVerContenido(Integer cantMisionesVerContenido) {
        this.cantMisionesVerContenido = cantMisionesVerContenido;
    }

    public Integer getCantMisionesSubirContenido() {
        return cantMisionesSubirContenido;
    }

    public void setCantMisionesSubirContenido(Integer cantMisionesSubirContenido) {
        this.cantMisionesSubirContenido = cantMisionesSubirContenido;
    }

    public Integer getCantMisionesSocial() {
        return cantMisionesSocial;
    }

    public void setCantMisionesSocial(Integer cantMisionesSocial) {
        this.cantMisionesSocial = cantMisionesSocial;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadisticaMisionGeneral)) {
            return false;
        }
        EstadisticaMisionGeneral other = (EstadisticaMisionGeneral) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.EstadisticaMisionGeneral[ id=" + id + " ]";
    }

}
