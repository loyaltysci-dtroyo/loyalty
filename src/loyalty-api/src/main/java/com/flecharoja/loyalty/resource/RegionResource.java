/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Region;
import com.flecharoja.loyalty.service.RegionBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * clase con recursos http para el manejo de regiones
 * @author wtencio
 */
@Api(value = "Region")
@Path("region")
public class RegionResource {
    
    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizaciones
    
    @EJB
    RegionBean regionBean;//EJB con los metodos de negocio para el manejo de regiones
    
    @Context
    SecurityContext context;//permite obtener informacion del usuario en sesion
    
    @Context
    HttpServletRequest request;
    
    
     /**
     * Metodo que acciona una tarea, la cual obtiene una region relacionada
     * al id que ingresa por parametro en el caso de error al ejecutar la tarea
     * se retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param idRegion identificador unico de la ubicacion
     * @return Lista de ubicaciones en formato JSON
     */
    @ApiOperation(value = "Obtener una region por su identificador",
            response = Region.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idRegion}")
    @Produces(MediaType.APPLICATION_JSON)
    public Region getRegionPorId(
            @ApiParam(value = "Identificador de la region", required = true)
            @PathParam("idRegion") String idRegion) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.UBICACIONES_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return regionBean.getRegionId(idRegion, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual inserta una nueva region dentro
     * de la base de datos, en el caso de error al ejecutar la tarea se retorna
     * un 500 (error interno del servidor) con un mensaje en el encabezado, de
     * no ser asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param nombreRegion objeto con la información a registrar
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Registrar una nueva region",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertRegion(
            @ApiParam(value = "Información de la region", required = true)
            String nombreRegion) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.UBICACIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return regionBean.insertRegion(nombreRegion, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual actualiza una region ya
     * existente dentro de la base de datos, en el caso de error al ejecutar la
     * tarea se retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param region objeto con la información a actualizar
     * @return Resultado de la operacion
     */
    @ApiOperation(value = "Modificación de una region")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void editRegion(
            @ApiParam(value = "Información de la region", required = true)
            Region region) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.UBICACIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        regionBean.updateRegion(region, usuarioContext,request.getLocale());
    }

   

    /**
     * Metodo que acciona una tarea, la cual elimina una region, en el caso de error al ejecutar la tarea
     * se retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param idRegion identificador unico de la region a eliminar el
     * estado
     * @return Resultado de la operacion
     */
    @ApiOperation(value = "Eliminación de una region")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idRegion}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeRegion(
            @ApiParam(value = "Identificador de la region")
            @PathParam("idRegion") String idRegion) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.UBICACIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        regionBean.deleteRegion(idRegion, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de regiones a
     * partir de una serie de palabras claves, en el caso de error al ejecutar
     * la tarea se retorna un 500 (error interno del servidor) con un mensaje en
     * el encabezado, de no ser asi se retorna un 200 (OK) con un resultado de
     * ser requerido este.
     *
     * @param busqueda Cadena de texto conformada de palabras claves
     * @param cantidad Parametro opcional con un valor por defecto de 10 que define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer registro desde donde devolver el listado de entidades
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Buscar regiones",
            responseContainer = "List",
            response = Region.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("buscar/{busqueda}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchRegiones(
            @ApiParam(value = "Palabra de búsqueda")
            @PathParam("busqueda") String busqueda, 
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.UBICACIONES_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
         Map<String, Object> respuesta = regionBean.searchRegiones(busqueda,cantidad,registro,request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Redireccion del metodo buscar sin ninguna cadena de texto a metodo
     * obtener regiones
     * @param cantidad Parametro opcional con un valor por defecto de 10 que define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer registro desde donde devolver el listado de entidades
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Buscar regiones",
            responseContainer = "List",
            response = Region.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("buscar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchRegiones(
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        return this.getRegiones(cantidad,registro);
    }

    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de todas las
     * regiones en el caso de error al ejecutar la tarea se
     * retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *

     * @param cantidad Parametro opcional con un valor por defecto de 10 que define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer registro desde donde devolver el listado de entidades
     * @return Representacion del listado en formato JSON
     */
    @ApiOperation(value = "Obtener regiones",
            responseContainer = "List",
            response = Region.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRegiones(
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.UBICACIONES_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = regionBean.getRegiones(cantidad,registro,request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }
}
