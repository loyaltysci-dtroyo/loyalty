package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Preferencia;
import com.flecharoja.loyalty.model.RespuestaPreferencia;
import com.flecharoja.loyalty.model.RespuestaPreferenciaPK;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.Busquedas;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * respuestas de miembros sobre preferencias
 *
 * @author svargas
 */
@Stateless
public class RespuestaPreferenciaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    AtributoMiembroBean atributoMiembroBean;//EJB con los metodos de negocio para el manejo de atributos de miembros

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene listado de respuestas a preferencias registrados de un
     * miembro
     *
     * @param idMiembro Identificador de un miembro
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return Lista de todas las respuesta-preferencias
     */
    public Map<String, Object> getRespuestasPreferenciasMiembro(String idMiembro, List<String> tipoRespuesta, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Preferencia> root = query.from(Preferencia.class);

        query = Busquedas.getCriteriaQueryPreferencia(cb, query, root, tipoRespuesta, busqueda, filtros, ordenTipo, ordenCampo);

        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        List<RespuestaPreferencia> respuestaMiembro = em.createNamedQuery("RespuestaPreferencia.findByIdMiembro").setParameter("idMiembro", idMiembro).getResultList();
        for (Object object : resultado) {
            Preferencia preferencia = (Preferencia) object;
            for (RespuestaPreferencia respuestaPreferencia : respuestaMiembro) {
                {
                    if (preferencia.getIdPreferencia().equals(respuestaPreferencia.getRespuestaPreferenciaPK().getIdPreferencia())) {
                        preferencia.setRespuestaMiembro(respuestaPreferencia.getRespuesta());
                    }
                }
            }
        }
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Metodo que obtiene un listado de todas respuestas-preferencias
     * registradas de todos los miembros
     *
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return Listado de los las respuestas-preferencias
     */
    public Map<String, Object> getRespuestasPreferencias(int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        List registros = new ArrayList();
        int total;
        try {
            registros = em.createNamedQuery("RespuestaPreferencia.findAll").getResultList();
            total = registros.size();
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createNamedQuery("RespuestaPreferencia.findAll")
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Metodo que guarda la respuesta a una preferencia por parte de un miembro
     *
     * @param idPreferencia Identificador de la preferencia
     * @param idMiembro Identificador del miembro
     * @param respuesta Texto con el contenido de la respuesta
     * @param usuario usuario en sesión
     * @param locale
     */
    public void insertRespuestaPreferencia(String idPreferencia, String idMiembro, String respuesta, String usuario, Locale locale) {
        if (respuesta == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "respuesta");
        }

        RespuestaPreferencia respuestaPreferencia = new RespuestaPreferencia(idMiembro, idPreferencia, Calendar.getInstance().getTime(), respuesta);
        try {
            Preferencia preferencia = em.find(Preferencia.class, idPreferencia);
            if (preferencia == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "preference_not_found");
            }
            if (preferencia.getIdAtributo() != null) {
                //copia la respuesta a la tabla atributo miembro
                atributoMiembroBean.assignAtributoDinamico(preferencia.getIdAtributo().getIdAtributo(), idMiembro, respuesta, usuario, locale);
            }
            respuestaPreferencia.setMiembro((Miembro) em.find(Miembro.class, idMiembro));
            respuestaPreferencia.setPreferencia((Preferencia) em.find(Preferencia.class, idPreferencia));

            em.persist(respuestaPreferencia);
            bitacoraBean.logAccion(RespuestaPreferencia.class, idPreferencia + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }

    }

    /**
     * Metodo que elimina una respuesta registrada de una preferencia por parte
     * de un miembro
     *
     * @param idPreferencia Identificador de la preferencia
     * @param idMiembro Identificador del miembro
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeRespuestaPreferencia(String idPreferencia, String idMiembro, String usuario, Locale locale) {
        RespuestaPreferencia respuestaPreferencia = em.find(RespuestaPreferencia.class, new RespuestaPreferenciaPK(idMiembro, idPreferencia));
        if (respuestaPreferencia == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "preference_member_not_exists");
        }
        em.remove(respuestaPreferencia);
        bitacoraBean.logAccion(RespuestaPreferencia.class, idPreferencia + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);

    }

    /**
     * Método que actualiza la respuesta a una preferencia de un miembro en
     * particular
     *
     * @param idPreferencia identificador de la preferencia
     * @param idMiembro identificador del miembro
     * @param nuevaRespuesta nueva respuesta a la preferencia
     * @param usuario usuario en sesión
     */
    public void updateRespuestaPreferencia(String idPreferencia, String idMiembro, String nuevaRespuesta, String usuario, Locale locale) {
        RespuestaPreferencia respuestaPreferencia = new RespuestaPreferencia();

        try {
            List<RespuestaPreferencia> respuestas = em.createNamedQuery("RespuestaPreferencia.findByIdMiembro").setParameter("idMiembro", idMiembro).getResultList();
            if (respuestas.isEmpty()) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "preference_member_not_exists");
            }

            if (existsAsignacion(respuestas, idPreferencia)) {
                respuestaPreferencia = em.find(RespuestaPreferencia.class, new RespuestaPreferenciaPK(idMiembro, idPreferencia));

                if (nuevaRespuesta == null || nuevaRespuesta.equals("null") || "".equals(nuevaRespuesta)) {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "respuesta");
                } else {
                    respuestaPreferencia.setRespuesta(nuevaRespuesta);
                }
                em.merge(respuestaPreferencia);
                em.flush();
                bitacoraBean.logAccion(RespuestaPreferencia.class, idPreferencia + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);

            } else {
                this.insertRespuestaPreferencia(idPreferencia, idMiembro, nuevaRespuesta, usuario, locale);
            }
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que verifica la existencia o no de una asignacion de prefencia con
     * miembro
     *
     * @param respuestas lista de preferencias que tienen respuesta del miembro
     * @param idPreferencia identificador de la preferencia
     * @return resultado de la operacion
     */
    private boolean existsAsignacion(List<RespuestaPreferencia> respuestas, String idPreferencia) {
        return respuestas.stream().anyMatch((respuesta) -> (respuesta.getRespuestaPreferenciaPK().getIdPreferencia().equals(idPreferencia)));
    }

}
