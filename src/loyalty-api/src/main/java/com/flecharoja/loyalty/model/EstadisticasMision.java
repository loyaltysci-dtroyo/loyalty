package com.flecharoja.loyalty.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class EstadisticasMision {
    
    @XmlRootElement
    public class Periodo {
        private String mes;
        private long cantRespuestas;
        private long cantRespuestasAprobadas;
        private long cantRespuestasRechazadas;
        private long cantElegiblesPromedio;
        private long cantVistasTotales;
        private long cantVistasUnicas;

        public Periodo(String mes, long cantRespuestas, long cantRespuestasAprobadas, long cantRespuestasRechazadas, long cantElegiblesPromedio, long cantVistasTotales, long cantVistasUnicas) {
            this.mes = mes;
            this.cantRespuestas = cantRespuestas;
            this.cantRespuestasAprobadas = cantRespuestasAprobadas;
            this.cantRespuestasRechazadas = cantRespuestasRechazadas;
            this.cantElegiblesPromedio = cantElegiblesPromedio;
            this.cantVistasTotales = cantVistasTotales;
            this.cantVistasUnicas = cantVistasUnicas;
        }

        public String getMes() {
            return mes;
        }

        public void setMes(String mes) {
            this.mes = mes;
        }

        public long getCantRespuestas() {
            return cantRespuestas;
        }

        public void setCantRespuestas(long cantRespuestas) {
            this.cantRespuestas = cantRespuestas;
        }

        public long getCantRespuestasAprobadas() {
            return cantRespuestasAprobadas;
        }

        public void setCantRespuestasAprobadas(long cantRespuestasAprobadas) {
            this.cantRespuestasAprobadas = cantRespuestasAprobadas;
        }

        public long getCantRespuestasRechazadas() {
            return cantRespuestasRechazadas;
        }

        public void setCantRespuestasRechazadas(long cantRespuestasRechazadas) {
            this.cantRespuestasRechazadas = cantRespuestasRechazadas;
        }

        public long getCantElegiblesPromedio() {
            return cantElegiblesPromedio;
        }

        public void setCantElegiblesPromedio(long cantElegiblesPromedio) {
            this.cantElegiblesPromedio = cantElegiblesPromedio;
        }

        public long getCantVistasTotales() {
            return cantVistasTotales;
        }

        public void setCantVistasTotales(long cantVistasTotales) {
            this.cantVistasTotales = cantVistasTotales;
        }

        public long getCantVistasUnicas() {
            return cantVistasUnicas;
        }

        public void setCantVistasUnicas(long cantVistasUnicas) {
            this.cantVistasUnicas = cantVistasUnicas;
        }
    }
    
    private String idMision;
    private String nombreMision;
    
    private long cantRespuestas;
    private long cantRespuestasAprobadas;
    private long cantRespuestasRechazadas;
    
    private long cantMiembrosElegibles;
    private long cantVistasUnicasElegibles;
    private long cantVistasTotales;
    
    private List<Periodo> periodos;

    public EstadisticasMision() {
        periodos = new ArrayList<>();
    }

    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    public String getNombreMision() {
        return nombreMision;
    }

    public void setNombreMision(String nombreMision) {
        this.nombreMision = nombreMision;
    }

    public long getCantRespuestas() {
        return cantRespuestas;
    }

    public void setCantRespuestas(long cantRespuestas) {
        this.cantRespuestas = cantRespuestas;
    }

    public long getCantRespuestasAprobadas() {
        return cantRespuestasAprobadas;
    }

    public void setCantRespuestasAprobadas(long cantRespuestasAprobadas) {
        this.cantRespuestasAprobadas = cantRespuestasAprobadas;
    }

    public long getCantRespuestasRechazadas() {
        return cantRespuestasRechazadas;
    }

    public void setCantRespuestasRechazadas(long cantRespuestasRechazadas) {
        this.cantRespuestasRechazadas = cantRespuestasRechazadas;
    }

    public long getCantMiembrosElegibles() {
        return cantMiembrosElegibles;
    }

    public void setCantMiembrosElegibles(long cantMiembrosElegibles) {
        this.cantMiembrosElegibles = cantMiembrosElegibles;
    }

    public long getCantVistasUnicasElegibles() {
        return cantVistasUnicasElegibles;
    }

    public void setCantVistasUnicasElegibles(long cantVistasUnicasElegibles) {
        this.cantVistasUnicasElegibles = cantVistasUnicasElegibles;
    }

    public long getCantVistasTotales() {
        return cantVistasTotales;
    }

    public void setCantVistasTotales(long cantVistasTotales) {
        this.cantVistasTotales = cantVistasTotales;
    }

    public List<Periodo> getPeriodos() {
        return periodos;
    }

    public void setPeriodos(List<Periodo> periodos) {
        this.periodos = periodos;
    }
    
    public void addPeriodo(String mes, long cantRespuestas, long cantRespuestasAprobadas, long cantRespuestasRechazadas, long cantElegiblesPromedio, long cantVistasTotales, long cantVistasUnicas) {
        this.periodos.add(new Periodo(mes, cantRespuestas, cantRespuestasAprobadas, cantRespuestasRechazadas, cantElegiblesPromedio, cantVistasTotales, cantVistasUnicas));
    }
}
