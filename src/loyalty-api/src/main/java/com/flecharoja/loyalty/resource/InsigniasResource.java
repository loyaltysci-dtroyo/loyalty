/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;


import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Insignias;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.NivelesInsignias;
import com.flecharoja.loyalty.service.InsigniasBean;
import com.flecharoja.loyalty.service.MiembroInsigniaNivelBean;
import com.flecharoja.loyalty.service.NivelesInsigniasBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase que provee de metodos http RESTful para el acceso a funcionalidades del
 * manejo de insignias y niveles de insignia
 * @author wtencio
 */
@Api(value = "Insignias")
@Path("insignias")
public class InsigniasResource {
    
    @EJB
    InsigniasBean insigniasBean; //EJB con los metodos para el manejo de insignias
    
    @EJB
    NivelesInsigniasBean nivelesInsigniasBean; // EJB con los metodos para el manejo de niveles de insignia
    
    @EJB
    MyKeycloakAuthz authzBean; // EJB con los metodos para el manejo de autorizaciones
    
    @EJB
    MiembroInsigniaNivelBean miembroInsigniaNivelBean;//EJB con los metodos para el manejo de la asociacion de miembros con insignias
    
    @Context
    SecurityContext context;// permite obtener informacion del usuario en sesion
    
    @Context
    HttpServletRequest request;
    
    
    /**
     * Metodo que manda a registrar en la bases de datos la informacion de una
     * nueva insignia y retorna el identificador de la insignia creada bajo un
     * estado de OK(200), de ocurrir un error se retornara una respuesta con un
     * diferente estado junto con un encabezado "Error-Reason" con un valor
     * numerico indicando la naturaleza del error.
     *
     * @param insignias Objeto que contenido la informacion de una nueva insignia a
     * registrar.
     * @return Información del resultado de la operacion.
     */
    @ApiOperation(value = "Registrar una insignia",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertInsignia(
            @ApiParam(value = "Información de la insignia", required = true)
            Insignias insignias) {
       String usuarioContext;
       String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
         return insigniasBean.insertInsignia(insignias, usuarioContext,request.getLocale());
    }
    
    
    /**
     * Metodo que modifica la informacion almacenada de una insignia en la bases
     * de datos y retorna una respuesta con un estado de OK(200), de ocurrir un
     * error se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param insignias Objeto que contiene la informacion de una nueva insignia a
     * registrar.
     * @return Informacion del resultado de la operacion.
     */
    @ApiOperation(value = "Modificación de una insignia")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void editInsignia(@ApiParam(value = "Información de la insignia") Insignias insignias) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        insigniasBean.updateInsignia(insignias, usuarioContext,request.getLocale());
    }
    
     /**
     * Metodo que elimina la informacion almacenada de una metrica en la bases
     * de datos y retorna una respuesta con un estado de OK(200), de ocurrir un
     * error se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param idInsignia Identificador de la metrica a remover.
     * @return Informacion del resultado de la operacion.
     */
    @ApiOperation(value = "Eliminación o desactivación de una insignia")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idInsignia}")
    public void removeInsignia(
            @ApiParam(value = "Identificador de la insignia", required = true)
            @PathParam("idInsignia") String idInsignia) {
        
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        insigniasBean.deleteInsignia(idInsignia, usuarioContext,request.getLocale());
    }
    
     /**
     * Metodo que obtiene un listado de todas las insignias por un rango
     * establecido usando los parametros de cantidad y registro bajo un estado
     * de OK(200) ademas de encabezados indicando del rango aceptable maximo y
     * el rango actual de resultados, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param estados
     * @param tipos
     * @param busqueda
     * @param filtros
     * @param ordenCampo
     * @param cantidad de registros máximo a devolver por página
     * @param ordenTipo
     * @param registro por el cual se desea empezar la búsqueda
     * @return Representacion del listado de metricas con toda su informacion en
     * formato JSON.
     */
     @ApiOperation(value = "Obtener insignias por estado o por defecto todos",
            responseContainer = "List",
            response = Insignias.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInsignias(
            @ApiParam(value = "Indicadores de estado de insignias") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de tipos de insignias") @QueryParam("tipo-insignia") List<String> tipos,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros  sobre donde aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {        
         String token;
         try{
             token = request.getHeader("Authorization").substring(6);
         }catch(NullPointerException e){
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
         }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = insigniasBean.getInsignias(estados, tipos, busqueda, filtros, ordenTipo, ordenCampo, cantidad, registro, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }
    
    
     /**
     * Metodo que obtiene la informacion de una metrica especifica segun su
     * numero de identificacion bajo un estado de OK(200), de ocurrir un error
     * se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param id Numero de identificacion de la metrica deseada.
     * @return Representacion de la informacion de una metrica encontrada en
     * formato JSON.
     */
     @ApiOperation(value = "Obtener una insignia por su identificador",
            response = Insignias.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idInsignia}")
    @Produces(MediaType.APPLICATION_JSON)
    public Insignias getInsigniaPorId(
            @ApiParam(value = "Identificador de la insignia")
            @PathParam("idInsignia") String id) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return insigniasBean.getInsigniaById(id,request.getLocale());
    }
   
    @GET
    @Path("buscar/{busqueda}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchInsignias(
            @PathParam("busqueda") String busqueda,
            @QueryParam("estado") String estado,
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @QueryParam("registro") int registro) {
        
        return this.getInsignias(estado==null?null:Arrays.asList(estado.split("-")), null, busqueda, null, null, null, cantidad, registro);
    }

    @GET
    @Path("buscar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchInsignias(
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @QueryParam("registro") int registro,
            @QueryParam("estado") String estado) {
        return this.getInsignias(estado==null?null:Arrays.asList(estado.split("-")), null, null, null, null, null, cantidad, registro);
    }
    
    
     /**
     * Metodo que obtiene en respuesta de true/false si un nombre interno ya
     * esta almacenado bajo un estado de OK(200), de ocurrir un error se
     * retornara una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param nombre Valor del nombre interno a verificar
     * @return Palabra clave con los siguientes posibles valores: "true" Nombre
     * interno existe, "false" Nombre interno no existe
     */
    @ApiOperation(value = "Verificacion de existencia de nombre interno de una insignia")
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("/exists/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean existsNombreInterno(
            @ApiParam(value = "Nombre de la insignia")
            @PathParam("nombre") String nombre) {
       
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return insigniasBean.existsNombreInterno(nombre);
    }
    
    
    
    
     //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++ Manejo de niveles_insignias
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
     /**
     * Metodo que almacena la informacion de un nuevo nivel de insignia, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idInsignia identificador de la insignia
     * @param nivel Objeto con la informacion de un nuevo nivel de insignia
     * @return Respuesta con el identificador del nivel creado
     */
    @ApiOperation(value = "Registrar un nuevo nivel a un grupo de niveles",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idInsignia}/niveles")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertNivelInsignia(
            @ApiParam(value = "Identificador de la insignia", required = true)
            @PathParam("idInsignia") String idInsignia, 
            @ApiParam(value = "Información del nivel de insignia", required = true)
            NivelesInsignias nivel) {
       
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return nivelesInsigniasBean.insertNivelInsignia(nivel, usuarioContext,idInsignia,request.getLocale());
    }
    
    /**
     * Metodo que modifica la informacion de un nivel de insignia existente, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param nivel Objeto con la informacion del nivel de insignias
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Modificación de un nivel de insignias")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("{idInsignia}/niveles")
    @Consumes(MediaType.APPLICATION_JSON)
    public void editNivelInsignia(
            @ApiParam(value = "Información del nivel", required = true)
            NivelesInsignias nivel) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        nivelesInsigniasBean.updateNivelInsignia(nivel, usuarioContext,request.getLocale());
    }
    
    
     /**
     * Metodo que elimina la informacion de un nivel de insignia existente a
     * partir de su identificador, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param idNivel Identificador del nivel de insignia
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminación de un nivel de una insignia")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idInsignia}/niveles/{idNivel}")
    public void removeNivel(
            @ApiParam(value = "Identificador del nivel", required = true)
            @PathParam("idNivel") String idNivel) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        nivelesInsigniasBean.deleteNivelInsignia(idNivel, usuarioContext,request.getLocale());
    }
    
    
     /**
     * Metodo que obtiene la informacion de un nivel de insignia existente a
     * partir de su identificador, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param idInsignia identificador de la insignia al que pertenece el nivel
     * @param idNivel Identificador del nivel de insignia
     * @return Respuesta OK con la informacion del nivel de insignia encontrado
     */
     @ApiOperation(value = "Obtener un nivel de una insignia",
            response = NivelesInsignias.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idInsignia}/niveles/{idNivel}")
    @Produces(MediaType.APPLICATION_JSON)
    public NivelesInsignias getNivelPorId(
            @ApiParam(value = "Identificador de la insignia", required = true)
            @PathParam("idInsignia") String idInsignia, 
            @ApiParam(value = "Identificador del nivel", required = true)
            @PathParam("idNivel") String idNivel) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return nivelesInsigniasBean.getNivel(idNivel,idInsignia,request.getLocale());
    }
    
    /**
     * Metodo que obtiene el listado total de niveles de metricas existente, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idInsignia identificador de la insignia
     * @return Respuesta OK con el listado de los niveles de metrica
     */
    @ApiOperation(value = "Obtener niveles de una insignia",
            responseContainer = "List",
            response = NivelesInsignias.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idInsignia}/niveles")
    @Produces(MediaType.APPLICATION_JSON)
    public List<NivelesInsignias> getNiveles(
            @ApiParam(value = "Identificador del grupo de niveles", required = true)
            @PathParam("idInsignia") String idInsignia) {
        String token = null;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return nivelesInsigniasBean.getNivelesInsignias(idInsignia);
    }
    
    /**
     * Metodo que obtiene en respuesta de true/false si un nombre interno ya
     * esta almacenado bajo un estado de OK(200), de ocurrir un error se
     * retornara una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param nombre Valor del nombre interno a verificar
     * @return Palabra clave con los siguientes posibles valores: "true" Nombre
     * interno existe, "false" Nombre interno no existe
     */
    @ApiOperation(value = "Verificacion de existencia de nombre interno de un nivel de insignia")
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("niveles/exists/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean existsNombreInternoInsignia(
            @ApiParam(value = "Nombre del nivel de insignia")
            @PathParam("nombre") String nombre) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return nivelesInsigniasBean.existsNombreInterno(nombre);
    }
    
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++ GET Miembros por insignia
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    
     
    /**
     * Metodo acciona una tarea que obtiene un listado de miembris que han sido
     * asignados a una insignia, en el caso de error al ejecutar la tarea se
     * retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param idInsignia identificador unico de la insignia
     * @param tipo
     * @param estados
     * @param generos
     * @param estadosCiviles
     * @param cantidad Parametro opcional con un valor por defecto de 10 que define la cantidad maxima de registros por respuesta
     * @param contactoEmail
     * @param contactoSMS
     * @param contactoNotificacion
     * @param contactoEstado
     * @param educaciones
     * @param tieneHijos
     * @param registro Parametro opcional que define el numero de primer registro desde donde devolver el listado de entidades
     * @param busqueda
     * @param ordenTipo
     * @param filtros
     * @param ordenCampo
     * @return Representacion en JSON del listado de miembros
     */
    @ApiOperation(value = "Obtener miembros asociados a una insignia",
            responseContainer = "List",
            response = Miembro.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idInsignia}/miembros")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMiembrosInsignia(
            @ApiParam(value = "Identificador de la insignia", required = true)
            @PathParam("idInsignia") String idInsignia,
            @ApiParam(value = "Indicador del tipo de lista de miembros") @QueryParam("tipo") String tipo,
            @ApiParam(value = "Indicadores de estado de miembros") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de genero de miembros") @QueryParam("genero") List<String> generos,
            @ApiParam(value = "Indicadores de estado civil de miembros") @QueryParam("estado-civil") List<String> estadosCiviles,
            @ApiParam(value = "Indicadores de educacion de miembros") @QueryParam("educacion") List<String> educaciones,
            @ApiParam(value = "Indicador booleano de contacto de email") @QueryParam("contacto-email") String contactoEmail,
            @ApiParam(value = "Indicador booleano de contacto de sms") @QueryParam("contacto-sms") String contactoSMS,
            @ApiParam(value = "Indicador booleano de contacto de notificacion") @QueryParam("contacto-notificacion") String contactoNotificacion,
            @ApiParam(value = "Indicador booleano de contacto de estado") @QueryParam("contacto-estado") String contactoEstado,
            @ApiParam(value = "Indicador booleano de tiene hijos") @QueryParam("tiene-hijos") String tieneHijos,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad) {
   
        
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.INSIGNIAS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = miembroInsigniaNivelBean.getMiembrosInsignias(idInsignia, tipo, estados, generos, estadosCiviles, educaciones, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }
    
  
    @GET
    @Path("{idInsignia}/no-miembros")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMiembrosNoInsignia(
            @ApiParam(value = "Identificador de la insignia", required = true)
            @PathParam("idInsignia") String idInsignia,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad) {
        return this.getMiembrosInsignia(idInsignia, Indicadores.DISPONIBLES+"", null, null, null, null, null, null, null, null, null, null, null, null, null, registro, cantidad);
    }
}
