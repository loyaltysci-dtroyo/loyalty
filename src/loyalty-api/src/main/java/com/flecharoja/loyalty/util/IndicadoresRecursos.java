/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.util;

/**
 *
 * @author wtencio
 */
public class IndicadoresRecursos {
    
    // Certificados
    public static final String ANULAR_CERTIFICADO = "Anular Certificado";
    // Apuestas (Mundo V)
    public static final String APUESTAS_ESCRITURA = "Apuestas Escritura";
    public static final String APUESTAS_LECTURA = "Apuestas Lectura";
    // Atributos dinámicos de miembro
    public static final String ATRIBUTOS_LECTURA = "Atributos Lectura";
    public static final String ATRIBUTOS_ESCRITURA = "Atributos Escritura";
    // Atributos de producto
    public static final String ATRIBUTOS_PRODUCTO_ESCRITURA = "Atributos Producto Escritura";
    public static final String ATRIBUTOS_PRODUCTO_LECTURA = "Atributos Producto Lectura";
    // Notificaciones
    public static final String CAMPANAS_ESCRITURA = "Campanas Escritura";
    public static final String CAMPANAS_LECTURA = "Campanas Lectura";
    // Categorías de misiones
    public static final String CATEGORIAS_ESCRITURA = "Categorias Escritura";
    public static final String CATEGORIAS_LECTURA = "Categorias Lectura";
    // Categorías de premio
    public static final String CATEGORIAS_PREMIO_ESCRITURA = "Categorias Premio Escritura";
    public static final String CATEGORIAS_PREMIO_LECTURA = "Categorias Premio Lectura";
    // Categorias de producto
    public static final String CATEGORIAS_PRODUCTO_ESCRITURA = "Categorias Producto Escritura";
    public static final String CATEGORIAS_PRODUCTO_LECTURA = "Categorias Producto Lectura";
    // Categorias de promoción
    public static final String CATEGORIAS_PROMOCION_ESCRITURA = "Categorias Promocion Escritura";
    public static final String CATEGORIAS_PROMOCION_LECTURA = "Categorias Promocion Lectura";
    // Cateorias de noticia
    public static final String CATEGORIAS_NOTICIA_LECTURA = "Categorias Noticia Lectura";
    public static final String CATEGORIAS_NOTICIA_ESCRITURA = "Categorias Noticia Escritura";
    // Configuracion del sistema
    public static final String CONFIGURACION_GENERAL = "Configuracion General";
    // Documentos
    public static final String DOCUMENTOS_ESCRITURA = "Documentos Escritura";
    public static final String DOCUMENTOS_LECTURA = "Documentos Lectura";
    // Grupos de miembro
    public static final String GRUPOS_ESCRITURA = "Grupos Escritura";
    public static final String GRUPOS_LECTURA = "Grupos Lectura";
    // Insignias 
    public static final String INSIGNIAS_ESCRITURA = "Insignias Escritura";
    public static final String INSIGNIAS_LECTURA = "Insignias Lectura";
    // Tabla de posiciones
    public static final String LEADERBOARDS_ESCRITURA = "Leaderboards Escritura";
    public static final String LEADERBOARDS_LECTURA = "Leaderboards Lectura";
    // Métricas del sistema
    public static final String METRICAS_ESCRITURA = "Metricas Escritura";
    public static final String METRICAS_LECTURA = "Metricas Lectura";
    // Miembros finales
    public static final String MIEMBROS_ESCRITURA = "Miembros Escritura";
    public static final String MIEMBROS_LECTURA = "Miembros Lectura";
    // Misiones / Desafios
    public static final String MISIONES_ESCRITURA = "Misiones Escritura";
    public static final String MISIONES_LECTURA = "Misiones Lectura";
    // Preferencias
    public static final String PREFERENCIAS_ESCRITURA = "Preferencias Escritura";
    public static final String PREFERENCIAS_LECTURA = "Preferencias Lectura";
    // Premios
    public static final String PREMIOS_ESCRITURA = "Premios Escritura";
    public static final String PREMIOS_LECTURA = "Premios Lectura";
    // Productos
    public static final String PRODUCTOS_ESCRITURA = "Productos Escritura";
    public static final String PRODUCTOS_LECTURA = "Productos Lectura";
    // Promociones
    public static final String PROMOCIONES_ESCRITURA = "Promociones Escritura";
    public static final String PROMOCIONES_LECTURA = "Promociones Lectura";
    // Recompensas
    public static final String RECOMPENSAS_ESCRITURA = "Recompensas Escritura";
    public static final String RECOMPENSAS_LECTURA = "Recompensas Lectura";
    // Reportes 
    public static final String REPORTES = "Reportes";
    // Roles de usuario
    public static final String ROLES_ESCRITURA = "Roles Escritura";
    public static final String ROLES_LECTURA = "Roles Lectura";
    // Segmentos
    public static final String SEGMENTOS_ESCRITURA = "Segmentos Escritura";
    public static final String SEGMENTOS_LECTURA = "Segmento Lectura";
    public static final String SEGMENTOS_ADMIN_REGLAS = "Segmentos Administracion Reglas";
    // Ubicaciones
    public static final String UBICACIONES_ESCRITURA = "Ubicaciones Escritura";
    public static final String UBICACIONES_LECTURA = "Ubicaciones Lectura";
    //Usuarios
    public static final String USUARIOS_ESCRITURA = "Usuarios Escritura";
    public static final String USUARIOS_LECTURA = "Usuarios Lectura";
    public static final String CREACION_USUARIOS = "Creacion Usuarios";
    // Workflow / tareas programadas
    public static final String WORKFLOW_ESCRITURA = "Workflow Escritura";
    public static final String WORKFLOW_LECTURA = "Workflow Lectura";  
    //Acceso a Bitacoras
    public static final String BITACORA_PERMISO = "Bitacora Lectura";
    //News Feed
    public static final String NEWS_FEED_ESCRITURA = "NewsFeed Escritura";
    public static final String NEWS_FEED_LECTURA = "NewsFeed Lectura";
    //Respuestas de mision
    public static final String APROBACION_MISION = "Gestión de Respuesta Misión";
    
}
