/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.Busquedas;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * * EJB encargado de ejecutar reglas de negocio y acciones de persistencia
 * sobre ubicaciones
 *
 * @author wtencio
 */
@Stateless
public class UbicacionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que a partir de un id dado busca y retorna de ser posible la
     * ubicacion con el mismo id
     *
     * @param idUbicacion identificador de la ubicacion a buscar
     * @param locale
     * @return Objeto con la informacion de la ubicacion encontrada
     */
    public Ubicacion getUbicacionPorId(String idUbicacion, Locale locale) {

        Ubicacion ubicacion = em.find(Ubicacion.class, idUbicacion);
        if (ubicacion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
        }
        return ubicacion;

    }

    /**
     * Metodo que guarda en la base de datos la informacion de una nueva
     * ubicacion
     *
     * @param ubicacion Objeto con los valores de la nueva ubicacion
     * @param usuarioContext usuario que se encuentra en sesion
     * @param locale
     * @return resultado de la operacion
     */
    public String insertUbicacion(Ubicacion ubicacion, String usuarioContext, Locale locale) {
        
        //Se valida que el código interno no exista
        if (existsCodigoInterno(ubicacion.getCodigoInterno())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "internal_code_already_exists");
        }
        //Se valida que el nombre de despliegue no exista
        if (existsNombreDespliegue(ubicacion.getNombreDespliegue())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "deployment_name_already_exists");
        }

        //se coloca el usuario de creacion y modificacion (que en este caso que es insertar son los mismos)
        ubicacion.setUsuarioCreacion(usuarioContext);
        ubicacion.setFechaCreacion(Calendar.getInstance().getTime());

        ubicacion.setUsuarioModificacion(usuarioContext);
        ubicacion.setFechaModificacion(Calendar.getInstance().getTime());
        //por omision sera Draft
        ubicacion.setIndEstado(Ubicacion.Estados.DRAFT.getValue());
        ubicacion.setIndCalendarizacion(Ubicacion.Efectividad.PERMANENTE.getValue());
        ubicacion.setNumVersion(1L);

        try {
            em.persist(ubicacion);
            em.flush();
            bitacoraBean.logAccion(Ubicacion.class, ubicacion.getIdUbicacion(), usuarioContext, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        return ubicacion.getIdUbicacion();
    }

    /**
     * Metodo que actualiza en la base de datos la informacion de una ubicacion
     *
     * @param ubicacion Objeto con los valores de la nueva ubicacion
     * @param usuarioContext id de keycloak del usuario que se encuentra en
     * sesion
     * @param locale
     */
    public void editUbicacion(Ubicacion ubicacion, String usuarioContext, Locale locale) {

        Ubicacion temp = em.find(Ubicacion.class, ubicacion.getIdUbicacion());

        if (temp == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
        }

        if (!temp.getCodigoInterno().equals(ubicacion.getCodigoInterno())) {
            if (existsCodigoInterno(ubicacion.getCodigoInterno())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "internal_code_already_exists");
            }
        }
        if(!temp.getNombreDespliegue().equals(ubicacion.getNombreDespliegue())){
            if (existsNombreDespliegue(ubicacion.getNombreDespliegue())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "deployment_name_already_exists");
            }
        }
        
        //verifica que el estado no sea nulo
        if (Ubicacion.Estados.get(ubicacion.getIndEstado()) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indEstado");
        }

        //verificacion de que posible cambio de estado sea valido (Activo NO A Borrador) y de edicion de entidad archivada
        if ((temp.getIndEstado().equals(Ubicacion.Estados.PUBLICADO_ACTIVO.getValue()) && ubicacion.getIndEstado().equals(Ubicacion.Estados.DRAFT.getValue()))
                || (temp.getIndEstado().equals(Ubicacion.Estados.PUBLICADO_INACTIVO.getValue()) && ubicacion.getIndEstado().equals(Ubicacion.Estados.DRAFT.getValue()))
                || temp.getIndEstado().equals(Ubicacion.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "location_not_change_state");
        }

        //verificacion de valores validos en los indicadores de dias y num de semana y fechas de calendarizacion
        if (ubicacion.getIndCalendarizacion().equals(Ubicacion.Efectividad.CALENDARIZADO.getValue())) {
            if (ubicacion.getFechaInicio() == null || ubicacion.getFechaFin() == null || ubicacion.getFechaInicio().compareTo(ubicacion.getFechaFin()) > 0) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "location_badly_scheduled");
            }
            if (ubicacion.getIndSemanaRecurrencia() != null && (ubicacion.getIndSemanaRecurrencia().compareTo(1) < 0 || ubicacion.getIndSemanaRecurrencia().compareTo(52) > 0)) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indSemanaRecurrencia");
            }

            if (ubicacion.getIndDiaRecurrencia() != null) {
                char[] validValues = {'L', 'M', 'K', 'J', 'V', 'S', 'D'};
                ubicacion.setIndDiaRecurrencia(ubicacion.getIndDiaRecurrencia().toUpperCase());
                for (char validValue : validValues) {
                    if (ubicacion.getIndDiaRecurrencia().indexOf(validValue) != ubicacion.getIndDiaRecurrencia().lastIndexOf(validValue)) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indDiaRecurrencia");
                    }
                }
            }
        } else {
            if (!ubicacion.getIndCalendarizacion().equals(Ubicacion.Efectividad.PERMANENTE.getValue())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indCalendarizacion");
            }
        }

        try {
            em.merge(ubicacion);
            em.flush();
            bitacoraBean.logAccion(Ubicacion.class, ubicacion.getIdUbicacion(), usuarioContext, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {//en caso de que alguna informacion no este valida o este "vieja"
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
    }

    /**
     * Método que elimina una ubicación que se encuentra en estado borrador
     *
     * @param idUbicacion idenficador de la ubicación que se desea cambiar
     * @param usuario usuario en sesión
     * @param locale
     */
    public void removeUbicacion(String idUbicacion, String usuario, Locale locale) {

        Ubicacion ubicacion = em.find(Ubicacion.class, idUbicacion);
        if (ubicacion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
        }
        if (ubicacion.getIndEstado().equals(Ubicacion.Estados.DRAFT.getValue())) {
            em.remove(ubicacion);
            bitacoraBean.logAccion(Ubicacion.class, ubicacion.getIdUbicacion(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        } else if (ubicacion.getIndEstado().equals(Ubicacion.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Ubicacion");
        } else {
            ubicacion.setFechaModificacion(Calendar.getInstance().getTime());
            ubicacion.setUsuarioModificacion(usuario);
            ubicacion.setIndEstado(Ubicacion.Estados.ARCHIVADO.getValue());
            em.merge(ubicacion);
            bitacoraBean.logAccion(Ubicacion.class, ubicacion.getIdUbicacion(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        }

    }

    /**
     * Metodo que obtiene una lista de ubicaciones almacenados por estado(s)
     *
     * @param estados
     * @param calendarizaciones
     * @param busqueda
     * @param filtros
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @param ordenCampo
     * @param ordenTipo
     * @return Lista de usuarios
     */
    public Map<String, Object> getUbicaciones(List<String> estados, List<String> calendarizaciones, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Ubicacion> root = query.from(Ubicacion.class);

        query = Busquedas.getCriteriaQueryUbicaciones(cb, query, root, estados, calendarizaciones, busqueda, filtros, ordenTipo, ordenCampo);
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }
    
    /** 
     * Método que verifica si el nombre interno existe
     */
    public boolean existsCodigoInterno(String codigo) {
        return ((Long) em.createNamedQuery("Ubicacion.existsCodigoInterno").setParameter("codigoInterno", codigo).getSingleResult()) > 0;
    }
    
    /** 
     * Método que verifica si el nombre de despliegue existe
     */
    public boolean existsNombreDespliegue(String nombre) {
        return ((Long) em.createNamedQuery("Ubicacion.existsNombreDespliegue").setParameter("nombreDespliegue", nombre).getSingleResult()) > 0;
    }

    //GENERAR CON COD.INTERNO O IDUBICACION???
    public String generarQrUbicacion(String idUbicacion, Locale locale) {
        int qr_image_width = 400;
        int qr_image_height = 400;
        String IMAGE_FORMAT = "png";
        // URL to be encoded
        String data = idUbicacion;

        // Encode URL in QR format
        BitMatrix matrix;
        Writer writer = new QRCodeWriter();

        try {
            matrix = writer.encode(data, BarcodeFormat.QR_CODE, qr_image_width, qr_image_height);
        } catch (WriterException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "error_qr");
        }

        // Create buffered image to draw to
        BufferedImage image = new BufferedImage(qr_image_width,
                qr_image_height, BufferedImage.TYPE_INT_RGB);

        // Iterate through the matrix and draw the pixels to the image
        for (int y = 0;
                y < qr_image_height;
                y++) {
            for (int x = 0; x < qr_image_width; x++) {
                int grayValue = (matrix.get(x, y) ? 0 : 1) & 0xff;
                image.setRGB(x, y, (grayValue == 0 ? 0 : 0xFFFFFF));
            }
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, IMAGE_FORMAT, baos);
        } catch (IOException ex) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "error_qr");
        }
        String encoded;
        try {
            encoded = Base64.getEncoder().encodeToString(baos.toByteArray());
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "file_invalid");
        }
        return encoded;
    }
}
