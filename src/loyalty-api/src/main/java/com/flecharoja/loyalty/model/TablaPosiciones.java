/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "TABLA_POSICIONES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TablaPosiciones.findAll", query = "SELECT t FROM TablaPosiciones t"),
    @NamedQuery(name = "TablaPosiciones.findByIdMetrica", query = "SELECT t FROM TablaPosiciones t WHERE t.idMetrica.idMetrica = :idMetrica"),
    @NamedQuery(name = "TablaPosiciones.countAll", query = "SELECT COUNT(t.idTabla) FROM TablaPosiciones t"),
    @NamedQuery(name = "TablaPosiciones.findByIdTabla", query = "SELECT t FROM TablaPosiciones t WHERE t.idTabla = :idTabla"),
    @NamedQuery(name = "TablaPosiciones.getImagenFromTabla", query = "SELECT t.imagen FROM TablaPosiciones t WHERE t.idTabla = :idTabla"),
    @NamedQuery(name = "TablaPosiciones.findByTipoByMetrica" , query = "SELECT t FROM TablaPosiciones t  WHERE t.idMetrica.idMetrica = :idMetrica AND t.indTipo = :indTipo"),
    @NamedQuery(name = "TablaPosiciones.existeLeaderboard" , query = "SELECT COUNT(t) FROM TablaPosiciones t  WHERE t.idMetrica.idMetrica = :idMetrica AND t.indTipo = :indTipo")
})
public class TablaPosiciones implements Serializable {

    public enum Calculo{
        SUMA('S'),
        PROMEDIO('P');
        
        private final char value;
        private static final Map<Character,Calculo> lookup = new HashMap<>();

        private Calculo(char value) {
            this.value = value;
        }
        
        static{
            for(Calculo calculo :values()){
                lookup.put(calculo.value, calculo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Calculo get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Tipo{
        MIEMBROS('U'),
        GRUPO('G'),
        GRUPO_ESPECIFICO('E');
        
        private final char value;
        private static final Map<Character, Tipo> lookup = new HashMap<>();

        private Tipo(char value) {
            this.value = value;
        }
        
        static{
            for(Tipo tipo:values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipo get(Character value){
            return value==null?null:lookup.get(value);
        }
        
        
    }
    
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(generator = "tablaposiciones_uuid")
    @GenericGenerator(name = "tablaposiciones_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_TABLA")
    private String idTabla;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    private String imagen;

    @Size(min = 1, max = 500)
    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "IND_GRUPAL_FORMA_CALCULO")
    private Character indGrupalFormaCalculo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "IND_FORMATO")
    private String indFormato;

    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    @JoinColumn(name = "ID_GRUPO", referencedColumnName = "ID_GRUPO")
    @ManyToOne(optional = false)
    private Grupo idGrupo;

    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    private Metrica idMetrica;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tablaPosiciones")
    private List<TabposGrupomiembro> tablaPosicionGrupos;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tablaPosiciones")
    private List<TabposMiembro> tablaPosicionMiembros;

    public TablaPosiciones() {
    }

    public TablaPosiciones(Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public String getIdTabla() {
        return idTabla;
    }

    public void setIdTabla(String idTabla) {
        this.idTabla = idTabla;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo == null ? null : Character.toUpperCase(indTipo);
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }


    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getIndGrupalFormaCalculo() {
        return indGrupalFormaCalculo;
    }

    public void setIndGrupalFormaCalculo(Character indGrupalFormaCalculo) {
        this.indGrupalFormaCalculo = indGrupalFormaCalculo == null ? null :Character.toUpperCase(indGrupalFormaCalculo);
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }
    
    public String getIndFormato() {
        return indFormato;
    }

    public void setIndFormato(String indFormato) {
        this.indFormato = indFormato;
    }

    public Grupo getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Grupo idGrupo) {
        this.idGrupo = idGrupo;
    }
    
    @XmlTransient
    @ApiModelProperty(hidden = true)
    public List<TabposGrupomiembro> getTablaPosicionGrupos() {
        return tablaPosicionGrupos;
    }
    
    public void setTablaPosicionGrupos(List<TabposGrupomiembro> tablaPosicionGrupos) {
        this.tablaPosicionGrupos = tablaPosicionGrupos;
    }
    
    @XmlTransient
    @ApiModelProperty(hidden = true)
    public List<TabposMiembro> getTablaPosicionMiembros() {
        return tablaPosicionMiembros;
    }

    public void setTablaPosicionMiembros(List<TabposMiembro> tablaPosicionMiembros) {
        this.tablaPosicionMiembros = tablaPosicionMiembros;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTabla != null ? idTabla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TablaPosiciones)) {
            return false;
        }
        TablaPosiciones other = (TablaPosiciones) object;
        if ((this.idTabla == null && other.idTabla != null) || (this.idTabla != null && !this.idTabla.equals(other.idTabla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TablaPosiciones{" + "idTabla=" + idTabla + ", nombre=" + nombre + ", indTipo=" + indTipo + ", imagen=" + imagen + ", descripcion=" + descripcion + ", indGrupalFormaCalculo=" + indGrupalFormaCalculo + ", fechaCreacion=" + fechaCreacion + ", usuarioCreacion=" + usuarioCreacion + ", fechaModificacion=" + fechaModificacion + ", usuarioModificacion=" + usuarioModificacion + ", indFormato=" + indFormato + ", numVersion=" + numVersion + ", idGrupo=" + idGrupo + ", idMetrica=" + idMetrica + '}';
    }

   

    

}
