package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas, wtencio
 */
@Entity
@Table(name = "METRICA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Metrica.findAll", query = "SELECT m FROM Metrica m ORDER BY m.fechaModificacion DESC"),
    @NamedQuery(name = "Metrica.countAll", query = "SELECT COUNT(m.idMetrica) FROM Metrica m"),
    @NamedQuery(name = "Metrica.findByIdMetrica", query = "SELECT m FROM Metrica m WHERE m.idMetrica = :idMetrica"),
    @NamedQuery(name = "Metrica.findByIndEstado", query = "SELECT m FROM Metrica m WHERE m.indEstado = :indEstado ORDER BY m.fechaModificacion DESC"),
    @NamedQuery(name = "Metrica.countByNombreInterno", query = "SELECT COUNT(m.idMetrica) FROM Metrica m WHERE m.nombreInterno = :nombreInterno"),
    @NamedQuery(name = "Metrica.ByIdGrupo", query = "SELECT m FROM Metrica m WHERE m.grupoNiveles.idGrupoNivel = :idGrupoNivel ORDER BY m.fechaModificacion DESC"),
    @NamedQuery(name = "Metrica.getIdMetricaByIndEstado", query = "SELECT m.idMetrica FROM Metrica m WHERE m.indEstado = :indEstado")
})
public class Metrica implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_EXPIRACION")
    private Boolean indExpiracion;
    
    @Version
    @Basic(optional = false)
    @NotNull()
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @OneToMany(mappedBy = "metrica")
    private List<MisionJuego> misionJuegoList;

  

    public enum Estados {
        PUBLICADO('P'),
        ARCHIVADO('A'),
        BORRADOR('B');

        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }

        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Estados get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }
    


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "metrica_uuid")
    @GenericGenerator(name = "metrica_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_METRICA")
    private String idMetrica;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "MEDIDA")
    private String medida;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;

    @Column(name = "DIAS_VENCIMIENTO")
    private Long diasVencimiento;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    @OneToMany(mappedBy = "idMetrica")
    private List<Producto> productoList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMetrica")
    private List<Premio> premioList;

    @OneToMany(mappedBy = "idMetricaInicial")
    private List<ConfiguracionGeneral> configuracionGeneralList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "metrica")
    private List<ReglaMiembroAtb> reglaMiembroAtbList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "metrica")
    private List<MiembroMetricaNivel> miembroMetricaNivelList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMetrica")
    private List<TablaPosiciones> tablaPosicionesList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMetrica")
    private List<ReglaMetricaCambio> reglaMetricaCambioList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMetrica")
    private List<ReglaMetricaBalance> reglaMetricaBalanceList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMetrica")
    private List<Mision> misionList;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;

    @JoinColumn(name = "GRUPO_NIVELES", referencedColumnName = "ID_GRUPO_NIVEL")
    @ManyToOne(optional = false)
    private GrupoNiveles grupoNiveles;

    public Metrica() {
    }

    public String getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMedida() {
        return medida;
    }

    public void setMedida(String medida) {
        this.medida = medida;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }

    public Long getDiasVencimiento() {
        return diasVencimiento;
    }

    public void setDiasVencimiento(Long diasVencimiento) {
        this.diasVencimiento = diasVencimiento;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<TablaPosiciones> getTablaPosicionesList() {
        return tablaPosicionesList;
    }

    public void setTablaPosicionesList(List<TablaPosiciones> tablaPosicionesList) {
        this.tablaPosicionesList = tablaPosicionesList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ReglaMetricaCambio> getReglaMetricaCambioList() {
        return reglaMetricaCambioList;
    }

    public void setReglaMetricaCambioList(List<ReglaMetricaCambio> reglaMetricaCambioList) {
        this.reglaMetricaCambioList = reglaMetricaCambioList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ReglaMetricaBalance> getReglaMetricaBalanceList() {
        return reglaMetricaBalanceList;
    }

    public void setReglaMetricaBalanceList(List<ReglaMetricaBalance> reglaMetricaBalanceList) {
        this.reglaMetricaBalanceList = reglaMetricaBalanceList;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado != null ? Character.toUpperCase(indEstado) : null;
    }

    public GrupoNiveles getGrupoNiveles() {
        return grupoNiveles;
    }

    public void setGrupoNiveles(GrupoNiveles grupoNiveles) {
        this.grupoNiveles = grupoNiveles;
    }

    public Boolean getIndExpiracion() {
        return indExpiracion;
    }

    public void setIndExpiracion(Boolean indExpiracion) {
        this.indExpiracion = indExpiracion;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ConfiguracionGeneral> getConfiguracionGeneralList() {
        return configuracionGeneralList;
    }

    public void setConfiguracionGeneralList(List<ConfiguracionGeneral> configuracionGeneralList) {
        this.configuracionGeneralList = configuracionGeneralList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<Premio> getPremioList() {
        return premioList;
    }

    public void setPremioList(List<Premio> premioList) {
        this.premioList = premioList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MiembroMetricaNivel> getMiembroMetricaNivelList() {
        return miembroMetricaNivelList;
    }

    public void setMiembroMetricaNivelList(List<MiembroMetricaNivel> miembroMetricaNivelList) {
        this.miembroMetricaNivelList = miembroMetricaNivelList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ReglaMiembroAtb> getReglaMiembroAtbList() {
        return reglaMiembroAtbList;
    }

    public void setReglaMiembroAtbList(List<ReglaMiembroAtb> reglaMiembroAtbList) {
        this.reglaMiembroAtbList = reglaMiembroAtbList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<Mision> getMisionList() {
        return misionList;
    }

    public void setMisionList(List<Mision> misionList) {
        this.misionList = misionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMetrica != null ? idMetrica.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Metrica)) {
            return false;
        }
        Metrica other = (Metrica) object;
        if ((this.idMetrica == null && other.idMetrica != null) || (this.idMetrica != null && !this.idMetrica.equals(other.idMetrica))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Metrica[ idMetrica=" + idMetrica + " ]";
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<Producto> getProductoList() {
        return productoList;
    }

    public void setProductoList(List<Producto> productoList) {
        this.productoList = productoList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MisionJuego> getMisionJuegoList() {
        return misionJuegoList;
    }

    public void setMisionJuegoList(List<MisionJuego> misionJuegoList) {
        this.misionJuegoList = misionJuegoList;
    }

  
}
