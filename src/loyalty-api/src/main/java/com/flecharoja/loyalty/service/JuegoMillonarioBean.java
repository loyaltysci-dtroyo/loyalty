/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.JuegoMillonario;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author Kevin Ramírez
 */
@Stateless
public class JuegoMillonarioBean {
    
    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora
    
    /**
     * Metodo que guarda en almacenamiento la informacion de una nueva metrica
     *
     * @param juego Objeto con los valores de la nueva metrica
     * @param usuario usuario en sesión
     * @param locale
     * @return Identificador asignado a la metrica almacenada
     */
    public String insertJuego(JuegoMillonario juego, String usuario, Locale locale) {
        if (juego == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        
        if (juego.getNombre() == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        
        Date date = Calendar.getInstance().getTime();//obtencion de la fecha actual

        //establecimiento de atributos por defecto
        juego.setUsuarioCreacion(usuario);
        juego.setUsuarioModificacion(usuario);
        juego.setFechaCreacion(date);
        juego.setFechaModificacion(date);
        juego.setNumVersion(new Long(1));

        //almacenamiento de la entidad y registro de la accion en la bitacora
        try {
            em.persist(juego);
            em.flush();
        } catch (ConstraintViolationException e) {//si se viola una restriccion de un atributo...
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        bitacoraBean.logAccion(JuegoMillonario.class, juego.getIdJuego(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);

        //se retorna el identificado creado
        return juego.getIdJuego();
    }
    
    
    /**
     * Metodo que modifica en el almacenamiento la informacion de una metrica
     * existente
     *
     * @param juego Objeto con la informacion nueva de una metrica existente
     * @param usuario usuario en sesión
     * @param locale
     */
    public void editJuego(JuegoMillonario juego, String usuario, Locale locale) {
        if (juego == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        //verificacion de la existencia de la entidad
        JuegoMillonario original = em.find(JuegoMillonario.class, juego.getIdJuego());
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "metric_not_found");
        }

        juego.setUsuarioModificacion(usuario);
        juego.setFechaModificacion(Calendar.getInstance().getTime());

        try {
            em.merge(juego);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(JuegoMillonario.class, juego.getIdJuego(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
    }
    
    /**
     * Metodo que modifica en el almacenamiento la informacion de una metrica
     * existente
     *
     * @param idJuego
     * @param complete Objeto con la informacion nueva de una metrica existente
     * @param usuario usuario en sesión
     * @param locale
     */
    public void updateIsCompleteJuego(String idJuego, boolean complete, String usuario, Locale locale) {
        JuegoMillonario juego = em.find(JuegoMillonario.class, idJuego);
        
        if (juego == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }

        juego.setComplete(complete?1:0);
        juego.setUsuarioModificacion(usuario);
        juego.setFechaModificacion(Calendar.getInstance().getTime());

        try {
            em.merge(juego);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(JuegoMillonario.class, juego.getIdJuego(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
    }
    
    
    /**
     * Obtencion de misiones en rango de registros (registro & cantidad) y
     * filtrado opcionalmente por indicadores y terminos de busqueda sobre
     * algunos campos
     *
     * @param busqueda Terminos de busqueda
     * @param filtros Listado de campos sobre que aplicar los terminos de
     * busqueda
     * @param cantidad Cantidad de registros deseados
     * @param registro Numero de registro inicial en el resultado
     * @param ordenTipo
     * @param ordenCampo
     * @param locale
     * @return Listado de misiones deseadas
     */
    public Map<String, Object> getJuegos(String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<JuegoMillonario> root = query.from(JuegoMillonario.class);

        query = Busquedas.getCriteriaQueryJuegoMillonario(cb, query, root, busqueda, filtros, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }
    
    /**
     * Metodo que obtiene la informacion de un tema millonario por el identificador
     * de la misma
     *
     * @param idJuego
     * @param locale
     * @return Respuesta con la informacion de la promocion encontrada
     */
    public JuegoMillonario getJuegoPorId(String idJuego,Locale locale) {
        JuegoMillonario juego = em.find(JuegoMillonario.class, idJuego);
        //verificacion de que la entidad exista
        if (juego == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "juego_not_found");
        }
        return juego;
    }
}
