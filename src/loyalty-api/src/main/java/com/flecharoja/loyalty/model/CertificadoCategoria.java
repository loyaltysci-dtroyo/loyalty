/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CERTIFICADO_CATEGORIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CertificadoCategoria.findAll", query = "SELECT c FROM CertificadoCategoria c"),
    @NamedQuery(name = "CertificadoCategoria.findByIdCertificadoCategoria", query = "SELECT c FROM CertificadoCategoria c WHERE c.idCertificado= :idCertificado"),
    @NamedQuery(name = "CertificadoCategoria.findByCategoria", query = "SELECT c FROM CertificadoCategoria c WHERE c.idCategoria= :categoriaProducto"),
    @NamedQuery(name = "CertificadoCategoria.existCertificado", query = "SELECT c FROM CertificadoCategoria c WHERE c.numCertificado= :numCertificado AND c.idCategoria= :idCategoria")
})
public class CertificadoCategoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "certificadoCategoria_uuid")
    @GenericGenerator(name = "certificadoCategoria_uuid", strategy = "uuid2")
    @Size(min = 1, max = 40)
    @Column(name = "ID_CERTIFICADO")
    private String idCertificado;
    
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_CATEGORIA")
    @ApiModelProperty(value = "Identificador la categoria", required = true)
    private String idCategoria;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NUM_CERTIFICADO")
    @ApiModelProperty(value = "Número de certificado asignado por el restaurante", required = true)
    private String numCertificado;
    
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRECIO")
    private Double precio;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTADO")
    @ApiModelProperty(value = "Estado del certificado (A asignado D disponible)", required = true)
    private Character estado;
    
    @Column(name = "FECHA_EXPIRACION")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Fecha de expiración del certificado", required = false)
    private Date fechaExpiracion;

    public CertificadoCategoria() {
    }

    public CertificadoCategoria(String idCertificado) {
        this.idCertificado = idCertificado;
    }
    
    public Character getEstado() {
        return estado;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Double getPrecio() {
        return precio;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Long getNumVersion() {
        return numVersion;
    }
    
    public String getIdCertificado() {
        return idCertificado;
    }

    public String getNumCertificado() {
        return numCertificado;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public void setIdCertificado(String idCertificado) {
        this.idCertificado = idCertificado;
    }

    public void setNumCertificado(String numCertificado) {
        this.numCertificado = numCertificado;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCertificado != null ? idCertificado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CertificadoCategoria)) {
            return false;
        }
        CertificadoCategoria other = (CertificadoCategoria) object;
        if ((this.idCertificado == null && other.idCertificado != null) || (this.idCertificado != null && !this.idCertificado.equals(other.idCertificado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.CertificadoCategoria[ idCertificado=" + idCertificado + " ]";
    }

//    
    
    
}
