package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CodigoCertificado;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.ExistenciasUbicacion;
import com.flecharoja.loyalty.model.ExistenciasUbicacionPK;
import com.flecharoja.loyalty.model.HistoricoMovimientos;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioListaMiembro;
import com.flecharoja.loyalty.model.PremioListaMiembroPK;
import com.flecharoja.loyalty.model.PremioMiembro;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.google.common.collect.Lists;
import com.google.common.primitives.Chars;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TemporalType;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author wtencio, svargas
 */
@Stateless
public class AsignacionPremioMiembroBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    private final Configuration hbaseConf;

    public AsignacionPremioMiembroBean() {
        this.hbaseConf = new Configuration();
        this.hbaseConf.addResource("conf/hbase/hbase-site.xml");
    }

    /**
     * otorga un premio por su id a un miembro tambien por su id segun restricciones impuestas
     * 
     * @param idPremio identificador del premio
     * @param idMiembro identificador del miembro
     * @param locale locale
     */
    public void otorgarPremio(String idPremio, String idMiembro, Locale locale) {
        CodigoCertificado codigoCertificado = null;
        //verificar el minimo que tiene de metrica el premio lo tenga el miembro
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "reward_not_found");
        }
        Miembro miembro = em.find(Miembro.class, idMiembro);
        if (miembro == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "member_not_found");
        }

        //verificacion de la elegibilidad de redimir el premio
        if (!(premio.getIndEstado().equals(Premio.Estados.PUBLICADO.getValue()) && checkElegibilidadPremioMiembro(idMiembro, idPremio) && checkIntervalosRespuestaPremioMiembro(premio, idMiembro) && checkCalendarizacionPremio(premio) && existsExitenciaUbicacion(idPremio, locale))) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "reward_not_available");
        }

        //registro en premio lista
        PremioMiembro premioMiembro = new PremioMiembro();
        premioMiembro.setEstado(PremioMiembro.Estados.REDIMIDO_SIN_RETIRAR.getValue());
        premioMiembro.setFechaAsignacion(Calendar.getInstance().getTime());
        premioMiembro.setIdMiembro(miembro);
        premioMiembro.setIndMotivoAsignacion(PremioMiembro.Motivos.REWARD.getValue());
        premioMiembro.setIdPremio(premio);
        if (premio.getCantDiasVencimiento()!=null && premio.getCantDiasVencimiento()>0) {
            premioMiembro.setFechaExpiracion(fechaVencimiento(new Date(), premio.getCantDiasVencimiento()));
        }
        if (Premio.Tipo.get(premio.getIndTipoPremio())==Premio.Tipo.CERTIFICADO) {
            List<CodigoCertificado> certificados = em.createNamedQuery("CodigoCertificado.findByIdPremioIndEstado")
                    .setParameter("idPremio", idPremio)
                    .setParameter("indEstado", CodigoCertificado.Estados.DISPONIBLE.getValue())
                    .getResultList();
            if (!certificados.isEmpty()) {
                codigoCertificado = certificados.get(0);
                premioMiembro.setCodigoCertificado(codigoCertificado.getCodigoCertificadoPK().getCodigo());
                codigoCertificado.setIndEstado(CodigoCertificado.Estados.OCUPADO.getValue());
            } else {
                throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "certificates_not_available");
            }
            em.merge(codigoCertificado);
        }
        //persisto
        em.persist(premioMiembro);
        
        afectarExistenciasHistorico(premio, miembro, premioMiembro.getIdPremioMiembro(), locale);

        try {
            em.flush();
        } catch (PersistenceException e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "redeem_failed");
        }

        //envio a cola de mensajes el evento de esta accion
        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.REMIDIO_PREMIO, idPremio, idMiembro);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
        }
    }

    /**
     * filtrado por inclusion/exclusion
     */
    private boolean checkElegibilidadPremioMiembro(String idMiembro, String idPremio) {
        //se retraen los segmentos elegibles para el miembro y se particionan en listas de 1000(por limitante de numero de identificadores en clausula SQL "IN" )
        List<List<String>> partitionsSegmentos;
        partitionsSegmentos = Lists.partition(getSegmentosPertenecientesMiembro(idMiembro), 999);

        //se consulta si exsite un registro de inclusion/exclusion del miembro para el premio
        PremioListaMiembro miembroIncluidoExcluido = em.find(PremioListaMiembro.class, new PremioListaMiembroPK(idMiembro, idPremio));
        if (miembroIncluidoExcluido != null) {
            //si existe... se filtra por su tipo... (inclusion o exclusion)
            switch (miembroIncluidoExcluido.getIndTipo()) {
                case Indicadores.INCLUIDO: {
                    return true;
                }
                case Indicadores.EXCLUIDO: {
                    return false;
                }
            }
        }

        //se verifica si existe algun segmento excluido en el premio que este dentro de los segmentos pertenecientes del miembro
        if (partitionsSegmentos.stream()
                .anyMatch((l) -> ((long) em.createNamedQuery("PremioListaSegmento.countByIdPremioAndSegmentosInListaAndByIndTipo")
                .setParameter("idPremio", idPremio)
                .setParameter("lista", l)
                .setParameter("indTipo", Indicadores.EXCLUIDO)
                .getSingleResult() > 0))) {
            return false;
        }
        //se comprueba si no existe ningun segmento incluido para el premio (se asume que es para todos)
        if ((long) em.createNamedQuery("PremioListaSegmento.countByIdPremioAndByIndTipo")
                .setParameter("idPremio", idPremio)
                .setParameter("indTipo", Indicadores.INCLUIDO)
                .getSingleResult() == 0) {
            return true;
        } else {
            //se verifica si existe algun segmento incluido en el premio que este dentro de los segmentos pertenecientes del miembro
            return partitionsSegmentos.stream()
                    .anyMatch((l) -> ((long) em.createNamedQuery("PremioListaSegmento.countByIdPremioAndSegmentosInListaAndByIndTipo")
                    .setParameter("idPremio", idPremio)
                    .setParameter("lista", l)
                    .setParameter("indTipo", Indicadores.INCLUIDO)
                    .getSingleResult() > 0));
        }
    }

    /**
     * filtrado de premio por tiempo transcurrido entre intervalos de respuestas
     */
    private boolean checkIntervalosRespuestaPremioMiembro(Premio premio, String idMiembro) {
        if (premio.getIndRespuesta() != null && premio.getIndRespuesta()) {
            Premio.IntervalosTiempoRespuesta intervalo;
            boolean flag = true;//bandera indicando si se aceptan nuevas rediciones (por defecto true)

            //verificacion de intervalos de respuesta totales
            intervalo = Premio.IntervalosTiempoRespuesta.get(premio.getIndIntervaloTotal());
            if (intervalo != null) {
                flag = checkIntervaloRespuestaPremio(null, premio.getIdPremio(), intervalo, premio.getCantTotal());
            }

            //verificacion de intervalos de respuesta totales por miembro..
            intervalo = Premio.IntervalosTiempoRespuesta.get(premio.getIndIntervaloMiembro());
            if (intervalo != null && flag) {
                flag = checkIntervaloRespuestaPremio(idMiembro, premio.getIdPremio(), intervalo, premio.getCantTotalMiembro());
            }

            //verificacion de tiempo entre respuestas del miembro...
            if (intervalo != null && flag) {
                Date fechaUltima = (Date) em.createNamedQuery("PremioMiembro.findByUltimaFechaAsignacion")
                        .setParameter("idMiembro", idMiembro)
                        .setParameter("idPremio", premio.getIdPremio())
                        .getSingleResult();
                if (fechaUltima != null) {
                    switch (intervalo) {
                        case POR_DIA: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.DAY_OF_YEAR, -premio.getCantIntervaloRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                        case POR_HORA: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.HOUR_OF_DAY, -premio.getCantIntervaloRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                        case POR_MINUTO: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.MINUTE, -premio.getCantIntervaloRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                    }
                }
            }
            return flag;
        }
        return true;
    }

    /**
     * true -> se aceptan nuevas respuestas false -> no se aceptan nuevas
     * respuestas
     */
    private boolean checkIntervaloRespuestaPremio(String idMiembro, String idPremio, Premio.IntervalosTiempoRespuesta intervalo, Long cantTotal) {
        /*
        Se obtienen la cantidad de respuestas de registros de respuestas de premio en rangos de tiempos de fecha
         */
        long cantidadRespuestas = 0;
        switch (intervalo) {
            case POR_SIEMPRE: {
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasPremio").setParameter("idPremio", idPremio).getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasPremioMiembro").setParameter("idPremio", idPremio).setParameter("idMiembro", idMiembro).getSingleResult();
                }
                break;
            }
            case POR_MES: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_SEMANA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.WEEK_OF_YEAR, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_DIA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_HORA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.HOUR_OF_DAY, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();

                }

                break;
            }
            case POR_MINUTO: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MINUTE, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
        }
        //mientras que la cantidad de respuestas siga siendo menor a la cantidad de respuestas totales aceptables..
        return cantidadRespuestas < cantTotal;
    }

    /**
     * Método que verifica que el premio este entre las fechas correctas
     */
    private boolean checkCalendarizacionPremio(Premio premio) {
        //se establece el indicador de respuesta como verdadero(todo es permitido hasta que sea negado)
        boolean flag = true;
        //se obtiene la fecha actual
        Calendar hoy = Calendar.getInstance();

        //si el premio es calendarizado
        if (premio.getEfectividadIndCalendarizado().equals(Premio.Efectividad.CALENDARIZADO.getValue())) {
            //si la fecha de inicio esta establecida, se comprueba que la fecha sea pasada a hoy o que sea el mismo dia
            if (premio.getEfectividadFechaInicio() != null) {
                flag = hoy.getTime().after(premio.getEfectividadFechaInicio()) || DateUtils.isSameDay(hoy.getTime(), premio.getEfectividadFechaInicio());
            }
            //mientras siga siendo verdadero...
            //si la fecha de fin esta establecida, se comprueba que la fecha sea posterior a hoy o que sea el mismo dia
            if (flag && premio.getEfectividadFechaFin() != null) {
                flag = hoy.getTime().before(premio.getEfectividadFechaFin()) || DateUtils.isSameDay(hoy.getTime(), premio.getEfectividadFechaFin());
            }

            //mientras siga siendo verdadero...
            if (flag && premio.getEfectividadIndDias() != null) {
                //se obtiene el numero de dia de hoy
                int hoyDia = hoy.get(Calendar.DAY_OF_WEEK);
                //se recorre los indicadores de dias de recurrencia en busca de que se cumpla al menos una condicion
                flag = Chars.asList(premio.getEfectividadIndDias().toCharArray()).stream().anyMatch((t) -> {
                    //segun el indicador de dia de recurrencia, se verifica si el numero de dia de hoy concuerda con el numero de dia asociado al indicador
                    switch (t) {
                        case 'L': {
                            return Calendar.MONDAY == hoyDia;
                        }
                        case 'K': {
                            return Calendar.TUESDAY == hoyDia;
                        }
                        case 'M': {
                            return Calendar.WEDNESDAY == hoyDia;
                        }
                        case 'J': {
                            return Calendar.THURSDAY == hoyDia;
                        }
                        case 'V': {
                            return Calendar.FRIDAY == hoyDia;
                        }
                        case 'S': {
                            return Calendar.SATURDAY == hoyDia;
                        }
                        case 'D': {
                            return Calendar.SUNDAY == hoyDia;
                        }
                        default: {
                            return false;
                        }
                    }
                });
            }
            if (flag && premio.getEfectividadIndSemana() != null) {
                long weeksPast = ChronoUnit.WEEKS.between(LocalDate.ofEpochDay(premio.getEfectividadFechaInicio() == null ? premio.getFechaPublicacion().getTime() : premio.getEfectividadFechaInicio().getTime()), LocalDate.now());
                if (weeksPast % premio.getEfectividadIndSemana().longValue() != 0) {
                    flag = false;
                }
            }
        }
        return flag;
    }

    /**
     * Método que dice si existen existencias del producto en la ubicacion
     * central
     *
     * @param idPremio identificador del premio
     * @param locale locale
     * @return true/false
     */
    private boolean existsExitenciaUbicacion(String idPremio, Locale locale) {
        ConfiguracionGeneral config = em.find(ConfiguracionGeneral.class, 0l);

        //se verifica que la integracion de inventario este inactiva, si no por defecto es un true
        if (config.getIntegracionInventarioPremio() == null || config.getIntegracionInventarioPremio() == false) {
            return true;
        } else {
            //se obtiene la ubicacion principal
            if (config.getUbicacionPrincipal() == null) {
                throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "location_undefined");
            }
            Ubicacion ubicacion = config.getUbicacionPrincipal();
            Premio premio = em.find(Premio.class, idPremio);
            //se verifica las existencias de premios tipo producto o la cantidad de certificados
            if (premio.getIndTipoPremio().equals(Premio.Tipo.PRODUCTO.getValue())) {
                ExistenciasUbicacion existencias = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(ubicacion.getIdUbicacion(), idPremio, config.getMes(), config.getPeriodo()));
                if (existencias == null) {
                    return false;
                }
                return existencias.getSaldoActual() >= 1;
            } else {
                long cantCertificados = (long) em.createNamedQuery("CodigoCertificado.countByIdPremio").setParameter("idPremio", idPremio).setParameter("indEstado", CodigoCertificado.Estados.DISPONIBLE.getValue()).getSingleResult();
                return cantCertificados >= 1;
            }
        }
    }

    /**
     * Afectacion de historico de existencias del premio
     * 
     * @param premio info del premio
     * @param miembro info del miembro
     * @param idPremioMiembro identificador del premio otorgado al miembro
     * @param locale locale
     */
    private void afectarExistenciasHistorico(Premio premio, Miembro miembro, String idPremioMiembro, Locale locale) {
        //se verifica que la integracion de premios inventario este activada
        ConfiguracionGeneral config = em.find(ConfiguracionGeneral.class, 0l);
        if (config.getIntegracionInventarioPremio() != null && config.getIntegracionInventarioPremio()) {
            if (config.getUbicacionPrincipal() == null) {
                throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "location_undefined");
            }
            //sobre la ubicacion general...
            Ubicacion ubicacion = config.getUbicacionPrincipal();
            HistoricoMovimientos historico;

            if (premio.getIndTipoPremio().equals(Premio.Tipo.PRODUCTO.getValue())) {
                ExistenciasUbicacion existencias = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(ubicacion.getIdUbicacion(), premio.getIdPremio(), config.getMes(), config.getPeriodo()));
                if (existencias == null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "no_record_reward");
                }

                if (existencias.getSaldoActual() >= 1) {
                    existencias.setSalidas(existencias.getSalidas() + 1);
                    existencias.setSaldoActual((existencias.getSaldoInicial() + existencias.getEntradas()) - existencias.getSalidas());
                }
                try {
                    em.merge(existencias);
                    em.flush();
                } catch (ConstraintViolationException e) {
                    throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
                }
                historico = new HistoricoMovimientos(premio.getPrecioPromedio(), miembro, premio, existencias.getUbicacion(), null, 1L, HistoricoMovimientos.Tipos.REDENCION_AUTO.getValue(), new Date(), HistoricoMovimientos.Status.REDIMIDO_SIN_RETIRAR.getValue(), idPremioMiembro);
                long totalPremios = (long) em.createNamedQuery("ExistenciasUbicacion.findByPremioByPeriodoByMes").setParameter("idPremio", premio).setParameter("mes", config.getMes()).setParameter("periodo", config.getPeriodo()).getSingleResult();
                premio.setTotalExistencias(totalPremios);
            } else {
                historico = new HistoricoMovimientos(null, miembro, premio, config.getUbicacionPrincipal(), null, 1L, HistoricoMovimientos.Tipos.REDENCION_AUTO.getValue(), new Date(), HistoricoMovimientos.Status.REDIMIDO_SIN_RETIRAR.getValue(), idPremioMiembro);
                long totalCertificados = (long) em.createNamedQuery("CodigoCertificado.countByIdPremio").setParameter("idPremio", premio).setParameter("indEstado", CodigoCertificado.Estados.DISPONIBLE.getValue()).getSingleResult();
                premio.setTotalExistencias(totalCertificados);
            }

            try {
                em.persist(historico);
                em.merge(premio);
            } catch (ConstraintViolationException e) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }
        }
    }
    
    /**
     * Metodo para obtener la fecha de vencimiento
     * @param fecha
     * @param dias
     * @return 
     */
    private Date fechaVencimiento(Date fecha, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 58);
        return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos

    }

    /**
     * Obtiene la lista de segmentos donde un miembro es elegible
     * 
     * @param idMiembro identificador del miembro
     * @return Lista de id's de segmentos
     */
    private List<String> getSegmentosPertenecientesMiembro(String idMiembro) {
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "ELEGIBILIDAD_MIEMBRO"));

            //se obtiene la fila (solo la columna familia de segmentos) con el rowkey igual al valor del identificador del miembro
            Get get = new Get(Bytes.toBytes(idMiembro));
            get.addFamily(Bytes.toBytes("SEGMENTOS"));
            Result result = table.get(get);

            //en caso de que no se haya encontrado alguna fila...
            if (result.isEmpty()) {
                return new ArrayList<>();
            }

            //por cada entrada de segmento (columna & valor), se filtran los que indiquen que el miembro pertenece y se mapea al texto de los identificadores de segmento
            return result.getFamilyMap(Bytes.toBytes("SEGMENTOS"))
                    .entrySet()
                    .stream()
                    .filter((t) -> Bytes.toBoolean(t.getValue()))
                    .map((t) -> Bytes.toString(t.getKey()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, Locale.getDefault(), "database_connection_failed");
        }
    }
}
