package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.model.PromocionCategoriaPromo;
import com.flecharoja.loyalty.model.PromocionListaMiembro;
import com.flecharoja.loyalty.model.PromocionListaSegmento;
import com.flecharoja.loyalty.model.PromocionListaUbicacion;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado del manejo de datos para el mantenimiento de promociones
 *
 * @author svargas
 */
@Stateless
public class PromocionBean {

    @EJB
    UtilBean utilBean;

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Obtencion de promociones en rango de registros (registro & cantidad) y filtrado opcionalmente por indicadores y terminos de busqueda sobre algunos campos
     * 
     * @param estados Listado de valores de indicadores de estados deseados
     * @param acciones Listado opcional de indicadores de tipos de acciones sobre que aplicar un filtrado
     * @param calendarizaciones Listado opcional de indicadores de tipos de calendarizacion sobre que aplicar un filtrado
     * @param busqueda Terminos de busqueda
     * @param filtros Listado de campos sobre que aplicar los terminos de busqueda
     * @param cantidad Cantidad de registros deseados
     * @param registro Numero de registro inicial en el resultado
     * @return Listado de misiones deseadas
     */
    public Map<String, Object> getPromociones(List<String> estados, List<String> acciones, List<String> calendarizaciones, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Promocion> root = query.from(Promocion.class);
        
        query = Busquedas.getCriteriaQueryPromociones(cb, query, root, estados, acciones, calendarizaciones, busqueda, filtros, ordenTipo, ordenCampo);
        
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * Metodo que registra una nueva promocion
     *
     * @param promocion Objecto promocion con la informacion de la misma
     * @param usuario usuario sesión
     * @param locale
     * @return Respuesta con el identificador de la promocion creada
     */
    public String insertPromocion(Promocion promocion, String usuario, Locale locale) {
        if (promocion==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Promo");
        }
        //verificacion de algunos atributos no requeridos
        if (promocion.getIdPromocion() != null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idPromocion");
        }

        //establecimiento de atributos por defecto
        promocion.setIndEstado(Promocion.Estados.BORRADOR.getValue());
        promocion.setIndCalendarizacion(Promocion.IndicadoresCalendarizacion.PERMANENTE.getValue());
        Date date = Calendar.getInstance().getTime();
        promocion.setFechaCreacion(date);
        promocion.setFechaModificacion(date);
        promocion.setNumVersion(new Long(1));

        promocion.setUsuarioCreacion(usuario);
        promocion.setUsuarioModificacion(usuario);

        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (promocion.getImagenArte() == null || promocion.getImagenArte().trim().isEmpty()) {
            promocion.setImagenArte(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                promocion.setImagenArte(filesUtils.uploadImage(promocion.getImagenArte(), MyAwsS3Utils.Folder.ARTE_PROMOCION, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }
        
        if (promocion.getEncabezadoArte()==null) {
            promocion.setEncabezadoArte(promocion.getNombre());
        }
        
        if (promocion.getDetalleArte()==null) {
            promocion.setDetalleArte(promocion.getDescripcion());
        }
        
        if (promocion.getIndTipoAccion()!=null) {
            Promocion.IndicadoresTiposAcciones tiposAccion = Promocion.IndicadoresTiposAcciones.get(promocion.getIndTipoAccion());
            String urlOrCodigo = promocion.getUrlOrCodigo();
            switch (tiposAccion) {
                case CODE_128: {
                    //Character Set: ASCII
                    if (urlOrCodigo.chars().anyMatch((value) -> value>127)) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "urlOrCodigo");
                    }
                    break;
                }
                case CODE_39: {
                    urlOrCodigo = urlOrCodigo.toUpperCase();
                    //Character Set: 0-9, A-Z, -, +, space, *, $, /, %, .
                    if (Pattern.compile("[^0-9A-Z-+*$/%. ]").matcher(urlOrCodigo).find()) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "urlOrCodigo");
                    }
                    promocion.setUrlOrCodigo(urlOrCodigo);
                    break;
                }
                case EAN_13: {
                    //Character Set: 0-9
                    //Characters Max Range: 12
                    if (Pattern.compile("[^0-9]").matcher(urlOrCodigo).find() || urlOrCodigo.length()>12) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "urlOrCodigo");
                    }
                    break;
                }
                case ITF: {
                    //Character Set: 0-9
                    //Characters Max Range: 13
                    if (Pattern.compile("[^0-9]").matcher(urlOrCodigo).find() || urlOrCodigo.length()>13) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "urlOrCodigo");
                    }
                    break;
                }
                case URL: {
                    //url-uri
                    try {
                        new URL(urlOrCodigo).toURI();
                    } catch (MalformedURLException | URISyntaxException e) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "urlOrCodigo");
                    }
                    break;
                }
            }
        } else {
            promocion.setUrlOrCodigo(null);
        }

        try {
            em.persist(promocion);
            em.flush();

        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (!promocion.getImagenArte().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(promocion.getImagenArte());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        bitacoraBean.logAccion(Promocion.class, promocion.getIdPromocion(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);

        return promocion.getIdPromocion();
    }

    /**
     * Metodo que modifica la informacion de una promocion
     *
     * @param promocion Objeto con la informacion de la promocion a editar
     * @param usuario usuario en sesión
     * @param locale
     */
    public void editPromocion(Promocion promocion, String usuario, Locale locale) {
        if (promocion==null) {
           throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Promo");
        }
        //verificacion de que la entidad existe
        Promocion original = em.find(Promocion.class, promocion.getIdPromocion());
       
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        
        Promocion.Estados estadoPromocion = Promocion.Estados.get(promocion.getIndEstado());
        if (estadoPromocion==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "attributes_invalid", "indEstado");
        }
        Promocion.Estados estadoOriginal = Promocion.Estados.get(original.getIndEstado());
        //verificacion de que posible cambio de estado sea valido (Activo/Inactivo NO A Borrador) y de edicion de entidad archivada
        if ((estadoOriginal==Promocion.Estados.PUBLICADO || estadoOriginal==Promocion.Estados.INACTIVO) && estadoPromocion==Promocion.Estados.BORRADOR) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "promo_not_change_state");
        }
        if (estadoOriginal==Promocion.Estados.ARCHIVADO) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", "Promo");
        }
        
        if (estadoPromocion==Promocion.Estados.PUBLICADO) {
            if (promocion.getEncabezadoArte()==null || promocion.getDetalleArte()==null) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "attributes_invalid", "encabezadoArte,detalleArte");
            }
        }

        //verificacion de valores validos en los indicadores de dias y num de semana y fechas de calendarizacion
        if (promocion.getIndCalendarizacion().equals(Promocion.IndicadoresCalendarizacion.CALENDARIZADO.getValue())) {
            if (promocion.getFechaInicio() == null || promocion.getFechaFin() == null || promocion.getFechaInicio().compareTo(promocion.getFechaFin()) > 0) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "promo_badly_scheduled");
            }
            
            if (promocion.getIndSemanaRecurrencia()!=null && (promocion.getIndSemanaRecurrencia().compareTo(1)<0 || promocion.getIndSemanaRecurrencia().compareTo(52)>0)) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indSemanaRecurrencia");
            }
            
            if (promocion.getIndDiaRecurrencia()!=null) {
                char[] validValues = {'L','M','K','J','V','S','D'};
                promocion.setIndDiaRecurrencia(promocion.getIndDiaRecurrencia().toUpperCase());
                for (char validValue : validValues) {
                    if (promocion.getIndDiaRecurrencia().indexOf(validValue)!=promocion.getIndDiaRecurrencia().lastIndexOf(validValue)) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indDiaRecurrencia");
                    }
                }
            }
        } else {
            if (!promocion.getIndCalendarizacion().equals(Promocion.IndicadoresCalendarizacion.PERMANENTE.getValue())) {
               throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indCalendarizado");
            }
        }
        
        if (promocion.getIndTipoAccion()!=null) {
            Promocion.IndicadoresTiposAcciones tiposAccion = Promocion.IndicadoresTiposAcciones.get(promocion.getIndTipoAccion());
            String urlOrCodigo = promocion.getUrlOrCodigo();
            switch (tiposAccion) {
                case CODE_128: {
                    //Character Set: ASCII
                    if (urlOrCodigo.chars().anyMatch((value) -> value>127)) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "urlOrCodigo");
                    }
                    break;
                }
                case CODE_39: {
                    urlOrCodigo = urlOrCodigo.toUpperCase();
                    //Character Set: 0-9, A-Z, -, +, space, *, $, /, %, .
                    if (Pattern.compile("[^0-9A-Z-+*$/%. ]").matcher(urlOrCodigo).find()) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "urlOrCodigo");
                    }
                    promocion.setUrlOrCodigo(urlOrCodigo);
                    break;
                }
                case EAN_13: {
                    //Character Set: 0-9
                    //Characters Max Range: 12
                    if (Pattern.compile("[^0-9]").matcher(urlOrCodigo).find() || urlOrCodigo.length()>12) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "urlOrCodigo");
                    }
                    break;
                }
                case ITF: {
                    //Character Set: 0-9
                    //Characters Max Range: 13
                    if (Pattern.compile("[^0-9]").matcher(urlOrCodigo).find() || urlOrCodigo.length()>13) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "urlOrCodigo");
                    }
                    break;
                }
                case URL: {
                    //url-uri
                    try {
                        new URL(urlOrCodigo).toURI();
                    } catch (MalformedURLException | URISyntaxException e) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "urlOrCodigo");
                    }
                    break;
                }
            }
        } else {
            promocion.setUrlOrCodigo(null);
        }

        //establecimiento de valores de atributos por defecto
        Date date = Calendar.getInstance().getTime();
        promocion.setFechaModificacion(date);

        promocion.setUsuarioModificacion(usuario);
        if (promocion.getIndEstado().equals(Promocion.Estados.PUBLICADO.getValue())) {
            promocion.setFechaPublicacion(date);
        }

        String urlImagenOriginal = original.getImagenArte();
        String urlImagenNueva = null;
        //si el atributo de imagen viene nula, se utiliza la predeterminada
        if (promocion.getImagenArte() == null || promocion.getImagenArte().trim().isEmpty()) {
            urlImagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            promocion.setImagenArte(urlImagenNueva);
        } else //ver si el valor en el atributo de imagen, difiere del almacenado
        if (!urlImagenOriginal.equals(promocion.getImagenArte())) {
            //se le intenta subir
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                urlImagenNueva = filesUtils.uploadImage(promocion.getImagenArte(), MyAwsS3Utils.Folder.ARTE_PROMOCION, null);
                promocion.setImagenArte(urlImagenNueva);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }

        try {
            em.merge(promocion);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//en caso de que alguna informacion no este valida o este "vieja"
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
           if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(Promocion.class, promocion.getIdPromocion(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        if (urlImagenNueva != null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Metodo que obtiene la informacion de una promocion por el identificador
     * de la misma
     *
     * @param idPromocion Identificador de la promocion
     * @param locale
     * @return Respuesta con la informacion de la promocion encontrada
     */
    public Promocion getPromocionPorId(String idPromocion,Locale locale) {
        Promocion promocion = em.find(Promocion.class, idPromocion);
        //verificacion de que la entidad exista
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        return promocion;
    }

    /**
     * Metodo que elimina o archiva una promocion segun por el valor de su
     * indicador de estado
     *
     * @param idPromocion Identificador de la promocion
     * @param usuario usuario en sesión
     * @param locale
     */
    public void removePromocion(String idPromocion, String usuario, Locale locale) {
        //verificacion que la entidad exista
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        
        String url = promocion.getImagenArte();

        if (promocion.getIndEstado().equals(Promocion.Estados.BORRADOR.getValue())) {
            em.remove(promocion);
            em.flush();
            if (!url.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(url);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            bitacoraBean.logAccion(Promocion.class, idPromocion, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        } else if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Promo");
        } else {
            promocion.setUsuarioModificacion(usuario);
            promocion.setIndEstado(Promocion.Estados.ARCHIVADO.getValue());

            em.merge(promocion);
            bitacoraBean.logAccion(Promocion.class, idPromocion, usuario, Bitacora.Accion.ENTIDAD_ARCHIVADA.getValue(),locale);
        }
    }

    /**
     * Metodo que verifica si existe una entidad de promocion con un nombre
     * interno igual a "nombre"
     *
     * @param nombre Valor del nombre interno a verificar
     * @return Respuesta con el valor booleano de la existencia del nombre
     * interno
     */
    public boolean existsNombreInterno(String nombre) {
        return ((Long) em.createNamedQuery("Promocion.countByNombreInterno").setParameter("nombreInterno", nombre).getSingleResult()) > 0;
    }

    /**
     * Metodo que clona una promocion a partir de su identificador junto con sus
     * listas de categorias de promocion, miembros, segmentos, ubicaciones
     *
     * @param idPromocion Identificador de la promocion objectivo
     * @param idUsuario Identificador del usuario creador
     * @param locale
     * @return Identificador de la promocion clonada
     */
    public String clonePromocion(String idPromocion, String idUsuario, Locale locale) {
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        em.detach(promocion);

        List<PromocionCategoriaPromo> categorias = em.createNamedQuery("PromocionCategoriaPromo.countByIdPromocion").setParameter("idPromocion", idPromocion).getResultList();
        List<PromocionListaMiembro> miembros = em.createNamedQuery("PromocionListaMiembro.findByIdPromocion").setParameter("idPromocion", idPromocion).getResultList();
        List<PromocionListaSegmento> segmentos = em.createNamedQuery("PromocionListaSegmento.findByIdPromocion").setParameter("idPromocion", idPromocion).getResultList();
        List<PromocionListaUbicacion> ubicaciones = em.createNamedQuery("PromocionListaUbicacion.findByIdPromocion").setParameter("idPromocion", idPromocion).getResultList();

        Date fecha = new Date();

        promocion.setIdPromocion(null);
        promocion.setIndEstado(Promocion.Estados.BORRADOR.getValue());
        promocion.setFechaCreacion(fecha);
        promocion.setFechaModificacion(fecha);
        promocion.setUsuarioCreacion(idUsuario);
        promocion.setUsuarioModificacion(idUsuario);
        promocion.setNumVersion(new Long(0));

        em.persist(promocion);
        em.flush();

        categorias.forEach((t) -> em.persist(new PromocionCategoriaPromo(t.getCategoria().getIdCategoria(), promocion.getIdPromocion(), fecha, idUsuario)));
        miembros.forEach((t) -> em.persist(new PromocionListaMiembro(t.getMiembro().getIdMiembro(), promocion.getIdPromocion(), t.getIndTipo(), fecha, idUsuario)));
        segmentos.forEach((t) -> em.persist(new PromocionListaSegmento(t.getSegmento().getIdSegmento(), promocion.getIdPromocion(), t.getIndTipo(), fecha, idUsuario)));
        ubicaciones.forEach((t) -> em.persist(new PromocionListaUbicacion(t.getUbicacion().getIdUbicacion(), promocion.getIdPromocion(), t.getIndTipo(), fecha, idUsuario)));

        em.flush();

        return promocion.getIdPromocion();
    }
}
