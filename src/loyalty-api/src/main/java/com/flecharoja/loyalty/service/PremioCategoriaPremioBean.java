/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CategoriaPremio;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioCategoriaPremio;
import com.flecharoja.loyalty.model.PremioCategoriaPremioPK;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Clase encargada de proporcionar los metodos necesarios para el manejo de la
 * asignacion de un premio con una categoria
 *
 * @author wtencio
 */
@Stateless
public class PremioCategoriaPremioBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de bitacora

    /**
     * Metodo que asigna a una categoria un premio, usando los identificadores
     * de los mismos
     *
     * @param idCategoria Identificador de la categoria de premio
     * @param idPremio Identificador del premio
     * @param usuario usuario en sesión
     * @param locale
     */
    public void assignCategoriaPremio(String idCategoria, String idPremio, String usuario, Locale locale) {
        //se verifica que tanto la categoria como la promocion existan
        Premio premio = (Premio) em.createNamedQuery("Premio.findByIdPremio").setParameter("idPremio", idPremio).getSingleResult();
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }

        try {
            em.getReference(CategoriaPremio.class, idCategoria).getIdCategoria();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_category_not_found");
        }

        //verifica que el premio a asignar a la categoria, no tenga un estado de borrador, ni archivado
        if (premio.getIndEstado().compareTo(Premio.Estados.ARCHIVADO.getValue()) == 0) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Premio");
        }

        //fecha de creacion
        Date date = Calendar.getInstance().getTime();

        try {
            em.persist(new PremioCategoriaPremio(idCategoria, idPremio, date, usuario));
            em.flush();
        } catch (EntityExistsException e) {//en el caso de que ya exista la entidad en la bases de datos
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "reward_category_already_exists");
        }

        bitacoraBean.logAccion(PremioCategoriaPremio.class, idCategoria + "/" + idPremio, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);

    }

    /**
     * Metodo que elimina la asignacion de un premio a una categoria de premio
     * por sus identificadores
     *
     * @param idCategoria Identificador de la categoria
     * @param idPremio Identificador del premio
     * @param usuario usuario en sesion
     * @param locale
     */
    public void unassignCategoriaPremio(String idCategoria, String idPremio, String usuario, Locale locale) {
        try {
            //se intenta remover la entidad a partir de la referencia de la misma por sus identificadores
            em.remove(em.getReference(PremioCategoriaPremio.class, new PremioCategoriaPremioPK(idCategoria, idPremio)));
            em.flush();
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_category_reward_not_found");
        }

        bitacoraBean.logAccion(PremioCategoriaPremio.class, idCategoria + "/" + idPremio, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);

    }

    /**
     * Metodo que asigna a una categoria una lista de premios, usando los
     * identificadores de los mismos
     *
     * @param idCategoria Identificador de la categoria de premios
     * @param listaIdPremios Lista de identificadores de premios
     * @param usuario usuario en sesión
     * @param locale
     */
    public void assignBatchCategoriaPremio(String idCategoria, List<String> listaIdPremios, String usuario, Locale locale) {
        //se verifica que la categoria exista
        try {
            em.getReference(CategoriaPremio.class, idCategoria).getIdCategoria();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_category_not_found");
        }

        for (String idPremio : listaIdPremios) {
            //se verifica que la promocion exista
            Premio premios = em.find(Premio.class, idPremio);
            if (premios == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
            }
            //verifica que la promocion a asignar a la categoria, no tenga un estado de borrador, ni archivado
            if (premios.getIndEstado().compareTo(Premio.Estados.ARCHIVADO.getValue()) == 0) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Premio");
            }
        }

        //fecha de creacion
        Date date = Calendar.getInstance().getTime();

        try {
            listaIdPremios.stream().forEach((idPremio) -> {
                em.persist(new PremioCategoriaPremio(idCategoria, idPremio, date, usuario));
            });
            em.flush();
        } catch (EntityExistsException e) {//en el caso de que ya exista la entidad en la bases de datos
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "reward_category_already_exists");
        }

        listaIdPremios.forEach((idPremio) -> {
            bitacoraBean.logAccion(PremioCategoriaPremio.class, idCategoria + "/" + idPremio, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        });

    }

    /**
     * Metodo que elimina la asignacion de una lista de premios a una categoria
     * de premio por sus identificadores
     *
     * @param idCategoria Identificador de la categoria
     * @param listaIdPremios Lista de identificadores de premios
     * @param usuario usuario en sesion
     * @param locale
     */
    public void unassignBatchCategoriaPremio(String idCategoria, List<String> listaIdPremios, String usuario, Locale locale) {
        try {
            listaIdPremios.stream().forEach((idPremio) -> {
                //se intenta remover la entidad a partir de la referencia de la misma por sus identificadores
                em.remove(em.getReference(PremioCategoriaPremio.class, new PremioCategoriaPremioPK(idCategoria, idPremio)));
            });
            em.flush();
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_category_reward_not_found");
        }

        listaIdPremios.forEach((idPremio) -> {
            bitacoraBean.logAccion(PremioCategoriaPremio.class, idCategoria + "/" + idPremio, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        });

    }

    /**
     * Metodo que obtiene un listado de premios que esten ligados a una
     * categoria de premio, especificado por su identificador y en un rango
     * definido por sus parametros
     *
     * @param estados
     * @param calendarizaciones
     * @param categoria
     * @param tipos
     * @param registro Parametro que indica el primer resultado desde donde
     * retornar los resultados
     * @param indEnvio
     * @param indAccionCategoria
     * @param cantidad Parametro que indica la cantidad de resultados en la
     * lista
     * @param locale
     * @param filtros
     * @param busqueda
     * @param ordenTipo
     * @param indRespuesta
     * @param ordenCampo
     * @return Respuesta con la lista encapsulada y encabezados acerca del rango
     * de respuesta y su total y rango maximo aceptado o estado erroneo y
     * encabezado con el codigo de error
     */
    public Map<String, Object> getPremiosPorIdCategoria(List<String> estados, List<String> tipos, List<String> calendarizaciones, String categoria, String indAccionCategoria, String indRespuesta, String indEnvio, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int registro, int cantidad, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Premio> root = query.from(Premio.class);

        query = Busquedas.getCriteriaQueryPremios(cb, query, root, estados, tipos, calendarizaciones, indRespuesta, indEnvio, categoria, indAccionCategoria, busqueda, filtros, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();

        //retorno del resultado y encabezados sobre el rango
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Metodo que obtiene un listado de categorias de premios que esten ligados
     * a un premio, especificado por su identificador y en un rango definido por
     * sus parametros
     *
     * @param idPremio Identificador del premio
     * @param registro Parametro que indica el primer resultado desde donde
     * retornar los resultados
     * @param cantidad Parametro que indica la cantidad de resultados en la
     * lista
     * @param locale
     * @return Respuesta con la lista encapsulada y encabezados acerca del rango
     * de respuesta y su total y rango maximo aceptado o estado erroneo y
     * encabezado con el codigo de error
     */
    public Map<String, Object> getCategoriasPorIdPremio(String idPremio, int registro, int cantidad, Locale locale) {
        Map<String, Object> respuesta = new HashMap<>();
        //verificacion de que la entidad exista
        try {
            em.getReference(Premio.class, idPremio).getIdPremio();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }

        //verifica que la cantidad en la peticion sea menor a la aceptada, de no ser asi se retorna una respuesta informando acerca de ello, junto con el encabezado de rango maximo
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Long total;
        List resultado;
        try {
            //se obtiene el total de registros
            total = ((Long) em.createNamedQuery("PremioCategoriaPremio.countByIdPremio")
                    .setParameter("idPremio", idPremio)
                    .getSingleResult());
            total -= total - 1 < 0 ? 0 : 1;

            //ve que si en el caso que el registro sea mayor que el total, este se reduce por cantidad (mientras que no sea <0) hasta que este entre un valor de 0 a total
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            //se obtiene el listado usando los parametros de registro y cantidad para limitar el rango de respuesta
            resultado = em.createNamedQuery("PremioCategoriaPremio.findCategoriasByIdPremio")
                    .setParameter("idPremio", idPremio)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();
        } catch (IllegalArgumentException e) {//en el caso que los valores de paginacion esten erroneos
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se establece la respuesta encapsulando el listado y estableciendo los encabezados de rango de la respuesta y rango aceptado
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    

    /**
     * Metodo que obtiene un listado de catgeorias que no estan ligados a un
     * premio, especificado por su identificador y en un rango definido por sus
     * parametros
     *
     * @param idPremio Identificador de la categoria
     * @param registro Parametro que indica el primer resultado desde donde
     * retornar los resultados
     * @param cantidad Parametro que indica la cantidad de resultados en la
     * lista
     * @param locale
     * @return Respuesta con la lista encapsulada y encabezados acerca del rango
     * de respuesta y su total y rango maximo aceptado o estado erroneo y
     * encabezado con el codigo de error
     */
    public Map<String, Object> getNoCategoriasPremio(String idPremio, int registro, int cantidad, Locale locale) {
        Map<String, Object> respuesta = new HashMap<>();
        //verificacion de que la entidad exista
        try {
            em.getReference(Premio.class, idPremio).getIdPremio();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }

        //verifica que la cantidad en la peticion sea menor a la aceptada, de no ser asi se retorna una respuesta informando acerca de ello, junto con el encabezado de rango maximo
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        List registros = new ArrayList();
        int total;
        List resultado;
        try {
            //se obtiene el total de registros
            registros = em.createNamedQuery("PremioCategoriaPremio.findCategoriasNotInPremio").setParameter("idPremio", idPremio).getResultList();
            total = registros.size();
            total -= total - 1 < 0 ? 0 : 1;

            //ve que si en el caso que el registro sea mayor que el total, este se reduce por cantidad (mientras que no sea <0) hasta que este entre un valor de 0 a total
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            //se obtiene el listado usando los parametros de registro y cantidad para limitar el rango de respuesta
            resultado = em.createNamedQuery("PremioCategoriaPremio.findCategoriasNotInPremio")
                    .setParameter("idPremio", idPremio)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();
        } catch (IllegalArgumentException e) {//en el caso que los valores de paginacion esten erroneos
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se establece la respuesta encapsulando el listado y estableciendo los encabezados de rango de la respuesta y rango aceptado
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }
}
