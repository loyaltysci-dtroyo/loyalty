package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.CategoriaNoticia;
import com.flecharoja.loyalty.model.Noticia;
import com.flecharoja.loyalty.model.NoticiaComentario;
import com.flecharoja.loyalty.model.RespuestaRegistroComentario;
import com.flecharoja.loyalty.service.NoticiaBean;
import com.flecharoja.loyalty.service.NoticiaCategoriaNoticiaBean;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase de recursos http disponibles para el manejo de noticias.
 *
 * @author svargas
 */
@Api(value = "Noticia")
@Path("noticia")
public class NoticiaResource {
    
    @EJB
    NoticiaBean noticiaBean;
    
    @EJB
    NoticiaCategoriaNoticiaBean noticiaCategoriaBean;

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @Context
    HttpServletRequest request;
    
    /**
     * Metodo que obtiene un listado de noticias por un rango establecido
     * usando los parametros de cantidad y registro y opcionalmente filtrados
     * por indicadores y terminos de busqueda aplicados sobre una serie de
     * campos de filtros ademas de encabezados indicando del rango aceptable maximo
     * y el rango actual de resultados.
     * 
     * @param estados Indicadores de los estados de las noticias
     * @param registro Numero de registro inicial de la lista
     * @param cantidad Cantidad de registros solicitados en la pagina
     * @param ordenCampo Indicador sobre el campo a ordenar
     * @param busqueda Palabras claves sobre que buscar/filtrar noticias
     * @param ordenTipo Indicador sobre el tipo de orden de la lista
     * @param filtros Indicadores sobre que campos aplicar la busqueda
     * @return Lista de noticias
     */
    @ApiOperation(value = "Obtener lista de noticias",
            responseContainer = "List",
            response = Noticia.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNoticias(
            @ApiParam(value = "Indicadores de estado de noticias") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Filtros sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }

        Map<String, Object> respuesta = noticiaBean.getNoticias(estados, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }

    /**
     * Metodo que registra la informacion de una nueva noticia y retorna el
     * identificador de la noticia creada.
     *
     * @param noticia Objecto con la informacion de una noticia requerida
     * para su registro
     * @return Respuesta con el identificador de la noticia creada
     */
    @ApiOperation(value = "Registrar noticia", response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertNoticia(
            @ApiParam(value = "Informacion de la noticia", required = true) Noticia noticia) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return noticiaBean.insertNoticia(noticia, usuario,request.getLocale());
    }

    /**
     * Metodo que modifica la informacion de una noticia existente.
     *
     * @param noticia Objecto con la informacion actualizada de una noticia
     * existente
     */
    @ApiOperation(value = "Modificacion de noticia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void editNoticia(
            @ApiParam(value = "Informacion de la noticia", required = true) Noticia noticia) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        noticiaBean.editNoticia(noticia, usuario,request.getLocale());
    }

    /**
     * Metodo que obtiene la informacion de una noticia segun su identificador
     * en el parametro de idNoticia.
     *
     * @param idNoticia Identificador de la noticia.
     * @return Respuesta con la informacion de la noticia deseada.
     */
    @ApiOperation(value = "Obtener una noticia por identificador",
            response = Noticia.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("{idNoticia}")
    @Produces(MediaType.APPLICATION_JSON)
    public Noticia getNoticia(
            @ApiParam(value = "Identificador de noticia", required = true)
            @PathParam("idNoticia") String idNoticia) {
        String token;
        try {
            context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return noticiaBean.getNoticia(idNoticia, request.getLocale());
    }

    /**
     * Metodo que elimina o archiva una noticia segun ciertas condiciones.
     *
     * @param idNoticia Identificador de la noticia
     */
    @ApiOperation(value = "Eliminacion/Archivado de noticia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @DELETE
    @Path("{idNoticia}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteNoticia(
            @ApiParam(value = "Identificador de noticia", required = true)
            @PathParam("idNoticia") String idNoticia) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        noticiaBean.deleteNoticia(idNoticia, usuario, request.getLocale());
    }

    /**
     * Metodo que obtiene un listado de comentarios de una noticia por un
     * rango establecido usando los parametros de cantidad y registro.
     * 
     * @param idNoticia Identificador de la categoria de noticias
     * @param untilDate
     * @param sinceDate
     * @return Lista de noticias
     */
    @ApiOperation(value = "Obtener lista de comentarios de una noticia",
            responseContainer = "List",
            response = NoticiaComentario.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("{idNoticia}/comentario")
    @Produces(MediaType.APPLICATION_JSON)
    public List<NoticiaComentario> getComentariosNoticia(
            @ApiParam(value = "Identificador de la noticia") @PathParam("idNoticia") String idNoticia,
            @ApiParam(value = "Fecha de ultimo registro para pagina anterior") @DefaultValue("0") @QueryParam("until") long untilDate,
            @ApiParam(value = "Fecha de primer registro para pagina siguiente (tiene precedencia sobre until))") @DefaultValue("0") @QueryParam("since") long sinceDate) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }

        return noticiaBean.getComentarios(idNoticia, null, untilDate, sinceDate, request.getLocale());
    }
    
    /**
     * Metodo que obtiene un listado de comentarios de un comentario por un
     * rango establecido usando los parametros de cantidad y registro.
     * 
     * @param idNoticia Identificador de la categoria de noticias
     * @param idComentario identificador de comentario referente
     * @param untilDate
     * @param sinceDate
     * @return Lista de noticias
     */
    @ApiOperation(value = "Obtener lista de comentarios de un comentario",
            responseContainer = "List",
            response = Noticia.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("{idNoticia}/comentario/{idComentario}/comentario")
    @Produces(MediaType.APPLICATION_JSON)
    public List<NoticiaComentario> getComentariosComentario(
            @ApiParam(value = "Identificador de la noticia") @PathParam("idNoticia") String idNoticia,
            @ApiParam(value = "Identificador de comentario referente") @PathParam("idComentario") String idComentario,
            @ApiParam(value = "Fecha de ultimo registro para pagina anterior", defaultValue = "0") @DefaultValue("0") @QueryParam("until") long untilDate,
            @ApiParam(value = "Fecha de primer registro para pagina siguiente", defaultValue = "0") @DefaultValue("0") @QueryParam("since") long sinceDate) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }

        return noticiaBean.getComentarios(idNoticia, idComentario, untilDate, sinceDate, request.getLocale());
    }

    /**
     * Metodo que registra la informacion de un nuevo comentario y retorna el
     * identificador creado.
     *
     * @param idNoticia Identificador de la noticia referente del comentario
     * @param comentario Objecto con la informacion de un comentario requerido
     * para su registro
     * @return Respuesta con el identificador del comentario y cant de comentarios actualizados
     */
    @ApiOperation(value = "Registrar comentario", response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("{idNoticia}/comentario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaRegistroComentario insertComentario(
            @ApiParam(value = "Identificador de la noticia") @PathParam("idNoticia") String idNoticia,
            @ApiParam(value = "Informacion del comentario", required = true) NoticiaComentario comentario) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return noticiaBean.insertNoticiaComentario(idNoticia, comentario, usuario,request.getLocale());
    }

    /**
     * Metodo que modifica la informacion de un comentario existente.
     *
     * @param idNoticia Identificador de la noticia referente del comentario
     * @param comentario Objecto con la informacion actualizada de un comentario
     * existente
     */
    @ApiOperation(value = "Modificacion de comentario")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Path("{idNoticia}/comentario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void editComentario(
            @ApiParam(value = "Identificador de la noticia") @PathParam("idNoticia") String idNoticia,
            @ApiParam(value = "Informacion del comentario", required = true) NoticiaComentario comentario) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        noticiaBean.editNoticiaComentario(idNoticia, comentario, usuario,request.getLocale());
    }

    /**
     * Metodo que elimina un comentario.
     *
     * @param idNoticia Identificador de la noticia
     * @param idComentario Identificador del comentario
     */
    @ApiOperation(value = "Eliminacion/Archivado de comentario")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @DELETE
    @Path("{idNoticia}/comentario/{idComentario}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteComentario(
            @ApiParam(value = "Identificador de noticia", required = true) @PathParam("idNoticia") String idNoticia,
            @ApiParam(value = "Identificador del comentario", required = true) @PathParam("idComentario") String idComentario) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        noticiaBean.deleteNoticiaComentario(idNoticia, idComentario, usuario, request.getLocale());
    }
    
    /**
     * Metodo que obtiene un listado de categorias asignadas o disponibles a
     * una noticia.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param indAccion
     * @param busqueda
     * @param filtros
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param idNoticia Identificador de la noticia
     * @return 
     */
    @ApiOperation(value = "Obtencion de noticias de una categoria de noticia",
            responseContainer = "List",
            response = CategoriaNoticia.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("{idNoticia}/categoria")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNoticias(
            @ApiParam(value = "Identificador de categoria de noticia", required = true) @PathParam("idCategoria") String idNoticia,
            @ApiParam(value = "Indicador sobre el tipo de lista por categoria") @QueryParam("ind-accion-lista") String indAccion,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Filtros sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = noticiaCategoriaBean.getCategoriasPorIdNoticia(idNoticia, indAccion, busqueda, Busquedas.TiposOrden.DESCENDIENTE.getValue(), Busquedas.CamposOrden.FECHA_MODIFICACION.getValue(), cantidad, registro, request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }

    /**
     * Metodo que asigna una noticia a una categoria de noticia usando sus
     * identificadores.
     *
     * @param idCategoria Identificador de la categoria de noticia
     * @param idNoticia Identificador de la noticia
     */
    @ApiOperation(value = "Asignacion de una noticia con una categoria de noticia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("{idNoticia}/categoria/{idCategoria}")
    @Produces(MediaType.APPLICATION_JSON)
    public void assignCategoriaNoticia(
            @ApiParam(value = "Identificador de categoria de noticia", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Identificador de noticia", required = true)
            @PathParam("idNoticia") String idNoticia) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        noticiaCategoriaBean.assignCategoriaNoticia(idCategoria, idNoticia, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que remueve la asignacion de una noticia a una categoria de
     * noticia usando sus identificadores.
     *
     * @param idCategoria Identificador de la categoria de noticia
     * @param idNoticia Identificador de la noticia
     */
    @ApiOperation(value = "Eliminacion de la asignacion de una noticia con una categoria de noticia")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Unauthorizated", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Not Found", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Server Error", response = MyExceptionResponseBody.class)
    })
    @DELETE
    @Path("{idNoticia}/categoria/{idCategoria}")
    @Produces(MediaType.APPLICATION_JSON)
    public void unassignCategoriaNoticia(
            @ApiParam(value = "Identificador de categoria de noticia", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Identificador de noticia", required = true)
            @PathParam("idNoticia") String idNoticia) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_NOTICIA_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.NEWS_FEED_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        noticiaCategoriaBean.unassignCategoriaNoticia(idCategoria, idNoticia, usuarioContext, request.getLocale());
    }
}
