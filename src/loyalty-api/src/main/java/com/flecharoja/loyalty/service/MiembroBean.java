package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.MiembroAtributo;
import com.flecharoja.loyalty.model.PeticionEnvioNotificacionCustom;
import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.util.EnvioNotificacion;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia
 * relacionados a miembros
 *
 * @author svargas, wtencio
 */
@Stateless
public class MiembroBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;

    @EJB
    RegistroMetricaService registroMetricaService;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Obtencion de miembros en rango de registros (registro & cantidad) y
     * filtrado opcionalmente por indicadores y terminos de busqueda sobre
     * algunos campos
     *
     * @param estados Listado de valores de indicadores de estados deseados
     * @param generos Listado de indicadores de genero de miembros deseados
     * @param estadosCivil Listado de indicadores de estados civiles deseados
     * @param educacion Listado de indicadores de educacion
     * @param contactoEmail
     * @param contactoSMS
     * @param busqueda Terminos de busqueda
     * @param filtros Listado de campos sobre que aplicar los terminos de
     * busqueda
     * @param tieneHijos
     * @param contactoEstado
     * @param cantidad Cantidad de registros deseados
     * @param contactoNotificacion
     * @param registro Numero de registro inicial en el resultado
     * @return Listado de miembros deseados
     */
    public Map<String, Object> getMiembros(List<String> estados, List<String> generos, List<String> estadosCivil, List<String> educacion, String contactoEmail, String contactoSMS, String contactoNotificacion, String contactoEstado, String tieneHijos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Miembro> root = query.from(Miembro.class);

        query = Busquedas.getCriteriaQueryMiembros(cb, query, root, estados, generos, estadosCivil, educacion, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //recurrido del resultado para la asignacion de informacion adicional (almacenada en keycloak)
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            resultado.stream().forEach((t) -> {
                Miembro miembro = (Miembro) t;
                try {
                    miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
                } catch(Exception e) {
                    miembro.setNombreUsuario("Prueba");
                }
            });
        }
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * Metodo que obtiene un miembro almacenados segun su id
     *
     * @param idMiembro Identificador de un miembro en la bases de datos
     * @param locale
     * @return Respuesta con un miembro con toda su informacion
     */
    public Miembro getMiembro(String idMiembro, Locale locale) {
        Miembro miembro = em.find(Miembro.class, idMiembro);
        //verificacion que la entidad exista
        if (miembro == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }
        //establecimiento de atributos adicionales de keycloak
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            try {
                miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
            } catch(Exception e) {
                miembro.setNombreUsuario("-");
            }
        }
        return miembro;
    }

    /**
     * Metodo que verifica si existe el nombre de usuario
     *
     * @param userName
     * @param locale
     * @return Respuesta con un miembro con toda su informacion
     */
    public boolean existsUserName(String userName, Locale locale) {
        String idMiembro;
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            try {
                idMiembro = keycloakUtils.getIdMember(userName);
            } catch(Exception e) {
                idMiembro = null;
            }
        }
        return idMiembro != null;

    }

    /**
     * Metodo que guarda en almacenamiento un nuevo miembro
     *
     * @param miembro Objeto con la informacion necesaria de un miembro
     * @param locale
     * @return Respuesta con el identificador asignado del miembro almacenado
     */
    public String insertMiembro(Miembro miembro, Locale locale) {
        if (miembro == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        //verificacion que los atributos de correo y contraseña esten presentes
        if (miembro.getContrasena() == null || miembro.getNombreUsuario() == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "pass, email");
        }

        if (miembro.getPaisResidencia() != null && !existsPais(miembro.getPaisResidencia())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "country_invalid");
        }

        if (existsUserName(miembro.getNombreUsuario(), locale)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "member_username_exists");
        }

        if (existsIdentificador(miembro.getDocIdentificacion())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "member_id_document_exists");
        }
        if (existsCorreo(miembro.getCorreo())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "member_email_exists");
        }
        if (!validatePassword(miembro.getContrasena())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "pass_invalid");
        }

        String idMiembro;
        String username = miembro.getNombreUsuario();
        String password = miembro.getContrasena();
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            idMiembro = keycloakUtils.createMember(miembro.getNombreUsuario(), miembro.getContrasena(), miembro.getCorreo());
            keycloakUtils.resetPasswordMember(idMiembro, miembro.getContrasena());
        } catch (Exception e) {
            if (e instanceof MyException) {
                throw e;
            } else {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "register_to_idp_failed");
            }
        }
        if (idMiembro == null) {
            throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "register_to_idp_failed");
        }

        try {
            //si la imagen viene nula, se le establece un url de imagen predefinida
            if (miembro.getAvatar() == null || miembro.getAvatar().trim().isEmpty()) {
                miembro.setAvatar(Indicadores.URL_IMAGEN_PREDETERMINADA);
            } else {
                //si no, se le intenta subir a cloudfiles
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    miembro.setAvatar(filesUtils.uploadImage(miembro.getAvatar(), MyAwsS3Utils.Folder.AVATAR_MIEMBRO, null));
                } catch (Exception e) {
                    //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }

            //fecha actual para las fechas de registro y modificacion
            Date date = Calendar.getInstance().getTime();
            //registrando en la base de datos
            //registro el uuid en el usuario de la base de datos
            miembro.setIdMiembro(idMiembro);
            //establecimiento de otros atributos por defecto
            miembro.setFechaCreacion(date);
            miembro.setFechaModificacion(date);
            miembro.setNumVersion(new Long(1));
            miembro.setIndEstadoMiembro(Miembro.Estados.ACTIVO.getValue());

            //conversion de valores de atributos opcionales a su par en mayuscula
            miembro.setIndEducacion(miembro.getIndEducacion() != null ? Character.toUpperCase(miembro.getIndEducacion()) : null);
            miembro.setIndEstadoCivil(miembro.getIndEstadoCivil() != null ? Character.toUpperCase(miembro.getIndEstadoCivil()) : null);
            miembro.setIndGenero(miembro.getIndGenero() != null ? Character.toUpperCase(miembro.getIndGenero()) : null);

            miembro.setIndContactoEmail(Boolean.TRUE);
            miembro.setIndContactoEstado(Boolean.TRUE);
            miembro.setIndContactoNotificacion(Boolean.TRUE);
            miembro.setIndContactoSms(Boolean.TRUE);
            miembro.setIndCambioPass(true);
            
            List<ConfiguracionGeneral> lista = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
            if (lista.isEmpty()) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "general_configuration_not_found");
            }
            
            ConfiguracionGeneral configuracion = lista.get(0);

            try {
                em.persist(miembro);
                em.flush();
            } catch (ConstraintViolationException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
                //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
                if (!miembro.getAvatar().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                    try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                        filesUtils.deleteFile(miembro.getAvatar());
                    } catch (Exception ex) {
                        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                    }
                }
                try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
                    keycloakUtils.deleteMember(idMiembro);
                }
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }

            bitacoraBean.logAccion(Miembro.class, idMiembro, miembro.getUsuarioCreacion(), Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), configuracion.getIdMetricaInicial().getIdMetrica(), miembro.getIdMiembro(), RegistroMetrica.TiposGane.BIENVENIDA, configuracion.getBonoEntrada(), null));
            
            return miembro.getIdMiembro();
        } catch (MyException e) {
            try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
                keycloakUtils.deleteMember(idMiembro);
            }
            throw e;
        }
    }
    
    /**
     * trabajos extras tras el registro de un miembro (uso interno)
     * 
     * @param miembro data de miembro
     * @param locale locale
     */
    @Asynchronous
    public void postRegistroMiembro(Miembro miembro, Locale locale) {
        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.REGISTRO_MIEMBRO, null, miembro.getIdMiembro());
        } catch (Exception ex) {
            Logger.getLogger(MiembroBean.class.getName()).log(Level.WARNING, null, ex);
        }
        
        ConfiguracionGeneral configuracion = em.find(ConfiguracionGeneral.class, 0L);
        
        if (configuracion.getBonoEntrada()!=null && configuracion.getIdMetricaInicial()!=null && configuracion.getBonoEntrada().compareTo(0d)>0) {
            try {
                registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), configuracion.getIdMetricaInicial().getIdMetrica(), miembro.getIdMiembro(), RegistroMetrica.TiposGane.BIENVENIDA, configuracion.getBonoEntrada(), null, null));
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
        }
        
        PeticionEnvioNotificacionCustom peticionEnvioNotificacion = new PeticionEnvioNotificacionCustom();
        ResourceBundle customNotifications;
        try {
            customNotifications = ResourceBundle.getBundle("custom_notifications.welcome_credentials", locale);
        } catch (NullPointerException | MissingResourceException e) {
            customNotifications = ResourceBundle.getBundle("custom_notifications.welcome_credentials", Locale.US);
        }
        peticionEnvioNotificacion.setCuerpo(customNotifications.getString("body"));
        peticionEnvioNotificacion.setTitulo(customNotifications.getString("subject"));
        peticionEnvioNotificacion.getMiembros().add(miembro.getIdMiembro());
        peticionEnvioNotificacion.getParametros().put("username", miembro.getNombreUsuario());
        peticionEnvioNotificacion.getParametros().put("password", miembro.getContrasena());
        peticionEnvioNotificacion.setTipoMsj(PeticionEnvioNotificacionCustom.Tipos.EMAIL.getValue());

        new EnvioNotificacion().enviarNotificacion(peticionEnvioNotificacion);
    }

    /**
     * Metodo que modifica en almacenamiento la informacion de un miembro
     * existente
     *
     * @param miembro Objeto con la informacion de un miembro existente a
     * actualizar
     * @param locale
     */
    public void editMiembro(Miembro miembro, Locale locale) {
        if (miembro == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        Miembro original = em.find(Miembro.class, miembro.getIdMiembro());
        String emailAnterior = original.getCorreo();
        Character estadoAnterior = original.getIndEstadoMiembro();
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        if (miembro.getCorreo() == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "email");
        }

        if (miembro.getPaisResidencia() != null && !existsPais(miembro.getPaisResidencia())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "country_invalid");
        }

        if (!miembro.getDocIdentificacion().equals(original.getDocIdentificacion())) {
            if (existsIdentificador(miembro.getDocIdentificacion())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "member_id_document_exists");
            }
        }

        if (!miembro.getCorreo().equals(original.getCorreo())) {
            if (existsCorreo(miembro.getCorreo())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "member_id_document_exists");
            } else {
                try (MyKeycloakUtils keycloak = new MyKeycloakUtils(locale)) {
                    keycloak.editEmail(miembro.getIdMiembro(), miembro.getCorreo());
                }
            }
        }

        String currentUrl = (String) em.createNamedQuery("Miembro.getAvatarFromMiembro").setParameter("idMiembro", miembro.getIdMiembro()).getSingleResult();
        String newUrl = null;
        //si el atributo de imagen viene nula (no deberia)
        if (miembro.getAvatar() == null || miembro.getAvatar().trim().isEmpty()) {
            miembro.setAvatar(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else //ver si el valor en el atributo de imagen, difiere del almacenado
        if (!currentUrl.equals(miembro.getAvatar())) {
            //se le intenta subir
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                newUrl = filesUtils.uploadImage(miembro.getAvatar(), MyAwsS3Utils.Folder.AVATAR_MIEMBRO, null);
                miembro.setAvatar(newUrl);
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }

        //establecimiento de valores por defecto
        miembro.setFechaModificacion(Calendar.getInstance().getTime());
        //conversion de valores de atributos opcionales a su par en mayuscula
        miembro.setIndEducacion(miembro.getIndEducacion() != null ? Character.toUpperCase(miembro.getIndEducacion()) : null);
        miembro.setIndEstadoCivil(miembro.getIndEstadoCivil() != null ? Character.toUpperCase(miembro.getIndEstadoCivil()) : null);
        miembro.setIndGenero(miembro.getIndGenero() != null ? Character.toUpperCase(miembro.getIndGenero()) : null);

        if (miembro.getContrasena() != null) {
            if (!validatePassword(miembro.getContrasena())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "pass_invalid");
            } else {
                try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
                    keycloakUtils.resetPasswordMember(miembro.getIdMiembro(), miembro.getContrasena());
                }
                miembro.setIndCambioPass(true);
            }
        }

        try {
            em.merge(miembro);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (newUrl != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(newUrl);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }

            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            if (estadoAnterior.equals(Miembro.Estados.ACTIVO.getValue())) {
                try (MyKeycloakUtils keycloak = new MyKeycloakUtils(locale)) {
                    try {
                        keycloak.enableDisableMember(miembro.getIdMiembro(), true);
                    } catch(Exception e2) {
                    }
                }
            } else {
                try (MyKeycloakUtils keycloak = new MyKeycloakUtils(locale)) {
                    try {
                        keycloak.enableDisableMember(miembro.getIdMiembro(), false);
                    } catch(Exception e2) {
                    }
                }
            }

            try (MyKeycloakUtils keycloak = new MyKeycloakUtils(locale)) {
                keycloak.editEmail(miembro.getIdMiembro(), emailAnterior);
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        if (newUrl != null && !currentUrl.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(currentUrl);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }

        bitacoraBean.logAccion(Miembro.class, miembro.getIdMiembro(), miembro.getUsuarioModificacion(), Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
    }

    /**
     * Metodo que deshabilita la cuenta de un miembro existente
     *
     * @param idMiembro Identificador de un miembro valido
     * @param causaSuspension Cadena de texto con informacion descriptiva de la
     * razon de suspension
     * @param usuarioContext Identificador del usuario modificador
     * @param locale
     */
    public void disableMiembro(String idMiembro, String causaSuspension, String usuarioContext, Locale locale) {
        Miembro miembro = em.find(Miembro.class, idMiembro);

        if (miembro == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        if (causaSuspension == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "causa suspension");
        }

        //se busca el usuario en keycloak y de inmediato se desabilita (enabled=false)
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            try {
                keycloakUtils.enableDisableMember(idMiembro, false);
            } catch(Exception e) {
            }
        }
        Date fecha = Calendar.getInstance().getTime();
        miembro.setIndEstadoMiembro(Miembro.Estados.INACTIVO.getValue());
        miembro.setUsuarioModificacion(usuarioContext);
        miembro.setFechaModificacion(fecha);
        miembro.setCausaSuspension(causaSuspension);
        miembro.setFechaSuspension(fecha);

        try {
            em.merge(miembro);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
                try {
                    keycloakUtils.enableDisableMember(idMiembro, true);
                } catch(Exception e2) {}
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(Miembro.class, idMiembro, usuarioContext, Bitacora.Accion.ENTIDAD_DESACTIVADA.getValue(), locale);
    }
    
    /**
     * activacion de un miembro
     * 
     * @param idMiembro id de miembro
     * @param usuarioContext id admin
     * @param locale locale
     */
    public void enableMiembro(String idMiembro, String usuarioContext, Locale locale) {
        Miembro miembro = em.find(Miembro.class, idMiembro);

        if (miembro == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        //se busca el usuario en keycloak y de inmediato se desabilita (enabled=false)
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            try {
                keycloakUtils.enableDisableMember(idMiembro, true);
            } catch(Exception e) {}
        }
        Date fecha = Calendar.getInstance().getTime();
        miembro.setIndEstadoMiembro(Miembro.Estados.ACTIVO.getValue());
        miembro.setUsuarioModificacion(usuarioContext);
        miembro.setFechaModificacion(fecha);
        miembro.setCausaSuspension(null);
        miembro.setFechaSuspension(null);

        try {
            em.merge(miembro);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
                try {
                    keycloakUtils.enableDisableMember(idMiembro, false);
                } catch(Exception e2) {}
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(Miembro.class, idMiembro, usuarioContext, Bitacora.Accion.ENTIDAD_ACTIVADA.getValue(), locale);
    }

    /**
     * Verifica si existe el pais de residencia
     *
     * @param paisResidencia
     * @return
     */
    private boolean existsPais(String paisResidencia) {
        return ((Long) em.createNamedQuery("Pais.countByAlfa3").setParameter("alfa3", paisResidencia).getSingleResult()) > 0;
    }

    /**
     * Verifica que el documento de identificacion existe o no
     *
     * @param docIdentificacion
     * @return
     */
    public boolean existsIdentificador(String docIdentificacion) {
        return ((Long) em.createNamedQuery("Miembro.countIdentificador").setParameter("docIdentificacion", docIdentificacion).getSingleResult()) > 0;
    }

    /**
     * comprobacion de existencia de correo registrado con algun miembro
     * 
     * @param correo valor del correo electronico
     * @return 
     */
    public boolean existsCorreo(String correo) {
        return ((Long) em.createNamedQuery("Miembro.countCorreo").setParameter("correo", correo).getSingleResult()) > 0;
    }

    /**
     * validacion de contraseña con un patron puesto
     * 
     * @param password valor de contraseña
     * @return 
     */
    public boolean validatePassword(String password) {
        Pattern pswNamePtrn
                = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=.,_]).{6,100})");
        Matcher mtch = pswNamePtrn.matcher(password);
        return mtch.matches();
    }

    /**
     * registro de un miembro a traves de importacion (uso interno)
     * 
     * @param miembro data de miembro
     * @param metricaGeneral metrica para bono
     * @param bonoEntrada cantidad de bono
     * @param locale locale
     */
    public void insertMiembroImport(Miembro miembro, String metricaGeneral, double bonoEntrada, Locale locale) {

        String idMiembro = null;
        try {
            try (MyKeycloakUtils keycloak = new MyKeycloakUtils(locale)) {
                idMiembro = keycloak.createMember(miembro.getNombreUsuario(), miembro.getContrasena(), miembro.getCorreo());
                keycloak.resetPasswordMember(idMiembro, miembro.getContrasena());
            }
            miembro.setIdMiembro(idMiembro);

            miembro.setAvatar(Indicadores.URL_IMAGEN_PREDETERMINADA);
            
            miembro.setIndCambioPass(true);

            em.persist(miembro);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            try (MyKeycloakUtils keycloak = new MyKeycloakUtils(locale)) {
                keycloak.deleteMember(idMiembro);
            }
        }
        bitacoraBean.logAccion(Miembro.class, idMiembro, miembro.getUsuarioCreacion(), Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);

        registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), metricaGeneral, miembro.getIdMiembro(), RegistroMetrica.TiposGane.BIENVENIDA, bonoEntrada, null, null));
        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.REGISTRO_MIEMBRO, null, miembro.getIdMiembro());
        } catch (Exception ex) {
            Logger.getLogger(MiembroBean.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    /**
     * actualizacion de un atributo de miembro
     * 
     * @param atributo indicador del atributo
     * @param valor valor del atributo
     * @param idMiembro id de miembro
     * @param usuario id admin
     * @param locale locale
     * @return id de miembro
     */
    public String actualizarAtributoMiembro(Miembro.Atributos atributo, String valor, String idMiembro, String usuario, Locale locale) {
        Miembro miembro = em.find(Miembro.class, idMiembro);
        if (miembro == null) {
            return idMiembro;
        }

        switch (atributo) {
            case NOMBRE: {
                miembro.setNombre(valor);
                break;
            }
            case APELLIDO: {
                miembro.setApellido(valor);
                break;
            }
            case APELLIDO2: {
                miembro.setApellido2(valor);
                break;
            }
            case CIUDAD_RESIDENCIA: {
                miembro.setCiudadResidencia(valor);
                break;
            }
            case CODIGO_POSTAL: {
                miembro.setCodigoPostal(valor);
                break;
            }
            case DIRECCION: {
                miembro.setDireccion(valor);
                break;
            }
            case ESTADO_RESIDENCIA: {
                miembro.setEstadoResidencia(valor);
                break;
            }
            case FECHA_NACIMIENTO: {
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    miembro.setFechaNacimiento(formatter.parse(valor));
                } catch (ParseException ex) {
                    return idMiembro;
                }
                break;
            }
            case IND_CONTACTO_EMAIL: {
                miembro.setIndContactoEmail(Boolean.valueOf(valor));
                break;
            }
            case IND_CONTACTO_ESTADO: {
                miembro.setIndContactoEstado(Boolean.valueOf(valor));
                break;
            }
            case IND_CONTACTO_NOTIFICACION: {
                miembro.setIndContactoNotificacion(Boolean.valueOf(valor));
                break;
            }
            case IND_CONTACTO_SMS: {
                miembro.setIndContactoSms(Boolean.valueOf(valor));
                break;
            }
            case IND_EDUCACION: {
                miembro.setIndEducacion(Miembro.ValoresIndEducacion.get(valor.charAt(0)).getValue());
                break;
            }
            case IND_ESTADO_CIVIL: {
                miembro.setIndEstadoCivil(Miembro.ValoresIndEstadoCivil.get(valor.charAt(0)).getValue());
                break;
            }
            case IND_GENERO: {
                miembro.setIndGenero(Miembro.ValoresIndGenero.get(valor.charAt(0)).getValue());
                break;
            }
            case IND_HIJOS: {
                miembro.setIndHijos(Boolean.valueOf(valor));
                break;
            }
            case INGRESO_ECONOMICO: {
                miembro.setIngresoEconomico(Double.valueOf(valor));
                break;
            }
            case PAIS_RESIDENCIA: {
                if (existsPais(valor)) {
                    miembro.setPaisResidencia(valor);
                } else {
                    return idMiembro;
                }
                break;
            }
            case TELEFONO_MOVIL: {
                miembro.setTelefonoMovil(valor);
                break;
            }
            case CORREO: {
                if (!existsCorreo(valor)) {
                    miembro.setCorreo(valor);
                } else {
                    return idMiembro;
                }

                break;
            }
            case CONTRASENA: {
                if (!validatePassword(valor)) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "pass_invalid");
                }
                try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
                    keycloakUtils.resetPasswordMember(idMiembro, valor);
                }
                miembro.setIndCambioPass(true);
                break;
            }
        }
        try {
            em.merge(miembro);
            bitacoraBean.logAccion(Miembro.class, miembro.getIdMiembro(), miembro.getUsuarioCreacion(), Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
        return null;
    }

    /**
     * obtencion de miembros referenciados por miembro
     * 
     * @param idMiembro identificador de miembro
     * @param cantidad cantidad (pagina)
     * @param registro registro (pagina)
     * @param locale locale
     * @return map con valores de paginacion y lista de miembros
     */
    public Map<String, Object> getMiembrosReferenciados(String idMiembro, int cantidad, int registro, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createNamedQuery("Miembro.countMiembroReferente").setParameter("idMiembro", idMiembro).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createNamedQuery("Miembro.findMiembroReferente")
                    .setParameter("idMiembro", idMiembro)
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //recurrido del resultado para la asignacion de informacion adicional (almacenada en keycloak)
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            resultado.stream().forEach((t) -> {
                Miembro miembro = (Miembro) t;
                try {
                    miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
                } catch(Exception e) {
                    miembro.setNombreUsuario("Prueba");
                }
            });
        }
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }
    
    /**
     * Metodo que obtiene la cantidad de miembros activos e inactivos
     */
    public JsonObject getEstadisticasMiembrosEstado(Locale locale) {
        return Json.createObjectBuilder()
                .add("cantMiembrosInactivos", ((long) em.createNamedQuery("Miembro.countMiembroEstado").setParameter("indEstado", Miembro.Estados.INACTIVO.getValue()).getSingleResult()))
                .add("cantMiembrosActivos", ((long) em.createNamedQuery("Miembro.countMiembroEstado").setParameter("indEstado", Miembro.Estados.ACTIVO.getValue()).getSingleResult()))
                .build();
    }
    
    /**
     * Método que devuelve la cantidad de miembros nuevos en el sistema en un rango de un mes
     */
    public Long cantidadMiembrosNuevos(Locale locale) {
        LocalDate fecha = LocalDate.now();
        fecha.minusMonths(1);
        return (Long) em.createNamedQuery("Miembro.countMiembrosNuevos").setParameter("fecha", (Date) java.sql.Date.valueOf(fecha)).getSingleResult();
    }

    /**
     * registro de miembro por importacion extendida (uso interno)
     * 
     * @param miembro data de miembro
     * @param atributosDinamicos data de atributos dinamicos
     * @param locale locale
     * @return id de miembro
     */
    public String insertMiembroExtendido(Miembro miembro, Map<String, String> atributosDinamicos, Locale locale) {
        
        String contrasena = miembro.getContrasena();
        if (contrasena==null) {
            do {                
                contrasena = RandomStringUtils.random(15, 0, 73, false, false, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#$%^&+=.,_".toCharArray(), new SecureRandom());
            } while (validatePassword(contrasena));
        } else {
            if (!validatePassword(contrasena)) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "attributes_invalid", "contrasena");
            }
        }
        if (existsIdentificador(miembro.getDocIdentificacion())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "member_id_document_exists");
        }
        
        if (miembro.getIndCambioPass()==null) {
            miembro.setIndCambioPass(true);
        }
        if (miembro.getIndContactoEmail()==null) {
            miembro.setIndContactoEmail(true);
        }
        if (miembro.getIndContactoEstado()==null) {
            miembro.setIndContactoEstado(true);
        }
        if (miembro.getIndContactoNotificacion()==null) {
            miembro.setIndContactoNotificacion(true);
        }
        if (miembro.getIndContactoSms()==null) {
            miembro.setIndContactoSms(true);
        }
        if (miembro.getIndMiembroSistema()==null) {
            miembro.setIndMiembroSistema(true);
        }
        if (miembro.getIndEstadoMiembro()==null) {
            miembro.setIndEstadoMiembro(Miembro.Estados.ACTIVO.getValue());
        }
        if (miembro.getAvatar()==null) {
            miembro.setAvatar(Indicadores.URL_IMAGEN_PREDETERMINADA);
        }
        Date fecha = new Date();
        miembro.setFechaCreacion(fecha);
        miembro.setFechaModificacion(fecha);
        if (miembro.getFechaIngreso()==null) {
            miembro.setFechaIngreso(fecha);
        }
        
        String idMiembro;
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            idMiembro = keycloakUtils.createMember(miembro.getNombreUsuario(), contrasena, miembro.getCorreo());
            if (miembro.getIndEstadoMiembro()==Miembro.Estados.INACTIVO.getValue()) {
                keycloakUtils.enableDisableMember(idMiembro, false);
                miembro.setFechaSuspension(fecha);
            }
        }
        miembro.setIdMiembro(idMiembro);
        
        em.persist(miembro);
        atributosDinamicos.entrySet().forEach((atributoDinamicoData) -> {
            em.merge(new MiembroAtributo(idMiembro, atributoDinamicoData.getKey(), atributoDinamicoData.getValue()));
        });
        
        try {
            em.flush();
        } catch (Exception e) {
            try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
                keycloakUtils.deleteMember(idMiembro);
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "could_not_be_persisted");
            }
        }
        
        return idMiembro;
    }
}
