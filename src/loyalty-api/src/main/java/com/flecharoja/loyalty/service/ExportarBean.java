/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.AtributoDinamico;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.MiembroAtributo;
import com.flecharoja.loyalty.model.MiembroAtributoPK;
import com.flecharoja.loyalty.model.TrabajosInternos;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author wtencio
 */
@Stateless
public class ExportarBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    TrabajoBean trabajoBean;

    private final Configuration configuration;

    public ExportarBean() {
        this.configuration = new Configuration();
        this.configuration.addResource("conf/hbase/hbase-site.xml");
    }

    /**
     * exportacion de miembros de un segmento en csv (metodo interno)
     * 
     * @param idTrabajo identificador del trabajo asignado
     * @param idSegmento identificador del segmento objetivo
     * @param locale locale
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    @Asynchronous
    public void exportarMiembrosSegmentosCSV(String idTrabajo, String idSegmento, Locale locale) {

        //busca los id's de los miembros del segmento
        List<String> idsMiembros = new ArrayList<>();
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "ELEGIBLES_SEGMENTO"));
            Get get = new Get(Bytes.toBytes(idSegmento));
            get.addFamily(Bytes.toBytes("MIEMBROS"));
            Result result = table.get(get);
            if (table.exists(get)) {
                idsMiembros = result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                        .filter((t) -> Bytes.toBoolean(t.getValue()))
                        .map((t) -> Bytes.toString(t.getKey()))
                        .collect(Collectors.toList());
            } else {
                idsMiembros = new ArrayList<>();
            }
        } catch (IOException e) {
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "Problemas con HBase", Double.valueOf(0));
        }

        //definicion de columnas
        StringBuilder builder = new StringBuilder();
        String ColumnNamesList = "idMiembro, docIdentificacion ,fechaIngreso, indEstadoMiembro, fechaExpiracion, direccion, ciudadResidencia, estadoResidencia, paisResidencia, codigoPostal, telefonoMovil, fechaNacimiento, indGenero, indEstadoCivil, frecuenciaCompra, fechaSuspension, causaSuspension, indEducacion, ingresoEconomico, comentarios, fechaCreacion, usuarioCreacion, fechaModificacion, usuarioModificacion, avatar, nombre, apellido, apellido2, contrasena, correo, nombreUsuario, indContactoEmail, indContactoSms, indContactoNotificacion, indContactoEstado, indHijos\n";
        builder.append(ColumnNamesList);

        //divicion del numero de miembros
        int buckets = 5;
        double completado = 0;
        int divide = idsMiembros.size() / buckets;
        if (idsMiembros.size() % buckets != 0) {
            divide++;
        }
        List<List<String>> partition = Lists.partition(idsMiembros, divide);
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            for (List<String> list : partition) {
                for (String idMiembro : list) {
                    //obtencion de la informacion del miembro
                    Miembro miembro = em.find(Miembro.class, idMiembro);
                    try {
                        miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
                    } catch(Exception e) {
                        miembro.setNombreUsuario("-");
                    }
                    
                    builder.append(miembro.toString());
                }
                //actualizacion del progreso
                completado += 20;
                if (completado <= 80) {
                    trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.PROCESANDO.getValue(), "Procesado " + completado + "% del archivo", completado);
                }
            }
        }

        byte[] bytes = builder.toString().getBytes();

        //almacenamiento del archivo resultante a s3
        String url;
        try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
            url = filesUtils.uploadFile(bytes, MyAwsS3Utils.Folder.EXPORTACIONES_SEGMENTO, idSegmento + ".csv");
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.COMPLETADO.getValue(), url, Double.valueOf(100));
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "Ocurrio un error con CloudFiles", Double.valueOf(0));
        }
    }

    /**
     * exportacion de miembros a csv
     * 
     * @param idTrabajo identificador del trabajo asignado
     * @param locale locale
     */
    public void exportarMiembrosCSV(String idTrabajo, Locale locale) {
        //obtengo los atributos dinamicos
        List<AtributoDinamico> atributos = em.createNamedQuery("AtributoDinamico.findAllActivos")
                .setParameter("indEstado", AtributoDinamico.Estados.PUBLICADO.getValue()).getResultList();

        //Obtengo la lista de miembros
        List<Miembro> miembros = em.createNamedQuery("Miembro.findAll").getResultList();

        StringBuilder builder = new StringBuilder();
        //atributos estaticos
        String columnNamesList = "idMiembro, nombre, apellido, genero, ciudad,estado residencia, fecha ingreso, fecha nacimiento,";
        //agrego los atributos dinamicos
        for (AtributoDinamico atributo : atributos) {
            if (atributo == atributos.get(atributos.size() - 1)) {
                columnNamesList += atributo.getNombre() + "\n";
            } else {
                columnNamesList += atributo.getNombre() + ", ";
            }
        }
        builder.append(columnNamesList);

        int buckets = 5;
        double completado = 0;
        int divide = miembros.size() / buckets;
        if (miembros.size() % buckets != 0) {
            divide++;
        }
        List<List<Miembro>> partition = Lists.partition(miembros, divide);
        for (List<Miembro> list : partition) {
            for (Miembro miembro : list) {
                String toStringMiembro = miembro.toString2();
                for (AtributoDinamico atributo : atributos) {
                    MiembroAtributo relacion = em.find(MiembroAtributo.class, new MiembroAtributoPK(miembro.getIdMiembro(), atributo.getIdAtributo()));
                    if(relacion == null){
                        toStringMiembro+=atributo.getValorDefecto()+", ";
                    }else{
                        toStringMiembro+=relacion.getValor()+", ";
                    }
                }
                toStringMiembro+="\n";
                builder.append(toStringMiembro);
            }
            completado += 20;
            if (completado <= 80) {
                trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.PROCESANDO.getValue(), "Procesado " + completado + "% del archivo", completado);
            }
        }

        byte[] bytes = builder.toString().getBytes();

        String url;
        try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
            url = filesUtils.uploadFile(bytes, MyAwsS3Utils.Folder.EXPORTACIONES_MIEMBRO, "members.csv");
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.COMPLETADO.getValue(), url, Double.valueOf(100));
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
            trabajoBean.actualizarTrabajo(idTrabajo, TrabajosInternos.Estados.FALLIDO.getValue(), "Ocurrio un error con CloudFiles", Double.valueOf(0));
        }
    }

}
