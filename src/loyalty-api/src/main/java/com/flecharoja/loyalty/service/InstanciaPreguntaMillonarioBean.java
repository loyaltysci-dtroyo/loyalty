/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.InstanciaMillonario;
import com.flecharoja.loyalty.model.InstanciaPreguntaMillonario;
import com.flecharoja.loyalty.model.PreguntaMillonario;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author kevin
 */

@Stateless
public class InstanciaPreguntaMillonarioBean {
    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    UtilBean utilBean;//EJB con metodos de negocio para el manejo de utilidades

    @EJB
    BitacoraBean bitacoraBean;//EJB con metodos de negocio para el manejo de bitacora
    
    @EJB PreguntaMillonarioBean preguntaMillonarioBean;

    /**
     * Método que registra una nueva insignia en la base de datos
     *
     * @param idInstanciaMillonario
     * @param preguntaMillonario
     * @param usuario usuario en sesión
     * @param locale
     */
    public void insertInstanciaPreguntaMillonario(String idInstanciaMillonario, PreguntaMillonario preguntaMillonario, String usuario, Locale locale) {
        InstanciaPreguntaMillonario instanciaPregunta = new InstanciaPreguntaMillonario();
        instanciaPregunta.setIdInstanciaJuego(idInstanciaMillonario);
        instanciaPregunta.setPregunta(preguntaMillonario.getPregunta());
        instanciaPregunta.setPuntos(preguntaMillonario.getPuntos());
        instanciaPregunta.setRespuesta1(preguntaMillonario.getRespuesta1());
        instanciaPregunta.setRespuesta2(preguntaMillonario.getRespuesta2());
        instanciaPregunta.setRespuesta3(preguntaMillonario.getRespuesta3());
        instanciaPregunta.setRespuesta4(preguntaMillonario.getRespuesta4());
        instanciaPregunta.setRespuestaCorrecta(preguntaMillonario.getRespuestaCorrecta());
        instanciaPregunta.setNumVersion(new Long(1));
        instanciaPregunta.setFechaCreacion(Calendar.getInstance().getTime());
        instanciaPregunta.setFechaModificacion(Calendar.getInstance().getTime());
        instanciaPregunta.setUsuarioModificacion(usuario);
        instanciaPregunta.setUsuarioCreacion(usuario);
        
        try {
            em.persist(instanciaPregunta);
            em.flush();
            //bitacoraBean.logAccion(Insignias.class, nuevaInstancia.getIdJuego(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }
    
    /**
     * Metodo que ontiene las preguntas de un juego
     * existente
     *
     * @param idInstanciaJuego
     * @param locale 
     */
    public void deletePreguntas(String idInstanciaJuego, Locale locale) {
        if (idInstanciaJuego == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        //verificacion de la existencia de la entidad
        List<InstanciaPreguntaMillonario> preguntas;
            preguntas = em.createNamedQuery("InstanciaPreguntaMillonario.findByInstanciaMillonario")
                    .setParameter("idInstanciaJuego", idInstanciaJuego)
                    .getResultList();
         
        try {
            for (int i = 0; i < preguntas.size(); i++) {
                em.remove(preguntas.get(i));
                em.flush();
            }
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }
}
