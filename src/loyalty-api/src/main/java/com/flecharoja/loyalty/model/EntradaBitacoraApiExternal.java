package com.flecharoja.loyalty.model;

import com.fasterxml.jackson.annotation.JsonRawValue;
import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class EntradaBitacoraApiExternal {
    
    private Date fecha;
    private String idEntrada;
    
    private String httpEstadoRespuesta;
    private String jsonRespuesta;
    private String jsonEntrada;

    public EntradaBitacoraApiExternal(Date fecha, String idEntrada, String httpEstadoRespuesta, String jsonRespuesta, String jsonEntrada) {
        this.fecha = fecha;
        this.idEntrada = idEntrada;
        this.httpEstadoRespuesta = httpEstadoRespuesta;
        this.jsonRespuesta = jsonRespuesta;
        this.jsonEntrada = jsonEntrada;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getIdEntrada() {
        return idEntrada;
    }

    public void setIdEntrada(String idEntrada) {
        this.idEntrada = idEntrada;
    }

    public String getHttpEstadoRespuesta() {
        return httpEstadoRespuesta;
    }

    public void setHttpEstadoRespuesta(String httpEstadoRespuesta) {
        this.httpEstadoRespuesta = httpEstadoRespuesta;
    }

    @JsonRawValue
    public String getJsonRespuesta() {
        return jsonRespuesta;
    }

    public void setJsonRespuesta(String jsonRespuesta) {
        this.jsonRespuesta = jsonRespuesta;
    }

    @JsonRawValue
    public String getJsonEntrada() {
        return jsonEntrada;
    }

    public void setJsonEntrada(String jsonEntrada) {
        this.jsonEntrada = jsonEntrada;
    }
}
