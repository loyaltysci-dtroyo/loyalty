package com.flecharoja.loyalty.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class EntradaRegistroActividad {
    
    public enum TiposActividad {
        ENVIO_NOTIFICACION("A"),
        ABRIO_NOTIFICACION("B"),
        VIO_MISION("C"),
        APROBO_MISION("D"),
        FALLO_MISION("E"),
        RESPONDIO_MISION("F"),
        VIO_PROMOCION("G"),
        MARCO_PROMOCION("H"),
        GANO_METRICA("I"),
        REDIMIO_PREMIO("J"),
        RECIBIO_PREMIO("K");
        
        private final String value;
        private static final Map<String, TiposActividad> lookup = new HashMap<>();

        private TiposActividad(String value) {
            this.value = value;
        }
        
        static {
            for (TiposActividad actividad : values()) {
                lookup.put(actividad.value, actividad);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static TiposActividad get(String value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    private Date fecha;
    
    private String indTipo;
    private String indTipoGane;
    
    private String idElemento;
    private String nombreElemento;
    private String nombreInternoElemento;
    
    private String idMiembro;
    private String nombreMiembro;

    public EntradaRegistroActividad() {
    }
    
    public EntradaRegistroActividad(String idElemento, String nombreElemento, String nombreInternoElemento, Date fecha, String indTipo, String idMiembro, String nombreMiembro) {
        this.fecha = fecha;
        this.indTipo = indTipo;
        this.idElemento = idElemento;
        this.nombreElemento = nombreElemento;
        this.nombreInternoElemento = nombreInternoElemento;
        this.idMiembro = idMiembro;
        this.nombreMiembro = nombreMiembro;
    }
    
    public EntradaRegistroActividad(Date fecha, String indTipo, String idMiembro, String nombreMiembro) {
        this.fecha = fecha;
        this.indTipo = indTipo;
        this.idMiembro = idMiembro;
        this.nombreMiembro = nombreMiembro;
    }
    
    public EntradaRegistroActividad(String idElemento, String nombreElemento, String nombreInternoElemento, Date fecha, String indTipo) {
        this.fecha = fecha;
        this.indTipo = indTipo;
        this.idElemento = idElemento;
        this.nombreElemento = nombreElemento;
        this.nombreInternoElemento = nombreInternoElemento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(String indTipo) {
        this.indTipo = indTipo;
    }

    public String getIndTipoGane() {
        return indTipoGane;
    }

    public void setIndTipoGane(String indTipoGane) {
        this.indTipoGane = indTipoGane;
    }

    public String getIdElemento() {
        return idElemento;
    }

    public void setIdElemento(String idElemento) {
        this.idElemento = idElemento;
    }

    public String getNombreElemento() {
        return nombreElemento;
    }

    public void setNombreElemento(String nombreElemento) {
        this.nombreElemento = nombreElemento;
    }

    public String getNombreInternoElemento() {
        return nombreInternoElemento;
    }

    public void setNombreInternoElemento(String nombreInternoElemento) {
        this.nombreInternoElemento = nombreInternoElemento;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getNombreMiembro() {
        return nombreMiembro;
    }

    public void setNombreMiembro(String nombreMiembro) {
        this.nombreMiembro = nombreMiembro;
    }
}
