package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.RespuestaMision;
import com.flecharoja.loyalty.model.RespuestaMisionEdicion;
import com.flecharoja.loyalty.service.RespuestasMisionBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Recursos para la lectura de respuestas de una mision por parte de todos los
 * miembros y la aprobacion/rechazo de cada respuesta pendiente
 *
 * @author svargas
 */
@Api(value = "Mision")
@Path("mision/{idMision}/respuestas")
public class MisionRespuestasResource {

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión
    
    @Context
    HttpServletRequest request;
    
    @EJB
    RespuestasMisionBean bean;

    /**
     * Identificador de promocion
     */
    @PathParam("idMision")
    String idMision;
    
    /**
     * Lectura de respuestas por estado en un rango de registros de las
     * respuestas de mision
     *
     * @param indEstado Indicador de estado
     * @param tipoOrden Indicador de orden de registros por fecha de respuesta
     * en orden DESCendiente o ASCendiente
     * @param cantidad Cantidad de registros en la respuesta
     * @param registro Numero de registro inicial deseado en la respuesta
     * @return Lista de respuestas de misiones
     */
    @ApiOperation(value = "Obtencion de respuestas de mision por miembros", response = RespuestaMision.class, responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRespuestas(
            @ApiParam(value = "Indicador del estado de las respuestas deseadas", defaultValue = "P") @DefaultValue("P") @QueryParam("estado") String indEstado,
            @ApiParam(value = "Indicador del tipo de orden de registros por fecha de respuesta", defaultValue = "DESC") @DefaultValue("DESC") @QueryParam("tipo-orden") String tipoOrden,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = bean.getRespuestasPorEstado(idMision, indEstado, registro, cantidad, request.getLocale(), tipoOrden);
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }
    
    /**
     * Metodo que cambia de estado a una respuesta de mision de un miembro en
     * una fecha especifica
     *
     * @param respuestaMision Entidad con el identificador del miembro, fecha
     * del registro y estado de respuesta deseado
     */
    @ApiOperation(value = "Edicion de estados de respuestas de mision de miembros")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void editRespuesta(
            @ApiParam(value = "Datos de la respuesta a editar") RespuestaMisionEdicion respuestaMision){
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.editRespuesta(idMision, respuestaMision, request.getLocale());
    }
}
