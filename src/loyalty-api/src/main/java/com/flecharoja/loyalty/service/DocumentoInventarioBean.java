/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.DocumentoDetalle;
import com.flecharoja.loyalty.model.DocumentoInventario;
import com.flecharoja.loyalty.model.HistoricoMovimientos;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class DocumentoInventarioBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    @EJB
    BitacoraBean bitacoraBean;

    @EJB
    ExistenciasUbicacionBean existenciasUbicacionBean;

    @EJB
    HistoricoMovimientosBean historicoBean;

    /**
     * Método que inserta un nuevo documento de inventario en la base de datos
     *
     * @param documento informacion del documento
     * @param usuario en sesion
     * @param locale
     * @return identificador del nuevo documento
     */
    public String insertDocumento(DocumentoInventario documento, String usuario, Locale locale) {
        //verifica que el documento no venga nulo
        if (documento == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }

        //verifica que el tipo y la ubicacion no sean nulas
        if (DocumentoInventario.Tipos.get(documento.getTipo()) == null || documento.getUbicacionOrigen() == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "tipo, ubicacion origen");
        }
        //verifica si el tipo es interubicacion la ubicacion destino no sea nula
        if (documento.getTipo().equals(DocumentoInventario.Tipos.INTER_UBICACION.getValue()) && documento.getUbicacionDestino() == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "ubicacion destino");
        }

        if (existsCodigoDocumento(documento.getCodigoDocumento())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "internal_code_already_exists");
        }

        //seteo de valores necesarios
        Date date = Calendar.getInstance().getTime();
        documento.setFecha(date);
        documento.setFechaCreacion(date);
        documento.setFechaModificacion(date);
        documento.setUsuarioCreacion(usuario);
        documento.setUsuarioModificacion(usuario);
        documento.setNumVersion(1L);
        documento.setIndCerrado(false);

        try {
            em.persist(documento);
            em.flush();
            bitacoraBean.logAccion(DocumentoInventario.class, documento.getNumDocumento(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        return documento.getNumDocumento();
    }

    /**
     * Método que elimina un documento mientras este no tenga detalles y no se
     * encuentre cerrado
     *
     * @param numDocumento identificador del documento
     * @param usuario en sesión
     * @param locale
     */
    public void eliminarDocumento(String numDocumento, String usuario, Locale locale) {
        DocumentoInventario documentoInventario = em.find(DocumentoInventario.class, numDocumento);
        //verifica que el documento no sea nulo
        if (documentoInventario == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "document_not_found");
        }
        //verifica que el documento no este cerrado
        if (documentoInventario.getIndCerrado()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "document_closed");
        }

        List<DocumentoDetalle> detalles = em.createNamedQuery("DocumentoDetalle.findByNumDocumento").setParameter("numDocumento", numDocumento).getResultList();
        //verifica que no tenga detalles
        if (!detalles.isEmpty()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "document_exists_details");
        }

        em.remove(documentoInventario);
        em.flush();
        bitacoraBean.logAccion(DocumentoInventario.class, numDocumento, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);

    }

    /**
     * Método que permite actualizar la informacion de un documento (tipo y
     * ubicacion origen) mientras este no tenga detalles ya registrados
     *
     * @param documentoInventario informacion del documento a actualizar
     * @param usuario en sesión
     * @param locale
     */
    public void updateDocumento(DocumentoInventario documentoInventario, String usuario, Locale locale) {
        DocumentoInventario original = em.find(DocumentoInventario.class, documentoInventario.getNumDocumento());
        if (!original.getCodigoDocumento().equals(documentoInventario.getCodigoDocumento())) {
            if (existsCodigoDocumento(documentoInventario.getCodigoDocumento())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "internal_code_already_exists");
            }
        }
        //verifico que el documento no venga null
        if (documentoInventario == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        //verifica el tipo y ubicacion origen no sea null        
        if (DocumentoInventario.Tipos.get(documentoInventario.getTipo()) == null || documentoInventario.getUbicacionOrigen() == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "tipo, ubicacion origen");
        }
        //verifica que si es interubicacion la ubicacion destino no sea nula
        if (documentoInventario.getTipo().equals(DocumentoInventario.Tipos.INTER_UBICACION.getValue()) && documentoInventario.getUbicacionDestino() == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "ubicacion destino");
        }
        //se verifica que no existan detalles de lo contrario no se puede actualiza
        List<DocumentoDetalle> detalles = em.createNamedQuery("DocumentoDetalle.findByNumDocumento").setParameter("numDocumento", documentoInventario.getNumDocumento()).getResultList();
        if (!detalles.isEmpty()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "document_exists_details");
        }
        //seteo de usuario y fecha de modificacion
        documentoInventario.setFechaModificacion(Calendar.getInstance().getTime());
        documentoInventario.setUsuarioModificacion(usuario);
        try {
            em.merge(documentoInventario);
            em.flush();
            bitacoraBean.logAccion(DocumentoInventario.class, documentoInventario.getNumDocumento(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
    }

    /**
     * Método que obtiene la información de un documento en particular
     *
     * @param numDocumento identificador del documento
     * @param locale
     * @return documento
     */
    public DocumentoInventario getDocumento(String numDocumento, Locale locale) {
        DocumentoInventario documentoInventario = em.find(DocumentoInventario.class, numDocumento);
        if (documentoInventario == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "document_not_found");
        }
        return documentoInventario;
    }

    /**
     * Método que obtiene una lista de documentos registrados, filtrados por el
     * indicador de tipo de documento y una cantidad y registros de busqueda
     *
     * @param idUbicacion
     * @param tipos
     * @param indCerrado
     * @param cantidad de registros máximos por pàgina
     * @param registro por el cual se comienza la busqueda
     * @param ordenCampo
     * @param locale
     * @param ordenTipo
     * @return lista de documentos
     */
    public Map<String, Object> getDocumentos(String idUbicacion, List<String> tipos, String indCerrado, String ordenTipo, String ordenCampo, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<DocumentoInventario> root = query.from(DocumentoInventario.class);

        query = Busquedas.getCriteriaQueryDocumentos(cb, query, root, idUbicacion, tipos, indCerrado, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();

        //retorno del resultado y encabezados sobre el rango
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;

    }

    /**
     * Método que permite cerrar un documento el cual se encuentra abierto,
     * registrando asi en las existencias de esa ubicacion de un premio en un
     * perìodo y mes definidos
     *
     * @param numDocumento ientificador del documento
     * @param usuario usuario en sesion
     * @param locale
     */
    public void closeDocumento(String numDocumento, String usuario, Locale locale) {
        DocumentoInventario documento = em.find(DocumentoInventario.class, numDocumento);
        //se verifica que el documento exista
        if (documento == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        //verifica que el documento no este ya cerrado
        if (documento.getIndCerrado()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "document_closed");
        }

        //se indica que se va a cerrar
        documento.setIndCerrado(true);
        try {

            //mandar a actualizar las existencias
            List<DocumentoDetalle> detalles = em.createNamedQuery("DocumentoDetalle.findByNumDocumento").setParameter("numDocumento", numDocumento).getResultList();
            detalles.stream().forEach((detalle) -> {
                if (documento.getTipo().equals(DocumentoInventario.Tipos.INTER_UBICACION.getValue())) {
                    existenciasUbicacionBean.updateExistencias(detalle.getCodPremio().getIdPremio(), documento.getUbicacionOrigen().getIdUbicacion(), documento.getUbicacionDestino().getIdUbicacion(), detalle.getCantidad().intValue(), 0, DocumentoInventario.Tipos.get(documento.getTipo()), usuario, locale);
                    historicoBean.insertHistorico(new HistoricoMovimientos(detalle.getPromedio(), null, detalle.getCodPremio(), documento.getUbicacionOrigen(), documento.getUbicacionDestino(), detalle.getCantidad(), documento.getTipo(), documento.getFecha(), null, documento,null), usuario, locale);
                } else if (documento.getTipo().equals(DocumentoInventario.Tipos.COMPRA.getValue())) {
                    existenciasUbicacionBean.updateExistencias(detalle.getCodPremio().getIdPremio(), documento.getUbicacionOrigen().getIdUbicacion(), null, detalle.getCantidad().intValue(), detalle.getPromedio(), DocumentoInventario.Tipos.get(documento.getTipo()), usuario, locale);
                    historicoBean.insertHistorico(new HistoricoMovimientos(detalle.getPromedio(), null, detalle.getCodPremio(), documento.getUbicacionOrigen(), null, detalle.getCantidad(), documento.getTipo(), documento.getFecha(), null, documento,null), usuario, locale);
                } else {
                    existenciasUbicacionBean.updateExistencias(detalle.getCodPremio().getIdPremio(), documento.getUbicacionOrigen().getIdUbicacion(), null, detalle.getCantidad().intValue(), 0, DocumentoInventario.Tipos.get(documento.getTipo()), usuario, locale);
                    historicoBean.insertHistorico(new HistoricoMovimientos(detalle.getPromedio(), null, detalle.getCodPremio(), documento.getUbicacionOrigen(), null, detalle.getCantidad(), documento.getTipo(), documento.getFecha(), null, documento,null), usuario, locale);

                }
            });
            em.merge(documento);
            em.flush();
            bitacoraBean.logAccion(DocumentoInventario.class, numDocumento, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    private boolean existsCodigoDocumento(String codigoDocumento) {
        return ((Long) em.createNamedQuery("Documento.existsCodigoInterno").setParameter("codigoInterno", codigoDocumento).getSingleResult()) > 0;
    }

}
