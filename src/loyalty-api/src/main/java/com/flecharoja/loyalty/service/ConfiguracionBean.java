package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.TablaPosiciones;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.validation.ConstraintViolationException;

/**
 * Clase encargada de proveer metodos para el mantenimiento de la informacion
 * general del sistema
 *
 * @author wtencio
 */


@Stateless
public class ConfiguracionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    UtilBean utilBean;//EJB con los metodos para el manejo de utilidades

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos necesarios para el manejo de bitacora

    /**
     * Método que actualiza la información general de la configuracion del
     * sistema
     *
     * @param configuracion informacion actualizada
     * @param usuario usuario en sesion
     * @param locale
     */
    public void updateConfiguracionGeneral(ConfiguracionGeneral configuracion, String usuario, Locale locale) {

        //verificacion de la existencia de la entidad
        ConfiguracionGeneral original;
        //Metrica metricaInicial;

        original = em.find(ConfiguracionGeneral.class, new Long(0));
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "general_configuration_not_found");
        }
        if (configuracion.getIdMetricaInicial() != null) {
            if(configuracion.getIdMetricaInicial().equals(original.getIdMetricaInicial()) == false){
                //metricaInicial = em.find(Metrica.class, configuracion.getIdMetricaInicial().getIdMetrica());
                //Se obtiene la cantidad de tablas de posicion asociadas a la métrica y que sean de tipo MIEMBRO
                Long cantidad = ((Long)em.createNamedQuery("TablaPosiciones.existeLeaderboard")
                    .setParameter("idMetrica", configuracion.getIdMetricaInicial().getIdMetrica())
                    .setParameter("indTipo", TablaPosiciones.Tipo.MIEMBROS.getValue()).getSingleResult());
                if (cantidad > 1) {
                    throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "metric_leaderbord_duplicated");
                }else{
                    original.setIdMetricaInicial(configuracion.getIdMetricaInicial());   
                }
            }
        }
        
        if (configuracion.getIdMetricaSecundaria() != null) {
            if(configuracion.getIdMetricaSecundaria().equals(original.getIdMetricaSecundaria()) == false){
                //metricaInicial = em.find(Metrica.class, configuracion.getIdMetricaInicial().getIdMetrica());
                //Se obtiene la cantidad de tablas de posicion asociadas a la métrica y que sean de tipo MIEMBRO
                Long cantidad = ((Long)em.createNamedQuery("TablaPosiciones.existeLeaderboard")
                    .setParameter("idMetrica", configuracion.getIdMetricaSecundaria().getIdMetrica())
                    .setParameter("indTipo", TablaPosiciones.Tipo.MIEMBROS.getValue()).getSingleResult());
                if (cantidad > 1) {
                    throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "metric_leaderbord_duplicated");
                }else{
                    original.setIdMetricaSecundaria(configuracion.getIdMetricaSecundaria());   
                }
            }
        }

        String currentUrl = (String) em.createNamedQuery("ConfiguracionGeneral.findByLogoEmpresa").setParameter("id", new Long(0)).getSingleResult();
        String newUrl = null;
        //si el atributo de imagen viene nula (no deberia)
        if (configuracion.getLogoEmpresa() == null || configuracion.getLogoEmpresa().isEmpty()) {
            original.setLogoEmpresa(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else { //ver si el valor en el atributo de imagen, difiere del almacenado
            if (!currentUrl.equals(configuracion.getLogoEmpresa())) {
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    newUrl = filesUtils.uploadImage(configuracion.getLogoEmpresa(), MyAwsS3Utils.Folder.IMAGENES_CONFIGURACION, null);
                    original.setLogoEmpresa(newUrl);
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }

        original.setNombreEmpresa(configuracion.getNombreEmpresa());
        original.setSitioWeb(configuracion.getSitioWeb());
        original.setFechaModificacion(Calendar.getInstance().getTime());
        original.setUsuarioModificacion(usuario);
        original.setBonoEntrada(configuracion.getBonoEntrada());
        original.setPorcentajeCompra(configuracion.getPorcentajeCompra());
        original.setUbicacionPrincipal(configuracion.getUbicacionPrincipal());
        original.setPeriodo(configuracion.getPeriodo());
        original.setMes(configuracion.getMes());
        original.setBonoReferencia(configuracion.getBonoReferencia());
        original.setIntegracionInventarioPremio(configuracion.getIntegracionInventarioPremio());
        original.setPoliticasSeguridad(this.updatePoliticas(configuracion.getPoliticasSeguridad(), locale));
        original.setMsjEmailDesde(configuracion.getMsjEmailDesde());
        original.setxFpSecuencia(configuracion.getxFpSecuencia());
        original.setIdProvider(configuracion.getIdProvider());
        original.setKeyEvertec(configuracion.getKeyEvertec());
        original.setIvEvertec(configuracion.getIvEvertec());
        original.setCurrencyCod(configuracion.getCurrencyCod());
        original.setUsuarioEver(configuracion.getUsuarioEver());
        original.setPasswordEver(configuracion.getUsuarioEver());
        original.setInvoiceSeq(configuracion.getInvoiceSeq());

        try {
            em.merge(original);
            em.flush();
            bitacoraBean.logAccion(ConfiguracionGeneral.class, "0", usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (newUrl != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(newUrl);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        if (newUrl != null && !currentUrl.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(currentUrl);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * actualizacion de politicas
     * 
     * @param imagen data de politicas
     * @param locale locale
     * @return url del data de politicas
     */
    private String updatePoliticas(String imagen, Locale locale) {
        String url = null;
        if (imagen != null) {
            String politicasActuales = (String) em.createNamedQuery("ConfiguracionGeneral.findByPoliticas").setParameter("id", new Long(0)).getSingleResult();
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                if (politicasActuales == null) {
                    url = filesUtils.uploadImage(imagen, MyAwsS3Utils.Folder.IMAGENES_CONFIGURACION, null);
                } else {
                    if (!politicasActuales.equals(imagen)) {
                        url = filesUtils.uploadImage(imagen, MyAwsS3Utils.Folder.IMAGENES_CONFIGURACION, null);
                    } else {
                        url = politicasActuales;
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }
        return url;
    }

    /**
     * Método que obtiene la información de la configuracion del sistema
     *
     * @param usuario usuario en sesión
     * @param locale
     * @return resultado de la operacion
     */
    public ConfiguracionGeneral getConfiguracion(String usuario, Locale locale) {
        ConfiguracionGeneral config;
        List<ConfiguracionGeneral> confi = null;
        try {
            //verifica si existe ya el registro, sino lo creo con valores todos en null
            confi = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
            if (confi.isEmpty()) {
                ConfiguracionGeneral configuracion = new ConfiguracionGeneral(new Long(0), null, usuario, Calendar.getInstance().getTime(), new Long(1));
                configuracion.setLogoEmpresa(Indicadores.URL_IMAGEN_PREDETERMINADA);
                configuracion.setIntegracionInventarioPremio(Boolean.FALSE);
                em.persist(configuracion);
                em.flush();
            }
            //obtiene la tupla con la información de la configuracion
            config = em.find(ConfiguracionGeneral.class, new Long(0));
        } catch (QueryTimeoutException e) {//en el caso de que se agote el tiempo de conexion con la bases de datos
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }
        return config;
    }

    /**
     * Método que obtiene la información basica de la empresa para usuarios sin
     * permiso
     *
     * @param usuario usuario en sesión
     * @return resultado de la operacion
     */
    public ConfiguracionGeneral getConfigBasica(String usuario, Locale locale) {
        Map<String, Object> respuesta = new HashMap<>();
        ConfiguracionGeneral config;
        List<ConfiguracionGeneral> confi = null;
        try {
            //verifica si existe ya el registro, sino lo creo con valores todos en null
            confi = em.createNamedQuery("ConfiguracionGeneral.findBasic").getResultList();
            if (confi.isEmpty()) {
                ConfiguracionGeneral configuracion = new ConfiguracionGeneral(new Long(0), null, usuario, Calendar.getInstance().getTime(), new Long(1));
                configuracion.setLogoEmpresa(Indicadores.URL_IMAGEN_PREDETERMINADA);
                em.persist(configuracion);
                em.flush();
            }
            //obtiene la tupla con la información de la configuracion
            config = em.find(ConfiguracionGeneral.class, new Long(0));
        } catch (QueryTimeoutException e) {//en el caso de que se agote el tiempo de conexion con la bases de datos
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "general_configuration_not_found");
        }

        ConfiguracionGeneral general = new ConfiguracionGeneral();
        general.setNombreEmpresa(config.getNombreEmpresa());
        general.setLogoEmpresa(config.getLogoEmpresa());
        return general;
    }
}
