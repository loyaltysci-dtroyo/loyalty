package com.flecharoja.loyalty.util;

import com.flecharoja.loyalty.exception.MyException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

/**
 *
 * @author wtencio
 */
@Stateless
public class MyKeycloakAuthz {

    private final String ADMIN_URL = "https://idp-ludis.loyaltysci.com/auth/realms/loyalty-admin/authz/entitlement/loyalty-api";
 

    public MyKeycloakAuthz() {

    }

    public List<String> getRPT(String token, Locale locale) {
        Client client = ClientBuilder.newClient();
        List<String> resources = new ArrayList<>();
        try {
            Response response = client.target(ADMIN_URL).request().header("Authorization", "bearer " + token).get();
            if (response.getStatus()!=200) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR,locale, "error_connecting_idp");
            }
            JsonObject data = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();
            String rpt = data.getString("rpt");
            String base64Url = rpt.split("[.]")[1];
            String base64 = base64Url.replace('-', '+').replace('_', '/');
            String resultado = new String(Base64.getDecoder().decode(base64));

            JsonObject object = Json.createReader(new StringReader(resultado)).readObject();
            JsonArray jsonArray = object.getJsonObject("authorization").getJsonArray("permissions");

            for (JsonValue jsonValue : jsonArray) {
                JsonObject resource = (JsonObject) jsonValue;
                resources.add(resource.getString("resource_set_name"));
            }
            return resources;
        } finally {
            client.close();
        }
    }
    
    public boolean tienePermiso(String nombreRecurso, String token, Locale locale){
        List<String> recursos = this.getRPT(token,locale);
        for (String recurso : recursos) {
            if(recurso.equals(nombreRecurso)){
                return true;
            }
        }
        return false;
    }
}
