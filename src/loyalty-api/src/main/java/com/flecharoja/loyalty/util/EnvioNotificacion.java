package com.flecharoja.loyalty.util;

import com.flecharoja.loyalty.model.PeticionEnvioNotificacion;
import com.flecharoja.loyalty.model.PeticionEnvioNotificacionCustom;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author svargas
 */
public class EnvioNotificacion {
    private static final String URL_API_INTERNO = "http://wildflyinterno:8080/loyalty-api-internal";
    private static final String PATH_API_INTERNO_ENVIO_MSJ = "webresources/v0/envio-notificacion";
    private static final String PATH_API_INTERNO_ENVIO_MSJ_PERSONALIZADO = "webresources/v0/envio-notificacion/personalizado";
    
    public boolean enviarNotificacion(PeticionEnvioNotificacionCustom peticionEnvioNotificacion) {
        Response response = ClientBuilder.newClient().target(URL_API_INTERNO).path(PATH_API_INTERNO_ENVIO_MSJ_PERSONALIZADO).request().post(Entity.entity(peticionEnvioNotificacion, MediaType.APPLICATION_JSON));
        if (response.getStatusInfo().getFamily()!=Response.Status.Family.SUCCESSFUL) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "envio de notificacion fallido, estado de respuesta: {0}, contenido: {1}", new Object[]{response.getStatus(), response.readEntity(String.class)});
            return false;
        } else {
            return true;
        }
    }
    
    public String enviarNotificacion(PeticionEnvioNotificacion peticionEnvioNotificacion) {
        System.out.println("Cantidad miembros: " + peticionEnvioNotificacion.getMiembros().size());
        Response response = ClientBuilder.newClient().target(URL_API_INTERNO).path(PATH_API_INTERNO_ENVIO_MSJ).request().post(Entity.entity(peticionEnvioNotificacion, MediaType.APPLICATION_JSON));
        if (response.getStatusInfo().getFamily()!=Response.Status.Family.SUCCESSFUL) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "envio de notificacion fallido, estado de respuesta: {0}, contenido: {1}", new Object[]{response.getStatus(), response.readEntity(String.class)});
            return null;
        } else {
            return Json.createReader(new StringReader(response.readEntity(String.class))).readObject().getString("idTrabajo");
        }
    }
}
