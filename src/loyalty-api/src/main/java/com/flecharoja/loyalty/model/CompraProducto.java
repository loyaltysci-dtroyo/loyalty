package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "COMPRA_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompraProducto.findAll", query = "SELECT c FROM CompraProducto c"),
    @NamedQuery(name = "CompraProducto.findByIdTransaccion", query = "SELECT c FROM CompraProducto c WHERE c.compraProductoPK.idTransaccion = :idTransaccion"),
    @NamedQuery(name = "CompraProducto.findByIdProducto", query = "SELECT c FROM CompraProducto c WHERE c.compraProductoPK.idProducto = :idProducto"),
    @NamedQuery(name = "CompraProducto.findByCantidad", query = "SELECT c FROM CompraProducto c WHERE c.cantidad = :cantidad"),
    @NamedQuery(name = "CompraProducto.findByValorUnitario", query = "SELECT c FROM CompraProducto c WHERE c.valorUnitario = :valorUnitario"),
    @NamedQuery(name = "CompraProducto.findByTotal", query = "SELECT c FROM CompraProducto c WHERE c.total = :total")})
public class CompraProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CompraProductoPK compraProductoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANTIDAD")
    private Long cantidad;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "VALOR_UNITARIO")
    private Double valorUnitario;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL")
    private Double total;
    
    @JoinColumn(name = "ID_PRODUCTO", referencedColumnName = "ID_PRODUCTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    
    @Column(name = "ID_TRANSACCION", insertable = false, updatable = false)
    private String idTransaccion;

    public CompraProducto() {
    }

    public CompraProducto(CompraProductoPK compraProductoPK) {
        this.compraProductoPK = compraProductoPK;
    }

    public CompraProducto(CompraProductoPK compraProductoPK, Long cantidad, Double valorUnitario, Double total) {
        this.compraProductoPK = compraProductoPK;
        this.cantidad = cantidad;
        this.valorUnitario = valorUnitario;
        this.total = total;
    }

    public CompraProducto(String idTransaccion, String idProducto) {
        this.compraProductoPK = new CompraProductoPK(idTransaccion, idProducto);
    }

    public CompraProductoPK getCompraProductoPK() {
        return compraProductoPK;
    }

    public void setCompraProductoPK(CompraProductoPK compraProductoPK) {
        this.compraProductoPK = compraProductoPK;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public Double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (compraProductoPK != null ? compraProductoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompraProducto)) {
            return false;
        }
        CompraProducto other = (CompraProducto) object;
        if ((this.compraProductoPK == null && other.compraProductoPK != null) || (this.compraProductoPK != null && !this.compraProductoPK.equals(other.compraProductoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CompraProducto[ compraProductoPK=" + compraProductoPK + " ]";
    }
    
}
