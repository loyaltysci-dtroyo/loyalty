/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.model.Zona;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB con los metodos de negocio encargados de proveer los metodos necesarios
 * para zonas
 *
 * @author wtencio
 */
@Stateless
public class ZonaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos para el manejo de bitacora

    /**
     * Método que inserta una nueva zona en la base de datos
     *
     * @param nombreZona nombre que identifica la zona
     * @param idUbicacion ubicacion a la que pertenece la zona
     * @param usuario usuario en sesión
     * @param locale
     * @return identificador generado de la nueva zona
     */
    public String insertZona(String nombreZona, String idUbicacion, String usuario, Locale locale) {
        Ubicacion ubicacion = em.find(Ubicacion.class, idUbicacion);
        if (ubicacion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
        }

        Zona zona = new Zona();
        zona.setNombreZona(nombreZona);
        zona.setUsuarioCreacion(usuario);
        zona.setFechaCreacion(Calendar.getInstance().getTime());
        zona.setUsuarioModificacion(usuario);
        zona.setFechaModificacion(Calendar.getInstance().getTime());
        zona.setNumVersion(new Long(1));
        zona.setIdUbicacion(ubicacion);

        try {
            em.persist(zona);
            bitacoraBean.logAccion(Zona.class, zona.getIdZona(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        return zona.getIdZona();
    }

    /**
     * Método que obtiene la información de una zona en particular
     *
     * @param idZona identificador de la zona
     * @param locale
     * @return informacion de la zona
     */
    public Zona getZonaId(String idZona, Locale locale) {
        Zona zona = (Zona) em.createNamedQuery("Zona.findByIdZona").setParameter("idZona", idZona).getSingleResult();
        if (zona == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "zone_not_found");
        }
        return zona;

    }

    /**
     * Método que devuelve una lista de zonas
     *
     * @param idUbicacion identificador de la ubicacion
     * @param cantidad de registros deseados por pagina
     * @param registro registro por el que se desea empezar la busqueda
     * @param locale
     * @return lista de zonas
     */
    public Map<String, Object> getZonas(String idUbicacion, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        try {

            total = ((Long) em.createNamedQuery("Zona.countZonasByIdUbicacion").setParameter("idUbicacion", idUbicacion).getSingleResult());
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createNamedQuery("Zona.findZonasByIdUbicacion")
                    .setParameter("idUbicacion", idUbicacion)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Método que actualiza la información de una zona
     *
     * @param zona informacion de la zona
     * @param usuario usuario en sesion
     * @param locale
     */
    public void updateZona(Zona zona, String usuario, Locale locale) {
        Zona temp = em.find(Zona.class, zona.getIdZona());
        if (temp == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "zone_not_found");
        }

        zona.setFechaModificacion(Calendar.getInstance().getTime());
        zona.setUsuarioModificacion(usuario);

        try {
            em.merge(zona);
            em.flush();
            bitacoraBean.logAccion(Zona.class, zona.getIdZona(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
             if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

    }

    /**
     * Método que verifica la existencia de un nombre de zona
     *
     * @param nombre nombre de la region a verificar
     * @return resultado de la operacion
     */
    public Boolean existsNombreZona(String nombre) {
        return ((Long) em.createNamedQuery("Zona.countByNombre").setParameter("nombreZona", nombre).getSingleResult()) > 0;
    }

    /**
     * Método que elimina una zona de ser posible
     *
     * @param idZona identificador de la zona
     * @param usuario usuario en sesion
     * @param locale
     */
    public void deleteZona(String idZona, String usuario, Locale locale) {

        Zona zona = (Zona) em.createNamedQuery("Zona.findByIdZona").setParameter("idZona", idZona).getSingleResult();
        if (zona == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "zone_not_found");
        }
        em.remove(zona);
        bitacoraBean.logAccion(Zona.class, zona.getIdZona(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

    /**
     * Método que realiza una busqueda en base a palabras claves sobre una serie
     * de atributos de la tabla de zona
     *
     * @param busqueda cadena de texto con las palabras claves
     * @param cantidad de registros maximos por página
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return listado de todos las zonas encontradas
     */
    public Map<String, Object> searchZonas(String busqueda, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        String[] palabrasClaves = busqueda.split(" ");
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<Zona> root = query.from(Zona.class);

            List<Predicate> predicates = new ArrayList<>();

            for (String palabraClave : palabrasClaves) {
                predicates.add(cb.like(cb.lower(root.get("nombreZona")), "%" + palabraClave.toLowerCase() + "%"));

            }

            query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
            
            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createQuery(query.select(root))
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }
}
