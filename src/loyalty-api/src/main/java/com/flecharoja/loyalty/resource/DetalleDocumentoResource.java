/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.DocumentoDetalle;
import com.flecharoja.loyalty.service.DetalleDocumentoBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api("Documento Inventario")
@Path("documento")
public class DetalleDocumentoResource {

    @Context
    SecurityContext context;

    @EJB
    MyKeycloakAuthz authzBean;

    @EJB
    DetalleDocumentoBean detalleDocumentoBean;
    
    @Context
    HttpServletRequest request;

    /**
     * Metodo que acciona una tarea, la cual inserta un detalle de documento
     * dentro de la base de datos, en el caso de error al ejecutar la tarea se
     * retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param detalle objeto con la información a registrar
     * @param numDocumento
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Registrar un nuevo detalle de documento",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{numDocumento}/detalle")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void insertDetalleDocumento(
            @ApiParam(value = "Información del detalle del documento a registrar", required = true) DocumentoDetalle detalle,
            @ApiParam(value = "Identificador del documento")
            @PathParam("numDocumento") String numDocumento) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.DOCUMENTOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        detalleDocumentoBean.nuevoDetalle(detalle, numDocumento, usuarioContext, request.getLocale());
    }
    
     /**
     * Metodo que acciona una tarea, la cual inserta un detalle de documento
     * dentro de la base de datos, en el caso de error al ejecutar la tarea se
     * retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param detalle objeto con la información a registrar
     * @param numDocumento
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Registrar un nuevo detalle de documento",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("{numDocumento}/detalle")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void updateDetalleDocumento(
            @ApiParam(value = "Información del detalle del documento a actualizar", required = true) DocumentoDetalle detalle,
            @ApiParam(value = "Identificador del documento")
            @PathParam("numDocumento") String numDocumento) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.DOCUMENTOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        detalleDocumentoBean.updateDetalle(detalle, numDocumento, usuarioContext,request.getLocale());

    }
    /**
     * Metodo que acciona una tarea, la cual elimina un documento y toda la
     * información relacionada dentro de la base de datos, en el caso de error
     * al ejecutar la tarea se retorna un 500 (error interno del servidor) con
     * un mensaje en el encabezado, de no ser asi se retorna un 200 (OK) con un
     * resultado de ser requerido este.
     *
     * @param numDocumento identificador unico del documento a eliminar
     * @param idDetalle
     * @return Resultado de la operacion
     */
    @ApiOperation(value = "Eliminación de un detalle de documento")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{numDocumento}/detalle/{idDetalle}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeDetalleDocumento(
            @ApiParam(value = "Identificador del documento", required = true)
            @PathParam("numDocumento") String numDocumento,
            @ApiParam(value = "Identificador del detalle", required = true)
            @PathParam("idDetalle") String idDetalle) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.DOCUMENTOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        detalleDocumentoBean.removeDetalle(numDocumento, idDetalle, usuarioContext,request.getLocale());

    }

    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de todos los
     * detalles de documentos, en el caso de error al ejecutar la tarea se
     * retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param numDocumento
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Representacion del listado en formato JSON
     */
    @ApiOperation(value = "Obtencion de los detalles de documentos",
            responseContainer = "List",
            response = DocumentoDetalle.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{numDocumento}/detalle")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDetallesDocumento(
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro,
            @ApiParam(value = "Indicador de tipo de documento")
            @PathParam("numDocumento") String numDocumento) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.DOCUMENTOS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = detalleDocumentoBean.getDetallesDocumento(numDocumento, cantidad, registro,request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Metodo que acciona una tarea, la cual obtiene la información de un
     * documento relacionado al id que pasa por parametro en el caso de error al
     * ejecutar la tarea se retorna un 500 (error interno del servidor) con un
     * mensaje en el encabezado, de no ser asi se retorna un 200 (OK) con un
     * resultado de ser requerido este.
     *
     * @param numDocumento identificador único del documento
     * @param idDetalle
     * @return
     */
    @ApiOperation(value = "Obtener un detalle de un documento de inventario",
            response = DocumentoDetalle.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{numDocumento}/detalle/{idDetalle}")
    @Produces(MediaType.APPLICATION_JSON)
    public DocumentoDetalle getDetalleDocumento(
            @ApiParam(value = "Identificador del documento", required = true)
            @PathParam("numDocumento") String numDocumento,
            @ApiParam(value = "Identificador del detalle", required = true)
            @PathParam("idDetalle") String idDetalle) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.DOCUMENTOS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return detalleDocumentoBean.getDetalle(numDocumento,idDetalle,request.getLocale());
    }
}
