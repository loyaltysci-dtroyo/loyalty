package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaMision {
    
    public enum EstadosRespuesta {
        GANADO("G"),
        FALLIDO("F"),
        PENDIENTE("P");
        
        private final String value;
        private static final Map<String, EstadosRespuesta> lookup = new HashMap<>();

        private EstadosRespuesta(String value) {
            this.value = value;
        }
        
        static {
            for (EstadosRespuesta estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public String getValue() {
            return value;
        }
        
        public static EstadosRespuesta get (String value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    private String idMiembro;
    
    private Miembro miembro;
    
    private long fecha;
    
    private List<RespuestaMisionEncuestaPregunta> respuestaEncuesta;
    
    private List<RespuestaMisionPreferencia> respuestaPreferencias;
    
    private List<RespuestaMisionPerfilAtributo> respuestaActualizarPerfil;
    
    private RespuestaMisionSubirContenido respuestaSubirContenido;
    
    private RespuestaMisionRedSocial respuestaRedSocial;
    
    private RespuestaMisionVerContenido respuestaVerContenido;
    
    private RespuestaMisionJuego respuestaJuego;

    public RespuestaMision() {
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public long getFecha() {
        return fecha;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    public List<RespuestaMisionEncuestaPregunta> getRespuestaEncuesta() {
        return respuestaEncuesta;
    }

    public void setRespuestaEncuesta(List<RespuestaMisionEncuestaPregunta> respuestaEncuesta) {
        this.respuestaEncuesta = respuestaEncuesta;
    }

    public List<RespuestaMisionPreferencia> getRespuestaPreferencias() {
        return respuestaPreferencias;
    }

    public void setRespuestaPreferencias(List<RespuestaMisionPreferencia> respuestaPreferencias) {
        this.respuestaPreferencias = respuestaPreferencias;
    }

    public List<RespuestaMisionPerfilAtributo> getRespuestaActualizarPerfil() {
        return respuestaActualizarPerfil;
    }

    public void setRespuestaActualizarPerfil(List<RespuestaMisionPerfilAtributo> respuestaActualizarPerfil) {
        this.respuestaActualizarPerfil = respuestaActualizarPerfil;
    }

    public RespuestaMisionSubirContenido getRespuestaSubirContenido() {
        return respuestaSubirContenido;
    }

    public void setRespuestaSubirContenido(RespuestaMisionSubirContenido respuestaSubirContenido) {
        this.respuestaSubirContenido = respuestaSubirContenido;
    }

    public RespuestaMisionRedSocial getRespuestaRedSocial() {
        return respuestaRedSocial;
    }

    public void setRespuestaRedSocial(RespuestaMisionRedSocial respuestaRedSocial) {
        this.respuestaRedSocial = respuestaRedSocial;
    }

    public RespuestaMisionVerContenido getRespuestaVerContenido() {
        return respuestaVerContenido;
    }

    public void setRespuestaVerContenido(RespuestaMisionVerContenido respuestaVerContenido) {
        this.respuestaVerContenido = respuestaVerContenido;
    }

    public RespuestaMisionJuego getRespuestaJuego() {
        return respuestaJuego;
    }

    public void setRespuestaJuego(RespuestaMisionJuego respuestaJuego) {
        this.respuestaJuego = respuestaJuego;
    }
}
