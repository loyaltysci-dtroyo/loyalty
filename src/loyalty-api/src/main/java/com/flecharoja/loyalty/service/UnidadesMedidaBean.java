/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.UnidadesMedida;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class UnidadesMedidaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;

    /**
     * Método que inserta en la base de datos una nueva unidad de medida
     *
     * @param descripcion descripcion de la unidad
     * @param usuario usuario en sesión
     * @param locale
     * @return identificador de la unidad creada
     */
    public String insertUnidadMedida(String descripcion, String usuario, Locale locale) {
        Date fecha = Calendar.getInstance().getTime();
        if (existsDescripcion(descripcion)) {
             throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale,"units_measure_already_exist");
        }
        UnidadesMedida unidades = new UnidadesMedida(descripcion.toUpperCase(), fecha, usuario, fecha, usuario, 1L);
        try {
            em.persist(unidades);
            bitacoraBean.logAccion(UnidadesMedida.class, unidades.getIdUnidad(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        return unidades.getIdUnidad();
    }

    /**
     * Método que actualiza informacion de la unidad de medida
     *
     * @param unidad informacion de la unidad de medida
     * @param usuario usuario en sesion
     * @param locale
     */
    public void updateUnidadMedida(UnidadesMedida unidad, String usuario, Locale locale) {
        UnidadesMedida original = em.find(UnidadesMedida.class, unidad.getIdUnidad());
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "units_measure_not_found");
        }

        if (!unidad.getDescripcion().toUpperCase().equals(original.getDescripcion()) && existsDescripcion(unidad.getDescripcion())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale,"units_measure_already_exist");
        }

        unidad.setFechaModificacion(Calendar.getInstance().getTime());
        unidad.setUsuarioCreacion(usuario);
        unidad.setDescripcion(unidad.getDescripcion().toUpperCase());

        try {
            em.merge(unidad);
            bitacoraBean.logAccion(UnidadesMedida.class, unidad.getIdUnidad(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
    }

    /**
     * Método que elimina una unidad de medida asociada al id que ingresa por
     * parametro
     *
     * @param idUnidad identificador de la unidad
     * @param usuario usuario en sesión
     * @param locale
     */
    public void removeUnidadMedida(String idUnidad, String usuario, Locale locale) {
        UnidadesMedida unidades = em.find(UnidadesMedida.class, idUnidad);
        if (unidades == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "units_measure_not_found");
        }

        em.remove(unidades);
        em.flush();
        bitacoraBean.logAccion(UnidadesMedida.class, idUnidad, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

    /**
     * Método que obtiene la informacion de la unidad de medida relacionado al
     * id que ingresa por parametro
     *
     * @param idUnidad identificador de la unidad
     * @param locale
     * @return informacion de la unidad de medida
     */
    public UnidadesMedida getUnidad(String idUnidad, Locale locale) {
        UnidadesMedida unidad = em.find(UnidadesMedida.class, idUnidad);
        if (unidad == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "units_measure_not_found");
        }
        return unidad;
    }

    /**
     * Método que obtiene una lista de unidades de medida definidos por rangos
     *
     * @param cantidad de registros maximos por pagina
     * @param registro por el cual se comienza la busqueda
     * @param locale
     * @return lista de unidades
     */
    public Map<String, Object> getUnidades(int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
             throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        try {
            total = (Long) em.createNamedQuery("UnidadesMedida.countAll").getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            resultado = em.createNamedQuery("UnidadesMedida.findAll")
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
             throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Método que realiza una busqueda de unidades de acuerdo a su descripcion
     *
     * @param busqueda palabras claves
     * @param cantidad de registros maximos por pagina
     * @param registro por el cual se comienza la busqueda
     * @param locale
     * @return lista de unidades
     */
    public Map<String, Object> searchUnidades(String busqueda, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        String[] palabrasClaves = busqueda.split(" ");
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<UnidadesMedida> root = query.from(UnidadesMedida.class);

            List<Predicate> predicates = new ArrayList<>();

            for (String palabraClave : palabrasClaves) {
                predicates.add(cb.like(cb.lower(root.get("descripcion")), "%" + palabraClave.toLowerCase() + "%"));
            }

            query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createQuery(query.select(root))
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        } catch (QueryTimeoutException e) {
           throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    public boolean existsDescripcion(String descripcion) {
        return ((Long) em.createNamedQuery("UnidadesMedida.existDescripcion").setParameter("descripcion", descripcion.toUpperCase()).getSingleResult()) > 0;
    }

}
