/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.AtributoDinamicoProducto;
import com.flecharoja.loyalty.service.AtributoDinamicoProductoBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase de recursos http disponibles para el acceso de funcionalidades
 * del manejo de atributos dinamicos
 * @author wtencio
 */
@Api(value = "Atributo Dinamico de productos")
@Path("atributo-producto")
public class AtributoDinamicoProductoResource {
    
    @Context
    SecurityContext context;//permite obtener informacion del usuario en sesion
    
    @Context
    HttpServletRequest request;
    
    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos para el manejo de autorizaciones
    
    @EJB
    AtributoDinamicoProductoBean atributoDinamicoProductoBean;//EJB con los metodos para el manejo de atributos dinamicos
    
    /**
     * Metodo que acciona una tarea, la cual obtiene un atributo de producto asociado al id
     * que ingresa por parametro, de ocurrir un error se retornara una respuesta
     * con un diferente estado junto con un encabezado "Error-Reason" con un
     * valor numerico indicando la naturaleza del error.
     *
     * @param idAtributo identificador unico del atributo dinamico de productos
     * @return información del atributo
     */
    @ApiOperation(value = "Obtener un atributo dinámico de productos por su id",
            response = AtributoDinamicoProducto.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idAtributo}")
    @Produces(MediaType.APPLICATION_JSON)
    public AtributoDinamicoProducto getAtributoProductoPorId(
            @ApiParam(value = "Identificador del atributo dinámico de productos", required = true)
            @PathParam("idAtributo") String idAtributo) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.ATRIBUTOS_PRODUCTO_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return atributoDinamicoProductoBean.getAtributoProductoPorId(idAtributo, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de todos los
     * atributos segun el estado definidos dentro de un parametro con
     * seperado por "-",por un rango establecido usando los parametros de
     * cantidad y registro (por defecto todos) bajo un
     * tipo de dato de OK(200), de ocurrir un error se retornara una respuesta
     * con un diferente estado junto con un encabezado "Error-Reason" con un
     * valor numerico indicando la naturaleza del error.
     *
     * @param estados
     * @param tipo
     * @param requerido
     * @param busqueda
     * @param filtros
     * @param ordenCampo
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param ordenTipo
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Representacion del listado en formato JSON
     */
    @ApiOperation(value = "Obtener atributos dinámicos de productos",
            responseContainer = "List",
            response = AtributoDinamicoProducto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAtributoProductos(
            @ApiParam(value = "Indicadores de estado") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de tipo de datos") @QueryParam("tipo-dato") List<String> tipo,
            @ApiParam(value = "Indicador de requerido") @QueryParam("requerido") String requerido,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.ATRIBUTOS_PRODUCTO_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = atributoDinamicoProductoBean.getAtributos(estados, tipo, requerido, busqueda, filtros, ordenTipo, ordenCampo, cantidad, registro, request.getLocale());
        return Response.ok()
                    .entity(respuesta.get("resultado"))
                    .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                    .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                    .build();
    }

    /**
     * Metodo que acciona una tarea, la cual registra un nuevo atributo de producto dentro
     * de la base de datos con la información que recibe retornando el id del
     * atributo que se acaba de registrar con OK(200), de ocurrir un error se
     * retornara una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param atributo objeto con la información
     * @return id del atributo en formato JSON
     */
    @ApiOperation(value = "Registrar un atributo dinámico de productos",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertAtributoDinamicoProducto(
            @ApiParam(value = "Información del atributo dinámico de producto", required = true) AtributoDinamicoProducto atributo) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.ATRIBUTOS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return atributoDinamicoProductoBean.insertAtributoProducto(atributo, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual actualiza un atributo dinamico de producto
     * dentro de la base de datos con la información que recibe en OK(200), de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param atributo objeto con la información del atributo a actualizar
     * @return Resultado de la operación
     */
    @ApiOperation(value = "Modificacion de un atributo dinámico de productos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateAtributoDinamico(
            @ApiParam(value = "Información de un atributo dinámico de productos", required = true) 
            AtributoDinamicoProducto atributo) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.ATRIBUTOS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        atributoDinamicoProductoBean.updateAtributoProducto(atributo, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual borra un atributo dinamico de productos dentro
     * de la base de datos relacionada al id que ingresa como parametro en
     * OK(200), de ocurrir un error se retornara una respuesta con un diferente
     * estado junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param idAtributo identificador unico del atributo
     * @return Resultado de la operación
     */
    @ApiOperation(value = "ELiminación o desactivación (según sea el caso) de un atributo dinámico de productos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idAtributo}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeAtributoDinamicoProducto(
            @ApiParam(value = "Identificador de un atributo dinámico de productos", required = true)
            @PathParam("idAtributo") String idAtributo) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.ATRIBUTOS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        atributoDinamicoProductoBean.removeAtributoProducto(idAtributo, usuarioContext,request.getLocale());
    }

    
    
}
