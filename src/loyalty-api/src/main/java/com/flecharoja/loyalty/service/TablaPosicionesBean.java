/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Grupo;
import com.flecharoja.loyalty.model.TablaPosiciones;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * Clase encargada de proveer los metodos de negocio encargados de tabla de
 * posiciones
 *
 * @author wtencio
 */
@Stateless
public class TablaPosicionesBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    UtilBean utilBean;//EJB con los metodos de negocio para el manejo de utilidades

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Método que inserta una nueva tabla de posiciones en la base de datos
     *
     * @param tablaPosiciones información de la tabla de posiciones
     * @param usuarioContext id del usuario que esta en sesión encargado de
     * registrar la tabla
     * @param locale
     */
    public void insertTablaPosiciones(TablaPosiciones tablaPosiciones, String usuarioContext, Locale locale) {
        ConfiguracionGeneral configuracion = em.find(ConfiguracionGeneral.class, new Long(0));
        String idMetrica = configuracion.getIdMetricaInicial().getIdMetrica();
        boolean cantidad = false;
        //verifica que no exista una tabla con la misma metrica, el mismo tipo de tabla
//        List<TablaPosiciones> verifica = em.createNamedQuery("TablaPosiciones.findByTipoByMetrica")
//                .setParameter("idMetrica", tablaPosiciones.getIdMetrica().getIdMetrica())
//                .setParameter("indTipo", tablaPosiciones.getIndTipo())
//                .getResultList();
        if(tablaPosiciones.getIdMetrica().getIdMetrica().equals(idMetrica) && tablaPosiciones.getIndTipo().equals(TablaPosiciones.Tipo.MIEMBROS.getValue())){
            cantidad = (Long)em.createNamedQuery("TablaPosiciones.existeLeaderboard")
                .setParameter("idMetrica", idMetrica)
                .setParameter("indTipo", TablaPosiciones.Tipo.MIEMBROS.getValue()).getSingleResult() > 0;
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "{0}", cantidad);
        }
        
        //Logger.getLogger(this.getClass().getName()).log(Level.INFO, "{0}", verifica.size());
        //if (!verifica.isEmpty()) {
         if (cantidad) {
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "leaderboard_alerady_exists");
        }
        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (tablaPosiciones.getImagen() == null || tablaPosiciones.getImagen().trim().isEmpty()) {
            tablaPosiciones.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                tablaPosiciones.setImagen(filesUtils.uploadImage(tablaPosiciones.getImagen(), MyAwsS3Utils.Folder.ARTE_TABLA_POSICIONES, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }
        tablaPosiciones.setFechaCreacion(Calendar.getInstance().getTime());
        tablaPosiciones.setUsuarioCreacion(usuarioContext);
        tablaPosiciones.setFechaModificacion(Calendar.getInstance().getTime());
        tablaPosiciones.setUsuarioModificacion(usuarioContext);
        tablaPosiciones.setNumVersion(new Long(1));

        tablaPosiciones.setIndTipo(Character.toUpperCase(tablaPosiciones.getIndTipo()));
        if (tablaPosiciones.getIndTipo().equals(TablaPosiciones.Tipo.GRUPO_ESPECIFICO.getValue()) || tablaPosiciones.getIndTipo().equals(TablaPosiciones.Tipo.MIEMBROS.getValue())) {
            tablaPosiciones.setIndGrupalFormaCalculo(null);
        } else {
            tablaPosiciones.setIndGrupalFormaCalculo(Character.toUpperCase(tablaPosiciones.getIndGrupalFormaCalculo()));
        }

        tablaPosiciones.setIndFormato(tablaPosiciones.getIndFormato().toUpperCase());
        try {
            if (tablaPosiciones.getIndTipo().equals(TablaPosiciones.Tipo.GRUPO_ESPECIFICO.getValue())) {
                if (tablaPosiciones.getIdGrupo() == null) {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idGrupo");
                } else {
                    Grupo grupo = em.find(Grupo.class, tablaPosiciones.getIdGrupo().getIdGrupo());
                    if (grupo != null) {
                        tablaPosiciones.setIdGrupo(grupo);
                    } else {
                        throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "group_not_found");
                    }
                }
            } else {
                tablaPosiciones.setIdGrupo(null);
            }

            em.persist(tablaPosiciones);
            bitacoraBean.logAccion(TablaPosiciones.class, tablaPosiciones.getIdTabla(), usuarioContext, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (!tablaPosiciones.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(tablaPosiciones.getImagen());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que actualiza la información de una tabla de posiciones ya
     * existente
     *
     * @param tablaPosiciones objeto con la información actualizada de la tabla
     * de posiciones
     * @param usuarioContext id del usuario que esta en sesion
     * @param locale
     */
    public void updateTablaPosiciones(TablaPosiciones tablaPosiciones, String usuarioContext, Locale locale) {
        TablaPosiciones temp;
        try {
            temp = (TablaPosiciones) em.createNamedQuery("TablaPosiciones.findByIdTabla").setParameter("idTabla", tablaPosiciones.getIdTabla()).getSingleResult();

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idTabla");
        }
        if (temp == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "leaderboard_not_found");
        }

        String currentUrl = (String) em.createNamedQuery("TablaPosiciones.getImagenFromTabla").setParameter("idTabla", tablaPosiciones.getIdTabla()).getSingleResult();
        String newUrl = null;
        //si el atributo de imagen viene nula (no deberia)
        if (tablaPosiciones.getImagen() == null || tablaPosiciones.getImagen().trim().isEmpty()) {
            newUrl = Indicadores.URL_IMAGEN_PREDETERMINADA;
            tablaPosiciones.setImagen(newUrl);
        } else //ver si el valor en el atributo de imagen, difiere del almacenado
        if (!currentUrl.equals(tablaPosiciones.getImagen())) {
            //se le intenta subir
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                newUrl = filesUtils.uploadImage(tablaPosiciones.getImagen(), MyAwsS3Utils.Folder.ARTE_TABLA_POSICIONES, null);
                tablaPosiciones.setImagen(newUrl);
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }

        tablaPosiciones.setFechaModificacion(Calendar.getInstance().getTime());
        tablaPosiciones.setUsuarioModificacion(usuarioContext);

        tablaPosiciones.setIndTipo(Character.toUpperCase(tablaPosiciones.getIndTipo()));
        tablaPosiciones.setIndFormato(tablaPosiciones.getIndFormato().toUpperCase());
        tablaPosiciones.setIndGrupalFormaCalculo(Character.toUpperCase(tablaPosiciones.getIndGrupalFormaCalculo()));

        try {
            if (tablaPosiciones.getIndTipo().equals(TablaPosiciones.Tipo.GRUPO_ESPECIFICO.getValue())) {
                if (tablaPosiciones.getIdGrupo() == null) {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idGrupo");
                } else {
                    Grupo grupo = (Grupo) em.createNamedQuery("Grupo.findByIdGrupo").setParameter("idGrupo", tablaPosiciones.getIdGrupo()).getSingleResult();
                    if (grupo.getIdGrupo() != null) {
                        tablaPosiciones.setIndFormato(tablaPosiciones.getIndFormato().toUpperCase());
                    } else {
                        throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "group_not_found");
                    }
                }
            } else {
                tablaPosiciones.setIdGrupo(null);
            }

            em.merge(tablaPosiciones);
            em.flush();
            bitacoraBean.logAccion(TablaPosiciones.class, tablaPosiciones.getIdTabla(), usuarioContext, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
            if (newUrl != null && !currentUrl.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(currentUrl);
                } catch (Exception e){
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                }
            }
        } catch (ConstraintViolationException | OptimisticLockException e) {//en el caso que la informacion no sea valida
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            if (newUrl != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(newUrl);
                } catch (Exception ex){
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
        
        if (newUrl != null && !currentUrl.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(currentUrl);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Método que elimina el registro de la tabla de posiciones asociada al id
     * que ingresa como parametro
     *
     * @param idTabla identificador unico de la tabla de posiciones
     * @param usuario usuario en sesión
     * @param locale
     */
    public void removeTablaPosiciones(String idTabla, String usuario, Locale locale) {
        TablaPosiciones tabla = (TablaPosiciones) em.createNamedQuery("TablaPosiciones.findByIdTabla").setParameter("idTabla", idTabla).getSingleResult();
        if (tabla == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "leaderboard_not_found");
        }
        //se obtiene el url de la imagen almacenada de la entidad y se borra
        String url = (String) em.createNamedQuery("TablaPosiciones.getImagenFromTabla").setParameter("idTabla", idTabla).getSingleResult();

        em.remove(tabla);
        em.flush();
        bitacoraBean.logAccion(TablaPosiciones.class, idTabla, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        
        if (!url.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(url);
            } catch (Exception ex){
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Método que obtiene un listado de tablas de posiciones de acuerdo al tipo
     * de tabla(grupo o usuario)
     *
     * @param tipo indicador del tipo de tabla a buscar (grupo o usuario)
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return lista de tabla de posiciones
     */
    public Map<String, Object> getTablasPosiciones(String tipo, int registro, int cantidad, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        try {
            if (tipo.equalsIgnoreCase("null")) {

                total = ((Long) em.createNamedQuery("TablaPosiciones.countAll").getSingleResult());
                total -= total - 1 < 0 ? 0 : 1;

                while (registro > total) {
                    registro = registro - cantidad > 0 ? registro - cantidad : 0;
                }
                resultado = em.createNamedQuery("TablaPosiciones.findAll")
                        .setMaxResults(cantidad)
                        .setFirstResult(registro)
                        .getResultList();

            } else {
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery<Object> query = cb.createQuery();
                Root<TablaPosiciones> root = query.from(TablaPosiciones.class);

                List<Predicate> predicates = new ArrayList<>();
                for (String e : tipo.split("-")) {
                    predicates.add(cb.equal(root.get("indTipo"), e.toUpperCase().charAt(0)));

                }
                query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));

                total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
                total -= total - 1 < 0 ? 0 : 1;
                while (registro > total) {
                    registro = registro - cantidad > 0 ? registro - cantidad : 0;
                }

                resultado = em.createQuery(query.select(root))
                        .setMaxResults(cantidad)
                        .setFirstResult(registro)
                        .getResultList();

            }
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Método que obtiene la información de una tabla de posiciones especifica
     * asociada al id que ingresa como parametro
     *
     * @param idTabla identificador unico de la tabla a buscar
     * @param locale
     * @return información de la tabla de posición en formato JSON
     */
    public TablaPosiciones getTablaPorId(String idTabla, Locale locale) {

        TablaPosiciones tabla = (TablaPosiciones) em.createNamedQuery("TablaPosiciones.findByIdTabla").setParameter("idTabla", idTabla).getSingleResult();
        if(tabla == null){
           throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "leaderboard_not_found");
        }
        return tabla;
    }

    /**
     * Metodo que realiza una busqueda en base a palabras claves sobre una serie
     * de atributos establecidos en la tabla de posiciones
     *
     * @param busqueda Cadena de texto con las palabras claves
     * @param tipo Indicador de los estados de las tablas deseados
     * @param registro del cual se quiere empezar la búsqueda
     * @param cantidad de registros que se desean por páginas
     * @param locale
     * @return Listado de todos las tablas encontrados
     */
    public Map<String, Object> searchTablas(String busqueda, String tipo, int registro, int cantidad, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        String[] palabrasClaves = busqueda.split(" ");
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<TablaPosiciones> root = query.from(TablaPosiciones.class);

            List<Predicate> predicates = new ArrayList<>();

            for (String palabraClave : palabrasClaves) {
                predicates.add(cb.like(cb.lower(root.get("nombre")), "%" + palabraClave.toLowerCase() + "%"));
                predicates.add(cb.like(cb.lower(root.get("descripcion")), "%" + palabraClave.toLowerCase() + "%"));

            }

            switch (tipo) {
                case "null": {
                    query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
                    break;
                }
                default: {
                    List<Predicate> predicates2 = new ArrayList<>();
                    for (String e : tipo.split("-")) {
                        predicates2.add(cb.equal(root.get("indTipo"), e.toUpperCase().charAt(0)));
                    }
                    query.where(cb.or(predicates2.toArray(new Predicate[predicates2.size()])), cb.or(predicates.toArray(new Predicate[predicates.size()])));
                }
            }

            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            resultado = em.createQuery(query.select(root))
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }
}
