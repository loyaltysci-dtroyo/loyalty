package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.CategoriaNoticia;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * Clase encargada de proveer metodos para el mantenimiento de categorias de
 * noticia asi como verificaciones y manejo de datos
 *
 * @author svargas
 */
@Stateless
public class CategoriaNoticiaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    private BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene el listado de categorias de noticias almacenadas en
     * un rango definido por parametros
     *
     * @param busqueda
     * @param indTipoOrden
     * @param indCampoOrden
     * @param registro Numero de registro inicial
     * @param cantidad Cantidad de resultados en la lista
     * @param locale
     * @return Lista y encabezados acerca del rango
     */
    public Map<String, Object> getCategoriasNoticias(String busqueda, String indTipoOrden, String indCampoOrden, int registro, int cantidad, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<CategoriaNoticia> root = query.from(CategoriaNoticia.class);

        //formacion del query segun los parametros de entrada
        query = Busquedas.getCriteriaQueryCategoriaNoticia(cb, query, root, busqueda, indTipoOrden, indCampoOrden);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * Metodo que obtiene la informacion almacenada de una categoria de
     * noticia
     *
     * @param idCategoria Identidicador de la categoria deseada
     * @param locale
     * @return Informacion de la categoria encontrada
     */
    public CategoriaNoticia getCategoriaNoticia(String idCategoria, Locale locale) {
        CategoriaNoticia categoria = em.find(CategoriaNoticia.class, idCategoria);
        //verifica que la entidad no sea nula (no se encontro)
        if (categoria == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "news_category_not_found");
        }
        return categoria;
    }

    /**
     * Metodo que almacena la informacion de una nueva categoria de noticia
     *
     * @param categoria Objeto con la informacion de la categoria a almacenar
     * @param usuario Identificador del usuario creador
     * @param locale
     * @return Identificador de la categoria generado por el sistema
     */
    public String insertCategoriaNoticia(CategoriaNoticia categoria, String usuario, Locale locale) {
        if (categoria==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", CategoriaNoticia.class.getSimpleName());
        }
        //Se valida que el nombre de despliegue no exista
        if (existsNombreInterno(categoria.getNombre())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "deployment_name_already_exists");
        }
        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (categoria.getImagen() == null || categoria.getImagen().trim().isEmpty()) {
            categoria.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                categoria.setImagen(filesUtils.uploadImage(categoria.getImagen(), MyAwsS3Utils.Folder.ARTE_CATEGORIA_NOTICIA, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }

        //fecha de registro y modificacion
        Date date = Calendar.getInstance().getTime();
        
        //establecimiento de atributos por defecto
        categoria.setFechaCreacion(date);
        categoria.setFechaModificacion(date);
        categoria.setNumVersion(1l);

        categoria.setUsuarioCreacion(usuario);
        categoria.setUsuarioModificacion(usuario);

        try {
            //se registra la categoria y la acccion en la bitacora
            em.persist(categoria);
            em.flush();//se manda a guardar a la bases de datos para la verificacion en el momento de la entidad, etc
        } catch (ConstraintViolationException e) {//si la entidad existe o se violo alguna restriccion de atributo
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (!categoria.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(categoria.getImagen());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t)-> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        
        bitacoraBean.logAccion(CategoriaNoticia.class, categoria.getIdCategoria(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        
        return categoria.getIdCategoria();
    }

    /**
     * Metodo que modifica la informacion de un registro de categoria de
     * noticia existente
     *
     * @param categoria Objeto con la informacion de la categoria a editar
     * @param usuario Identificador del usuario modificador
     * @param locale
     */
    public void editCategoriaNoticia(CategoriaNoticia categoria, String usuario, Locale locale) {
        if (categoria==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "information_empty", CategoriaNoticia.class.getSimpleName());
        }
        //verificacion que la entidad exista
        CategoriaNoticia original;
        try {
            original = em.find(CategoriaNoticia.class, categoria.getIdCategoria());
            if (original==null) {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException e) {//si el identificador de la entidad no es valido
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_category_not_found");
        }
        
         if(!original.getNombre().equals(categoria.getNombre())){
            if (existsNombreInterno(categoria.getNombre())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "deployment_name_already_exists");
            }
        }

        String urlImagenOriginal = original.getImagen();//url imagen original
        String urlImagenNueva = null;//url imagen nueva
        //si el atributo de imagen viene nula (no deberia)
        if (categoria.getImagen() == null || categoria.getImagen().trim().isEmpty()) {
            //establecimiento de la imagen por defecto
            urlImagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            categoria.setImagen(urlImagenNueva);
        } else //ver si el valor en el atributo de imagen, difiere del almacenado
        {
            if (!urlImagenOriginal.equals(categoria.getImagen())) {//si la imagen es diferente a la almacenada
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    urlImagenNueva = filesUtils.uploadImage(categoria.getImagen(), MyAwsS3Utils.Folder.ARTE_CATEGORIA_NOTICIA, null);
                    categoria.setImagen(urlImagenNueva);
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }

        //establecimiento de atributos por defecto
        categoria.setUsuarioModificacion(usuario);
        categoria.setFechaModificacion(Calendar.getInstance().getTime());

        try {
            //se modifica la entidad y se registra la accion en bitacora
            em.merge(categoria);
            em.flush();//se manda a guardar a la bases de datos para la verificacion en el momento de la entidad, etc
        } catch (ConstraintViolationException | OptimisticLockException e) {//en el caso que la informacion no sea valida
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if(e instanceof ConstraintViolationException){
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "attributes_invalid", ((ConstraintViolationException )e).getConstraintViolations().stream().map((t)->t.getPropertyPath().toString()).collect(Collectors.joining(", ")) );
            }else{
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
           
        }
        
        bitacoraBean.logAccion(CategoriaNoticia.class, categoria.getIdCategoria(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        
        if (urlImagenNueva != null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Metodo que elimina un registro de una categoria de noticia existente
     * por su identificador
     *
     * @param idCategoria Identificador de la categoria a eliminar
     * @param usuario Identificador del usuario modificador
     * @param locale
     */
    public void removeCategoriaNoticia(String idCategoria, String usuario, Locale locale) {
        CategoriaNoticia categoria = em.find(CategoriaNoticia.class, idCategoria);//se busca la entidad
        if (categoria == null) {//si no se encontro...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_category_not_found");
        }

        //se obtiene el url de la imagen almacenada de la entidad y borra
        String url = categoria.getImagen();
        if (!url.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(url);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
        
        //se manda a borrar la entidad y se registra la accion en la bitacora
        em.remove(categoria);
        bitacoraBean.logAccion(CategoriaNoticia.class, idCategoria, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }
    
    /**
     * Metodo que verifica si existe algun registro de categoria de noticia
     * con un valor en la columna de nombre con el mismo valor del
     * parametro nombre
     *
     * @param nombre Nombre interno a verificar
     * @return Existencia del nombre interno
     */
    public Boolean existsNombreInterno(String nombre) {
        return ((Long) em.createNamedQuery("CategoriaNoticia.countByNombre").setParameter("nombre", nombre).getSingleResult()) > 0;
    }
}
