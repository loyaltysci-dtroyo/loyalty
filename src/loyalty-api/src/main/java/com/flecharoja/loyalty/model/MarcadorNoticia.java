package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MARCADOR_NOTICIA")
@NamedQueries({
    @NamedQuery(name = "MarcadorNoticia.countByIdNoticia", query = "SELECT COUNT(m) FROM MarcadorNoticia m WHERE m.marcadorNoticiaPK.idNoticia = :idNoticia")
})
public class MarcadorNoticia implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MarcadorNoticiaPK marcadorNoticiaPK;
    
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @JoinColumn(name = "ID_NOTICIA", referencedColumnName = "ID_NOTICIA", insertable = false, updatable = false)
    @ManyToOne
    private Noticia noticia;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne
    private Miembro miembro;

    public MarcadorNoticia() {
    }

    public MarcadorNoticiaPK getMarcadorNoticiaPK() {
        return marcadorNoticiaPK;
    }

    public void setMarcadorNoticiaPK(MarcadorNoticiaPK marcadorNoticiaPK) {
        this.marcadorNoticiaPK = marcadorNoticiaPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Noticia getNoticia() {
        return noticia;
    }

    public void setNoticia(Noticia noticia) {
        this.noticia = noticia;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (marcadorNoticiaPK != null ? marcadorNoticiaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarcadorNoticia)) {
            return false;
        }
        MarcadorNoticia other = (MarcadorNoticia) object;
        if ((this.marcadorNoticiaPK == null && other.marcadorNoticiaPK != null) || (this.marcadorNoticiaPK != null && !this.marcadorNoticiaPK.equals(other.marcadorNoticiaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MarcadorNoticia[ marcadorNoticiaPK=" + marcadorNoticiaPK + " ]";
    }
    
}
