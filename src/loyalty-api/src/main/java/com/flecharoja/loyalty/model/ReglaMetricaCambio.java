package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "REGLA_METRICA_CAMBIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReglaMetricaCambio.findByIdLista", query = "SELECT r FROM ReglaMetricaCambio r WHERE r.idLista = :idLista ORDER BY r.fechaCreacion DESC")
})
public class ReglaMetricaCambio implements Serializable {
    
    public enum ValoresIndCambio {
        ACUMULADO('A'),
        VENCIDO('B'),
        REDIMIDO('C'),
        DISPONIBLE('D');

        public final Character value;
        private static final Map<Character, ValoresIndCambio> lookup = new HashMap<>();

        private ValoresIndCambio(char value) {
            this.value = value;
        }

        static {
            for (ValoresIndCambio valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public static ValoresIndCambio get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum ValoresIndTipo {
        ULTIMO('B'),
        ANTERIOR('C'),
        DESDE('D'),
        ENTRE('E'),
        MES_ACTUAL('F'),
        ANO_ACTUAL('G'),
        HASTA('H');

        public final Character value;
        private static final Map<Character, ValoresIndTipo> lookup = new HashMap<>();

        private ValoresIndTipo(char value) {
            this.value = value;
        }

        static {
            for (ValoresIndTipo valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public static ValoresIndTipo get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    public enum ValoresIndValor {
        DIA('D'),
        MES('M'),
        ANO('A');

        public final Character value;
        private static final Map<Character, ValoresIndValor> lookup = new HashMap<>();

        private ValoresIndValor(char value) {
            this.value = value;
        }

        static {
            for (ValoresIndValor valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public static ValoresIndValor get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "reglametricacambio_uuid")
    @GenericGenerator(name = "reglametricacambio_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_REGLA")
    private String idRegla;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_CAMBIO")
    private Character indCambio;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Column(name = "IND_VALOR")
    private Character indValor;
    
    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    
    @Column(name = "FECHA_FINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFinal;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_LISTA")
    private String idLista;
    
    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    private Metrica idMetrica;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_OPERADOR")
    private Character indOperador;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "VALOR_COMPARACION")
    private String valorComparacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(max = 40)
    @Column(name = "NOMBRE")
    private String nombre;

    public ReglaMetricaCambio() {
    }

    public String getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(String idRegla) {
        this.idRegla = idRegla;
    }

    public Character getIndCambio() {
        return indCambio;
    }

    public void setIndCambio(Character indCambio) {
        this.indCambio = indCambio!=null?Character.toUpperCase(indCambio):null;
    }

    public Character getIndOperador() {
        return indOperador;
    }

    public void setIndOperador(Character indOperador) {
        this.indOperador = indOperador!=null?Character.toUpperCase(indOperador):null;
    }

    public String getValorComparacion() {
        return valorComparacion;
    }

    public void setValorComparacion(String valorComparacion) {
        this.valorComparacion = valorComparacion;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo!=null?Character.toUpperCase(indTipo):null;
    }

    public Character getIndValor() {
        return indValor;
    }

    public void setIndValor(Character indValor) {
        this.indValor = indValor!=null?Character.toUpperCase(indValor):null;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }
    
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getIdLista() {
        return idLista;
    }

    public void setIdLista(String idLista) {
        this.idLista = idLista;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegla != null ? idRegla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReglaMetricaCambio)) {
            return false;
        }
        ReglaMetricaCambio other = (ReglaMetricaCambio) object;
        if ((this.idRegla == null && other.idRegla != null) || (this.idRegla != null && !this.idRegla.equals(other.idRegla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ReglaMetricaCambio[ idRegla=" + idRegla + " ]";
    }
    
}
