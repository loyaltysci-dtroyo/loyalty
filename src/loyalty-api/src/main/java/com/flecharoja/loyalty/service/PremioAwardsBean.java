/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CodigoCertificado;
import com.flecharoja.loyalty.model.CodigoCertificadoPK;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioListaMiembro;
import com.flecharoja.loyalty.model.PremioMiembro;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.validation.ConstraintViolationException;

/**
 * EJÇB con los metodos de negocio para el manejo de awards de premios
 * @author wtencio
 */
@Stateless
public class PremioAwardsBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de bitacora


    /**
     * Método que asigna un premio gratis a un miembro
     * @param award informacion necesaria para registrar el award
     * @param usuario usuario en sesion
     * @param locale
     */
    public void assignAwardMiembro(PremioMiembro award, String usuario,Locale locale) {
        Premio premio;
        Miembro miembro;
         
        try {
            premio = em.find(Premio.class, award.getIdPremio().getIdPremio());
            if(premio == null){
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
            }
            miembro = em.find(Miembro.class, award.getIdMiembro().getIdMiembro());
            if(miembro == null){
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
            }
            if (premio.getIndEstado().equals(Premio.Estados.ARCHIVADO.getValue())) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Premio");
            }

            PremioMiembro awards = new PremioMiembro();
            Date date = Calendar.getInstance().getTime();
            if(premio.getIndTipoPremio().equals(Premio.Tipo.CERTIFICADO.getValue()) && "null".equals(award.getCodigoCertificado())){
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "codigoCertificado");
            }else{
                CodigoCertificado codigo = em.find(CodigoCertificado.class, new CodigoCertificadoPK(award.getCodigoCertificado(), award.getIdPremio().getIdPremio()));
                //verifico que el certificado de dicho premio exista
                if(codigo == null){
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "certificate_not_found");
                }
                //verifico que el estado del codigo sea disponible
                if(codigo.getIndEstado().equals(CodigoCertificado.Estados.DISPONIBLE.getValue())){
                    awards.setCodigoCertificado(award.getCodigoCertificado());
                }else{
                    throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "certificate_not_available");
                }
                
            }
            //asignacion de valores necesarios
            awards.setIdPremio(premio);
            awards.setIdMiembro(miembro);
            awards.setFechaAsignacion(date);
            awards.setUsuarioAsignador(usuario);
            awards.setEstado(PremioMiembro.Estados.REDIMIDO_SIN_RETIRAR.getValue());
            awards.setMotivoAsignacion(award.getMotivoAsignacion());
            awards.setIndMotivoAsignacion(PremioMiembro.Motivos.AWARD.getValue());

            em.persist(awards);

            bitacoraBean.logAccion(PremioMiembro.class, awards.getIdPremioMiembro(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);

        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        } 

    }

    /**
     * Método que revoca la asignacion de un award a un miembro
     *
     * @param idPremioMiembro
     * @param usuario usuario en sesion
     * @param locale
     */
    public void unassignAwardMiembro(String idPremioMiembro, String usuario, Locale locale) {

        try {
            em.remove(em.getReference(PremioMiembro.class, idPremioMiembro));
            em.flush();
            bitacoraBean.logAccion(PremioListaMiembro.class, idPremioMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_member_not_found");
        } 
    }

   

 
    /**
     * Método que obtiene un listado de awards o recompemsas gratis asignados a un miembro en un
     * rango de respuesta 
     *
     * @param idMiembro identificador del miembro
     * @param registro numero de registro inicial
     * @param cantidad cantidad de registros deseados
     * @param locale
     * @return Respuesta con el listado de miembros y su rango de respuesta
     */
    public Map<String, Object> getAwardMiembro(String idMiembro, int registro, int cantidad, Locale locale) {
        //verificacion de que el rango este dentro del valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }

        Map<String, Object> respuesta =  new HashMap<>();
        List resultado = new ArrayList();
        List registros = new ArrayList();

        int total;
        try {
            registros = em.createNamedQuery("PremioMiembro.findByIdMiembroAndEstadoAward")
                    .setParameter("idMiembro", idMiembro)
                    .getResultList();
            total = registros.size();
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createNamedQuery("PremioMiembro.findByIdMiembroAndEstadoAward")
                    .setParameter("idMiembro", idMiembro)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (QueryTimeoutException e) {
           throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO,locale, "database_connection_failed");
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        respuesta.put("rango",  registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

}
