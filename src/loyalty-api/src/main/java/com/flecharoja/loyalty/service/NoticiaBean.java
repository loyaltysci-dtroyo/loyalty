package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.Noticia;
import com.flecharoja.loyalty.model.NoticiaComentario;
import com.flecharoja.loyalty.model.RespuestaRegistroComentario;
import com.flecharoja.loyalty.model.Usuario;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author svargas
 */
@Stateless
public class NoticiaBean {
    
    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @EJB
    BitacoraBean bitacoraBean;

    /**
     * obtencion de lista de noticias paginadas filtradas por indicadores y busqueda
     * 
     * @param estados filtro por estado
     * @param busqueda texto de busqueda
     * @param filtros filtros de campos a aplicar la busqueda
     * @param cantidad paginacion
     * @param registro paginacion
     * @param ordenTipo tipo de orden de lista
     * @param ordenCampo campo por que ordenar la lista
     * @param locale locale
     * @return lista de noticias
     */
    public Map<String, Object> getNoticias(List<String> estados, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Noticia> root = query.from(Noticia.class);
        
        query = Busquedas.getCriteriaQueryNoticias(cb, query, root, estados, busqueda, filtros, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            resultado.stream().forEach((t) -> {
                //definicion de datos extras
                Noticia noticia = (Noticia) t;

                noticia.setCantMarcas((long) em.createNamedQuery("MarcadorNoticia.countByIdNoticia").setParameter("idNoticia", noticia.getIdNoticia()).getSingleResult());
                noticia.setCantComentarios(noticia.getComentarios().stream().filter((n) -> n.getComentarioReferente()==null).count());

                if (noticia.getIndAutorAdmin()!=null && noticia.getIndAutorAdmin()) {
                    Usuario admin = em.find(Usuario.class, noticia.getIdAutor());
                    noticia.setNombreAutor(admin.getNombrePublico());
                    noticia.setAvatarAutor(admin.getAvatar());
                } else {
                    try {
                        noticia.setNombreAutor(keycloakUtils.getUsernameMember(noticia.getIdAutor()));
                    } catch (Exception e2) {
                        noticia.setNombreAutor("Nulo");
                    }
                    noticia.setAvatarAutor((String) em.createNamedQuery("Miembro.getAvatarFromMiembro").setParameter("idMiembro", noticia.getIdAutor()).getSingleResult());
                }
            });
        }
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }
    
    /**
     * obtencion de detalle de una noticia por id
     * 
     * @param idNoticia identificador de noticia
     * @param locale locale
     * @return detalle de noticia
     */
    public Noticia getNoticia(String idNoticia, Locale locale) {
        Noticia noticia = em.find(Noticia.class, idNoticia);
        if (noticia==null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "noticia");
        }
        
        //definicion de datos extras
        noticia.setCantMarcas((long) em.createNamedQuery("MarcadorNoticia.countByIdNoticia").setParameter("idNoticia", idNoticia).getSingleResult());
        noticia.setCantComentarios(noticia.getComentarios().stream().filter((n) -> n.getComentarioReferente()==null).count());
        
        if (noticia.getIndAutorAdmin()!=null && noticia.getIndAutorAdmin()) {
            Usuario admin = em.find(Usuario.class, noticia.getIdAutor());
            noticia.setNombreAutor(admin.getNombrePublico());
            noticia.setAvatarAutor(admin.getAvatar());
        } else {
            try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
                try {
                    noticia.setNombreAutor(keycloakUtils.getUsernameMember(noticia.getIdAutor()));
                } catch (Exception e2) {
                    noticia.setNombreAutor("Nulo");
                }
            }
            noticia.setAvatarAutor((String) em.createNamedQuery("Miembro.getAvatarFromMiembro").setParameter("idMiembro", noticia.getIdAutor()).getSingleResult());
        }
        
        return noticia;
    }
    
    /**
     * obtencion de lista de comentarios paginados para una noticia/comentario
     * 
     * @param idNoticia identificador de la noticia padre
     * @param idComentario identificador opcional del comentario referente
     * @param untilDate parametro de paginacion por fecha
     * @param sinceDate parametro de paginacion por fecha
     * @param locale locale
     * @return lista de comentarios
     */
    public List<NoticiaComentario> getComentarios(String idNoticia, String idComentario, long untilDate, long sinceDate, Locale locale) {
        Noticia noticia = em.find(Noticia.class, idNoticia);
        if (noticia==null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "referenced_entity_not_found", "noticia");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<NoticiaComentario> query = cb.createQuery(NoticiaComentario.class);
        Root<NoticiaComentario> root = query.from(NoticiaComentario.class);
        
        if (idComentario==null) {
            query.where(cb.and(cb.equal(root.get("noticia"), noticia), cb.isNull(root.get("comentarioReferente"))));
        } else {
            NoticiaComentario comentarioReferente = em.find(NoticiaComentario.class, idComentario);
            if (comentarioReferente==null) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "referenced_entity_not_found", "comentario");
            }
            query.where(cb.and(cb.equal(root.get("noticia"), noticia), cb.equal(root.get("comentarioReferente"), comentarioReferente)));
        }
        
        //Se define el limite de registros elegibles segun la definicion de algunos de los atributos de fecha y el orden de estos
        if (sinceDate>0) {
            //registros nuevos de forma ascendente (pagina siguiente)
            query.where(query.getRestriction(), cb.greaterThan(root.<Date>get("fechaCreacion"), new Date(sinceDate))).orderBy(cb.asc(root.get("fechaCreacion")));
        } else {
            if (untilDate>0) {
                //registros anteriores
                query.where(query.getRestriction(), cb.lessThan(root.<Date>get("fechaCreacion"), new Date(untilDate)));
            }
            //si no se definio ninguna fecha, se trata de la pagina inicial (los registros a partir del mas reciente)
            //registros de forma descente para pagina anterior e inicial
            query.orderBy(cb.desc(root.get("fechaCreacion")));
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List<NoticiaComentario> resultado = em.createQuery(query.select(root))
                .setMaxResults(10)
                .getResultList();
        
        if (!resultado.isEmpty()) {
            NoticiaComentario ultimoComentario = resultado.get(resultado.size()-1);
            List<NoticiaComentario> resultList = em.createQuery(query.where(cb.and(cb.equal(root.get("fechaCreacion"), ultimoComentario.getFechaCreacion()), cb.not(root.in(resultado.stream().filter((t) -> t.getFechaCreacion().equals(ultimoComentario.getFechaCreacion())).collect(Collectors.toList())))))).getResultList();
            resultado.addAll(resultList);

            //se calcula la cantidad de comentarios por cada uno de estos, nombre del autor y avatar correspondiente
            try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
                resultado.stream().forEach((t) -> {
                    t.setCantComentarios(t.getComentarios().stream().count());

                    if (t.getIndAutorAdmin()!=null && t.getIndAutorAdmin()) {
                        Usuario admin = em.find(Usuario.class, t.getIdAutor());
                        t.setNombreAutor(admin.getNombrePublico());
                        t.setAvatarAutor(admin.getAvatar());
                    } else {
                        try {
                            noticia.setNombreAutor(keycloakUtils.getUsernameMember(noticia.getIdAutor()));
                        } catch (Exception e2) {
                            noticia.setNombreAutor("Nulo");
                        }
                        t.setAvatarAutor((String) em.createNamedQuery("Miembro.getAvatarFromMiembro").setParameter("idMiembro", t.getIdAutor()).getSingleResult());
                    }
                });
            }
            
            //invertir en caso de lista inicial de comentarios
            if (sinceDate==0 && untilDate==0) {
                resultado.sort((o1, o2) -> o1.getFechaCreacion().compareTo(o2.getFechaCreacion()));
            }
        }

        return resultado;
    }
    
    /**
     * registro de noticia
     * 
     * @param noticia data de noticia
     * @param idUsuario id admin
     * @param locale locale
     * @return id de noticia creado
     */
    public String insertNoticia(Noticia noticia, String idUsuario, Locale locale) {
        if (noticia==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Noticia");
        }
        //verificacion de algunos atributos no requeridos
        if (noticia.getIdNoticia() != null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idNoticia");
        }

        //establecimiento de atributos por defecto
        noticia.setIndEstado(Noticia.Estados.BORRADOR.getValue());
        Date date = new Date();
        noticia.setFechaCreacion(date);
        noticia.setFechaModificacion(date);
        noticia.setNumVersion(1l);

        noticia.setIndAutorAdmin(true);
        noticia.setIdAutor(idUsuario);

        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (noticia.getImagen() == null || noticia.getImagen().trim().isEmpty()) {
            noticia.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                noticia.setImagen(filesUtils.uploadImage(noticia.getImagen(), MyAwsS3Utils.Folder.ARTE_NOTICIA, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }

        try {
            em.persist(noticia);
            em.flush();
        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (!noticia.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(noticia.getImagen());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        bitacoraBean.logAccion(Noticia.class, noticia.getIdNoticia(), idUsuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);

        return noticia.getIdNoticia();
    }
    
    /**
     * edicion de registro de noticia
     * 
     * @param noticia data
     * @param idUsuario id admin
     * @param locale locale
     */
    public void editNoticia(Noticia noticia, String idUsuario, Locale locale) {
        //comprobacion y busqueda de datos de noticia
        if (noticia==null) {
           throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Noticia");
        }
        if (noticia.getIdNoticia()==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idNoticia");
        }
        
        //verificacion de que la entidad existe
        Noticia original = em.find(Noticia.class, noticia.getIdNoticia());
       
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "noticia");
        }
        
        if (original.getIndAutorAdmin()==null || !original.getIndAutorAdmin()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "comment_not_owned");
        }
        
        Noticia.Estados estado = Noticia.Estados.get(noticia.getIndEstado());
        if (estado==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "attributes_invalid", "indEstado");
        }
        Noticia.Estados estadoOriginal = Noticia.Estados.get(original.getIndEstado());
        //verificacion de que posible cambio de estado sea valido (Activo/Inactivo NO A Borrador) y de edicion de entidad archivada
        if ((estadoOriginal==Noticia.Estados.PUBLICADO || estadoOriginal==Noticia.Estados.INACTIVO) && estado==Noticia.Estados.BORRADOR) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "illegal_entity_state_change", "noticia");
        }
        if (estadoOriginal==Noticia.Estados.ARCHIVADO) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", "Noticia");
        }

        //establecimiento de valores de atributos por defecto
        Date date = new Date();
        noticia.setFechaModificacion(date);
        if (noticia.getIndEstado().equals(Noticia.Estados.PUBLICADO.getValue())) {
            noticia.setFechaPublicacion(date);
        }
        noticia.setIndAutorAdmin(true);
        noticia.setIdAutor(idUsuario);

        String urlImagenOriginal = original.getImagen();
        String urlImagenNueva = null;
        //si el atributo de imagen viene nula, se utiliza la predeterminada
        if (noticia.getImagen() == null || noticia.getImagen().trim().isEmpty()) {
            urlImagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            noticia.setImagen(urlImagenNueva);
        } else {
            //ver si el valor en el atributo de imagen, difiere del almacenado
            if (!urlImagenOriginal.equals(noticia.getImagen())) {
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    urlImagenNueva = filesUtils.uploadImage(noticia.getImagen(), MyAwsS3Utils.Folder.ARTE_NOTICIA, null);
                    noticia.setImagen(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }

        try {
            em.merge(noticia);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//en caso de que alguna informacion no este valida o este "vieja"
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
           if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(Noticia.class, noticia.getIdNoticia(), idUsuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        if (urlImagenNueva != null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }
    
    /**
     * eliminacion o archivado de noticia
     * 
     * @param idNoticia identificador de noticia
     * @param idUsuario id admin
     * @param locale locale
     */
    public void deleteNoticia(String idNoticia, String idUsuario, Locale locale) {
        //verificacion que la entidad exista
        Noticia noticia = em.find(Noticia.class, idNoticia);
        if (noticia == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "noticia");
        }
        
        String url = noticia.getImagen();

        if (noticia.getIndEstado().equals(Noticia.Estados.BORRADOR.getValue())) {
            em.remove(noticia);
            em.flush();
            if (!url.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(url);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            bitacoraBean.logAccion(Noticia.class, idNoticia, idUsuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        } else if (noticia.getIndEstado().equals(Noticia.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Noticia");
        } else {
            noticia.setIndEstado(Noticia.Estados.ARCHIVADO.getValue());

            em.merge(noticia);
            bitacoraBean.logAccion(Noticia.class, idNoticia, idUsuario, Bitacora.Accion.ENTIDAD_ARCHIVADA.getValue(),locale);
        }
    }
    
    /**
     * registro de comentario a noticia/comentario
     * 
     * @param idNoticia id de noticia
     * @param comentario data de comentario
     * @param idUsuario id admin
     * @param locale locale
     * @return id de comentario creado
     */
    public RespuestaRegistroComentario insertNoticiaComentario(String idNoticia, NoticiaComentario comentario, String idUsuario, Locale locale) {
        if (comentario==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "NoticiaComentario");
        }
        //verificacion de algunos atributos no requeridos
        if (comentario.getIdComentario()!= null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idComentario");
        }
        
        Noticia noticia = em.find(Noticia.class, idNoticia);
        if (noticia==null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "noticia");
        }
        //verificacion de estado de noticia
        if (Noticia.Estados.get(noticia.getIndEstado())!=Noticia.Estados.PUBLICADO) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "news_must_be_published");
        }
        
        //verificacion de comentario referente
        if (comentario.getComentarioReferente()!=null) {
            if (comentario.getComentarioReferente().getIdComentario()==null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "attributes_invalid", "comentarioReferente.idComentario");
            }
            NoticiaComentario comentarioReferente = em.find(NoticiaComentario.class, comentario.getComentarioReferente().getIdComentario());
            if (comentarioReferente==null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "comentarioReferente");
            }
            comentario.setComentarioReferente(comentarioReferente);
        }

        //establecimiento de atributos por defecto
        comentario.setNoticia(noticia);
        Date date = new Date();
        comentario.setFechaCreacion(date);
        comentario.setFechaModificacion(date);
        comentario.setNumVersion(1l);

        comentario.setIndAutorAdmin(true);
        comentario.setIdAutor(idUsuario);

        try {
            em.persist(comentario);
            em.flush();
        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        bitacoraBean.logAccion(NoticiaComentario.class, comentario.getIdComentario(), idUsuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        
        if (comentario.getComentarioReferente()!=null) {
            return new RespuestaRegistroComentario(comentario.getIdComentario(), comentario.getComentarioReferente().getComentarios().stream().count());
        } else {
            em.refresh(noticia);
            return new RespuestaRegistroComentario(comentario.getIdComentario(), noticia.getComentarios().stream().count());
        }
    }
    
    /**
     * edicion de comentario en noticia
     * 
     * @param idNoticia identificador de noticia
     * @param comentario data
     * @param idUsuario id admin
     * @param locale locale
     */
    public void editNoticiaComentario(String idNoticia, NoticiaComentario comentario, String idUsuario, Locale locale) {
        if (comentario==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "NoticiaComentario");
        }
        //verificacion de algunos atributos no requeridos
        if (comentario.getIdComentario()== null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idComentario");
        }
        
        NoticiaComentario original = em.find(NoticiaComentario.class, comentario.getIdComentario());
        if (original==null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "noticiaComentario");
        }
        
        //verificacion de autor de comentario
        if (original.getIndAutorAdmin()==null || !original.getIndAutorAdmin()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "comment_not_owned");
        }
        if (!original.getIdAutor().equals(idUsuario)) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "comment_not_owned");
        }
        
        Noticia noticia = em.find(Noticia.class, idNoticia);
        if (noticia==null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "noticia");
        }
        
        //verificacion de comentario referente
        if (comentario.getComentarioReferente()!=null) {
            if (comentario.getComentarioReferente().getIdComentario()==null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "attributes_invalid", "comentarioReferente.idComentario");
            }
            NoticiaComentario comentarioReferente = em.find(NoticiaComentario.class, comentario.getComentarioReferente().getIdComentario());
            if (comentarioReferente==null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "comentarioReferente");
            }
            comentario.setComentarioReferente(comentarioReferente);
        }

        //establecimiento de atributos por defecto
        comentario.setNoticia(noticia);
        comentario.setFechaModificacion(new Date());

        try {
            em.merge(comentario);
            em.flush();
        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        bitacoraBean.logAccion(NoticiaComentario.class, comentario.getIdComentario(), idUsuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
    }
    
    /**
     * eliminacion de comentario en noticia
     * 
     * @param idNoticia id de noticia
     * @param idComentario id de comentario
     * @param idUsuario id admin
     * @param locale locale
     */
    public void deleteNoticiaComentario(String idNoticia, String idComentario, String idUsuario, Locale locale) {
        NoticiaComentario comentario = em.find(NoticiaComentario.class, idComentario);
        if (comentario == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "noticiaComentario");
        }
        if (!comentario.getNoticia().getIdNoticia().equals(idNoticia)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idNoticia");
        }
        em.remove(comentario);
        em.flush();
        bitacoraBean.logAccion(NoticiaComentario.class, idComentario, idUsuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }
}