package com.flecharoja.loyalty.model;

import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class DataImportacionMiembroExtendido {
    
    private String data;
    
    private Map<Integer, String> atributos;
    
    private Map<Integer, String> atributosDinamicos;

    public DataImportacionMiembroExtendido() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Map<Integer, String> getAtributos() {
        return atributos;
    }

    public void setAtributos(Map<Integer, String> atributos) {
        this.atributos = atributos;
    }

    public Map<Integer, String> getAtributosDinamicos() {
        return atributosDinamicos;
    }

    public void setAtributosDinamicos(Map<Integer, String> atributosDinamicos) {
        this.atributosDinamicos = atributosDinamicos;
    }
}
