package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionSubirContenido;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Date;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado del manejo de datos para el mantenimiento de misiones de tipo
 * subir contenido
 *
 * @author svargas
 */
@Stateless
public class MisionSubirContenidoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene la informacion de subir contenido de una mision
     *
     * @param idMision Identificador de mision
     * @param locale
     * @return Informacion de la definicion de subir contenido
     */
    public MisionSubirContenido getSubirContenidoMision(String idMision,Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.SUBIR_CONTENIDO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        MisionSubirContenido subirContenido = em.find(MisionSubirContenido.class, idMision);
        if (subirContenido == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_upload_content_not_found");
        }

        return subirContenido;
    }

    /**
     * Metodo que registra/edita la definicion de subir contenido de una mision
     *
     * @param idMision Identificador de la mision
     * @param subirContenido Informacion de subir contenido
     * @param idUsuario Identificador del usuario creador/modificador
     * @param locale
     */
    public void createEditSubirContenidoMision(String idMision, MisionSubirContenido subirContenido, String idUsuario,Locale locale) {
        if (subirContenido==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "MisionSubirContenido");
        }
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.SUBIR_CONTENIDO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        MisionSubirContenido original = em.find(MisionSubirContenido.class, idMision);

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }

        Date fecha = new Date();
        if (original == null) {
            subirContenido.setFechaCreacion(fecha);
            subirContenido.setUsuarioCreacion(idUsuario);
            subirContenido.setNumVersion(new Long(1));
        }
        subirContenido.setIdMision(idMision);
        subirContenido.setFechaModificacion(fecha);
        subirContenido.setUsuarioModificacion(idUsuario);
        
        MisionSubirContenido.Tipos tipo = MisionSubirContenido.Tipos.get(subirContenido.getIndTipo());
        if (tipo==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indTipo");
        }

        try {
            em.merge(subirContenido);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//en caso de que la entidad no sea valida
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(MisionSubirContenido.class, idMision, idUsuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * Metodo que elimina la informacion de subir contenido de una mision
     *
     * @param idMision Identificador de la mision
     * @param idUsuario Identificador del usuario modificador
     * @param locale
     */
    public void removeSubirContenidoMision(String idMision, String idUsuario,Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.SUBIR_CONTENIDO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
             throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }

        try {
            em.remove(em.getReference(MisionSubirContenido.class, idMision));
            em.flush();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_upload_content_not_found");
        }

        bitacoraBean.logAccion(MisionSubirContenido.class, idMision, idUsuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }
}
