/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO_LISTA_SEGMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PremioListaSegmento.countByIdPremioAndSegmentosInListaAndByIndTipo", query = "SELECT COUNT(s) FROM PremioListaSegmento s WHERE s.premioListaSegmentoPK.idPremio = :idPremio AND s.premioListaSegmentoPK.idSegmento IN :lista AND s.indTipo = :indTipo"),
    @NamedQuery(name = "PremioListaSegmento.countByIdPremioAndByIndTipo", query = "SELECT COUNT(s) FROM PremioListaSegmento s WHERE s.premioListaSegmentoPK.idPremio = :idPremio AND s.indTipo = :indTipo"),
    @NamedQuery(name = "PremioListaSegmento.findAll", query = "SELECT p FROM PremioListaSegmento p"),
    @NamedQuery(name = "PremioListaSegmento.findByIdSegmento", query = "SELECT p FROM PremioListaSegmento p WHERE p.premioListaSegmentoPK.idSegmento = :idSegmento"),
    @NamedQuery(name = "PremioListaSegmento.findByIdPremio", query = "SELECT p FROM PremioListaSegmento p WHERE p.premioListaSegmentoPK.idPremio = :idPremio"),
    @NamedQuery(name = "PremioListaSegmento.findByIndTipo", query = "SELECT p FROM PremioListaSegmento p WHERE p.indTipo = :indTipo"),
    @NamedQuery(name = "PremioListaSegmento.countByIdPremio", query = "SELECT COUNT(p) FROM PremioListaSegmento p WHERE p.premioListaSegmentoPK.idPremio = :idPremio"),
    @NamedQuery(name = "PremioListaSegmento.findSegmentosByIdPremio", query = "SELECT p.segmento FROM PremioListaSegmento p WHERE p.premioListaSegmentoPK.idPremio = :idPremio"),
    @NamedQuery(name = "PremioListaSegmento.countSegmentosNotInPremioListaSegmento", query = "SELECT COUNT(s) FROM Segmento s WHERE s.indEstado = :estado AND s.idSegmento NOT IN (SELECT p.premioListaSegmentoPK.idSegmento FROM PremioListaSegmento p WHERE p.premioListaSegmentoPK.idPremio = :idPremio)"),
    @NamedQuery(name = "PremioListaSegmento.findSegmentosNotInPremioListaSegmento", query = "SELECT s FROM Segmento s WHERE s.indEstado = :estado AND s.idSegmento NOT IN (SELECT p.premioListaSegmentoPK.idSegmento FROM PremioListaSegmento p WHERE p.premioListaSegmentoPK.idPremio = :idPremio)"),
    @NamedQuery(name = "PremioListaSegmento.countByIdPremioIndTipo", query = "SELECT COUNT(p) FROM PremioListaSegmento p WHERE p.premioListaSegmentoPK.idPremio = :idPremio AND p.indTipo = :indTipo"),
    @NamedQuery(name = "PremioListaSegmento.findSegmentosByIdPremioIndTipo", query = "SELECT p.segmento FROM PremioListaSegmento p WHERE p.premioListaSegmentoPK.idPremio = :idPremio AND p.indTipo = :indTipo"),
    @NamedQuery(name = "PremioListaSegmento.getIdSegmentoByIdPremioIndTipo", query = "SELECT p.premioListaSegmentoPK.idSegmento FROM PremioListaSegmento p WHERE p.premioListaSegmentoPK.idPremio = :idPremio AND p.indTipo = :indTipo")
})
public class PremioListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PremioListaSegmentoPK premioListaSegmentoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_PREMIO", referencedColumnName = "ID_PREMIO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Premio premio;
    
    @JoinColumn(name = "ID_SEGMENTO", referencedColumnName = "ID_SEGMENTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Segmento segmento;

    public PremioListaSegmento() {
    }

    public PremioListaSegmento(PremioListaSegmentoPK premioListaSegmentoPK) {
        this.premioListaSegmentoPK = premioListaSegmentoPK;
    }

    public PremioListaSegmento(String idSegmento, String idPremio, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.premioListaSegmentoPK = new PremioListaSegmentoPK(idSegmento, idPremio);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public PremioListaSegmento(String idSegmento, String idPremio) {
        this.premioListaSegmentoPK = new PremioListaSegmentoPK(idSegmento, idPremio);
    }

     @ApiModelProperty(hidden = true)
    public PremioListaSegmentoPK getPremioListaSegmentoPK() {
        return premioListaSegmentoPK;
    }

    public void setPremioListaSegmentoPK(PremioListaSegmentoPK premioListaSegmentoPK) {
        this.premioListaSegmentoPK = premioListaSegmentoPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Premio getPremio() {
        return premio;
    }

    public void setPremio(Premio premio) {
        this.premio = premio;
    }

    public Segmento getSegmento() {
        return segmento;
    }

    public void setSegmento(Segmento segmento) {
        this.segmento = segmento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (premioListaSegmentoPK != null ? premioListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PremioListaSegmento)) {
            return false;
        }
        PremioListaSegmento other = (PremioListaSegmento) object;
        if ((this.premioListaSegmentoPK == null && other.premioListaSegmentoPK != null) || (this.premioListaSegmentoPK != null && !this.premioListaSegmentoPK.equals(other.premioListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.PremioListaSegmento[ premioListaSegmentoPK=" + premioListaSegmentoPK + " ]";
    }
    
}
