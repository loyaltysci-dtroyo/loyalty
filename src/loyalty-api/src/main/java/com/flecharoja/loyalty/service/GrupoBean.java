/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Grupo;
import com.flecharoja.loyalty.model.GrupoMiembro;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 *
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia
 * relacionados a grupos
 *
 * @author wtencio
 */
@Stateless
public class GrupoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    UtilBean utilBean;//EJB con los metodos de negocio para el manejo de utilidades

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Método que obtiene la información de un grupo asociado al id que ingresa
     * como parametro
     *
     * @param idGrupo identificador único del grupo
     * @param locale
     * @return objeto con la información del grupo
     */
    public Grupo getGrupoPorId(String idGrupo, Locale locale) {
        Grupo grupo = em.find(Grupo.class, idGrupo);
        if (grupo == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "group_not_found");
        }

        return grupo;
    }

    /**
     * Método que registra un nuevo grupo en la base de datos
     *
     * @param grupo objeto con la información a registrar
     * @param usuarioContext id del usuario que se encuentra en sesion
     * @return id del grupo que se acaba de registrar
     */
    public String insertGrupo(Grupo grupo, String usuarioContext, Locale locale) {
        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (grupo.getAvatar() == null || grupo.getAvatar().trim().isEmpty()) {
            grupo.setAvatar(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                grupo.setAvatar(filesUtils.uploadImage(grupo.getAvatar(), MyAwsS3Utils.Folder.ARTE_GRUPO_MIEMBRO, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }
        //asignación de valores necesarios
        grupo.setFechaCreacion(Calendar.getInstance().getTime());
        grupo.setFechaModificacion(Calendar.getInstance().getTime());

        grupo.setUsuarioCreacion(usuarioContext);
        grupo.setUsuarioModificacion(usuarioContext);

        grupo.setNumVersion(new Long(1));

        try {
            em.persist(grupo);
            em.flush();
            bitacoraBean.logAccion(Grupo.class, grupo.getIdGrupo(), usuarioContext, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (!grupo.getAvatar().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(grupo.getAvatar());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));

        }
        return grupo.getIdGrupo();
    }

    /**
     * Método que actualiza la información de un grupo ya existente
     *
     * @param grupo información actualizada del grupo
     * @param usuarioContext id del usuario en sesion
     * @param locale
     */
    public void editGrupo(Grupo grupo, String usuarioContext, Locale locale) {
        Grupo temp = em.find(Grupo.class, grupo.getIdGrupo());

        if (temp == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "group_not_found");
        }

        String currentUrl = (String) em.createNamedQuery("Grupo.getAvatarFromGrupo").setParameter("idGrupo", grupo.getIdGrupo()).getSingleResult();
        String newUrl = null;
        //si el atributo de imagen viene nula (no deberia)
        if (grupo.getAvatar() == null || grupo.getAvatar().trim().isEmpty()) {
            newUrl = Indicadores.URL_IMAGEN_PREDETERMINADA;
            grupo.setAvatar(newUrl);

        } else { //ver si el valor en el atributo de imagen, difiere del almacenado
            if (!currentUrl.equals(grupo.getAvatar())) {
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    newUrl = filesUtils.uploadImage(grupo.getAvatar(), MyAwsS3Utils.Folder.ARTE_GRUPO_MIEMBRO, null);
                    grupo.setAvatar(newUrl);
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }

        grupo.setFechaModificacion(Calendar.getInstance().getTime());
        grupo.setUsuarioModificacion(usuarioContext);

        try {
            //verificar que la imagen no cambio y de ser asi subirla la nueva y eliminar la que ya estaba
            em.merge(grupo);
            em.flush();
            bitacoraBean.logAccion(Grupo.class, grupo.getIdGrupo(), usuarioContext, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {//en el caso que la informacion no sea valida
            if (newUrl != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(newUrl);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        if (newUrl != null && !currentUrl.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(currentUrl);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Método que elimina un registro existente relacionado al id que ingresa
     * como parametro
     *
     * @param idGrupo identificador unico del grupo a eliminar
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeGrupo(String idGrupo, String usuario,Locale locale) {

        Grupo grupo = em.find(Grupo.class, idGrupo);
        if(grupo == null){
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "group_not_found");
        }
        List<GrupoMiembro> grupoMiembro = em.createNamedQuery("GrupoMiembro.findByIdGrupo").setParameter("idGrupo", grupo.getIdGrupo()).getResultList();
        if (!grupoMiembro.isEmpty()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "cannot_remove_group");
        }
        //se obtiene el url de la imagen almacenada de la entidad y borra
        String url = grupo.getAvatar();
        if (!url.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(url);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
        em.remove(grupo);
        bitacoraBean.logAccion(Grupo.class, idGrupo, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

   
    /**
     * Metodo que obtiene una lista de grupos definidos por el indicador de
     * estado del mismo
     *
     * @param indVisibilidad
     * @param busqueda
     * @param filtros
     * @param cantidad de registros que se quieren por pagina
     * @param ordenCampo
     * @param ordenTipo
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return Lista de grupos
     */
    public Map<String, Object> getGrupos(List<String> indVisibilidad, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo ,int cantidad, int registro,Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Grupo> root = query.from(Grupo.class);

        query = Busquedas.getCriteriaQueryGrupos(cb, query, root, indVisibilidad, busqueda, filtros, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();

        //retorno del resultado y encabezados sobre el rango
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

}
