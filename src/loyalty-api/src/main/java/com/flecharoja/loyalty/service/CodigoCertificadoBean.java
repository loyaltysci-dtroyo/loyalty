/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.CodigoCertificado;
import com.flecharoja.loyalty.model.CodigoCertificadoPK;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioMiembro;
import com.flecharoja.loyalty.util.Indicadores;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * codigos de certificados
 *
 * @author wtencio
 */
@Stateless
public class CodigoCertificadoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos para el manejo de bitacora

    /**
     * Método que inserta un nuevo registro de un nuevo certificado
     *
     * @param idPremio identificador del premio
     * @param idCodigo identificador del codigo
     * @param usuario usuario en sesión
     * @param locale
     */
    public void insertCodigo(String idPremio, String idCodigo, String usuario, Locale locale) {
        CodigoCertificado codigo = new CodigoCertificado(idCodigo, idPremio);
        Premio premio;
        premio = em.find(Premio.class, idPremio);

        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }

        CodigoCertificado verificar = em.find(CodigoCertificado.class, new CodigoCertificadoPK(idCodigo, idPremio));
        if (verificar != null) {
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "certificate_already_exists");
        }

        if (!premio.getIndTipoPremio().equals(Premio.Tipo.CERTIFICADO.getValue())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "reward_not_certified");
        }

        if (existsCodigo(idCodigo)) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "certificate_already_exists");
        }

        codigo.setFechaCreacion(Calendar.getInstance().getTime());
        codigo.setUsuarioCreacion(usuario);
        codigo.setPremio(premio);
        codigo.setIndEstado(CodigoCertificado.Estados.DISPONIBLE.getValue());

        try {
            premio.setTotalExistencias(premio.getTotalExistencias() + 1);
            //persito el codigo
            em.persist(codigo);
            em.merge(premio);
            bitacoraBean.logAccion(CodigoCertificado.class, codigo.getCodigoCertificadoPK().getCodigo() + "/" + codigo.getCodigoCertificadoPK().getIdPremio(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

    }

    /**
     * Método que elimina un codigo de un certificado
     *
     * @param idPremio identificador del premio
     * @param idCodigo identificador del codigo
     * @param usuario usuario en sesión
     * @param locale
     */
    public void deleteCodigo(String idPremio, String idCodigo, String usuario, Locale locale) {
        CodigoCertificado codigo;
        codigo = em.find(CodigoCertificado.class, new CodigoCertificadoPK(idCodigo, idPremio));
        if (codigo == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "certificate_not_found");
        }
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }
        if (codigo.getIndEstado().equals(CodigoCertificado.Estados.DISPONIBLE.getValue())) {
            premio.setTotalExistencias(premio.getTotalExistencias() - 1);
            em.merge(premio);

            em.remove(codigo);
            bitacoraBean.logAccion(CodigoCertificado.class, codigo.getCodigoCertificadoPK().getCodigo() + "/" + codigo.getCodigoCertificadoPK().getIdPremio(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        } else if (codigo.getIndEstado().equals(CodigoCertificado.Estados.OCUPADO.getValue())) {
            codigo.setIndEstado(CodigoCertificado.Estados.ANULADO.getValue());
            em.merge(codigo);
            bitacoraBean.logAccion(CodigoCertificado.class, codigo.getCodigoCertificadoPK().getCodigo() + "/" + codigo.getCodigoCertificadoPK().getIdPremio(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        }

    }

    /**
     * anulacion de certificado otorgado a un usuario
     * 
     * @param idPremio identificador del premio
     * @param idCodigo identificador del certificado
     * @param usuario id admin
     * @param locale  locale
     */
    public void anularCodigo(String idPremio, String idCodigo, String usuario, Locale locale) {
        CodigoCertificado codigo;

        //busqueda del certificado en el premio
        codigo = em.find(CodigoCertificado.class, new CodigoCertificadoPK(idCodigo, idPremio));

        if (codigo == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "certificate_not_found");
        }
        //verificacion del estado del certificado
        if (codigo.getIndEstado().equals(CodigoCertificado.Estados.OCUPADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "certificate_not_available");
        }
        codigo.setIndEstado(CodigoCertificado.Estados.ANULADO.getValue());
        
        em.merge(codigo);
        bitacoraBean.logAccion(CodigoCertificado.class, codigo.getCodigoCertificadoPK().getCodigo() + "/" + codigo.getCodigoCertificadoPK().getIdPremio(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);

    }

    /**
     * Metodo que obtiene una lista de certificados almacenados por un estado
     *
     * @param idPremio
     * @param estados Valores de los estados
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return Lista de certificados
     */
    public Map<String, Object> getCodigosCertificadosPorPremio(String idPremio, List<String> estados, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();

        List resultado = new ArrayList();
        Long total;

        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<CodigoCertificado> root = query.from(CodigoCertificado.class);

            List<Predicate> predicates = new ArrayList<>();
            Premio premio = em.find(Premio.class, idPremio);
            if (estados!=null && !estados.isEmpty()) {
                estados.forEach((e) -> predicates.add(cb.equal(root.get("indEstado"), e.toUpperCase().charAt(0))));
            }
            
            if (predicates.isEmpty()) {
                query.where(cb.equal(root.get("premio"), premio));
            } else {
                query.where(cb.equal(root.get("premio"), premio), cb.and(cb.or(predicates.toArray(new Predicate[predicates.size()]))));
            }

            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            resultado = em.createQuery(query.select(root))
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();
            
            for (Object object : resultado) {
                CodigoCertificado codigo = (CodigoCertificado) object;
                if(codigo.getIndEstado().equals(CodigoCertificado.Estados.OCUPADO.getValue())){
                    PremioMiembro premioMiembro = (PremioMiembro) em.createNamedQuery("PremioMiembro.findByCodigoCertificadoIdPremioIndEstado")
                            .setParameter("codigoCertificado", codigo.getCodigoCertificadoPK().getCodigo())
                            .setParameter("idPremio", codigo.getCodigoCertificadoPK().getIdPremio())
                            .setParameter("estado", PremioMiembro.Estados.REDIMIDO_SIN_RETIRAR.getValue())
                            .setParameter("estado2", PremioMiembro.Estados.REDIMIDO_RETIRADO.getValue())
                            .getSingleResult();
                    codigo.setFechaOtorgue(premioMiembro.getFechaAsignacion());
                    codigo.setNombreMiembro(premioMiembro.getIdMiembro().getNombre()+" "+premioMiembro.getIdMiembro().getApellido());
                    codigo.setIdMiembro(premioMiembro.getIdMiembro().getIdMiembro());
                    codigo.setIndEstadoPremioMiembro(premioMiembro.getEstado());
                }
                
            }
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * verificacion de la existencia del certificado
     * 
     * @param codigo valor del certificado
     * @return existe o no uno o mas certificados
     */
    public boolean existsCodigo(String codigo) {
        return ((Long) em.createNamedQuery("CodigoCertificado.countByCodigo").setParameter("codigo", codigo).getSingleResult()) > 0;
    }

    /**
     * generacion de certificados automaticos
     * 
     * @param cantidad # de certificados deseados
     * @param idPremio identificador del premio
     * @param usuario id admin
     * @param locale locale
     */
    public void generarCodigosAuto(Long cantidad, String idPremio, String usuario, Locale locale) {
        //verificacion inicial sobre premio objetivo
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }
        
        //verificacion del tipo de premio
        if (!premio.getIndTipoPremio().equals(Premio.Tipo.CERTIFICADO.getValue())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "reward_not_certified");
        }

        //generacion de certificados
        for (int i = 0; i < cantidad; i++) {
            String idCodigo = UUID.randomUUID().toString();

            if (existsCodigo(idCodigo)) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "certificate_already_exists");
            }
            
            CodigoCertificado codigo = new CodigoCertificado(idCodigo, idPremio);

            codigo.setFechaCreacion(new Date());
            codigo.setUsuarioCreacion(usuario);
            codigo.setPremio(premio);
            codigo.setIndEstado(CodigoCertificado.Estados.DISPONIBLE.getValue());
            
            em.persist(codigo);
        }

        premio.setTotalExistencias(premio.getTotalExistencias() + cantidad);
        em.merge(premio);

        em.flush();

        bitacoraBean.logAccion(CodigoCertificado.class, "auto-gen("+cantidad+")/" + idPremio, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
    }

}
