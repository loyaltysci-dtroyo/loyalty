/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Insignias;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * Clase encargada de proveer metodos para el mantenimiento de la de la
 * informacion de insignias
 *
 * @author wtencio
 */
@Stateless
public class InsigniasBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    UtilBean utilBean;//EJB con metodos de negocio para el manejo de utilidades

    @EJB
    BitacoraBean bitacoraBean;//EJB con metodos de negocio para el manejo de bitacora

    /**
     * Método que registra una nueva insignia en la base de datos
     *
     * @param insignia información de la insignia
     * @param usuario usuario en sesión
     * @param locale
     * @return id del usuario en sesión
     */
    public String insertInsignia(Insignias insignia, String usuario, Locale locale) {

        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (insignia.getImagen() == null || insignia.getImagen().trim().isEmpty()) {
            insignia.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                insignia.setImagen(filesUtils.uploadImage(insignia.getImagen(), MyAwsS3Utils.Folder.ARTE_INSIGNIA, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }

        insignia.setTipo(Character.toUpperCase(insignia.getTipo()));
        insignia.setUsuarioCreacion(usuario);
        insignia.setFechaCreacion(Calendar.getInstance().getTime());
        insignia.setFechaModificacion(Calendar.getInstance().getTime());
        insignia.setUsuarioModificacion(usuario);
        insignia.setNombreInterno(insignia.getNombreInterno().toUpperCase());
        insignia.setEstado(Insignias.Estados.BORRADOR.getValue());
        insignia.setNumVersion(new Long(1));

        try {
            em.persist(insignia);
            em.flush();
            bitacoraBean.logAccion(Insignias.class, insignia.getIdInsignia(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (!insignia.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(insignia.getImagen());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));

        }
        return insignia.getIdInsignia();
    }

    /**
     * Método que actualiza la información de una insignia
     *
     * @param insignia información de la insignia a actualizar
     * @param usuario usuario en sesión
     * @param locale
     */
    public void updateInsignia(Insignias insignia, String usuario,Locale locale) {
        //verificacion de la existencia de la entidad
        Insignias original = em.find(Insignias.class, insignia.getIdInsignia());

        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE,locale, "badge_not_found");
        }

        String urlImagenOriginal = original.getImagen();//url imagen original
        String urlImagenNueva = null;//url imagen nueva
        //si el atributo de imagen viene nula (no deberia)
        if (insignia.getImagen() == null || insignia.getImagen().trim().isEmpty()) {
            //establecimiento de la imagen por defecto
            urlImagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            insignia.setImagen(urlImagenNueva);
        } else //ver si el valor en el atributo de imagen, difiere del almacenado
        {
            if (!urlImagenOriginal.equals(insignia.getImagen())) {//si la imagen es diferente a la almacenada
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    urlImagenNueva = filesUtils.uploadImage(insignia.getImagen(), MyAwsS3Utils.Folder.ARTE_INSIGNIA, null);
                    insignia.setImagen(urlImagenNueva);
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }

        //verificacion de cambio de estado no permitido o modificacion de entidad archivada
        Character originalEstado = original.getEstado();
        Character newEstado = insignia.getEstado();
        if ((originalEstado.equals(Insignias.Estados.PUBLICADO.getValue()) && newEstado.equals(Insignias.Estados.BORRADOR.getValue()))
                || originalEstado.equals(Insignias.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", this.getClass().getSimpleName());
        }

        if (newEstado.equals(Insignias.Estados.PUBLICADO.getValue())) {
            insignia.setFechaPublicacion(Calendar.getInstance().getTime());
        } else if (newEstado.equals(Insignias.Estados.ARCHIVADO.getValue())) {
            insignia.setFechaArchivado(Calendar.getInstance().getTime());
        }

        insignia.setUsuarioModificacion(usuario);
        insignia.setFechaModificacion(Calendar.getInstance().getTime());

        try {
            em.merge(insignia);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(Insignias.class, insignia.getIdInsignia(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        if (urlImagenNueva != null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Método que elimina si es el caso o envia a archivar la insignia que esta
     * asociada al id que ingresa por parametro
     *
     * @param idInsignia identificador único de la insignia
     * @param usuario usuario en sesión
     * @param locale
     */
    public void deleteInsignia(String idInsignia, String usuario,Locale locale) {
        //verificacion que la entidad no sea nulo
        Insignias insignia = em.find(Insignias.class, idInsignia);
        if (insignia == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE,locale, "badge_not_found");
        }

        //comprobacion que si la insignia esta en condicion de borrador, esta pueda ser eliminada, si no, se marca como archivada
        if (insignia.getEstado().compareTo(Insignias.Estados.BORRADOR.getValue()) == 0) {
            //se obtiene el url de la imagen almacenada de la entidad y borra
            String url = insignia.getImagen();
            if (!url.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(url);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            em.remove(insignia);

            bitacoraBean.logAccion(Insignias.class, idInsignia, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        } else {
            //verificacion que la insignia no este en condicion actual de archivado
            if (insignia.getEstado().compareTo(Insignias.Estados.ARCHIVADO.getValue()) == 0) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived",this.getClass().getSimpleName() );
            }

            insignia.setUsuarioModificacion(usuario);
            insignia.setFechaModificacion(Calendar.getInstance().getTime());
            insignia.setEstado(Insignias.Estados.ARCHIVADO.getValue());
            insignia.setFechaArchivado(Calendar.getInstance().getTime());

            em.merge(insignia);

            bitacoraBean.logAccion(Insignias.class, idInsignia, usuario, Bitacora.Accion.ENTIDAD_ARCHIVADA.getValue(),locale);
        }

    }

    /**
     * Método que devuelve la información de la insignia según su identificador
     *
     * @param idInsignia identificador único de la métrica
     * @param locale
     * @return resultado de la operación
     */
    public Insignias getInsigniaById(String idInsignia,Locale locale) {
        Insignias insignia = em.find(Insignias.class, idInsignia);//se busca la entidad
        //verificacion que la insignia no sea nula (no se encontro)
        if (insignia == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE,locale, "badge_not_found");
        }
        return insignia;
    }

    /**
     * Método que obtiene un listado de insignias según su estado, en un rango
     * de resultados según parametros
     *
     * @param estados
     * @param tiposInsignia
     * @param busqueda
     * @param filtros
     * @param ordenTipo
     * @param ordenCampo
     * @param registro número de registro en el cual se comienza la búsqueda
     * @param cantidad cantidad máxima de resultados
     * @param locale
     * @return listado de insignias y rango de la respuesta
     */
    public Map<String, Object> getInsignias(List<String> estados, List<String> tiposInsignia, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int cantidad, int registro, Locale locale) {
        //verificacion que el rango de paginacion sea dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Insignias> root = query.from(Insignias.class);
        
        query = Busquedas.getCriteriaQueryInsignias(cb, query, root, estados, tiposInsignia, busqueda, filtros, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        
        //retorno del resultado y encabezados sobre el rango
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Metodo que verifica si existe una insignia almacenada con el mismo nombre
     * interno que el proveido
     *
     * @param nombre Valor del nombre interno a verificar
     * @return Expresion booleana de la existencia de un nombre interno igual
     */
    public Boolean existsNombreInterno(String nombre) {
        //se obtiene la cantidad de registros con el mismo nombre interno y se pregunta si este es mayor que 0 (existe al menos uno)
        return ((Long) em.createNamedQuery("Insignias.countByNombreInterno").setParameter("nombreInterno", nombre.toUpperCase()).getSingleResult()) > 0;
    }

}
