package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.ListaReglas;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.ReglaMetricaCambio;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.flecharoja.loyalty.util.ReglasSegmento;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * reglas de cambio de metrica
 *
 * @author svargas
 */
@Stateless
public class ReglaMetricaCambioBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    @EJB
    ElegibilidadMiembroBean elegibilidadBean;

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * @param idLista Identificador de la lista de reglas
     * @param locale
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List getReglasMetricaCambioLista(String idLista, Locale locale) {
        try {
            em.getReference(ListaReglas.class, idLista).getIdLista();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idLista");
        }

        return em.createNamedQuery("ReglaMetricaCambio.findByIdLista").setParameter("idLista", idLista).getResultList();
    }

    /**
     * Metodo que crea una nueva regla en una lista especifica
     *
     * @param idSegmento
     * @param idLista Identificador de la lista de reglas
     * @param reglaMetricaCambio Objeto con la informacion de la regla
     * @param usuario usuario en sesión
     * @param locale
     * @return Respuesta con el identificador de la regla creada
     */
    public String createReglaMetricaCambio(String idSegmento, String idLista, ReglaMetricaCambio reglaMetricaCambio, String usuario, Locale locale) {
        if (reglaMetricaCambio == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "ReglaMetricaCambio");
        }

        Date date = Calendar.getInstance().getTime();
        reglaMetricaCambio.setFechaCreacion(date);

        reglaMetricaCambio.setUsuarioCreacion(usuario);

        ListaReglas listaReglas = em.find(ListaReglas.class, idLista);
        if (listaReglas == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        if (!listaReglas.getIdSegmento().getIdSegmento().equals(segmento.getIdSegmento())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "segment_id_invalid");
        }
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
           throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        Metrica metrica;
        try {
            metrica = em.find(Metrica.class, reglaMetricaCambio.getIdMetrica().getIdMetrica());
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idMetrica");
        }

        if (metrica == null || !metrica.getIndEstado().equals(Metrica.Estados.PUBLICADO.getValue())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "metrica, indEstado");
        }

        ReglasSegmento.ValoresIndOperador operador = ReglasSegmento.ValoresIndOperador.get(reglaMetricaCambio.getIndOperador());
        if (operador == null
                || (operador != ReglasSegmento.ValoresIndOperador.IGUAL
                && operador != ReglasSegmento.ValoresIndOperador.MAYOR
                && operador != ReglasSegmento.ValoresIndOperador.MAYOR_IGUAL
                && operador != ReglasSegmento.ValoresIndOperador.MENOR
                && operador != ReglasSegmento.ValoresIndOperador.MENOR_IGUAL)) {
             throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
        }

        if (ReglaMetricaCambio.ValoresIndCambio.get(reglaMetricaCambio.getIndCambio()) == null) {
             throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "metrica");
        }

        ReglaMetricaCambio.ValoresIndTipo indTipo = ReglaMetricaCambio.ValoresIndTipo.get(reglaMetricaCambio.getIndTipo());
        if (indTipo == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indTipo");
        }
        switch (indTipo) {
            case ULTIMO:
            case ANTERIOR: {
                if (ReglaMetricaCambio.ValoresIndValor.get(reglaMetricaCambio.getIndValor()) == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indValor");
                }
                break;
            }
            case ENTRE: {
                if (reglaMetricaCambio.getFechaFinal() == null || reglaMetricaCambio.getFechaInicio() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "fechaFinal, fechaInicio");
                }
                break;
            }
            case DESDE: {
                if (reglaMetricaCambio.getFechaInicio() == null) {
                   throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "fechaInicio");
                }
                break;
            }
            case HASTA: {
                if (reglaMetricaCambio.getFechaFinal() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "fechaFin");
                }
                break;
            }
        }

        try {
            //asignacion y conversion de valores de atributos por defecto
            reglaMetricaCambio.setIdLista(idLista);
            reglaMetricaCambio.setIdMetrica(metrica);

            //almacenamiento de la regla
            em.persist(reglaMetricaCambio);

            em.flush();
            bitacoraBean.logAccion(ReglaMetricaCambio.class, reglaMetricaCambio.getIdRegla(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
            //actualizacion de la lista de reglas
            listaReglas.setUsuarioModificacion(usuario);
            listaReglas.setFechaModificacion(date);
            em.merge(listaReglas);
            bitacoraBean.logAccion(ReglaMetricaCambio.class, listaReglas.getIdLista(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (ConstraintViolationException | IllegalArgumentException | NullPointerException e) {//atributos no validos
            if(e instanceof ConstraintViolationException){
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }else{
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "unknown_error");
            }
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestRecalculoRegla(reglaMetricaCambio.getIdRegla(), ReglasSegmento.TiposReglasSegmento.METRICA_CAMBIO);
            } catch (Exception ex) {
                Logger.getLogger(ReglaMetricaCambioBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }

        return reglaMetricaCambio.getIdRegla();
    }

    /**
     * Metodo que elimina de una lista especifica una regla
     *
     * @param idSegmento
     * @param idLista Identificador de la lista de reglas
     * @param idRegla Identificador de la regla
     * @param usuario usuario sesión
     */
    public void deleteReglaMetricaCambio(String idSegmento, String idLista, String idRegla, String usuario, Locale locale) {
        Date date = Calendar.getInstance().getTime();

        ListaReglas listaReglas = em.find(ListaReglas.class, idLista);
        if (listaReglas == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        if (!listaReglas.getIdSegmento().getIdSegmento().equals(segmento.getIdSegmento())) {
             throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "segment_id_invalid");
        }
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        try {
            //eliminacion de la regla (si es encontrada)
            em.remove(em.getReference(ReglaMetricaCambio.class, idRegla));

            em.flush();

            elegibilidadBean.deleteResultadoElegibilidadRegla(ReglasSegmento.TiposReglasSegmento.METRICA_CAMBIO, idRegla, Locale.getDefault());

            bitacoraBean.logAccion(ReglaMetricaCambio.class, idRegla, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
            //actualizacion de la lista de reglas
            listaReglas.setUsuarioModificacion(usuario);
            listaReglas.setFechaModificacion(date);
            em.merge(listaReglas);
            bitacoraBean.logAccion(ReglaMetricaCambio.class, listaReglas.getIdLista(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (EntityNotFoundException e) {//lista de reglas o regla no encontrada
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "metric_rule_change_not_found");
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestActualizacionSegmento(idSegmento);
            } catch (Exception ex) {
                Logger.getLogger(ReglaMetricaCambioBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }
}
