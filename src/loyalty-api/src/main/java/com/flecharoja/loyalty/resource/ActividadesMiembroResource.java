package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.EntradaRegistroActividad;
import com.flecharoja.loyalty.service.RegistrosActividadesBean;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase de recursos http disponibles para el manejo de promociones.
 *
 * @author svargas
 */
@Api(value = "Actividades")
@Path("actividades/miembro/{idMiembro}")
public class ActividadesMiembroResource {

    @EJB
    RegistrosActividadesBean actividadesBean;

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @Context
    HttpServletRequest request;

    @PathParam("idMiembro")
    String idMiembro;

    @DefaultValue("1M")
    @QueryParam("periodo")
    String periodoTiempo;

    @ApiOperation(value = "Obtener las actividades del miembro en promociones",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMiembro", value = "Identificador del miembro", required = true, dataType = "string", paramType = "path")
        ,
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/promocion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadPromocionesPorMiembro() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadPromocionesPorMiembro(idMiembro, periodoTiempo, request.getLocale());
    }

    @ApiOperation(value = "Obtener las actividades del miembro en metricas",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMiembro", value = "Identificador del miembro", required = true, dataType = "string", paramType = "path")
        ,
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/metrica")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadMetricaPorMiembro() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadMetricaPorMiembro(idMiembro, periodoTiempo, request.getLocale());
    }

    @ApiOperation(value = "Obtener las actividades del miembro en misiones",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMiembro", value = "Identificador del miembro", required = true, dataType = "string", paramType = "path")
        ,
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/mision")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadMisionesPorMiembro() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadMisionesPorMiembro(idMiembro, periodoTiempo, request.getLocale());
    }

    @ApiOperation(value = "Obtener las actividades del miembro en notificaciones",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMiembro", value = "Identificador del miembro", required = true, dataType = "string", paramType = "path")
        ,
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/notificacion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadNotificacionesPorMiembro() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadNotificacionesPorMiembro(idMiembro, periodoTiempo, request.getLocale());
    }

    @ApiOperation(value = "Obtener las actividades del miembro en premios",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMiembro", value = "Identificador del miembro", required = true, dataType = "string", paramType = "path")
        ,
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/premio")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadPremiosRecompensasPorMiembro() {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadPremiosRecompensasPorMiembro(idMiembro, periodoTiempo, request.getLocale());
    }
}
