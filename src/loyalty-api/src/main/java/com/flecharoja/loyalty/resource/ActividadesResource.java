package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.EntradaRegistroActividad;
import com.flecharoja.loyalty.service.RegistrosActividadesBean;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase de recursos http disponibles para el manejo de promociones.
 *
 * @author svargas
 */
@Api(value = "Actividades")
@Path("actividades")
public class ActividadesResource {

    @EJB
    RegistrosActividadesBean actividadesBean;

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @Context
    HttpServletRequest request;

    @DefaultValue("1M")
    @QueryParam("periodo")
    String periodoTiempo;

    @ApiOperation(value = "Obtener las actividades de todas las promociones",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/promocion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadPromocionesTodos() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PROMOCIONES_LECTURA, token, request.getLocale()) ) { 
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadPromocionesTodos(periodoTiempo, request.getLocale());
    }

    @ApiOperation(value = "Obtener las actividades de una promocion",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/promocion/{idPromocion}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadPromocionesPorPromocion(@ApiParam(value = "Identificador de la promocion", required = true) @PathParam("idPromocion") String idPromocion) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PROMOCIONES_LECTURA, token, request.getLocale())) { //|| !authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token, request.getLocale())
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadPromocionesPorPromocion(idPromocion, periodoTiempo, request.getLocale());
    }

    @ApiOperation(value = "Obtener las actividades de todas las metricas",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/metrica")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadMetricaTodos() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.METRICAS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadMetricaTodos(periodoTiempo, request.getLocale());
    }

    @ApiOperation(value = "Obtener las actividades de una metrica",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/metrica/{idMetrica}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadMetricaPorMetrica(@ApiParam(value = "Identificador de la metrica", required = true) @PathParam("idMetrica") String idMetrica) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.METRICAS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadMetricaPorMetrica(idMetrica, periodoTiempo, request.getLocale());

    }

    @ApiOperation(value = "Obtener las actividades de todas las misiones",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/mision")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadMisionesTodos() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadMisionesTodos(periodoTiempo, request.getLocale());
    }

    @ApiOperation(value = "Obtener las actividades de una mision",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/mision/{idMision}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadMisionesPorMision(@ApiParam(value = "Identificador de la mision", required = true) @PathParam("idMision") String idMision) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadMisionesPorMision(idMision, periodoTiempo, request.getLocale());
    }

    @ApiOperation(value = "Obtener las actividades de todas las notificaciones",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/notificacion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadNotificacionesTodos() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadNotificacionesTodos(periodoTiempo, request.getLocale());
    }

    @ApiOperation(value = "Obtener las actividades de una mision",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/notificacion/{idNotificacion}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadNotificacionesPorNotificacion(@ApiParam(value = "Identificador de la notificacion", required = true) @PathParam("idNotificacion") String idNotificacion) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadNotificacionesPorNotificacion(idNotificacion, periodoTiempo, request.getLocale());
    }

    @ApiOperation(value = "Obtener las actividades de todas los premios",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/premio")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadPremiosRecompensasTodos() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadPremiosRecompensasTodos(periodoTiempo, request.getLocale());
    }

    @ApiOperation(value = "Obtener las actividades de un premio",
            responseContainer = "List",
            response = EntradaRegistroActividad.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "periodo", value = "Indicador del periodo de tiempo de actividad", required = true, dataType = "string", paramType = "query", defaultValue = "1M")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/premio/{idPremio}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaRegistroActividad> getActividadPremiosRecompensasPorPremio(@ApiParam(value = "Identificador del premio", required = true) @PathParam("idPremio") String idPremio) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return actividadesBean.getActividadPremiosRecompensasPorPremio(idPremio, periodoTiempo, request.getLocale());
    }
}
