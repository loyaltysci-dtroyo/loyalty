/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "ESTADISTICA_MIEMBRO_PREMIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadisticaMiembroPremio.findAll", query = "SELECT e FROM EstadisticaMiembroPremio e")
    , @NamedQuery(name = "EstadisticaMiembroPremio.findByIdMiembro", query = "SELECT e FROM EstadisticaMiembroPremio e WHERE e.miembro.idMiembro     = :idMiembro")
    , @NamedQuery(name = "EstadisticaMiembroPremio.findByCantPremiosRedimidos", query = "SELECT e FROM EstadisticaMiembroPremio e WHERE e.cantPremiosRedimidos = :cantPremiosRedimidos")})
public class EstadisticaMiembroPremio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @Size(min = 1, max = 40)
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO")
    @OneToOne(optional = false)
    private Miembro miembro;
    @Column(name = "CANT_PREMIOS_REDIMIDOS")
    private Integer cantPremiosRedimidos;

    public EstadisticaMiembroPremio() {
    }

    public Integer getCantPremiosRedimidos() {
        return cantPremiosRedimidos;
    }

    public void setCantPremiosRedimidos(Integer cantPremiosRedimidos) {
        this.cantPremiosRedimidos = cantPremiosRedimidos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (miembro != null ? miembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadisticaMiembroPremio)) {
            return false;
        }
        EstadisticaMiembroPremio other = (EstadisticaMiembroPremio) object;
        if ((this.miembro == null && other.miembro != null) || (this.miembro != null && !this.miembro.equals(other.miembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.EstadisticaMiembroPremio[ idMiembro=" + miembro.toString() + " ]";
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

}
