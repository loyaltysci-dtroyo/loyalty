package com.flecharoja.loyalty.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class EstadisticasMetrica {
    
    @XmlRootElement
    public class Periodo {
        private String mes;
        private double cantAcumulada;
        private double cantRedimida;
        private double cantVencida;

        public Periodo(String mes, double cantAcumulada, double cantRedimida, double cantVencida) {
            this.mes = mes;
            this.cantAcumulada = cantAcumulada;
            this.cantRedimida = cantRedimida;
            this.cantVencida = cantVencida;
        }

        public String getMes() {
            return mes;
        }

        public void setMes(String mes) {
            this.mes = mes;
        }

        public double getCantAcumulada() {
            return cantAcumulada;
        }

        public void setCantAcumulada(double cantAcumulada) {
            this.cantAcumulada = cantAcumulada;
        }

        public double getCantRedimida() {
            return cantRedimida;
        }

        public void setCantRedimida(double cantRedimida) {
            this.cantRedimida = cantRedimida;
        }

        public double getCantVencida() {
            return cantVencida;
        }

        public void setCantVencida(double cantVencida) {
            this.cantVencida = cantVencida;
        }
    }
    
    private String idMetrica;
    private String nombreMetrica;
    private String nombreInternoMetrica;
    
    private double cantAcumulada;
    private double cantRedimida;
    private double cantVencida;
    
    private List<Periodo> periodos;
    
    private List<EstadisticaIndividualMetricaNivel> estadisticasNivelMetrica;

    public EstadisticasMetrica() {
        periodos = new ArrayList<>();
    }

    public String getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    public String getNombreMetrica() {
        return nombreMetrica;
    }

    public void setNombreMetrica(String nombreMetrica) {
        this.nombreMetrica = nombreMetrica;
    }

    public String getNombreInternoMetrica() {
        return nombreInternoMetrica;
    }

    public void setNombreInternoMetrica(String nombreInternoMetrica) {
        this.nombreInternoMetrica = nombreInternoMetrica;
    }

    public double getCantAcumulada() {
        return cantAcumulada;
    }

    public void setCantAcumulada(double cantAcumulada) {
        this.cantAcumulada = cantAcumulada;
    }

    public double getCantRedimida() {
        return cantRedimida;
    }

    public void setCantRedimida(double cantRedimida) {
        this.cantRedimida = cantRedimida;
    }

    public double getCantVencida() {
        return cantVencida;
    }

    public void setCantVencida(double cantVencida) {
        this.cantVencida = cantVencida;
    }

    public List<Periodo> getPeriodos() {
        return periodos;
    }

    public void setPeriodos(List<Periodo> periodos) {
        this.periodos = periodos;
    }
    
    public void addPeriodo(String mes, double cantAcumulada, double cantRedimida, double cantVencida) {
        this.periodos.add(new Periodo(mes, cantAcumulada, cantRedimida, cantVencida));
    }

    public List<EstadisticaIndividualMetricaNivel> getEstadisticasNivelMetrica() {
        return estadisticasNivelMetrica;
    }

    public void setEstadisticasNivelMetrica(List<EstadisticaIndividualMetricaNivel> estadisticasNivelMetrica) {
        this.estadisticasNivelMetrica = estadisticasNivelMetrica;
    }
}
