package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.ListaReglas;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.ReglaMetricaBalance;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.flecharoja.loyalty.util.ReglasSegmento;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * reglas de balance de metrica
 *
 * @author svargas
 */
@Stateless
public class ReglaMetricaBalanceBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    @EJB
    ElegibilidadMiembroBean elegibilidadBean;

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * @param idLista Identificador de la lista de reglas
     * @param locale
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List getReglasMetricaBalanceLista(String idLista, Locale locale) {
        try {
            em.getReference(ListaReglas.class, idLista).getIdLista();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idLista");
        }

        return em.createNamedQuery("ReglaMetricaBalance.findByIdLista").setParameter("idLista", idLista).getResultList();
    }

    /**
     * Metodo que crea una nueva regla en una lista especifica
     *
     * @param idSegmento Identificador de segmento
     * @param idLista Identificador de la lista de reglas
     * @param reglaMetricaBalance Objeto con la informacion de la regla
     * @param usuario usuario en sesión
     * @param locale
     * @return Respuesta con el identificador de la regla creada
     */
    public String createReglaMetricaBalance(String idSegmento, String idLista, ReglaMetricaBalance reglaMetricaBalance, String usuario, Locale locale) {
        if (reglaMetricaBalance == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "ReglaMetricaBalance");
        }

        Date date = Calendar.getInstance().getTime();
        reglaMetricaBalance.setFechaCreacion(date);

        reglaMetricaBalance.setUsuarioCreacion(usuario);

        ListaReglas listaReglas = em.find(ListaReglas.class, idLista);
        if (listaReglas == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        if (!listaReglas.getIdSegmento().getIdSegmento().equals(segmento.getIdSegmento())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "segment_id_invalid");
        }
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        Metrica metrica;
        try {
            metrica = em.find(Metrica.class, reglaMetricaBalance.getIdMetrica().getIdMetrica());
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idMetrica");
        }

        //comprobacion de indicadores
        if (ReglaMetricaBalance.ValoresIndBalance.get(reglaMetricaBalance.getIndBalance()) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indBalance");
        }
        ReglasSegmento.ValoresIndOperador operador = ReglasSegmento.ValoresIndOperador.get(reglaMetricaBalance.getIndOperador());
        if (operador == null
                || (operador != ReglasSegmento.ValoresIndOperador.IGUAL
                && operador != ReglasSegmento.ValoresIndOperador.MAYOR
                && operador != ReglasSegmento.ValoresIndOperador.MAYOR_IGUAL
                && operador != ReglasSegmento.ValoresIndOperador.MENOR
                && operador != ReglasSegmento.ValoresIndOperador.MENOR_IGUAL)) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
        }

        if (metrica == null || !metrica.getIndEstado().equals(Metrica.Estados.PUBLICADO.getValue())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "metrica");
        }

        //asignacion y conversion de valores de atributos por defecto
        reglaMetricaBalance.setIdLista(idLista);
        reglaMetricaBalance.setIdMetrica(metrica);

        try {
            //almacenamiento de la regla
            em.persist(reglaMetricaBalance);

            em.flush();

            bitacoraBean.logAccion(ReglaMetricaBalance.class, reglaMetricaBalance.getIdRegla(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);

            //actualizacion de la lista de reglas
            listaReglas.setUsuarioModificacion(usuario);
            listaReglas.setFechaModificacion(date);
            em.merge(listaReglas);
            bitacoraBean.logAccion(ReglaMetricaBalance.class, listaReglas.getIdLista(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (ConstraintViolationException | IllegalArgumentException | NullPointerException e) {//atributos no validos
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "unknown_error");
            }
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestRecalculoRegla(reglaMetricaBalance.getIdRegla(), ReglasSegmento.TiposReglasSegmento.METRICA_BALANCE);
            } catch (Exception ex) {
                Logger.getLogger(ReglaMetricaBalanceBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }

        return reglaMetricaBalance.getIdRegla();
    }

    /**
     * Metodo que elimina de una lista especifica una regla
     *
     * @param idSegmento Identificador de segmento
     * @param idLista Identificador de la lista de reglas
     * @param idRegla Identificador de la regla
     * @param usuario usuario en sesión
     * @param locale
     */
    public void deleteReglaMetricaBalance(String idSegmento, String idLista, String idRegla, String usuario, Locale locale) {
        Date date = Calendar.getInstance().getTime();

        ListaReglas listaReglas = em.find(ListaReglas.class, idLista);
        if (listaReglas == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        if (!listaReglas.getIdSegmento().getIdSegmento().equals(segmento.getIdSegmento())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "segment_id_invalid");
        }
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        try {
            //eliminacion de la regla (si es encontrada)
            em.remove(em.getReference(ReglaMetricaBalance.class, idRegla));

            em.flush();

            elegibilidadBean.deleteResultadoElegibilidadRegla(ReglasSegmento.TiposReglasSegmento.METRICA_BALANCE, idRegla, Locale.getDefault());

            bitacoraBean.logAccion(ReglaMetricaBalance.class, idRegla, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
            //actualizacion de la lista de reglas
            listaReglas.setUsuarioModificacion(usuario);
            listaReglas.setFechaModificacion(date);
            em.merge(listaReglas);
            bitacoraBean.logAccion(ReglaMetricaBalance.class, listaReglas.getIdLista(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (EntityNotFoundException e) {//lista de reglas o regla no encontrada
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestActualizacionSegmento(idSegmento);
            } catch (Exception ex) {
                Logger.getLogger(ReglaMetricaBalanceBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }
}
