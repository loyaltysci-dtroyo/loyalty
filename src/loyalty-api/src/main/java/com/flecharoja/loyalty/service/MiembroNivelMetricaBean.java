/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.MiembroMetricaNivel;
import com.flecharoja.loyalty.model.NivelMetrica;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.validation.ConstraintViolationException;

/**
 * Clase encargada de poseer los metodos de negocio para el manejo de niveles de
 * metrica para un miembro
 *
 * @author wtencio
 */
@Stateless
public class MiembroNivelMetricaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB encargado de la persistencia en bitácora

    /**
     * Método que asigna a un miembro un nivel de una métrica especifica
     *
     * @param idMiembro identificador único del miembro
     * @param idNivel identificador único del nivel
     * @param idMetrica identificador único de la métrica
     * @param usuario usuario en sesión
     * @param locale
     */
    public void assignMiembroNivelMetrica(String idMiembro, String idNivel, String idMetrica, String usuario, Locale locale) {
        MiembroMetricaNivel miembroMetricaNivel = new MiembroMetricaNivel(idMiembro, idMetrica);

        try {
            Miembro miembro = em.find(Miembro.class, idMiembro);
            NivelMetrica nivelMetrica = em.find(NivelMetrica.class, idNivel);
            Metrica metrica = em.find(Metrica.class, idMetrica);

            if (metrica.getGrupoNiveles().getIdGrupoNivel().equals(nivelMetrica.getGrupoNiveles().getIdGrupoNivel())) {
                miembroMetricaNivel.setMetrica(metrica);
                miembroMetricaNivel.setMiembro(miembro);
                miembroMetricaNivel.setIdNivel(nivelMetrica);
                miembroMetricaNivel.setFechaCreacion(Calendar.getInstance().getTime());
                miembroMetricaNivel.setUsuarioCreacion(usuario);

                em.persist(miembroMetricaNivel);
                em.flush();
                bitacoraBean.logAccion(MiembroMetricaNivel.class, idMiembro + "/" + idMetrica, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "metric_level_not_metric");
            }
        } catch (EntityExistsException e) {
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "metric_member_exists");
        } catch (ConstraintViolationException e) {//atributos no validos
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que quita la relación de un miembro con una métrica y su nivel
     *
     * @param idMiembro identificador único del miembro
     * @param idMetrica identificador único de la métrica
     * @param usuario usuario en sesión
     * @param locale
     */
    public void unassignMiembroNivelMetrica(String idMiembro, String idMetrica, String usuario, Locale locale) {
        MiembroMetricaNivel miembroMetricaNivel = (MiembroMetricaNivel) em.createNamedQuery("MiembroMetricaNivel.findByIdMetricaByIdMiembro").setParameter("idMetrica", idMetrica).setParameter("idMiembro", idMiembro).getSingleResult();
        if (miembroMetricaNivel == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "metric_member_not_found");
        }

        em.remove(miembroMetricaNivel);
        bitacoraBean.logAccion(MiembroMetricaNivel.class, idMetrica + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

    /**
     * Método que devuelve una lista de niveles que están asociados a un miembro
     * especifico
     *
     * @param idMiembro identificador del miembro
     * @param tipo
     * @param estados
     * @param cantidad de registros que se quieren por pagina
     * @param indExpiracion
     * @param busqueda
     * @param ordenTipo
     * @param ordenCampo
     * @param filtros
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return lista de niveles de un miembro
     */
    public Map<String, Object> getMetricasMiembro(String idMiembro,String tipo, List<String> estados,  String indExpiracion, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Metrica> root = query.from(Metrica.class);
        
        query = Busquedas.getCriteriaQueryMetrica(cb, query, root, estados, indExpiracion, busqueda, filtros, ordenTipo, ordenCampo);
        Subquery<String> subquery = query.subquery(String.class);
        Root<MiembroMetricaNivel> rootSubquery = subquery.from(MiembroMetricaNivel.class);
        subquery.select(rootSubquery.get("miembroMetricaNivelPK").get("idMetrica"));
        if (tipo == null || tipo.toUpperCase().charAt(0) == Indicadores.ASIGNADO) {
            subquery.where(cb.equal(rootSubquery.get("miembroMetricaNivelPK").get("idMiembro"), idMiembro));
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idMetrica")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idMetrica")).value(subquery)));
            }
        } else {
            if (tipo.toUpperCase().charAt(0) == Indicadores.DISPONIBLES) {
                subquery.where(cb.equal(rootSubquery.get("miembroMetricaNivelPK").get("idMiembro"), idMiembro));
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.equal(root.get("indEstado"), Metrica.Estados.PUBLICADO.getValue()), cb.or(cb.in(root.get("idMetrica")).value(subquery).not()));
                } else {
                    query.where(cb.equal(root.get("indEstado"), Metrica.Estados.PUBLICADO.getValue()), cb.or(cb.in(root.get("idMetrica")).value(subquery).not()));
                }
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "tipo");
            }
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;

    }
    
    public Map<String, Object> getMiembrosMetrica(String idMetrica, String tipo, List<String> estados, List<String> generos, List<String> estadosCivil, List<String> educacion, String contactoEmail, String contactoSMS, String contactoNotificacion, String contactoEstado, String tieneHijos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Miembro> root = query.from(Miembro.class);

        query = Busquedas.getCriteriaQueryMiembros(cb, query, root, estados, generos, estadosCivil, educacion, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, ordenTipo, ordenCampo);
        Subquery<String> subquery = query.subquery(String.class);
        Root<MiembroMetricaNivel> rootSubquery = subquery.from(MiembroMetricaNivel.class);
        subquery.select(rootSubquery.get("miembroMetricaNivelPK").get("idMiembro"));
        if (tipo == null || tipo.toUpperCase().charAt(0) == Indicadores.ASIGNADO) {
            subquery.where(cb.equal(rootSubquery.get("miembroMetricaNivelPK").get("idMetrica"), idMetrica));
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            }
        } else {
            if (tipo.toUpperCase().charAt(0) == Indicadores.DISPONIBLES) {
                subquery.where(cb.equal(rootSubquery.get("miembroMetricaNivelPK").get("idMetrica"), idMetrica));
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                } else {
                    query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                }
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "tipo");
            }
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //recurrido del resultado para la asignacion de informacion adicional (almacenada en keycloak)
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            resultado.stream().forEach((t) -> {
                Miembro miembro = (Miembro) t;
                try {
                    miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
                } catch (Exception e2) {
                    miembro.setNombreUsuario("Prueba");
                }
            });
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }


}
