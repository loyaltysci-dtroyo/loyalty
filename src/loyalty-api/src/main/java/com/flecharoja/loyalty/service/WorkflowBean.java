/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Workflow;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;
import static com.flecharoja.loyalty.model.Workflow.DisparadoresIndicador;
import com.flecharoja.loyalty.model.WorkflowNodo;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * flujos de trabajo (workflows)
 *
 * @author faguilar
 */
@Stateless
public class WorkflowBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Obtencion de notificaciones en rango de registros (registro & cantidad) y
     * filtrado opcionalmente por indicadores y terminos de busqueda sobre
     * algunos campos
     *
     * @param estados Listado de valores de indicadores de estados deseados
     * @param tipos Listado de valores de indicadores de tipos de mision
     * deseados
     * @param busqueda Terminos de busqueda
     * @param filtros Listado de campos sobre que aplicar los terminos de
     * busqueda
     * @param cantidad Cantidad de registros deseados
     * @param registro Numero de registro inicial en el resultado
     * @return Listado de misiones deseadas
     */
    public Map<String, Object> getWorkflows(List<String> estados, List<String> tipos, String busqueda, List<String> filtros, int cantidad, int registro, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Workflow> root = query.from(Workflow.class);

        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((String t) -> Workflow.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definieron valores de indicadores de tipos de mision...
        if (tipos != null && !tipos.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de tipos de mision validos y definidos
            predicates.add(tipos.stream().distinct().filter((t) -> Workflow.DisparadoresIndicador.get(t) != null).map((t) -> cb.equal(root.get("indDisparador"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombreWorkflow")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("nombreInternoWorkflow")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombreWorkflow")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "nombre-interno": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombreInternoWorkflow")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }

                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }

        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");

        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * Metodo que obtiene un workflow por su numero de identificacion
     *
     * @param idWorkflow Identificacion del workflow
     * @param locale
     * @return Respuesta con la informacion del workflow
     */
    public Workflow getWorkflowPorId(String idWorkflow, Locale locale) {
        Workflow workflow = em.find(Workflow.class, idWorkflow);
        //verificacion que el workflow exista
        if (workflow == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "workflow_not_found");
        }
        return workflow;
    }

    /**
     * Metodo que almacena la informacion de un workflow
     *
     * @param workflow Objeto con la informacion del workflow
     * @param usuario usuario en sesión
     * @param locale
     * @return Respuesta con el identificacion del workflow almacenado
     */
    public String insertWorkflow(Workflow workflow, String usuario, Locale locale) {
        if (workflow == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Workflow");
        }
        //establecimiento de valores por defecto
        Date date = Calendar.getInstance().getTime();
        workflow.setFechaCreacion(date);
        workflow.setFechaModificacion(date);
        workflow.setNumVersion(new Long(1));

        workflow.setUsuarioCreacion(usuario);
        workflow.setUsuarioModificacion(usuario);
        workflow.setIndEstado(Workflow.Estados.BORRADOR.getValue());

        /*
        Si el workflow es de tipo workflow de mision, se establece el valor del indicador de workflow de mision
        Esto para optimizar el procesamiento en storm (evita hacer multiples consultas) 
         */
        if (DisparadoresIndicador.get(String.valueOf((workflow.getIndDisparador()))) == DisparadoresIndicador.MIE_MISION_ESPEC) {
            workflow.setIsWorkflowMisionEspec(true);
        }

        try {
            em.persist(workflow);
            em.flush();
            bitacoraBean.logAccion(Workflow.class, workflow.getIdWorkflow(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException e) {//atributos no validos
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        return workflow.getIdWorkflow();
    }

    /**
     * Metodo que modifica la informacion de un workflow almacenado
     *
     * @param workflow Objeto con la informacion del workflow
     * @param usuario usuario en sesión
     * @param locale
     */
    public void editWorkflow(Workflow workflow, String usuario, Locale locale) {
        if (workflow == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Workflow");
        }
        Workflow original;
        try {
            original = em.find(Workflow.class, workflow.getIdWorkflow());
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idWorkflow");
        }
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "workflow_not_found");
        }
        /*
        Si el workflow es de tipo workflow de mision, se establece el valor del indicador de workflow de mision Al igual que en los nodos
        Esto para optimizar el procesamiento en storm (evita hacer multiples consultas) 
         */
        if (!original.isIsWorkflowMisionEspec()
                && DisparadoresIndicador.get(String.valueOf((workflow.getIndDisparador()))) == DisparadoresIndicador.MIE_MISION_ESPEC) {
            workflow.setIsWorkflowMisionEspec(true);
            WorkflowNodo wn = workflow.getIdSucesor();
            while (wn != null) {
                wn.setIsNodoMisionEspecial(true);
                em.persist(wn);
                wn = workflow.getIdSucesor();
            }
        } else if (original.isIsWorkflowMisionEspec()
                && !(DisparadoresIndicador.get(String.valueOf((workflow.getIndDisparador()))) == DisparadoresIndicador.MIE_MISION_ESPEC)) {
            workflow.setIsWorkflowMisionEspec(false);
            WorkflowNodo wn = workflow.getIdSucesor();
            while (wn != null) {
                wn.setIsNodoMisionEspecial(false);
                em.persist(wn);
                wn = workflow.getIdSucesor();
            }
        }
        //establecimiento de valores por defecto
        Date date = Calendar.getInstance().getTime();
        workflow.setFechaModificacion(date);
        workflow.setUsuarioModificacion(usuario);

        if (Workflow.Estados.get(workflow.getIndEstado()) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indEstado");
        }

        //verificacion de que posible cambio de estado sea valido (Activo NO A Borrador o estado almacenado como Archivado)
        if ((original.getIndEstado().equals(Workflow.Estados.ACTIVO.getValue()) && workflow.getIndEstado().equals(Workflow.Estados.BORRADOR.getValue()))
                || original.getIndEstado().equals(Workflow.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "workflow_not_change_state");
        }

//        //establecimiento de valores por defecto en condicion del valor de indicador de estado
//        if (workflow.getIndEstado()==Workflow.Estados.ACTIVO.getValue()) {
//            workflow.setFechaPublicacion(date);
//        } else if (workflow.getIndEstado().equalsIgnoreCase(Workflow.Estados.ARCHIVADO.getValue())) {
//            em.createNamedQuery("WorkflowListaWorkflow.deleteAllByIdWorkflowNotInWorkflowArchivada").setParameter("idWorkflow", workflow.getIdWorkflow()).executeUpdate();
//            workflow.setFechaArchivado(date);
//        }
        try {
            em.merge(workflow);
            em.flush();
            bitacoraBean.logAccion(Workflow.class, workflow.getIdWorkflow(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

    }

    /**
     * Metodo que modifica la informacion de un workflow para que este pase a
     * estado de archivo
     *
     * @param idWorkflow Identificacion del workflow
     * @param usuario usuario en sesión
     * @param locale
     */
    public void disableWorkflow(String idWorkflow, String usuario, Locale locale) {
        Date date = Calendar.getInstance().getTime();
        Workflow workflow = em.find(Workflow.class, idWorkflow);
        if (workflow == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "workflow_not_found");
        }
        if (workflow.getIndEstado().equals(Workflow.Estados.BORRADOR.getValue())) {
            em.remove(workflow);
            bitacoraBean.logAccion(Workflow.class, workflow.getIdWorkflow(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        } else {
            workflow.setFechaModificacion(date);
            workflow.setUsuarioModificacion(usuario);
            workflow.setIndEstado(Workflow.Estados.ARCHIVADO.getValue());
            em.merge(workflow);
            bitacoraBean.logAccion(Workflow.class, workflow.getIdWorkflow(), usuario, Bitacora.Accion.ENTIDAD_ARCHIVADA.getValue(), locale);
        }
    }

    /**
     * Metodo que elimina un workflow de la base de datos
     *
     * @param idWorkflow Identificacion del workflow
     * @param usuario usuario en sesión
     * @param locale
     */
    public void removeWorkflow(String idWorkflow, String usuario, Locale locale) {
        Workflow workflow = em.find(Workflow.class, idWorkflow);
        if (workflow == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "workflow_not_found");
        }
        if (workflow.getIndEstado().equals(Workflow.Estados.BORRADOR.getValue())) {
            em.remove(workflow);
            bitacoraBean.logAccion(Workflow.class, workflow.getIdWorkflow(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        } else if (workflow.getIndEstado().equals(Workflow.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Workflow");
        } else {
            Date date = Calendar.getInstance().getTime();
            workflow.setFechaCreacion(date);
            workflow.setUsuarioModificacion(usuario);
            workflow.setIndEstado(Workflow.Estados.ARCHIVADO.getValue());

            em.merge(workflow);
            bitacoraBean.logAccion(Workflow.class, workflow.getIdWorkflow(), usuario, Bitacora.Accion.ENTIDAD_ARCHIVADA.getValue(), locale);
        }
    }

    /**
     * Metodo que verifica si existe un workflow almacenado con el mismo nombre
     * de despliegue que el proveido
     *
     * @param nombre Valor del de despliegue interno a verificar
     * @return Respuesta con el valor booleano de la existencia del nombre de
     * despliegue
     */
    public boolean existsNombreInterno(String nombre) {
        return ((Long) em.createNamedQuery("Workflow.countByNombreInterno").setParameter("nombreInterno", nombre).getSingleResult()) > 0;
    }

    /**
     * Metodo que realiza una busqueda en base a palabras claves sobre una serie
     * de atributos, opcionalmente por estados y en un rango de registros
     *
     * @param busqueda Cadena de texto con las palabras claves
     * @param estados Indicador de los estados de los workflows deseados
     * @param registro Numero de registro inicial
     * @param cantidad Cantidad de registros
     * @param locale
     * @return Listado de todos los workflows encontrados
     */
    public Map<String, Object> searchWorkflows(String busqueda, String estados, int registro, int cantidad, Locale locale) {
        //verificacion que el rango de registros solicitado este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        Map<String, Object> respuesta = new HashMap<>();
        List resultado;
        Long total;

        String[] palabrasClaves = busqueda.split(" ");
        try {
            //definicion del query
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<Workflow> root = query.from(Workflow.class);

            //definicion de los predicados de comparacion de las palabras claves con un numero de columnas seleccionadas
            List<Predicate> predicates = new ArrayList<>();
            for (String palabraClave : palabrasClaves) {
                predicates.add(cb.like(cb.upper(root.get("nombre")), "%" + palabraClave.toUpperCase() + "%"));
                predicates.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + palabraClave.toUpperCase() + "%"));
            }
            //establecimiento de la clausula WHERE
            if (estados == null) {//en el caso de que no se hayan pasado algun estado, se ignora el filtrado por los mismos
                query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
            } else {
                //definicion de predicado adicional para el filtrado por estados
                List<Predicate> predicates2 = new ArrayList<>();
                for (String e : estados.split("-")) {
                    predicates2.add(cb.equal(root.get("indEstado"), e.toUpperCase()));
                }
                query.where(cb.or(predicates2.toArray(new Predicate[predicates2.size()])), cb.or(predicates.toArray(new Predicate[predicates.size()])));
            }

            //conteo del total
            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            //recuperacion de la lista de resultados en un rango valido
            resultado = em.createQuery(query.select(root))
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();
        } catch (IllegalArgumentException e) {//paginacion erronea
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        respuesta.put("resultado", resultado);
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);

        return respuesta;
    }

}
