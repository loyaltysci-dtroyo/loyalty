package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NOTICIA_COMENTARIO")
@XmlRootElement
public class NoticiaComentario implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ApiModelProperty(value = "Identificador del comentario", readOnly = true)
    @Id
    @GeneratedValue(generator = "noticia_comentario_uuid")
    @GenericGenerator(name = "noticia_comentario_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_COMENTARIO")
    private String idComentario;
    
    @ApiModelProperty(value = "Comentario", required = true)
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "CONTENIDO")
    private String contenido;
    
    @ApiModelProperty(hidden = true)
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @ApiModelProperty(hidden = true)
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @ApiModelProperty(value = "Indicador si la noticia fue creada por un administrador", readOnly = true)
    @Column(name = "IND_AUTOR_ADMIN")
    private Boolean indAutorAdmin;
    
    @ApiModelProperty(value = "Identificador del autor")
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "ID_AUTOR")
    private String idAutor;
    
    @ApiModelProperty(hidden = true)
    @JoinColumn(name = "ID_NOTICIA", referencedColumnName = "ID_NOTICIA")
    @ManyToOne(optional = false)
    private Noticia noticia;
    
    @ApiModelProperty(hidden = true)
    @JoinColumn(name = "ID_COMENTARIO_REFERENTE", referencedColumnName = "ID_COMENTARIO")
    @ManyToOne()
    private NoticiaComentario comentarioReferente;
    
    @ApiModelProperty(hidden = true)
    @Basic(optional = false)
    @NotNull
    @Version
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @ApiModelProperty(hidden = true)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "comentarioReferente")
    private List<NoticiaComentario> comentarios;
    
    @ApiModelProperty(value = "Cantidad de comentarios", readOnly = true)
    @Transient
    private long cantComentarios;
    
    @ApiModelProperty(value = "Nombre publico del autor/Nombre de usuario", readOnly = true)
    @Transient
    private String nombreAutor;
    
    @ApiModelProperty(value = "URL del avatar del autor", readOnly = true)
    @Transient
    private String avatarAutor;

    public NoticiaComentario() {
    }

    public String getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(String idComentario) {
        this.idComentario = idComentario;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Boolean getIndAutorAdmin() {
        return indAutorAdmin;
    }

    public void setIndAutorAdmin(Boolean indAutorAdmin) {
        this.indAutorAdmin = indAutorAdmin;
    }

    public String getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(String idAutor) {
        this.idAutor = idAutor;
    }

    @XmlTransient
    public Noticia getNoticia() {
        return noticia;
    }

    public void setNoticia(Noticia noticia) {
        this.noticia = noticia;
    }

    public NoticiaComentario getComentarioReferente() {
        return comentarioReferente;
    }

    public void setComentarioReferente(NoticiaComentario comentarioReferente) {
        this.comentarioReferente = comentarioReferente;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @XmlTransient
    public List<NoticiaComentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<NoticiaComentario> comentarios) {
        this.comentarios = comentarios;
    }

    public long getCantComentarios() {
        return cantComentarios;
    }

    public void setCantComentarios(long cantComentarios) {
        this.cantComentarios = cantComentarios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComentario != null ? idComentario.hashCode() : 0);
        return hash;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    public String getAvatarAutor() {
        return avatarAutor;
    }

    public void setAvatarAutor(String avatarAutor) {
        this.avatarAutor = avatarAutor;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NoticiaComentario)) {
            return false;
        }
        NoticiaComentario other = (NoticiaComentario) object;
        if ((this.idComentario == null && other.idComentario != null) || (this.idComentario != null && !this.idComentario.equals(other.idComentario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.NoticiaComentario[ idComentario=" + idComentario + " ]";
    }
    
}
