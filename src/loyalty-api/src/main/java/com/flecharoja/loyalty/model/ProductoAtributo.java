/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PRODUCTO_ATRIBUTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoAtributo.findAll", query = "SELECT p FROM ProductoAtributo p"),
    @NamedQuery(name = "ProductoAtributo.findByIdProductoByIdAtributo", query = "SELECT p FROM ProductoAtributo p WHERE p.productoAtributoPK.idProducto = :idProducto AND p.productoAtributoPK.idAtributo = :idAtributo"),
    @NamedQuery(name = "ProductoAtributo.countByIdAtributo", query = "SELECT COUNT(p) FROM ProductoAtributo p WHERE p.productoAtributoPK.idAtributo = :idAtributo"),
    @NamedQuery(name = "ProductoAtributo.findByIdProducto", query = "SELECT p FROM ProductoAtributo p WHERE p.productoAtributoPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoAtributo.findByNoIdProducto", query = "SELECT a FROM AtributoDinamicoProducto a  WHERE a.idAtributo NOT IN (SELECT p.productoAtributoPK.idAtributo FROM ProductoAtributo p WHERE p.productoAtributoPK.idProducto = :idProducto)")
})

public class ProductoAtributo implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoAtributoPK productoAtributoPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "VALOR")
    private String valor;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_ATRIBUTO", referencedColumnName = "ID_ATRIBUTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private AtributoDinamicoProducto atributoDinamicoProducto;
    
    @JoinColumn(name = "ID_PRODUCTO", referencedColumnName = "ID_PRODUCTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;

    public ProductoAtributo() {
    }

    public ProductoAtributo(ProductoAtributoPK productoAtributoPK) {
        this.productoAtributoPK = productoAtributoPK;
    }

    public ProductoAtributo(ProductoAtributoPK productoAtributoPK, String valor, Date fechaCreacion, String usuarioCreacion) {
        this.productoAtributoPK = productoAtributoPK;
        this.valor = valor;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public ProductoAtributo(String idProducto, String idAtributo) {
        this.productoAtributoPK = new ProductoAtributoPK(idProducto, idAtributo);
    }

    @ApiModelProperty(hidden = true)
    public ProductoAtributoPK getProductoAtributoPK() {
        return productoAtributoPK;
    }

    public void setProductoAtributoPK(ProductoAtributoPK productoAtributoPK) {
        this.productoAtributoPK = productoAtributoPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public AtributoDinamicoProducto getAtributoDinamicoProducto() {
        return atributoDinamicoProducto;
    }

    public void setAtributoDinamicoProducto(AtributoDinamicoProducto atributoDinamicoProducto) {
        this.atributoDinamicoProducto = atributoDinamicoProducto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoAtributoPK != null ? productoAtributoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoAtributo)) {
            return false;
        }
        ProductoAtributo other = (ProductoAtributo) object;
        if ((this.productoAtributoPK == null && other.productoAtributoPK != null) || (this.productoAtributoPK != null && !this.productoAtributoPK.equals(other.productoAtributoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ProductoAtributo[ productoAtributoPK=" + productoAtributoPK + " ]";
    }
    
}
