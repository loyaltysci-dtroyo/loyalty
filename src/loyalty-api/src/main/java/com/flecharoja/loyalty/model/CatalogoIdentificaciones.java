/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CATALOGO_IDENTIFICACIONES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatalogoIdentificaciones.findAll", query = "SELECT c FROM CatalogoIdentificaciones c"),
    @NamedQuery(name = "CatalogoIdentificaciones.findByIdIdentificacion", query = "SELECT c FROM CatalogoIdentificaciones c WHERE c.idIdentificacion = :idIdentificacion"),
    @NamedQuery(name = "CatalogoIdentificaciones.findByNombre", query = "SELECT c FROM CatalogoIdentificaciones c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CatalogoIdentificaciones.findByCantidadDigitos", query = "SELECT c FROM CatalogoIdentificaciones c WHERE c.cantidadDigitos = :cantidadDigitos"),
    @NamedQuery(name = "CatalogoIdentificaciones.findByFechaCreacion", query = "SELECT c FROM CatalogoIdentificaciones c WHERE c.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "CatalogoIdentificaciones.findByUsuarioCreacion", query = "SELECT c FROM CatalogoIdentificaciones c WHERE c.usuarioCreacion = :usuarioCreacion"),
    @NamedQuery(name = "CatalogoIdentificaciones.findByFechaModificacion", query = "SELECT c FROM CatalogoIdentificaciones c WHERE c.fechaModificacion = :fechaModificacion"),
    @NamedQuery(name = "CatalogoIdentificaciones.findByUsuarioModificacion", query = "SELECT c FROM CatalogoIdentificaciones c WHERE c.usuarioModificacion = :usuarioModificacion"),
    @NamedQuery(name = "CatalogoIdentificaciones.findByNumVersion", query = "SELECT c FROM CatalogoIdentificaciones c WHERE c.numVersion = :numVersion"),
    @NamedQuery(name = "CatalogoIdentificaciones.findByIdentificador", query = "SELECT c FROM CatalogoIdentificaciones c WHERE c.identificador = :identificador")})
public class CatalogoIdentificaciones implements Serializable {

    @Version
    @Basic(optional = false)
    @NotNull()
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "identificacion_uuid")
    @GenericGenerator(name = "identificacion_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_IDENTIFICACION")
    private String idIdentificacion;
     
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANTIDAD_DIGITOS")
    private BigInteger cantidadDigitos;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDENTIFICADOR")
    private Character identificador;

    public CatalogoIdentificaciones() {
    }

    public CatalogoIdentificaciones(String idIdentificacion) {
        this.idIdentificacion = idIdentificacion;
    }

    public CatalogoIdentificaciones(String idIdentificacion, String nombre, BigInteger cantidadDigitos, Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion, Character identificador) {
        this.idIdentificacion = idIdentificacion;
        this.nombre = nombre;
        this.cantidadDigitos = cantidadDigitos;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
        this.identificador = identificador;
    }

    public String getIdIdentificacion() {
        return idIdentificacion;
    }

    public void setIdIdentificacion(String idIdentificacion) {
        this.idIdentificacion = idIdentificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigInteger getCantidadDigitos() {
        return cantidadDigitos;
    }

    public void setCantidadDigitos(BigInteger cantidadDigitos) {
        this.cantidadDigitos = cantidadDigitos;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Character getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Character identificador) {
        this.identificador = identificador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idIdentificacion != null ? idIdentificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatalogoIdentificaciones)) {
            return false;
        }
        CatalogoIdentificaciones other = (CatalogoIdentificaciones) object;
        if ((this.idIdentificacion == null && other.idIdentificacion != null) || (this.idIdentificacion != null && !this.idIdentificacion.equals(other.idIdentificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CatalogoIdentificaciones[ idIdentificacion=" + idIdentificacion + " ]";
    }
   
}
