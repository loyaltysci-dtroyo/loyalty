package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.service.MisionListaUbicacionBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase de recursos http para el manejo de la inclusion y exclusion de
 * ubicaciones en misiones
 *
 * @author svargas
 */
@Api(value = "Mision")
@Path("mision/{idMision}/ubicacion")
public class MisionListaUbicacionResource {

    @EJB
    MisionListaUbicacionBean bean;

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión
    
    @Context
    HttpServletRequest request;

    /**
     * Identificador de mision
     */
    @PathParam("idMision")
    String idMision;

    /**
     * Metodo que obtiene un listado de ubicaciones incluidos, excluidos o
     * disponibles para una mision por su identificador segun el parametro de
     * accion y en un rango de registros segun los parametros de registro y
     * cantidad, de ocurrir un error se retornara una Respuesta con un estado
     * erroneo junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param accion Indicador del tipo de accion incluido/excluido/disponibles
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Obtener lista de ubicaciones de mision",
            responseContainer = "List",
            response = Ubicacion.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUbicaciones(
            @ApiParam(value = "Indicador del tipo de lista de ubicaciones")
            @QueryParam("accion") String accion,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.UBICACIONES_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = bean.getUbicaciones(idMision, accion == null ? null : accion.charAt(0), registro, cantidad,request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }

    /**
     * Metodo que incluye o excluye una lista de identificadores de uubicaciones
     * a una mision por su identificador segun el parametro de accion, de
     * ocurrir un error se retornara una Respuesta con un estado erroneo junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param listaIdUbicacion Lista de identificadores de ubicaciones
     * @param accion Parametro con el indicador de la accion deseada
     * (incluir/excluir)
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asginacion de lista de ubicaciones a mision")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void includeExcludeBatchUbicacion(
            @ApiParam(value = "Identificadores de segmentos", required = true) List<String> listaIdUbicacion,
            @ApiParam(value = "Indicador de tipo de lista a asignar", required = true)
            @QueryParam("accion") String accion) {
        if (accion == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, request.getLocale(), "attributes_invalid", "accion");
        }
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.includeExcludeBatchUbicacion(idMision, listaIdUbicacion, accion.charAt(0), usuario,request.getLocale());
    }

    /**
     * Metodo que elimina la inclusion o exclusion de una lista de
     * identificadores de ubicaciones a una mision por su identificador, de
     * ocurrir un error se retornara una Respuesta con un estado erroneo junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param listaIdUbicacion Lista de identificadores de ubicaciones
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asginacion de lista de ubicaciones a mision")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("remover-lista")
    public void removeBatchUbicacion(
            @ApiParam(value = "Identificadores de segmentos", required = true) List<String> listaIdUbicacion) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.removeBatchUbicacion(idMision, listaIdUbicacion, usuario,request.getLocale());
    }

    /**
     * Metodo que incluye o excluye una ubicacion a una mision por sus
     * identificadores segun el parametro de accion, de ocurrir un error se
     * retornara una Respuesta con un estado erroneo junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param idUbicacion Identificador de la ubicacion
     * @param accion Parametro con el indicador de la accion deseada
     * (incluir/excluir)
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asignacion de ubicacion a mision")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idUbicacion}")
    public void includeExcludeUbicaciones(
            @ApiParam(value = "Identificador de ubicacion", required = true)
            @PathParam("idUbicacion") String idUbicacion,
            @ApiParam(value = "Indicador de tipo de lista a asignar", required = true)
            @QueryParam("accion") String accion) {
        if (accion == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, request.getLocale(), "attributes_invalid", "accion");
        }
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.includeExcludeUbicaciones(idMision, idUbicacion, accion.charAt(0), usuario,request.getLocale());
    }

    /**
     * Metodo que elimina la inclusion o exclusion de una ubicacion a una mision
     * por sus identificadores segun el parametro de accion, de ocurrir un error
     * se retornara una Respuesta con un estado erroneo junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param idUbicacion Identificador de la ubicacion
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asignacion de ubicacion a segmento")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idUbicacion}")
    public void removeUbicacion(
            @ApiParam(value = "Identificador de ubicacion", required = true)
            @PathParam("idUbicacion") String idUbicacion) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.removeUbicacion(idMision, idUbicacion, usuario,request.getLocale());
    }
}
