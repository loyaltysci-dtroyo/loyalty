package com.flecharoja.loyalty.util;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Rol;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author svargas, faguilar
 */
public class MyKeycloakUtilsAdmin implements AutoCloseable {

    private final String CLIENT_SECRET = "f9068861-b0b6-46d0-92ae-7acff94d4ba0";
    private final String CLIENT_ID = "loyalty-api";
    private final String ADMIN_URL = "https://idp-ludis.loyaltysci.com/auth/admin/realms/loyalty-admin";
    private final String TOKEN_ENDPOINT = "https://idp-ludis.loyaltysci.com/auth/realms/loyalty-admin/protocol/openid-connect/token";
    private final String LOGOUT_ENDPOINT = "https://idp-ludis.loyaltysci.com/auth/realms/loyalty-admin/protocol/openid-connect/logout";

    private final JsonObject keycloakSession;

    Locale locale;

    public MyKeycloakUtilsAdmin(Locale locale) {
        this.locale = locale;
        Client client = ClientBuilder.newClient();
        try {
            String body = "client_id=" + CLIENT_ID
                    + "&client_secret=" + CLIENT_SECRET
                    + "&grant_type=client_credentials";
            Response response = client.target(TOKEN_ENDPOINT).request().post(Entity.entity(body, MediaType.APPLICATION_FORM_URLENCODED));
            if (response.getStatus() != 200) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
            this.keycloakSession = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();
        } finally {
            client.close();
        }
    }

    public String createUser(String username, String email, String password) {
        if (username.trim().isEmpty() || password.trim().isEmpty()) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "invalid_username_password");
        }
        Client client = ClientBuilder.newClient();
        try {
            String body = Json.createObjectBuilder()
                    .add("username", username.trim())
                    .add("email", email)
                    .add("enabled", true)
                    .add("credentials", Json.createArrayBuilder()
                            .add(Json.createObjectBuilder()
                                    .add("type", "password")
                                    .add("temporary", false)
                                    .add("value", password.trim())))
                    .build().toString();
            Response response = client.target(ADMIN_URL + "/users").request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).post(Entity.entity(body, MediaType.APPLICATION_JSON));
            if (response.getStatus() == 409 || response.getStatus() == 400) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "invalid_username_password");
            }
            if (response.getStatus() != 201) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
            String[] segments = response.getHeaderString("Location").split("/");
            return segments[segments.length - 1];
        } finally {
            client.close();
        }
    }

    public void deleteUser(String id) {
        Client client = ClientBuilder.newClient();
        try {
            Response response = client.target(ADMIN_URL + "/users/" + id).request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).delete();
            if (response.getStatus() != 204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }

    public void resetPasswordUser(String id, String password) {
        if (password.trim().isEmpty()) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "invalid_username_password");
        }
        Client client = ClientBuilder.newClient();
        try {
            String body = Json.createObjectBuilder()
                    .add("type", "password")
                    .add("temporary", false)
                    .add("value", password.trim())
                    .build().toString();
            Response response = client.target(ADMIN_URL + "/users/" + id+"/reset-password").request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).put(Entity.entity(body, MediaType.APPLICATION_JSON));
            if (response.getStatus() != 204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }

    public void enableDisableUser(String id, boolean action) {
        Client client = ClientBuilder.newClient();
        try {
            String body = Json.createObjectBuilder()
                    .add("id", id)
                    .add("enabled", action)
                    .build().toString();
            Response response = client.target(ADMIN_URL + "/users/" + id).request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).put(Entity.entity(body, MediaType.APPLICATION_JSON));
            if (response.getStatus() != 204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }

    public void editEmail(String id, String email) {
        Client client = ClientBuilder.newClient();
        try {
            String body = Json.createObjectBuilder()
                    .add("id", id)
                    .add("email", email)
                    .build().toString();
            Response response = client.target(ADMIN_URL + "/users/" + id).request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).put(Entity.entity(body, MediaType.APPLICATION_JSON));
            if (response.getStatus() != 204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }

    public String getUsernameUser(String id) {
        Client client = ClientBuilder.newClient();
        try {
            Response response = client.target(ADMIN_URL + "/users/" + id).request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).get();
            if (response.getStatus() != 200) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
            JsonObject data = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();
            return data.getString("username");
        } finally {
            client.close();
        }
    }

    public String getIdUser(String username) {
        Client client = ClientBuilder.newClient();
        try {
            Response response = client.target(ADMIN_URL + "/users").queryParam("username", username).request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).get();
            String idMember = null;
            if (response.getStatus() == 200) {
                for (JsonValue jsonValue : Json.createReader(new StringReader(response.readEntity(String.class))).readArray()) {
                    JsonObject data = (JsonObject) jsonValue;
                    if (data.getString("username").equalsIgnoreCase(username)) {
                        idMember = data.getString("id");
                    }
                }
            }
            return idMember;
        } finally {
            client.close();
        }
    }

    /**
     * ************************************************************************************************************************************
     * ***********************************************************************************************************************
     * ********************************************ROLES***********************************************************
     * **************************************************************************************************************
     * **************************************************************************************************************************************
     */
    /**
     *
     * @param idUsuario
     * @return
     */
    public List<Rol> getRolesUsuario(String idUsuario) {
        Client client = ClientBuilder.newClient();
        try {
            Response response = client.target(ADMIN_URL + "/users/" + idUsuario + "/role-mappings").request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).get();
            if (response.getStatus() != 200) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
            JsonObject data = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();
            try {
                if (data.containsKey("realmMappings")) {
                    return data.getJsonArray("realmMappings").stream().map((value) -> {
                        JsonObject object = (JsonObject) value;
                        return new Rol(object.getString("id"), object.getString("name"), !object.containsKey("description") ? "" : object.getString("description"));
                    }).collect(Collectors.toList());
                } else {
                    return new ArrayList<>();
                }
            } catch (ClassCastException | NullPointerException e) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_processing_json");
            }
        } finally {
            client.close();
        }
    }

    public List<Rol> getRoles() {
        List<Rol> roles = new ArrayList<>();
        Client client = ClientBuilder.newClient();
        try {
            System.out.println("TOKEN: " + keycloakSession.getString("access_token"));
            System.out.println("URL: " + ADMIN_URL + "/roles/");
            Response response = client.target(ADMIN_URL + "/roles/").request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).get();
            if (response.getStatus() != 200) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
            for (JsonValue jsonValue : Json.createReader(new StringReader(response.readEntity(String.class))).readArray()) {
                JsonObject object = (JsonObject) jsonValue;
                roles.add(new Rol(object.getString("id"), object.getString("name"), !object.containsKey("description") ? "" : object.getString("description")));
            }
        } finally {
            client.close();
        }
        return roles;
    }

    public List<Rol> getRolesDisponiblesUsuario(String idUser) {
        List<Rol> roles = new ArrayList<>();
        Client client = ClientBuilder.newClient();
        try {
            Response response = client.target(ADMIN_URL + "/users/" + idUser + "/role-mappings/realm/available").request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).get();
            if (response.getStatus() != 200) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
            for (JsonValue jsonValue : Json.createReader(new StringReader(response.readEntity(String.class))).readArray()) {
                JsonObject object = (JsonObject) jsonValue;
                roles.add(new Rol(object.getString("id"), object.getString("name"), !object.containsKey("description")  ? "" : object.getString("description")));
            }
        } finally {
            client.close();
        }
        return roles;
    }

    public void createRol(String name, String description) {
        if (name.trim().isEmpty() || description.trim().isEmpty()) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "invalid_username_password");
        }
        Client client = ClientBuilder.newClient();
        try {
            String body = Json.createObjectBuilder()
                    .add("name", name)
                    .add("description", description)
                    .build().toString();
            Response response = client.target(ADMIN_URL + "/roles").request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).post(Entity.entity(body, MediaType.APPLICATION_JSON));
            if (response.getStatus() == 409 || response.getStatus() == 400) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "invalid_username_password");
            }
            if (response.getStatus() != 201) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }

    public void editRol(String roleName, String newDescription) {
        Client client = ClientBuilder.newClient();
        try {
            String body = Json.createObjectBuilder()
                    .add("name", roleName)
                    .add("description", newDescription)
                    .build().toString();
            Response response = client.target(ADMIN_URL + "/roles/" + roleName).request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).put(Entity.entity(body, MediaType.APPLICATION_JSON));
            if (response.getStatus() != 204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }

    public void deleteRol(String rolName) {
        Client client = ClientBuilder.newClient();
        try {
            Response response = client.target(ADMIN_URL + "/roles/" + rolName).request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).delete();
            if (response.getStatus() != 204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }

    public Rol getRol(String roleName) {
        Client client = ClientBuilder.newClient();
        Rol rol;
        try {
            Response response = client.target(ADMIN_URL + "/roles/" + roleName).request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).get();
            if (response.getStatus() != 200) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
            JsonObject data = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();
            rol = new Rol(data.getString("id"), data.getString("name"), !data.containsKey("description") ? "" : data.getString("description"));
            return rol;
        } finally {
            client.close();
        }

    }

    /**
     * ************************************************************************************************************************************
     * ***********************************************************************************************************************
     * ********************************************ROL-USUARIO***********************************************************
     * **************************************************************************************************************
     * **************************************************************************************************************************************
     */
    /**
     *
     * @param roles
     * @param idUser
     */
    public void assignRolUser(List<Rol> roles, String idUser) {
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        for (Rol rol : roles) {
            jsonArrayBuilder.add(
                    Json.createObjectBuilder()
                            .add("id", rol.getId())
                            .add("name", rol.getNombre())
            );
        }

        JsonArray rolesJson = jsonArrayBuilder.build();
        Client client = ClientBuilder.newClient();
        try {
            Response response = client.target(ADMIN_URL + "/users/" + idUser + "/role-mappings/realm").request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).post(Entity.entity(rolesJson.toString(), MediaType.APPLICATION_JSON));
            if (response.getStatus() == 409 || response.getStatus() == 400) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, response.readEntity(String.class));
            }
            if (response.getStatus() != 204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }

    public void unassignRolUser(List<Rol> roles, String idUser) {
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        for (Rol rol : roles) {
            jsonArrayBuilder.add(
                    Json.createObjectBuilder()
                            .add("id", rol.getId())
                            .add("name", rol.getNombre())
            );
        }

        JsonArray rolesJson = jsonArrayBuilder.build();

        Client client = ClientBuilder.newClient();
        try {

            Response response = client.target(ADMIN_URL + "/users/" + idUser + "/role-mappings/realm").request().header("Authorization", "bearer " + keycloakSession.getString("access_token")).build("DELETE", Entity.entity(rolesJson.toString(), MediaType.APPLICATION_JSON)).invoke();
            if (response.getStatus() != 204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }

    @Override
    public void close() {
        Client client = ClientBuilder.newClient();
        try {
            String body = "client_id=" + CLIENT_ID
                    + "&client_secret=" + CLIENT_SECRET
                    + "&refresh_token=" + keycloakSession.getString("refresh_token");
            Response response = client.target(LOGOUT_ENDPOINT).request().post(Entity.entity(body, MediaType.APPLICATION_FORM_URLENCODED));
            if (response.getStatus() != 204) {
                throw new MyException(MyException.ErrorSistema.KEYCLOAK_ERROR, locale, "error_connecting_idp");
            }
        } finally {
            client.close();
        }
    }
}
