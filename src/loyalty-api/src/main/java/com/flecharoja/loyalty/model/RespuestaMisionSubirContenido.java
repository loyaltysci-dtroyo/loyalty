package com.flecharoja.loyalty.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaMisionSubirContenido {
    
    private String datosContenido;
    
    private MisionSubirContenido detalleSubirContenido;

    public RespuestaMisionSubirContenido() {
    }

    public String getDatosContenido() {
        return datosContenido;
    }

    public void setDatosContenido(String datosContenido) {
        this.datosContenido = datosContenido;
    }

    public MisionSubirContenido getDetalleSubirContenido() {
        return detalleSubirContenido;
    }

    public void setDetalleSubirContenido(MisionSubirContenido detalleSubirContenido) {
        this.detalleSubirContenido = detalleSubirContenido;
    }
}
