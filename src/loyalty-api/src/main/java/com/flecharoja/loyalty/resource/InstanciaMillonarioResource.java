/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.service.InstanciaMillonarioBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * Clase que provee de metodos http RESTful para el acceso a funcionalidades del
 * manejo de grupos
 *
 * @author wtencio
 */
@Api(value = "instancia millonario")
@Path("instancia-millonario")
public class InstanciaMillonarioResource {

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @EJB
    InstanciaMillonarioBean instanciaMillonarioBean;

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion
    
    @Context
    HttpServletRequest request;

    /**
     * Metodo que acciona una tarea, la cual inserta un nuevo grupo dentro de la
     * base de datos, en el caso de error al ejecutar la tarea se retorna un 500
     * (error interno del servidor) con un mensaje en el encabezado, de no ser
     * asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param idJuegoMillonario
     * @param idMision
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Registrar una instancia del juego millonario",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String insertInstanciaMillonario(
            @ApiParam(value = "idJuegoMillonario") @QueryParam("idJuegoMillonario") String idJuegoMillonario,
            @ApiParam(value = "idMision") @QueryParam("idMision") String idMision
    ) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return instanciaMillonarioBean.insertInstanciaMillonario(idJuegoMillonario, idMision, usuarioContext,request.getLocale());
    }
}
