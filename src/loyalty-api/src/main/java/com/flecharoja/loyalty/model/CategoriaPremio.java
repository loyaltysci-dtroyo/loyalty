/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CATEGORIA_PREMIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoriaPremio.findAll", query = "SELECT c FROM CategoriaPremio c ORDER BY c.fechaModificacion DESC"),
    @NamedQuery(name = "CategoriaPremio.findByIdCategoria", query = "SELECT c FROM CategoriaPremio c WHERE c.idCategoria = :idCategoria"),
    @NamedQuery(name = "CategoriaPremio.countAll", query = "SELECT COUNT(c.idCategoria) FROM CategoriaPremio c"),
    @NamedQuery(name = "CategoriaPremio.findByNombre", query = "SELECT c FROM CategoriaPremio c WHERE c.nombre = :nombre ORDER BY c.fechaModificacion DESC"),
    @NamedQuery(name = "CategoriaPremio.findByImagen", query = "SELECT c FROM CategoriaPremio c WHERE c.imagen = :imagen"),
    @NamedQuery(name = "CategoriaPremio.countByNombreInterno", query = "SELECT COUNT(c.idCategoria) FROM CategoriaPremio c WHERE c.nombreInterno = :nombreInterno")})
public class CategoriaPremio implements Serializable {

   

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "categpremio_uuid")
    @GenericGenerator(name = "categpremio_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_CATEGORIA")
    private String idCategoria;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    private String imagen;
    
    @Size(min = 1, max = 200)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoriaPremio")
    private List<PremioCategoriaPremio> premioCategoriaPremioList;

    public CategoriaPremio() {
    }

    public CategoriaPremio(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public CategoriaPremio(String idCategoria, String nombre, String imagen, String descripcion, String usuarioCreacion, Date fechaCreacion, String usuarioModificacion, Date fechaModificacion, Long numVersion) {
        this.idCategoria = idCategoria;
        this.nombre = nombre;
        this.imagen = imagen;
        this.descripcion = descripcion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaCreacion = fechaCreacion;
        this.usuarioModificacion = usuarioModificacion;
        this.fechaModificacion = fechaModificacion;
        this.numVersion = numVersion;
    }

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @XmlTransient
    public List<PremioCategoriaPremio> getPremioCategoriaPremioList() {
        return premioCategoriaPremioList;
    }

    public void setPremioCategoriaPremioList(List<PremioCategoriaPremio> premioCategoriaPremioList) {
        this.premioCategoriaPremioList = premioCategoriaPremioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCategoria != null ? idCategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaPremio)) {
            return false;
        }
        CategoriaPremio other = (CategoriaPremio) object;
        if ((this.idCategoria == null && other.idCategoria != null) || (this.idCategoria != null && !this.idCategoria.equals(other.idCategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CategoriaPremio[ idCategoria=" + idCategoria + " ]";
    }

    

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }
    
}
