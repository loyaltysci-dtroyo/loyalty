/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.DataImportacionMiembroExtendido;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.TrabajosInternos;
import java.util.Locale;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author wtencio, svargas
 */
@Stateless
public class ImportarExportarTrabajoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    ExportarBean exportarBean;
    
    @EJB
    ImportarBean importarBean;
    
    @EJB
    TrabajoBean trabajoBean;

    /**
     * detone de trabajo de exportacion de miembros de un segmento
     * 
     * @param idSegmento id de segmento
     * @param locale locale
     * @return id de trabajo asignado
     */
    public String exportarTrabajo(String idSegmento, Locale locale) {
        String idTrabajo = trabajoBean.crearTrabajo(TrabajosInternos.Tipo.EXPORTACION.getValue());
        exportarBean.exportarMiembrosSegmentosCSV(idTrabajo, idSegmento,locale);
        return idTrabajo;
    }
    
    /**
     * detone de trabajo de exportacion de miembros
     * 
     * @param locale locale
     * @return id de trabajo asignado
     */
    public String exportarMiembros(Locale locale) {
        String idTrabajo = trabajoBean.crearTrabajo(TrabajosInternos.Tipo.EXPORTACION.getValue());
        exportarBean.exportarMiembrosCSV(idTrabajo,locale);
        return idTrabajo;
    }
    
    /**
     * detone de trabajo de importacion de miembros a segmento
     * 
     * @param base64 data
     * @param idSegmento id de segmento
     * @param atributo id atributo a identificar miembros
     * @param accion accion a realizar
     * @param usuario id admin
     * @param locale locale
     * @return id de trabajo asignado
     */
    public String importarMiembroSegmento(String base64, String idSegmento, String atributo, String accion,String usuario, Locale locale){
        String idTrabajo = trabajoBean.crearTrabajo(TrabajosInternos.Tipo.IMPORTACION.getValue());
        importarBean.importarMiembroSegmento(base64, Miembro.Atributos.get(atributo),idSegmento, idTrabajo,accion.charAt(0), usuario,locale);
        return idTrabajo;
    }
    
    /**
     * detone de trabajo de importacion de miembros
     * 
     * @param base64 data
     * @param usuario id admin
     * @param locale locale
     * @return id de trabajo asignado
     */
    public String importarMiembro(String base64,String usuario, Locale locale){
        String idTrabajo = trabajoBean.crearTrabajo(TrabajosInternos.Tipo.IMPORTACION.getValue());
        importarBean.importarMiembro(base64, idTrabajo, usuario, locale);
        return idTrabajo;
    }
    
    /**
     * Creacion de trabajo interno sobre la importacion de miembro y asignacion
     * del mismo al detone del trabajo async
     * 
     * @param data Datos para el trabajo de importacion
     * @param usuario Identificador del usuario administrativo detonador del trabajo
     * @param locale Locale de la peticion
     * @param doPostInsertActions Indicador sobre si se realiza acciones post
     * registro (bonus de bienvenida, email de bienvenida con credenciales, etc)
     * @return Identificador del trabajo interno asignado
     */
    public String importarMiembro(DataImportacionMiembroExtendido data, String usuario, Locale locale, boolean doPostInsertActions){
        String idTrabajo = trabajoBean.crearTrabajo(TrabajosInternos.Tipo.IMPORTACION.getValue());
        importarBean.importarMiembro(data, idTrabajo, usuario, locale, doPostInsertActions);
        return idTrabajo;
    }
    
    /**
     * detone de trabajo de actualizacion de informacion de miembros
     * 
     * @param base64 data
     * @param idAtributo id de atributo
     * @param tipoAtributo indicador de tipo de atributo
     * @param tipoIdentificacion tipo de identificacion
     * @param usuario id admin
     * @param locale locale
     * @return id de trabajo asignado
     */
    public String importarInfoMiembro(String base64, String idAtributo, String tipoAtributo,String tipoIdentificacion,String usuario, Locale locale){
        String idTrabajo = trabajoBean.crearTrabajo(TrabajosInternos.Tipo.IMPORTACION.getValue());
        importarBean.actualizaInfoMiembro(base64, idAtributo, tipoAtributo.charAt(0),tipoIdentificacion, idTrabajo, usuario, locale);
        return idTrabajo;
    }
    
    /**
     * detone de trabajo de importacion de certificados
     * 
     * @param base64 data
     * @param idPremio id de premio
     * @param usuario id admin
     * @param locale locale
     * @return id de trabajo asignado
     */
    public String importarCertificados(String base64, String idPremio,String usuario, Locale locale){
        String idTrabajo = trabajoBean.crearTrabajo(TrabajosInternos.Tipo.IMPORTACION.getValue());
        importarBean.importarCodigoCertificado(base64, idPremio, idTrabajo, usuario, locale);
        return idTrabajo;
    }
    
    /**
     * detone de trabajo de importacion de certificados para una categoria
     * 
     * @param base64 data
     * @param idCategoriaProducto id de la categoria del producto
     * @param usuario id admin
     * @param locale locale
     * @return id de trabajo asignado
     */
    public String importarCertificadosCategoriaProducto(String base64, String idCategoriaProducto,String usuario, Locale locale){
        String idTrabajo = trabajoBean.crearTrabajo(TrabajosInternos.Tipo.IMPORTACION.getValue());
        importarBean.importarCertificadoCategoriaProducto(base64, idCategoriaProducto, idTrabajo, usuario, locale);
        return idTrabajo;
    }
}
