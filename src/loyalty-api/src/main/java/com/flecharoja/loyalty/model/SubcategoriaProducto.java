/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "SUBCATEGORIA_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SubcategoriaProducto.findAll", query = "SELECT s FROM SubcategoriaProducto s WHERE s.idCategoria.idCategoria = :idCategoria"),
    @NamedQuery(name = "SubcategoriaProducto.findDisponibleProducto", query = "SELECT s FROM SubcategoriaProducto s WHERE s.idSubcategoria NOT IN :lista AND s.idCategoria.idCategoria = :idCategoria"),
    @NamedQuery(name = "SubcategoriaProducto.findByIdSubcategoria", query = "SELECT s FROM SubcategoriaProducto s WHERE s.idSubcategoria = :idSubcategoria"),
    @NamedQuery(name = "SubcategoriaProducto.countByNombreInterno", query = "SELECT COUNT(c.idSubcategoria) FROM SubcategoriaProducto c WHERE c.nombreInterno = :nombreInterno"),
    @NamedQuery(name = "SubcategoriaProducto.countAll", query = "SELECT COUNT(c.idSubcategoria) FROM SubcategoriaProducto c WHERE c.idCategoria.idCategoria = :idCategoria"),
})
public class SubcategoriaProducto implements Serializable {

    @Version
    @Basic(optional = false)
    @NotNull()
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "subcategoria_producto_uuid")
    @GenericGenerator(name = "subcategoria_producto_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_SUBCATEGORIA")
    private String idSubcategoria;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;
    
    @Size(max = 300)
    @Column(name = "IMAGEN")
    private String imagen;
    
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    
    @JoinColumn(name = "ID_CATEGORIA", referencedColumnName = "ID_CATEGORIA")
    @ManyToOne(optional = false)
    private CategoriaProducto idCategoria;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "subcategoriaProducto")
    private List<ProductoCategSubcateg> productoCategSubcategList;

    public SubcategoriaProducto() {
    }

    public SubcategoriaProducto(String idSubcategoria) {
        this.idSubcategoria = idSubcategoria;
    }

    public SubcategoriaProducto(String idSubcategoria, String nombre, String nombreInterno, Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.idSubcategoria = idSubcategoria;
        this.nombre = nombre;
        this.nombreInterno = nombreInterno;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public String getIdSubcategoria() {
        return idSubcategoria;
    }

    public void setIdSubcategoria(String idSubcategoria) {
        this.idSubcategoria = idSubcategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public CategoriaProducto getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(CategoriaProducto idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ProductoCategSubcateg> getProductoCategSubcategList() {
        return productoCategSubcategList;
    }

    public void setProductoCategSubcategList(List<ProductoCategSubcateg> productoCategSubcategList) {
        this.productoCategSubcategList = productoCategSubcategList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSubcategoria != null ? idSubcategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubcategoriaProducto)) {
            return false;
        }
        SubcategoriaProducto other = (SubcategoriaProducto) object;
        if ((this.idSubcategoria == null && other.idSubcategoria != null) || (this.idSubcategoria != null && !this.idSubcategoria.equals(other.idSubcategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SubcategoriaProducto{" + "numVersion=" + numVersion + ", idSubcategoria=" + idSubcategoria + ", nombre=" + nombre + ", nombreInterno=" + nombreInterno + ", imagen=" + imagen + ", descripcion=" + descripcion + ", fechaCreacion=" + fechaCreacion + ", usuarioCreacion=" + usuarioCreacion + ", fechaModificacion=" + fechaModificacion + ", usuarioModificacion=" + usuarioModificacion + ", idCategoria=" + idCategoria + '}';
    }

    

   

   
}
