package com.flecharoja.loyalty.exception;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.ejb.ApplicationException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 *
 * @author svargas
 */
@ApplicationException(rollback = true)
public class MyException extends WebApplicationException {
    
    public enum ErrorPeticion {
        ARGUMENTOS_ERRONEOS(100),//400
        ARGUMENTOS_PAGINACION_ERRONEOS(101),//400
        ENTIDAD_NO_VALIDA(104),//400
        IMAGEN_NO_VALIDA(106),//400
        ATRIBUTO_CUERPO_INVALIDO(110);

        private static final Response.Status status = Response.Status.BAD_REQUEST;
        private final int value;

        private ErrorPeticion(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static final Response.Status getStatus() {
            return status;
        }
    }
    public enum ErrorAutenticacion {
        ERROR_AUTENTICACION(107);//401

        private static final Response.Status status = Response.Status.UNAUTHORIZED;
        private final int value;

        private ErrorAutenticacion(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Response.Status getStatus() {
            return status;
        }
    }
    public enum ErrorAutorizacion {
        OPERACION_NO_PERMITIDA(105),//403
        SIN_AUTORIZACION(108);//403//403

        private static final Response.Status status = Response.Status.FORBIDDEN;
        private final int value;

        private ErrorAutorizacion(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Response.Status getStatus() {
            return status;
        }
    }
    public enum ErrorRecursoNoEncontrado {
        ENTIDAD_NO_EXISTENTE(102);//404//404

        private static final Response.Status status = Response.Status.NOT_FOUND;
        private final int value;

        private ErrorRecursoNoEncontrado(int value) {
            this.value = value;
        }

        public static Response.Status getStatus() {
            return status;
        }

        public int getValue() {
            return value;
        }
    }
    public enum ErrorSistema {
         KEYCLOAK_ERROR(109),
        TIEMPO_CONEXION_DB_AGOTADO(200),
        ENTIDAD_EXISTENTE(201),
        PROBLEMAS_HBASE(202),
        OPERACION_FALLIDA(203),
        ERROR_DESCONOCIDO(300);

        private final int value;
        private final static Response.Status status = Response.Status.INTERNAL_SERVER_ERROR;

        private ErrorSistema(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Response.Status getStatus() {
            return status;
        }
    }
    
    public static final String ENCABEZADO_CODIGO_ERROR = "Error-Reason";
    
    private static final String BUNDLE_ERRORS_MESSAGES = "messages.error";
    private static final String BUNDLE_PARAM_NAMES = "names.param";
    
    public MyException(ErrorPeticion codigo, Locale locale, String key, Object... args) {
        super(Response.status(ErrorPeticion.getStatus())
                .entity(new MyExceptionResponseBody(codigo.getValue(), getMessageString(key, locale, args), getLocaleLang(locale)))
                .build()
        );
    }
    
    public MyException(ErrorAutenticacion codigo, Locale locale, String key, Object... args) {
        super(Response.status(ErrorAutenticacion.getStatus())
                .entity(new MyExceptionResponseBody(codigo.getValue(), getMessageString(key, locale, args), getLocaleLang(locale)))
                .build()
        );
    }
    
    public MyException(ErrorAutorizacion codigo, Locale locale, String key, Object... args) {
        super(Response.status(ErrorAutorizacion.getStatus())
                .entity(new MyExceptionResponseBody(codigo.getValue(), getMessageString(key, locale, args), getLocaleLang(locale)))
                .build()
        );
    }
    
    public MyException(ErrorRecursoNoEncontrado codigo, Locale locale, String key, Object... args) {
        super(Response.status(ErrorRecursoNoEncontrado.getStatus())
                .entity(new MyExceptionResponseBody(codigo.getValue(), getMessageString(key, locale, args), getLocaleLang(locale)))
                .build()
        );
    }
    
    public MyException(ErrorSistema codigo, Locale locale, String key, Object... args) {
        super(Response.status(ErrorSistema.getStatus())
                .entity(new MyExceptionResponseBody(codigo.getValue(), getMessageString(key, locale, args), getLocaleLang(locale)))
                .build()
        );
    }
    
    private static String getMessageString(String key, Locale locale, Object... params) {
        try {
            if (params.length>0) {
                return MessageFormat.format(ResourceBundle.getBundle(BUNDLE_ERRORS_MESSAGES, locale).getString(key), getParamsString(locale, params));
            } else {
                return ResourceBundle.getBundle(BUNDLE_ERRORS_MESSAGES, locale).getString(key);
            }
        } catch (MissingResourceException | IllegalArgumentException e) {
            if (!locale.getLanguage().equals(Locale.US.getLanguage())) {
                return getMessageString(key, Locale.US, getParamsString(locale, params));
            }
            return '!'+key+'!';
        }
    }
    
    private static Object[] getParamsString(Locale locale, Object... params) {
        return Arrays.stream(params).map((param) -> {
            try {
                return ResourceBundle.getBundle(BUNDLE_PARAM_NAMES, locale).getString((String) param);
            } catch (MissingResourceException | IllegalArgumentException e) {
                try {
                    return ResourceBundle.getBundle(BUNDLE_PARAM_NAMES, Locale.US).getString((String) param);
                } catch (MissingResourceException | IllegalArgumentException ex) {
                    return param;
                }
            }
        }).toArray();
    }
    
    private static String getLocaleLang(Locale locale) {
        String lang;
        try {
            lang = ResourceBundle.getBundle(BUNDLE_ERRORS_MESSAGES, locale).getLocale().getLanguage();
        } catch (MissingResourceException e) {
            lang = Locale.US.getLanguage();
        }
        return lang;
    }
}
