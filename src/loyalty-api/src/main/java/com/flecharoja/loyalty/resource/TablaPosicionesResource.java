/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.TablaPosiciones;
import com.flecharoja.loyalty.service.TablaPosicionesBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase que provee de metodos http RESTful para el acceso a funcionalidades del
 * manejo de tabla de posiciones
 *
 * @author wtencio
 */
@Api(value = "Leaderboards")
@Path("tablaposiciones")
public class TablaPosicionesResource {

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @EJB
    TablaPosicionesBean tablaPosicionesBean;//EJB con los metodos de negocio para el manejo de tabla de posiciones

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion
    
    @Context
    HttpServletRequest request;

   

    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de tablas de
     * posiciones por su tipo (grupal o por usuario) en el caso de error al
     * ejecutar la tarea se retorna un 500 (error interno del servidor) con un
     * mensaje en el encabezado, de no ser asi se retorna un 200 (OK) con un
     * resultado de ser requerido este.
     *
     * @param tipo indicador que indica el tipo de tabla que desea buscar
     * @param cantidad Parametro opcional con un valor por defecto de 10 que define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer registro desde donde devolver el listado de entidades
     * @return Lista de tablas de ubicaciones en formato JSON
     */
    @ApiOperation(value = "Obtener tabla de posiciones por su tipo, por defecto todos",
            responseContainer = "List",
            response = TablaPosiciones.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTablasPosiciones(
            @ApiParam(value = "Indicador del tipo")
            @DefaultValue("null") @QueryParam("tipo") String tipo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.LEADERBOARDS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = tablaPosicionesBean.getTablasPosiciones(tipo,registro, cantidad, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Metodo que acciona una tarea, la cual obtiene una tabla de posiciones
     * especifica por su id que ingresa como parametro en el caso de error al
     * ejecutar la tarea se retorna un 500 (error interno del servidor) con un
     * mensaje en el encabezado, de no ser asi se retorna un 200 (OK) con un
     * resultado de ser requerido este.
     *
     * @param idTabla identificador unico de la tabla de posiciones
     * @return objeto tipo tabla de posiciones en formato JSON
     */
    @ApiOperation(value = "Obtener un grupo por su identificador",
            response = TablaPosiciones.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idTabla}")
    @Produces(MediaType.APPLICATION_JSON)
    public TablaPosiciones getTablasPosicionesPorId(
            @ApiParam(value = "Identificador de la tabla", required = true)
            @PathParam("idTabla") String idTabla) {
        
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.LEADERBOARDS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return tablaPosicionesBean.getTablaPorId(idTabla, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de tablas de
     * posiciones según el parametro de busqueda (palabras clave) en el caso de
     * error al ejecutar la tarea se retorna un 500 (error interno del servidor)
     * con un mensaje en el encabezado, de no ser asi se retorna un 200 (OK) con
     * un resultado de ser requerido este.
     *
     * @param busqueda cadena de texto conformada por las palabras clave
     * @param tipo indicadores opcionales del tipo de tabla a buscar
     * @param cantidad Parametro opcional con un valor por defecto de 10 que define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer registro desde donde devolver el listado de entidades
     * @return Lista de ubicaciones en formato JSON
     */
    @ApiOperation(value = "Buscar tablas de posiciones",
            responseContainer = "List",
            response = TablaPosiciones.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("buscar/{busqueda}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchTablaPosiciones(
            @ApiParam(value = "Palabra de búsqueda")
            @PathParam("busqueda") String busqueda, 
            @ApiParam(value = "Indicador del tipo de grupo por el que se desea filtrar")
            @DefaultValue("null") @QueryParam("tipo") String tipo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
       
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.LEADERBOARDS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = tablaPosicionesBean.searchTablas(busqueda,tipo,registro, cantidad, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Redireccion del metodo buscar sin ninguna cadena de texto a metodo
     * obtener grupos
     *
     * @param tipo Indicadores opcionales de tipos de tabla deseados
     * @param cantidad Parametro opcional con un valor por defecto de 10 que define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer registro desde donde devolver el listado de entidades
     * @return Respuesta con el estado de la operacion
     */
    
    @ApiOperation(value = "Buscar tablas de posiciones",
            responseContainer = "List",
            response = TablaPosiciones.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("buscar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchTablaPosiciones(
            @ApiParam(value = "Indicador del tipo de grupo por el que se desea filtrar")
            @DefaultValue("null") @QueryParam("tipo") String tipo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro
            ) {
        return this.getTablasPosiciones(tipo, cantidad, registro);
    }

    /**
     * Metodo que acciona una tarea, la cual registra una nueva tabla de
     * posiciones en el caso de error al ejecutar la tarea se retorna un 500
     * (error interno del servidor) con un mensaje en el encabezado, de no ser
     * asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param tablaPosiciones objeto con la información a registrar
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Registrar una nueva tabla de posiciones")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void insertTablaPosiciones(  
            @ApiParam(value = "Información de la tabla de posiciones", required = true)
            TablaPosiciones tablaPosiciones) {
        
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.LEADERBOARDS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        tablaPosicionesBean.insertTablaPosiciones(tablaPosiciones, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual actualiza la informacion de una
     * tabla de posiciones en el caso de error al ejecutar la tarea se retorna
     * un 500 (error interno del servidor) con un mensaje en el encabezado, de
     * no ser asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param tablaPosiciones objeto con la información actualizada
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Modificación de una tabla de posiciones")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateTablaPosiciones(
            @ApiParam(value = "Información de la tabla de posiciones", required = true)
            TablaPosiciones tablaPosiciones) {
       String usuarioContext;
       String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.LEADERBOARDS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        tablaPosicionesBean.updateTablaPosiciones(tablaPosiciones, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual elimina una tabla de posiciones en
     * el caso de error al ejecutar la tarea se retorna un 500 (error interno
     * del servidor) con un mensaje en el encabezado, de no ser asi se retorna
     * un 200 (OK) con un resultado de ser requerido este.
     *
     * @param idTabla identificador unico de la tabla de posiciones
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Eliminación de una tabla de posiciones")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idTabla}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteTablaPosiciones(
            @ApiParam(value = "Identidicador de la tabla de posiciones", required = true)
            @PathParam("idTabla") String idTabla) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.LEADERBOARDS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        tablaPosicionesBean.removeTablaPosiciones(idTabla, usuarioContext, request.getLocale());
    }

}
