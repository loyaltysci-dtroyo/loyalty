package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CategoriaPromocion;
import com.flecharoja.loyalty.model.PromocionCategoriaPromo;
import com.flecharoja.loyalty.model.PromocionCategoriaPromoPK;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

/**
 * Clase encargada de proveer metodos para el mantenimiento de listados y
 * asignaciones de promociones con categorias de promocion asi como
 * verificaciones y manejo de datos
 *
 * @author svargas
 */
@Stateless
public class PromocionCategoriaPromocionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene un listado de promociones que esten ligados a una
     * categoria de promocion, especificado por su identificador y en un rango
     * definido por sus parametros
     *
     * @param idCategoria Identificador de la categoria
     * @param registro Parametro que indica el primer resultado desde donde
     * retornar los resultados
     * @param cantidad Parametro que indica la cantidad de resultados en la
     * lista
     * @param locale
     * @return Respuesta con la lista encapsulada y encabezados acerca del rango
     * de respuesta y su total y rango maximo aceptado o estado erroneo y
     * encabezado con el codigo de error
     */
    public Map<String, Object> getPromocionesPorIdCategoria(String idCategoria, int registro, int cantidad, Locale locale) {
        //verificacion de que la entidad exista
        try {
            em.getReference(CategoriaPromocion.class, idCategoria).getIdCategoria();
        } catch (EntityNotFoundException e) {
             throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_category_not_found");
        }

        //verifica que la cantidad en la peticion sea menor a la aceptada, de no ser asi se retorna una respuesta informando acerca de ello, junto con el encabezado de rango maximo
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        Map<String, Object> respuesta = new HashMap<>();

        Long total;
        List resultado;
        try {
            //se obtiene el total de registros
            total = ((Long) em.createNamedQuery("PromocionCategoriaPromo.countByIdCategoria")
                    .setParameter("idCategoria", idCategoria)
                    .getSingleResult());
            total -= total - 1 < 0 ? 0 : 1;

            //ve que si en el caso que el registro sea mayor que el total, este se reduce por cantidad (mientras que no sea <0) hasta que este entre un valor de 0 a total
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            //se obtiene el listado usando los parametros de registro y cantidad para limitar el rango de respuesta
            resultado = em.createNamedQuery("PromocionCategoriaPromo.findPromocionesByIdCategoria")
                    .setParameter("idCategoria", idCategoria)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();
        } catch (IllegalArgumentException e) {//en el caso que los valores de paginacion esten erroneos
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        //se establece la respuesta encapsulando el listado y estableciendo los encabezados de rango de la respuesta y rango aceptado
        respuesta.put("resultado", resultado);
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        
        return respuesta;
    }

    /**
     * Metodo que obtiene un listado de categorias de promocion que esten
     * ligados a una promocion, especificado por su identificador y en un rango
     * definido por sus parametros
     *
     * @param idPromocion Identificador de la promocion
     * @param registro Parametro que indica el primer resultado desde donde
     * retornar los resultados
     * @param cantidad Parametro que indica la cantidad de resultados en la
     * lista
     * @param locale
     * @return Respuesta con la lista encapsulada y encabezados acerca del rango
     * de respuesta y su total y rango maximo aceptado o estado erroneo y
     * encabezado con el codigo de error
     */
    public Map<String, Object> getCategoriasPorIdPromocion(String idPromocion, int registro, int cantidad, Locale locale) {
        //verificacion de que la entidad exista
        try {
            em.getReference(Promocion.class, idPromocion).getIdPromocion();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_category_not_found");
        }

        //verifica que la cantidad en la peticion sea menor a la aceptada, de no ser asi se retorna una respuesta informando acerca de ello, junto con el encabezado de rango maximo
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        Map<String, Object> respuesta = new HashMap<>();

        Long total;
        List resultado;
        try {
            //se obtiene el total de registros
            total = ((Long) em.createNamedQuery("PromocionCategoriaPromo.countByIdPromocion")
                    .setParameter("idPromocion", idPromocion)
                    .getSingleResult());
            total -= total - 1 < 0 ? 0 : 1;

            //ve que si en el caso que el registro sea mayor que el total, este se reduce por cantidad (mientras que no sea <0) hasta que este entre un valor de 0 a total
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            //se obtiene el listado usando los parametros de registro y cantidad para limitar el rango de respuesta
            resultado = em.createNamedQuery("PromocionCategoriaPromo.findCategoriasByIdPromocion")
                    .setParameter("idPromocion", idPromocion)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();
        } catch (IllegalArgumentException e) {//en el caso que los valores de paginacion esten erroneos
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        //se establece la respuesta encapsulando el listado y estableciendo los encabezados de rango de la respuesta y rango aceptado
        respuesta.put("resultado", resultado);
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        
        return respuesta;
    }

    /**
     * Metodo que asigna a una categoria una promocion, usando los
     * identificadores de los mismos
     *
     * @param idCategoria Identificador de la categoria de promocion
     * @param idPromocion Identificador de la promocion
     * @param usuario usuario en sesión
     * @param locale
     */
    public void assignCategoriaPromocion(String idCategoria, String idPromocion, String usuario, Locale locale) {
        //se verifica que tanto la categoria como la promocion existan
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        try {
            em.getReference(CategoriaPromocion.class, idCategoria).getIdCategoria();
        } catch (EntityNotFoundException e) {
           throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_category_not_found");
        }

        //verifica que la promocion a asignar a la categoria, no tenga un estado de borrador, ni archivado
        if (promocion.getIndEstado().compareTo(Promocion.Estados.ARCHIVADO.getValue()) == 0) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Promo");
        }

        //fecha de creacion
        Date date = Calendar.getInstance().getTime();

        try {
            em.persist(new PromocionCategoriaPromo(idCategoria, idPromocion, date, usuario));
            em.flush();
        } catch (EntityExistsException e) {//en el caso de que ya exista la entidad en la bases de datos
           throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "promo_category_promo_already_exist");
        }

        bitacoraBean.logAccion(PromocionCategoriaPromo.class, idCategoria + "/" + idPromocion, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
    }

    /**
     * Metodo que elimina la asignacion de una promocion a una categoria de
     * promocion por sus identificadores
     *
     * @param idCategoria Identificador de la categoria
     * @param idPromocion Identificador de la promocion
     * @param usuario usuario en sesion
     * @param locale
     */
    public void unassignCategoriaPromocion(String idCategoria, String idPromocion, String usuario,Locale locale) {
        try {
            //se intenta remover la entidad a partir de la referencia de la misma por sus identificadores
            em.remove(em.getReference(PromocionCategoriaPromo.class, new PromocionCategoriaPromoPK(idCategoria, idPromocion)));
            em.flush();
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_category_promo_not_found");
        }

        bitacoraBean.logAccion(PromocionCategoriaPromo.class, idCategoria + "/" + idPromocion, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }

    /**
     * Metodo que asigna a una categoria una lista de promociones, usando los
     * identificadores de los mismos
     *
     * @param idCategoria Identificador de la categoria de promocion
     * @param listaIdPromocion Lista de identificadores de promocion
     * @param usuario usuario en sesión
     * @param locale
     */
    public void assignBatchCategoriaPromocion(String idCategoria, List<String> listaIdPromocion, String usuario, Locale locale) {
        //se verifica que la categoria exista
        try {
            em.getReference(CategoriaPromocion.class, idCategoria).getIdCategoria();
        } catch (EntityNotFoundException e) {
             throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_category_not_found");
        }

        for (String idPromocion : listaIdPromocion) {
            //se verifica que la promocion exista
            Promocion promocion = em.find(Promocion.class, idPromocion);
            if (promocion == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
            }
            //verifica que la promocion a asignar a la categoria, no tenga un estado de borrador, ni archivado
            if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Promo");
            }
        }

        //fecha de creacion
        Date date = Calendar.getInstance().getTime();

        try {
            listaIdPromocion.stream().forEach((idPromocion) -> {
                em.persist(new PromocionCategoriaPromo(idCategoria, idPromocion, date, usuario));
            });
            em.flush();
        } catch (EntityExistsException e) {//en el caso de que ya exista la entidad en la bases de datos
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "promo_category_promo_already_exist");
        }

        listaIdPromocion.forEach((idPromocion) -> {
            bitacoraBean.logAccion(PromocionCategoriaPromo.class, idCategoria + "/" + idPromocion, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        });
    }

    /**
     * Metodo que elimina la asignacion de una lista de promociones a una
     * categoria de promocion por sus identificadores
     *
     * @param idCategoria Identificador de la categoria
     * @param listaIdPromocion Lista de identificadores de promocion
     * @param usuario usuario en sesion
     * @param locale
     */
    public void unassignBatchCategoriaPromocion(String idCategoria, List<String> listaIdPromocion, String usuario,Locale locale) {
        try {
            listaIdPromocion.stream().forEach((idPromocion) -> {
                //se intenta remover la entidad a partir de la referencia de la misma por sus identificadores
                em.remove(em.getReference(PromocionCategoriaPromo.class, new PromocionCategoriaPromoPK(idCategoria, idPromocion)));
            });
            em.flush();
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_category_promo_not_found");
        }

        listaIdPromocion.forEach((idPromocion) -> {
            bitacoraBean.logAccion(PromocionCategoriaPromo.class, idCategoria + "/" + idPromocion, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        });
    }
}
