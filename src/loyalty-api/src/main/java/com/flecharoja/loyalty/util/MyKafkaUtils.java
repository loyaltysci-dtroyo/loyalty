package com.flecharoja.loyalty.util;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author svargas
 */
@Stateless
public class MyKafkaUtils {
    
    public enum EventosSistema {
        ENTRA_GRUPO(1),
        SALE_GRUPO(2),
        ASIGNACION_INSIGNIA(3),
        DESASIGNACION_INSIGNIA(4),
        ASIGNACION_TABPOS(5),
        DESASIGNACION_TABPOS(6),
        GANA_METRICA(7),
        ALCANZA_NIVEL_METRICA(10),
        MIEMBRO_DEJA_NIVEL_METRICA(11),
        APROBO_MISION(13),
        REPROBO_MISION(14),
        RECIBIO_PREMIO(17),
        REMIDIO_PREMIO(18),
        COMPRO_ALGUN_PRODUCTO(19),
        CUMPLEANOS(23),
        REGISTRO_MIEMBRO(24),
        COMPRA_PRODUCTO_X(31),
        ANIVERSARIO_PROGRAMA(32),;

        private final int value;

        private EventosSistema(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    private KafkaProducer<Object, Object> producer;
    
    private static int id = 0;

    @PostConstruct
    private void init() {
        try {
            Properties properties = new Properties();
            properties.put("client.id", "producer-"+id++);
            properties.load(MyKafkaUtils.class.getResourceAsStream("/conf/kafka/kafka-producer.properties"));
            producer = new KafkaProducer<>(properties);
        } catch (IOException ex) {
            Logger.getLogger(MyKafkaUtils.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }
    
    @Asynchronous
    public void sendRequestRecalculoRegla(String idRegla, ReglasSegmento.TiposReglasSegmento tipo) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("indTipoRegla", tipo.getValue());
        builder.add("idRegla", idRegla);
        producer.send(new ProducerRecord<>("calculo-reglas", builder.build().toString()));
    }
    
    @Asynchronous
    public void sendRequestActualizacionSegmento(String idSegmento) {
        producer.send(new ProducerRecord<>("actualizar-segmentos", idSegmento));
    }
    
    @Asynchronous
    public void notifyEvento (EventosSistema evento, String idElemento, String idMiembro) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("indEvento", evento.getValue());
        if (idElemento != null) {
            builder.add("idElemento", idElemento);
        }
        builder.add("idMiembro", idMiembro);
        producer.send(new ProducerRecord<>("eventos-sistema", builder.build().toString()));
    }
    
    @PreDestroy
    private void preDestroy() {
        producer.close();
    }
}
