/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "TRANS_CARRITO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TransCarrito.findAll", query = "SELECT t FROM TransCarrito t"),
    @NamedQuery(name = "TransCarrito.findByIdTransCarrito", query = "SELECT t FROM TransCarrito t WHERE t.numTransaccion= :numTransaccion")
})
public class TransCarrito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "NUM_TRANSACCION")
    @ApiModelProperty(value = "Identificador único del carrito", required = true)
    private String numTransaccion;
    
    @Column(name = "FECHA_ARCHIVADO")
    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Fecha de creación del carrito")
    private Date datetime;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;
    
    @Size(min = 1, max = 40)
    @Column(name = "CERTIFICADO")
    @ApiModelProperty(value = "Número de certificado asignado por el restaurante", required = true)
    private String numCertificado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTADO")
    @ApiModelProperty(value = "Estado del carrito (P o C)", required = true)
    private Character estado;
    
    @Size(min = 1, max = 20)
    @Column(name = "CARD_ID")
    @ApiModelProperty(value = "Número de tarjeta con el que se pagó", required = false)
    private String cardId;
    
    @Column(name = "SUB_TOTAL")
    @ApiModelProperty(value = "Sub total del carrito", required = false)
    private Double subTotal;
    
    @Column(name = "IMP")
    @ApiModelProperty(value = "Impuesto del carrito", required = false)
    private Double imp;
    
    @Column(name = "TOTAL")
    @ApiModelProperty(value = "Total del carrito", required = false)
    private Double total;

    public TransCarrito() {
    }

    public TransCarrito(String numTransaccion) {
        this.numTransaccion = numTransaccion;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getNumCertificado() {
        return numCertificado;
    }

    public void setNumCertificado(String numCertificado) {
        this.numCertificado = numCertificado;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Double getImp() {
        return imp;
    }

    public void setImp(Double imp) {
        this.imp = imp;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public String getNumTransaccion() {
        return numTransaccion;
    }

    public void setNumTransaccion(String numTransaccion) {
        this.numTransaccion = numTransaccion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numTransaccion != null ? numTransaccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransCarrito)) {
            return false;
        }
        TransCarrito other = (TransCarrito) object;
        if ((this.numTransaccion == null && other.numTransaccion != null) || (this.numTransaccion != null && !this.numTransaccion.equals(other.numTransaccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.TransCarrito[ numTransaccion=" + numTransaccion + " ]";
    }

//    
    
    
}
