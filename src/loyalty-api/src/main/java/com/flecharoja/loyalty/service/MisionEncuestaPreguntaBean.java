package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionEncuestaPregunta;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado del manejo de datos para el mantenimiento de misiones de tipo
 * encuesta
 *
 * @author svargas
 */
@Stateless
public class MisionEncuestaPreguntaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    @EJB
    UtilBean utilBean;

    /**
     * Metodo que obtiene el listado de todas las preguntas/calificaciones que
     * una mision tiene asociada
     *
     * @param idMision Identificador de mision
     * @param locale
     * @return Listado de preguntas/calificaciones
     */
    public List getPreguntasEncuestaMision(String idMision, Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.ENCUESTA.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }
        List resultado = em.createNamedQuery("MisionEncuestaPregunta.findAllByIdMision").setParameter("idMision", idMision).getResultList();

        return resultado;
    }

    /**
     * Metodo que obtiene el la informacion de una pregunta/calificacion de una
     * mision
     *
     * @param idMision Identificador de mision
     * @param idPregunta Identificador de pregunta
     * @param locale
     * @return Informacion de la pregunta/calificacion
     */
    public MisionEncuestaPregunta getPreguntaEncuestaMisionPorId(String idMision, String idPregunta, Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.ENCUESTA.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        MisionEncuestaPregunta pregunta = em.find(MisionEncuestaPregunta.class, idPregunta);

        if (pregunta == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_question_not_found");
        }

        if (!pregunta.getMision().getIdMision().equals(idMision)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idMision");
        }

        return pregunta;
    }

    /**
     * Metodo que registra y asocia una pregunta/calificacion a una mision
     *
     * @param idMision Identificador de mision
     * @param pregunta Informacion de la pregunta
     * @param idUsuario Identificador del usuario creador
     * @param locale
     * @return Identificador de la pregunta
     */
    public String insertPreguntaEncuestaMision(String idMision, MisionEncuestaPregunta pregunta, String idUsuario, Locale locale) {
        if (pregunta == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Pregunta");
        }
        //verificacion que los id's sean validos
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.ENCUESTA.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }

        MisionEncuestaPregunta.TiposPregunta tipoPreguntaMisionEncuesta = MisionEncuestaPregunta.TiposPregunta.get(pregunta.getIndTipoPregunta());
        if (tipoPreguntaMisionEncuesta == null || pregunta.getIndRespuestaCorrecta() == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indTipoPregunta, indRespuestaCorrecta");
        }
        switch (tipoPreguntaMisionEncuesta) {
            case CALIFICACION: {
                if (pregunta.getIndComentario() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indComentario");
                }
                MisionEncuestaPregunta.TiposRespuestaCorrecta trc = MisionEncuestaPregunta.TiposRespuestaCorrecta.get(pregunta.getIndRespuestaCorrecta());
                if (trc == null && (trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.CUALQUIERA_DE
                        || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.SON)) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                }
                if (trc != MisionEncuestaPregunta.TiposRespuestaCorrecta.TODAS) {
                    try {
                        if (Integer.parseInt(pregunta.getRespuestasCorrectas())>5 || Integer.parseInt(pregunta.getRespuestasCorrectas())<1) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "respuestasCorrectas");
                        }
                    } catch (NumberFormatException | NullPointerException e) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                    }
                }
                break;
            }
            case ENCUESTA: {
                MisionEncuestaPregunta.TiposRespuesta tr = MisionEncuestaPregunta.TiposRespuesta.get(pregunta.getIndTipoRespuesta());
                MisionEncuestaPregunta.TiposRespuestaCorrecta trc = MisionEncuestaPregunta.TiposRespuestaCorrecta.get(pregunta.getIndRespuestaCorrecta());
                if (tr == null || trc == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                }
                if (trc != MisionEncuestaPregunta.TiposRespuestaCorrecta.TODAS) {
                    if (pregunta.getRespuestas() == null || Arrays.stream(pregunta.getRespuestas().split("\n")).anyMatch((t) -> t.trim().length()==0)) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "respuestas");
                    }
                }
                switch (tr) {
                    case SELECCION_UNICA: {
                        if (trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.IGUAL
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.MAYOR_IGUAL
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.MENOR_IGUAL
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.SON) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                        }
                        if (trc != MisionEncuestaPregunta.TiposRespuestaCorrecta.TODAS) {
                            try {
                                if (Integer.parseInt(pregunta.getRespuestasCorrectas())<1||Integer.parseInt(pregunta.getRespuestasCorrectas())>pregunta.getRespuestas().split("\n").length) {
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "respuestasCorrectas");
                                }
                            } catch (NumberFormatException | NullPointerException e) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                            }
                        }
                        break;
                    }
                    case SELECCION_MULTIPLE: {
                        if (trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.IGUAL
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.MAYOR_IGUAL
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.MENOR_IGUAL
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.ES
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.CUALQUIERA_DE) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                        }
                        if (trc != MisionEncuestaPregunta.TiposRespuestaCorrecta.TODAS) {
                            Supplier<Stream<Integer>> respuestasSeleccionadas;
                            try {
                                respuestasSeleccionadas = () -> Arrays.stream(pregunta.getRespuestasCorrectas().split("\n")).map((t)->Integer.parseInt(t));
//                                Arrays.stream(pregunta.getRespuestasCorrectas().split("\n")).forEach((t) -> Integer.parseInt(t));
                            } catch (NumberFormatException | NullPointerException e) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                            }
                            
                            if (respuestasSeleccionadas.get().count()>respuestasSeleccionadas.get().distinct().count()) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "respuestasCorrectas");
                            }
                            
                            int cantidadOpciones = Arrays.asList(pregunta.getRespuestas().split("\n")).size();
                            
                            if (respuestasSeleccionadas.get().anyMatch((t)->t>cantidadOpciones || t<1)) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "respuestasCorrectas");
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }

        Date fecha = new Date();
        pregunta.setMision(mision);
        pregunta.setFechaCreacion(fecha);
        pregunta.setFechaModificacion(fecha);
        pregunta.setUsuarioCreacion(idUsuario);
        pregunta.setUsuarioModificacion(idMision);
        pregunta.setNumVersion(new Long(1));

        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (pregunta.getImagen() == null || pregunta.getImagen().trim().isEmpty()) {
            pregunta.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                pregunta.setImagen(filesUtils.uploadImage(pregunta.getImagen(), MyAwsS3Utils.Folder.IMAGEN_MISION_ENCUESTA, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }

        try {
            em.persist(pregunta);
            em.flush();
        } catch (ConstraintViolationException e) {
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (pregunta.getImagen() != null && !pregunta.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(pregunta.getImagen());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        bitacoraBean.logAccion(MisionEncuestaPregunta.class, idMision + "/" + pregunta.getIdPregunta(), idUsuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        return pregunta.getIdPregunta();
    }

    /**
     * Metodo que modifica una pregunta existente de una mision
     *
     * @param idMision Identificador de la mision
     * @param pregunta Informacion de la pregunta
     * @param idUsuario Identificador del usuario modificador
     * @param locale
     */
    public void editPreguntaEncuestaMision(String idMision, MisionEncuestaPregunta pregunta, String idUsuario,Locale locale) {
        if (pregunta == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Pregunta");
        }
        //verificacion que los id's sean validos
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.ENCUESTA.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }

        if (pregunta.getIdPregunta() == null) {
             throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idPregunta");
        }

        MisionEncuestaPregunta original = em.find(MisionEncuestaPregunta.class, pregunta.getIdPregunta());
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_question_not_found");
        }

        if (!original.getMision().getIdMision().equals(idMision)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idMision");
        }       

        MisionEncuestaPregunta.TiposPregunta tipoPreguntaMisionEncuesta = MisionEncuestaPregunta.TiposPregunta.get(pregunta.getIndTipoPregunta());
        if (tipoPreguntaMisionEncuesta == null || pregunta.getIndRespuestaCorrecta() == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indTipoPregunta, indRespuestaCorrecta");
        }
        switch (tipoPreguntaMisionEncuesta) {
            case CALIFICACION: {
                if (pregunta.getIndComentario() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indComentario");
                }
                MisionEncuestaPregunta.TiposRespuestaCorrecta trc = MisionEncuestaPregunta.TiposRespuestaCorrecta.get(pregunta.getIndRespuestaCorrecta());
                if (trc == null && (trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.CUALQUIERA_DE
                        || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.ES)) {
                     throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                }
                if (trc != MisionEncuestaPregunta.TiposRespuestaCorrecta.TODAS) {
                    try {
                        Integer.parseInt(pregunta.getRespuestasCorrectas());
                    } catch (NumberFormatException | NullPointerException e) {
                         throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                    }
                }
                break;
            }
            case ENCUESTA: {
                MisionEncuestaPregunta.TiposRespuesta tr = MisionEncuestaPregunta.TiposRespuesta.get(pregunta.getIndTipoRespuesta());
                MisionEncuestaPregunta.TiposRespuestaCorrecta trc = MisionEncuestaPregunta.TiposRespuestaCorrecta.get(pregunta.getIndRespuestaCorrecta());
                if (tr == null || trc == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                }
                if (trc != MisionEncuestaPregunta.TiposRespuestaCorrecta.TODAS) {
                    if (pregunta.getRespuestas() == null || Arrays.stream(pregunta.getRespuestas().split("\n")).anyMatch((t) -> t.trim().length()==0)) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "respuestas");
                    }
                }
                switch (tr) {
                    case SELECCION_UNICA: {
                        if (trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.IGUAL
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.MAYOR_IGUAL
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.MENOR_IGUAL
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.SON) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                        }
                        if (trc != MisionEncuestaPregunta.TiposRespuestaCorrecta.TODAS) {
                            try {
                                if (Integer.parseInt(pregunta.getRespuestasCorrectas())<1||Integer.parseInt(pregunta.getRespuestasCorrectas())>pregunta.getRespuestas().split("\n").length) {
                                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "respuestasCorrectas");
                                }
                            } catch (NumberFormatException | NullPointerException e) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                            }
                        }
                        break;
                    }
                    case SELECCION_MULTIPLE: {
                        if (trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.IGUAL
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.MAYOR_IGUAL
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.MENOR_IGUAL
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.ES
                                || trc == MisionEncuestaPregunta.TiposRespuestaCorrecta.CUALQUIERA_DE) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                        }
                        if (trc != MisionEncuestaPregunta.TiposRespuestaCorrecta.TODAS) {
                            Supplier<Stream<Integer>> respuestasSeleccionadas;
                            try {
                                respuestasSeleccionadas = () -> Arrays.stream(pregunta.getRespuestasCorrectas().split("\n")).map((t)->Integer.parseInt(t));
                            } catch (NumberFormatException | NullPointerException e) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "type_answer_not_valid_question_type");
                            }
                            
                            if (respuestasSeleccionadas.get().count()>respuestasSeleccionadas.get().distinct().count()) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "respuestasCorrectas");
                            }
                            
                            int cantidadOpciones = Arrays.asList(pregunta.getRespuestas().split("\n")).size();
                            
                            if (respuestasSeleccionadas.get().anyMatch((t)->t>cantidadOpciones || t<1)) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "respuestasCorrectas");
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }

        pregunta.setFechaModificacion(new Date());
        pregunta.setUsuarioModificacion(idUsuario);
        pregunta.setMision(mision);

        String urlImagenNueva = null;
        String urlImagenOriginal = original.getImagen();

        //si el atributo de imagen viene nula, se utiliza la predeterminada
        if (pregunta.getImagen() == null || pregunta.getImagen().trim().isEmpty()) {
            urlImagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            pregunta.setImagen(urlImagenNueva);
        } else {
            //ver si el valor en el atributo de imagen, difiere del almacenado
            if (!urlImagenOriginal.equals(pregunta.getImagen())) {
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    urlImagenNueva = filesUtils.uploadImage(pregunta.getImagen(), MyAwsS3Utils.Folder.IMAGEN_MISION_ENCUESTA, null);
                    pregunta.setImagen(urlImagenNueva);
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }

        try {
            em.merge(pregunta);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(MisionEncuestaPregunta.class, idMision + "/" + pregunta.getIdPregunta(), idUsuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        if (urlImagenNueva != null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Metodo que elimina una pregunta de una mision
     *
     * @param idMision Identificador de la mision
     * @param idPregunta Identificador de pregunta
     * @param idUsuario Identificador del usuario modificador
     * @param locale
     */
    public void removePreguntaEncuestaMision(String idMision, String idPregunta, String idUsuario,Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        MisionEncuestaPregunta pregunta = em.find(MisionEncuestaPregunta.class, idPregunta);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.ENCUESTA.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }
        
        if (pregunta==null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_question_not_found");
        }

        try {
            //verificacion que la entidad exista y su eliminacion
            em.remove(pregunta);
            if (!pregunta.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(pregunta.getImagen());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            em.flush();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_question_not_found");
        }

        bitacoraBean.logAccion(MisionEncuestaPregunta.class, idMision + "/" + idPregunta, idUsuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }
}
