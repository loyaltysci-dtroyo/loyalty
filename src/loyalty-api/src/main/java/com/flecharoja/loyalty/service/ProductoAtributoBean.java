/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.AtributoDinamicoProducto;
import com.flecharoja.loyalty.model.Producto;
import com.flecharoja.loyalty.model.ProductoAtributo;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.validation.ConstraintViolationException;

/**
 * Clase encargada de proveer los metodos necesarios para la asignacion de un
 * atributo dinamico a un producto
 *
 * @author wtencio
 */
@Stateless
public class ProductoAtributoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de bitacora

    /**
     * Método que asigna un atributo dinamico a un producto
     *
     * @param idAtributo identificador del atributo
     * @param idProducto identificador de producto
     * @param valor valor a tomar el atributo para el producto
     * @param usuario usuario en sesion
     * @param locale
     */
    public void assingAtributoProducto(String idAtributo, String idProducto, String valor, String usuario, Locale locale) {
        ProductoAtributo productoAtributo = new ProductoAtributo(idProducto, idAtributo);

        try {
            Producto producto = em.find(Producto.class, idProducto);
            AtributoDinamicoProducto atributo = em.find(AtributoDinamicoProducto.class, idProducto);

            if (valor == null && !atributo.getIndTipo().equals(AtributoDinamicoProducto.Tipos.OPCIONES.getValue())) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "valor");
            } else if (valor == null && atributo.getIndTipo().equals(AtributoDinamicoProducto.Tipos.OPCIONES.getValue())) {
                productoAtributo.setValor(atributo.getValorOpciones());
            } else {
                productoAtributo.setValor(valor);
            }

            productoAtributo.setAtributoDinamicoProducto(atributo);
            productoAtributo.setProducto(producto);
            productoAtributo.setUsuarioCreacion(usuario);
            productoAtributo.setFechaCreacion(Calendar.getInstance().getTime());

            em.persist(productoAtributo);
            em.flush();
            bitacoraBean.logAccion(ProductoAtributo.class, idAtributo + "/" + idProducto, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);

        } catch (ConstraintViolationException e) {//atributos no validos
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que desasigna un atributo de un producto
     *
     * @param idAtributo identificador del atributo
     * @param idProducto identificador del producto
     * @param usuario usuario en sesion
     * @param locale
     */
    public void unassingAtributoProducto(String idAtributo, String idProducto, String usuario, Locale locale) {

        ProductoAtributo productoAtributo = (ProductoAtributo) em.createNamedQuery("ProductoAtributo.findByIdProductoByIdAtributo").setParameter("idAtributo", idAtributo).setParameter("idProducto", idProducto).getSingleResult();

        if (productoAtributo == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_dynamic_attribute_not_found");
        }
        em.remove(productoAtributo);
        bitacoraBean.logAccion(ProductoAtributo.class, idAtributo + "/" + idProducto, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);

    }

    /**
     * Método que actualiza el valor de un atributo para un producto
     *
     * @param idAtributo identificador del atributo
     * @param idProducto identificador del producto
     * @param valor valor a actualizar
     * @param usuario usuario en sesion
     * @param locale
     */
    public void updateAtributoProducto(String idAtributo, String idProducto, String valor, String usuario, Locale locale) {

        ProductoAtributo productoAtributo = (ProductoAtributo) em.createNamedQuery("ProductoAtributo.findByIdProductoByIdAtributo").setParameter("idAtributo", idAtributo).setParameter("idProducto", idProducto).getSingleResult();
        if (productoAtributo == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_dynamic_attribute_not_found");
        }
        productoAtributo.setValor(valor);

        em.merge(productoAtributo);
        bitacoraBean.logAccion(ProductoAtributo.class, idAtributo + "/" + idProducto, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);

    }

    /**
     * Método que obtiene un listado de atributos asociados a un producto en
     * especifico
     *
     * @param idProducto identificador del producto
     * @param estados
     * @param tipo
     * @param tipos
     * @param cantidad de registros maximos por pagina
     * @param busqueda
     * @param indRequerido
     * @param filtros
     * @param ordenTipo
     * @param ordenCampo
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return lista de atributos de producto
     */
    public Map<String, Object> getAtributosPorProducto(String idProducto, String tipo, List<String> estados, List<String> tipos, String indRequerido, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<AtributoDinamicoProducto> root = query.from(AtributoDinamicoProducto.class);

        query = Busquedas.getCriteriaQueryAtributoDinamicoProducto(cb, query, root, estados, tipos, indRequerido, busqueda, filtros, ordenTipo, ordenCampo);

        Subquery<String> subquery = query.subquery(String.class);
        Root<ProductoAtributo> rootSubquery = subquery.from(ProductoAtributo.class);
        subquery.select(rootSubquery.get("productoAtributoPK").get("idAtributo"));
        if (tipo == null || tipo.toUpperCase().charAt(0) == Indicadores.ASIGNADO) {
            subquery.where(cb.equal(rootSubquery.get("productoAtributoPK").get("idProducto"), idProducto));
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idAtributo")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idAtributo")).value(subquery)));
            }
        } else {
            if (tipo.toUpperCase().charAt(0) == Indicadores.DISPONIBLES) {
                subquery.where(cb.equal(rootSubquery.get("productoAtributoPK").get("idProducto"), idProducto));
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.equal(root.get("indEstado"), AtributoDinamicoProducto.Estados.PUBLICADO.getValue()), cb.or(cb.in(root.get("idAtributo")).value(subquery).not()));
                } else {
                    query.where(cb.equal(root.get("indEstado"), AtributoDinamicoProducto.Estados.PUBLICADO.getValue()), cb.or(cb.in(root.get("idAtributo")).value(subquery).not()));
                }
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "tipo");
            }
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        List<ProductoAtributo> productoAtributo = em.createNamedQuery("ProductoAtributo.findByIdProducto").setParameter("idProducto", idProducto).getResultList();
        for (Object object : resultado) {
            AtributoDinamicoProducto atributo = (AtributoDinamicoProducto) object;
            for (ProductoAtributo productoAtb : productoAtributo) {
                if (atributo.getIdAtributo().equals(productoAtb.getProductoAtributoPK().getIdAtributo())) {
                    atributo.setValorAtributo(productoAtb.getValor());
                }
            }
        }
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }
}
