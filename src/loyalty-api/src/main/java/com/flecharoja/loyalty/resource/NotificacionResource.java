package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.service.NotificacionBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase con recursos y metodos http para el manejo de envio de notificaciones.
 *
 * @author svargas, faguilar
 */
@Api(value = "Notificacion")
@Path("notificacion")
public class NotificacionResource {

    @EJB
    NotificacionBean bean;

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @Context
    HttpServletRequest request;

    /**
     * Metodo que obtiene un listado de notificaciones opcionalmente por
     * indicadores y terminos de busqueda, de no pasar alguno, se obtienen
     * todos, adicionalmente se recupera en un rango de resultados definidos por
     * los parametros de registro y cantidad, de ocurrir un error se retornara
     * una Respuesta con un estado indicando error junto con un encabezado
     * (Error-Reason) con un codigo de error referente a la naturaleza del error
     * ocurrido.
     *
     * @param estados Listado opcional de estados deseados de las notificaciones
     * @param tipos Listado de indicadores de tipos de notificaciones
     * @param eventos Listado de indicadores de eventos de notificaciones
     * @param objetivos Listado de indicadores de objetivo de notificaciones
     * @param busqueda Terminos de busqueda a aplicar
     * @param filtros Campos sobre donde se va a aplicar los terminos de
     * busqueda
     * @param ordenTipo
     * @param ordenCampo
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta con el listado de notificaciones en un rango valido,
     * junto con encabezados sobre el rango de resultados y rango maximo valido
     * por peticion
     */
    @ApiOperation(value = "Obtener lista de notificaciones",
            responseContainer = "List",
            response = Notificacion.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNotificaciones(
            @ApiParam(value = "Indicadores de estado de notificaciones") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de tipo de notificaciones") @QueryParam("tipo") List<String> tipos,
            @ApiParam(value = "Indicadores de evento de notificaciones") @QueryParam("evento") List<String> eventos,
            @ApiParam(value = "Indicadores de objetivos de notificaciones") @QueryParam("objetivo") List<String> objetivos,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros  sobre donde aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = bean.getNotificaciones(estados, tipos, eventos, objetivos, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    //TODO BORRAR /buscar/*
    @GET
    @Path("/buscar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchNotificaciones(
            @QueryParam("estado") String estado,
            @QueryParam("evento") List<String> evento,
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @QueryParam("registro") int registro) {
        return this.getNotificaciones(estado == null ? null : Arrays.asList(estado.split("-")), null, evento, null, null, null, null, null, cantidad, registro);
    }

    @GET
    @Path("/buscar/{busqueda}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchNotificaciones(
            @PathParam("busqueda") String busqueda,
            @QueryParam("estado") String estado,
            @QueryParam("evento") List<String> evento,
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @QueryParam("registro") int registro) {
        return this.getNotificaciones(estado == null ? null : Arrays.asList(estado.split("-")), null, evento, null, busqueda, null, null, null, cantidad, registro);
    }

    /**
     * Metodo que obtiene la informacion de una notificacion por su
     * identificador, de ocurrir un error se retornara una Respuesta con un
     * estado indicando error junto con un encabezado (Error-Reason) con un
     * codigo de error referente a la naturaleza del error ocurrido.
     *
     * @param idNotificacion Identificador de la notificacion deseada
     * @return Respuesta con la informacion de la notificacion encontrada
     */
    @ApiOperation(value = "Obtener una notificacion por identificador",
            response = Notificacion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idNotificacion}")
    @Produces(MediaType.APPLICATION_JSON)
    public Notificacion getNotificacionPorId(
            @ApiParam(value = "Identificador de notificacion", required = true)
            @PathParam("idNotificacion") String idNotificacion) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bean.getNotificacionPorId(idNotificacion, request.getLocale());
    }

    /**
     * Metodo que registra la informacion de una notificacion y retorna el
     * identificador generado para la misma, de ocurrir un error se retornara
     * una Respuesta con un estado indicando error junto con un encabezado
     * (Error-Reason) con un codigo de error referente a la naturaleza del error
     * ocurrido.
     *
     * @param notificacion Objecto con la informacion requerida de una
     * notificacion
     * @return Respuesta con el identificador de la notificacion creada
     */
    @ApiOperation(value = "Registrar notificacion",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertNotificacion(
            @ApiParam(value = "Informacion de la notificacion", required = true) Notificacion notificacion) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        notificacion.setUsuarioCreacion(usuarioContext);
        notificacion.setUsuarioModificacion(usuarioContext);
        return bean.insertNotificacion(notificacion, request.getLocale());
    }

    /**
     * Metodo que modifica la informacion de una notificacion que ya ha sido
     * registrada, de ocurrir un error se retornara una Respuesta con un estado
     * indicando error junto con un encabezado (Error-Reason) con un codigo de
     * error referente a la naturaleza del error ocurrido.
     *
     * @param notificacion Objecto de una notificacion registrada con la
     * informacion modificada
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Modificacion de notificacion")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void editNotificacion(
            @ApiParam(value = "Informacion de la notificacion", required = true) Notificacion notificacion) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        notificacion.setUsuarioModificacion(usuarioContext);
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.enviarNotificacion(bean.editNotificacion(notificacion, request.getLocale()), request.getLocale());
    }

    /**
     * Metodo que elimina o archiva el registro de una notificacion segun el
     * estado de la misma obtenida por su identificador, de ocurrir un error se
     * retornara una Respuesta con un estado indicando error junto con un
     * encabezado (Error-Reason) con un codigo de error referente a la
     * naturaleza del error ocurrido.
     *
     * @param idNotificacion Identificador de la notificacion
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion/Archivado de notificacion")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idNotificacion}")
    public void removeNotificacion(
            @ApiParam(value = "Identificador de notificacion", required = true)
            @PathParam("idNotificacion") String idNotificacion) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.removeNotificacion(idNotificacion, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que retorna en terminos de true/false si existe un registro con un
     * nombre interno igual al valor del parametro de nombreInterno, de ocurrir
     * un error se retornara una Respuesta con un estado indicando error junto
     * con un encabezado (Error-Reason) con un codigo de error referente a la
     * naturaleza del error ocurrido.
     *
     * @param nombreInterno Parametro con el valor del nombre interno de
     * notificacion a verificar
     * @return Respuesta con el inidcador de existencia del nombre interno
     * (true/false)
     */
    @ApiOperation(value = "Verificacion de existencia de nombre interno en notificaciones",
            response = Boolean.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("comprobar-nombre/{nombreInterno}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean existsNombreInterno(
            @ApiParam(value = "Nombre interno de notificacion", required = true)
            @PathParam("nombreInterno") String nombreInterno) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bean.existsNombreInterno(nombreInterno);
    }
}
