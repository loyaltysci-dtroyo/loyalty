/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author kramirez
 */
@Entity
@Table(name = "PREGUNTA_MILLONARIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PreguntaMillonario.findAll", query = "SELECT j FROM PreguntaMillonario j"),
    @NamedQuery(name = "PreguntaMillonario.findByJuego", query = "SELECT j FROM PreguntaMillonario j where j.idJuego= :idJuego ORDER BY j.puntos ASC")
})
public class PreguntaMillonario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "pregunta_millonario_uuid")
    @GenericGenerator(name = "pregunta_millonario_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_PREGUNTA")
    private String idPregunta;
    
    @Version
    @Basic(optional = false)
    @NotNull()
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "ID_JUEGO")
    private String idJuego;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Pregunta")
    private String pregunta;
    
    @Basic(optional = true)
    @Column(name = "RESPUESTA_1")
    private String respuesta1;
    
    @Basic(optional = true)
    @Column(name = "RESPUESTA_2")
    private String respuesta2;
    
    @Basic(optional = true)
    @Column(name = "RESPUESTA_3")
    private String respuesta3;
    
    @Basic(optional = true)
    @Column(name = "RESPUESTA_4")
    private String respuesta4;
    
    @Basic(optional = true)
    @Column(name = "RESPUESTA_CORRECTA")
    private int respuestaCorrecta;
    
    @Basic(optional = true)
    @Column(name = "PUNTOS")
    private int puntos;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    public PreguntaMillonario() {
    }

    public PreguntaMillonario(String usuario, Long numVersion, String idJuego) {
        this.idJuego = idJuego;
        this.usuarioCreacion = usuario;
        this.usuarioModificacion = usuario;
        this.fechaModificacion = new Date();
        this.numVersion = numVersion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public String getIdPregunta() {
        return idPregunta;
    }

    public String getIdJuego() {
        return idJuego;
    }

    public int getPuntos() {
        return puntos;
    }

    public String getPregunta() {
        return pregunta;
    }

    public String getRespuesta1() {
        return respuesta1;
    }

    public String getRespuesta2() {
        return respuesta2;
    }

    public String getRespuesta3() {
        return respuesta3;
    }

    public String getRespuesta4() {
        return respuesta4;
    }

    public int getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

    public void setIdPregunta(String idPregunta) {
        this.idPregunta = idPregunta;
    }

    public void setIdJuego(String idJuego) {
        this.idJuego = idJuego;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public void setRespuesta1(String respuesta1) {
        this.respuesta1 = respuesta1;
    }

    public void setRespuesta2(String respuesta2) {
        this.respuesta2 = respuesta2;
    }

    public void setRespuesta3(String respuesta3) {
        this.respuesta3 = respuesta3;
    }

    public void setRespuesta4(String respuesta4) {
        this.respuesta4 = respuesta4;
    }

    public void setRespuestaCorrecta(int respuestaCorrecta) {
        this.respuestaCorrecta = respuestaCorrecta;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPregunta != null ? idPregunta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Juego)) {
            return false;
        }
        PreguntaMillonario other = (PreguntaMillonario) object;
        if ((this.idPregunta == null && other.idPregunta != null) || (this.idPregunta != null && !this.idPregunta.equals(other.idPregunta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Juego{" + "numVersion=" + numVersion + ", pregunta=" + pregunta + ", respuestaCorrecta=" + respuestaCorrecta + ", fechaCreacion=" + fechaCreacion + ", usuarioCreacion=" + usuarioCreacion + ", fechaModificacion=" + fechaModificacion + ", usuarioModificacion=" + usuarioModificacion + '}';
    }
}
