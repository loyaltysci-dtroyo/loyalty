package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author svargas
 */
@Embeddable
public class EstadisticaIndividualMetricaNivelPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_NIVEL_METRICA")
    private String idNivel;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_METRICA")
    private String idMetrica;

    public EstadisticaIndividualMetricaNivelPK() {
    }

    public EstadisticaIndividualMetricaNivelPK(String idNivel, String idMetrica) {
        this.idNivel = idNivel;
        this.idMetrica = idMetrica;
    }

    public String getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(String idNivel) {
        this.idNivel = idNivel;
    }

    public String getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivel != null ? idNivel.hashCode() : 0);
        hash += (idMetrica != null ? idMetrica.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadisticaIndividualMetricaNivelPK)) {
            return false;
        }
        EstadisticaIndividualMetricaNivelPK other = (EstadisticaIndividualMetricaNivelPK) object;
        if ((this.idNivel == null && other.idNivel != null) || (this.idNivel != null && !this.idNivel.equals(other.idNivel))) {
            return false;
        }
        if ((this.idMetrica == null && other.idMetrica != null) || (this.idMetrica != null && !this.idMetrica.equals(other.idMetrica))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.EstadisticaIndividualMetricaNivelPK[ idNivel=" + idNivel + ", idMetrica=" + idMetrica + " ]";
    }
    
}
