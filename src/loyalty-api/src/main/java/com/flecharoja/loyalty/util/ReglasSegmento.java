package com.flecharoja.loyalty.util;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author svargas
 */
public class ReglasSegmento {
    
    public enum ValoresIndOperador {
        ES('A'),
        NO_ES('B'),
        ES_UNO_DE('C'),
        NO_ES_UNO_DE('D'),
        INICIA_CON('E'),
        TERMINA_CON('F'),
        CONTIENE('G'),
        NO_CONTIENE('H'),
        ESTA_VACIO('I'),
        NO_ESTA_VACIO('J'),
        IGUAL('V'),
        MAYOR('W'),
        MAYOR_IGUAL('X'),
        MENOR('Y'),
        MENOR_IGUAL('Z'),
        FECHA_ANTES_DE('O'),
        FECHA_A_PARTIR_DE('P'),
        FECHA_IGUAL('Q'),
        OPCIONES_SON('U'),
        OPCIONES_CUALQUIERA('K');
        
        public final Character value;
        private static final Map<Character, ValoresIndOperador> lookup = new HashMap<>();

        private ValoresIndOperador(char value) {
            this.value = value;
        }
        
        static {
            for (ValoresIndOperador valor : values()) {
                lookup.put(valor.value, valor);
            }
        }
        
        public static ValoresIndOperador get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum TiposReglasSegmento {
        PREFERENCIA("P", "PRE_"),
        ENCUESTA("E", "MIS_ENC_"),
        ATRIBUTO_MIEMBRO("C", "ATR_MIE_"),
        METRICA_CAMBIO("B", "MET_CAM_"),
        METRICA_BALANCE("A", "MET_BAL_"),
        AVANZADA("D", "AVA_");

        private final String value, prefix;

        private TiposReglasSegmento(String value, String prefix) {
            this.value = value;
            this.prefix = prefix;
        }

        public String getValue() {
            return value;
        }

        public String getNombreColumna(String idRegla) {
            return prefix + idRegla;
        }
    }
}
