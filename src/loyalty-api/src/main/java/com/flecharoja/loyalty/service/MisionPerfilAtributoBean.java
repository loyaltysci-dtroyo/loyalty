package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.AtributoDinamico;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionPerfilAtributo;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado del manejo de datos para el mantenimiento de misiones de tipo
 * actualizacion de perfil
 *
 * @author svargas
 */
@Stateless
public class MisionPerfilAtributoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene un listado de atributos de perfil de una mision
     *
     * @param idMision Identificador de mision
     * @param locale
     * @return Listado de atributos
     */
    public List getAtributosPerfilMision(String idMision, Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.PERFIL.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        List resultado = em.createNamedQuery("MisionPerfilAtributo.findAllByIdMision").setParameter("idMision", idMision).getResultList();

        return resultado;
    }

    /**
     * Metodo que obtiene la informacion de un atributo de una mision
     *
     * @param idMision Identificador de mision
     * @param idAtributo Identificador de atributo
     * @param locale
     * @return Informacion del atributo
     */
    public MisionPerfilAtributo getAtributoPerfilMisionPorId(String idMision, String idAtributo, Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.PERFIL.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        MisionPerfilAtributo atributo = em.find(MisionPerfilAtributo.class, idAtributo);

        if (atributo == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_profile_attribute");
        }

        if (!atributo.getIdMision().getIdMision().equals(idMision)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idMision");
        }

        return atributo;
    }

    /**
     * Metodo que registra y asigna un atributo de una mision
     *
     * @param idMision Identificador de mision
     * @param atributo Informacion de atributo
     * @param idUsuario Identificador del usuario creador
     * @param locale
     * @return Identificador del atributo
     */
    public String insertAtributoPerfilMision(String idMision, MisionPerfilAtributo atributo, String idUsuario, Locale locale) {
        //verificacion que los id's sean validos
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.PERFIL.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }

        MisionPerfilAtributo.Atributos tipoAtributo = MisionPerfilAtributo.Atributos.get(atributo.getIndAtributo());
        if (tipoAtributo == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indAtributo");
        }
        if (tipoAtributo == MisionPerfilAtributo.Atributos.ATRIBUTO_DINAMICO) {
            if (atributo.getIdAtributoDinamico() == null || atributo.getIdAtributoDinamico().getIdAtributo() == null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idAtributoDinamico");
            }
            AtributoDinamico atributoDinamico = em.find(AtributoDinamico.class, atributo.getIdAtributoDinamico().getIdAtributo());
            if (atributoDinamico == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "dynamic_attribute_not_found");
            }
            atributo.setIdAtributoDinamico(atributoDinamico);

            if (verifyIndAtributo(idMision, atributo.getIndAtributo(), atributoDinamico.getIdAtributo())) {
                throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "challenge_already_exists_attribute");
            }
        } else {
            if (verifyIndAtributo(idMision, atributo.getIndAtributo(), null)) {
                throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "challenge_already_exists_attribute");
            }
        }

        Date fecha = new Date();
        atributo.setIdMision(mision);
        atributo.setFechaCreacion(fecha);
        atributo.setFechaModificacion(fecha);
        atributo.setUsuarioCreacion(idUsuario);
        atributo.setUsuarioModificacion(idMision);
        atributo.setNumVersion(new Long(1));

        try {
            em.persist(atributo);
            em.flush();
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        bitacoraBean.logAccion(MisionPerfilAtributo.class, idMision + "/" + atributo.getIdAtributo(), idUsuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        return atributo.getIdAtributo();
    }

    /**
     * Metodo que edita la informacion de un atributo de una mision
     *
     * @param idMision Identificador de mision
     * @param atributo Informacion del atributo
     * @param idUsuario Identificador del usuario modificador
     * @param locale
     */
    public void editAtributoPerfilMision(String idMision, MisionPerfilAtributo atributo, String idUsuario, Locale locale) {
        //verificacion que los id's sean validos
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.PERFIL.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        if (atributo.getIdAtributo() == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idAtributo");
        }

        MisionPerfilAtributo original = em.find(MisionPerfilAtributo.class, atributo.getIdAtributo());
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_profile_attribute");
        }

        if (!original.getIdMision().getIdMision().equals(idMision)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idMision");
        }

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }

        if (MisionPerfilAtributo.Atributos.get(atributo.getIndAtributo()) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indAtributo");
        }
        if (atributo.getIndAtributo().equals(MisionPerfilAtributo.Atributos.ATRIBUTO_DINAMICO.getValue())) {
            if (atributo.getIdAtributoDinamico() == null || atributo.getIdAtributoDinamico().getIdAtributo() == null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idAtributoDinamico");
            }
            AtributoDinamico atributoDinamico = em.find(AtributoDinamico.class, atributo.getIdAtributoDinamico().getIdAtributo());
            if (atributoDinamico == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "dynamic_attribute_not_found");
            }
            atributo.setIdAtributoDinamico(atributoDinamico);

            if (verifyIndAtributo(idMision, atributo.getIndAtributo(), atributoDinamico.getIdAtributo())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "challenge_already_exists_attribute");
            }
        } else {
            if (verifyIndAtributo(idMision, atributo.getIndAtributo(), null)) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "challenge_already_exists_attribute");
            }
        }

        atributo.setFechaModificacion(new Date());
        atributo.setUsuarioModificacion(idUsuario);
        atributo.setIdMision(mision);

        try {
            em.merge(atributo);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(MisionPerfilAtributo.class, idMision + "/" + atributo.getIdAtributo(), idUsuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * Metodo que elimina un atributo de una mision
     *
     * @param idMision Identificador de la mision
     * @param idAtributo Identificador del atributo
     * @param idUsuario Identificador del usuario modificador
     * @param locale
     */
    public void removeAtributoPerfilMision(String idMision, String idAtributo, String idUsuario, Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.PERFIL.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }

        try {
            //verificacion que la entidad exista y su eliminacion
            em.remove(em.getReference(MisionPerfilAtributo.class, idAtributo));
            em.flush();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_profile_attribute");
        }

        bitacoraBean.logAccion(MisionPerfilAtributo.class, idMision + "/" + idAtributo, idUsuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }

    /**
     * Metodo que verifica la existencia de un registro de atributo de una
     * mision con el mismo valor de indAtributo / indAtributo+idAtributoDinamico
     *
     * @param idMision Identificador de mision
     * @param indAtributo Indicador de atributo
     * @param idAtributoDinamico Identificador de Atributo dinamico
     * @return true/false
     */
    private boolean verifyIndAtributo(String idMision, char indAtributo, String idAtributoDinamico) {
        long cantidad;
        if (indAtributo == MisionPerfilAtributo.Atributos.ATRIBUTO_DINAMICO.getValue()) {
            cantidad = (Long) em.createNamedQuery("MisionPerfilAtributo.countAllByIdMisionIdAtributoDinamico")
                    .setParameter("idMision", idMision)
                    .setParameter("idAtributoDinamico", idAtributoDinamico)
                    .getSingleResult();
        } else {
            cantidad = (Long) em.createNamedQuery("MisionPerfilAtributo.countAllByIdMisionIndAtributo")
                    .setParameter("idMision", idMision)
                    .setParameter("indAtributo", indAtributo)
                    .getSingleResult();
        }

        return cantidad > 0;
    }

    /**
     * Metodo que verifica la existencia de un registro de atributo de una
     * mision con el mismo valor de indAtributo / indAtributo+idAtributoDinamico
     *
     * @param idMision Identificador de mision
     * @param indAtributo Indicador de atributo
     * @param idAtributoDinamico Identificador de Atributo dinamico
     * @param locale
     * @return Respuesta con la existencia de registros
     */
    public boolean existsAtributoByIndAtributo(String idMision, String indAtributo, String idAtributoDinamico, Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.PERFIL.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        boolean respuesta;

        respuesta = verifyIndAtributo(idMision, indAtributo.toUpperCase().charAt(0), idAtributoDinamico);

        return respuesta;
    }
}
