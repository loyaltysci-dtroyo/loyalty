package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionRedSocial;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;


/**
 * EJB encargado del manejo de datos para el mantenimiento de misiones de tipo
 * red social
 *
 * @author svargas
 */
@Stateless
public class MisionRedSocialBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    @EJB
    UtilBean utilBean;

    /**
     * Metodo que obtiene la informacion de red social de una mision
     *
     * @param idMision Identificador de mision
     * @param locale
     * @return Informacion de la definicion de red social
     */
    public MisionRedSocial getRedSocialMision(String idMision,Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.RED_SOCIAL.getValue())) {
             throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        MisionRedSocial verContenido = em.find(MisionRedSocial.class, idMision);
        if (verContenido == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_social_network_not_found");
        }

        return verContenido;
    }

    /**
     * Metodo que registra/edita la definicion de red social de una mision
     *
     * @param idMision Identificador de la mision
     * @param redSocial Informacion de red social
     * @param idUsuario Identificador del usuario creador/modificador
     * @param locale
     */
    public void createEditRedSocialMision(String idMision, MisionRedSocial redSocial, String idUsuario,Locale locale) {
        if (redSocial==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "MisionRedSocial");
        }
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.RED_SOCIAL.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        MisionRedSocial original = em.find(MisionRedSocial.class, idMision);

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }

        Date fecha = new Date();
        if (original == null) {
            redSocial.setFechaCreacion(fecha);
            redSocial.setUsuarioCreacion(idUsuario);
            redSocial.setNumVersion(new Long(1));
        }
        redSocial.setIdMision(idMision);
        redSocial.setFechaModificacion(fecha);
        redSocial.setUsuarioModificacion(idUsuario);
        
        String urlImagenVieja = original == null || original.getUrlImagen()==null ? null : original.getUrlImagen();
        String urlImagenNueva = null;
        MisionRedSocial.Tipos tipoMisionRedSocial = MisionRedSocial.Tipos.get(redSocial.getIndTipo());
        if (tipoMisionRedSocial==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indTipo");
        }
        switch(tipoMisionRedSocial) {
            case ENLACE: {
                try {
                    URL url = new URL(redSocial.getUrlObjectivo());
                    url.toURI();
                } catch (MalformedURLException | URISyntaxException e) {
                   throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "url");
                }
                if (redSocial.getUrlImagen()!=null && (urlImagenVieja==null || !redSocial.getUrlImagen().equals(urlImagenVieja))) {
                    try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                        urlImagenNueva = filesUtils.uploadImage(redSocial.getUrlImagen(), MyAwsS3Utils.Folder.IMAGEN_MISION_RED_SOCIAL, null);
                        redSocial.setUrlImagen(urlImagenNueva);
                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                        throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                    }
                }
                if (redSocial.getMensaje()==null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "mensaje");
                }
                break;
            }
            case ME_GUSTA: {
                try {
                    URL url = new URL(redSocial.getUrlObjectivo());
                    url.toURI();
                } catch (MalformedURLException | URISyntaxException e) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "url");
                }
                break;
            }
            case MENSAJE: {
                if (redSocial.getMensaje()==null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "mensaje");
                }
                break;
            }
            case IMAGEN: {
                if (redSocial.getUrlImagen()!=null && (urlImagenVieja==null || !redSocial.getUrlImagen().equals(urlImagenVieja))) {
                    try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                        urlImagenNueva = filesUtils.uploadImage(redSocial.getUrlImagen(), MyAwsS3Utils.Folder.IMAGEN_MISION_RED_SOCIAL, null);
                        redSocial.setUrlImagen(urlImagenNueva);
                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                        throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                    }
                }
                if (redSocial.getMensaje()==null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "mensaje");
                }
                break;
            }
            case TWEET: {
                try {
                    URL url = new URL(redSocial.getUrlObjectivo());
                    url.toURI();
                } catch (MalformedURLException | URISyntaxException e) {
                   throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "url");
                }
                if (redSocial.getUrlImagen()!=null && (urlImagenVieja==null || !redSocial.getUrlImagen().equals(urlImagenVieja))) {
                    try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                        urlImagenNueva = filesUtils.uploadImage(redSocial.getUrlImagen(), MyAwsS3Utils.Folder.IMAGEN_MISION_RED_SOCIAL, null);
                        redSocial.setUrlImagen(urlImagenNueva);
                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                        throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                    }
                }
                if (redSocial.getMensaje()==null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "mensaje");
                }
                break;
            }
            case LIKE_TWEET: {
                try {
                    URL url = new URL(redSocial.getUrlObjectivo());
                    url.toURI();
                } catch (MalformedURLException | URISyntaxException e) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "url");
                }
                break;
            }
            case LIKE_INSTAGRAM: {
                try {
                    URL url = new URL(redSocial.getUrlObjectivo());
                    url.toURI();
                } catch (MalformedURLException | URISyntaxException e) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "url");
                }
                break;
            }
        }

        try {
            em.merge(redSocial);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//en caso de que la entidad no sea valida
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
        
        if (urlImagenNueva!=null && urlImagenVieja!=null && !urlImagenVieja.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenVieja);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }

        bitacoraBean.logAccion(MisionRedSocial.class, idMision, idUsuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * Metodo que elimina la informacion de red social de una mision
     *
     * @param idMision Identificador de la mision
     * @param idUsuario Identificador del usuario modificador
     * @param locale
     */
    public void removeRedSocialMision(String idMision, String idUsuario,Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        MisionRedSocial redSocial = em.find(MisionRedSocial.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (redSocial==null) {
            return;
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.RED_SOCIAL.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }
        
        
        String urlImagenVieja = redSocial.getUrlImagen();

        em.remove(redSocial);
        em.flush();
        
        if (urlImagenVieja!=null && !urlImagenVieja.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenVieja);
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
            }
        }

        bitacoraBean.logAccion(MisionRedSocial.class, idMision, idUsuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }
}
