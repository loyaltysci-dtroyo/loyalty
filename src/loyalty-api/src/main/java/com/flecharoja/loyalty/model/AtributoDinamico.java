package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "ATRIBUTO_DINAMICO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AtributoDinamico.findAll", query = "SELECT a FROM AtributoDinamico a ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamico.findAllActivos", query = "SELECT a FROM AtributoDinamico a WHERE a.indEstado = :indEstado ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamico.countAll", query = "SELECT COUNT(a.idAtributo) FROM AtributoDinamico a"),
    @NamedQuery(name = "AtributoDinamico.findByIdAtributo", query = "SELECT a FROM AtributoDinamico a WHERE a.idAtributo = :idAtributo"),
    @NamedQuery(name = "AtributoDinamico.countNombreInterno", query = "SELECT COUNT(a) FROM AtributoDinamico a WHERE a.nombreInterno = :nombreInterno")
})
public class AtributoDinamico implements Serializable {
    
    public enum Estados{
        BORRADOR('D'),
        PUBLICADO('P'),
        ARCHIVADO('A');
        
        private final char value;
        private static final Map<Character,Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static {
            for(Estados estado: values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum TiposDato {
        FECHA('F'),
        TEXTO('T'),
        NUMERICO('N'),
        BOOLEANO('B');
        
        private final char value;
        private static final Map<Character,TiposDato> lookup = new HashMap<>();

        private TiposDato(char value) {
            this.value = value;
        }
        
        static{
            for(TiposDato estado: values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        public static final TiposDato get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "atributo_uuid")
    @GenericGenerator(name = "atributo_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_ATRIBUTO")
    private String idAtributo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
   
    @Size(min = 1, max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
  
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_DATO")
    private Character indTipoDato;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "VALOR_DEFECTO")
    private String valorDefecto;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_VISIBLE")
    private Boolean indVisible;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_REQUERIDO")
    private Boolean indRequerido;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @OneToMany(mappedBy = "idAtributo")
    private List<Preferencia> preferenciaList;
    
    @OneToMany(mappedBy = "atributoDinamico")
    private List<ReglaMiembroAtb> reglaMiembroAtbList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "atributoDinamico")
    private List<MiembroAtributo> miembroAtributoList;
    
    @Transient
    private String valorAtributoMiembro;

    public AtributoDinamico() {
    }

    public AtributoDinamico(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public AtributoDinamico(String idAtributo, String nombre, String descripcion, Character indTipoDato, Boolean indVisible, String valorDefecto, Boolean indRequerido, Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.idAtributo = idAtributo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.indTipoDato = indTipoDato;
        this.indVisible = indVisible;
        this.valorDefecto = valorDefecto;
        this.indRequerido = indRequerido;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public String getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getIndTipoDato() {
        return indTipoDato;
    }

    public void setIndTipoDato(Character indTipoDato) {
        this.indTipoDato = indTipoDato;
    }

    public Boolean getIndVisible() {
        return indVisible;
    }

    public void setIndVisible(Boolean indVisible) {
        this.indVisible = indVisible;
    }

    public String getValorDefecto() {
        return valorDefecto;
    }

    public void setValorDefecto(String valorDefecto) {
        this.valorDefecto = valorDefecto;
    }

    public Boolean getIndRequerido() {
        return indRequerido;
    }

    public void setIndRequerido(Boolean indRequerido) {
        this.indRequerido = indRequerido;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getValorAtributoMiembro() {
        return valorAtributoMiembro;
    }

    public void setValorAtributoMiembro(String valorAtributoMiembro) {
        this.valorAtributoMiembro = valorAtributoMiembro;
    }
    
     
    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }
    
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MiembroAtributo> getMiembroAtributoList() {
        return miembroAtributoList;
    }

    public void setMiembroAtributoList(List<MiembroAtributo> miembroAtributoList) {
        this.miembroAtributoList = miembroAtributoList;
    }
   
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<Preferencia> getPreferenciaList() {
        return preferenciaList;
    }

    public void setPreferenciaList(List<Preferencia> preferenciaList) {
        this.preferenciaList = preferenciaList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ReglaMiembroAtb> getReglaMiembroAtbList() {
        return reglaMiembroAtbList;
    }

    public void setReglaMiembroAtbList(List<ReglaMiembroAtb> reglaMiembroAtbList) {
        this.reglaMiembroAtbList = reglaMiembroAtbList;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtributo != null ? idAtributo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtributoDinamico)) {
            return false;
        }
        AtributoDinamico other = (AtributoDinamico) object;
        if ((this.idAtributo == null && other.idAtributo != null) || (this.idAtributo != null && !this.idAtributo.equals(other.idAtributo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AtributoDinamico{" + "idAtributo=" + idAtributo + ", nombre=" + nombre + ", descripcion=" + descripcion + ", indTipoDato=" + indTipoDato + ", valorDefecto=" + valorDefecto + ", fechaCreacion=" + fechaCreacion + ", usuarioCreacion=" + usuarioCreacion + ", fechaModificacion=" + fechaModificacion + ", usuarioModificacion=" + usuarioModificacion + ", indEstado=" + indEstado + ", indVisible=" + indVisible + ", indRequerido=" + indRequerido + ", numVersion=" + numVersion + '}';
    }

  

}
