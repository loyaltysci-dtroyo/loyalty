package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia
 * relacionado a metricas
 *
 * @author svargas
 */
@Stateless
public class MetricaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene un listado de metricas almacenadas segun su indicador
     * de estado en un rango de resultados segun parametros
     *
     * @param estados
     * @param indExpiracion
     * @param busqueda
     * @param filtros
     * @param ordenTipo
     * @param ordenCampo
     * @param registro Numero de registro desde donde empieza los resultados
     * @param cantidad Cantidad maxima de resultados deseado por pagina
     * @param locale
     * @return Listado de metricas y rango de la respuesta
     */
    public Map<String, Object> getMetricas(List<String> estados,  String indExpiracion, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int registro, int cantidad, Locale locale) {
        //verificacion que el rango de paginacion sea dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Metrica> root = query.from(Metrica.class);

        query = Busquedas.getCriteriaQueryMetrica(cb, query, root, estados, indExpiracion, busqueda, filtros, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();

        //retorno del resultado y encabezados sobre el rango
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Metodo recupera de la bases de datos la informacion de una metrica segun
     * su identificador
     *
     * @param idMetrica Identificador de la metrica deseada
     * @param locale
     * @return Informacion de la metrica encontrada
     */
    public Metrica getMetrica(String idMetrica, Locale locale) {

        Metrica metrica = em.find(Metrica.class, idMetrica);//se busca la entidad
        //verificacion que la metrica no sea nula (no se encontro)
        if (metrica == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "metric_not_found");
        }
        return metrica;
    }

    /**
     * Metodo que verifica si existe una metrica almacenada con el mismo nombre
     * interno que el proveido
     *
     * @param nombre Valor del nombre interno a verificar
     * @return Expresion booleana de la existencia de un nombre interno igual
     */
    public Boolean existsNombreInterno(String nombre) {
        //se obtiene la cantidad de registros con el mismo nombre interno y se pregunta si este es mayor que 0 (existe al menos uno)
        return ((Long) em.createNamedQuery("Metrica.countByNombreInterno").setParameter("nombreInterno", nombre).getSingleResult()) > 0;
    }

    /**
     * Metodo que guarda en almacenamiento la informacion de una nueva metrica
     *
     * @param metrica Objeto con los valores de la nueva metrica
     * @param usuario usuario en sesión
     * @param locale
     * @return Identificador asignado a la metrica almacenada
     */
    public String insertMetrica(Metrica metrica, String usuario, Locale locale) {
        if (metrica == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        Date date = Calendar.getInstance().getTime();//obtencion de la fecha actual

        //establecimiento de atributos por defecto
        metrica.setUsuarioCreacion(usuario);
        metrica.setUsuarioModificacion(usuario);
        metrica.setFechaCreacion(date);
        metrica.setFechaModificacion(date);
        metrica.setNumVersion(new Long(1));
        metrica.setIndEstado(Metrica.Estados.BORRADOR.getValue());

        //almacenamiento de la entidad y registro de la accion en la bitacora
        try {
            em.persist(metrica);
            em.flush();
        } catch (ConstraintViolationException e) {//si se viola una restriccion de un atributo...
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        bitacoraBean.logAccion(Metrica.class, metrica.getIdMetrica(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);

        //se retorna el identificado creado
        return metrica.getIdMetrica();
    }

    /**
     * Metodo que modifica en el almacenamiento la informacion de una metrica
     * existente
     *
     * @param metrica Objeto con la informacion nueva de una metrica existente
     * @param usuario usuario en sesión
     * @param locale
     */
    public void editMetrica(Metrica metrica, String usuario, Locale locale) {
        if (metrica == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        //verificacion de la existencia de la entidad
        Metrica original = em.find(Metrica.class, metrica.getIdMetrica());
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "metric_not_found");
        }

        //verificacion de cambio de estado no permitido o modificacion de entidad archivada
        Character originalIndEstado = original.getIndEstado();
        Character newIndEstado = metrica.getIndEstado();
        if ((originalIndEstado.equals(Metrica.Estados.PUBLICADO.getValue()) && newIndEstado.equals(Metrica.Estados.BORRADOR.getValue()))
                || originalIndEstado.equals(Metrica.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", this.getClass().getSimpleName());
        }

        metrica.setUsuarioModificacion(usuario);
        metrica.setFechaModificacion(Calendar.getInstance().getTime());

        try {
            em.merge(metrica);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(Metrica.class, metrica.getIdMetrica(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);

    }

    /**
     * Metodo que establece una metrica existente con estado de archivado
     *
     * @param idMetrica Valor de identificacion de la metrica a archivar
     * @param usuario usuario en sesión
     * @param locale
     */
    public void removeMetrica(String idMetrica, String usuario, Locale locale) {
        //verificacion que la entidad no sea nulo
        Metrica metrica = em.find(Metrica.class, idMetrica);
        if (metrica == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }

        //comprobacion que si la metrica esta en condicion de borrador, esta pueda ser eliminada, si no, se marca como archivada
        if (metrica.getIndEstado().compareTo(Metrica.Estados.BORRADOR.getValue()) == 0) {
            em.remove(metrica);

            bitacoraBean.logAccion(Metrica.class, idMetrica, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        } else {
            //verificacion que la metrica no este en condicion actual de archivado
            if (metrica.getIndEstado().compareTo(Metrica.Estados.ARCHIVADO.getValue()) == 0) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", this.getClass().getSimpleName());
            }

            metrica.setUsuarioModificacion(usuario);

            metrica.setIndEstado(Metrica.Estados.ARCHIVADO.getValue());
            metrica.setFechaModificacion(Calendar.getInstance().getTime());

            em.merge(metrica);

            bitacoraBean.logAccion(Metrica.class, idMetrica, usuario, Bitacora.Accion.ENTIDAD_ARCHIVADA.getValue(), locale);
        }

    }
}
