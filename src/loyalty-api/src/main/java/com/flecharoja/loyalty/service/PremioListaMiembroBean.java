/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioListaMiembro;
import com.flecharoja.loyalty.model.PremioListaMiembroPK;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * Clase encargada de proveer los metodos para el manejo de elegibles de
 * miembros en un premio
 *
 * @author wtencio
 */
@Stateless
public class PremioListaMiembroBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de bitacora

    /**
     * Método que asigna (incluye o excluye) a un miembro de un premio
     *
     * @param idPremio identificador del premio
     * @param idMiembro identificador del miembro
     * @param indAccion tipo de accion(incluir o excluir)
     * @param usuario usuario en sesion
     * @param locale
     */
    public void includeExcludeMiembro(String idPremio, String idMiembro, Character indAccion, String usuario, Locale locale) {
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }
        try {
            em.getReference(Miembro.class, idMiembro).getIdMiembro();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        if (premio.getIndEstado().equals(Premio.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Premio");
        }

        Date date = Calendar.getInstance().getTime();
        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea una valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            PremioListaMiembro premioListaMiembro = new PremioListaMiembro(idMiembro, idPremio, indAccion, date, usuario);
            em.merge(premioListaMiembro);
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        bitacoraBean.logAccion(PremioListaMiembro.class, idPremio + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);

    }

    /**
     * Método que revoca la asignacion de un miembro a una promocion
     *
     * @param idPremio identificador del premio
     * @param idMiembro identificador del miembro
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeMiembro(String idPremio, String idMiembro, String usuario, Locale locale) {
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }
        try {
            em.getReference(Miembro.class, idMiembro).getIdMiembro();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }
        //entidad de entidad archivada
        if (premio.getIndEstado().equals(Premio.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Premio");
        }

        em.remove(em.getReference(PremioListaMiembro.class, new PremioListaMiembroPK(idMiembro, idPremio)));
        em.flush();
        bitacoraBean.logAccion(PremioListaMiembro.class, idPremio + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

    /**
     * Método que asigna (incluye o excluye) una lista de miembros de un premio
     *
     * @param idPremio identificador del premio
     * @param listaIdMiembros lista de identificadores de miembros
     * @param indAccion tipo de accion (incluye/excluye)
     * @param usuario usuario en sesion
     * @param locale
     */
    public void includeExcludeBatchMiembro(String idPremio, List<String> listaIdMiembros, Character indAccion, String usuario, Locale locale) {
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }
        try {

            listaIdMiembros = listaIdMiembros.stream().distinct().collect(Collectors.toList());

            for (String idMiembro : listaIdMiembros) {
                em.getReference(Miembro.class, idMiembro).getIdMiembro();
            }

        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        //entidad de entidad archivada
        if (premio.getIndEstado().equals(Premio.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Premio");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea una valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            for (String idMiembro : listaIdMiembros) {
                em.merge(new PremioListaMiembro(idMiembro, idPremio, indAccion, date, usuario));
            }

        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        for (String idMiembro : listaIdMiembros) {
            bitacoraBean.logAccion(PremioListaMiembro.class, idPremio + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        }

    }

    /**
     * Método que elimina una lista de miembros de un premio
     *
     * @param idPremio identificador del premio
     * @param listaIdMiembros lista de identificadores de miembros
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeBatchMiembro(String idPremio, List<String> listaIdMiembros, String usuario, Locale locale) {
        Premio premio = em.find(Premio.class, idPremio);
        try {

            listaIdMiembros = listaIdMiembros.stream().distinct().collect(Collectors.toList());
            listaIdMiembros.stream().forEach((idMiembro) -> {
                em.getReference(Miembro.class, idMiembro).getIdMiembro();
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        //entidad de entidad archivada
        if (premio.getIndEstado().equals(Premio.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Premio");
        }

        for (String idMiembro : listaIdMiembros) {
            em.remove(em.getReference(PremioListaMiembro.class, new PremioListaMiembroPK(idMiembro, idPremio)));
        }
        em.flush();

        for (String idMiembro : listaIdMiembros) {
            bitacoraBean.logAccion(PremioListaMiembro.class, idPremio + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        }

    }

    /**
     * Método que obtiene un listado de miembros asignados a un premio en un
     * rango de respuesta y opcionalmente por el tipo de acción
     *
     * @param idPremio identificador del premio
     * @param indAccion indicador de tipo de accion deseado
     * @param estados
     * @param registro numero de registro inicial
     * @param generos
     * @param estadosCivil
     * @param educacion
     * @param contactoSMS
     * @param cantidad cantidad de registros deseados
     * @param contactoNotificacion
     * @param contactoEstado
     * @param busqueda
     * @param contactoEmail
     * @param tieneHijos
     * @param locale
     * @param filtros
     * @param ordenCampo
     * @param ordenTipo
     * @return Respuesta con el listado de miembros y su rango de respuesta
     */
    public Map<String, Object> getMiembros(String idPremio, String indAccion, List<String> estados, List<String> generos, List<String> estadosCivil, List<String> educacion, String contactoEmail, String contactoSMS, String contactoNotificacion, String contactoEstado, String tieneHijos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion de que el rango este dentro del valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
           throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Miembro> root = query.from(Miembro.class);
        
        query = Busquedas.getCriteriaQueryMiembros(cb, query, root, estados, generos, estadosCivil, educacion, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, ordenTipo, ordenCampo);
        
        Subquery<String> subquery = query.subquery(String.class);
        Root<PremioListaMiembro> rootSubquery = subquery.from(PremioListaMiembro.class);
        subquery.select(rootSubquery.get("premioListaMiembroPK").get("idMiembro"));
        if (indAccion==null) {
            subquery.where(cb.equal(rootSubquery.get("premioListaMiembroPK").get("idPremio"), idPremio));
            if (query.getRestriction()!=null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            }
        } else {
            switch(indAccion.toUpperCase().charAt(0)) {
                case Indicadores.INCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("premioListaMiembroPK").get("idPremio"), idPremio), cb.equal(rootSubquery.get("indTipo"), Indicadores.INCLUIDO));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.EXCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("premioListaMiembroPK").get("idPremio"), idPremio), cb.equal(rootSubquery.get("indTipo"), Indicadores.EXCLUIDO));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.DISPONIBLES: {
                    subquery.where(cb.equal(rootSubquery.get("premioListaMiembroPK").get("idPremio"), idPremio));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.equal(root.get("indEstadoMiembro"), Miembro.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                    } else {
                        query.where(cb.equal(root.get("indEstadoMiembro"), Miembro.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                    }
                    break;
                }
                default: {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "indAccion");
                }
            }
        }
        
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        //recurrido del resultado para la asignacion de informacion adicional (almacenada en keycloak)
       try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            resultado.stream().forEach((t) -> {
                Miembro miembro = (Miembro) t;
                try {
                    miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
                } catch(Exception e2) {
                    miembro.setNombreUsuario("Prueba");
                }
            });
        }
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }
}
