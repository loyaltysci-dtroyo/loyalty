package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaMisionEncuestaPregunta {
    private String idPregunta;
    
    private MisionEncuestaPregunta detallePregunta;
    
    private String respuesta;
    
    private String comentarios;

    public RespuestaMisionEncuestaPregunta() {
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(String idPregunta) {
        this.idPregunta = idPregunta;
    }

    public MisionEncuestaPregunta getDetallePregunta() {
        return detallePregunta;
    }

    public void setDetallePregunta(MisionEncuestaPregunta detallePregunta) {
        this.detallePregunta = detallePregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
}
