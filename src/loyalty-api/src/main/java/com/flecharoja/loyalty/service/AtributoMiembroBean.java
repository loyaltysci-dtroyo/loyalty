package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.AtributoDinamico;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.MiembroAtributo;
import com.flecharoja.loyalty.model.MiembroAtributoPK;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia
 * relacionado a atributos dinamicos / miembros
 *
 * @author svargas
 */
@Stateless
public class AtributoMiembroBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene los atributos dinámicos ligados a un miembro definidos
     * por un rango ya sea dado por el usuario o el default para devolver la
     * cantidad de tuplas
     *
     * @param idMiembro Identificador de un miembro valido
     * @param estados
     * @param tipoDato
     * @param cantidad de registros que se quieren por pagina
     * @param indVisible
     * @param busqueda
     * @param registro del cual se inicia la busqueda
     * @param indRequerido
     * @param ordenCampo
     * @param ordenTipo
     * @param locale
     * @param filtros
     * @return Lista de atributos dinamicos asociados a un miembro
     */
    public Map<String, Object> getAtributosDinamicosPorMiembro(String idMiembro, List<String> estados, List<String> tipoDato, String indVisible, String indRequerido, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<AtributoDinamico> root = query.from(AtributoDinamico.class);

        query = Busquedas.getCriteriaQueryAtributoDinamico(cb, query, root, estados, tipoDato, indVisible, indRequerido, busqueda, filtros, ordenTipo, ordenCampo);
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        for (Object object : resultado) {
            AtributoDinamico atributo = (AtributoDinamico) object;
            MiembroAtributo miembroAtributo = em.find(MiembroAtributo.class, new MiembroAtributoPK(idMiembro, atributo.getIdAtributo()));
            if (miembroAtributo == null) {
                atributo.setValorAtributoMiembro(atributo.getValorDefecto());
            } else {
                atributo.setValorAtributoMiembro(miembroAtributo.getValor());
            }
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

//        
        return respuesta;
    }

    /**
     * Metodo que obtiene los miembros de un atributo dinámico valido definido
     * en un rango de la cantidad de tuplas devueltas.
     *
     * @param idAtributo Identificador de un atributo valido
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return Lista de miembros asociados a un miembro
     */
    public Map<String, Object> getMiembrosAtributoDinamico(String idAtributo, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        List registros = new ArrayList();
        int total;
        try {
            registros = em.createNamedQuery("MiembroAtributo.findMiembrosByIdAtributo").setParameter("idAtributo", idAtributo).getResultList();
            total = registros.size();
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createNamedQuery("MiembroAtributo.findMiembrosByIdAtributo").setParameter("idAtributo", idAtributo)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * Metodo que asigna un atributo dinamico a un miembro por sus id's
     *
     * @param idAtributo Identificador de un atributo dinamico valido
     * @param idMiembro Identificador de un miembro valido
     * @param valor del atributo dinámico
     * @param usuario usuario en sesión
     * @param locale
     */
    public void assignAtributoDinamico(String idAtributo, String idMiembro, String valor, String usuario, Locale locale) {
        MiembroAtributo atributo = new MiembroAtributo(idMiembro, idAtributo);
        //se revisa si el valor no es nulo
        AtributoDinamico atributoDinamico = em.find(AtributoDinamico.class, idAtributo);

        //verificacion de valor segun el tipo de dato
        AtributoDinamico.TiposDato tipoDato = AtributoDinamico.TiposDato.get(atributoDinamico.getIndTipoDato());
        switch (tipoDato) {
            case BOOLEANO: {
                if (valor.equalsIgnoreCase("TRUE") || valor.equalsIgnoreCase("FALSE")) {
                    atributo.setFechaCreacion(new Date());
                    atributo.setValor(valor.toLowerCase());
                } else {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", valor);
                }
                break;
            }
            case FECHA: {
                Date fecha;
                try {
                    fecha = new Date(Long.parseLong(valor));
                } catch (NumberFormatException e) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", valor);
                }
                atributo.setFechaCreacion(new Date());
                atributo.setValor(String.valueOf(fecha.getTime()));
                break;
            }
            case NUMERICO: {
                BigDecimal valorATomar;
                try {
                    valorATomar = new BigDecimal(valor);
                } catch (NumberFormatException e) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", valor);
                }
                atributo.setFechaCreacion(new Date());
                atributo.setValor(valorATomar.toString());
                break;
            }
            case TEXTO: {
                atributo.setFechaCreacion(new Date());
                atributo.setValor(valor);
                break;
            }
        }

        try {
            em.persist(atributo);
            em.flush();
            bitacoraBean.logAccion(MiembroAtributo.class, idAtributo + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

    }

    /**
     * Metodo que revoca un atributo dinamico a un miembro por sus id's
     *
     * @param idAtributo Identificador de un atributo dinamico valido
     * @param idMiembro Identificador de un miembro valido
     * @param usuario usuario en sesion
     * @param locale
     */
    public void unassignAtributoDinamico(String idAtributo, String idMiembro, String usuario, Locale locale) {
        try {
            em.remove(em.createNamedQuery("MiembroAtributo.findByIdAtributoIdMiembro").setParameter("idAtributo", idAtributo).setParameter("idMiembro", idMiembro).getSingleResult());
            bitacoraBean.logAccion(MiembroAtributo.class, idAtributo + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "dynamic_attribute_member");
        }

    }

    /**
     * Metodo que asigna un atributo dinamico a un miembro por sus id's
     *
     * @param idAtributo Identificador de un atributo dinamico valido
     * @param idMiembro Identificador de un miembro valido
     * @param valor del atributo dinámico
     * @param usuario usuario en sesión
     * @param locale
     */
    public void updateAtributoDinamico(String idAtributo, String idMiembro, String valor, String usuario, Locale locale) {
        try {
            AtributoDinamico atributoDinamico = em.find(AtributoDinamico.class, idAtributo);
            //busqueda del miembroAtributo para el atributo dinamico
            MiembroAtributo miembroAtributo = em.find(MiembroAtributo.class, new MiembroAtributoPK(idMiembro, atributoDinamico.getIdAtributo()));
            //asignacion y comprobacion del valor segun el tipo de dato del atributo dinamico
            switch (AtributoDinamico.TiposDato.get(atributoDinamico.getIndTipoDato())) {
                case BOOLEANO: {
                    if (valor.equalsIgnoreCase("TRUE") || valor.equalsIgnoreCase("FALSE")) {
                        if (miembroAtributo == null) {
                            miembroAtributo = new MiembroAtributo(idMiembro, atributoDinamico.getIdAtributo(), new Date(), valor.toLowerCase());
                        } else {
                            miembroAtributo.setFechaCreacion(new Date());
                            miembroAtributo.setValor(valor.toLowerCase());
                        }
                    } else {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", valor);
                    }
                    break;
                }
                case FECHA: {
                    Date fecha;
                    try {
                        fecha = new Date(Long.parseLong(valor));
                    } catch (NumberFormatException e) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", valor);
                    }
                    if (miembroAtributo == null) {
                        miembroAtributo = new MiembroAtributo(idMiembro, atributoDinamico.getIdAtributo(), new Date(), String.valueOf(fecha.getTime()));
                    } else {
                        miembroAtributo.setFechaCreacion(new Date());
                        miembroAtributo.setValor(String.valueOf(fecha.getTime()));
                    }
                    break;
                }
                case NUMERICO: {
                    BigDecimal valorATomar;
                    try {
                        valorATomar = new BigDecimal(valor);
                    } catch (NumberFormatException e) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "value_attribute_invalid", valor);
                    }
                    if (miembroAtributo == null) {
                        miembroAtributo = new MiembroAtributo(idMiembro, atributoDinamico.getIdAtributo(), new Date(), valorATomar.toString());
                    } else {
                        miembroAtributo.setFechaCreacion(new Date());
                        miembroAtributo.setValor(valorATomar.toString());
                    }
                    break;
                }
                case TEXTO: {
                    if (miembroAtributo == null) {
                        miembroAtributo = new MiembroAtributo(idMiembro, atributoDinamico.getIdAtributo(), new Date(), valor);
                    } else {
                        miembroAtributo.setFechaCreacion(new Date());
                        miembroAtributo.setValor(valor);
                    }
                    break;
                }
            }

            em.merge(miembroAtributo);
            em.flush();
            bitacoraBean.logAccion(MiembroAtributo.class, idAtributo + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "dynamic_attribute_member");
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

    }

    /**
     * Método que verifica la existencia o no de una asignacion de atributo con
     * un miembro
     *
     * @param atributos lista de atributos relacionados con el miembro
     * @param idAtributo identificador del atributo
     * @return resultado de la operacion
     */
    public boolean existsAsignacion(List<MiembroAtributo> atributos, String idAtributo) {
        return atributos.stream().anyMatch((atributo) -> (atributo.getMiembroAtributoPK().getIdAtributo().equals(idAtributo)));
    }

    /**
     * Actualiza la informacion de un atributo dinamico a un miembro (uso interno)
     * 
     * @param idAtributo identificacion del atributo
     * @param idMiembro identificacion del miembro
     * @param valor valor del atributo dinamico
     * @param usuario identificador del administrador
     * @param locale locale
     */
    public void actualizarInfoMiembro(String idAtributo, String idMiembro, String valor, String usuario, Locale locale) {

        try {

            List<MiembroAtributo> atributos = em.createNamedQuery("MiembroAtributo.findByIdMiembro").setParameter("idMiembro", idMiembro).getResultList();
            MiembroAtributo atributo;
            if (!atributos.isEmpty() && existsAsignacion(atributos, idAtributo)) {
                atributo = em.find(MiembroAtributo.class, new MiembroAtributoPK(idMiembro, idAtributo));
                atributo.setValor(valor);
                em.merge(atributo);
                em.flush();
                bitacoraBean.logAccion(MiembroAtributo.class, idAtributo + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);

            } else {
                atributo = new MiembroAtributo(idMiembro, idAtributo);

                atributo.setFechaCreacion(Calendar.getInstance().getTime());
                atributo.setValor(valor);
                em.persist(atributo);
                em.flush();
                bitacoraBean.logAccion(MiembroAtributo.class, idAtributo + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);

            }
            //se revisa si el valor no es nulo
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "dynamic_attribute_member");
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }
}
