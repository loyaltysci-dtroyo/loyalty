/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.service.UbicacionBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * Clase que provee de metodos http RESTful para el acceso a funcionalidades del
 * manejo de ubicaciones
 *
 * @author wtencio
 */
@Api(value = "Ubicacion")
@Path("ubicacion")
public class UbicacionResource {

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @EJB
    UbicacionBean ubicacionBean;//EJB con los metodos de negocio para el manejo de ubicaciones

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion
    
    @Context
    HttpServletRequest request;

    /**
     * Metodo que acciona una tarea, la cual obtiene una ubicacion relacionada
     * al id que ingresa por parametro en el caso de error al ejecutar la tarea
     * se retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param idUbicacion identificador unico de la ubicacion
     * @return Lista de ubicaciones en formato JSON
     */
    @ApiOperation(value = "Obtener una ubicación por su identificador",
            response = Ubicacion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idUbicacion}")
    @Produces(MediaType.APPLICATION_JSON)
    public Ubicacion getUbicacionPorId(
            @ApiParam(value = "Identificador de la ubicación", required = true)
            @PathParam("idUbicacion") String idUbicacion) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.UBICACIONES_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return ubicacionBean.getUbicacionPorId(idUbicacion, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual inserta una nueva ubicacion dentro
     * de la base de datos, en el caso de error al ejecutar la tarea se retorna
     * un 500 (error interno del servidor) con un mensaje en el encabezado, de
     * no ser asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param ubicacion objeto con la información a registrar
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Registrar una nueva ubicación",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertUbicacion(
            @ApiParam(value = "Información de la ubicación", required = true)
            Ubicacion ubicacion) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.UBICACIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return ubicacionBean.insertUbicacion(ubicacion, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual actualiza una ubicacion ya
     * existente dentro de la base de datos, en el caso de error al ejecutar la
     * tarea se retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param ubicacion objeto con la información a actualizar
     */
    @ApiOperation(value = "Modificación de una ubicación")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void editUbicacion(
            @ApiParam(value = "Información de la ubicación", required = true)
            Ubicacion ubicacion) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.UBICACIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        ubicacionBean.editUbicacion(ubicacion, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual elimina una ubicacion que contenga
     * unicamente el estado de borrador en el caso de error al ejecutar la tarea
     * se retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param idUbicacion identificador unico de la ubicacion a actualizar el
     * estado
     */
    @ApiOperation(value = "Eliminación de una ubicación")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("eliminar/{idUbicacion}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeUbicacion(
            @ApiParam(value = "Identificador de la ubicación")
            @PathParam("idUbicacion") String idUbicacion) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.UBICACIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        ubicacionBean.removeUbicacion(idUbicacion, usuarioContext,request.getLocale());
    }



    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de todas las
     * ubicaciones segun su estado definidos dentro de un parametro con cada
     * estado seperado por "-", en el caso de error al ejecutar la tarea se
     * retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param estados
     * @param calendarizaciones
     * @param busqueda
     * @param filtros
     * @param ordenTipo
     * @param cantidad Parametro opcional con un valor por defecto de 10 que define la cantidad maxima de registros por respuesta
     * @param ordenCampo
     * @param registro Parametro opcional que define el numero de primer registro desde donde devolver el listado de entidades
     * @return Representacion del listado en formato JSON
     */
    @ApiOperation(value = "Obtener ubicaciones por estado, por defecto todos",
            responseContainer = "List",
            response = Ubicacion.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUbicaciones(
            @ApiParam(value = "Indicadores de estado") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de calendarizaciones") @QueryParam("calendarizacion") List<String> calendarizaciones,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.UBICACIONES_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = ubicacionBean.getUbicaciones(estados, calendarizaciones, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

}
