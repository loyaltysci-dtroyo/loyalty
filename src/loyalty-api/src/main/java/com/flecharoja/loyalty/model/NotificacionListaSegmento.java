package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NOTIFICACION_LISTA_SEGMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotificacionListaSegmento.countByIdNotificacion", query = "SELECT COUNT(n) FROM NotificacionListaSegmento n WHERE n.notificacionListaSegmentoPK.idNotificacion = :idNotificacion"),
    @NamedQuery(name = "NotificacionListaSegmento.getIdSegmentoByIdNotificacion", query = "SELECT n.notificacionListaSegmentoPK.idSegmento FROM NotificacionListaSegmento n WHERE n.notificacionListaSegmentoPK.idNotificacion = :idNotificacion")
})
public class NotificacionListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected NotificacionListaSegmentoPK notificacionListaSegmentoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_NOTIFICACION", referencedColumnName = "ID_NOTIFICACION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Notificacion notificacion;
    
    @JoinColumn(name = "ID_SEGMENTO", referencedColumnName = "ID_SEGMENTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Segmento segmento;

    public NotificacionListaSegmento() {
    }

    public NotificacionListaSegmento(String idNotificacion, String idSegmento, Date fechaCreacion, String usuarioCreacion) {
        this.notificacionListaSegmentoPK = new NotificacionListaSegmentoPK(idNotificacion, idSegmento);
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public NotificacionListaSegmentoPK getNotificacionListaSegmentoPK() {
        return notificacionListaSegmentoPK;
    }

    public void setNotificacionListaSegmentoPK(NotificacionListaSegmentoPK notificacionListaSegmentoPK) {
        this.notificacionListaSegmentoPK = notificacionListaSegmentoPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Notificacion getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(Notificacion notificacion) {
        this.notificacion = notificacion;
    }

    public Segmento getSegmento() {
        return segmento;
    }

    public void setSegmento(Segmento segmento) {
        this.segmento = segmento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notificacionListaSegmentoPK != null ? notificacionListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificacionListaSegmento)) {
            return false;
        }
        NotificacionListaSegmento other = (NotificacionListaSegmento) object;
        if ((this.notificacionListaSegmentoPK == null && other.notificacionListaSegmentoPK != null) || (this.notificacionListaSegmentoPK != null && !this.notificacionListaSegmentoPK.equals(other.notificacionListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.NotificacionListaSegmento[ notificacionListaSegmentoPK=" + notificacionListaSegmentoPK + " ]";
    }
    
}
