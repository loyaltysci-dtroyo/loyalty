/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PRODUCTO_LISTA_SEGMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoListaSegmento.findAll", query = "SELECT p FROM ProductoListaSegmento p"),
    @NamedQuery(name = "ProductoListaSegmento.findByIdSegmento", query = "SELECT p FROM ProductoListaSegmento p WHERE p.productoListaSegmentoPK.idSegmento = :idSegmento"),
    @NamedQuery(name = "ProductoListaSegmento.findByIdProducto", query = "SELECT p FROM ProductoListaSegmento p WHERE p.productoListaSegmentoPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoListaSegmento.countByIdProducto", query = "SELECT COUNT(p) FROM ProductoListaSegmento p WHERE p.productoListaSegmentoPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoListaSegmento.findSegmentosByIdProducto", query = "SELECT p.segmento FROM ProductoListaSegmento p WHERE p.productoListaSegmentoPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoListaSegmento.countSegmentosNotInProductoListaSegmento", query = "SELECT COUNT(s) FROM Segmento s WHERE s.indEstado = :estado AND s.idSegmento NOT IN (SELECT p.productoListaSegmentoPK.idSegmento FROM ProductoListaSegmento p WHERE p.productoListaSegmentoPK.idProducto = :idProducto)"),
    @NamedQuery(name = "ProductoListaSegmento.findSegmentosNotInProductoListaSegmento", query = "SELECT s FROM Segmento s WHERE s.indEstado = :estado AND s.idSegmento NOT IN (SELECT p.productoListaSegmentoPK.idSegmento FROM ProductoListaSegmento p WHERE p.productoListaSegmentoPK.idProducto = :idProducto)"),
    @NamedQuery(name = "ProductoListaSegmento.countByIdProductoIndTipo", query = "SELECT COUNT(p) FROM ProductoListaSegmento p WHERE p.productoListaSegmentoPK.idProducto = :idProducto AND p.indTipo = :indTipo"),
    @NamedQuery(name = "ProductoListaSegmento.findSegmentosByIdProductoIndTipo", query = "SELECT p.segmento FROM ProductoListaSegmento p WHERE p.productoListaSegmentoPK.idProducto = :idProducto AND p.indTipo = :indTipo")

})


public class ProductoListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoListaSegmentoPK productoListaSegmentoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_PRODUCTO", referencedColumnName = "ID_PRODUCTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    
    @JoinColumn(name = "ID_SEGMENTO", referencedColumnName = "ID_SEGMENTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Segmento segmento;

    public ProductoListaSegmento() {
    }

    public ProductoListaSegmento(ProductoListaSegmentoPK productoListaSegmentoPK) {
        this.productoListaSegmentoPK = productoListaSegmentoPK;
    }

    public ProductoListaSegmento(String idSegmento, String idProducto, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.productoListaSegmentoPK = new ProductoListaSegmentoPK(idSegmento, idProducto);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public ProductoListaSegmento(String idSegmento, String idProducto) {
        this.productoListaSegmentoPK = new ProductoListaSegmentoPK(idSegmento, idProducto);
    }

    @ApiModelProperty(hidden = true)
    public ProductoListaSegmentoPK getProductoListaSegmentoPK() {
        return productoListaSegmentoPK;
    }

    public void setProductoListaSegmentoPK(ProductoListaSegmentoPK productoListaSegmentoPK) {
        this.productoListaSegmentoPK = productoListaSegmentoPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Segmento getSegmento() {
        return segmento;
    }

    public void setSegmento(Segmento segmento) {
        this.segmento = segmento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoListaSegmentoPK != null ? productoListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoListaSegmento)) {
            return false;
        }
        ProductoListaSegmento other = (ProductoListaSegmento) object;
        if ((this.productoListaSegmentoPK == null && other.productoListaSegmentoPK != null) || (this.productoListaSegmentoPK != null && !this.productoListaSegmentoPK.equals(other.productoListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ProductoListaSegmento[ productoListaSegmentoPK=" + productoListaSegmentoPK + " ]";
    }
    
}
