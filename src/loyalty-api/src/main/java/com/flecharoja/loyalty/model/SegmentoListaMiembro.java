package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "SEGMENTO_LISTA_MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SegmentoListaMiembro.findIdSegmentosByIdMiembroIndTipo", query = "SELECT s.segmentoListaMiembroPK.idSegmento FROM SegmentoListaMiembro s WHERE s.segmentoListaMiembroPK.idMiembro = :idMiembro AND s.indTipo = :indTipo"),
    @NamedQuery(name = "SegmentoListaMiembro.findAll", query = "SELECT s FROM SegmentoListaMiembro s ORDER BY s.fechaCreacion"),
    @NamedQuery(name = "SegmentoListaMiembro.findMiembrosNotInSegmentoListaMiembro", query = "SELECT m FROM Miembro m WHERE m.idMiembro NOT IN (SELECT s.segmentoListaMiembroPK.idMiembro FROM SegmentoListaMiembro s WHERE s.segmentoListaMiembroPK.idSegmento = :idSegmento)"),
    @NamedQuery(name = "SegmentoListaMiembro.countMiembrosNotInSegmentoListaMiembro", query = "SELECT COUNT(m) FROM Miembro m WHERE m.idMiembro NOT IN (SELECT s.segmentoListaMiembroPK.idMiembro FROM SegmentoListaMiembro s WHERE s.segmentoListaMiembroPK.idSegmento = :idSegmento)"),
    @NamedQuery(name = "SegmentoListaMiembro.findByIdMiembro", query = "SELECT s FROM SegmentoListaMiembro s WHERE s.segmentoListaMiembroPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "SegmentoListaMiembro.findByIdSegmento", query = "SELECT s FROM SegmentoListaMiembro s WHERE s.segmentoListaMiembroPK.idSegmento = :idSegmento"),
    @NamedQuery(name = "SegmentoListaMiembro.findByIdSegmentoIdMiembro", query = "SELECT s FROM SegmentoListaMiembro s WHERE s.segmentoListaMiembroPK.idSegmento = :idSegmento AND s.segmentoListaMiembroPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "SegmentoListaMiembro.findByIndTipo", query = "SELECT s FROM SegmentoListaMiembro s WHERE s.indTipo = :indTipo"),
    @NamedQuery(name = "SegmentoListaMiembro.findByIdSegmentoIndTipo", query = "SELECT s FROM SegmentoListaMiembro s WHERE s.segmentoListaMiembroPK.idSegmento = :idSegmento AND s.indTipo = :indTipo"),
    @NamedQuery(name = "SegmentoListaMiembro.findMiembrosByIdSegmento", query = "SELECT s.miembro FROM SegmentoListaMiembro s WHERE s.segmentoListaMiembroPK.idSegmento = :idSegmento"),
    @NamedQuery(name = "SegmentoListaMiembro.findMiembrosByIdSegmentoIndTipo", query = "SELECT s.miembro FROM SegmentoListaMiembro s WHERE s.segmentoListaMiembroPK.idSegmento = :idSegmento AND s.indTipo = :indTipo"),
    @NamedQuery(name = "SegmentoListaMiembro.countByIdSegmento", query = "SELECT COUNT(s) FROM SegmentoListaMiembro s WHERE s.segmentoListaMiembroPK.idSegmento = :idSegmento"),
    @NamedQuery(name = "SegmentoListaMiembro.countByIdSegmentoIndTipo", query = "SELECT COUNT(s) FROM SegmentoListaMiembro s WHERE s.segmentoListaMiembroPK.idSegmento = :idSegmento AND s.indTipo = :indTipo")
})
public class SegmentoListaMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SegmentoListaMiembroPK segmentoListaMiembroPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;
    
    @JoinColumn(name = "ID_SEGMENTO", referencedColumnName = "ID_SEGMENTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Segmento segmento;

    public SegmentoListaMiembro() {
    }

    public SegmentoListaMiembro(String idMiembro, String idSegmento, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.segmentoListaMiembroPK = new SegmentoListaMiembroPK(idMiembro, idSegmento);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public SegmentoListaMiembroPK getSegmentoListaMiembroPK() {
        return segmentoListaMiembroPK;
    }

    public void setSegmentoListaMiembroPK(SegmentoListaMiembroPK segmentoListaMiembroPK) {
        this.segmentoListaMiembroPK = segmentoListaMiembroPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo!=null?Character.toUpperCase(indTipo):null;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public Segmento getSegmento() {
        return segmento;
    }

    public void setSegmento(Segmento segmento) {
        this.segmento = segmento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segmentoListaMiembroPK != null ? segmentoListaMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SegmentoListaMiembro)) {
            return false;
        }
        SegmentoListaMiembro other = (SegmentoListaMiembro) object;
        if ((this.segmentoListaMiembroPK == null && other.segmentoListaMiembroPK != null) || (this.segmentoListaMiembroPK != null && !this.segmentoListaMiembroPK.equals(other.segmentoListaMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.SegmentoListaMiembro[ segmentoListaMiembroPK=" + segmentoListaMiembroPK + " ]";
    }
    
}
