/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.UnidadesMedida;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.ExistenciasUbicacion;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB con los metodos de negocio necesarios para el manejo de premios
 *
 * @author wtencio
 */
@Stateless
public class PremioBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    UtilBean utilBean;//EJB con los metodos de negocio para el manejo de utilidades

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de bitacora

    /**
     * Método que registra un nuevo premio en la base de datos
     *
     * @param premio información del premio a insertar
     * @param usuario usuario en sesión
     * @param locale
     * @return respuesta con el identificador del premio
     */
    public String insertPremio(Premio premio, String usuario, Locale locale) {

        //establecimiento de atributos por defecto
        premio.setIndEstado(Premio.Estados.BORRADOR.getValue());
        premio.setTotalExistencias(0L);
        premio.setPrecioPromedio(0D);
        premio.setEfectividadIndCalendarizado(Premio.Efectividad.PERMANENTE.getValue());
        premio.setFechaCreacion(Calendar.getInstance().getTime());
        premio.setFechaModificacion(Calendar.getInstance().getTime());
        premio.setUsuarioCreacion(usuario);
        premio.setUsuarioModificacion(usuario);
        premio.setNombreInterno(premio.getNombreInterno().toUpperCase());
        premio.setNumVersion(1L);
        premio.setEncabezadoArte(premio.getNombre());
        premio.setDetalleArte(premio.getDescripcion());

        if (premio.getCantMinAcumulado() == null) {
            premio.setCantMinAcumulado(Double.valueOf(0));
        }
        if (existsCodigoInterno(premio.getCodPremio())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "internal_code_already_exists");
        }

        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (premio.getImagenArte() == null || premio.getImagenArte().trim().isEmpty()) {
            premio.setImagenArte(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                premio.setImagenArte(filesUtils.uploadImage(premio.getImagenArte(), MyAwsS3Utils.Folder.ARTE_PREMIO, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }
        //verificacion de atributos requeridos para la definicion de limites de respuesta
        if (premio.getIndRespuesta() != null && premio.getIndRespuesta()) {
            // si los valores son nulos se asumen valores por defecto (en lugar de lanzar exepcion)
            if (premio.getIndIntervaloTotal() != null && Premio.IntervalosTiempoRespuesta.get(premio.getIndIntervaloTotal()) == null || premio.getCantTotal() == null) {
                premio.setIndIntervaloTotal(Premio.IntervalosTiempoRespuesta.POR_SIEMPRE.getValue());
            }
            if (premio.getIndIntervaloMiembro() != null
                    && (Premio.IntervalosTiempoRespuesta.get(premio.getIndIntervaloMiembro()) == null || premio.getCantTotalMiembro() == null)) {
                premio.setIndIntervaloMiembro(Premio.IntervalosTiempoRespuesta.POR_SIEMPRE.getValue());
            }
            if (premio.getIndIntervaloRespuesta() != null
                    && (Premio.IntervalosTiempoRespuesta.get(premio.getIndIntervaloRespuesta()) == null || premio.getCantIntervaloRespuesta() == null)) {
                premio.setIndIntervaloRespuesta(Premio.IntervalosTiempoRespuesta.POR_SIEMPRE.getValue());
            }
        }

        //Verifica que si se estable la unidad de medida esa unidad exista
        if (premio.getUnidadMedida() != null) {
            UnidadesMedida unidad = em.find(UnidadesMedida.class, premio.getUnidadMedida().getIdUnidad());
            if (unidad == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "units_measure_not_found");
            }
        }
        try {
            em.persist(premio);
            em.flush();

        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (!premio.getImagenArte().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(premio.getImagenArte());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        bitacoraBean.logAccion(Premio.class, premio.getIdPremio(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);

        return premio.getIdPremio();
    }

    /**
     * Método que edita la información de un prmeio ya existente
     *
     * @param premio información actualizada
     * @param usuario usuario en sesión
     * @param locale
     */
    public void editPremio(Premio premio, String usuario, Locale locale) {
        Premio original = em.find(Premio.class, premio.getIdPremio());

        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }

        if (!original.getCodPremio().equals(premio.getCodPremio())) {
            if (existsCodigoInterno(premio.getCodPremio())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "internal_code_already_exists");
            }
        }

        String urlImagenOriginal = original.getImagenArte();//url imagen original
        String urlImagenNueva = null;//url imagen nueva
        //si el atributo de imagen viene nula (no deberia)
        if (premio.getImagenArte() == null || premio.getImagenArte().trim().isEmpty()) {
            //establecimiento de la imagen por defecto
            urlImagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            premio.setImagenArte(urlImagenNueva);
        } else { //ver si el valor en el atributo de imagen, difiere del almacenado
            if (!urlImagenOriginal.equals(premio.getImagenArte())) {//si la imagen es diferente a la almacenada
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    urlImagenNueva = filesUtils.uploadImage(premio.getImagenArte(), MyAwsS3Utils.Folder.ARTE_PREMIO, null);
                    premio.setImagenArte(urlImagenNueva);
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }

        //verificacion de que el posible cambio de esta sea valido  (Activo NO A Borrador) y de edicion de entidad archivada
        Character originalEstado = original.getIndEstado();
        Character newEstado = premio.getIndEstado();
        if ((originalEstado.equals(Premio.Estados.PUBLICADO.getValue()) && newEstado.equals(Premio.Estados.BORRADOR.getValue()))
                || originalEstado.equals(Premio.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "reward_not_change_state");
        }

        if (newEstado.equals(Premio.Estados.PUBLICADO.getValue())) {
            premio.setFechaPublicacion(Calendar.getInstance().getTime());
        } else if (newEstado.equals(Premio.Estados.ARCHIVADO.getValue())) {
            premio.setFechaArchivado(Calendar.getInstance().getTime());
        }

        if(premio.getEfectividadIndCalendarizado() == null){
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "efectividadIndCalendarizado");
        }
        //si es calendarizado, verifica las fechas no esten vacias
        if (premio.getEfectividadIndCalendarizado().equals(Premio.Efectividad.CALENDARIZADO.getValue())) {
            if (premio.getEfectividadFechaInicio() == null || premio.getEfectividadFechaFin() == null || premio.getEfectividadFechaInicio().compareTo(premio.getEfectividadFechaFin()) > 0) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "reward_badly_scheduled");
            }
            if (premio.getEfectividadIndSemana() != null && (premio.getEfectividadIndSemana().compareTo(1) < 0 || premio.getEfectividadIndSemana().compareTo(52) > 0)) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indSemanaRecurrencia");
                }

                if (premio.getEfectividadIndDias() != null) {
                    char[] validValues = {'L', 'M', 'K', 'J', 'V', 'S', 'D'};
                    premio.setEfectividadIndDias(premio.getEfectividadIndDias().toUpperCase());
                    for (char validValue : validValues) {
                        if (premio.getEfectividadIndDias().indexOf(validValue) != premio.getEfectividadIndDias().lastIndexOf(validValue)) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indDiaRecurrencia");
                        }
                    }
                }
        } else {
            if (!premio.getEfectividadIndCalendarizado().equals(Premio.Efectividad.PERMANENTE.getValue())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "efectividadIndCalendarizado");
            }
        }

        //Verifica que si se estable la unidad de medida esa unidad exista
        if (premio.getUnidadMedida() != null) {
            UnidadesMedida unidad = em.find(UnidadesMedida.class, premio.getUnidadMedida().getIdUnidad());
            if (unidad == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "units_measure_not_found");
            }
        }

        premio.setFechaModificacion(Calendar.getInstance().getTime());
        premio.setUsuarioModificacion(usuario);

         //verificacion de atributos requeridos para la definicion de limites de respuesta
        if (premio.getIndRespuesta() != null && premio.getIndRespuesta()) {
            if (Premio.IntervalosTiempoRespuesta.get(premio.getIndIntervaloTotal()) == null || premio.getCantTotal() == null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indIntervaloTotal, cantTotal");
            }
            if (premio.getIndIntervaloMiembro()!= null) {
                if (Premio.IntervalosTiempoRespuesta.get(premio.getIndIntervaloMiembro()) == null || premio.getCantTotalMiembro() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indIntervaloMiembro, cantTotalMiembro");
                }
                if (premio.getIndIntervaloRespuesta()!= null && (Premio.IntervalosTiempoRespuesta.get(premio.getIndIntervaloRespuesta()) == null || premio.getCantIntervaloRespuesta()== null)) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indIntervalo, cantIntervalo");
                }
            }
        }
        try {
            em.merge(premio);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//en caso de que alguna informacion no este valida o este "vieja"
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(Premio.class, premio.getIdPremio(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        if (urlImagenNueva != null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Método que elimina o archiva cualquiera que sea el caso un premio
     *
     * @param idPremio identificador del premio
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removePremio(String idPremio, String usuario, Locale locale) {
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }

        if (premio.getIndEstado().equals(Premio.Estados.BORRADOR.getValue())) {
            em.remove(premio);
            if (!premio.getImagenArte().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(premio.getImagenArte());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            bitacoraBean.logAccion(Premio.class, idPremio, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        } else if (premio.getIndEstado().compareTo(Premio.Estados.ARCHIVADO.getValue()) == 0) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Premio");
        } else {
            premio.setFechaArchivado(Calendar.getInstance().getTime());
            premio.setUsuarioModificacion(usuario);
            premio.setIndEstado(Premio.Estados.ARCHIVADO.getValue());

            em.merge(premio);
            bitacoraBean.logAccion(Premio.class, idPremio, usuario, Bitacora.Accion.ENTIDAD_ARCHIVADA.getValue(), locale);
        }
    }

    /**
     * Metodo que verifica si existe una entidad de premio con un nombre interno
     * igual a "nombre"
     *
     * @param nombre Valor del nombre interno a verificar
     * @return Respuesta con el valor booleano de la existencia del nombre
     * interno
     */
    public Boolean existsNombreInterno(String nombre) {
        return ((Long) em.createNamedQuery("Premio.countByNombreInterno").setParameter("nombreInterno", nombre.toUpperCase()).getSingleResult()) > 0;
    }

    /**
     * Metodo que verifica si existe una entidad de premio con un codigo interno
     * igual a "codigo"
     *
     * @param codPremio Valor del nombre interno a verificar
     * @return Respuesta con el valor booleano de la existencia del nombre
     * interno
     */
    private Boolean existsCodigoInterno(String codPremio) {
        return ((Long) em.createNamedQuery("Premio.countCodInterno").setParameter("codPremio", codPremio.toUpperCase()).getSingleResult()) > 0;
    }

    /**
     * Metodo que obtiene un listado de premios en un rango de numero de
     * registros y opcionalmente por estado
     *
     * @param estados
     * @param tipos
     * @param calendarizaciones
     * @param categoria
     * @param indAccionCategoria
     * @param indRespuesta
     * @param indEnvio
     * @param busqueda
     * @param ordenTipo
     * @param filtros
     * @param registro Numero de registro inicial
     * @param ordenCampo
     * @param cantidad Cantidad de registros en la respuesta
     * @param locale
     * @return Respuesta con un listado de premios y el rango de registros
     * obtenidos
     */
    public Map<String, Object> getPremios(List<String> estados, List<String> tipos, List<String> calendarizaciones, String categoria, String indAccionCategoria, String indRespuesta, String indEnvio, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int registro, int cantidad, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Premio> root = query.from(Premio.class);

        query = Busquedas.getCriteriaQueryPremios(cb, query, root, estados, tipos, calendarizaciones, indRespuesta, indEnvio, categoria, indAccionCategoria, busqueda, filtros, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();

        //retorno del resultado y encabezados sobre el rango
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Metodo que obtiene la informacion de un premio por el identificador de la
     * misma
     *
     * @param idPremio Identificador del premio
     * @param locale
     * @return Respuesta con la informacion de la promocion encontrada
     */
    public Premio getPremioPorId(String idPremio, Locale locale) {

        Premio premio = em.find(Premio.class, idPremio);
        //verificacion de que la entidad exista
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }

        return premio;
    }

    /**
     * obtencion de premios en la ubicacion central
     * 
     * @param busqueda parametro de busqueda a los premios
     * @param locale locale
     * @return lista de premios
     */
    public List<Premio> getPremiosUbicacionCentral(String busqueda, Locale locale) {
        String idUbicacion;
        List<Premio> premios = new ArrayList<>();
        int mes;
        int periodo;
        
        //obtencion de ubicacion central, periodo y mes
        List<ConfiguracionGeneral> confi = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
        if (confi.isEmpty()) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "general_configuration_not_found");
        } else {
            idUbicacion = confi.get(0).getUbicacionPrincipal().getIdUbicacion();
            mes = confi.get(0).getMes();
            periodo = confi.get(0).getPeriodo();
        }

        //obtencion de existencias en la ubicacion
        List<ExistenciasUbicacion> existencias = em.createNamedQuery("ExistenciasUbicacion.findByUbicacionCantidad").setParameter("idUbicacion", idUbicacion)
                .setParameter("mes", mes).setParameter("periodo", periodo).getResultList();

        //mapeo a premios y filtrado por busqueda
        if (busqueda != null) {
            premios = existencias.stream().map((ExistenciasUbicacion t) -> {
                Premio premio = em.find(Premio.class, t.getExistenciasUbicacionPK().getCodPremio());
                return premio;
            }).filter((Premio p) -> (p.getNombre().toLowerCase().contains(busqueda.toLowerCase()) || p.getCodPremio().contains(busqueda)))
                    .collect(Collectors.toList());
        } else {
            premios = existencias.stream().map((ExistenciasUbicacion t) -> {
                Premio premio = em.find(Premio.class, t.getExistenciasUbicacionPK().getCodPremio());
                return premio;
            }).collect(Collectors.toList());
        }
        return premios;
    }

}
