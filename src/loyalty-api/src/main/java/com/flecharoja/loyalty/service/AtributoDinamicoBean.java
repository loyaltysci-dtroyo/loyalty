/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.AtributoDinamico;
import com.flecharoja.loyalty.model.Bitacora;
;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 *
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * atributos dinamicos
 *
 * @author wtencio
 */


@Stateless
public class AtributoDinamicoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Método que obtiene la información de un atributo especifico, relacionado
     * con el id que pasa por parametro
     *
     * @param idAtributo identificador único del atributo dinámico
     * @param locale
     * @return objeto tipo AtributoDinamico con la información encontrada
     */
    public AtributoDinamico getAtributoPorId(String idAtributo, Locale locale) {
        AtributoDinamico atributo = new AtributoDinamico();
        try {
            atributo = (AtributoDinamico) em.createNamedQuery("AtributoDinamico.findByIdAtributo").setParameter("idAtributo", idAtributo).getSingleResult();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "dynamic_attribute_not_found");
        }
        return atributo;
    }

    /**
     * Método que registra un nuevo atributo dentro de la base de datos
     *
     * @param atributoDinamico objeto con la información a registrar
     * @param usuarioContext id del usuario que esta en sesion y es el encargado
     * de registrar el nuevo atributo
     * @param locale
     * @return id del atributo que se acaba de insertar
     */
    public String insertAtributoDinamico(AtributoDinamico atributoDinamico, String usuarioContext, Locale locale) {
        //verificacion de nombre interno no duplicado
        if (atributoDinamico.getNombreInterno() != null && existsNombreInterno(atributoDinamico.getNombreInterno())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "internal_name_exists");
        } else if (atributoDinamico.getNombreInterno() == null) {
            atributoDinamico.setNombreInterno(getNombreInterno(atributoDinamico.getNombre()));
        }
        
        //establecimiento de atributos por defecto
        atributoDinamico.setFechaCreacion(Calendar.getInstance().getTime());
        atributoDinamico.setUsuarioCreacion(usuarioContext);

        atributoDinamico.setFechaModificacion(Calendar.getInstance().getTime());
        atributoDinamico.setUsuarioModificacion(usuarioContext);
        atributoDinamico.setNumVersion(new Long(1));

        atributoDinamico.setIndTipoDato(Character.toUpperCase(atributoDinamico.getIndTipoDato()));
        atributoDinamico.setIndEstado(AtributoDinamico.Estados.BORRADOR.getValue());

        try {
            em.persist(atributoDinamico);
            em.flush();
            bitacoraBean.logAccion(AtributoDinamico.class, atributoDinamico.getIdAtributo(), usuarioContext, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        
        //retorne del id del atributo
        return atributoDinamico.getIdAtributo();
    }

    /**
     * Método que actualiza la información de un atributo ya existente
     *
     * @param atributoDinamico objeto con la información actualizada
     * @param usuarioContext id del usuario que se encuentra en sesion,
     * encargado de modificar el atributo
     */
    public void updateAtributoDinamico(AtributoDinamico atributoDinamico, String usuarioContext, Locale locale) {
        //busqueda del atributo referente
        AtributoDinamico temp;
        try {
            temp = em.find(AtributoDinamico.class, atributoDinamico.getIdAtributo());
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "dynamic_attribute_invalid");
        }
        if (temp == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "dynamic_attribute_not_found");
        }

        if (AtributoDinamico.Estados.get(atributoDinamico.getIndEstado()) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indEstado");
        }

        //verificacion de no cambio de estado ilegal
        if ((temp.getIndEstado().equals(AtributoDinamico.Estados.PUBLICADO.getValue()) && atributoDinamico.getIndEstado().equals(AtributoDinamico.Estados.BORRADOR.getValue()))
                || temp.getIndEstado().equals(AtributoDinamico.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", this.getClass().getSimpleName());
        }

        atributoDinamico.setFechaModificacion(Calendar.getInstance().getTime());
        atributoDinamico.setUsuarioModificacion(usuarioContext);
        try {
            em.merge(atributoDinamico);
            em.flush();
            bitacoraBean.logAccion(AtributoDinamico.class, atributoDinamico.getIdAtributo(), usuarioContext, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

    }

    /**
     * Verifica que si existe uno o mas atributos con el mismo valor en el nombre interno
     * 
     * @param nombreInterno valor a buscar/comparar
     * @return boolean
     */
    private boolean existsNombreInterno(String nombreInterno) {
        return ((long) em.createNamedQuery("AtributoDinamico.countNombreInterno").setParameter("nombreInterno", nombreInterno).getSingleResult()) > 0;
    }

    /**
     * Método que desactiva el atributo dinámico relacionado al id que ingresa
     * por parametro
     *
     * @param idAtributo identificador único del atributo dinámico
     * @param usuario usuario que esta en sesion
     * @param locale
     */
    public void desactiveAtributoDinamico(String idAtributo, String usuario, Locale locale) {
        AtributoDinamico atributo = new AtributoDinamico();
        //busqueda del atributo referente
        atributo = em.find(AtributoDinamico.class, idAtributo);
        if (atributo == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "dynamic_attribute_not_found");
        }

        //si el atributo esta en estado borrador se elimina, de caso contrario se archiva
        if (atributo.getIndEstado().equals(AtributoDinamico.Estados.BORRADOR.getValue())) {
            em.remove(atributo);
            em.flush();
            bitacoraBean.logAccion(AtributoDinamico.class, idAtributo, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        } else {

            if (atributo.getIndEstado().equals(AtributoDinamico.Estados.ARCHIVADO.getValue())) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", this.getClass().getSimpleName());
            }
            atributo.setIndEstado(AtributoDinamico.Estados.ARCHIVADO.getValue());
            atributo.setUsuarioModificacion(usuario);
            atributo.setFechaModificacion(Calendar.getInstance().getTime());

            em.merge(atributo);
            em.flush();
            bitacoraBean.logAccion(AtributoDinamico.class, idAtributo, usuario, Bitacora.Accion.ENTIDAD_DESACTIVADA.getValue(), locale);
        }

    }

    /**
     * Metodo que obtiene una lista de atributos dinamicos almacenados por tipo
     * de dato del atributo(texto, numerico...)
     *
     * @param estados
     * @param cantidad de registros que se quieren por pagina
     * @param indVisible
     * @param indRequerido
     * @param tipoDato
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @param filtros
     * @param ordenTipo
     * @param busqueda
     * @param ordenCampo
     * @return Lista de atributos dinamicos
     */
    public Map<String, Object> getAtributos(List<String> estados, List<String> tipoDato, String indVisible, String indRequerido, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<AtributoDinamico> root = query.from(AtributoDinamico.class);

        query = Busquedas.getCriteriaQueryAtributoDinamico(cb, query, root, estados, tipoDato, indVisible, indRequerido, busqueda, filtros, ordenTipo, ordenCampo);
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

//        
        return respuesta;
    }

    /**
     * Generacion de nombre interno a partir de un valor
     * 
     * @param nombre valor a transformar en equivalente nombre interno segun reglas
     * @return posible valor a usar como nombre interno
     */
    private String getNombreInterno(String nombre) {
        char[] caracteres = nombre.toCharArray();
        for (int i = 0; i < nombre.length() - 2; i++) {
            if (caracteres[i] == ' ' || caracteres[i] == '.' || caracteres[i] == ',') {
                caracteres[i + 1] = Character.toUpperCase(caracteres[i + 1]);
            }
        }
        String texto = new String(caracteres);
        String cadena = "";
        for (int x = 0; x < texto.length(); x++) {
            if (texto.charAt(x) != ' ') {
                cadena += texto.charAt(x);
            }
        }
        return cadena;
    }

}
