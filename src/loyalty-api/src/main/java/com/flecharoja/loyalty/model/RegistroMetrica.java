package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "REGISTRO_METRICA")
@NamedQueries({
    @NamedQuery(name = "RegistroMetrica.findAllUntilFecha", query = "SELECT r FROM RegistroMetrica r WHERE r.registroMetricaPK.fecha >= :fecha ORDER BY r.registroMetricaPK.fecha DESC"),
    @NamedQuery(name = "RegistroMetrica.findByIdMiembroUntilFecha", query = "SELECT r FROM RegistroMetrica r WHERE r.idMiembro = :idMiembro AND r.registroMetricaPK.fecha >= :fecha ORDER BY r.registroMetricaPK.fecha DESC"),
    @NamedQuery(name = "RegistroMetrica.findByIdMetricaUntilFecha", query = "SELECT r FROM RegistroMetrica r WHERE r.idMetrica = :idMetrica AND r.registroMetricaPK.fecha >= :fecha ORDER BY r.registroMetricaPK.fecha DESC")
})
public class RegistroMetrica implements Serializable {
    
    public enum TiposGane {
        BIENVENIDA("B"),
        TRANSACCION("T"),
        REVERSION("V"),
        MISION("M");
        
        private final String value;

        private TiposGane(String value) {
            this.value = value;
        }
    }
    
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RegistroMetricaPK registroMetricaPK;
    
    @Column(name = "ID_METRICA")
    private String idMetrica;
    
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;
    
    @Column(name = "IND_TIPO_GANE")
    private String indTipoGane;

    @Column(name = "CANTIDAD")
    private Double cantidad;

    @Column(name = "ID_MISION")
    private String idMision;
    
    @Column(name = "ID_TRANSACCION")
    private String idTransaccion;
    
    @Column(name = "DISPONIBLE_ACTUAL")
    private Double disponibleActual;

    public RegistroMetrica() {
    }

    public RegistroMetrica(Date fecha, String idMetrica, String idMiembro, TiposGane tipoGane, Double cantidad, String idTransaccion, String idMision) {
        this.registroMetricaPK = new RegistroMetricaPK(fecha, null);
        this.idMetrica = idMetrica;
        this.idMiembro = idMiembro;
        this.indTipoGane = tipoGane.value;
        this.cantidad = cantidad;
        this.idTransaccion = idTransaccion;
        this.idMision = idMision;
    }
    
    public RegistroMetrica(Date fecha, String idMetrica, String idMiembro, TiposGane tipoGane, Double cantidad, String idMision) {
        this.registroMetricaPK = new RegistroMetricaPK(fecha, null);
        this.idMetrica = idMetrica;
        this.idMiembro = idMiembro;
        this.indTipoGane = tipoGane.value;
        this.cantidad = cantidad;
        this.idMision = idMision;
    }

    public RegistroMetricaPK getRegistroMetricaPK() {
        return registroMetricaPK;
    }

    public void setRegistroMetricaPK(RegistroMetricaPK registroMetricaPK) {
        this.registroMetricaPK = registroMetricaPK;
    }

    public String getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIndTipoGane() {
        return indTipoGane;
    }

    public void setIndTipoGane(String indTipoGane) {
        this.indTipoGane = indTipoGane;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Double getDisponibleActual() {
        return disponibleActual;
    }

    public void setDisponibleActual(Double disponibleActual) {
        this.disponibleActual = disponibleActual;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (registroMetricaPK != null ? registroMetricaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegistroMetrica)) {
            return false;
        }
        RegistroMetrica other = (RegistroMetrica) object;
        if ((this.registroMetricaPK == null && other.registroMetricaPK != null) || (this.registroMetricaPK != null && !this.registroMetricaPK.equals(other.registroMetricaPK))) {
            return false;
        }
        return true;
    }
}