/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Grupo;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.service.GrupoMiembroBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase que provee de metodos http RESTful para el acceso a funcionalidades del
 * manejo de grupo-miembro
 *
 * @author wtencio
 */
@Api(value = "Grupo")
@Path("grupo")
public class GrupoMiembroResource {

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @EJB
    GrupoMiembroBean grupoMiembroBean;//EJB con los metodos de negocio para el manejo de la asociación de miembros a grupos

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion
    
    @Context
    HttpServletRequest request;


    /**
     * Metodo que acciona una tarea, la cual registra un miembro dentro de un
     * grupo especifico en el caso de error al ejecutar la tarea se retorna un
     * 500 (error interno del servidor) con un mensaje en el encabezado, de no
     * ser asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param idGrupo identificador único del grupo
     * @param idsMiembros lista de ids de los miembros que se quieren insertar a
     * un grupo
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Asignación de un grupo a una lista de miembros")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idGrupo}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void insertMiembroGrupoPorLista(
            @ApiParam(value = "Identificador del grupo", required = true)
            @PathParam("idGrupo") String idGrupo,
            @ApiParam(value = "Lista de id's de miembros", required = true) List<String> idsMiembros) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.GRUPOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        grupoMiembroBean.insertMiembroGrupoPorLista(idGrupo, idsMiembros, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual registra un miembro dentro de un
     * grupo especifico en el caso de error al ejecutar la tarea se retorna un
     * 500 (error interno del servidor) con un mensaje en el encabezado, de no
     * ser asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param idGrupo identificador único del grupo
     * @param idMiembro identificador único del miembro
     */
    @ApiOperation(value = "Asignación de un grupo a un miembro")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idGrupo}/miembro/{idMiembro}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void insertMiembroGrupo(
            @ApiParam(value = "Identificador del grupo", required = true)
            @PathParam("idGrupo") String idGrupo,
            @ApiParam(value = "Identificador del miembro")
            @PathParam("idMiembro") String idMiembro) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.GRUPOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        grupoMiembroBean.insertMiembroGrupo(idGrupo, idMiembro, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual elimina un miembro de un grupo
     * especifico en el caso de error al ejecutar la tarea se retorna un 500
     * (error interno del servidor) con un mensaje en el encabezado, de no ser
     * asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param idMiembro identificador unico del miembro a eliminar
     * @param idGrupo identificador del grupo
     */
    @ApiOperation(value = "Desasignación de un miembro de un grupo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idGrupo}/miembro/{idMiembro}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeMiembroGrupo(
            @ApiParam(value = "Identificador del miembro", required = true)
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Identificador del grupo", required = true)
            @PathParam("idGrupo") String idGrupo) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.GRUPOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        grupoMiembroBean.removeMiembroGrupo(idGrupo, idMiembro, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual elimina un miembro de un grupo
     * especifico en el caso de error al ejecutar la tarea se retorna un 500
     * (error interno del servidor) con un mensaje en el encabezado, de no ser
     * asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param idsMiembros lista de miembros que se quieren eliminar
     * @param idGrupo identificador del grupo
     * @return Resultado de la operacion
     */
    @ApiOperation(value = "Desasignación de una lista miembro de un grupo")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("{idGrupo}/remover-miembros")
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeMiembroGrupoPorLista(
            @ApiParam(value = "Identificador del grupo")
            @PathParam("idGrupo") String idGrupo,
            @ApiParam(value = "Lista de id's de miembros") List<String> idsMiembros) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.GRUPOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        grupoMiembroBean.removeMiembrosGrupoPorLista(idGrupo, idsMiembros, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de miembros
     * asociados a un grupo en particular en el caso de error al ejecutar la
     * tarea se retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param idGrupo identificador único del grupo
     * @param tipo
     * @param estados
     * @param generos
     * @param estadosCiviles
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param educaciones
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param contactoNotificacion
     * @param contactoEmail
     * @param tieneHijos
     * @param busqueda
     * @param ordenTipo
     * @param contactoSMS
     * @param ordenCampo
     * @param filtros
     * @param contactoEstado
     * @return Lista de miembros en formato JSON
     */
    @ApiOperation(value = "Obtener miembros de un grupo especifico",
            responseContainer = "List",
            response = Miembro.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idGrupo}/miembros")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMiembrosGrupo(  
            @ApiParam(value = "Identificador del grupo", required = true)
            @PathParam("idGrupo") String idGrupo,
            @ApiParam(value = "Indicador del tipo de lista de miembro") @QueryParam("tipo") String tipo,
            @ApiParam(value = "Indicadores de estado de miembros") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de genero de miembros") @QueryParam("genero") List<String> generos,
            @ApiParam(value = "Indicadores de estado civil de miembros") @QueryParam("estado-civil") List<String> estadosCiviles,
            @ApiParam(value = "Indicadores de educacion de miembros") @QueryParam("educacion") List<String> educaciones,
            @ApiParam(value = "Indicador booleano de contacto de email") @QueryParam("contacto-email") String contactoEmail,
            @ApiParam(value = "Indicador booleano de contacto de sms") @QueryParam("contacto-sms") String contactoSMS,
            @ApiParam(value = "Indicador booleano de contacto de notificacion") @QueryParam("contacto-notificacion") String contactoNotificacion,
            @ApiParam(value = "Indicador booleano de contacto de estado") @QueryParam("contacto-estado") String contactoEstado,
            @ApiParam(value = "Indicador booleano de tiene hijos") @QueryParam("tiene-hijos") String tieneHijos,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.GRUPOS_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = grupoMiembroBean.getMiembrosGrupo(idGrupo, tipo, estados, generos, estadosCiviles, educaciones, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Metodo acciona una tarea que obtiene la cantidad de miembros que existen
     * en un grupo, en el caso de error al ejecutar la tarea se retorna un 500
     * (error interno del servidor) con un mensaje en el encabezado, de no ser
     * asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param idGrupo identificador único del grupo
     * @return Representacion en JSON de la cantidad de miembros en un grupo
     */
    @ApiOperation(value = "Obtener la cantidad de miembros en un grupo",
            responseContainer = "List",
            response = Grupo.class
    )
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idGrupo}/cantidad-miembros")
    @Produces(MediaType.APPLICATION_JSON)
    public Long cantidadMiembrosGrupo(
            @ApiParam(value = "Identificador del grupo", required = true)
            @PathParam("idGrupo") String idGrupo) {
        
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.GRUPOS_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return grupoMiembroBean.cantidadMiembrosGrupo(idGrupo);
    }

    @GET
    @Path("{idGrupo}/miembros-excluidos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response miembrosNotInGrupo(
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro,
            @ApiParam(value = "Identificador del grupo")
            @PathParam("idGrupo") String idGrupo ) {
        return this.getMiembrosGrupo(idGrupo, Indicadores.DISPONIBLES+"", null, null, null, null, null, null, null, null,null , null, null, null, null, cantidad, registro);
    }

}
