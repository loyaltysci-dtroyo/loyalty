/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Rol;
import com.flecharoja.loyalty.model.Usuario;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import com.flecharoja.loyalty.util.MyKeycloakUtilsAdmin;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 *
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * usuario
 *
 * @author wtencio
 */
@Stateless
public class UsuarioBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    private BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que guarda en la base de datos la informacion de un nuevo usuario
     *
     * @param usuario Objeto con los valores del nuevo usuario
     * @param usuarioContext usuario que se encuentra en sesion
     * @param locale
     * @return resultado de la operacion
     */
    public String insertUsuario(Usuario usuario, String usuarioContext, Locale locale) {

        if (usuario.getContrasena() == null || usuario.getCorreo() == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "contraseña, correo");
        }

        if (existUserName(usuario.getNombreUsuario(),locale)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "user_username_exists");
        }
        if (existEmail(usuario.getCorreo())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "user_email_exists");
        }
        //registro las credenciales(contraseña) del nuevo usuario

        String usuarioID;
        try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
            usuarioID = keycloakAdmin.createUser(usuario.getNombreUsuario(), usuario.getCorreo(), usuario.getContrasena());
            keycloakAdmin.resetPasswordUser(usuarioID, usuario.getContrasena());
        }
        
        try {
            //si la imagen viene nula, se le establece un url de imagen predefinida
            if (usuario.getAvatar() == null || usuario.getAvatar().trim().isEmpty()) {
                usuario.setAvatar(Indicadores.URL_IMAGEN_PREDETERMINADA);
            } else {
                //si no, se le intenta subir a cloudfiles
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    usuario.setAvatar(filesUtils.uploadImage(usuario.getAvatar(), MyAwsS3Utils.Folder.AVATAR_USUARIO, null));
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }

            //registrando en la base de datos
            //registro el uuid en el usuario de la base de datos
            usuario.setIdUsuario(usuarioID);
            //se le pasa al usuario los valores de fecha de creacion, que seria la actual
            usuario.setFechaCreacion(Calendar.getInstance().getTime());
            usuario.setUsuarioCreacion(usuarioContext);
            //la fecha de modificacion y el usuario va a hacer los mismos que la de creacion por ser la primera vez
            //que se guarda en la base
            usuario.setFechaModificacion(Calendar.getInstance().getTime());
            usuario.setUsuarioModificacion(usuarioContext);
            //se pone el indicador de estado en "A"= activo
            usuario.setIndEstado(Usuario.Estados.ACTIVO.getValue());
            usuario.setNumVersion(new Long(1));

            try {
                em.persist(usuario);
                em.flush();
                bitacoraBean.logAccion(Usuario.class, usuario.getIdUsuario(), usuarioContext, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            } catch (ConstraintViolationException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
                //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
                if (!usuario.getAvatar().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                    try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                        filesUtils.deleteFile(usuario.getAvatar());
                    } catch (Exception ex) {
                        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                    }
                }
                try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                    keycloakAdmin.deleteUser(usuarioID);
                }

                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));

            }
            return usuario.getIdUsuario();
        } catch (MyException e) {
            try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                keycloakAdmin.deleteUser(usuarioID);
            }
            throw e;
        }

    }

    /**
     * Metodo que a partir de un id dado busca y retorna de ser posible el
     * usuario con el mismo id
     *
     * @param id Valor del id del usuario deseado
     * @param locale
     * @return Objeto con la informacion del usuario encontrado
     */
    public Usuario getUsuarioPorId(String id, Locale locale) {
        Usuario usuario = em.find(Usuario.class, id);
        if (usuario == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "user_not_found");
        }
        try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
            try {
                usuario.setNombreUsuario(keycloakAdmin.getUsernameUser(usuario.getIdUsuario()));
            }catch (Exception e) {
                usuario.setNombreUsuario("Admin");
            }
        }
        return usuario;
    }

    /**
     * Metodo que pone desactivo de la base de datos la informacion de un
     * usuario existente
     *
     * @param id Valor de identificacion del usuario a desactivar
     * @param usuarioContext id del usuario en sesion
     * @param locale
     */
    public void disableUser(String id, String usuarioContext, Locale locale) {

        //se busca el usuario en la base de datos
        Usuario usuario = em.find(Usuario.class, id);
        if (usuario == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "user_not_found");
        }
        try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
            keycloakAdmin.enableDisableUser(usuario.getIdUsuario(), false);
        }
        // se actualiza la informacion del usuario que modifico(en este caso puso inactivo) el usuario
        usuario.setFechaModificacion(Calendar.getInstance().getTime());
        usuario.setUsuarioModificacion(usuarioContext);
        usuario.setIndEstado(Usuario.Estados.INACTIVO.getValue());

        em.merge(usuario);
        bitacoraBean.logAccion(Usuario.class, id, usuarioContext, Bitacora.Accion.ENTIDAD_DESACTIVADA.getValue(), locale);

    }

    /**
     * Metodo que modifica en la base de datos la informacion de un usuario
     * existente
     *
     * @param usuario Objeto con la informacion actualizada del usuario
     * existente
     * @param usuarioContext id del usuario en sesion
     * @param locale
     */
    public void editUsuario(Usuario usuario, String usuarioContext, Locale locale) {
        Usuario temp = em.find(Usuario.class, usuario.getIdUsuario());
        if (temp == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "user_not_found");
        }
        String emailAnterior = temp.getCorreo();
        Character estadoAnterior = temp.getIndEstado();
        String currentUrl = (String) em.createNamedQuery("Usuario.getAvatarFromUsuario").setParameter("idUsuario", usuario.getIdUsuario()).getSingleResult();
        String newUrl = null;
        //si el atributo de imagen viene nula 
        if (usuario.getAvatar() == null || usuario.getAvatar().trim().isEmpty()) {
            usuario.setAvatar(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else //ver si el valor en el atributo de imagen, difiere del almacenado
        if (!currentUrl.equals(usuario.getAvatar())) {
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                newUrl = filesUtils.uploadImage(usuario.getAvatar(), MyAwsS3Utils.Folder.AVATAR_USUARIO, null);
                usuario.setAvatar(newUrl);
            } catch (Exception e){
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }
        if (usuario.getIndEstado().equals(Usuario.Estados.ACTIVO.getValue())) {
            try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                keycloakAdmin.enableDisableUser(usuario.getIdUsuario(), true);
            }
        } else {
            try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                keycloakAdmin.enableDisableUser(usuario.getIdUsuario(), false);
            }
        }

        if (usuario.getCorreo() == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "contraseña, correo");
        } else {
            if (!usuario.getCorreo().equals(temp.getCorreo())) {
                if (existEmail(usuario.getCorreo())) {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "user_email_exists");
                } else {
                    try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                        keycloakAdmin.editEmail(usuario.getIdUsuario(), usuario.getCorreo());
                    }
                }
            }
        }

        if (usuario.getContrasena() != null) {
            if (!validatePassword(usuario.getContrasena())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "pass_invalid");
            } else {
                try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                    keycloakAdmin.resetPasswordUser(usuario.getIdUsuario(), usuario.getContrasena());
                }
            }
        }

        //se determina valores necesarios
        usuario.setFechaModificacion(Calendar.getInstance().getTime());
        usuario.setUsuarioModificacion(usuarioContext);

        try {
            em.merge(usuario);
            em.flush();
            bitacoraBean.logAccion(Usuario.class, usuario.getIdUsuario(), usuarioContext, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            if (estadoAnterior.equals(Usuario.Estados.ACTIVO.getValue())) {
                try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                    keycloakAdmin.enableDisableUser(usuario.getIdUsuario(), true);
                }
            } else {
                try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                    keycloakAdmin.enableDisableUser(usuario.getIdUsuario(), false);
                }
            }

            try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                keycloakAdmin.editEmail(usuario.getIdUsuario(), emailAnterior);
            }
            
            if (newUrl != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(newUrl);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }

            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
        
        if (newUrl != null && !currentUrl.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(currentUrl);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Metodo que pone activo nuevamente de la base de datos la informacion de
     * un usuario existente
     *
     * @param id Valor de identificacion del usuario a activar
     * @param usuarioContext id del usuario en sesion
     * @param locale
     */
    public void activateUsuario(String id, String usuarioContext, Locale locale) {
        //se busca el usuario en la base de datos
        Usuario usuario = em.find(Usuario.class, id);
        if (usuario == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "user_not_found");
        }
        try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
            keycloakAdmin.enableDisableUser(usuario.getIdUsuario(), true);
        }
        //determinación de datos necesarios
        usuario.setFechaModificacion(Calendar.getInstance().getTime());
        usuario.setUsuarioModificacion(usuarioContext);
        usuario.setIndEstado(Usuario.Estados.ACTIVO.getValue());
        try {
            em.merge(usuario);
            bitacoraBean.logAccion(Usuario.class, id, usuarioContext, Bitacora.Accion.ENTIDAD_ACTIVADA.getValue(), locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                keycloakAdmin.enableDisableUser(usuario.getIdUsuario(), false);
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
    }

    /**
     * Metodo que obtiene una lista de usuarios almacenados por estado(s)
     *
     * @param estados
     * @param busqueda
     * @param filtros
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param ordenTipo
     * @param ordenCampo
     * @param locale
     * @return Lista de usuarios
     */
    public Map<String, Object> getUsuarios(List<String> estados, String busqueda, List<String> filtros, int registro, int cantidad, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Usuario> root = query.from(Usuario.class);

        query = Busquedas.getCriteriaQueryUsuarios(cb, query, root, estados, busqueda, filtros, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //recurrido del resultado para la asignacion de informacion adicional (almacenada en keycloak)
        try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
            resultado.stream().forEach((t) -> {
                Usuario usuario = (Usuario) t;
                try {
                    usuario.setNombreUsuario(keycloakAdmin.getUsernameUser(usuario.getIdUsuario()));
                } catch (Exception e2) {
                    usuario.setNombreUsuario("-");
                }
            });
        }
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;

    }

    /**
     * Método que verifica si el nombre de usuario que entra por parametro ya
     * existe en algún registro
     *
     * @param nombreUsuario cadena de caracteres
     * @param locale
     * @return true si existe o false si no existe
     */
    public Boolean existUserName(String nombreUsuario,Locale locale) {
        String idUsuario;
        try (MyKeycloakUtilsAdmin keycloakUtils = new MyKeycloakUtilsAdmin(locale)) {
            try {
                idUsuario = keycloakUtils.getIdUser(nombreUsuario);
            }catch(Exception e2) {
                    idUsuario = null;
            }
        }
        return idUsuario != null;
    }

    /**
     * obtencion de roles de usuario
     * 
     * @param idUsuario id de admin
     * @param cantidad paginacion
     * @param registro paginacion
     * @param locale locale
     * @return lista de roles
     */
    public Map<String, Object> getRolesPorUsuario(String idUsuario, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        Map<String, Object> respuesta = new HashMap<>();
        List<Rol> roles = new ArrayList<>();

        int total = 0;
        try {
            try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                roles = keycloakAdmin.getRolesUsuario(idUsuario);
            }
            total = roles.size();
            total -= total - 1 < 0 ? 0 : 1;

            //reduccion del numero de registro inicial dentro de un rango valido
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

        } catch (IllegalArgumentException e) {//posiblemente pueda ocurrir al establecer los parametros de registro y cantidad erroneos (ejm: menores a 0)
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", roles.subList(registro, registro + cantidad > roles.size() ? roles.size() : registro + cantidad));
        return respuesta;
    }

    /**
     * comprobacion de existencia de otro admin con el mismo correo
     * 
     * @param correo valor del correo electronico
     * @return boolean
     */
    private boolean existEmail(String correo) {
        return ((Long) em.createNamedQuery("Usuario.countCorreo").setParameter("correo", correo).getSingleResult()) > 0;
    }

    /**
     * comprobacion de contraseña valida y segura
     * 
     * @param password valor de contraseña
     * @return boolenan
     */
    public boolean validatePassword(String password) {
        Pattern pswNamePtrn
                = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=.,_]).{6,100})");
        Matcher mtch = pswNamePtrn.matcher(password);
        return mtch.matches();
    }

}
