package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_LISTA_UBICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MisionListaUbicacion.findUbicacionesByIdMision", query = "SELECT m.ubicacion FROM MisionListaUbicacion m WHERE m.misionListaUbicacionPK.idMision = :idMision ORDER BY m.fechaCreacion DESC"),
    @NamedQuery(name = "MisionListaUbicacion.countUbicacionesByIdMision", query = "SELECT COUNT(m) FROM MisionListaUbicacion m WHERE m.misionListaUbicacionPK.idMision = :idMision"),
    @NamedQuery(name = "MisionListaUbicacion.findUbicacionesByIdMisionIndTipo", query = "SELECT m.ubicacion FROM MisionListaUbicacion m WHERE m.misionListaUbicacionPK.idMision = :idMision AND m.indTipo = :indTipo ORDER BY m.fechaCreacion DESC"),
    @NamedQuery(name = "MisionListaUbicacion.countUbicacionesByIdMisionIndTipo", query = "SELECT COUNT(m) FROM MisionListaUbicacion m WHERE m.misionListaUbicacionPK.idMision = :idMision AND m.indTipo = :indTipo"),
    @NamedQuery(name = "MisionListaUbicacion.findUbicacionesByIndEstadoNotInListaByIdMision", query = "SELECT u FROM Ubicacion u WHERE u.indEstado = :indEstado AND u.idUbicacion NOT IN (SELECT m.misionListaUbicacionPK.idUbicacion FROM MisionListaUbicacion m WHERE m.misionListaUbicacionPK.idMision = :idMision) ORDER BY u.fechaModificacion DESC"),
    @NamedQuery(name = "MisionListaUbicacion.countUbicacionesByIndEstadoNotInListaByIdMision", query = "SELECT COUNT(u) FROM Ubicacion u WHERE u.indEstado = :indEstado AND u.idUbicacion NOT IN (SELECT m.misionListaUbicacionPK.idUbicacion FROM MisionListaUbicacion m WHERE m.misionListaUbicacionPK.idMision = :idMision)")
})
public class MisionListaUbicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MisionListaUbicacionPK misionListaUbicacionPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_MISION", referencedColumnName = "ID_MISION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Mision mision;
    
    @JoinColumn(name = "ID_UBICACION", referencedColumnName = "ID_UBICACION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ubicacion ubicacion;

    public MisionListaUbicacion() {
    }

    public MisionListaUbicacion(String idUbicacion, String idMision, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.misionListaUbicacionPK = new MisionListaUbicacionPK(idUbicacion, idMision);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public MisionListaUbicacionPK getMisionListaUbicacionPK() {
        return misionListaUbicacionPK;
    }

    public void setMisionListaUbicacionPK(MisionListaUbicacionPK misionListaUbicacionPK) {
        this.misionListaUbicacionPK = misionListaUbicacionPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo == null ? null : Character.toUpperCase(indTipo);
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Mision getMision() {
        return mision;
    }

    public void setMision(Mision mision) {
        this.mision = mision;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (misionListaUbicacionPK != null ? misionListaUbicacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionListaUbicacion)) {
            return false;
        }
        MisionListaUbicacion other = (MisionListaUbicacion) object;
        if ((this.misionListaUbicacionPK == null && other.misionListaUbicacionPK != null) || (this.misionListaUbicacionPK != null && !this.misionListaUbicacionPK.equals(other.misionListaUbicacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionListaUbicacion[ misionListaUbicacionPK=" + misionListaUbicacionPK + " ]";
    }
    
}
