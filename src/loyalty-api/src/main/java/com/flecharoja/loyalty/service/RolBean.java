/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Rol;
import com.flecharoja.loyalty.model.Usuario;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyKeycloakUtilsAdmin;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * * EJB encargado de ejecutar reglas de negocio y acciones de persistencia
 * sobre roles (keycloak)
 *
 * @author wtencio
 */
@Stateless
public class RolBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene un listado de roles almacenados en el servidor de
     * keycloak
     *
     * @param busqueda
     * @param cantidad
     * @param registro
     * @param locale
     * @return Una coleccion de roles encontrados
     */
    public Map<String, Object> getRoles(String busqueda, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        int total;
        List<Rol> roles = new ArrayList<>();
        List<Rol> resultado = new ArrayList<>();
        try {
            try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                roles = keycloakAdmin.getRoles();
            }
            if (busqueda != null) {
                for (Rol rol : roles) {
                    if (rol.getNombre().toLowerCase().contains(busqueda.toLowerCase())) {
                        resultado.add(rol);
                    }
                }
            } else {
                resultado = roles;
            }

            total = resultado.size();
            total -= total - 1 < 0 ? 0 : 1;

            //reduccion del numero de registro inicial dentro de un rango valido
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
        } catch (IllegalArgumentException e) {//posiblemente pueda ocurrir al establecer los parametros de registro y cantidad erroneos (ejm: menores a 0)
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado.subList(registro, registro + cantidad > resultado.size() ? resultado.size() : registro + cantidad));
        return respuesta;

    }

    /**
     * Metodo que a partir de un nombre dado busca y retorna de ser posible el
     * rol con el mismo nombre
     *
     * @param nombreRol Valor del nombre del rol deseado
     * @param locale
     * @return Objeto con la informacion del rol encontrado en keycloak
     */
    public Rol getRolPorNombre(String nombreRol, Locale locale) {
        try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
            return keycloakAdmin.getRol(nombreRol);
        }
    }

    /**
     * Metodo que guarda en el servidor de keycloak la informacion de un nuevo
     * rol
     *
     * @param rol Objeto con los valores del nuevo rol para keycloak
     * @param usuario usuario en sesion
     * @param locale
     */
    public void insertRol(Rol rol, String usuario, Locale locale) {

        if (rol.getDescripcion() == null) {
            rol.setDescripcion(rol.getNombre());
        }
        try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
            keycloakAdmin.createRol(rol.getNombre(), rol.getDescripcion());
        }
        bitacoraBean.logAccion(Rol.class, rol.getNombre(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);

    }

    /**
     * Metodo que modifica del servidor de keycloak la informacion de un rol
     * existente
     *
     * @param rol Objeto con la informacion actualizada del rol existente
     * @param usuario usuario en sesión
     * @param locale
     */
    public void editRol(Rol rol, String usuario, Locale locale) {
        if (rol.getDescripcion() == null) {
            rol.setDescripcion(rol.getNombre());
        }
        try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
            keycloakAdmin.editRol(rol.getNombre(), rol.getDescripcion());
        }
        bitacoraBean.logAccion(Rol.class, rol.getNombre(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);

    }

    /**
     * Metodo que elimina del servidor de keycloak la informacion de un rol
     * existente
     *
     * @param nombreRol Valor de identificacion del rol a eliminar
     * @param usuario usuario sesion
     * @param locale
     */
    public void removeRol(String nombreRol, String usuario, Locale locale) {
        try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
            keycloakAdmin.deleteRol(nombreRol);
        }
        bitacoraBean.logAccion(Rol.class, nombreRol, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);

    }

    /**
     * Método que obtiene los usuarios según el rol que ingresa por parametro
     *
     * @param nombreRol identificador del rol
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return lista de usuarios filtrados por el nombre del rol
     */
    public Map<String, Object> getUsuariosPorRol(String nombreRol, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List<Usuario> resultado = new ArrayList();
        int total = 0;
        try {

            //usuarios activos
            List<Usuario> usuarios = em.createNamedQuery("Usuario.findByIndEstado").setParameter("indEstado", Usuario.Estados.ACTIVO.getValue()).setFirstResult(registro).getResultList();
            for (Usuario usuario : usuarios) {

                List<Rol> listRoles = new ArrayList<>();
                try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                    listRoles = keycloakAdmin.getRolesUsuario(usuario.getIdUsuario());
                }

                for (Rol rol : listRoles) {
                    if (rol.getNombre().equalsIgnoreCase(nombreRol)) {
                        resultado.add(usuario);
                    }
                }
            }
            total = resultado.size();
            total -= total - 1 < 0 ? 0 : 1;

            //reduccion del numero de registro inicial dentro de un rango valido
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado.subList(registro, registro + cantidad > resultado.size() ? resultado.size() : registro + cantidad));
        return respuesta;
    }

    /**
     * Método que devuelve una lista de roles que estan disponibles para un
     * usuario especifico
     *
     * @param idUsuario identificador unico del usuario
     * @param busqueda
     * @param locale
     * @return lista de roles disponibles para el usuario
     */
    public Map<String, Object> getRolesDisponiblesUsuario(String idUsuario, String busqueda, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List<Rol> rolesDisponibles = new ArrayList<>();
        List<Rol> resultado = new ArrayList<>();
        int total;
        try {
            try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
                rolesDisponibles = keycloakAdmin.getRolesDisponiblesUsuario(idUsuario);
            }
            if (busqueda != null) {
                for (Rol rol : rolesDisponibles) {
                    if (rol.getNombre().toLowerCase().contains(busqueda.toLowerCase())) {
                        resultado.add(rol);
                    }
                }
            } else {
                resultado = rolesDisponibles;
            }

            total = resultado.size();
            total -= total - 1 < 0 ? 0 : 1;

            //reduccion del numero de registro inicial dentro de un rango valido
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado.subList(registro, registro + cantidad > resultado.size() ? resultado.size() : registro + cantidad));
        return respuesta;
    }

}
