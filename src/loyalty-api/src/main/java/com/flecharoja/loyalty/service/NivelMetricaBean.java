package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.GrupoNiveles;
import com.flecharoja.loyalty.model.MiembroMetricaNivel;
import com.flecharoja.loyalty.model.NivelMetrica;
import com.flecharoja.loyalty.model.ReglaMiembroAtb;
import com.flecharoja.loyalty.model.Bitacora;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Array;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * niveles de metrica
 *
 * @author svargas
 */
@Stateless
public class NivelMetricaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene un listado de niveles de metricas almacenadas
     *
     * @param idGrupoNivel identificador unico del grupo a los que pertenecen
     * los niveles
     * @return Respuesta con una coleccion de niveles encontrados
     */
    public List<NivelMetrica> getNiveles(String idGrupoNivel) {
        List<NivelMetrica> resultList = em.createNamedQuery("NivelMetrica.findNivelesByIdGrupo").setParameter("idGrupoNivel", idGrupoNivel).getResultList();
        Collections.sort(resultList, (NivelMetrica p1, NivelMetrica p2) -> (p1.getMetricaInicial().intValue()-p2.getMetricaInicial().intValue()));
        return resultList;
                
    }

    /**
     * Metodo que a partir de un id proveido busca y retorna de ser posible la
     * informacion de un nivel de metrica con el mismo id
     *
     * @param idNivel Valor del id del nivel de metrica deseada
     * @param idGrupoNivel identificador del grupo al que pertenece el nivel
     * @param locale
     * @return Respuesta con la informacion del nivel de la metrica encontrada
     */
    public NivelMetrica getNivel(String idNivel, String idGrupoNivel, Locale locale) {
        NivelMetrica nivel = em.find(NivelMetrica.class, idNivel);
        //verificacion que la entidad haya sido encontrado
        if (nivel == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "metric_level_not_found");
        }
        if (!nivel.getGrupoNiveles().getIdGrupoNivel().equals(idGrupoNivel)) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "metric_level_not_metric");
        }
        return nivel;
    }
    
     /**
     * Metodo que a partir de un id proveido busca y retorna de ser posible la
     * informacion de un nivel de metrica con el mismo id
     *
     * @param idNivel Valor del id del nivel de metrica deseada
     * @param locale
     * @return Respuesta con la informacion del nivel de la metrica encontrada
     */
    public NivelMetrica getNivel(String idNivel, Locale locale) {
        NivelMetrica nivel = em.find(NivelMetrica.class, idNivel);
        //verificacion que la entidad haya sido encontrado
        if (nivel == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "metric_level_not_found");
        }
        return nivel;
    }

    /**
     * Metodo que guarda en almacenamiento la informacion de un nuevo nivel de
     * metrica
     *
     * @param nivel Objeto con la informacion de un nuevo nivel
     * @param usuario usuario en sesión
     * @param grupo identificador del grupo de niveles
     * @param locale
     * @return Respuesta con el identificador asignado al nivel de metrica
     * almacenado
     */
    public String insertNivel(NivelMetrica nivel, String usuario, String grupo, Locale locale) {
        GrupoNiveles temp = em.find(GrupoNiveles.class, grupo);

        if (temp == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "group_levels_not_found");
        }
        
        if(existeNombre(temp.getIdGrupoNivel(), nivel.getNombre())){
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "metric_name_already_exists");
        }

        if (existeMetricaInicial(temp.getIdGrupoNivel(), nivel.getMetricaInicial())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "initial_metric_already_exists");
        }

        //establecimiento de atributos por defecto
        Date date = Calendar.getInstance().getTime();

        nivel.setUsuarioCreacion(usuario);
        nivel.setUsuarioModificacion(usuario);

        nivel.setFechaCreacion(date);
        nivel.setFechaModificacion(date);
        nivel.setNumVersion(new Long(1));
        nivel.setGrupoNiveles(temp);
        
        if (nivel.getMultiplicador()!=null && nivel.getMultiplicador()<=0 && nivel.getMultiplicador()>100) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "multiplicador");
        }

        try {
            em.persist(nivel);
            em.flush();
            bitacoraBean.logAccion(NivelMetrica.class, nivel.getIdNivel(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {//en caso de que la informacion del nivel no sea valido
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        return nivel.getIdNivel();
    }

    /**
     * Metodo que modifica en el almecenamiento la informacion de un nivel
     * existente
     *
     * @param nivel Objeto con la informacion nueva de un nivel existente
     * @param usuario usuario en sesión
     * @param locale
     */
    public void editNivel(NivelMetrica nivel, String usuario, Locale locale) {
        //verificacion que el nivel de metrica exista
        NivelMetrica tempNivel = em.find(NivelMetrica.class, nivel.getIdNivel());

        if (tempNivel == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "group_levels_not_found");
        }

        GrupoNiveles temp = em.find(GrupoNiveles.class, nivel.getGrupoNiveles().getIdGrupoNivel());

        if (tempNivel.getMetricaInicial().compareTo(nivel.getMetricaInicial()) != 0) {
            if (existeMetricaInicial(temp.getIdGrupoNivel(), nivel.getMetricaInicial())) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "initial_metric_already_exists");
            }
        }
        
        if (tempNivel.getNombre().compareTo(nivel.getNombre()) != 0) {
            if (existeNombre(temp.getIdGrupoNivel(), nivel.getNombre())) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "metric_name_already_exists");
            }
        }

        //establecimiento de valores por defecto
        nivel.setUsuarioModificacion(usuario);
        nivel.setFechaModificacion(Calendar.getInstance().getTime());
        
        if (nivel.getMultiplicador()!=null && nivel.getMultiplicador()<=0 && nivel.getMultiplicador()>100) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "multiplicador");
        }

        try {
            em.merge(nivel);
            em.flush();
            bitacoraBean.logAccion(NivelMetrica.class, nivel.getIdNivel(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

    }

    /**
     * Metodo que elimina del almacenamiento la informacion de una metrica
     * existente
     *
     * @param idNivel Valor de identificacion de la metrica a eliminar
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeNivel(String idNivel, String usuario, Locale locale) {

        List<ReglaMiembroAtb> reglaMiembro = em.createNamedQuery("ReglaMiembroAtb.findByIdNivelMetrica").setParameter("idNivel", idNivel).getResultList();

        if (!reglaMiembro.isEmpty()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "metric_level_used");
        }

        List<MiembroMetricaNivel> miembroMetrica = em.createNamedQuery("MiembroMetricaNivel.findByIdNivel").setParameter("idNivel", idNivel).getResultList();

        if (!miembroMetrica.isEmpty()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "metric_level_used");
        }

        NivelMetrica nivel = em.find(NivelMetrica.class, idNivel);
        if (nivel == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "metric_level_not_found");
        }
        em.remove(nivel);
        bitacoraBean.logAccion(NivelMetrica.class, idNivel, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

    /**
     * Método que verifica la existencia de una metrica inicial
     *
     * @param idGrupoNivel
     * @param metricaInicial
     * @return
     */
    private boolean existeMetricaInicial(String idGrupoNivel, Double metricaInicial) {
        return ((Long) em.createNamedQuery("NivelMetrica.countNivelGrupo").setParameter("idGrupoNivel", idGrupoNivel).setParameter("metricaInicial", metricaInicial).getSingleResult()) > 0;
    }
    
    private boolean existeNombre(String idGrupoNivel, String nombre){
        return ((Long) em.createNamedQuery("NivelMetrica.countNombre").setParameter("idGrupoNivel", idGrupoNivel).setParameter("nombre", nombre).getSingleResult()) > 0;
    }

    /**
     * Método que devuelve las metricas por nivel
     *
     * @param idGrupoNivel
     * @return
     */
    public Map<String, Object> getMetricasPorNivel(String idGrupoNivel) {
        Map<String, Object> respuesta = new HashMap<>();

        GrupoNiveles grupo = (GrupoNiveles) em.createNamedQuery("GrupoNiveles.findByIdGrupoNivel").setParameter("idGrupoNivel", idGrupoNivel).getSingleResult();
        respuesta.put("listaMetricas", grupo.getMetricaList());
        return respuesta;
    }
}
