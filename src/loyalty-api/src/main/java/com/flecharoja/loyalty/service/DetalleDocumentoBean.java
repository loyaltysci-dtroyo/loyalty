/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.DocumentoDetalle;
import com.flecharoja.loyalty.model.DocumentoInventario;
import com.flecharoja.loyalty.model.ExistenciasUbicacion;
import com.flecharoja.loyalty.model.ExistenciasUbicacionPK;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.util.Indicadores;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class DetalleDocumentoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;

    /**
     * Método que registra un nuevo detalle de un documento
     *
     * @param detalle informacion del detalle del documento
     * @param numDocumento identificador del documento
     * @param usuario en sesión
     * @param locale
     */
    public void nuevoDetalle(DocumentoDetalle detalle, String numDocumento, String usuario, Locale locale) {
        //busco el documento al que pertenece el detalle 
        DocumentoInventario documento = em.find(DocumentoInventario.class, numDocumento);
        if (documento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "document_not_found");
        }
        //verifica que la cantidad y el cod de premio no sean nulos
        if (detalle.getCantidad() == null || detalle.getCodPremio() == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "cantidad, precio");
        }

        if (existeDetalle(numDocumento, detalle.getCodPremio().getIdPremio())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "detail_document_exists");
        }

        //verifica que las existencias de ese premio en ese ubicacion en periodo actual alcancen para el ajuste
        if (documento.getTipo().equals(DocumentoInventario.Tipos.AJUSTE_MENOS.getValue()) || documento.getTipo().equals(DocumentoInventario.Tipos.INTER_UBICACION.getValue())) {
            //si existe en el mes actual y año actual un registro actualizo
            List<ConfiguracionGeneral> configuracion = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
            if (configuracion.isEmpty()) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "general_configuration_not_found");
            }
            ConfiguracionGeneral config = configuracion.get(0);
            ExistenciasUbicacion existenciaActual = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(documento.getUbicacionOrigen().getIdUbicacion(), detalle.getCodPremio().getIdPremio(), config.getMes(), config.getPeriodo()));
            if (existenciaActual == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "no_stock", detalle.getCodPremio().getNombre(), documento.getUbicacionOrigen().getNombre());
            }
            int disponibilidad = existenciaActual.getSaldoActual();
            if (disponibilidad < detalle.getCantidad().intValue() || disponibilidad - detalle.getCantidad().intValue() < 0) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "insufficient_existence");
            }
        }else if(documento.getTipo().equals(DocumentoInventario.Tipos.COMPRA.getValue())){
            //si existe en el mes actual y año actual un registro actualizo
            List<ConfiguracionGeneral> configuracion = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
            if (configuracion.isEmpty()) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "general_configuration_not_found");
            }
            Integer saldoActual ;
            ConfiguracionGeneral config = configuracion.get(0);
            ExistenciasUbicacion existenciaActual = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(documento.getUbicacionOrigen().getIdUbicacion(), detalle.getCodPremio().getIdPremio(), config.getMes(), config.getPeriodo()));
            if (existenciaActual == null) {
                saldoActual = 0;
            }else{
                saldoActual = existenciaActual.getSaldoActual();
            }
            Double precioPromedioActual = detalle.getCodPremio().getPrecioPromedio();
            int nuevoSaldo = detalle.getCantidad().intValue()+saldoActual;
            Double precioPromedio = ((saldoActual*precioPromedioActual)+(detalle.getCantidad().intValue()*detalle.getPrecio().intValue()))/nuevoSaldo;
            detalle.setPromedio(precioPromedio);
        }
        //seteo de valores ocupados
        detalle.setNumDocumento(documento);
        detalle.setFechaCreacion(Calendar.getInstance().getTime());
        detalle.setFechaModificacion(Calendar.getInstance().getTime());
        detalle.setUsuarioCreacion(usuario);
        detalle.setUsuarioModificacion(usuario);
        detalle.setNumVersion(1L);

        try {
            em.persist(detalle);
            em.flush();
            bitacoraBean.logAccion(DocumentoDetalle.class, numDocumento, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que elimina un detalle de un documento especifico, mientras el
     * documento no este cerrado
     *
     * @param numDocumento
     * @param idDetalleDocumento
     * @param usuario
     * @param locale
     */
    public void removeDetalle(String numDocumento, String idDetalleDocumento, String usuario, Locale locale) {
        DocumentoInventario documento = em.find(DocumentoInventario.class, numDocumento);
        //verifico que el documento exista
        if (documento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "document_not_found");
        }
        //verifica si el documento ya esta cerrado no se puede borrar nada
        if (documento.getIndCerrado()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "document_closed");
        }

        DocumentoDetalle detalle = em.find(DocumentoDetalle.class,idDetalleDocumento);
        //verifico que el detalle exista
        if (detalle == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "document_detail_not_found");
        }

        //si los documentos no existen no deja continuar
        if (!detalle.getNumDocumento().getNumDocumento().equals(numDocumento)) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "document_not_found");
        }

        //elimino el detalle
        em.remove(detalle);
        em.flush();
        bitacoraBean.logAccion(DocumentoDetalle.class, numDocumento + "/" + idDetalleDocumento, usuario, Bitacora.Accion.ENTIDAD_DESACTIVADA.getValue(),locale);

    }

    /**
     * Método que actualiza la informacion (cantidad y premio) del detalle del
     * documento
     *
     * @param detalle informacion del documento
     * @param numDocumento identificador del documento
     * @param usuario en sesión
     * @param locale
     */
    public void updateDetalle(DocumentoDetalle detalle, String numDocumento, String usuario,Locale locale) {
        DocumentoInventario documento = em.find(DocumentoInventario.class, detalle.getNumDocumento().getNumDocumento());
        //verifico que el documento exista
        if (documento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "document_not_found");
        }
        //verifica si el documento ya esta cerrado no se puede borrar nada
        if (documento.getIndCerrado()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "document_closed");
        }
        //si los documentos no existen no deja continuar
        if (!detalle.getNumDocumento().getNumDocumento().equals(numDocumento)) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "document_not_found");
        }

        DocumentoDetalle original = em.find(DocumentoDetalle.class, detalle.getNumDetalleDocumento());
        //verifica que el detalle exista
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "document_detail_not_found");
        }
        //verifica que la cantidad y el cod premio no sean nulos
        if (detalle.getCantidad() == null || detalle.getCodPremio() == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "cantidad, precio");
        }
        //verifica que las existencias de ese premio en ese ubicacion en periodo actual alcancen para el ajuste
        if (documento.getTipo().equals(DocumentoInventario.Tipos.AJUSTE_MENOS.getValue()) || documento.getTipo().equals(DocumentoInventario.Tipos.INTER_UBICACION.getValue())) {
            Calendar calendar = Calendar.getInstance();
            ExistenciasUbicacion existenciaActual = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(documento.getUbicacionOrigen().getIdUbicacion(), detalle.getCodPremio().getIdPremio(), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR)));
            int disponibilidad = (existenciaActual.getSaldoInicial()+existenciaActual.getEntradas())-existenciaActual.getSalidas();
            if (disponibilidad < detalle.getCantidad().intValue() || disponibilidad - detalle.getCantidad().intValue() < 0) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "insufficient_existence");
            }
        }else if(documento.getTipo().equals(DocumentoInventario.Tipos.COMPRA.getValue())){
             //si existe en el mes actual y año actual un registro actualizo
            List<ConfiguracionGeneral> configuracion = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
            if (configuracion.isEmpty()) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "general_configuration_not_found");
            }
            Integer saldoActual ;
            ConfiguracionGeneral config = configuracion.get(0);
            ExistenciasUbicacion existenciaActual = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(documento.getUbicacionOrigen().getIdUbicacion(), detalle.getCodPremio().getIdPremio(), config.getMes(), config.getPeriodo()));
            if (existenciaActual == null) {
                saldoActual = 0;
            }else{
                saldoActual = existenciaActual.getSaldoActual();
            }
            Double precioPromedioActual = detalle.getCodPremio().getPrecioPromedio();
            int nuevoSaldo = detalle.getCantidad().intValue()+saldoActual;
            Double precioPromedio = ((saldoActual*precioPromedioActual)+(detalle.getCantidad().intValue()*detalle.getPrecio().intValue()))/nuevoSaldo;
            detalle.setPromedio(precioPromedio);
        }

        //seteo valores importantes
        detalle.setUsuarioModificacion(usuario);
        detalle.setFechaModificacion(Calendar.getInstance().getTime());
        try {
            em.merge(detalle);
            em.flush();
            bitacoraBean.logAccion(DocumentoDetalle.class, detalle.getNumDocumento() + "/" + detalle.getNumDetalleDocumento(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que obtiene una lista de detalles de un documento especifico
     *
     * @param numDocumento identificador del documento
     * @param cantidad de registros máximos por página
     * @param registro por el cual se desea comenzar la búsqueda
     * @param locale
     * @return lista de detalles
     */
    public Map<String, Object> getDetallesDocumento(String numDocumento, int cantidad, int registro,Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }

        Map<String, Object> respuesta = new HashMap<>();
        Long total;
        List<DocumentoDetalle> detalles = new ArrayList<>();
        try {
            total = (Long) em.createNamedQuery("DocumentoDetalle.countByNumDocumento").setParameter("numDocumento", numDocumento).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            detalles = em.createNamedQuery("DocumentoDetalle.findByNumDocumento")
                    .setParameter("numDocumento", numDocumento)
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {//posiblemente pueda ocurrir al establecer los parametros de registro y cantidad erroneos (ejm: menores a 0)
           throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", detalles);
        return respuesta;
    }

    /**
     * Método que devuelve la informacion de un detalle especifico
     *
     * @param numDocumento identificador el documento
     * @param idDetalle identificador del detalle
     * @param locale
     * @return informacion del detalle
     */
    public DocumentoDetalle getDetalle(String numDocumento, String idDetalle,Locale locale) {
        DocumentoDetalle detalle = em.find(DocumentoDetalle.class, idDetalle);
        if (detalle == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "document_detail_not_found");
        }
        return detalle;
    }

    /**
     * Método que obtiene un listado de premios que se encuentran en una
     * ubicacion, los cuales seran los unicos en poder ajustar en mas o menos o
     * dar por interubicacion
     *
     * @param idUbicacion identificador de la ubicacion
     * @param cantidad de registros maximos por página
     * @param registro por el cual se comienza la busqueda
     * @param locale
     * @return lista de premios disponibles
     */
    public Map<String, Object> getPremiosDisponiblesUbicacion(String idUbicacion, int cantidad, int registro,Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }

        Map<String, Object> respuesta = new HashMap<>();
        List<Premio> premios = new ArrayList<>();
        List<Premio> resultado = new ArrayList<>();
        int total;
        ConfiguracionGeneral configuracion = em.find(ConfiguracionGeneral.class, new Long(0));
        if(configuracion == null){
             throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "general_configuration_not_found");
        }
        Integer mes = configuracion.getMes();
        Integer periodo = configuracion.getPeriodo();

        try {
            premios = em.createNamedQuery("ExistenciasUbicacion.findPremiosByUbicacion")
                    .setParameter("ubicacion", idUbicacion)
                    .setParameter("mes", mes)
                    .setParameter("periodo", periodo)
                    .getResultList();
            resultado = premios.stream()
                    .filter((p)->p.getIndTipoPremio().equals(Premio.Tipo.PRODUCTO.getValue()))
                    .collect(Collectors.toList());
            //obtencion del total de premios resultantes
            total = resultado.size();
            total -= total - 1 < 0 ? 0 : 1;

            //reduccion del numero de registro inicial dentro de un rango valido
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
        } catch (IllegalArgumentException e) {//posiblemente pueda ocurrir al establecer los parametros de registro y cantidad erroneos (ejm: menores a 0)
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado.subList(registro, registro + cantidad > resultado.size() ? resultado.size() : registro + cantidad));
        return respuesta;

    }

    public boolean existeDetalle(String numDocumento, String idPremio) {
        return ((Long) em.createNamedQuery("DocumentoDetalle.findByNumDocumentoIdPremio").setParameter("codPremio", idPremio).setParameter("numDocumento", numDocumento).getSingleResult()) > 0;
    }

}
