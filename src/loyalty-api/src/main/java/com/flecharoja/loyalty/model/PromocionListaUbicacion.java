package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PROMOCION_LISTA_UBICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PromocionListaUbicacion.findAll", query = "SELECT p FROM PromocionListaUbicacion p"),
    @NamedQuery(name = "PromocionListaUbicacion.findByIndEstadoUbicacionesByIndEstadoNotInPromocionListaUbicacion", query = "SELECT u FROM Ubicacion u WHERE u.indEstado = :indEstado AND u.idUbicacion NOT IN (SELECT p.promocionListaUbicacionPK.idUbicacion FROM PromocionListaUbicacion p WHERE p.promocionListaUbicacionPK.idPromocion = :idPromocion)"),
    @NamedQuery(name = "PromocionListaUbicacion.countByIndEstadoUbicacionesByIndEstadoNotInPromocionListaUbicacion", query = "SELECT COUNT(u) FROM Ubicacion u WHERE u.indEstado = :indEstado AND u.idUbicacion NOT IN (SELECT p.promocionListaUbicacionPK.idUbicacion FROM PromocionListaUbicacion p WHERE p.promocionListaUbicacionPK.idPromocion = :idPromocion)"),
    @NamedQuery(name = "PromocionListaUbicacion.findByIdUbicacion", query = "SELECT p FROM PromocionListaUbicacion p WHERE p.promocionListaUbicacionPK.idUbicacion = :idUbicacion"),
    @NamedQuery(name = "PromocionListaUbicacion.findByIdPromocion", query = "SELECT p FROM PromocionListaUbicacion p WHERE p.promocionListaUbicacionPK.idPromocion = :idPromocion"),
    @NamedQuery(name = "PromocionListaUbicacion.findUbicacionByIdPromocion", query = "SELECT p.ubicacion FROM PromocionListaUbicacion p WHERE p.promocionListaUbicacionPK.idPromocion = :idPromocion"),
    @NamedQuery(name = "PromocionListaUbicacion.findUbicacionByIdPromocionIndTipo", query = "SELECT p.ubicacion FROM PromocionListaUbicacion p WHERE p.promocionListaUbicacionPK.idPromocion = :idPromocion AND p.indTipo = :indTipo"),
    @NamedQuery(name = "PromocionListaUbicacion.countByIdPromocion", query = "SELECT COUNT(p) FROM PromocionListaUbicacion p WHERE p.promocionListaUbicacionPK.idPromocion = :idPromocion"),
    @NamedQuery(name = "PromocionListaUbicacion.countByIdPromocionIndTipo", query = "SELECT COUNT(p) FROM PromocionListaUbicacion p WHERE p.promocionListaUbicacionPK.idPromocion = :idPromocion AND p.indTipo = :indTipo"),
    @NamedQuery(name = "PromocionListaUbicacion.findByIdPromocionIdUbicacion", query = "SELECT p FROM PromocionListaUbicacion p WHERE p.promocionListaUbicacionPK.idPromocion = :idPromocion AND p.promocionListaUbicacionPK.idUbicacion = :idUbicacion"),
    @NamedQuery(name = "PromocionListaUbicacion.findByIndTipo", query = "SELECT p FROM PromocionListaUbicacion p WHERE p.indTipo = :indTipo")})
public class PromocionListaUbicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PromocionListaUbicacionPK promocionListaUbicacionPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_PROMOCION", referencedColumnName = "ID_PROMOCION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Promocion promocion;
    
    @JoinColumn(name = "ID_UBICACION", referencedColumnName = "ID_UBICACION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ubicacion ubicacion;

    public PromocionListaUbicacion() {
    }

    public PromocionListaUbicacion(PromocionListaUbicacionPK promocionListaUbicacionPK) {
        this.promocionListaUbicacionPK = promocionListaUbicacionPK;
    }

    public PromocionListaUbicacion(String idUbicacion, String idPromocion, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.promocionListaUbicacionPK = new PromocionListaUbicacionPK(idUbicacion, idPromocion);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public PromocionListaUbicacion(String idUbicacion, String idPromocion) {
        this.promocionListaUbicacionPK = new PromocionListaUbicacionPK(idUbicacion, idPromocion);
    }

    public PromocionListaUbicacionPK getPromocionListaUbicacionPK() {
        return promocionListaUbicacionPK;
    }

    public void setPromocionListaUbicacionPK(PromocionListaUbicacionPK promocionListaUbicacionPK) {
        this.promocionListaUbicacionPK = promocionListaUbicacionPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo!=null?Character.toUpperCase(indTipo):null;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (promocionListaUbicacionPK != null ? promocionListaUbicacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromocionListaUbicacion)) {
            return false;
        }
        PromocionListaUbicacion other = (PromocionListaUbicacion) object;
        if ((this.promocionListaUbicacionPK == null && other.promocionListaUbicacionPK != null) || (this.promocionListaUbicacionPK != null && !this.promocionListaUbicacionPK.equals(other.promocionListaUbicacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.PromocionListaUbicacion[ promocionListaUbicacionPK=" + promocionListaUbicacionPK + " ]";
    }
    
}
