package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MARCADOR_PROMOCION")
@NamedQueries({
    @NamedQuery(name = "MarcadorPromocion.countIdMiembrosGroupByIdPromocion", query = "SELECT m.marcadorPromocionPK.idPromocion, COUNT(m.marcadorPromocionPK.idMiembro) FROM MarcadorPromocion m GROUP BY m.marcadorPromocionPK.idPromocion"),
    @NamedQuery(name = "MarcadorPromocion.countAllByIdPromocion", query = "SELECT COUNT(m) FROM MarcadorPromocion m WHERE m.marcadorPromocionPK.idPromocion = :idPromocion"),
    @NamedQuery(name = "MarcadorPromocion.findAllByIdMiembro", query = "SELECT m FROM MarcadorPromocion m WHERE m.marcadorPromocionPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "MarcadorPromocion.findAllByIdPromocion", query = "SELECT m FROM MarcadorPromocion m WHERE m.marcadorPromocionPK.idPromocion = :idPromocion"),
    @NamedQuery(name = "MarcadorPromocion.findAll", query = "SELECT m FROM MarcadorPromocion m")
})
public class MarcadorPromocion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MarcadorPromocionPK marcadorPromocionPK;
    
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @JoinColumn(name = "ID_PROMOCION", referencedColumnName = "ID_PROMOCION", insertable = false, updatable = false)
    @ManyToOne
    private Promocion promocion;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne
    private Miembro miembro;

    public MarcadorPromocion() {
    }

    public MarcadorPromocion(String idPromocion, String idMiembro) {
        this.marcadorPromocionPK = new MarcadorPromocionPK(idPromocion, idMiembro);
    }

    public MarcadorPromocionPK getMarcadorPromocionPK() {
        return marcadorPromocionPK;
    }

    public void setMarcadorPromocionPK(MarcadorPromocionPK marcadorPromocionPK) {
        this.marcadorPromocionPK = marcadorPromocionPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (marcadorPromocionPK != null ? marcadorPromocionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarcadorPromocion)) {
            return false;
        }
        MarcadorPromocion other = (MarcadorPromocion) object;
        if ((this.marcadorPromocionPK == null && other.marcadorPromocionPK != null) || (this.marcadorPromocionPK != null && !this.marcadorPromocionPK.equals(other.marcadorPromocionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MarcadorPromocion[ marcadorPromocionPK=" + marcadorPromocionPK + " ]";
    }
    
}
