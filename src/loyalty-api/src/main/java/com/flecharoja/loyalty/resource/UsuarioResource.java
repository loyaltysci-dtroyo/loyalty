/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Rol;
import com.flecharoja.loyalty.model.Usuario;
import com.flecharoja.loyalty.service.UsuarioBean;
import com.flecharoja.loyalty.service.UsuarioRolBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * Clase que provee de metodos http RESTful para el acceso a funcionalidades del
 * manejo de usuarios
 *
 * @author wtencio
 */
@Api(value = "Usuario")
@Path("usuario")
public class UsuarioResource {
//

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @EJB
    UsuarioBean usuarioBean;//EJB con los metodos de negocio para el manejo de usuarios

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion
    @EJB
    UsuarioRolBean usuarioRolBean;//EJB con los metodos de negocio para el manejo de asignacion de roles

    @Context
    HttpServletRequest request;

    /**
     * Método que inserta un nuevo usuario, una parte de la información en la
     * base de datos y otra parte en el servidor keycloak, de ocurrir un error
     * se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param usuario objeto que tiene la información a registrar del usuario
     * @return Información del resultado de la operación
     */
    @ApiOperation(value = "Registrar un nuevo usuario",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertUsuario(
            @ApiParam(value = "Información del usuario", required = true) Usuario usuario) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.USUARIOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return usuarioBean.insertUsuario(usuario, usuarioContext, request.getLocale());
    }

    /**
     * Método que obtiene el usuario asociado al id que pasa por parámetro, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error, de no ser asi se retorna un 200 (OK) con un
     * resultado de ser requerido este.
     *
     *
     * @param idUsuario identificador único del usuario
     * @return representación del usuario en formato JSON
     */
    @ApiOperation(value = "Obtener un grupo por su identificador",
            response = Usuario.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idUsuario}")
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario getUsuarioPorId(
            @ApiParam(value = "Identificador del usuario", required = true)
            @PathParam("idUsuario") String idUsuario) {
        String token;
        try {
            context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.USUARIOS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return usuarioBean.getUsuarioPorId(idUsuario, request.getLocale());
    }

    /**
     * Método que "elimina"(no se puede eliminar la información del usuario
     * totalmente), pone en estado inactivo el usuario, de ocurrir un error se
     * retornara una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error,
     * de no ser asi se retorna un 200 (OK) con un resultado de ser requerido
     * este.
     *
     * @param idUsuario identificador único del usuario
     * @return Información del resultado de la operación
     */
    @ApiOperation(value = "Desactivación de un usuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("eliminar/{idUsuario}")
    @Produces(MediaType.APPLICATION_JSON)
    public void removeUsuario(
            @ApiParam(value = "Identificador del usuario", required = true)
            @PathParam("idUsuario") String idUsuario) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.USUARIOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        usuarioBean.disableUser(idUsuario, usuarioContext, request.getLocale());
    }

    /**
     * Método que actualiza la información de un usuario ya registrado, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error, de no ser asi se retorna un 200 (OK) con un
     * resultado de ser requerido este.
     *
     * @param usuario objeto con la información del usuario que se quiere
     * actualizar
     * @return Información del resultado de la operación
     */
    @ApiOperation(value = "Modificación de un usuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void editUsuario(
            @ApiParam(value = "Información del usuario", required = true) Usuario usuario) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.USUARIOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        usuarioBean.editUsuario(usuario, usuarioContext, request.getLocale());
    }

    /**
     * Método que "elimina"(no se puede eliminar la información del usuario
     * totalmente), pone en estado inactivo el usuario, de ocurrir un error se
     * retornara una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error,
     * de no ser asi se retorna un 200 (OK) con un resultado de ser requerido
     * este.
     *
     * @param idUsuario identificador único del usuario
     * @return Información del resultado de la operación
     */
    @ApiOperation(value = "Reactivar  un usuario")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("activar/{idUsuario}")
    @Produces(MediaType.APPLICATION_JSON)
    public void activateUsuario(
            @ApiParam(value = "Identificador del usuario", required = true)
            @PathParam("idUsuario") String idUsuario) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.USUARIOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        usuarioBean.activateUsuario(idUsuario, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de usuarios a
     * partir de una serie de palabras claves, en el caso de error al ejecutar
     * la tarea se retorna un 500 (error interno del servidor) con un mensaje en
     * el encabezado, de no ser asi se retorna un 200 (OK) con un resultado de
     * ser requerido este.
     *
     * @param busqueda Cadena de texto conformada de palabras claves
     * @param estado Indicadores opcionales de estados deseados
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta con el estado de la operacion
     */
    /*@ApiOperation(value = "Buscar usuarios",
            responseContainer = "List",
            response = Usuario.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("buscar/{busqueda}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchUsuarios(
            @ApiParam(value = "Palabra de búsqueda")
            @PathParam("busqueda") String busqueda,
            @ApiParam(value = "Indicador de estado del usuario")
            @DefaultValue("null") @QueryParam("estado") String estado,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        return this.getUsuarios(busqueda, null, null, null, null, cantidad, registro);
    }*/

    /**
     * Redireccion del metodo buscar sin ninguna cadena de texto a metodo
     * obtener segmentos
     *
     * @param estado Indicadores opcionales de estados deseados
     * (ind1-ind2-ind3-...)
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta con el estado de la operacion
     */
    /*@ApiOperation(value = "Buscar usuarios",
            responseContainer = "List",
            response = Usuario.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("buscar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchUsuarios(
            @ApiParam(value = "Indicador de estado del usuario")
            @DefaultValue("null") @QueryParam("estado") String estado,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        return this.getUsuarios(null, null, null, null, null, cantidad, registro);
    }*/

    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de todos los
     * usuarios segun su estado definidos dentro de un parametro con cada estado
     * seperado por "-", en el caso de error al ejecutar la tarea se retorna un
     * 500 (error interno del servidor) con un mensaje en el encabezado, de no
     * ser asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param busqueda
     * @param estados
     * @param filtros
     * @param ordenTipo
     * @param ordenCampo
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Representacion del listado en formato JSON
     */
    @ApiOperation(value = "Obtener usuarios por estado, por defecto todos",
            responseContainer = "List",
            response = Usuario.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsuarios(
            @ApiParam(value = "Palabra de búsqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Indicador de estado del usuario") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Campos de filtros sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try {
            context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.USUARIOS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = usuarioBean.getUsuarios(estados, busqueda, filtros, registro, cantidad, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Metodo que acciona una tarea, la cual obtiene en respuesta de true/false
     * si un nombre de usuario ya esta almacenado, en el caso de error al
     * ejecutar la tarea se retorna un 500 (error interno del servidor) con un
     * mensaje en el encabezado, de no ser asi se retorna un 200 (OK) con un
     * resultado de ser requerido este.
     *
     * @param nombre Valor del nombre de despliegue a verificar
     * @return Palabra clave con los siguientes posibles valores: "true" Nombre
     * de despliegue existe, "false" Nombre de despliegue no existe
     */
    @ApiOperation(value = "Verificacion de existencia del nombre de usuario")
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("/exists/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean existsNombreUsuario(
            @ApiParam(value = "Nombre de usuario")
            @PathParam("nombre") String nombre) {
        String token;
        try {
            context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.USUARIOS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return usuarioBean.existUserName(nombre, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual obtiene los roles de un usuario, en
     * el caso de error al ejecutar la tarea se retorna un 500 (error interno
     * del servidor) con un mensaje en el encabezado, de no ser asi se retorna
     * un 200 (OK) con un resultado de ser requerido este.
     *
     * @param idUsuario Valor del nombre de despliegue a verificar
     * @param cantidad cantidad de registros que se desean por página
     * @param registro registro por el cual se desea empezar la busqueda
     * @return Palabra clave con los siguientes posibles valores: "true" Nombre
     * de despliegue existe, "false" Nombre de despliegue no existe
     */
    @ApiOperation(value = "Obtener roles de un usuario",
            responseContainer = "List",
            response = Rol.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idUsuario}/roles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRolesPorUsuario(
            @ApiParam(value = "Identificador del usuario")
            @PathParam("idUsuario") String idUsuario,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        String token;
        try {
            context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.USUARIOS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = usuarioBean.getRolesPorUsuario(idUsuario, cantidad, registro, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Metodo que acciona una tarea, la cual registra una lista de roles a un
     * usuario, en el caso de error al ejecutar la tarea se retorna un 500
     * (error interno del servidor) con un mensaje en el encabezado, de no ser
     * asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param idUsuario identificador único del usuario
     * @param nombreRoles nombre del rol a asignar
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Asignación de una lista de roles a un usuario")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idUsuario}/roles")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void assignListaRolUsuario(
            @ApiParam(value = "Identificador del usuario", required = true)
            @PathParam("idUsuario") String idUsuario,
            @ApiParam(value = "Lista de roles a asignar", required = true) List<Rol> nombreRoles) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.USUARIOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        usuarioRolBean.assignListRolUsuario(idUsuario, nombreRoles, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual registra una lista de roles a un
     * usuario, en el caso de error al ejecutar la tarea se retorna un 500
     * (error interno del servidor) con un mensaje en el encabezado, de no ser
     * asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param idUsuario identificador único del usuario
     * @param nombreRoles nombre del rol a asignar
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Desasignación de una lista de roles a un usuario")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idUsuario}/remover-roles")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void unassignListaRolUsuario(
            @ApiParam(value = "Identificador del usuario", required = true)
            @PathParam("idUsuario") String idUsuario,
            @ApiParam(value = "Lista de roles a desasignar", required = true) List<Rol> nombreRoles) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.USUARIOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        usuarioRolBean.unassignLisRolUsuario(idUsuario, nombreRoles, usuarioContext, request.getLocale());
    }

    /**
     * Método que obtiene el usuario en sesión, de ocurrir un error se retornara
     * una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error,
     * de no ser asi se retorna un 200 (OK) con un resultado de ser requerido
     * este.
     *
     *
     * @return representación del usuario en formato JSON
     */
    @ApiOperation(value = "Obtener el perfil del usuario en sesión",
            response = Usuario.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("perfil")
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario getPerfil() {
        String usuarioContext;
        try {
            usuarioContext = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return usuarioBean.getUsuarioPorId(usuarioContext, request.getLocale());
    }
}
