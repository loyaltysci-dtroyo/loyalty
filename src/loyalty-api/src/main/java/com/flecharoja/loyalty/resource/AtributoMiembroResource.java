package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.service.AtributoMiembroBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase que provee de metodos http RESTful para el acceso a funcionalidades del
 * manejo de miembros por atributo dinamico.
 *
 * @author svargas
 */
@Api(value = "Atributo Dinamico")
@Path("atributo")
public class AtributoMiembroResource {

    @EJB
    AtributoMiembroBean bean; //EJB con los metodos de negocio para el manejo de atributos a miembros

    @EJB
    MyKeycloakAuthz authzBean; //EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @Context
    HttpServletRequest request;

    /**
     * Metodo acciona una tarea que obtiene un listado de miembros que han sido
     * asignados a un atributo dinamico, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error, de no ser asi se
     * retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param idAtributo identificador unico de la
     * @return Representacion en JSON del listado de miembros
     */
    @ApiOperation(value = "Obtener miembros de un atributo dinámico",
            responseContainer = "List",
            response = Miembro.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idAtributo}/miembros")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMiembrosAtributosDinamicos(
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro,
            @ApiParam(value = "Identificador del atributo dinámico")
            @PathParam("idAtributo") String idAtributo) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.ATRIBUTOS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = bean.getMiembrosAtributoDinamico(idAtributo, cantidad, registro, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();

    }

    /**
     * Metodo que acciona una tarea, la cual asigna a un miembro un atributo
     * dinamico, de ocurrir un error se retornara una respuesta con un diferente
     * estado junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error, de no ser asi se retorna un 200 (OK)
     * con un resultado de ser requerido este.
     *
     * @param idMiembro Identificador del miembro
     * @param idAtributo identificador unico del atributo
     * @param valor valor que tendra el atributo del miembro
     * @return Informacion sobre el estado de la operacion
     */
    @ApiOperation(value = "Asignar un atributo dinámico a un miembro",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idAtributo}/miembro/{idMiembro}")
    @Consumes(MediaType.TEXT_PLAIN)
    public void assignAtributoDinamico(
            @ApiParam(value = "Identificador del miembro")
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Identificador del atributo dinámico", required = true)
            @PathParam("idAtributo") String idAtributo,
            @ApiParam(value = "Valor a tomar del atributo dinámico", required = true)
            @DefaultValue("null") String valor) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token, request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.ATRIBUTOS_ESCRITURA, token, request.getLocale()) ) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.assignAtributoDinamico(idAtributo, idMiembro, valor, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual elimina la asignacion de un miembro
     * a un atributo dinamico, de ocurrir un error se retornara una respuesta
     * con un diferente estado junto con un encabezado "Error-Reason" con un
     * valor numerico indicando la naturaleza del error, de no ser asi se
     * retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param idMiembro Identificador del miembro
     * @param idAtributo identificador unico del atributo
     * @return Informacion sobre el estado de la operacion
     */
    @ApiOperation(value = "Desasignación de un atributo dinámico a un miembro")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idAtributo}/miembro/{idMiembro}")
    public void unassignAtributoDinamico(
            @ApiParam(value = "Identificador del miembro", required = true)
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Identificador del atributo", required = true)
            @PathParam("idAtributo") String idAtributo) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token, request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.ATRIBUTOS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.unassignAtributoDinamico(idAtributo, idMiembro, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual asigna a un miembro un atributo
     * dinamico, de ocurrir un error se retornara una respuesta con un diferente
     * estado junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error, de no ser asi se retorna un 200 (OK)
     * con un resultado de ser requerido este.
     *
     * @param idMiembro Identificador del miembro
     * @param idAtributo identificador unico del atributo
     * @param valor valor que tendra el atributo del miembro
     * @return Informacion sobre el estado de la operacion
     */
    @ApiOperation(value = "Modificar el valor de un atributo dinámico a un miembro",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("{idAtributo}/miembro/{idMiembro}")
    @Consumes(MediaType.TEXT_PLAIN)
    public void updateAtributoDinamico(
            @ApiParam(value = "Identificador del miembro")
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Identificador del atributo dinámico", required = true)
            @PathParam("idAtributo") String idAtributo,
            @ApiParam(value = "Valor a tomar del atributo dinámico", required = true)
            @DefaultValue("null") String valor) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token, request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.ATRIBUTOS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.updateAtributoDinamico(idAtributo, idMiembro, valor, usuarioContext, request.getLocale());
    }
}
