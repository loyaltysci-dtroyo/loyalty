package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.Producto;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * Clase encargada de proveer los metodos necesarios para el manejo de productos
 *
 * @author svargas
 */
@Stateless
public class ProductoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de bitacora

    @EJB
    UtilBean utilBean;//EJB con los metodos de negocio para el manejo de utilidades

    /**
     * Metodo que obtiene un listado de productos en un rango de numero de
     * registros y opcionalmente por estado
     *
     * @param estados
     * @param calendarizaciones
     * @param registro Numero de registro inicial
     * @param indEntrega
     * @param indRespuesta
     * @param indImpuesto
     * @param filtros
     * @param busqueda
     * @param cantidad Cantidad de registros en la respuesta
     * @param ordenTipo
     * @param locale
     * @param ordenCampo
     * @return Respuesta con un listado de productos y el rango de registros
     * obtenidos
     */
    public Map<String, Object> getProductos(List<String> estados, List<String> calendarizaciones, List<String> indEntrega,String indRespuesta,String indImpuesto,String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int registro, int cantidad, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Producto> root = query.from(Producto.class);

        query = Busquedas.getCriteriaQueryProductos(cb, query, root, estados, calendarizaciones, indEntrega, indRespuesta, indImpuesto, busqueda, filtros, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();

        //retorno del resultado y encabezados sobre el rango
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Metodo que obtiene la informacion de un producto por el identificador de
     * la misma
     *
     * @param idProducto Identificador del producto
     * @param locale
     * @return Respuesta con la informacion del producto encontrado
     */
    public Producto getProducto(String idProducto, Locale locale) {

        Producto producto = em.find(Producto.class, idProducto);
        //verificacion de que la entidad exista
        if (producto == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
        }

        return producto;
    }

    /**
     * Método que inserta un nuevo registro de producto en la base de datos
     *
     * @param producto informacion del producto
     * @param idUsuario identificador del usuario en sesion
     * @param locale
     * @return identificador del producto registrado
     */
    public String insertProducto(Producto producto, String idUsuario, Locale locale) {
        
        producto.setEfectividadIndCalendarizado(Producto.Efectividad.PERMANENTE.getValue());
        producto.setIndEstado(Producto.Estados.BORRADOR.getValue());
        producto.setFechaCreacion(Calendar.getInstance().getTime());
        producto.setUsuarioCreacion(idUsuario);
        producto.setFechaModificacion(Calendar.getInstance().getTime());
        producto.setUsuarioModificacion(idUsuario);
        producto.setNombreInterno(producto.getNombreInterno().toUpperCase());
        producto.setEncabezadoArte(producto.getNombre());
        producto.setDetalleArte(producto.getDescripcion());
        producto.setNumVersion(1L);

        if(existeCodigoInterno(producto.getCodProducto())){
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "internal_code_already_exists");
        }
        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (producto.getImagenArte() == null || producto.getImagenArte().trim().isEmpty()) {
            producto.setImagenArte(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                producto.setImagenArte(filesUtils.uploadImage(producto.getImagenArte(), MyAwsS3Utils.Folder.ARTE_PRODUCTO, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }

        if ((producto.getIndEntrega().equals(Producto.Entrega.UBICACION.getValue())
                && producto.getIdUbicacion() == null) || (producto.getIndEntrega().equals(Producto.Entrega.DOMICILIO.getValue())
                && producto.getMontoEntrega() == null)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idUbicacion, montoEntrega");
        }
        
         //verificacion de atributos requeridos para la definicion de limites de respuesta
        if (producto.getLimiteIndRespuesta() != null && producto.getLimiteIndRespuesta()) {
            // si los valores son nulos se asumen valores por defecto (en lugar de lanzar exepcion)
            if (producto.getLimiteIndIntervaloTotal() != null && Producto.IntervalosTiempoRespuesta.get(producto.getLimiteIndIntervaloTotal()) == null || producto.getLimiteCantTotal() == null) {
                producto.setLimiteIndIntervaloTotal(Producto.IntervalosTiempoRespuesta.POR_SIEMPRE.getValue());
            }
            if (producto.getLimiteIndIntervaloMiembro() != null
                    && (Producto.IntervalosTiempoRespuesta.get(producto.getLimiteIndIntervaloMiembro()) == null || producto.getLimiteCantTotalMiembro() == null)) {
                producto.setLimiteIndIntervaloMiembro(Producto.IntervalosTiempoRespuesta.POR_SIEMPRE.getValue());
            }
            if (producto.getLimiteIndIntervaloRespuesta() != null
                    && (Producto.IntervalosTiempoRespuesta.get(producto.getLimiteIndIntervaloRespuesta()) == null || producto.getLimiteCantInterRespuesta() == null)) {
                producto.setLimiteIndIntervaloRespuesta(Producto.IntervalosTiempoRespuesta.POR_SIEMPRE.getValue());
            }
        }

        try {
            em.persist(producto);
            em.flush();

        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (!producto.getImagenArte().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(producto.getImagenArte());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        bitacoraBean.logAccion(Producto.class, producto.getIdProducto(), idUsuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);

        return producto.getIdProducto();
    }

    /**
     * Método que actualiza la información de un producto
     *
     * @param producto informacion del producto
     * @param idUsuario usuario en sesion
     * @param locale
     */
    public void editProducto(Producto producto, String idUsuario, Locale locale) {
        Producto original = em.find(Producto.class, producto.getIdProducto());
      
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
        }
        
        if(!original.getCodProducto().equals(producto.getCodProducto())){
            if(existeCodigoInterno(producto.getCodProducto())){
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "internal_code_already_exists");
            }
        }

        String urlImagenOriginal = original.getImagenArte();//url imagen original
        String urlImagenNueva = null;//url imagen nueva
        //si el atributo de imagen viene nula (no deberia)
        if (producto.getImagenArte() == null || producto.getImagenArte().trim().isEmpty()) {
            //establecimiento de la imagen por defecto
            urlImagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            producto.setImagenArte(urlImagenNueva);
        } else //ver si el valor en el atributo de imagen, difiere del almacenado
         if (!urlImagenOriginal.equals(producto.getImagenArte())) {//si la imagen es diferente a la almacenada
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    urlImagenNueva = filesUtils.uploadImage(producto.getImagenArte(), MyAwsS3Utils.Folder.ARTE_PRODUCTO, null);
                    producto.setImagenArte(urlImagenNueva);
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }

        //verificacion de que el posible cambio de esta sea valido  (Activo NO A Borrador) y de edicion de entidad archivada
        Character originalEstado = original.getIndEstado();
        Character newEstado = producto.getIndEstado();
        if ((originalEstado.equals(Producto.Estados.PUBLICADO.getValue()) && newEstado.equals(Producto.Estados.BORRADOR.getValue()))
                || originalEstado.equals(Producto.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "product_not_change_state");
        }

        if (newEstado.equals(Producto.Estados.PUBLICADO.getValue())) {
            producto.setFechaPublicacion(Calendar.getInstance().getTime());
        } else if (newEstado.equals(Producto.Estados.ARCHIVADO.getValue())) {
            producto.setFechaArchivado(Calendar.getInstance().getTime());
        }

        //si es calendarizado, verifica las fechas no esten vacias
        if (producto.getEfectividadIndCalendarizado().equals(Premio.Efectividad.CALENDARIZADO.getValue())) {
            if (producto.getEfectividadFechaInicio() == null || producto.getEfectividadFechaFin() == null || producto.getEfectividadFechaInicio().compareTo(producto.getEfectividadFechaFin()) > 0) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "product_badly_scheduled");
            }
            if (producto.getEfectividadIndSemana() != null && (producto.getEfectividadIndSemana().compareTo(1) < 0 || producto.getEfectividadIndSemana().compareTo(52) > 0)) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indSemanaRecurrencia");
                }

                if (producto.getEfectividadIndDias() != null) {
                    char[] validValues = {'L', 'M', 'K', 'J', 'V', 'S', 'D'};
                    producto.setEfectividadIndDias(producto.getEfectividadIndDias().toUpperCase());
                    for (char validValue : validValues) {
                        if (producto.getEfectividadIndDias().indexOf(validValue) != producto.getEfectividadIndDias().lastIndexOf(validValue)) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indDiaRecurrencia");
                        }
                    }
                }
        }else{
            if (!producto.getEfectividadIndCalendarizado().equals(Premio.Efectividad.PERMANENTE.getValue())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "efectividadIndCalendarizado");
            }
        }
        
         //verificacion de atributos requeridos para la definicion de limites de respuesta
        if (producto.getLimiteIndRespuesta() != null && producto.getLimiteIndRespuesta()) {
            if (Producto.IntervalosTiempoRespuesta.get(producto.getLimiteIndIntervaloTotal()) == null || producto.getLimiteCantTotal() == null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indIntervaloTotal, cantTotal");
            }
            if (producto.getLimiteIndIntervaloMiembro() != null) {
                if (Producto.IntervalosTiempoRespuesta.get(producto.getLimiteIndIntervaloMiembro()) == null || producto.getLimiteCantTotalMiembro() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indIntervaloMiembro, cantTotalMiembro");
                }
                if (producto.getLimiteIndIntervaloRespuesta() != null && (Producto.IntervalosTiempoRespuesta.get(producto.getLimiteIndIntervaloRespuesta()) == null || producto.getLimiteCantInterRespuesta() == null)) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indIntervalo, cantIntervalo");
                }
            }
        }

        producto.setFechaModificacion(Calendar.getInstance().getTime());
        producto.setUsuarioModificacion(idUsuario);

        try {
            em.merge(producto);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//en caso de que alguna informacion no este valida o este "vieja"
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
             if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(Producto.class, producto.getIdProducto(), idUsuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        if (urlImagenNueva != null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Método que elimina o archiva un producto dependiendo del caso
     *
     * @param idProducto identificador del producto
     * @param idUsuario identificador del usuario en sesion
     * @param locale
     */
    public void removeProducto(String idProducto, String idUsuario, Locale locale) {
        Producto producto = em.find(Producto.class, idProducto);
         if (producto == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
        }
       
        if (producto.getIndEstado().equals(Producto.Estados.BORRADOR.getValue())) {
            em.remove(producto);
            if (!producto.getImagenArte().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(producto.getImagenArte());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            bitacoraBean.logAccion(Producto.class, idProducto, idUsuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        } else if (producto.getIndEstado().compareTo(Producto.Estados.ARCHIVADO.getValue()) == 0) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Producto");
        } else {
            producto.setFechaArchivado(Calendar.getInstance().getTime());
            producto.setUsuarioModificacion(idUsuario);
            producto.setIndEstado(Producto.Estados.ARCHIVADO.getValue());

            em.merge(producto);
            bitacoraBean.logAccion(Producto.class, idProducto, idUsuario, Bitacora.Accion.ENTIDAD_ARCHIVADA.getValue(),locale);
        }
    }

    /**
     * Método que verifica la existencia de un nombre interno en algun registro
     * de productos
     *
     * @param nombreInterno nombre que se desea buscar
     * @return resultado true si existe o false si no existe
     */
    public Boolean existsNombreInterno(String nombreInterno) {
        return ((Long) em.createNamedQuery("Producto.countByNombreInterno").setParameter("nombreInterno", nombreInterno.toUpperCase()).getSingleResult()) > 0;
    }

    private boolean existeCodigoInterno(String codProducto) {
        return  ((Long) em.createNamedQuery("Producto.existsCodigoInterno").setParameter("codigoInterno", codProducto).getSingleResult()) > 0;
    }

}
