package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.ListaReglas;
import com.flecharoja.loyalty.service.ListaReglasBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase de recursos http disponibles para el acceso a funcionalidades del
 * manejo de listas de reglas de segmentos.
 *
 * @author svargas
 */
@Api(value = "Segmento")
@Path("segmento/{idSegmento}/reglas")
public class ListaReglasResource {
    
    /**
     * Parametro de ruta con el identificador de segmento de
     * la lista de reglas
     */
    @PathParam("idSegmento")
    String idSegmento;

    @EJB
    ListaReglasBean bean; //EJB con los metodos de negocio para el manejo de listas de reglas

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión
    
    @Context
    HttpServletRequest request;

    /**
     * Metodo que obtiene un listado de listas de reglas que un segmento tiene
     * relacionado en un rango establecido por los parametros de registro y
     * cantidad y retorna una respuesta OK(200) con la lista de reglas y
     * encabezados acerca del rango y rango aceptado, de ocurrir un error se
     * retornara una respuesta con un estado de error y un encabezado
     * (Error-Reason) con un valor numerico indicando la naturaleza del error.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta OK con el listado de reglas y los encabezados de rango
     */
    @ApiOperation(value = "Obtener listas de reglas de un segmento",
            responseContainer = "List",
            response = ListaReglas.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idSegmento", value = "Identificador del segmento", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListaReglas(
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = bean.getListasReglas(idSegmento, registro, cantidad,request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }

    /**
     * Metodo que registra la informacion de una nueva lista de reglas y retorna
     * el identificador creado para la lista de reglas en una respuesta con un
     * estado de OK(200), de ocurrir un error se retornara una respuesta con un
     * estado de error y un encabezado (Error-Reason) con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param listaReglas Objeto con la informacion de la lista de reglas a
     * ingresar
     * @return Respuesta OK con el identificador de la lista creada
     */
    @ApiOperation(value = "Registro de una lista de reglas de un segmento",
            response = String.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idSegmento", value = "Identificador del segmento", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertListaReglas(
            @ApiParam(value = "Informacion de la lista de reglas", required = true) ListaReglas listaReglas) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bean.insertListaReglas(idSegmento, listaReglas, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que modifica la informacion de una lista de reglas existente y
     * retorna una respuesta con un estado de OK(200), de ocurrir un error se
     * retornara una respuesta con un estado de error y un encabezado
     * (Error-Reason) con un valor numerico indicando la naturaleza del error.
     *
     * @param listaReglas Objeto con la informacion de la lista de reglas a
     * modificar
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Modificacion de lista de reglas de un segmento")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idSegmento", value = "Identificador del segmento", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void editListaReglas(
            @ApiParam(value = "Informacion de la lista de reglas", required = true) ListaReglas listaReglas) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.editListaReglas(idSegmento, listaReglas, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que obtiene la informacion de una lista de reglas por medio de su
     * identificador de lista y la retorna junto una respuesta con un estado de
     * OK(200), de ocurrir un error se retornara una respuesta con un estado de
     * error y un encabezado (Error-Reason) con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idLista Identificador de la lista de reglas
     * @return Respuesta OK con la informacion de la lista de reglas deseada
     */
    @ApiOperation(value = "Obtencion de lista de reglas de un segmento por identificador", response = ListaReglas.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idSegmento", value = "Identificador del segmento", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idLista}")
    @Produces(MediaType.APPLICATION_JSON)
    public ListaReglas getListaReglasPorId(
            @ApiParam(value = "Identificador de lista de reglas", required = true)
            @PathParam("idLista") String idLista) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bean.getListaReglasPorId(idSegmento, idLista,request.getLocale());
    }

    /**
     * Metodo que elimina la informacion de una lista de reglas existente y
     * retorna una respuesta con un estado de OK(200), de ocurrir un error se
     * retornara una respuesta con un estado de error y un encabezado
     * (Error-Reason) con un valor numerico indicando la naturaleza del error.
     *
     * @param idLista Identificador de la lista de reglas
     */
    @ApiOperation(value = "Eliminacion de lista de reglas de un segmento")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idSegmento", value = "Identificador del segmento", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idLista}")
    public void removeListaReglas(
            @ApiParam(value = "Identificador de lista de reglas", required = true)
            @PathParam("idLista") String idLista) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.deleteListaReglas(idSegmento, idLista, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que intercambia la posicion de dos listas de reglas (atributo
     * orden) y retorna una respuesta con un estado de OK(200), de ocurrir un
     * error se retornara una respuesta con un estado de error y un encabezado
     * (Error-Reason) con un valor numerico indicando la naturaleza del error.
     *
     * @param listas Listado de las listas de reglas a intercambiar
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Intercambio de orden entre listas de reglas")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idSegmento", value = "Identificador del segmento", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("/intercambiar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void swapOrdenListas(
            @ApiParam(value = "Lista de 2 listas de reglas", required = true) List<ListaReglas> listas) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_ADMIN_REGLAS, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.swapOrdenListas(idSegmento, listas, usuarioContext,request.getLocale());
    }
}
