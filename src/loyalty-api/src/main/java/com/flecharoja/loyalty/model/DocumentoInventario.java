/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "DOCUMENTO_INVENTARIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentoInventario.findAll", query = "SELECT d FROM DocumentoInventario d ORDER BY d.indCerrado ASC, d.fechaModificacion DESC"),
    @NamedQuery(name = "DocumentoInventario.findByUbicacion", query = "SELECT d FROM DocumentoInventario d WHERE d.ubicacionOrigen.idUbicacion = :idUbicacion ORDER BY d.indCerrado ASC, d.fechaModificacion DESC"),
    @NamedQuery(name = "DocumentoInventario.countByUbicacion", query = "SELECT COUNT(d) FROM DocumentoInventario d WHERE d.ubicacionOrigen.idUbicacion = :idUbicacion"),
    @NamedQuery(name = "DocumentoInventario.countAll", query = "SELECT COUNT(d) FROM DocumentoInventario d"),
    @NamedQuery(name = "DocumentoInventario.findByNumDocumento", query = "SELECT d FROM DocumentoInventario d WHERE d.numDocumento = :numDocumento"),
    @NamedQuery(name = "DocumentoInventario.findByFecha", query = "SELECT d FROM DocumentoInventario d WHERE d.fecha = :fecha"),
    @NamedQuery(name = "DocumentoInventario.findByTipo", query = "SELECT d FROM DocumentoInventario d WHERE d.tipo = :tipo"),
    @NamedQuery(name = "DocumentoInventario.findByUbicacionByTipoAbierto", query = "SELECT d FROM DocumentoInventario d WHERE d.ubicacionOrigen.idUbicacion = :idUbicacion AND d.tipo = :tipo AND d.indCerrado = :indCerrado"),
    @NamedQuery(name = "Documento.existsCodigoInterno", query = "SELECT COUNT(d) FROM DocumentoInventario d WHERE d.codigoDocumento = :codigoInterno"),

})
public class DocumentoInventario implements Serializable {

    @Version
    @Basic(optional = false)
    @NotNull()
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_CERRADO")
    private Boolean indCerrado;
    
    @Size(max = 200)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Size(max = 40)
    @Column(name = "CODIGO_DOCUMENTO")
    private String codigoDocumento;
    
    @JoinColumn(name = "COD_PROVEEDOR", referencedColumnName = "ID_PROVEEDOR")
    @ManyToOne
    private Proveedor codProveedor;
  
    
    public enum Tipos{
        REDENCION_AUTO('R'),
        COMPRA('C'),
        AJUSTE_MAS('A'),
        AJUSTE_MENOS('M'),
        INTER_UBICACION('I');
        
        public final char value;
        public static final Map<Character,Tipos> lookup= new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }
        
        static{
            for(Tipos tipo : values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipos get(Character value){
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "documento_uuid")
    @GenericGenerator(name = "documento_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "NUM_DOCUMENTO")
    private String numDocumento;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO")
    private Character tipo;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "numDocumento")
    private List<HistoricoMovimientos> historicoMovimientosList;
    
    @JoinColumn(name = "UBICACION_ORIGEN", referencedColumnName = "ID_UBICACION")
    @ManyToOne(optional = false)
    private Ubicacion ubicacionOrigen;
    
    @JoinColumn(name = "UBICACION_DESTINO", referencedColumnName = "ID_UBICACION")
    @ManyToOne
    private Ubicacion ubicacionDestino;
    
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Size(max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Size(max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    

    public DocumentoInventario() {
    }

    public DocumentoInventario(String numDocumento) {
        this.numDocumento = numDocumento;
    }

    public DocumentoInventario(String numDocumento, Date fecha, Character tipo) {
        this.numDocumento = numDocumento;
        this.fecha = fecha;
        this.tipo = tipo;
    }

    public String getNumDocumento() {
        return numDocumento;
    }

    public void setNumDocumento(String numDocumento) {
        this.numDocumento = numDocumento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<HistoricoMovimientos> getHistoricoMovimientosList() {
        return historicoMovimientosList;
    }

    public void setHistoricoMovimientosList(List<HistoricoMovimientos> historicoMovimientosList) {
        this.historicoMovimientosList = historicoMovimientosList;
    }

    public Ubicacion getUbicacionOrigen() {
        return ubicacionOrigen;
    }

    public void setUbicacionOrigen(Ubicacion ubicacionOrigen) {
        this.ubicacionOrigen = ubicacionOrigen;
    }

    public Ubicacion getUbicacionDestino() {
        return ubicacionDestino;
    }

    public void setUbicacionDestino(Ubicacion ubicacionDestino) {
        this.ubicacionDestino = ubicacionDestino;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numDocumento != null ? numDocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentoInventario)) {
            return false;
        }
        DocumentoInventario other = (DocumentoInventario) object;
        if ((this.numDocumento == null && other.numDocumento != null) || (this.numDocumento != null && !this.numDocumento.equals(other.numDocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.DocumentoInventario[ numDocumento=" + numDocumento + " ]";
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

  

    public Boolean getIndCerrado() {
        return indCerrado;
    }

    public void setIndCerrado(Boolean indCerrado) {
        this.indCerrado = indCerrado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigoDocumento() {
        return codigoDocumento;
    }

    public void setCodigoDocumento(String codigoDocumento) {
        this.codigoDocumento = codigoDocumento;
    }

    public Proveedor getCodProveedor() {
        return codProveedor;
    }

    public void setCodProveedor(Proveedor codProveedor) {
        this.codProveedor = codProveedor;
    }
}
