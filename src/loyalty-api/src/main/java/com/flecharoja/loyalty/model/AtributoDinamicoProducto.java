/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "ATRIBUTO_DINAMICO_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AtributoDinamicoProducto.findAll", query = "SELECT a FROM AtributoDinamicoProducto a ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamicoProducto.countAll", query = "SELECT COUNT(a.idAtributo) FROM AtributoDinamicoProducto a"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByIdAtributo", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.idAtributo = :idAtributo"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByNombre", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.nombre = :nombre ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByDescripcion", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.descripcion = :descripcion ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByIndTipo", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.indTipo = :indTipo ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByValorOpciones", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.valorOpciones = :valorOpciones ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByIndRequerido", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.indRequerido = :indRequerido ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByIndEstado", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.indEstado = :indEstado ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByFechaCreacion", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.fechaCreacion = :fechaCreacion ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByUsuarioCreacion", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.usuarioCreacion = :usuarioCreacion ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByFechaModificacion", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.fechaModificacion = :fechaModificacion ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByUsuarioModificacion", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.usuarioModificacion = :usuarioModificacion ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByNumVersion", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.numVersion = :numVersion ORDER BY a.fechaModificacion DESC"),
    @NamedQuery(name = "AtributoDinamicoProducto.existsNombreInterno", query = "SELECT COUNT(a) FROM AtributoDinamicoProducto a WHERE a.nombreInterno = :nombreInterno"),
})
public class AtributoDinamicoProducto implements Serializable {

    public enum Tipos {
        NUMERICO('N'),
        TEXTO('T'),
        OPCIONES('M');
        
        private final char value;
        private static final Map<Character,Tipos>  lookup = new HashMap<>();
        
        private Tipos(char value){
            this.value=value;
        }
        
        static{
            for(Tipos tipo : values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipos get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Estados{
        BORRADOR('B'),
        PUBLICADO('P'),
        ARCHIVADO('A');
        
        private final char value;
        private static final Map<Character,Estados> lookup = new HashMap<>();
        
        private Estados (char value){
            this.value = value;
        }
        
        static{
            for(Estados estado : values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @Size(max = 300)
    @Column(name = "IMAGEN")
    private String imagen;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "atributo_producto_uuid")
    @GenericGenerator(name = "atributo_producto_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_ATRIBUTO")
    private String idAtributo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;
    
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Size(max = 300)
    @Column(name = "VALOR_OPCIONES")
    private String valorOpciones;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_REQUERIDO")
    private Boolean indRequerido;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    
    @Transient
    private String valorAtributo;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "atributoDinamicoProducto")
    private List<ProductoAtributo> productoAtributoList;

    public AtributoDinamicoProducto() {
    }

    public AtributoDinamicoProducto(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public AtributoDinamicoProducto(String idAtributo, String nombre, Character indTipo, Boolean indRequerido, Character indEstado, Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.idAtributo = idAtributo;
        this.nombre = nombre;
        this.indTipo = indTipo;
        this.indRequerido = indRequerido;
        this.indEstado = indEstado;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public String getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo == null ? null : Character.toUpperCase(indTipo);
    }

    public String getValorOpciones() {
        return valorOpciones;
    }

    public void setValorOpciones(String valorOpciones) {
        this.valorOpciones = valorOpciones;
    }

    public Boolean getIndRequerido() {
        return indRequerido;
    }

    public void setIndRequerido(Boolean indRequerido) {
        this.indRequerido = indRequerido;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getValorAtributo() {
        return valorAtributo;
    }

    public void setValorAtributo(String valorAtributo) {
        this.valorAtributo = valorAtributo;
    }
    
    

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ProductoAtributo> getProductoAtributoList() {
        return productoAtributoList;
    }

    public void setProductoAtributoList(List<ProductoAtributo> productoAtributoList) {
        this.productoAtributoList = productoAtributoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtributo != null ? idAtributo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtributoDinamicoProducto)) {
            return false;
        }
        AtributoDinamicoProducto other = (AtributoDinamicoProducto) object;
        if ((this.idAtributo == null && other.idAtributo != null) || (this.idAtributo != null && !this.idAtributo.equals(other.idAtributo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.AtributoDinamicoProducto[ idAtributo=" + idAtributo + " ]";
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    
}
