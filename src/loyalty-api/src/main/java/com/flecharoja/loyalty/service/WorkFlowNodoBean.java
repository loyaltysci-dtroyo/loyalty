/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Insignias;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.model.Workflow;
import com.flecharoja.loyalty.model.WorkflowNodo;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * Bean encargado de realizar operaciones de mantenimiento de datos de los
 * workflow
 *
 * @author faguilar
 */
@Stateless
public class WorkFlowNodoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene una lista de nodos del workflow para un id de workflow
     * y los retorna en una lista ordenada
     *
     * @param idWorkflow Valores de los indicadores de estado a buscar
     * @param locale
     * @return Respuesta con una lista de workflows
     */
    public ArrayList<WorkflowNodo> getNodos(String idWorkflow, Locale locale) {
        if (idWorkflow == null || idWorkflow.equals("")) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idWorkflow");
        }
        ArrayList<WorkflowNodo> resultado = new ArrayList<>();
        Workflow padre = em.find(Workflow.class, idWorkflow);
        WorkflowNodo nodoActual = padre.getIdSucesor();

        while (nodoActual != null) {
            resultado.add(nodoActual);
            nodoActual = nodoActual.getIdSucesor();
        }

        return resultado;
    }

    /**
     * Metodo que obtiene un nodo de wworkflow por su numero de identificacion
     *
     * @param idWorkflow Identificacion del workflow
     * @param locale
     * @return Respuesta con la informacion del workflow
     */
    public WorkflowNodo getNodoPorId(String idWorkflow, Locale locale) {
        if (idWorkflow == null || idWorkflow.equals("")) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idWorkflow");
        }
        WorkflowNodo workflow = em.find(WorkflowNodo.class, idWorkflow);
        //verificacion que el workflow exista
        if (workflow == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "workflow_node_not_found");
        }
        return workflow;
    }

    /**
     * Metodo que crea y almacena un nuevo a partir de la posicion de un nodo
     * padre
     *
     * @param idNodoPadre El nodo a partir del cual se debe insertar el nuevo
     * nodo
     * @param nodoInsertar El nodo que se insertara propiamente
     * @param usuario usuario en sesión
     * @param locale
     * @return Respuesta con el identificacion del workflow almacenado
     */
    public String insertNodo(String idNodoPadre, WorkflowNodo nodoInsertar, String usuario, Locale locale) {
        if (idNodoPadre == null || idNodoPadre.equals("") || nodoInsertar == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idNodoPadre, nodoInsertar");
        }
        switch (nodoInsertar.getIndTipoTarea()) {
            case 'A': {
                if (em.find(Promocion.class, nodoInsertar.getReferencia()) == null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
                }
                break;
            }
            case 'B': {
                if (em.find(Notificacion.class, nodoInsertar.getReferencia()) == null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
                }
                break;
            }
            case 'C': {
                if (em.find(Insignias.class, nodoInsertar.getReferencia()) == null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "badge_not_found");
                }
                break;
            }
            case 'D': {
                if (em.find(Premio.class, nodoInsertar.getReferencia()) == null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
                }
                break;
            }
            case 'E': {
                if (em.find(Mision.class, nodoInsertar.getReferencia()) == null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
                }
                break;
            }
        }

        Workflow raiz = em.find(Workflow.class, idNodoPadre);
        WorkflowNodo nodoActual = em.find(WorkflowNodo.class, idNodoPadre);

        //Se establece el flag de mision especial si la raiz tambien lo tiene
        nodoInsertar.setIsNodoMisionEspecial(raiz.isIsWorkflowMisionEspec());

        if (raiz == null && nodoActual == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "raiz, nodoActual");
        }
        //establecimiento de valores por defecto
        Date date = Calendar.getInstance().getTime();
        nodoInsertar.setFechaCreacion(date);
        nodoInsertar.setFechaModificacion(date);
        nodoInsertar.setNumVersion(new Long(1));

        nodoInsertar.setUsuarioCreacion(usuario);
        nodoInsertar.setUsuarioModificacion(usuario);

        if (raiz != null && nodoActual == null) {
            if (raiz.getIdSucesor() != null) {
                nodoInsertar.setIdSucesor(raiz.getIdSucesor());
                raiz.setIdSucesor(nodoInsertar);
            } else {
                raiz.setIdSucesor(nodoInsertar);
            }
            try {
                em.persist(raiz);
                em.persist(nodoInsertar);
                em.flush();
                bitacoraBean.logAccion(WorkFlowNodoBean.class, nodoInsertar.getIdNodo(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            } catch (ConstraintViolationException e) {//atributos no validos
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }
        }
        if (raiz == null && nodoActual != null) {
            if (nodoActual.getIdSucesor() != null) {
                nodoInsertar.setIdSucesor(nodoActual.getIdSucesor());
                nodoActual.setIdSucesor(nodoInsertar);
            } else {
                nodoActual.setIdSucesor(nodoInsertar);
            }
            try {
                em.persist(raiz);
                em.persist(nodoInsertar);
                em.flush();
                bitacoraBean.logAccion(WorkFlowNodoBean.class, nodoInsertar.getIdNodo(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            } catch (ConstraintViolationException e) {//atributos no validos
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }
        }

        return nodoInsertar.getIdNodo();
    }

    /**
     * Metodo que almacena la informacion de un workflow al final de la cola de
     * nodos
     *
     * @param nodoInsertar El nodo a insertar
     * @param idWorkflow Objeto con la informacion del workflow
     * @param usuario usuario en sesión
     * @param locale
     * @return Respuesta con el identificacion del workflow almacenado
     */
    public String pushNodo(WorkflowNodo nodoInsertar, String idWorkflow, String usuario, Locale locale) {

        Workflow raiz = em.find(Workflow.class, idWorkflow);
        if (raiz == null || idWorkflow == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idWorkflow");
        }
        /*Se valida que si es un workflow eapecial de mision entonces se valide que el elemento del nodo sea una mision*/
        if (raiz.isIsWorkflowMisionEspec()) {
            //Se establece el flag de mision especial si la raiz tambien lo tiene
            nodoInsertar.setIsNodoMisionEspecial(raiz.isIsWorkflowMisionEspec());
            //como el workflow especial solo trabaja para misiones entonces se valida que la referencia se mision
            if (em.find(Mision.class, nodoInsertar.getReferencia()) == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
            }
        } else {
            // Se valida que el elemento de la referencia del nodo exista en la base de datos
            switch (nodoInsertar.getIndTipoTarea()) {
                //promociones
                case 'A': {
                    if (em.find(Promocion.class, nodoInsertar.getReferencia()) == null) {
                        throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
                    }
                    break;
                }
                //notificaciones
                case 'B': {
                    if (em.find(Notificacion.class, nodoInsertar.getReferencia()) == null) {
                        throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
                    }
                    break;
                }
                //insignia
                case 'C': {
                    if (em.find(Insignias.class, nodoInsertar.getReferencia()) == null) {
                        throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "badge_not_found");
                    }
                    break;
                }
                //premio
                case 'D': {
                    if (em.find(Premio.class, nodoInsertar.getReferencia()) == null) {
                        throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
                    }
                    break;
                }
                //misiones
                case 'E': {
                    if (em.find(Mision.class, nodoInsertar.getReferencia()) == null) {
                        throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
                    }
                    break;
                }
            }
        }
        //establecimiento de valores por defecto
        Date date = Calendar.getInstance().getTime();
        nodoInsertar.setFechaCreacion(date);
        nodoInsertar.setFechaModificacion(date);
        nodoInsertar.setNumVersion(new Long(1));

        nodoInsertar.setUsuarioCreacion(usuario);
        nodoInsertar.setUsuarioModificacion(usuario);

        WorkflowNodo nodoTmp = raiz.getIdSucesor();
        if (nodoTmp != null) {
            while (nodoTmp != null && nodoTmp.getIdSucesor() != null) {
                nodoTmp = nodoTmp.getIdSucesor();
            }
            try {
                nodoTmp.setIdSucesor(nodoInsertar);
                em.merge(nodoTmp);
                em.persist(nodoInsertar);
                em.flush();
                bitacoraBean.logAccion(WorkflowNodo.class, nodoInsertar.getIdNodo(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            } catch (ConstraintViolationException e) {//atributos no validos
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }
        } else {
            try {
                raiz.setIdSucesor(nodoInsertar);
                em.merge(raiz);
                em.persist(nodoInsertar);
                bitacoraBean.logAccion(WorkflowNodo.class, nodoInsertar.getIdNodo(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            } catch (ConstraintViolationException e) {//atributos no validos
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }
        }
        return nodoInsertar.getIdNodo();
    }

    /**
     * Metodo que modifica la informacion de un nodo de workflow almacenado
     *
     * @param nodoEditar Objeto con la informacion del workflow
     * @param usuario usuario en sesión
     * @param locale
     */
    public void editNodoWorkflow(WorkflowNodo nodoEditar, String usuario, Locale locale) {
        if (nodoEditar == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "WorkflowNodo");
        }
        //se verifica que el nodo pertenezca a un workflow especial
        if (nodoEditar.isIsNodoMisionEspecial()) {
            //como el workflow especial solo trabaja para misiones entonces se valida que la referencia se mision
            if (em.find(Mision.class, nodoEditar.getReferencia()) == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
            }
        } else {
            switch (nodoEditar.getIndTipoTarea()) {
                case 'A': {
                    if (em.find(Promocion.class, nodoEditar.getReferencia()) == null) {
                        throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
                    }
                    break;
                }
                case 'B': {
                    if (em.find(Notificacion.class, nodoEditar.getReferencia()) == null) {
                        throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
                    }
                    break;
                }
                case 'C': {
                    if (em.find(Insignias.class, nodoEditar.getReferencia()) == null) {
                        throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "badge_not_found");
                    }
                    break;
                }
                case 'D': {
                    if (em.find(Premio.class, nodoEditar.getReferencia()) == null) {
                        throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
                    }
                    break;
                }
                case 'E': {
                    if (em.find(Mision.class, nodoEditar.getReferencia()) == null) {
                        throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
                    }
                    break;
                }
            }
        }
        WorkflowNodo original;
        try {
            original = em.find(WorkflowNodo.class, nodoEditar.getIdNodo());
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idNodo");
        }
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "workflow_node_not_found");
        }
        //establecimiento de valores por defecto
        Date date = Calendar.getInstance().getTime();
        nodoEditar.setFechaModificacion(date);
        nodoEditar.setUsuarioModificacion(usuario);
        try {
            em.merge(nodoEditar);
            em.flush();
            bitacoraBean.logAccion(Workflow.class, nodoEditar.getIdNodo(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

    }

    /**
     * Metodo que elimina un workflow de la base de datos
     *
     * @param idNodo Identificacion del workflow
     * @param usuario usuario en sesión
     * @param locale
     */
    public void removeNodoWorkflow(String idNodo, String usuario, Locale locale) {
        WorkflowNodo nodoEliminar = em.find(WorkflowNodo.class, idNodo);
        if (nodoEliminar == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "workflow_node_not_found");
        }
        try {
            //primero se encuentra el Workflow raiz
            Workflow raiz = (Workflow) em.createNamedQuery("Workflow.findByIdNodo").setParameter("idNodo", nodoEliminar.getIdNodo()).getSingleResult();
            //si el nodo a eliminar tiene un sucesor   
            if (nodoEliminar.getIdSucesor() != null) {
                //se asigna ese sucesor como sucesor de la raiz
                raiz.setIdSucesor(nodoEliminar.getIdSucesor());
            } else {
                raiz.setIdSucesor(null);
            }
            nodoEliminar.setIdSucesor(null);
        } catch (javax.persistence.NoResultException e) {
            WorkflowNodo nodoPadre = (WorkflowNodo) em.createNamedQuery("WorkflowNodo.findByIdSucesor").setParameter("idNodo", nodoEliminar.getIdNodo()).getSingleResult();
            if (nodoEliminar.getIdSucesor() != null) {
                nodoPadre.setIdSucesor(nodoEliminar.getIdSucesor());
            } else {
                nodoPadre.setIdSucesor(null);
            }
            nodoEliminar.setIdSucesor(null);
        }
        em.flush();
        em.remove(nodoEliminar);
        bitacoraBean.logAccion(Workflow.class, nodoEliminar.getIdNodo(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
    }
}
