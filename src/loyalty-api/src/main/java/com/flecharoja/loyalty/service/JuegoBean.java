/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Juego;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionJuego;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class JuegoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;
    
    @EJB
    InstanciaMillonarioBean instanciaMillonarioBean;

    /**
     * registro de juego de mision
     * 
     * @param juego data de juego
     * @param idMision id de mision
     * @param usuario id admin
     * @param locale locale
     */
    public void insertJuego(Juego juego, String idMision, String usuario, Locale locale) {
        if (Juego.Tipo.get(juego.getTipo()) == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "Tipo");
        }
        
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
        }
        
        //verificacion del tipo de mision
        if (!mision.getIndTipoMision().equals(Mision.Tipos.JUEGO_PREMIO.getValue())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "challenge_not_game");
        }
        
        if (juego.getTipo().equals(Juego.Tipo.MILLONARIO.getValue())) {
            if (juego.getIdJuegoMillonario() == null) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_details_not_set");
            }
            
            if (juego.getIdInstanciaMillonario() != null) {
                instanciaMillonarioBean.deleteInstancia(juego.getIdInstanciaMillonario(), usuario, locale);
            }
            juego.setIdInstanciaMillonario(instanciaMillonarioBean.insertInstanciaMillonario(juego.getIdJuegoMillonario(), idMision, usuario, locale));
        }

        //verificacion de datos de juego
        if (Juego.Estrategia.get(juego.getEstrategia()) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "estrategia");
        }

        if (juego.getTipo().equals(Juego.Tipo.ROMPECABEZAS.getValue())) {
            if (juego.getImagen() == null || juego.getImagen().trim().isEmpty()) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "imagen");
            } else {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    List<String> imagenes = this.getImagenesRompecabezas(filesUtils, juego.getImagen(), locale);
                    juego.setImagen(filesUtils.uploadImage(juego.getImagen(), MyAwsS3Utils.Folder.ARTE_JUEGO, null));
                    juego.setImagenesRompecabezas(imagenes.stream().collect(Collectors.joining(", ")));
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }

            if (juego.getTiempo() == null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "cantPiezas, tiempo");
            }
        }

        //set de atributos por defecto
        juego.setFechaCreacion(Calendar.getInstance().getTime());
        juego.setFechaModificacion(Calendar.getInstance().getTime());
        juego.setUsuarioCreacion(usuario);
        juego.setUsuarioModificacion(usuario);
        juego.setMision(mision);
        juego.setIdMision(idMision);
        juego.setNumVersion(1L);

        try {
            em.persist(juego);
            bitacoraBean.logAccion(Juego.class, idMision, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * edicion de juego de una mision
     * 
     * @param juego data de juego
     * @param idMision id de mision
     * @param usuario id admin
     * @param locale locale
     */
    public void editJuego(Juego juego, String idMision, String usuario, Locale locale) {
        Juego original = em.find(Juego.class, juego.getIdMision());
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_game_not_found");
        }
        //verificacion del tipo de juego
        if (Juego.Tipo.get(juego.getTipo()) == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "Tipo");
        }
        
        if (juego.getTipo().equals(Juego.Tipo.MILLONARIO.getValue())) {
            if (juego.getIdJuegoMillonario() == null) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_details_not_set");
            }
            if (juego.getIdInstanciaMillonario() != null) {
                instanciaMillonarioBean.deleteInstancia(juego.getIdInstanciaMillonario(), usuario, locale);
            }
            juego.setIdInstanciaMillonario(instanciaMillonarioBean.insertInstanciaMillonario(juego.getIdJuegoMillonario(), idMision, usuario, locale));
        }

        //verificacion que el juego sea de la mision referente
        if (!juego.getIdMision().equals(idMision)) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idMision");
        }
        
        //edicion de la imagen de juego o imagen de rompecabezas (division de imagen y guardado de partes)
        String currentUrl = (String) em.createNamedQuery("Juego.findImagen").setParameter("idJuego", juego.getIdMision()).getSingleResult();
        String newUrl = null;
        //si la original es rompecabezas y la actual es rompecabezas
        if (original.getTipo().equals(Juego.Tipo.ROMPECABEZAS.getValue()) && juego.getTipo().equals(Juego.Tipo.ROMPECABEZAS.getValue())) {
            //si el atributo de imagen viene nula (no deberia)
            if (juego.getImagen() == null || juego.getImagen().trim().isEmpty()) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "imagen");
            } else //ver si el valor en el atributo de imagen, difiere del almacenado
            if (!currentUrl.equals(juego.getImagen())) {
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    //url nueva imagen
                    newUrl = filesUtils.uploadImage(juego.getImagen(), MyAwsS3Utils.Folder.ARTE_JUEGO, null);
                    //obtengo las imagenes que ya estan guardadas
                    if (original.getImagenesRompecabezas() != null) {
                        String imagenesViejas = original.getImagenesRompecabezas();
                        //obtengo el array de las url
                        String[] split = imagenesViejas.split(", ");
                        //elimino cada url
                        for (String url : split) {
                            filesUtils.deleteFile(url);
                        }
                    }
                    //subo la nueva imagen en pedazos
                    List<String> imagenesRompecabezas = this.getImagenesRompecabezas(filesUtils, juego.getImagen(), locale);
                    juego.setImagen(newUrl);
                    juego.setImagenesRompecabezas(imagenesRompecabezas.stream().collect(Collectors.joining(", ")));
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }else if(!original.getTipo().equals(Juego.Tipo.ROMPECABEZAS.getValue()) && juego.getTipo().equals(Juego.Tipo.ROMPECABEZAS.getValue())){
            if (juego.getImagen() == null || juego.getImagen().trim().isEmpty()) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "imagen");
            } else {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    List<String> imagenes = this.getImagenesRompecabezas(filesUtils, juego.getImagen(), locale);
                    juego.setImagen(filesUtils.uploadImage(juego.getImagen(), MyAwsS3Utils.Folder.ARTE_JUEGO, null));
                    juego.setImagenesRompecabezas(imagenes.stream().collect(Collectors.joining(", ")));
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }

            if (juego.getTiempo() == null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "cantPiezas, tiempo");
            }
        }

        juego.setFechaModificacion(Calendar.getInstance().getTime());
        juego.setUsuarioModificacion(usuario);

        try {
            em.merge(juego);
            em.flush();
            bitacoraBean.logAccion(Juego.class, juego.getIdMision(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (newUrl != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(newUrl);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
        
        if (newUrl != null && !currentUrl.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(currentUrl);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * borrado de juego de una mision
     * 
     * @param idMision id de mision
     * @param usuario id admin
     * @param locale locale
     * @return data eliminada
     */
    public Juego deleteJuego(String idMision, String usuario, Locale locale) {
        //busqueda de juego en la mision referente
        Juego juego = (Juego) em.createNamedQuery("Juego.findIdMision").setParameter("idMision", idMision).getSingleResult();
        Juego juego2 = juego;
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, juego.toString());
        if (juego == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_game_not_found");
        }
        List<MisionJuego> misionesJuegos = em.createNamedQuery("MisionJuego.findByIdJuego").setParameter("idMision", idMision).getResultList();
        if (!misionesJuegos.isEmpty()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "rewards_for_game");
        }

        em.remove(em.find(Juego.class, idMision));
        em.flush();
        bitacoraBean.logAccion(Juego.class, juego.getIdMision(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        return juego2;
    }

    /**
     * obtencion de juego por el id de mision
     * 
     * @param idMision id de mision
     * @param locale locale
     * @return data de juego
     */
    public Juego getJuegoByIdMision(String idMision, Locale locale) {
        Juego juego = em.find(Juego.class, idMision);
        if (juego == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_game_not_found");
        }
        return juego;
    }

    /**
     * obtencion de data de imagenes generadas de una imagen original para el rompecabezas
     * 
     * @param filesUtils utilidad de archivos remotos
     * @param base64 data de imagen a trabajar
     * @param locale locale
     * @return enlaces de imagenes resultantes
     * @throws IOException 
     */
    public List<String> getImagenesRompecabezas(MyAwsS3Utils filesUtils, String base64, Locale locale) throws IOException {
        BufferedImage image;
        List<String> listaUrl = new ArrayList<>();
        byte[] imageByte;

        imageByte = Base64.getDecoder().decode(base64);
        try (ByteArrayInputStream bis = new ByteArrayInputStream(imageByte)) {
            image = ImageIO.read(bis);
        }

        int rows = 3; //You should decide the values for rows and cols variables
        int cols = 3;
        int chunks = rows * cols;

        int chunkWidth = image.getWidth() / cols; // determines the chunk width and height
        int chunkHeight = image.getHeight() / rows;
        int count = 0;
        BufferedImage imgs[] = new BufferedImage[chunks]; //Image array to hold image chunks
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                //Initialize the image array with image chunks
                imgs[count] = new BufferedImage(chunkWidth, chunkHeight, image.getType());

                // draws the image chunk
                Graphics2D gr = imgs[count++].createGraphics();
                gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, chunkWidth * y, chunkHeight * x, chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight, null);
                gr.dispose();
            }
        }

        //writing mini images into image files
        for (BufferedImage img : imgs) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            byte[] byteArray = baos.toByteArray();
            listaUrl.add(filesUtils.uploadImage(Base64.getEncoder().encodeToString(byteArray), MyAwsS3Utils.Folder.IMAGENES_JUEGO_ROMPECABEZAS, null));
        }
        return listaUrl;
    }
}
