package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "WORKFLOW_LISTA_SEGMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WorkflowListaSegmento.findAll", query = "SELECT w FROM WorkflowListaSegmento w")
    , @NamedQuery(name = "WorkflowListaSegmento.findByIdSegmento", query = "SELECT w FROM WorkflowListaSegmento w WHERE w.workflowListaSegmentoPK.idSegmento = :idSegmento")
    , @NamedQuery(name = "WorkflowListaSegmento.findByIdWorkflow", query = "SELECT w FROM WorkflowListaSegmento w WHERE w.workflowListaSegmentoPK.idWorkflow = :idWorkflow")
    , @NamedQuery(name = "WorkflowListaSegmento.findByIndTipo", query = "SELECT w FROM WorkflowListaSegmento w WHERE w.indTipo = :indTipo")
    , @NamedQuery(name = "WorkflowListaSegmento.findByFechaCreacion", query = "SELECT w FROM WorkflowListaSegmento w WHERE w.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "WorkflowListaSegmento.findByUsuarioCreacion", query = "SELECT w FROM WorkflowListaSegmento w WHERE w.usuarioCreacion = :usuarioCreacion")})
public class WorkflowListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected WorkflowListaSegmentoPK workflowListaSegmentoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @JoinColumn(name = "ID_WORKFLOW", referencedColumnName = "ID_WORKFLOW", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Workflow workflow;
    @JoinColumn(name = "ID_SEGMENTO", referencedColumnName = "ID_SEGMENTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Segmento segmento;

    public WorkflowListaSegmento() {
    }

    public WorkflowListaSegmento(WorkflowListaSegmentoPK workflowListaSegmentoPK) {
        this.workflowListaSegmentoPK = workflowListaSegmentoPK;
    }

    public WorkflowListaSegmento(WorkflowListaSegmentoPK workflowListaSegmentoPK, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.workflowListaSegmentoPK = workflowListaSegmentoPK;
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public WorkflowListaSegmento(String idSegmento, String idWorkflow) {
        this.workflowListaSegmentoPK = new WorkflowListaSegmentoPK(idSegmento, idWorkflow);
    }
    
    public WorkflowListaSegmento(String idSegmento, String idWorkflow, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.workflowListaSegmentoPK = new WorkflowListaSegmentoPK(idSegmento, idWorkflow);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public WorkflowListaSegmentoPK getWorkflowListaSegmentoPK() {
        return workflowListaSegmentoPK;
    }

    public void setWorkflowListaSegmentoPK(WorkflowListaSegmentoPK workflowListaSegmentoPK) {
        this.workflowListaSegmentoPK = workflowListaSegmentoPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Workflow getWorkflow() {
        return workflow;
    }

    public void setWorkflow(Workflow workflow) {
        this.workflow = workflow;
    }

    public Segmento getSegmento() {
        return segmento;
    }

    public void setSegmento(Segmento segmento) {
        this.segmento = segmento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (workflowListaSegmentoPK != null ? workflowListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkflowListaSegmento)) {
            return false;
        }
        WorkflowListaSegmento other = (WorkflowListaSegmento) object;
        if ((this.workflowListaSegmentoPK == null && other.workflowListaSegmentoPK != null) || (this.workflowListaSegmentoPK != null && !this.workflowListaSegmentoPK.equals(other.workflowListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.WorkflowListaSegmento[ workflowListaSegmentoPK=" + workflowListaSegmentoPK + " ]";
    }

}
