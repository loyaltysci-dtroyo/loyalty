/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;


import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.service.ProductoListaSegmentoBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * clase de recursos http para el manejo de segmentos
 * incluidos/excluidos de productos
 * @author wtencio
 */
@Api(value = "Producto")
@Path("producto/{idProducto}/segmento")
public class ProductoListaSegmentoResource {
    
    @EJB
    MyKeycloakAuthz authzBean;//EJB con metodos para el manejo de autorizaciones
    
    @EJB
    ProductoListaSegmentoBean productoListaSegmentoBean;//EJB con los metodos de negocio para en el manejo de producto lista segmento(excluir/incluir)
    
    @Context
    SecurityContext context;
    
    @PathParam("idProducto")
    String idProducto;
    
     @Context
    HttpServletRequest request;
    
     /**
     * Metodo que incluye o excluye una lista de identificadores de segmento a
     * un producto por su identificador segun el parametro de accion, de
     * ocurrir un error se retornara una Respuesta con un estado erroneo junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param listaIdSegmento Lista de identificadores de segmento
     * @param accion Parametro con el indicador de la accion deseada
     * (incluir/excluir)
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asignacion de lista de segmentos a un producto")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idProducto", value = "Identificador de producto", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void assignBatchSegmento(
            @ApiParam(value = "Identificadores de segmentos", required = true) List<String> listaIdSegmento,
            @ApiParam(value = "Indicador de tipo de lista a asignar", required = true)
            @QueryParam("accion") String accion) {
        
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoListaSegmentoBean.includeExcludeBatchSegmento(idProducto,listaIdSegmento,accion.charAt(0), usuarioContext,request.getLocale());
    }
    
    /**
     * Metodo que elimina la inclusion o exclusion de una lista de
     * identificadores de segmento a un producto por su identificador, de
     * ocurrir un error se retornara una Respuesta con un estado erroneo junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param listaIdSegmento Lista de identificadores de segmento
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asignacion de lista de segmentos a un producto")
     @ApiImplicitParams({
        @ApiImplicitParam(name = "idProducto", value = "Identificador de producto", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("remover-lista")
    public void unassignBatchSegmento(
            @ApiParam(value = "Identificadores de segmentos", required = true) List<String> listaIdSegmento) {
        
         String usuarioContext;
         String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoListaSegmentoBean.removeBatchSegmento(idProducto,listaIdSegmento,usuarioContext,request.getLocale());
    }
    
    /**
     * Metodo que incluye o excluye un segmento a un producto por sus
     * identificadores segun el parametro de accion, de ocurrir un error se
     * retornara una Respuesta con un estado erroneo junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param idSegmento Identificador del segmento
     * @param accion Parametro con el indicador de la accion deseada
     * (incluir/excluir)
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asignacion de segmento a un producto")
     @ApiImplicitParams({
        @ApiImplicitParam(name = "idProducto", value = "Identificador de producto", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idSegmento}")
    public void assignSegmento(
            @ApiParam(value = "Identificador de segmento", required = true)
            @PathParam("idSegmento") String idSegmento,
            @ApiParam(value = "Indicador de tipo de lista a asignar", required = true)
            @QueryParam("accion") String accion) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoListaSegmentoBean.includeExcludeSegmento(idProducto,idSegmento,accion.charAt(0), usuarioContext,request.getLocale());
    }
    
    /**
     * Metodo que elimina la inclusion o exclusion de un segmento a un
     * producto por sus identificadores segun el parametro de accion, de
     * ocurrir un error se retornara una Respuesta con un estado erroneo junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idSegmento Identificador del segmento
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asignacion de segmento a un producto")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idProducto", value = "Identificador de producto", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idSegmento}")
    public void unassignSegmento(
            @ApiParam(value = "Identificador de segmento", required = true)
            @PathParam("idSegmento") String idSegmento) {
       
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoListaSegmentoBean.removeSegmento(idProducto,idSegmento, usuarioContext,request.getLocale());
    }
    
    /**
     * Metodo que obtiene un listado de segmentos incluidos, excluidos o
     * disponibles para un premio por su identificador segun el parametro de
     * accion y en un rango de registros segun los parametros de registro y
     * cantidad, de ocurrir un error se retornara una Respuesta con un estado
     * erroneo junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param accion Indicador del tipo de accion incluido/excluido/disponibles
     * @param estados
     * @param tipos
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param filtros
     * @param ordenTipo
     * @param ordenCampo
     * @param busqueda
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Obtener lista de segmentos de un producto",
            responseContainer = "List",
            response = Segmento.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
     @ApiImplicitParams({
        @ApiImplicitParam(name = "idProducto", value = "Identificador de producto", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSegmentos(
           @ApiParam(value = "Indicador del tipo de lista de segmentos") @QueryParam("accion") String accion,
            @ApiParam(value = "Indicadores de estado") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de tipo") @QueryParam("tipo") List<String> tipos,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad) {
   
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = productoListaSegmentoBean.getSegmentos(idProducto, accion, estados, tipos, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }
}
