/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CategoriaPremio;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.service.CategoriaPremioBean;
import com.flecharoja.loyalty.service.PremioBean;
import com.flecharoja.loyalty.service.PremioCategoriaPremioBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * clase de recursos http RESTful disponibles para el manejo de categorias de
 * premio
 *
 * @author wtencio
 */
@Api(value = "Categoria Premio")
@Path("categoria-premio")
public class CategoriaPremioResource {

    @EJB
    MyKeycloakAuthz authzBean;//EJB con metodos de negocio encargado para el manejo de autorizaciones

    @EJB
    CategoriaPremioBean categoriaPremioBean;//EJB con metodos de negocio para el manejo de categorias de premios

    @EJB
    PremioCategoriaPremioBean premioCategoriaPremioBean; //EJB con metodos de negocio para el manejo de asignacion de categorias a premios

    @EJB
    PremioBean premioBean;

    @Context
    SecurityContext context; //permite obtener informacion del usuario en sesion

    @Context
    HttpServletRequest request;

    /**
     * Metodo que registra la informacion de una nueva categoria de premio y
     * retorna el identificador creado de la misma bajo un estado de OK(200), de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param categoria Objeto con la informacion necesaria de una categoria de
     * premio para registrar
     * @return Respuesta OK con el identificador de la categoria de premio
     * creada
     */
    @ApiOperation(value = "Registrar categorias de premio",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertCategoriaPremio(
            @ApiParam(value = "Informacion de la categoria de premio", required = true) CategoriaPremio categoria) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PREMIO_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return categoriaPremioBean.insertCategoriaPremio(categoria, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que modifica la informacion de una categoria de premio existente y
     * retorna una respuesta bajo el estado de OK(200), de ocurrir un error se
     * retornara una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param categoria Objeto con la informacion necesaria de una categoria de
     * premio para registrar
     */
    @ApiOperation(value = "Modificacion de categoria de premio")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void editCategoriaPremio(
            @ApiParam(value = "Informacion de una categoria de premio", required = true) CategoriaPremio categoria) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PREMIO_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        categoriaPremioBean.editCategoriaPremio(categoria, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que elimina un registro de categoria de premio existente y retorna
     * una respuesta bajo el estado de OK(200), de ocurrir un error se retornara
     * una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param idCategoria Identificador de la categoria de premio deseada
     */
    @ApiOperation(value = "Eliminacion de categoria de premio")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idCategoria}")
    public void removeCategoriaPremio(
            @ApiParam(value = "Identificador de una categoria de promocion", required = true)
            @PathParam("idCategoria") String idCategoria) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PREMIO_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        categoriaPremioBean.removeCategoriaPremio(idCategoria, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que obtiene la informacion de una categoria de premio segun su
     * identificador bajo un estado de OK(200), de ocurrir un error se retornara
     * una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param idCategoria Identificador de la categoria de premio deseada
     * @return Respuesta OK con la informacion de la categoria de premio
     * encontrada
     */
    @ApiOperation(value = "Obtener una categoria de premio por identificador",
            response = CategoriaPremio.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idCategoria}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategoriaPremioPorId(
            @ApiParam(value = "Identificador de la categoria de premio", required = true)
            @PathParam("idCategoria") String idCategoria) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PREMIO_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = categoriaPremioBean.getCategoriaPremioPorId(idCategoria, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .build();

    }

    /**
     * Metodo que obtiene un listado de categorias de premios por un rango
     * establecido usando los parametros de cantidad y registro bajo un estado
     * de OK(200) ademas de encabezados indicando del rango aceptable maximo y
     * el rango actual de resultados, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param busqueda
     * @param ordenTipo
     * @param ordenCampo
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta OK con el listado de categorias de premio y los
     * encabezados de rango
     */
    @ApiOperation(value = "Obtener categorias de premio",
            responseContainer = "List",
            response = CategoriaPremio.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategoriasPremio(
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PREMIO_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = categoriaPremioBean.getCategoriasPremio(busqueda, ordenTipo, ordenCampo, registro, cantidad, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();

    }

    /**
     * Metodo que obtiene en termino de true/false la existencia de un nombre
     * interno de una categoria de promicion en algun registro de categorias de
     * premios ya existente, de ocurrir un error se retornara una respuesta con
     * un diferente estado junto con un encabezado "Error-Reason" con un valor
     * numerico indicando la naturaleza del error.
     *
     * @param nombreInterno Paramatro con el nombre interno a verificar
     * @return Respuesta con el valor booleano de la existencia del nombre
     * interno
     */
    @ApiOperation(value = "Verificacion de existencia de nombre interno en premio",
            response = Boolean.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("/exists/{nombreInterno}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean existsNombreInterno(
            @ApiParam(value = "Nombre interno de categoria promocion", required = true)
            @PathParam("nombreInterno") String nombreInterno) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PREMIO_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return categoriaPremioBean.existsNombreInterno(nombreInterno);
    }

    /**
     * Metodo que obtiene un listado de premios asignadas a una categoria de
     * premio segun el identificador de la categoria bajo un estado de OK(200)
     * por un rango establecido usando los parametros de cantidad y registro, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param estados
     * @param tipos
     * @param calendarizaciones
     * @param indEnvio
     * @param indRespuesta
     * @param categoria
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param idCategoria Identificador de la categoria de premio
     * @param accion
     * @param busqueda
     * @param ordenCampo
     * @param filtros
     * @param ordenTipo
     * @return Respuesta OK con el listado de promociones de una categoria de
     * promociones
     */
    @ApiOperation(value = "Obtencion de premios de una categoria de premio",
            responseContainer = "List",
            response = Premio.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idCategoria}/premio")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPremiosPorIdCategoria(
            @ApiParam(value = "Identificador de categoria de premio", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Indicadores de estado del premio") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de tipo del premio") @QueryParam("tipo") List<String> tipos,
            @ApiParam(value = "Indicadores de calendarizaciones") @QueryParam("calendarizacion") List<String> calendarizaciones,
            @ApiParam(value = "Indicadores de envio del premio") @QueryParam("envio") String indEnvio,
            @ApiParam(value = "Indicadores de respuesta") @QueryParam("respuesta") String indRespuesta,
            @ApiParam(value = "Indicadores de accion con respecto a la categoria") @QueryParam("accion") String accion,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros  sobre donde aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {

        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PREMIO_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = premioBean.getPremios(estados, tipos, calendarizaciones, idCategoria, accion, indRespuesta, indEnvio, busqueda, filtros, ordenTipo, ordenCampo, registro, cantidad, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Metodo que obtiene un listado de premios asignadas a una categoria de
     * premio segun el identificador de la categoria bajo un estado de OK(200)
     * por un rango establecido usando los parametros de cantidad y registro, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param idPremio Identificador de la categoria de premio
     * @return Respuesta OK con el listado de promociones de una categoria de
     * promociones
     */
    @ApiOperation(value = "Obtencion de premios de una categoria de premio",
            responseContainer = "List",
            response = Premio.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("/premio/{idPremio}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategoriasPorIdPremio(
            @ApiParam(value = "Identificador de  premio", required = true)
            @PathParam("idPremio") String idPremio,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {

        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = premioCategoriaPremioBean.getCategoriasPorIdPremio(idPremio, registro, cantidad, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Metodo que asigna una lista de identificadores de premio a una categoria
     * de premio por su identificador y retorna una respuesta con un estado de
     * OK(200), de ocurrir un error se retornara una respuesta con un diferente
     * estado junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param idCategoria Identificador de la categoria de premio
     * @param listaIdPremio Lista de identificadores de premios
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asignacion de listas de premios a categoria de premio")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{idCategoria}/premio")
    public void assignBatchCategoriaPremio(
            @ApiParam(value = "Identificador de categoria de premio", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Lista de identificadores de premios", required = true) List<String> listaIdPremio) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PREMIO_ESCRITURA, token, request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        premioCategoriaPremioBean.assignBatchCategoriaPremio(idCategoria, listaIdPremio, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que asigna una lista de identificadores de premio a una categoria
     * de premio por su identificador y retorna una respuesta con un estado de
     * OK(200), de ocurrir un error se retornara una respuesta con un diferente
     * estado junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param idCategoria Identificador de la categoria de premio
     * @param listaIdPremio Lista de identificadores de premios
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asignacion de una lista de premios a categoria de premios")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{idCategoria}/premio/remover-lista")
    public void unassignBatchCategoriaPremio(
            @ApiParam(value = "Identificador de categoria de premio", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Lista de identificadores de premios", required = true) List<String> listaIdPremio) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PREMIO_ESCRITURA, token, request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        premioCategoriaPremioBean.unassignBatchCategoriaPremio(idCategoria, listaIdPremio, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que asigna un premio a una categoria de premio usando sus
     * identificadores y retorna una respuesta con un estado de OK(200), de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idCategoria Identificador de la categoria de premio
     * @param idPremio Identificador del premio
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asignacion de un premio con una categoria de premio")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idCategoria}/premio/{idPremio}")
    public void assignCategoriaPremio(
            @ApiParam(value = "Identificador de categoria de premio", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Identificador de premio", required = true)
            @PathParam("idPremio") String idPremio) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PREMIO_ESCRITURA, token, request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        premioCategoriaPremioBean.assignCategoriaPremio(idCategoria, idPremio, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que remueve la asignacion de un premio a una categoria de premio
     * usando sus identificadores y retorna una respuesta con un estado de
     * OK(200), de ocurrir un error se retornara una respuesta con un diferente
     * estado junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param idCategoria Identificador de la categoria de premio
     * @param idPremio Identificador del premio
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asignacion de un premio con una categoria de premio")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idCategoria}/premio/{idPremio}")
    public void unassignCategoriaPremio(
            @ApiParam(value = "Identificador de categoria de premio", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Identificador de premio", required = true)
            @PathParam("idPremio") String idPremio) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PREMIO_ESCRITURA, token, request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        premioCategoriaPremioBean.unassignCategoriaPremio(idCategoria, idPremio, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que obtiene un listado de categorias no asignadas a un premio
     * segun el identificador del premio bajo un estado de OK(200) por un rango
     * establecido usando los parametros de cantidad y registro, de ocurrir un
     * error se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param idPremio Identificador del premio
     * @return Respuesta OK con el listado de promociones de una categoria de
     * promociones
     */
    @ApiOperation(value = "Obtencion de categorias que no estan asignadas a un premio",
            responseContainer = "List",
            response = CategoriaPremio.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("/premio/{idPremio}/categorias-desligadas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNoCategoriaPorPremio(
            @ApiParam(value = "Identificador del premio", required = true)
            @PathParam("idPremio") String idPremio,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {

        String token;
        try{
           token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = premioCategoriaPremioBean.getNoCategoriasPremio(idPremio, registro, cantidad, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }
}
