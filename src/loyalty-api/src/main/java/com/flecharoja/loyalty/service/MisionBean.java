package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionRedSocial;
import com.flecharoja.loyalty.model.MisionSubirContenido;
import com.flecharoja.loyalty.model.MisionVerContenido;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.Juego;
import com.flecharoja.loyalty.model.MisionEncuestaPregunta;
import com.flecharoja.loyalty.model.MisionJuego;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado del manejo de datos para el mantenimiento de misiones
 *
 * @author svargas
 */
@Stateless
public class MisionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    @EJB
    UtilBean utilBean;//EJB con los metodos de negocio para el manejo de utilidades

    /**
     * Metodo que obtiene la informacion de una mision por el identificador de
     * la misma
     *
     * @param idMision Identificador de la mision
     * @return Respuesta con la informacion de la mision encontrada
     */
    public Mision getMisionPorId(String idMision, Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        //verificacion de que la identidad exista
        if (mision == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
        }
        return mision;
    }

    /**
     * Obtencion de misiones en rango de registros (registro & cantidad) y
     * filtrado opcionalmente por indicadores y terminos de busqueda sobre
     * algunos campos
     *
     * @param estados Listado de valores de indicadores de estados deseados
     * @param tipos Listado de valores de indicadores de tipos de mision
     * deseados
     * @param aprobaciones Listado de valores de tipos de aprobaciones de mision
     * deseados
     * @param busqueda Terminos de busqueda
     * @param filtros Listado de campos sobre que aplicar los terminos de
     * busqueda
     * @param cantidad Cantidad de registros deseados
     * @param registro Numero de registro inicial en el resultado
     * @return Listado de misiones deseadas
     */
    public Map<String, Object> getMisiones(List<String> estados, List<String> tipos, List<String> aprobaciones, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Mision> root = query.from(Mision.class);

        query = Busquedas.getCriteriaQueryMisiones(cb, query, root, estados, tipos, aprobaciones, busqueda, filtros, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * Metodo que registra una nueva mision
     *
     * @param mision Objeto mision con la informacion de la misma
     * @param usuario Identificador del usuario en sesion
     * @param locale
     * @return Respuesta con el identificador de la mision creada
     */
    public String insertMision(Mision mision, String usuario, Locale locale) {
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        
        Mision.Tipos tipoMision = Mision.Tipos.get(mision.getIndTipoMision());
        if (tipoMision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indTipoMision");
        }

        if (mision.getIdMetrica() != null || mision.getIdMetrica().getIdMetrica() != null) {
            mision.setIdMetrica(em.getReference(Metrica.class, mision.getIdMetrica().getIdMetrica()));
        }

        mision.setIndEstado(Mision.Estados.BORRADOR.getValue());
        Date fecha = new Date();
        mision.setFechaCreacion(fecha);
        mision.setFechaModificacion(fecha);
        mision.setUsuarioCreacion(usuario);
        mision.setUsuarioModificacion(usuario);
        mision.setNumVersion(new Long(1));

        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (mision.getImagen() == null || mision.getImagen().trim().isEmpty() || mision.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
            mision.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                mision.setImagen(filesUtils.uploadImage(mision.getImagen(), MyAwsS3Utils.Folder.ARTE_MISION, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }
        //verificacion de atributos requeridos para la definicion de limites de respuesta
        if (mision.getIndRespuesta() != null && mision.getIndRespuesta()) {
            // si los valores son nulos se asumen valores por defecto (en lugar de lanzar exepcion)
            if (mision.getIndIntervaloTotal() != null && Mision.IntervalosTiempoRespuesta.get(mision.getIndIntervaloTotal()) == null || mision.getCantTotal() == null) {
                mision.setIndIntervaloTotal(Mision.IntervalosTiempoRespuesta.POR_SIEMPRE.getValue());
            }
            if (mision.getIndIntervaloMimebro() != null
                    && (Mision.IntervalosTiempoRespuesta.get(mision.getIndIntervaloMimebro()) == null || mision.getCantTotalMiembro() == null)) {
                mision.setIndIntervaloMimebro(Mision.IntervalosTiempoRespuesta.POR_SIEMPRE.getValue());
            }
            if (mision.getIndIntervalo() != null
                    && (Mision.IntervalosTiempoRespuesta.get(mision.getIndIntervalo()) == null || mision.getCantIntervalo() == null)) {
                mision.setIndIntervalo(Mision.IntervalosTiempoRespuesta.POR_SIEMPRE.getValue());
            }
        }

        if (mision.getEncabezadoArte() == null) {
            mision.setEncabezadoArte(mision.getNombre());
        }

        if (mision.getTextoArte() == null) {
            mision.setTextoArte(mision.getDescripcion());
        }

        mision.setIndCalendarizacion(Mision.IndicadoresCalendarizacion.PERMANENTE.getValue());

        if (tipoMision!=Mision.Tipos.JUEGO_PREMIO || mision.getIndAprovacion()!=null) {
            if (Mision.Aprobaciones.get(mision.getIndAprovacion()) == null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "challenge_approvement_invalid");
            }
        } else {
            mision.setIndAprovacion(Mision.Aprobaciones.AUTOMATICO.getValue());
        }

        try {
            em.persist(mision);
            em.flush();
        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (mision.getImagen() != null && !mision.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(mision.getImagen());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        bitacoraBean.logAccion(Mision.class, mision.getIdMision(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);

        return mision.getIdMision();
    }

    /**
     * Metodo que modifica la informacion de una modificacion
     *
     * @param mision Objecto con la informacion de la mision a editar
     * @param usuario Identificador del usuario en sesion
     * @param locale
     */
    public void editMision(Mision mision, String usuario, Locale locale) {
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        //verificacion de que la entidad existe
        Mision original;
        try {
            original = em.find(Mision.class, mision.getIdMision());
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "challenge_id_invalid");
        }
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
        }

        mision.setIndTipoMision(original.getIndTipoMision());

        Mision.Estados estadoMision = Mision.Estados.get(mision.getIndEstado());
        if (estadoMision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indEstado");
        }
        Mision.Estados estadoOriginal = Mision.Estados.get(original.getIndEstado());
        //verificacion de que posible cambio de estado sea valido (Activo/Inactivo NO A Borrador) y de edicion de entidad archivada
        if ((estadoOriginal == Mision.Estados.PUBLICADO || estadoOriginal == Mision.Estados.INACTIVO) && estadoMision == Mision.Estados.BORRADOR) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_not_change_state");
        }
        if (estadoOriginal == Mision.Estados.ARCHIVADO) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", this.getClass().getSimpleName());
        }

        //establecimiento de valores de atributos por defecto
        Date fecha = new Date();
        mision.setFechaModificacion(fecha);
        mision.setUsuarioModificacion(usuario);
        if (estadoMision == Mision.Estados.PUBLICADO) {
            mision.setFechaPublicacion(fecha);

            if (mision.getEncabezadoArte() == null || mision.getTextoArte() == null) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_display");
            }

            switch (Mision.Tipos.get(mision.getIndTipoMision())) {
                case ENCUESTA: {
                    List<MisionEncuestaPregunta> preguntas = em.createNamedQuery("MisionEncuestaPregunta.findAllByIdMision").setParameter("idMision", mision.getIdMision()).getResultList();
                    if (preguntas.isEmpty()) {
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_details_not_set");
                    }
                    if (preguntas.stream().anyMatch((pregunta) -> {
                        if (MisionEncuestaPregunta.TiposPregunta.get(pregunta.getIndTipoPregunta())!=MisionEncuestaPregunta.TiposPregunta.ENCUESTA) {
                            return false;
                        }
                        switch (MisionEncuestaPregunta.TiposRespuesta.get(pregunta.getIndTipoRespuesta())) {
                            case SELECCION_UNICA: {
                                return pregunta.getRespuestas().isEmpty() || pregunta.getRespuestas().split("\n").length<1;
                            }
                            case SELECCION_MULTIPLE: {
                                return pregunta.getRespuestas().isEmpty() || pregunta.getRespuestas().split("\n").length<2;
                            }
                            default: {
                                return false;
                            }
                        }
                    })) {
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "questions_without_answers");
                    }
                    break;
                }
                case PERFIL: {
                    if (((long) em.createNamedQuery("MisionPerfilAtributo.countAllByIdMision").setParameter("idMision", mision.getIdMision()).getSingleResult()) == 0) {
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_details_not_set");
                    }
                    break;
                }
                case RED_SOCIAL: {
                    if (em.find(MisionRedSocial.class, mision.getIdMision()) == null) {
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_details_not_set");
                    }
                    break;
                }
                case SUBIR_CONTENIDO: {
                    if (em.find(MisionSubirContenido.class, mision.getIdMision()) == null) {
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_details_not_set");
                    }
                    break;
                }
                case VER_CONTENIDO: {
                    if (em.find(MisionVerContenido.class, mision.getIdMision()) == null) {
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_details_not_set");
                    }
                    break;
                }
                case PREFERENCIAS: {
                    if (((long) em.createNamedQuery("MisionPreferencia.countByIdMision").setParameter("idMision", mision.getIdMision()).getSingleResult()) == 0) {
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_details_not_set");
                    }
                    break;
                }
                case JUEGO_PREMIO: {
                    Juego juego = em.find(Juego.class, mision.getIdMision());
                    if (juego == null) {
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_details_not_set");
                    }
                    if (Juego.Tipo.get(juego.getTipo()) != null && Juego.Tipo.get(juego.getTipo()).getValue() != Juego.Tipo.ROMPECABEZAS.getValue()) {
                        if (((long) em.createNamedQuery("MisionJuego.findGracias").setParameter("idMision", mision.getIdMision()).setParameter("tipoPremio", MisionJuego.Tipo.GRACIAS.getValue()).getSingleResult()) == 0) {
                            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "thank_reward");
                        }
                    }
                    if (juego.getTipo().equals(Juego.Tipo.RASPADITA.getValue()) && ((long) em.createNamedQuery("MisionJuego.countByIdJuego").setParameter("idMision", mision.getIdMision()).getSingleResult()) < 6) {
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_details_not_set");
                    } else if (juego.getTipo().equals(Juego.Tipo.RULETA.getValue()) && ((long) em.createNamedQuery("MisionJuego.countByIdJuego").setParameter("idMision", mision.getIdMision()).getSingleResult()) < 7) {
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_details_not_set");
                    } else if (juego.getTipo().equals(Juego.Tipo.ROMPECABEZAS.getValue()) && ((long) em.createNamedQuery("MisionJuego.countByIdJuego").setParameter("idMision", mision.getIdMision()).getSingleResult()) < 1) {
                        throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_details_not_set");
                    }
                    break;
                }
                case REALIDAD_AUMENTADA:{
                    if(mision.getMisionRealidadAumentada().getIdBlippar()<=0){
                         throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idBlippar");
                    }
                }
                
            }

            if (mision.getIndCalendarizacion() == null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indCalendarizado");
            }
            //verificacion de valores validos en los indicadores de dias y num de semana y fechas de calendarizacion
            if (mision.getIndCalendarizacion().equals(Mision.IndicadoresCalendarizacion.CALENDARIZADO.getValue())) {
                if (mision.getFechaInicio() == null || mision.getFechaFin() == null || mision.getFechaInicio().compareTo(mision.getFechaFin()) > 0) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "challenge_badly_scheduled");
                }

                if (mision.getIndSemanaRecurrencia() != null && (mision.getIndSemanaRecurrencia().compareTo(BigInteger.ONE) < 0 || mision.getIndSemanaRecurrencia().compareTo(BigInteger.valueOf(52)) > 0)) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indSemanaRecurrencia");
                }

                if (mision.getIndDiaRecurrencia() != null) {
                    char[] validValues = {'L', 'M', 'K', 'J', 'V', 'S', 'D'};
                    mision.setIndDiaRecurrencia(mision.getIndDiaRecurrencia().toUpperCase());
                    for (char validValue : validValues) {
                        if (mision.getIndDiaRecurrencia().indexOf(validValue) != mision.getIndDiaRecurrencia().lastIndexOf(validValue)) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indDiaRecurrencia");
                        }
                    }
                }
            } else {
                if (!mision.getIndCalendarizacion().equals(Mision.IndicadoresCalendarizacion.PERMANENTE.getValue())) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indCalendarizado");
                }
            }
        }

        String urlImagenNueva = null;
        String urlImagenOriginal = original.getImagen();

        //si el atributo de imagen viene nula, se utiliza la predeterminada
        if (mision.getImagen() == null || mision.getImagen().trim().isEmpty()) {
            urlImagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            mision.setImagen(urlImagenNueva);
        } else {
            //ver si el valor en el atributo de imagen, difiere del almacenado
            if (!urlImagenOriginal.equals(mision.getImagen())) {
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    urlImagenNueva = filesUtils.uploadImage(mision.getImagen(), MyAwsS3Utils.Folder.ARTE_MISION, null);
                    mision.setImagen(urlImagenNueva);
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }
        //verificacion de atributos requeridos para la definicion de limites de respuesta
        if (mision.getIndRespuesta() != null && mision.getIndRespuesta()) {
            if (Mision.IntervalosTiempoRespuesta.get(mision.getIndIntervaloTotal()) == null || mision.getCantTotal() == null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indIntervaloTotal, cantTotal");
            }
            if (mision.getIndIntervaloMimebro() != null) {
                if (Mision.IntervalosTiempoRespuesta.get(mision.getIndIntervaloMimebro()) == null || mision.getCantTotalMiembro() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indIntervaloMiembro, cantTotalMiembro");
                }
                if (mision.getIndIntervalo() != null && (Mision.IntervalosTiempoRespuesta.get(mision.getIndIntervalo()) == null || mision.getCantIntervalo() == null)) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indIntervalo, cantIntervalo");
                }
            }
        }

        if (Mision.Aprobaciones.get(mision.getIndAprovacion()) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "challenge_approvement_invalid");
        }

        try {
            em.merge(mision);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//en caso de que alguna informacion no este valida o este "vieja"
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(Mision.class, mision.getIdMision(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        if (urlImagenNueva != null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Metodo que elimina o archiva una mision segun por el valor de su
     * indicador de estado
     *
     * @param idMision Identificador de la mision
     * @param usuario Identificador del usuario en sesion
     * @param locale
     */
    public void deleteMision(String idMision, String usuario, Locale locale) {
        //verificacion que la entidad exista
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
        }

        if (mision.getIndEstado().equals(Mision.Estados.BORRADOR.getValue())) {
            em.remove(mision);

            if (!mision.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(mision.getImagen());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }

            bitacoraBean.logAccion(Mision.class, idMision, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        } else {
            if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", this.getClass().getSimpleName());
            } else {
                Date fecha = new Date();
                mision.setFechaModificacion(fecha);

                mision.setUsuarioModificacion(usuario);
                mision.setIndEstado(Mision.Estados.ARCHIVADO.getValue());

                em.merge(mision);
                bitacoraBean.logAccion(Mision.class, idMision, usuario, Bitacora.Accion.ENTIDAD_ARCHIVADA.getValue(), locale);
            }
        }
    }
}
