/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CategoriaProducto;
import com.flecharoja.loyalty.model.Producto;
import com.flecharoja.loyalty.model.SubcategoriaProducto;
import com.flecharoja.loyalty.service.ProductoCatSubcategoriaBean;
import com.flecharoja.loyalty.service.SubCategoriaProductoBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * clase con recursos http para el manejo de subcategorias de productos
 *
 * @author wtencio
 */
@Api(value = "Categorias de productos")
@Path("categoria-producto")
public class SubCategoriaProductoResource {

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos para el manejo de autorizaciones

    @EJB
    SubCategoriaProductoBean subcategoriaProductoBean;//EJB con los metodos de negocio para el manejo de subcategorias de productos

    @EJB
    ProductoCatSubcategoriaBean productoCatSubcategoriaBean;//EJB con los metodos para el manejo de asignacion de categorias y subcategorias a un producto

    @Context
    SecurityContext context;//permite obtener informacion del usuario en sesion

    @Context
    HttpServletRequest request;

    /**
     * Metodo que registra la informacion de una nueva subcategoria de premio y
     * retorna el identificador creado de la misma bajo un estado de OK(200), de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param subcategoria Objeto con la informacion necesaria de una
     * subcategoria de producto para registrar
     * @param idCategoria identificador de la categoria
     * @return Respuesta OK con el identificador de la subcategoria de producto
     * creada
     */
    @ApiOperation(value = "Registrar subcategorias de productos",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("/{idCategoria}/subcategoria")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertSubCategoriaProducto(
            @ApiParam(value = "Informacion de la subcategoria de producto", required = true) SubcategoriaProducto subcategoria,
            @ApiParam(value = "Identificador de la categoria")
            @PathParam("idCategoria") String idCategoria) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return subcategoriaProductoBean.insertSubCategoriaProducto(subcategoria, usuarioContext, idCategoria, request.getLocale());
    }

    /**
     * Metodo que modifica la informacion de una subcategoria de producto
     * existente y retorna una respuesta bajo el estado de OK(200), de ocurrir
     * un error se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param subcategoria Objeto con la informacion necesaria de una
     * subcategoria de producto para registrar
     * @param idCategoria identificador de la categoria
     * @return Respuesta con estado de la operacion
     */
    @ApiOperation(value = "Modificacion de subcategoria de producto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("/{idCategoria}/subcategoria")
    @Consumes(MediaType.APPLICATION_JSON)
    public void editSubCategoriaProducto(
            @ApiParam(value = "Informacion de una subcategoria de producto", required = true) SubcategoriaProducto subcategoria,
            @ApiParam(value = "Identificador de la categoria")
            @PathParam("idCategoria") String idCategoria) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        subcategoriaProductoBean.editCategoriaProducto(subcategoria, usuarioContext, idCategoria, request.getLocale());
    }

    /**
     * Metodo que elimina un registro de subcategoria de producto existente y
     * retorna una respuesta bajo el estado de OK(200), de ocurrir un error se
     * retornara una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param idSubcategoria Identificador de la categoria de premio deseada
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de subcategoria de producto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("/{idCategoria}/subcategoria/{idSubcategoria}")
    public void removeSubCategoriaProducto(
            @ApiParam(value = "Identificador de una subcategoria de producto", required = true)
            @PathParam("idSubcategoria") String idSubcategoria) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        subcategoriaProductoBean.removeSubCategoriaProducto(idSubcategoria, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que obtiene la informacion de una categoria de producto segun su
     * identificador bajo un estado de OK(200), de ocurrir un error se retornara
     * una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param idSubcategoria Identificador de la categoria de producto deseada
     * @param idCategoria identificador de la categoria
     * @return Respuesta OK con la informacion de la categoria de producto
     * encontrada
     */
    @ApiOperation(value = "Obtener una subcategoria de producto de una categoria dada",
            response = SubcategoriaProducto.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("/{idCategoria}/subcategoria/{idSubcategoria}")
    @Produces(MediaType.APPLICATION_JSON)
    public SubcategoriaProducto getCategoriaProductoPorId(
            @ApiParam(value = "Identificador de la subcategoria de producto", required = true)
            @PathParam("idSubcategoria") String idSubcategoria,
            @ApiParam(value = "Identificador de la categoria")
            @PathParam("idCategoria") String idCategoria) {

        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PRODUCTO_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return subcategoriaProductoBean.getSubCategoriaProductoPorId(idSubcategoria, idCategoria, request.getLocale());

    }

    /**
     * Metodo que obtiene un listado de subcategorias de productos por un rango
     * establecido usando los parametros de cantidad y registro bajo un estado
     * de OK(200) ademas de encabezados indicando del rango aceptable maximo y
     * el rango actual de resultados, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param idCategoria dientificador de la categoria
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta OK con el listado de categorias de premio y los
     * encabezados de rango
     */
    @ApiOperation(value = "Obtener subcategorias de producto",
            responseContainer = "List",
            response = SubcategoriaProducto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("/{idCategoria}/subcategoria")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubCategorias(
            @ApiParam(value = "Identificador de la categoria", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }

        Map<String, Object> respuesta = subcategoriaProductoBean.getSubCategorias(idCategoria, registro, cantidad, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Metodo de redireccion al metodo de obtencion de lista de subcategorias de
     * producto
     *
     * @param idCategoria identificador de la categoria
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta con el listado de los premios en un rango valido junto
     * con encabezados sobre el rango actual
     */
    @GET
    @Path("/{idCategoria}/subcategoria/buscar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchSubCategoriasProducto(
            @ApiParam(value = "Identificador de la categoria", required = true)
            @PathParam("idCategoria") String idCategoria,
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @QueryParam("registro") int registro) {
        return this.getSubCategorias(idCategoria, cantidad, registro);
    }

    /**
     * Metodo que obtiene un listado de subcategorias de producto filtrados por
     * palabras claves contenidas en el parametro busqueda y en un rango
     * establecido usando los parametros de cantidad y registro, ademas de
     * encabezados indicando del rango aceptable maximo y el rango actual de
     * resultados, de ocurrir un error se retornara una respuesta con un
     * diferente estado junto con un encabezado "Error-Reason" con un valor
     * numerico indicando la naturaleza del error.
     *
     * @param idCategoria identificador de la categoria
     * @param busqueda Parametro con las palabras claves separadas por un
     * espacio
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta con el listado de premios en un rango valido junto con
     * encabezados sobre el rango actual
     */
    @ApiOperation(value = "Busqueda de subcategorias de producto",
            responseContainer = "List",
            response = SubcategoriaProducto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("/{idCategoria}/subcategoria/buscar/{busqueda}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchSubCategoriasProducto(
            @ApiParam(value = "Identificador de la categoria", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Palabras claves de busqueda", required = true)
            @PathParam("busqueda") String busqueda,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {

        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PREMIO_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = subcategoriaProductoBean.searchSubCategoriasProducto(idCategoria, busqueda, cantidad, registro, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();

    }

    /**
     * Metodo que obtiene en termino de true/false la existencia de un nombre
     * interno de una subcategoria de producto en algun registro de categorias
     * de premios ya existente, de ocurrir un error se retornara una respuesta
     * con un diferente estado junto con un encabezado "Error-Reason" con un
     * valor numerico indicando la naturaleza del error.
     *
     * @param nombreInterno Paramatro con el nombre interno a verificar
     * @return Respuesta con el valor booleano de la existencia del nombre
     * interno
     */
    @ApiOperation(value = "Verificacion de existencia de nombre interno en subcategoria producto",
            response = Boolean.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("subcategoria/exists/{nombreInterno}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean existsNombreInterno(
            @ApiParam(value = "Nombre interno de categoria producto", required = true)
            @PathParam("nombreInterno") String nombreInterno) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PRODUCTO_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return subcategoriaProductoBean.existsNombreInterno(nombreInterno);
    }

    /**
     * **************************************************************
     * Manejo de asignar una categoria y subcategoria a un producto
     ***************************************************************
     */
    /**
     * Metodo que asigna un producto una categoria y una subcategoria usando sus
     * identificadores y retorna una respuesta con un estado de OK(200), de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idCategoria identificador de la categoria
     * @param idSubcatgeoria Identificador de la categoria de producto
     * @param idProducto Identificador del producto
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asignacion de una categoria y subcategoria a un producto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("/{idCategoria}/subcategoria/{idSubCategoria}/producto/{idProducto}")
    public void assignCatSubCategotiaProducto(
            @ApiParam(value = "Identificador de la categoria", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Identificador de la subcategoria de producto", required = true)
            @PathParam("idSubCategoria") String idSubcatgeoria,
            @ApiParam(value = "Identificador del producto", required = true)
            @PathParam("idProducto") String idProducto) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoCatSubcategoriaBean.assingProductoSubcategoriaCategoria(idProducto, idSubcatgeoria, idCategoria, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que remueve la asignacion de un producto con una categoria y
     * subcategoria de producto usando sus identificadores y retorna una
     * respuesta con un estado de OK(200), de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param idSubcategoria Identificador de la subcategoria de producto
     * @param idProducto Identificador del producto
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asignacion de un producto con una categoria y subcatgeoria de producto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("/subcategoria/{idSubCategoria}/producto/{idProducto}")
    public void unassignCatSubCategoriaProducto(
            @ApiParam(value = "Identificador de la subcategoria de producto", required = true)
            @PathParam("idSubCategoria") String idSubcategoria,
            @ApiParam(value = "Identificador de producto", required = true)
            @PathParam("idProducto") String idProducto) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoCatSubcategoriaBean.unassingProductoSubcategoriaCategoria(idProducto, idSubcategoria, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que obtiene un listado de producto asignadas a una categoria y
     * subcategoria de producto segun el identificador de la categoria bajo un
     * estado de OK(200) por un rango establecido usando los parametros de
     * cantidad y registro, de ocurrir un error se retornara una respuesta con
     * un diferente estado junto con un encabezado "Error-Reason" con un valor
     * numerico indicando la naturaleza del error.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param idProducto Identificador de la categoria de premio
     * @return Respuesta OK con el listado de productos de una categoria de
     * productos
     */
    @ApiOperation(value = "Obtencion de productos de una categoria y subcategoria de productos",
            responseContainer = "List",
            response = Producto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("/producto/{idProducto}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubcategoriasProducto(
            @ApiParam(value = "Identificador de  producto", required = true)
            @PathParam("idProducto") String idProducto,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {

        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = productoCatSubcategoriaBean.getSubcategoriasProducto(idProducto, cantidad, registro,request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Metodo que asigna una lista de identificadores de productos a una
     * categoria y subcategoria de producto por su identificador y retorna una
     * respuesta con un estado de OK(200), de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param idCategoria identificador de la categoria
     * @param idSubcategoria Identificador de la subcategoria de producto
     * @param listaIdProducto Lista de identificadores de productos
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asignacion de listas de productos a una categoria y subcategoria de producto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{idCategoria}/subcategoria/{idSubcategoria}/producto")
    public void assingBatchProductoSubcategoriaCategoria(
            @ApiParam(value = "Identificador de la categoria")
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Identificador de subcategoria de producto", required = true)
            @PathParam("idSubcategoria") String idSubcategoria,
            @ApiParam(value = "Lista de identificadores de productos", required = true) List<String> listaIdProducto) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoCatSubcategoriaBean.assingBatchProductoSubcategoriaCategoria(listaIdProducto, idSubcategoria, idCategoria, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que desasigna una lista de identificadores de productos a una
     * categoria y subcategoria de producto por su identificador y retorna una
     * respuesta con un estado de OK(200), de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param idSubcategoria Identificador de la subcategoria de producto
     * @param listaIdProducto Lista de identificadores de productos
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asignacion de una lista de productos a una categoria y subcategoria de productos")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{idSubcategoria}/producto/remover-lista")
    public void unassignBatchCategoriaPremio(
            @ApiParam(value = "Identificador de subcategoria de producto", required = true)
            @PathParam("idSubcategoria") String idSubcategoria,
            @ApiParam(value = "Lista de identificadores de productos", required = true) List<String> listaIdProducto) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
           throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoCatSubcategoriaBean.unassingBatchProductoSubcategoriaCategoria(idSubcategoria, listaIdProducto, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que obtiene un listado de productos no asignadas a una categoria y
     * subcategoria de producto segun el identificador de la subcategoria bajo
     * un estado de OK(200) por un rango establecido usando los parametros de
     * cantidad y registro, de ocurrir un error se retornara una respuesta con
     * un diferente estado junto con un encabezado "Error-Reason" con un valor
     * numerico indicando la naturaleza del error.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param idSubcategoria Identificador de la subcategoria de producto
     * @return Respuesta OK con el listado de productos de una categoria de
     * producto
     */
    @ApiOperation(value = "Obtencion de productos que no estan dentro de una categoria y subcategoria de producto",
            responseContainer = "List",
            response = Producto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idSubcategoria}/producto-desligado")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNoProductosPorIdSubcategoriaCategoria(
            @ApiParam(value = "Identificador de subcategoria de producto", required = true)
            @PathParam("idSubcategoria") String idSubcategoria,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {

        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CATEGORIAS_PRODUCTO_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = productoCatSubcategoriaBean.getNoProductosPorIdSubcategoriaCategoria(idSubcategoria, registro, cantidad,request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Metodo que obtiene un listado de categorias no asignadas a un producto
     * OK(200) por un rango establecido usando los parametros de cantidad y
     * registro, de ocurrir un error se retornara una respuesta con un diferente
     * estado junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param idProducto Identificador de la subcategoria de producto
     * @return Respuesta OK con el listado de productos de una categoria de
     * producto
     */
    @ApiOperation(value = "Obtencion de categorias que no estan asignados a un producto",
            responseContainer = "List",
            response = CategoriaProducto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("producto/{idProducto}/categoria-desligado")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategoriaNotInProducto(
            @ApiParam(value = "Identificador de subcategoria de producto", required = true)
            @PathParam("idProducto") String idProducto,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {

        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = productoCatSubcategoriaBean.getCategoriaNotInProducto(idProducto, cantidad, registro,request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Metodo que obtiene un listado de subcategorias no asignadas a un producto
     * OK(200) por un rango establecido usando los parametros de cantidad y
     * registro, de ocurrir un error se retornara una respuesta con un diferente
     * estado junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param idCategoria identificador de la categoria
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param idProducto Identificador del producto
     * @return Respuesta OK con el listado de subcategorias disponibles para un
     * producto
     */
    @ApiOperation(value = "Obtencion de subcategorias que no estan asignados a un producto",
            responseContainer = "List",
            response = SubcategoriaProducto.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("producto/{idProducto}/categoria/{idCategoria}/subcategoria-disponible")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubcategoriasDisponiblesProducto(
            @ApiParam(value = "Identificador del producto", required = true)
            @PathParam("idProducto") String idProducto,
            @ApiParam(value = "Identificador de la categoria de producto", required = true)
            @PathParam("idCategoria") String idCategoria,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {

        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
             throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = productoCatSubcategoriaBean.getSubCategoriasDisponiblesProducto(idProducto, idCategoria, cantidad, registro,request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }
}
