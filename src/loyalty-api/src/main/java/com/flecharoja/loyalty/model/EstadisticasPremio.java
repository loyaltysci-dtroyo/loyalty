package com.flecharoja.loyalty.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class EstadisticasPremio {
    
    @XmlRootElement
    public class Periodo {
        private String mes;
        private long cantRedenciones;
        private long cantElegiblesPromedio;

        public Periodo(String mes, long cantRedenciones, long cantElegiblesPromedio) {
            this.mes = mes;
            this.cantRedenciones = cantRedenciones;
            this.cantElegiblesPromedio = cantElegiblesPromedio;
        }

        public String getMes() {
            return mes;
        }

        public void setMes(String mes) {
            this.mes = mes;
        }

        public long getCantRedenciones() {
            return cantRedenciones;
        }

        public void setCantRedenciones(long cantRedenciones) {
            this.cantRedenciones = cantRedenciones;
        }

        public long getCantElegiblesPromedio() {
            return cantElegiblesPromedio;
        }

        public void setCantElegiblesPromedio(long cantElegiblesPromedio) {
            this.cantElegiblesPromedio = cantElegiblesPromedio;
        }
    }
    
    private String idPremio;
    private String nombrePremio;
    private String nombreInternoPremio;
    
    private long cantRedenciones;
    private long cantIntentario;
    
    private long cantMiembrosElegibles;
    private long cantRedencionesElegibles;
    
    private List<Periodo> periodos;

    public EstadisticasPremio() {
        periodos = new ArrayList<>();
    }

    public String getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(String idPremio) {
        this.idPremio = idPremio;
    }

    public String getNombrePremio() {
        return nombrePremio;
    }

    public void setNombrePremio(String nombrePremio) {
        this.nombrePremio = nombrePremio;
    }

    public String getNombreInternoPremio() {
        return nombreInternoPremio;
    }

    public void setNombreInternoPremio(String nombreInternoPremio) {
        this.nombreInternoPremio = nombreInternoPremio;
    }

    public long getCantRedenciones() {
        return cantRedenciones;
    }

    public void setCantRedenciones(long cantRedenciones) {
        this.cantRedenciones = cantRedenciones;
    }

    public long getCantIntentario() {
        return cantIntentario;
    }

    public void setCantIntentario(long cantIntentario) {
        this.cantIntentario = cantIntentario;
    }

    public long getCantMiembrosElegibles() {
        return cantMiembrosElegibles;
    }

    public void setCantMiembrosElegibles(long cantMiembrosElegibles) {
        this.cantMiembrosElegibles = cantMiembrosElegibles;
    }

    public long getCantRedencionesElegibles() {
        return cantRedencionesElegibles;
    }

    public void setCantRedencionesElegibles(long cantRedencionesElegibles) {
        this.cantRedencionesElegibles = cantRedencionesElegibles;
    }

    public List<Periodo> getPeriodos() {
        return periodos;
    }

    public void setPeriodos(List<Periodo> periodos) {
        this.periodos = periodos;
    }
    
    public void addPeriodo(String mes, long cantRedenciones, long cantElegiblesPromedio) {
        this.periodos.add(new Periodo(mes, cantRedenciones, cantElegiblesPromedio));
    }
}
