package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author svargas
 */
@Embeddable
public class InstanciaNotificacionPK implements Serializable {

    @Column(name = "ID_INSTANCIA")
    private String idInstancia;
    
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;

    public InstanciaNotificacionPK() {
    }

    public InstanciaNotificacionPK(String idInstancia, String idMiembro) {
        this.idInstancia = idInstancia;
        this.idMiembro = idMiembro;
    }

    public String getIdInstancia() {
        return idInstancia;
    }

    public void setIdInstancia(String idInstancia) {
        this.idInstancia = idInstancia;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInstancia != null ? idInstancia.hashCode() : 0);
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstanciaNotificacionPK)) {
            return false;
        }
        InstanciaNotificacionPK other = (InstanciaNotificacionPK) object;
        if ((this.idInstancia == null && other.idInstancia != null) || (this.idInstancia != null && !this.idInstancia.equals(other.idInstancia))) {
            return false;
        }
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.InstanciaNotificacionPK[ idInstancia=" + idInstancia + ", idMiembro=" + idMiembro + " ]";
    }
    
}
