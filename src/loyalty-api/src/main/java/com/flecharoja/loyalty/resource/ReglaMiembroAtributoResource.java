package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.ReglaMiembroAtb;
import com.flecharoja.loyalty.service.ReglaMiembroAtributoBean;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase de recursos http para el acceso a funcionalidades del manejo de reglas
 * de atributos de miembro
 *
 * @author svargas
 */
@Api(value = "Segmento")
@Path("segmento/{idSegmento}/reglas/{idLista}/miembro-atributo")
public class ReglaMiembroAtributoResource {

    @EJB
    ReglaMiembroAtributoBean bean; //EJB con los metodos de negocio para el manejo de reglas de cambio de metrica

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión
    
    @Context
    HttpServletRequest request;

    /**
     * Identificador de la lista de reglas
     */
    @PathParam("idLista")
    String idLista; //Identificador de la lista de reglas

    /**
     * Identificador del segmento
     */
    @PathParam("idSegmento")
    String idSegmento; //identificador del segmento

    /**
     * Metodo que obtiene un listado de todas las reglas de este tipo contenidas
     * en una lista de reglas especifica, en el caso de error se retorna una
     * respuesta con estado erroneo con un encabezado "Error-Reason" con un
     * valor numerico indicando la naturaleza del error.
     *
     * @return Respuesta con el listado de reglas encontradas
     */
    @ApiOperation(value = "Obtener reglas miembro atributo de lista de segmento",
            responseContainer = "List",
            response = ReglaMiembroAtb.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idSegmento", value = "Identificador de segmento", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "idLista", value = "Identificador de lista de reglas", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List getReglasLista() {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bean.getReglasMiembroAtributoLista(idLista,request.getLocale());
    }

    /**
     * Metodo que registra la informacion de una nueva regla a una lista de
     * reglas existente, en el caso de error se retorna una respuesta con estado
     * erroneo con un encabezado "Error-Reason" con un valor numerico indicando
     * la naturaleza del error.
     *
     * @param reglaMiembroAtributo Objeto con la informacion de la regla
     * @return Respuesta con el identificador de la regla creada
     */
    @ApiOperation(value = "Registrar regla miembro atributo",
            response = String.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idSegmento", value = "Identificador de segmento", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "idLista", value = "Identificador de lista de reglas", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertRegla(
            @ApiParam(value = "Informacion de regla miembro atributo", required = true) ReglaMiembroAtb reglaMiembroAtributo) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bean.createReglaMiembroAtributo(idSegmento, idLista, reglaMiembroAtributo, usuario,request.getLocale());
    }

    /**
     * Metodo que elimina la informacion de una regla existente de ser posible
     * de una lista de reglas, en el caso de error se retorna una respuesta con
     * estado erroneo con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param idRegla Identificador de la regla
     */
    @ApiOperation(value = "Remover regla miembro atributo")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idSegmento", value = "Identificador de segmento", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "idLista", value = "Identificador de lista de reglas", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idRegla}")
    public void removeRegla(
            @ApiParam(value = "Identificador de regla", required = true)
            @PathParam("idRegla") String idRegla) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.deleteReglaMiembroAtributo(idSegmento, idLista, idRegla, usuario,request.getLocale());
    }
}
