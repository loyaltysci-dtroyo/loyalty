package com.flecharoja.loyalty.util;

import java.text.SimpleDateFormat;

/**
 *
 * @author svargas, wtencio
 */
public class Indicadores {
    
    public static final String TIPO_ORDEN_PREDETERMINADO = "DESC";
    public static final String CAMPO_ORDEN_PREDETERMINADO = "FC";
     
    public static final char INCLUIDO = 'I';
    public static final char EXCLUIDO = 'E';
    public static final char ASIGNADO = 'A';
    public static final char DISPONIBLES = 'D';
    
    public static final char IND_ENTIDAD_MIEMBRO = 'M';
    public static final char IND_ENTIDAD_SEGMENTO = 'S';
    public static final char IND_ENTIDAD_UBICACION = 'U';
    
    public static final String PAGINACION_RANGO_ACEPTADO = "Accept-Range";
    public static final int PAGINACION_RANGO_ACEPTADO_VALOR = 50;
    public static final String PAGINACION_RANGO_DEFECTO = "10";
    public static final String PAGINACION_RANGO_CONTENIDO = "Content-Range";
    
    public static final String URL_IMAGEN_PREDETERMINADA = "https://loyalty.flecharoja.com/resources/img/generic0.jpg";
    public static String CONTRASENA_TEMPORAL_PREDETERMINADA = "ABcd@123";
    
    public static final SimpleDateFormat ANGULAR_DATE_STRING_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    
    public static final String[] PUNTOS_MILLONARIO = {
        "100","200","300","400","500","750","1000","1500","2000"
            ,"3000","5000","7500","10000","15000","30000"
    };
}
