/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author wtencio
 */
@Embeddable
public class JuegoPremioJuegoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_PREMIO_JUEGO")
    private String idPremioJuego;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ID_JUEGO")
    private String idJuego;

    public JuegoPremioJuegoPK() {
    }

    public JuegoPremioJuegoPK(String idPremioJuego, String idJuego) {
        this.idPremioJuego = idPremioJuego;
        this.idJuego = idJuego;
    }

    public String getIdPremioJuego() {
        return idPremioJuego;
    }

    public void setIdPremioJuego(String idPremioJuego) {
        this.idPremioJuego = idPremioJuego;
    }

    public String getIdJuego() {
        return idJuego;
    }

    public void setIdJuego(String idJuego) {
        this.idJuego = idJuego;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPremioJuego != null ? idPremioJuego.hashCode() : 0);
        hash += (idJuego != null ? idJuego.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JuegoPremioJuegoPK)) {
            return false;
        }
        JuegoPremioJuegoPK other = (JuegoPremioJuegoPK) object;
        if ((this.idPremioJuego == null && other.idPremioJuego != null) || (this.idPremioJuego != null && !this.idPremioJuego.equals(other.idPremioJuego))) {
            return false;
        }
        if ((this.idJuego == null && other.idJuego != null) || (this.idJuego != null && !this.idJuego.equals(other.idJuego))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.JuegoPremioJuegoPK[ idPremioJuego=" + idPremioJuego + ", idJuego=" + idJuego + " ]";
    }
    
}
