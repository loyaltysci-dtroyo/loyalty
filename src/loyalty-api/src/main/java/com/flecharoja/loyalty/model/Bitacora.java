/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "BITACORA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bitacora.findAll", query = "SELECT b FROM Bitacora b ORDER BY b.fecha DESC")
    ,
    @NamedQuery(name = "Bitacora.findById", query = "SELECT b FROM Bitacora b WHERE b.id = :id")
    ,
    @NamedQuery(name = "Bitacora.findByAccion", query = "SELECT b FROM Bitacora b WHERE b.accion = :accion ORDER BY b.fecha DESC")
    ,
    @NamedQuery(name = "Bitacora.findByDetalle", query = "SELECT b FROM Bitacora b WHERE b.detalle = :detalle ORDER BY b.fecha DESC")
    ,
    @NamedQuery(name = "Bitacora.countAll", query = "SELECT COUNT(b.id) FROM Bitacora b")
})
public class Bitacora implements Serializable {

    public enum Accion {
        ENTIDAD_INSERTADA("A"),
        ENTIDAD_ACTUALIZA("B"),
        ENTIDAD_ELIMINADA("C"),
        ENTIDAD_PUBLICADA("D"),
        ENTIDAD_ARCHIVADA("E"),
        ENTIDAD_DESACTIVADA("F"),
        ENTIDAD_ACTIVADA("G"),
        ENTIDAD_DRAFT("H");

        private final String value;
        private static final Map<String, Accion> lookup = new HashMap<>();

        private Accion(String value) {
            this.value = value;
        }
        
        static{
            for(Accion accion : Accion.values()){
                lookup.put(accion.value, accion);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static Accion get(String value){
            return value==null?null:lookup.get(value);
        }
        
        

    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ACCION")
    private String accion;

    @Size(max = 500)
    @Column(name = "DETALLE")
    private String detalle;

    @JoinColumn(name = "USUARIO", referencedColumnName = "ID_USUARIO")
    @ManyToOne(optional = false)
    private Usuario usuario;

    public Bitacora() {
    }

    public Bitacora(Long id) {
        this.id = id;
    }

    public Bitacora(Long id, String accion) {
        this.id = id;
        this.accion = accion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bitacora)) {
            return false;
        }
        Bitacora other = (Bitacora) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Bitacora[ id=" + id + " ]";
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}
