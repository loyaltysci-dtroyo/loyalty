package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.ReglaAsignacionBadge;
import com.flecharoja.loyalty.service.ReglaAsignacionBadgeBean;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author faguilar
 */
@Api(value = "ReglaAsignacionBadge")
@Path("insignias/{idInsignia}/regla-asignacion")
public class ReglaAsignacionBadgeResource {
     private static final Logger LOG = Logger.getLogger(ReglaAsignacionBadgeResource.class.getName());
    @Context
    SecurityContext context;//permite obtener información del usuario  en sesión

    @EJB
    MyKeycloakAuthz authzBean;//EJB con metodos de negocio para el manejo de autorizacion

    @EJB
    ReglaAsignacionBadgeBean asignacionBadgeBean;//EJB con metodos de negocio para el manejo de badges

    @Context
    HttpServletRequest request;

    /**
     * Identificador del segmento
     */
    @PathParam("idInsignia")
    String idInsignia; //identificador del badge

    /**
     * Metodo que registra una nueva regla de badge, de ocurrir un error se
     * retornara una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param reglaAsignacionBadge Objecto con la informacion de regla de
     * badge requerido para su registro
     * @return Respuesta con el identificador del badge creado
     */
    @ApiOperation(value = "Regla de Asignacion Badge",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertReglaAsignacionBadge(@ApiParam(value = "Informacion de la regla de asignacion de badge", required = true) ReglaAsignacionBadge reglaAsignacionBadge) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return asignacionBadgeBean.createReglaAsignacionBadge(idInsignia,reglaAsignacionBadge, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que modifica la informacion de una regla de asignacion de badge,
     * de ocurrir un error se retornara una respuesta con un diferente estado
     * junto con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param reglaAsignacionBadge Objecto con la informacion actualizada de
     * una promocion existente para su registro
     */
    @ApiOperation(value = "Modificacion de regla de asignacion de badge")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void editReglaAsignacionBadge(
            @ApiParam(value = "Informacion de la regla de asignacion de badge", required = true) ReglaAsignacionBadge reglaAsignacionBadge) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        asignacionBadgeBean.updateReglaAsignacionBadge(idInsignia, reglaAsignacionBadge, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que elimina o archiva una regla de badge segun ciertas
     * condiciones, de ocurrir un error se retornara una respuesta con un
     * diferente estado junto con un encabezado "Error-Reason" con un valor
     * numerico indicando la naturaleza del error.
     *
     * @param idRegla Identificador de la regla de badge
     */
    @ApiOperation(value = "Eliminacion/Archivado de regla de badge")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idRegla}")
    public void deleteReglaAsignacionBadge(
            @ApiParam(value = "Identificador de regla", required = true)
            @PathParam("idRegla") String idRegla) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        asignacionBadgeBean.deleteReglaAsignacionBadge(idRegla, usuarioContext, request.getLocale());
    }

    /* Metodo que obtiene la informacion de una regla de asignacion de badge segun su identificador
     * en el parametro de idRegla, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param idRegla Identificador de la promocion.
     * @return Respuesta con la informacion del badge deseado.
     */
    @ApiOperation(value = "Obtener regla por identificador",
            response = ReglaAsignacionBadge.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idRegla}")
    @Produces(MediaType.APPLICATION_JSON)
    public ReglaAsignacionBadge getReglaPorId(
            @ApiParam(value = "Identificador de Regla", required = true)
            @PathParam("idRegla") String idRegla) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return asignacionBadgeBean.getRegla(idRegla, request.getLocale());
    }

    /**
     * Metodo que obtiene un listado de reglas, de ocurrir un error se retornara
     * una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     */
    @ApiOperation(value = "Obtener lista de reglas",
            responseContainer = "List",
            response = ReglaAsignacionBadge.class
    )
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListaReglas() {
        String token;
        List respuesta = null;
        try {
            try {
                token = request.getHeader("Authorization").substring(6);
            } catch (NullPointerException e) {
                throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
            }
            if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_LECTURA, token, request.getLocale())) {
                throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
            }
            respuesta =  asignacionBadgeBean.getReglasLista(this.idInsignia,request.getLocale());
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e.getCause());
        }
        return Response.ok().entity(respuesta).build();

    }
}
