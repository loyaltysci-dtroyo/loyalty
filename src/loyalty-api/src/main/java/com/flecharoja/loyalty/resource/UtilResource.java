package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Pais;
import com.flecharoja.loyalty.service.UtilBean;
import com.flecharoja.loyalty.util.Indicadores;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * clase con recursos http para el manejo de recursos de utilidad
 *
 * @author wtencio
 */
@Api(value = "Util")
@Path("util")
public class UtilResource {

    @EJB
    UtilBean utilBean;//EJB con los metodos de negocio para el manejo de utilidades

    @Context
    SecurityContext context;

    @Context
    HttpServletRequest request;

    /**
     * Metodo que acciona una tarea, la cual obtiene la lista de paises en el
     * caso de error al ejecutar la tarea se retorna un 500 (error interno del
     * servidor) con un mensaje en el encabezado, de no ser asi se retorna un
     * 200 (OK) con un resultado de ser requerido este.
     *
     * @return listado de paises en formato JSON
     */
    @ApiOperation(value = "Obtener registros de bitácora filtrado por una acción, por defecto todos",
            responseContainer = "List",
            response = Pais.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("paises")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pais> getPaises() {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return utilBean.getPaises();

    }
    /**
     * Metodo que acciona una tarea, la cual obtiene el país buscado en el
     * caso de error al ejecutar la tarea se retorna un 500 (error interno del
     * servidor) con un mensaje en el encabezado, de no ser asi se retorna un
     * 200 (OK) con un resultado de ser requerido este.
     *
     * @return país en formato JSON
     */
    @ApiOperation(value = "Obtener la información de un país por medio del alfa 3",
            response = Pais.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("pais/{alfa3}")
    @Produces(MediaType.APPLICATION_JSON)
    public Pais getPaisPorNombre(
            @ApiParam(value="Identificador por nombre de país", required = true)
            @PathParam("alfa3") String alfa3 ) {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return utilBean.getPaisPorNombre(alfa3, request.getLocale());

    }

    /**
     * Metodo que acciona una tarea, la cual obtiene una lista de resultados de
     * diversas sugerencias de palabras relacionadas al estado y ciudad de un
     * miembro o ubicacion en el caso de error al ejecutar la tarea se retorna
     * un 500 (error interno del servidor) con un mensaje en el encabezado, de
     * no ser asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param valor cadena de texto con valores a buscar
     * @param entidad identificador de la entidad a buscar (ubicacion o miembro)
     * @param atributo identificador del atributo de la entidad a
     * buscar(ubicacion->ciudad o estado, miembro->ciudadresidencia,
     * estadoresidencia)
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Obtener sugerencias de valores que ya se encuentren en la base de datos y puedan servir",
            responseContainer = "List",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("sugerencias/{busqueda}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSugerencias(
            @ApiParam(value = "Palabra para la búsqueda", required = true)
            @PathParam("busqueda") String valor,
            @ApiParam(value = "Indicador de entidad en la que se desea buscar", required = true)
            @QueryParam("entidad") String entidad,
            @ApiParam(value = "Indicador del atributo en el que se desea buscar", required = true)
            @QueryParam("atributo") String atributo) {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        Map<String, Object> respuesta = utilBean.getSugerencias(entidad.charAt(0), atributo, valor, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();

    }

    /**
     * Metodo que acciona una tarea, el cual obtiene de todos los miembros por
     * estado o todos por defecto las edades clasificadas segun un rango en el
     * caso de error al ejecutar la tarea se retorna un 500 (error interno del
     * servidor) con un mensaje en el encabezado, de no ser asi se retorna un
     * 200 (OK) con un resultado de ser requerido este.
     *
     * @param indicador
     * @return
     */
    @ApiOperation(value = "Obtener cantidad de personas registradas en el sistema por rango de edad")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("rango-edad")
    @Produces(MediaType.APPLICATION_JSON)
    public String getRangosEdades(
            @ApiParam(value = "Indicador de miembros activos")
            @QueryParam("indicador") String indicador) {
        try {
            context.getUserPrincipal().getName();
            
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return utilBean.getMiembrosRangoEdad(indicador, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, el cual obtiene la clasificacion de los
     * miembros segun el genero y por estado( activos o por defecto todos) caso
     * de error al ejecutar la tarea se retorna un 500 (error interno del
     * servidor) con un mensaje en el encabezado, de no ser asi se retorna un
     * 200 (OK) con un resultado de ser requerido este.
     *
     * @param indicador
     * @return
     */
    @ApiOperation(value = "Obtener cantidad de personas registradas en el sistema por genero")
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("rango-genero")
    @Produces(MediaType.APPLICATION_JSON)
    public String getRangoGenero(
            @ApiParam(value = "Indicador de miembros activos")
            @QueryParam("indicador") String indicador) {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return utilBean.getCantGenero(indicador);
    }

    
}
