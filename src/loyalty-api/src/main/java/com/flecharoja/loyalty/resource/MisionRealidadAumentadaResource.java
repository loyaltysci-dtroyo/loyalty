/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.MisionRealidadAumentada;
import com.flecharoja.loyalty.service.MisionRealidadAumentadaBean;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author faguilar
 */
@Api(value = "Mision")
@Path("mision/{idMision}/realidad-aumentada")
public class MisionRealidadAumentadaResource {
    
    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @EJB
    MisionRealidadAumentadaBean realidadAumentadaBean;

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión
    
    @Context
    HttpServletRequest request;
    
     /**
     * Identificador de la mision
     */
    @PathParam("idMision")
    String idMision;

    /**
     * Metodo que obtiene la definicion de reto de RA de una mision
     * especifica segun su tipo, en el caso de error se retorna una respuesta
     * con estado erroneo con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @return Respuesta con la informacion de la definicion de realidad aumentada de
     * una mision
     */
    @ApiOperation(value = "Obtener el reto de la mision de RA",
            response = MisionRealidadAumentada.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public MisionRealidadAumentada getRealidadAumentadaMision() {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return realidadAumentadaBean.getRealidadAumentadaMision(idMision,request.getLocale());
    }

       
    /**
     * Metodo que registra o edita la informacion de una mision de realidad aumentada en una
     * mision existente, en el caso de error se retorna una respuesta con estado
     * erroneo con un encabezado "Error-Reason" con un valor numerico indicando
     * la naturaleza del error.
     *
     * @param redSocial Objeto con la informacion de red social de la
     * mision
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Registrar o editar definicion de realidad aumentada de la mision")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void createEditRedSocialMision(
            @ApiParam(value = "Definicion de realidad aumentada", required = true) MisionRealidadAumentada redSocial) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        realidadAumentadaBean.createEditRealidadAumentadaMision(idMision, redSocial, usuario,request.getLocale());
    }

    /**
     * Metodo que elimina la informacion de una mision de realidad aumentada en una
     * mision existente, en el caso de error se retorna una respuesta con estado
     * erroneo con un encabezado "Error-Reason" con un valor numerico indicando
     * la naturaleza del error.
     *
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Remover definicion de realidad aumentada de la mision")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    public void removeRedSocialMision() {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        realidadAumentadaBean.removeRealidadAumentadaMision(idMision, usuario,request.getLocale());
    }
    

}
