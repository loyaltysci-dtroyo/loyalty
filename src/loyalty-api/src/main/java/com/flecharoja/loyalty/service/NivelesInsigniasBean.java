/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Insignias;
import com.flecharoja.loyalty.model.NivelesInsignias;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * Clase encargada de proveer metodos para el mantenimiento de la informacion
 * niveles de insignias
 *
 * @author wtencio
 */
@Stateless
public class NivelesInsigniasBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con metodos de negocio para el manejo de bitacora

    @EJB
    UtilBean utilBean;//EJB con metodos de negocio para el manejo de utilidades

    /**
     * Método que registra un nuevo nivel de una insignia en particular
     *
     * @param nivelInsignias información de la insignia
     * @param usuario usuario en sesión
     * @param insignia id de la insignia en la que va a ser registrado el nivel
     * @param locale
     * @return id de la nueva insignia
     */
    public String insertNivelInsignia(NivelesInsignias nivelInsignias, String usuario, String insignia, Locale locale) {
        Insignias temp = em.find(Insignias.class, insignia);

        if (temp == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "badge_level_not_found");
        }

        if (temp.getTipo().equals(Insignias.Tipos.STATUS.getValue())) {
            //si la imagen viene nula, se le establece un url de imagen predefinida
            if (nivelInsignias.getImagen() == null || nivelInsignias.getImagen().trim().isEmpty()) {
                nivelInsignias.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
            } else {
                //si no, se le intenta subir a cloudfiles
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    nivelInsignias.setImagen(filesUtils.uploadImage(nivelInsignias.getImagen(), MyAwsS3Utils.Folder.ARTE_NIVEL_INSIGNIA, null));
                } catch (Exception e) {
                    //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }

            //establecimiento de atributos por defecto
            Date date = Calendar.getInstance().getTime();

            nivelInsignias.setFechaCreacion(date);
            nivelInsignias.setUsuarioCreacion(usuario);
            nivelInsignias.setNombreInterno(nivelInsignias.getNombreInterno().toUpperCase());
            nivelInsignias.setUsuarioModificacion(usuario);
            nivelInsignias.setFechaModificacion(date);
            nivelInsignias.setNumVersion(new Long(1));
            nivelInsignias.setIdInsignia(temp);

            try {
                em.persist(nivelInsignias);
                em.flush();
                bitacoraBean.logAccion(NivelesInsignias.class, nivelInsignias.getIdNivel(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
            } catch (ConstraintViolationException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
                //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
                if (!nivelInsignias.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                    try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                        filesUtils.deleteFile(nivelInsignias.getImagen());
                    } catch (Exception ex) {
                        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                    }
                }
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }
        } else {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "badge_type_not_valid");
        }

        return nivelInsignias.getIdNivel();
    }

    /**
     * Método que actualiza la información de un nivel de insignia
     *
     * @param nivelInsignia información del nivel de insignia
     * @param usuario usuario en sesión
     * @param locale
     */
    public void updateNivelInsignia(NivelesInsignias nivelInsignia, String usuario, Locale locale) {
        //verificacion que el nivel de insignia exista
        NivelesInsignias tempNivel = em.find(NivelesInsignias.class, nivelInsignia.getIdNivel());

        if (tempNivel == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "badge_level_not_found");
        }

        String urlImagenOriginal = tempNivel.getImagen();//url imagen original
        String urlImagenNueva = null;//url imagen nueva
        //si el atributo de imagen viene nula (no deberia)
        if (nivelInsignia.getImagen() == null || nivelInsignia.getImagen().trim().isEmpty()) {
            //establecimiento de la imagen por defecto
            urlImagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            nivelInsignia.setImagen(urlImagenNueva);
        } else //ver si el valor en el atributo de imagen, difiere del almacenado
        if (!urlImagenOriginal.equals(nivelInsignia.getImagen())) {//si la imagen es diferente a la almacenada
            //se le intenta subir
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                urlImagenNueva = filesUtils.uploadImage(nivelInsignia.getImagen(), MyAwsS3Utils.Folder.ARTE_NIVEL_INSIGNIA, null);
                nivelInsignia.setImagen(urlImagenNueva);
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }

        //establecimiento de valores por defecto
        nivelInsignia.setUsuarioModificacion(usuario);
        nivelInsignia.setFechaModificacion(Calendar.getInstance().getTime());

        try {
            em.merge(nivelInsignia);
            em.flush();
            bitacoraBean.logAccion(NivelesInsignias.class, nivelInsignia.getIdNivel(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
        
        if (urlImagenNueva != null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Método que elimina un nivel de insignia de la base de datos
     *
     * @param idNivel identificador del nivel de insignia
     * @param usuario usuario en sesión
     * @param locale
     */
    public void deleteNivelInsignia(String idNivel, String usuario,Locale locale) {

        //REVISAR QUE NO ESTE EN NINGUNA LISTA DE REGLAS DE NIVELES
        NivelesInsignias nivel = em.find(NivelesInsignias.class, idNivel);
        if(nivel==null){
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "badge_level_not_found");
        }
        //se obtiene el url de la imagen almacenada de la entidad y borra
        String url = nivel.getImagen();
        if (!url.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(url);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
        em.remove(nivel);
        bitacoraBean.logAccion(NivelesInsignias.class, idNivel, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

    /**
     * Metodo que obtiene un listado de niveles de insignias almacenadas
     *
     * @param idInsignia identificador unico de la insignia al que pertenece el
     * nivel
     * @return Respuesta con una coleccion de niveles encontrados
     */
    public List<NivelesInsignias> getNivelesInsignias(String idInsignia) {
       return em.createNamedQuery("NivelesInsignias.findNivelesByIdInsignia").setParameter("idInsignia", idInsignia).getResultList();
    }

    /**
     * Metodo que a partir de un id proveido busca y retorna de ser posible la
     * informacion de un nivel de metrica con el mismo id
     *
     * @param idNivel Valor del id del nivel de metrica deseada
     * @param idInsignia identificador de la insignia
     * @param locale
     * @return Respuesta con la informacion del nivel de la insignia encontrada
     */
    public NivelesInsignias getNivel(String idNivel, String idInsignia,Locale locale) {
        NivelesInsignias nivel = em.find(NivelesInsignias.class, idNivel);
        //verificacion que la entidad haya sido encontrado
        if (nivel == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "badge_level_not_found");
        }
        if (!nivel.getIdInsignia().getIdInsignia().equals(idInsignia)) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "badge_level_not_badge");
        }
       return nivel;
    }

    /**
     * Metodo que verifica si existe un nivel de insignia almacenado con el
     * mismo nombre interno que el proveido
     *
     * @param nombre Valor del nombre interno a verificar
     * @return Expresion booleana de la existencia de un nombre interno igual
     */
    public Boolean existsNombreInterno(String nombre) {
        //se obtiene la cantidad de registros con el mismo nombre interno y se pregunta si este es mayor que 0 (existe al menos uno)
        return ((Long) em.createNamedQuery("NivelesInsignias.countByNombreInterno").setParameter("nombreInterno", nombre.toUpperCase()).getSingleResult()) > 0;
    }
}
