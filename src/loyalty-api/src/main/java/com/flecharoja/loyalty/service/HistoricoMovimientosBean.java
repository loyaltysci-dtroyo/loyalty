/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.HistoricoMovimientos;

import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.PremioMiembro;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class HistoricoMovimientosBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;

    /**
     * guardado de historico de movimientos
     * 
     * @param historico data
     * @param usuario id admin
     * @param locale locale
     */
    public void insertHistorico(HistoricoMovimientos historico, String usuario, Locale locale) {
        try {
            em.persist(historico);
            em.flush();
            bitacoraBean.logAccion(HistoricoMovimientos.class, historico.getIdHistorico(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * edicion del estado del historico de movimientos
     * 
     * @param historico data
     * @param usuario id admin
     * @param locale locale
     */
    public void editEstadoHistorico(HistoricoMovimientos historico, String usuario, Locale locale) {
        HistoricoMovimientos original = em.find(HistoricoMovimientos.class, historico.getIdHistorico());
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "history_not_found");
        }
        
        //verificacion de estado
        if (HistoricoMovimientos.Status.get(historico.getStatus()) == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "status");
        }
        
        //verificacion de premio de miembro
        PremioMiembro premioMiembro = em.find(PremioMiembro.class, historico.getIdPremioMiembro());
        if(premioMiembro == null){
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_member_not_found");
        }
        
        //segun el estado de historico...
        if(historico.getStatus()==HistoricoMovimientos.Status.REDIMIDO_RETIRADO.getValue()){
            premioMiembro.setEstado(PremioMiembro.Estados.REDIMIDO_RETIRADO.getValue());
            premioMiembro.setFechaReclamo(new Date());
        }else if(historico.getStatus()==HistoricoMovimientos.Status.ANULADO.getValue()){
            premioMiembro.setEstado(PremioMiembro.Estados.ANULADO.getValue());
        }
        try {
     
            em.merge(historico);
            em.merge(premioMiembro);
            bitacoraBean.logAccion(HistoricoMovimientos.class, historico.getIdHistorico(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {//en el caso que la informacion no sea valida
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
    }

    /**
     * obtencion de historicos
     * 
     * @param estados estados de historicos
     * @param tipoMovimiento tipos de movimientos
     * @param premio id premio
     * @param ubicacion id ubicacion
     * @param miembro id miembro
     * @param cantidad cantidad
     * @param registro # registro
     * @param locale locale
     * @return lista de historicos
     */
    public Map<String, Object> getHistorico(List<String> estados, List<String> tipoMovimiento, String premio, String ubicacion, String miembro,  int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<HistoricoMovimientos> root = query.from(HistoricoMovimientos.class);

        //obtencion de query segun los parametros de entrada
        query = Busquedas.getCriteriaQueryHistoricoMovimientos(cb, query, root, estados, tipoMovimiento, premio, ubicacion, miembro);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();

        //retorno del resultado y encabezados sobre el rango
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

}
