/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Producto;
import com.flecharoja.loyalty.model.ProductoListaUbicacion;
import com.flecharoja.loyalty.model.ProductoListaUbicacionPK;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * clase encargada para el manejo de los elegibles de ubicaciones de un producto
 *
 * @author wtencio
 */
@Stateless
public class ProductoListaUbicacionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que asigna (incluye o excluye) a una ubicacion de un producto
     *
     * @param idProducto Identificador del producto
     * @param idUbicacion Identificador de la ubicacion
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesión
     * @param locale
     */
    public void includeExcludeUbicaciones(String idProducto, String idUbicacion, Character indAccion, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Ubicacion ubicacion = em.find(Ubicacion.class, idUbicacion);
        if (ubicacion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
        }
        Producto producto = em.find(Producto.class, idProducto);
        if (producto == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
        }

        //verificacion de entidades archivadas/borrador
        if (producto.getIndEstado().equals(Producto.Estados.ARCHIVADO.getValue())
                || !ubicacion.getIndEstado().equals(Ubicacion.Estados.PUBLICADO_ACTIVO.getValue())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "operation_over_archived", "Producto, Ubicacion");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            ProductoListaUbicacion productoListaUbicacion = new ProductoListaUbicacion(idUbicacion, idProducto, indAccion, date, usuario);

            em.merge(productoListaUbicacion);
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        bitacoraBean.logAccion(ProductoListaUbicacion.class, idProducto + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);

    }

    /**
     * Método que revoca la asignacion de una ubicacion a un producto
     *
     * @param idProducto identificador del producto
     * @param idUbicacion identificador de la ubicacion
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeUbicacion(String idProducto, String idUbicacion, String usuario, Locale locale) {
        Producto producto = em.find(Producto.class, idProducto);
        if (producto == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
        }
        try {

            em.getReference(Ubicacion.class, idUbicacion).getIdUbicacion();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
        }
        //entidad de entidad archivada
        if (producto.getIndEstado().equals(Producto.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Producto");
        }

        em.remove(em.getReference(ProductoListaUbicacion.class, new ProductoListaUbicacionPK(idUbicacion, idProducto)));
        em.flush();
        bitacoraBean.logAccion(ProductoListaUbicacion.class, idProducto + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

    /**
     * Método que asigna (incluye o excluye) una lista de ubicaciones de un
     * producto
     *
     * @param idProducto identificador del producto
     * @param listaIdUbicaciones lista de identificadores de ubicaciones
     * @param indAccion tipo de accion (incluye/excluye)
     * @param usuario usuario en sesion
     * @param locale
     */
    public void includeExcludeBatchUbicacion(String idProducto, List<String> listaIdUbicaciones, Character indAccion, String usuario, Locale locale) {
        Producto producto = em.find(Producto.class, idProducto);
        if (producto == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
        }
        try {

            listaIdUbicaciones = listaIdUbicaciones.stream().distinct().collect(Collectors.toList());

            for (String idUbicacion : listaIdUbicaciones) {
                em.getReference(Ubicacion.class, idUbicacion).getIdUbicacion();
            }

        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
        }

        //entidad de entidad archivada
        if (producto.getIndEstado().equals(Producto.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Producto");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea una valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            for (String idUbicacion : listaIdUbicaciones) {
                em.merge(new ProductoListaUbicacion(idUbicacion, idProducto, indAccion, date, usuario));
            }

        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        for (String idUbicacion : listaIdUbicaciones) {
            bitacoraBean.logAccion(ProductoListaUbicacion.class, idProducto + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        }
    }

    /**
     * Método que elimina una lista de ubicaciones de un producto
     *
     * @param idProducto identificador del producto
     * @param listaIdUbicaciones lista de identificadores de ubicaciones
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeBatchUbicacion(String idProducto, List<String> listaIdUbicaciones, String usuario, Locale locale) {
        Producto producto = em.find(Producto.class, idProducto);
        if (producto == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
        }
        try {
            listaIdUbicaciones = listaIdUbicaciones.stream().distinct().collect(Collectors.toList());
            listaIdUbicaciones.stream().forEach((idUbicacion) -> {
                em.getReference(Ubicacion.class, idUbicacion).getIdUbicacion();
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
        }
        //entidad de entidad archivada
        if (producto.getIndEstado().equals(Producto.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Producto");
        }

        for (String idUbicacion : listaIdUbicaciones) {
            em.remove(em.getReference(ProductoListaUbicacion.class, new ProductoListaUbicacionPK(idUbicacion, idProducto)));
        }
        em.flush();

        for (String idMiembro : listaIdUbicaciones) {
            bitacoraBean.logAccion(ProductoListaUbicacion.class, idProducto + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        }

    }

    /**
     * Método que obtiene un listado de ubicaciones asignados a un producto en
     * un rango de respuesta y opcionalmente por el tipo de acción
     *
     * @param idProducto identificador del producto
     * @param indAccion indicador de tipo de accion deseado
     * @param estados
     * @param calendarizaciones
     * @param registro numero de registro inicial
     * @param filtros
     * @param ordenTipo
     * @param ordenCampo
     * @param busqueda
     * @param cantidad cantidad de registros deseados
     * @param locale
     * @return Respuesta con el listado de ubicaciones y su rango de respuesta
     */
   public Map<String, Object> getUbicaciones(String idProducto, String indAccion, List<String> estados,List<String> calendarizaciones, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int registro, int cantidad, Locale locale) {
        //verificacion de que el rango este dentro del valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Ubicacion> root = query.from(Ubicacion.class);

        query = Busquedas.getCriteriaQueryUbicaciones(cb, query, root, estados, calendarizaciones, busqueda, filtros, ordenTipo, ordenCampo);

        Subquery<String> subquery = query.subquery(String.class);
        Root<ProductoListaUbicacion> rootSubquery = subquery.from(ProductoListaUbicacion.class);
        subquery.select(rootSubquery.get("productoListaUbicacionPK").get("idUbicacion"));
        if (indAccion == null) {
            subquery.where(cb.equal(rootSubquery.get("productoListaUbicacionPK").get("idProducto"), idProducto));
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idUbicacion")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idUbicacion")).value(subquery)));
            }
        } else {
            switch (indAccion.toUpperCase().charAt(0)) {
                case Indicadores.INCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("productoListaUbicacionPK").get("idProducto"), idProducto), cb.equal(rootSubquery.get("indTipo"), Indicadores.INCLUIDO));
                    if (query.getRestriction() != null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idUbicacion")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idUbicacion")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.EXCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("productoListaUbicacionPK").get("idProducto"), idProducto), cb.equal(rootSubquery.get("indTipo"), Indicadores.EXCLUIDO));
                    if (query.getRestriction() != null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idUbicacion")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idUbicacion")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.DISPONIBLES: {
                    subquery.where(cb.equal(rootSubquery.get("productoListaUbicacionPK").get("idProducto"), idProducto));
                    if (query.getRestriction() != null) {
                        query.where(query.getRestriction(), cb.equal(root.get("indEstado"), Ubicacion.Estados.PUBLICADO_ACTIVO.getValue()), cb.or(cb.in(root.get("idUbicacion")).value(subquery).not()));
                    } else {
                        query.where(cb.equal(root.get("indEstado"), Ubicacion.Estados.PUBLICADO_ACTIVO.getValue()), cb.or(cb.in(root.get("idUbicacion")).value(subquery).not()));
                    }
                    break;
                }
                default: {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "indAccion");
                }
            }
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

}
