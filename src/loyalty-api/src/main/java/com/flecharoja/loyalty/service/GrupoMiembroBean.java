package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Grupo;
import com.flecharoja.loyalty.model.GrupoMiembro;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.validation.ConstraintViolationException;

/**
 * * EJB encargado de ejecutar reglas de negocio y acciones de persistencia
 * relacionados a grupos-miembros
 *
 * @author wtencio
 */
@Stateless
public class GrupoMiembroBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Método que asigna una lista de miembros a un grupo especifico
     *
     * @param idGrupo identificador único del grupo
     * @param idsMiembro lista de id's de miembro
     * @param usuarioContext usuario que esta en sesión
     * @param locale
     */
    public void insertMiembroGrupoPorLista(String idGrupo, List<String> idsMiembro, String usuarioContext, Locale locale) {
        idsMiembro.forEach((idMiembro) -> {
            GrupoMiembro gruposMiembro = new GrupoMiembro(idMiembro, idGrupo);
            try {
                gruposMiembro.setMiembro((Miembro) em.createNamedQuery("Miembro.findByIdMiembro").setParameter("idMiembro", idMiembro).getSingleResult());
                gruposMiembro.setGrupo((Grupo) em.createNamedQuery("Grupo.findByIdGrupo").setParameter("idGrupo", idGrupo).getSingleResult());
                gruposMiembro.setFechaCreacion(Calendar.getInstance().getTime());
                gruposMiembro.setUsuarioCreacion(usuarioContext);

                em.persist(gruposMiembro);
                em.flush();
                bitacoraBean.logAccion(GrupoMiembro.class, idGrupo + "/" + idMiembro, usuarioContext, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            } catch (EntityNotFoundException e) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_or_group_not_found");
            } catch (ConstraintViolationException e) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }
        });

    }

    /**
     * Método que asigna un miembro a un grupo especifico
     *
     * @param idGrupo identificador único del grupo
     * @param idMiembro identificador unico del miembro que se desea asignar al
     * grupo
     * @param usuarioContext usuario que esta en sesión
     * @param locale
     */
    public void insertMiembroGrupo(String idGrupo, String idMiembro, String usuarioContext, Locale locale) {
        GrupoMiembro gruposMiembro = new GrupoMiembro(idMiembro, idGrupo);
        try {
            gruposMiembro.setMiembro((Miembro) em.createNamedQuery("Miembro.findByIdMiembro").setParameter("idMiembro", idMiembro).getSingleResult());
            gruposMiembro.setGrupo((Grupo) em.createNamedQuery("Grupo.findByIdGrupo").setParameter("idGrupo", idGrupo).getSingleResult());
            gruposMiembro.setFechaCreacion(Calendar.getInstance().getTime());
            gruposMiembro.setUsuarioCreacion(usuarioContext);

            em.persist(gruposMiembro);
            em.flush();

            bitacoraBean.logAccion(GrupoMiembro.class, idGrupo + "/" + idMiembro, usuarioContext, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_or_group_not_found");
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que remueve un miembro de un grupo especifico
     *
     * @param idGrupo identificador único del grupo
     * @param idMiembro identificador único del miembro
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeMiembroGrupo(String idGrupo, String idMiembro, String usuario, Locale locale) {
        GrupoMiembro grupoMiembro = (GrupoMiembro) em.createNamedQuery("GrupoMiembro.findByIdGrupoIdMiembro").setParameter("idGrupo", idGrupo).setParameter("idMiembro", idMiembro).getSingleResult();
        if (grupoMiembro == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_group_not_found");
        }
        em.remove(grupoMiembro);
        bitacoraBean.logAccion(GrupoMiembro.class, idGrupo + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);

    }

    /**
     * Método que remueve una lista de miembros de un grupo especifico
     *
     * @param idGrupo identificador único del grupo
     * @param idsMiembro lista de id de los miembros a eliminar
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeMiembrosGrupoPorLista(String idGrupo, List<String> idsMiembro, String usuario, Locale locale) {
        idsMiembro.forEach((idMiembro) -> {
            GrupoMiembro grupoMiembro = (GrupoMiembro) em.createNamedQuery("GrupoMiembro.findByIdGrupoIdMiembro").setParameter("idGrupo", idGrupo).setParameter("idMiembro", idMiembro).getSingleResult();
            if (grupoMiembro == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_group_not_found");
            }

            em.remove(grupoMiembro);
            bitacoraBean.logAccion(GrupoMiembro.class, idGrupo + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);

        });

    }

    /**
     * Método que devuelve una lista de miembros que están asociados a un grupo
     * especifico
     *
     * @param idGrupo identificador unico del grupo
     * @param tipo
     * @param estados
     * @param generos
     * @param busqueda
     * @param estadosCivil
     * @param educacion
     * @param filtros
     * @param contactoNotificacion
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param contactoEstado
     * @param contactoSMS
     * @param contactoEmail
     * @param ordenTipo
     * @param ordenCampo
     * @param locale
     * @param tieneHijos
     * @return Lista de miembros del grupo
     */
    public Map<String, Object> getMiembrosGrupo(String idGrupo, String tipo, List<String> estados, List<String> generos, List<String> estadosCivil, List<String> educacion, String contactoEmail, String contactoSMS, String contactoNotificacion, String contactoEstado, String tieneHijos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Miembro> root = query.from(Miembro.class);

        query = Busquedas.getCriteriaQueryMiembros(cb, query, root, estados, generos, estadosCivil, educacion, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, ordenTipo, ordenCampo);
        Subquery<String> subquery = query.subquery(String.class);
        Root<GrupoMiembro> rootSubquery = subquery.from(GrupoMiembro.class);
        subquery.select(rootSubquery.get("grupoMiembroPK").get("idMiembro"));
        if (tipo == null || tipo.toUpperCase().charAt(0) == Indicadores.ASIGNADO) {
            subquery.where(cb.equal(rootSubquery.get("grupoMiembroPK").get("idGrupo"), idGrupo));
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            }
        } else {
            if (tipo.toUpperCase().charAt(0) == Indicadores.DISPONIBLES) {
                subquery.where(cb.equal(rootSubquery.get("grupoMiembroPK").get("idGrupo"), idGrupo));
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.equal(root.get("indEstadoMiembro"), Miembro.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                } else {
                    query.where(cb.equal(root.get("indEstadoMiembro"), Miembro.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                }
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "tipo");
            }
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;

    }

    /**
     * Metodo que obtiene los grupos al que pertenece un miembro especifico
     *
     * @param idMiembro Identificador de un miembro valido
     * @param tipo
     * @param indVisibilidad
     * @param cantidad de registros que se quieren por pagina
     * @param busqueda
     * @param registro del cual se inicia la busqueda
     * @param ordenCampo
     * @param ordenTipo
     * @param filtros
     * @param locale
     * @return Lista de grupos asociados a un miembro
     */
    public Map<String, Object> getGruposMiembro(String idMiembro, String tipo, List<String> indVisibilidad, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Grupo> root = query.from(Grupo.class);

        query = Busquedas.getCriteriaQueryGrupos(cb, query, root, indVisibilidad, busqueda, filtros, ordenTipo, ordenCampo);
        Subquery<String> subquery = query.subquery(String.class);
        Root<GrupoMiembro> rootSubquery = subquery.from(GrupoMiembro.class);
        subquery.select(rootSubquery.get("grupoMiembroPK").get("idGrupo"));
        if (tipo == null || tipo.toUpperCase().charAt(0) == Indicadores.ASIGNADO) {
            subquery.where(cb.equal(rootSubquery.get("grupoMiembroPK").get("idMiembro"), idMiembro));
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idGrupo")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idGrupo")).value(subquery)));
            }
        } else {
            if (tipo.toUpperCase().charAt(0) == Indicadores.DISPONIBLES) {
                subquery.where(cb.equal(rootSubquery.get("grupoMiembroPK").get("idMiembro"), idMiembro));
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(cb.in(root.get("idGrupo")).value(subquery).not()));
                } else {
                    query.where(cb.or(cb.in(root.get("idGrupo")).value(subquery).not()));
                }
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "tipo");
            }
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * Método que devuelve la cantidad de miembros que posee un grupo especifico
     *
     * @param idGrupo identificador único del grupo del cual se desea saber la
     * cantidad de miembros que tiene
     * @return cantidad de miembros en el grupo
     */
    public Long cantidadMiembrosGrupo(String idGrupo) {
        return (Long) em.createNamedQuery("GrupoMiembro.countByGrupo").setParameter("idGrupo", idGrupo).getSingleResult();
    }

}
