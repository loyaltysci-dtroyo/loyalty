/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "REGION_UBICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegionUbicacion.findAll", query = "SELECT r FROM RegionUbicacion r"),
    @NamedQuery(name = "RegionUbicacion.findByIdRegionByIdUbicacion", query = "SELECT r FROM RegionUbicacion r WHERE r.regionUbicacionPK.idRegion = :idRegion AND r.regionUbicacionPK.idUbicacion = :idUbicacion"),
    @NamedQuery(name = "RegionUbicacion.findByIdRegion", query = "SELECT r.ubicacion FROM RegionUbicacion r WHERE r.regionUbicacionPK.idRegion = :idRegion"),
    @NamedQuery(name = "RegionUbicacion.findUbicacionesNoRegion", query = "SELECT u FROM Ubicacion u WHERE  u.idUbicacion NOT IN (SELECT r.regionUbicacionPK.idUbicacion FROM RegionUbicacion r WHERE r.regionUbicacionPK.idRegion = :idRegion)"),
    @NamedQuery(name = "RegionUbicacion.findByFechaCreacion", query = "SELECT r FROM RegionUbicacion r WHERE r.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "RegionUbicacion.findByUsuarioCreacion", query = "SELECT r FROM RegionUbicacion r WHERE r.usuarioCreacion = :usuarioCreacion")})
public class RegionUbicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RegionUbicacionPK regionUbicacionPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_REGION", referencedColumnName = "ID_REGION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Region region;
    
    @JoinColumn(name = "ID_UBICACION", referencedColumnName = "ID_UBICACION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ubicacion ubicacion;

    public RegionUbicacion() {
    }

    public RegionUbicacion(RegionUbicacionPK regionUbicacionPK) {
        this.regionUbicacionPK = regionUbicacionPK;
    }

    public RegionUbicacion(RegionUbicacionPK regionUbicacionPK, Date fechaCreacion, String usuarioCreacion) {
        this.regionUbicacionPK = regionUbicacionPK;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public RegionUbicacion(String idRegion, String idUbicacion) {
        this.regionUbicacionPK = new RegionUbicacionPK(idRegion, idUbicacion);
    }

    @ApiModelProperty(hidden = true)
    public RegionUbicacionPK getRegionUbicacionPK() {
        return regionUbicacionPK;
    }

    public void setRegionUbicacionPK(RegionUbicacionPK regionUbicacionPK) {
        this.regionUbicacionPK = regionUbicacionPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (regionUbicacionPK != null ? regionUbicacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegionUbicacion)) {
            return false;
        }
        RegionUbicacion other = (RegionUbicacion) object;
        if ((this.regionUbicacionPK == null && other.regionUbicacionPK != null) || (this.regionUbicacionPK != null && !this.regionUbicacionPK.equals(other.regionUbicacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.RegionUbicacion[ regionUbicacionPK=" + regionUbicacionPK + " ]";
    }
    
}
