package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.service.NotificacionListaSegmentoBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase con recursos y metodos http para el manejo de la asignacion de
 * promociones con envios de notificaciones.
 *
 * @author svargas
 */
@Api(value = "Notificacion")
@Path("notificacion/{idNotificacion}/segmento")
public class NotificacionListaSegmentoResource {

    @EJB
    NotificacionListaSegmentoBean bean;

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario que esta en sesión
    
    @Context
    HttpServletRequest request;

    /**
     * Identificador de notificacion
     */
    @PathParam("idNotificacion")
    String idNotificacion;

    /**
     * Metodo que obtiene un listado de segmentos asignados o no (segun el
     * parametro de tipo) a una notificacion por su identificador, esta lista
     * viene en un rango valido segun los parametros de registro y cantidad
     * junto con encabezados sobre el rango actual y cantidad maxima permitida
     * por peticion, de ocurrir un error se retornara una Respuesta con un
     * estado indicando error junto con un encabezado de un codigo de error
     * (Error-Reason) referente a la naturaleza del error.
     *
     * @param tipo Indicador del tipo de listado (Disponibles no asignados a la
     * notificacion = D, Asignados a la notificacion = A), de no estar presente
     * se asume los asignados
     * @param estados Parametro opcional que contiene los estados de los
     * segmentos deseadas
     * @param tipos Parametros de indicadores de tipo de segmento (indEstatico)
     * @param busqueda Terminos de busqueda
     * @param filtros Parametros de campos de segmentos sobre donde aplicar los
     * filtros de busqueda
     * @param ordenTipo
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param ordenCampo
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @return Respuesta con el listado re registros en un rango valido junto
     * con encabezados sobre rangos
     */
    @ApiOperation(value = "Obtener lista de segmentos de notificacion",
            responseContainer = "List",
            response = Segmento.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idNotificacion", value = "Identificador de notificacion", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSegmentosPorNotificacionId(
            @ApiParam(value = "Indicador del tipo de lista de miembros") @QueryParam("tipoLista") String tipo,
            @ApiParam(value = "Indicadores de estado") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de tipo") @QueryParam("tipo") List<String> tipos,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = bean.getSegmentos(idNotificacion, tipo, estados, tipos, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }

    /**
     * Metodo que realiza la asignacion de un segmento a una notificacion por
     * sus identificadores, de ocurrir un error se retornara una Respuesta con
     * un estado indicando error junto con un encabezado de un codigo de error
     * (Error-Reason) referente a la naturaleza del error.
     *
     * @param idSegmento Identificador del segmento
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asignacion de segmento a notificacion")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idNotificacion", value = "Identificador de notificacion", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idSegmento}")
    public void assignSegmentoNotificacion(
            @ApiParam(value = "Identificador de segmento", required = true)
            @PathParam("idSegmento") String idSegmento) {
         String usuarioContext;
         String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.assignSegmentoNotificacion(idNotificacion, idSegmento, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que realiza la asignacion de un listado de identificadores de
     * segmentos a una notificacion por sus identificador, de ocurrir un error
     * se retornara una Respuesta con un estado indicando error junto con un
     * encabezado de un codigo de error (Error-Reason) referente a la naturaleza
     * del error.
     *
     * @param segmentos Listado de identificadores de segmentos
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asginacion de lista de segmentos a notificacion")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idNotificacion", value = "Identificador de notificacion", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void assignBatchSegmentosNotificacion(
            @ApiParam(value = "Identificadores de segmentos", required = true) List<String> segmentos) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.assignBatchSegmentosNotificacion(idNotificacion, segmentos, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que elimina la asignacion de un segmento a una notificacion por
     * sus identificadores, de ocurrir un error se retornara una Respuesta con
     * un estado indicando error junto con un encabezado de un codigo de error
     * (Error-Reason) referente a la naturaleza del error.
     *
     * @param idSegmento Identificador del segmento
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asignacion de segmento a notificacion")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idNotificacion", value = "Identificador de notificacion", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idSegmento}")
    public void unassignSegmentoNotificacion(
            @ApiParam(value = "Identificador de segmento", required = true)
            @PathParam("idSegmento") String idSegmento) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.unassignSegmentoNotificacion(idNotificacion, idSegmento, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que elimina la asignacion de un listado de identificadores de
     * segmentos a una notificacion por sus identificador, de ocurrir un error
     * se retornara una Respuesta con un estado indicando error junto con un
     * encabezado de un codigo de error (Error-Reason) referente a la naturaleza
     * del error.
     *
     * @param segmentos Listado de identificadores de segmentos
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asginacion de lista de segmentos a notificacion")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idNotificacion", value = "Identificador de notificacion", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("{remover-lista}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void unassignBatchSegmentosNotificacion(
            @ApiParam(value = "Identificadores de segmentos", required = true) List<String> segmentos) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.unassignBatchSegmentosNotificacion(idNotificacion, segmentos, usuarioContext,request.getLocale());
    }
}
