/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Juego;
import com.flecharoja.loyalty.model.MisionJuego;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.util.Indicadores;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class MisionJuegoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;

    /**
     * Método que registra en la base de datos un nuevo juego
     *
     * @param misionJuego
     * @param idJuego
     * @param usuario usuario en sesion
     * @param locale
     */
    public void insertMisionJuego(MisionJuego misionJuego, String usuario, String idJuego, Locale locale) {
        //se verifica que el juego al que pertenece exista
        Juego juego = em.find(Juego.class, idJuego);
        if (juego == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_game_not_found");
        }

        //verifica que si el premio es tipo premio, este no venga vacio y que exista
        if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.PREMIO.getValue()) && misionJuego.getIdPremio() != null) {
            try {
                em.getReference(Premio.class, misionJuego.getIdPremio().getIdPremio());
                misionJuego.setMetrica(null);
                misionJuego.setCantMetrica(null);
            } catch (IllegalArgumentException | EntityNotFoundException e) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
            }
            //o si es de tipo metrica si la cant no viene se pone la que se le configuro a la mision
        } else if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.METRICA.getValue())) {
            Metrica metrica = em.find(Metrica.class, misionJuego.getMetrica().getIdMetrica());
            if (metrica == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "metric_not_found");
            }
            misionJuego.setMetrica(metrica);
            if (misionJuego.getCantMetrica() == null) {
                misionJuego.setCantMetrica(Double.valueOf(0));
            }
            misionJuego.setIdPremio(null);
        } else {
            //si no es de tipo gracias es porque es un identificador de tipo no valido
            if (!misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indTipoPremio");
            }
        }
        //se obtiene la lista de misiones de juegos que ya existe del juego
        List<MisionJuego> misionesJuegos = em.createNamedQuery("MisionJuego.findByIdJuego").setParameter("idMision", idJuego).getResultList();

        //si la lista de juegos de la misma mision no esta vacia 
        if (!misionesJuegos.isEmpty()) {
            if (juego.getTipo().equals(Juego.Tipo.RASPADITA.getValue()) && misionesJuegos.size() == 6) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_game_reward_exceeds");
            }
            if (juego.getTipo().equals(Juego.Tipo.ROMPECABEZAS.getValue()) && misionesJuegos.size() == 2) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_game_reward_exceeds");
            }
            if (juego.getTipo().equals(Juego.Tipo.RULETA.getValue()) && misionesJuegos.size() == 8) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_game_reward_exceeds");
            }
        }

        //se valida que el campo de probabilidad no este vacio si la estrategia es de probabilidad
        if (juego.getEstrategia().equals(Juego.Estrategia.PROBABILIDAD.getValue()) && misionJuego.getProbabilidad() == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "probabilidad");
        }
        
        if(misionJuego.getIndRespuesta() != null && misionJuego.getIndRespuesta()){
            // si los valores son nulos se asumen valores por defecto
            if(misionJuego.getIndInterTotal() != null && MisionJuego.IntervalosTiempoRespuesta.get(misionJuego.getIndInterTotal()) == null || misionJuego.getCantInterTotal() == null){
                misionJuego.setIndInterTotal(MisionJuego.IntervalosTiempoRespuesta.POR_SIEMPRE.getValue());
            }
            if(misionJuego.getIndInterMiembro() != null && (MisionJuego.IntervalosTiempoRespuesta.get(misionJuego.getIndInterMiembro())==null || misionJuego.getCantInterMiembro() == null)){
                misionJuego.setIndInterMiembro(MisionJuego.IntervalosTiempoRespuesta.POR_SIEMPRE.getValue());
            }
        }
        
        misionJuego.setIdJuego(juego);
        misionJuego.setUsuarioCreacion(usuario);
        misionJuego.setFechaCreacion(Calendar.getInstance().getTime());
        misionJuego.setUsuarioModificacion(usuario);
        misionJuego.setFechaModificacion(Calendar.getInstance().getTime());
        misionJuego.setNumVersion(1L);

        try {
            em.persist(misionJuego);
            em.flush();
            bitacoraBean.logAccion(MisionJuego.class, misionJuego.getIdMisionJuego(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que actualiza la informacion de un juego ya registrado
     *
     * @param misionJuego informacion a actualizar
     * @param usuario usuario en sesion
     * @param idJuego
     * @param locale
     */
    public void updateMisionJuego(MisionJuego misionJuego, String usuario, String idJuego, Locale locale) {
        //obtengo mision juego que esta en la base de datos
        MisionJuego original = (MisionJuego) em.createNamedQuery("MisionJuego.findByIdMisionJuego").setParameter("idMisionJuego", misionJuego.getIdMisionJuego()).getSingleResult();
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_game_not_found");
        }

        //verifico que el id del juego sea valido
        if (!original.getIdJuego().getMision().getIdMision().equals(idJuego)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idMision");
        }

        //verifica que si el premio es tipo premio, este no venga vacio y que exista
        if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.PREMIO.getValue()) && misionJuego.getIdPremio() != null) {
            try {
                em.getReference(Premio.class, misionJuego.getIdPremio().getIdPremio());
            } catch (IllegalArgumentException | EntityNotFoundException e) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
            }
            //o si es de tipo metrica la metrica venga y sino pone la que se configuro en la mision
        } else if (misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.METRICA.getValue())) {
            if (misionJuego.getCantMetrica() == null) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "cantMetrica");
            }
        } else {
            //si no es indicador validos del tipo de premio
            if (!misionJuego.getIndTipoPremio().equals(MisionJuego.Tipo.GRACIAS.getValue())) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idPremio");
            }
        }
        
        //limites
        if(misionJuego.getIndRespuesta() != null && misionJuego.getIndRespuesta()){
            if(MisionJuego.IntervalosTiempoRespuesta.get(misionJuego.getIndInterTotal())== null || misionJuego.getCantInterTotal() == null){
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indInterTotal, cantInterTotal");
            }
            if(misionJuego.getIndInterMiembro() != null){
                if(MisionJuego.IntervalosTiempoRespuesta.get(misionJuego.getIndInterMiembro())== null || misionJuego.getCantInterMiembro() == null){
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indInterMiembro, cantInterMiembro");
                }
            }
        }

        misionJuego.setFechaModificacion(Calendar.getInstance().getTime());
        misionJuego.setUsuarioModificacion(usuario);

        try {
            em.merge(misionJuego);
            em.flush();
            bitacoraBean.logAccion(MisionJuego.class, misionJuego.getIdMisionJuego(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
    }

    /**
     * Método que elimina un registro de juego de la base de datos
     *
     * @param idMisionJuego identificador del juego
     * @param idJuego
     * @param usuario usuario en sesion
     * @param locale
     */
    public void deleteJuego(String idMisionJuego, String idJuego, String usuario, Locale locale) {
        MisionJuego misionJuego = em.find(MisionJuego.class, idMisionJuego);

        if (misionJuego == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_game_not_found");
        }
        if (misionJuego.getIdJuego().getMision().getIdMision().equals(idJuego));
        em.remove(misionJuego);
        em.flush();
        bitacoraBean.logAccion(MisionJuego.class, idMisionJuego, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);

    }

    /**
     * Método que obtiene la informacion de un juego relacionado con su
     * identificador
     *
     * @param idMisionJuego
     * @param idJuego identificador del juego
     * @param locale
     * @return informacion del juego
     */
    public MisionJuego getMisionJuegoPorId(String idMisionJuego, String idJuego, Locale locale) {
        MisionJuego misionJuego = em.find(MisionJuego.class, idMisionJuego);
        if (misionJuego == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_game_not_found");
        }

        if (!misionJuego.getIdJuego().getMision().getIdMision().equals(idJuego)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idMision");
        }

        return misionJuego;
    }

    /**
     * Método que permite obtener una lista de juegos definidos por el tipo de
     * juego, en su defecto todos
     *
     * @param idMision
     * @param cantidad
     * @param registro
     * @param locale
     * @return lista de juegos
     */
    public Map<String, Object> getMisionJuegos(String idMision, int cantidad, int registro, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        try {
            total = ((Long) em.createNamedQuery("MisionJuego.countByIdJuego").setParameter("idMision", idMision).getSingleResult());
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createNamedQuery("MisionJuego.findByIdJuego")
                    .setParameter("idMision", idMision)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;

    }

}
