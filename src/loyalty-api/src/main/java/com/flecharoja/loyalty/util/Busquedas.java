package com.flecharoja.loyalty.util;

import com.flecharoja.loyalty.model.AtributoDinamico;
import com.flecharoja.loyalty.model.AtributoDinamicoProducto;
import com.flecharoja.loyalty.model.CategoriaMision;
import com.flecharoja.loyalty.model.CategoriaNoticia;
import com.flecharoja.loyalty.model.CategoriaPremio;
import com.flecharoja.loyalty.model.CategoriaProducto;
import com.flecharoja.loyalty.model.CategoriaPromocion;
import com.flecharoja.loyalty.model.DocumentoInventario;
import com.flecharoja.loyalty.model.Grupo;
import com.flecharoja.loyalty.model.HistoricoMovimientos;
import com.flecharoja.loyalty.model.Insignias;
import com.flecharoja.loyalty.model.JuegoMillonario;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.Noticia;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.model.Preferencia;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioCategoriaPremio;
import com.flecharoja.loyalty.model.Producto;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.model.TrabajosInternos;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.model.Usuario;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 *
 * @author svargas
 */
public class Busquedas {

    public enum CamposOrden {
        FECHA_CREACION("FC"),
        FECHA_MODIFICACION("FM"),
        NOMBRE("N");

        private final String value;
        private static final Map<String, CamposOrden> lookup = new HashMap<>();

        private CamposOrden(String value) {
            this.value = value;
        }

        static {
            for (CamposOrden to : values()) {
                lookup.put(to.value, to);
            }
        }

        public String getValue() {
            return value;
        }

        public static CamposOrden get(String value) {
            return value == null ? null : lookup.get(value.toUpperCase());
        }
    }

    public enum TiposOrden {
        DESCENDIENTE("DESC"),
        ASCENDIENTE("ASC");

        private final String value;
        private static final Map<String, TiposOrden> lookup = new HashMap<>();

        private TiposOrden(String value) {
            this.value = value;
        }

        static {
            for (TiposOrden to : values()) {
                lookup.put(to.value, to);
            }
        }

        public String getValue() {
            return value;
        }

        public static TiposOrden get(String value) {
            return value == null ? null : lookup.get(value.toUpperCase());
        }
    }

    public static CriteriaQuery<Object> getCriteriaQueryMiembros(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Miembro> root, List<String> estados, List<String> generos, List<String> estadosCivil, List<String> educacion, String contactoEmail, String contactoSMS, String contactoNotificacion, String contactoEstado, String tieneHijos, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> Miembro.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstadoMiembro"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        if (generos != null && !generos.isEmpty()) {
            predicates.add(generos.stream().distinct().filter((t) -> Miembro.ValoresIndGenero.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indGenero"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        if (estadosCivil != null && !estadosCivil.isEmpty()) {
            predicates.add(estadosCivil.stream().distinct().filter((t) -> Miembro.ValoresIndEstadoCivil.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstadoCivil"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        if (educacion != null && !educacion.isEmpty()) {
            predicates.add(educacion.stream().distinct().filter((t) -> Miembro.ValoresIndEducacion.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEducacion"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        List<Predicate> predicatesBoolean = new ArrayList<>();
        if (contactoEmail != null) {
            switch (contactoEmail.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.isTrue(root.get("indContactoEmail")));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.isFalse(root.get("indContactoEmail")));
                    predicatesBoolean.add(cb.isNull(root.get("indContactoEmail")));
                    break;
                }
            }
        }
        if (contactoEstado != null) {
            switch (contactoEstado.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.isTrue(root.get("indContactoEstado")));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.isFalse(root.get("indContactoEstado")));
                    predicatesBoolean.add(cb.isNull(root.get("indContactoEstado")));
                    break;
                }
            }
        }
        if (contactoNotificacion != null) {
            switch (contactoNotificacion.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.isTrue(root.get("indContactoNotificacion")));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.isFalse(root.get("indContactoNotificacion")));
                    predicatesBoolean.add(cb.isNull(root.get("indContactoNotificacion")));
                    break;
                }
            }
        }
        if (contactoSMS != null) {
            switch (contactoSMS.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.isTrue(root.get("indContactoSms")));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.isFalse(root.get("indContactoSms")));
                    predicatesBoolean.add(cb.isNull(root.get("indContactoSms")));
                    break;
                }
            }
        }
        if (tieneHijos != null) {
            switch (tieneHijos.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.isTrue(root.get("indHijos")));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.isFalse(root.get("indHijos")));
                    predicatesBoolean.add(cb.isNull(root.get("indHijos")));
                    break;
                }
            }
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("docIdentificacion")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("direccion")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("ciudadResidencia")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("paisResidencia")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("codigoPostal")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("comentarios")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("apellido")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("apellido2")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("correo")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "doc-identificacion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("docIdentificacion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "direccion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("direccion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "ciudad-residencia": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("ciudadResidencia")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "pais-residencia": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("paisResidencia")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "codigo-postal": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("codigoPostal")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "comentarios": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("comentarios")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "primer-apellido": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("apellido")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "segundo-apellido": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("apellido2")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "email": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("correo")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }
        if (!predicatesBoolean.isEmpty()) {
            //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.and(predicatesBoolean.toArray(new Predicate[predicatesBoolean.size()])));
            } else {
                query.where(cb.and(predicatesBoolean.toArray(new Predicate[predicatesBoolean.size()])));
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryMisiones(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Mision> root, List<String> estados, List<String> tipos, List<String> aprobaciones, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> Mision.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definieron valores de indicadores de tipos de mision...
        if (tipos != null && !tipos.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de tipos de mision validos y definidos
            predicates.add(tipos.stream().distinct().filter((t) -> Mision.Tipos.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indTipoMision"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definieron valores de indicadores de tipos de aprobacion de mision...
        if (aprobaciones != null && !aprobaciones.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de tipos de aprobacion validos y definidos
            predicates.add(aprobaciones.stream().distinct().filter((t) -> Mision.Aprobaciones.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indAprovacion"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("tags")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("encabezadoArte")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("subEncabezadoArte")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("textoArte")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "descripcion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "tags": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("tags")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "encabezado-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("encabezadoArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "subencabezado-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("subEncabezadoArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "texto-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("textoArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }
    
    public static CriteriaQuery<Object> getCriteriaQueryJuegoMillonario(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<JuegoMillonario> root, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();


        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "descripcion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        predicates.stream().filter((predicate) -> (!predicate.isEmpty())).forEachOrdered((predicate) -> {
            //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
            } else {
                query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
            }
        }); //si que la lista de restricciones no este vacia...

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }
        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryNotificaciones(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Notificacion> root, List<String> estados, List<String> tipos, List<String> eventos, List<String> objetivos, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> Notificacion.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definieron valores de indicadores de tipos de mision...
        if (tipos != null && !tipos.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de tipos de mision validos y definidos
            predicates.add(tipos.stream().distinct().filter((t) -> Notificacion.Tipos.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indTipo"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definieron valores de indicadores de tipos de aprobacion de mision...
        if (eventos != null && !eventos.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de tipos de aprobacion validos y definidos
            predicates.add(eventos.stream().distinct().filter((t) -> Notificacion.Eventos.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEvento"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definieron valores de indicadores de tipos de aprobacion de mision...
        if (objetivos != null && !objetivos.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de tipos de aprobacion validos y definidos
            predicates.add(objetivos.stream().distinct().filter((t) -> Notificacion.Objectivos.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indObjetivo"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("texto")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("encabezadoArte")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "nombre-interno": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "texto": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("texto")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "encabezado-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("encabezadoArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryPromociones(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Promocion> root, List<String> estados, List<String> acciones, List<String> calendarizaciones, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> Promocion.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definieron valores de indicadores de tipos de acciones...
        if (acciones != null && !acciones.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de tipos de acciones validos y definidos
            predicates.add(acciones.stream().distinct().filter((t) -> Promocion.IndicadoresTiposAcciones.get(t.toUpperCase().charAt(0)) != null).map((t) -> cb.equal(root.get("indTipoAccion"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definieron valores de indicadores de tipos de calendarizacion de promocion...
        if (calendarizaciones != null && !calendarizaciones.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de tipos de calendarizaciones validos y definidos
            predicates.add(calendarizaciones.stream().distinct().filter((t) -> Promocion.IndicadoresCalendarizacion.get(t.toUpperCase().charAt(0)) != null).map((t) -> cb.equal(root.get("indCalendarizacion"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("tags")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("encabezadoArte")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("subencabezadoArte")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("detalleArte")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "nombre-interno": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "descripcion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "tags": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("tags")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "encabezado-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("encabezadoArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "subencabezado-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("subencabezadoArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "detalle-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("detalleArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQuerySegmentos(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Segmento> root, List<String> estados, List<String> tipos, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> Segmento.Estados.get(t) != null).map((t) -> cb.equal(root.get("indEstado"), t)).collect(Collectors.toList()));
        }

        //si se definieron valores de indicadores de estatico de segmento...
        if (tipos != null && !tipos.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de tipos de mision validos y definidos
            predicates.add(tipos.stream().distinct().filter((t) -> Segmento.Tipos.get(t.toUpperCase().charAt(0)) != null).map((t) -> cb.equal(root.get("indEstatico"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("nombreDespliegue")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("tags")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "nombre-despliegue": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombreDespliegue")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "descripcion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "tags": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("tags")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryInsignias(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Insignias> root, List<String> estados, List<String> tipos, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre insignias
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> Insignias.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("estado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definieron valores de indicadores de tipos de insignia...
        if (tipos != null && !tipos.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de tipos de insignia validos y definidos
            predicates.add(tipos.stream().distinct().filter((t) -> Insignias.Tipos.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("tipo"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombreInsignia")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombreInsignia")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "nombre-interno": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "descripcion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombreInsignia"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombreInsignia"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryMetrica(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Metrica> root, List<String> estados, String indExpiracion, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> Metrica.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        List<Predicate> predicatesBoolean = new ArrayList<>();
        if (indExpiracion != null) {
            switch (indExpiracion.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.isTrue(root.get("indExpiracion")));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.isFalse(root.get("indExpiracion")));
                    predicatesBoolean.add(cb.isNull(root.get("indExpiracion")));
                    break;
                }
            }
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("medida")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "medida": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("medida")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "nombre-interno": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }
        if (!predicatesBoolean.isEmpty()) {
            //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.and(predicatesBoolean.toArray(new Predicate[predicatesBoolean.size()])));
            } else {
                query.where(cb.and(predicatesBoolean.toArray(new Predicate[predicatesBoolean.size()])));
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryGrupos(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Grupo> root, List<String> indVisibilidad, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (indVisibilidad != null && !indVisibilidad.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(indVisibilidad.stream().distinct().filter((t) -> Grupo.Visibilidad.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indVisible"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "descripcion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryDocumentos(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<DocumentoInventario> root, String idUbicacion, List<String> tipos, String indCerrado, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<Predicate> predicates = new ArrayList<>();// para tipos

        //si se definieron valores de indicadores de estado...
        if (tipos != null && !tipos.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates = tipos.stream().distinct().filter((t) -> DocumentoInventario.Tipos.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("tipo"), t.toUpperCase().charAt(0))).collect(Collectors.toList());
        }

        Predicate predicateUbicacion = null;
        Predicate predicateIndCerrado = null;

        //si se definieron valores de indicadores de estado...
        if (idUbicacion != null && !idUbicacion.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicateUbicacion = cb.equal(root.get("ubicacionOrigen").get("idUbicacion"), idUbicacion);
        }

        if (indCerrado != null) {
            switch (indCerrado.toUpperCase()) {
                case "TRUE": {
                    predicateIndCerrado = cb.equal(root.get("indCerrado"), true);
                    break;
                }
                case "FALSE": {
                    predicateIndCerrado = cb.equal(root.get("indCerrado"), false);
                    break;
                }
            }
        }

        if (!predicates.isEmpty()) {
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(predicates.toArray(new Predicate[predicates.size()])));
            } else {
                query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
            }
        }

        if (predicateUbicacion != null) {
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), predicateUbicacion);
            } else {
                query.where(predicateUbicacion);
            }
        }
        if (predicateIndCerrado != null) {
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), predicateIndCerrado);
            } else {
                query.where(predicateIndCerrado);
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }

        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryPremios(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Premio> root, List<String> estados, List<String> tipos, List<String> calendarizaciones, String indRespuesta, String indEnvio, String categoria, String indAccionCategoria, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> Premio.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definieron valores de indicadores de tipos de mision...
        if (tipos != null && !tipos.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de tipos de mision validos y definidos
            predicates.add(tipos.stream().distinct().filter((t) -> Premio.Tipo.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indTipoPremio"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        if (calendarizaciones != null && !calendarizaciones.isEmpty()) {
            predicates.add(calendarizaciones.stream().distinct().filter((t) -> Ubicacion.Efectividad.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indCalendarizacion"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        List<Predicate> predicatesBoolean = new ArrayList<>();
        if (indRespuesta != null) {
            switch (indRespuesta.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.equal(root.get("limiteIndRespuesta"), true));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.equal(root.get("limiteIndRespuesta"), false));
                    break;
                }
            }
        }
        if (indEnvio != null) {
            switch (indEnvio.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.equal(root.get("indEnvio"), true));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.equal(root.get("indEnvio"), false));
                    break;
                }
            }
        }

//        Predicate predicateCategoria = null;
        if (categoria != null) {
            Subquery<String> subquery = query.subquery(String.class);
            Root<PremioCategoriaPremio> rootSubquery = subquery.from(PremioCategoriaPremio.class);
            subquery.select(rootSubquery.get("premioCategoriaPremioPK").get("idPremio"));
            if (indAccionCategoria == null || indAccionCategoria.toUpperCase().charAt(0) == Indicadores.ASIGNADO) {
                subquery.where(cb.equal(rootSubquery.get("premioCategoriaPremioPK").get("idCategoria"), categoria));
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(cb.in(root.get("idPremio")).value(subquery)));
                } else {
                    query.where(cb.or(cb.in(root.get("idPremio")).value(subquery)));
                }
            } else {
                if (indAccionCategoria.toUpperCase().charAt(0) == Indicadores.DISPONIBLES) {
                    subquery.where(cb.equal(rootSubquery.get("premioCategoriaPremioPK").get("idCategoria"), categoria));
                    if (query.getRestriction() != null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idPremio")).value(subquery).not()));
                    } else {
                        query.where(cb.or(cb.in(root.get("idPremio")).value(subquery).not()));
                    }
                }
            }

        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("encabezadoArte")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("subencabezadoArte")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("detalleArte")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("codPremio")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "codPremio":{
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("codPremio")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "descripcion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "encabezado-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("encabezadoArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "subencabezado-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("subEncabezadoArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "detalle-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("detalleArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        if (!predicatesBoolean.isEmpty()) {
            //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.and(predicatesBoolean.toArray(new Predicate[predicatesBoolean.size()])));
            } else {
                query.where(cb.and(predicatesBoolean.toArray(new Predicate[predicatesBoolean.size()])));
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryUbicaciones(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Ubicacion> root, List<String> estados, List<String> calendarizaciones, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> Ubicacion.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        if (calendarizaciones != null && !calendarizaciones.isEmpty()) {
            predicates.add(calendarizaciones.stream().distinct().filter((t) -> Ubicacion.Efectividad.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indCalendarizacion"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("indDirPais")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("indDirEstado")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("indDirCiudad")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("direccion")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("horarioAtencion")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("telefono")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("codigoInterno")), "%"+ termino.toUpperCase()+ "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "pais": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("indDirPais")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "estado": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("indDirEstado")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "ciudad": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("indDirCiudad")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "direccion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("direccion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "horario": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("horarioAtencion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "telefono": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("telefono")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "codigoInterno": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("codigoInterno")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryProductos(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Producto> root, List<String> estados, List<String> calendarizaciones, List<String> indEntrega, String indRespuesta, String indImpuesto, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> Producto.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        if (calendarizaciones != null && !calendarizaciones.isEmpty()) {
            predicates.add(calendarizaciones.stream().distinct().filter((t) -> Producto.Efectividad.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("efectividadIndCalendarizado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        if (indEntrega != null && !indEntrega.isEmpty()) {
            predicates.add(indEntrega.stream().distinct().filter((t) -> Producto.Entrega.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEntrega"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        List<Predicate> predicatesBoolean = new ArrayList<>();
        if (indImpuesto != null) {
            switch (indImpuesto.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.equal(root.get("indImpuesto"), true));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.equal(root.get("indImpuesto"), false));
                    break;
                }
            }
        }
        if (indRespuesta != null) {
            switch (indRespuesta.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.equal(root.get("limiteIndRespuesta"), true));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.equal(root.get("limiteIndRespuesta"), false));
                    break;
                }
            }
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("sku")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("tags")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("encabezadoArte")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("subencabezadoArte")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("detalleArte")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "descripcion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "sku": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("sku")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "tags": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("tags")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "encabezado-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("encabezadoArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "subencabezado-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("subencabezadoArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "detalle-arte": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("detalleArte")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }
        if (!predicatesBoolean.isEmpty()) {
            //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.and(predicatesBoolean.toArray(new Predicate[predicatesBoolean.size()])));
            } else {
                query.where(cb.and(predicatesBoolean.toArray(new Predicate[predicatesBoolean.size()])));
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryAtributoDinamicoProducto(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<AtributoDinamicoProducto> root, List<String> estados, List<String> tipo, String indRequerido, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> AtributoDinamicoProducto.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        if (tipo != null && !tipo.isEmpty()) {
            predicates.add(tipo.stream().distinct().filter((t) -> AtributoDinamicoProducto.Tipos.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indTipo"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        List<Predicate> predicatesBoolean = new ArrayList<>();
        if (indRequerido != null) {
            switch (indRequerido.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.equal(root.get("indRequerido"), true));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.equal(root.get("indRequerido"), false));
                    break;
                }
            }
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("valorDefecto")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "descripcion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "valor-defecto": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("valorDefecto")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }
        if (!predicatesBoolean.isEmpty()) {
            //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.and(predicatesBoolean.toArray(new Predicate[predicatesBoolean.size()])));
            } else {
                query.where(cb.and(predicatesBoolean.toArray(new Predicate[predicatesBoolean.size()])));
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryAtributoDinamico(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<AtributoDinamico> root, List<String> estados, List<String> tipoDato, String indVisible, String indRequerido, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> AtributoDinamico.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        if (tipoDato != null && !tipoDato.isEmpty()) {
            predicates.add(tipoDato.stream().distinct().filter((t) -> AtributoDinamico.TiposDato.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indTipoDato"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        List<Predicate> predicatesBoolean = new ArrayList<>();
        if (indVisible != null) {
            switch (indVisible.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.equal(root.get("indVisible"), true));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.equal(root.get("indVisible"), false));
                    break;
                }
            }
        }
        if (indRequerido != null) {
            switch (indRequerido.toUpperCase()) {
                case "TRUE": {
                    predicatesBoolean.add(cb.equal(root.get("indRequerido"), true));
                    break;
                }
                case "FALSE": {
                    predicatesBoolean.add(cb.equal(root.get("indRequerido"), false));
                    break;
                }
            }
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("valorDefecto")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "descripcion": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "valor-defecto": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("valorDefecto")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }
        if (!predicatesBoolean.isEmpty()) {
            //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.and(predicatesBoolean.toArray(new Predicate[predicatesBoolean.size()])));
            } else {
                query.where(cb.and(predicatesBoolean.toArray(new Predicate[predicatesBoolean.size()])));
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombre"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombre"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }
    
        public static CriteriaQuery<Object> getCriteriaQueryPreferencia(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Preferencia> root, List<String> tipoRespuesta, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        if (tipoRespuesta != null && !tipoRespuesta.isEmpty()) {
            predicates.add(tipoRespuesta.stream().distinct().filter((t) -> Preferencia.Respuesta.get(t) != null).map((t) -> cb.equal(root.get("indTipoRespuesta"), t.toUpperCase())).collect(Collectors.toList()));
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("pregunta")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("respuestas")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "pregunta": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("pregunta")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "respuestas": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("respuestas")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }
        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryTrabajos(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<TrabajosInternos> root, List<String> tipos, List<String> estados, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<Predicate> predicates = new ArrayList<>();// para tipos
        List<Predicate> predicatesTipos = new ArrayList<>();// para tipos
        //si se definieron valores de indicadores de tipo...
        if (tipos != null && !tipos.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicatesTipos = tipos.stream().distinct().filter((t) -> TrabajosInternos.Tipo.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indTipo"), t.toUpperCase().charAt(0))).collect(Collectors.toList());
        }
        if (!predicatesTipos.isEmpty()) {
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(predicatesTipos.toArray(new Predicate[predicatesTipos.size()])));
            } else {
                query.where(cb.or(predicatesTipos.toArray(new Predicate[predicatesTipos.size()])));
            }
        }
        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates = estados.stream().distinct().filter((t) -> TrabajosInternos.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList());
        }
        if (!predicates.isEmpty()) {
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(predicates.toArray(new Predicate[predicates.size()])));
            } else {
                query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
            }
        }
        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_CREACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }

        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryUsuarios(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Usuario> root, List<String> estados, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> Usuario.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }
        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("nombrePublico")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("correo")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "nombre": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("nombrePublico")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "email": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("correo")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }
        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
            case NOMBRE: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(cb.upper(root.get("nombrePublico"))));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(cb.upper(root.get("nombrePublico"))));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }

    public static CriteriaQuery<Object> getCriteriaQueryHistoricoMovimientos(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<HistoricoMovimientos> root, List<String> estados, List<String> tipoMovimiento, String premio,String ubicacion, String miembro)  {
        //listado de listas de restricciones a aplicar sobre misiones
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (tipoMovimiento != null && !tipoMovimiento.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(tipoMovimiento.stream().distinct().filter((t) -> HistoricoMovimientos.Tipos.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("tipoMovimiento"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        if (estados != null && !estados.isEmpty()) {
            predicates.add(estados.stream().distinct().filter((t) -> HistoricoMovimientos.Status.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("status"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        Predicate predicateUbicacion = null;
        //si se definieron valores de indicadores de estado...
        if (ubicacion != null && !ubicacion.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicateUbicacion = cb.equal(root.get("ubicacionOrigen").get("idUbicacion"), ubicacion);
        }
        
        Predicate predicatePremio = null;
        //si se definieron valores de indicadores de estado...
        if (premio != null && !premio.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicatePremio = cb.equal(root.get("codPremio").get("idPremio"), premio);
        }
        Predicate predicateMiembro = null;
        //si se definieron valores de indicadores de estado...
        if (miembro != null && !miembro.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicatePremio = cb.equal(root.get("idMiembro").get("correo"), miembro);
        }
        
//        Predicate predicateFecha = null;
//        //si se definieron valores de indicadores de estado...
//        if (fecha != null && !fecha.isEmpty()) {
//            DateFormat formatter = new SimpleDateFormat("dd-MMM-yy");
//            Date date = formatter.parse(fecha);
//            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
//            predicateFecha = cb.(root.<Date>get("fecha"), date);
//        }
        
        if (predicateUbicacion != null) {
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), predicateUbicacion);
            } else {
                query.where(predicateUbicacion);
            }
        }
        if (predicatePremio != null) {
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), predicatePremio);
            } else {
                query.where(predicatePremio);
            }
        }
        if (predicateMiembro != null) {
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), predicateMiembro);
            } else {
                query.where(predicateMiembro);
            }
        }
//        if (predicateFecha != null) {
//            if (query.getRestriction() != null) {
//                query.where(query.getRestriction(), predicateFecha);
//            } else {
//                query.where(predicateFecha);
//            }
//        }
        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }
        
        query.orderBy(cb.desc(root.get("fecha")));
        return query;
    }
    
    public static CriteriaQuery<Object> getCriteriaQueryNoticias(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<Noticia> root, List<String> estados, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre noticias
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definieron valores de indicadores de estado...
        if (estados != null && !estados.isEmpty()) {
            //se agrega una lista de restricciones sobre los valores de estados validos y definidos
            predicates.add(estados.stream().distinct().filter((t) -> Promocion.Estados.get(t.charAt(0)) != null).map((t) -> cb.equal(root.get("indEstado"), t.toUpperCase().charAt(0))).collect(Collectors.toList()));
        }

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //si no se definio ningun filtro...
            if (filtros == null || filtros.isEmpty()) {
                //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
                for (String termino : busqueda.split(" ")) {
                    list.add(cb.like(cb.upper(root.get("titulo")), "%" + termino.toUpperCase() + "%"));
                    list.add(cb.like(cb.upper(root.get("contenido")), "%" + termino.toUpperCase() + "%"));
                }
            } else {
                //si existe algun campo de filtro puesto...
                if (!filtros.isEmpty()) {
                    //por cada campo de filtro definido se compara los terminos de busqueda con el tipo de campo disponible
                    for (String filtro : filtros) {
                        switch (filtro) {
                            case "titulo": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("titulo")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                            case "contenido": {
                                for (String termino : busqueda.split(" ")) {
                                    list.add(cb.like(cb.upper(root.get("contenido")), "%" + termino.toUpperCase() + "%"));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }
    
    public static CriteriaQuery<Object> getCriteriaQueryCategoriaMision(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<CategoriaMision> root, String busqueda, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre noticias
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
            for (String termino : busqueda.split(" ")) {
                list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                list.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + termino.toUpperCase() + "%"));
                list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }
    
    public static CriteriaQuery<Object> getCriteriaQueryCategoriaNoticia(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<CategoriaNoticia> root, String busqueda, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre noticias
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
            for (String termino : busqueda.split(" ")) {
                list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }
    
    public static CriteriaQuery<Object> getCriteriaQueryCategoriaPremio(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<CategoriaPremio> root, String busqueda, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre noticias
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
            for (String termino : busqueda.split(" ")) {
                list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                list.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + termino.toUpperCase() + "%"));
                list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }
    
    public static CriteriaQuery<Object> getCriteriaQueryCategoriaProducto(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<CategoriaProducto> root, String busqueda, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre noticias
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
            for (String termino : busqueda.split(" ")) {
                list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                list.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + termino.toUpperCase() + "%"));
                list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }
    
    public static CriteriaQuery<Object> getCriteriaQueryCategoriaPromocion(CriteriaBuilder cb, CriteriaQuery<Object> query, Root<CategoriaPromocion> root, String busqueda, String ordenTipo, String ordenCampo) {
        //listado de listas de restricciones a aplicar sobre noticias
        List<List<Predicate>> predicates = new ArrayList<>();

        //si se definio los terminos de busqueda....
        if (busqueda != null && !busqueda.trim().isEmpty()) {
            List<Predicate> list = new ArrayList<>();//lista de restricciones para terminos de busqueda

            //por cada termino de busqueda se compara con todos los campos disponibles de busqueda
            for (String termino : busqueda.split(" ")) {
                list.add(cb.like(cb.upper(root.get("nombre")), "%" + termino.toUpperCase() + "%"));
                list.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + termino.toUpperCase() + "%"));
                list.add(cb.like(cb.upper(root.get("descripcion")), "%" + termino.toUpperCase() + "%"));
            }
            predicates.add(list);//se agrega a la lista de listas de restricciones
        }

        //por cada lista de restriccion definida
        for (List<Predicate> predicate : predicates) {
            //si que la lista de restricciones no este vacia...
            if (!predicate.isEmpty()) {
                //si el query ya tiene definido restricciones pasada, se agrega... si no se define por primera vez
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(predicate.toArray(new Predicate[predicate.size()])));
                } else {
                    query.where(cb.or(predicate.toArray(new Predicate[predicate.size()])));
                }
            }
        }

        CamposOrden co = CamposOrden.get(ordenCampo);
        if (co == null) {
            co = CamposOrden.FECHA_MODIFICACION;
        }
        TiposOrden to = TiposOrden.get(ordenTipo);
        if (to == null) {
            to = TiposOrden.DESCENDIENTE;
        }
        switch (co) {
            case FECHA_CREACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaCreacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaCreacion")));
                        break;
                    }
                }
                break;
            }
            case FECHA_MODIFICACION: {
                switch (to) {
                    case DESCENDIENTE: {
                        query.orderBy(cb.desc(root.get("fechaModificacion")));
                        break;
                    }
                    case ASCENDIENTE: {
                        query.orderBy(cb.asc(root.get("fechaModificacion")));
                        break;
                    }
                }
                break;
            }
        }

        return query;
    }
}
