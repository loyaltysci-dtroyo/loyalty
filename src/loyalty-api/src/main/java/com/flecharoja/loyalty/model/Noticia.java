package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NOTICIA")
@XmlRootElement
public class Noticia implements Serializable {
    
    public enum Estados {
        PUBLICADO('A'),
        ARCHIVADO('B'),
        BORRADOR('C'),
        INACTIVO('D');
        
        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public char getValue() {
            return value;
        }
        
        public static Estados get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "noticia_uuid")
    @GenericGenerator(name = "noticia_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_NOTICIA")
    private String idNoticia;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "TITULO")
    private String titulo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "CONTENIDO")
    private String contenido;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    private String imagen;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;
    
    @Column(name = "IND_AUTOR_ADMIN")
    private Boolean indAutorAdmin;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "ID_AUTOR")
    private String idAutor;
    
    @Basic(optional = false)
    @NotNull
    @Version
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "noticia")
    private List<NoticiaComentario> comentarios;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "noticia")
    private List<MarcadorNoticia> marcas;
    
    @ApiModelProperty(hidden = true)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "noticia")
    private List<NoticiaCategoriaNoticia> noticiaCategoriaNoticiaList;
    
    @Transient
    private long cantComentarios;
    
    @Transient
    private long cantMarcas;
    
    @ApiModelProperty(value = "Nombre publico del autor/Nombre de usuario", readOnly = true)
    @Transient
    private String nombreAutor;
    
    @ApiModelProperty(value = "URL del avatar del autor", readOnly = true)
    @Transient
    private String avatarAutor;

    public Noticia() {
    }

    public String getIdNoticia() {
        return idNoticia;
    }

    public void setIdNoticia(String idNoticia) {
        this.idNoticia = idNoticia;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Boolean getIndAutorAdmin() {
        return indAutorAdmin;
    }

    public void setIndAutorAdmin(Boolean indAutorAdmin) {
        this.indAutorAdmin = indAutorAdmin;
    }

    public String getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(String idAutor) {
        this.idAutor = idAutor;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<NoticiaComentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<NoticiaComentario> comentarios) {
        this.comentarios = comentarios;
    }
    
    public long getCantComentarios() {
        return cantComentarios;
    }
    
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MarcadorNoticia> getMarcas() {
        return marcas;
    }
    
    public void setMarcas(List<MarcadorNoticia> marcas) {
        this.marcas = marcas;
    }

    @XmlTransient
    public List<NoticiaCategoriaNoticia> getNoticiaCategoriaNoticiaList() {
        return noticiaCategoriaNoticiaList;
    }

    public void setNoticiaCategoriaNoticiaList(List<NoticiaCategoriaNoticia> noticiaCategoriaNoticiaList) {
        this.noticiaCategoriaNoticiaList = noticiaCategoriaNoticiaList;
    }

    public void setCantComentarios(long cantComentarios) {
        this.cantComentarios = cantComentarios;
    }
    
    public long getCantMarcas() {
        return cantMarcas;
    }

    public void setCantMarcas(long cantMarcas) {
        this.cantMarcas = cantMarcas;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    public String getAvatarAutor() {
        return avatarAutor;
    }

    public void setAvatarAutor(String avatarAutor) {
        this.avatarAutor = avatarAutor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNoticia != null ? idNoticia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Noticia)) {
            return false;
        }
        Noticia other = (Noticia) object;
        if ((this.idNoticia == null && other.idNoticia != null) || (this.idNoticia != null && !this.idNoticia.equals(other.idNoticia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Noticia[ idNoticia=" + idNoticia + " ]";
    }
    
}
