/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.InstanciaMillonario;
import com.flecharoja.loyalty.model.InstanciaPreguntaMillonario;
import com.flecharoja.loyalty.model.Juego;
import com.flecharoja.loyalty.model.JuegoMillonario;
import com.flecharoja.loyalty.model.MisionJuego;
import com.flecharoja.loyalty.model.PreguntaMillonario;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author kevin
 */

@Stateless
public class InstanciaMillonarioBean {
    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    UtilBean utilBean;//EJB con metodos de negocio para el manejo de utilidades

    @EJB
    BitacoraBean bitacoraBean;//EJB con metodos de negocio para el manejo de bitacora
    
    @EJB PreguntaMillonarioBean preguntaMillonarioBean;
    
    @EJB InstanciaPreguntaMillonarioBean instanciaPreguntaMillonarioBean;

    /**
     * Método que registra una nueva insignia en la base de datos
     *
     * @param idJuegoMillonario
     * @param idMision
     * @param usuario usuario en sesión
     * @param locale
     * @return id del usuario en sesión
     */
    public String insertInstanciaMillonario(String idJuegoMillonario, String idMision, String usuario, Locale locale) {
        JuegoMillonario juegoMillonario = em.find(JuegoMillonario.class, idJuegoMillonario);
        if (juegoMillonario == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE,locale, "juego_not_found");
        }
        
        List<PreguntaMillonario> preguntas = preguntaMillonarioBean.getPreguntas(idJuegoMillonario, locale);
        InstanciaMillonario nuevaInstancia = new InstanciaMillonario();
        nuevaInstancia.setDescripcion(juegoMillonario.getDescripcion());
        nuevaInstancia.setIdMision(idMision);
        nuevaInstancia.setNombre(juegoMillonario.getNombre());
        nuevaInstancia.setNumVersion(new Long(1));
        nuevaInstancia.setFechaCreacion(Calendar.getInstance().getTime());
        nuevaInstancia.setFechaModificacion(Calendar.getInstance().getTime());
        nuevaInstancia.setUsuarioModificacion(usuario);
        nuevaInstancia.setUsuarioCreacion(usuario);
        
        try {
            em.persist(nuevaInstancia);
            em.flush();
            bitacoraBean.logAccion(InstanciaMillonario.class, nuevaInstancia.getIdJuego(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            for (int i=0 ; i < preguntas.size(); i++) {
                instanciaPreguntaMillonarioBean.insertInstanciaPreguntaMillonario(
                        nuevaInstancia.getIdJuego(), preguntas.get(i), usuario, locale);
            }
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        
        return nuevaInstancia.getIdJuego();
    }
    
    /**
     * borrado de juego de una mision
     * 
     * @param idJuegoMillonario
     * @param usuario id admin
     * @param locale locale
     */
    public void deleteInstancia(String idJuegoMillonario, String usuario, Locale locale) {
        //busqueda de juego en la mision referente
        InstanciaMillonario juego = em.find(InstanciaMillonario.class, idJuegoMillonario);
        
        if (juego == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_game_not_found");
        }
        
        instanciaPreguntaMillonarioBean.deletePreguntas(idJuegoMillonario, locale);
        em.remove(em.find(InstanciaMillonario.class, idJuegoMillonario));
        em.flush();
        bitacoraBean.logAccion(InstanciaMillonario.class, idJuegoMillonario, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
    }
}
