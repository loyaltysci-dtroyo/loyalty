/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.Proveedor;
import com.flecharoja.loyalty.util.Indicadores;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class ProveedorBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;

    /**
     * Método que registra un nuevo proveedor en la base de datos
     *
     * @param proveedor info del proveedor
     * @param usuario en sesion
     * @param locale
     * @return 
     */
    public String insertProveedor(Proveedor proveedor, String usuario, Locale locale) {
        if (proveedor.getCodigoProveedor() != null) {
            if (existsCodigoProveedor(proveedor.getCodigoProveedor())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "provider_code_already_exists");
            }
        } else {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "codigoProveedor");
        }
        proveedor.setFechaCreacion(Calendar.getInstance().getTime());
        proveedor.setFechaModificacion(Calendar.getInstance().getTime());
        proveedor.setUsuarioCreacion(usuario);
        proveedor.setUsuarioModificacion(usuario);
        proveedor.setNumVersion(1L);

        try {
            em.persist(proveedor);
            bitacoraBean.logAccion(Proveedor.class, proveedor.getIdProveedor(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        return proveedor.getIdProveedor();
    }

    /**
     * Método que actualiza la informacion del proveedor
     *
     * @param proveedor info del proveedor
     * @param usuario en sesion
     * @param locale
     */
    public void updateProveedor(Proveedor proveedor, String usuario, Locale locale) {
        Proveedor original = em.find(Proveedor.class, proveedor.getIdProveedor());
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "provider_not_found");
        }

        if (proveedor.getCodigoProveedor() != null) {
            if (!proveedor.getCodigoProveedor().equals(original.getCodigoProveedor()) && existsCodigoProveedor(proveedor.getCodigoProveedor())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "provider_code_already_exists");
            }
        } else {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "codigoProveedor");
        }

        proveedor.setFechaModificacion(Calendar.getInstance().getTime());
        proveedor.setUsuarioModificacion(usuario);

        try {
            em.merge(proveedor);
            bitacoraBean.logAccion(Proveedor.class, proveedor.getIdProveedor(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
    }

    /**
     * Método que elimina un proveedor registrado
     *
     * @param idProveedor identificador del proveedor
     * @param usuario en sesion
     * @param locale
     */
    public void deleteProveedor(String idProveedor, String usuario, Locale locale) {
        Proveedor proveedor = em.find(Proveedor.class, idProveedor);
        if (proveedor == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "provider_not_found");
        }
        em.remove(proveedor);
        bitacoraBean.logAccion(Proveedor.class, proveedor.getIdProveedor(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
    }

    /**
     * Método que obtiene la info de un proveedor espcifico
     *
     * @param idProveedor
     * @param locale
     * @return
     */
    public Proveedor getProveedor(String idProveedor, Locale locale) {
        Proveedor proveedor = em.find(Proveedor.class, idProveedor);
        if (proveedor == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "provider_not_found");
        }
        return proveedor;
    }

    /**
     * Método que obtiene la lista de proveedores filtrados por cantidades y
     * registros de obtencion de resultados
     *
     * @param busqueda
     * @param cantidad maxima de registros por pagina
     * @param registro por el cual se desea empezar la busqueda
     * @param locale
     * @return lista de proveedores
     */
    public Map<String, Object> getProveedores(String busqueda, int cantidad, int registro, Locale locale) {
        //verifica que la cantidad en la peticion sea menor a la aceptada, de no ser asi se retorna una respuesta informando acerca de ello, junto con el encabezado de rango maximo
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        Long total;//almacena el total de registros
        List resultado;//almacena el resultado

        try {
            if (busqueda == null && busqueda.trim().isEmpty()) {
                total = ((Long) em.createNamedQuery("Proveedor.countAll").setFirstResult(registro).setMaxResults(cantidad).getSingleResult());//se obtiene el total de registros
                total -= total - 1 < 0 ? 0 : 1;//reduce el total en uno (mientras que no sea <0) para que concuerde con el numero de ultimo registro

                //se obtiene el listado usando los parametros de registro y cantidad para limitar el rango de respuesta
                resultado = em.createNamedQuery("Proveedor.findAll")
                        .setFirstResult(registro)
                        .setMaxResults(cantidad)
                        .getResultList();
            } else {
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery<Object> query = cb.createQuery();
                Root<Proveedor> root = query.from(Proveedor.class);

                List<Predicate> predicates = new ArrayList<>();

                for (String termino : busqueda.split(" ")) {
                    predicates.add(cb.like(cb.lower(root.get("descripcion")), "%" + termino.toLowerCase() + "%"));
                    predicates.add(cb.like(cb.lower(root.get("direccion")), "%" + termino.toLowerCase() + "%"));
                    predicates.add(cb.like(cb.lower(root.get("telefono")), "%" + termino.toLowerCase()+ "%"));
                    predicates.add(cb.like(cb.lower(root.get("cedJuridica")), "%" + termino.toLowerCase()+ "%"));
                    predicates.add(cb.like(cb.lower(root.get("codigoProveedor")), "%" + termino.toLowerCase() + "%"));
                }

                query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
                total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
                total -= total - 1 < 0 ? 0 : 1;

                resultado = em.createQuery(query.select(root))
                        .setFirstResult(registro)
                        .setMaxResults(cantidad)
                        .getResultList();
            }
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
        } catch (IllegalArgumentException e) {//posiblemente pueda ocurrir al establecer los parametros de registro y cantidad erroneos (ejm: menores a 0)
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se establece la respuesta encapsulando el listado y estableciendo los encabezados de rango de la respuesta y cantidad maximo aceptado
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    public Map<String, Object> searchProveedores(String busqueda, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        String[] palabrasClaves = busqueda.split(" ");
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<Proveedor> root = query.from(Proveedor.class);

            List<Predicate> predicates = new ArrayList<>();

            for (String palabraClave : palabrasClaves) {
                predicates.add(cb.like(cb.lower(root.get("descripcion")), "%" + palabraClave.toLowerCase() + "%"));
                predicates.add(cb.like(cb.lower(root.get("direccion")), "%" + palabraClave.toLowerCase() + "%"));
                predicates.add(cb.like(cb.lower(root.get("telefono")), "%" + palabraClave.toUpperCase() + "%"));
                predicates.add(cb.like(cb.lower(root.get("cedJuridica")), "%" + palabraClave.toUpperCase() + "%"));
                predicates.add(cb.like(cb.lower(root.get("codProveedor")), "%" + palabraClave.toUpperCase() + "%"));
            }

            query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    public boolean existsCodigoProveedor(String codigoProveedor) {
        return ((Long) em.createNamedQuery("Proveedor.countCodigoProveedor").setParameter("codigoProveedor", codigoProveedor).getSingleResult()) > 0;
    }
}
