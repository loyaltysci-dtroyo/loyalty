/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.service.ProductoAtributoBean;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * clase de recursos http para el manejo de atributos dinamicos de productos
 * @author wtencio
 */
@Api(value = "Atributo Dinamico de productos")
@Path("atributo-producto")
public class ProductoAtributoResource {
    
    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos para el manejo de autorizaciones
    
    @EJB
    ProductoAtributoBean productoAtributoBean;//EJB con los metodos para el manejo de la asociacion de productos y atributos
    
    @Context
    SecurityContext context;//permite obtener informacion del usuario en sesion
    
    @Context
    HttpServletRequest request;
    
    
    
    
    
     /**
     * Metodo que asigna un atributo a dinamico a un producto usando sus
     * identificadores y retorna una respuesta con un estado de OK(200), de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idAtributo Identificador de la categoria de premio
     * @param idProducto Identificador del premio
     * @param valor valor del atributo
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asignacion de un atributo a un producto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idAtributo}/producto/{idProducto}")
    @Consumes(MediaType.TEXT_PLAIN)
    public void assignAtributoProducto(
            @ApiParam(value = "Identificador del atributo dinamico", required = true)
            @PathParam("idAtributo") String idAtributo,
            @ApiParam(value = "Identificador del producto", required = true)
            @PathParam("idProducto") String idProducto,
            @ApiParam(value = "Valor del atributo dinámico", required = true)
            String valor) {
        
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.ATRIBUTOS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoAtributoBean.assingAtributoProducto(idAtributo, idProducto,valor, usuarioContext,request.getLocale());
    }
    
    /**
     * Metodo que remueve la asignacion de un atributo con un producto
     * usando sus identificadores y retorna una respuesta con un
     * estado de OK(200), de ocurrir un error se retornara una respuesta con un
     * diferente estado junto con un encabezado "Error-Reason" con un valor
     * numerico indicando la naturaleza del error.
     *
     * @param idAtributo identificador del atributo
     * @param idProducto identificador del producto
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asignacion de un atributo con un producto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idAtributo}/producto/{idProducto}")
    public void unassingAtributoProducto(
            @ApiParam(value = "Identificador del atributo dinamico", required = true)
            @PathParam("idAtributo") String idAtributo,
            @ApiParam(value = "Identificador del producto", required = true)
            @PathParam("idProducto") String idProducto) {
       
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoAtributoBean.unassingAtributoProducto(idAtributo, idProducto, usuarioContext,request.getLocale());
    }
    
    /**
     * Metodo que actualiza el valor de  un atributo dinamico a un producto usando sus
     * identificadores y retorna una respuesta con un estado de OK(200), de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idAtributo Identificador de la categoria de premio
     * @param idProducto Identificador del premio
     * @param valor valor del atributo
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Actualizacion de un valor de un atributo a un producto")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("{idAtributo}/producto/{idProducto}")
    @Consumes(MediaType.TEXT_PLAIN)
    public void updateAtributoProducto(
            @ApiParam(value = "Identificador del atributo dinamico", required = true)
            @PathParam("idAtributo") String idAtributo,
            @ApiParam(value = "Identificador del producto", required = true)
            @PathParam("idProducto") String idProducto,
            @ApiParam(value = "Valor del atributo dinámico", required = true)
            String valor) {
        
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.ATRIBUTOS_PRODUCTO_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoAtributoBean.updateAtributoProducto(idAtributo, idProducto,valor, usuarioContext,request.getLocale());
    }
    
    
   
}
