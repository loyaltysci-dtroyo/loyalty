package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.EntradaBitacoraApiExternal;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.Stateless;
import javax.json.Json;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author svargas
 */
@Stateless
public class BitacoraApiExternalBean {
    
    //valores de los periodos de tiempo disponibles
    private enum PeriodosTiempo {
        UN_DIA("1D"),
        UNA_SEMANA("1S"),
        DOS_SEMANAS("2S"),
        UN_MES("1M"),
        DOS_MESES("2M"),
        TRES_MESES("3M"),
        SEIS_MESES("6M"),
        NUEVE_MESES("9M"),
        UN_ANO("1Y"),
        DOS_ANOS("2Y"),
        TODOS("ALL");
        
        private final String value;
        private static final Map<String, PeriodosTiempo> lookup = new HashMap<>();

        private PeriodosTiempo(String value) {
            this.value = value;
        }
        
        static {
            for (PeriodosTiempo periodo : values()) {
                lookup.put(periodo.value, periodo);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static PeriodosTiempo get(String value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    private final Configuration configuration;

    public BitacoraApiExternalBean() {
        this.configuration = new Configuration();
        this.configuration.addResource("conf/hbase/hbase-site.xml");
    }
    
    /**
     * obtencion de la fecha final para el periodo elegido en el momento actual
     * TODO actualizar a DateTime
     * 
     * @param indPeriodoTiempo indicador del periodo de tiempo
     * @return fecha generada
     */
    private Date getFechaFinPeriodo(String indPeriodoTiempo) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        
        PeriodosTiempo pt = PeriodosTiempo.get(indPeriodoTiempo);
        if (pt==null) {
            pt = PeriodosTiempo.UN_MES;
        }
        switch (pt) {
            case UN_DIA: {
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                break;
            }
            case UNA_SEMANA: {
                calendar.add(Calendar.WEEK_OF_YEAR, -1);
                break;
            }
            case DOS_SEMANAS: {
                calendar.add(Calendar.WEEK_OF_YEAR, -2);
                break;
            }
            case UN_MES: {
                calendar.add(Calendar.MONTH, -1);
                break;
            }
            case DOS_MESES: {
                calendar.add(Calendar.MONTH, -2);
                break;
            }
            case TRES_MESES: {
                calendar.add(Calendar.MONTH, -3);
                break;
            }
            case SEIS_MESES: {
                calendar.add(Calendar.MONTH, -6);
                break;
            }
            case NUEVE_MESES: {
                calendar.add(Calendar.MONTH, -9);
                break;
            }
            case UN_ANO: {
                calendar.add(Calendar.YEAR, -1);
                break;
            }
            case DOS_ANOS: {
                calendar.add(Calendar.YEAR, -2);
                break;
            }
            case TODOS: {
                return null;
            }
        }
        
        return calendar.getTime();
    }
    
    /**
     * obtencion de registros en la bitacora escrita a hbase en un periodo de tiempo
     * 
     * @param indPeriodoTiempo valor del indicador del periodo de tiempo elegido
     * @param locale locale
     * @return lista de entradas de bitacora registradas
     */
    public List<EntradaBitacoraApiExternal> getBitacoraApiExternal(String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaBitacoraApiExternal> bitacora = new ArrayList<>();
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "LOG_EXCEPTIONS_EXTERNAL"));
            Scan scan = new Scan();
            scan.setReversed(true); //escaneo al reverso (para el orden de fecha mas nueva a mas vieja)
            
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    //comprobacion de fecha de registro para parar el escaneo
                    Date fechaEvento = new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[0]));
                    if (fechaFinPeriodo!=null && fechaEvento.before(fechaFinPeriodo) && !DateUtils.isSameDay(fechaEvento, fechaFinPeriodo)) {
                        break;
                    }
                    
                    //agregado y mapeo a registro de bitacora
                    bitacora.add(new EntradaBitacoraApiExternal(new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[0])),
                            Bytes.toString(result.getRow()).split("&")[1],
                            Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("HTTP_STATUS"))),
                            Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("RESPONSE_BODY"))),
                            result.containsColumn(Bytes.toBytes("DETAILS"), Bytes.toBytes("PETITION_BODY"))?Bytes.toString(result.getValue(Bytes.toBytes("DETAILS"), Bytes.toBytes("PETITION_BODY"))):null
                    ));
                }
            }
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
        
        return bitacora;
    }
}
