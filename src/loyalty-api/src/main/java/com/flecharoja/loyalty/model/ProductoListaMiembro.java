/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PRODUCTO_LISTA_MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoListaMiembro.findAll", query = "SELECT p FROM ProductoListaMiembro p"),
    @NamedQuery(name = "ProductoListaMiembro.findByIdMiembro", query = "SELECT p FROM ProductoListaMiembro p WHERE p.productoListaMiembroPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "ProductoListaMiembro.findByIdProducto", query = "SELECT p FROM ProductoListaMiembro p WHERE p.productoListaMiembroPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoListaMiembro.findByIndTipo", query = "SELECT p FROM ProductoListaMiembro p WHERE p.indTipo = :indTipo"),
    @NamedQuery(name = "ProductoListaMiembro.countByIdProducto", query = "SELECT COUNT(p) FROM ProductoListaMiembro p WHERE p.productoListaMiembroPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoListaMiembro.findMiembrosByIdProducto", query = "SELECT p.miembro FROM ProductoListaMiembro p WHERE p.productoListaMiembroPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoListaMiembro.countMiembrosNotInProductoListaMiembro", query = "SELECT COUNT(m) FROM Miembro m WHERE m.indEstadoMiembro = :estado AND m.idMiembro NOT IN (SELECT p.productoListaMiembroPK.idMiembro FROM ProductoListaMiembro p WHERE p.productoListaMiembroPK.idProducto = :idProducto)"),
    @NamedQuery(name = "ProductoListaMiembro.findMiembrosNotInProductoListaMiembro", query = "SELECT m FROM Miembro m WHERE m.indEstadoMiembro = :estado AND m.idMiembro NOT IN (SELECT p.productoListaMiembroPK.idMiembro FROM ProductoListaMiembro p WHERE p.productoListaMiembroPK.idProducto = :idProducto)"),
    @NamedQuery(name = "ProductoListaMiembro.countByIdProductoIndTipo", query = "SELECT COUNT(p) FROM ProductoListaMiembro p WHERE p.productoListaMiembroPK.idProducto = :idProducto AND p.indTipo = :indTipo"),
    @NamedQuery(name = "ProductoListaMiembro.findMiembrosByIdProductoIndTipo", query = "SELECT p.miembro FROM ProductoListaMiembro p WHERE p.productoListaMiembroPK.idProducto = :idProducto AND p.indTipo = :indTipo")

})



public class ProductoListaMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoListaMiembroPK productoListaMiembroPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;
    
    @JoinColumn(name = "ID_PRODUCTO", referencedColumnName = "ID_PRODUCTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;

    public ProductoListaMiembro() {
    }

    public ProductoListaMiembro(ProductoListaMiembroPK productoListaMiembroPK) {
        this.productoListaMiembroPK = productoListaMiembroPK;
    }

    public ProductoListaMiembro(String idMiembro, String idProducto, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.productoListaMiembroPK = new ProductoListaMiembroPK(idMiembro, idProducto);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public ProductoListaMiembro(String idMiembro, String idProducto) {
        this.productoListaMiembroPK = new ProductoListaMiembroPK(idMiembro, idProducto);
    }

    @ApiModelProperty(hidden = true)
    public ProductoListaMiembroPK getProductoListaMiembroPK() {
        return productoListaMiembroPK;
    }

    public void setProductoListaMiembroPK(ProductoListaMiembroPK productoListaMiembroPK) {
        this.productoListaMiembroPK = productoListaMiembroPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoListaMiembroPK != null ? productoListaMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoListaMiembro)) {
            return false;
        }
        ProductoListaMiembro other = (ProductoListaMiembro) object;
        if ((this.productoListaMiembroPK == null && other.productoListaMiembroPK != null) || (this.productoListaMiembroPK != null && !this.productoListaMiembroPK.equals(other.productoListaMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ProductoListaMiembro[ productoListaMiembroPK=" + productoListaMiembroPK + " ]";
    }
    
}
