/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Insignias;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.MiembroInsigniaNivel;
import com.flecharoja.loyalty.model.NivelesInsignias;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.validation.ConstraintViolationException;

/**
 * Clase encargada de proveer metodos para el mantenimiento de la informacion de
 * asocion de miembros con insignias
 *
 * @author wtencio
 */
@Stateless
public class MiembroInsigniaNivelBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean; // EJB con metodos de negocio para el manejo de bitacora

    /**
     * Método que asigna a un miembro un nivel de una insignia especifica, solo
     * para insignias de tipo status
     *
     * @param idMiembro identificador único del miembro
     * @param idNivel identificador único del nivel
     * @param idInsignia identificador de la insignia
     * @param usuario usuario en sesión
     * @param locale
     */
    public void assignMiembroInsigniaNivel(String idMiembro, String idNivel, String idInsignia, String usuario, Locale locale) {
        MiembroInsigniaNivel miembroInsigniaNivel = new MiembroInsigniaNivel(idMiembro, idInsignia);

        try {
            Miembro miembro = em.find(Miembro.class, idMiembro);
            NivelesInsignias nivelInsignia = em.find(NivelesInsignias.class, idNivel);
            Insignias insignia = em.find(Insignias.class, idInsignia);
            //verifica que la insignia no este en estado archivado o en borrador
            if (insignia.getEstado().equals(Insignias.Estados.BORRADOR.getValue()) || insignia.getEstado().equals(Insignias.Estados.ARCHIVADO.getValue())) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", this.getClass().getSimpleName());
            }

            if (insignia.getIdInsignia().equals(nivelInsignia.getIdInsignia().getIdInsignia())) {
                miembroInsigniaNivel.setInsignias(insignia);
                miembroInsigniaNivel.setMiembro(miembro);
                miembroInsigniaNivel.setIdNivel(nivelInsignia);
                miembroInsigniaNivel.setFechaCreacion(Calendar.getInstance().getTime());
                miembroInsigniaNivel.setUsuarioCreacion(usuario);

                em.persist(miembroInsigniaNivel);
                em.flush();
                bitacoraBean.logAccion(MiembroInsigniaNivel.class, idMiembro + "/" + idInsignia, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "badge_level_not_badge");
            }

        } catch (EntityExistsException e) {
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "badge_member_exists");
        } catch (ConstraintViolationException e) {//atributos no validos
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que asigna a un miembro a una insignia collector
     *
     * @param idMiembro identificador único del miembro
     * @param idInsignia identificador de la insignia
     * @param usuario usuario en sesión
     * @param locale
     */
    public void assignMiembroInsignia(String idMiembro, String idInsignia, String usuario, Locale locale) {
        MiembroInsigniaNivel miembroInsigniaNivel = new MiembroInsigniaNivel(idMiembro, idInsignia);

        try {
            Miembro miembro = em.find(Miembro.class, idMiembro);
            Insignias insignia = em.find(Insignias.class, idInsignia);

            if (insignia.getEstado().equals(Insignias.Estados.BORRADOR.getValue()) || insignia.getEstado().equals(Insignias.Estados.ARCHIVADO.getValue())) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", this.getClass().getSimpleName());
            }
            if (insignia.getTipo().equals(Insignias.Tipos.COLLECTOR.getValue())) {
                miembroInsigniaNivel.setInsignias(insignia);
                miembroInsigniaNivel.setMiembro(miembro);
                miembroInsigniaNivel.setIdNivel(null);
                miembroInsigniaNivel.setFechaCreacion(Calendar.getInstance().getTime());
                miembroInsigniaNivel.setUsuarioCreacion(usuario);

                em.persist(miembroInsigniaNivel);
                em.flush();
                bitacoraBean.logAccion(MiembroInsigniaNivel.class, idMiembro + "/" + idInsignia, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "tipo");
            }
        } catch (EntityExistsException e) {
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "badge_member_exists");
        } catch (ConstraintViolationException e) {//atributos no validos
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

    }

    /**
     * Método que quita la relación de un miembro con una insignia y su nivel
     *
     * @param idMiembro identificador único del miembro
     * @param idInsignia identificador único de una insignia
     * @param usuario usuario en sesión
     * @param locale
     */
    public void unassignMiembroNivelMetrica(String idMiembro, String idInsignia, String usuario, Locale locale) {
        MiembroInsigniaNivel miembroInsigniaNivel = (MiembroInsigniaNivel) em.createNamedQuery("MiembroInsigniaNivel.findByIdInsigniaByIdMiembro").setParameter("idInsignia", idInsignia).setParameter("idMiembro", idMiembro).getSingleResult();
        if (miembroInsigniaNivel == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "bagde_member_not_found");
        }
        em.remove(miembroInsigniaNivel);
        bitacoraBean.logAccion(MiembroInsigniaNivel.class, idInsignia + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);

    }

    /**
     * Método que devuelve una lista de niveles de insignias que están asociados
     * a un miembro especifico
     *
     * @param idMiembro identificador del miembro
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return Lista de niveles
     */
    public Map<String, Object> getNivelesInsigniaMiembro(String idMiembro, int cantidad, int registro, Locale locale) {

        Map<String, Object> respuesta = new HashMap<>();
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        List resultado = new ArrayList();
        List registros = new ArrayList();

        int total;
        try {
            registros = em.createNamedQuery("MiembroInsigniaNivel.findByIdMiembro").setParameter("idMiembro", idMiembro).getResultList();
            total = registros.size();
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createNamedQuery("MiembroInsigniaNivel.findByIdMiembro").setParameter("idMiembro", idMiembro)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Método que obtiene la lista de insignias ganadas o no por un miembro dependiendo del tipo 
     * @param idMiembro
     * @param tipo
     * @param estados
     * @param tiposInsignia
     * @param busqueda
     * @param filtros
     * @param ordenTipo
     * @param ordenCampo
     * @param cantidad
     * @param registro
     * @param locale
     * @return 
     */
    public Map<String, Object> getInsigniasMiembro(String idMiembro, String tipo, List<String> estados, List<String> tiposInsignia, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Insignias> root = query.from(Insignias.class);

        query = Busquedas.getCriteriaQueryInsignias(cb, query, root, estados, tiposInsignia, busqueda, filtros, ordenTipo, ordenCampo);
        Subquery<String> subquery = query.subquery(String.class);
        Root<MiembroInsigniaNivel> rootSubquery = subquery.from(MiembroInsigniaNivel.class);
        subquery.select(rootSubquery.get("miembroInsigniaNivelPK").get("idInsignia"));
        if (tipo == null || tipo.toUpperCase().charAt(0) == Indicadores.ASIGNADO) {
            subquery.where(cb.equal(rootSubquery.get("miembroInsigniaNivelPK").get("idMiembro"), idMiembro));
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idInsignia")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idInsignia")).value(subquery)));
            }
        } else {
            if (tipo.toUpperCase().charAt(0) == Indicadores.DISPONIBLES) {
                subquery.where(cb.equal(rootSubquery.get("miembroInsigniaNivelPK").get("idMiembro"), idMiembro));
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.equal(root.get("estado"), Insignias.Estados.PUBLICADO.getValue()), cb.or(cb.in(root.get("idInsignia")).value(subquery).not()));
                } else {
                    query.where(cb.equal(root.get("estado"), Insignias.Estados.PUBLICADO.getValue()), cb.or(cb.in(root.get("idInsignia")).value(subquery).not()));
                }
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "tipo");
            }
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * Método que obtiene la lista de miembros disponibles o asignados a una insignia dependiendo del tipo
     * @param idInsignia
     * @param tipo
     * @param estados
     * @param generos
     * @param estadosCivil
     * @param educacion
     * @param contactoEmail
     * @param contactoSMS
     * @param contactoNotificacion
     * @param contactoEstado
     * @param tieneHijos
     * @param busqueda
     * @param filtros
     * @param cantidad
     * @param registro
     * @param ordenTipo
     * @param ordenCampo
     * @param locale
     * @return 
     */
    public Map<String, Object> getMiembrosInsignias(String idInsignia, String tipo, List<String> estados, List<String> generos, List<String> estadosCivil, List<String> educacion, String contactoEmail, String contactoSMS, String contactoNotificacion, String contactoEstado, String tieneHijos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Miembro> root = query.from(Miembro.class);

        query = Busquedas.getCriteriaQueryMiembros(cb, query, root, estados, generos, estadosCivil, educacion, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, ordenTipo, ordenCampo);
        Subquery<String> subquery = query.subquery(String.class);
        Root<MiembroInsigniaNivel> rootSubquery = subquery.from(MiembroInsigniaNivel.class);
        subquery.select(rootSubquery.get("miembroInsigniaNivelPK").get("idMiembro"));
        if (tipo == null || tipo.toUpperCase().charAt(0) == Indicadores.ASIGNADO) {
            subquery.where(cb.equal(rootSubquery.get("miembroInsigniaNivelPK").get("idInsignia"), idInsignia));
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            }
        } else {
            if (tipo.toUpperCase().charAt(0) == Indicadores.DISPONIBLES) {
                subquery.where(cb.equal(rootSubquery.get("miembroInsigniaNivelPK").get("idInsignia"), idInsignia));
                if (query.getRestriction() != null) {
                    query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                } else {
                    query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                }
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "tipo");
            }
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //recurrido del resultado para la asignacion de informacion adicional (almacenada en keycloak)
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            resultado.stream().forEach((t) -> {
                Miembro miembro = (Miembro) t;
                try {
                    miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
                } catch (Exception e2) {
                    miembro.setNombreUsuario("Prueba");
                }
            });
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

}
