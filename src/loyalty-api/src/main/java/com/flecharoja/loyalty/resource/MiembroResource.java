package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.AtributoDinamico;
import com.flecharoja.loyalty.model.Grupo;
import com.flecharoja.loyalty.model.Insignias;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.MiembroMetricaNivel;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.model.TransaccionCompra;
import com.flecharoja.loyalty.service.AtributoMiembroBean;
import com.flecharoja.loyalty.service.ElegibilidadMiembroBean;
import com.flecharoja.loyalty.service.RegistrosActividadesBean;
import com.flecharoja.loyalty.service.GrupoMiembroBean;
import com.flecharoja.loyalty.service.MiembroBean;
import com.flecharoja.loyalty.service.MiembroInsigniaNivelBean;
import com.flecharoja.loyalty.service.MiembroNivelMetricaBean;
import com.flecharoja.loyalty.service.TransaccionBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase recursos http para el acceso a funcionalidades del manejo de miembros
 *
 * @author svargas, wtencio
 */
@Api(value = "Miembro")
@Path("miembro")
public class MiembroResource {

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @EJB
    MiembroBean bean; //EJB con los metodos de negocio para el manejo de miembros
    
    @EJB
    AtributoMiembroBean atributoMiembroBean; //EJB con los metodos de negocio para el manejo de atributos a miembros
    
    @EJB
    GrupoMiembroBean grupoMiembroBean;

    @EJB
    ElegibilidadMiembroBean elegibilidadBean;
    
    @EJB
    MiembroInsigniaNivelBean miembroInsigniaNivelBean;

    @EJB
    MiembroNivelMetricaBean miembroNivelMetricaBean;
    
    @Context
    HttpServletRequest request;
    
    @EJB
    RegistrosActividadesBean estadisticasBean;
    
    @EJB
    TransaccionBean transaccionBean;

    /**
     * Metodo que obtiene un listado de todos los miembros en un rango de numero
     * de registros validos segun los parametros de cantidad y registro,
     * filtrandolos opcionalmente por estados (por defecto todos), de ocurrir un
     * error se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param estados Parametro opcional con los indicadores de estados deseados
     * @param generos Indicadores de generos deseados
     * @param estadosCiviles Indicadores de estados civiles deseados
     * @param educaciones Indicadores de educaciones deseados
     * @param contactoEmail Indicador booleano sobre el contacto de email
     * @param contactoSMS Indicador booleando sobre el contacto de sms
     * @param contactoNotificacion Indicador booleando sobre el contacto de notificacion push
     * @param contactoEstado Indicador booleando sobre el contacto de estado
     * @param tieneHijos Indicador booleando sobre el tener hijos
     * @param busqueda Terminos de busqueda
     * @param filtros Campos sobre que aplicar la busqueda
     * @param ordenTipo
     * @param ordenCampo
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta con el listado de miembros en un rango de registros y
     * encabezado con el valor del rango y maximo aceptado
     */
    @ApiOperation(value = "Obtener listas de miembros",
            responseContainer = "List",
            response = Miembro.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMiembros(
            @ApiParam(value = "Indicadores de estado de miembros") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de genero de miembros") @QueryParam("genero") List<String> generos,
            @ApiParam(value = "Indicadores de estado civil de miembros") @QueryParam("estado-civil") List<String> estadosCiviles,
            @ApiParam(value = "Indicadores de educacion de miembros") @QueryParam("educacion") List<String> educaciones,
            @ApiParam(value = "Indicador booleano de contacto de email") @QueryParam("contacto-email") String contactoEmail,
            @ApiParam(value = "Indicador booleano de contacto de sms") @QueryParam("contacto-sms") String contactoSMS,
            @ApiParam(value = "Indicador booleano de contacto de notificacion") @QueryParam("contacto-notificacion") String contactoNotificacion,
            @ApiParam(value = "Indicador booleano de contacto de estado") @QueryParam("contacto-estado") String contactoEstado,
            @ApiParam(value = "Indicador booleano de tiene hijos") @QueryParam("tiene-hijos") String tieneHijos,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        List<String> estadosTemp = new ArrayList<>();
        estados.forEach((estado) -> estadosTemp.addAll(Arrays.asList(estado.split("-"))));
        respuesta = bean.getMiembros(estadosTemp, generos, estadosCiviles, educaciones, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }

    //TODO BORRAR /buscar/*
    
    @GET
    @Path("/buscar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchMiembros(
            @QueryParam("estado") String estado,
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @QueryParam("registro") int registro) {
        return this.getMiembros(estado==null?null:Arrays.asList(estado.split("-")), null, null, null, null, null, null, null, null, null, null, null, null, cantidad, registro);
    }
    
    @GET
    @Path("/buscar/{busqueda}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchMiembros(
            @ApiParam(value = "Palabras claves de busqueda")
            @PathParam("busqueda") String busqueda,
            @ApiParam(value = "Indicadores de estado de miembros")
            @QueryParam("estado") String estado,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        return this.getMiembros(estado==null?null:Arrays.asList(estado.split("-")), null, null, null, null, null, null, null, null, busqueda, null, null, null, cantidad, registro);
    }

    /**
     * Metodo que obtiene la informacion de un miembro por su identificador, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idMiembro Identificador del miembro
     * @return Informacion del miembro representado en formato JSON
     */
    @ApiOperation(value = "Obtencion de informacion de miembro por identificador",
            response = Miembro.class)
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idMiembro}")
    @Produces(MediaType.APPLICATION_JSON)
    public Miembro getMiembroPorId(
            @ApiParam(value = "Identificador de miembro", required = true)
            @PathParam("idMiembro") String idMiembro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bean.getMiembro(idMiembro,request.getLocale());
    }
    
//      /**
//     * Metodo que obtiene la informacion de un miembro por su identificador, de
//     * ocurrir un error se retornara una respuesta con un diferente estado junto
//     * con un encabezado "Error-Reason" con un valor numerico indicando la
//     * naturaleza del error.
//     *
//     * @param idMiembro Identificador del miembro
//     * @return Informacion del miembro representado en formato JSON
//     */
//    @ApiOperation(value = "Obtencion de informacion de miembro por identificador",
//            response = Miembro.class)
//    @ApiResponses(value = {
//        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
//            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
//        }),
//        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
//            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
//        }),
//        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
//            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
//        })
//    })
//    @GET
//    @Path("{idMiembro}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response existsNombreUsuario(
//            @ApiParam(value = "Identificador de miembro", required = true)
//            @PathParam("idMiembro") String idMiembro) {
//        String token = request.getHeader("Authorization").substring(6);
//        if (token != null && authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
//            Miembro miembro;
//            try {
//                miembro = bean.getMiembro(idMiembro,request.getLocale());
//            } catch (Exception ex) {
//                if (ex instanceof WebApplicationException) {
//                    throw ex;
//                }
//                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//                throw new MyException(MyException.ErrorSistema.ERROR_DESCONOCIDO, request.getLocale(), "unknown_error");
//            }
//            return Response.ok(miembro).build();
//        } else {
//            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
//        }
//    }

    /**
     * Metodo que almacena la informacion de un nuevo miembro y retorna el
     * identificador creado del mismo, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param miembro Objeto con la informacion del nuevo miembro
     * @return Respuesta OK con el identificador del miembro creado
     */
    @ApiOperation(value = "Registro de miembro",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertMiembro(
            @ApiParam(value = "Informacion de miembro", required = true) Miembro miembro) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
       
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        miembro.setUsuarioCreacion(usuarioContext);
        miembro.setUsuarioModificacion(usuarioContext);
        
        String idMiembro = bean.insertMiembro(miembro, request.getLocale());
        bean.postRegistroMiembro(miembro, request.getLocale());
        
        return idMiembro;
    }

    /**
     * Metodo que modifica la informacion de un miembro existente, de ocurrir un
     * error se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param miembro Objeto con la informacion del miembro
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Modificacion de miembro")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void editMiembro(
            @ApiParam(value = "Informacion de miembro", required = true) Miembro miembro) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        miembro.setUsuarioModificacion(usuarioContext);
        bean.editMiembro(miembro,request.getLocale());
    }

    /**
     * Metodo que establece la condicion de un miembro en suspendido agregando
     * la razon de su suspencion dentro del parametro causaSuspension, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idMiembro Identificador del miembro
     * @param causaSuspension Texto con informacion sobre la causa
     */
    @ApiOperation(value = "Suspension de miembro")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("{idMiembro}/suspender")
    @Consumes(MediaType.TEXT_PLAIN)
    public void disableMiembro(
            @ApiParam(value = "Identificador de miembro", required = true)
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Texto sobre la causa se suspension", required = true) String causaSuspension) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.disableMiembro(idMiembro, causaSuspension, usuarioContext,request.getLocale());
    }
    
    @ApiOperation(value = "Activacion de miembro")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("{idMiembro}/activar")
    @Consumes(MediaType.APPLICATION_JSON)
    public void enableMiembro(
            @ApiParam(value = "Identificador de miembro", required = true)
            @PathParam("idMiembro") String idMiembro) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        bean.enableMiembro(idMiembro, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que verifica la existencia del atributo de nombre de usuario de
     * miembro existente y retorna una respuesta en terminos de true/false, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param email Valor del nombre de usuario a verificar
     * @return Palabra clave con los siguientes posibles valores: "true" Nombre
     * de despliegue existe, "false" Nombre de despliegue no existe
     */
    @ApiOperation(value = "Verificacion de existencia de nombre de usuario en miembros")
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("comprobar-email/{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean existsEmail(
            @ApiParam(value = "Email del miembro", required = true)
            @PathParam("email") String email) {
        String token;
        try{
            token =request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bean.existsCorreo(email);
    }
    /**
     * Metodo que verifica la existencia del atributo de nombre de usuario de
     * miembro existente y retorna una respuesta en terminos de true/false, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param nombre Valor del nombre de usuario a verificar
     * @return Palabra clave con los siguientes posibles valores: "true" Nombre
     * de despliegue existe, "false" Nombre de despliegue no existe
     */
    @ApiOperation(value = "Verificacion de existencia de nombre de usuario en miembros")
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("comprobar-nombre/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean existsNombreUsuario(
            @ApiParam(value = "Nombre de usuario del miembro", required = true)
            @PathParam("nombre") String nombre) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bean.existsUserName(nombre,request.getLocale());
    }
    
    /**
     * Metodo que verifica la existencia del atributo de nombre de usuario de
     * miembro existente y retorna una respuesta en terminos de true/false, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param pass Valor del nombre de usuario a verificar
     * @return Palabra clave con los siguientes posibles valores: "true" Nombre
     * de despliegue existe, "false" Nombre de despliegue no existe
     */
    @ApiOperation(value = "Verificacion de existencia de nombre de usuario en miembros")
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("validar-pass/{pass}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean validatePassword(
            @ApiParam(value = "Password", required = true)
            @PathParam("pass") String pass) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bean.validatePassword(pass);
    }

//    /**
//     * Nota: eliminar subrecurso
//     *
//     * @param nombre --
//     * @return --
//     */
//    @GET
//    @Path("exists/{nombre}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response oldExistsNombreUsuario(@PathParam("nombre") String nombre) {
//        return existsNombreUsuario(nombre);
//    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++ Miembro eligible <-> Segmento ++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /**
     * Metodo que obtiene un listado de todos los segmento donde un miembro por
     * su identificador esta presente en la lista de eligibles, los registros
     * estaran en un rango valido segun los parametros de cantidad y registro,
     * filtrandolos opcionalmente por estados (por defecto todos), de ocurrir un
     * error se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param idMiembro Identificador de miembro
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta con el listado de segmentos en un rango de registros y
     * encabezado con el valor del rango y maximo aceptado
     */
    @ApiOperation(value = "Obtencion de segmentos de miembro eligible",
            responseContainer = "List",
            response = Segmento.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idMiembro}/segmento")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSegmentosPorMiembroEligible(
            @ApiParam(value = "Identificador de miembro", required = true)
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_LECTURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
            
        Map<String, Object> respuesta = elegibilidadBean.getSegmentosPorMiembroEligible(idMiembro, registro, cantidad,Locale.getDefault());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++ Manejo de miembro-metrica-nivel
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    /**
     * Metodo que asigna un nivel de una metrica a un miembro especifico, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idMiembro identificador unico del miembro
     * @param idMetrica identificador unico de la metrica
     * @param idNivel identificador del nivel
     * @return Respuesta OK con el identificador del miembro creado
     */
    @ApiOperation(value = "Asignacion de un nivel de metrica inicial a un miembro")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idMiembro}/metrica/{idMetrica}/nivel/{idNivel}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void assignMiembroMetricaNivel(
            @ApiParam(value = "Identificador de miembro", required = true)
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Identificador de metrica", required = true)
            @PathParam("idMetrica") String idMetrica,
            @ApiParam(value = "Identificador de nivel de metrica", required = true)
            @PathParam("idNivel") String idNivel) {
        
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale()) ||
                !authzBean.tienePermiso(IndicadoresRecursos.METRICAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        miembroNivelMetricaBean.assignMiembroNivelMetrica(idMiembro,idNivel,idMetrica, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que quita un nivel de una metrica a un miembro especifico, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idMiembro identificador unico del miembro
     * @param idMetrica identificador unico de la metrica
     */
    @ApiOperation(value = "Eliminacion de la asignacion de un nivel de metrica inicial a un miembro")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idMiembro}/metrica/{idMetrica}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void unassignMiembroMetricaNivel(
            @ApiParam(value = "Identificador de miembro", required = true)
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Identificador de metrica", required = true)
            @PathParam("idMetrica") String idMetrica) {
        
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.METRICAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        miembroNivelMetricaBean.unassignMiembroNivelMetrica(idMiembro,idMetrica, usuarioContext,request.getLocale() );
    }

    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de niveles
     * asociados a un miembro en particular en el caso de error al ejecutar la
     * tarea se retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param idMiembro identificador unido del miembro
     * @param tipo
     * @param estados
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param expiracion
     * @param busqueda
     * @param ordenTipo
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param filtros
     * @param ordenCampo
     * @return Lista de miembros en formato JSON
     */
    @ApiOperation(value = "Obtener listas niveles iniciales de un miembro",
            responseContainer = "List",
            response = MiembroMetricaNivel.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idMiembro}/metricas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMetricasMiembro(
            @ApiParam("Identificador de miembro")
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Indicador del tipo de lista de metricas") @QueryParam("tipo") String tipo,
            @ApiParam(value = "Indicadores de estado de metricas") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de expiracion de metrica") @QueryParam("expiracion") String expiracion,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros  sobre donde aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = miembroNivelMetricaBean.getMetricasMiembro(idMiembro, tipo, estados, expiracion, busqueda, filtros, ordenTipo, ordenCampo, cantidad, registro, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();

    }

    
    
    /**
     * Metodo acciona una tarea que obtiene un listado de insignias que han sido
     * asignados a un miembro, en el caso de error al ejecutar la tarea se
     * retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param idMiembro identificador unico del miembro
     * @param tipo
     * @param estados
     * @param tipos
     * @param busqueda
     * @param filtros
     * @param ordenTipo
     * @param ordenCampo
     * @param cantidad Parametro opcional con un valor por defecto de 10 que define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer registro desde donde devolver el listado de entidades
     * @return Representacion en JSON del listado de miembros
     */
    @ApiOperation(value = "Obtener insignias de un miembro",
            responseContainer = "List",
            response = Insignias.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idMiembro}/insignias")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInsigniasMiembro(
            @ApiParam(value = "Identificador del miembro", required = true)
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Indicador del tipo de lista de insignias") @QueryParam("tipo") String tipo,
            @ApiParam(value = "Indicadores de estado de insignias") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de tipos de insignias") @QueryParam("tipo-insignia") List<String> tipos,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros  sobre donde aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
       
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = miembroInsigniaNivelBean.getInsigniasMiembro(idMiembro, tipo, estados, tipos, busqueda, filtros, ordenTipo, ordenCampo, cantidad, registro, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }
    
    
  
//    @GET
//    @Path("{idMiembro}/no-insignias")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response getInsigniasNoMiembro(
//            @ApiParam(value = "Identificador del miembro", required = true)
//            @PathParam("idMiembro") String idMiembro,
//            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
//            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
//            @QueryParam("cantidad") int cantidad,
//            @ApiParam(value = "Numero de registro inicial")
//            @QueryParam("registro") int registro) {
//        
//       return this.getInsigniasMiembro(idMiembro, Indicadores.DISPONIBLES+"", null, null, null, null, null, null, cantidad, registro);
//    }
    
    
     /**
     * Metodo acciona una tarea que obtiene un listado de grupos que han sido
     * asignados a un miembro, en el caso de error al ejecutar la tarea se
     * retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param idMiembro identificador unico del miembro
     * @param tipo
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param busqueda
     * @param filtros
     * @param visibilidad
     * @param ordenTipo
     * @param ordenCampo
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Representacion en JSON del listado de miembros
     */
    @ApiOperation(value = "Obtener grupos al que pertenece un miembro",
            responseContainer = "List",
            response = Grupo.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idMiembro}/grupos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGruposMiembro(
            @ApiParam(value = "Identificador del miembro", required = true)
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Indicador del tipo de lista de grupo") @QueryParam("tipo") String tipo,
            @ApiParam(value = "Indicadores de estado de visibilidad del grupo") @QueryParam("visibilidad") List<String> visibilidad,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros  sobre donde aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
       
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = grupoMiembroBean.getGruposMiembro(idMiembro, tipo, visibilidad, busqueda, filtros, ordenTipo, ordenCampo, cantidad, registro, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }
    
    
//    @GET
//    @Path("{idMiembro}/grupos-excluidos")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response getGruposNotInMiembro(
//            @PathParam("idMiembro") String idMiembro,
//            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
//            @QueryParam("cantidad") int cantidad,
//            @QueryParam("registro") int registro) {
//        return this.getGruposMiembro(idMiembro, Indicadores.DISPONIBLES+"", null, null, null, null, null, cantidad, registro);
//    }
    
    /**
     * Metodo acciona una tarea que obtiene un listado de atributos que han sido
     * asignados a un miembro, de ocurrir un error se retornara una respuesta
     * con un diferente estado junto con un encabezado "Error-Reason" con un
     * valor numerico indicando la naturaleza del error, de no ser asi se
     * retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que define la cantidad maxima de registros por respuesta
     * @param estados
     * @param tipoDatos
     * @param visibilidad
     * @param requerido
     * @param busqueda
     * @param filtros
     * @param ordenTipo
     * @param ordenCampo
     * @param registro Parametro opcional que define el numero de primer registro desde donde devolver el listado de entidades
     * @param idMiembro identificador unico del miembro
     * @return Representacion en JSON del listado de miembros
     */
    @ApiOperation(value = "Obtener atributos dinámicos de un miembro",
            responseContainer = "List",
            response = AtributoDinamico.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idMiembro}/atributos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAtributosDinamicosMiembro(
            @ApiParam(value = "Identificador del miembro", required = true)
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Indicadores de estado") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de tipo de datos") @QueryParam("tipo-dato") List<String> tipoDatos,
            @ApiParam(value = "Indicador de visibilidad") @QueryParam("visible") String visibilidad,
            @ApiParam(value = "Indicador de requerido") @QueryParam("requerido") String requerido,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if ( !authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = atributoMiembroBean.getAtributosDinamicosPorMiembro(idMiembro, estados, tipoDatos, visibilidad, requerido, busqueda, filtros, ordenTipo, ordenCampo, cantidad, registro, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }
    
    
    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de todas las
     * transacciones de compra realizada por un miembro,en el caso de error al ejecutar la tarea se
     * retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param idMiembro
     * @param cantidad Parametro opcional con un valor por defecto de 10 que define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer registro desde donde devolver el listado de entidades
     * @return Representacion del listado en formato JSON
     */
    @ApiOperation(value = "Obtener transacciones de compra realizadas por un miembro",
            responseContainer = "List",
            response = TransaccionCompra.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idMiembro}/transacciones")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTransacciones(
            @ApiParam(value = "Identificador del miembro") @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = transaccionBean.getTransaccionesMiembro(idMiembro, cantidad, registro, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }
    
    
     /**
     * Metodo que obtiene un listado de todos los miembros referenciados por el miembro 
     * que pasa por parametro en un rango de numero
     * de registros validos segun los parametros de cantidad y registro,
     * filtrandolos opcionalmente por estados (por defecto todos), de ocurrir un
     * error se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     * @param idMiembro
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta con el listado de miembros en un rango de registros y
     * encabezado con el valor del rango y maximo aceptado
     */
    @ApiOperation(value = "Obtener listas de miembros referenciados",
            responseContainer = "List",
            response = Miembro.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idMiembro}/referenciados")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMiembrosReferenciados(
            @ApiParam(value = "Identificador del miembro", required = true) @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = bean.getMiembrosReferenciados(idMiembro, cantidad, registro, request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }
    
    /**
     * Metodo que obtiene la cantidad de miembros activos e inactivos, de ocurrir un
     * error se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     * 
     */
    @ApiOperation(value = "Obtener la cantidad de miembros activos e inactivos",
            response = Json.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    //   Top de Miembros ganadores de premio 
    @GET
    @Path("cantidad-estado")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getEstadisticasMiembrosEstado() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bean.getEstadisticasMiembrosEstado(request.getLocale());
    }
    @GET
    @Path("cantidad-nuevos")
    @Produces(MediaType.APPLICATION_JSON)
    public Long cantidadMiembrosNuevos() {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bean.cantidadMiembrosNuevos(request.getLocale());
    }
}
