package com.flecharoja.loyalty.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaMisionVerContenido {
    private MisionVerContenido detalleVerContenido;

    public RespuestaMisionVerContenido() {
    }

    public MisionVerContenido getDetalleVerContenido() {
        return detalleVerContenido;
    }

    public void setDetalleVerContenido(MisionVerContenido detalleVerContenido) {
        this.detalleVerContenido = detalleVerContenido;
    }
}
