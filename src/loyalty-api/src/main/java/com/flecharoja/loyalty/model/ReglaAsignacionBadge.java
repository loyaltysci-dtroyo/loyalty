/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "REGLA_ASIGNACION_BADGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReglaAsignacionBadge.findAll", query = "SELECT r FROM ReglaAsignacionBadge r")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByIdRegla", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.idRegla = :idRegla")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByIdBadge", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.idBadge = :idBadge")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByIndOperadorRegla", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.indOperadorRegla = :indOperadorRegla")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByIndOperadorComparacion", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.indOperadorComparacion = :indOperadorComparacion")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByValorComparacion", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.valorComparacion = :valorComparacion")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByIndTipoValorComparacion", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.indTipoValorComparacion = :indTipoValorComparacion")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByFechaCantidad", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.fechaCantidad = :fechaCantidad")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByFechaInicio", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.fechaInicio = :fechaInicio")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByFechaFin", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.fechaFin = :fechaFin")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByFechaIndOperador", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.fechaIndOperador = :fechaIndOperador")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByUsuarioCreacion", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.usuarioCreacion = :usuarioCreacion")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByUsuarioModificiacion", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.usuarioModificiacion = :usuarioModificiacion")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByFechaCreacion", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByFechaModificacion", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.fechaModificacion = :fechaModificacion")
    , @NamedQuery(name = "ReglaAsignacionBadge.findByIndTipoRegla", query = "SELECT r FROM ReglaAsignacionBadge r WHERE r.indTipoRegla = :indTipoRegla")})
public class ReglaAsignacionBadge implements Serializable {

    public enum TiposRegla {
        EJECUCION_RETOS('A'),
        REFERIO_MIEMBRO('B'),
        ASCENDIO_NIVEL('C'),
        ACUMULADO_COMPRAS('D'),
        FRECUENCIA_COMPRAS('E');

        private final char value;
        private static final Map<Character, TiposRegla> lookup = new HashMap<>();

        private TiposRegla(char value) {
            this.value = value;
        }

        static {
            for (TiposRegla tipo : TiposRegla.values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static TiposRegla get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }
    
     public enum TiposValorComparacion {
        ENCUESTA('E'),
        PERFIL('P'),
        VER_CONTENIDO('V'),
        SUBIR_CONTENIDO('S'),
        RED_SOCIAL('R'),
        JUEGO_PREMIO('J'),
        PREFERENCIAS('A');

        private final char value;
        private static final Map<Character, TiposValorComparacion> lookup = new HashMap<>();

        private TiposValorComparacion(char value) {
            this.value = value;
        }

        static {
            for (TiposValorComparacion tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static TiposValorComparacion get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    

    public enum TiposUltimaFecha {
        DIA('D'),
        MES('M'),
        ANO('A');

        private final char value;
        private static final Map<Character, TiposUltimaFecha> lookup = new HashMap<>();

        private TiposUltimaFecha(char value) {
            this.value = value;
        }

        static {
            for (TiposUltimaFecha tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static TiposUltimaFecha get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum ValoresIndOperador {
        IGUAL("V"),
        MAYOR("W"),
        MAYOR_IGUAL("X"),
        MENOR("Y"),
        MENOR_IGUAL("Z");

        public final String value;
        private static final Map<String, ValoresIndOperador> lookup = new HashMap<>();

        private ValoresIndOperador(String value) {
            this.value = value;
        }

        static {
            for (ValoresIndOperador valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public static ValoresIndOperador get(String value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum ValoresIndOperadorRegla {
        OR("O"),
        AND("A");

        private final String value;
        private static final Map<String, ValoresIndOperadorRegla> lookup = new HashMap<>();

        private ValoresIndOperadorRegla(String value) {
            this.value = value;
        }

        static {
            for (ValoresIndOperadorRegla valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public static ValoresIndOperadorRegla get(String value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum TiposFecha {
        ULTIMO('B'),
        ANTERIOR('C'),
        DESDE('D'),
        ENTRE('E'),
        MES_ACTUAL('F'),
        ANO_ACTUAL('G'),
        HASTA('H');

        public final Character value;
        private static final Map<Character, TiposFecha> lookup = new HashMap<>();

        private TiposFecha(char value) {
            this.value = value;
        }

        static {
            for (TiposFecha valor : values()) {
                lookup.put(valor.value, valor);
            }
        }

        public static TiposFecha get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(generator = "reglabadge_uuid")
    @GenericGenerator(name = "reglabadge_uuid", strategy = "uuid2")
    @Size(min = 1, max = 40)
    @Column(name = "ID_REGLA")
    private String idRegla;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_BADGE")
    private String idBadge;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "IND_OPERADOR_REGLA")
    private String indOperadorRegla;//operador para comparar esta regla con otras reglas

    @Size(max = 5)
    @Column(name = "IND_OPERADOR_COMPARACION")
    private String indOperadorComparacion; //operador de comparacion para el valor de comparacion

    @Size(max = 40)
    @Column(name = "VALOR_COMPARACION")
    private String valorComparacion;

    @Size(max = 5)
    @Column(name = "IND_TIPO_VALOR_COMPARACION")
    private String indTipoValorComparacion;//p ej tipo de mision cuando el valorcomparacion es una mision

    @Size(max = 5)
    @Column(name = "FECHA_CANTIDAD")
    private String fechaCantidad; // cantidad para cuando se usa el indUltimo

    @Size(max = 1)
    @Column(name = "FECHA_IND_ULTIMO")
    private String fechaIndUltimo; //indicador de ultimo (TiposUltimaFecha)

    @Size(max = 1)
    @Column(name = "FECHA_IND_TIPO")
    private String fechaIndTipo; //indicador de ultimo (TiposUltimaFecha)

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;

    @Size(max = 45)
    @Column(name = "FECHA_IND_OPERADOR")
    private String fechaIndOperador;//aplican operadores de fecha (TiposFecha)

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICIACION")
    private String usuarioModificiacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "IND_TIPO_REGLA")
    private String indTipoRegla;//indica el tipo de regla (TiposRegla)

    public ReglaAsignacionBadge() {
    }

    public ReglaAsignacionBadge(String idRegla) {
        this.idRegla = idRegla;
    }

    public ReglaAsignacionBadge(String idRegla, String idBadge, String indOperadorRegla, String usuarioCreacion, String usuarioModificiacion, Date fechaCreacion, Date fechaModificacion, String indTipoRegla) {
        this.idRegla = idRegla;
        this.idBadge = idBadge;
        this.indOperadorRegla = indOperadorRegla;
        this.usuarioCreacion = usuarioCreacion;
        this.usuarioModificiacion = usuarioModificiacion;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.indTipoRegla = indTipoRegla;
    }

    public String getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(String idRegla) {
        this.idRegla = idRegla;
    }

    public String getIdBadge() {
        return idBadge;
    }

    public void setIdBadge(String idBadge) {
        this.idBadge = idBadge;
    }

    public String getIndOperadorRegla() {
        return indOperadorRegla;
    }

    public void setIndOperadorRegla(String indOperadorRegla) {
        this.indOperadorRegla = indOperadorRegla;
    }

    public String getIndOperadorComparacion() {
        return indOperadorComparacion;
    }

    public void setIndOperadorComparacion(String indOperadorComparacion) {
        this.indOperadorComparacion = indOperadorComparacion;
    }

    public String getValorComparacion() {
        return valorComparacion;
    }

    public void setValorComparacion(String valorComparacion) {
        this.valorComparacion = valorComparacion;
    }

    public String getIndTipoValorComparacion() {
        return indTipoValorComparacion;
    }

    public void setIndTipoValorComparacion(String indTipoValorComparacion) {
        this.indTipoValorComparacion = indTipoValorComparacion;
    }

    public String getFechaCantidad() {
        return fechaCantidad;
    }

    public void setFechaCantidad(String fechaCantidad) {
        this.fechaCantidad = fechaCantidad;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getFechaIndOperador() {
        return fechaIndOperador;
    }

    public void setFechaIndOperador(String fechaIndOperador) {
        this.fechaIndOperador = fechaIndOperador;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getUsuarioModificiacion() {
        return usuarioModificiacion;
    }

    public void setUsuarioModificiacion(String usuarioModificiacion) {
        this.usuarioModificiacion = usuarioModificiacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getIndTipoRegla() {
        return indTipoRegla;
    }

    public void setIndTipoRegla(String indTipoRegla) {
        this.indTipoRegla = indTipoRegla;
    }

    public String getFechaIndUltimo() {
        return fechaIndUltimo;
    }

    public void setFechaIndUltimo(String fechaIndUltimo) {
        this.fechaIndUltimo = fechaIndUltimo;
    }

    public String getFechaIndTipo() {
        return fechaIndTipo;
    }

    public void setFechaIndTipo(String fechaIndTipo) {
        this.fechaIndTipo = fechaIndTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegla != null ? idRegla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReglaAsignacionBadge)) {
            return false;
        }
        ReglaAsignacionBadge other = (ReglaAsignacionBadge) object;
        if ((this.idRegla == null && other.idRegla != null) || (this.idRegla != null && !this.idRegla.equals(other.idRegla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ReglaAsignacionBadge[ idRegla=" + idRegla + " ]";
    }

}
