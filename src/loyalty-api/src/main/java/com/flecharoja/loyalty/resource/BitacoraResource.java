package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.EntradaBitacoraApiExternal;
import com.flecharoja.loyalty.service.BitacoraApiExternalBean;
import com.flecharoja.loyalty.service.BitacoraBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase que provee de metodos http RESTful para el acceso a funcionalidades del
 * manejo de bitacora.
 *
 * @author wtencio, svargas
 */

@Api("Bitacora")
@Path("bitacora")
public class BitacoraResource {

    @EJB
    BitacoraBean bitacoraBean; //EJB con los metodos de negocio para el manejo de la bitacora
    
    @EJB
    BitacoraApiExternalBean bitacoraApiExternalBean;

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de autorización

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @Context
    HttpServletRequest request;

    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de todas las
     * bitacoras segun el tipo de accion definidos dentro de un parametro con
     * seperado por "-",por un rango establecido usando los parametros de
     * cantidad y registro y opcionalmente filtrados por indicadores de estado
     * contenidos dentro del parametro de estado (por defecto todos) bajo un
     * tipo de dato de OK(200), de ocurrir un error se retornara una respuesta
     * con un diferente estado junto con un encabezado "Error-Reason" con un
     * valor numerico indicando la naturaleza del error.
     *
     * @param accion indicador del tipo de accion que se realizo
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Representacion del listado en formato JSON
     */
    @ApiOperation(value = "Obtener registros de bitácora filtrado por una acción, por defecto todos",
            responseContainer = "List",
            response = Bitacora.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("admin")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBitacoraAccion(
            @ApiParam(value = "Indicador de acción de un registro de bitácora", required = false)
            @DefaultValue("null") @QueryParam("accion") String accion,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.BITACORA_PERMISO, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = bitacoraBean.getBitacoraAccion(accion, cantidad, registro, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Recurso que lee el historial de bitacora del api external sobre errores
     * de peticiones y otros en terminos de periodos de tiempo (hoy hasta X) o
     * todos los registros
     * 
     * @param periodoTiempo Indicador del periodo de tiempo deseado, por
     * defecto con un valor de un mes
     * @return Lista de entradas de bitacora del api external
     */
    @ApiOperation(value = "Obtener la bitacora del api external",
            responseContainer = "List",
            response = EntradaBitacoraApiExternal.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("external")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntradaBitacoraApiExternal> getBitacoraApiExternal(@QueryParam("periodo") @DefaultValue("1M") String periodoTiempo) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.BITACORA_PERMISO, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return bitacoraApiExternalBean.getBitacoraApiExternal(periodoTiempo, request.getLocale());
    }
}
