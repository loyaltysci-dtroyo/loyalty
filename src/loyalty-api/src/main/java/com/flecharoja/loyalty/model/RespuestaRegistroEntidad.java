package com.flecharoja.loyalty.model;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaRegistroEntidad {
    private String internalIdCreated;
    private Date dateCreation;

    public RespuestaRegistroEntidad() {
    }

    public RespuestaRegistroEntidad(String internalIdCreated, Date dateCreation) {
        this.internalIdCreated = internalIdCreated;
        this.dateCreation = dateCreation;
    }

    public String getInternalIdCreated() {
        return internalIdCreated;
    }

    public void setInternalIdCreated(String internalIdCreated) {
        this.internalIdCreated = internalIdCreated;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }
}
