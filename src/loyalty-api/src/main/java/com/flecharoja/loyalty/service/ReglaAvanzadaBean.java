package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.GrupoMiembro;
import com.flecharoja.loyalty.model.Insignias;
import com.flecharoja.loyalty.model.ListaReglas;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.Producto;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.model.ReglaAvanzada;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.Grupo;
import com.flecharoja.loyalty.model.GrupoNiveles;
import com.flecharoja.loyalty.model.NivelMetrica;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.flecharoja.loyalty.util.ReglasSegmento;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * reglas de cambio de metrica
 *
 * @author svargas
 */
@Stateless
public class ReglaAvanzadaBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora
    
    @EJB
    ElegibilidadMiembroBean elegibilidadBean;

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * @param idLista Identificador de la lista de reglas
     * @param locale
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List getReglasAvanzadasLista(String idLista,Locale locale) {
        try {
            em.getReference(ListaReglas.class, idLista).getIdLista();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idLista");
        }
        
        return em.createNamedQuery("ReglaAvanzada.findByIdLista").setParameter("idLista", idLista).getResultList();
    }

    /**
     * Metodo que crea una nueva regla en una lista especifica
     *
     * @param idSegmento Identificador de segmento
     * @param idLista Identificador de la lista de reglas
     * @param reglaAvanzada Objeto con la informacion de la regla
     * @param usuario usuario en sesion
     * @param locale
     * @return Respuesta con el identificador de la regla creada
     */
    public String createReglaAvanzada(String idSegmento, String idLista, ReglaAvanzada reglaAvanzada, String usuario, Locale locale) {
        if (reglaAvanzada==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "ReglaAvanzada");
        }
        
        Date date = Calendar.getInstance().getTime();
        reglaAvanzada.setFechaCreacion(date);

        reglaAvanzada.setUsuarioCreacion(usuario);

        ListaReglas listaReglas = em.find(ListaReglas.class, idLista);
        if (listaReglas == null ) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        if (!listaReglas.getIdSegmento().getIdSegmento().equals(segmento.getIdSegmento())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "segment_id_invalid");
        }
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        reglaAvanzada.setIdLista(idLista);
        
        //comprobacion de datos segun el tipo de regla
        ReglaAvanzada.TiposRegla tipoReglaAvanzada = ReglaAvanzada.TiposRegla.get(reglaAvanzada.getIndTipoRegla());
        if (tipoReglaAvanzada==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "intTipoRegla");
        }
        boolean checkFechas = false;
        switch(tipoReglaAvanzada) {
            case ULTIMA_CONEXION:
            case UNIERON_PROGRAMA: {
                checkFechas = true;
                break;
            }
            case RECIBIERON_MENSAJE:
            case ABRIERON_MENSAJE: {
                if (reglaAvanzada.getReferencia()==null || em.find(Notificacion.class, reglaAvanzada.getReferencia())==null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
                }
                checkFechas = true;
                break;
            }
            case CAMBIARON_NIVEL: {
                if (reglaAvanzada.getReferencia()==null || em.find(Metrica.class, reglaAvanzada.getReferencia())==null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "metric_not_found");
                }
                checkFechas = true;
                break;
            }
            case VIERON_MISION:
            case COMPLETARON_MISION:
            case GANARON_MISION: {
                if (reglaAvanzada.getReferencia()==null || em.find(Mision.class, reglaAvanzada.getReferencia())==null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
                }
                checkFechas = true;
                break;
            }
            case CANTIDAD_COMPRAS_UBICACION: {
                if (reglaAvanzada.getIndOperador()==null || (reglaAvanzada.getReferencia()!=null && em.find(Ubicacion.class, reglaAvanzada.getReferencia())==null) || "VWXYZ".indexOf(reglaAvanzada.getIndOperador())==-1) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
                }
                checkFechas = true;
                try {
                    Integer.parseInt(reglaAvanzada.getValorComparacion0());
                } catch (NumberFormatException e) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                }
                break;
            }
            case REDIMIERON_PREMIO: {
                if (reglaAvanzada.getReferencia()==null || em.find(Premio.class, reglaAvanzada.getReferencia())==null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
                }
                checkFechas = true;
                break;
            }
            case COMPRARON_PRODUCTO: {
                if (reglaAvanzada.getReferencia()==null || em.find(Producto.class, reglaAvanzada.getReferencia())==null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
                }
                checkFechas = true;
                break;
            }
            case GASTARON_DINERO: {
                checkFechas = true;
            }
            case REFERENCIAS_MIEMBROS: {
                if (reglaAvanzada.getIndOperador()==null || "VWXYZ".indexOf(reglaAvanzada.getIndOperador())==-1) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
                }
                try {
                    Integer.parseInt(reglaAvanzada.getValorComparacion0());
                } catch (NumberFormatException e) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                }
                break;
            }
            
            case PERTENECEN_GRUPO_MIE: {
                if (reglaAvanzada.getReferencia()==null || em.find(Grupo.class, reglaAvanzada.getReferencia())==null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_group_not_found");
                }
                break;
            }
            case PERTENECEN_SEGMENTO: {
                if (reglaAvanzada.getReferencia()==null || em.find(Segmento.class, reglaAvanzada.getReferencia())==null) {
                      throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
                }
                break;
            }
            case POSICION_TABLA_POS: {
                if (reglaAvanzada.getReferencia()==null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "referencia");
                }
                try {
                    em.createNamedQuery("TablaPosiciones.findByIdTabla").setParameter("idTabla", reglaAvanzada.getReferencia()).getSingleResult();
                } catch (NoResultException e) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "leaderboard_not_found");
                }
                
                try {
                    if (Long.parseLong(reglaAvanzada.getValorComparacion0()) > Long.parseLong(reglaAvanzada.getValorComparacion1())) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                    }
                } catch (NumberFormatException e) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                }
                break;
            }
            case VIERON_PROMOCION:
            case REDIMIERON_PROMOCION: {
                if (reglaAvanzada.getReferencia()==null || em.find(Promocion.class, reglaAvanzada.getReferencia())==null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
                }
                break;
            }
            case TIENE_INSIGNIA: {
                if (reglaAvanzada.getReferencia()==null || em.find(Insignias.class, reglaAvanzada.getReferencia())==null) {
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "badge_not_found");
                }
                break;
            }
            case TIPO_DISPOSITIVO: {
                if (reglaAvanzada.getValorComparacion0()==null
                        ||reglaAvanzada.getValorComparacion0().length()>1
                        || ReglaAvanzada.TiposDispositivos.get(reglaAvanzada.getValorComparacion0().charAt(0))==null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                }
                break;
            }
            case PERTENECE_NIVEL: {
                if (reglaAvanzada.getReferencia()==null || em.find(Metrica.class, reglaAvanzada.getReferencia())==null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "metric_not_found");
                }
                if (reglaAvanzada.getValorComparacion0()==null || em.find(NivelMetrica.class, reglaAvanzada.getValorComparacion0())==null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "metric_tier_not_found");
                }
                if (reglaAvanzada.getValorComparacion1()==null || em.find(GrupoNiveles.class, reglaAvanzada.getValorComparacion1())==null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "metric_tiers_group_not_found");
                }
                break;
            }
        }
        if (checkFechas) {
            if (reglaAvanzada.getFechaIndUltimo()!=null) {
                if (reglaAvanzada.getFechaInicio()!=null
                        || reglaAvanzada.getFechaFinal()!=null
                        || reglaAvanzada.getFechaCantidad()==null
                        || ReglaAvanzada.TiposUltimaFecha.get(reglaAvanzada.getFechaIndUltimo())==null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "advanced_rule_date_invalid");
                }
            } else {
                if (reglaAvanzada.getFechaInicio()!=null
                        && reglaAvanzada.getFechaFinal()!=null
                        && reglaAvanzada.getFechaInicio().after(reglaAvanzada.getFechaFinal())) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "advanced_rule_date_invalid");
                }
            }
        }

        em.persist(reglaAvanzada);

        em.flush();
        bitacoraBean.logAccion(ReglaAvanzada.class, reglaAvanzada.getIdRegla(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        //actualizacion de la lista de reglas
        listaReglas.setUsuarioModificacion(usuario);
        listaReglas.setFechaModificacion(date);
        em.merge(listaReglas);
        bitacoraBean.logAccion(ListaReglas.class, listaReglas.getIdLista(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        
        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestRecalculoRegla(reglaAvanzada.getIdRegla(), ReglasSegmento.TiposReglasSegmento.AVANZADA);
            } catch (Exception ex) {
                Logger.getLogger(ReglaAvanzadaBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }
        
        return reglaAvanzada.getIdRegla();
    }

    /**
     * Metodo que elimina de una lista especifica una regla
     *
     * @param idSegmento Identificador de segmento
     * @param idLista Identificador de la lista de reglas
     * @param idRegla Identificador de la regla
     * @param usuario usuario en sesión
     * @param locale
     */
    public void deleteReglaAvanzada(String idSegmento, String idLista, String idRegla, String usuario, Locale locale) {
        Date date = Calendar.getInstance().getTime();

        ListaReglas listaReglas = em.find(ListaReglas.class, idLista);
        if (listaReglas == null ) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }
        if (!listaReglas.getIdSegmento().getIdSegmento().equals(segmento.getIdSegmento())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "segment_id_invalid");
        }
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        try {
            //eliminacion de la regla (si es encontrada)
            em.remove(em.getReference(ReglaAvanzada.class, idRegla));

            em.flush();
            
            elegibilidadBean.deleteResultadoElegibilidadRegla(ReglasSegmento.TiposReglasSegmento.AVANZADA, idRegla, Locale.getDefault());
            
            bitacoraBean.logAccion(ReglaAvanzada.class, idRegla, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
            //actualizacion de la lista de reglas
            listaReglas.setUsuarioModificacion(usuario);
            listaReglas.setFechaModificacion(date);
            em.merge(listaReglas);
            bitacoraBean.logAccion(ListaReglas.class, listaReglas.getIdLista(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (EntityNotFoundException e) {//lista de reglas o regla no encontrada
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
        
        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestActualizacionSegmento(idSegmento);
            } catch (Exception ex) {
                Logger.getLogger(ReglaAvanzadaBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }
}
