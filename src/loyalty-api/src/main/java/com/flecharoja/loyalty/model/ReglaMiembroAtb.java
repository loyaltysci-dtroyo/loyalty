package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "REGLA_MIEMBRO_ATB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReglaMiembroAtb.findByIdLista", query = "SELECT r FROM ReglaMiembroAtb r WHERE r.idLista = :idLista ORDER BY r.fechaCreacion DESC"),
    @NamedQuery(name = "ReglaMiembroAtb.findByIdNivelMetrica", query = "SELECT r FROM ReglaMiembroAtb r WHERE r.nivelMetrica.idNivel = :idNivel ORDER BY r.fechaCreacion")
})
public class ReglaMiembroAtb implements Serializable {
    
    public enum Atributos {
        IND_MIEMBRO_SISTEMA('G'),
        FECHA_NACIMIENTO('A'),
        IND_GENERO('B'),
        IND_ESTADO_CIVIL('C'),
        FRECUENCIA_COMPRA('D'),
        NOMBRE('E'),
        APELLIDO('F'),
        APELLIDO2('R'),
        IND_ESTADO_MIEMBRO('H'),
        TIER_INICIAL('I'),
        TIER_PROGRESO('J'),
        CIUDAD_RESIDENCIA('K'),
        ESTADO_RESIDENCIA('L'),
        PAIS_RESIDENCIA('M'),
        CODIGO_POSTAL('N'),
        IND_EDUCACION('O'),
        INGRESO_ECONOMICO('P'),
        IND_HIJOS('Q'),
        ATRIBUTO_DINAMICO('R'),
        IND_CONTACTO_EMAIL('S'),
        IND_CONTACTO_NOTIFICACION('T'),
        IND_CONTACTO_SMS('U'),
        IND_CONTACTO_ESTADO('V');
        
        public final Character value;
        private static final Map<Character, Atributos> lookup = new HashMap<>();

        private Atributos(char value) {
            this.value = value;
        }
        
        static {
            for (Atributos atributo : values()) {
                lookup.put(atributo.value, atributo);
            }
        }
        
        public static Atributos get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "reglamiembroatb_uuid")
    @GenericGenerator(name = "reglamiembroatb_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_REGLA")
    private String idRegla;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ATRIBUTO")
    private Character indAtributo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_OPERADOR")
    private Character indOperador;
    
    @Size(min = 1, max = 100)
    @Column(name = "VALOR_COMPARACION")
    private String valorComparacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_LISTA")
    private String idLista;
    
    @Basic(optional = false)
    @NotNull
    @Size(max = 40)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @JoinColumn(name = "ID_ATRIBUTO_DINAMICO", referencedColumnName = "ID_ATRIBUTO")
    @ManyToOne
    private AtributoDinamico atributoDinamico;
    
    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne
    private Metrica metrica;
    
    @JoinColumn(name = "ID_NIVEL_METRICA", referencedColumnName = "ID_NIVEL")
    @ManyToOne
    private NivelMetrica nivelMetrica;

    public ReglaMiembroAtb() {
    }

    public String getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(String idRegla) {
        this.idRegla = idRegla;
    }

    public Character getIndAtributo() {
        return indAtributo;
    }

    public void setIndAtributo(Character indAtributo) {
        this.indAtributo = indAtributo!=null?Character.toUpperCase(indAtributo):null;
    }

    public Character getIndOperador() {
        return indOperador;
    }

    public void setIndOperador(Character indOperador) {
        this.indOperador = indOperador!=null?Character.toUpperCase(indOperador):null;
    }

    public String getValorComparacion() {
        return valorComparacion;
    }

    public void setValorComparacion(String valorComparacion) {
        this.valorComparacion = valorComparacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getIdLista() {
        return idLista;
    }

    public void setIdLista(String idLista) {
        this.idLista = idLista;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public AtributoDinamico getAtributoDinamico() {
        return atributoDinamico;
    }

    public void setAtributoDinamico(AtributoDinamico atributoDinamico) {
        this.atributoDinamico = atributoDinamico;
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }

    public NivelMetrica getNivelMetrica() {
        return nivelMetrica;
    }

    public void setNivelMetrica(NivelMetrica nivelMetrica) {
        this.nivelMetrica = nivelMetrica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegla != null ? idRegla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReglaMiembroAtb)) {
            return false;
        }
        ReglaMiembroAtb other = (ReglaMiembroAtb) object;
        if ((this.idRegla == null && other.idRegla != null) || (this.idRegla != null && !this.idRegla.equals(other.idRegla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ReglaMiembroAtb[ idRegla=" + idRegla + " ]";
    }

   
}
