/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.DocumentoInventario;
import com.flecharoja.loyalty.model.ExistenciasUbicacion;
import com.flecharoja.loyalty.model.ExistenciasUbicacionPK;
import com.flecharoja.loyalty.model.Premio;
import java.math.BigInteger;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author wtencio
 */
@Stateless
public class ExistenciasUbicacionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;

    /**
     * Método que actualiza la información de existencias de una ubicacion con
     * un premio especifico en un periodo y mes actuales
     *
     * @param idPremio identificador del premio
     * @param idUbicacionOrigen identificador de la ubicacion origen (bodega)
     * donde se compra o se realizan ajustes
     * @param idUbicacionDestino identificador de la ubicacion destino en el
     * caso que el documento sea de tipo interubicacion
     * @param cantidad de premios los cuales se ajustan, compran o rediman
     * @param tipo indicador del tipo de documento(Ajuste de mas o menos,
     * interubicacion, compra o redencion)
     * @param usuario en sesión
     * @param locale
     */
    public void updateExistencias(String idPremio, String idUbicacionOrigen, String idUbicacionDestino, int cantidad, double precioPromedio, DocumentoInventario.Tipos tipo, String usuario, Locale locale) {
        //si existe en el mes actual y año actual un registro actualizo
        List<ConfiguracionGeneral> configuracion = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
        if (configuracion.isEmpty()) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "general_configuration_not_found");
        }
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }
        ConfiguracionGeneral config = configuracion.get(0);

        //verifica si existe un registro de lo contrario lo crea xq de no existir ningun registro es porque no existen registros de otros mses
        ExistenciasUbicacion existencia = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(idUbicacionOrigen, idPremio, config.getMes(), config.getPeriodo()));
        if (existencia == null) {
            ExistenciasUbicacion existenciaRegistrar = new ExistenciasUbicacion(new ExistenciasUbicacionPK(idUbicacionOrigen, idPremio, config.getMes(), config.getPeriodo()), 0, 0, 0, 0);
            try {
                em.persist(existenciaRegistrar);
                em.flush();
                bitacoraBean.logAccion(ExistenciasUbicacion.class, idPremio + "/" + idUbicacionOrigen, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            } catch (ConstraintViolationException e) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }

            existencia = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(idUbicacionOrigen, idPremio, config.getMes(), config.getPeriodo()));
        }

        switch (tipo) {
            case REDENCION_AUTO: {
                existencia.setSalidas(existencia.getSalidas() + cantidad);
                existencia.setSaldoActual((existencia.getSaldoInicial() + existencia.getEntradas()) - existencia.getSalidas());
                break;
            }
            case AJUSTE_MAS: {
                existencia.setEntradas(existencia.getEntradas() + cantidad);
                existencia.setSaldoActual((existencia.getSaldoInicial() + existencia.getEntradas()) - existencia.getSalidas());
                break;
            }
            case COMPRA: {
                premio.setPrecioPromedio(precioPromedio);
                existencia.setEntradas(existencia.getEntradas() + cantidad);
                existencia.setSaldoActual((existencia.getSaldoInicial() + existencia.getEntradas()) - existencia.getSalidas());
                try {
                    em.merge(existencia);
                    em.merge(premio);
                    //actualizar cantidad total
                    em.flush();
                    bitacoraBean.logAccion(ExistenciasUbicacion.class, idPremio + "/" + idUbicacionOrigen, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
                } catch (ConstraintViolationException e) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
                }
                break;
            }
            case AJUSTE_MENOS: {
                //verifica nuevamente que las existencias alcancen
                ExistenciasUbicacion existenciaActual = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(idUbicacionOrigen, idPremio, config.getMes(), config.getPeriodo()));
                int disponibilidad = existenciaActual.getSaldoActual();
                if (disponibilidad < cantidad || disponibilidad - cantidad < 0) {
                    throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "insufficient_existence");
                }
                existencia.setSalidas(existencia.getSalidas() + cantidad);
                existencia.setSaldoActual((existencia.getSaldoInicial() + existencia.getEntradas()) - existencia.getSalidas());
                break;
            }
            case INTER_UBICACION: {
                // se verifica nuevamente que las existencias no queden en numeros rojos
                ExistenciasUbicacion existenciaActual = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(idUbicacionOrigen, idPremio, config.getMes(), config.getPeriodo()));
                int disponibilidad = existenciaActual.getSaldoActual();
                if (disponibilidad < cantidad || disponibilidad - cantidad < 0) {
                    throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "insufficient_existence");
                }

                //se registra las salidas en la ubicacion origen
                existencia.setSalidas(existencia.getSalidas() + cantidad);
                existencia.setSaldoActual((existencia.getSaldoInicial() + existencia.getEntradas()) - existencia.getSalidas());
                ExistenciasUbicacion ubicacionDestino = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(idUbicacionDestino, idPremio, config.getMes(), config.getPeriodo()));
                //si no existe un registro en la ubicacion destino con el premio el periodo y el mes se crea un nuevo registro y se obtiene
                if (ubicacionDestino == null) {
                    ExistenciasUbicacion existenciaDestino = new ExistenciasUbicacion(new ExistenciasUbicacionPK(idUbicacionDestino, idPremio, config.getMes(), config.getPeriodo()), 0, 0, 0, 0);
                    try {
                        em.persist(existenciaDestino);
                        em.flush();
                        bitacoraBean.logAccion(ExistenciasUbicacion.class, idPremio + "/" + idUbicacionDestino, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
                    } catch (ConstraintViolationException e) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
                    }
                    ubicacionDestino = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(idUbicacionDestino, idPremio, config.getMes(), config.getPeriodo()));
                }

                //se suma en entradas la cantidad en la ubicacion destino
                ubicacionDestino.setEntradas(ubicacionDestino.getEntradas() + cantidad);
                ubicacionDestino.setSaldoActual((ubicacionDestino.getSaldoInicial() + ubicacionDestino.getEntradas()) - ubicacionDestino.getSalidas());
                try {
                    em.merge(ubicacionDestino);
                    em.flush();
                    bitacoraBean.logAccion(ExistenciasUbicacion.class, idPremio + "/" + idUbicacionOrigen, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
                } catch (ConstraintViolationException e) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
                }
                break;
            }
            default: {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "document_invalid_type");
            }
        }

        try {
            em.merge(existencia);
            em.flush();
            long cantidadTotal = (long) em.createNamedQuery("ExistenciasUbicacion.findByPremioByPeriodoByMes").setParameter("idPremio", premio.getIdPremio()).setParameter("mes", config.getMes()).setParameter("periodo",config.getPeriodo()).getSingleResult();
            premio.setTotalExistencias(cantidadTotal);
            em.merge(premio);
            //actualizar cantidad total
           
            bitacoraBean.logAccion(ExistenciasUbicacion.class, idPremio + "/" + idUbicacionOrigen, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que acciona la funcion de cierre de mes la cual actualiza toda las
     * existencias de una ubicacion en particular
     *
     * @param idUbicacion identificador de la ubicacion
     * @param usuario usuario en sesion
     * @param locale
     */
    public void cierreMes(String usuario, Locale locale) {

        List<ConfiguracionGeneral> configuracion = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
        if (configuracion.isEmpty()) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "general_configuration_not_found");
        }
        ConfiguracionGeneral config = configuracion.get(0);
        Integer periodoActual = config.getPeriodo();
        Integer mesActual = config.getMes();

        //obtengo el ultimo mes del periodo 
        Integer mesUltimo = 12;// siempre 12 va a hacer el ultimo mes

        //se obtiene las existencias del periodo, mes y ubicacion 
        List<ExistenciasUbicacion> existencias = em.createNamedQuery("ExistenciasUbicacion.findByMesPeriodo")
                .setParameter("mes", mesActual)
                .setParameter("periodo", periodoActual)
                .getResultList();

        //en el caso que el mes actual sea el ultimo mes
        if (mesActual == mesUltimo) {
            //se le suma uno al periodo actual
            periodoActual = periodoActual+1;
            //el mes actual se vuelve a uno(primer mes del año)
            mesActual = 1;
        } else {
            //sino al mes actual se suma 1 y el periodo queda igual
            mesActual = mesActual+1;
        }

        //se recorre las existencias para crear los nuevos registros
        for (ExistenciasUbicacion existencia : existencias) {
            int saldoInicial = existencia.getSaldoActual();
            ExistenciasUbicacion nuevaExistencia = new ExistenciasUbicacion(new ExistenciasUbicacionPK(existencia.getUbicacion().getIdUbicacion(), existencia.getPremio().getIdPremio(), mesActual, periodoActual), saldoInicial, 0, 0, saldoInicial);
            //seteo los nuevos valores de periodo y mes
            config.setMes(mesActual);
            config.setPeriodo(periodoActual);
            try {
                em.persist(nuevaExistencia);
                em.merge(config);
                em.flush();
                bitacoraBean.logAccion(ExistenciasUbicacion.class, existencia.getPremio().getIdPremio() + "/" + existencia.getExistenciasUbicacionPK().getCodUbicacion(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            } catch (ConstraintViolationException e) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            }
        }
    }
}
