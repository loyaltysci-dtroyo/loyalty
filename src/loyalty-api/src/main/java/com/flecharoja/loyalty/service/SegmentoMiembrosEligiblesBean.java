package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.util.Indicadores;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * EJB encargado del manejo de subrecursos para el manejo de segmentos
 *
 * @author svargas
 */
@Stateless
public class SegmentoMiembrosEligiblesBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    private final Configuration hbaseConf;
    
    private final Properties sparkConf;
    
    public SegmentoMiembrosEligiblesBean() throws IOException {
        this.hbaseConf = new Configuration();
        this.hbaseConf.addResource("conf/hbase/hbase-site.xml");
        
        this.sparkConf = new Properties();
        this.sparkConf.load(SegmentoMiembrosEligiblesBean.class.getResourceAsStream("/conf/spark/spark-cluster.properties"));
    }
    
    /**
     * Metodo que obtiene la lista total de identificadores de miembros
     * eligibles de un segmento
     *
     * @param idSegmento Identificador de segmento
     * @param locale
     * @return Lista de identificadores de miembros
     */
    public List<String> getIdMiembrosEligibles(String idSegmento, Locale locale) {
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "ELEGIBLES_SEGMENTO"));
            Get get = new Get(Bytes.toBytes(idSegmento));
            get.addFamily(Bytes.toBytes("MIEMBROS"));
            Result result = table.get(get);
            if (table.exists(get)) {
                return result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                        .filter((t) -> Bytes.toBoolean(t.getValue()))
                        .map((t) -> Bytes.toString(t.getKey()))
                        .collect(Collectors.toList());
            } else {
                return null;
            }
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
    }

    /**
     * Metodo que obtiene una lista de los miembros eligibles del ultimo calculo
     * de eligibles de un segmento en un rango
     *
     * @param idSegmento Identificador de segmento
     * @param registro Numero de registro inicial
     * @param cantidad Cantidad de registros en la respuesta
     * @param locale
     * @return Respuesta con el listado de miembros en un rango valido
     */
    public Map<String, Object> getListaEligibles(String idSegmento, int registro, int cantidad, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        if (registro < 0 || cantidad < 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        Map<String, Object> respuesta = new HashMap<>();

        List<Miembro> resultado;
        long total;
        
        List<String> idsMiembros;
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "ELEGIBLES_SEGMENTO"));
            Get get = new Get(Bytes.toBytes(idSegmento));
            get.addFamily(Bytes.toBytes("MIEMBROS"));
            Result result = table.get(get);
            if (table.exists(get)) {
                idsMiembros = result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                        .filter((t) -> Bytes.toBoolean(t.getValue()))
                        .map((t) -> Bytes.toString(t.getKey()))
                        .collect(Collectors.toList());
            } else {
                idsMiembros = new ArrayList<>();
            }
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
        
        if (idsMiembros.isEmpty()) {
            resultado = new ArrayList<>();
            total = 0;
            registro = 0;
        } else {
            total = idsMiembros.size();
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            int registroFinal = cantidad + registro;
            registroFinal = registroFinal < idsMiembros.size() ? registroFinal : idsMiembros.size();

            List<String> idsRequeridos = idsMiembros.subList(registro, registroFinal);

            if (idsRequeridos.isEmpty()) {
                resultado = new ArrayList();
            } else {
                resultado = em.createNamedQuery("Miembro.findAllByIdMiembro").setParameter("lista", idsRequeridos).getResultList();
            }
        }
        
        respuesta.put("resultado", resultado);
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);

        return respuesta;
    }

    /**
     * Metodo que obtiene la informacion general generada por parte del ultimo
     * calculo de eligibles de un segmento
     *
     * @param idSegmento Identificador del segmento
     * @param locale
     * @return Respuesta con la informacion general obtenida
     */
    public JsonObject getInfoEligibles(String idSegmento, Locale locale) {
        JsonObjectBuilder resultado = Json.createObjectBuilder();
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "ELEGIBLES_SEGMENTO"));
            Get get = new Get(Bytes.toBytes(idSegmento));
            get.addFamily(Bytes.toBytes("DATA"));
            if (!table.exists(get)) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "recalculation_info_not_found");
            }
            Result result = table.get(get);

            resultado.add("ultimaActualizacion", Bytes.toLong(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("ULTIMA_ACTUALIZACION"))));

            resultado.add("listasProcesadas", Bytes.toLong(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("LISTAS_PROCESADAS"))));
            resultado.add("listasFallidas", Bytes.toLong(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("LISTAS_FALLIDAS"))));

            resultado.add("reglasProcesadas", Bytes.toLong(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("REGLAS_PROCESADAS"))));
            resultado.add("reglasFallidas", Bytes.toLong(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("REGLAS_FALLIDAS"))));

            resultado.add("cantidadMiembros", Bytes.toLong(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("CANTIDAD_MIEMBROS"))));

        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
        
        return resultado.build();
    }

    /**
     * Metodo que realiza una peticion de iniciar una aplicacion para el
     * recalculo de eligibles de un segmento al cluster de Apache Spark por
     * medio de su interfaz RESTful, el identificador del segmento debe existir
     * y tener un estado de activo
     *
     * @param idSegmento Identificador del segmento
     * @param locale
     * @return Respuesta con el identificador de la aplicacion creada en el
     * cluster
     */
    public String recalculateEligibles(String idSegmento, Locale locale) {
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
             throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }
        if (!segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "attributes_invalid","indEstado");
        }

        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://" + sparkConf.getProperty("masterRestHost"))
                .path("v1/submissions/create");
        Invocation.Builder request = target.request();

        JsonObjectBuilder body = Json.createObjectBuilder();
        body.add("action", "CreateSubmissionRequest");
        body.add("appArgs", Json.createArrayBuilder().add(idSegmento));
        body.add("appResource", "file:" + sparkConf.getProperty("loyaltyJarDir"));
        body.add("clientSparkVersion", "2.1.0");
        body.add("environmentVariables", Json.createObjectBuilder().add("SPARK_ENV_LOADED", "1"));
        body.add("mainClass", "com.flecharoja.loyalty.service.RecalculoSegmento");
        body.add("sparkProperties", Json.createObjectBuilder()
                .add("spark.app.name", "")
                .add("spark.submit.deployMode", "cluster")
                .add("spark.jars", "file:" + sparkConf.getProperty("loyaltyJarDir"))
                .add("spark.driver.supervise", "false")
                .add("spark.eventLog.enabled", "false")
                .add("spark.master", "spark://" + sparkConf.getProperty("masterRestHost"))
        );

        Response response;
        try {
            response = request.post(Entity.entity(body.build(), MediaType.APPLICATION_JSON));
        } catch (ResponseProcessingException e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "recalculation_failed");
        }

        JsonObject responseJson = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();

        if (responseJson.getBoolean("success")) {
            return responseJson.getString("submissionId");
        } else {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "recalculation_failed");
        }
    }

    /**
     * Metodo que realiza una peticion de obtener el estado de una aplicacion
     * por su identificador al cluster de Apache Spark por medio de su interfaz
     * RESTful
     *
     * @param idTrabajo Identificador de la aplicacion
     * @param locale
     * @return Respuesta con el estado de la aplicacion en terminos de RUNNING,
     * FINISHED, FAILED, etc
     */
    public String statusRecalculateJob(String idTrabajo, Locale locale) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://" + sparkConf.getProperty("masterRestHost"))
                .path("v1/submissions/status").path(idTrabajo);

        Response response;
        try {
            response = target.request().get();
        } catch (ProcessingException e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "recalculation_failed");
        }

        JsonObject responseJson = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();

        if (responseJson.getBoolean("success")) {
            return responseJson.getString("driverState");
        } else {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "recalculation_failed");
        }
    }

    /**
     * Metodo que realiza una peticion de cancelar una aplicacion en proceso por
     * su identificador al cluster de Apache Spark por medio de su interfaz
     * RESTful
     *
     * @param idTrabajo Identificador de la aplicacion
     * @param locale
     */
    public void cancelRecalculateJob(String idTrabajo ,Locale locale) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://" + sparkConf.getProperty("masterRestHost"))
                .path("v1/submissions/kill").path(idTrabajo);

        Response response;
        try {
            response = target.request().post(Entity.json(""));
        } catch (ResponseProcessingException e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "recalculation_failed");
        }

        JsonObject responseJson = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();

        if (!responseJson.getBoolean("success")) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "recalculation_failed");
        }
    }
}
