/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "TRABAJOS_INTERNOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TrabajosInternos.findAll", query = "SELECT t FROM TrabajosInternos t")
    , @NamedQuery(name = "TrabajosInternos.findByIdTrabajo", query = "SELECT t FROM TrabajosInternos t WHERE t.idTrabajo = :idTrabajo")
    , @NamedQuery(name = "TrabajosInternos.findByIndEstado", query = "SELECT t FROM TrabajosInternos t WHERE t.indEstado = :indEstado")
    , @NamedQuery(name = "TrabajosInternos.findByResultado", query = "SELECT t FROM TrabajosInternos t WHERE t.resultado = :resultado")})
public class TrabajosInternos implements Serializable {

    

    public enum Estados{
        PROCESANDO('P'),
        FALLIDO('F'),
        COMPLETADO('C');
        
        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();
        
        private Estados(char value) {
            this.value = value;
        }
        
        static {
            for(Estados estado : values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Estados get (Character value){
            return value==null?null:lookup.get(value);
        }    
    }
    
    public enum Tipo{
        NOTIFICACION('N'),
        EXPORTACION('E'),
        IMPORTACION('I');
        
        private final char value;
        private static final Map<Character, Tipo> lookup = new HashMap<>();
        
        private Tipo(char value) {
            this.value = value;
        }
        
        static {
            for(Tipo tipo : values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipo get (Character value){
            return value==null?null:lookup.get(value);
        }  
    }
    
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "ubicacion_uuid")
    @GenericGenerator(name = "ubicacion_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_TRABAJO")
    private String idTrabajo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Column(name = "RESULTADO")
    private String resultado;
    
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Column(name = "FECHA_ACTUALIZACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    
    @Column(name = "PORCENTAJE_COMPLETADO")
    private Double porcentajeCompletado;
    

    public TrabajosInternos() {
    }

    public TrabajosInternos(String idTrabajo) {
        this.idTrabajo = idTrabajo;
    }

    public TrabajosInternos(Character indEstado, String resultado, Character indTipo, Date fechaCreacion, Date fechaActualizacion, Double porcentajeCompletado) {
        this.indEstado = indEstado;
        this.resultado = resultado;
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.fechaActualizacion = fechaActualizacion;
        this.porcentajeCompletado = porcentajeCompletado;
    }

   

    

    public String getIdTrabajo() {
        return idTrabajo;
    }

    public void setIdTrabajo(String idTrabajo) {
        this.idTrabajo = idTrabajo;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTrabajo != null ? idTrabajo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrabajosInternos)) {
            return false;
        }
        TrabajosInternos other = (TrabajosInternos) object;
        if ((this.idTrabajo == null && other.idTrabajo != null) || (this.idTrabajo != null && !this.idTrabajo.equals(other.idTrabajo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.TrabajosInternos[ idTrabajo=" + idTrabajo + " ]";
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Double getPorcentajeCompletado() {
        return porcentajeCompletado;
    }

    public void setPorcentajeCompletado(Double porcentajeCompletado) {
        this.porcentajeCompletado = porcentajeCompletado;
    }
    
}
