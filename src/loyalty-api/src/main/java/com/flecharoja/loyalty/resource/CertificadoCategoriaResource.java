package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.CertificadoCategoria;
import com.flecharoja.loyalty.service.CertificadoCategoriaBean;
import com.flecharoja.loyalty.util.Indicadores;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "CertificadoCategoria")
@Path("categoria-producto/{idCategoriaProducto}/certificado")
public class CertificadoCategoriaResource {

    @EJB
    CertificadoCategoriaBean certificadoCategoriaBean;

    @Context
    SecurityContext context;

    @Context
    HttpServletRequest request;
    
    @PathParam("idCategoriaProducto")
    String idCategoriaProducto;

    /**
     * Método que obtiene un listado de productos
     *
     * @return lista de productos
     */
    @ApiOperation(value = "Lista de certificados para una categoria",
            responseContainer = "List",
            response = CertificadoCategoria.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idCategoriaProducto", value = "Identificador de la categoria", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<CertificadoCategoria> getCertificados(){
        try {
            context.getUserPrincipal().getName();

        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return certificadoCategoriaBean.getCertificados(idCategoriaProducto);
    }
    
    
    /**
     * Método para pagar un carrito
     *
     * @param certificadoCategoria
     * @return Estado
     */
    @ApiOperation(value = "Actualizar un certificado",
            responseContainer = "CertificadoCategoria",
            response = CertificadoCategoria.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public CertificadoCategoria updateCertificadoCategoria(@ApiParam(value = "certificadoCategoria con los nuevos valores") CertificadoCategoria certificadoCategoria) {
        try {
            context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return certificadoCategoriaBean.updateCertificadoCategoria(certificadoCategoria);
    }
    
    /**
     * Método para insertar un certificado
     *
     * @param numCertificado
     * @return certificadoCategoria
     */
    @ApiOperation(value = "Insertar un certificado",
            responseContainer = "CertificadoCategoria",
            response = CertificadoCategoria.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idCategoriaProducto", value = "Identificador de la categoria", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class)
        ,
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public CertificadoCategoria insertarCertificadoCategoria(
            @ApiParam(value = "Número de certificado") String numCertificado) {
        String usuarioContext;
        try {
            usuarioContext = context.getUserPrincipal().getName();
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        return certificadoCategoriaBean.insertarCertificadoCategoria(idCategoriaProducto, numCertificado, usuarioContext, request.getLocale());
    }
}
