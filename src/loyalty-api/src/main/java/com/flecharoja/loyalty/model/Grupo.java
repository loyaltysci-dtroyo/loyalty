
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "GRUPO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grupo.findAll", query = "SELECT g FROM Grupo g ORDER BY g.fechaModificacion DESC"),
    @NamedQuery(name = "Grupo.countAll", query = "SELECT COUNT(g.idGrupo) FROM Grupo g"),
    @NamedQuery(name = "Grupo.findByIdGrupo", query = "SELECT g FROM Grupo g WHERE g.idGrupo = :idGrupo"),
    @NamedQuery(name = "Grupo.getAvatarFromGrupo", query = "SELECT g.avatar FROM Grupo g WHERE g.idGrupo = :idGrupo")})
public class Grupo implements Serializable, Comparable<Grupo> {

    public enum Visibilidad{
        VISIBLE('V'),
        INVISIBLE('I');
        
        private final char value;
        private static final Map<Character,Visibilidad> lookup = new HashMap<>();

        private Visibilidad(char value) {
            this.value = value;
        }
        
        static {
            for(Visibilidad visibilidad : values()){
                lookup.put(visibilidad.value, visibilidad);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Visibilidad get (Character value){
            return value==null?null:lookup.get(value);
        }
        
    }

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(generator = "grupo_uuid")
    @GenericGenerator(name = "grupo_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_GRUPO")
    private String idGrupo;

   
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;

    
    @Size(min = 1, max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Basic(optional = false)
    @NotNull
    @Size(max = 300)
    @Column(name = "AVATAR")
    private String avatar;

   
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_VISIBLE")
    private Character indVisible;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @OneToMany(mappedBy = "idGrupo")
    private List<TablaPosiciones> tablaPosicionesList;
   
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grupo")
    private List<GrupoMiembro> grupoMiembroList;

    @Transient
    private BigDecimal rankingMiembros;

    public Grupo() {
    }

    public Grupo(Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public String getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Character getIndVisible() {
        return indVisible;
    }

    public void setIndVisible(Character indVisible) {
        this.indVisible = indVisible == null ? null : Character.toUpperCase(indVisible);
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public BigDecimal getRankingMiembros() {
        return rankingMiembros;
    }

    public void setRankingMiembros(BigDecimal rankingMiembros) {
        this.rankingMiembros = rankingMiembros;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<GrupoMiembro> getGrupoMiembroList() {
        return grupoMiembroList;
    }

    public void setGrupoMiembroList(List<GrupoMiembro> grupoMiembroList) {
        this.grupoMiembroList = grupoMiembroList;
    }

    @Override
    public int compareTo(Grupo grupo) {

        return this.rankingMiembros.compareTo(grupo.getRankingMiembros());

    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGrupo != null ? idGrupo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupo)) {
            return false;
        }
        Grupo other = (Grupo) object;
        if ((this.idGrupo == null && other.idGrupo != null) || (this.idGrupo != null && !this.idGrupo.equals(other.idGrupo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Grupo[ idGrupo=" + idGrupo + " ]";
    }

    
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<TablaPosiciones> getTablaPosicionesList() {
        return tablaPosicionesList;
    }

    public void setTablaPosicionesList(List<TablaPosiciones> tablaPosicionesList) {
        this.tablaPosicionesList = tablaPosicionesList;
    }

}
