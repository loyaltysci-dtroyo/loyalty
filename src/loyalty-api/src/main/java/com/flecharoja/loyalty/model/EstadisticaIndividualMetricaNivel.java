package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "ESTADISTICA_INDIVIDUAL_METRICA_NIVEL")
@NamedQueries({
    @NamedQuery(name = "EstadisticaIndividualMetricaNivel.getByIdMetrica", query = "SELECT e FROM EstadisticaIndividualMetricaNivel e WHERE e.estadisticaIndividualMetricaNivelPK.idMetrica = :idMetrica")
})
@XmlRootElement
public class EstadisticaIndividualMetricaNivel implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @XmlTransient
    @EmbeddedId
    protected EstadisticaIndividualMetricaNivelPK estadisticaIndividualMetricaNivelPK;
    
    @JoinColumn(name = "ID_NIVEL_METRICA", referencedColumnName = "ID_NIVEL", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private NivelMetrica nivelMetrica;
    
    @Column(name = "CANT_MIEMBROS")
    private Long cantMiembros;

    public EstadisticaIndividualMetricaNivel() {
    }
    
    public EstadisticaIndividualMetricaNivelPK getEstadisticaIndividualMetricaNivelPK() {
        return estadisticaIndividualMetricaNivelPK;
    }

    public void setEstadisticaIndividualMetricaNivelPK(EstadisticaIndividualMetricaNivelPK estadisticaIndividualMetricaNivelPK) {
        this.estadisticaIndividualMetricaNivelPK = estadisticaIndividualMetricaNivelPK;
    }

    public Long getCantMiembros() {
        return cantMiembros;
    }

    public void setCantMiembros(Long cantMiembros) {
        this.cantMiembros = cantMiembros;
    }

    public NivelMetrica getNivelMetrica() {
        return nivelMetrica;
    }

    public void setNivelMetrica(NivelMetrica nivelMetrica) {
        this.nivelMetrica = nivelMetrica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estadisticaIndividualMetricaNivelPK != null ? estadisticaIndividualMetricaNivelPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadisticaIndividualMetricaNivel)) {
            return false;
        }
        EstadisticaIndividualMetricaNivel other = (EstadisticaIndividualMetricaNivel) object;
        if ((this.estadisticaIndividualMetricaNivelPK == null && other.estadisticaIndividualMetricaNivelPK != null) || (this.estadisticaIndividualMetricaNivelPK != null && !this.estadisticaIndividualMetricaNivelPK.equals(other.estadisticaIndividualMetricaNivelPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.EstadisticaIndividualMetricaNivel[ estadisticaIndividualMetricaNivelPK=" + estadisticaIndividualMetricaNivelPK + " ]";
    }
    
}
