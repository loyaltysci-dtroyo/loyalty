/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "MIEMBRO_PRUEBAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MiembroPruebas.findAll", query = "SELECT m FROM MiembroPruebas m")})
  public class MiembroPruebas implements Serializable {

    @Size(max = 50)
    @Column(name = "CORREO")
    private String correo;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "miembro_pruebas_uuid")
    @GenericGenerator(name = "miembro_pruebas_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DOC_IDENTIFICACION")
    private String docIdentificacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO_MIEMBRO")
    private Character indEstadoMiembro;
    @Column(name = "FECHA_EXPIRACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaExpiracion;
    @Size(max = 200)
    @Column(name = "DIRECCION")
    private String direccion;
    @Size(max = 100)
    @Column(name = "CIUDAD_RESIDENCIA")
    private String ciudadResidencia;
    @Size(max = 100)
    @Column(name = "ESTADO_RESIDENCIA")
    private String estadoResidencia;
    @Size(max = 20)
    @Column(name = "PAIS_RESIDENCIA")
    private String paisResidencia;
    @Size(max = 10)
    @Column(name = "CODIGO_POSTAL")
    private String codigoPostal;
    @Size(max = 15)
    @Column(name = "TELEFONO_MOVIL")
    private String telefonoMovil;
    @Column(name = "FECHA_NACIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;
    @Column(name = "IND_GENERO")
    private Character indGenero;
    @Column(name = "IND_CONTACTO_EMAIL")
    private Character indContactoEmail;
    @Column(name = "IND_CONTACTO_SMS")
    private Character indContactoSms;
    @Column(name = "IND_CONTACTO_NOTIFICACION")
    private Character indContactoNotificacion;
    @Column(name = "IND_CONTACTO_ESTADO")
    private Character indContactoEstado;
    @Column(name = "IND_ESTADO_CIVIL")
    private Character indEstadoCivil;
    @Size(max = 20)
    @Column(name = "FRECUENCIA_COMPRA")
    private String frecuenciaCompra;
    @Column(name = "FECHA_SUSPENSION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSuspension;
    @Size(max = 200)
    @Column(name = "CAUSA_SUSPENSION")
    private String causaSuspension;
    @Column(name = "IND_EDUCACION")
    private Character indEducacion;
    @Column(name = "INGRESO_ECONOMICO")
    private BigInteger ingresoEconomico;
    @Column(name = "IND_HIJOS")
    private Character indHijos;
    @Size(max = 500)
    @Column(name = "COMENTARIOS")
    private String comentarios;
    @Size(max = 40)
    @Column(name = "TIPO_MIEMBRO")
    private String tipoMiembro;
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Size(max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Size(max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    @Size(max = 300)
    @Column(name = "AVATAR")
    private String avatar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "APELLIDO")
    private String apellido;
    @Size(max = 100)
    @Column(name = "APELLIDO2")
    private String apellido2;
    @Size(max = 40)
    @Column(name = "MIEMBRO_REFERENTE")
    private String miembroReferente;
    @Column(name = "IND_TIPO_DISPOSITIVO")
    private Character indTipoDispositivo;
    @Column(name = "FECHA_ULTIMA_CONEXION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaUltimaConexion;
    @Size(max = 200)
    @Column(name = "FCM_TOKEN_REGISTRO")
    private String fcmTokenRegistro;
    

    public MiembroPruebas() {
    }

    public MiembroPruebas(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public MiembroPruebas(String idMiembro, String docIdentificacion, Date fechaIngreso, Character indEstadoMiembro, String nombre, String apellido) {
        this.idMiembro = idMiembro;
        this.docIdentificacion = docIdentificacion;
        this.fechaIngreso = fechaIngreso;
        this.indEstadoMiembro = indEstadoMiembro;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getDocIdentificacion() {
        return docIdentificacion;
    }

    public void setDocIdentificacion(String docIdentificacion) {
        this.docIdentificacion = docIdentificacion;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Character getIndEstadoMiembro() {
        return indEstadoMiembro;
    }

    public void setIndEstadoMiembro(Character indEstadoMiembro) {
        this.indEstadoMiembro = indEstadoMiembro;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudadResidencia() {
        return ciudadResidencia;
    }

    public void setCiudadResidencia(String ciudadResidencia) {
        this.ciudadResidencia = ciudadResidencia;
    }

    public String getEstadoResidencia() {
        return estadoResidencia;
    }

    public void setEstadoResidencia(String estadoResidencia) {
        this.estadoResidencia = estadoResidencia;
    }

    public String getPaisResidencia() {
        return paisResidencia;
    }

    public void setPaisResidencia(String paisResidencia) {
        this.paisResidencia = paisResidencia;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Character getIndGenero() {
        return indGenero;
    }

    public void setIndGenero(Character indGenero) {
        this.indGenero = indGenero;
    }

    public Character getIndContactoEmail() {
        return indContactoEmail;
    }

    public void setIndContactoEmail(Character indContactoEmail) {
        this.indContactoEmail = indContactoEmail;
    }

    public Character getIndContactoSms() {
        return indContactoSms;
    }

    public void setIndContactoSms(Character indContactoSms) {
        this.indContactoSms = indContactoSms;
    }

    public Character getIndContactoNotificacion() {
        return indContactoNotificacion;
    }

    public void setIndContactoNotificacion(Character indContactoNotificacion) {
        this.indContactoNotificacion = indContactoNotificacion;
    }

    public Character getIndContactoEstado() {
        return indContactoEstado;
    }

    public void setIndContactoEstado(Character indContactoEstado) {
        this.indContactoEstado = indContactoEstado;
    }

    public Character getIndEstadoCivil() {
        return indEstadoCivil;
    }

    public void setIndEstadoCivil(Character indEstadoCivil) {
        this.indEstadoCivil = indEstadoCivil;
    }

    public String getFrecuenciaCompra() {
        return frecuenciaCompra;
    }

    public void setFrecuenciaCompra(String frecuenciaCompra) {
        this.frecuenciaCompra = frecuenciaCompra;
    }

    public Date getFechaSuspension() {
        return fechaSuspension;
    }

    public void setFechaSuspension(Date fechaSuspension) {
        this.fechaSuspension = fechaSuspension;
    }

    public String getCausaSuspension() {
        return causaSuspension;
    }

    public void setCausaSuspension(String causaSuspension) {
        this.causaSuspension = causaSuspension;
    }

    public Character getIndEducacion() {
        return indEducacion;
    }

    public void setIndEducacion(Character indEducacion) {
        this.indEducacion = indEducacion;
    }

    public BigInteger getIngresoEconomico() {
        return ingresoEconomico;
    }

    public void setIngresoEconomico(BigInteger ingresoEconomico) {
        this.ingresoEconomico = ingresoEconomico;
    }

    public Character getIndHijos() {
        return indHijos;
    }

    public void setIndHijos(Character indHijos) {
        this.indHijos = indHijos;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getTipoMiembro() {
        return tipoMiembro;
    }

    public void setTipoMiembro(String tipoMiembro) {
        this.tipoMiembro = tipoMiembro;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getMiembroReferente() {
        return miembroReferente;
    }

    public void setMiembroReferente(String miembroReferente) {
        this.miembroReferente = miembroReferente;
    }

    public Character getIndTipoDispositivo() {
        return indTipoDispositivo;
    }

    public void setIndTipoDispositivo(Character indTipoDispositivo) {
        this.indTipoDispositivo = indTipoDispositivo;
    }

    public Date getFechaUltimaConexion() {
        return fechaUltimaConexion;
    }

    public void setFechaUltimaConexion(Date fechaUltimaConexion) {
        this.fechaUltimaConexion = fechaUltimaConexion;
    }

    public String getFcmTokenRegistro() {
        return fcmTokenRegistro;
    }

    public void setFcmTokenRegistro(String fcmTokenRegistro) {
        this.fcmTokenRegistro = fcmTokenRegistro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiembroPruebas)) {
            return false;
        }
        MiembroPruebas other = (MiembroPruebas) object;
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MiembroPruebas[ idMiembro=" + idMiembro + " ]";
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
}
