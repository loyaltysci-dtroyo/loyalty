package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.MisionEncuestaPregunta;
import com.flecharoja.loyalty.service.MisionEncuestaPreguntaBean;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase de recursos http para el acceso a funcionalidades del manejo de
 * misiones del tipo encuestas
 *
 * @author svargas
 */
@Api(value = "Mision")
@Path("mision/{idMision}/encuesta")
public class MisionEncuestaResource {

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @EJB
    MisionEncuestaPreguntaBean encuestaPreguntaBean;

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión
    
    @Context
    HttpServletRequest request;

    /**
     * Identificador de la mision
     */
    @PathParam("idMision")
    String idMision;

    /**
     * Metodo que obtiene un listado de todas las preguntas de una encuesta en
     * una mision especifica segun su tipo, en el caso de error se retorna una
     * respuesta con estado erroneo con un encabezado "Error-Reason" con un
     * valor numerico indicando la naturaleza del error.
     *
     * @return Respuesta con el listado de preguntas de la encuesta de la mision
     * encontradas
     */
    @ApiOperation(value = "Obtener preguntas de la encuesta de la mision",
            responseContainer = "List",
            response = MisionEncuestaPregunta.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List getPreguntasEncuestaMision() {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return encuestaPreguntaBean.getPreguntasEncuestaMision(idMision,request.getLocale());
    }

    /**
     * Metodo que obtiene una pregunta de una encuesta en una mision especifica
     * segun su tipo, en el caso de error se retorna una respuesta con estado
     * erroneo con un encabezado "Error-Reason" con un valor numerico indicando
     * la naturaleza del error.
     *
     * @param idPregunta Identificador de la pregunta
     * @return Respuesta con el listado de preguntas de la encuesta de la mision
     * encontradas
     */
    @ApiOperation(value = "Obtener pregunta de la encuesta de la mision por identificadores",
            response = MisionEncuestaPregunta.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idPregunta}")
    @Produces(MediaType.APPLICATION_JSON)
    public MisionEncuestaPregunta getPreguntaEncuestaMisionPorId(
            @ApiParam(value = "Identificador de pregunta", required = true)
            @PathParam("idPregunta") String idPregunta) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return encuestaPreguntaBean.getPreguntaEncuestaMisionPorId(idMision, idPregunta,request.getLocale());
    }

    /**
     * Metodo que crea una pregunta de una encuesta en una mision existente, en
     * el caso de error se retorna una respuesta con estado erroneo con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param pregunta Informacion de pregunta
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Crear pregunta de encuesta de la mision",
            response = String.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertPreguntaEncuestaMision(
            @ApiParam(value = "Informacion de pregunta", required = true) MisionEncuestaPregunta pregunta) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return encuestaPreguntaBean.insertPreguntaEncuestaMision(idMision, pregunta, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que edita una pregunta de una encuesta en una mision existente, en
     * el caso de error se retorna una respuesta con estado erroneo con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param pregunta Informacion de pregunta
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Editar pregunta de encuesta de la mision")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void editPreguntaEncuestaMision(
            @ApiParam(value = "Informacion de pregunta", required = true) MisionEncuestaPregunta pregunta) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        encuestaPreguntaBean.editPreguntaEncuestaMision(idMision, pregunta, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que elimina una pregunta de una encuesta en una mision existente,
     * en el caso de error se retorna una respuesta con estado erroneo con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param idPregunta Identificador de pregunta
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Remover pregunta de encuesta de la mision")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idPregunta}")
    public void removePreguntaEncuestaMision(
            @ApiParam(value = "Identificador de pregunta", required = true)
            @PathParam("idPregunta") String idPregunta) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        encuestaPreguntaBean.removePreguntaEncuestaMision(idMision, idPregunta, usuarioContext,request.getLocale());
    }

}
