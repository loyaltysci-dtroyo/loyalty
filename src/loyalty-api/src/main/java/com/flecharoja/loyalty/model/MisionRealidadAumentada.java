/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "MISION_REALIDAD_AUMENTADA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MisionRealidadAumentada.findAll", query = "SELECT m FROM MisionRealidadAumentada m")
    , @NamedQuery(name = "MisionRealidadAumentada.findByIdMision", query = "SELECT m FROM MisionRealidadAumentada m WHERE m.idMision = :idMision")
    , @NamedQuery(name = "MisionRealidadAumentada.findByIdBlippar", query = "SELECT m FROM MisionRealidadAumentada m WHERE m.idBlippar = :idBlippar")
    , @NamedQuery(name = "MisionRealidadAumentada.findByFechaCreacion", query = "SELECT m FROM MisionRealidadAumentada m WHERE m.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "MisionRealidadAumentada.findByFechaModificacion", query = "SELECT m FROM MisionRealidadAumentada m WHERE m.fechaModificacion = :fechaModificacion")
    , @NamedQuery(name = "MisionRealidadAumentada.findByUsuarioCreacion", query = "SELECT m FROM MisionRealidadAumentada m WHERE m.usuarioCreacion = :usuarioCreacion")
    , @NamedQuery(name = "MisionRealidadAumentada.findByUsuarioModificacion", query = "SELECT m FROM MisionRealidadAumentada m WHERE m.usuarioModificacion = :usuarioModificacion")
    , @NamedQuery(name = "MisionRealidadAumentada.findByNumVersion", query = "SELECT m FROM MisionRealidadAumentada m WHERE m.numVersion = :numVersion")})
public class MisionRealidadAumentada implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_MISION")
    private String idMision;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_BLIPPAR")
    private int idBlippar;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @Size(max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private long numVersion;
    
    
    @JoinColumn(name = "ID_MISION", referencedColumnName = "ID_MISION", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Mision mision;

    public MisionRealidadAumentada() {
    }

    public MisionRealidadAumentada(String idMision) {
        this.idMision = idMision;
    }

    public MisionRealidadAumentada(String idMision, int idBlippar, Date fechaCreacion, String usuarioCreacion, long numVersion) {
        this.idMision = idMision;
        this.idBlippar = idBlippar;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.numVersion = numVersion;
    }
    @XmlTransient
    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    public int getIdBlippar() {
        return idBlippar;
    }

    public void setIdBlippar(int idBlippar) {
        this.idBlippar = idBlippar;
    }
    
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
  
    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
   
    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }
   
    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }
    
    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }
   
    public long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(long numVersion) {
        this.numVersion = numVersion;
    }
    @XmlTransient
    public Mision getMision() {
        return mision;
    }

    public void setMision(Mision mision) {
        this.mision = mision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMision != null ? idMision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionRealidadAumentada)) {
            return false;
        }
        MisionRealidadAumentada other = (MisionRealidadAumentada) object;
        if ((this.idMision == null && other.idMision != null) || (this.idMision != null && !this.idMision.equals(other.idMision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionRealidadAumentada[ idMision=" + idMision + " ]";
    }
    
}
