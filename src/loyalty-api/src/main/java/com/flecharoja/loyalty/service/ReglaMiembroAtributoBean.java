package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.AtributoDinamico;
import com.flecharoja.loyalty.model.ListaReglas;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.NivelMetrica;
import com.flecharoja.loyalty.model.ReglaMiembroAtb;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.flecharoja.loyalty.util.ReglasSegmento;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * reglas de cambio de metrica
 *
 * @author svargas
 */
@Stateless
public class ReglaMiembroAtributoBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    @EJB
    ElegibilidadMiembroBean elegibilidadBean;

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * @param idLista Identificador de la lista de reglas
     * @param locale
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List getReglasMiembroAtributoLista(String idLista, Locale locale) {
        try {
            em.getReference(ListaReglas.class, idLista).getIdLista();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idLista");
        }

        return em.createNamedQuery("ReglaMiembroAtb.findByIdLista").setParameter("idLista", idLista).getResultList();
    }

    /**
     * Metodo que crea una nueva regla en una lista especifica
     *
     * @param idSegmento Identificador de segmento
     * @param idLista Identificador de la lista de reglas
     * @param reglaMiembroAtb Objeto con la informacion de la regla
     * @param usuario usuario en sesion
     * @param locale
     * @return Respuesta con el identificador de la regla creada
     */
    public String createReglaMiembroAtributo(String idSegmento, String idLista, ReglaMiembroAtb reglaMiembroAtb, String usuario, Locale locale) {
        if (reglaMiembroAtb == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "ReglaMiembroAtb");
        }

        Date date = Calendar.getInstance().getTime();
        reglaMiembroAtb.setFechaCreacion(date);

        reglaMiembroAtb.setUsuarioCreacion(usuario);

        ListaReglas listaReglas = em.find(ListaReglas.class, idLista);
        if (listaReglas == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        if (!listaReglas.getIdSegmento().getIdSegmento().equals(segmento.getIdSegmento())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "segment_id_invalid");
        }
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        reglaMiembroAtb.setIdLista(idLista);

        //verificacion de algunos atributos requeridos
        ReglaMiembroAtb.Atributos atributo = ReglaMiembroAtb.Atributos.get(reglaMiembroAtb.getIndAtributo());
        ReglasSegmento.ValoresIndOperador operador = ReglasSegmento.ValoresIndOperador.get(reglaMiembroAtb.getIndOperador());
        if (atributo == null || operador == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indAtributo, indOperador");
        }

        boolean checkValorComparacion = !(operador == ReglasSegmento.ValoresIndOperador.ESTA_VACIO || operador == ReglasSegmento.ValoresIndOperador.NO_ESTA_VACIO);

        //todo verificar que si el operador es del tipo es nulo, algo asi, no compruebe el valor de comparacion
        //casos especiales de atributos
        switch (atributo) {
            case FECHA_NACIMIENTO: {
                if (checkValorComparacion) {
                    try {
                        Indicadores.ANGULAR_DATE_STRING_FORMAT.parse(reglaMiembroAtb.getValorComparacion());
                    } catch (ParseException | NullPointerException ex) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                    }
                }
                break;
            }
            case ATRIBUTO_DINAMICO: {
                if (reglaMiembroAtb.getAtributoDinamico() == null || reglaMiembroAtb.getAtributoDinamico().getIdAtributo() == null || reglaMiembroAtb.getValorComparacion() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "atributoDinamico, valorComparacion");
                } else {
                    AtributoDinamico atributoDinamico = em.find(AtributoDinamico.class, reglaMiembroAtb.getAtributoDinamico().getIdAtributo());
                    if (atributoDinamico == null) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "atributoDinamico");
                    }
                    reglaMiembroAtb.setAtributoDinamico(atributoDinamico);

                    switch (AtributoDinamico.TiposDato.get(atributoDinamico.getIndTipoDato())) {
                        case BOOLEANO: {
                            if (!(reglaMiembroAtb.getValorComparacion().equalsIgnoreCase("TRUE") || reglaMiembroAtb.getValorComparacion().equalsIgnoreCase("FALSE"))) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                            }
                            break;
                        }
                        case FECHA: {
                            try {
                                Indicadores.ANGULAR_DATE_STRING_FORMAT.parse(reglaMiembroAtb.getValorComparacion());
                            } catch (ParseException ex) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                            }
                            break;
                        }
                        case NUMERICO: {
                            try {
                                new BigDecimal(reglaMiembroAtb.getValorComparacion());
                            } catch (NumberFormatException e) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                            }
                            break;
                        }
                    }
                }
                break;
            }
            case IND_EDUCACION: {
                if (checkValorComparacion && (reglaMiembroAtb.getValorComparacion().length() > 1 || Miembro.ValoresIndEducacion.get(reglaMiembroAtb.getValorComparacion().charAt(0)) == null)) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                }
                break;
            }
            case IND_ESTADO_MIEMBRO: {
                if (reglaMiembroAtb.getValorComparacion() == null || reglaMiembroAtb.getValorComparacion().length() > 1 || Miembro.Estados.get(reglaMiembroAtb.getValorComparacion().charAt(0)) == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                }
                break;
            }
            case IND_ESTADO_CIVIL: {
                if (checkValorComparacion && (reglaMiembroAtb.getValorComparacion().length() > 1 || Miembro.ValoresIndEstadoCivil.get(reglaMiembroAtb.getValorComparacion().charAt(0)) == null)) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                }
                break;
            }
            case IND_GENERO: {
                if (checkValorComparacion && (reglaMiembroAtb.getValorComparacion().length() > 1 || Miembro.ValoresIndGenero.get(reglaMiembroAtb.getValorComparacion().charAt(0)) == null)) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                }
                break;
            }
            case IND_MIEMBRO_SISTEMA:
            case IND_CONTACTO_SMS:
            case IND_CONTACTO_NOTIFICACION:
            case IND_CONTACTO_ESTADO:
            case IND_CONTACTO_EMAIL:
            case IND_HIJOS: {
                if (checkValorComparacion && !(reglaMiembroAtb.getValorComparacion().equals("1") || reglaMiembroAtb.getValorComparacion().equals("0"))) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                }
                break;
            }
            case PAIS_RESIDENCIA: {
                if (checkValorComparacion) {
                    long count = (long) em.createNamedQuery("Pais.countByAlfa3").setParameter("alfa3", reglaMiembroAtb.getValorComparacion()).getSingleResult();
                    if (count != 1) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                    }
                }
                break;
            }
            case TIER_INICIAL:
            case TIER_PROGRESO: {
                if (reglaMiembroAtb.getMetrica() == null || reglaMiembroAtb.getMetrica().getIdMetrica() == null || reglaMiembroAtb.getNivelMetrica() == null || reglaMiembroAtb.getNivelMetrica().getIdNivel() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "metrica, nivelMetrica");
                } else {
                    Metrica metrica = em.find(Metrica.class, reglaMiembroAtb.getMetrica().getIdMetrica());
                    NivelMetrica nivelMetrica = em.find(NivelMetrica.class, reglaMiembroAtb.getNivelMetrica().getIdNivel());

                    if (!metrica.getGrupoNiveles().getNivelMetricaList().contains(nivelMetrica)) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "nivelMetrica");
                    }

                    reglaMiembroAtb.setMetrica(metrica);
                    reglaMiembroAtb.setNivelMetrica(nivelMetrica);
                }
                break;
            }
            case INGRESO_ECONOMICO: {
                if (checkValorComparacion) {
                    try {
                        Integer.parseInt(reglaMiembroAtb.getValorComparacion());
                    } catch (NumberFormatException e) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                    }
                }
                break;
            }
            case NOMBRE:
            case APELLIDO: {
                if (!checkValorComparacion) {
                    //el operador debe ser sobre el valor de comparacion null y son atributos requeridos
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                }
                if (reglaMiembroAtb.getValorComparacion() == null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                }
                break;
            }
        }

        try {
            //almacenamiento de la regla
            em.persist(reglaMiembroAtb);
            em.flush();
            bitacoraBean.logAccion(ReglaMiembroAtb.class, reglaMiembroAtb.getIdRegla(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
            //actualizacion de la lista de reglas
            listaReglas.setUsuarioModificacion(usuario);
            listaReglas.setFechaModificacion(date);
            em.merge(listaReglas);
            bitacoraBean.logAccion(ReglaMiembroAtb.class, listaReglas.getIdLista(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (ConstraintViolationException e) {//atributos no validos
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestRecalculoRegla(reglaMiembroAtb.getIdRegla(), ReglasSegmento.TiposReglasSegmento.ATRIBUTO_MIEMBRO);
            } catch (Exception ex) {
                Logger.getLogger(ReglaMiembroAtributoBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }

        return reglaMiembroAtb.getIdRegla();
    }

    /**
     * Metodo que elimina de una lista especifica una regla
     *
     * @param idSegmento Identificador de segmento
     * @param idLista Identificador de la lista de reglas
     * @param idRegla Identificador de la regla
     * @param usuario usuario en sesión
     * @param locale
     */
    public void deleteReglaMiembroAtributo(String idSegmento, String idLista, String idRegla, String usuario, Locale locale) {
        Date date = Calendar.getInstance().getTime();

        ListaReglas listaReglas = em.find(ListaReglas.class, idLista);
        if (listaReglas == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }
        if (!listaReglas.getIdSegmento().getIdSegmento().equals(segmento.getIdSegmento())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "segment_id_invalid");
        }
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        try {
            //eliminacion de la regla (si es encontrada)
            em.remove(em.getReference(ReglaMiembroAtb.class, idRegla));

            em.flush();

            elegibilidadBean.deleteResultadoElegibilidadRegla(ReglasSegmento.TiposReglasSegmento.ATRIBUTO_MIEMBRO, idRegla, Locale.getDefault());

            bitacoraBean.logAccion(ReglaMiembroAtb.class, idRegla, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
            //actualizacion de la lista de reglas
            listaReglas.setUsuarioModificacion(usuario);
            listaReglas.setFechaModificacion(date);
            em.merge(listaReglas);
            bitacoraBean.logAccion(ReglaMiembroAtb.class, listaReglas.getIdLista(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (EntityNotFoundException e) {//lista de reglas o regla no encontrada
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "metric_rule_atb_not_found");
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestActualizacionSegmento(idSegmento);
            } catch (Exception ex) {
                Logger.getLogger(ReglaMiembroAtributoBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }
}
