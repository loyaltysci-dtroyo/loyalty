/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.DataImportacionMiembroExtendido;
import com.flecharoja.loyalty.service.ImportarExportarTrabajoBean;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "Importaciones")
@Path("importar")
public class ImportResource {

    @EJB
    ImportarExportarTrabajoBean trabajoBean;

    @Context
    HttpServletRequest request;

    @Context
    SecurityContext context;

    @EJB
    MyKeycloakAuthz authzBean;

    /**
     * Método que llama un trabajo el cual realiza la importacion de miembros a
     * un segmento estatico
     *
     * @param idSegmento identificador del segmento
     * @param atributo doc. de identificacion o id del miembro
     * @param accion incluir o excluir
     * @param base64 archivo con la lista de miembros
     * @return id del trabajo
     */
    @ApiOperation(value = "Importación de miembros a un segmento estático",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("miembro/segmento/{idSegmento}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String importMiembrosSegmentos(
            @ApiParam(value = "Identificador del segmento", required = true) @PathParam("idSegmento") String idSegmento,
            @ApiParam(value = "Indicador del tipo de atributo que viene como llave (doc de identificación o id del miembro)") @QueryParam("atributo-clave") String atributo,
            @ApiParam(value = "Indicador del tipo de acción que se le da a los miembros (excluir/incluir)") @QueryParam("accion-miembro") String accion,
            @ApiParam(value = "Archivo con la lista de miembros") String base64) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_ESCRITURA, token, request.getLocale()) && !authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return trabajoBean.importarMiembroSegmento(base64, idSegmento, atributo, accion, usuario, request.getLocale());
    }

    /**
     * Método que llama a un trabajo el cual realiza la importacion de nuevos
     * miembros al sistema
     *
     * @param base64 archivo con los miembros
     * @return id del trabajo
     */
    @ApiOperation(value = "Importación de miembros",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("miembro")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String importMiembros(
            @ApiParam(value = "Archivo con la información de los miembros") String base64) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return trabajoBean.importarMiembro(base64, usuario, request.getLocale());
    }
    
    /**
     * Metodo para la importacion de miembros con la definicion especifica de
     * atributos regulares + atributos dinamicos
     * 
     * @param data Datos del archivo csv a importar y definicion de atributos
     * numero de columna
     * @param doPostInsertActions Indicador sobre si se realiza acciones post
     * registro (bonus de bienvenida, email de bienvenida con credenciales, etc)
     * @return Identificador del trabajo interno asignado a la tarea de importacion
     */
    @ApiOperation(value = "Importación de miembros de forma extendida con atributos dinamicos",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("miembro/extendido")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String importMiembrosExtendido(
            @ApiParam(value = "Datos con la informacion de la importacion de miembros") DataImportacionMiembroExtendido data,
            @ApiParam(value = "Indicador sobre realizacion de acciones post registro (bonus de bienvenida, email de bienvenida, etc)") @QueryParam(value = "do-post-insert-actions") @DefaultValue(value = "false") boolean doPostInsertActions) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return trabajoBean.importarMiembro(data, usuario, request.getLocale(), doPostInsertActions);
    }
    
    /**
     * Método que llama a un trabajo el cual realiza la importacion de nuevos
     * miembros al sistema
     *
     * @param tipoIdentificacion
     * @param atributo
     * @param tipoAtributo
     * @param base64 archivo con los miembros
     * @return id del trabajo
     */
    @ApiOperation(value = "Importacion de informacion de atributos para actualizar en miembros",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Path("miembro/actualizar")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String importInfoMiembro(
            @QueryParam("identificacion") String tipoIdentificacion,
            @QueryParam("atributo") String atributo,
            @QueryParam("tipo-atributo") String tipoAtributo,
            String base64) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token, request.getLocale()) && !authzBean.tienePermiso(IndicadoresRecursos.ATRIBUTOS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
         return trabajoBean.importarInfoMiembro(base64, atributo, tipoAtributo,tipoIdentificacion, usuario, request.getLocale());
    }

    /**
     * Método que llama un trabajo el cual realiza la importacion de codigos de
     * certificados para un premio
     *
     * @param idPremio identificador del premio
     * @param base64 archivo con los codigos de los certificados
     * @return id del trabajo
     */
    @ApiOperation(value = "Importación de codigos de certificados para un premio",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("certificados")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String importCertificados(
            @ApiParam(value = "Identificador del premio al cual se importar los codigos") @QueryParam("premio") String idPremio,
            @ApiParam(value = "Archivo con la lista de códigos a importar") String base64) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return trabajoBean.importarCertificados(base64, idPremio, usuario, request.getLocale());
    }
    
    /**
     * Método que llama un trabajo el cual realiza la importacion de numCertificados de
     * certificados para una categoria de producto
     *
     * @param idCategoriaProducto identificador de la categoria
     * @param base64 archivo con los codigos de los certificados
     * @return id del trabajo
     */
    @ApiOperation(value = "Importación de codigos de certificados para una categoria",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("certificadosCategoriaProducto")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String importCertificadosCategoriaProducto(
            @ApiParam(value = "Identificador del premio al cual se importar los codigos") @QueryParam("categoria") String idCategoriaProducto,
            @ApiParam(value = "Archivo con la lista de códigos a importar") String base64) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return trabajoBean.importarCertificadosCategoriaProducto(base64, idCategoriaProducto, usuario, request.getLocale());
    }
}
