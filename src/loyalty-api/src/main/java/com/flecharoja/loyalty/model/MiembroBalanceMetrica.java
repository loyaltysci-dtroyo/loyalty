package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MIEMBRO_BALANCE_METRICA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MiembroBalanceMetrica.findByIdMiembro", query = "SELECT m FROM MiembroBalanceMetrica m WHERE m.miembroBalanceMetricaPK.idMiembro = :idMiembro")
    , @NamedQuery(name = "MiembroBalanceMetrica.findByIdMetrica", query = "SELECT m FROM MiembroBalanceMetrica m WHERE m.miembroBalanceMetricaPK.idMetrica = :idMetrica")
})
public class MiembroBalanceMetrica implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MiembroBalanceMetricaPK miembroBalanceMetricaPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "PROGRESO_ACTUAL")
    private Double progresoActual;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_ACUMULADO")
    private Double totalAcumulado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "DISPONIBLE")
    private Double disponible;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "VENCIDO")
    private Double vencido;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "REDIMIDO")
    private Double redimido;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Metrica metrica;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;

    public MiembroBalanceMetrica() {
    }
    
    public MiembroBalanceMetrica(String idMiembro, String idMetrica) {
        this.miembroBalanceMetricaPK = new MiembroBalanceMetricaPK(idMiembro, idMetrica);
        this.progresoActual = Double.valueOf(0);
        this.totalAcumulado = Double.valueOf(0);
        this.disponible = Double.valueOf(0);
        this.vencido = Double.valueOf(0);
        this.redimido = Double.valueOf(0);
        this.numVersion = 1L;
    }

    @XmlTransient
    @ApiModelProperty(hidden = true)
    public MiembroBalanceMetricaPK getMiembroBalanceMetricaPK() {
        return miembroBalanceMetricaPK;
    }

    public void setMiembroBalanceMetricaPK(MiembroBalanceMetricaPK miembroBalanceMetricaPK) {
        this.miembroBalanceMetricaPK = miembroBalanceMetricaPK;
    }

    public Double getProgresoActual() {
        return progresoActual;
    }

    public void setProgresoActual(Double progresoActual) {
        this.progresoActual = progresoActual;
    }

    public Double getTotalAcumulado() {
        return totalAcumulado;
    }

    public void setTotalAcumulado(Double totalAcumulado) {
        this.totalAcumulado = totalAcumulado;
    }

    public Double getDisponible() {
        return disponible;
    }

    public void setDisponible(Double disponible) {
        this.disponible = disponible;
    }

    public Double getVencido() {
        return vencido;
    }

    public void setVencido(Double vencido) {
        this.vencido = vencido;
    }

    public Double getRedimido() {
        return redimido;
    }

    public void setRedimido(Double redimido) {
        this.redimido = redimido;
    }

    @XmlTransient
    @ApiModelProperty(hidden = true)
    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }

    @XmlTransient
    @ApiModelProperty(hidden = true)
    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (miembroBalanceMetricaPK != null ? miembroBalanceMetricaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiembroBalanceMetrica)) {
            return false;
        }
        MiembroBalanceMetrica other = (MiembroBalanceMetrica) object;
        if ((this.miembroBalanceMetricaPK == null && other.miembroBalanceMetricaPK != null) || (this.miembroBalanceMetricaPK != null && !this.miembroBalanceMetricaPK.equals(other.miembroBalanceMetricaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MiembroBalanceMetrica[ miembroBalanceMetricaPK=" + miembroBalanceMetricaPK + " ]";
    }
    
}
