package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "LISTA_REGLAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ListaReglas.countByIdSegmento", query = "SELECT COUNT(l.idSegmento) FROM ListaReglas l WHERE l.idSegmento.idSegmento = :idSegmento"),
    @NamedQuery(name = "ListaReglas.findByIdLista", query = "SELECT l FROM ListaReglas l WHERE l.idLista = :idLista"),
    @NamedQuery(name = "ListaReglas.findByIdSegmento", query = "SELECT l FROM ListaReglas l WHERE l.idSegmento.idSegmento = :idSegmento ORDER BY l.orden")})
public class ListaReglas implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "listareglas_uuid")
    @GenericGenerator(name = "listareglas_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_LISTA")
    private String idLista;
    
    @Basic(optional = false)
    @NotNull
    @Size(min=1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_OPERADOR")
    private Character indOperador;
    
    @Column(name = "ORDEN")
    private Long orden;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @JoinColumn(name = "ID_SEGMENTO", referencedColumnName = "ID_SEGMENTO")
    @ManyToOne(optional = false)
    private Segmento idSegmento;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLista")
    private List<ReglaMiembroAtb> reglasMiembroAtb;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLista")
    private List<ReglaMetricaCambio> reglasMetricaCambio;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLista")
    private List<ReglaMetricaBalance> reglasMetricaBalance;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLista")
    private List<ReglaAvanzada> reglasAvanzada;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLista")
    private List<ReglaEncuesta> reglasEncuesta;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLista")
    private List<ReglaPreferencia> reglasPreferencia;
    
    public ListaReglas() {
    }

    public String getIdLista() {
        return idLista;
    }

    public void setIdLista(String idLista) {
        this.idLista = idLista;
    }

    public Character getIndOperador() {
        return indOperador;
    }

    public void setIndOperador(Character indOperador) {
        this.indOperador = indOperador!=null?Character.toUpperCase(indOperador):null;
    }

    public Long getOrden() {
        return orden;
    }

    public void setOrden(Long orden) {
        this.orden = orden;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Segmento getIdSegmento() {
        return idSegmento;
    }
    
    public void setIdSegmento(Segmento idSegmento) {
        this.idSegmento = idSegmento;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ReglaMiembroAtb> getReglasMiembroAtb() {
        return reglasMiembroAtb;
    }

    public void setReglasMiembroAtb(List<ReglaMiembroAtb> reglasMiembroAtb) {
        this.reglasMiembroAtb = reglasMiembroAtb;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ReglaMetricaCambio> getReglasMetricaCambio() {
        return reglasMetricaCambio;
    }

    public void setReglasMetricaCambio(List<ReglaMetricaCambio> reglasMetricaCambio) {
        this.reglasMetricaCambio = reglasMetricaCambio;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ReglaMetricaBalance> getReglasMetricaBalance() {
        return reglasMetricaBalance;
    }

    public void setReglasMetricaBalance(List<ReglaMetricaBalance> reglasMetricaBalance) {
        this.reglasMetricaBalance = reglasMetricaBalance;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ReglaAvanzada> getReglasAvanzada() {
        return reglasAvanzada;
    }

    public void setReglasAvanzada(List<ReglaAvanzada> reglasAvanzada) {
        this.reglasAvanzada = reglasAvanzada;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ReglaEncuesta> getReglasEncuesta() {
        return reglasEncuesta;
    }

    public void setReglasEncuesta(List<ReglaEncuesta> reglasEncuesta) {
        this.reglasEncuesta = reglasEncuesta;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ReglaPreferencia> getReglasPreferencia() {
        return reglasPreferencia;
    }

    public void setReglasPreferencia(List<ReglaPreferencia> reglasPreferencia) {
        this.reglasPreferencia = reglasPreferencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLista != null ? idLista.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ListaReglas)) {
            return false;
        }
        ListaReglas other = (ListaReglas) object;
        if ((this.idLista == null && other.idLista != null) || (this.idLista != null && !this.idLista.equals(other.idLista))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ListaReglas[ idLista=" + idLista + " ]";
    }
    
}
