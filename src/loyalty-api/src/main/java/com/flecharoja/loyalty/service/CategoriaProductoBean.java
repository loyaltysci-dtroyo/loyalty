/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;;
import com.flecharoja.loyalty.model.CategoriaProducto;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de proveer metodos para el mantenimiento de catagorias de
 * productos asi como para la verificacion y manejo de datos
 *
 * @author wtencio
 */
@Stateless
public class CategoriaProductoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    private BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de bitacora

    /**
     * Método que almacena la informacion de una nueva categoria de productos
     *
     * @param categoria informacion de la categoria
     * @param usuario usuario en sesion
     * @param locale
     * @return identificador de la categoria
     */
    public String insertCategoriaProducto(CategoriaProducto categoria, String usuario, Locale locale) {
        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (categoria.getImagen() == null || categoria.getImagen().trim().isEmpty()) {
            categoria.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                categoria.setImagen(filesUtils.uploadImage(categoria.getImagen(), MyAwsS3Utils.Folder.ARTE_CATEGORIA_PRODUCTO, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }

        categoria.setFechaCreacion(Calendar.getInstance().getTime());
        categoria.setNombreInterno(categoria.getNombreInterno().toUpperCase());
        categoria.setUsuarioCreacion(usuario);
        categoria.setFechaModificacion(Calendar.getInstance().getTime());
        categoria.setUsuarioModificacion(usuario);
        categoria.setNumVersion(new Long(1));

        try {
            em.persist(categoria);
            em.flush();
        } catch (ConstraintViolationException e) {
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (!categoria.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(categoria.getImagen());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        bitacoraBean.logAccion(CategoriaProducto.class, categoria.getIdCategoria(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        return categoria.getIdCategoria();
    }

    /**
     * Método que actualiza la informacion de una categoria de producto
     *
     * @param categoria informacion de la categoria de producto
     * @param usuario usuario en sesion
     * @param locale
     */
    public void editCategoriaProducto(CategoriaProducto categoria, String usuario, Locale locale) {

        //verificacion que la entidad exista
        CategoriaProducto original = em.find(CategoriaProducto.class, categoria.getIdCategoria());

        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_category_not_found");
        }

        String urlImagenOriginal = original.getImagen();//url imagen original
        String urlImagenNueva = null;//url imagen nueva
        //si el atributo de imagen viene nula (no deberia)
        if (categoria.getImagen() == null || categoria.getImagen().trim().isEmpty()) {
            //establecimiento de la imagen por defecto
            urlImagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            categoria.setImagen(urlImagenNueva);
        } else //ver si el valor en el atributo de imagen, difiere del almacenado
        {
            if (!urlImagenOriginal.equals(categoria.getImagen())) {//si la imagen es diferente a la almacenada
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    urlImagenNueva = filesUtils.uploadImage(categoria.getImagen(), MyAwsS3Utils.Folder.ARTE_CATEGORIA_PRODUCTO, null);
                    categoria.setImagen(urlImagenNueva);
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }

        //establecimiento de atributos por defecto
        categoria.setUsuarioModificacion(usuario);
        categoria.setFechaModificacion(Calendar.getInstance().getTime());

        try {
            //se modifica la entidad y se registra la accion en bitacora
            em.merge(categoria);
            em.flush();//se manda a guardar a la bases de datos para la verificacion en el momento de la entidad, etc
        } catch (ConstraintViolationException | OptimisticLockException e) {//en el caso que la informacion no sea valida
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

        bitacoraBean.logAccion(CategoriaProducto.class, categoria.getIdCategoria(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);

        if (urlImagenNueva != null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(urlImagenOriginal);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Método que elimina una categoria de producto
     *
     * @param idCategoria identificador de la categoria
     * @param usuario identificador del usuario
     * @param locale locale
     */
    public void removeCategoriaProducto(String idCategoria, String usuario, Locale locale) {
        CategoriaProducto categoria = em.find(CategoriaProducto.class, idCategoria);//se busca la entidad

        if (categoria == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_category_not_found");
        }

        //se obtiene el url de la imagen almacenada de la entidad y borra
        String url = categoria.getImagen();
        if (!url.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                filesUtils.deleteFile(url);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            }
        }
        //se manda a borrar la entidad y se registra la accion en la bitacora
        em.remove(categoria);

        bitacoraBean.logAccion(CategoriaProducto.class, idCategoria, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

    /**
     * Método que obtiene la informacion de una categoria de producto
     *
     * @param idCategoria identificador de categoria
     * @param locale
     * @return informacion de la categoria
     */
    public CategoriaProducto getCategoriaProductoPorId(String idCategoria, Locale locale) {
        CategoriaProducto categoria = em.find(CategoriaProducto.class, idCategoria);
        //verifica que la entidad no sea nula (no se encontro)
        if (categoria == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_category_not_found");
        }
        return categoria;
    }

    /**
     * Metodo que verifica si existe algun registro de categoria de producto con
     * un valor en la columna de nombreInterno con el mismo valor del parametro
     * nombre
     *
     * @param nombre Nombre interno a verificar
     * @return Existencia del nombre interno
     */
    public Boolean existsNombreInterno(String nombre) {
        return ((Long) em.createNamedQuery("CategoriaProducto.countByNombreInterno").setParameter("nombreInterno", nombre.toUpperCase()).getSingleResult()) > 0;
    }

    /**
     * Metodo que obtiene el listado de categorias de productos almacenadas en
     * un rango definido por parametros
     *
     * @param registro Numero de registro inicial
     * @param cantidad Cantidad de resultados en la lista
     * @param locale
     * @return Lista y encabezados acerca del rango
     */
    public Map<String, Object> getCategoriasProducto(String busqueda, String indTipoOrden, String indCampoOrden, int registro, int cantidad, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<CategoriaProducto> root = query.from(CategoriaProducto.class);

        query = Busquedas.getCriteriaQueryCategoriaProducto(cb, query, root, busqueda, indTipoOrden, indCampoOrden);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

}
