package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_LISTA_SEGMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MisionListaSegmento.getAllIdSegmentosByIdMisionIndTipo", query = "SELECT m.misionListaSegmentoPK.idSegmento FROM MisionListaSegmento m WHERE m.misionListaSegmentoPK.idMision = :idMision AND m.indTipo = :indTipo")
})
public class MisionListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MisionListaSegmentoPK misionListaSegmentoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_MISION", referencedColumnName = "ID_MISION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Mision mision;
    
    @JoinColumn(name = "ID_SEGMENTO", referencedColumnName = "ID_SEGMENTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Segmento segmento;

    public MisionListaSegmento() {
    }

    public MisionListaSegmento(String idSegmento, String idMision, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.misionListaSegmentoPK = new MisionListaSegmentoPK(idSegmento, idMision);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public MisionListaSegmentoPK getMisionListaSegmentoPK() {
        return misionListaSegmentoPK;
    }

    public void setMisionListaSegmentoPK(MisionListaSegmentoPK misionListaSegmentoPK) {
        this.misionListaSegmentoPK = misionListaSegmentoPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo == null ? null : Character.toUpperCase(indTipo);
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Mision getMision() {
        return mision;
    }

    public void setMision(Mision mision) {
        this.mision = mision;
    }

    public Segmento getSegmento() {
        return segmento;
    }

    public void setSegmento(Segmento segmento) {
        this.segmento = segmento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (misionListaSegmentoPK != null ? misionListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionListaSegmento)) {
            return false;
        }
        MisionListaSegmento other = (MisionListaSegmento) object;
        if ((this.misionListaSegmentoPK == null && other.misionListaSegmentoPK != null) || (this.misionListaSegmentoPK != null && !this.misionListaSegmentoPK.equals(other.misionListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionListaSegmento[ misionListaSegmentoPK=" + misionListaSegmentoPK + " ]";
    }
    
}
