package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.model.SegmentoListaMiembro;
import com.flecharoja.loyalty.model.SegmentoListaMiembroPK;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * la lista de miembros excluidos / incluidos de un segmento
 *
 * @author svargas
 */
@Stateless
public class SegmentoListaMiembroBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * obtencion de lista de miembros disponibles/excluidos/incluidos para un segmento
     * 
     * @param idSegmento id de segmento
     * @param indAccion indicador de tipo de lista
     * @param estados filtro de miembros
     * @param generos filtro de miembros
     * @param estadosCivil filtro de miembros
     * @param educacion filtro de miembros
     * @param contactoEmail filtro de miembros
     * @param contactoSMS filtro de miembros
     * @param contactoNotificacion filtro de miembros
     * @param contactoEstado filtro de miembros
     * @param tieneHijos filtro de miembros
     * @param busqueda filtro de miembros
     * @param filtros filtro de miembros
     * @param cantidad paginacion
     * @param registro paginacion
     * @param ordenTipo orden de paginacion
     * @param ordenCampo campo a ordenar la pagina
     * @param locale locale
     * @return lista de miembros
     */
    public Map<String, Object> getMiembros(String idSegmento, String indAccion, List<String> estados, List<String> generos, List<String> estadosCivil, List<String> educacion, String contactoEmail, String contactoSMS, String contactoNotificacion, String contactoEstado, String tieneHijos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Miembro> root = query.from(Miembro.class);

        //formacion de query de busqueda de mimebros
        query = Busquedas.getCriteriaQueryMiembros(cb, query, root, estados, generos, estadosCivil, educacion, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, ordenTipo, ordenCampo);

        //segun el tipo de lista se forma sub query para el filtro de miembros
        Subquery<String> subquery = query.subquery(String.class);
        Root<SegmentoListaMiembro> rootSubquery = subquery.from(SegmentoListaMiembro.class);
        subquery.select(rootSubquery.get("segmentoListaMiembroPK").get("idMiembro"));
        if (indAccion == null) {
            subquery.where(cb.equal(rootSubquery.get("segmentoListaMiembroPK").get("idSegmento"), idSegmento));
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            }
        } else {
            switch (indAccion.toUpperCase().charAt(0)) {
                case Indicadores.INCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("segmentoListaMiembroPK").get("idSegmento"), idSegmento), cb.equal(rootSubquery.get("indTipo"), Indicadores.INCLUIDO));
                    if (query.getRestriction() != null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.EXCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("segmentoListaMiembroPK").get("idSegmento"), idSegmento), cb.equal(rootSubquery.get("indTipo"), Indicadores.EXCLUIDO));
                    if (query.getRestriction() != null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.DISPONIBLES: {
                    subquery.where(cb.equal(rootSubquery.get("segmentoListaMiembroPK").get("idSegmento"), idSegmento));
                    if (query.getRestriction() != null) {
                        query.where(query.getRestriction(), cb.equal(root.get("indEstadoMiembro"), Miembro.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                    } else {
                        query.where(cb.equal(root.get("indEstadoMiembro"), Miembro.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                    }
                    break;
                }
                default: {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
                }
            }
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //recurrido del resultado para la asignacion de informacion adicional (almacenada en keycloak)
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            resultado.stream().forEach((t) -> {
                Miembro miembro = (Miembro) t;
                try {
                    miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
                } catch(Exception e2) {
                    miembro.setNombreUsuario("Prueba");
                }
            });
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * Metodo que obtiene un listado de los miembros excluidos e incluidos o
     * ambos filtrados adicionamente por palabras claves
     *
     * @param idSegmento Identificador del segmento
     * @param indTipo Indicador del tipo de miembros incluidos/excluidos
     * @param registro Numero de registro inicial
     * @param cantidad Cantidad de registros
     * @param busqueda Cadena de texto con palabras claves
     * @param locale
     * @return Respuesta con una lista de miembros en un rango de resultados
     */
    public Map<String, Object> searchMiembros(String idSegmento, Character indTipo, int registro, int cantidad, String busqueda, Locale locale) {
        //verificacion que el rango de registros solicitados esten dentro de lo aceptable
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }

        Map<String, Object> respuesta = new HashMap<>();

        List<Miembro> resultado;
        Long total;

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        String[] terminos = busqueda.split(" ");

        if (indTipo == null) {
            Root<SegmentoListaMiembro> root = query.from(SegmentoListaMiembro.class);

            List<Predicate> predicate0 = new ArrayList<>();
            for (String termino : terminos) {
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("nombre")), "%" + termino.toLowerCase() + "%"));
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("apellido")), "%" + termino.toLowerCase() + "%"));
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("apellido2")), "%" + termino.toLowerCase() + "%"));
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("docIdentificacion")), "%" + termino.toLowerCase() + "%"));
            }

            query.where(cb.or(predicate0.toArray(new Predicate[predicate0.size()])),
                    cb.and(cb.equal(root.get("segmento").get("idSegmento"), idSegmento)));

            //obtencion del total
            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            try {
                resultado = em.createQuery(query.select(root.get("miembro")))
                        .setFirstResult(registro)
                        .setMaxResults(cantidad)
                        .getResultList()
                        .stream().map((Object t) -> (Miembro) t).collect(Collectors.toList());
            } catch (IllegalArgumentException e) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
            }
        } else {
            if (indTipo.equals(Indicadores.DISPONIBLES)) {
                Root<Miembro> root1 = query.from(Miembro.class);

                Subquery<Miembro> subquery = query.subquery(Miembro.class);
                Root<SegmentoListaMiembro> root2 = subquery.from(SegmentoListaMiembro.class);

                List<Predicate> predicate0 = new ArrayList<>();
                for (String termino : terminos) {
                    predicate0.add(cb.like(cb.lower(root1.get("nombre")), "%" + termino.toLowerCase() + "%"));
                    predicate0.add(cb.like(cb.lower(root1.get("apellido")), "%" + termino.toLowerCase() + "%"));
                    predicate0.add(cb.like(cb.lower(root1.get("apellido2")), "%" + termino.toLowerCase() + "%"));
                    predicate0.add(cb.like(cb.lower(root1.get("docIdentificacion")), "%" + termino.toLowerCase() + "%"));
                }

                Predicate predicate1 = cb.not(root1.in(subquery.select(root2.get("miembro")).where(cb.equal(root2.get("segmento").get("idSegmento"), idSegmento))));

                query.where(cb.or(predicate0.toArray(new Predicate[predicate0.size()])), cb.and(predicate1));

                //obtencion del total
                total = (Long) em.createQuery(query.select(cb.count(root1))).getSingleResult();
                total -= total - 1 < 0 ? 0 : 1;
                while (registro > total) {
                    registro = registro - cantidad > 0 ? registro - cantidad : 0;
                }
                try {
                    resultado = em.createQuery(query.select(root1))
                            .setFirstResult(registro)
                            .setMaxResults(cantidad)
                            .getResultList()
                            .stream().map((Object t) -> (Miembro) t).collect(Collectors.toList());
                } catch (IllegalArgumentException e) {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
                }
            } else {
                Root<SegmentoListaMiembro> root = query.from(SegmentoListaMiembro.class);

                List<Predicate> predicate0 = new ArrayList<>();
                for (String termino : terminos) {
                    predicate0.add(cb.like(cb.lower(root.get("miembro").get("nombre")), "%" + termino.toLowerCase() + "%"));
                    predicate0.add(cb.like(cb.lower(root.get("miembro").get("apellido")), "%" + termino.toLowerCase() + "%"));
                    predicate0.add(cb.like(cb.lower(root.get("miembro").get("apellido2")), "%" + termino.toLowerCase() + "%"));
                    predicate0.add(cb.like(cb.lower(root.get("miembro").get("docIdentificacion")), "%" + termino.toLowerCase() + "%"));
                }

                query.where(cb.or(predicate0.toArray(new Predicate[predicate0.size()])),
                        cb.and(cb.equal(root.get("segmento").get("idSegmento"), idSegmento)),
                        cb.and(cb.equal(root.get("indTipo"), indTipo)));

                //obtencion del total
                total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
                total -= total - 1 < 0 ? 0 : 1;
                while (registro > total) {
                    registro = registro - cantidad > 0 ? registro - cantidad : 0;
                }

                try {
                    resultado = em.createQuery(query.select(root.get("miembro")))
                            .setFirstResult(registro)
                            .setMaxResults(cantidad)
                            .getResultList()
                            .stream().map((Object t) -> (Miembro) t).collect(Collectors.toList());
                } catch (IllegalArgumentException e) {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
                }
            }
        }

        //recorrido de la listas de usuarios para poder establecer el los demas atributos de keycloak
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            resultado.stream().forEach((miembro) -> {
                try {
                    miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
                } catch(Exception e2) {
                    miembro.setNombreUsuario("Prueba");
                }
            });
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * Metodo que incluye o excluye en un segmento a un miembro
     *
     * @param idSegmento Identificador del segmento
     * @param idMiembro Identificador del miembro
     * @param accion Indicador de la accion deseada (incluir, excluir)
     * @param usuario Identificador del usuario creador
     * @param locale
     */
    public void includeExcludeMiembro(String idSegmento, String idMiembro, Character accion, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }
        try {
            em.getReference(Miembro.class, idMiembro).getIdMiembro();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        //conversion del valor a su par en mayuscula y comprobacion que su valor sea valido
        accion = Character.toUpperCase(accion);
        if (accion.compareTo(Indicadores.EXCLUIDO) != 0 && accion.compareTo(Indicadores.INCLUIDO) != 0) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "accion");
        }

        //asignacion de valores por defecto
        Date date = Calendar.getInstance().getTime();

        //creacion de la entidad
        SegmentoListaMiembro segmentoListaMiembro = new SegmentoListaMiembro(idMiembro, idSegmento, accion, date, usuario);

        em.merge(segmentoListaMiembro);
        bitacoraBean.logAccion(SegmentoListaMiembro.class, idSegmento + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);

        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestActualizacionSegmento(idSegmento);
            } catch (Exception ex) {
                Logger.getLogger(SegmentoListaMiembroBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Metodo que elimina de la inclusion o exclusion en un segmento a un
     * miembro
     *
     * @param idSegmento Identificador del segmento
     * @param idMiembro Identificador del miembro
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeMiembro(String idSegmento, String idMiembro, String usuario, Locale locale) {
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }
        try {
            em.getReference(Miembro.class, idMiembro).getIdMiembro();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        try {
            //eliminacion de la entidad (si es que existe)
            em.remove(em.getReference(SegmentoListaMiembro.class, new SegmentoListaMiembroPK(idMiembro, idSegmento)));
            em.flush();
            bitacoraBean.logAccion(SegmentoListaMiembro.class, idSegmento + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_member_not_found");
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestActualizacionSegmento(idSegmento);
            } catch (Exception ex) {
                Logger.getLogger(SegmentoListaMiembroBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Metodo que incluye o excluye en un segmento a un miembro
     *
     * @param idSegmento Identificador del segmento
     * @param listaIdMiembro Identificadores de miembros
     * @param accion Indicador de la accion deseada (incluir, excluir)
     * @param usuario usuario sesión
     * @param locale
     */
    public void includeExcludeBatchMiembro(String idSegmento, List<String> listaIdMiembro, Character accion, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Segmento segmento = em.getReference(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }
        try {
            listaIdMiembro.stream().forEach((idMiembro) -> {
                em.getReference(Miembro.class, idMiembro).getIdMiembro();
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        //conversion del valor a su par en mayuscula y comprobacion que su valor sea valido
        accion = Character.toUpperCase(accion);
        if (accion.compareTo(Indicadores.EXCLUIDO) != 0 && accion.compareTo(Indicadores.INCLUIDO) != 0) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "accion");
        }

        //asignacion de valores por defecto
        Date date = Calendar.getInstance().getTime();

        //creacion de la entidad
        for (String idMiembro : listaIdMiembro) {
            em.merge(new SegmentoListaMiembro(idMiembro, idSegmento, accion, date, usuario));
            bitacoraBean.logAccion(SegmentoListaMiembro.class, idSegmento + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestActualizacionSegmento(idSegmento);
            } catch (Exception ex) {
                Logger.getLogger(SegmentoListaMiembroBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    /**
     * Metodo que elimina de la inclusion o exclusion en un segmento a un
     * miembro
     *
     * @param idSegmento Identificador del segmento
     * @param listaIdMiembro Identificadores de miembros
     * @param usuario usuario en sesión
     * @param locale
     */
    public void removeBatchMiembro(String idSegmento, List<String> listaIdMiembro, String usuario, Locale locale) {
        Segmento segmento = em.getReference(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }
        try {
            listaIdMiembro.stream().forEach((idMiembro) -> {
                em.getReference(Miembro.class, idMiembro).getIdMiembro();
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        try {
            listaIdMiembro.stream().forEach((idMiembro) -> {
                //eliminacion de la entidad (si es que existe)
                em.remove(em.getReference(SegmentoListaMiembro.class, new SegmentoListaMiembroPK(idMiembro, idSegmento)));
                bitacoraBean.logAccion(SegmentoListaMiembro.class, idSegmento + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
            });
            em.flush();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_member_not_found");
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestActualizacionSegmento(idSegmento);
            } catch (Exception ex) {
                Logger.getLogger(SegmentoListaMiembroBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }
}
