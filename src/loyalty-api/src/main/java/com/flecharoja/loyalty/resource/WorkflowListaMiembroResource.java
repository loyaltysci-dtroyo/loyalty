package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.service.WorkflowListaMiembroBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase con recursos http para el manejo de listas de miembros
 * incluidos/excluidos de workflows
 *
 * @author svargas
 */
@Api(value = "Workflow")
@Path("workflow/{idWorkflow}/miembro")
public class WorkflowListaMiembroResource {

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @EJB
    WorkflowListaMiembroBean listaMiembroBean;

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión
    
    @Context
    HttpServletRequest request;

    /**
     * Identificador de promocion
     */
    @PathParam("idWorkflow")
    String idWorkflow;

    /**
     * Metodo que obtiene un listado de miembros incluidos, excluidos de un
     * workflow por su identificador o los disponibles para su inclusion/exclusion
     * segun el valor del parametro de accion y en un rango de registros validos
     * segun los parametros de registro y cantidad.
     *
     * @param accion Indicador del tipo de accion incluido/excluido/disponibles
     * @param estados Parametro opcional con los indicadores de estados deseados
     * @param generos Indicadores de generos deseados
     * @param estadosCiviles Indicadores de estados civiles deseados
     * @param educaciones Indicadores de educaciones deseados
     * @param contactoEmail Indicador booleano sobre el contacto de email
     * @param contactoSMS Indicador booleando sobre el contacto de sms
     * @param contactoNotificacion Indicador booleando sobre el contacto de notificacion push
     * @param contactoEstado Indicador booleando sobre el contacto de estado
     * @param tieneHijos Indicador booleando sobre el tener hijos
     * @param busqueda Terminos de busqueda
     * @param filtros Campos sobre que aplicar la busqueda
     * @param ordenTipo
     * @param ordenCampo
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @return Respuesta con el listado de miembros deseados en un rango valido
     * junto con encabezados sobre el rango
     */
    @ApiOperation(value = "Obtener lista de miembros de workflow",
            responseContainer = "List",
            response = Miembro.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idWorkflow", value = "Identificador de workflow", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMiembros(
            @ApiParam(value = "Indicador del tipo de lista de miembros") @QueryParam("accion") String accion,
            @ApiParam(value = "Indicadores de estado de miembros") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de genero de miembros") @QueryParam("genero") List<String> generos,
            @ApiParam(value = "Indicadores de estado civil de miembros") @QueryParam("estado-civil") List<String> estadosCiviles,
            @ApiParam(value = "Indicadores de educacion de miembros") @QueryParam("educacion") List<String> educaciones,
            @ApiParam(value = "Indicador booleano de contacto de email") @QueryParam("contacto-email") String contactoEmail,
            @ApiParam(value = "Indicador booleano de contacto de sms") @QueryParam("contacto-sms") String contactoSMS,
            @ApiParam(value = "Indicador booleano de contacto de notificacion") @QueryParam("contacto-notificacion") String contactoNotificacion,
            @ApiParam(value = "Indicador booleano de contacto de estado") @QueryParam("contacto-estado") String contactoEstado,
            @ApiParam(value = "Indicador booleano de tiene hijos") @QueryParam("tiene-hijos") String tieneHijos,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = listaMiembroBean.getMiembros(idWorkflow, accion, estados, generos, estadosCiviles, educaciones, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }

    /**
     * Metodo que incluye o excluye segun el parametro accion, un listado de
     * identificadores de miembros a un workflow por su identificador.
     *
     * @param listaIdMiembro Lista de identificadores de miembros
     * @param accion Parametro indicando el tipo de accion (incluir/excluir)
     */
    @ApiOperation(value = "Asginacion de lista de miembros a workflow")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idWorkflow", value = "Identificador de workflow", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void includeExcludeBatchMiembro(
            @ApiParam(value = "Identificadores de miembros", required = true) List<String> listaIdMiembro,
            @ApiParam(value = "Indicador de tipo de lista a asignar", required = true)
            @QueryParam("accion") String accion) {
        if (accion == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, request.getLocale(), "attributes_invalid", "accion");
        }
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        listaMiembroBean.includeExcludeBatchMiembro(idWorkflow, listaIdMiembro, accion.toUpperCase().charAt(0), usuarioContext,request.getLocale());
    }

    /**
     * Metodo que elimina la inclusion o exclusion de un listado de
     * identificadores de miembros a un workflow.
     *
     * @param listaIdMiembro Lista de identificadores de miembros
     */
    @ApiOperation(value = "Eliminacion de la asginacion de lista de miembros a workflow")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idWorkflow", value = "Identificador de workflow", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("remover-lista")
    public void removeBatchMiembro(
            @ApiParam(value = "Identificadores de miembros", required = true) List<String> listaIdMiembro) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        listaMiembroBean.removeBatchMiembro(idWorkflow, listaIdMiembro, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que incluye o excluye segun el parametro accion, un miembro a un
     * workflow por sus identificadores.
     *
     * @param idMiembro Identificador del miembro
     * @param accion Parametro indicando el tipo de accion (incluir/excluir)
     */
    @ApiOperation(value = "Asignacion de miembro a workflow")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idWorkflow", value = "Identificador de workflow", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @POST
    @Path("{idMiembro}")
    public void includeExcludeMiembro(
            @ApiParam(value = "Identificador de miembro", required = true)
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Indicador de tipo de lista a asignar", required = true)
            @QueryParam("accion") String accion) {
        if (accion == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, request.getLocale(), "attributes_invalid", "accion");
        }
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        listaMiembroBean.includeExcludeMiembro(idWorkflow, idMiembro, accion.toUpperCase().charAt(0), usuarioContext,request.getLocale());
    }

    /**
     * Metodo que elimina la inclusion o exclusion de un miembro a un workflow.
     *
     * @param idMiembro Identificador del miembro
     */
    @ApiOperation(value = "Eliminacion de la asignacion de miembro a workflow")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idWorkflow", value = "Identificador de workflow", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @DELETE
    @Path("{idMiembro}")
    public void removeMiembro(
            @ApiParam(value = "Identificador de miembro", required = true)
            @PathParam("idMiembro") String idMiembro) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        listaMiembroBean.removeMiembro(idWorkflow, idMiembro, usuarioContext,request.getLocale());
    }
}
