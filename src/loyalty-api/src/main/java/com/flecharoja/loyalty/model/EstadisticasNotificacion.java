package com.flecharoja.loyalty.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class EstadisticasNotificacion {
    
    @XmlRootElement
    public class Periodo {
        private String mes;
        private long cantMsjsEnviados;
        private long cantMsjsAbiertos;
        private long cantMiembrosEnviados;

        public Periodo(String mes, long cantMsjsEnviados, long cantMsjsAbiertos, long cantMiembrosEnviados) {
            this.mes = mes;
            this.cantMsjsEnviados = cantMsjsEnviados;
            this.cantMsjsAbiertos = cantMsjsAbiertos;
            this.cantMiembrosEnviados = cantMiembrosEnviados;
        }

        public String getMes() {
            return mes;
        }

        public void setMes(String mes) {
            this.mes = mes;
        }

        public long getCantMsjsEnviados() {
            return cantMsjsEnviados;
        }

        public void setCantMsjsEnviados(long cantMsjsEnviados) {
            this.cantMsjsEnviados = cantMsjsEnviados;
        }

        public long getCantMsjsAbiertos() {
            return cantMsjsAbiertos;
        }

        public void setCantMsjsAbiertos(long cantMsjsAbiertos) {
            this.cantMsjsAbiertos = cantMsjsAbiertos;
        }

        public long getCantMiembrosEnviados() {
            return cantMiembrosEnviados;
        }

        public void setCantMiembrosEnviados(long cantMiembrosEnviados) {
            this.cantMiembrosEnviados = cantMiembrosEnviados;
        }
    }
    
    private String idNotificacion;
    private String nombreNotificacion;
    private String nombreInternoNotificacion;
    
    private long cantMsjsEnviados;
    private long cantMsjsAbiertos;
    private long cantMiembrosEnviados;
    private Long cantMiembrosObjetivo;
    
    private List<Periodo> periodos;

    public EstadisticasNotificacion() {
        periodos = new ArrayList<>();
    }

    public String getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(String idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public String getNombreNotificacion() {
        return nombreNotificacion;
    }

    public void setNombreNotificacion(String nombreNotificacion) {
        this.nombreNotificacion = nombreNotificacion;
    }

    public String getNombreInternoNotificacion() {
        return nombreInternoNotificacion;
    }

    public void setNombreInternoNotificacion(String nombreInternoNotificacion) {
        this.nombreInternoNotificacion = nombreInternoNotificacion;
    }

    public long getCantMsjsEnviados() {
        return cantMsjsEnviados;
    }

    public void setCantMsjsEnviados(long cantMsjsEnviados) {
        this.cantMsjsEnviados = cantMsjsEnviados;
    }

    public long getCantMsjsAbiertos() {
        return cantMsjsAbiertos;
    }

    public void setCantMsjsAbiertos(long cantMsjsAbiertos) {
        this.cantMsjsAbiertos = cantMsjsAbiertos;
    }

    public long getCantMiembrosEnviados() {
        return cantMiembrosEnviados;
    }

    public void setCantMiembrosEnviados(long cantMiembrosEnviados) {
        this.cantMiembrosEnviados = cantMiembrosEnviados;
    }

    public Long getCantMiembrosObjetivo() {
        return cantMiembrosObjetivo;
    }

    public void setCantMiembrosObjetivo(Long cantMiembrosObjetivo) {
        this.cantMiembrosObjetivo = cantMiembrosObjetivo;
    }

    public List<Periodo> getPeriodos() {
        return periodos;
    }

    public void setPeriodos(List<Periodo> periodos) {
        this.periodos = periodos;
    }
    
    public void addPeriodo(String mes, long cantMsjsEnviados, long cantMsjsAbiertos, long cantMiembrosEnviados) {
        this.periodos.add(new Periodo(mes, cantMsjsEnviados, cantMsjsAbiertos, cantMiembrosEnviados));
    }
}
