package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_ENCUESTA_PREGUNTA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MisionEncuestaPregunta.findAllByIdMision", query = "SELECT m FROM MisionEncuestaPregunta m WHERE m.mision.idMision = :idMision ORDER BY m.fechaModificacion DESC"),
    @NamedQuery(name = "MisionEncuestaPregunta.countAllByIdMision", query = "SELECT COUNT(m) FROM MisionEncuestaPregunta m WHERE m.mision.idMision = :idMision")
})
public class MisionEncuestaPregunta implements Serializable {
    
    public enum TiposPregunta {
        ENCUESTA('E'),
        CALIFICACION('C');
        
        private final char value;
        private static final Map<Character, TiposPregunta> lookup = new HashMap<>();

        private TiposPregunta(char value) {
            this.value = value;
        }
        
        static {
            for (TiposPregunta pregunta : values()) {
                lookup.put(pregunta.value, pregunta);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static TiposPregunta get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum TiposRespuesta {
        SELECCION_MULTIPLE("RM"),
        SELECCION_UNICA("RU"),
        RESPUESTA_ABIERTA_TEXTO("RT"),
        RESPUESTA_ABIERTA_NUMERICO("RN");
        
        private final String value;
        private static final Map<String, TiposRespuesta> lookup = new HashMap<>();

        private TiposRespuesta(String value) {
            this.value = value;
        }
        
        static {
            for (TiposRespuesta pregunta : values()) {
                lookup.put(pregunta.value, pregunta);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static TiposRespuesta get (String value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum TiposRespuestaCorrecta {
        TODAS('T'),
        ES('U'),
        CUALQUIERA_DE('C'),
        SON('G'),
        MENOR_IGUAL('D'),
        IGUAL('E'),
        MAYOR_IGUAL('F');
        
        private final char value;
        private static final Map<Character, TiposRespuestaCorrecta> lookup = new HashMap<>();

        private TiposRespuestaCorrecta(char value) {
            this.value = value;
        }
        
        static {
            for (TiposRespuestaCorrecta pregunta : values()) {
                lookup.put(pregunta.value, pregunta);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static TiposRespuestaCorrecta get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "mision_encuesta_pregunta_uuid")
    @GenericGenerator(name = "mision_encuesta_pregunta_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_PREGUNTA")
    private String idPregunta;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_PREGUNTA")
    private Character indTipoPregunta;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "PREGUNTA")
    private String pregunta;
    
    @Size(max = 300)
    @Column(name = "RESPUESTAS")
    private String respuestas;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "IND_TIPO_RESPUESTA")
    private String indTipoRespuesta;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    private String imagen;
    
    @Column(name = "IND_COMENTARIO")
    private Boolean indComentario;
    
    @Size(max = 500)
    @Column(name = "COMENTARIOS")
    private String comentarios;
    
    @Column(name = "IND_RESPUESTA_CORRECTA")
    private Character indRespuestaCorrecta;
    
    @Size(max = 20)
    @Column(name = "RESPUESTAS_CORRECTAS")
    private String respuestasCorrectas;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Version
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @JoinColumn(name = "ID_MISION", referencedColumnName = "ID_MISION")
    @ManyToOne(optional = false)
    private Mision mision;

    public MisionEncuestaPregunta() {
    }

    public String getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(String idPregunta) {
        this.idPregunta = idPregunta;
    }

    public Character getIndTipoPregunta() {
        return indTipoPregunta;
    }

    public void setIndTipoPregunta(Character indTipoPregunta) {
        this.indTipoPregunta = indTipoPregunta==null ? null : Character.toUpperCase(indTipoPregunta);
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(String respuestas) {
        this.respuestas = respuestas;
    }

    public String getIndTipoRespuesta() {
        return indTipoRespuesta;
    }

    public void setIndTipoRespuesta(String indTipoRespuesta) {
        this.indTipoRespuesta = indTipoRespuesta;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Boolean getIndComentario() {
        return indComentario;
    }

    public void setIndComentario(Boolean indComentario) {
        this.indComentario = indComentario;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Character getIndRespuestaCorrecta() {
        return indRespuestaCorrecta;
    }

    public void setIndRespuestaCorrecta(Character indRespuestaCorrecta) {
        this.indRespuestaCorrecta = indRespuestaCorrecta == null ? null : Character.toUpperCase(indRespuestaCorrecta);
    }

    public String getRespuestasCorrectas() {
        return respuestasCorrectas;
    }

    public void setRespuestasCorrectas(String respuestasCorrectas) {
        this.respuestasCorrectas = respuestasCorrectas;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Mision getMision() {
        return mision;
    }

    public void setMision(Mision mision) {
        this.mision = mision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPregunta != null ? idPregunta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionEncuestaPregunta)) {
            return false;
        }
        MisionEncuestaPregunta other = (MisionEncuestaPregunta) object;
        if ((this.idPregunta == null && other.idPregunta != null) || (this.idPregunta != null && !this.idPregunta.equals(other.idPregunta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionEncuestaPregunta[ idPregunta=" + idPregunta + " ]";
    }
    
}
