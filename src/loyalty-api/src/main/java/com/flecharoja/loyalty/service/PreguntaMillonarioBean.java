/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.PreguntaMillonario;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;
import com.flecharoja.loyalty.util.Indicadores;
import java.util.ArrayList;

/**
 *
 * @author kevin
 */
@Stateless
public class PreguntaMillonarioBean {
    
    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora
    
    @EJB JuegoMillonarioBean juegoMillonarioBean;
    
    /**
     * Metodo que guarda en almacenamiento la informacion de una nueva metrica
     *
     * @param pregunta
     * @param usuario usuario en sesión
     * @param locale
     * @return Identificador asignado a la metrica almacenada
     */
    public String insertPregunta(PreguntaMillonario pregunta, String usuario, Locale locale) {
        if (pregunta == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        if (pregunta.getIdJuego() == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        Date date = Calendar.getInstance().getTime();//obtencion de la fecha actual

        //establecimiento de atributos por defecto
        pregunta.setUsuarioCreacion(usuario);
        pregunta.setUsuarioModificacion(usuario);
        pregunta.setFechaCreacion(date);
        pregunta.setFechaModificacion(date);
        pregunta.setNumVersion(new Long(1));
        //almacenamiento de la entidad y registro de la accion en la bitacora
        try {
            em.persist(pregunta);
            em.flush();
        } catch (ConstraintViolationException e) {//si se viola una restriccion de un atributo...
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        
        bitacoraBean.logAccion(PreguntaMillonario.class, pregunta.getIdPregunta(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        juegoMillonarioBean.updateIsCompleteJuego(pregunta.getIdJuego(), isJuegoComplete(pregunta.getIdJuego(), locale), usuario, locale);
        
        return pregunta.getIdPregunta();
    }
    
    
    /**
     * Metodo que modifica en el almacenamiento la informacion de una metrica
     * existente
     *
     * @param pregunta
     * @param usuario usuario en sesión
     * @param locale
     */
    public void editPregunta(PreguntaMillonario pregunta, String usuario, Locale locale) {
        if (pregunta == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        //verificacion de la existencia de la entidad
        PreguntaMillonario original = em.find(PreguntaMillonario.class, pregunta.getIdPregunta());
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "metric_not_found");
        }

        pregunta.setUsuarioModificacion(usuario);
        pregunta.setFechaModificacion(Calendar.getInstance().getTime());

        try {
            em.merge(pregunta);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
        
        bitacoraBean.logAccion(PreguntaMillonario.class, pregunta.getIdPregunta(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        juegoMillonarioBean.updateIsCompleteJuego(pregunta.getIdJuego(), isJuegoComplete(pregunta.getIdJuego(), locale), usuario, locale);
    }
    
    public boolean isJuegoComplete(String idJuego, Locale locale) {
        List<PreguntaMillonario> preguntas = getPreguntas(idJuego, locale);
        List<String> puntosObtenidos = new ArrayList<>();
        
        for (int i=0;i<preguntas.size(); i++) {
            if (!puntosObtenidos.contains(preguntas.get(i).getPuntos()+"")) {
                puntosObtenidos.add(preguntas.get(i).getPuntos()+"");
            }
        }
        
        for (String PUNTOS_MILLONARIO : Indicadores.PUNTOS_MILLONARIO) {
            if (!puntosObtenidos.contains(PUNTOS_MILLONARIO)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Metodo que ontiene las preguntas de un juego
     * existente
     *
     * @param idJuego
     * @param locale
     * @return 
     */
    public List<PreguntaMillonario> getPreguntas(String idJuego, Locale locale) {
        if (idJuego == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        //verificacion de la existencia de la entidad
        List<PreguntaMillonario> preguntas;
            preguntas = em.createNamedQuery("PreguntaMillonario.findByJuego")
                    .setParameter("idJuego", idJuego)
                    .getResultList();
        return preguntas;
    }
    
    /**
     * Metodo que ontiene las preguntas de un juego
     * existente
     *
     * @param idPregunta
     * @param locale
     * @return 
     */
    public PreguntaMillonario getPregunta(String idPregunta, Locale locale) {
        if (idPregunta == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", this.getClass().getSimpleName());
        }
        return em.find(PreguntaMillonario.class, idPregunta);
    }
}
