package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.MisionPreferencia;
import com.flecharoja.loyalty.model.MisionPreferenciaPK;
import com.flecharoja.loyalty.model.Preferencia;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * EJB encargado del manejo de datos para el mantenimiento de misiones de tipo
 * preferencia
 *
 * @author svargas
 */
@Stateless
public class MisionPreferenciaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    @EJB
    UtilBean utilBean;

    /**
     * obtencion de preferencias referidas para una mision
     * 
     * @param idMision identificador de mision
     * @param tipo accion de asignado o disponible
     * @param locale locale
     * @return lista de preferencias
     */
    public List getPreferenciasMision(String idMision, String tipo, Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.PREFERENCIAS.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }
        
        List resultado;
        if (tipo==null || tipo.toUpperCase().charAt(0)==Indicadores.ASIGNADO) {
            resultado = em.createNamedQuery("MisionPreferencia.getPreferenciaByIdMision").setParameter("idMision", idMision).getResultList();
        } else {
            if (tipo.toUpperCase().charAt(0)==Indicadores.DISPONIBLES) {
                resultado = em.createNamedQuery("MisionPreferencia.getPreferenciaNotInByIdMision").setParameter("idMision", idMision).getResultList();
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "tipo");
            }
        }

        return resultado;
    }

    /**
     * asignacion de una preferencia a una mision
     * 
     * @param idMision identificador de mision
     * @param idPreferencias identificador de preferencia
     * @param idUsuario id admin
     * @param locale locale
     */
    public void assignPreferenciasMision(String idMision, List<String> idPreferencias, String idUsuario, Locale locale) {//verificacion que los id's sean validos
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.PREFERENCIAS.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }
        
        idPreferencias.stream().forEach((idPreferencia) -> {
            Preferencia preferencia = em.find(Preferencia.class, idPreferencia);
            if (preferencia==null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idPreferencia");
            }
            em.merge(new MisionPreferencia(idMision, idPreferencia, new Date(), idUsuario));
            bitacoraBean.logAccion(MisionPreferencia.class, idMision + "/" + idPreferencia, idUsuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        });
    }
    
    /**
     * eliminacion de relacion de preferencia con mision
     * 
     * @param idMision identificador de mision
     * @param idPreferencias identificador de preferencia
     * @param idUsuario id admin
     * @param locale locale
     */
    public void deletePeferenciasMision(String idMision, List<String> idPreferencias, String idUsuario, Locale locale) {//verificacion que los id's sean validos
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (!mision.getIndTipoMision().equals(Mision.Tipos.PREFERENCIAS.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_type_invalid");
        }

        //verificacion de entidad archivada
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }
        
        idPreferencias.stream().forEach((idPreferencia) -> {
            MisionPreferencia misionPreferencia = em.find(MisionPreferencia.class, new MisionPreferenciaPK(idMision, idPreferencia));
            if (misionPreferencia==null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idPreferencia");
            }
            em.remove(misionPreferencia);
            bitacoraBean.logAccion(MisionPreferencia.class, idMision + "/" + idPreferencia, idUsuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        });
    }

}
