/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "TABPOS_GRUPOMIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TabposGrupomiembro.findAll", query = "SELECT t FROM TabposGrupomiembro t"),
    @NamedQuery(name = "TabposGrupomiembro.findByIdTabla", query = "SELECT t FROM TabposGrupomiembro t WHERE t.tabposGrupomiembroPK.idTabla = :idTabla ORDER BY t.acumulado DESC "),
    @NamedQuery(name = "TabposGrupomiembro.findByIdGrupo", query = "SELECT t FROM TabposGrupomiembro t WHERE t.tabposGrupomiembroPK.idGrupo = :idGrupo"),
    @NamedQuery(name = "TabposGrupomiembro.findByAcumulado", query = "SELECT t FROM TabposGrupomiembro t WHERE t.acumulado = :acumulado")})
public class TabposGrupomiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TabposGrupomiembroPK tabposGrupomiembroPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACUMULADO")
    private Double acumulado;
    @JoinColumn(name = "ID_GRUPO", referencedColumnName = "ID_GRUPO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Grupo grupo;
    @JoinColumn(name = "ID_TABLA", referencedColumnName = "ID_TABLA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TablaPosiciones tablaPosiciones;

    public TabposGrupomiembro() {
    }

    public TabposGrupomiembro(TabposGrupomiembroPK tabposGrupomiembroPK) {
        this.tabposGrupomiembroPK = tabposGrupomiembroPK;
    }

    public TabposGrupomiembro(TabposGrupomiembroPK tabposGrupomiembroPK, Double acumulado) {
        this.tabposGrupomiembroPK = tabposGrupomiembroPK;
        this.acumulado = acumulado;
    }

    public TabposGrupomiembro(String idTabla, String idGrupo) {
        this.tabposGrupomiembroPK = new TabposGrupomiembroPK(idTabla, idGrupo);
        this.acumulado = Double.valueOf(0);
    }

    @ApiModelProperty(hidden = true)
    public TabposGrupomiembroPK getTabposGrupomiembroPK() {
        return tabposGrupomiembroPK;
    }

    public void setTabposGrupomiembroPK(TabposGrupomiembroPK tabposGrupomiembroPK) {
        this.tabposGrupomiembroPK = tabposGrupomiembroPK;
    }

    public Double getAcumulado() {
        return acumulado;
    }

    public void setAcumulado(Double acumulado) {
        this.acumulado = acumulado;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public TablaPosiciones getTablaPosiciones() {
        return tablaPosiciones;
    }

    public void setTablaPosiciones(TablaPosiciones tablaPosiciones) {
        this.tablaPosiciones = tablaPosiciones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tabposGrupomiembroPK != null ? tabposGrupomiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TabposGrupomiembro)) {
            return false;
        }
        TabposGrupomiembro other = (TabposGrupomiembro) object;
        if ((this.tabposGrupomiembroPK == null && other.tabposGrupomiembroPK != null) || (this.tabposGrupomiembroPK != null && !this.tabposGrupomiembroPK.equals(other.tabposGrupomiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.TabposGrupomiembro[ tabposGrupomiembroPK=" + tabposGrupomiembroPK + " ]";
    }
    
}
