/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Region;
import com.flecharoja.loyalty.model.RegionUbicacion;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.validation.ConstraintViolationException;

/**
 * EJB con los metodos para el manejo de la asignacion de ubicaciones a regiones
 *
 * @author wtencio
 */
@Stateless
public class RegionUbicacionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos para el manejo de bitacora

    /**
     * Método que asigna una ubicacion a una region
     *
     * @param idRegion identificador de la region
     * @param idUbicacion identificadpr de la ubicacion
     * @param usuario usuario en sesion
     * @param locale
     */
    public void assignRegionUbicacion(String idRegion, String idUbicacion, String usuario, Locale locale) {
        RegionUbicacion regionUbicacion = new RegionUbicacion(idRegion, idUbicacion);
        try {
            Region region = em.find(Region.class, idRegion);
            Ubicacion ubicacion = em.find(Ubicacion.class, idUbicacion);

            regionUbicacion.setFechaCreacion(Calendar.getInstance().getTime());
            regionUbicacion.setRegion(region);
            regionUbicacion.setUbicacion(ubicacion);
            regionUbicacion.setUsuarioCreacion(usuario);

            em.persist(regionUbicacion);
            em.flush();
            bitacoraBean.logAccion(RegionUbicacion.class, idRegion + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que desasigna una ubicacion a una region
     *
     * @param idRegion identificador de la region
     * @param idUbicacion identificador de la ubicacion
     * @param usuario usuario en sesion
     * @param locale
     */
    public void unassignRegionUbicacion(String idRegion, String idUbicacion, String usuario, Locale locale) {

        RegionUbicacion regionUbicacion = (RegionUbicacion) em.createNamedQuery("RegionUbicacion.findByIdRegionByIdUbicacion").setParameter("idRegion", idRegion).setParameter("idUbicacion", idUbicacion).getSingleResult();
        if (regionUbicacion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "region_location_not_found");
        }
        em.remove(regionUbicacion);
        em.flush();
        bitacoraBean.logAccion(RegionUbicacion.class, idRegion + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

    /**
     * Método que obtiene una lista de ubicaciones pertenecientes a una region
     *
     * @param idRegion identificador de la region
     * @param cantidad de registros maximos por pagina
     * @param registro por el cual se comienza laa busqueda
     * @param locale
     * @return lista con las ubicaciones
     */
    public Map<String, Object> getUbicacionesRegion(String idRegion, int cantidad, int registro, Locale locale) {
        Map<String, Object> respuesta = new HashMap<>();
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        List resultado = new ArrayList();
        List registros = new ArrayList();

        int total;
        try {
            registros = em.createNamedQuery("RegionUbicacion.findByIdRegion").setParameter("idRegion", idRegion).getResultList();
            total = registros.size();
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createNamedQuery("RegionUbicacion.findByIdRegion").setParameter("idRegion", idRegion)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (QueryTimeoutException e) {
             throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO,locale, "database_connection_failed");
        } catch (IllegalArgumentException  e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Método que muestra una lista de ubicaciones no pertenecientes a una
     * region
     *
     * @param idRegion identificador de region
     * @param cantidad de registros maximos por pagina
     * @param registro por el cual se inicia la busqueda
     * @param locale
     * @return lista de ubicaciones
     */
    public Map<String, Object> getUbicacionesNoRegion(String idRegion, int cantidad, int registro, Locale locale) {
        Map<String, Object> respuesta = new HashMap<>();
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        List resultado = new ArrayList();
        List registros = new ArrayList();

        int total;
        try {
            registros = em.createNamedQuery("RegionUbicacion.findUbicacionesNoRegion")
                    .setParameter("idRegion", idRegion)
                    .getResultList();
            total = registros.size();
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createNamedQuery("RegionUbicacion.findUbicacionesNoRegion")
                    .setParameter("idRegion", idRegion)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (QueryTimeoutException e) {
             throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO,locale, "database_connection_failed");
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }
}
