/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;


import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CategoriaProducto;
import com.flecharoja.loyalty.model.CertificadoCategoria;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author Kevin
 */
@Stateless
public class CertificadoCategoriaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;


    /**
     * Método que actualiza un certificado, ya sea la fecha de expiracion o el numCertificado
     *
     * @param categoriaProducto
     * @return lista de certificados disponibles
     */
    public List<CertificadoCategoria> getCertificados(String categoriaProducto) {
        return em.createNamedQuery("CertificadoCategoria.findByCategoria").setParameter("categoriaProducto", categoriaProducto).getResultList();
    }
    
    /**
     * Método que obtiene una lista de certificados disponibles
     *
     * @param newCertificado
     * @return Certificado actualizado
     */
    public CertificadoCategoria updateCertificadoCategoria(CertificadoCategoria newCertificado) {
        if (newCertificado.getIdCertificado() == null) {
            //Imprimir error
        }
        
        CertificadoCategoria certificado = em.find(CertificadoCategoria.class, newCertificado.getIdCertificado());
        
        if (newCertificado.getFechaExpiracion() != null) {
            certificado.setFechaExpiracion(newCertificado.getFechaExpiracion());
        }
        
        if (newCertificado.getNumCertificado() != null) {
            certificado.setNumCertificado(newCertificado.getNumCertificado());
        }
        
        try {
            em.merge(certificado);
            em.flush();
        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            //throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        return certificado;
    }
    
    /**
     * Método que obtiene una lista de certificados disponibles
     *
     * @param idCategoriaProducto
     * @param numCertificado
     * @param usuario
     * @return Certificado actualizado
     */
    public CertificadoCategoria insertarCertificadoCategoria(String idCategoriaProducto, String numCertificado, String usuario, Locale locale) {
        if (idCategoriaProducto == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "idCategoria_not_null");
        }
        if (numCertificado == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "numCertificado_not_null");
        }
        CategoriaProducto categoria = em.find(CategoriaProducto.class, idCategoriaProducto);
        
        if (categoria == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "categoria_not_found");
        }
        
        if (existsCertificado(idCategoriaProducto, numCertificado)) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "certificado_exist");
        }
        
        CertificadoCategoria certificado = new CertificadoCategoria();
        certificado.setIdCategoria(idCategoriaProducto);
        certificado.setNumCertificado(numCertificado);
        certificado.setEstado('D');
        certificado.setPrecio(0.0);
        certificado.setFechaCreacion(Calendar.getInstance().getTime());
        certificado.setUsuarioCreacion(usuario);
        certificado.setNumVersion(new Long(1));
        
        try {
            em.persist(certificado);
            em.flush();
        } catch (ConstraintViolationException e) {//en caso de que la entidad no sea valida
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        return certificado;
    }
    
    /**
     * verificacion de la existencia del certificado
     * 
     * @param numCertificado
     * @param idCategoria
     * @return existe o no uno o mas certificados
     */
    public boolean existsCertificado(String idCategoria, String numCertificado) {
        return em.createNamedQuery("CertificadoCategoria.existCertificado").setParameter("numCertificado", numCertificado).setParameter("idCategoria", idCategoria).getResultList().size() > 0;
    }
}
