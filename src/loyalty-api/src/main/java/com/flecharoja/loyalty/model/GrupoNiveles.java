/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "GRUPO_NIVELES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GrupoNiveles.findAll", query = "SELECT g FROM GrupoNiveles g ORDER BY g.fechaModificacion DESC"),
    @NamedQuery(name = "GrupoNiveles.countAll", query = "SELECT COUNT(g.idGrupoNivel) FROM GrupoNiveles g"),
    @NamedQuery(name = "GrupoNiveles.findByIdGrupoNivel", query = "SELECT g FROM GrupoNiveles g WHERE g.idGrupoNivel = :idGrupoNivel"),
    @NamedQuery(name = "GrupoNiveles.findByNombre", query = "SELECT g FROM GrupoNiveles g WHERE g.nombre = :nombre ORDER BY g.fechaModificacion DESC"),
    @NamedQuery(name = "GrupoNiveles.countByNombre", query = "SELECT COUNT(g.idGrupoNivel) FROM GrupoNiveles g WHERE g.nombre = :nombre")

})

public class GrupoNiveles implements Serializable {

    @Version
    @Basic(optional = false)
    @NotNull()
    @Column(name = "NUM_VERSION")
    private Long numVersion;

   

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "gruponiveles_uuid")
    @GenericGenerator(name = "gruponiveles_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_GRUPO_NIVEL")
    private String idGrupoNivel;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    
     @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
     
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grupoNiveles")
    private List<Metrica> metricaList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grupoNiveles")
    private List<NivelMetrica> nivelMetricaList;

    public GrupoNiveles() {
    }

    public GrupoNiveles(String idGrupoNivel) {
        this.idGrupoNivel = idGrupoNivel;
    }

    public GrupoNiveles(String idGrupoNivel, String nombre) {
        this.idGrupoNivel = idGrupoNivel;
        this.nombre = nombre;
    }

    public String getIdGrupoNivel() {
        return idGrupoNivel;
    }

    public void setIdGrupoNivel(String idGrupoNivel) {
        this.idGrupoNivel = idGrupoNivel;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<Metrica> getMetricaList() {
        return metricaList;
    }

    public void setMetricaList(List<Metrica> metricaList) {
        this.metricaList = metricaList;
    }
    
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<NivelMetrica> getNivelMetricaList() {
        return nivelMetricaList;
    }

    public void setNivelMetricaList(List<NivelMetrica> nivelMetricaList) {
        this.nivelMetricaList = nivelMetricaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGrupoNivel != null ? idGrupoNivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupoNiveles)) {
            return false;
        }
        GrupoNiveles other = (GrupoNiveles) object;
        if ((this.idGrupoNivel == null && other.idGrupoNivel != null) || (this.idGrupoNivel != null && !this.idGrupoNivel.equals(other.idGrupoNivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.GrupoNiveles[ idGrupoNivel=" + idGrupoNivel + " ]";
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }
}
