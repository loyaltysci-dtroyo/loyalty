/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "ESTADISTICA_SEGMENTO_INDIVIDUAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadisticaSegmentoIndividual.findAllByIndEstado", query = "SELECT e FROM EstadisticaSegmentoIndividual e WHERE e.segmento.indEstado = :indEstado"),
    @NamedQuery(name = "EstadisticaSegmentoIndividual.findAll", query = "SELECT e FROM EstadisticaSegmentoIndividual e")
    , @NamedQuery(name = "EstadisticaSegmentoIndividual.findByIdSegmento", query = "SELECT e FROM EstadisticaSegmentoIndividual e WHERE e.segmento.idSegmento = :idSegmento")
    , @NamedQuery(name = "EstadisticaSegmentoIndividual.findByMiembrosNuevos", query = "SELECT e FROM EstadisticaSegmentoIndividual e WHERE e.miembrosNuevos = :miembrosNuevos")})
public class EstadisticaSegmentoIndividual implements Serializable {
    @Id
    @NotNull
    @Size(min = 1, max = 40)
    @JoinColumn(name = "ID_SEGMENTO", referencedColumnName = "ID_SEGMENTO")
    @OneToOne(optional = false)
    private Segmento segmento;
    private static final long serialVersionUID = 1L;
    @Column(name = "MIEMBROS_NUEVOS")
    private Integer miembrosNuevos;

    public EstadisticaSegmentoIndividual() {
    }


    public Integer getMiembrosNuevos() {
        return miembrosNuevos;
    }

    public void setMiembrosNuevos(Integer miembrosNuevos) {
        this.miembrosNuevos = miembrosNuevos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segmento != null ? segmento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadisticaSegmentoIndividual)) {
            return false;
        }
        EstadisticaSegmentoIndividual other = (EstadisticaSegmentoIndividual) object;
        if ((this.segmento == null && other.segmento != null) || (this.segmento != null && !this.segmento.equals(other.segmento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.EstadisticaSegmentoIndividual[ idSegmento=" + segmento + " ]";
    }

    public Segmento getSegmento() {
        return segmento;
    }

    public void setSegmento(Segmento segmento) {
        this.segmento = segmento;
    }


}
