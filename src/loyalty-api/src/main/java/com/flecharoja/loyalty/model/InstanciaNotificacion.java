package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "INSTANCIA_NOTIFICACION")
@NamedQueries({
    @NamedQuery(name = "InstanciaNotificacion.findAllByIdMiembro", query = "SELECT i FROM InstanciaNotificacion i WHERE i.instanciaNotificacionPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "InstanciaNotificacion.findAll", query = "SELECT i FROM InstanciaNotificacion i"),
    @NamedQuery(name = "InstanciaNotificacion.findAllByIdNotificacion", query = "SELECT i FROM InstanciaNotificacion i WHERE i.notificacion.idNotificacion = :idNotificacion")
})
public class InstanciaNotificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected InstanciaNotificacionPK instanciaNotificacionPK;
    
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    
    @Column(name = "FECHA_ENTREGA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEntrega;
    
    @Column(name = "FECHA_VISTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVisto;
    
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @JoinColumn(name = "ID_NOTIFICACION", referencedColumnName = "ID_NOTIFICACION")
    @ManyToOne
    private Notificacion notificacion;

    public InstanciaNotificacion() {
    }

    public InstanciaNotificacion(String idInstancia, String idMiembro) {
        this.instanciaNotificacionPK = new InstanciaNotificacionPK(idInstancia, idMiembro);
    }

    public InstanciaNotificacionPK getInstanciaNotificacionPK() {
        return instanciaNotificacionPK;
    }

    public void setInstanciaNotificacionPK(InstanciaNotificacionPK instanciaNotificacionPK) {
        this.instanciaNotificacionPK = instanciaNotificacionPK;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Date getFechaVisto() {
        return fechaVisto;
    }

    public void setFechaVisto(Date fechaVisto) {
        this.fechaVisto = fechaVisto;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Notificacion getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(Notificacion notificacion) {
        this.notificacion = notificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instanciaNotificacionPK != null ? instanciaNotificacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstanciaNotificacion)) {
            return false;
        }
        InstanciaNotificacion other = (InstanciaNotificacion) object;
        if ((this.instanciaNotificacionPK == null && other.instanciaNotificacionPK != null) || (this.instanciaNotificacionPK != null && !this.instanciaNotificacionPK.equals(other.instanciaNotificacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.InstanciaNotificacion[ instanciaNotificacionPK=" + instanciaNotificacionPK + " ]";
    }
    
}
