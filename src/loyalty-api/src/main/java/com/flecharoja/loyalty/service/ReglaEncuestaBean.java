package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.ListaReglas;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionEncuestaPregunta;
import com.flecharoja.loyalty.model.ReglaEncuesta;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import com.flecharoja.loyalty.util.ReglasSegmento;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * reglas de cambio de metrica
 *
 * @author svargas
 */
@Stateless
public class ReglaEncuestaBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    @EJB
    ElegibilidadMiembroBean elegibilidadBean;

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * @param idLista Identificador de la lista de reglas
     * @param locale
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List getReglasEncuestaLista(String idLista, Locale locale) {
        try {
            em.getReference(ListaReglas.class, idLista).getIdLista();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idLista");
        }

        return em.createNamedQuery("ReglaEncuesta.findByIdLista").setParameter("idLista", idLista).getResultList();
    }

    /**
     * Metodo que crea una nueva regla en una lista especifica
     *
     * @param idSegmento Identificador de segmento
     * @param idLista Identificador de la lista de reglas
     * @param regla Objeto con la informacion de la regla
     * @param usuario usuario en sesion
     * @param locale
     * @return Respuesta con el identificador de la regla creada
     */
    public String createReglaEncuesta(String idSegmento, String idLista, ReglaEncuesta regla, String usuario, Locale locale) {
        if (regla == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "ReglaEncuesta");
        }

        Date date = Calendar.getInstance().getTime();
        regla.setFechaCreacion(date);

        regla.setUsuarioCreacion(usuario);

        ListaReglas listaReglas = em.find(ListaReglas.class, idLista);
        if (listaReglas == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        if (!listaReglas.getIdSegmento().getIdSegmento().equals(segmento.getIdSegmento())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "segment_id_invalid");
        }
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        regla.setIdLista(idLista);

        //verificacion de algunos atributos requeridos
        MisionEncuestaPregunta pregunta = regla.getPregunta();
        if (pregunta==null || pregunta.getIdPregunta()==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "pregunta");
        }
        pregunta = em.find(MisionEncuestaPregunta.class, pregunta.getIdPregunta());
        if (pregunta==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "pregunta");
        }
        if (Mision.Estados.get(pregunta.getMision().getIndEstado())==Mision.Estados.BORRADOR) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_selected_cant_be_draft");
        }
        regla.setPregunta(pregunta);
        
        ReglasSegmento.ValoresIndOperador operador = ReglasSegmento.ValoresIndOperador.get(regla.getIndOperador());
        if (operador==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
        }
        if (regla.getValorComparacion()==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
        }
        switch (MisionEncuestaPregunta.TiposPregunta.get(pregunta.getIndTipoPregunta())) {
            case ENCUESTA: {
                switch (MisionEncuestaPregunta.TiposRespuesta.get(pregunta.getIndTipoRespuesta())) {
                    case RESPUESTA_ABIERTA_NUMERICO: {
                        switch (operador) {
                            case IGUAL:
                            case MAYOR:
                            case MAYOR_IGUAL:
                            case MENOR:
                            case MENOR_IGUAL:
                            {
                                break;
                            }
                            default: {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
                            }
                        }
                        break;
                    }
                    case RESPUESTA_ABIERTA_TEXTO: {
                        switch (operador) {
                            case CONTIENE:
                            case ES:
                            case ES_UNO_DE:
                            case INICIA_CON:
                            case NO_CONTIENE:
                            case NO_ES:
                            case NO_ES_UNO_DE:
                            case TERMINA_CON:
                            {
                                break;
                            }
                            default: {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
                            }
                        }
                        break;
                    }
                    case SELECCION_MULTIPLE: {
                        switch (operador) {
                            case OPCIONES_CUALQUIERA:
                            case OPCIONES_SON:
                            {
                                break;
                            }
                            default: {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
                            }
                        }
                        int cantOpciones = pregunta.getRespuestas().split("\n").length;
                        try {
                            if (Arrays.stream(regla.getValorComparacion().split("\n")).map((t) -> Integer.parseInt(t)).anyMatch((t) -> t<1 || t>cantOpciones)) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                            }
                        } catch (NumberFormatException e) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                        }
                        break;
                    }
                    case SELECCION_UNICA: {
                        switch (operador) {
                            case IGUAL:
                            case OPCIONES_CUALQUIERA:
                            {
                                break;
                            }
                            default: {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
                            }
                        }
                        int cantOpciones = pregunta.getRespuestas().split("\n").length;
                        try {
                            if (Arrays.stream(regla.getValorComparacion().split("\n")).map((t) -> Integer.parseInt(t)).anyMatch((t) -> t<1 || t>cantOpciones)) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                            }
                        } catch (NumberFormatException e) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                        }
                        break;
                    }
                }
                break;
            }
            case CALIFICACION: {
                switch (operador) {
                    case IGUAL:
                    case MAYOR:
                    case MAYOR_IGUAL:
                    case MENOR:
                    case MENOR_IGUAL:
                    {
                        break;
                    }
                    default: {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
                    }
                }
                int valorComparacion;
                try {
                    valorComparacion = Integer.parseInt(regla.getValorComparacion());
                } catch (NumberFormatException e) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                }
                if (valorComparacion>5 || valorComparacion<1) {
                        throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "valorComparacion");
                    }
                break;
            }
        }

        try {
            //almacenamiento de la regla
            em.persist(regla);
            em.flush();
            bitacoraBean.logAccion(ReglaEncuesta.class, regla.getIdRegla(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
            //actualizacion de la lista de reglas
            listaReglas.setUsuarioModificacion(usuario);
            listaReglas.setFechaModificacion(date);
            em.merge(listaReglas);
            bitacoraBean.logAccion(ListaReglas.class, listaReglas.getIdLista(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (ConstraintViolationException e) {//atributos no validos
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestRecalculoRegla(regla.getIdRegla(), ReglasSegmento.TiposReglasSegmento.ENCUESTA);
            } catch (Exception ex) {
                Logger.getLogger(ReglaEncuestaBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }

        return regla.getIdRegla();
    }

    /**
     * Metodo que elimina de una lista especifica una regla
     *
     * @param idSegmento Identificador de segmento
     * @param idLista Identificador de la lista de reglas
     * @param idRegla Identificador de la regla
     * @param usuario usuario en sesión
     * @param locale
     */
    public void deleteReglaEncuesta(String idSegmento, String idLista, String idRegla, String usuario, Locale locale) {
        Date date = Calendar.getInstance().getTime();

        ListaReglas listaReglas = em.find(ListaReglas.class, idLista);
        if (listaReglas == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }
        if (!listaReglas.getIdSegmento().getIdSegmento().equals(segmento.getIdSegmento())) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "segment_id_invalid");
        }
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Segmento");
        }

        try {
            //eliminacion de la regla (si es encontrada)
            em.remove(em.getReference(ReglaEncuesta.class, idRegla));

            em.flush();

            elegibilidadBean.deleteResultadoElegibilidadRegla(ReglasSegmento.TiposReglasSegmento.ENCUESTA, idRegla, Locale.getDefault());

            bitacoraBean.logAccion(ReglaEncuesta.class, idRegla, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
            //actualizacion de la lista de reglas
            listaReglas.setUsuarioModificacion(usuario);
            listaReglas.setFechaModificacion(date);
            em.merge(listaReglas);
            bitacoraBean.logAccion(ListaReglas.class, listaReglas.getIdLista(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (EntityNotFoundException e) {//lista de reglas o regla no encontrada
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rule_not_found");
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            try {
                myKafkaUtils.sendRequestActualizacionSegmento(idSegmento);
            } catch (Exception ex) {
                Logger.getLogger(ReglaEncuestaBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }
}
