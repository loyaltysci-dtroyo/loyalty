/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Preferencia;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.Busquedas;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * * EJB encargado de ejecutar reglas de negocio y acciones de persistencia
 * sobre preferencias
 *
 * @author wtencio
 */
@Stateless
public class PreferenciaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene una preferencia asociada al id que ingresa por
     * parametro
     *
     * @param idPreferencia identificador unico de la preferencia
     * @param locale
     * @return Objeto con la informacion de la preferencia
     */
    public Preferencia getPreferenciaPorId(String idPreferencia, Locale locale) {
        Preferencia preferencia = em.find(Preferencia.class, idPreferencia);
        if (preferencia == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "preference_not_found");
        }
        return preferencia;
    }

    /**
     * Metodo que obtiene una lista de preferencias almacenados por tipo de
     * respuesta
     *
     * @param tipoRespuesta Valores de los indicadores de los tipos de respuesta
     * a buscar
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return Lista de preferencias
     */
    public Map<String, Object> getPreferencias(List<String> tipoRespuesta, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int cantidad, int registro, Locale locale) {

        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Preferencia> root = query.from(Preferencia.class);

        query = Busquedas.getCriteriaQueryPreferencia(cb, query, root, tipoRespuesta, busqueda, filtros, ordenTipo, ordenCampo);

        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Método que registra una nueva preferencia en la base de datos
     *
     * @param preferencia objeto con la información a registrar
     * @param usuarioContext id del usuario que esta en sesion (usuario creador)
     * @param locale
     * @return id de la nueva preferencia que se registro
     */
    public String insertPreferencia(Preferencia preferencia, String usuarioContext, Locale locale) {
        try {
            //asignación de valores necesarios para la inserción
            preferencia.setFechaCreacion(Calendar.getInstance().getTime());
            preferencia.setUsuarioCreacion(usuarioContext);

            preferencia.setFechaModificacion(Calendar.getInstance().getTime());
            preferencia.setUsuarioModificacion(usuarioContext);

            preferencia.setNumVersion(new Long(1));

            if (preferencia.getIndTipoRespuesta().equalsIgnoreCase(Preferencia.Respuesta.TEXTO.getValue())) {
                preferencia.setRespuestas(null);
                em.persist(preferencia);
                bitacoraBean.logAccion(Preferencia.class, preferencia.getIdPreferencia(), usuarioContext, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
            } else if (preferencia.getIndTipoRespuesta().equalsIgnoreCase(Preferencia.Respuesta.RESPTA_MULTIPLE.getValue())
                    || preferencia.getIndTipoRespuesta().equalsIgnoreCase(Preferencia.Respuesta.SELECCION_UNICA.getValue())) {

                if (preferencia.getRespuestas() != null) {
                    em.persist(preferencia);
                    bitacoraBean.logAccion(Preferencia.class, preferencia.getIdPreferencia(), usuarioContext, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
                } else {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "respuestas");
                }
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indTipoRespuesta");
            }

        } catch (ConstraintViolationException e) {//atributos no validos
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        return preferencia.getIdPreferencia();
    }

    /**
     * Método que actualiza la información de una preferencia existente en la
     * base de datos
     *
     * @param preferencia objeto con la información a actualizar
     * @param usuarioContext id del usuario que esta en sesión
     * @param locale
     */
    public void updatePreferencia(Preferencia preferencia, String usuarioContext, Locale locale) {
        Preferencia temp = em.find(Preferencia.class, preferencia.getIdPreferencia());

        if (temp == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "preference_not_found");
        }

        //se actualiza ela fecha y el usuario modificacion            
        preferencia.setFechaModificacion(Calendar.getInstance().getTime());
        preferencia.setUsuarioModificacion(usuarioContext);

        try {
            if (preferencia.getIndTipoRespuesta().equalsIgnoreCase(Preferencia.Respuesta.TEXTO.getValue())) {
                preferencia.setRespuestas(null);
                em.merge(preferencia);
                em.flush();
                bitacoraBean.logAccion(Preferencia.class, preferencia.getIdPreferencia(), usuarioContext, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
            } else if (preferencia.getIndTipoRespuesta().equalsIgnoreCase(Preferencia.Respuesta.RESPTA_MULTIPLE.getValue())
                    || preferencia.getIndTipoRespuesta().equalsIgnoreCase(Preferencia.Respuesta.SELECCION_UNICA.getValue())) {

                if (preferencia.getRespuestas() != null) {
                    em.merge(preferencia);
                    em.flush();
                    bitacoraBean.logAccion(Preferencia.class, preferencia.getIdPreferencia(), usuarioContext, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
                } else {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "respuestas");
                }
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indTipoRespuesta");
            }

        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
    }

    /**
     * Método que elimina un registro de la base de datos asociado al id de la
     * preferencia que pasa por parametro
     *
     * @param idPreferencia identificador único de la preferencia que será
     * eliminada
     * @param usuarioContext usuario en sesión
     * @param locale
     */
    public void removePreferencia(String idPreferencia, String usuarioContext, Locale locale) {

        Preferencia preferencia = em.find(Preferencia.class, idPreferencia);
        if (preferencia == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "preference_not_found");
        }
        em.remove(preferencia);
        bitacoraBean.logAccion(Preferencia.class, preferencia.getIdPreferencia(), usuarioContext, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);

    }

}
