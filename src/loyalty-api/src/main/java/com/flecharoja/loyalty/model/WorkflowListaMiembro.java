package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "WORKFLOW_LISTA_MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WorkflowListaMiembro.findAll", query = "SELECT w FROM WorkflowListaMiembro w")
    , @NamedQuery(name = "WorkflowListaMiembro.findByIdMiembro", query = "SELECT w FROM WorkflowListaMiembro w WHERE w.workflowListaMiembroPK.idMiembro = :idMiembro")
    , @NamedQuery(name = "WorkflowListaMiembro.findByIdWorkflow", query = "SELECT w FROM WorkflowListaMiembro w WHERE w.workflowListaMiembroPK.idWorkflow = :idWorkflow")
    , @NamedQuery(name = "WorkflowListaMiembro.findByIndTipo", query = "SELECT w FROM WorkflowListaMiembro w WHERE w.indTipo = :indTipo")
    , @NamedQuery(name = "WorkflowListaMiembro.findByFechaCreacion", query = "SELECT w FROM WorkflowListaMiembro w WHERE w.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "WorkflowListaMiembro.findByUsuarioCreacion", query = "SELECT w FROM WorkflowListaMiembro w WHERE w.usuarioCreacion = :usuarioCreacion")})
public class WorkflowListaMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected WorkflowListaMiembroPK workflowListaMiembroPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @JoinColumn(name = "ID_WORKFLOW", referencedColumnName = "ID_WORKFLOW", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Workflow workflow;
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;

    public WorkflowListaMiembro() {
    }

    public WorkflowListaMiembro(WorkflowListaMiembroPK workflowListaMiembroPK) {
        this.workflowListaMiembroPK = workflowListaMiembroPK;
    }

    public WorkflowListaMiembro(WorkflowListaMiembroPK workflowListaMiembroPK, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.workflowListaMiembroPK = workflowListaMiembroPK;
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public WorkflowListaMiembro(String idMiembro, String idWorkflow) {
        this.workflowListaMiembroPK = new WorkflowListaMiembroPK(idMiembro, idWorkflow);
    }
    
    public WorkflowListaMiembro(String idMiembro, String idWorkflow, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.workflowListaMiembroPK = new WorkflowListaMiembroPK(idMiembro, idWorkflow);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public WorkflowListaMiembroPK getWorkflowListaMiembroPK() {
        return workflowListaMiembroPK;
    }

    public void setWorkflowListaMiembroPK(WorkflowListaMiembroPK workflowListaMiembroPK) {
        this.workflowListaMiembroPK = workflowListaMiembroPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Workflow getWorkflow() {
        return workflow;
    }

    public void setWorkflow(Workflow workflow) {
        this.workflow = workflow;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (workflowListaMiembroPK != null ? workflowListaMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkflowListaMiembro)) {
            return false;
        }
        WorkflowListaMiembro other = (WorkflowListaMiembro) object;
        if ((this.workflowListaMiembroPK == null && other.workflowListaMiembroPK != null) || (this.workflowListaMiembroPK != null && !this.workflowListaMiembroPK.equals(other.workflowListaMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.WorkflowListaMiembro[ workflowListaMiembroPK=" + workflowListaMiembroPK + " ]";
    }

}
