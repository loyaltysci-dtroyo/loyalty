/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CategoriaProducto;
import com.flecharoja.loyalty.model.Producto;
import com.flecharoja.loyalty.model.ProductoCategSubcateg;
import com.flecharoja.loyalty.model.ProductoCategSubcategPK;
import com.flecharoja.loyalty.model.SubcategoriaProducto;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.validation.ConstraintViolationException;

/**
 * EJB con los metodos de negocio para el manejo de asignacion de subcategorias
 * de una categoria de un producto
 *
 * @author wtencio
 */
@Stateless
public class ProductoCatSubcategoriaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de bitacora

    /**
     * Método que asigna un producto a una subcategoria de una categoria
     * especifica
     *
     * @param idProducto identificador del producto
     * @param idSubcategoria identificador de la subcategoria
     * @param idCategoria identificador de la categoria
     * @param usuario usuario en sesion
     * @param locale
     */
    public void assingProductoSubcategoriaCategoria(String idProducto, String idSubcategoria, String idCategoria, String usuario, Locale locale) {
        ProductoCategSubcateg productoCategSubcateg = new ProductoCategSubcateg(idProducto, idSubcategoria);

        try {
            Producto producto = em.find(Producto.class, idProducto);
            SubcategoriaProducto subcategoria = em.find(SubcategoriaProducto.class, idSubcategoria);
            CategoriaProducto categoria = em.find(CategoriaProducto.class, idCategoria);

            if (subcategoria.getIdCategoria().getIdCategoria().equals(categoria.getIdCategoria())) {
                productoCategSubcateg.setProducto(producto);
                productoCategSubcateg.setIdCategoria(categoria);
                productoCategSubcateg.setSubcategoriaProducto(subcategoria);
                productoCategSubcateg.setFechaCreacion(Calendar.getInstance().getTime());
                productoCategSubcateg.setUsuarioCreacion(usuario);

                em.persist(productoCategSubcateg);
                em.flush();
                bitacoraBean.logAccion(ProductoCategSubcateg.class, idProducto + "/" + idCategoria, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idCategoria");
            }

        } catch (EntityExistsException e) {
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "product_subcategory_producto_already_exist");
        } catch (ConstraintViolationException e) {//atributos no validos
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que elimina la relacion de un producto con una categoria y una
     * subcategoria de esta
     *
     * @param idProducto identificador del producto
     * @param idSubcategoria identificador de la subcategoria
     * @param usuario usuario en sesion
     * @param locale
     */
    public void unassingProductoSubcategoriaCategoria(String idProducto, String idSubcategoria, String usuario, Locale locale) {
        ProductoCategSubcateg productoCategSubcateg = (ProductoCategSubcateg) em.createNamedQuery("ProductoCategSubcateg.findByIdProductoByIdSubcategoria").setParameter("idProducto", idProducto).setParameter("idSubcategoria", idSubcategoria).getSingleResult();
        if (productoCategSubcateg == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "Product relationship with subcategory not found");
        }

        em.remove(productoCategSubcateg);
        bitacoraBean.logAccion(ProductoCategSubcateg.class, idProducto + "/" + idSubcategoria, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

    /**
     * Método que asigna una categoria y una subcategoria a una lista de
     * productos
     *
     * @param idsProducto lista de identificadores de los producto
     * @param idSubcategoria identificador de la subcategoria
     * @param idCategoria identificador de la categoria
     * @param usuario usuario en sesion
     * @param locale
     */
    public void assingBatchProductoSubcategoriaCategoria(List<String> idsProducto, String idSubcategoria, String idCategoria, String usuario,Locale locale) {
        CategoriaProducto categoriaProducto = em.find(CategoriaProducto.class, idCategoria);
        
        if(categoriaProducto==null){
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_category_not_found");
        }

        for (String idProducto : idsProducto) {
            //se verifica que el producto exista
            Producto producto = em.find(Producto.class, idProducto);
            if (producto == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
            }
            //verifica que el producto a asignar a la categoria, no tenga un estado de borrador, ni archivado
            if (producto.getIndEstado().compareTo(Producto.Estados.ARCHIVADO.getValue()) == 0) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Producto");
            }
        }

        //fecha de creacion
        Date date = Calendar.getInstance().getTime();

        try {
            idsProducto.stream().forEach((idProducto) -> {
                em.persist(new ProductoCategSubcateg(idProducto, idSubcategoria, categoriaProducto, date, usuario));
            });
            em.flush();
        } catch (EntityExistsException e) {//en el caso de que ya exista la entidad en la bases de datos
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "product_subcategory_producto_already_exist");
        } catch (ConstraintViolationException e) {//atributos no validos
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        idsProducto.forEach((idProducto) -> {
            bitacoraBean.logAccion(ProductoCategSubcateg.class, idSubcategoria + "/" + idProducto, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        });

    }

    /**
     * Metodo que elimina la asignacion de una lista de productos a una
     * categoria y subcategoria de producto por sus identificadores
     *
     * @param idSubcategoria Identificador de la subcategoria
     * @param listaIdProductos Lista de identificadores de productos
     * @param usuario usuario en sesion
     * @param locale
     */
    public void unassingBatchProductoSubcategoriaCategoria(String idSubcategoria, List<String> listaIdProductos, String usuario, Locale locale) {
        try {
            listaIdProductos.stream().forEach((idProducto) -> {
                //se intenta remover la entidad a partir de la referencia de la misma por sus identificadores
                em.remove(em.getReference(ProductoCategSubcateg.class, new ProductoCategSubcategPK(idProducto, idSubcategoria)));
            });
            em.flush();
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
             throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "Product relationship with subcategory not found");
        }

        listaIdProductos.forEach((idProducto) -> {
            bitacoraBean.logAccion(ProductoCategSubcateg.class, idSubcategoria + "/" + idProducto, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        });

    }

    /**
     * Método que devuelve una lista de subacategorias pertenecientes a un
     * producto en particular
     *
     * @param idProducto identificador del producto
     * @param cantidad de registros maximos por pagina
     * @param registro por el cual se desea empezar la busqueda
     * @param locale
     * @return listado de subcategorias de un producto
     */
    public Map<String, Object> getSubcategoriasProducto(String idProducto, int cantidad, int registro, Locale locale) {
        Map<String, Object> respuesta = new HashMap<>();
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        List resultado = new ArrayList();
        List registros = new ArrayList();

        int total;
        try {

            registros = em.createNamedQuery("ProductoCategSubcateg.findByIdProducto").setParameter("idProducto", idProducto).getResultList();
            total = registros.size();
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createNamedQuery("ProductoCategSubcateg.findByIdProducto").setParameter("idProducto", idProducto)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO,locale, "database_connection_failed");
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Metodo que obtiene un listado de productos que no estan ligados a una
     * categoria y subcategoria de producto, especificado por su identificador y
     * en un rango definido por sus parametros
     *
     * @param idSubcategoria Identificador de la subcategoria
     * @param registro Parametro que indica el primer resultado desde donde
     * retornar los resultados
     * @param cantidad Parametro que indica la cantidad de resultados en la
     * lista
     * @param locale
     * @return Respuesta con la lista encapsulada y encabezados acerca del rango
     * de respuesta y su total y rango maximo aceptado o estado erroneo y
     * encabezado con el codigo de error
     */
    public Map<String, Object> getNoProductosPorIdSubcategoriaCategoria(String idSubcategoria, int registro, int cantidad, Locale locale) {
        Map<String, Object> respuesta = new HashMap<>();
        //verificacion de que la entidad exista
        try {
            em.getReference(SubcategoriaProducto.class, idSubcategoria).getIdCategoria();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_subcategory_not_found");
        }

        //verifica que la cantidad en la peticion sea menor a la aceptada, de no ser asi se retorna una respuesta informando acerca de ello, junto con el encabezado de rango maximo
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }

        List registros = new ArrayList();
        int total;
        List resultado;
        try {
            //se obtiene el total de registros
            registros = em.createNamedQuery("ProductoCategSubcateg.findProductoNotInSubcategoria").setParameter("idSubcategoria", idSubcategoria).getResultList();
            total = registros.size();
            total -= total - 1 < 0 ? 0 : 1;

            //ve que si en el caso que el registro sea mayor que el total, este se reduce por cantidad (mientras que no sea <0) hasta que este entre un valor de 0 a total
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            //se obtiene el listado usando los parametros de registro y cantidad para limitar el rango de respuesta
            resultado = em.createNamedQuery("ProductoCategSubcateg.findProductoNotInSubcategoria")
                    .setParameter("idSubcategoria", idSubcategoria)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();
        } catch (IllegalArgumentException e) {//en el caso que los valores de paginacion esten erroneos
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        //se establece la respuesta encapsulando el listado y estableciendo los encabezados de rango de la respuesta y rango aceptado
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * Método que obtiene la lista de categorias no asociadas a un producto
     *
     * @param idProducto identificador del producto
     * @param cantidad de registros maximos por pagina
     * @param registro por el cual se comienza la busqueda
     * @param locale
     * @return lista de categorias
     */
    public Map<String, Object> getCategoriaNotInProducto(String idProducto, int cantidad, int registro, Locale locale) {
        Map<String, Object> respuesta = new HashMap<>();
        try {
            em.getReference(Producto.class, idProducto).getIdProducto();
        } catch (EntityNotFoundException e) {
           throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
        }

        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
           throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }

        List registros = new ArrayList();
        int total;
        List resultado = new ArrayList();

        try {
            registros = em.createNamedQuery("ProductoCategSubcateg.findCategoriaNotInProducto").setParameter("idProducto", idProducto).getResultList();
            total = registros.size();
            total -= total - 1 < 0 ? 0 : 1;

            //ve que si en el caso que el registro sea mayor que el total, este se reduce por cantidad (mientras que no sea <0) hasta que este entre un valor de 0 a total
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            resultado = em.createNamedQuery("ProductoCategSubcateg.findCategoriaNotInProducto")
                    .setParameter("idProducto", idProducto)
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado.stream()
                .collect(Collectors.groupingBy(CategoriaProducto::getIdCategoria)));

        return respuesta;
    }

    /**
     * Método que devuelve una lista de subcategorias disponibles para un
     * producto especifico en una categoria especifica
     *
     * @param idProducto identificador del producto
     * @param idCategoria identificador de la categoria
     * @param cantidad de registros maximos por pagina
     * @param registro por el cual se inicia la busqueda
     * @param locale
     * @return listado de subcategorias
     */
    public Map<String, Object> getSubCategoriasDisponiblesProducto(String idProducto, String idCategoria, int cantidad, int registro, Locale locale) {
        Map<String, Object> respuesta = new HashMap<>();
        try {
            em.getReference(Producto.class, idProducto);
            em.getReference(CategoriaProducto.class, idCategoria);
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "product_not_found");
        }

        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }

        List lista = new ArrayList();
        List<SubcategoriaProducto> resultado = new ArrayList();
        int total;
        try {
            //obtengo la lista de ids de subcategorias que ya tiene el producto con la categoria que ingresan por parametro
            lista = em.createNamedQuery("ProductoCategSubcateg.findSubcategNoDisponibles").setParameter("idProducto", idProducto).setParameter("idCategoria", idCategoria).getResultList();
            //si la lista no esta vacia se buscan las subcategorias cuyos id's no se encuentren en la lista anterior
            if (!lista.isEmpty()) {
                resultado = em.createNamedQuery("SubcategoriaProducto.findDisponibleProducto").setParameter("lista", lista).setParameter("idCategoria", idCategoria).getResultList();
                //si la lista esta vacia se devuelven todas las subcategorias de la categoria que ingresa por parametro
            } else {
                resultado = em.createNamedQuery("SubcategoriaProducto.findAll").setParameter("idCategoria", idCategoria).getResultList();
            }
            total = resultado.size();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado.subList(registro, registro + cantidad > resultado.size() ? resultado.size() : registro + cantidad));
        return respuesta;
    }
}
