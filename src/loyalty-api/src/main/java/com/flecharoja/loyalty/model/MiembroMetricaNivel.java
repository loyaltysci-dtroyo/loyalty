package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio, svargas
 */
@Entity
@Table(name = "MIEMBRO_METRICA_NIVEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MiembroMetricaNivel.findAll", query = "SELECT m FROM MiembroMetricaNivel m"),
    @NamedQuery(name = "MiembroMetricaNivel.findByIdMiembro", query = "SELECT m.idNivel, m.metrica FROM MiembroMetricaNivel m WHERE m.miembroMetricaNivelPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroMetricaNivel.findByFechaCreacion", query = "SELECT m FROM MiembroMetricaNivel m WHERE m.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "MiembroMetricaNivel.findByUsuarioCreacion", query = "SELECT m FROM MiembroMetricaNivel m WHERE m.usuarioCreacion = :usuarioCreacion"),
    @NamedQuery(name = "MiembroMetricaNivel.findByIdMetrica", query = "SELECT m FROM MiembroMetricaNivel m WHERE m.miembroMetricaNivelPK.idMetrica = :idMetrica"),
    @NamedQuery(name = "MiembroMetricaNivel.findByIdMetricaByIdMiembro", query= "SELECT m FROM MiembroMetricaNivel m WHERE m.miembroMetricaNivelPK.idMetrica = :idMetrica AND m.miembroMetricaNivelPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroMetricaNivel.findByIdNivel", query = "SELECT m FROM MiembroMetricaNivel m WHERE m.idNivel.idNivel = :idNivel")
})
public class MiembroMetricaNivel implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MiembroMetricaNivelPK miembroMetricaNivelPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Metrica metrica;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;
    
    @JoinColumn(name = "ID_NIVEL", referencedColumnName = "ID_NIVEL")
    @ManyToOne(optional = false)
    private NivelMetrica idNivel;

    public MiembroMetricaNivel() {
    }

    public MiembroMetricaNivel(MiembroMetricaNivelPK miembroMetricaNivelPK) {
        this.miembroMetricaNivelPK = miembroMetricaNivelPK;
    }

    public MiembroMetricaNivel(MiembroMetricaNivelPK miembroMetricaNivelPK, Date fechaCreacion, String usuarioCreacion) {
        this.miembroMetricaNivelPK = miembroMetricaNivelPK;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public MiembroMetricaNivel(String idMiembro, String idMetrica) {
        this.miembroMetricaNivelPK = new MiembroMetricaNivelPK(idMiembro, idMetrica);
    }

    @ApiModelProperty(hidden = true)
    public MiembroMetricaNivelPK getMiembroMetricaNivelPK() {
        return miembroMetricaNivelPK;
    }

    public void setMiembroMetricaNivelPK(MiembroMetricaNivelPK miembroMetricaNivelPK) {
        this.miembroMetricaNivelPK = miembroMetricaNivelPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public NivelMetrica getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(NivelMetrica idNivel) {
        this.idNivel = idNivel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (miembroMetricaNivelPK != null ? miembroMetricaNivelPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiembroMetricaNivel)) {
            return false;
        }
        MiembroMetricaNivel other = (MiembroMetricaNivel) object;
        if ((this.miembroMetricaNivelPK == null && other.miembroMetricaNivelPK != null) || (this.miembroMetricaNivelPK != null && !this.miembroMetricaNivelPK.equals(other.miembroMetricaNivelPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MiembroMetricaNivel[ miembroMetricaNivelPK=" + miembroMetricaNivelPK + " ]";
    }
    
}
