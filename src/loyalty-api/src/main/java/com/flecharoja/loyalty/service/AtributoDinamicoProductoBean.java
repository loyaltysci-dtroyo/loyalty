/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.AtributoDinamicoProducto;
import com.flecharoja.loyalty.model.Bitacora;;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * atributos dinámicos de productos
 *
 * @author wtencio
 */
@Stateless
public class AtributoDinamicoProductoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos para el manejo de bitacora

    /**
     * Método que registra en la base de datos un nuevo atributo dinámico de un
     * producto
     *
     * @param atributoProducto informacion del atributo
     * @param usuario usuario en sesion
     * @param locale
     * @return identificador del atributo registrado
     */
    public String insertAtributoProducto(AtributoDinamicoProducto atributoProducto, String usuario, Locale locale) {

        if (atributoProducto.getIndTipo().equals(AtributoDinamicoProducto.Tipos.OPCIONES.getValue()) && atributoProducto.getValorOpciones() == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "valorOpciones");
        }
        //Se valida que el nombre de despliegue no exista
        if (existeNombreInterno(atributoProducto.getNombreInterno())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "internal_name_exists");
        }

        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (atributoProducto.getImagen() == null || atributoProducto.getImagen().trim().isEmpty()) {
            atributoProducto.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a s3
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                atributoProducto.setImagen(filesUtils.uploadImage(atributoProducto.getImagen(), MyAwsS3Utils.Folder.ARTE_ATRIBUTO_DINAMICO_PRODUCTO, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }

        //definicion de atributos por defecto
        atributoProducto.setIndEstado(AtributoDinamicoProducto.Estados.BORRADOR.getValue());
        atributoProducto.setFechaCreacion(Calendar.getInstance().getTime());
        atributoProducto.setUsuarioCreacion(usuario);
        atributoProducto.setFechaModificacion(Calendar.getInstance().getTime());
        atributoProducto.setUsuarioModificacion(usuario);
        atributoProducto.setNumVersion(new Long(1));

        try {
            em.persist(atributoProducto);
            em.flush();
            bitacoraBean.logAccion(AtributoDinamicoProducto.class, atributoProducto.getIdAtributo(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            //verificacion que la imagen establecida no sea la predefinida, de no serla... eliminarla
            if (!atributoProducto.getImagen().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(atributoProducto.getImagen());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));

        }

        return atributoProducto.getIdAtributo();
    }

    /**
     * Método que actualiza la informacion de un atributo ya existente
     *
     * @param atributoProducto informacion del atributo
     * @param usuario usuario en sesion
     * @param locale
     */
    public void updateAtributoProducto(AtributoDinamicoProducto atributoProducto, String usuario, Locale locale) {

        AtributoDinamicoProducto original = em.find(AtributoDinamicoProducto.class, atributoProducto.getIdAtributo());

        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "dynamic_attribute_product_invalid");
        }

        if (AtributoDinamicoProducto.Estados.get(atributoProducto.getIndEstado()) == null || AtributoDinamicoProducto.Tipos.get(atributoProducto.getIndTipo()) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indEstado, indTipo");
        }

        //verificacion de cambio de estado ilegal
        if (original.getIndEstado().equals(AtributoDinamicoProducto.Estados.PUBLICADO.getValue()) && atributoProducto.getIndEstado().equals(AtributoDinamicoProducto.Estados.BORRADOR.getValue())
                || original.getIndEstado().equals(AtributoDinamicoProducto.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", this.getClass().getSimpleName());
        }

        //verificacion de valores
        if (atributoProducto.getIndTipo().equals(AtributoDinamicoProducto.Tipos.OPCIONES.getValue()) && atributoProducto.getValorOpciones() == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "valorOpciones");
        }
        if(!original.getNombreInterno().equals(atributoProducto.getNombreInterno())){
            if (existeNombreInterno(atributoProducto.getNombreInterno())) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "internal_name_exists");
            }
        }

        String urlImagenOriginal = original.getImagen();//url imagen original
        String urlImagenNueva = null;//url imagen nueva
        //si el atributo de imagen viene nula (no deberia)
        if (atributoProducto.getImagen() == null || atributoProducto.getImagen().trim().isEmpty()) {
            //establecimiento de la imagen por defecto
            urlImagenNueva = Indicadores.URL_IMAGEN_PREDETERMINADA;
            atributoProducto.setImagen(urlImagenNueva);
        } else {
            //ver si el valor en el atributo de imagen, difiere del almacenado
            if (!urlImagenOriginal.equals(atributoProducto.getImagen())) {//si la imagen es diferente a la almacenada
                //se le intenta subir
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    urlImagenNueva = filesUtils.uploadImage(atributoProducto.getImagen(), MyAwsS3Utils.Folder.ARTE_ATRIBUTO_DINAMICO_PRODUCTO, null);
                    atributoProducto.setImagen(urlImagenNueva);
                } catch (Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                    throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
                }
            }
        }

        atributoProducto.setFechaModificacion(Calendar.getInstance().getTime());
        atributoProducto.setUsuarioModificacion(usuario);

        try {
            em.merge(atributoProducto);
            em.flush();
            bitacoraBean.logAccion(AtributoDinamicoProducto.class, atributoProducto.getIdAtributo(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);

            if (urlImagenNueva != null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenOriginal);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (urlImagenNueva != null) { //si se guardo una nueva imagen, se elimina
                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                    filesUtils.deleteFile(urlImagenNueva);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                }
            }
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }

        }

    }

    /**
     * Método que elimina o desactiva un atributo dinamico de productos
     *
     * @param idAtributo identificador del atributo
     * @param usuario usuario en sesion
     */
    public void removeAtributoProducto(String idAtributo, String usuario, Locale locale) {
        AtributoDinamicoProducto atributo = em.find(AtributoDinamicoProducto.class, idAtributo);

        if (atributo == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "dynamic_attribute_product_invalid");
        }

        //obtiene un valor booleano si existe alguna asociacion de producto con el atributo
        Boolean existeRelacion = (Long) (em.createNamedQuery("ProductoAtributo.countByIdAtributo").setParameter("idAtributo", idAtributo).getSingleResult()) > 0;
        //si existe un relacion
        if (existeRelacion) {
            //verifica que el estado no sea archivado de lo contrario manda un error
            if (atributo.getIndEstado().equals(AtributoDinamicoProducto.Estados.ARCHIVADO.getValue())) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", this.getClass().getSimpleName());
            }
            //si no es archivado el estado actual lo manda a archivar
            atributo.setIndEstado(AtributoDinamicoProducto.Estados.ARCHIVADO.getValue());
            em.merge(atributo);
            em.flush();
            bitacoraBean.logAccion(AtributoDinamicoProducto.class, idAtributo, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
        } else {
            //se obtiene el url de la imagen almacenada de la entidad y borra
            String url = atributo.getImagen();
            //si no existe relacion y esta en estado borrador lo elimina de la base
            if (atributo.getIndEstado().equals(AtributoDinamicoProducto.Estados.BORRADOR.getValue())) {

                if (!url.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                    try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                        filesUtils.deleteFile(url);
                    } catch (Exception ex) {
                        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                    }
                }
                em.remove(atributo);
                em.flush();
                bitacoraBean.logAccion(AtributoDinamicoProducto.class, idAtributo, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
                //sino si el estado es publicado lo manda a archivar
            } else if (atributo.getIndEstado().equals(AtributoDinamicoProducto.Estados.PUBLICADO.getValue())) {
                atributo.setIndEstado(AtributoDinamicoProducto.Estados.ARCHIVADO.getValue());
                em.merge(atributo);
                em.flush();
                bitacoraBean.logAccion(AtributoDinamicoProducto.class, idAtributo, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(), locale);
            }
        }
    }

    /**
     * Método que obtiene la informacion del atributo de producto que pasa por
     * parametro
     *
     * @param idAtributo identificador del atributo
     * @return resultado de la operacion
     */
    public AtributoDinamicoProducto getAtributoProductoPorId(String idAtributo, Locale locale) {
        AtributoDinamicoProducto atributo = em.find(AtributoDinamicoProducto.class, idAtributo);
        if (atributo == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "dynamic_attribute_product_invalid");
        }

        return atributo;
    }

    /**
     * Metodo que obtiene una lista de atributos dinamicos de productos
     * almacenados por tipo de dato del atributo(texto, numerico...)
     *
     * @param estados
     * @param tipo
     * @param estado Valores de los indicadores de los tipos de datos a buscar
     * @param indRequerido
     * @param filtros
     * @param busqueda
     * @param ordenCampo
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @param ordenTipo
     * @return Lista de atributos dinamicos
     */
    public Map<String, Object> getAtributos(List<String> estados,  List<String> tipo, String indRequerido, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo,int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<AtributoDinamicoProducto> root = query.from(AtributoDinamicoProducto.class);

        query = Busquedas.getCriteriaQueryAtributoDinamicoProducto(cb, query, root, estados, tipo, indRequerido, busqueda, filtros, ordenTipo, ordenCampo);
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }
    
    /** 
     * Método que verifica si el nombre interno existe
     */
    public boolean existeNombreInterno(String nombre) {
        return ((Long) em.createNamedQuery("AtributoDinamicoProducto.existsNombreInterno").setParameter("nombreInterno", nombre).getSingleResult()) > 0;
    }
}
