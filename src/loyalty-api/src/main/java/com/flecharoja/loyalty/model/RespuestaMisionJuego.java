package com.flecharoja.loyalty.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@XmlRootElement
public class RespuestaMisionJuego {
    private Juego detalleMisionJuego;

    public RespuestaMisionJuego() {
    }

    public Juego getDetalleMisionJuego() {
        return detalleMisionJuego;
    }

    public void setDetalleMisionJuego(Juego detalleMisionJuego) {
        this.detalleMisionJuego = detalleMisionJuego;
    }

}
