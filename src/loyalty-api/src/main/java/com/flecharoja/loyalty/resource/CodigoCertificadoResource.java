/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CodigoCertificado;
import com.flecharoja.loyalty.service.CodigoCertificadoBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * clase der recursos http RESTful para el manejo de codigo de certificado
 * @author wtencio
 */
@Api(value = "Premio")
@Path("premio/{idPremio}/certificado")
public class CodigoCertificadoResource {
    
    @Context 
    SecurityContext context;//permite obtener informacion del usuario en sesión
    
    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizaciones
    
    @EJB
    CodigoCertificadoBean codigoCertificadoBean;//EJB con los metodos de negocio para el manejo de codigo de certificados
    
    @Context
    HttpServletRequest request;
    
    /**
     * Identificador de premio
     */
    @PathParam("idPremio")
    String idPremio;
     /**
     * Metodo que registra la informacion de un nuevo certificado, de ocurrir un error se retornara
     * una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param codigo
     */
    @ApiOperation(value = "Registrar certificado de un premio",
            response = String.class)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idPremio", value = "Identificador de premio", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public void insertCodigoCertificado(
            @ApiParam(value = "Codigo del certificado", required = true) 
                    String codigo) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        codigoCertificadoBean.insertCodigo(idPremio,codigo, usuarioContext, request.getLocale());
    }
    
     /**
     * Metodo que elimina de ser posible un codigo de certificado, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idCertificado Identificador del certificado
     */
    @ApiOperation(value = "Eliminacion de un certificado")
     @ApiImplicitParams({
        @ApiImplicitParam(name = "idPremio", value = "Identificador de premio", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{idCertificado}")
    public void deleteCertificado(
            @ApiParam(value = "Identificador de premio", required = true)
            @PathParam("idCertificado") String idCertificado) {
        
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        codigoCertificadoBean.deleteCodigo(idPremio,idCertificado,usuarioContext,request.getLocale());
    }
    
    /**
     * Metodo que anula un codigo de certificado, de
     * ocurrir un error se retornara una respuesta con un diferente estado junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param idCertificado Identificador del certificado
     */
    @ApiOperation(value = "Anulación de un certificado")
     @ApiImplicitParams({
        @ApiImplicitParam(name = "idPremio", value = "Identificador de premio", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{idCertificado}/anular")
    public void anularCertificado(
            @ApiParam(value = "Identificador de premio", required = true)
            @PathParam("idCertificado") String idCertificado) {
        
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.ANULAR_CERTIFICADO, token,request.getLocale()) || !authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        codigoCertificadoBean.anularCodigo(idPremio,idCertificado,usuarioContext,request.getLocale());
    }
    
    /**
     * Metodo que obtiene un listado de premios por un rango establecido
     * usando los parametros de cantidad y registro y opcionalmente filtrados
     * por indicadores de estado contenidos dentro del parametro de estado (por
     * defecto todos) ademas de encabezados indicando del rango aceptable maximo
     * y el rango actual de resultados, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param estados Parametro opcional que contiene los estados de los
     *  certificados deseados
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Respuesta con el listado de los certificados en un rango valido
     * junto con encabezados sobre el rango actual
     */
    @ApiOperation(value = "Obtener lista de certificadoe",
            responseContainer = "List",
            response = CodigoCertificado.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
     @ApiImplicitParams({
        @ApiImplicitParam(name = "idPremio", value = "Identificador de premio", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCertificados(
            @ApiParam(value = "Indicadores de estado de Promociones")
            @DefaultValue("null") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {
        
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta;
        respuesta = codigoCertificadoBean.getCodigosCertificadosPorPremio(idPremio,estados,cantidad, registro,request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }
    
     /**
     * Metodo que autoregistra certificados, de ocurrir un error se retornara
     * una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param cantidad de codigos a generar
     */
    @ApiOperation(value = "Autogeneracion de certificados de un premio")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idPremio", value = "Identificador de premio", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("generar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.TEXT_PLAIN)
    public void generarCodigos(
            @ApiParam(value = "Cantidad de certificados a registrar", required = true) 
            @QueryParam("cantidad") Long cantidad) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        codigoCertificadoBean.generarCodigosAuto(cantidad, idPremio, usuarioContext, request.getLocale());
    }
    
}
