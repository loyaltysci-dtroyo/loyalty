package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NOTICIA_LISTA_SEGMENTO")
public class NoticiaListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected NoticiaListaSegmentoPK noticiaListaSegmentoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_NOTICIA", referencedColumnName = "ID_NOTICIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Noticia noticia;
    
    @JoinColumn(name = "ID_SEGMENTO", referencedColumnName = "ID_SEGMENTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Segmento segmento;

    public NoticiaListaSegmento() {
    }

    public NoticiaListaSegmento(String idSegmento, String idNoticia, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.noticiaListaSegmentoPK = new NoticiaListaSegmentoPK(idSegmento, idNoticia);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public NoticiaListaSegmentoPK getNoticiaListaSegmentoPK() {
        return noticiaListaSegmentoPK;
    }

    public void setNoticiaListaSegmentoPK(NoticiaListaSegmentoPK noticiaListaSegmentoPK) {
        this.noticiaListaSegmentoPK = noticiaListaSegmentoPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo!=null?Character.toUpperCase(indTipo):null;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Noticia getNoticia() {
        return noticia;
    }

    public void setNoticia(Noticia noticia) {
        this.noticia = noticia;
    }

    public Segmento getSegmento() {
        return segmento;
    }

    public void setSegmento(Segmento segmento) {
        this.segmento = segmento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noticiaListaSegmentoPK != null ? noticiaListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NoticiaListaSegmento)) {
            return false;
        }
        NoticiaListaSegmento other = (NoticiaListaSegmento) object;
        if ((this.noticiaListaSegmentoPK == null && other.noticiaListaSegmentoPK != null) || (this.noticiaListaSegmentoPK != null && !this.noticiaListaSegmentoPK.equals(other.noticiaListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.NoticiaListaSegmento[ noticiaListaSegmentoPK=" + noticiaListaSegmentoPK + " ]";
    }
    
}
