/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.util;

/**
 *
 * @author wtencio
 */
public class IndicadoresBitacora {
    

    public static final String ENTIDAD_INSERTADA = "A";
    public static final String ENTIDAD_ACTUALIZA = "B";
    public static final String ENTIDAD_ELIMINADA = "C";
    public static final String ENTIDAD_PUBLICADA = "D";
    public static final String ENTIDAD_ARCHIVADA = "E";
    public static final String ENTIDAD_DESACTIVADA = "F";
    public static final String ENTIDAD_ACTIVADA = "G";
    public static final String ENTIDAD_DRAFT = "H";
    
}
