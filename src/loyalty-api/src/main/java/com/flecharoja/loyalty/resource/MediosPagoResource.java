/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.MediosPago;
import com.flecharoja.loyalty.service.MediosPagoBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * clase de recursos http RESTful para el manejo de medios de pago
 * @author wtencio
 */
@Api(value = "Medios de pago")
@Path("medios-pago")
public class MediosPagoResource {
    
    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizaciones
    
    @EJB
    MediosPagoBean mediosPagoBean;//EJB con los metodos de negocio para el manejo de medios de pago
    
    @Context
    SecurityContext context;
    
    @Context
    HttpServletRequest request;
    
    /**
     * Metodo que acciona una tarea, la cual registra un nuevo medio de pago en
     * la base de datos con la información que recibe retornando un OK(200), de ocurrir un error se
     * retornara una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param medioPago objeto con la información
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Registrar un medio de pago",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void insertMedioPAgo(
            @ApiParam(value = "Información del medio de pago", required = true) MediosPago medioPago) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CONFIGURACION_GENERAL, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        mediosPagoBean.insertMedioPago(medioPago, usuarioContext, request.getLocale());
    }
    
    
    /**
     * Metodo que acciona una tarea, la cual borra un medio de pago dentro
     * de la base de datos relacionada al id que ingresa como parametro en
     * OK(200), de ocurrir un error se retornara una respuesta con un diferente
     * estado junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param idMedio identificador unico del medio de pago que se quiere
     * eliminar
     * @return Resultado de la operación
     */
    @ApiOperation(value = "Eliminación de un medio de pago")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idMedio}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeMedioPago(
            @ApiParam(value = "Identificador de un atributo dinámico", required = true)
            @PathParam("idMedio") String idMedio) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CONFIGURACION_GENERAL, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        mediosPagoBean.eliminarMedioPago(idMedio, usuarioContext,request.getLocale());
    }
    
    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de todos los
     * medios de pago definidospor un rango establecido usando los parametros de
     * cantidad y registro con un resultado de OK(200), de ocurrir un error se retornara una respuesta
     * con un diferente estado junto con un encabezado "Error-Reason" con un
     * valor numerico indicando la naturaleza del error.
     *
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Representacion del listado en formato JSON
     */
    @ApiOperation(value = "Obtener medios de pago",
            responseContainer = "List",
            response = MediosPago.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMediosPago(
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO)
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial")
            @QueryParam("registro") int registro) {

        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CONFIGURACION_GENERAL, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = mediosPagoBean.getMediosPago(cantidad,registro,request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();

    }
}
