package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CategoriaMision;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionCategoriaMision;
import com.flecharoja.loyalty.model.MisionCategoriaMisionPK;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

/**
 * Clase encargada de proveer metodos para el mantenimiento de listados y
 * asignaciones de misiones con categorias de mision asi como verificaciones y
 * manejo de datos
 *
 * @author svargas
 */
@Stateless
public class MisionCategoriaMisionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene un listado de mision que esten ligados a una categoria
     * de promocion, especificado por su identificador y en un rango definido
     * por sus parametros
     *
     * @param idCategoria Identificador de la categoria
     * @param registro Parametro que indica el primer resultado desde donde
     * retornar los resultados
     * @param cantidad Parametro que indica la cantidad de resultados en la
     * lista
     * @param locale
     * @return Respuesta con la lista encapsulada y encabezados acerca del rango
     * de respuesta y su total y rango maximo aceptado o estado erroneo y
     * encabezado con el codigo de error
     */
    public Map<String, Object> getMisionesPorIdCategoria(String idCategoria, int registro, int cantidad,Locale locale) {
        //verificacion de que la entidad exista
        if (em.find(CategoriaMision.class, idCategoria) == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_category_not_found");
        }

        //verifica que la cantidad en la peticion sea menor a la aceptada, de no ser asi se retorna una respuesta informando acerca de ello, junto con el encabezado de rango maximo
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        Map<String, Object> respuesta = new HashMap<>();

        Long total;
        List resultado;
        try {
            //se obtiene el total de registros
            total = ((Long) em.createNamedQuery("MisionCategoriaMision.countMisionesByIdCategoria")
                    .setParameter("idCategoria", idCategoria)
                    .getSingleResult());
            total -= total - 1 < 0 ? 0 : 1;

            //ve que si en el caso que el registro sea mayor que el total, este se reduce por cantidad (mientras que no sea <0) hasta que este entre un valor de 0 a total
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            //se obtiene el listado usando los parametros de registro y cantidad para limitar el rango de respuesta
            resultado = em.createNamedQuery("MisionCategoriaMision.findMisionesByIdCategoria")
                    .setParameter("idCategoria", idCategoria)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();
        } catch (IllegalArgumentException e) {//en el caso que los valores de paginacion esten erroneos
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        //se establece la respuesta encapsulando el listado y estableciendo los encabezados de rango de la respuesta y rango aceptado
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * Metodo que obtiene un listado de categorias de misiones que esten ligados
     * a una mision, especificado por su identificador y en un rango definido
     * por sus parametros
     *
     * @param idMision Identificador de la mision
     * @param registro Parametro que indica el primer resultado desde donde
     * retornar los resultados
     * @param cantidad Parametro que indica la cantidad de resultados en la
     * lista
     * @param locale
     * @return Respuesta con la lista encapsulada y encabezados acerca del rango
     * de respuesta y su total y rango maximo aceptado o estado erroneo y
     * encabezado con el codigo de error
     */
    public Map<String, Object> getCategoriasPorIdMision(String idMision, int registro, int cantidad, Locale locale) {
        //verificacion de que la entidad exista
        if (em.find(Mision.class, idMision) == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_category_not_found");
        }

        //verifica que la cantidad en la peticion sea menor a la aceptada, de no ser asi se retorna una respuesta informando acerca de ello, junto con el encabezado de rango maximo
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
           throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        Map<String, Object> respuesta = new HashMap<>();

        Long total;
        List resultado;
        try {
            //se obtiene el total de registros
            total = ((Long) em.createNamedQuery("MisionCategoriaMision.countCategoriasByIdMision")
                    .setParameter("idMision", idMision)
                    .getSingleResult());
            total -= total - 1 < 0 ? 0 : 1;

            //ve que si en el caso que el registro sea mayor que el total, este se reduce por cantidad (mientras que no sea <0) hasta que este entre un valor de 0 a total
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            //se obtiene el listado usando los parametros de registro y cantidad para limitar el rango de respuesta
            resultado = em.createNamedQuery("MisionCategoriaMision.findCategoriasByIdMision")
                    .setParameter("idMision", idMision)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();
        } catch (IllegalArgumentException e) {//en el caso que los valores de paginacion esten erroneos
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        //se establece la respuesta encapsulando el listado y estableciendo los encabezados de rango de la respuesta y rango aceptado
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * Metodo que asigna a una categoria una mision, usando los identificadores
     * de los mismos
     *
     * @param idCategoria Identificador de la categoria de mision
     * @param idMision Identificador de la mision
     * @param usuario usuario en sesión
     * @param locale
     */
    public void assignCategoriaMision(String idCategoria, String idMision, String usuario,Locale locale) {
        //se verifica que tanto la categoria como la promocion existan
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
        }
        if (em.find(CategoriaMision.class, idCategoria) == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_category_not_found");
        }

        //verifica que la promocion a asignar a la categoria, no tenga un estado de borrador, ni archivado
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
        }

        //fecha de creacion
        Date date = Calendar.getInstance().getTime();

        try {
            em.persist(new MisionCategoriaMision(idCategoria, idMision, date, usuario));
            em.flush();
        } catch (EntityExistsException e) {//en el caso de que ya exista la entidad en la bases de datos
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "challenge_asociated_category");
        }

        bitacoraBean.logAccion(MisionCategoriaMision.class, idCategoria + "/" + idMision, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
    }

    /**
     * Metodo que elimina la asignacion de una mision a una categoria de mision
     * por sus identificadores
     *
     * @param idCategoria Identificador de la categoria
     * @param idMision Identificador de la mision
     * @param usuario usuario en sesion
     * @param locale
     */
    public void unassignCategoriaMision(String idCategoria, String idMision, String usuario,Locale locale) {
        try {
            //se intenta remover la entidad a partir de la referencia de la misma por sus identificadores
            em.remove(em.getReference(MisionCategoriaMision.class, new MisionCategoriaMisionPK(idCategoria, idMision)));
            em.flush();
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_category_not_foound");
        }

        bitacoraBean.logAccion(MisionCategoriaMision.class, idCategoria + "/" + idMision, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }

    /**
     * Metodo que asigna a una categoria una lista de misiones, usando los
     * identificadores de los mismos
     *
     * @param idCategoria Identificador de la categoria de promocion
     * @param listaIdMision Lista de identificadores de mision
     * @param usuario usuario en sesión
     * @param locale
     */
    public void assignBatchCategoriaMision(String idCategoria, List<String> listaIdMision, String usuario,Locale locale) {
        //se verifica que la categoria exista
        if (em.find(CategoriaMision.class, idCategoria) == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_category_not_found");
        }

        for (String idMision : listaIdMision) {
            //se verifica que la promocion exista
            Mision mision = em.find(Mision.class, idMision);
            if (mision == null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Mision");
            }
            //verifica que la promocion a asignar a la categoria, no tenga un estado de borrador, ni archivado
            if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Mision");
            }
        }

        //fecha de creacion
        Date date = Calendar.getInstance().getTime();

        try {
            listaIdMision.stream().forEach((idMision) -> {
                em.persist(new MisionCategoriaMision(idCategoria, idMision, date, usuario));
            });
            em.flush();
        } catch (EntityExistsException e) {//en el caso de que ya exista la entidad en la bases de datos
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "challenge_asociated_category");
        }

        listaIdMision.forEach((idMision) -> {
            bitacoraBean.logAccion(MisionCategoriaMision.class, idCategoria + "/" + idMision, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        });
    }

    /**
     * Metodo que elimina la asignacion de una lista de misiones a una categoria
     * de mision por sus identificadores
     *
     * @param idCategoria Identificador de la categoria
     * @param listaIdMision Lista de identificadores de mision
     * @param usuario usuario en sesion
     * @param locale
     */
    public void unassignBatchCategoriaMision(String idCategoria, List<String> listaIdMision, String usuario,Locale locale) {
        try {
            listaIdMision.stream().forEach((idMision) -> {
                //se intenta remover la entidad a partir de la referencia de la misma por sus identificadores
                em.remove(em.getReference(MisionCategoriaMision.class, new MisionCategoriaMisionPK(idCategoria, idMision)));
            });
            em.flush();
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_category_not_foound");
        }

        listaIdMision.forEach((idMision) -> {
            bitacoraBean.logAccion(MisionCategoriaMision.class, idCategoria + "/" + idMision, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        });
    }
}
