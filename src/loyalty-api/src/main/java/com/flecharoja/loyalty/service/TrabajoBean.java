/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.TrabajosInternos;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * manejo de trabajos internos
 *
 * @author wtencio
 */
@Stateless
public class TrabajoBean {
    
    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * creacion de registro de trabajo interno (uso interno)
     * 
     * @param tipoTrabajo indicador sobre el tipo de trabajo
     * @return 
     */
    public String crearTrabajo(char tipoTrabajo){
        TrabajosInternos trabajosInternos = new TrabajosInternos(TrabajosInternos.Estados.PROCESANDO.getValue(), "Procesando", tipoTrabajo, Calendar.getInstance().getTime(), Calendar.getInstance().getTime(), Double.valueOf(0));

        em.persist(trabajosInternos);
        em.flush();
        return trabajosInternos.getIdTrabajo();
    }
    
    /**
     * actualizacion de estado del trabajo interno (uso interno)
     * 
     * @param idTrabajo id de trabajo
     * @param indEstado indicador de estado
     * @param resultado data resultante
     * @param porcentaje porcentaje completado
     */
    public void actualizarTrabajo(String idTrabajo, Character indEstado, String resultado, Double porcentaje){
        TrabajosInternos trabajo = em.find(TrabajosInternos.class, idTrabajo);
        trabajo.setIndEstado(indEstado);
        trabajo.setResultado(resultado);
        trabajo.setPorcentajeCompletado(porcentaje);
        trabajo.setFechaActualizacion(new Date());
        em.merge(trabajo);
    }
    
    /**
     * actualizacion de estado del trabajo interno (uso interno)
     * 
     * @param idTrabajo id de trabajo
     * @param indEstado indicador de estado
     * @param resultado data resultante
     */
    public void actualizarTrabajo(String idTrabajo, Character indEstado, String resultado){
        TrabajosInternos trabajo = em.find(TrabajosInternos.class, idTrabajo);
        trabajo.setIndEstado(indEstado);
        trabajo.setResultado(resultado);
        trabajo.setFechaActualizacion(new Date());
        em.merge(trabajo);
    }
    
    /**
     * obtencion de lista de trabajos internos
     * 
     * @param tipos filtro por tipo de trabajo
     * @param estados filtro por estado de trabajo
     * @param ordenTipo orden de paginacion
     * @param ordenCampo campo por donde ordenar registros
     * @param cantidad paginacion
     * @param registro paginacion
     * @param locale locale
     * @return lista de trabajos internos
     */
    public Map<String, Object> getTrabajos(List<String> tipos, List<String> estados,String ordenTipo, String ordenCampo,int cantidad, int registro, Locale locale){
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<TrabajosInternos> root = query.from(TrabajosInternos.class);

        query = Busquedas.getCriteriaQueryTrabajos(cb, query, root, tipos, estados, ordenTipo, ordenCampo);

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();

        //retorno del resultado y encabezados sobre el rango
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }
}
