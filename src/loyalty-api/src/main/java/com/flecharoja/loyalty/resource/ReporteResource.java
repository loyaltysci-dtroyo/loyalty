/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.service.ReporteBean;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.Locale;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author wtencio
 */
@Api(value = "Reporte")
@Path("reporte")
public class ReporteResource {

    @EJB
    ReporteBean reporteBean;

    @Context
    SecurityContext context;

    @Context
    HttpServletRequest request;

    @EJB
    MyKeycloakAuthz authzBean;

    /**
     * Metodo que acciona una tarea, la cual reporte que obtiene las existencias
     * de un premio , en el caso de error al ejecutar la tarea se retorna un 500
     * (error interno del servidor) con un mensaje en el encabezado, de no ser
     * asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param ubicacion por la cual desea ser filtrada la busqueda
     * @param premio por el cual se desea filtrar la busqueda
     * @param periodo por el cual se desea filtrar la busqueda
     * @param mes por el cual se desea filtrar la busqueda
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Reporte de existencias de un premio",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("existencias")
    @Produces(MediaType.APPLICATION_JSON)
    public String getExistencias(
            @ApiParam(value = "Ubicacion por la cual se desea filtrar la busqueda") @QueryParam("ubicacion") String ubicacion,
            @ApiParam(value = "Premio por el cual se desea filtrar la busqueda") @QueryParam("premio") String premio,
            @ApiParam(value = "Periodo por el cual se desea filtrar la busqueda", required = true) @QueryParam("periodo") Integer periodo,
            @ApiParam(value = "Mes por el cual se desea filtrar la busqueda", required = true) @QueryParam("mes") Integer mes) {
        String token;
        try {
            context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.REPORTES, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return reporteBean.reporteConsolidadoPDF(mes, periodo, ubicacion, premio, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual reporte que obtiene un dcoumento de
     * inventario, en el caso de error al ejecutar la tarea se retorna un 500
     * (error interno del servidor) con un mensaje en el encabezado, de no ser
     * asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param documento identificador del documento
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Exportación de un documento de inventario a formato PDF",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("documento-inventario")
    @Produces(MediaType.APPLICATION_JSON)
    public String reporteDocumento(
            @ApiParam(value = "Identificador del documento de inventario", required = true)
            @QueryParam("documento") String documento) {
        String token;
        try {
            context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.REPORTES, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return reporteBean.reporteDocumentoInventario(documento, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual crea el reporte de movimientos de
     * un premio en el caso de error al ejecutar la tarea se retorna un 500
     * (error interno del servidor) con un mensaje en el encabezado, de no ser
     * asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param fechaInicio por el cual se filtra
     * @param fechaFin por el cual se filtra
     * @param ubicacion por la cual se filtra
     * @param premio por el cual se filtra
     * @param tipoMovimiento por el cual se filtra la busqueda
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Reporte de movimientos de premios",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("movimientos")
    @Produces(MediaType.APPLICATION_JSON)
    public String reporteMovimientos(
            @ApiParam(value = "Fecha de inicio del rango de fechas", required = true) @QueryParam("fecha-inicio") Long fechaInicio,
            @ApiParam(value = "Fecha de fin del rango de fechas", required = true) @QueryParam("fecha-fin") Long fechaFin,
            @ApiParam(value = "Ubicacion por la cual se desea filtrar la busqueda") @QueryParam("ubicacion") String ubicacion,
            @ApiParam(value = "Premio por la cual se desea filtrar la busqueda") @QueryParam("premio") String premio,
            @ApiParam(value = "Tipo de movimiento por la cual se desea filtrar la busqueda") @QueryParam("tipo-movimiento") String tipoMovimiento) {
        String token;
        try {
            context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.REPORTES, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return reporteBean.reporteMovimientos(fechaInicio, fechaFin, ubicacion, premio, tipoMovimiento, Locale.ITALY);
    }

    /**
     * Metodo que acciona una tarea, la cual crea el catalogo de premios en el
     * caso de error al ejecutar la tarea se retorna un 500 (error interno del
     * servidor) con un mensaje en el encabezado, de no ser asi se retorna un
     * 200 (OK) con un resultado de ser requerido este.
     *
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Catalogo de premios en formato PDF",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("catalogo-premios")
    @Produces(MediaType.APPLICATION_JSON)
    public String reporteCatalogoPremios() {
        String token;
        try {
            context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.REPORTES, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return reporteBean.reporteCatalogoPremios(Locale.ITALY);
    }
    
    
    /**
     * Metodo que acciona una tarea, la cual crea el reporte de movimientos de
     * un premio en el caso de error al ejecutar la tarea se retorna un 500
     * (error interno del servidor) con un mensaje en el encabezado, de no ser
     * asi se retorna un 200 (OK) con un resultado de ser requerido este.
     *
     * @param fechaInicio por el cual se filtra
     * @param fechaFin por el cual se filtra
     * @param idCategoria
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Reporte de carritos",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("carritos")
    @Produces(MediaType.APPLICATION_JSON)
    public String reporteCarritos(
            @ApiParam(value = "Fecha de inicio del rango de fechas", required = true) @QueryParam("fecha-inicio") Long fechaInicio,
            @ApiParam(value = "Fecha de fin del rango de fechas", required = true) @QueryParam("fecha-fin") Long fechaFin,
            @ApiParam(value = "Categoria por la cual se desea filtrar la busqueda") @QueryParam("idCategoria") String idCategoria) {
        String token;
        try {
            context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.REPORTES, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return reporteBean.reporteCarritos(fechaInicio, fechaFin, idCategoria, Locale.ITALY);
    }
}
