package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.model.NotificacionListaSegmento;
import com.flecharoja.loyalty.model.NotificacionListaSegmentoPK;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * EJB encargado del manejo de datos y aplicacion de logica de negocio al manejo
 * de listas de segmentos de una notificacion.
 *
 * @author svargas
 */
@Stateless
public class NotificacionListaSegmentoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * obtencion de lista de segmentos segun el tipo de accion para una notificacion
     * 
     * @param idNotificacion id de notificacion
     * @param tipo tipo de lista
     * @param estados filtro de segmento
     * @param tipos filtro de segmento
     * @param busqueda filtro de segmento
     * @param filtros filtro de segmento
     * @param cantidad paginacion
     * @param registro paginacion
     * @param ordenTipo tipo de orden en paginacion
     * @param ordenCampo campo a ordenar para paginacion
     * @param locale locale
     * @return lista de segmentos
     */
    public Map<String, Object> getSegmentos(String idNotificacion, String tipo, List<String> estados, List<String> tipos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
           throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Segmento> root = query.from(Segmento.class);
        
        //query de busqueda de segmentos
        query = Busquedas.getCriteriaQuerySegmentos(cb, query, root, estados, tipos, busqueda, filtros, ordenTipo, ordenCampo);
        
        //subquery para el filtro de segmentos por tipo de accion
        Subquery<String> subquery = query.subquery(String.class);
        Root<NotificacionListaSegmento> rootSubquery = subquery.from(NotificacionListaSegmento.class);
        subquery.select(rootSubquery.get("notificacionListaSegmentoPK").get("idSegmento"));
        if (tipo==null || tipo.toUpperCase().charAt(0)==Indicadores.ASIGNADO) {
            subquery.where(cb.equal(rootSubquery.get("notificacionListaSegmentoPK").get("idNotificacion"), idNotificacion));
            if (query.getRestriction()!=null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idSegmento")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idSegmento")).value(subquery)));
            }
        } else {
            if (tipo.toUpperCase().charAt(0)==Indicadores.DISPONIBLES) {
                subquery.where(cb.equal(rootSubquery.get("notificacionListaSegmentoPK").get("idNotificacion"), idNotificacion));
                if (query.getRestriction()!=null) {
                    query.where(query.getRestriction(), cb.equal(root.get("indEstado"), Segmento.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idSegmento")).value(subquery).not()));
                } else {
                    query.where(cb.equal(root.get("indEstado"), Segmento.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idSegmento")).value(subquery).not()));
                }
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "tipo");
            }
        }
        
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * Metodo que asigna un segmento a una notificacion
     *
     * @param idNotificacion Identificador de la notificacion
     * @param idSegmento Identificador del segmento
     * @param usuarioCreacion Identificador del usuario creador
     * @param locale
     */
    public void assignSegmentoNotificacion(String idNotificacion, String idSegmento, String usuarioCreacion, Locale locale) {
        Notificacion notificacion = em.find(Notificacion.class, idNotificacion);//se busca la notificacion almacenada
        if (notificacion == null) {//si no se pudo encontrar la notificacion...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }
        try {
            em.getReference(Segmento.class, idSegmento).getIdSegmento();//se busca la referencia del segmento y se detona su busqueda
        } catch (EntityNotFoundException e) {//no se pudo encontrar el segmento
             throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idSegmento");
        }

        //si la notificacion tiene un estado de archivado o ejecutado...
        if (notificacion.getIndEstado().equals(Notificacion.Estados.ARCHIVADO.getValue()) || notificacion.getIndEstado().equals(Notificacion.Estados.EJECUTADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Notificacion");
        }

        try {
            //se registra la asignacion y la accion en la bitacora
            em.persist(new NotificacionListaSegmento(idNotificacion, idSegmento, Calendar.getInstance().getTime(), usuarioCreacion));
            em.flush();
            bitacoraBean.logAccion(NotificacionListaSegmento.class, idNotificacion + "/" + idSegmento, usuarioCreacion, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (EntityExistsException e) {//si la asignacion existe...
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "notification_segment_already_exists");
        }
    }

    /**
     * Metodo que asigna un listado de identificadores de segmentos a una
     * notificacion
     *
     * @param idNotificacion Identificador de notificacion
     * @param segmentos Lista de identificadores de segmentos
     * @param usuarioCreacion Identificador de usuario creacion
     * @param locale
     */
    public void assignBatchSegmentosNotificacion(String idNotificacion, List<String> segmentos, String usuarioCreacion, Locale locale) {
        Notificacion notificacion = em.find(Notificacion.class, idNotificacion);//se busca la notificacion
        if (notificacion == null) {//si no se encontro la notificacion...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }

        //si el estado de la notificacion es archivado o ejecutado
        if (notificacion.getIndEstado().equals(Notificacion.Estados.ARCHIVADO.getValue()) || notificacion.getIndEstado().equals(Notificacion.Estados.EJECUTADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Notificacion");
        }

        Date fecha = Calendar.getInstance().getTime();//obtencion de la fecha actual

        for (String idSegmento : segmentos) {
            try {
                em.getReference(Segmento.class, idSegmento).getIdSegmento();//se obtiene la referencia del segmento y se detona la busqueda
            } catch (EntityNotFoundException e) {//si no se encontro
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idSegmento");
            }

            try {
                em.persist(new NotificacionListaSegmento(idNotificacion, idSegmento, fecha, usuarioCreacion));
            } catch (EntityExistsException e) {//entidad ya existente
                throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "notification_segment_already_exists");
            }

        }

        em.flush();

        //se registra las asignaciones y acciones en bitacora
        segmentos.forEach((idSegmento) -> {
            bitacoraBean.logAccion(NotificacionListaSegmento.class, idNotificacion + "/" + idSegmento, usuarioCreacion, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        });
    }

    /**
     * Metodo que elimina la asignacion de un segmento con una notificacion
     *
     * @param idNotificacion Identificador de la notificacion
     * @param idSegmento Identificador del segmento
     * @param usuarioModificacion Identificador del usuario modificador
     * @param locale
     */
    public void unassignSegmentoNotificacion(String idNotificacion, String idSegmento, String usuarioModificacion,Locale locale) {
        Notificacion notificacion = em.find(Notificacion.class, idNotificacion);//se busca la notificacion
        if (notificacion == null) {//si no se encontro la notificacion
           throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }
        try {
            em.getReference(Segmento.class, idSegmento).getIdSegmento();//se busca la referencia de la entidad y se detona su busqueda
        } catch (EntityNotFoundException e) {//no se pudo encontrar el miembro
             throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idSegmento");
        }

        //si la notificacion tiene un estado de archivado o ejecutado...
        if (notificacion.getIndEstado().equals(Notificacion.Estados.ARCHIVADO.getValue()) || notificacion.getIndEstado().equals(Notificacion.Estados.EJECUTADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Notificacion");
        }

        //se elimina la asignacion y se registra la accion
        try {
            em.remove(em.getReference(NotificacionListaSegmento.class, new NotificacionListaSegmentoPK(idNotificacion, idSegmento)));
            em.flush();
            bitacoraBean.logAccion(NotificacionListaSegmento.class, idNotificacion + "/" + idSegmento, usuarioModificacion, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_segment_not_found");
        }
    }

    /**
     * Metodo que elimina la asignacion de un listado de identificadores de
     * segmentos de una notificacion
     *
     * @param idNotificacion Identificador de la notificacion
     * @param segmentos Identificador del segmento
     * @param usuarioModificacion Identificador del usuario modificador
     * @param locale
     */
    public void unassignBatchSegmentosNotificacion(String idNotificacion, List<String> segmentos, String usuarioModificacion,Locale locale) {
        Notificacion notificacion = em.find(Notificacion.class, idNotificacion);//se busca la notificacion
        if (notificacion == null) {//si no se encontro la notificacion...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }

        //si el estado de la notificacion es archivado o ejecutado
        if (notificacion.getIndEstado().equals(Notificacion.Estados.ARCHIVADO.getValue()) || notificacion.getIndEstado().equals(Notificacion.Estados.EJECUTADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Notificacion");
        }

        //se elimina las asignaciones y se registra las acciones en bitacora
        try {
            segmentos.forEach((idSegmento) -> {
                em.remove(em.getReference(NotificacionListaSegmento.class, new NotificacionListaSegmentoPK(idNotificacion, idSegmento)));
            });
            em.flush();
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_segment_not_found");
        }

        segmentos.forEach((idSegmento) -> {
            bitacoraBean.logAccion(NotificacionListaSegmento.class, idNotificacion + "/" + idSegmento, usuarioModificacion, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        });
    }
}
