package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PROMOCION_LISTA_SEGMENTO")
@XmlRootElement
@NamedQueries({
    //@NamedQuery(name = "PromocionListaSegmento.deleteAllByIdSegmentoNotInPromocionArchivada", query = "DELETE FROM PromocionListaSegmento p WHERE p.promocionListaSegmentoPK.idSegmento = :idSegmento AND p.promocion.indEstado = 'B'"),
    @NamedQuery(name = "PromocionListaSegmento.deleteAllByIdSegmentoNotInPromocionArchivada", query = "DELETE FROM PromocionListaSegmento p WHERE p.promocionListaSegmentoPK.idSegmento  =:idSegmento"),
    @NamedQuery(name = "PromocionListaSegmento.findByIdPromocion", query = "SELECT p FROM PromocionListaSegmento p WHERE p.promocionListaSegmentoPK.idPromocion = :idPromocion"),
    @NamedQuery(name = "PromocionListaSegmento.findAllIdSegmentoByIdPromocionIndTipo", query = "SELECT p.promocionListaSegmentoPK.idSegmento FROM PromocionListaSegmento p WHERE p.promocionListaSegmentoPK.idPromocion = :idPromocion AND p.indTipo = :indTipo"),
})
public class PromocionListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PromocionListaSegmentoPK promocionListaSegmentoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_PROMOCION", referencedColumnName = "ID_PROMOCION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Promocion promocion;
    
    @JoinColumn(name = "ID_SEGMENTO", referencedColumnName = "ID_SEGMENTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Segmento segmento;

    public PromocionListaSegmento() {
    }

    public PromocionListaSegmento(PromocionListaSegmentoPK promocionListaSegmentoPK) {
        this.promocionListaSegmentoPK = promocionListaSegmentoPK;
    }

    public PromocionListaSegmento(String idSegmento, String idPromocion, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.promocionListaSegmentoPK = new PromocionListaSegmentoPK(idSegmento, idPromocion);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public PromocionListaSegmento(String idSegmento, String idPromocion) {
        this.promocionListaSegmentoPK = new PromocionListaSegmentoPK(idSegmento, idPromocion);
    }

    public PromocionListaSegmentoPK getPromocionListaSegmentoPK() {
        return promocionListaSegmentoPK;
    }

    public void setPromocionListaSegmentoPK(PromocionListaSegmentoPK promocionListaSegmentoPK) {
        this.promocionListaSegmentoPK = promocionListaSegmentoPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo!=null?Character.toUpperCase(indTipo):null;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

    public Segmento getSegmento() {
        return segmento;
    }

    public void setSegmento(Segmento segmento) {
        this.segmento = segmento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (promocionListaSegmentoPK != null ? promocionListaSegmentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromocionListaSegmento)) {
            return false;
        }
        PromocionListaSegmento other = (PromocionListaSegmento) object;
        if ((this.promocionListaSegmentoPK == null && other.promocionListaSegmentoPK != null) || (this.promocionListaSegmentoPK != null && !this.promocionListaSegmentoPK.equals(other.promocionListaSegmentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.PromocionListaSegmento[ promocionListaSegmentoPK=" + promocionListaSegmentoPK + " ]";
    }
    
}
