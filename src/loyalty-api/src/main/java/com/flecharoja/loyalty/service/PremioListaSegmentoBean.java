/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioListaSegmento;
import com.flecharoja.loyalty.model.PremioListaSegmentoPK;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * EJB encargado de el manejo de asignaciones de segmentos con premios
 *
 * @author wtencio
 */
@Stateless
public class PremioListaSegmentoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;

    /**
     * Metodo que asigna (incluye o excluye) a un segmento de un premio
     *
     * @param idPremio Identificador del premio
     * @param idSegmento Identificador del segmento
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesión
     * @param locale
     */
    public void includeExcludeSegmento(String idPremio, String idSegmento, Character indAccion, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        //verificacion de entidades archivadas/borrador
        if (premio.getIndEstado().equals(Premio.Estados.ARCHIVADO.getValue())
                || !segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "operation_over_archived", "Premio, Segmento");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            PremioListaSegmento premioListaSegmento = new PremioListaSegmento(idSegmento, idPremio, indAccion, date, usuario);
            em.merge(premioListaSegmento);
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        bitacoraBean.logAccion(PremioListaSegmento.class, idPremio + "/" + idSegmento, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * Metodo que revoca la asignacion de un segmento a un premio
     *
     * @param idPremio Identificador del premio
     * @param idSegmento Identificador del segmento
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeSegmento(String idPremio, String idSegmento, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Premio premio = em.find(Premio.class, idPremio);

        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }
        try {

            em.getReference(Segmento.class, idSegmento).getIdSegmento();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }
        //verificacion de entidades archivadas
        if (premio.getIndEstado().equals(Premio.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Premio");
        }

        //verificacion que la entidad exista y su eliminacion
        em.remove(em.getReference(PremioListaSegmento.class, new PremioListaSegmentoPK(idSegmento, idPremio)));

        bitacoraBean.logAccion(PremioListaSegmento.class, idPremio + "/" + idSegmento, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);

    }

    /**
     * Método que asigna (incluye o excluye) una lista de segmentos de un premio
     *
     * @param idPremio identificador del premio
     * @param listaIdSegmentos lista de identificadores de segmentos
     * @param indAccion tipo de accion (incluye/excluye)
     * @param usuario usuario en sesion
     * @param locale
     */
    public void includeExcludeBatchSegmento(String idPremio, List<String> listaIdSegmentos, Character indAccion, String usuario, Locale locale) {
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }
        try {

            listaIdSegmentos = listaIdSegmentos.stream().distinct().collect(Collectors.toList());

            for (String idSegmento : listaIdSegmentos) {
                em.getReference(Segmento.class, idSegmento).getIdSegmento();
            }

        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        //entidad de entidad archivada
        if (premio.getIndEstado().equals(Premio.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "operation_over_archived", "Premio");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea una valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            for (String idSegmento : listaIdSegmentos) {
                em.merge(new PremioListaSegmento(idSegmento, idPremio, indAccion, date, usuario));
            }

        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        for (String idSegmento : listaIdSegmentos) {
            bitacoraBean.logAccion(PremioListaSegmento.class, idPremio + "/" + idSegmento, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        }
    }

    /**
     * Método que elimina la asignacion de una lista de segmentos de un premio
     *
     * @param idPremio identificador del premio
     * @param listaIdSegmentos lista de identificadores de segmentos
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeBatchSegmento(String idPremio, List<String> listaIdSegmentos, String usuario, Locale locale) {
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "reward_not_found");
        }
        try {

            listaIdSegmentos = listaIdSegmentos.stream().distinct().collect(Collectors.toList());
            listaIdSegmentos.stream().forEach((idSegmento) -> {
                em.getReference(Segmento.class, idSegmento).getIdSegmento();
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        //entidad de entidad archivada
        if (premio.getIndEstado().equals(Premio.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "operation_over_archived", "Premio");
        }

        for (String idSegmento : listaIdSegmentos) {
            em.remove(em.getReference(PremioListaSegmento.class, new PremioListaSegmentoPK(idSegmento, idPremio)));
        }
        em.flush();

        for (String idSegmento : listaIdSegmentos) {
            bitacoraBean.logAccion(PremioListaSegmento.class, idPremio + "/" + idSegmento, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        }

    }

    /**
     * Metodo que obtiene un listado de segmentos asignados a un premio en un
     * rango de respuesta y opcionalmente por el tipo de accion
     * (incluido/excluido)
     *
     * @param idPremio Identificador del premio
     * @param indAccion Indicador del tipo de accion deseado
     * @param estados
     * @param tipos
     * @param busqueda
     * @param registro Numero de registro inicial
     * @param filtros
     * @param cantidad Cantidad de registros deseados
     * @param locale
     * @param ordenTipo
     * @param ordenCampo
     * @return Respuesta con el listado de segmentos y su rango
     */
    public Map<String, Object> getSegmentos(String idPremio, String indAccion, List<String> estados, List<String> tipos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion de que el rango este dentro del valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Segmento> root = query.from(Segmento.class);
        
        query = Busquedas.getCriteriaQuerySegmentos(cb, query, root, estados, tipos, busqueda, filtros, ordenTipo, ordenCampo);
        
        Subquery<String> subquery = query.subquery(String.class);
        Root<PremioListaSegmento> rootSubquery = subquery.from(PremioListaSegmento.class);
        subquery.select(rootSubquery.get("premioListaSegmentoPK").get("idSegmento"));
        if (indAccion==null) {
            subquery.where(cb.equal(rootSubquery.get("premioListaSegmentoPK").get("idPremio"), idPremio));
            if (query.getRestriction()!=null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idSegmento")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idSegmento")).value(subquery)));
            }
        } else {
            switch(indAccion.toUpperCase().charAt(0)) {
                case Indicadores.INCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("premioListaSegmentoPK").get("idPremio"), idPremio), cb.equal(rootSubquery.get("indTipo"), Indicadores.INCLUIDO));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idSegmento")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idSegmento")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.EXCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("premioListaSegmentoPK").get("idPremio"), idPremio), cb.equal(rootSubquery.get("indTipo"), Indicadores.EXCLUIDO));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idSegmento")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idSegmento")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.DISPONIBLES: {
                    subquery.where(cb.equal(rootSubquery.get("premioListaSegmentoPK").get("idPremio"), idPremio));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.equal(root.get("indEstado"), Segmento.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idSegmento")).value(subquery).not()));
                    } else {
                        query.where(cb.equal(root.get("indEstado"), Segmento.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idSegmento")).value(subquery).not()));
                    }
                    break;
                }
                default: {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "indAccion");
                }
            }
        }
        
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;

    }

}
