package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * segmentos
 *
 * @author svargas
 */
@Stateless
public class SegmentoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora
    
    @EJB
    SegmentoMiembrosEligiblesBean eligiblesBean;

    /**
     * Obtencion de segmentos en rango de registros (registro & cantidad) y filtrado opcionalmente por indicadores y terminos de busqueda sobre algunos campos
     * 
     * @param estados Listado de valores de indicadores de estados deseados
     * @param tipos Listado de valores de indicadores de tipos de segmento (estatico/dinamico) deseados
     * @param busqueda Terminos de busqueda
     * @param filtros Listado de campos sobre que aplicar los terminos de busqueda
     * @param cantidad Cantidad de registros deseados
     * @param registro Numero de registro inicial en el resultado
     * @return Listado de segmentos deseados
     */
    public Map<String, Object> getSegmentos(List<String> estados, List<String> tipos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Segmento> root = query.from(Segmento.class);
        
        query = Busquedas.getCriteriaQuerySegmentos(cb, query, root, estados, tipos, busqueda, filtros, ordenTipo, ordenCampo);
        
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * Metodo que obtiene un segmento por su numero de identificacion
     *
     * @param idSegmento Identificacion del segmento
     * @param locale
     * @return Respuesta con la informacion del segmento
     */
    public Segmento getSegmentoPorId(String idSegmento, Locale locale) {
        Segmento segmento = em.find(Segmento.class, idSegmento);
        //verificacion que el segmento exista
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }
        return segmento;
    }

    /**
     * Metodo que almacena la informacion de un segmento
     *
     * @param segmento Objeto con la informacion del segmento
     * @param usuario usuario en sesión
     * @param locale
     * @return Respuesta con el identificacion del segmento almacenado
     */
    public String insertSegmento(Segmento segmento, String usuario, Locale locale) {
        if (segmento==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Segmento");
        }
        //establecimiento de valores por defecto
        Date date = Calendar.getInstance().getTime();
        segmento.setFechaCreacion(date);
        segmento.setFechaModificacion(date);
        segmento.setNumVersion(new Long(1));

        segmento.setUsuarioCreacion(usuario);
        segmento.setUsuarioModificacion(usuario);
        segmento.setIndEstado(Segmento.Estados.BORRADOR.getValue());

        try {
            em.persist(segmento);
            em.flush();
            bitacoraBean.logAccion(Segmento.class, segmento.getIdSegmento(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {//atributos no validos
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        return segmento.getIdSegmento();
    }

    /**
     * Metodo que modifica la informacion de un segmento almacenado
     *
     * @param segmento Objeto con la informacion del segmento
     * @param usuario usuario en sesión
     * @param locale
     */
    public void editSegmento(Segmento segmento, String usuario, Locale locale) {
        if (segmento==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Segmento");
        }
        Segmento original;
        try {
            original = em.find(Segmento.class, segmento.getIdSegmento());
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idSegmento");
        }
        if (original == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        //establecimiento de valores por defecto
        Date date = Calendar.getInstance().getTime();
        segmento.setFechaModificacion(date);

        segmento.setUsuarioModificacion(usuario);
        
        if (Segmento.Estados.get(segmento.getIndEstado())==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indEstado");
        }

        //verificacion de que posible cambio de estado sea valido (Activo NO A Borrador o estado almacenado como Archivado)
        if ((original.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue()) && segmento.getIndEstado().equals(Segmento.Estados.BORRADOR.getValue()))
                || original.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "segment_not_change_state");
        }

        //establecimiento de valores por defecto en condicion del valor de indicador de estado
        if (segmento.getIndEstado().equalsIgnoreCase(Segmento.Estados.ACTIVO.getValue())) {
            segmento.setFechaPublicacion(date);
        } else if (segmento.getIndEstado().equalsIgnoreCase(Segmento.Estados.ARCHIVADO.getValue())) {
            em.createNamedQuery("PromocionListaSegmento.deleteAllByIdSegmentoNotInPromocionArchivada").setParameter("idSegmento", segmento.getIdSegmento()).executeUpdate();
            segmento.setFechaArchivado(date);
        }

        try {
            em.merge(segmento);
            em.flush();
            bitacoraBean.logAccion(Segmento.class, segmento.getIdSegmento(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
        
        if (segmento.getIndEstado().equalsIgnoreCase(Segmento.Estados.ACTIVO.getValue())) {
            eligiblesBean.recalculateEligibles(segmento.getIdSegmento(), locale);
        }
    }

    /**
     * Metodo que modifica la informacion de un segmento para que este pase a
     * estado de archivo
     *
     * @param idSegmento Identificacion del segmento
     * @param usuario usuario en sesión
     * @param locale
     */
    public void disableSegmento(String idSegmento, String usuario, Locale locale) {
        Date date = Calendar.getInstance().getTime();

        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        if (segmento.getIndEstado().equals(Segmento.Estados.BORRADOR.getValue())) {
            em.remove(segmento);
            bitacoraBean.logAccion(Segmento.class, segmento.getIdSegmento(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        } else {
            segmento.setFechaArchivado(date);

            segmento.setUsuarioModificacion(usuario);
            segmento.setIndEstado(Segmento.Estados.ARCHIVADO.getValue());

            em.createNamedQuery("PromocionListaSegmento.deleteAllByIdSegmentoNotInPromocionArchivada").setParameter("idSegmento", segmento.getIdSegmento()).executeUpdate();

            em.merge(segmento);
            bitacoraBean.logAccion(Segmento.class, segmento.getIdSegmento(), usuario, Bitacora.Accion.ENTIDAD_ARCHIVADA.getValue(),locale);
        }
    }

    /**
     * Metodo que verifica si existe un segmento almacenado con el mismo nombre
     * de despliegue que el proveido
     *
     * @param nombre Valor del de despliegue interno a verificar
     * @return Respuesta con el valor booleano de la existencia del nombre de
     * despliegue
     */
    public boolean existsNombreDespliegue(String nombre) {
        return ((Long) em.createNamedQuery("Segmento.countByNombreDespliegue").setParameter("nombreDespliegue", nombre).getSingleResult()) > 0;
    }
}
