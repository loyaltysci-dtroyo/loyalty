/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CATEGORIA_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoriaProducto.findAll", query = "SELECT c FROM CategoriaProducto c ORDER BY c.fechaModificacion DESC"),
    @NamedQuery(name = "CategoriaProducto.findByIdCategoria", query = "SELECT c FROM CategoriaProducto c WHERE c.idCategoria = :idCategoria"),
    @NamedQuery(name = "CategoriaProducto.countByNombreInterno", query = "SELECT COUNT(c.idCategoria) FROM CategoriaProducto c WHERE c.nombreInterno = :nombreInterno"),
    @NamedQuery(name = "CategoriaProducto.countAll", query = "SELECT COUNT(c.idCategoria) FROM CategoriaProducto c"),
})
public class CategoriaProducto implements Serializable {

    @Version
    @Basic(optional = false)
    @NotNull()
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "categoria_producto_uuid")
    @GenericGenerator(name = "categoria_producto_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_CATEGORIA")
    private String idCategoria;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;
    
    @Size(max = 300)
    @Column(name = "IMAGEN")
    private String imagen;
    
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Basic(optional = false)
    @Size(min = 1, max = 300)
    @Column(name = "LOGIN_ID")
    private String loginId;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCategoria")
    private List<SubcategoriaProducto> subcategoriaProductoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCategoria")
    private List<ProductoCategSubcateg> productoCategSubcategList;

    public CategoriaProducto() {
    }

    public CategoriaProducto(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public CategoriaProducto(String idCategoria, String nombre, String nombreInterno, Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.idCategoria = idCategoria;
        this.nombre = nombre;
        this.nombreInterno = nombreInterno;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }
    
    

    
    @XmlTransient
    public List<SubcategoriaProducto> getSubcategoriaProductoList() {
        return subcategoriaProductoList;
    }

    public void setSubcategoriaProductoList(List<SubcategoriaProducto> subcategoriaProductoList) {
        this.subcategoriaProductoList = subcategoriaProductoList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<ProductoCategSubcateg> getProductoCategSubcategList() {
        return productoCategSubcategList;
    }

    public void setProductoCategSubcategList(List<ProductoCategSubcateg> productoCategSubcategList) {
        this.productoCategSubcategList = productoCategSubcategList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCategoria != null ? idCategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaProducto)) {
            return false;
        }
        CategoriaProducto other = (CategoriaProducto) object;
        if ((this.idCategoria == null && other.idCategoria != null) || (this.idCategoria != null && !this.idCategoria.equals(other.idCategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CategoriaProducto[ idCategoria=" + idCategoria + " ]";
    }

      
}
