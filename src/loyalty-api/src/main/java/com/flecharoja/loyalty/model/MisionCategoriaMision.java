package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_CATEGORIA_MISION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MisionCategoriaMision.findMisionesByIdCategoria", query = "SELECT m.mision FROM MisionCategoriaMision m WHERE m.misionCategoriaMisionPK.idCategoria = :idCategoria ORDER BY m.mision.fechaModificacion DESC"),
    @NamedQuery(name = "MisionCategoriaMision.findCategoriasByIdMision", query = "SELECT m.categoriaMision FROM MisionCategoriaMision m WHERE m.misionCategoriaMisionPK.idMision = :idMision ORDER BY m.categoriaMision.fechaModificacion DESC"),
    @NamedQuery(name = "MisionCategoriaMision.countMisionesByIdCategoria", query = "SELECT COUNT(m) FROM MisionCategoriaMision m WHERE m.misionCategoriaMisionPK.idCategoria = :idCategoria"),
    @NamedQuery(name = "MisionCategoriaMision.countCategoriasByIdMision", query = "SELECT COUNT(m) FROM MisionCategoriaMision m WHERE m.misionCategoriaMisionPK.idMision = :idMision")
})
public class MisionCategoriaMision implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MisionCategoriaMisionPK misionCategoriaMisionPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_CATEGORIA", referencedColumnName = "ID_CATEGORIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CategoriaMision categoriaMision;
    
    @JoinColumn(name = "ID_MISION", referencedColumnName = "ID_MISION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Mision mision;

    public MisionCategoriaMision() {
    }

    public MisionCategoriaMision(String idCategoria, String idMision, Date fechaCreacion, String usuarioCreacion) {
        this.misionCategoriaMisionPK = new MisionCategoriaMisionPK(idCategoria, idMision);
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public MisionCategoriaMisionPK getMisionCategoriaMisionPK() {
        return misionCategoriaMisionPK;
    }

    public void setMisionCategoriaMisionPK(MisionCategoriaMisionPK misionCategoriaMisionPK) {
        this.misionCategoriaMisionPK = misionCategoriaMisionPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public CategoriaMision getCategoriaMision() {
        return categoriaMision;
    }

    public void setCategoriaMision(CategoriaMision categoriaMision) {
        this.categoriaMision = categoriaMision;
    }

    public Mision getMision() {
        return mision;
    }

    public void setMision(Mision mision) {
        this.mision = mision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (misionCategoriaMisionPK != null ? misionCategoriaMisionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionCategoriaMision)) {
            return false;
        }
        MisionCategoriaMision other = (MisionCategoriaMision) object;
        if ((this.misionCategoriaMisionPK == null && other.misionCategoriaMisionPK != null) || (this.misionCategoriaMisionPK != null && !this.misionCategoriaMisionPK.equals(other.misionCategoriaMisionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionCategoriaMision[ misionCategoriaMisionPK=" + misionCategoriaMisionPK + " ]";
    }
    
}
