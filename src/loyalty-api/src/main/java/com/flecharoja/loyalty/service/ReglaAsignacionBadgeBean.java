/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.Insignias;
import com.flecharoja.loyalty.model.NivelMetrica;
import com.flecharoja.loyalty.model.ReglaAsignacionBadge;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author faguilar
 */
@Stateless
public class ReglaAsignacionBadgeBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * @param idPremio
     * @param locale
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List getReglasLista(String idPremio, Locale locale) {
        try {
            return em.createNamedQuery("ReglaAsignacionBadge.findByIdBadge").setParameter("idBadge", idPremio).getResultList();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idLista");
        }
    }

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * @param idRegla Identificador de la lista de reglas
     * @param locale
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public ReglaAsignacionBadge getRegla(String idRegla, Locale locale) {
        try {
            return (ReglaAsignacionBadge) em.createNamedQuery("ReglaAsignacionBadge.findByIdRegla").setParameter("idRegla", idRegla).getSingleResult();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idLista");
        }
    }

    /**
     * Metodo que crea una nueva regla en una lista especifica
     *
     * @param idBadge
     * @param reglaAsignacionBadge Objeto con la informacion de la regla
     * @param usuario usuario en sesion
     * @param locale
     * @return Respuesta con el identificador de la regla creada
     */
    public String createReglaAsignacionBadge(String idBadge, ReglaAsignacionBadge reglaAsignacionBadge, String usuario, Locale locale) {
        if (reglaAsignacionBadge == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "ReglaAsignacionBadge");
        }
        Insignias insignia = em.find(Insignias.class, idBadge);
        if (insignia == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "badge_type_not_valid");
        } else {
            reglaAsignacionBadge.setIdBadge(idBadge);
        }
        if (ReglaAsignacionBadge.TiposRegla.ASCENDIO_NIVEL.getValue() == reglaAsignacionBadge.getIndTipoRegla().charAt(0)) {
            NivelMetrica nivel = em.find(NivelMetrica.class, reglaAsignacionBadge.getValorComparacion());
            if (nivel == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "tier_type_not_valid");
            }
        }
        if (ReglaAsignacionBadge.TiposRegla.get(reglaAsignacionBadge.getIndTipoRegla().charAt(0)) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "intTipoRegla");
        }
        if (reglaAsignacionBadge.getFechaIndTipo() != null && ReglaAsignacionBadge.TiposFecha.get(reglaAsignacionBadge.getFechaIndTipo().charAt(0)) != null) {
            if ((ReglaAsignacionBadge.TiposFecha.ANTERIOR.toString().equals(reglaAsignacionBadge.getFechaIndTipo()))
                    || (ReglaAsignacionBadge.TiposFecha.DESDE.toString().equals(reglaAsignacionBadge.getFechaIndTipo()) && reglaAsignacionBadge.getFechaInicio() == null)
                    || (ReglaAsignacionBadge.TiposFecha.ENTRE.toString().equals(reglaAsignacionBadge.getFechaIndTipo()) && (reglaAsignacionBadge.getFechaFin() == null || reglaAsignacionBadge.getFechaInicio() == null))
                    || (ReglaAsignacionBadge.TiposFecha.HASTA.toString().equals(reglaAsignacionBadge.getFechaIndTipo()) && reglaAsignacionBadge.getFechaFin() == null)
                    || (ReglaAsignacionBadge.TiposFecha.ULTIMO.toString().equals(reglaAsignacionBadge.getFechaIndTipo()) && ReglaAsignacionBadge.TiposUltimaFecha.get(reglaAsignacionBadge.getFechaIndUltimo().charAt(0)) == null)) {

                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "advanced_rule_date_invalid");
            }
        } else {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Date");
        }

        if (ReglaAsignacionBadge.ValoresIndOperador.get(reglaAsignacionBadge.getIndOperadorComparacion()) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
        }

        if (ReglaAsignacionBadge.ValoresIndOperadorRegla.get(reglaAsignacionBadge.getIndOperadorRegla()) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperadorRegla");
        }

        Date date = Calendar.getInstance().getTime();
        reglaAsignacionBadge.setUsuarioCreacion(usuario);
        reglaAsignacionBadge.setUsuarioModificiacion(usuario);
        reglaAsignacionBadge.setFechaCreacion(date);
        reglaAsignacionBadge.setFechaModificacion(date);
        em.persist(reglaAsignacionBadge);
        em.flush();

        bitacoraBean.logAccion(ReglaAsignacionBadge.class, reglaAsignacionBadge.getIdRegla(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        return reglaAsignacionBadge.getIdRegla();
    }

    /**
     * Metodo que crea una nueva regla en una lista especifica
     *
     * @param idBadge
     * @param reglaAsignacionBadge Objeto con la informacion de la regla
     * @param usuario usuario en sesion
     * @param locale
     * @return Respuesta con el identificador de la regla creada
     */
    public String updateReglaAsignacionBadge(String idBadge, ReglaAsignacionBadge reglaAsignacionBadge, String usuario, Locale locale) {
        if (reglaAsignacionBadge == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "ReglaAsignacionBadge");
        }
        Insignias insignia = em.find(Insignias.class, idBadge);
        if (insignia == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "badge_type_not_valid");
        } else {
            reglaAsignacionBadge.setIdBadge(insignia.getIdInsignia());
        }
        if (ReglaAsignacionBadge.TiposRegla.ASCENDIO_NIVEL.getValue() == reglaAsignacionBadge.getIndTipoRegla().charAt(0)) {
            NivelMetrica nivel = em.find(NivelMetrica.class, reglaAsignacionBadge.getValorComparacion());
            if (nivel == null) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "tier_type_not_valid");
            }
        }
        if (ReglaAsignacionBadge.TiposRegla.get(reglaAsignacionBadge.getIndTipoRegla().charAt(0)) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "intTipoRegla");
        }
        if (reglaAsignacionBadge.getFechaIndTipo() != null && ReglaAsignacionBadge.TiposFecha.get(reglaAsignacionBadge.getFechaIndTipo().charAt(0)) != null) {
            if ((ReglaAsignacionBadge.TiposFecha.ANTERIOR.toString().equals(reglaAsignacionBadge.getFechaIndTipo()))
                    || (ReglaAsignacionBadge.TiposFecha.DESDE.toString().equals(reglaAsignacionBadge.getFechaIndTipo()) && reglaAsignacionBadge.getFechaInicio() == null)
                    || (ReglaAsignacionBadge.TiposFecha.ENTRE.toString().equals(reglaAsignacionBadge.getFechaIndTipo()) && (reglaAsignacionBadge.getFechaFin() == null || reglaAsignacionBadge.getFechaInicio() == null))
                    || (ReglaAsignacionBadge.TiposFecha.HASTA.toString().equals(reglaAsignacionBadge.getFechaIndTipo()) && reglaAsignacionBadge.getFechaFin() == null)
                    || (ReglaAsignacionBadge.TiposFecha.ULTIMO.toString().equals(reglaAsignacionBadge.getFechaIndTipo()) && ReglaAsignacionBadge.TiposUltimaFecha.get(reglaAsignacionBadge.getFechaIndUltimo().charAt(0)) == null)) {

                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "advanced_rule_date_invalid");
            }
        } else {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Date");
        }

        if (ReglaAsignacionBadge.ValoresIndOperador.get(reglaAsignacionBadge.getIndOperadorComparacion()) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
        }

        if (ReglaAsignacionBadge.ValoresIndOperadorRegla.get(reglaAsignacionBadge.getIndOperadorRegla()) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperadorRegla");
        }

        //establecimiento de valores por defecto
        Date date = Calendar.getInstance().getTime();
        reglaAsignacionBadge.setFechaModificacion(date);
        reglaAsignacionBadge.setUsuarioModificiacion(usuario);
        em.merge(reglaAsignacionBadge);
        em.flush();
        bitacoraBean.logAccion(ReglaAsignacionBadge.class, reglaAsignacionBadge.getIdRegla(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        return reglaAsignacionBadge.getIdRegla();
    }

    /**
     * Metodo que elimina de una lista especifica una regla
     *
     * @param idRegla Identificador de la regla
     * @param usuario usuario en sesión
     * @param locale
     */
    public void deleteReglaAsignacionBadge(String idRegla, String usuario, Locale locale) {
        Date date = Calendar.getInstance().getTime();
        try {
            //eliminacion de la regla (si es encontrada)
            em.remove(em.getReference(ReglaAsignacionBadge.class, idRegla));
            em.flush();
            bitacoraBean.logAccion(ReglaAsignacionBadge.class, idRegla, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        } catch (EntityNotFoundException e) {//lista de reglas o regla no encontrada
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
    }
}
