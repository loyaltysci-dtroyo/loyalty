/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Region;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB con los metodos de negocio para el manejo de regiones
 *
 * @author wtencio
 */
@Stateless
public class RegionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de bitacora

    /**
     * Método que inserta una nueva region en la base de datos
     *
     * @param nombreRegion nombre que identifica la regiòn
     * @param usuario usuario en sesión
     * @param locale
     * @return identificador generado de la nueva región
     */
    public String insertRegion(String nombreRegion, String usuario, Locale locale) {
        Region region = new Region();
        region.setNombreRegion(nombreRegion);
        region.setUsuarioCreacion(usuario);
        region.setFechaCreacion(Calendar.getInstance().getTime());
        region.setUsuarioModificacion(usuario);
        region.setFechaModificacion(Calendar.getInstance().getTime());
        region.setNumVersion(new Long(1));

        try {
            em.persist(region);
            bitacoraBean.logAccion(Region.class, region.getIdRegion(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        return region.getIdRegion();
    }

    /**
     * Método que obtiene la información de una region en particular
     *
     * @param idRegion identificador de la región
     * @param locale
     * @return informacion de la región
     */
    public Region getRegionId(String idRegion, Locale locale) {

        Region region = (Region) em.createNamedQuery("Region.findByIdRegion").setParameter("idRegion", idRegion).getSingleResult();
        if (region == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "region_not_found");
        }
        return region;

    }

    /**
     * Método que devuelve una lista de regiones
     *
     * @param cantidad de registros deseados por pagina
     * @param registro registro por el que se desea empezar la busqueda
     * @param locale
     * @return lista de regiones
     */
    public Map<String, Object> getRegiones(int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        try {

            total = ((Long) em.createNamedQuery("Region.countAll").getSingleResult());
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createNamedQuery("Region.findAll")
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Método que actualiza la información de una region
     *
     * @param region informacion de la region
     * @param usuario usuario en sesion
     * @param locale
     */
    public void updateRegion(Region region, String usuario, Locale locale) {
        Region temp = em.find(Region.class, region.getIdRegion());

        if (temp == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "region_not_found");
        }

        region.setFechaModificacion(Calendar.getInstance().getTime());
        region.setUsuarioModificacion(usuario);

        try {
            em.merge(region);
            em.flush();
            bitacoraBean.logAccion(Region.class, region.getIdRegion(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

    }

    /**
     * Método que verifica la existencia de un nombre de region
     *
     * @param nombre nombre de la region a verificar
     * @return resultado de la operacion
     */
    public Boolean existsNombreRegion(String nombre) {
        return ((Long) em.createNamedQuery("Region.countByNombre").setParameter("nombre", nombre).getSingleResult()) > 0;
    }

    /**
     * Método que elimina una region de ser posible
     *
     * @param idRegion identificador de la region
     * @param usuario usuario en sesion
     * @param locale
     */
    public void deleteRegion(String idRegion, String usuario, Locale locale) {

        Region region = (Region) em.createNamedQuery("Region.findByIdRegion").setParameter("idRegion", idRegion).getSingleResult();
        if (region == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "region_not_found");
        }
        if (region.getRegionUbicacionList().isEmpty()) {
            em.remove(region);
            bitacoraBean.logAccion(Region.class, region.getIdRegion(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        } else {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "region_location_exists");
        }

    }

    /**
     * Método que realiza una busqueda en base a palabras claves sobre una serie
     * de atributos de la tabla de region
     *
     * @param busqueda cadena de texto con las palabras claves
     * @param cantidad de registros maximos por página
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return listado de todos las regiones encontradas
     */
    public Map<String, Object> searchRegiones(String busqueda, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        String[] palabrasClaves = busqueda.split(" ");
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<Region> root = query.from(Region.class);

            List<Predicate> predicates = new ArrayList<>();

            for (String palabraClave : palabrasClaves) {
                predicates.add(cb.like(cb.lower(root.get("nombreRegion")), "%" + palabraClave.toLowerCase() + "%"));

            }

            query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));

            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            resultado = em.createQuery(query.select(root))
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }
}
