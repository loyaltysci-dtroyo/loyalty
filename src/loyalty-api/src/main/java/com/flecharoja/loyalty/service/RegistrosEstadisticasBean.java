package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.BalanceMetricaMensual;
import com.flecharoja.loyalty.model.EstadisticaMiembroGeneral;
import com.flecharoja.loyalty.model.EstadisticaMiembroPremio;
import com.flecharoja.loyalty.model.EstadisticaMisionGeneral;
import com.flecharoja.loyalty.model.EstadisticaMisionIndividual;
import com.flecharoja.loyalty.model.EstadisticaSegmentoGeneral;
import com.flecharoja.loyalty.model.EstadisticaSegmentoIndividual;
import com.flecharoja.loyalty.model.EstadisticasMetrica;
import com.flecharoja.loyalty.model.EstadisticasMision;
import com.flecharoja.loyalty.model.EstadisticasNotificacion;
import com.flecharoja.loyalty.model.EstadisticasPremio;
import com.flecharoja.loyalty.model.EstadisticasPromocion;
import com.flecharoja.loyalty.model.InstanciaNotificacion;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.PremioMiembro;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.model.RespuestaMision;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.util.Indicadores;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.RegexStringComparator;
import org.apache.hadoop.hbase.filter.RowFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author svargas
 */
@Stateless
public class RegistrosEstadisticasBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    private final Configuration configuration;

    public RegistrosEstadisticasBean() {
        this.configuration = new Configuration();
        this.configuration.addResource("conf/hbase/hbase-site.xml");
    }

    //********************************************************************************************PROMOCIONES********************************************************************************************
    /**
     * obtencion de estadisticas de todas las promociones
     * 
     * @param locale locale
     * @return lista de estadisticas
     */
    public List<EstadisticasPromocion> getEstadisticasPromociones(Locale locale) {
        List<String> resultado = em.createNamedQuery("Promocion.getIdPromocionByIndEstado").setParameter("indEstado", Promocion.Estados.PUBLICADO.getValue()).getResultList();
        return resultado.stream().map((idPromocion) -> getEstadisticasPromocion(idPromocion, locale)).collect(Collectors.toList());
    }

    /**
     * obtencion de estadisticas de una promocion
     * 
     * @param idPromocion id de promocion
     * @param locale locale
     * @return estadistica de promocion
     */
    public EstadisticasPromocion getEstadisticasPromocion(String idPromocion, Locale locale) {
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "promo_not_found");
        }

        EstadisticasPromocion estadisticasPromocion = new EstadisticasPromocion();

        estadisticasPromocion.setIdPromocion(idPromocion);
        estadisticasPromocion.setNombrePromocion(promocion.getNombre());
        estadisticasPromocion.setNombreInternoPromocion(promocion.getNombreInterno());

        List<Object[]> list = em.createNamedQuery("DataPromosDiario.getMarcasElegiblesByIdPromocionGroupByMes").setParameter("idPromocion", idPromocion).getResultList();

        Calendar calendar = Calendar.getInstance();
        for (int i = 1; i <= 4; i++) {

            long cantMarcasPromedio;
            long cantMiembrosElegiblesPromedio;
            Optional<Object[]> optional = list.stream().filter((t) -> (int) t[0] == calendar.get(Calendar.MONTH)).findFirst();
            if (optional.isPresent()) {
                cantMarcasPromedio = ((long) optional.get()[2]) / ((long) optional.get()[1]);
                cantMiembrosElegiblesPromedio = ((long) optional.get()[3]) / ((long) optional.get()[1]);
            } else {
                cantMarcasPromedio = 0;
                cantMiembrosElegiblesPromedio = 0;
            }

            long cantVistasTotales;
            long cantVistasUnicas;
            try (Connection connection = ConnectionFactory.createConnection(configuration)) {

                Calendar fechaInicio = Calendar.getInstance();
                fechaInicio.set(Calendar.HOUR_OF_DAY, 23);
                fechaInicio.set(Calendar.MINUTE, 59);
                fechaInicio.set(Calendar.SECOND, 59);
                fechaInicio.set(Calendar.MILLISECOND, 999);
                fechaInicio.set(Calendar.MONTH, calendar.get(Calendar.MONTH));

                List<String> vistasMiembros = new ArrayList<>();
                Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "VISTAS_PROMOCION"));
                Scan scan = new Scan().setStartRow(Bytes.toBytes(String.format("%013d", fechaInicio.getTimeInMillis()) + "&00000000-0000-0000-0000-000000000000"));
                fechaInicio.set(Calendar.MILLISECOND, 1);
                fechaInicio.add(Calendar.MONTH, -1);
                scan.setStopRow(Bytes.toBytes(String.format("%013d", fechaInicio.getTimeInMillis()) + "&ZZZZZZZZ-ZZZZ-ZZZZ-ZZZZ-ZZZZZZZZZZZZ")).setFilter(new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("PROMOCION"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes(idPromocion)));
                try (ResultScanner scanner = table.getScanner(scan.setReversed(true))) {
                    scanner.forEach((result) -> vistasMiembros.add(Bytes.toString(result.getRow()).split("&")[1]));
                }

                cantVistasTotales = vistasMiembros.size();

                cantVistasUnicas = vistasMiembros.stream().distinct().count();
            } catch (IOException e) {
                System.out.println("1 - Error conecting to hbase: " + e.getMessage());
                throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
            }

            estadisticasPromocion.addPeriodo(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG_FORMAT, locale), cantMarcasPromedio, cantMiembrosElegiblesPromedio, cantVistasTotales, cantVistasUnicas);

            calendar.add(Calendar.MONTH, -1);
        }

        Set<String> miembros = new HashSet<>();
        miembros.addAll(em.createNamedQuery("PromocionListaMiembro.getIdMiembroByIdPromocionIndTipo").setParameter("idPromocion", idPromocion).setParameter("indTipo", Indicadores.INCLUIDO).getResultList());
        List<String> miembrosExcluidos = em.createNamedQuery("PromocionListaMiembro.getIdMiembroByIdPromocionIndTipo").setParameter("idPromocion", idPromocion).setParameter("indTipo", Indicadores.EXCLUIDO).getResultList();
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "ELEGIBLES_SEGMENTO"));
            List<String> segmentosIncluidos = em.createNamedQuery("PromocionListaSegmento.findAllIdSegmentoByIdPromocionIndTipo").setParameter("idPromocion", idPromocion).setParameter("indTipo", Indicadores.INCLUIDO).getResultList();
            if (segmentosIncluidos.isEmpty() && miembros.isEmpty()) {
                miembros.addAll(em.createNamedQuery("Miembro.findAllIdMiembro").getResultList());
            }
            for (String idSegmento : segmentosIncluidos) {
                Get get = new Get(Bytes.toBytes(idSegmento));
                get.addFamily(Bytes.toBytes("MIEMBROS"));
                Result result = table.get(get);
                if (table.exists(get)) {
                    miembros.addAll(result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                            .filter((t) -> Bytes.toBoolean(t.getValue()))
                            .map((t) -> Bytes.toString(t.getKey()))
                            .collect(Collectors.toList()));
                }
            }
            for (Object idSegmento : em.createNamedQuery("PromocionListaSegmento.findAllIdSegmentoByIdPromocionIndTipo").setParameter("idPromocion", idPromocion).setParameter("indTipo", Indicadores.EXCLUIDO).getResultList()) {
                Get get = new Get(Bytes.toBytes((String) idSegmento));
                get.addFamily(Bytes.toBytes("MIEMBROS"));
                Result result = table.get(get);
                if (table.exists(get)) {
                    miembros.removeAll(result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                            .filter((t) -> Bytes.toBoolean(t.getValue()))
                            .map((t) -> Bytes.toString(t.getKey()))
                            .collect(Collectors.toList()));
                }
            }
            miembros.removeAll(miembrosExcluidos);
            estadisticasPromocion.setCantMiembrosElegibles(miembros.size());

            List<String> vistasMiembros = new ArrayList<>();
            table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "VISTAS_PROMOCION"));
            try (ResultScanner scanner = table.getScanner(new Scan().setFilter(new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("PROMOCION"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes(idPromocion))))) {
                scanner.forEach((result) -> vistasMiembros.add(Bytes.toString(result.getRow()).split("&")[1]));
            }

            estadisticasPromocion.setCantVistasUnicasElegibles(miembros.stream().filter((t) -> vistasMiembros.contains(t)).count());
            estadisticasPromocion.setCantVistasTotales(vistasMiembros.size());
        } catch (IOException e) {
            System.out.println("Error conecting to hbase: " + e.getMessage());
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }

        return estadisticasPromocion;
    }

    //********************************************************************************************MISIONES********************************************************************************************
    
    /**
     * obtencion de estadisticas de misiones
     * 
     * @param locale locale
     * @return lista de estadisticas
     */
    public List<EstadisticasMision> getEstadisticasMisiones(Locale locale) {
        List<String> resultado = em.createNamedQuery("Mision.getIdMisionByIndEstado").setParameter("indEstado", Mision.Estados.PUBLICADO.getValue()).getResultList();
        return resultado.stream().map((idMision) -> getEstadisticasMision(idMision, locale)).collect(Collectors.toList());
    }

    /**
     * obtencion de estadistica de una mision
     * 
     * @param idMision id de mision
     * @param locale locale
     * @return estadistica
     */
    public EstadisticasMision getEstadisticasMision(String idMision, Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "challenge_not_found");
        }

        EstadisticasMision estadisticasMisiones = new EstadisticasMision();

        estadisticasMisiones.setIdMision(idMision);
        estadisticasMisiones.setNombreMision(mision.getNombre());

        Map<Long, RespuestaMision.EstadosRespuesta> respuestas = new HashMap<>();
        Set<String> miembros = new HashSet<>();
        miembros.addAll(em.createNamedQuery("MisionListaMiembro.getIdMiembroByIdMisionIndTipo").setParameter("idMision", idMision).setParameter("indTipo", Indicadores.INCLUIDO).getResultList());
        List<String> miembrosExcluidos = em.createNamedQuery("MisionListaMiembro.getIdMiembroByIdMisionIndTipo").setParameter("idMision", idMision).setParameter("indTipo", Indicadores.EXCLUIDO).getResultList();
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "ELEGIBLES_SEGMENTO"));
            List<String> segmentosIncluidos = em.createNamedQuery("MisionListaSegmento.getAllIdSegmentosByIdMisionIndTipo").setParameter("idMision", idMision).setParameter("indTipo", Indicadores.INCLUIDO).getResultList();
            if (segmentosIncluidos.isEmpty() && miembros.isEmpty()) {
                miembros.addAll(em.createNamedQuery("Miembro.findAllIdMiembro").getResultList());
            }
            for (String idSegmento : segmentosIncluidos) {
                Get get = new Get(Bytes.toBytes(idSegmento));
                get.addFamily(Bytes.toBytes("MIEMBROS"));
                Result result = table.get(get);
                if (table.exists(get)) {
                    miembros.addAll(result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                            .filter((t) -> Bytes.toBoolean(t.getValue()))
                            .map((t) -> Bytes.toString(t.getKey()))
                            .collect(Collectors.toList()));
                }
            }
            for (Object idSegmento : em.createNamedQuery("MisionListaSegmento.getAllIdSegmentosByIdMisionIndTipo").setParameter("idMision", idMision).setParameter("indTipo", Indicadores.EXCLUIDO).getResultList()) {
                Get get = new Get(Bytes.toBytes((String) idSegmento));
                get.addFamily(Bytes.toBytes("MIEMBROS"));
                Result result = table.get(get);
                if (table.exists(get)) {
                    miembros.removeAll(result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                            .filter((t) -> Bytes.toBoolean(t.getValue()))
                            .map((t) -> Bytes.toString(t.getKey()))
                            .collect(Collectors.toList()));
                }
            }
            miembros.removeAll(miembrosExcluidos);
            estadisticasMisiones.setCantMiembrosElegibles(miembros.size());

            List<String> vistasMiembros = new ArrayList<>();
            table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "VISTAS_MISION"));
            try (ResultScanner scanner = table.getScanner(new Scan().setFilter(new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("MISION"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes(idMision))))) {
                scanner.forEach((result) -> vistasMiembros.add(Bytes.toString(result.getRow()).split("&")[1]));
            }

            estadisticasMisiones.setCantVistasUnicasElegibles(miembros.stream().distinct().filter((t) -> vistasMiembros.contains(t)).count());
            estadisticasMisiones.setCantVistasTotales(vistasMiembros.size());

            long cantRespuestas = 0;
            long cantRespuestasAprobadas = 0;
            long cantRespuestasRechazadas = 0;
            table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "REGISTRO_MISION"));
            try (ResultScanner scanner = table.getScanner(new Scan().setFilter(new RowFilter(CompareFilter.CompareOp.EQUAL, new RegexStringComparator(idMision + "&.*"))).addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO")))) {
                for (Result result : scanner) {
                    RespuestaMision.EstadosRespuesta estado = RespuestaMision.EstadosRespuesta.get(Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"))));
                    if (estado == null) {
                        estado = RespuestaMision.EstadosRespuesta.PENDIENTE;
                    }
                    switch (estado) {
                        case GANADO: {
                            cantRespuestasAprobadas++;
                            break;
                        }
                        case FALLIDO: {
                            cantRespuestasRechazadas++;
                            break;
                        }
                    }
                    respuestas.put(Long.parseLong(Bytes.toString(result.getRow()).split("&")[2]), estado);
                    cantRespuestas++;
                }
            }
            estadisticasMisiones.setCantRespuestas(cantRespuestas);
            estadisticasMisiones.setCantRespuestasAprobadas(cantRespuestasAprobadas);
            estadisticasMisiones.setCantRespuestasRechazadas(cantRespuestasRechazadas);
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
        }

        List<Object[]> list = em.createNamedQuery("DataMisionesDiario.getElegiblesByIdMisionGroupByMes").setParameter("idMision", idMision).getResultList();

        Calendar calendar = Calendar.getInstance();
        for (int i = 1; i <= 4; i++) {

            long cantMiembrosElegiblesPromedio;
            Optional<Object[]> optional = list.stream().filter((t) -> (int) t[0] == calendar.get(Calendar.MONTH)).findFirst();
            if (optional.isPresent()) {
                cantMiembrosElegiblesPromedio = ((long) optional.get()[2]) / ((long) optional.get()[1]);
            } else {
                cantMiembrosElegiblesPromedio = 0;
            }

            long cantVistasTotales;
            long cantVistasUnicas;
            long cantRespuestas = 0;
            long cantRespuestasAprobadas = 0;
            long cantRespuestasRechazadas = 0;
            try (Connection connection = ConnectionFactory.createConnection(configuration)) {

                Calendar fechaInicio = Calendar.getInstance();
                fechaInicio.set(Calendar.HOUR_OF_DAY, 23);
                fechaInicio.set(Calendar.MINUTE, 59);
                fechaInicio.set(Calendar.SECOND, 59);
                fechaInicio.set(Calendar.MILLISECOND, 999);
                fechaInicio.set(Calendar.MONTH, calendar.get(Calendar.MONTH));

                List<String> vistasMiembros = new ArrayList<>();
                Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "VISTAS_MISION"));

                Scan scanVistas = new Scan()
                        .setFilter(new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("MISION"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes(idMision)))
                        .setStartRow(Bytes.toBytes(String.format("%013d", fechaInicio.getTimeInMillis()) + "&00000000-0000-0000-0000-000000000000"));
                long finalPeriodo = fechaInicio.getTimeInMillis();
                fechaInicio.set(Calendar.MILLISECOND, 1);
                fechaInicio.add(Calendar.MONTH, -1);
                scanVistas.setStopRow(Bytes.toBytes(String.format("%013d", fechaInicio.getTimeInMillis()) + "&ZZZZZZZZ-ZZZZ-ZZZZ-ZZZZ-ZZZZZZZZZZZZ"));
                long inicioPeriodo = fechaInicio.getTimeInMillis();

                try (ResultScanner scanner = table.getScanner(scanVistas.setReversed(true))) {
                    scanner.forEach((result) -> vistasMiembros.add(Bytes.toString(result.getRow()).split("&")[1]));
                }

                cantVistasTotales = vistasMiembros.size();

                cantVistasUnicas = vistasMiembros.stream().distinct().count();

                Supplier<Stream<Map.Entry<Long, RespuestaMision.EstadosRespuesta>>> supplier = () -> respuestas.entrySet().stream().filter((entry) -> entry.getKey() <= finalPeriodo && entry.getKey() >= inicioPeriodo);
                cantRespuestas = supplier.get().count();
                cantRespuestasAprobadas = supplier.get().filter((t) -> t.getValue() == RespuestaMision.EstadosRespuesta.GANADO).count();
                cantRespuestasRechazadas = supplier.get().filter((t) -> t.getValue() == RespuestaMision.EstadosRespuesta.FALLIDO).count();
            } catch (IOException e) {
                throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
            }

            estadisticasMisiones.addPeriodo(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG_FORMAT, locale), cantRespuestas, cantRespuestasAprobadas, cantRespuestasRechazadas, cantMiembrosElegiblesPromedio, cantVistasTotales, cantVistasUnicas);

            calendar.add(Calendar.MONTH, -1);
        }

        return estadisticasMisiones;
    }

    //********************************************************************************************NOTIFICACIONES********************************************************************************************
    /**
     * obtencion de estadisticas de notificaciones
     * 
     * @param locale locale
     * @return lista de estadisticas de notificaciones
     */
    public List<EstadisticasNotificacion> getEstadisticasNotificaciones(Locale locale) {
        List<String> resultado = em.createNamedQuery("Notificacion.getIdNotificacionNotByIndEstado").setParameter("indEstado", Notificacion.Estados.BORRADOR.getValue()).getResultList();
        return resultado.stream().map((idNotificacion) -> getEstadisticasNotificacion(idNotificacion, locale)).collect(Collectors.toList());
    }

    /**
     * obtencion de estadisticas de una notificacion
     * 
     * @param idNotificacion id de notificacion
     * @param locale locale
     * @return estadistica
     */
    public EstadisticasNotificacion getEstadisticasNotificacion(String idNotificacion, Locale locale) {
        Notificacion notificacion = em.find(Notificacion.class, idNotificacion);
        if (notificacion == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "notification_not_found");
        }

        EstadisticasNotificacion estadisticasNotificacion = new EstadisticasNotificacion();

        estadisticasNotificacion.setIdNotificacion(notificacion.getIdNotificacion());
        estadisticasNotificacion.setNombreInternoNotificacion(notificacion.getNombreInterno());
        estadisticasNotificacion.setNombreNotificacion(notificacion.getNombre());

        List<InstanciaNotificacion> instancias = em.createNamedQuery("InstanciaNotificacion.findAllByIdNotificacion").setParameter("idNotificacion", idNotificacion).getResultList();

        estadisticasNotificacion.setCantMiembrosEnviados(instancias.stream().map((instancia) -> instancia.getInstanciaNotificacionPK().getIdMiembro()).distinct().count());
        estadisticasNotificacion.setCantMsjsEnviados(instancias.size());
        estadisticasNotificacion.setCantMsjsAbiertos(instancias.stream().filter((instancia) -> instancia.getFechaVisto() != null).count());

        if (notificacion.getIndEvento() != Notificacion.Eventos.DETONADOR.getValue()) {
            Set<String> miembros = new HashSet<>();
            miembros.addAll(em.createNamedQuery("NotificacionListaMiembro.getIdMiembroByIdNotificacion").setParameter("idNotificacion", idNotificacion).getResultList());
            List<String> segmentos = em.createNamedQuery("NotificacionListaSegmento.getIdSegmentoByIdNotificacion").setParameter("idNotificacion", idNotificacion).getResultList();
            if (!segmentos.isEmpty()) {
                try (Connection connection = ConnectionFactory.createConnection(configuration)) {
                    Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "ELEGIBLES_SEGMENTO"));

                    List<Get> gets = new ArrayList<>();
                    segmentos.forEach((idSegmento) -> {
                        gets.add(new Get(Bytes.toBytes(idSegmento)));
                    });

                    for (Result result : table.get(gets)) {
                        if (!result.isEmpty()) {
                            result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream().filter((entry) -> (Bytes.toBoolean(entry.getValue()))).forEach((entry) -> {
                                miembros.add(Bytes.toString(entry.getKey()));
                            });
                        }
                    }
                } catch (IOException e) {
                    throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
                }
            }
            estadisticasNotificacion.setCantMiembrosObjetivo(miembros.stream().count());
        }

        Calendar calendar = Calendar.getInstance();
        for (int i = 1; i <= 4; i++) {

            Calendar fechaInicio = Calendar.getInstance();
            fechaInicio.set(Calendar.HOUR_OF_DAY, 23);
            fechaInicio.set(Calendar.MINUTE, 59);
            fechaInicio.set(Calendar.SECOND, 59);
            fechaInicio.set(Calendar.MILLISECOND, 999);
            fechaInicio.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
            Date finalPeriodo = fechaInicio.getTime();
            fechaInicio.set(Calendar.MILLISECOND, 1);
            fechaInicio.add(Calendar.MONTH, -1);
            Date inicioPeriodo = fechaInicio.getTime();

            estadisticasNotificacion.addPeriodo(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG_FORMAT, locale),
                    instancias.stream().filter((instancia) -> instancia.getFechaEnvio() != null && instancia.getFechaEnvio().before(finalPeriodo) && instancia.getFechaEnvio().after(inicioPeriodo)).count(),
                    instancias.stream().filter((instancia) -> instancia.getFechaVisto() != null && instancia.getFechaVisto().before(finalPeriodo) && instancia.getFechaVisto().after(inicioPeriodo)).count(),
                    instancias.stream().filter((instancia) -> instancia.getFechaEnvio() != null ? instancia.getFechaEnvio().before(finalPeriodo) && instancia.getFechaEnvio().after(inicioPeriodo) : instancia.getFechaCreacion().before(finalPeriodo) && instancia.getFechaCreacion().after(inicioPeriodo)).map((instancia) -> instancia.getInstanciaNotificacionPK().getIdMiembro()).distinct().count());

            calendar.add(Calendar.MONTH, -1);
        }

        return estadisticasNotificacion;
    }

    //********************************************************************************************PREMIOS********************************************************************************************
    /**
     * obtencion de estaditicas de premios
     * 
     * @param locale locale
     * @return lista de estadisticas
     */
    public List<EstadisticasPremio> getEstadisticasPremios(Locale locale) {
        List<String> resultado = em.createNamedQuery("Premio.getIdPremioByIndEstado").setParameter("indEstado", Premio.Estados.PUBLICADO.getValue()).getResultList();
        return resultado.stream().map((idPremio) -> getEstadisticasPremio(idPremio, locale)).collect(Collectors.toList());
    }

    /**
     * obtencion de estadisticas de un premio
     * 
     * @param idPremio id de premio
     * @param locale locale
     * @return estadisticas
     */
    public EstadisticasPremio getEstadisticasPremio(String idPremio, Locale locale) {
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "challenge_not_found");
        }

        EstadisticasPremio estadisticasPremio = new EstadisticasPremio();

        estadisticasPremio.setIdPremio(idPremio);
        estadisticasPremio.setNombrePremio(premio.getNombre());
        estadisticasPremio.setNombreInternoPremio(premio.getNombreInterno());
        
        estadisticasPremio.setCantIntentario(premio.getTotalExistencias());
        
        Set<String> miembros = new HashSet<>();
        miembros.addAll(em.createNamedQuery("PremioListaMiembro.getIdMiembroByIdPremioIndTipo").setParameter("idPremio", idPremio).setParameter("indTipo", Indicadores.INCLUIDO).getResultList());
        List<String> miembrosExcluidos = em.createNamedQuery("PremioListaMiembro.getIdMiembroByIdPremioIndTipo").setParameter("idPremio", idPremio).setParameter("indTipo", Indicadores.EXCLUIDO).getResultList();
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "ELEGIBLES_SEGMENTO"));
            List<String> segmentosIncluidos = em.createNamedQuery("PremioListaSegmento.getIdSegmentoByIdPremioIndTipo").setParameter("idPremio", idPremio).setParameter("indTipo", Indicadores.INCLUIDO).getResultList();
            if (segmentosIncluidos.isEmpty() && miembros.isEmpty()) {
                miembros.addAll(em.createNamedQuery("Miembro.findAllIdMiembro").getResultList());
            }
            for (String idSegmento : segmentosIncluidos) {
                Get get = new Get(Bytes.toBytes(idSegmento));
                get.addFamily(Bytes.toBytes("MIEMBROS"));
                Result result = table.get(get);
                if (table.exists(get)) {
                    miembros.addAll(result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                            .filter((t) -> Bytes.toBoolean(t.getValue()))
                            .map((t) -> Bytes.toString(t.getKey()))
                            .collect(Collectors.toList()));
                }
            }
            for (Object idSegmento : em.createNamedQuery("PremioListaSegmento.getIdSegmentoByIdPremioIndTipo").setParameter("idPremio", idPremio).setParameter("indTipo", Indicadores.EXCLUIDO).getResultList()) {
                Get get = new Get(Bytes.toBytes((String) idSegmento));
                get.addFamily(Bytes.toBytes("MIEMBROS"));
                Result result = table.get(get);
                if (table.exists(get)) {
                    miembros.removeAll(result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream()
                            .filter((t) -> Bytes.toBoolean(t.getValue()))
                            .map((t) -> Bytes.toString(t.getKey()))
                            .collect(Collectors.toList()));
                }
            }
            miembros.removeAll(miembrosExcluidos);
            estadisticasPremio.setCantMiembrosElegibles(miembros.size());
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
        }

        List<PremioMiembro> redenciones = em.createNamedQuery("PremioMiembro.findByIdPremioIndMotivoAsignacion").setParameter("idPremio", idPremio).setParameter("indMotivoAsignacion", PremioMiembro.Motivos.PREMIO.getValue()).getResultList();

        estadisticasPremio.setCantRedencionesElegibles(redenciones.stream().filter((redencion) -> miembros.contains(redencion.getIdMiembro().getIdMiembro())).count());
        estadisticasPremio.setCantRedenciones(redenciones.size());

        List<Object[]> list = em.createNamedQuery("DataPremiosDiario.getElegiblesByIdPremioGroupByMes").setParameter("idPremio", idPremio).getResultList();

        Calendar calendar = Calendar.getInstance();
        for (int i = 1; i <= 4; i++) {

            long cantMiembrosElegiblesPromedio;
            Optional<Object[]> optional = list.stream().filter((t) -> (int) t[0] == calendar.get(Calendar.MONTH)).findFirst();
            if (optional.isPresent()) {
                cantMiembrosElegiblesPromedio = ((long) optional.get()[2]) / ((long) optional.get()[1]);
            } else {
                cantMiembrosElegiblesPromedio = 0;
            }

            Calendar fechaInicio = Calendar.getInstance();
            fechaInicio.set(Calendar.HOUR_OF_DAY, 23);
            fechaInicio.set(Calendar.MINUTE, 59);
            fechaInicio.set(Calendar.SECOND, 59);
            fechaInicio.set(Calendar.MILLISECOND, 999);
            fechaInicio.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
            long finalPeriodo = fechaInicio.getTimeInMillis();
            fechaInicio.set(Calendar.MILLISECOND, 1);
            fechaInicio.add(Calendar.MONTH, -1);
            long inicioPeriodo = fechaInicio.getTimeInMillis();

            estadisticasPremio.addPeriodo(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG_FORMAT, locale), redenciones.stream().filter((redencion) -> redencion.getFechaAsignacion().getTime() >= inicioPeriodo && redencion.getFechaAsignacion().getTime() <= finalPeriodo).count(), cantMiembrosElegiblesPromedio);

            calendar.add(Calendar.MONTH, -1);
        }

        return estadisticasPremio;
    }

    //********************************************************************************************METRICA********************************************************************************************
    /**
     * obtencion de lista de estadisticas de metricas
     * @param locale locale
     * @return lista de estadisticas de metricas
     */
    public List<EstadisticasMetrica> getEstadisticasMetricas(Locale locale) {
        List<String> resultado = em.createNamedQuery("Metrica.getIdMetricaByIndEstado").setParameter("indEstado", Metrica.Estados.PUBLICADO.getValue()).getResultList();
        return resultado.stream().map((idMetrica) -> getEstadisticasMetrica(idMetrica, locale)).collect(Collectors.toList());
    }

    /**
     * obtencion de estadisticas de una metrica
     * @param idMetrica id de metrica
     * @param locale locale
     * @return estadistica
     */
    public EstadisticasMetrica getEstadisticasMetrica(String idMetrica, Locale locale) {
        Metrica metrica = em.find(Metrica.class, idMetrica);
        if (metrica == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "challenge_not_found");
        }

        EstadisticasMetrica estadisticasMetrica = new EstadisticasMetrica();
        estadisticasMetrica.setIdMetrica(idMetrica);
        estadisticasMetrica.setNombreMetrica(metrica.getNombre());
        estadisticasMetrica.setNombreInternoMetrica(metrica.getNombreInterno());

        Map<String, Double> registrosAcumulados = new TreeMap<>();
        Map<String, Double> registrosDisponibles = new TreeMap<>();
        Map<String, Double> registrosVencidos = new TreeMap<>();
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "REGISTRO_METRICA"));
            Scan scan = new Scan();
            scan.setFilter(new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("METRICA"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes(idMetrica)));
            Calendar calendar = Calendar.getInstance();
            table.getScanner(scan).forEach((result) -> {
                calendar.setTimeInMillis(Long.parseLong(Bytes.toString(result.getRow()).split("&")[0]));
                registrosAcumulados.merge(calendar.get(Calendar.YEAR) + "/" + calendar.get(Calendar.MONTH), Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("CANTIDAD"))), Double::sum);
                if (Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("CANTIDAD"))) != Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("DISPONIBLE")))) {
                    if (Bytes.toBoolean(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("VENCIDO")))) {
                        registrosVencidos.merge(calendar.get(Calendar.YEAR) + "/" + calendar.get(Calendar.MONTH), Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("CANTIDAD"))) - Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("DISPONIBLE"))), Double::sum);
                    } else {
                        registrosDisponibles.merge(calendar.get(Calendar.YEAR) + "/" + calendar.get(Calendar.MONTH), Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("CANTIDAD"))) - Bytes.toDouble(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("DISPONIBLE"))), Double::sum);
                    }
                }
            });
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }

        estadisticasMetrica.setCantAcumulada(registrosAcumulados.values().stream().mapToDouble(Double::doubleValue).sum());
        estadisticasMetrica.setCantRedimida(registrosDisponibles.values().stream().mapToDouble(Double::doubleValue).sum());
        estadisticasMetrica.setCantVencida(registrosVencidos.values().stream().mapToDouble(Double::doubleValue).sum());

        Calendar calendar = Calendar.getInstance();
        for (int i = 1; i <= 4; i++) {
            estadisticasMetrica.addPeriodo(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG_FORMAT, locale), registrosAcumulados.entrySet().stream().filter((entry) -> Integer.parseInt(entry.getKey().split("/")[0]) == calendar.get(Calendar.YEAR) && Integer.parseInt(entry.getKey().split("/")[1]) == calendar.get(Calendar.MONTH)).map(Map.Entry::getValue).mapToDouble(Double::doubleValue).sum(), registrosDisponibles.entrySet().stream().filter((entry) -> Integer.parseInt(entry.getKey().split("/")[0]) == calendar.get(Calendar.YEAR) && Integer.parseInt(entry.getKey().split("/")[1]) == calendar.get(Calendar.MONTH)).map(Map.Entry::getValue).mapToDouble(Double::doubleValue).sum(), registrosVencidos.entrySet().stream().filter((entry) -> Integer.parseInt(entry.getKey().split("/")[0]) == calendar.get(Calendar.YEAR) && Integer.parseInt(entry.getKey().split("/")[1]) == calendar.get(Calendar.MONTH)).map(Map.Entry::getValue).mapToDouble(Double::doubleValue).sum());

            calendar.add(Calendar.MONTH, -1);
        }
        
        estadisticasMetrica.setEstadisticasNivelMetrica(em.createNamedQuery("EstadisticaIndividualMetricaNivel.getByIdMetrica").setParameter("idMetrica", idMetrica).getResultList());

        return estadisticasMetrica;
    }

    /**
     * obtencion de estadisticas general de segmentacion de miembros
     * 
     * @param locale locale
     * @return estaditicas
     */
    public EstadisticaSegmentoGeneral getEstadisticasMiembrosSegmentosGeneral(Locale locale) {
        return em.createNamedQuery("EstadisticaSegmentoGeneral.findAll", EstadisticaSegmentoGeneral.class).getSingleResult();
    }

    /**
     * obtencion de estadisticas general de miembros
     * 
     * @param locale locale
     * @return lista de estadisticas
     */
    public List<EstadisticaMiembroGeneral> getEstadisticasMiembros(Locale locale) {
        return em.createNamedQuery("EstadisticaMiembroGeneral.findAll", EstadisticaMiembroGeneral.class).getResultList();
    }

    /**
     * obtencion de estadisticas individual de segmentos
     * 
     * @param locale locale
     * @return lista de estadisticas
     */
    public List<EstadisticaSegmentoIndividual> getEstadisticasSegmentosMiembrosIndividual(Locale locale) {
        return em.createNamedQuery("EstadisticaSegmentoIndividual.findAllByIndEstado", EstadisticaSegmentoIndividual.class).setParameter("indEstado", Segmento.Estados.ACTIVO.getValue()).getResultList();
    }

    /**
     * obtencion de estadisticas de miembros general
     * 
     * @param locale locale
     * @return estadisticas
     */
    public EstadisticaMiembroGeneral getEstadisticasMiembrosGeneral(Locale locale) {
        return em.createNamedQuery("EstadisticaMiembroGeneral.findAll", EstadisticaMiembroGeneral.class).getSingleResult();
    }

    /**
     * obtencion de estadisticas de misiones
     * 
     * @param locale locale
     * @return lista de estadisticas
     */
    public List<EstadisticaMisionIndividual> getEstadisticasMisionIndividual(Locale locale) {
        return em.createNamedQuery("EstadisticaMisionIndividual.findAll", EstadisticaMisionIndividual.class).getResultList();
    }
    
    /**
     * obtencion de estaditicas de mision general
     * 
     * @param locale locale
     * @return estadisticas
     */
    public EstadisticaMisionGeneral getEstadisticasMisionGeneral(Locale locale) {
        return em.createNamedQuery("EstadisticaMisionGeneral.findById", EstadisticaMisionGeneral.class).setParameter("id", 0).getSingleResult();
    }

    /**
     * obtencion de estaditicas de miembros sobre premios
     * 
     * @param locale locale
     * @return lista de estadisticas
     */
    public List<EstadisticaMiembroPremio> getEstadisticasMiembrosGanadores(Locale locale) {
        return em.createNamedQuery("EstadisticaMiembroPremio.findAll", EstadisticaMiembroPremio.class).getResultList();
    }
    
    /**
     * obtencion de estadisticas de balances mensuales para una metrica
     * 
     * @param year valor del año deseado
     * @param idMetrica id de metrica
     * @param locale locale
     * @return lista mensual del año con estadisticas
     */
    public List<BalanceMetricaMensual> getEstadisticasBalancesMensuales(String year, String idMetrica, Locale locale) {
        int ano;
        if (year==null) {
            ano = Year.now().getValue();
        } else {
            try {
                ano = Year.parse(year).getValue();
            } catch (DateTimeParseException e) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "queryparam_year_value_error");
            }
        }
        return idMetrica==null
                ? em.createNamedQuery("BalanceMetricaMensual.findByAno").setParameter("ano", ano).getResultList()
                : em.createNamedQuery("BalanceMetricaMensual.findByAnoIdMetrica").setParameter("ano", ano).setParameter("idMetrica", idMetrica).getResultList();
    }
}
