/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "ESTADISTICA_MIEMBRO_GENERAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadisticaMiembroGeneral.findAll", query = "SELECT e FROM EstadisticaMiembroGeneral e")
    , @NamedQuery(name = "EstadisticaMiembroGeneral.findById", query = "SELECT e FROM EstadisticaMiembroGeneral e WHERE e.id = :id")
    , @NamedQuery(name = "EstadisticaMiembroGeneral.findByCantMiembrosEdad1825", query = "SELECT e FROM EstadisticaMiembroGeneral e WHERE e.cantMiembrosEdad1825 = :cantMiembrosEdad1825")
    , @NamedQuery(name = "EstadisticaMiembroGeneral.findByCantMiembrosEdad2535", query = "SELECT e FROM EstadisticaMiembroGeneral e WHERE e.cantMiembrosEdad2535 = :cantMiembrosEdad2535")
    , @NamedQuery(name = "EstadisticaMiembroGeneral.findByCantMiembrosEdad3545", query = "SELECT e FROM EstadisticaMiembroGeneral e WHERE e.cantMiembrosEdad3545 = :cantMiembrosEdad3545")
    , @NamedQuery(name = "EstadisticaMiembroGeneral.findByCantMiembrosEdad4555", query = "SELECT e FROM EstadisticaMiembroGeneral e WHERE e.cantMiembrosEdad4555 = :cantMiembrosEdad4555")
    , @NamedQuery(name = "EstadisticaMiembroGeneral.findByCantMiembrosEdadMayor55", query = "SELECT e FROM EstadisticaMiembroGeneral e WHERE e.cantMiembrosEdadMayor55 = :cantMiembrosEdadMayor55")})
public class EstadisticaMiembroGeneral implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "CANT_MIEMBROS_EDAD_18_25")
    private Integer cantMiembrosEdad1825;
    @Column(name = "CANT_MIEMBROS_EDAD_25_35")
    private Integer cantMiembrosEdad2535;
    @Column(name = "CANT_MIEMBROS_EDAD_35_45")
    private Integer cantMiembrosEdad3545;
    @Column(name = "CANT_MIEMBROS_EDAD_45_55")
    private Integer cantMiembrosEdad4555;
    @Column(name = "CANT_MIEMBROS_EDAD_MAYOR_55")
    private Integer cantMiembrosEdadMayor55;
    
    @Column(name = "CANT_MIEMBROS_REFERIDOS")
    private Long cantMiembrosReferidos;

    public EstadisticaMiembroGeneral() {
    }

    public EstadisticaMiembroGeneral(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCantMiembrosEdad1825() {
        return cantMiembrosEdad1825;
    }

    public void setCantMiembrosEdad1825(Integer cantMiembrosEdad1825) {
        this.cantMiembrosEdad1825 = cantMiembrosEdad1825;
    }

    public Integer getCantMiembrosEdad2535() {
        return cantMiembrosEdad2535;
    }

    public void setCantMiembrosEdad2535(Integer cantMiembrosEdad2535) {
        this.cantMiembrosEdad2535 = cantMiembrosEdad2535;
    }

    public Integer getCantMiembrosEdad3545() {
        return cantMiembrosEdad3545;
    }

    public void setCantMiembrosEdad3545(Integer cantMiembrosEdad3545) {
        this.cantMiembrosEdad3545 = cantMiembrosEdad3545;
    }

    public Integer getCantMiembrosEdad4555() {
        return cantMiembrosEdad4555;
    }

    public void setCantMiembrosEdad4555(Integer cantMiembrosEdad4555) {
        this.cantMiembrosEdad4555 = cantMiembrosEdad4555;
    }

    public Integer getCantMiembrosEdadMayor55() {
        return cantMiembrosEdadMayor55;
    }

    public void setCantMiembrosEdadMayor55(Integer cantMiembrosEdadMayor55) {
        this.cantMiembrosEdadMayor55 = cantMiembrosEdadMayor55;
    }

    public Long getCantMiembrosReferidos() {
        return cantMiembrosReferidos;
    }

    public void setCantMiembrosReferidos(Long cantMiembrosReferidos) {
        this.cantMiembrosReferidos = cantMiembrosReferidos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadisticaMiembroGeneral)) {
            return false;
        }
        EstadisticaMiembroGeneral other = (EstadisticaMiembroGeneral) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.EstadisticaMiembroGeneral[ id=" + id + " ]";
    }
    
}
