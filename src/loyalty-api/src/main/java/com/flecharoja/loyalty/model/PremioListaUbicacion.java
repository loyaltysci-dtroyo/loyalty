/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO_LISTA_UBICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PremioListaUbicacion.findAll", query = "SELECT p FROM PremioListaUbicacion p"),
    @NamedQuery(name = "PremioListaUbicacion.findByIdUbicacion", query = "SELECT p FROM PremioListaUbicacion p WHERE p.premioListaUbicacionPK.idUbicacion = :idUbicacion"),
    @NamedQuery(name = "PremioListaUbicacion.countByIdPremio", query = "SELECT COUNT(p) FROM PremioListaUbicacion p WHERE p.premioListaUbicacionPK.idPremio = :idPremio"),
    @NamedQuery(name = "PremioListaUbicacion.findUbicacionesByIdPremio", query = "SELECT p.ubicacion FROM PremioListaUbicacion p WHERE p.premioListaUbicacionPK.idPremio = :idPremio"),
    @NamedQuery(name = "PremioListaUbicacion.countUbicacionesNotInPremioListaUbicacion", query = "SELECT COUNT(u) FROM Ubicacion u WHERE u.indEstado = :estado AND u.idUbicacion NOT IN (SELECT p.premioListaUbicacionPK.idUbicacion FROM PremioListaUbicacion p WHERE p.premioListaUbicacionPK.idPremio = :idPremio)"),
    @NamedQuery(name = "PremioListaUbicacion.findUbicacionesNotInPremioListaUbicacion", query = "SELECT m FROM Ubicacion m WHERE m.indEstado = :estado AND m.idUbicacion NOT IN (SELECT p.premioListaUbicacionPK.idUbicacion FROM PremioListaUbicacion p WHERE p.premioListaUbicacionPK.idPremio = :idPremio)"),
    @NamedQuery(name = "PremioListaUbicacion.countByIdPremioIndTipo", query = "SELECT COUNT(p) FROM PremioListaUbicacion p WHERE p.premioListaUbicacionPK.idPremio = :idPremio AND p.indTipo = :indTipo"),
    @NamedQuery(name = "PremioListaUbicacion.findUbicacionesByIdPremioIndTipo", query = "SELECT p.ubicacion FROM PremioListaUbicacion p WHERE p.premioListaUbicacionPK.idPremio = :idPremio AND p.indTipo = :indTipo")
})
public class PremioListaUbicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PremioListaUbicacionPK premioListaUbicacionPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_PREMIO", referencedColumnName = "ID_PREMIO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Premio premio;
    
    @JoinColumn(name = "ID_UBICACION", referencedColumnName = "ID_UBICACION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ubicacion ubicacion;

    public PremioListaUbicacion() {
    }

    public PremioListaUbicacion(PremioListaUbicacionPK premioListaUbicacionPK) {
        this.premioListaUbicacionPK = premioListaUbicacionPK;
    }

    public PremioListaUbicacion(String idUbicacion, String idPremio, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.premioListaUbicacionPK = new PremioListaUbicacionPK(idUbicacion, idPremio);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public PremioListaUbicacion(String idUbicacion, String idPremio) {
        this.premioListaUbicacionPK = new PremioListaUbicacionPK(idUbicacion, idPremio);
    }
    @ApiModelProperty(hidden = true)
    public PremioListaUbicacionPK getPremioListaUbicacionPK() {
        return premioListaUbicacionPK;
    }

    public void setPremioListaUbicacionPK(PremioListaUbicacionPK premioListaUbicacionPK) {
        this.premioListaUbicacionPK = premioListaUbicacionPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Premio getPremio() {
        return premio;
    }

    public void setPremio(Premio premio) {
        this.premio = premio;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (premioListaUbicacionPK != null ? premioListaUbicacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PremioListaUbicacion)) {
            return false;
        }
        PremioListaUbicacion other = (PremioListaUbicacion) object;
        if ((this.premioListaUbicacionPK == null && other.premioListaUbicacionPK != null) || (this.premioListaUbicacionPK != null && !this.premioListaUbicacionPK.equals(other.premioListaUbicacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.PremioListaUbicacion[ premioListaUbicacionPK=" + premioListaUbicacionPK + " ]";
    }
    
}
