package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.service.MetricaBean;
import com.flecharoja.loyalty.service.MiembroNivelMetricaBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase de recursos http disponibles para el acceso a funcionalidades del
 * manejo de metricas y niveles de metricas
 *
 * @author svargas
 */
@Api(value = "Metrica")
@Path("metrica")
public class MetricaResource {

    @EJB
    MetricaBean metricaBean; //EJB con los metodos de negocio para el manejo de metricas
    
    @EJB
    MiembroNivelMetricaBean miembroNivelMetricaBean;

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión
    
    @Context
    HttpServletRequest request;

    

    /**
     * Metodo que obtiene un listado de todas las metricas por un rango
     * establecido usando los parametros de cantidad y registro bajo un estado
     * de OK(200) ademas de encabezados indicando del rango aceptable maximo y
     * el rango actual de resultados, de ocurrir un error se retornara una
     * respuesta con un diferente estado junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param estados
     * @param busqueda
     * @param expiracion
     * @param ordenTipo
     * @param cantidad
     * @param registro
     * @param filtros
     * @param ordenCampo
     * @return Representacion del listado de metricas con toda su informacion en
     * formato JSON.
     */
     @ApiOperation(value = "Obtener metricas por estado o por defecto todos",
            responseContainer = "List",
            response = Metrica.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMetricas(
            @ApiParam(value = "Indicadores de estado de metricas") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de expiracion de metrica") @QueryParam("expiracion") String expiracion,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros  sobre donde aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
       
       
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.METRICAS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
         Map<String, Object> respuesta = metricaBean.getMetricas(estados, expiracion, busqueda, filtros, ordenTipo, ordenCampo, registro, cantidad, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Metodo que obtiene la informacion de una metrica especifica segun su
     * numero de identificacion bajo un estado de OK(200), de ocurrir un error
     * se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param id Numero de identificacion de la metrica deseada.
     * @return Representacion de la informacion de una metrica encontrada en
     * formato JSON.
     */
     @ApiOperation(value = "Obtener una métrica por su identificador",
            response = Metrica.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idMetrica}")
    @Produces(MediaType.APPLICATION_JSON)
    public Metrica getMetricaPorId(
            @ApiParam(value = "Identificador de la métrica")
            @PathParam("idMetrica") String id) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.METRICAS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return metricaBean.getMetrica(id,request.getLocale());
    }

    /**
     * Metodo que obtiene en respuesta de true/false si un nombre interno ya
     * esta almacenado bajo un estado de OK(200), de ocurrir un error se
     * retornara una respuesta con un diferente estado junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param nombre Valor del nombre interno a verificar
     * @return Palabra clave con los siguientes posibles valores: "true" Nombre
     * interno existe, "false" Nombre interno no existe
     */
    @ApiOperation(value = "Verificacion de existencia de nombre de una métrica")
    @ApiResponses(value = {
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("/exists/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean existsNombreInterno(
            @ApiParam(value = "Nombre de la métrica")
            @PathParam("nombre") String nombre) {
        
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.METRICAS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return metricaBean.existsNombreInterno(nombre);
    }

    /**
     * Metodo que manda a registrar en la bases de datos la informacion de una
     * nueva metrica y retorna el identificador de la metrica creada bajo un
     * estado de OK(200), de ocurrir un error se retornara una respuesta con un
     * diferente estado junto con un encabezado "Error-Reason" con un valor
     * numerico indicando la naturaleza del error.
     *
     * @param metrica Objeto que contenido la informacion de una nueva metrica a
     * registrar.
     * @return Informacion del resultado de la operacion.
     */
    @ApiOperation(value = "Registrar una métrica",
            response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertMetrica(
            @ApiParam(value = "Información de la métrica", required = true)
            Metrica metrica) {
        
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.METRICAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return metricaBean.insertMetrica(metrica, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que modifica la informacion almacenada de una metrica en la bases
     * de datos y retorna una respuesta con un estado de OK(200), de ocurrir un
     * error se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param metrica Objeto que contiene la informacion de una nueva metrica a
     * registrar.
     */
    @ApiOperation(value = "Modificacion de una métrica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void editMetrica( @ApiParam(value = "Información de la métrica") Metrica metrica) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.METRICAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        metricaBean.editMetrica(metrica, usuarioContext,request.getLocale());
    }

    /**
     * Metodo que elimina la informacion almacenada de una metrica en la bases
     * de datos y retorna una respuesta con un estado de OK(200), de ocurrir un
     * error se retornara una respuesta con un diferente estado junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param idMetrica Identificador de la metrica a remover.
     */
    @ApiOperation(value = "Eliminación o desactivación de una métrica")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idMetrica}")
    public void removeMetrica(
            @ApiParam(value = "Identificador de la métrica", required = true)
            @PathParam("idMetrica") String idMetrica) {
        
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.METRICAS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        metricaBean.removeMetrica(idMetrica, usuarioContext,request.getLocale());
    }
    

    @GET
    @Path("buscar/{busqueda}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchAtributos(
            @PathParam("busqueda") String busqueda,
            @QueryParam("dato") String estado,
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @QueryParam("registro") int registro) {

        return this.getMetricas(estado==null?null:Arrays.asList(estado.split("-")), null, busqueda, null, null, null, cantidad, registro);

    }

  
    @GET
    @Path("buscar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchAtributos(
            @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO)
            @QueryParam("cantidad") int cantidad,
            @QueryParam("registro") int registro,
            @QueryParam("estado") String estado) {
       return this.getMetricas(estado==null?null:Arrays.asList(estado.split("-")), null, null, null, null, null, cantidad, registro);
    }
    
    
     /**
     * Metodo acciona una tarea que obtiene un listado de miembros que han sido
     * asignados a una metrica, en el caso de error al ejecutar la tarea se
     * retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param idMetrica identificador unico de la insignia
     * @param tipo
     * @param estados
     * @param generos
     * @param estadosCiviles
     * @param cantidad Parametro opcional con un valor por defecto de 10 que define la cantidad maxima de registros por respuesta
     * @param contactoEmail
     * @param contactoSMS
     * @param contactoNotificacion
     * @param contactoEstado
     * @param educaciones
     * @param tieneHijos
     * @param registro Parametro opcional que define el numero de primer registro desde donde devolver el listado de entidades
     * @param busqueda
     * @param ordenTipo
     * @param filtros
     * @param ordenCampo
     * @return Representacion en JSON del listado de miembros
     */
    @ApiOperation(value = "Obtener miembros asociados a una metrica",
            responseContainer = "List",
            response = Miembro.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("{idMetrica}/miembros")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMiembrosInsignia(
            @ApiParam(value = "Identificador de la metrica", required = true)
            @PathParam("idMetrica") String idMetrica,
            @ApiParam(value = "Indicador del tipo de lista de miembros") @QueryParam("tipo") String tipo,
            @ApiParam(value = "Indicadores de estado de miembros") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de genero de miembros") @QueryParam("genero") List<String> generos,
            @ApiParam(value = "Indicadores de estado civil de miembros") @QueryParam("estado-civil") List<String> estadosCiviles,
            @ApiParam(value = "Indicadores de educacion de miembros") @QueryParam("educacion") List<String> educaciones,
            @ApiParam(value = "Indicador booleano de contacto de email") @QueryParam("contacto-email") String contactoEmail,
            @ApiParam(value = "Indicador booleano de contacto de sms") @QueryParam("contacto-sms") String contactoSMS,
            @ApiParam(value = "Indicador booleano de contacto de notificacion") @QueryParam("contacto-notificacion") String contactoNotificacion,
            @ApiParam(value = "Indicador booleano de contacto de estado") @QueryParam("contacto-estado") String contactoEstado,
            @ApiParam(value = "Indicador booleano de tiene hijos") @QueryParam("tiene-hijos") String tieneHijos,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = miembroNivelMetricaBean.getMiembrosMetrica(idMetrica, tipo, estados, generos, estadosCiviles, educaciones, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

}
