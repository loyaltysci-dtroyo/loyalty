/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

/**
 *
 * @author wtencio
 */
@Stateless
public class ReporteBean {

   private final String dataSource = "jdbc/LoyaltyDataSource";
       
    /**
     * Método que realiza el reporte en formato PDF de las existencias de
     * premios en una ubicacion o todas
     *
     * @param mes del cual se quieren ver las existencias
     * @param periodo del cual se quieren ver las existencias
     * @param ubicacion por la cual se desea filtrar el resultado
     * @param premio por el cual se desea filtrar el resultado
     * @param locale
     * @return archivo en base64
     */
    public String reporteConsolidadoPDF(Integer mes, Integer periodo, String ubicacion, String premio, Locale locale) {
        try {
            // Llamamos al metodo para obtener la conexion  
            InitialContext initialContext = new InitialContext();
            DataSource ds = (DataSource) initialContext.lookup(dataSource);
            Connection cn = ds.getConnection();
            //se crean los parametros de filtrado
            Map parametersPDF = new HashMap();
            parametersPDF.put("mes", mes);
            parametersPDF.put("periodo", periodo);
            parametersPDF.put("ubicacion", ubicacion);
            parametersPDF.put("premio", premio);
            parametersPDF.put("SUBREPORT_DIR", "//reportes//");
            //se busca el jasper del reporte donde se tenga guardado
            JasperReport jasperReport0 = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("//reportes//existenciasv2.jasper"));

            List<JasperPrint> lReports = new ArrayList();
            lReports.add(JasperFillManager.fillReport(jasperReport0, parametersPDF, cn));
            java.io.ByteArrayOutputStream reportePDF = new java.io.ByteArrayOutputStream();

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(lReports));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reportePDF));
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            configuration.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configuration);
            exporter.exportReport();
            //se obtiene el resultado en un arreglo de byte
            byte results[] = reportePDF.toByteArray();

            // Cerramos la conexion
            cn.close();
            String encoded;
            try {
                encoded = Base64.getEncoder().encodeToString(results);
            } catch (IllegalArgumentException e) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "file_invalid");
            }
            return encoded;

        } catch (NamingException | SQLException | JRException e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "error_document");
        }
    }

    /**
     * Método que crea el reporte de un documento de inventario
     *
     * @param documento identificador del documento
     * @param locale
     * @return archivo en base64
     */
    public String reporteDocumentoInventario(String documento, Locale locale) {
        try {
            // Llamamos al metodo para obtener la conexion  
            InitialContext initialContext = new InitialContext();
            DataSource ds = (DataSource) initialContext.lookup(dataSource);
            Connection cn = ds.getConnection();
            //se crean los parametros
            Map parametersPDF = new HashMap();
            parametersPDF.put("num_Documento", documento);
            parametersPDF.put("SUBREPORT_DIR", "//reportes//");
            //se busca el jasper del reporte
            JasperReport jasperReport0 = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("//reportes//documento.jasper"));

            List<JasperPrint> lReports = new ArrayList();
            lReports.add(JasperFillManager.fillReport(jasperReport0, parametersPDF, cn));
            java.io.ByteArrayOutputStream reportePDF = new java.io.ByteArrayOutputStream();

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(lReports));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reportePDF));
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            configuration.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configuration);
            exporter.exportReport();

            byte results[] = reportePDF.toByteArray();
            // Cerramos la conexion
            cn.close();
            //se convierte en base64
            String encoded;
            try {
                encoded = Base64.getEncoder().encodeToString(results);
            } catch (IllegalArgumentException e) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "file_invalid");
            }
            return encoded;

        } catch (NamingException | SQLException | JRException e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "error_document");
        }
    }

    /**
     * Método que crea el reporte de todos los movimientos de un articulo en un
     * rango de fechas determinadas
     *
     * @param fechaInicio del rango
     * @param fechaFin del rago
     * @param ubicacion por la cual se desea filtrar
     * @param premio por el cual se desea filtrar
     * @param tipoMovimiento por el cual se desea filtrar
     * @param locale
     * @return archivo en base64
     */
    public String reporteMovimientos(Long fechaInicio, Long fechaFin, String ubicacion, String premio, String tipoMovimiento, Locale locale) {
        try {
            // Llamamos al metodo para obtener la conexion  
            InitialContext initialContext = new InitialContext();
            DataSource ds = (DataSource) initialContext.lookup(dataSource);
            Connection cn = ds.getConnection();
            //creamos los filtros
            Map parametersPDF = new HashMap();
            parametersPDF.put("fechaInicio",new Timestamp(fechaInicio));
            parametersPDF.put("fechaFin", new Timestamp(fechaFin));
            parametersPDF.put("ubicacion", ubicacion);
            parametersPDF.put("premio", premio);
            parametersPDF.put("tipoMovimiento", tipoMovimiento);
            //se busca la plantilla del reporte
            JasperReport jasperReport0 = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("//reportes//movimientos.jasper"));

            List<JasperPrint> lReports = new ArrayList();
            lReports.add(JasperFillManager.fillReport(jasperReport0, parametersPDF, cn));
            java.io.ByteArrayOutputStream reportePDF = new java.io.ByteArrayOutputStream();

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(lReports));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reportePDF));
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            configuration.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configuration);
            exporter.exportReport();
            
            //se obtiene el resultado en un arreglo de byte
            byte results[] = reportePDF.toByteArray();
            // Cerramos la conexion
            cn.close();
            //se convierte el archivo a base64
            String encoded;
            try {
                encoded = Base64.getEncoder().encodeToString(results);
            } catch (IllegalArgumentException e) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "file_invalid");
            }
            return encoded;
        } catch (NamingException | SQLException | JRException e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "error_document");
        }

    }

    /**
     * Método que crea el reporte de catalogo de premios
     *
     * @param locale
     * @return archivo en base64
     */
    public String reporteCatalogoPremios(Locale locale) {
        try {
            // Llamamos al metodo para obtener la conexion  
            InitialContext initialContext = new InitialContext();
            DataSource ds = (DataSource) initialContext.lookup(dataSource);
            Connection cn = ds.getConnection();

            Map parametersPDF = new HashMap();
            //se busca la plantilla del reporte
            JasperReport jasperReport0 = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("//reportes//catalogoPremios.jasper"));

            List<JasperPrint> lReports = new ArrayList();
            lReports.add(JasperFillManager.fillReport(jasperReport0, parametersPDF, cn));
            java.io.ByteArrayOutputStream reportePDF = new java.io.ByteArrayOutputStream();

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(lReports));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reportePDF));
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            configuration.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configuration);
            exporter.exportReport();
            
            //se obtiene el resultado en un arreglo de byte
            byte results[] = reportePDF.toByteArray();
            // Cerramos la conexion
            cn.close();
            //se convierte el archivo a base64
            String encoded;
            try {
                encoded = Base64.getEncoder().encodeToString(results);
            } catch (IllegalArgumentException e) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "file_invalid");
            }
            return encoded;
        } catch (NamingException | SQLException | JRException e) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "error_document");
        }

    }
    
    
    /**
     * Método que crea el reporte de los carritos en un
     * rango de fechas determinadas y por categoria
     *
     * @param fechaInicio del rango
     * @param fechaFin del rago
     * @param idCategoria por la cual se desea filtrar
     * @param locale
     * @return archivo en base64
     */
    public String reporteCarritos(Long fechaInicio, Long fechaFin, String idCategoria, Locale locale) {
        try {
            // Llamamos al metodo para obtener la conexion  
            InitialContext initialContext = new InitialContext();
            DataSource ds = (DataSource) initialContext.lookup(dataSource);
            Connection cn = ds.getConnection();
            //creamos los filtros
            Map parametersPDF = new HashMap();
            parametersPDF.put("fechaInicio",new Timestamp(fechaInicio));
            parametersPDF.put("fechaFin", new Timestamp(fechaFin));
            parametersPDF.put("idCategoria", idCategoria);
            //se busca la plantilla del reporte
            JasperReport jasperReport0 = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("//reportes//carritos.jasper"));
            List<JasperPrint> lReports = new ArrayList();
            lReports.add(JasperFillManager.fillReport(jasperReport0, parametersPDF, cn));
            java.io.ByteArrayOutputStream reportePDF = new java.io.ByteArrayOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(lReports));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reportePDF));
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            configuration.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configuration);
            System.out.println("6");
            exporter.exportReport();
            System.out.println("7");
            //se obtiene el resultado en un arreglo de byte
            byte results[] = reportePDF.toByteArray();
            // Cerramos la conexion
            cn.close();
            //se convierte el archivo a base64
            String encoded;
            System.out.println("8");
            try {
                System.out.println("9");
                encoded = Base64.getEncoder().encodeToString(results);
                System.out.println("FIIIIINNNNNNNNNNNNN");
            } catch (IllegalArgumentException e) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "file_invalid");
            }
            return encoded;
        } catch (NamingException | SQLException | JRException e) {
            e.printStackTrace();
            System.out.println("Get message: " + e.getMessage());
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "error_document");
        }

    }
}
