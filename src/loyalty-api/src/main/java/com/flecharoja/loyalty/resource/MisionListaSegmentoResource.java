package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.service.MisionListaSegmentoBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase con recursos http para el manejo de la inclusion/exclusion de segmentos
 * a misiones
 *
 * @author svargas
 */
@Api(value = "Mision")
@Path("mision/{idMision}/segmento")
public class MisionListaSegmentoResource {

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @EJB
    MisionListaSegmentoBean listaSegmentoBean;

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión
    
    @Context
    HttpServletRequest request;

    /**
     * Identificador de promocion
     */
    @PathParam("idMision")
    String idMision;

    /**
     * Metodo que obtiene un listado de segmentos incluidos, excluidos o
     * disponibles para una mision por su identificador segun el parametro de
     * accion y en un rango de registros segun los parametros de registro y
     * cantidad, de ocurrir un error se retornara una Respuesta con un estado
     * erroneo junto con un encabezado "Error-Reason" con un valor numerico
     * indicando la naturaleza del error.
     *
     * @param accion Indicador del tipo de accion incluido/excluido/disponibles
     * @param estados Parametro opcional que contiene los estados de los
     * segmentos deseadas
     * @param tipos Parametros de indicadores de tipo de segmento (indEstatico)
     * @param busqueda Terminos de busqueda
     * @param filtros Parametros de campos de segmentos sobre donde aplicar los
     * filtros de busqueda
     * @param ordenTipo
     * @param ordenCampo
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Obtener lista de segmentos de mision",
            responseContainer = "List",
            response = Segmento.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSegmentos(
            @ApiParam(value = "Indicador del tipo de lista de segmentos") @QueryParam("accion") String accion,
            @ApiParam(value = "Indicadores de estado") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de tipo") @QueryParam("tipo") List<String> tipos,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = listaSegmentoBean.getSegmentos(idMision, accion, estados, tipos, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
            .entity(respuesta.get("resultado"))
            .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
            .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
            .build();
    }

    /**
     * Metodo que incluye o excluye una lista de identificadores de segmento a
     * una mision por su identificador segun el parametro de accion, de ocurrir
     * un error se retornara una Respuesta con un estado erroneo junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param listaIdSegmento Lista de identificadores de segmento
     * @param accion Parametro con el indicador de la accion deseada
     * (incluir/excluir)
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asginacion de lista de segmentos a mision")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void includeExcludeBatchSegmento(
            @ApiParam(value = "Identificadores de segmentos", required = true) List<String> listaIdSegmento,
            @ApiParam(value = "Indicador de tipo de lista a asignar", required = true)
            @QueryParam("accion") String accion) {
        if (accion == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, request.getLocale(), "attributes_invalid", "accion");
        }
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        listaSegmentoBean.includeExcludeBatchSegmento(idMision, listaIdSegmento, accion.toUpperCase().charAt(0), usuario,request.getLocale());
    }

    /**
     * Metodo que elimina la inclusion o exclusion de una lista de
     * identificadores de segmento a una mision por su identificador, de ocurrir
     * un error se retornara una Respuesta con un estado erroneo junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param listaIdSegmento Lista de identificadores de segmento
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asginacion de lista de segmentos a mision")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("remover-lista")
    public void removeBatchSegmento(
            @ApiParam(value = "Identificadores de segmentos", required = true) List<String> listaIdSegmento) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        listaSegmentoBean.removeBatchSegmento(idMision, listaIdSegmento, usuario,request.getLocale());
    }

    /**
     * Metodo que incluye o excluye un segmento a una mision por sus
     * identificadores segun el parametro de accion, de ocurrir un error se
     * retornara una Respuesta con un estado erroneo junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param idSegmento Identificador del segmento
     * @param accion Parametro con el indicador de la accion deseada
     * (incluir/excluir)
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Asignacion de segmento a mision")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idSegmento}")
    public void includeExcludeSegmento(
            @ApiParam(value = "Identificador de segmento", required = true)
            @PathParam("idSegmento") String idSegmento,
            @ApiParam(value = "Indicador de tipo de lista a asignar", required = true)
            @QueryParam("accion") String accion) {
        if (accion == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, request.getLocale(), "attributes_invalid", "accion");
        }
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        listaSegmentoBean.includeExcludeSegmento(idMision, idSegmento, accion.toUpperCase().charAt(0), usuario,request.getLocale());
    }

    /**
     * Metodo que elimina la inclusion o exclusion de un segmento a una mision
     * por sus identificadores segun el parametro de accion, de ocurrir un error
     * se retornara una Respuesta con un estado erroneo junto con un encabezado
     * "Error-Reason" con un valor numerico indicando la naturaleza del error.
     *
     * @param idSegmento Identificador del segmento
     * @return Respuesta con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asignacion de segmento a mision")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idMision", value = "Identificador de mision", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idSegmento}")
    public void removeSegmento(
            @ApiParam(value = "Identificador de segmento", required = true)
            @PathParam("idSegmento") String idSegmento) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        listaSegmentoBean.removeSegmento(idMision, idSegmento, usuario,request.getLocale());
    }
}
