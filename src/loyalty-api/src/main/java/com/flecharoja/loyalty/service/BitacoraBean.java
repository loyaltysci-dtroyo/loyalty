/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;;
import com.flecharoja.loyalty.model.Usuario;

import com.flecharoja.loyalty.util.Indicadores;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar las acciones de persistencia relacionado a la
 * bitácora general de la aplicación
 *
 * @author wtencio
 */
@Stateless
public class BitacoraBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Método que registra en la base de datos información de acciones
     * realizadas que afectan la base de datos
     *
     * @param t nombre de la clase en el cual sucede la acción
     * @param id id de la entidad que se esta viendo afectada por el cambio
     * @param usuario id del usuario que esta en sesión
     * @param accion indicador de la acción realizada
     * @param locale
     */
    public void logAccion(Class<?> t, String id, String usuario, String accion, Locale locale) {
        String nombreClase = t.getName();
        nombreClase = nombreClase.substring(nombreClase.lastIndexOf(".") + 1);

        JsonObjectBuilder createObjectBuilder = Json.createObjectBuilder();
        createObjectBuilder.add("entidad", nombreClase);
        createObjectBuilder.add("id", id);
        String detalle = createObjectBuilder.build().toString();

        Bitacora bitacora = new Bitacora();
        bitacora.setDetalle(detalle);
        bitacora.setAccion(accion);
        bitacora.setUsuario(em.getReference(Usuario.class, usuario));
        bitacora.setFecha(Calendar.getInstance().getTime());

        try {
            em.persist(bitacora);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", e.getConstraintViolations().stream().map((c)->c.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Metodo que realiza una busqueda en base a palabras claves sobre una serie
     * de atributos de la entidad bitacora, filtrado por un rango de tuplas que
     * puede devolver
     *
     * @param busqueda Cadena de texto con las palabras claves
     * @param accion Indicador del tipo de accion que se realizo
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return Listado de todos los grupos encontrados
     */
    public Map<String, Object> searchBitacora(String busqueda, String accion, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale,"pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        //formacion de query segun los parametros ingresados
        String[] palabrasClaves = busqueda.split(" ");
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<Bitacora> root = query.from(Bitacora.class);

            List<Predicate> predicates = new ArrayList<>();

            for (String palabraClave : palabrasClaves) {
                predicates.add(cb.like(cb.lower(root.get("detalle")), "%" + palabraClave.toLowerCase() + "%"));
            }

            switch (accion) {
                case "null": {
                    query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
                    break;
                }
                default: {
                    List<Predicate> predicates2 = new ArrayList<>();
                    for (String e : accion.split("-")) {
                        predicates2.add(cb.equal(root.get("accion"), e.toUpperCase()));
                    }
                    query.where(cb.or(predicates2.toArray(new Predicate[predicates2.size()])), cb.or(predicates.toArray(new Predicate[predicates.size()])));
                }
            }

            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            resultado = em.createQuery(query.select(root))
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO,locale,"database_connection_failed");
        }
        respuesta.put("rango",  registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Metodo que obtiene una lista de bitacoras almacenados por tipo de accion
     *
     * @param accion Valores de los indicadores de los tipos de datos a buscar
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return Lista de bitacoras
     */
    public Map<String,Object> getBitacoraAccion(String accion, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
           throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;
        try {
            if (accion.equalsIgnoreCase("null")) {

                total = ((Long) em.createNamedQuery("Bitacora.countAll").getSingleResult());
                total -= total - 1 < 0 ? 0 : 1;

                while (registro > total) {
                    registro = registro - cantidad > 0 ? registro - cantidad : 0;
                }
                resultado = em.createNamedQuery("Bitacora.findAll")
                        .setMaxResults(cantidad)
                        .setFirstResult(registro)
                        .getResultList();

            } else {
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery<Object> query = cb.createQuery();
                Root<Bitacora> root = query.from(Bitacora.class);

                List<Predicate> predicates = new ArrayList<>();
                for (String e : accion.split("-")) {
                    predicates.add(cb.equal(root.get("accion"), e.toUpperCase()));

                }
                query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));

                total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
                total -= total - 1 < 0 ? 0 : 1;
                while (registro > total) {
                    registro = registro - cantidad > 0 ? registro - cantidad : 0;
                }

                resultado = em.createQuery(query.select(root))
                        .setMaxResults(cantidad)
                        .setFirstResult(registro)
                        .getResultList();

            }
        } catch (IllegalArgumentException e) {
           throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }

       respuesta.put("rango",  registro + "-" + (registro + cantidad - 1) + "/" + total);
       respuesta.put("resultado", resultado);
       
       return respuesta;
    }

}
