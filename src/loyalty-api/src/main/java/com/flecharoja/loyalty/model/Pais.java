/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PAIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pais.findAll", query = "SELECT p FROM Pais p ORDER BY p.nombrePais ASC"),
    @NamedQuery(name = "Pais.findByIdPais", query = "SELECT p FROM Pais p WHERE p.idPais = :idPais"),
    @NamedQuery(name = "Pais.findByAlfa2", query = "SELECT p FROM Pais p WHERE p.alfa2 = :alfa2"),
    @NamedQuery(name = "Pais.findByAlfa3", query = "SELECT p FROM Pais p WHERE p.alfa3 = :alfa3"),
    @NamedQuery(name = "Pais.countByAlfa3", query = "SELECT COUNT(p) FROM Pais p WHERE p.alfa3 = :alfa3"),
    @NamedQuery(name = "Pais.countByAlfa2OrAlfa3", query = "SELECT COUNT(p) FROM Pais p WHERE p.alfa3 = :alfa3 OR p.alfa2 = :alfa2"),
    @NamedQuery(name = "Pais.findByCodigoNumero", query = "SELECT p FROM Pais p WHERE p.codigoNumero = :codigoNumero"),
    @NamedQuery(name = "Pais.findByNombrePais", query = "SELECT p FROM Pais p WHERE p.nombrePais = :nombrePais")})
public class Pais implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PAIS")
    private BigDecimal idPais;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "ALFA2")
    private String alfa2;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "ALFA3")
    private String alfa3;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODIGO_NUMERO")
    private long codigoNumero;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_PAIS")
    private String nombrePais;

    public Pais() {
    }

    public Pais(BigDecimal idPais) {
        this.idPais = idPais;
    }

    public Pais(BigDecimal idPais, String alfa2, String alfa3, long codigoNumero, String nombrePais) {
        this.idPais = idPais;
        this.alfa2 = alfa2;
        this.alfa3 = alfa3;
        this.codigoNumero = codigoNumero;
        this.nombrePais = nombrePais;
    }

    public BigDecimal getIdPais() {
        return idPais;
    }

    public void setIdPais(BigDecimal idPais) {
        this.idPais = idPais;
    }

    public String getAlfa2() {
        return alfa2;
    }

    public void setAlfa2(String alfa2) {
        this.alfa2 = alfa2;
    }

    public String getAlfa3() {
        return alfa3;
    }

    public void setAlfa3(String alfa3) {
        this.alfa3 = alfa3;
    }

    public long getCodigoNumero() {
        return codigoNumero;
    }

    public void setCodigoNumero(long codigoNumero) {
        this.codigoNumero = codigoNumero;
    }

    public String getNombrePais() {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais) {
        this.nombrePais = nombrePais;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPais != null ? idPais.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pais)) {
            return false;
        }
        Pais other = (Pais) object;
        if ((this.idPais == null && other.idPais != null) || (this.idPais != null && !this.idPais.equals(other.idPais))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Pais[ idPais=" + idPais + " ]";
    }
    
}
