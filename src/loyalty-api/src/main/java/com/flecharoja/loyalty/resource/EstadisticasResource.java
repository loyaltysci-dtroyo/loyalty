package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.exception.MyExceptionResponseBody;
import com.flecharoja.loyalty.model.BalanceMetricaMensual;
import com.flecharoja.loyalty.model.EstadisticaMiembroGeneral;
import com.flecharoja.loyalty.model.EstadisticaMiembroPremio;
import com.flecharoja.loyalty.model.EstadisticaMisionGeneral;
import com.flecharoja.loyalty.model.EstadisticaMisionIndividual;
import com.flecharoja.loyalty.model.EstadisticaSegmentoGeneral;
import com.flecharoja.loyalty.model.EstadisticaSegmentoIndividual;
import com.flecharoja.loyalty.model.EstadisticasMetrica;
import com.flecharoja.loyalty.model.EstadisticasMision;
import com.flecharoja.loyalty.model.EstadisticasNotificacion;
import com.flecharoja.loyalty.model.EstadisticasPremio;
import com.flecharoja.loyalty.model.EstadisticasPromocion;
import com.flecharoja.loyalty.service.RegistrosEstadisticasBean;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase de recursos http disponibles para el manejo de promociones.
 *
 * @author svargas
 */
@Api(value = "Estadisticas")
@Path("estadisticas")
public class EstadisticasResource {

    @EJB
    RegistrosEstadisticasBean estadisticasBean;

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @Context
    HttpServletRequest request;

    @ApiOperation(value = "Obtener estadisticas de todas las promociones",
            responseContainer = "List",
            response = EstadisticasPromocion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/promocion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EstadisticasPromocion> getEstadisticasPromociones() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PROMOCIONES_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasPromociones(request.getLocale());
    }

    @ApiOperation(value = "Obtener estadisticas de una promocion",
            response = EstadisticasPromocion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/promocion/{idPromocion}")
    @Produces(MediaType.APPLICATION_JSON)
    public EstadisticasPromocion getEstadisticasPromocion(@ApiParam(value = "Identificador de la promocion", required = true) @PathParam("idPromocion") String idPromocion) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PROMOCIONES_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasPromocion(idPromocion, request.getLocale());
    }

    @ApiOperation(value = "Obtener estadisticas de todas las mision",
            responseContainer = "List",
            response = EstadisticasMision.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/mision")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EstadisticasMision> getEstadisticasMisiones() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasMisiones(request.getLocale());
    }

    @ApiOperation(value = "Obtener estadisticas de una mision",
            response = EstadisticasMision.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/mision/{idMision}")
    @Produces(MediaType.APPLICATION_JSON)
    public EstadisticasMision getEstadisticasMision(@ApiParam(value = "Identificador de la mision", required = true) @PathParam("idMision") String idMision) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasMision(idMision, request.getLocale());
    }

    @ApiOperation(value = "Obtener estadisticas de todas las notificaciones",
            responseContainer = "List",
            response = EstadisticasNotificacion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/notificacion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EstadisticasNotificacion> getEstadisticasNotificaciones() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasNotificaciones(request.getLocale());
    }

    @ApiOperation(value = "Obtener estadisticas de una notificacion",
            response = EstadisticasNotificacion.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/notificacion/{idNotificacion}")
    @Produces(MediaType.APPLICATION_JSON)
    public EstadisticasNotificacion getEstadisticasNotificacion(@ApiParam(value = "Identificador de la notificacion", required = true) @PathParam("idNotificacion") String idNotificacion) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.CAMPANAS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasNotificacion(idNotificacion, request.getLocale());
    }

    @ApiOperation(value = "Obtener estadisticas de todos los premios",
            responseContainer = "List",
            response = EstadisticasPremio.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/premio")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EstadisticasPremio> getEstadisticasPremios() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasPremios(request.getLocale());
    }

    @ApiOperation(value = "Obtener estadisticas de un premio",
            response = EstadisticasPremio.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/premio/{idPremio}")
    @Produces(MediaType.APPLICATION_JSON)
    public EstadisticasPremio getEstadisticasPremio(@ApiParam(value = "Identificador de premio", required = true) @PathParam("idPremio") String idPremio) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PREMIOS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasPremio(idPremio, request.getLocale());
    }

    @ApiOperation(value = "Obtener estadisticas de todas las metricas",
            responseContainer = "List",
            response = EstadisticasMetrica.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/metrica")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EstadisticasMetrica> getEstadisticasMetricas() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.METRICAS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasMetricas(request.getLocale());
    }

    @ApiOperation(value = "Obtener estadisticas de una metrica",
            response = EstadisticasMetrica.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/metrica/{idMetrica}")
    @Produces(MediaType.APPLICATION_JSON)
    public EstadisticasMetrica getEstadisticasMetrica(@ApiParam(value = "Identificador de la metrica", required = true) @PathParam("idMetrica") String idMetrica) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.METRICAS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasMetrica(idMetrica, request.getLocale());
    }

    //Miembros nuevos para todos los segmentos
    @ApiOperation(value = "Obtener estadisticas de nuevos miembros en segmentos",
            responseContainer = "List",
            response = EstadisticaSegmentoIndividual.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/segmentos/miembros-nuevos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EstadisticaSegmentoIndividual> getEstadisticasSegmentoMiembrosNuevos() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasSegmentosMiembrosIndividual(request.getLocale());
    }

    //Miembros segmentados vs no segmentados
    @ApiOperation(value = "Obtener estadisticas de miembros segmentados y no segmentados",
            response = EstadisticaSegmentoGeneral.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/miembros/segmentados")
    @Produces(MediaType.APPLICATION_JSON)
    public EstadisticaSegmentoGeneral getEstadisticasMiembrosSegmentados() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.SEGMENTOS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasMiembrosSegmentosGeneral(request.getLocale());
    }

    //Distribución de edades por miembro
    @ApiOperation(value = "Obtener estadisticas de edades de miembros",
            response = EstadisticaMiembroGeneral.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/miembros/distribucion-edades")
    @Produces(MediaType.APPLICATION_JSON)
    public EstadisticaMiembroGeneral getEstadisticasMiembrosEdades() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasMiembrosGeneral(request.getLocale());
    }

    //Miembros mas activos (top 10 estadisticas de completado de mision en orden ascendente)
    @ApiOperation(value = "Lista de top 10 de miembros mas activos (complete de mision) por mision",
            responseContainer = "List",
            response = EstadisticaMisionIndividual.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/miembros/activos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EstadisticaMisionIndividual> getEstadisticasMiembrosActivos() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasMisionIndividual(request.getLocale());
    }
    
     //Miembros mas activos (top 10 estadisticas de completado de mision en orden ascendente)
    @ApiOperation(value = "Lista de top 10 de miembros mas activos (complete de mision) en general",
            response = EstadisticaMisionGeneral.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/mision/general")
    @Produces(MediaType.APPLICATION_JSON)
    public EstadisticaMisionGeneral getEstadisticasMisionIndividual() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasMisionGeneral(request.getLocale());
    }

//   Top de Miembros ganadores de premio 
    @ApiOperation(value = "Lista de top de miembros ganadores de premios",
            responseContainer = "List",
            response = EstadisticaMiembroPremio.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/miembros/ganadores")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EstadisticaMiembroPremio> getEstadisticasMiembrosGanadores() {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MISIONES_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        return estadisticasBean.getEstadisticasMiembrosGanadores(request.getLocale());
    }

    @ApiOperation(value = "Lista de estadisticas de balances de metrica anual por mes",
            responseContainer = "List",
            response = BalanceMetricaMensual.class)
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 401, message = "Error de autorizacion", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 403, message = "Operacion no permitida", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = MyExceptionResponseBody.class),
        @ApiResponse(code = 500, message = "Error del sistema", response = MyExceptionResponseBody.class)
    })
    @GET
    @Path("/balances-mensuales")
    @Produces(MediaType.APPLICATION_JSON)
    public List<BalanceMetricaMensual> getEstadisticasBalancesMensuales(@ApiParam(value = "Año a mostrar", defaultValue = "2017") @QueryParam("y") String anoString,
            @ApiParam(value = "Identificador de metrica a filtrar") @QueryParam("idm") String idMetrica) {
        String token;
        try {
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.METRICAS_LECTURA, token, request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        
        return estadisticasBean.getEstadisticasBalancesMensuales(anoString, idMetrica, request.getLocale());
    }
}
