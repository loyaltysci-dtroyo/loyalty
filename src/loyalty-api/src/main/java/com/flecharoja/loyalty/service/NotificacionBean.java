package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.PeticionEnvioNotificacion;
import com.flecharoja.loyalty.util.EnvioNotificacion;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado del manejo de datos y aplicacion de logica de negocio al manejo
 * de notificaciones.
 *
 * @author svargas, faguilar
 */
@Stateless
public class NotificacionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    @EJB
    UtilBean utilBean;
    
    /**
     * Obtencion de notificaciones en rango de registros (registro & cantidad) y
     * filtrado opcionalmente por indicadores y terminos de busqueda sobre
     * algunos campos
     * 
     * @param estados Listado de valores de indicadores de estados deseados
     * @param tipos Listado de valores de indicadores de tipos de mision deseados
     * @param eventos Listado de valores de tipos de eventos de mision deseados
     * @param objetivos
     * @param busqueda Terminos de busqueda
     * @param filtros Listado de campos sobre que aplicar los terminos de busqueda
     * @param cantidad Cantidad de registros deseados
     * @param registro Numero de registro inicial en el resultado
     * @return Listado de misiones deseadas
     */
    public Map<String, Object> getNotificaciones(List<String> estados, List<String> tipos, List<String> eventos, List<String> objetivos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Notificacion> root = query.from(Notificacion.class);
        
        query = Busquedas.getCriteriaQueryNotificaciones(cb, query, root, estados, tipos, eventos, objetivos, busqueda, filtros, ordenTipo, ordenCampo);
        
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * Metodo que obtiene la informacion de una notificacion segun su estado
     *
     * @param idNotificacion Identificador de la notificacion deseada
     * @param locale
     * @return Informacion de la notificacion encontrada
     */
    public Notificacion getNotificacionPorId(String idNotificacion,Locale locale) {
        //busqueda de una entidad de notificacion bajo el identificador pasado
        Notificacion campana = em.find(Notificacion.class, idNotificacion);

        if (campana == null) {//si no se encontro...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }

        return campana;
    }

    /**
     * Metodo que registra la informacion de una nueva notificacion
     *
     * @param notificacion Objeto con la informacion de la notificacion
     * @param locale
     * @return Identificador de la notificacion creada
     */
    public String insertNotificacion(Notificacion notificacion,Locale locale) {
        if (notificacion==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Notificacion");
        }
        Date fecha = Calendar.getInstance().getTime();//obtenncion de la fecha actual

        //establecimiento de atributos por defecto
        notificacion.setIndEstado(Notificacion.Estados.BORRADOR.getValue());
        notificacion.setFechaCreacion(fecha);
        notificacion.setFechaModificacion(fecha);
        notificacion.setNumVersion(new Long(1));
        
        Notificacion.Tipos tipoNotificacion = Notificacion.Tipos.get(notificacion.getIndTipo());
        if (tipoNotificacion==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "tipoNotificacion");
        }
        
        try {
            em.persist(notificacion);
            em.flush();
            bitacoraBean.logAccion(Notificacion.class, notificacion.getIdNotificacion(), notificacion.getUsuarioCreacion(), Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException  e) {//restriccion de atributos violada
             throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "attributes_invalid", e.getConstraintViolations().stream().map((t)->t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }

        return notificacion.getIdNotificacion();
    }

    /**
     * Metodo que modifica la informacion de una notificacion existente
     *
     * @param notificacion Objeto con la informacion de una notificacion
     * @param locale
     * @return informacion de la notificacion actualizada
     */
    public Notificacion editNotificacion(Notificacion notificacion, Locale locale) {
        if (notificacion==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Notificacion");
        }
        Notificacion original;
        try {
            //busqueda de la notificacion original
            original = em.find(Notificacion.class, notificacion.getIdNotificacion());
        } catch (IllegalArgumentException e) {//identificador de la notificacion no valido
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idNotificacion");
        }
        if (original == null || notificacion.getIndEstado() == null) {//si no se encontro alguna notificacion o el estado enviado no esta presente...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }

        //si el estado alamcenado es ejecutado o archivado o el estado almacenado es publicado y se desea pasar a borrador...
        if (original.getIndEstado().equals(Notificacion.Estados.EJECUTADO.getValue())
                || original.getIndEstado().equals(Notificacion.Estados.ARCHIVADO.getValue())
                || (original.getIndEstado().equals(Notificacion.Estados.PUBLICADO.getValue()) && notificacion.getIndEstado().equals(Notificacion.Estados.BORRADOR.getValue()))) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "notification_not_change_state");
        }
        
        Notificacion.Tipos tipoNotificacion = Notificacion.Tipos.get(notificacion.getIndTipo());
        if (tipoNotificacion==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "tipoNotificacion");
        }
        
        //borrado de contenido de despliegue al cambio de tipo de notificacion
        if (!original.getIndTipo().equals(notificacion.getIndTipo())) {
            switch (tipoNotificacion) {
                case EMAIL: {
                    if (original.getImagenArte()!=null && !original.getImagenArte().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                        try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                            filesUtils.deleteFile(original.getImagenArte());
                        } catch (Exception ex) {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                        }
                    }
                    break;
                }
                case NOTIFICACION_PUSH: {
                    if (original.getTexto()!=null) {
                        int i = 0;
                        while (original.getTexto().matches(original.getIdNotificacion()+"-"+i)) {                            
                            i++;
                        }
                        try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                            for (int j = 0; j < i; j++) {
                                filesUtils.deleteFile(MyAwsS3Utils.Folder.IMAGEN_NOTIFICACION_EMAIL.getPath()+"/"+original.getIdNotificacion()+"-"+j);
                            }
                        } catch (Exception ex) {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                        }
                    }
                    break;
                }
            }
            notificacion.setTexto(null);
        }

        Date fecha = Calendar.getInstance().getTime();//obtencion de la fecha actual

        if (notificacion.getIndEstado().equals(Notificacion.Estados.PUBLICADO.getValue())) { //si el estado nuevo es publicado...
            Notificacion.Eventos eventoNotificacion = Notificacion.Eventos.get(notificacion.getIndEvento());
            if (eventoNotificacion==null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indEvento");
            }
            if (eventoNotificacion.equals(Notificacion.Eventos.CALENDARIZADO) && (notificacion.getFechaEjecucion() == null || notificacion.getFechaEjecucion().before(Calendar.getInstance().getTime()))) {//si el indicador de evento es calendarizado y la fecha no esta presente o esta en el pasado...
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "notification_badly_scheduled");
            }
            
            //el texto y datos de notificacion deben estar presentes
            if (notificacion.getEncabezadoArte()==null || notificacion.getEncabezadoArte().isEmpty() || notificacion.getTexto()==null || notificacion.getTexto().isEmpty()) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "encabezadoArte, texto");
            }
        }
        
        //se establecen atributos por defecto
        notificacion.setFechaModificacion(fecha);
        
        String urlImagenOriginal = original.getImagenArte();
        String urlImagenNueva = null;
        List<String> archivosSubidos = new ArrayList<>();
        List<String> archivosSubidosOriginales = new ArrayList<>();
        List<String> archivosSubidosResultantes = new ArrayList<>();
        if (Notificacion.Tipos.get(original.getIndTipo())==Notificacion.Tipos.EMAIL && original.getTexto()!=null) {
            String urlRegex = "(<img src=\"https:((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
            Pattern pattern = Pattern.compile(urlRegex);
            Matcher urlMatcher = pattern.matcher(original.getTexto());

            while (urlMatcher.find()) {
                String url = notificacion.getTexto().substring(urlMatcher.start(0), urlMatcher.end(0));
                url = url.substring(url.indexOf("\"") + 1);
                archivosSubidosOriginales.add(url);
            }
        }
        
        if (tipoNotificacion==Notificacion.Tipos.NOTIFICACION_PUSH) {
            
            //si el atributo de imagen viene nula, se utiliza la predeterminada
            if (notificacion.getImagenArte() == null || notificacion.getImagenArte().trim().isEmpty()) {
                notificacion.setImagenArte(Indicadores.URL_IMAGEN_PREDETERMINADA);
            } else {
                //ver si el valor en el atributo de imagen, difiere del almacenado
                if (urlImagenOriginal==null || !urlImagenOriginal.equals(notificacion.getImagenArte())) {
                    //se le intenta subir
                    try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                        urlImagenNueva = filesUtils.uploadImage(notificacion.getImagenArte(), MyAwsS3Utils.Folder.IMAGEN_NOTIFICACION_PUSH, null);
                        notificacion.setImagenArte(urlImagenNueva);
                    } catch (Exception e) {
                        throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA,locale, "invalid_image");
                    }
                }
            }
            if (notificacion.getIndObjetivo()!=null) {
                Notificacion.Objectivos objectivo = Notificacion.Objectivos.get(notificacion.getIndObjetivo());
                if (objectivo==null) {
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indObjetivo");
                }
                switch (objectivo) {
                    case PROMOCION: {
                        if (notificacion.getPromocion()==null) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "promocion");
                        } else {
                            try {
                                notificacion.setPromocion(em.getReference(Promocion.class, notificacion.getPromocion().getIdPromocion()));
                            } catch (IllegalArgumentException e) {
                                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idPromocion");
                            }
                        }
                        break;
                    }
                    case MISION: {
                        if (notificacion.getMision()==null || notificacion.getMision().getIdMision()==null || em.find(Mision.class, notificacion.getMision().getIdMision())==null) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "mision");
                        }
                        break;
                    }
                    case RECOMPENSA: {
                        if (notificacion.getPremio()==null || notificacion.getPremio().getIdPremio()==null || em.find(Premio.class, notificacion.getPremio().getIdPremio())==null) {
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "premio");
                        }
                        break;
                    }
                }
            }
        } else {
            if (tipoNotificacion==Notificacion.Tipos.EMAIL) {
                notificacion.setImagenArte(null);
                notificacion.setIndObjetivo(null);
                notificacion.setPromocion(null);
                notificacion.setMision(null);
                notificacion.setPremio(null);
                
                if (notificacion.getTexto()!=null) {
                    String html = notificacion.getTexto();
                    String htmlResult = "";
                    Pattern pattern = Pattern.compile("<img src=\"data:.*?\\/.*?;base64,.*?\">");
                    Matcher matcher = pattern.matcher(html);
                    int index = 0;
                    int newIndex;
                    int i=0;
                    while (matcher.find()) {            
                        newIndex = matcher.start();
                        String tipo = html.substring(html.indexOf(":", newIndex)+1, html.indexOf(";", html.indexOf("\"", newIndex)+1));
                        String base64 = html.substring(html.indexOf(",", newIndex)+1, html.indexOf("\"", html.indexOf("\"", newIndex)+1));

                        String url;
                        try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                            url = filesUtils.uploadImage(base64, MyAwsS3Utils.Folder.IMAGEN_NOTIFICACION_EMAIL, null);
                            archivosSubidos.add(url);
                        } catch (Exception ex) {
                            if (!archivosSubidos.isEmpty()) {
                                try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                                    archivosSubidos.forEach((t) -> filesUtils.deleteFile(t));
                                } catch (Exception e) {
                                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                                }
                            }
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "url");
                        }

                        htmlResult = htmlResult+(html.substring(index, html.indexOf("\"", newIndex)+1))+url;
                        index = html.indexOf("\"", html.indexOf("\"", newIndex)+1);
                        i++;
                    }
                    notificacion.setTexto(htmlResult+(html.substring(index)));
                    
                    String urlRegex = "(<img src=\"https:((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
                    pattern = Pattern.compile(urlRegex);
                    Matcher urlMatcher = pattern.matcher(notificacion.getTexto());

                    while (urlMatcher.find()) {
                        String url = notificacion.getTexto().substring(urlMatcher.start(0), urlMatcher.end(0));
                        url = url.substring(url.indexOf("\"") + 1);
                        archivosSubidosResultantes.add(url);
                    }
                }
            }
        }

        try {
            //se almacena la notificacion modificada y se guarda un registro de la accion en bitacora
            em.merge(notificacion);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//restriccion de atributo violado o numero de version de la entidad antiguo
            switch (tipoNotificacion) {
                case EMAIL: {
                    if (!archivosSubidos.isEmpty()) {
                        try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                            archivosSubidos.forEach((url) -> filesUtils.deleteFile(url));
                        } catch (Exception ex) {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                        }
                    }
                    break;
                }
                case NOTIFICACION_PUSH: {
                    if (urlImagenNueva!=null) {
                        try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                            filesUtils.deleteFile(urlImagenNueva);
                        } catch (Exception ex) {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                        }
                    }
                    break;
                }
            }
            if(e instanceof ConstraintViolationException){
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "attributes_invalid", ((ConstraintViolationException )e).getConstraintViolations().stream().map((t)->t.getPropertyPath().toString()).collect(Collectors.joining(", ")) );
            }else{
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
           
        }
        
        try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
            if (urlImagenNueva != null && urlImagenOriginal!=null && !urlImagenOriginal.equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) { //si se guardo una nueva imagen, se elimina la anterior
                filesUtils.deleteFile(urlImagenOriginal);
            }
            archivosSubidosOriginales.stream().filter((url) -> !archivosSubidosResultantes.contains(url)).forEach((url) -> filesUtils.deleteFile(url));
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
        }
        
        bitacoraBean.logAccion(Notificacion.class, notificacion.getIdNotificacion(), notificacion.getUsuarioModificacion(), Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        
        return notificacion;
    }
    
    /**
     * contacto con api interno para el envio de notificacion
     * 
     * @param notificacion data
     * @param locale locale
     */
    public void enviarNotificacion (Notificacion notificacion, Locale locale) {
        if (Notificacion.Estados.get(notificacion.getIndEstado()) == Notificacion.Estados.PUBLICADO && Notificacion.Eventos.get(notificacion.getIndEvento()) == Notificacion.Eventos.MANUAL) {
            if (((long)em.createNamedQuery("NotificacionListaMiembro.countByIdNotificacion").setParameter("idNotificacion", notificacion.getIdNotificacion()).getSingleResult())==0 &&
                    ((long)em.createNamedQuery("NotificacionListaSegmento.countByIdNotificacion").setParameter("idNotificacion", notificacion.getIdNotificacion()).getSingleResult())==0) {
                throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "notification_without_objective_members");
            }            
            PeticionEnvioNotificacion peticionEnvioNotificacion = new PeticionEnvioNotificacion();
            peticionEnvioNotificacion.setIdNotificacion(notificacion.getIdNotificacion());
            if (new EnvioNotificacion().enviarNotificacion(peticionEnvioNotificacion) != null) {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, "notificacion enviandose...");
            }
        }
    }

    /**
     * Metodo que elimina o archiva una notificacion por su identificador
     *
     * @param idNotificacion Identificador de la notificacion
     * @param usuarioModificacion Identificador del usuario modificador
     * @param locale
     */
    public void removeNotificacion(String idNotificacion, String usuarioModificacion,Locale locale) {
        Notificacion notificacion = em.find(Notificacion.class, idNotificacion);//se busca la entidad alacenada
        if (notificacion == null) {//si no se encontro la entidad...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }

        Notificacion.Estados estadoNotificacion = Notificacion.Estados.get(notificacion.getIndEstado());
        switch(estadoNotificacion) {
            case BORRADOR: {
                switch (Notificacion.Tipos.get(notificacion.getIndTipo())) {
                    case NOTIFICACION_PUSH: {
                        if (notificacion.getImagenArte()!=null && !notificacion.getImagenArte().equals(Indicadores.URL_IMAGEN_PREDETERMINADA)) {
                            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                                filesUtils.deleteFile(notificacion.getImagenArte());
                            } catch (Exception ex) {
                                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                            }
                        }
                        break;
                    }
                    case EMAIL: {
                        if (notificacion.getTexto()!=null) {
                            String urlRegex = "(<img src=\"https:((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
                            Pattern pattern = Pattern.compile(urlRegex);
                            Matcher urlMatcher = pattern.matcher(notificacion.getTexto());

                            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                            while (urlMatcher.find())
                                {
                                    String url = notificacion.getTexto().substring(urlMatcher.start(0),urlMatcher.end(0));
                                    url = url.substring(url.indexOf("\"")+1);
                                    filesUtils.deleteFile(url);
                                }
                            } catch (Exception ex) {
                                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
                            }
                        }
                        break;
                    }
                }
                em.remove(notificacion);//se elimina de la db
                bitacoraBean.logAccion(Notificacion.class, notificacion.getIdNotificacion(), usuarioModificacion, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
                break;
            }
            case PUBLICADO: {
                //estabecimiento de atributos por defecto
                notificacion.setIndEstado(Notificacion.Estados.ARCHIVADO.getValue());
                notificacion.setFechaModificacion(Calendar.getInstance().getTime());
                notificacion.setUsuarioModificacion(usuarioModificacion);

                //guardado de la modificacion y registro de la accion en bitacora
                em.merge(notificacion);
                bitacoraBean.logAccion(Notificacion.class, notificacion.getIdNotificacion(), usuarioModificacion, Bitacora.Accion.ENTIDAD_ARCHIVADA.getValue(),locale);
                break;
            }
            default: {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "attributes_invalid", "indEstado");
            }
        }
    }

    /**
     * Metodo que verifica la existencia de un nombre interno en algun otro
     * registro
     *
     * @param nombreInterno Nombre interno a verificar su existencia
     * @return La existencia del nombre interno
     */
    public boolean existsNombreInterno(String nombreInterno) {
        //se cuentan todos los registro con el nombre interno igual al pasado y si este es mayor que 0 (existe al menos 1)...
        return ((Long) em.createNamedQuery("Notificacion.countByNombreInterno").setParameter("nombreInterno", nombreInterno).getSingleResult()) > 0;
    }
}
