package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.ListaReglas;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia
 * relacionado a listas de reglas de segmentos
 *
 * @author svargas
 */
@Stateless
public class ListaReglasBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene la lista de reglas de un segmento segun su
     * identificador y en un rango establecido por parametros
     *
     * @param idSegmento Identificacion del segmento
     * @param registro Numero de registro inicial
     * @param cantidad Cantidad de registro en el resultado
     * @param locale
     * @return Lista con la informacion de la lista de reglas
     */
    public Map<String, Object> getListasReglas(String idSegmento, int registro, int cantidad,Locale locale) {
        //verificacion de que la entidad exista
        try {
            em.getReference(Segmento.class, idSegmento).getIdSegmento();//se obtiene la referencia del segmento y se detona su busqueda
        } catch (EntityNotFoundException e) {//si no se encontro...
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale,"segment_id_invalid");
        }

        //verifica que la cantidad solicitada este dentro del rango aceptado, de no ser asi se respondera informando acerca de parametros erroneos junto con la cabezera con el valor de rango aceptado
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        Map<String, Object> respuesta = new HashMap<>();

        Long total = ((Long) em.createNamedQuery("ListaReglas.countByIdSegmento").setParameter("idSegmento", idSegmento).getSingleResult());//obtencion del total de registros
        total -= total - 1 < 0 ? 0 : 1;//se reduce en uno para que calce con el ultimo registro

        //en el caso de que el registro solicitado sobrepasa el ultimo, se reduce en la cantidad de registros solicitados hasta que este dentro de un rango valido
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se recupera el listado en el rango establecido por los parametros
        List<ListaReglas> resultado;
        try {
            resultado = em.createNamedQuery("ListaReglas.findByIdSegmento")
                    .setParameter("idSegmento", idSegmento)
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();
        } catch (IllegalArgumentException e) {//posiblemente pueda ocurrir al establecer los parametros de registro y cantidad erroneos (ejm: menores a 0)
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }

        //se retorna la respuesta con el listado y encabezados acerca del rango y total y cantidad maxima aceptado
        respuesta.put("resultado", resultado);
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        
        return respuesta;
    }

    /**
     * Metodo que retorna la informacion de una lista de reglas por su
     * identificador
     *
     * @param idSegmento Identificador del segmento
     * @param idLista Identificacion de la lista
     * @param locale
     * @return Informacion de la lista de reglas
     */
    public ListaReglas getListaReglasPorId(String idSegmento, String idLista,Locale locale) {
        ListaReglas listaReglas = em.find(ListaReglas.class, idLista);//se busca la lista de reglas
        if (listaReglas == null) {//si no se encontro...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE,locale, "rules_list_not_found");
        }
        if (!listaReglas.getIdSegmento().getIdSegmento().equals(idSegmento)) {//si el identificador del segmento de la lista defiere al del parametro...
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS,locale, "segment_id_invalid");
        }
        return listaReglas;
    }

    /**
     * Metodo que almacena la informacion de una lista de reglas ligado a un
     * segmento por su identificador
     *
     * @param idSegmento Identificador del segmento
     * @param listaReglas Objeto con la informacion de la lista de reglas
     * @param usuario Identificador del usuario creador
     * @param locale
     * @return Identificador de la lista creada
     */
    public String insertListaReglas(String idSegmento, ListaReglas listaReglas, String usuario, Locale locale) {
        if (listaReglas==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "information_empty", this.getClass().getSimpleName());
        }
        //verificacion de la existencia de entidad
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {//si no se encontro la entidad...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE,locale, "segment_not_found");
        }

        //verificacion de edicion a entidad archivada
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale,"operation_over_archived",this.getClass().getSimpleName());
        }

        //establecimiento de datos por defecto
        Date date = Calendar.getInstance().getTime();
        listaReglas.setFechaCreacion(date);
        listaReglas.setFechaModificacion(date);
        listaReglas.setNumVersion(1l);

        listaReglas.setUsuarioCreacion(usuario);
        listaReglas.setUsuarioModificacion(usuario);
        listaReglas.setIdSegmento(segmento);

        try {
            //registro de la lista y de la accion en bitacora
            em.persist(listaReglas);
            em.flush();
        } catch (ConstraintViolationException e) {//en el caso de que la informacion no sea valida
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        
        bitacoraBean.logAccion(ListaReglas.class, listaReglas.getIdLista(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        
        return listaReglas.getIdLista();
    }

    /**
     * Metodo que edita la informacion almacenada de una lista de reglas ligado
     * a un segmento por su identificador
     *
     * @param idSegmento Identificador del segmento
     * @param listaReglas Objeto con la informacion de la lista de reglas
     * @param usuario Identificador del usuario modificador
     * @param locale
     */
    public void editListaReglas(String idSegmento, ListaReglas listaReglas, String usuario,Locale locale) {
        if (listaReglas==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "information_empty", this.getClass().getSimpleName());
        }
        //verificacion de la existencia de entidades
        ListaReglas original;
        try {
            original = em.find(ListaReglas.class, listaReglas.getIdLista());//se busca la lista
        } catch (IllegalArgumentException e) {//si el identificador de la lista no es valido
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE,locale, "rules_list_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);//se busca el segmento
        if (segmento == null) {//si el segmento no se pudo encontrar...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE,locale, "segment_not_found");
        }
        if (original == null) {//si la lista no se pudo encontrar...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE,locale, "rules_list_not_found");
        }

        //verificacion de edicion a entidad archivada
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale,"operation_over_archived",this.getClass().getSimpleName());
        }

        //verificacion de identificadores inmutables de entidad
        if (!original.getIdSegmento().getIdSegmento().equals(idSegmento)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS,locale, "segment_id_invalid");
        }

        listaReglas.setIdSegmento(segmento);
        listaReglas.setFechaModificacion(Calendar.getInstance().getTime());
        //TODO obtener de keycloak el accesstoken y su idToken, del idToken obtener el id de usuario pertinente y asignarlo a los siguientes atributos:
        listaReglas.setUsuarioModificacion(usuario);

        try {
            //se modifica la entidad y se registra la accion en bitacora
            em.merge(listaReglas);
            em.flush();
        } catch (ConstraintViolationException | OptimisticLockException e) {//en el caso de que la informacion no sea valida
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }
        
        if (original.getOrden().compareTo(listaReglas.getOrden())!=0) {
            try {
                myKafkaUtils.sendRequestActualizacionSegmento(idSegmento);
            } catch (Exception ex) {
                Logger.getLogger(ListaReglasBean.class.getName()).log(Level.WARNING, null, ex);
            }
        }
        
        bitacoraBean.logAccion(ListaReglas.class, listaReglas.getIdLista(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * Metodo que intercambia el orden entre dos listas de reglas
     *
     * @param idSegmento Identificador del segmento
     * @param listas Objeto con la informacion de la lista de reglas
     * @param idUsuario Identificador del usuario modificador
     * @param locale
     */
    public void swapOrdenListas(String idSegmento, List<ListaReglas> listas, String idUsuario, Locale locale) {
        if (listas.size() != 2) {//en el caso de que el tamaño de las listas a intercambiar de lugar sea mayor a 2
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS,locale, "more_lists");
        }

        //se obtienen los valores de cada lista
        ListaReglas lista0, lista1;
        try {
            lista0 = em.find(ListaReglas.class, listas.get(0).getIdLista());
            lista1 = em.find(ListaReglas.class, listas.get(1).getIdLista());
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "bad_id_rule_list");
        }

        //se verifica que ninguna sea nula (no se encontro alguna)
        if (lista0 == null || lista1 == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "information_empty", this.getClass().getSimpleName());
        }
        
        lista0.setUsuarioModificacion(idUsuario);
        lista1.setUsuarioModificacion(idUsuario);

        //verificacion de que las listas pertenescan al mismo segmento y que el segmento no este archivado
        if (!lista0.getIdSegmento().getIdSegmento().equals(lista1.getIdSegmento().getIdSegmento())
                || lista0.getIdSegmento().getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "rule_list_not_belong_segment");
        }

        //se verifica que alguna sea referente del segmento pasado por parametros
        if (!lista0.getIdSegmento().getIdSegmento().equals(idSegmento)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale,"rule_list_not_segment");
        }

        //Se intercambian valores
        Long orden = lista0.getOrden();
        lista0.setOrden(lista1.getOrden());
        lista1.setOrden(orden);

        //se modifican las entidades
        em.merge(lista0);
        em.merge(lista1);
        
        try {
            myKafkaUtils.sendRequestActualizacionSegmento(idSegmento);
        } catch (Exception ex) {
            Logger.getLogger(ListaReglasBean.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    /**
     * Metodo que elimina una lista de reglas de la bases de datos en base a su
     * identificador
     *
     * @param idSegmento Identificador del segmento
     * @param idLista Identificador de la lista de reglas
     * @param usuario usuario en sesion
     * @param locale
     */
    public void deleteListaReglas(String idSegmento, String idLista, String usuario,Locale locale) {
        ListaReglas listaReglas = em.find(ListaReglas.class, idLista);//se busca la lista de reglas
        if (listaReglas == null) {//si no se encontro
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA,locale, "information_empty", this.getClass().getSimpleName());
        }

        //se verifica que la lista sea referente del segmento por parametro
        if (!listaReglas.getIdSegmento().getIdSegmento().equals(idSegmento)) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS,locale, "segment_id_invalid");
        }

        //verificacion de edicion a entidad archivada
        if (listaReglas.getIdSegmento().getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale,"operation_over_archived",this.getClass().getSimpleName());
        }

        //se elimina la lista
        em.remove(listaReglas);

        //se obtiene las listas de reglas restantes y se edita el orden de cada uno, de ser necesario (los que esten por encima)
        Long orden = listaReglas.getOrden();
        List<ListaReglas> lista = em.createNamedQuery("ListaReglas.findByIdSegmento").setParameter("idSegmento", idSegmento).getResultList();
        for (ListaReglas temp : lista) {
            //si el orden es mayor al eliminado
            if (temp.getOrden().compareTo(orden) > 0) {
                temp.setOrden(temp.getOrden()-1);
                //se modifica la lista
                em.merge(temp);
            }
        }
        em.flush();
        bitacoraBean.logAccion(ListaReglas.class, idLista, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);//se registra la accion en la bitacora
    }
}
