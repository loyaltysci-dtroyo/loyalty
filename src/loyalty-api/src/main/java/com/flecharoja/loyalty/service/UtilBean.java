package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Pais;
import com.flecharoja.loyalty.model.Ubicacion;

import com.flecharoja.loyalty.util.Indicadores;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.time.DateUtils;

/**
 * Clase con metodos adicionales
 *
 * @author wtencio, svargas
 */
@Stateless
public class UtilBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Método que obtiene la lista de paises del mundo
     *
     * @return lista de paises
     */
    public List<Pais> getPaises() {
        return em.createNamedQuery("Pais.findAll").getResultList();

    }
    
    /**
     * Método que obtiene la información de un país en especifico, relacionado
     * con el alfa 3 que pasa por parametro
     *
     * @param alfa3 identificador único del país
     * * @param locale
     * @return objeto tipo Pais con la información encontrada
     */
    public Pais getPaisPorNombre(String alfa3, Locale locale) {
        Pais pais = new Pais();
        try {
            pais = (Pais) em.createNamedQuery("Pais.findByAlfa3").setParameter("alfa3", alfa3.toUpperCase()).getSingleResult();
        } catch (NoResultException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "country_not_found");
        } catch (NonUniqueResultException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "country_undefined");
        }
        return pais;
    }

    /**
     * Método que retorna una cantidad establecida de sugerencias a las palabras
     * que son ingresadas por los usuarios
     *
     * @param entidad identificador de la entidad en la que se desea
     * buscar(miembro o ubicacion)
     * @param atributo identificador del atributo de la entidad escogida a
     * buscar
     * @param valor cadena de texto con las palabras que ha ingresado el usuario
     * @param locale
     * @return lista de palabras sugeridas para la búsqueda
     */
    public Map<String, Object> getSugerencias(Character entidad, String atributo, String valor,Locale locale) {
        try {
            Map<String, Object> respuesta = new HashMap<>();
            List<String> resultados = new ArrayList<>();
            CriteriaBuilder cb = em.getCriteriaBuilder();
            //si la entidad es miembro
            if (entidad == Indicadores.IND_ENTIDAD_MIEMBRO) {
                CriteriaQuery<String> query = cb.createQuery(String.class);
                Root<Miembro> root = query.from(Miembro.class);

                //si la entidad es miembro con atributo ciudad
                if (atributo.equals(Miembro.Residencia.CIUDAD.getValue())) {
                    Predicate predicates = cb.like(cb.lower(root.get("ciudadResidencia")), valor.toLowerCase() + "%");
                    query.where(cb.or(predicates));
                    resultados = em.createQuery(query.select(root.get("ciudadResidencia")).groupBy(root.get("ciudadResidencia"))).setMaxResults(4).getResultList();

                    //si la entidad es miembro con el atributo estado
                } else if (atributo.equals(Miembro.Residencia.ESTADO.getValue())) {
                    Predicate predicates = cb.like(cb.lower(root.get("estadoResidencia")), valor.toLowerCase() + "%");
                    query.where(cb.or(predicates));
                    resultados = em.createQuery(query.select(root.get("estadoResidencia")).groupBy(root.get("estadoResidencia"))).setMaxResults(4).getResultList();

                }

                //si la entidad es ubicacion
            } else if (entidad == Indicadores.IND_ENTIDAD_UBICACION) {
                CriteriaQuery<String> query = cb.createQuery(String.class);
                Root<Ubicacion> root = query.from(Ubicacion.class);

                //si la entidad es ubicacion con atributo ciudad
                if (atributo.equalsIgnoreCase(Ubicacion.Direccion.CIUDAD.getValue())) {
                    Predicate predicates = cb.like(cb.lower(root.get("indDirCiudad")), valor.toLowerCase() + "%");
                    query.where(cb.or(predicates));
                    resultados = em.createQuery(query.select(root.get("indDirCiudad")).groupBy(root.get("indDirCiudad"))).setMaxResults(4).getResultList();
                    //si la entidad es ubicacion con atributo estado
                } else if (atributo.equalsIgnoreCase(Ubicacion.Direccion.ESTADO.getValue())) {
                    Predicate predicates = cb.like(cb.lower(root.get("indDirEstado")), valor.toLowerCase() + "%");
                    query.where(cb.or(predicates));
                    resultados = em.createQuery(query.select(root.get("indDirEstado")).groupBy(root.get("indDirEstado"))).setMaxResults(4).getResultList();

                }
            }

            respuesta.put("resultado", resultados);
            return respuesta;

        }  catch (QueryTimeoutException e) {
             throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }

    }

    /**
     * Método que permite obtener las edades de los miembros del sistema
     * clasificados por edades
     *
     * @param indicadorActivos
     * @param locale
     * @return json en formato string
     */
    public String getMiembrosRangoEdad(String indicadorActivos, Locale locale) {
        List<Miembro> miembros = new ArrayList<>();
        //obtengo la lista de miembros
        if (indicadorActivos != null && Character.toUpperCase(indicadorActivos.charAt(0)) == Miembro.Estados.ACTIVO.getValue()) {
            miembros = em.createNamedQuery("Miembro.findAllActivos").getResultList();
        } else {
            miembros = em.createNamedQuery("Miembro.findAll").getResultList();
        }

        JsonObjectBuilder json = Json.createObjectBuilder();
        int rango1524 = 0;
        int rango2534 = 0;
        int rango3544 = 0;
        int rango4554 = 0;
        int rango5564 = 0;
        int otros = 0;

        List<Integer> lista = new ArrayList<>();
        //obtengo la fecha de hoy
        Calendar hoy = Calendar.getInstance();
        //creo la instancia calendar de la fecha de nacimiento
        Calendar fechaNacimiento = Calendar.getInstance();
        //obtengo el año actual
        int añoHoy = hoy.get(Calendar.YEAR);
        //si la lista de miembros es vacia no se realiza nada
        if (miembros.isEmpty()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "member_not_found");
        }

        for (Miembro miembro : miembros) {
            if (miembro.getFechaNacimiento() == null) {
                lista.add(1);
            } else {
                //paso la fecha de nacimiento a un calendar
                fechaNacimiento.setTime(miembro.getFechaNacimiento());

                //resto para obtener los años
                int año = hoy.get(Calendar.YEAR) - fechaNacimiento.get(Calendar.YEAR);
                //seteo el año actual a la fecha de cumpleaños
                fechaNacimiento.set(Calendar.YEAR, añoHoy);
                //verifico que si la fecha resultante esta antes de la fecha de cumpleaños y no sea el dia de cumpleaños le reste uni
                if (hoy.before(fechaNacimiento) && !DateUtils.isSameDay(hoy, fechaNacimiento)) {
                    año = año - 1;
                }
                lista.add(año);
            }
        }
        //si la lista de edades es vacia, no se puede realizar nada
        if (lista.isEmpty()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "member_not_found");
        }
        //recorro las edades para clasificar por rangos
        for (Integer edad : lista) {
            if (edad >= 15 && edad < 25) {
                rango1524 += 1;
            } else if (edad >= 25 && edad < 35) {
                rango2534 += 1;
            } else if (edad >= 35 && edad < 45) {
                rango3544 += 1;
            } else if (edad >= 45 && edad < 55) {
                rango4554 += 1;
            } else if (edad >= 55 && edad < 65) {
                rango5564 += 1;
            } else {
                otros += 1;
            }
        }
        //creo el objeto json con los resultados de los rangos
        json.add("15-24", rango1524);
        json.add("25-34", rango2534);
        json.add("35-44", rango3544);
        json.add("45-54", rango4554);
        json.add("55-64", rango5564);
        json.add("otros", otros);
        //se convierte en un string y lo retorno
        String resultado = json.build().toString();
        return resultado;
    }

    /**
     * Método que obtiene la cantidad de miembros que hay en el sistema por
     * genero y por estado
     *
     * @param indicadorEstado indicador de estado
     * @return resultado
     */
    public String getCantGenero(String indicadorEstado) {

        JsonObjectBuilder json = Json.createObjectBuilder();
        long femenino = 0;
        long masculino = 0;
        long otros = 0;
        //si el indicador es diferente de null y es activo
        if (indicadorEstado != null && Character.toUpperCase(indicadorEstado.charAt(0)) == Miembro.Estados.ACTIVO.getValue()) {
            femenino = (long) em.createNamedQuery("Miembro.countGeneroEstado").setParameter("indGenero", Miembro.ValoresIndGenero.FEMENINO.getValue()).setParameter("indEstado", Miembro.Estados.ACTIVO.getValue()).getSingleResult();
            masculino = (long) em.createNamedQuery("Miembro.countGeneroEstado").setParameter("indGenero", Miembro.ValoresIndGenero.MASCULINO.getValue()).setParameter("indEstado", Miembro.Estados.ACTIVO.getValue()).getSingleResult();
            otros = (long) em.createNamedQuery("Miembro.countAllActivos").getSingleResult() - (femenino + masculino);
            //si el indicador no esta o es diferente de activo
        } else {
            femenino = (long) em.createNamedQuery("Miembro.countGenero").setParameter("indGenero", Miembro.ValoresIndGenero.FEMENINO.getValue()).getSingleResult();
            masculino = (long) em.createNamedQuery("Miembro.countGenero").setParameter("indGenero", Miembro.ValoresIndGenero.MASCULINO.getValue()).getSingleResult();
            otros = (long) em.createNamedQuery("Miembro.countAll").getSingleResult() - (femenino + masculino);
        }
        //seteo el json
        json.add("femenino", femenino);
        json.add("masculino", masculino);
        json.add("noDefinido", otros);

        String respuesta = json.build().toString();
        return respuesta;
    }

    public String typeActivity() {
        JsonObjectBuilder json = Json.createObjectBuilder();
        //CANTIDAD DE LAS MISIONES
        int countActivityMisiones = 0;
        int countMembersMisiones = 0;

        //actualizacion de perfil
        return json.build().toString();
    }

}
