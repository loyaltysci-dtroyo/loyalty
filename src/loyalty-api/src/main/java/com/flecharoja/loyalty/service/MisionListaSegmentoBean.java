package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionListaSegmento;
import com.flecharoja.loyalty.model.MisionListaSegmentoPK;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 *
 * @author svargas
 */
@Stateless
public class MisionListaSegmentoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene un listado de segmentos asignados a una mision en
     * un rango de respuesta y opcionalmente por el tipo de accion
     * (incluido/excluido)
     *
     * @param idMision Identificador de la mision
     * @param indAccion Indicador del tipo de accion deseado
     * @param estados Listado de valores de indicadores de estados deseados
     * @param tipos Listado de valores de indicadores de tipos de segmento (estatico/dinamico) deseados
     * @param busqueda Terminos de busqueda
     * @param filtros Listado de campos sobre que aplicar los terminos de busqueda
     * @param registro Numero de registro inicial
     * @param cantidad Cantidad de registros deseados
     * @param locale
     * @return Respuesta con el listado de segmentos y su rango
     */
    public Map<String, Object> getSegmentos(String idMision, String indAccion, List<String> estados, List<String> tipos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Segmento> root = query.from(Segmento.class);
        
        //formacion del query de busqueda de segmentos
        query = Busquedas.getCriteriaQuerySegmentos(cb, query, root, estados, tipos, busqueda, filtros, ordenTipo, ordenCampo);
        
        //segun el tipo de accion se forma un query adicional para su busqueda y filtrado
        Subquery<String> subquery = query.subquery(String.class);
        Root<MisionListaSegmento> rootSubquery = subquery.from(MisionListaSegmento.class);
        subquery.select(rootSubquery.get("misionListaSegmentoPK").get("idSegmento"));
        if (indAccion==null) {
            subquery.where(cb.equal(rootSubquery.get("misionListaSegmentoPK").get("idMision"), idMision));
            if (query.getRestriction()!=null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idSegmento")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idSegmento")).value(subquery)));
            }
        } else {
            switch(indAccion.toUpperCase().charAt(0)) {
                case Indicadores.INCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("misionListaSegmentoPK").get("idMision"), idMision), cb.equal(rootSubquery.get("indTipo"), Indicadores.INCLUIDO));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idSegmento")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idSegmento")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.EXCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("misionListaSegmentoPK").get("idMision"), idMision), cb.equal(rootSubquery.get("indTipo"), Indicadores.EXCLUIDO));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idSegmento")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idSegmento")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.DISPONIBLES: {
                    subquery.where(cb.equal(rootSubquery.get("misionListaSegmentoPK").get("idMision"), idMision));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.equal(root.get("indEstado"), Segmento.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idSegmento")).value(subquery).not()));
                    } else {
                        query.where(cb.equal(root.get("indEstado"), Segmento.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idSegmento")).value(subquery).not()));
                    }
                    break;
                }
                default: {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "indAccion");
                }
            }
        }
        
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * Metodo que asigna (incluye o excluye) a un segmento de una mision
     *
     * @param idMision Identificador de la mision
     * @param idSegmento Identificador del segmento
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesión
     * @param locale
     */
    public void includeExcludeSegmento(String idMision, String idSegmento, Character indAccion, String usuario,Locale locale) {
        //verificacion que los id's sean validos
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        //verificacion de entidades archivadas/borrador
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())
                || !Segmento.Estados.get(segmento.getIndEstado()).equals(Segmento.Estados.ACTIVO)) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "operation_over_archived", "Mision, Segmento");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            MisionListaSegmento promocionListaSegmento = new MisionListaSegmento(idSegmento, idMision, indAccion, date, usuario);

            em.merge(promocionListaSegmento);
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        bitacoraBean.logAccion(MisionListaSegmento.class, idMision + "/" + idSegmento, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * Metodo que revoca la asignacion de un segmento a una mision
     *
     * @param idMision Identificador de la mision
     * @param idSegmento Identificador del segmento
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeSegmento(String idMision, String idSegmento, String usuario,Locale locale) {
        //verificacion que los id's sean validos
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
        }
        try {
            em.getReference(Segmento.class, idSegmento).getIdSegmento();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        //verificacion de entidades archivadas
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", "Mision");
        }

        try {
            //verificacion que la entidad exista y su eliminacion
            em.remove(em.getReference(MisionListaSegmento.class, new MisionListaSegmentoPK(idSegmento, idMision)));
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_list_segment_not_found");
        }

        bitacoraBean.logAccion(MisionListaSegmento.class, idMision + "/" + idSegmento, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }

    /**
     * Metodo que asigna (incluye o excluye) una lista de segmentos de una
     * mision
     *
     * @param idMision Identificador de la mision
     * @param listaIdSegmento Lista de identificadores de segmentos
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesión
     * @param locale
     */
    public void includeExcludeBatchSegmento(String idMision, List<String> listaIdSegmento, Character indAccion, String usuario,Locale locale) {
        //verificacion que los id's sean validos
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
        }

        listaIdSegmento = listaIdSegmento.stream().distinct().collect(Collectors.toList());

        //verificacion de entidades archivadas
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", "Mision");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            for (String idSegmento : listaIdSegmento) {
                //verificacion que los id's sean validos
                Segmento segmento = em.find(Segmento.class, idSegmento);
                if (segmento == null) {
                    em.clear();
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
                }

                //verificacion de entidades archivadas
                if (!Segmento.Estados.get(segmento.getIndEstado()).equals(Segmento.Estados.ACTIVO)) {
                    em.clear();
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "segment_inactive");
                }

                em.merge(new MisionListaSegmento(idSegmento, idMision, indAccion, date, usuario));
            }
            em.flush();
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        for (String idSegmento : listaIdSegmento) {
            bitacoraBean.logAccion(MisionListaSegmento.class, idMision + "/" + idSegmento, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);

        }
    }

    /**
     * Metodo que revoca la asignacion de una lista de segmentos a una mision
     *
     * @param idMision Identificador de la mision
     * @param listaIdSegmento Lista de identificadores de segmentos
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeBatchSegmento(String idMision, List<String> listaIdSegmento, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
        }

        listaIdSegmento = listaIdSegmento.stream().distinct().collect(Collectors.toList());

        for (String idSegmento : listaIdSegmento) {
            try {
                em.getReference(Segmento.class, idSegmento).getIdSegmento();
            } catch (EntityNotFoundException e) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
            }
        }

        //verificacion de entidades archivadas
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", "Mision");
        }

        try {
            listaIdSegmento.stream().forEach((idSegmento) -> {
                //verificacion que la entidad exista y su eliminacion
                em.remove(em.getReference(MisionListaSegmento.class, new MisionListaSegmentoPK(idSegmento, idMision)));
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_list_segment_not_found");
        }

        for (String idSegmento : listaIdSegmento) {
            bitacoraBean.logAccion(MisionListaSegmento.class, idMision + "/" + idSegmento, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        }
    }
}
