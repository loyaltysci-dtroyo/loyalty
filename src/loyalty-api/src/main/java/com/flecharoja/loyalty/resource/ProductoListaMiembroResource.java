/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.service.ProductoListaMiembroBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * clase con recursos http para el manejo de lista de miembro
 * incluidos/excluidos de producto
 *
 * @author wtencio
 */
@Api(value = "Producto")
@Path("producto/{idProducto}/miembro")
public class ProductoListaMiembroResource {

    @EJB
    ProductoListaMiembroBean productoListaMiembroBean;//EJB con los metodos de negocio para el manejo de la lita de miembros(incluir/excluir)

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos para el manejo de autorizaciones

    @Context
    SecurityContext context;//Permite obtener informacion del usuario en sesion

    @Context
    HttpServletRequest request;

    /**
     * Identificador del producto
     */
    @PathParam("idProducto")
    String idProducto;

    /**
     * Metodo que incluye o excluye segun el parametro accion, un listado de
     * identificadores de miembros a un premio por su identificador, de ocurrir
     * un error de retornara una Respuesta con un estado erroneo junto con un
     * encabezado "Error-Reason" con un valor numerico indicando la naturaleza
     * del error.
     *
     * @param listaIdMiembro Lista de identificadores de miembros
     * @param accion Parametro indicando el tipo de accion (incluir/excluir)
     * @return Resultado con el estado de la operacion
     */
    @ApiOperation(value = "Asignacion de lista de miembros a un producto")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idProducto", value = "Identificador del producto", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void assignBatchMiembro(
            @ApiParam(value = "Identificadores de miembros", required = true) List<String> listaIdMiembro,
            @ApiParam(value = "Indicador de tipo de lista a asignar", required = true)
            @QueryParam("accion") String accion) {
        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoListaMiembroBean.includeExcludeBatchMiembro(idProducto, listaIdMiembro, accion.charAt(0), usuarioContext, request.getLocale());
    }

    /**
     * Metodo que elimina la inclusion o exclusion de un listado de
     * identificadores de miembros a un producto por su identificador, de
     * ocurrir un error de retornara una Respuesta con un estado erroneo junto
     * con un encabezado "Error-Reason" con un valor numerico indicando la
     * naturaleza del error.
     *
     * @param listaIdMiembro Lista de identificadores de miembros
     * @return Resultado con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asignacion de lista de miembros a un producto")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idProducto", value = "Identificador del producto", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("remover-lista")
    public void unassignBatchMiembro(
            @ApiParam(value = "Identificadores de miembros", required = true) List<String> listaIdMiembro) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoListaMiembroBean.removeBatchMiembro(idProducto, listaIdMiembro, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que incluye o excluye segun el parametro accion, un miembro a un
     * producto por sus identificadores, de ocurrir un error de retornara una
     * Respuesta con un estado erroneo junto con un encabezado "Error-Reason"
     * con un valor numerico indicando la naturaleza del error.
     *
     * @param idMiembro Identificador del miembro
     * @param accion Parametro indicando el tipo de accion (incluir/excluir)
     * @return Resultado con el estado de la operacion
     */
    @ApiOperation(value = "Asignacion de miembro a un producto")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idProducto", value = "Identificador del producto", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("{idMiembro}")
    public void assignMiembro(
            @ApiParam(value = "Identificador de miembro", required = true)
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Indicador de tipo de lista a asignar", required = true)
            @QueryParam("accion") String accion) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoListaMiembroBean.includeExcludeMiembro(idProducto, idMiembro, accion.charAt(0), usuarioContext, request.getLocale());
    }

    /**
     * Metodo que elimina la inclusion o exclusion de un miembro a un producto
     * por sus identificadores, de ocurrir un error de retornara una Respuesta
     * con un estado erroneo junto con un encabezado "Error-Reason" con un valor
     * numerico indicando la naturaleza del error.
     *
     * @param idMiembro Identificador del miembro
     * @return Resultado con el estado de la operacion
     */
    @ApiOperation(value = "Eliminacion de la asignacion de miembro a un premio")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idProducto", value = "Identificador del producto", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada")
        ,
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("{idMiembro}")
    public void unassignMiembro(
            @ApiParam(value = "Identificador de miembro", required = true)
            @PathParam("idMiembro") String idMiembro) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_ESCRITURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        productoListaMiembroBean.removeMiembro(idProducto, idMiembro, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que obtiene un listado de miembros incluidos, excluidos de un
     * producto por su identificador o los disponibles para su
     * inclusion/exclusion segun el valor del parametro de accion y en un rango
     * de registros validos segun los parametros de registro y cantidad, de
     * ocurrir un error se retornara una respuesta con un estado indicando que
     * hubo un error junto con un encabezado "Error-Reason" con un valor
     * numerico indicando la naturaleza del error.
     *
     * @param accion Indicador del tipo de accion incluido/excluido/disponibles
     * @param estados
     * @param generos
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @param estadosCiviles
     * @param educaciones
     * @param contactoEmail
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param contactoEstado
     * @param tieneHijos
     * @param contactoSMS
     * @param filtros
     * @param contactoNotificacion
     * @param ordenCampo
     * @param busqueda
     * @param ordenTipo
     * @return Respuesta con el listado de miembros deseados en un rango valido
     * junto con encabezados sobre el rango
     */
    @ApiOperation(value = "Obtener lista de miembros de un producto",
            responseContainer = "List",
            response = Miembro.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class)
                ,
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idProducto", value = "Identificador del producto", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
        ,
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMiembros(
            @ApiParam(value = "Indicador del tipo de lista de miembros") @QueryParam("accion") String accion,
            @ApiParam(value = "Indicadores de estado de miembros") @QueryParam("estado") List<String> estados,
            @ApiParam(value = "Indicadores de genero de miembros") @QueryParam("genero") List<String> generos,
            @ApiParam(value = "Indicadores de estado civil de miembros") @QueryParam("estado-civil") List<String> estadosCiviles,
            @ApiParam(value = "Indicadores de educacion de miembros") @QueryParam("educacion") List<String> educaciones,
            @ApiParam(value = "Indicador booleano de contacto de email") @QueryParam("contacto-email") String contactoEmail,
            @ApiParam(value = "Indicador booleano de contacto de sms") @QueryParam("contacto-sms") String contactoSMS,
            @ApiParam(value = "Indicador booleano de contacto de notificacion") @QueryParam("contacto-notificacion") String contactoNotificacion,
            @ApiParam(value = "Indicador booleano de contacto de estado") @QueryParam("contacto-estado") String contactoEstado,
            @ApiParam(value = "Indicador booleano de tiene hijos") @QueryParam("tiene-hijos") String tieneHijos,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos de filtros sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro) {
        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.PRODUCTOS_LECTURA, token,request.getLocale())) {
           throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = productoListaMiembroBean.getMiembros(idProducto, accion, estados, generos, estadosCiviles, educaciones, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, cantidad, registro, ordenTipo, ordenCampo, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

}
