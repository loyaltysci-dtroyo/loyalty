/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.GrupoNiveles;
import com.flecharoja.loyalty.model.ListaReglas;
import com.flecharoja.loyalty.model.NivelMetrica;
import com.flecharoja.loyalty.model.Premio;
import com.flecharoja.loyalty.model.ReglaAsignacionPremio;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author faguilar
 */
@Stateless
public class ReglaAsignacionPremioBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * @param idPremio
     * @param locale
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List getReglasLista(String idPremio, Locale locale) {
        try {
            return em.createNamedQuery("ReglaAsignacionPremio.findByPremio").setParameter("idPremio", idPremio).getResultList();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idLista");
        }
    }

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * @param idRegla Identificador de la lista de reglas
     * @param locale
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public ReglaAsignacionPremio getRegla(String idRegla, Locale locale) {
        try {
            return (ReglaAsignacionPremio) em.createNamedQuery("ReglaAsignacionPremio.findByIdRegla").setParameter("idRegla", idRegla).getSingleResult();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idLista");
        }
    }

    /**
     * Metodo que crea una nueva regla en una lista especifica
     *
     * @param idPremio
     * @param reglaAsignacionPremio Objeto con la informacion de la regla
     * @param usuario usuario en sesion
     * @param locale
     * @return Respuesta con el identificador de la regla creada
     */
    public String createReglaAsignacionPremio(String idPremio, ReglaAsignacionPremio reglaAsignacionPremio, String usuario, Locale locale) {
        if (reglaAsignacionPremio == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "ReglaAsignacionPremio");
        }
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "badge_type_not_valid");
        } else {
            reglaAsignacionPremio.setPremio(premio);
        }
        if (reglaAsignacionPremio.getTier() != null) {
            NivelMetrica nivel = em.find(NivelMetrica.class, reglaAsignacionPremio.getTier().getIdNivel());
            if (nivel != null) {
                reglaAsignacionPremio.setTier(nivel);
            }
        }

        if (ReglaAsignacionPremio.TiposRegla.get(reglaAsignacionPremio.getIndTipoRegla().charAt(0)) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "intTipoRegla");
        }
        if (reglaAsignacionPremio.getFechaIndTipo() != null && ReglaAsignacionPremio.TiposFecha.get(reglaAsignacionPremio.getFechaIndTipo().charAt(0)) != null) {
            if ((ReglaAsignacionPremio.TiposFecha.ANTERIOR.toString().equals(reglaAsignacionPremio.getFechaIndTipo()))
                    || (ReglaAsignacionPremio.TiposFecha.DESDE.toString().equals(reglaAsignacionPremio.getFechaIndTipo()) && reglaAsignacionPremio.getFechaInicio() == null)
                    || (ReglaAsignacionPremio.TiposFecha.ENTRE.toString().equals(reglaAsignacionPremio.getFechaIndTipo()) && (reglaAsignacionPremio.getFechaFinal() == null || reglaAsignacionPremio.getFechaInicio() == null))
                    || (ReglaAsignacionPremio.TiposFecha.HASTA.toString().equals(reglaAsignacionPremio.getFechaIndTipo()) && reglaAsignacionPremio.getFechaFinal() == null)
                    || (ReglaAsignacionPremio.TiposFecha.ULTIMO.toString().equals(reglaAsignacionPremio.getFechaIndTipo()) && ReglaAsignacionPremio.TiposUltimaFecha.get(reglaAsignacionPremio.getFechaIndUltimo().charAt(0)) == null)) {

                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "advanced_rule_date_invalid");
            }
        } else {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Date");
        }
        
        if (ReglaAsignacionPremio.ValoresIndOperador.get(reglaAsignacionPremio.getIndOperador())==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
        }
        
        if (ReglaAsignacionPremio.ValoresIndOperadorRegla.get(reglaAsignacionPremio.getIndOperadorRegla())==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperadorRegla");
        }
        
        Date date = Calendar.getInstance().getTime();
        reglaAsignacionPremio.setUsuarioCreacion(usuario);
        reglaAsignacionPremio.setUsuarioModificacion(usuario);
        reglaAsignacionPremio.setFechaCreacion(date);
        reglaAsignacionPremio.setFechaModificacion(date);
        em.persist(reglaAsignacionPremio);
        em.flush();

        bitacoraBean.logAccion(ReglaAsignacionPremio.class, reglaAsignacionPremio.getIdRegla(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        return reglaAsignacionPremio.getIdRegla();
    }

    /**
     * Metodo que crea una nueva regla en una lista especifica
     *
     * @param idPremio
     * @param reglaAsignacionPremio Objeto con la informacion de la regla
     * @param usuario usuario en sesion
     * @param locale
     * @return Respuesta con el identificador de la regla creada
     */
    public String updateReglaAsignacionPremio(String idPremio, ReglaAsignacionPremio reglaAsignacionPremio, String usuario, Locale locale) {
        if (reglaAsignacionPremio == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "ReglaAsignacionPremio");
        }
        Premio premio = em.find(Premio.class, idPremio);
        if (premio == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "badge_type_not_valid");
        } else {
            reglaAsignacionPremio.setPremio(premio);
        }
        if (ReglaAsignacionPremio.TiposRegla.get(reglaAsignacionPremio.getIndTipoRegla().charAt(0)) == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "intTipoRegla");
        }
         if (reglaAsignacionPremio.getTier() != null) {
            NivelMetrica nivel = em.find(NivelMetrica.class, reglaAsignacionPremio.getTier().getIdNivel());
            if (nivel != null) {
                reglaAsignacionPremio.setTier(nivel);
            }
        }
        if (reglaAsignacionPremio.getFechaIndTipo() != null && ReglaAsignacionPremio.TiposFecha.get(reglaAsignacionPremio.getFechaIndTipo().charAt(0)) != null) {
            if ((ReglaAsignacionPremio.TiposFecha.ANTERIOR.toString().equals(reglaAsignacionPremio.getFechaIndTipo()))
                    || (ReglaAsignacionPremio.TiposFecha.DESDE.toString().equals(reglaAsignacionPremio.getFechaIndTipo()) && reglaAsignacionPremio.getFechaInicio() == null)
                    || (ReglaAsignacionPremio.TiposFecha.ENTRE.toString().equals(reglaAsignacionPremio.getFechaIndTipo()) && (reglaAsignacionPremio.getFechaFinal() == null || reglaAsignacionPremio.getFechaInicio() == null))
                    || (ReglaAsignacionPremio.TiposFecha.HASTA.toString().equals(reglaAsignacionPremio.getFechaIndTipo()) && reglaAsignacionPremio.getFechaFinal() == null)
                    || (ReglaAsignacionPremio.TiposFecha.ULTIMO.toString().equals(reglaAsignacionPremio.getFechaIndTipo()) && ReglaAsignacionPremio.TiposUltimaFecha.get(reglaAsignacionPremio.getFechaIndUltimo().charAt(0)) == null)) {

                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "advanced_rule_date_invalid");
            }
        } else {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", "Date");
        }
        
        if (ReglaAsignacionPremio.ValoresIndOperador.get(reglaAsignacionPremio.getIndOperador())==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperador");
        }
        
        if (ReglaAsignacionPremio.ValoresIndOperadorRegla.get(reglaAsignacionPremio.getIndOperadorRegla())==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indOperadorRegla");
        }
        
        //establecimiento de valores por defecto
        Date date = Calendar.getInstance().getTime();
        reglaAsignacionPremio.setFechaModificacion(date);
        reglaAsignacionPremio.setUsuarioModificacion(usuario);
        em.merge(reglaAsignacionPremio);
        em.flush();
        bitacoraBean.logAccion(ReglaAsignacionPremio.class, reglaAsignacionPremio.getIdRegla(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(), locale);
        return reglaAsignacionPremio.getIdRegla();
    }

    /**
     * Metodo que elimina de una lista especifica una regla
     *
     * @param idRegla Identificador de la regla
     * @param usuario usuario en sesión
     * @param locale
     */
    public void deleteReglaAsignacionPremio(String idRegla, String usuario, Locale locale) {
        Date date = Calendar.getInstance().getTime();
        try {
            //eliminacion de la regla (si es encontrada)
            em.remove(em.getReference(ReglaAsignacionPremio.class, idRegla));
            em.flush();
            bitacoraBean.logAccion(ReglaAsignacionPremio.class, idRegla, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(), locale);
        } catch (EntityNotFoundException e) {//lista de reglas o regla no encontrada
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "rules_list_not_found");
        }
    }
}
