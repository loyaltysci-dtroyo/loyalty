package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.Workflow;
import com.flecharoja.loyalty.model.WorkflowListaMiembro;
import com.flecharoja.loyalty.model.WorkflowListaMiembroPK;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * EJB encargado de el manejo de asignaciones de miembros con workflows
 *
 * @author svargas
 */
@Stateless
public class WorkflowListaMiembroBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * obtencion de lista de miembros disponibles/incluidos/excluidos para un workflow
     * 
     * @param idWorkflow id de workflow
     * @param indAccion indicador de tipo de lista
     * @param estados filtro de miembros
     * @param generos filtro de miembros
     * @param estadosCivil filtro de miembros
     * @param educacion filtro de miembros
     * @param contactoEmail filtro de miembros
     * @param contactoSMS filtro de miembros
     * @param contactoNotificacion filtro de miembros
     * @param contactoEstado filtro de miembros
     * @param tieneHijos filtro de miembros
     * @param busqueda filtro de miembros
     * @param filtros filtro de miembros
     * @param cantidad paginacion
     * @param registro paginacion
     * @param ordenTipo tipo de orden de lista
     * @param ordenCampo campo a ordenar lista
     * @param locale locale
     * @return lista de miembros
     */
    public Map<String, Object> getMiembros(String idWorkflow, String indAccion, List<String> estados, List<String> generos, List<String> estadosCivil, List<String> educacion, String contactoEmail, String contactoSMS, String contactoNotificacion, String contactoEstado, String tieneHijos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Miembro> root = query.from(Miembro.class);
        
        //formacion de query de busqueda de miembros
        query = Busquedas.getCriteriaQueryMiembros(cb, query, root, estados, generos, estadosCivil, educacion, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, ordenTipo, ordenCampo);
        
        //segun el tipo de lista se forma sub query para el filtro de miembros
        Subquery<String> subquery = query.subquery(String.class);
        Root<WorkflowListaMiembro> rootSubquery = subquery.from(WorkflowListaMiembro.class);
        subquery.select(rootSubquery.get("workflowListaMiembroPK").get("idMiembro"));
        if (indAccion==null) {
            subquery.where(cb.equal(rootSubquery.get("workflowListaMiembroPK").get("idWorkflow"), idWorkflow));
            if (query.getRestriction()!=null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            }
        } else {
            switch(indAccion.toUpperCase().charAt(0)) {
                case Indicadores.INCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("workflowListaMiembroPK").get("idWorkflow"), idWorkflow), cb.equal(rootSubquery.get("indTipo"), Indicadores.INCLUIDO));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.EXCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("workflowListaMiembroPK").get("idWorkflow"), idWorkflow), cb.equal(rootSubquery.get("indTipo"), Indicadores.EXCLUIDO));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.DISPONIBLES: {
                    subquery.where(cb.equal(rootSubquery.get("workflowListaMiembroPK").get("idWorkflow"), idWorkflow));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.equal(root.get("indEstadoMiembro"), Miembro.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                    } else {
                        query.where(cb.equal(root.get("indEstadoMiembro"), Miembro.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                    }
                    break;
                }
                default: {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "indAccion");
                }
            }
        }
        
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        //recurrido del resultado para la asignacion de informacion adicional (almacenada en keycloak)
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            resultado.stream().forEach((t) -> {
                Miembro miembro = (Miembro) t;
                try {
                    miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
                } catch(Exception e2) {
                    miembro.setNombreUsuario("Prueba");
                }
            });
        }
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * Metodo que asigna (incluye o excluye) a un miembro de un workflow
     *
     * @param idWorkflow Identificador del workflow
     * @param idMiembro Identificador del miembro
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesion
     * @param locale
     */
    public void includeExcludeMiembro(String idWorkflow, String idMiembro, Character indAccion, String usuario,Locale locale) {
        //verificacion que los id's sean validos
        Workflow workflow = em.find(Workflow.class, idWorkflow);
        if (workflow == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "referenced_entity_not_found", "workflow");
        }
        try {
            em.getReference(Miembro.class, idMiembro).getIdMiembro();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        //verificacion de entidad archivada
        if (workflow.getIndEstado().equals(Workflow.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", "Workflow");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            WorkflowListaMiembro workflowListaMiembro = new WorkflowListaMiembro(idMiembro, idWorkflow, indAccion, date, usuario);

            em.merge(workflowListaMiembro);
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        bitacoraBean.logAccion(WorkflowListaMiembro.class, idWorkflow + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * Metodo que revoca la asignacion de un miembro a un workflow
     *
     * @param idWorkflow Identificador del workflow
     * @param idMiembro Identificador del miembro
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeMiembro(String idWorkflow, String idMiembro, String usuario,Locale locale) {
        Workflow workflow = em.find(Workflow.class, idWorkflow);
        if (workflow == null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "referenced_entity_not_found", "workflow");
        }
        try {
            em.getReference(Miembro.class, idMiembro).getIdMiembro();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        //verificacion de entidad archivada
        if (workflow.getIndEstado().equals(Workflow.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", "Workflow");
        }

        try {
            //verificacion que la entidad exista y su eliminacion
            em.remove(em.getReference(WorkflowListaMiembro.class, new WorkflowListaMiembroPK(idMiembro, idWorkflow)));
            em.flush();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "workflow-member");
        }

        bitacoraBean.logAccion(WorkflowListaMiembro.class, idWorkflow + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }

    /**
     * Metodo que asigna (incluye o excluye) una lista de miembros de un workflow
     *
     * @param idWorkflow Identificador del workflow
     * @param listaIdMiembro Lista de identificadores de miembros
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesión
     * @param locale
     */
    public void includeExcludeBatchMiembro(String idWorkflow, List<String> listaIdMiembro, Character indAccion, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Workflow workflow = em.find(Workflow.class, idWorkflow);
        if (workflow == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "workflow");
        }

        listaIdMiembro = listaIdMiembro.stream().distinct().collect(Collectors.toList());

        try {
            listaIdMiembro.stream().forEach((idMiembro) -> {
                em.getReference(Miembro.class, idMiembro).getIdMiembro();
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        //verificacion de entidad archivada
        if (workflow.getIndEstado().equals(Workflow.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", this.getClass().getSimpleName());
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            for (String idMiembro : listaIdMiembro) {
                em.merge(new WorkflowListaMiembro(idMiembro, idWorkflow, indAccion, date, usuario));
            }
            em.flush();
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        for (String idMiembro : listaIdMiembro) {
            bitacoraBean.logAccion(WorkflowListaMiembro.class, idWorkflow + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        }
    }

    /**
     * Metodo que revoca la asignacion de una lista de miembros a un workflow
     *
     * @param idWorkflow Identificador de workflow
     * @param listaIdMiembro Lista de identificadores de miembros
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeBatchMiembro(String idWorkflow, List<String> listaIdMiembro, String usuario,Locale locale) {
        //verificacion que los id's sean validos
        Workflow workflow = em.find(Workflow.class, idWorkflow);
        if (workflow == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "workflow");
        }

        listaIdMiembro = listaIdMiembro.stream().distinct().collect(Collectors.toList());

        try {
            listaIdMiembro.stream().forEach((idMiembro) -> {
                em.getReference(Miembro.class, idMiembro).getIdMiembro();
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        //verificacion de entidad archivada
        if (workflow.getIndEstado().equals(Workflow.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", this.getClass().getSimpleName());
        }

        try {
            listaIdMiembro.stream().forEach((idMiembro) -> {
                //verificacion que la entidad exista y su eliminacion
                em.remove(em.getReference(WorkflowListaMiembro.class, new WorkflowListaMiembroPK(idMiembro, idWorkflow)));
            });
            em.flush();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "referenced_entity_not_found", "workflow-member");
        }

        for (String idMiembro : listaIdMiembro) {
            bitacoraBean.logAccion(WorkflowListaMiembro.class, idWorkflow + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        }
    }
}
