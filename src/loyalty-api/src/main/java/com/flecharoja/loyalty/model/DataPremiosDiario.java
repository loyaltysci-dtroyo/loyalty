package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "DATA_PREMIOS_DIARIO")
@NamedQueries({
    @NamedQuery(name = "DataPremiosDiario.getElegiblesByIdPremioGroupByMes", query = "SELECT d.dataPremiosDiarioPK.mes, COUNT(d), SUM(d.cantElegibles) FROM DataPremiosDiario d WHERE d.dataPremiosDiarioPK.idPremio = :idPremio GROUP BY d.dataPremiosDiarioPK.mes"),
    @NamedQuery(name = "DataPremiosDiario.deleteAllByMes", query = "DELETE FROM DataPremiosDiario d WHERE d.dataPremiosDiarioPK.mes = :mes")
})
public class DataPremiosDiario implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DataPremiosDiarioPK dataPremiosDiarioPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANT_ELEGIBLES")
    private Long cantElegibles;

    public DataPremiosDiario() {
    }

    public DataPremiosDiario(Integer dia, Integer mes, String idMision) {
        this.dataPremiosDiarioPK = new DataPremiosDiarioPK(dia, mes, idMision);
    }

    public DataPremiosDiarioPK getDataPremiosDiarioPK() {
        return dataPremiosDiarioPK;
    }

    public void setDataPremiosDiarioPK(DataPremiosDiarioPK dataPremiosDiarioPK) {
        this.dataPremiosDiarioPK = dataPremiosDiarioPK;
    }

    public Long getCantElegibles() {
        return cantElegibles;
    }

    public void setCantElegibles(Long cantElegibles) {
        this.cantElegibles = cantElegibles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dataPremiosDiarioPK != null ? dataPremiosDiarioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DataPremiosDiario)) {
            return false;
        }
        DataPremiosDiario other = (DataPremiosDiario) object;
        if ((this.dataPremiosDiarioPK == null && other.dataPremiosDiarioPK != null) || (this.dataPremiosDiarioPK != null && !this.dataPremiosDiarioPK.equals(other.dataPremiosDiarioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.DataPremiosDiario[ dataPremiosDiarioPK=" + dataPremiosDiarioPK + " ]";
    }
    
}
