package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.model.NotificacionListaMiembro;
import com.flecharoja.loyalty.model.NotificacionListaMiembroPK;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;


/**
 * EJB encargado del manejo de datos y aplicacion de logica de negocio al manejo
 * de listas de miembros de una notificacion.
 *
 * @author svargas
 */
@Stateless
public class NotificacionListaMiembroBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora
 
    /**
     * obtencion de lista de miembros segun accion para una notificacion
     * 
     * @param idNotificacion id de notificacion
     * @param tipo tipo indicando la accion
     * @param estados Listado de valores de indicadores de estados deseados
     * @param generos Listado de indicadores de genero de miembros deseados
     * @param estadosCivil Listado de indicadores de estados civiles deseados
     * @param educacion Listado de indicadores de educacion
     * @param contactoEmail
     * @param contactoSMS
     * @param busqueda Terminos de busqueda
     * @param filtros Listado de campos sobre que aplicar los terminos de
     * busqueda
     * @param tieneHijos
     * @param contactoEstado
     * @param cantidad Cantidad de registros deseados
     * @param contactoNotificacion
     * @param registro Numero de registro inicial en el resultado
     * @param ordenTipo
     * @param ordenCampo
     * @param locale
     * @return 
     */
    public Map<String, Object> getMiembros(String idNotificacion, String tipo, List<String> estados, List<String> generos, List<String> estadosCivil, List<String> educacion, String contactoEmail, String contactoSMS, String contactoNotificacion, String contactoEstado, String tieneHijos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
           throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Miembro> root = query.from(Miembro.class);
        
        //preparacion de query de busqueda de miembros
        query = Busquedas.getCriteriaQueryMiembros(cb, query, root, estados, generos, estadosCivil, educacion, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, ordenTipo, ordenCampo);
        
        //segun el tipo de accion se prepara subquery para el filtrado de miembros
        Subquery<String> subquery = query.subquery(String.class);
        Root<NotificacionListaMiembro> rootSubquery = subquery.from(NotificacionListaMiembro.class);
        subquery.select(rootSubquery.get("notificacionListaMiembroPK").get("idMiembro"));
        if (tipo==null || tipo.toUpperCase().charAt(0)==Indicadores.ASIGNADO) {
            subquery.where(cb.equal(rootSubquery.get("notificacionListaMiembroPK").get("idNotificacion"), idNotificacion));
            if (query.getRestriction()!=null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            }
        } else {
            if (tipo.toUpperCase().charAt(0)==Indicadores.DISPONIBLES) {
                subquery.where(cb.equal(rootSubquery.get("notificacionListaMiembroPK").get("idNotificacion"), idNotificacion));
                if (query.getRestriction()!=null) {
                    query.where(query.getRestriction(), cb.equal(root.get("indEstadoMiembro"), Miembro.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                } else {
                    query.where(cb.equal(root.get("indEstadoMiembro"), Miembro.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                }
            } else {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "tipo");
            }
        }
        
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        //recurrido del resultado para la asignacion de informacion adicional (almacenada en keycloak)
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            resultado.stream().forEach((t) -> {
                Miembro miembro = (Miembro) t;
                try {
                    miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
                } catch(Exception e2) {
                    miembro.setNombreUsuario("Prueba");
                }
            });
        }
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * Metodo que asigna un miembro a una notificacion
     *
     * @param idNotificacion Identificador de la notificacion
     * @param idMiembro Identificador del miembro
     * @param usuarioCreacion Identificador del usuario creador
     * @param locale
     */
    public void assignMiembroNotificacion(String idNotificacion, String idMiembro, String usuarioCreacion,Locale locale) {
        Notificacion notificacion = em.find(Notificacion.class, idNotificacion);//se busca la notificacion almacenada
        if (notificacion == null) {//si no se pudo encontrar la notificacion...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }
        try {
            em.getReference(Miembro.class, idMiembro).getIdMiembro();//se busca la referencia de un miembro y se detona su busqueda
        } catch (EntityNotFoundException e) {//no se pudo encontrar el miembro
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idMiembro");
        }

        //si la notificacion tiene un estado de archivado o ejecutado...
        if (notificacion.getIndEstado().equals(Notificacion.Estados.ARCHIVADO.getValue()) || notificacion.getIndEstado().equals(Notificacion.Estados.EJECUTADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Notificacion");
        }

        try {
            //se registra la asignacion y la accion en la bitacora
            em.persist(new NotificacionListaMiembro(idNotificacion, idMiembro, Calendar.getInstance().getTime(), usuarioCreacion));
            em.flush();
            bitacoraBean.logAccion(NotificacionListaMiembro.class, idNotificacion + "/" + idMiembro, usuarioCreacion, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (EntityExistsException e) {//si la asignacion existe...
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "notification_member_already_exists");
        }
    }

    /**
     * Metodo que asigna un listado de identificadores de miembros a una
     * notificacion
     *
     * @param idNotificacion Identificador de la notificacion
     * @param miembros Listado de identificadores de miembros
     * @param usuarioCreacion Identificador del usuario creador
     * @param locale
     */
    public void assignBatchMiembrosNotificacion(String idNotificacion, List<String> miembros, String usuarioCreacion, Locale locale) {
        Notificacion campana = em.find(Notificacion.class, idNotificacion);//se busca la notificacion
        if (campana == null) {//si no se encontro la notificacion...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }

        //si el estado de la notificacion es archivado o ejecutado
        if (campana.getIndEstado().equals(Notificacion.Estados.ARCHIVADO.getValue()) || campana.getIndEstado().equals(Notificacion.Estados.EJECUTADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived","Notificacion");
        }

        Date fecha = Calendar.getInstance().getTime();//obtencion de la fecha actual

        for (String idMiembro : miembros) {
            try {
                em.getReference(Miembro.class, idMiembro).getIdMiembro();//se obtiene la referencia del miembro y se detona su busqueda
            } catch (EntityNotFoundException e) {//si no se encontro...
                em.clear();
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idMiembro");
            }

            try {
                em.persist(new NotificacionListaMiembro(idNotificacion, idMiembro, fecha, usuarioCreacion));
            } catch (EntityExistsException e) {//entidad ya existente
                throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "notification_member_already_exists");
            }
        }
        
        em.flush();
        
        //se registra las asignaciones y acciones en bitacora
        miembros.forEach((idMiembro) -> {
            bitacoraBean.logAccion(NotificacionListaMiembro.class, idNotificacion + "/" + idMiembro, usuarioCreacion, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        });
    }

    /**
     * Metodo que elimina la asignacion de un miembro con una notificacion
     *
     * @param idNotificacion Identificador de la notificacion
     * @param idMiembro Identificador del miembro
     * @param usuarioModificacion Identificador del usuario modificador
     * @param locale
     */
    public void unassignMiembroNotificacion(String idNotificacion, String idMiembro, String usuarioModificacion, Locale locale) {
        Notificacion notificacion = em.find(Notificacion.class, idNotificacion);//se busca la notificacion
        if (notificacion == null) {//si no se encontro la notificacion...
           throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }
        try {
            em.getReference(Miembro.class, idMiembro).getIdMiembro();//se busca la referencia de un miembro y se detona su busqueda
        } catch (EntityNotFoundException e) {//no se pudo encontrar el miembro
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idMiembro");
        }

        //si la notifiacion tiene un estado de archivado o ejecutado...
        if (notificacion.getIndEstado().equals(Notificacion.Estados.ARCHIVADO.getValue()) || notificacion.getIndEstado().equals(Notificacion.Estados.EJECUTADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived","Notificacion");
        }

        //se elimina la asignacion y se registra la accion
        try {
            em.remove(em.getReference(NotificacionListaMiembro.class, new NotificacionListaMiembroPK(idNotificacion, idMiembro)));
            em.flush();
            bitacoraBean.logAccion(NotificacionListaMiembro.class, idNotificacion + "/" + idMiembro, usuarioModificacion, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_member_not_found");
        }
    }

    /**
     * Metodo que elimina la asignacion de un listado de identificadores de
     * miembros a una notificacion
     *
     * @param idNotificacion Identificador de la notificacion
     * @param miembros Identificadores de miembros
     * @param usuarioModificacion Identificador del usuario modificador
     * @param locale
     */
    public void unassignBatchMiembrosNotificacion(String idNotificacion, List<String> miembros, String usuarioModificacion, Locale locale) {
        Notificacion notificacion = em.find(Notificacion.class, idNotificacion);//se busca la notificacion
        if (notificacion == null) {//si no se encontro la notificacion...
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }
        for (String idMiembro : miembros) {
            try {
                em.getReference(Miembro.class, idMiembro).getIdMiembro();//se busca la referencia de un miembro y se detona su busqueda
            } catch (EntityNotFoundException e) {//no se pudo encontrar el miembro
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "idMiembro");
            }
        }

        //si el estado de la notificacion es archivado o ejecutado...
        if (notificacion.getIndEstado().equals(Notificacion.Estados.ARCHIVADO.getValue()) || notificacion.getIndEstado().equals(Notificacion.Estados.EJECUTADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived","Notificacion");
        }

        //se elimina las asignaciones y se registra las acciones en bitacora
        try {
            miembros.forEach((idMiembro) -> {
                em.remove(em.getReference(NotificacionListaMiembro.class, new NotificacionListaMiembroPK(idNotificacion, idMiembro)));
            });
            em.flush();
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
             throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_member_not_found");
        }
        
        for (String idMiembro : miembros) {
            bitacoraBean.logAccion(NotificacionListaMiembro.class, idNotificacion + "/" + idMiembro, usuarioModificacion, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        }
    }
}
