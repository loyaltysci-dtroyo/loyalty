/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PRODUCTO_LISTA_UBICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoListaUbicacion.findAll", query = "SELECT p FROM ProductoListaUbicacion p"),
    @NamedQuery(name = "ProductoListaUbicacion.findByIdUbicacion", query = "SELECT p FROM ProductoListaUbicacion p WHERE p.productoListaUbicacionPK.idUbicacion = :idUbicacion"),
    @NamedQuery(name = "ProductoListaUbicacion.findByIdProducto", query = "SELECT p FROM ProductoListaUbicacion p WHERE p.productoListaUbicacionPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoListaUbicacion.countByIdProducto", query = "SELECT COUNT(p) FROM ProductoListaUbicacion p WHERE p.productoListaUbicacionPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoListaUbicacion.findUbicacionesByIdProducto", query = "SELECT p.ubicacion FROM ProductoListaUbicacion p WHERE p.productoListaUbicacionPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoListaUbicacion.countUbicacionesNotInProductoListaUbicacion", query = "SELECT COUNT(u) FROM Ubicacion u WHERE u.indEstado = :estado AND u.idUbicacion NOT IN (SELECT p.productoListaUbicacionPK.idUbicacion FROM ProductoListaUbicacion p WHERE p.productoListaUbicacionPK.idProducto = :idProducto)"),
    @NamedQuery(name = "ProductoListaUbicacion.findUbicacionesNotInProductoListaUbicacion", query = "SELECT m FROM Ubicacion m WHERE m.indEstado = :estado AND m.idUbicacion NOT IN (SELECT p.productoListaUbicacionPK.idUbicacion FROM ProductoListaUbicacion p WHERE p.productoListaUbicacionPK.idProducto = :idProducto)"),
    @NamedQuery(name = "ProductoListaUbicacion.countByIdProductoIndTipo", query = "SELECT COUNT(p) FROM ProductoListaUbicacion p WHERE p.productoListaUbicacionPK.idProducto = :idProducto AND p.indTipo = :indTipo"),
    @NamedQuery(name = "ProductoListaUbicacion.findUbicacionesByIdProductoIndTipo", query = "SELECT p.ubicacion FROM ProductoListaUbicacion p WHERE p.productoListaUbicacionPK.idProducto = :idProducto AND p.indTipo = :indTipo")
    
})



public class ProductoListaUbicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoListaUbicacionPK productoListaUbicacionPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_PRODUCTO", referencedColumnName = "ID_PRODUCTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    
    @JoinColumn(name = "ID_UBICACION", referencedColumnName = "ID_UBICACION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ubicacion ubicacion;

    public ProductoListaUbicacion() {
    }

    public ProductoListaUbicacion(ProductoListaUbicacionPK productoListaUbicacionPK) {
        this.productoListaUbicacionPK = productoListaUbicacionPK;
    }

    public ProductoListaUbicacion(String idUbicacion, String idProducto, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.productoListaUbicacionPK = new ProductoListaUbicacionPK(idUbicacion, idProducto);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public ProductoListaUbicacion(String idUbicacion, String idProducto) {
        this.productoListaUbicacionPK = new ProductoListaUbicacionPK(idUbicacion, idProducto);
    }

    @ApiModelProperty(hidden = true)
    public ProductoListaUbicacionPK getProductoListaUbicacionPK() {
        return productoListaUbicacionPK;
    }

    public void setProductoListaUbicacionPK(ProductoListaUbicacionPK productoListaUbicacionPK) {
        this.productoListaUbicacionPK = productoListaUbicacionPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoListaUbicacionPK != null ? productoListaUbicacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoListaUbicacion)) {
            return false;
        }
        ProductoListaUbicacion other = (ProductoListaUbicacion) object;
        if ((this.productoListaUbicacionPK == null && other.productoListaUbicacionPK != null) || (this.productoListaUbicacionPK != null && !this.productoListaUbicacionPK.equals(other.productoListaUbicacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ProductoListaUbicacion[ productoListaUbicacionPK=" + productoListaUbicacionPK + " ]";
    }
    
}
