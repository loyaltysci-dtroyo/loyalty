package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.model.PromocionListaSegmento;
import com.flecharoja.loyalty.model.PromocionListaSegmentoPK;
import com.flecharoja.loyalty.model.Segmento;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * EJB encargado de el manejo de asignaciones de segmentos con promociones
 *
 * @author svargas
 */
@Stateless
public class PromocionListaSegmentoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * obtencion de segmentos disponibles/incluidos/excluidos para una promocion
     * 
     * @param idPromocion id de promocion
     * @param indAccion indicador de tipo de lista
     * @param estados filtro de segmento
     * @param tipos filtro de segmento
     * @param busqueda filtro de segmento
     * @param filtros filtro de segmento
     * @param cantidad paginacion
     * @param registro paginacion
     * @param ordenTipo tipo de orden de lista
     * @param ordenCampo campo para orden de lista
     * @param locale locale
     * @return lista de segmentos
     */
    public Map<String, Object> getSegmentos(String idPromocion, String indAccion, List<String> estados, List<String> tipos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Segmento> root = query.from(Segmento.class);
        
        //query de busqueda de segmentos
        query = Busquedas.getCriteriaQuerySegmentos(cb, query, root, estados, tipos, busqueda, filtros, ordenTipo, ordenCampo);
        
        //subquery para filtrado de segmentos segun el indicador de accion
        Subquery<String> subquery = query.subquery(String.class);
        Root<PromocionListaSegmento> rootSubquery = subquery.from(PromocionListaSegmento.class);
        subquery.select(rootSubquery.get("promocionListaSegmentoPK").get("idSegmento"));
        if (indAccion==null) {
            subquery.where(cb.equal(rootSubquery.get("promocionListaSegmentoPK").get("idPromocion"), idPromocion));
            if (query.getRestriction()!=null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idSegmento")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idSegmento")).value(subquery)));
            }
        } else {
            switch(indAccion.toUpperCase().charAt(0)) {
                case Indicadores.INCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("promocionListaSegmentoPK").get("idPromocion"), idPromocion), cb.equal(rootSubquery.get("indTipo"), Indicadores.INCLUIDO));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idSegmento")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idSegmento")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.EXCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("promocionListaSegmentoPK").get("idPromocion"), idPromocion), cb.equal(rootSubquery.get("indTipo"), Indicadores.EXCLUIDO));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idSegmento")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idSegmento")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.DISPONIBLES: {
                    subquery.where(cb.equal(rootSubquery.get("promocionListaSegmentoPK").get("idPromocion"), idPromocion));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.equal(root.get("indEstado"), Segmento.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idSegmento")).value(subquery).not()));
                    } else {
                        query.where(cb.equal(root.get("indEstado"), Segmento.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idSegmento")).value(subquery).not()));
                    }
                    break;
                }
                default: {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
                }
            }
        }
        
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * Metodo que asigna (incluye o excluye) a un segmento de una promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param idSegmento Identificador del segmento
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesión
     * @param locale
     */
    public void includeExcludeSegmento(String idPromocion, String idSegmento, Character indAccion, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Promocion promocion = em.find(Promocion.class, idPromocion);
        
        if (promocion == null) {
           throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        Segmento segmento = em.find(Segmento.class, idSegmento);
        if (segmento == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        //verificacion de entidades archivadas/borrador
        if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())
                || !segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "operation_over_archived", "Promo, Segmento");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            PromocionListaSegmento promocionListaSegmento = new PromocionListaSegmento(idSegmento, idPromocion, indAccion, date, usuario);

            em.merge(promocionListaSegmento);
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        bitacoraBean.logAccion(PromocionListaSegmento.class, idPromocion + "/" + idSegmento, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * Metodo que revoca la asignacion de un segmento a una promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param idSegmento Identificador del segmento
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeSegmento(String idPromocion, String idSegmento, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        try {
            em.getReference(Segmento.class, idSegmento).getIdSegmento();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
        }

        //verificacion de entidades archivadas
        if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Promo");
        }

        try {
            //verificacion que la entidad exista y su eliminacion
            em.remove(em.getReference(PromocionListaSegmento.class, new PromocionListaSegmentoPK(idSegmento, idPromocion)));
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_segment_not_found");
        }

        bitacoraBean.logAccion(PromocionListaSegmento.class, idPromocion + "/" + idSegmento, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }

    /**
     * Metodo que asigna (incluye o excluye) una lista de segmentos de una
     * promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param listaIdSegmento Lista de identificadores de segmentos
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesión
     * @param locale
     */
    public void includeExcludeBatchSegmento(String idPromocion, List<String> listaIdSegmento, Character indAccion, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }

        listaIdSegmento = listaIdSegmento.stream().distinct().collect(Collectors.toList());

        //verificacion de entidades archivadas
        if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Promo");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            for (String idSegmento : listaIdSegmento) {
                //verificacion que los id's sean validos
                Segmento segmento = em.find(Segmento.class, idSegmento);
                if (segmento == null) {
                    em.clear();
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
                }

                //verificacion de entidades archivadas
                if (!segmento.getIndEstado().equals(Segmento.Estados.ACTIVO.getValue())) {
                    throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "attributes_invalid", "indEstado");
                }

                em.merge(new PromocionListaSegmento(idSegmento, idPromocion, indAccion, date, usuario));
            }
            em.flush();
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        for (String idSegmento : listaIdSegmento) {
            bitacoraBean.logAccion(PromocionListaSegmento.class, idPromocion + "/" + idSegmento, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);

        }
    }

    /**
     * Metodo que revoca la asignacion de una lista de segmentos a una promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param listaIdSegmento Lista de identificadores de segmentos
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeBatchSegmento(String idPromocion, List<String> listaIdSegmento, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }

        listaIdSegmento = listaIdSegmento.stream().distinct().collect(Collectors.toList());

        for (String idSegmento : listaIdSegmento) {
            try {
                em.getReference(Segmento.class, idSegmento).getIdSegmento();
            } catch (EntityNotFoundException e) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "segment_not_found");
            }
        }

        //verificacion de entidades archivadas
        if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Promo");
        }

        try {
            listaIdSegmento.stream().forEach((idSegmento) -> {
                //verificacion que la entidad exista y su eliminacion
                em.remove(em.getReference(PromocionListaSegmento.class, new PromocionListaSegmentoPK(idSegmento, idPromocion)));
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_segment_not_found");
        }

        for (String idSegmento : listaIdSegmento) {
            bitacoraBean.logAccion(PromocionListaSegmento.class, idPromocion + "/" + idSegmento, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        }
    }
}
