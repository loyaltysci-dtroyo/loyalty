package com.flecharoja.loyalty.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaMisionEdicion {
    
    public enum EstadosRespuesta {
        GANADO("G"),
        FALLIDO("F"),
        PENDIENTE("P");
        
        private final String value;
        private static final Map<String, EstadosRespuesta> lookup = new HashMap<>();

        private EstadosRespuesta(String value) {
            this.value = value;
        }
        
        static {
            for (EstadosRespuesta estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public String getValue() {
            return value;
        }
        
        public static EstadosRespuesta get (String value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    private String idMiembro;
    
    private String indEstado;
    
    private Date fecha;

    public RespuestaMisionEdicion() {
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(String indEstado) {
        this.indEstado = indEstado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
