package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionListaUbicacion;
import com.flecharoja.loyalty.model.MisionListaUbicacionPK;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

/**
 * EJB encargado de el manejo de asignaciones de ubicaciones con misiones
 *
 * @author svargas
 */
@Stateless
public class MisionListaUbicacionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene un listado de ubicaciones asignados a una mision en
     * un rango de respuesta y opcionalmente por el tipo de accion
     * (incluido/excluido)
     *
     * @param idMision Identificador de la mision
     * @param indAccion Indicador de tipo de accion deseado
     * @param registro Numero de registro inicial
     * @param cantidad Cantidad de registros deseados
     * @param locale
     * @return Respuesta con el listado de miembros y su rango de respuesta
     */
    public Map<String, Object> getUbicaciones(String idMision, Character indAccion, int registro, int cantidad, Locale locale) {
        //verificacion de que el rango este dentro del valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        Map<String, Object> respuesta = new HashMap<>();

        List resultado;
        Long total;

        try {
            if (indAccion == null) {//en caso de que no se haya establecido el tipo de accion deseado, se recuperan todos
                total = ((Long) em.createNamedQuery("MisionListaUbicacion.countUbicacionesByIdMision").setParameter("idMision", idMision).getSingleResult());
                total -= total - 1 < 0 ? 0 : 1;

                while (registro > total) {
                    registro = registro - cantidad > 0 ? registro - cantidad : 0;
                }

                resultado = em.createNamedQuery("MisionListaUbicacion.findUbicacionesByIdMision")
                        .setParameter("idMision", idMision)
                        .setMaxResults(cantidad)
                        .setFirstResult(registro)
                        .getResultList();
            } else {
                indAccion = Character.toUpperCase(indAccion);

                if (indAccion.compareTo(Indicadores.DISPONIBLES) == 0) {
                    total = (Long) em.createNamedQuery("MisionListaUbicacion.countUbicacionesByIndEstadoNotInListaByIdMision")
                            .setParameter("indEstado", Ubicacion.Estados.PUBLICADO_ACTIVO.getValue())
                            .setParameter("idMision", idMision)
                            .getSingleResult();
                    total -= total - 1 < 0 ? 0 : 1;

                    while (registro > total) {
                        registro = registro - cantidad > 0 ? registro - cantidad : 0;
                    }

                    //obtencion de la lista en un rango valido
                    resultado = em.createNamedQuery("MisionListaUbicacion.findUbicacionesByIndEstadoNotInListaByIdMision")
                            .setParameter("indEstado", Ubicacion.Estados.PUBLICADO_ACTIVO.getValue())
                            .setParameter("idMision", idMision)
                            .setMaxResults(cantidad)
                            .setFirstResult(registro)
                            .getResultList();
                } else {
                    total = ((Long) em.createNamedQuery("MisionListaUbicacion.countUbicacionesByIdMisionIndTipo")
                            .setParameter("idMision", idMision)
                            .setParameter("indTipo", indAccion)
                            .getSingleResult());
                    total -= total - 1 < 0 ? 0 : 1;

                    while (registro > total) {
                        registro = registro - cantidad > 0 ? registro - cantidad : 0;
                    }

                    resultado = em.createNamedQuery("MisionListaUbicacion.findUbicacionesByIdMisionIndTipo")
                            .setParameter("idMision", idMision)
                            .setParameter("indTipo", indAccion)
                            .setMaxResults(cantidad)
                            .setFirstResult(registro)
                            .getResultList();
                }
            }
        } catch (IllegalArgumentException e) {//error paginacion
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * Metodo que asigna (incluye o excluye) a una ubicacion de una mision
     *
     * @param idMision Identificador de la mision
     * @param idUbicacion Identificador de la ubicacion
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesión
     * @param locale
     */
    public void includeExcludeUbicaciones(String idMision, String idUbicacion, Character indAccion, String usuario,Locale locale) {
        //verificacion que los id's sean validos
        Ubicacion ubicacion = em.find(Ubicacion.class, idUbicacion);
        if (ubicacion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
        }
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
        }

        //verificacion de entidades archivadas/borrador
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())
                || !ubicacion.getIndEstado().equals(Ubicacion.Estados.PUBLICADO_ACTIVO.getValue())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "operation_over_archived", "Mision, Ubicacion");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            MisionListaUbicacion misionListaUbicacion = new MisionListaUbicacion(idUbicacion, idMision, indAccion, date, usuario);

            em.merge(misionListaUbicacion);
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        bitacoraBean.logAccion(MisionListaUbicacion.class, idMision + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * Metodo que revoca la asignacion de una ubicacion a una mision
     *
     * @param idMision Identificador de la mision
     * @param idUbicacion Identificador de la ubicacion
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeUbicacion(String idMision, String idUbicacion, String usuario,Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
        }
        try {
            em.getReference(Ubicacion.class, idUbicacion).getIdUbicacion();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
        }

        //verificacion de entidades archivadas/borrador
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", "Mision");
        }

        try {
            //verificacion que la entidad exista y su eliminacion
            em.remove(em.getReference(MisionListaUbicacion.class, new MisionListaUbicacionPK(idUbicacion, idMision)));
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_list_location_not_found");
        }

        bitacoraBean.logAccion(MisionListaUbicacion.class, idMision + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }

    /**
     * Metodo que asigna (incluye o excluye) una lista de ubicaciones de una
     * mision
     *
     * @param idMision Identificador de la mision
     * @param listaIdUbicacion Identificadores de ubicaciones
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario sesión
     * @param locale
     */
    public void includeExcludeBatchUbicacion(String idMision, List<String> listaIdUbicacion, Character indAccion, String usuario,Locale locale) {
        //verificacion que los id's sean validos
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
        }

        listaIdUbicacion = listaIdUbicacion.stream().distinct().collect(Collectors.toList());

        //verificacion de entidades archivadas
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", "Mision");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            for (String idUbicacion : listaIdUbicacion) {
                //verificacion que los id's sean validos
                Ubicacion ubicacion = em.find(Ubicacion.class, idUbicacion);
                if (ubicacion == null) {
                    em.clear();
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
                }

                //verificacion de entidades archivadas
                if (!ubicacion.getIndEstado().equals(Ubicacion.Estados.PUBLICADO_ACTIVO.getValue())) {
                    em.clear();
                    throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "location_inactive");
                }

                em.merge(new MisionListaUbicacion(idUbicacion, idMision, indAccion, date, usuario));
            }
            em.flush();
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        for (String idUbicacion : listaIdUbicacion) {
            bitacoraBean.logAccion(MisionListaUbicacion.class, idMision + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        }
    }

    /**
     * Metodo que revoca la asignacion de una lista de ubicaciones a una
     * mision
     *
     * @param idMision Identificador de la mision
     * @param listaIdUbicacion Identificadores de ubicaciones
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeBatchUbicacion(String idMision, List<String> listaIdUbicacion, String usuario,Locale locale) {
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_not_found");
        }

        listaIdUbicacion = listaIdUbicacion.stream().distinct().collect(Collectors.toList());

        for (String idUbicacion : listaIdUbicacion) {
            try {
                em.getReference(Ubicacion.class, idUbicacion).getIdUbicacion();
            } catch (EntityNotFoundException e) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
            }
        }

        //verificacion de entidades archivadas/borrador
        if (mision.getIndEstado().equals(Mision.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "operation_over_archived", "Mision");
        }

        try {
            listaIdUbicacion.stream().forEach((idUbicacion) -> {
                //verificacion que la entidad exista y su eliminacion
                em.remove(em.getReference(MisionListaUbicacion.class, new MisionListaUbicacionPK(idUbicacion, idMision)));
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_list_location_not_found");
        }

        for (String idUbicacion : listaIdUbicacion) {
            bitacoraBean.logAccion(MisionListaUbicacion.class, idMision + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        }
    }
}
