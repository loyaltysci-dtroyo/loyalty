/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "TABPOS_MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TabposMiembro.findAll", query = "SELECT t FROM TabposMiembro t ORDER BY t.acumulado DESC"),
    @NamedQuery(name = "TabposMiembro.findByIdMiembroAcumulado" , query = "SELECT t.acumulado FROM TabposMiembro t WHERE t.tabposMiembroPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "TabposMiembro.findByIdTabla", query = "SELECT t FROM TabposMiembro t WHERE t.tabposMiembroPK.idTabla = :idTabla ORDER BY t.acumulado DESC"),
    @NamedQuery(name = "TabposMiembro.findByIdMiembro", query = "SELECT t.tablaPosiciones FROM TabposMiembro t WHERE t.tabposMiembroPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "TabposMiembro.findByIdMiembroTabla", query = "SELECT t FROM TabposMiembro t WHERE t.tabposMiembroPK.idMiembro = :idMiembro AND t.tabposMiembroPK.idTabla = :idTabla"),
    @NamedQuery(name = "TabposMiembro.countByIdMiembroTabla", query = "SELECT COUNT(t) FROM TabposMiembro t WHERE t.tabposMiembroPK.idMiembro = :idMiembro AND t.tabposMiembroPK.idTabla = :idTabla"),
    @NamedQuery(name = "TabposMiembro.countByIdMiembro", query = "SELECT COUNT(t) FROM TabposMiembro t WHERE t.tabposMiembroPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "TabposMiembro.findByAcumulado", query = "SELECT t FROM TabposMiembro t WHERE t.acumulado = :acumulado")})
public class TabposMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected TabposMiembroPK tabposMiembroPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACUMULADO")
    private Double acumulado;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;
    
    @JoinColumn(name = "ID_TABLA", referencedColumnName = "ID_TABLA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TablaPosiciones tablaPosiciones;

    public TabposMiembro() {
    }

    public TabposMiembro(String idTabla, String idMiembro) {
        this.tabposMiembroPK = new TabposMiembroPK(idTabla, idMiembro);
        this.acumulado = Double.valueOf(0);
    }

    @ApiModelProperty(hidden = true)
    public TabposMiembroPK getTabposMiembroPK() {
        return tabposMiembroPK;
    }

    public void setTabposMiembroPK(TabposMiembroPK tabposMiembroPK) {
        this.tabposMiembroPK = tabposMiembroPK;
    }

    public Double getAcumulado() {
        return acumulado;
    }

    public void setAcumulado(Double acumulado) {
        this.acumulado = acumulado;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public TablaPosiciones getTablaPosiciones() {
        return tablaPosiciones;
    }

    public void setTablaPosiciones(TablaPosiciones tablaPosiciones) {
        this.tablaPosiciones = tablaPosiciones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tabposMiembroPK != null ? tabposMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TabposMiembro)) {
            return false;
        }
        TabposMiembro other = (TabposMiembro) object;
        if ((this.tabposMiembroPK == null && other.tabposMiembroPK != null) || (this.tabposMiembroPK != null && !this.tabposMiembroPK.equals(other.tabposMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.TabposMiembro[ tabposMiembroPK=" + tabposMiembroPK + " ]";
    }

}
