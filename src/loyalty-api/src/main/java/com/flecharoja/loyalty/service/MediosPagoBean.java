/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.MediosPago;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyAwsS3Utils;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 * Clase encargada de proveer los métodos necesarios para el manejo de medios de pago
 * @author wtencio
 */
@Stateless
public class MediosPagoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos necesarios para el manejo de bitacora

    @EJB
    UtilBean utilBean;//EJB con los metodos para el manejo de utilidades

    /**
     * Método que registra un nuevo medio de pago
     *
     * @param pago informacion del medio de pago
     * @param usuario usuario en sesion
     * @param locale
     */
    public void insertMedioPago(MediosPago pago, String usuario,Locale locale) {
        
        ConfiguracionGeneral configuracion = em.find(ConfiguracionGeneral.class, new Long(0));
        if (configuracion == null){
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE,locale, "general_configuration_not_found");
        }
        pago.setFechaCreacion(Calendar.getInstance().getTime());
        pago.setUsuarioCreacion(usuario);
        pago.setIdConfiguracion(configuracion);
        
        //si la imagen viene nula, se le establece un url de imagen predefinida
        if (pago.getImagen() == null || pago.getImagen().trim().isEmpty()) {
            pago.setImagen(Indicadores.URL_IMAGEN_PREDETERMINADA);
        } else {
            //si no, se le intenta subir a cloudfiles
            try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
                pago.setImagen(filesUtils.uploadImage(pago.getImagen(), MyAwsS3Utils.Folder.ARTE_MEDIO_PAGO, null));
            } catch (Exception e) {
                //en el caso de que ocurra un error (al procesar el base64 o subir la imagen)
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, e);
                throw new MyException(MyException.ErrorPeticion.IMAGEN_NO_VALIDA, locale, "invalid_image");
            }
        }
        try {
            em.persist(pago);
            bitacoraBean.logAccion(MediosPago.class, pago.getIdTipo(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
    }

    /**
     * Método que elimina un medio de pago
     *
     * @param idMedioPago identificador del medio de pago
     * @param usuario usuario en sesion
     * @param locale
     */
    public void eliminarMedioPago(String idMedioPago, String usuario,Locale locale) {
        MediosPago medio = em.find(MediosPago.class, idMedioPago);
        if(medio == null){
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE,locale, "means_payment_not_found");
        }
        try (MyAwsS3Utils filesUtils = new MyAwsS3Utils()) {
            filesUtils.deleteFile(medio.getImagen());
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
        }
        em.remove(medio);
    }

    /**
     * Método para obtener los medios de pago restringidos por una cantidad y registros dados
     * @param cantidad maxima de registros por pagina
     * @param registro por el cual se comienza la busqueda
     * @param locale
     * @return lista de medios de pago
     */
    public Map<String, Object> getMediosPago(int cantidad, int registro,Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado;
        Long total;
        try {
  
          
            //se obtiene el total de registros
            total = (Long) em.createNamedQuery("MediosPago.countAll").getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            //se obtiene los registros en un rango valido
            resultado = em.createNamedQuery("MediosPago.findAll")
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();

        } catch (IllegalArgumentException e) {//en el caso de argumentos validos (paginacion)
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

}
