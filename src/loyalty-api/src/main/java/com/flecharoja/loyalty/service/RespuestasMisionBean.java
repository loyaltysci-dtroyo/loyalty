package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Juego;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.MisionEncuestaPregunta;
import com.flecharoja.loyalty.model.MisionPerfilAtributo;
import com.flecharoja.loyalty.model.MisionRedSocial;
import com.flecharoja.loyalty.model.MisionSubirContenido;
import com.flecharoja.loyalty.model.MisionVerContenido;
import com.flecharoja.loyalty.model.Preferencia;
import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.model.RespuestaMision;
import com.flecharoja.loyalty.model.RespuestaMisionEdicion;
import com.flecharoja.loyalty.model.RespuestaMisionEncuestaPregunta;
import com.flecharoja.loyalty.model.RespuestaMisionJuego;
import com.flecharoja.loyalty.model.RespuestaMisionPerfilAtributo;
import com.flecharoja.loyalty.model.RespuestaMisionPreferencia;
import com.flecharoja.loyalty.model.RespuestaMisionRedSocial;
import com.flecharoja.loyalty.model.RespuestaMisionSubirContenido;
import com.flecharoja.loyalty.model.RespuestaMisionVerContenido;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.PrefixFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * EJB encargado del manejo de aprobacion / rechazo de respuestas de misiones
 *
 * @author svargas
 */
@Stateless
public class RespuestasMisionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    private final Configuration configuration;
    
    @EJB
    RegistroMetricaService registroMetricaService;
    
    @EJB
    MyKafkaUtils myKafkaUtils;
    
    public RespuestasMisionBean() {
        this.configuration = new Configuration();
        this.configuration.addResource("conf/hbase/hbase-site.xml");
    }
    
    /**
     * obtencion de respuestas de mision por estado de la misma para una mision
     * 
     * @param idMision identificador de mision
     * @param indEstado indicador de estado de respuesta
     * @param registro paginacion
     * @param cantidad paginacion
     * @param locale locale
     * @param tipoOrden tipo de orden de paginacion
     * @return 
     */
    public Map<String, Object> getRespuestasPorEstado(String idMision, String indEstado, int registro, int cantidad, Locale locale, String tipoOrden) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        if (registro < 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }
        
        if (RespuestaMision.EstadosRespuesta.get(indEstado)==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indEstado");
        }
        
        Mision mision = em.find(Mision.class, idMision);
        if (mision==null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "challenge_not_found");
        }
        Map<String, Object> respuesta = new HashMap<>();

        long total;
        
        List<RespuestaMision> respuestas = new ArrayList<>();
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "REGISTRO_MISION"));
            
            //definicion de escaner
            Scan scan = new Scan();
            FilterList filterList = new FilterList(
                    new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes(indEstado)),
                    new PrefixFilter(Bytes.toBytes(idMision))
            );
            scan.setFilter(filterList);
            scan.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"));
            scan.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("RESPUESTA"));
            
            //segun el tipo de orden se realiza el escaneo al revez o descendente
            if (tipoOrden==null || tipoOrden.equalsIgnoreCase("DESC")) {
                scan.setReversed(true);
            }

            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    RespuestaMision respuestaMision = new RespuestaMision();
                    respuestaMision.setIdMiembro(Bytes.toString(result.getRow()).split("&")[1]);
                    respuestaMision.setFecha(Long.parseLong(Bytes.toString(result.getRow()).split("&")[2]));
                    
                    //segun el tipo de mision se lee la respuesta
                    try {
                        JsonObject readObject = Json.createReader(new StringReader(Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("RESPUESTA"))))).readObject();
                        switch(Mision.Tipos.get(mision.getIndTipoMision())) {
                            case ENCUESTA: {
                                JsonArray jsonArray = readObject.getJsonArray("respuestaEncuesta");
                                if (jsonArray!=null) {
                                    respuestaMision.setRespuestaEncuesta(jsonArray.stream().map((jsonValue) -> {
                                        JsonObject jsonObject = (JsonObject) jsonValue;
                                        RespuestaMisionEncuestaPregunta encuestaPregunta = new RespuestaMisionEncuestaPregunta();

                                        encuestaPregunta.setComentarios(jsonObject.containsKey("comentarios")?jsonObject.getString("comentarios"):null);
                                        encuestaPregunta.setRespuesta(jsonObject.getString("respuesta"));
                                        encuestaPregunta.setIdPregunta(jsonObject.getString("idPregunta"));

                                        return encuestaPregunta;
                                    }).collect(Collectors.toList()));
                                } else {
                                    respuestaMision.setRespuestaEncuesta(new ArrayList<>());
                                }
                                break;
                            }
                            case PERFIL: {
                                JsonArray jsonArray = readObject.getJsonArray("respuestaActualizarPerfil");
                                if (jsonArray!=null) {
                                    respuestaMision.setRespuestaActualizarPerfil(jsonArray.stream().map((jsonValue) -> {
                                        JsonObject jsonObject = (JsonObject) jsonValue;
                                        RespuestaMisionPerfilAtributo perfilAtributo = new RespuestaMisionPerfilAtributo();

                                        perfilAtributo.setIdAtributo(jsonObject.getString("idAtributo"));
                                        perfilAtributo.setRespuesta(jsonObject.getString("respuesta"));

                                        return perfilAtributo;
                                    }).collect(Collectors.toList()));
                                } else {
                                    respuestaMision.setRespuestaActualizarPerfil(new ArrayList<>());
                                }
                                break;
                            }
                            case RED_SOCIAL: {
                                respuestaMision.setRespuestaRedSocial(new RespuestaMisionRedSocial());
                                break;
                            }
                            case SUBIR_CONTENIDO: {
                                RespuestaMisionSubirContenido respuestaMisionSubirContenido = new RespuestaMisionSubirContenido();
                                respuestaMisionSubirContenido.setDatosContenido(readObject.getJsonObject("respuestaSubirContenido").getString("datosContenido"));
                                respuestaMision.setRespuestaSubirContenido(respuestaMisionSubirContenido);
                                break;
                            }
                            case VER_CONTENIDO: {
                                respuestaMision.setRespuestaVerContenido(new RespuestaMisionVerContenido());
                                break;
                            }
                            case JUEGO_PREMIO: {
                                respuestaMision.setRespuestaJuego(new RespuestaMisionJuego());
                                break;
                            }
                            case PREFERENCIAS: {
                                JsonArray jsonArray = readObject.getJsonArray("respuestaPreferencias");
                                if (jsonArray!=null) {
                                    respuestaMision.setRespuestaPreferencias(jsonArray.stream().map((jsonValue) -> {
                                        JsonObject jsonObject = (JsonObject) jsonValue;
                                        RespuestaMisionPreferencia preferencia = new RespuestaMisionPreferencia();

                                        preferencia.setRespuesta(jsonObject.getString("respuesta"));
                                        preferencia.setIdPreferencia(jsonObject.getString("idPreferencia"));

                                        return preferencia;
                                    }).collect(Collectors.toList()));
                                } else {
                                    respuestaMision.setRespuestaPreferencias(new ArrayList<>());
                                }
                                break;
                            }
                        }
                    } catch(JsonException | ClassCastException | NullPointerException | IllegalStateException e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Respuesta de mision no se pudo leer", e);
                    }
                    
                    respuestas.add(respuestaMision);
                }
            }
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
        
        if (respuestas.isEmpty()) {
            total = 0;
            registro = 0;
        } else {
            total = respuestas.size();
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
        }
        
        //seleccion de respuesta segun la pagina deseada
        List<RespuestaMision> respuestasSeleccionadas = respuestas.subList(registro, registro + cantidad > respuestas.size() ? respuestas.size() : registro + cantidad);
        
        //agregado de detalles en cada respuesta
        respuestasSeleccionadas.forEach((respuestaMision) -> {
            respuestaMision.setMiembro(em.find(Miembro.class, respuestaMision.getIdMiembro()));
            try {
                switch(Mision.Tipos.get(mision.getIndTipoMision())) {
                    case ENCUESTA: {
                        respuestaMision.getRespuestaEncuesta().forEach((encuestaPregunta) -> encuestaPregunta.setDetallePregunta(em.find(MisionEncuestaPregunta.class, encuestaPregunta.getIdPregunta())));
                        break;
                    }
                    case PERFIL: {
                        respuestaMision.getRespuestaActualizarPerfil().forEach((perfilAtributo) -> perfilAtributo.setDetalleAtributo(em.find(MisionPerfilAtributo.class, perfilAtributo.getIdAtributo())));
                        break;
                    }
                    case RED_SOCIAL: {
                        respuestaMision.getRespuestaRedSocial().setDetalleRedSocial(em.find(MisionRedSocial.class, mision.getIdMision()));
                        break;
                    }
                    case SUBIR_CONTENIDO: {
                        respuestaMision.getRespuestaSubirContenido().setDetalleSubirContenido(em.find(MisionSubirContenido.class, mision.getIdMision()));
                        break;
                    }
                    case VER_CONTENIDO: {
                        respuestaMision.getRespuestaVerContenido().setDetalleVerContenido(em.find(MisionVerContenido.class, mision.getIdMision()));
                        break;
                    }
                    case JUEGO_PREMIO: {
                        respuestaMision.getRespuestaJuego().setDetalleMisionJuego(em.find(Juego.class, idMision));
                        break;
                    }
                    case PREFERENCIAS: {
                        respuestaMision.getRespuestaPreferencias().forEach((preferencia) -> preferencia.setPreferencia(em.find(Preferencia.class, preferencia.getIdPreferencia())));
                        break;
                    }
                }
            } catch (NullPointerException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Respuesta de mision no encontrada");
            }
        });
        
        respuesta.put("resultado", respuestasSeleccionadas);
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, indEstado);

        return respuesta;
    }
    
    /**
     * cambio de estado de una respuesta de mision
     * 
     * @param idMision identificador de mision
     * @param respuestaMision data
     * @param locale locale
     */
    public void editRespuesta(String idMision, RespuestaMisionEdicion respuestaMision, Locale locale) {
        if (respuestaMision==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "information_empty", RespuestaMision.class.getSimpleName());
        }
        
        if (RespuestaMisionEdicion.EstadosRespuesta.get(respuestaMision.getIndEstado())==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "indEstado");
        }
        
        Mision mision = em.find(Mision.class, idMision);
        if (mision==null) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "challenge_not_found");
        }
        
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "REGISTRO_MISION"));
            
            Result result = table.get(new Get(Bytes.toBytes(idMision+"&"+respuestaMision.getIdMiembro()+"&"+String.format("%013d", respuestaMision.getFecha().getTime()))));
            
            if (result.isEmpty()) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idMiembro, fecha");
            }
            
            if (Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"))).equals(RespuestaMisionEdicion.EstadosRespuesta.PENDIENTE.getValue())) {
                table.put(new Put(Bytes.toBytes(idMision+"&"+respuestaMision.getIdMiembro()+"&"+String.format("%013d", respuestaMision.getFecha().getTime()))).addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"), Bytes.toBytes(respuestaMision.getIndEstado())));
            } else {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "challenge_response_edited");
            }
            
        } catch (IOException ex) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
        
        if (RespuestaMisionEdicion.EstadosRespuesta.get(respuestaMision.getIndEstado())==RespuestaMisionEdicion.EstadosRespuesta.GANADO) {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.APROBO_MISION, idMision, respuestaMision.getIdMiembro());
            registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), mision.getIdMetrica().getIdMetrica(), respuestaMision.getIdMiembro(), RegistroMetrica.TiposGane.MISION, mision.getCantMetrica(), null, idMision));
        }
    }

}
