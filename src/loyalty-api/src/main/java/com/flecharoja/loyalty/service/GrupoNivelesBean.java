/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.GrupoNiveles;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.MiembroMetricaNivel;
import com.flecharoja.loyalty.model.NivelMetrica;
import com.flecharoja.loyalty.model.ReglaMiembroAtb;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.QueryTimeoutException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia
 * relacionados a grupo-niveles
 *
 * @author wtencio
 */
@Stateless
public class GrupoNivelesBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Método que inserta un nuevo grupo de niveles en la base de datos
     *
     * @param nombre que identificara el nuevo grupo
     * @param usuario usuario que esta en sesión
     * @param locale
     * @return id del nuevo grupo registrado
     */
    public String insertGrupoNivel(String nombre, String usuario, Locale locale) {
        GrupoNiveles grupoNiveles = new GrupoNiveles();
        grupoNiveles.setNombre(nombre);
        grupoNiveles.setFechaCreacion(Calendar.getInstance().getTime());
        grupoNiveles.setUsuarioCreacion(usuario);
        grupoNiveles.setFechaModificacion(Calendar.getInstance().getTime());
        grupoNiveles.setUsuarioModificacion(usuario);
        grupoNiveles.setNumVersion(new Long(1));
        try {
            em.persist(grupoNiveles);
            bitacoraBean.logAccion(GrupoNiveles.class, grupoNiveles.getIdGrupoNivel(), usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        return grupoNiveles.getIdGrupoNivel();
    }

    /**
     * Método que obtiene la información de un grupo especifico asociado al id
     * que ingresa como parametro
     *
     * @param idGrupoNivel identificador del grupo que se desea buscar
     * @param locale
     * @return información del grupo-nivel
     */
    public GrupoNiveles getGrupoNivelId(String idGrupoNivel, Locale locale) {

        GrupoNiveles grupoNiveles = em.find(GrupoNiveles.class, idGrupoNivel);
        if (grupoNiveles == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "group_levels_not_found");
        }
        return grupoNiveles;
    }

    /**
     * Método que devuelve una lista con todos los grupos de niveles
     *
     * @param cantidad cantiadd de registros maximos por pagina
     * @param registro registro por el cual se comienza la busqueda
     * @param locale
     * @return lista de grupos-niveles
     */
    public Map<String, Object> getGruposNivel(int cantidad, int registro, Locale locale) {
        Map<String, Object> respuesta = new HashMap<>();
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        List resultado = new ArrayList();
        Long total;

        try {

            total = ((Long) em.createNamedQuery("GrupoNiveles.countAll").getSingleResult());
            total -= total - 1 < 0 ? 0 : 1;

            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            resultado = em.createNamedQuery("GrupoNiveles.findAll")
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

    /**
     * Método que actualiza el nombre del grupo de niveles
     *
     * @param grupoNivel objeto con la información actualizada
     * @param usuario usuario en sesión
     * @param locale
     */
    public void updateGrupoNivel(GrupoNiveles grupoNivel, String usuario, Locale locale) {
        GrupoNiveles temp;
        temp = em.find(GrupoNiveles.class, grupoNivel.getIdGrupoNivel());

        if (temp == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "group_levels_not_found");
        }

        grupoNivel.setFechaModificacion(Calendar.getInstance().getTime());
        grupoNivel.setUsuarioModificacion(usuario);
        try {
            em.merge(grupoNivel);
            em.flush();
            bitacoraBean.logAccion(GrupoNiveles.class, grupoNivel.getIdGrupoNivel(), usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        } catch (ConstraintViolationException | OptimisticLockException e) {
            if (e instanceof ConstraintViolationException) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", ((ConstraintViolationException) e).getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
            } else {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "outdated_data");
            }
        }

    }

    /**
     * Método que verifica si el nombre del grupo nivel ya existe o no
     *
     * @param nombreNivel valor a verificar
     * @return resultado de la operación
     */
    public Boolean existNombreGrupoNivel(String nombreNivel) {
        return ((Long) em.createNamedQuery("GrupoNiveles.countByNombre").setParameter("nombre", nombreNivel).getSingleResult()) > 0;
    }

    /**
     * Método que elimina un grupo de niveles si es de ser posible
     *
     * @param idGrupoNivel identificador del grupo de niveles
     * @param usuario usuario en sesion
     * @param locale
     */
    public void deleteGrupoNivel(String idGrupoNivel, String usuario,Locale locale) {

        GrupoNiveles grupoNivel = em.find(GrupoNiveles.class, idGrupoNivel);
        List<NivelMetrica> nivelesGrupo = grupoNivel.getNivelMetricaList();
        List<Metrica> metricas = em.createNamedQuery("Metrica.ByIdGrupo").setParameter("idGrupoNivel", idGrupoNivel).getResultList();

        //si ambas listas estan vacias lo elimina de una vez
        if (nivelesGrupo.isEmpty() && metricas.isEmpty()) {
            em.remove(grupoNivel);
            bitacoraBean.logAccion(NivelMetrica.class, grupoNivel.getIdGrupoNivel(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
            //si los niveles estan vacios y las metricas no, no lo puede eliminar
        } else if (nivelesGrupo.isEmpty() && !metricas.isEmpty()) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "group_of_levels_used");
            //si metricas esta vacio y niveles no, se tiene que verificar que los niveles no esten asociados a nada
        } else if (!nivelesGrupo.isEmpty() && metricas.isEmpty()) {

            for (NivelMetrica nivelGrupo : nivelesGrupo) {
                List<ReglaMiembroAtb> reglaMiembro = em.createNamedQuery("ReglaMiembroAtb.findByIdNivelMetrica").setParameter("idNivel", nivelGrupo.getIdNivel()).getResultList();
                //si alguna regla utiliza el nivel no se puede eliminar
                if (!reglaMiembro.isEmpty()) {
                     throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "group_of_levels_used");
                }

                List<MiembroMetricaNivel> miembroMetrica = em.createNamedQuery("MiembroMetricaNivel.findByIdNivel").setParameter("idNivel", nivelGrupo.getIdNivel()).getResultList();
                //si si algun miembro utiliza el nivel no se puede eliminar
                if (!miembroMetrica.isEmpty()) {
                     throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "group_of_levels_used");
                }

            }
            //si no retorno ningun error, quiere decir que se puede eliminar el grupo
            em.remove(grupoNivel);
            bitacoraBean.logAccion(NivelMetrica.class, grupoNivel.getIdGrupoNivel(), usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
            //si ambas listas no estan vacias, no se puede eliminar
        } else if (!nivelesGrupo.isEmpty() && !metricas.isEmpty()) {
             throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA,locale, "group_of_levels_used");
        }

    }

    /**
     * Metodo que realiza una busqueda en base a palabras claves sobre una serie
     * de atributos de la tabla grupo niveles en la base datos con un rango de
     * paginación
     *
     * @param busqueda Cadena de texto con las palabras claves
     * @param cantidad de registros que se quieren por pagina
     * @param registro del cual se inicia la busqueda
     * @param locale
     * @return Listado de todos los atributos encontrados
     */
    public Map<String, Object> searchGruposNiveles(String busqueda, int cantidad, int registro, Locale locale) {
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = new ArrayList();
        Long total;

        String[] palabrasClaves = busqueda.split(" ");
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<GrupoNiveles> root = query.from(GrupoNiveles.class);

            List<Predicate> predicates = new ArrayList<>();

            for (String palabraClave : palabrasClaves) {
                predicates.add(cb.like(cb.lower(root.get("nombre")), "%" + palabraClave.toLowerCase() + "%"));

            }

            query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));

            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }

            resultado = em.createQuery(query.select(root))
                    .setMaxResults(cantidad)
                    .setFirstResult(registro)
                    .getResultList();

        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        } catch (QueryTimeoutException e) {
            throw new MyException(MyException.ErrorSistema.TIEMPO_CONEXION_DB_AGOTADO, locale, "database_connection_failed");
        }
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        return respuesta;
    }

}
