/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.RespuestaPreferencia;
import com.flecharoja.loyalty.service.RespuestaPreferenciaBean;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.IndicadoresRecursos;
import com.flecharoja.loyalty.util.MyKeycloakAuthz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Clase que provee de metodos http RESTful para el acceso a funcionalidades del
 * manejo de respuesta-preferencia
 *
 * @author wtencio
 */
@Api(value = "Preferencia")
@Path("preferencia")
public class RespuestaPreferenciaResource {

    @Context
    SecurityContext context;//permite obtener información del usuario en sesión

    @EJB
    RespuestaPreferenciaBean respuestaPreferenciaBean;//EJB con los metodos de negocio para el manejo de respuestas a preferencias

    @EJB
    MyKeycloakAuthz authzBean;//EJB con los metodos de negocio para el manejo de autorizacion
    
    @Context
    HttpServletRequest request;


    /**
     * Metodo que acciona una tarea, la cual obtiene un listado de todas las
     * respuestas de las preferencias que tiene un miembro en particular en el
     * caso de error al ejecutar la tarea se retorna un 500 (error interno del
     * servidor) con un mensaje en el encabezado, de no ser asi se retorna un
     * 200 (OK) con un resultado de ser requerido este.
     *
     * @param idMiembro identificador unico del miembro al cual se le quiere
     * averiguar las preferencias y respuestas
     * @param tipoRespuesta
     * @param busqueda
     * @param filtros
     * @param cantidad Parametro opcional con un valor por defecto de 10 que
     * define la cantidad maxima de registros por respuesta
     * @param ordenCampo
     * @param ordenTipo
     * @param registro Parametro opcional que define el numero de primer
     * registro desde donde devolver el listado de entidades
     * @return Lista de respuestas en formato JSON
     */
    @ApiOperation(value = "Obtener las respuestas a preferencias de un miembro",
            responseContainer = "List",
            response = RespuestaPreferencia.class,
            responseHeaders = {
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_CONTENIDO, response = String.class),
                @ResponseHeader(name = Indicadores.PAGINACION_RANGO_ACEPTADO, response = Integer.class)
            })

    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @GET
    @Path("/miembro/{idMiembro}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRespuestasPreferenciasMiembro(
            @ApiParam(value = "Identificador del miembro") @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Indicadores de tipo de datos") @QueryParam("tipo-respuesta") List<String> tipoRespuesta,
            @ApiParam(value = "Terminos de busqueda") @QueryParam("busqueda") String busqueda,
            @ApiParam(value = "Campos sobre que aplicar la busqueda") @QueryParam("filtro") List<String> filtros,
            @ApiParam(value = "Indicador del tipo de orden de resultados (desc/asc)", defaultValue = Indicadores.TIPO_ORDEN_PREDETERMINADO) @QueryParam("tipo-orden") @DefaultValue(Indicadores.TIPO_ORDEN_PREDETERMINADO) String ordenTipo,
            @ApiParam(value = "Indicador del campo por el cual ordenar los campos", defaultValue = Indicadores.CAMPO_ORDEN_PREDETERMINADO) @QueryParam("campo-orden") @DefaultValue(Indicadores.CAMPO_ORDEN_PREDETERMINADO) String ordenCampo,
            @ApiParam(value = "Numero de registro inicial") @QueryParam("registro") int registro,
            @ApiParam(value = "Cantidad de registros", defaultValue = Indicadores.PAGINACION_RANGO_DEFECTO) @DefaultValue(Indicadores.PAGINACION_RANGO_DEFECTO) @QueryParam("cantidad") int cantidad) {


        String token;
        try{
            token = request.getHeader("Authorization").substring(6);
        }catch(NullPointerException e){
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_LECTURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        Map<String, Object> respuesta = respuestaPreferenciaBean.getRespuestasPreferenciasMiembro(idMiembro, tipoRespuesta, busqueda, filtros, ordenTipo, ordenCampo, cantidad, registro, request.getLocale());
        return Response.ok()
                .entity(respuesta.get("resultado"))
                .header(Indicadores.PAGINACION_RANGO_CONTENIDO, respuesta.get("rango"))
                .header(Indicadores.PAGINACION_RANGO_ACEPTADO, Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR)
                .build();
    }

    /**
     * Método que acciona una tarea, la cual insertar una nueva respuesta de un
     * miembro especifico con una preferencia especifica en el caso de error al
     * ejecutar la tarea se retorna un 500 (error interno del servidor) con un
     * mensaje en el encabezado, de no ser asi se retorna un 200 (OK) con un
     * resultado de ser requerido este.
     *
     * @param idMiembro identificador unico de miembro
     * @param idPreferencia
     * @param respuesta respuesta del miembro a la pregunta de la preferencia
     * @return resultado de la operacion
     */
    @ApiOperation(value = "Asignación de una respuesta a una preferencia de un miembro",
            response = String.class)

    @ApiResponses(value = {
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @POST
    @Path("/{idPreferencia}/miembro/{idMiembro}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void insertRespuestaPreferencia(
            @ApiParam(value = "Identificador del miembro", required = true)
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Identificador de la preferencia", required = true)
            @PathParam("idPreferencia") String idPreferencia,
            @ApiParam(value = "Respuesta a la preferencia") String respuesta) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        respuestaPreferenciaBean.insertRespuestaPreferencia(idPreferencia, idMiembro, respuesta, usuarioContext, request.getLocale());

    }

    /**
     * Metodo que acciona una tarea, la cual elimina una respuesta de un miembro
     * especifico con una preferencia especifica en el caso de error al ejecutar
     * la tarea se retorna un 500 (error interno del servidor) con un mensaje en
     * el encabezado, de no ser asi se retorna un 200 (OK) con un resultado de
     * ser requerido este.
     *
     * @param idMiembro identificador unico del miembro
     * @param idPreferencia
     * @return Lista de preferencias en formato JSON
     */
    @ApiOperation(value = "Eliminación de una respuesta de una preferencia de un miembro")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idPreferencia", value = "Identificador de la preferencia", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no Encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })
    @DELETE
    @Path("/{idPreferencia}/miembro/{idMiembro}")
    public void removeRespuestaPreferencia(
            @ApiParam(value = "Identificador del miembro")
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Identificador de la preferencia", required = true)
            @PathParam("idPreferencia") String idPreferencia) {

        String usuarioContext;
        String token;
        try {
            usuarioContext = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        respuestaPreferenciaBean.removeRespuestaPreferencia(idPreferencia, idMiembro, usuarioContext, request.getLocale());
    }

    /**
     * Metodo que acciona una tarea, la cual actualiza la respuesta de una
     * preferencia de un miembro especifico (lo que realiza es un borrado del
     * registro y una nueva insersión) en el caso de error al ejecutar la tarea
     * se retorna un 500 (error interno del servidor) con un mensaje en el
     * encabezado, de no ser asi se retorna un 200 (OK) con un resultado de ser
     * requerido este.
     *
     * @param idMiembro identificador unico del miembro
     * @param idPreferencia
     * @param respuesta nueva respuesta que se desea registrar
     * @return Lista de preferencias en formato JSON
     */
    @ApiOperation(value = "Modificacion de una respuesta a una preferencia")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "idPreferencia", value = "Identificador de la preferencia", required = true, dataType = "string", paramType = "query")
    })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Operacion completada"),
        @ApiResponse(code = 400, message = "Error del cliente", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 401, message = "Error de autorizacion", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 404, message = "Recurso no encontrado", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        }),
        @ApiResponse(code = 500, message = "Error del sistema", responseHeaders = {
            @ResponseHeader(name = MyException.ENCABEZADO_CODIGO_ERROR, response = Integer.class)
        })
    })

    @PUT
    @Path("/{idPreferencia}/miembro/{idMiembro}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateRespuestaPreferenciaMiembro(
            @ApiParam(value = "Identificador del miembro")
            @PathParam("idMiembro") String idMiembro,
            @ApiParam(value = "Identificador de la preferencia", required = true)
            @PathParam("idPreferencia") String idPreferencia,
            @ApiParam(value = "Respuesta a la preferencia actualizada") String respuesta) {
        String usuario;
        String token;
        try {
            usuario = context.getUserPrincipal().getName();
            token = request.getHeader("Authorization").substring(6);
        } catch (NullPointerException e) {
            throw new MyException(MyException.ErrorAutenticacion.ERROR_AUTENTICACION, request.getLocale(), "header_authorization_invalid");
        }
        if (!authzBean.tienePermiso(IndicadoresRecursos.MIEMBROS_ESCRITURA, token,request.getLocale())) {
            throw new MyException(MyException.ErrorAutorizacion.SIN_AUTORIZACION, request.getLocale(), "without_authorization");
        }
        respuestaPreferenciaBean.updateRespuestaPreferencia(idPreferencia, idMiembro, respuesta,usuario,request.getLocale());

    }

}
