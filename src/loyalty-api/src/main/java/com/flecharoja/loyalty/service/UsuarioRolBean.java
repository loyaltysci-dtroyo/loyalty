/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Usuario;
import com.flecharoja.loyalty.model.Rol;
import com.flecharoja.loyalty.util.MyKeycloakUtilsAdmin;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * EJB con los metodo necesarios para el manejo de la asignacion de roles un
 * usuario
 *
 * @author wtencio
 */
@Stateless
public class UsuarioRolBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Método que asigna un rol a un usuario especifico
     *
     * @param idUsuario identificador del usuario
     * @param nombreRol nombre del rol a asignar
     * @param usuarioCont usuario en sesión
     * @param locale
     */
    public void assignRolUsuario(String idUsuario, String nombreRol, String usuarioCont, Locale locale) {
//        try {
//            //obtengo el rol    
//            RoleRepresentation rolRepresention = this.keycloak.realm("loyalty").roles().get(nombreRol).toRepresentation();
//            List<RoleRepresentation> listRoles = new ArrayList<>();
//            listRoles.add(rolRepresention);
//
//            //busco el usuario y se lo agrego
//            Usuario usuario = em.find(Usuario.class, idUsuario);
//            keycloak.realm("loyalty").users().get(usuario.getIdUsuario()).roles().realmLevel().add(listRoles);
//            bitacoraBean.logAccion(Usuario.class, idUsuario, usuarioCont, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
//        } catch (NotFoundException e) {
//            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "user_not_found");
//        } 
    }

    /**
     * Método que asigna un rol a un usuario especifico
     *
     * @param idUsuario identificador del usuario
     * @param nombreRoles lista de nombres de roles que se van a asignar
     * @param usuarioCont usuario en sesión
     * @param locale
     */
    public void assignListRolUsuario(String idUsuario, List<Rol> nombreRoles, String usuarioCont, Locale locale) {
        Usuario usuario = em.find(Usuario.class, idUsuario);
        if (usuario == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "user_not_found");
        }
        try (MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)) {
            keycloakAdmin.assignRolUser(nombreRoles, idUsuario);
        }
    }

    /**
     * Método que quita un rol a un usuario especifico
     *
     * @param idUsuario identificador del usuario
     * @param nombreRol nombre del rol que se desea desasignar
     * @param usuarioSesion usuario en sesion
     * @param locale
     */
    public void unassignRolUsuario(String idUsuario, String nombreRol, String usuarioSesion, Locale locale) {
//        try {
//            RoleRepresentation rolRepresention = this.keycloak.realm("loyalty").roles().get(nombreRol).toRepresentation();
//            List<RoleRepresentation> listRoles = new ArrayList<>();
//            listRoles.add(rolRepresention);
//            //busco el usuario y se lo agrego
//            Usuario usuario = em.find(Usuario.class, idUsuario);
//            keycloak.realm("loyalty").users().get(usuario.getIdUsuario()).roles().realmLevel().remove(listRoles);
//            bitacoraBean.logAccion(Usuario.class, idUsuario, usuarioSesion, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
//
//        } catch (NotFoundException e) {
//            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "user_not_found");
//        }
    }

    /**
     * Método que quita una lista de roles a un usuario especifico
     *
     * @param idUsuario identificador del usuario
     * @param nombreRoles lista de nombres de roles
     * @param usuarioSesion usuario en sesion
     * @param locale
     */
    public void unassignLisRolUsuario(String idUsuario, List<Rol> nombreRoles, String usuarioSesion, Locale locale) {
        Usuario usuario = em.find(Usuario.class, idUsuario);
        if (usuario == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "user_not_found");
        }
        
        try(MyKeycloakUtilsAdmin keycloakAdmin = new MyKeycloakUtilsAdmin(locale)){
            keycloakAdmin.unassignRolUser(nombreRoles, idUsuario);
        }
    }
}
