package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mision.getIdMisionByIndEstado", query = "SELECT m.idMision FROM Mision m WHERE m.indEstado = :indEstado")
})
public class Mision implements Serializable {

    @Column(name = "IND_RESPUESTA")
    private Boolean indRespuesta;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    public enum Estados {
        PUBLICADO('A'),
        ARCHIVADO('B'),
        BORRADOR('C'),
        INACTIVO('D');

        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }

        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Estados get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum Tipos {
        ENCUESTA('E'),
        PERFIL('P'),
        VER_CONTENIDO('V'),
        SUBIR_CONTENIDO('S'),
        RED_SOCIAL('R'),
        JUEGO_PREMIO('J'),
        REALIDAD_AUMENTADA('B'),
        PREFERENCIAS('A');

        private final char value;
        private static final Map<Character, Tipos> lookup = new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }

        static {
            for (Tipos tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static Tipos get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum IntervalosTiempoRespuesta {
        POR_SIEMPRE('A'),
        POR_MINUTO('B'),
        POR_HORA('C'),
        POR_DIA('D'),
        POR_SEMANA('E'),
        POR_MES('F');

        private final char value;
        private static final Map<Character, IntervalosTiempoRespuesta> lookup = new HashMap<>();

        private IntervalosTiempoRespuesta(char value) {
            this.value = value;
        }

        static {
            for (IntervalosTiempoRespuesta tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static IntervalosTiempoRespuesta get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum Aprobaciones {
        AUTOMATICO('A'),
        MANUAL('B');

        private final char value;
        private static final Map<Character, Aprobaciones> lookup = new HashMap<>();

        private Aprobaciones(char value) {
            this.value = value;
        }

        static {
            for (Aprobaciones tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static Aprobaciones get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }
    
    public enum IndicadoresCalendarizacion {
        PERMANENTE('P'),
        CALENDARIZADO('C');
        
        private final char value;
        private static final Map<Character, IndicadoresCalendarizacion> lookup = new HashMap<>();

        private IndicadoresCalendarizacion(char value) {
            this.value = value;
        }
        
        static {
            for (IndicadoresCalendarizacion calendarizacion : values()) {
                lookup.put(calendarizacion.value, calendarizacion);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static IndicadoresCalendarizacion get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "mision_uuid")
    @GenericGenerator(name = "mision_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_MISION")
    private String idMision;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "NOMBRE")
    private String nombre;

    @Size(min = 1, max = 300)
    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    private String imagen;

    @Basic(optional = false)
    @NotNull
    @Column(name = "CANT_METRICA")
    private Double cantMetrica;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_APROVACION")
    private Character indAprovacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_MISION")
    private Character indTipoMision;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;

    @Size(max = 200)
    @Column(name = "TAGS")
    private String tags;


    @Column(name = "CANT_TOTAL")
    private Long cantTotal;

    @Column(name = "IND_INTERVALO_TOTAL")
    private Character indIntervaloTotal;

    @Column(name = "CANT_TOTAL_MIEMBRO")
    private Long cantTotalMiembro;

    @Column(name = "IND_INTERVALO_MIMEBRO")
    private Character indIntervaloMimebro;

    @Column(name = "CANT_INTERVALO")
    private Long cantIntervalo;

    @Column(name = "IND_INTERVALO")
    private Character indIntervalo;

    @Column(name = "ENCABEZADO_ARTE")
    private String encabezadoArte;

    @Column(name = "SUBENCABEZADO_ARTE")
    private String subEncabezadoArte;

    @Column(name = "TEXTO_ARTE")
    private String textoArte;


    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    private Metrica idMetrica;

    @Column(name = "IND_CALENDARIZACION")
    private Character indCalendarizacion;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;

    @Size(max = 7)
    @Column(name = "IND_DIA_RECURRENCIA")
    private String indDiaRecurrencia;

    @Column(name = "IND_SEMANA_RECURRENCIA")
    private BigInteger indSemanaRecurrencia;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idJuego")
    private List<MisionJuego> misionJuegoList;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "mision")
    private Juego juego;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mision")
    private List<MisionListaSegmento> misionListaSegmentoList;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "mision")
    private MisionVerContenido misionVerContenido;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mision")
    private List<MisionListaUbicacion> misionListaUbicacionList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMision")
    private List<MisionPerfilAtributo> misionPerfilAtributoList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mision")
    private List<MisionEncuestaPregunta> misionEncuestaPreguntaList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "misionPreferenciaPK.idMision")
    private List<MisionPreferencia> misionPreferenciaList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mision")
    private List<MisionListaMiembro> misionListaMiembroList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mision")
    private List<MisionCategoriaMision> misionCategoriaMisionList;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "mision")
    private MisionRedSocial misionRedSocial;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "mision")
    private MisionSubirContenido misionSubirContenido;
    
    @Column(name = "IND_DEFECTO_NINGUNO")
    private Boolean indDefectoNinguno;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "mision")
    private MisionRealidadAumentada misionRealidadAumentada;

    public Mision() {
    }

    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getCantMetrica() {
        return cantMetrica;
    }

    public void setCantMetrica(Double cantMetrica) {
        this.cantMetrica = cantMetrica;
    }

    public Character getIndAprovacion() {
        return indAprovacion;
    }

    public void setIndAprovacion(Character indAprovacion) {
        this.indAprovacion = indAprovacion == null ? null : Character.toUpperCase(indAprovacion);
    }

    public Character getIndTipoMision() {
        return indTipoMision;
    }

    public void setIndTipoMision(Character indTipoMision) {
        this.indTipoMision = indTipoMision == null ? null : Character.toUpperCase(indTipoMision);
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Boolean getIndRespuesta() {
        return indRespuesta;
    }

    public void setIndRespuesta(Boolean indRespuesta) {
        this.indRespuesta = indRespuesta;
    }

    public Long getCantTotal() {
        return cantTotal;
    }

    public void setCantTotal(Long cantTotal) {
        this.cantTotal = cantTotal;
    }

    public Character getIndIntervaloTotal() {
        return indIntervaloTotal;
    }

    public void setIndIntervaloTotal(Character indIntervaloTotal) {
        this.indIntervaloTotal = indIntervaloTotal == null ? null : Character.toUpperCase(indIntervaloTotal);
    }

    public Long getCantTotalMiembro() {
        return cantTotalMiembro;
    }

    public void setCantTotalMiembro(Long cantTotalMiembro) {
        this.cantTotalMiembro = cantTotalMiembro;
    }

    public Character getIndIntervaloMimebro() {
        return indIntervaloMimebro;
    }

    public void setIndIntervaloMimebro(Character indIntervaloMimebro) {
        this.indIntervaloMimebro = indIntervaloMimebro == null ? null : Character.toUpperCase(indIntervaloMimebro);
    }

    public Long getCantIntervalo() {
        return cantIntervalo;
    }

    public void setCantIntervalo(Long cantIntervalo) {
        this.cantIntervalo = cantIntervalo;
    }

    public Character getIndIntervalo() {
        return indIntervalo;
    }

    public void setIndIntervalo(Character indIntervalo) {
        this.indIntervalo = indIntervalo == null ? null : Character.toUpperCase(indIntervalo);
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MisionListaSegmento> getMisionListaSegmentoList() {
        return misionListaSegmentoList;
    }

    public void setMisionListaSegmentoList(List<MisionListaSegmento> misionListaSegmentoList) {
        this.misionListaSegmentoList = misionListaSegmentoList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public MisionVerContenido getMisionVerContenido() {
        return misionVerContenido;
    }

    public void setMisionVerContenido(MisionVerContenido misionVerContenido) {
        this.misionVerContenido = misionVerContenido;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MisionListaUbicacion> getMisionListaUbicacionList() {
        return misionListaUbicacionList;
    }

    public void setMisionListaUbicacionList(List<MisionListaUbicacion> misionListaUbicacionList) {
        this.misionListaUbicacionList = misionListaUbicacionList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MisionListaMiembro> getMisionListaMiembroList() {
        return misionListaMiembroList;
    }

    public void setMisionListaMiembroList(List<MisionListaMiembro> misionListaMiembroList) {
        this.misionListaMiembroList = misionListaMiembroList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MisionCategoriaMision> getMisionCategoriaMisionList() {
        return misionCategoriaMisionList;
    }

    public void setMisionCategoriaMisionList(List<MisionCategoriaMision> misionCategoriaMisionList) {
        this.misionCategoriaMisionList = misionCategoriaMisionList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MisionPerfilAtributo> getMisionPerfilAtributoList() {
        return misionPerfilAtributoList;
    }

    public void setMisionPerfilAtributoList(List<MisionPerfilAtributo> misionPerfilAtributoList) {
        this.misionPerfilAtributoList = misionPerfilAtributoList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MisionEncuestaPregunta> getMisionEncuestaPreguntaList() {
        return misionEncuestaPreguntaList;
    }

    public void setMisionEncuestaPreguntaList(List<MisionEncuestaPregunta> misionEncuestaPreguntaList) {
        this.misionEncuestaPreguntaList = misionEncuestaPreguntaList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MisionPreferencia> getMisionPreferenciaList() {
        return misionPreferenciaList;
    }

    public void setMisionPreferenciaList(List<MisionPreferencia> misionPreferenciaList) {
        this.misionPreferenciaList = misionPreferenciaList;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public MisionRedSocial getMisionRedSocial() {
        return misionRedSocial;
    }

    public void setMisionRedSocial(MisionRedSocial misionRedSocial) {
        this.misionRedSocial = misionRedSocial;
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public MisionSubirContenido getMisionSubirContenido() {
        return misionSubirContenido;
    }

    public MisionRealidadAumentada getMisionRealidadAumentada() {
        return misionRealidadAumentada;
    }

    public void setMisionRealidadAumentada(MisionRealidadAumentada misionRealidadAumentada) {
        this.misionRealidadAumentada = misionRealidadAumentada;
    }

    public void setMisionSubirContenido(MisionSubirContenido misionSubirContenido) {
        this.misionSubirContenido = misionSubirContenido;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getEncabezadoArte() {
        return encabezadoArte;
    }

    public void setEncabezadoArte(String encabezadoArte) {
        this.encabezadoArte = encabezadoArte;
    }

    public String getSubEncabezadoArte() {
        return subEncabezadoArte;
    }

    public void setSubEncabezadoArte(String subEncabezadoArte) {
        this.subEncabezadoArte = subEncabezadoArte;
    }

    public String getTextoArte() {
        return textoArte;
    }

    public void setTextoArte(String textoArte) {
        this.textoArte = textoArte;
    }

    public Character getIndCalendarizacion() {
        return indCalendarizacion;
    }

    public void setIndCalendarizacion(Character indCalendarizacion) {
        this.indCalendarizacion = indCalendarizacion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getIndDiaRecurrencia() {
        return indDiaRecurrencia;
    }

    public void setIndDiaRecurrencia(String indDiaRecurrencia) {
        this.indDiaRecurrencia = indDiaRecurrencia;
    }

    public BigInteger getIndSemanaRecurrencia() {
        return indSemanaRecurrencia;
    }

    public void setIndSemanaRecurrencia(BigInteger indSemanaRecurrencia) {
        this.indSemanaRecurrencia = indSemanaRecurrencia;
    }

    public Boolean getIndDefectoNinguno() {
        return indDefectoNinguno;
    }

    public void setIndDefectoNinguno(Boolean indDefectoNinguno) {
        this.indDefectoNinguno = indDefectoNinguno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMision != null ? idMision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mision)) {
            return false;
        }
        Mision other = (Mision) object;
        if ((this.idMision == null && other.idMision != null) || (this.idMision != null && !this.idMision.equals(other.idMision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Mision[ idMision=" + idMision + " ]";
    }
   
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public List<MisionJuego> getMisionJuegoList() {
        return misionJuegoList;
    }

    public void setMisionJuegoList(List<MisionJuego> misionJuegoList) {
        this.misionJuegoList = misionJuegoList;
    }
    
    @ApiModelProperty(hidden = true)
    @XmlTransient
    public Juego getJuego() {
        return juego;
    }

    public void setJuego(Juego juego) {
        this.juego = juego;
    }

}
