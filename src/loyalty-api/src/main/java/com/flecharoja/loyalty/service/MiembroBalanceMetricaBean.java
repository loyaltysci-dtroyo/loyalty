package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.MiembroBalanceMetrica;
import com.flecharoja.loyalty.model.MiembroBalanceMetricaPK;
import java.util.List;
import java.util.Locale;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * EJB encargado del acceso de los datos de la billetera de un miembro
 * almacenado en HBase via API REST
 *
 * @author svargas
 */
@Stateless
public class MiembroBalanceMetricaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * obtencion de datos de la billetera de una metrica para un miembro
     *
     * @param idMiembro id de miembro
     * @param idMetrica id de metrica
     * @return data de billetera
     */
    public MiembroBalanceMetrica getBilleteraMiembroMetrica(String idMiembro, String idMetrica) {
        MiembroBalanceMetrica balance = em.find(MiembroBalanceMetrica.class, new MiembroBalanceMetricaPK(idMiembro, idMetrica));
        if (balance==null) {
            balance = new MiembroBalanceMetrica(idMiembro, idMetrica);
        }
        return balance;
    }

    public MiembroBalanceMetrica getBilleteraMiembroMetricaPrincipal(String idMiembro, Locale locale) {
        ConfiguracionGeneral configuracionGeneral = em.find(ConfiguracionGeneral.class, 0l);
        if (configuracionGeneral == null) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "general_configuration_not_found");
        }

        if (configuracionGeneral.getIdMetricaInicial() == null) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "main_metric_not_found");
        }

        MiembroBalanceMetrica balance = em.find(MiembroBalanceMetrica.class, new MiembroBalanceMetricaPK(idMiembro, configuracionGeneral.getIdMetricaInicial().getIdMetrica()));
        if (balance == null) {
            balance = new MiembroBalanceMetrica(idMiembro, configuracionGeneral.getIdMetricaInicial().getIdMetrica());
        }
        return balance;
    }
    
    public MiembroBalanceMetrica getBilleteraMiembroMetricaSecundaria(String idMiembro, Locale locale) {
        ConfiguracionGeneral configuracionGeneral = em.find(ConfiguracionGeneral.class, 0l);
        if (configuracionGeneral == null) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "general_configuration_not_found");
        }

        if (configuracionGeneral.getIdMetricaSecundaria() == null) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "secondary_metric_not_found");
        }

        MiembroBalanceMetrica balance = em.find(MiembroBalanceMetrica.class, new MiembroBalanceMetricaPK(idMiembro, configuracionGeneral.getIdMetricaSecundaria().getIdMetrica()));
        if (balance == null) {
            balance = new MiembroBalanceMetrica(idMiembro, configuracionGeneral.getIdMetricaSecundaria().getIdMetrica());
        }
        return balance;
    }

    /**
     * obtencion de todas las billeteras de un miembro
     *
     * @param idMiembro id de miembro
     * @return lista de billeteras
     */
    public List<MiembroBalanceMetrica> getBilleterasMiembro(String idMiembro) {
        List<MiembroBalanceMetrica> balances = em.createNamedQuery("MiembroBalanceMetrica.findByIdMiembro").setParameter("idMiembro", idMiembro).getResultList();
        return balances;
    }
}
