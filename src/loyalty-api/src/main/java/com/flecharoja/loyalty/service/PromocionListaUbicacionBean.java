package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.model.PromocionListaUbicacion;
import com.flecharoja.loyalty.model.PromocionListaUbicacionPK;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * EJB encargado de el manejo de asignaciones de ubicaciones con promociones
 *
 * @author svargas
 */
@Stateless
public class PromocionListaUbicacionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * Metodo que obtiene un listado de ubicaciones asignados a una promocion en
     * un rango de respuesta y opcionalmente por el tipo de accion
     * (incluido/excluido)
     *
     * @param idPromocion Identificador de la promocion
     * @param indAccion Indicador de tipo de accion deseado
     * @param estados
     * @param calendarizaciones
     * @param registro Numero de registro inicial
     * @param filtros
     * @param busqueda
     * @param ordenCampo
     * @param ordenTipo
     * @param cantidad Cantidad de registros deseados
     * @param locale
     * @return Respuesta con el listado de miembros y su rango de respuesta
     */
    public Map<String, Object> getUbicaciones(String idPromocion, String indAccion, List<String> estados,List<String> calendarizaciones, String busqueda, List<String> filtros, String ordenTipo, String ordenCampo, int registro, int cantidad, Locale locale) {
        //verificacion de que el rango este dentro del valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Ubicacion> root = query.from(Ubicacion.class);

        query = Busquedas.getCriteriaQueryUbicaciones(cb, query, root, estados, calendarizaciones, busqueda, filtros, ordenTipo, ordenCampo);

        Subquery<String> subquery = query.subquery(String.class);
        Root<PromocionListaUbicacion> rootSubquery = subquery.from(PromocionListaUbicacion.class);
        subquery.select(rootSubquery.get("promocionListaUbicacionPK").get("idUbicacion"));
        if (indAccion == null) {
            subquery.where(cb.equal(rootSubquery.get("promocionListaUbicacionPK").get("idPromocion"), idPromocion));
            if (query.getRestriction() != null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idUbicacion")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idUbicacion")).value(subquery)));
            }
        } else {
            switch (indAccion.toUpperCase().charAt(0)) {
                case Indicadores.INCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("promocionListaUbicacionPK").get("idPromocion"), idPromocion), cb.equal(rootSubquery.get("indTipo"), Indicadores.INCLUIDO));
                    if (query.getRestriction() != null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idUbicacion")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idUbicacion")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.EXCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("promocionListaUbicacionPK").get("idPromocion"), idPromocion), cb.equal(rootSubquery.get("indTipo"), Indicadores.EXCLUIDO));
                    if (query.getRestriction() != null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idUbicacion")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idUbicacion")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.DISPONIBLES: {
                    subquery.where(cb.equal(rootSubquery.get("promocionListaUbicacionPK").get("idPromocion"), idPromocion));
                    if (query.getRestriction() != null) {
                        query.where(query.getRestriction(), cb.equal(root.get("indEstado"), Ubicacion.Estados.PUBLICADO_ACTIVO.getValue()), cb.or(cb.in(root.get("idUbicacion")).value(subquery).not()));
                    } else {
                        query.where(cb.equal(root.get("indEstado"), Ubicacion.Estados.PUBLICADO_ACTIVO.getValue()), cb.or(cb.in(root.get("idUbicacion")).value(subquery).not()));
                    }
                    break;
                }
                default: {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "indAccion");
                }
            }
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * Metodo que asigna (incluye o excluye) a una ubicacion de una promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param idUbicacion Identificador de la ubicacion
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesión
     * @param locale
     */
    public void includeExcludeUbicaciones(String idPromocion, String idUbicacion, Character indAccion, String usuario,Locale locale) {
        //verificacion que los id's sean validos
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        Ubicacion ubicacion = em.find(Ubicacion.class, idUbicacion);
        if (ubicacion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
        }

        //verificacion de entidades archivadas/borrador
        if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())
                || !ubicacion.getIndEstado().equals(Ubicacion.Estados.PUBLICADO_ACTIVO.getValue())) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "operation_over_archived", "Promo, Ubicacion");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            PromocionListaUbicacion promocionListaUbicacion = new PromocionListaUbicacion(idUbicacion, idPromocion, indAccion, date, usuario);

            em.merge(promocionListaUbicacion);
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        bitacoraBean.logAccion(PromocionListaUbicacion.class, idPromocion + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * Metodo que revoca la asignacion de una ubicacion a una promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param idUbicacion Identificador de la ubicacion
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeUbicacion(String idPromocion, String idUbicacion, String usuario, Locale locale) {
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        try {
            em.getReference(Ubicacion.class, idUbicacion).getIdUbicacion();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
        }

        //verificacion de entidades archivadas/borrador
        if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Promo");
        }

        try {
            //verificacion que la entidad exista y su eliminacion
            em.remove(em.getReference(PromocionListaUbicacion.class, new PromocionListaUbicacionPK(idUbicacion, idPromocion)));
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_location_not_found");
        }

        bitacoraBean.logAccion(PromocionListaUbicacion.class, idPromocion + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }

    /**
     * Metodo que asigna (incluye o excluye) una lista de ubicaciones de una
     * promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param listaIdUbicacion Identificadores de ubicaciones
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario sesión
     * @param locale
     */
    public void includeExcludeBatchUbicacion(String idPromocion, List<String> listaIdUbicacion, Character indAccion, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }

        listaIdUbicacion = listaIdUbicacion.stream().distinct().collect(Collectors.toList());

        //verificacion de entidades archivadas
        if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
           throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "operation_over_archived", "Promo");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            for (String idUbicacion : listaIdUbicacion) {
                //verificacion que los id's sean validos
                Ubicacion ubicacion = em.find(Ubicacion.class, idUbicacion);
                if (ubicacion == null) {
                    em.clear();
                    throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
                }

                //verificacion de entidades archivadas
                if (!ubicacion.getIndEstado().equals(Ubicacion.Estados.PUBLICADO_ACTIVO.getValue())) {
                    throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "attributes_invalid", "indEstado");
                }

                em.merge(new PromocionListaUbicacion(idUbicacion, idPromocion, indAccion, date, usuario));
            }
            em.flush();
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        for (String idUbicacion : listaIdUbicacion) {
            bitacoraBean.logAccion(PromocionListaUbicacion.class, idPromocion + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        }
    }

    /**
     * Metodo que revoca la asignacion de una lista de ubicaciones a una
     * promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param listaIdUbicacion Identificadores de ubicaciones
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeBatchUbicacion(String idPromocion, List<String> listaIdUbicacion, String usuario,Locale locale) {
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }

        listaIdUbicacion = listaIdUbicacion.stream().distinct().collect(Collectors.toList());

        for (String idUbicacion : listaIdUbicacion) {
            try {
                em.getReference(Ubicacion.class, idUbicacion).getIdUbicacion();
            } catch (EntityNotFoundException e) {
                throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "location_not_found");
            }
        }

        //verificacion de entidades archivadas/borrador
        if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Promo");
        }

        try {
            listaIdUbicacion.stream().forEach((idUbicacion) -> {
                //verificacion que la entidad exista y su eliminacion
                em.remove(em.getReference(PromocionListaUbicacion.class, new PromocionListaUbicacionPK(idUbicacion, idPromocion)));
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_location_not_found");
        }

        for (String idUbicacion : listaIdUbicacion) {
            bitacoraBean.logAccion(PromocionListaUbicacion.class, idPromocion + "/" + idUbicacion, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        }
    }
}
