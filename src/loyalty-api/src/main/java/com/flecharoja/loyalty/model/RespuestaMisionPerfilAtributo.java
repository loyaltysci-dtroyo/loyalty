package com.flecharoja.loyalty.model;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaMisionPerfilAtributo {
    private String idAtributo;
    
    private MisionPerfilAtributo detalleAtributo;
    
    private String respuesta;

    public RespuestaMisionPerfilAtributo() {
    }

    @ApiModelProperty(hidden = true)
    @XmlTransient
    public String getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public MisionPerfilAtributo getDetalleAtributo() {
        return detalleAtributo;
    }

    public void setDetalleAtributo(MisionPerfilAtributo detalleAtributo) {
        this.detalleAtributo = detalleAtributo;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
}
