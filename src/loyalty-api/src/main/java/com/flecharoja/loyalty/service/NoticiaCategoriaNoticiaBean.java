package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.model.CategoriaNoticia;
import com.flecharoja.loyalty.model.Noticia;
import com.flecharoja.loyalty.model.NoticiaCategoriaNoticia;
import com.flecharoja.loyalty.model.NoticiaCategoriaNoticiaPK;
import com.flecharoja.loyalty.util.Busquedas;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * Clase encargada de proveer metodos para el mantenimiento de listados y
 * asignaciones de noticia con categorias de noticia asi como verificaciones y
 * manejo de datos
 *
 * @author svargas
 */
@Stateless
public class NoticiaCategoriaNoticiaBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora

    /**
     * obtencion de noticias por categoria
     * 
     * @param idCategoria identificador de categoria
     * @param indAccion indicador de accion de disponible/asignado
     * @param estados filtro de estado de noticia
     * @param busqueda filtro de busqueda de noticia
     * @param filtros filtros para busqueda de noticia
     * @param cantidad paginacion
     * @param registro paginacion
     * @param ordenTipo tipo de orden paginacion
     * @param ordenCampo campo de orden para paginacion
     * @param locale locale
     * @return lista de noticias
     */
    public Map<String, Object> getNoticiasPorIdCategoria(String idCategoria, String indAccion, List<String> estados, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion de que la entidad exista
        if (em.find(CategoriaNoticia.class, idCategoria) == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "news_category_not_found");
        }

        //verifica que la cantidad en la peticion sea menor a la aceptada, de no ser asi se retorna una respuesta informando acerca de ello, junto con el encabezado de rango maximo
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Noticia> root = query.from(Noticia.class);
        
        //formacion de query de busqueda de noticias
        query = Busquedas.getCriteriaQueryNoticias(cb, query, root, estados, busqueda, filtros, ordenTipo, ordenCampo);

        //sub query para ell filtrado por accion
        Subquery<String> subquery = query.subquery(String.class);
        Root<NoticiaCategoriaNoticia> rootSubquery = subquery.from(NoticiaCategoriaNoticia.class);
        subquery.select(rootSubquery.get("noticiaCategoriaNoticiaPK").get("idNoticia")).where(cb.equal(rootSubquery.get("noticiaCategoriaNoticiaPK").get("idCategoria"), idCategoria));
        switch(indAccion.toUpperCase().charAt(0)) {
            case Indicadores.DISPONIBLES: {
                if (query.getRestriction()!=null) {
                    query.where(query.getRestriction(), cb.equal(root.get("indEstado"), Noticia.Estados.PUBLICADO.getValue()), cb.or(cb.in(root.get("idNoticia")).value(subquery).not()));
                } else {
                    query.where(cb.equal(root.get("indEstado"), Noticia.Estados.PUBLICADO.getValue()), cb.or(cb.in(root.get("idNoticia")).value(subquery).not()));
                }
                break;
            }
            case Indicadores.ASIGNADO: {
                if (query.getRestriction()!=null) {
                    query.where(query.getRestriction(), cb.or(cb.in(root.get("idNoticia")).value(subquery)));
                } else {
                    query.where(cb.or(cb.in(root.get("idNoticia")).value(subquery)));
                }
                break;
            }
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        resultado.stream().forEach((t) -> {
            Noticia noticia = (Noticia) t;
            
            noticia.setCantMarcas((long) em.createNamedQuery("MarcadorNoticia.countByIdNoticia").setParameter("idNoticia", noticia.getIdNoticia()).getSingleResult());
            noticia.setCantComentarios(noticia.getComentarios().stream().count());
        });
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * obtencion de categorias por noticia
     * 
     * @param idNoticia id de noticia
     * @param indAccion indicador de accion (disponible/asignado)
     * @param busqueda busqueda para categoria
     * @param indTipoOrden tipo de orden paginacion
     * @param indCampoOrden campo para orden de paginacion
     * @param registro paginacion
     * @param cantidad paginacion
     * @param locale locale
     * @return lista de categorias
     */
    public Map<String, Object> getCategoriasPorIdNoticia(String idNoticia, String indAccion, String busqueda, String indTipoOrden, String indCampoOrden, int registro, int cantidad, Locale locale) {
        //verificacion de que la entidad exista
        if (em.find(Noticia.class, idNoticia) == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "challenge_category_not_found");
        }

        //verifica que la cantidad en la peticion sea menor a la aceptada, de no ser asi se retorna una respuesta informando acerca de ello, junto con el encabezado de rango maximo
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
           throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<CategoriaNoticia> root = query.from(CategoriaNoticia.class);

        //query de busqueda de categoria de noticia
        query = Busquedas.getCriteriaQueryCategoriaNoticia(cb, query, root, busqueda, indTipoOrden, indCampoOrden);
        
        //sub query segun la accion para filtro de categorias
        Subquery<String> subquery = query.subquery(String.class);
        Root<NoticiaCategoriaNoticia> rootSubquery = subquery.from(NoticiaCategoriaNoticia.class);
        subquery.select(rootSubquery.get("noticiaCategoriaNoticiaPK").get("idCategoria")).where(cb.equal(rootSubquery.get("noticiaCategoriaNoticiaPK").get("idNoticia"), idNoticia));
        switch(indAccion.toUpperCase().charAt(0)) {
            case Indicadores.DISPONIBLES: {
                if (query.getRestriction()!=null) {
                    query.where(query.getRestriction(), cb.or(cb.in(root.get("idCategoria")).value(subquery).not()));
                } else {
                    query.where(cb.or(cb.in(root.get("idCategoria")).value(subquery).not()));
                }
                break;
            }
            case Indicadores.ASIGNADO: {
                if (query.getRestriction()!=null) {
                    query.where(query.getRestriction(), cb.or(cb.in(root.get("idCategoria")).value(subquery)));
                } else {
                    query.where(cb.or(cb.in(root.get("idCategoria")).value(subquery)));
                }
                break;
            }
        }

        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }

        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }

        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;
    }

    /**
     * asignacion de categoria a noticia
     * 
     * @param idCategoria id de categoria
     * @param idNoticia id de noticia
     * @param usuario id admin
     * @param locale locale
     */
    public void assignCategoriaNoticia(String idCategoria, String idNoticia, String usuario,Locale locale) {
        //se verifica que tanto la categoria como la noticia existan
        Noticia noticia = em.find(Noticia.class, idNoticia);
        if (noticia == null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "news_not_found");
        }
        if (em.find(CategoriaNoticia.class, idCategoria) == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "news_category_not_found");
        }

        //verifica que la promocion a asignar a la categoria, no tenga un estado de borrador, ni archivado
        if (noticia.getIndEstado().equals(Noticia.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Noticia");
        }

        //fecha de creacion
        Date date = Calendar.getInstance().getTime();

        try {
            em.persist(new NoticiaCategoriaNoticia(idCategoria, idNoticia, date, usuario));
            em.flush();
        } catch (EntityExistsException e) {//en el caso de que ya exista la entidad en la bases de datos
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "news_asociated_category");
        }

        bitacoraBean.logAccion(NoticiaCategoriaNoticia.class, idCategoria + "/" + idNoticia, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
    }

    /**
     * eliminacion de asignacion de una categoria a una noticia
     * 
     * @param idCategoria id de categoria
     * @param idNoticia ide de noticia
     * @param usuario id admin
     * @param locale locale
     */
    public void unassignCategoriaNoticia(String idCategoria, String idNoticia, String usuario,Locale locale) {
        try {
            //se intenta remover la entidad a partir de la referencia de la misma por sus identificadores
            em.remove(em.getReference(NoticiaCategoriaNoticia.class, new NoticiaCategoriaNoticiaPK(idCategoria, idNoticia)));
            em.flush();
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "news_category_not_foound");
        }

        bitacoraBean.logAccion(NoticiaCategoriaNoticia.class, idCategoria + "/" + idNoticia, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }

    /**
     * asignacion de batch de noticias a categoria
     * 
     * @param idCategoria id de categoria
     * @param listaIdNoticia lista de id's de noticias
     * @param usuario id admin
     * @param locale locale
     */
    public void assignBatchCategoriaMision(String idCategoria, List<String> listaIdNoticia, String usuario,Locale locale) {
        //se verifica que la categoria exista
        if (em.find(CategoriaNoticia.class, idCategoria) == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "news_category_not_found");
        }

        for (String idNoticia : listaIdNoticia) {
            //se verifica que la promocion exista
            Noticia noticia = em.find(Noticia.class, idNoticia);
            if (noticia == null) {
                throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "news_not_found");
            }
            //verifica que la promocion a asignar a la categoria, no tenga un estado de borrador, ni archivado
            if (noticia.getIndEstado().equals(Noticia.Estados.ARCHIVADO.getValue())) {
                throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Noticia");
            }
        }

        //fecha de creacion
        Date date = Calendar.getInstance().getTime();

        try {
            listaIdNoticia.stream().forEach((idNoticia) -> {
                em.persist(new NoticiaCategoriaNoticia(idCategoria, idNoticia, date, usuario));
            });
            em.flush();
        } catch (EntityExistsException e) {//en el caso de que ya exista la entidad en la bases de datos
            throw new MyException(MyException.ErrorSistema.ENTIDAD_EXISTENTE, locale, "news_asociated_category");
        }

        listaIdNoticia.forEach((idMision) -> {
            bitacoraBean.logAccion(NoticiaCategoriaNoticia.class, idCategoria + "/" + idMision, usuario, Bitacora.Accion.ENTIDAD_INSERTADA.getValue(),locale);
        });
    }

    /**
     * eliminacion de asignacion de noticias con una categoria
     * 
     * @param idCategoria id de categoria
     * @param listaIdNoticia lista de id's de noticia
     * @param usuario id admin
     * @param locale locale
     */
    public void unassignBatchCategoriaMision(String idCategoria, List<String> listaIdNoticia, String usuario,Locale locale) {
        try {
            listaIdNoticia.stream().forEach((idNoticia) -> {
                //se intenta remover la entidad a partir de la referencia de la misma por sus identificadores
                em.remove(em.getReference(NoticiaCategoriaNoticia.class, new NoticiaCategoriaNoticiaPK(idCategoria, idNoticia)));
            });
            em.flush();
        } catch (EntityNotFoundException e) {//en el caso de que no se encuentre la referencia
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "news_category_not_foound");
        }

        listaIdNoticia.forEach((idMision) -> {
            bitacoraBean.logAccion(NoticiaCategoriaNoticia.class, idCategoria + "/" + idMision, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        });
    }
}
