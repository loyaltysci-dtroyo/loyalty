package com.flecharoja.loyalty.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RespuestaMisionRedSocial {
    private MisionRedSocial detalleRedSocial;

    public RespuestaMisionRedSocial() {
    }

    public MisionRedSocial getDetalleRedSocial() {
        return detalleRedSocial;
    }

    public void setDetalleRedSocial(MisionRedSocial detalleRedSocial) {
        this.detalleRedSocial = detalleRedSocial;
    }
}
