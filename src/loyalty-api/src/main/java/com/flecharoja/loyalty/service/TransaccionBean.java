package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.CompraProducto;
import com.flecharoja.loyalty.model.CompraProductoPK;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Producto;
import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.model.RespuestaRegistroEntidad;
import com.flecharoja.loyalty.model.TransaccionCompra;
import com.flecharoja.loyalty.model.Ubicacion;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.util.MyKafkaUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author svargas
 */
@Stateless
public class TransaccionBean {
    
    @EJB
    MyKafkaUtils myKafkaUtils;
    
    @EJB
    private RegistroMetricaService registroMetricaService;
    
    @EJB
    private AutoAsignacionPremios autoAsignacionPremios;

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * registro de una transaccion de compra
     * 
     * @param transaccionCompra data de transaccion
     * @param locale locale
     * @return respuesta con el id de transaccion creado
     */
    public RespuestaRegistroEntidad registrarTransaccion(TransaccionCompra transaccionCompra, Locale locale) {
        //comprobacion de datos
        if (transaccionCompra==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "");
        }
        
        if (transaccionCompra.getMiembro()==null || transaccionCompra.getMiembro().getIdMiembro()==null) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "");
        }
        Miembro miembro = em.find(Miembro.class, transaccionCompra.getMiembro().getIdMiembro());
        if (miembro==null) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "");
        }
        transaccionCompra.setMiembro(miembro);
        
        Ubicacion ubicacion = transaccionCompra.getUbicacion();
        if (ubicacion!=null && ubicacion.getIdUbicacion()!=null) {
            ubicacion = em.find(Ubicacion.class, ubicacion.getIdUbicacion());
            if (ubicacion==null) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "");
            }
            transaccionCompra.setUbicacion(ubicacion);
        }
        
        if (transaccionCompra.getFecha()==null) {
            transaccionCompra.setFecha(new Date());
        }
        
        if (transaccionCompra.getSubtotal()==null || transaccionCompra.getImpuesto()==null || transaccionCompra.getTotal()==null) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "", "subTotal, taxes, total");
        }
        
        if ((transaccionCompra.getSubtotal()+transaccionCompra.getImpuesto())!=transaccionCompra.getTotal()) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "", "subTotal, taxes, total");
        }
        
        //comprobacion de datos de detalle de productos
        List<CompraProducto> productsDetails = transaccionCompra.getCompraProductos();
        Map<String, CompraProducto> mapProductDetails;
        if (productsDetails!=null) {
            mapProductDetails = productsDetails.stream().collect(Collectors.toMap((t) -> t.getCompraProductoPK().getIdProducto(), (t) -> {
                if (t.getCantidad()==null || t.getProducto()==null || t.getProducto().getIdProducto()==null || t.getTotal()==null || t.getValorUnitario()==null || t.getCantidad().compareTo(0l)<=0 || t.getTotal().compareTo(0d)<=0 || t.getValorUnitario().compareTo(0d)<=0) {
                    return null;
                }
                if (em.find(Producto.class, t.getProducto().getIdProducto())==null) {
                    return null;
                }
                return t;
            }));

            Supplier<Stream<String>> supplier = () -> mapProductDetails.entrySet().stream().filter((t) -> t.getValue()==null).map((t) -> t.getKey());
            if (supplier.get().count()>0) {
                throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "products_definitions_invalid", supplier.get().collect(Collectors.joining(", ")));
            }
        } else {
            mapProductDetails = null;
        }
        
        //almacenamiento de transaccion y luego de detalle de productos
        transaccionCompra.setCompraProductos(null);
        try {
            em.persist(transaccionCompra);
            em.flush();
            if (mapProductDetails!=null && !mapProductDetails.isEmpty()) {
                mapProductDetails.values().stream().forEach((t) -> {
                    t.setCompraProductoPK(new CompraProductoPK(transaccionCompra.getIdTransaccion(), t.getProducto().getIdProducto()));
                    em.persist(t);
                });
            }
            em.flush();
        } catch (ConstraintViolationException e) {
            throw new MyException(MyException.ErrorPeticion.ATRIBUTO_CUERPO_INVALIDO, locale, "transaction_details_invalid", e.getConstraintViolations().stream().map((t) -> t.getPropertyPath().toString()).collect(Collectors.joining(", ")));
        }
        
        //notificacion de evento de compra
        try {
            myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.COMPRO_ALGUN_PRODUCTO, transaccionCompra.getIdTransaccion(), transaccionCompra.getMiembro().getIdMiembro());
            if (mapProductDetails!=null && !mapProductDetails.isEmpty()) {
                mapProductDetails.values().stream().forEach((t) -> myKafkaUtils.notifyEvento(MyKafkaUtils.EventosSistema.COMPRA_PRODUCTO_X, t.getProducto().getIdProducto(), transaccionCompra.getMiembro().getIdMiembro()));
            }
        } catch (Exception ex) {
            Logger.getLogger(TransaccionBean.class.getName()).log(Level.WARNING, null, ex);
        }
        
        if (transaccionCompra.getIdMetrica() == null) {
            ConfiguracionGeneral config = em.find(ConfiguracionGeneral.class, 0L);
            //calculo de metrica ganada
            if (config.getPorcentajeCompra()!=null && config.getIdMetricaInicial()!=null && config.getPorcentajeCompra()>0d) {
                double metricaGanada;
                if(config.getIdMetricaInicial().getMedida().equals("V")) {
                    metricaGanada = 1;
                } else {
                    metricaGanada = transaccionCompra.getTotal()*(config.getPorcentajeCompra()/100);
                }
                if (metricaGanada < 0) {
                    registroMetricaService.revertirMetrica(new RegistroMetrica(new Date(), config.getIdMetricaInicial().getIdMetrica(), transaccionCompra.getMiembro().getIdMiembro(), RegistroMetrica.TiposGane.REVERSION, metricaGanada*-1, transaccionCompra.getIdTransaccion(), null));
                } else {
                    registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), config.getIdMetricaInicial().getIdMetrica(), transaccionCompra.getMiembro().getIdMiembro(), RegistroMetrica.TiposGane.TRANSACCION, metricaGanada, transaccionCompra.getIdTransaccion(), null));
                }
            }
        } else {
            Metrica metrica = em.find(Metrica.class, transaccionCompra.getIdMetrica());
            double metricaGanada;
            if(metrica.getMedida().equals("V")) {
                metricaGanada = 1;
            } else {
                metricaGanada = transaccionCompra.getTotal()*(transaccionCompra.getCantMetrica()/100);
            }
            if (metricaGanada<0) {
                    registroMetricaService.revertirMetrica(new RegistroMetrica(new Date(), transaccionCompra.getIdMetrica(), transaccionCompra.getMiembro().getIdMiembro(), RegistroMetrica.TiposGane.REVERSION, metricaGanada*-1, transaccionCompra.getIdTransaccion(), null));
                } else {
                    registroMetricaService.insertRegistroMetrica(new RegistroMetrica(new Date(), transaccionCompra.getIdMetrica(), transaccionCompra.getMiembro().getIdMiembro(), RegistroMetrica.TiposGane.TRANSACCION, metricaGanada, transaccionCompra.getIdTransaccion(), null));
                }
        }
        
        //detone de comprobacion de asignacion de premios
        autoAsignacionPremios.eventosCompra(transaccionCompra);
        
        return new RespuestaRegistroEntidad(transaccionCompra.getIdTransaccion(), new Date());
    }
    
    /**
     * obtencion de lista de transacciones de un miembro
     * 
     * @param idMiembro id de miembro
     * @param cantidad paginacion
     * @param registro paginacion
     * @param locale locale
     * @return lista de transacciones
     */
    public Map<String, Object> getTransaccionesMiembro(String idMiembro, int cantidad, int registro, Locale locale){
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "cantidad");
        }
        List<TransaccionCompra> resultado;
        long total;
        try{
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        total = (long) em.createNamedQuery("TransaccionCompra.countByIdMiembro").setParameter("idMiembro", idMiembro).getSingleResult();
        total -= total - 1 < 0 ? 0 : 1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        resultado = em.createNamedQuery("TransaccionCompra.findByIdMiembro")
                .setParameter("idMiembro", idMiembro)
                .setFirstResult(registro)
                .setMaxResults(cantidad)
                .getResultList();
        
        
        }catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS, locale, "pagination_value_invalid", "registro");
        }
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);

        return respuesta;

    }
    
}
