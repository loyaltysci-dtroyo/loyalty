package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.EntradaRegistroActividad;
import com.flecharoja.loyalty.model.InstanciaNotificacion;
import com.flecharoja.loyalty.model.MarcadorPromocion;
import com.flecharoja.loyalty.model.Metrica;
import com.flecharoja.loyalty.model.Mision;
import com.flecharoja.loyalty.model.PremioMiembro;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.model.RespuestaMision;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.KeyOnlyFilter;
import org.apache.hadoop.hbase.filter.RegexStringComparator;
import org.apache.hadoop.hbase.filter.RowFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author svargas
 */
@Stateless
public class RegistrosActividadesBean {
    
    private enum PeriodosTiempo {
        UN_DIA("1D"),
        UNA_SEMANA("1S"),
        DOS_SEMANAS("2S"),
        UN_MES("1M"),
        DOS_MESES("2M"),
        TRES_MESES("3M"),
        SEIS_MESES("6M"),
        NUEVE_MESES("9M"),
        UN_ANO("1Y"),
        DOS_ANOS("2Y");
        
        private final String value;
        private static final Map<String, PeriodosTiempo> lookup = new HashMap<>();

        private PeriodosTiempo(String value) {
            this.value = value;
        }
        
        static {
            for (PeriodosTiempo periodo : values()) {
                lookup.put(periodo.value, periodo);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static PeriodosTiempo get(String value) {
            return value==null?null:lookup.get(value);
        }
    }

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    private final Configuration configuration;

    public RegistrosActividadesBean() {
        this.configuration = new Configuration();
        this.configuration.addResource("conf/hbase/hbase-site.xml");
    }
    
    private Date getFechaFinPeriodo(String indPeriodoTiempo) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        
        PeriodosTiempo pt = PeriodosTiempo.get(indPeriodoTiempo);
        if (pt==null) {
            pt = PeriodosTiempo.UN_MES;
        }
        switch (pt) {
            case UN_DIA: {
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                break;
            }
            case UNA_SEMANA: {
                calendar.add(Calendar.WEEK_OF_YEAR, -1);
                break;
            }
            case DOS_SEMANAS: {
                calendar.add(Calendar.WEEK_OF_YEAR, -2);
                break;
            }
            case UN_MES: {
                calendar.add(Calendar.MONTH, -1);
                break;
            }
            case DOS_MESES: {
                calendar.add(Calendar.MONTH, -2);
                break;
            }
            case TRES_MESES: {
                calendar.add(Calendar.MONTH, -3);
                break;
            }
            case SEIS_MESES: {
                calendar.add(Calendar.MONTH, -6);
                break;
            }
            case NUEVE_MESES: {
                calendar.add(Calendar.MONTH, -9);
                break;
            }
            case UN_ANO: {
                calendar.add(Calendar.YEAR, -1);
                break;
            }
            case DOS_ANOS: {
                calendar.add(Calendar.YEAR, -2);
                break;
            }
        }
        
        return calendar.getTime();
    }
    
    //********************************************************************************************NOTIFICACIONES********************************************************************************************
    
    /**
     * obtencion de actividades de notificaciones por miembro
     * 
     * @param idMiembro id de miembro
     * @param indPeriodoTiempo indicador de periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadNotificacionesPorMiembro(String idMiembro, String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaRegistroActividad> actividadMiembro = new ArrayList<>();
        List<InstanciaNotificacion> instancias = em.createNamedQuery("InstanciaNotificacion.findAllByIdMiembro").setParameter("idMiembro", idMiembro).getResultList();
        instancias.stream().filter((instancia) -> instancia.getFechaCreacion().after(fechaFinPeriodo) || DateUtils.isSameDay(instancia.getFechaCreacion(), fechaFinPeriodo)).map((instancia) -> {
            actividadMiembro.add(new EntradaRegistroActividad(instancia.getNotificacion().getIdNotificacion(), instancia.getNotificacion().getNombre(), instancia.getNotificacion().getNombreInterno(), instancia.getFechaEnvio()==null?instancia.getFechaCreacion():instancia.getFechaEnvio(), EntradaRegistroActividad.TiposActividad.ENVIO_NOTIFICACION.getValue()));
            return instancia;
        }).filter((instancia) -> (instancia.getFechaVisto()!=null)).forEach((instancia) -> actividadMiembro.add(new EntradaRegistroActividad(instancia.getNotificacion().getIdNotificacion(), instancia.getNotificacion().getNombre(), instancia.getNotificacion().getNombreInterno(), instancia.getFechaVisto(), EntradaRegistroActividad.TiposActividad.ABRIO_NOTIFICACION.getValue())));
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    /**
     * obtencion de actividades de todos los miembros en todas las notificaciones
     * 
     * @param indPeriodoTiempo indicador de periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadNotificacionesTodos(String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaRegistroActividad> actividadMiembro = new ArrayList<>();
        List<InstanciaNotificacion> instancias = em.createNamedQuery("InstanciaNotificacion.findAll").getResultList();
        instancias.stream().filter((instancia) -> instancia.getFechaCreacion().after(fechaFinPeriodo) || DateUtils.isSameDay(instancia.getFechaCreacion(), fechaFinPeriodo)).forEach((instancia) -> {
            Object[] data = (Object[]) em.createNamedQuery("Miembro.getNombreApellidosByIdMiembro").setParameter("idMiembro", instancia.getInstanciaNotificacionPK().getIdMiembro()).getSingleResult();
            String nombreCompleto = ((String)data[0])+" "+((String)data[1])+(data[2]==null?"":" "+(String)data[2]);
            actividadMiembro.add(new EntradaRegistroActividad(instancia.getNotificacion().getIdNotificacion(), instancia.getNotificacion().getNombre(), instancia.getNotificacion().getNombreInterno(), instancia.getFechaEnvio()==null?instancia.getFechaCreacion():instancia.getFechaEnvio(), EntradaRegistroActividad.TiposActividad.ENVIO_NOTIFICACION.getValue(), instancia.getInstanciaNotificacionPK().getIdMiembro(), nombreCompleto));
            if (instancia.getFechaVisto()!=null) {
                actividadMiembro.add(new EntradaRegistroActividad(instancia.getNotificacion().getIdNotificacion(), instancia.getNotificacion().getNombre(), instancia.getNotificacion().getNombreInterno(), instancia.getFechaVisto(), EntradaRegistroActividad.TiposActividad.ABRIO_NOTIFICACION.getValue(), instancia.getInstanciaNotificacionPK().getIdMiembro(), nombreCompleto));
            }
        });
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    /**
     * obtencion de actividades de miembros por notificacion
     * 
     * @param idNotificacion id de notificacion
     * @param indPeriodoTiempo indicador de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadNotificacionesPorNotificacion(String idNotificacion, String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaRegistroActividad> actividadMiembro = new ArrayList<>();
        List<InstanciaNotificacion> instancias = em.createNamedQuery("InstanciaNotificacion.findAllByIdNotificacion").setParameter("idNotificacion", idNotificacion).getResultList();
        instancias.stream().filter((instancia) -> instancia.getFechaCreacion().after(fechaFinPeriodo) || DateUtils.isSameDay(instancia.getFechaCreacion(), fechaFinPeriodo)).forEach((instancia) -> {
            Object[] data = (Object[]) em.createNamedQuery("Miembro.getNombreApellidosByIdMiembro").setParameter("idMiembro", instancia.getInstanciaNotificacionPK().getIdMiembro()).getSingleResult();
            String nombreCompleto = ((String)data[0])+" "+((String)data[1])+(data[2]==null?"":" "+(String)data[2]);
            actividadMiembro.add(new EntradaRegistroActividad(instancia.getFechaEnvio()==null?instancia.getFechaCreacion():instancia.getFechaEnvio(), EntradaRegistroActividad.TiposActividad.ENVIO_NOTIFICACION.getValue(), instancia.getInstanciaNotificacionPK().getIdMiembro(), nombreCompleto));
            if (instancia.getFechaVisto()!=null) {
                actividadMiembro.add(new EntradaRegistroActividad(instancia.getFechaVisto(), EntradaRegistroActividad.TiposActividad.ABRIO_NOTIFICACION.getValue(), instancia.getInstanciaNotificacionPK().getIdMiembro(), nombreCompleto));
            }
        });
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    //********************************************************************************************PROMOCIONES********************************************************************************************
    
    /**
     * obtencion de actividades de promociones por miembro
     * 
     * @param idMiembro id de miembro
     * @param indPeriodoTiempo indicador de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadPromocionesPorMiembro(String idMiembro, String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaRegistroActividad> actividadMiembro = new ArrayList<>();
        List<MarcadorPromocion> marcadores = em.createNamedQuery("MarcadorPromocion.findAllByIdMiembro").setParameter("idMiembro", idMiembro).getResultList();
        marcadores.stream().filter((instancia) -> instancia.getFecha().after(fechaFinPeriodo) || DateUtils.isSameDay(instancia.getFecha(), fechaFinPeriodo)).forEach((marcador) -> actividadMiembro.add(new EntradaRegistroActividad(marcador.getPromocion().getIdPromocion(), marcador.getPromocion().getNombre(), marcador.getPromocion().getNombreInterno(), marcador.getFecha(), EntradaRegistroActividad.TiposActividad.MARCO_PROMOCION.getValue())));
        
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "VISTAS_PROMOCION"));
            FilterList filterList = new FilterList(new RowFilter(CompareFilter.CompareOp.EQUAL, new RegexStringComparator(".*&"+idMiembro)));
            Scan scan = new Scan();
            scan.setFilter(filterList);
            scan.setReversed(true);
            
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    Date fechaEvento = new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[0]));
                    if (fechaEvento.before(fechaFinPeriodo) && !DateUtils.isSameDay(fechaEvento, fechaFinPeriodo)) {
                        break;
                    }
                    
                    Promocion promocion = em.find(Promocion.class, Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("PROMOCION"))));
                    actividadMiembro.add(new EntradaRegistroActividad(promocion.getIdPromocion(), promocion.getNombre(), promocion.getNombreInterno(), fechaEvento, EntradaRegistroActividad.TiposActividad.VIO_PROMOCION.getValue()));
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    /**
     * obtencion de actividades de todos los miembros en promociones
     * 
     * @param indPeriodoTiempo indicador del periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadPromocionesTodos(String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaRegistroActividad> actividadMiembro = new ArrayList<>();
        List<MarcadorPromocion> marcadores = em.createNamedQuery("MarcadorPromocion.findAll").getResultList();
        marcadores.stream().filter((instancia) -> instancia.getFecha().after(fechaFinPeriodo) || DateUtils.isSameDay(instancia.getFecha(), fechaFinPeriodo)).forEach((marcador) -> actividadMiembro.add(new EntradaRegistroActividad(marcador.getPromocion().getIdPromocion(), marcador.getPromocion().getNombre(), marcador.getPromocion().getNombreInterno(), marcador.getFecha(), EntradaRegistroActividad.TiposActividad.MARCO_PROMOCION.getValue(), marcador.getMiembro().getIdMiembro(), marcador.getMiembro().getNombre()+" "+marcador.getMiembro().getApellido()+(marcador.getMiembro().getApellido2()==null?"":" "+marcador.getMiembro().getApellido2()))));
        
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "VISTAS_PROMOCION"));
            Scan scan = new Scan();
            scan.setReversed(true);
            
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    Object[] data;
                    try {
                        data = (Object[]) em.createNamedQuery("Miembro.getNombreApellidosByIdMiembro").setParameter("idMiembro", Bytes.toString(result.getRow()).split("&")[1]).getSingleResult();
                    } catch(EntityNotFoundException e) {
                        continue;
                    }
                    String nombreCompleto = ((String)data[0])+" "+((String)data[1])+(data[2]==null?"":" "+(String)data[2]);
                    Date fechaEvento = new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[0]));
                    if (fechaEvento.before(fechaFinPeriodo) && !DateUtils.isSameDay(fechaEvento, fechaFinPeriodo)) {
                        break;
                    }
                    
                    Promocion promocion = em.find(Promocion.class, Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("PROMOCION"))));
                    actividadMiembro.add(new EntradaRegistroActividad(promocion.getIdPromocion(), promocion.getNombre(), promocion.getNombreInterno(), fechaEvento, EntradaRegistroActividad.TiposActividad.VIO_PROMOCION.getValue(), Bytes.toString(result.getRow()).split("&")[1], nombreCompleto));
                }
            }
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    /**
     * obtencion de actividades de miembros sobre una promocion
     * 
     * @param idPromocion id de promocion
     * @param indPeriodoTiempo indicador del periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadPromocionesPorPromocion(String idPromocion, String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaRegistroActividad> actividadPromocion = new ArrayList<>();
        List<MarcadorPromocion> marcadores = em.createNamedQuery("MarcadorPromocion.findAllByIdPromocion").setParameter("idPromocion", idPromocion).getResultList();
        marcadores.stream()
                .filter((instancia) -> instancia.getFecha().after(fechaFinPeriodo) || DateUtils.isSameDay(instancia.getFecha(), fechaFinPeriodo))
                .forEach((marcador) -> actividadPromocion.add(new EntradaRegistroActividad(marcador.getFecha(), EntradaRegistroActividad.TiposActividad.MARCO_PROMOCION.getValue(), marcador.getMiembro().getIdMiembro(), marcador.getMiembro().getNombre()+" "+marcador.getMiembro().getApellido()+(marcador.getMiembro().getApellido2()==null?"":" "+marcador.getMiembro().getApellido2()))));
        
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "VISTAS_PROMOCION"));
            FilterList filterList = new FilterList(new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("PROMOCION"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes(idPromocion)), new KeyOnlyFilter());
            Scan scan = new Scan();
            scan.setFilter(filterList);
            scan.setReversed(true);
            
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    Date fechaEvento = new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[0]));
                    if (fechaEvento.before(fechaFinPeriodo) && !DateUtils.isSameDay(fechaEvento, fechaFinPeriodo)) {
                        break;
                    }
                    
                    Object[] data;
                    try {
                        data = (Object[]) em.createNamedQuery("Miembro.getNombreApellidosByIdMiembro").setParameter("idMiembro", Bytes.toString(result.getRow()).split("&")[1]).getSingleResult();
                    } catch(EntityNotFoundException e) {
                        continue;
                    }
                    String nombreCompleto = ((String)data[0])+" "+((String)data[1])+(data[2]==null?"":" "+(String)data[2]);
                    actividadPromocion.add(new EntradaRegistroActividad(fechaEvento, EntradaRegistroActividad.TiposActividad.VIO_PROMOCION.getValue(), Bytes.toString(result.getRow()).split("&")[1], nombreCompleto));
                }
            }
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
        
        actividadPromocion.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadPromocion;
    }
    
    //********************************************************************************************MISIONES********************************************************************************************
    
    /**
     * obtencion de actividades de un miembro en una mision
     * @param idMiembro id de miembro
     * @param indPeriodoTiempo indicador del periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadMisionesPorMiembro(String idMiembro, String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaRegistroActividad> actividadMiembro = new ArrayList<>();
        
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "VISTAS_MISION"));
            Scan scan = new Scan();
            FilterList filterList = new FilterList(new RowFilter(CompareFilter.CompareOp.EQUAL, new RegexStringComparator(".*&"+idMiembro)));
            scan.setFilter(filterList);
            scan.setReversed(true);
            
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    Date fechaEvento = new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[0]));
                    if (fechaEvento.before(fechaFinPeriodo) && !DateUtils.isSameDay(fechaEvento, fechaFinPeriodo)) {
                        break;
                    }
                    
                    Mision mision = em.find(Mision.class, Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("MISION"))));
                    actividadMiembro.add(new EntradaRegistroActividad(mision.getIdMision(), mision.getNombre(), null, fechaEvento, EntradaRegistroActividad.TiposActividad.VIO_MISION.getValue()));
                }
            }
            
            table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "REGISTRO_MISION"));
            filterList = new FilterList(new RowFilter(CompareFilter.CompareOp.EQUAL, new RegexStringComparator(".*&"+idMiembro+"&.*")));
            scan.setFilter(filterList);
            scan.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"));
            
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    Date fechaEvento = new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[2]));
                    if (fechaEvento.before(fechaFinPeriodo) && !DateUtils.isSameDay(fechaEvento, fechaFinPeriodo)) {
                        break;
                    }
                    
                    Mision mision = em.find(Mision.class, Bytes.toString(result.getRow()).split("&")[0]);
                    RespuestaMision.EstadosRespuesta estadoRespuesta = RespuestaMision.EstadosRespuesta.get(Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"))));
                    switch(estadoRespuesta!=null?estadoRespuesta:RespuestaMision.EstadosRespuesta.PENDIENTE) {
                        case GANADO: {
                            actividadMiembro.add(new EntradaRegistroActividad(mision.getIdMision(), mision.getNombre(), null, new Date(result.getColumnLatestCell(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO")).getTimestamp()), EntradaRegistroActividad.TiposActividad.APROBO_MISION.getValue()));
                            break;
                        }
                        case FALLIDO: {
                            actividadMiembro.add(new EntradaRegistroActividad(mision.getIdMision(), mision.getNombre(), null, new Date(result.getColumnLatestCell(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO")).getTimestamp()), EntradaRegistroActividad.TiposActividad.FALLO_MISION.getValue()));
                            break;
                        }
                    }
                    actividadMiembro.add(new EntradaRegistroActividad(mision.getIdMision(), mision.getNombre(), null, fechaEvento, EntradaRegistroActividad.TiposActividad.RESPONDIO_MISION.getValue()));
                }
            }
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    /**
     * obtencion de actividades de los miembros sobre misiones
     * 
     * @param indPeriodoTiempo indicador del periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadMisionesTodos(String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaRegistroActividad> actividadMiembro = new ArrayList<>();
        
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "VISTAS_MISION"));
            Scan scan = new Scan();
            scan.setReversed(true);
            
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    Object[] data;
                    try {
                        data = (Object[]) em.createNamedQuery("Miembro.getNombreApellidosByIdMiembro").setParameter("idMiembro", Bytes.toString(result.getRow()).split("&")[1]).getSingleResult();
                    } catch(EntityNotFoundException e) {
                        continue;
                    }
                    String nombreCompleto = ((String)data[0])+" "+((String)data[1])+(data[2]==null?"":" "+(String)data[2]);
                    Date fechaEvento = new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[0]));
                    if (fechaEvento.before(fechaFinPeriodo) && !DateUtils.isSameDay(fechaEvento, fechaFinPeriodo)) {
                        break;
                    }
                    
                    Mision mision = em.find(Mision.class, Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("MISION"))));
                    actividadMiembro.add(new EntradaRegistroActividad(mision.getIdMision(), mision.getNombre(), null, fechaEvento, EntradaRegistroActividad.TiposActividad.VIO_MISION.getValue(), Bytes.toString(result.getRow()).split("&")[1], nombreCompleto));
                }
            }
            
            table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "REGISTRO_MISION"));
            scan.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"));
            
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    Object[] data;
                    try {
                        data = (Object[]) em.createNamedQuery("Miembro.getNombreApellidosByIdMiembro").setParameter("idMiembro", Bytes.toString(result.getRow()).split("&")[1]).getSingleResult();
                    } catch(EntityNotFoundException e) {
                        continue;
                    }
                    String nombreCompleto = ((String)data[0])+" "+((String)data[1])+(data[2]==null?"":" "+(String)data[2]);
                    Date fechaEvento = new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[2]));
                    if (fechaEvento.before(fechaFinPeriodo) && !DateUtils.isSameDay(fechaEvento, fechaFinPeriodo)) {
                        break;
                    }
                    
                    Mision mision = em.find(Mision.class, Bytes.toString(result.getRow()).split("&")[0]);
                    RespuestaMision.EstadosRespuesta estadoRespuesta = RespuestaMision.EstadosRespuesta.get(Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"))));
                    switch(estadoRespuesta!=null?estadoRespuesta:RespuestaMision.EstadosRespuesta.PENDIENTE) {
                        case GANADO: {
                            actividadMiembro.add(new EntradaRegistroActividad(mision.getIdMision(), mision.getNombre(), null, new Date(result.getColumnLatestCell(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO")).getTimestamp()), EntradaRegistroActividad.TiposActividad.APROBO_MISION.getValue(), Bytes.toString(result.getRow()).split("&")[1], nombreCompleto));
                            break;
                        }
                        case FALLIDO: {
                            actividadMiembro.add(new EntradaRegistroActividad(mision.getIdMision(), mision.getNombre(), null, new Date(result.getColumnLatestCell(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO")).getTimestamp()), EntradaRegistroActividad.TiposActividad.FALLO_MISION.getValue(), Bytes.toString(result.getRow()).split("&")[1], nombreCompleto));
                            break;
                        }
                    }
                    actividadMiembro.add(new EntradaRegistroActividad(mision.getIdMision(), mision.getNombre(), null, fechaEvento, EntradaRegistroActividad.TiposActividad.RESPONDIO_MISION.getValue(), Bytes.toString(result.getRow()).split("&")[1], nombreCompleto));
                }
            }
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    /**
     * obtencion de actividades de miembros en una mision
     * 
     * @param idMision id de mision
     * @param indPeriodoTiempo indicador del periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadMisionesPorMision(String idMision, String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaRegistroActividad> actividadMiembro = new ArrayList<>();
        
        try (Connection connection = ConnectionFactory.createConnection(configuration)) {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "VISTAS_MISION"));
            Scan scan = new Scan();
            FilterList filterList = new FilterList(new SingleColumnValueFilter(Bytes.toBytes("DATA"), Bytes.toBytes("MISION"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes(idMision)), new KeyOnlyFilter());
            scan.setFilter(filterList);
            scan.setReversed(true);
            
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    Date fechaEvento = new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[0]));
                    if (fechaEvento.before(fechaFinPeriodo) && !DateUtils.isSameDay(fechaEvento, fechaFinPeriodo)) {
                        break;
                    }
                    
                    Object[] data;
                    try {
                        data = (Object[]) em.createNamedQuery("Miembro.getNombreApellidosByIdMiembro").setParameter("idMiembro", Bytes.toString(result.getRow()).split("&")[1]).getSingleResult();
                    } catch(EntityNotFoundException e) {
                        continue;
                    }
                    String nombreCompleto = ((String)data[0])+" "+((String)data[1])+(data[2]==null?"":" "+(String)data[2]);
                    actividadMiembro.add(new EntradaRegistroActividad(fechaEvento, EntradaRegistroActividad.TiposActividad.VIO_MISION.getValue(), Bytes.toString(result.getRow()).split("&")[1], nombreCompleto));
                }
            }
            
            table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "REGISTRO_MISION"));
            filterList = new FilterList(new RowFilter(CompareFilter.CompareOp.EQUAL, new RegexStringComparator(idMision+".*")));
            scan.setFilter(filterList);
            scan.addColumn(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"));
            
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    Date fechaEvento = new Date(Long.parseLong(Bytes.toString(result.getRow()).split("&")[2]));
                    if (fechaEvento.before(fechaFinPeriodo) && !DateUtils.isSameDay(fechaEvento, fechaFinPeriodo)) {
                        break;
                    }
                    
                    Object[] data;
                    try {
                        data = (Object[]) em.createNamedQuery("Miembro.getNombreApellidosByIdMiembro").setParameter("idMiembro", Bytes.toString(result.getRow()).split("&")[1]).getSingleResult();
                    } catch(EntityNotFoundException e) {
                        continue;
                    }
                    String nombreCompleto = ((String)data[0])+" "+((String)data[1])+(data[2]==null?"":" "+(String)data[2]);
                    
                    RespuestaMision.EstadosRespuesta estadoRespuesta = RespuestaMision.EstadosRespuesta.get(Bytes.toString(result.getValue(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO"))));
                    switch(estadoRespuesta!=null?estadoRespuesta:RespuestaMision.EstadosRespuesta.PENDIENTE) {
                        case GANADO: {
                            actividadMiembro.add(new EntradaRegistroActividad(new Date(result.getColumnLatestCell(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO")).getTimestamp()), EntradaRegistroActividad.TiposActividad.APROBO_MISION.getValue(), Bytes.toString(result.getRow()).split("&")[1], nombreCompleto));
                            break;
                        }
                        case FALLIDO: {
                            actividadMiembro.add(new EntradaRegistroActividad(new Date(result.getColumnLatestCell(Bytes.toBytes("DATA"), Bytes.toBytes("ESTADO")).getTimestamp()), EntradaRegistroActividad.TiposActividad.FALLO_MISION.getValue(), Bytes.toString(result.getRow()).split("&")[1], nombreCompleto));
                            break;
                        }
                    }
                    actividadMiembro.add(new EntradaRegistroActividad(fechaEvento, EntradaRegistroActividad.TiposActividad.RESPONDIO_MISION.getValue(), Bytes.toString(result.getRow()).split("&")[1], nombreCompleto));
                }
            }
        } catch (IOException e) {
            throw new MyException(MyException.ErrorSistema.PROBLEMAS_HBASE, locale, "database_connection_failed");
        }
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    //********************************************************************************************METRICA********************************************************************************************
    
    /**
     * obtencion de actividades de metrica por miembro
     * 
     * @param idMiembro id de miembro
     * @param indPeriodoTiempo indicador del periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadMetricaPorMiembro(String idMiembro, String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<RegistroMetrica> registroMetricas = em.createNamedQuery("RegistroMetrica.findByIdMiembroUntilFecha")
                .setParameter("idMiembro", idMiembro)
                .setParameter("fecha", fechaFinPeriodo)
                .getResultList();
        
        List<EntradaRegistroActividad> actividadMiembro = registroMetricas
                .stream()
                .map(r -> {
                    Metrica metrica = em.find(Metrica.class, r.getIdMetrica());
                    EntradaRegistroActividad entrada = new EntradaRegistroActividad(metrica.getIdMetrica(), r.getCantidad()+" "+metrica.getNombre(), metrica.getNombreInterno(), r.getRegistroMetricaPK().getFecha(), EntradaRegistroActividad.TiposActividad.GANO_METRICA.getValue());
                    entrada.setIndTipoGane(r.getIndTipoGane());
                    return entrada;
                }).collect(Collectors.toList());
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    /**
     * obtencion de actividades de miembros en metricas
     * 
     * @param indPeriodoTiempo indicador del periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadMetricaTodos(String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<RegistroMetrica> registroMetricas = em.createNamedQuery("RegistroMetrica.findAllUntilFecha")
                .setParameter("fecha", fechaFinPeriodo)
                .getResultList();
        
        List<EntradaRegistroActividad> actividadMiembro = registroMetricas
                .stream()
                .map(r -> {
                    Object[] data = (Object[]) em.createNamedQuery("Miembro.getNombreApellidosByIdMiembro").setParameter("idMiembro", r.getIdMiembro()).getSingleResult();
                    String nombreCompleto = ((String)data[0])+" "+((String)data[1])+(data[2]==null?"":" "+(String)data[2]);
                    
                    Metrica metrica = em.find(Metrica.class, r.getIdMetrica());
                    EntradaRegistroActividad entrada = new EntradaRegistroActividad(metrica.getIdMetrica(), r.getCantidad()+" "+metrica.getNombre(), metrica.getNombreInterno(), r.getRegistroMetricaPK().getFecha(), EntradaRegistroActividad.TiposActividad.GANO_METRICA.getValue(), r.getIdMiembro(), nombreCompleto);
                    entrada.setIndTipoGane(r.getIndTipoGane());
                    return entrada;
                }).collect(Collectors.toList());
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    /**
     * obtencion de actividades de miembros en una metrica
     * 
     * @param idMetrica id de metrica
     * @param indPeriodoTiempo indicador del periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadMetricaPorMetrica(String idMetrica, String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<RegistroMetrica> registroMetricas = em.createNamedQuery("RegistroMetrica.findByIdMetricaUntilFecha")
                .setParameter("idMetrica", idMetrica)
                .setParameter("fecha", fechaFinPeriodo)
                .getResultList();
        
        List<EntradaRegistroActividad> actividadMiembro = registroMetricas
                .stream()
                .map(r -> {
                    Object[] data = (Object[]) em.createNamedQuery("Miembro.getNombreApellidosByIdMiembro").setParameter("idMiembro", r.getIdMiembro()).getSingleResult();
                    String nombreCompleto = ((String)data[0])+" "+((String)data[1])+(data[2]==null?"":" "+(String)data[2]);
                    
                    EntradaRegistroActividad entrada = new EntradaRegistroActividad(r.getRegistroMetricaPK().getFecha(), EntradaRegistroActividad.TiposActividad.GANO_METRICA.getValue(), r.getIdMiembro(), nombreCompleto);
                    entrada.setIndTipoGane(r.getIndTipoGane());
                    return entrada;
                }).collect(Collectors.toList());
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    //********************************************************************************************PREMIOS/RECOMPENSAS********************************************************************************************
    
    /**
     * obtencion de actividades de premios por miembro
     * 
     * @param idMiembro id de miembro
     * @param indPeriodoTiempo indicador del periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadPremiosRecompensasPorMiembro(String idMiembro, String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaRegistroActividad> actividadMiembro = new ArrayList<>();
        
        List<PremioMiembro> premios = em.createNamedQuery("PremioMiembro.findByIdMiembro").setParameter("idMiembro", idMiembro).getResultList();
        premios.stream().filter((premio) -> premio.getFechaAsignacion().after(fechaFinPeriodo) || DateUtils.isSameDay(premio.getFechaAsignacion(), fechaFinPeriodo)).forEach((premio) -> {
            switch(PremioMiembro.Motivos.get(premio.getIndMotivoAsignacion())) {
                case AWARD: {
                    actividadMiembro.add(new EntradaRegistroActividad(premio.getIdPremio().getIdPremio(), premio.getIdPremio().getNombre(), premio.getIdPremio().getNombreInterno(), premio.getFechaAsignacion(), EntradaRegistroActividad.TiposActividad.RECIBIO_PREMIO.getValue()));
                    break;
                }
                case PREMIO: {
                    actividadMiembro.add(new EntradaRegistroActividad(premio.getIdPremio().getIdPremio(), premio.getIdPremio().getNombre(), premio.getIdPremio().getNombreInterno(), premio.getFechaAsignacion(), EntradaRegistroActividad.TiposActividad.REDIMIO_PREMIO.getValue()));
                    break;
                }
                case PREMIO_MISION: {
                    actividadMiembro.add(new EntradaRegistroActividad(premio.getIdPremio().getIdPremio(), premio.getIdPremio().getNombre(), premio.getIdPremio().getNombreInterno(), premio.getFechaAsignacion(), EntradaRegistroActividad.TiposActividad.APROBO_MISION.getValue()));
                    break;
                }
                case REWARD: {
                    actividadMiembro.add(new EntradaRegistroActividad(premio.getIdPremio().getIdPremio(), premio.getIdPremio().getNombre(), premio.getIdPremio().getNombreInterno(), premio.getFechaAsignacion(), EntradaRegistroActividad.TiposActividad.RECIBIO_PREMIO.getValue()));
                    break;
                }
            }
        });
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    /**
     * obtencion de actividades de miembros en premios
     * 
     * @param indPeriodoTiempo indicador del periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadPremiosRecompensasTodos(String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaRegistroActividad> actividadMiembro = new ArrayList<>();
        
        List<PremioMiembro> premios = em.createNamedQuery("PremioMiembro.findAll").getResultList();
        premios.stream().filter((premio) -> premio.getFechaAsignacion().after(fechaFinPeriodo) || DateUtils.isSameDay(premio.getFechaAsignacion(), fechaFinPeriodo)).forEach((premio) -> {
            switch(PremioMiembro.Motivos.get(premio.getIndMotivoAsignacion())) {
                case AWARD: {
                    actividadMiembro.add(new EntradaRegistroActividad(premio.getIdPremio().getIdPremio(), premio.getIdPremio().getNombre(), premio.getIdPremio().getNombreInterno(), premio.getFechaAsignacion(), EntradaRegistroActividad.TiposActividad.RECIBIO_PREMIO.getValue(),premio.getIdMiembro().getIdMiembro(), premio.getIdMiembro().getNombre()+" "+premio.getIdMiembro().getApellido()+(premio.getIdMiembro().getApellido2()==null?"":" "+premio.getIdMiembro().getApellido2())));
                    break;
                }
                case PREMIO: {
                    actividadMiembro.add(new EntradaRegistroActividad(premio.getIdPremio().getIdPremio(), premio.getIdPremio().getNombre(), premio.getIdPremio().getNombreInterno(), premio.getFechaAsignacion(), EntradaRegistroActividad.TiposActividad.REDIMIO_PREMIO.getValue(),premio.getIdMiembro().getIdMiembro(), premio.getIdMiembro().getNombre()+" "+premio.getIdMiembro().getApellido()+(premio.getIdMiembro().getApellido2()==null?"":" "+premio.getIdMiembro().getApellido2())));
                    break;
                }
                case PREMIO_MISION: {
                    actividadMiembro.add(new EntradaRegistroActividad(premio.getIdPremio().getIdPremio(), premio.getIdPremio().getNombre(), premio.getIdPremio().getNombreInterno(), premio.getFechaAsignacion(), EntradaRegistroActividad.TiposActividad.APROBO_MISION.getValue(),premio.getIdMiembro().getIdMiembro(), premio.getIdMiembro().getNombre()+" "+premio.getIdMiembro().getApellido()+(premio.getIdMiembro().getApellido2()==null?"":" "+premio.getIdMiembro().getApellido2())));
                    break;
                }
                case REWARD: {
                    actividadMiembro.add(new EntradaRegistroActividad(premio.getIdPremio().getIdPremio(), premio.getIdPremio().getNombre(), premio.getIdPremio().getNombreInterno(), premio.getFechaAsignacion(), EntradaRegistroActividad.TiposActividad.RECIBIO_PREMIO.getValue(),premio.getIdMiembro().getIdMiembro(), premio.getIdMiembro().getNombre()+" "+premio.getIdMiembro().getApellido()+(premio.getIdMiembro().getApellido2()==null?"":" "+premio.getIdMiembro().getApellido2())));
                    break;
                }
            }
        });
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
    
    /**
     * obtencion de actividades de miembro en un premio
     * 
     * @param idPremio id de premio
     * @param indPeriodoTiempo indicador del periodo de tiempo
     * @param locale locale
     * @return lista de actividades
     */
    public List<EntradaRegistroActividad> getActividadPremiosRecompensasPorPremio(String idPremio, String indPeriodoTiempo, Locale locale) {
        Date fechaFinPeriodo = getFechaFinPeriodo(indPeriodoTiempo);
        
        List<EntradaRegistroActividad> actividadMiembro = new ArrayList<>();
        
        List<PremioMiembro> premios = em.createNamedQuery("PremioMiembro.findByIdPremio").setParameter("idPremio", idPremio).getResultList();
        premios.stream().filter((premio) -> premio.getFechaAsignacion().after(fechaFinPeriodo) || DateUtils.isSameDay(premio.getFechaAsignacion(), fechaFinPeriodo)).forEach((premio) -> {
            switch(PremioMiembro.Motivos.get(premio.getIndMotivoAsignacion())) {
                case AWARD: {
                    actividadMiembro.add(new EntradaRegistroActividad(premio.getFechaAsignacion(), EntradaRegistroActividad.TiposActividad.RECIBIO_PREMIO.getValue(), premio.getIdMiembro().getIdMiembro(), premio.getIdMiembro().getNombre()+" "+premio.getIdMiembro().getApellido()+(premio.getIdMiembro().getApellido2()==null?"":" "+premio.getIdMiembro().getApellido2())));
                    break;
                }
                case PREMIO: {
                    actividadMiembro.add(new EntradaRegistroActividad(premio.getFechaAsignacion(), EntradaRegistroActividad.TiposActividad.REDIMIO_PREMIO.getValue(), premio.getIdMiembro().getIdMiembro(), premio.getIdMiembro().getNombre()+" "+premio.getIdMiembro().getApellido()+(premio.getIdMiembro().getApellido2()==null?"":" "+premio.getIdMiembro().getApellido2())));
                    break;
                }
                case PREMIO_MISION: {
                    actividadMiembro.add(new EntradaRegistroActividad(premio.getIdPremio().getIdPremio(), premio.getIdPremio().getNombre(), premio.getIdPremio().getNombreInterno(), premio.getFechaAsignacion(), EntradaRegistroActividad.TiposActividad.APROBO_MISION.getValue(),premio.getIdMiembro().getIdMiembro(), premio.getIdMiembro().getNombre()+" "+premio.getIdMiembro().getApellido()+(premio.getIdMiembro().getApellido2()==null?"":" "+premio.getIdMiembro().getApellido2())));
                    break;
                }
                case REWARD: {
                    actividadMiembro.add(new EntradaRegistroActividad(premio.getIdPremio().getIdPremio(), premio.getIdPremio().getNombre(), premio.getIdPremio().getNombreInterno(), premio.getFechaAsignacion(), EntradaRegistroActividad.TiposActividad.RECIBIO_PREMIO.getValue(),premio.getIdMiembro().getIdMiembro(), premio.getIdMiembro().getNombre()+" "+premio.getIdMiembro().getApellido()+(premio.getIdMiembro().getApellido2()==null?"":" "+premio.getIdMiembro().getApellido2())));
                    break;
                }
            }
        });
        
        actividadMiembro.sort((o1, o2) -> Long.compare(o2.getFecha().getTime(), o1.getFecha().getTime()));
        
        return actividadMiembro;
    }
}
