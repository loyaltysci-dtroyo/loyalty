package com.flecharoja.loyalty.model;

/**
 *
 * @author svargas
 */
public class RespuestaRegistroComentario {
    
    private String idComentario;
    
    private long cantComentarios;

    public RespuestaRegistroComentario() {
    }

    public RespuestaRegistroComentario(String idComentario, long cantComentarios) {
        this.idComentario = idComentario;
        this.cantComentarios = cantComentarios;
    }

    public String getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(String idComentario) {
        this.idComentario = idComentario;
    }

    public long getCantComentarios() {
        return cantComentarios;
    }

    public void setCantComentarios(long cantComentarios) {
        this.cantComentarios = cantComentarios;
    }
}
