package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Promocion;
import com.flecharoja.loyalty.model.PromocionListaMiembro;
import com.flecharoja.loyalty.model.PromocionListaMiembroPK;
import com.flecharoja.loyalty.util.Busquedas;
import com.flecharoja.loyalty.util.Indicadores;
import com.flecharoja.loyalty.model.Bitacora;
import com.flecharoja.loyalty.util.MyKeycloakUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * EJB encargado de el manejo de asignaciones de miembros con promociones
 *
 * @author svargas
 */
@Stateless
public class PromocionListaMiembroBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    BitacoraBean bitacoraBean;//EJB con los metodos de negocio para el manejo de la bitacora
    
    /**
     * obtencion de miembros disponibles/incluidos/exlcuidos de una promocion
     * 
     * @param idPromocion id de promocion
     * @param indAccion indicador sobre tipo de lista
     * @param estados filtro de miembro
     * @param generos filtro de miembro
     * @param estadosCivil filtro de miembro
     * @param educacion filtro de miembro
     * @param contactoEmail filtro de miembro
     * @param contactoSMS filtro de miembro
     * @param contactoNotificacion filtro de miembro
     * @param contactoEstado filtro de miembro
     * @param tieneHijos filtro de miembro
     * @param busqueda filtro de miembro
     * @param filtros filtro de miembro
     * @param cantidad paginacion
     * @param registro paginacion
     * @param ordenTipo tipo de orden en lista
     * @param ordenCampo campo por donde ordenar lista
     * @param locale locale
     * @return lista de miembros
     */
    public Map<String, Object> getMiembros(String idPromocion,  String indAccion, List<String> estados, List<String> generos, List<String> estadosCivil, List<String> educacion, String contactoEmail, String contactoSMS, String contactoNotificacion, String contactoEstado, String tieneHijos, String busqueda, List<String> filtros, int cantidad, int registro, String ordenTipo, String ordenCampo, Locale locale) {
        //verificacion que el rango de registros en la peticion, este dentro de lo valido
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Miembro> root = query.from(Miembro.class);
        
        //formacion de query de busqueda
        query = Busquedas.getCriteriaQueryMiembros(cb, query, root, estados, generos, estadosCivil, educacion, contactoEmail, contactoSMS, contactoNotificacion, contactoEstado, tieneHijos, busqueda, filtros, ordenTipo, ordenCampo);
        
        //sub query para el filtro de miembros segun el tipo de lista
        Subquery<String> subquery = query.subquery(String.class);
        Root<PromocionListaMiembro> rootSubquery = subquery.from(PromocionListaMiembro.class);
        subquery.select(rootSubquery.get("promocionListaMiembroPK").get("idMiembro"));
        if (indAccion==null) {
            subquery.where(cb.equal(rootSubquery.get("promocionListaMiembroPK").get("idPromocion"), idPromocion));
            if (query.getRestriction()!=null) {
                query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            } else {
                query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
            }
        } else {
            switch(indAccion.toUpperCase().charAt(0)) {
                case Indicadores.INCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("promocionListaMiembroPK").get("idPromocion"), idPromocion), cb.equal(rootSubquery.get("indTipo"), Indicadores.INCLUIDO));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.EXCLUIDO: {
                    subquery.where(cb.equal(rootSubquery.get("promocionListaMiembroPK").get("idPromocion"), idPromocion), cb.equal(rootSubquery.get("indTipo"), Indicadores.EXCLUIDO));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    } else {
                        query.where(cb.or(cb.in(root.get("idMiembro")).value(subquery)));
                    }
                    break;
                }
                case Indicadores.DISPONIBLES: {
                    subquery.where(cb.equal(rootSubquery.get("promocionListaMiembroPK").get("idPromocion"), idPromocion));
                    if (query.getRestriction()!=null) {
                        query.where(query.getRestriction(), cb.equal(root.get("indEstadoMiembro"), Miembro.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                    } else {
                        query.where(cb.equal(root.get("indEstadoMiembro"), Miembro.Estados.ACTIVO.getValue()), cb.or(cb.in(root.get("idMiembro")).value(subquery).not()));
                    }
                    break;
                }
                default: {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "arguments_invalid", "indAccion");
                }
            }
        }
        
        //se obtiene le total y se reduce el numero de registro inicial de ser necesario
        long total = (long) em.createQuery(query.select(cb.count(root))).getSingleResult();
        total -= total-1<0?0:1;
        while (registro > total) {
            registro = registro - cantidad > 0 ? registro - cantidad : 0;
        }
        
        //se obtiene el listado de restricciones sobre el rango deseado
        List resultado;
        try {
            resultado = em.createQuery(query.select(root))
                    .setFirstResult(registro)
                    .setMaxResults(cantidad)
                    .getResultList();
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
        }
        
        //recurrido del resultado para la asignacion de informacion adicional (almacenada en keycloak)
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            resultado.stream().forEach((t) -> {
                Miembro miembro = (Miembro) t;
                try {
                    miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
                } catch(Exception e2) {
                    miembro.setNombreUsuario("Prueba");
                }
            });
        }
        
        //se regresa la respuesta con el rango y resultado
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("rango", registro+"-"+(registro+cantidad-1)+"/"+total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }

    /**
     * Metodo que asigna (incluye o excluye) a un miembro de una promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param idMiembro Identificador del miembro
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesion
     * @param locale
     */
    public void includeExcludeMiembro(String idPromocion, String idMiembro, Character indAccion, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        try {
            em.getReference(Miembro.class, idMiembro).getIdMiembro();
        } catch (EntityNotFoundException e) {
             throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        //verificacion de entidad archivada
        if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Promo");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            PromocionListaMiembro promocionListaMiembro = new PromocionListaMiembro(idMiembro, idPromocion, indAccion, date, usuario);

            em.merge(promocionListaMiembro);
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        bitacoraBean.logAccion(PromocionListaMiembro.class, idPromocion + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
    }

    /**
     * Metodo que revoca la asignacion de un miembro a una promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param idMiembro Identificador del miembro
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeMiembro(String idPromocion, String idMiembro, String usuario, Locale locale) {
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }
        try {
            em.getReference(Miembro.class, idMiembro).getIdMiembro();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        //verificacion de entidad archivada
        if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Promo");
        }

        try {
            //verificacion que la entidad exista y su eliminacion
            em.remove(em.getReference(PromocionListaMiembro.class, new PromocionListaMiembroPK(idMiembro, idPromocion)));
            em.flush();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_member_not_found");
        }

        bitacoraBean.logAccion(PromocionListaMiembro.class, idPromocion + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
    }

    /**
     * Metodo que asigna (incluye o excluye) una lista de miembros de una
     * promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param listaIdMiembro Lista de identificadores de miembros
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesión
     * @param locale
     */
    public void includeExcludeBatchMiembro(String idPromocion, List<String> listaIdMiembro, Character indAccion, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
             throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }

        listaIdMiembro = listaIdMiembro.stream().distinct().collect(Collectors.toList());

        try {
            listaIdMiembro.stream().forEach((idMiembro) -> {
                em.getReference(Miembro.class, idMiembro).getIdMiembro();
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        //verificacion de entidad archivada
        if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Promo");
        }

        Date date = Calendar.getInstance().getTime();

        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea uno valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            for (String idMiembro : listaIdMiembro) {
                em.merge(new PromocionListaMiembro(idMiembro, idPromocion, indAccion, date, usuario));
            }
            em.flush();
        } else {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_ERRONEOS, locale, "attributes_invalid", "indAccion");
        }

        for (String idMiembro : listaIdMiembro) {
            bitacoraBean.logAccion(PromocionListaMiembro.class, idPromocion + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ACTUALIZA.getValue(),locale);
        }
    }

    /**
     * Metodo que revoca la asignacion de una lista de miembros a una promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param listaIdMiembro Lista de identificadores de miembros
     * @param usuario usuario en sesion
     * @param locale
     */
    public void removeBatchMiembro(String idPromocion, List<String> listaIdMiembro, String usuario, Locale locale) {
        //verificacion que los id's sean validos
        Promocion promocion = em.find(Promocion.class, idPromocion);
        if (promocion == null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_not_found");
        }

        listaIdMiembro = listaIdMiembro.stream().distinct().collect(Collectors.toList());

        try {
            listaIdMiembro.stream().forEach((idMiembro) -> {
                em.getReference(Miembro.class, idMiembro).getIdMiembro();
            });
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "member_not_found");
        }

        //verificacion de entidad archivada
        if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "operation_over_archived", "Promo");
        }

        try {
            listaIdMiembro.stream().forEach((idMiembro) -> {
                //verificacion que la entidad exista y su eliminacion
                em.remove(em.getReference(PromocionListaMiembro.class, new PromocionListaMiembroPK(idMiembro, idPromocion)));
            });
            em.flush();
        } catch (EntityNotFoundException e) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "promo_member_not_found");
        }

        for (String idMiembro : listaIdMiembro) {
            bitacoraBean.logAccion(PromocionListaMiembro.class, idPromocion + "/" + idMiembro, usuario, Bitacora.Accion.ENTIDAD_ELIMINADA.getValue(),locale);
        }
    }

    /**
     * Metodo que obtiene un listado de los miembros excluidos e incluidos o
     * ambos filtrados adicionamente por palabras claves
     *
     * @param idPromocion Identificador de promocion
     * @param indTipo Indicador del tipo de miembros incluidos/excluidos
     * @param registro Numero de registro inicial
     * @param cantidad Cantidad de registros
     * @param busqueda Cadena de texto con palabras claves
     * @param locale
     * @return Respuesta con una lista de miembros en un rango de resultados
     */
    public Map<String, Object> searchMiembros(String idPromocion, Character indTipo, int registro, int cantidad, String busqueda, Locale locale) {
        //verificacion que el rango de registros solicitados esten dentro de lo aceptable
        if (cantidad > Indicadores.PAGINACION_RANGO_ACEPTADO_VALOR || cantidad <= 0) {
            throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "cantidad");
        }
        
        Map<String, Object> respuesta = new HashMap<>();

        List<Miembro> resultado;
        Long total;
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        String[] terminos = busqueda.split(" ");
        
        if (indTipo==null) {
            Root<PromocionListaMiembro> root = query.from(PromocionListaMiembro.class);
            
            List<Predicate> predicate0 = new ArrayList<>();
            for (String termino : terminos) {
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("nombre")), "%" + termino.toLowerCase() + "%"));
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("apellido")), "%" + termino.toLowerCase() + "%"));
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("apellido2")), "%" + termino.toLowerCase() + "%"));
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("docIdentificacion")), "%" + termino.toLowerCase() + "%"));
            }
            
            query.where(cb.or(predicate0.toArray(new Predicate[predicate0.size()])),
                    cb.and(cb.equal(root.get("segmento").get("idSegmento"), idPromocion)));
            
            //obtencion del total
            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;
            while (registro > total) {
                registro = registro - cantidad > 0 ? registro - cantidad : 0;
            }
            
            try {
                resultado = em.createQuery(query.select(root.get("miembro")))
                        .setFirstResult(registro)
                        .setMaxResults(cantidad)
                        .getResultList()
                        .stream().map((Object t) -> (Miembro) t).collect(Collectors.toList());
            } catch (IllegalArgumentException e) {
                throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
            }
        } else {
            if (indTipo.equals(Indicadores.DISPONIBLES)) {
                Root<Miembro> root1 = query.from(Miembro.class);

                Subquery<Miembro> subquery = query.subquery(Miembro.class);
                Root<PromocionListaMiembro> root2 = subquery.from(PromocionListaMiembro.class);
                
                List<Predicate> predicate0 = new ArrayList<>();
                for (String termino : terminos) {
                    predicate0.add(cb.like(cb.lower(root1.get("nombre")), "%" + termino.toLowerCase() + "%"));
                    predicate0.add(cb.like(cb.lower(root1.get("apellido")), "%" + termino.toLowerCase() + "%"));
                    predicate0.add(cb.like(cb.lower(root1.get("apellido2")), "%" + termino.toLowerCase() + "%"));
                    predicate0.add(cb.like(cb.lower(root1.get("docIdentificacion")), "%" + termino.toLowerCase() + "%"));
                }

                Predicate predicate1 = cb.not(root1.in(subquery.select(root2.get("miembro")).where(cb.equal(root2.get("segmento").get("idSegmento"), idPromocion))));

                query.where(cb.or(predicate0.toArray(new Predicate[predicate0.size()])), cb.and(predicate1));

                //obtencion del total
                total = (Long) em.createQuery(query.select(cb.count(root1))).getSingleResult();
                total -= total - 1 < 0 ? 0 : 1;
                while (registro > total) {
                    registro = registro - cantidad > 0 ? registro - cantidad : 0;
                }
                try {
                    resultado = em.createQuery(query.select(root1))
                            .setFirstResult(registro)
                            .setMaxResults(cantidad)
                            .getResultList()
                            .stream().map((Object t) -> (Miembro) t).collect(Collectors.toList());
                } catch (IllegalArgumentException e) {
                    throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
                }
            } else {
                Root<PromocionListaMiembro> root = query.from(PromocionListaMiembro.class);

                List<Predicate> predicate0 = new ArrayList<>();
                for (String termino : terminos) {
                    predicate0.add(cb.like(cb.lower(root.get("miembro").get("nombre")), "%" + termino.toLowerCase() + "%"));
                    predicate0.add(cb.like(cb.lower(root.get("miembro").get("apellido")), "%" + termino.toLowerCase() + "%"));
                    predicate0.add(cb.like(cb.lower(root.get("miembro").get("apellido2")), "%" + termino.toLowerCase() + "%"));
                    predicate0.add(cb.like(cb.lower(root.get("miembro").get("docIdentificacion")), "%" + termino.toLowerCase() + "%"));
                }

                query.where(cb.or(predicate0.toArray(new Predicate[predicate0.size()])),
                        cb.and(cb.equal(root.get("segmento").get("idSegmento"), idPromocion)),
                        cb.and(cb.equal(root.get("indTipo"), indTipo)));
                
                //obtencion del total
                total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
                total -= total - 1 < 0 ? 0 : 1;
                while (registro > total) {
                    registro = registro - cantidad > 0 ? registro - cantidad : 0;
                }

                try {
                    resultado = em.createQuery(query.select(root.get("miembro")))
                            .setFirstResult(registro)
                            .setMaxResults(cantidad)
                            .getResultList()
                            .stream().map((Object t) -> (Miembro) t).collect(Collectors.toList());
                } catch (IllegalArgumentException e) {
                   throw new MyException(MyException.ErrorPeticion.ARGUMENTOS_PAGINACION_ERRONEOS,locale, "pagination_value_invalid", "registro");
                }
            }
        }
        
        //recorrido de la listas de usuarios para poder establecer el los demas atributos de keycloak
        try (MyKeycloakUtils keycloakUtils = new MyKeycloakUtils(locale)) {
            resultado.stream().forEach((miembro) -> {
                try {
                    miembro.setNombreUsuario(keycloakUtils.getUsernameMember(miembro.getIdMiembro()));
                } catch(Exception e2) {
                    miembro.setNombreUsuario("Prueba");
                }
            });
        }
        
        respuesta.put("rango", registro + "-" + (registro + cantidad - 1) + "/" + total);
        respuesta.put("resultado", resultado);
        
        return respuesta;
    }
}
