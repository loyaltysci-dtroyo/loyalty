package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.model.RegistroMetrica;
import com.flecharoja.loyalty.service.RegistroMetricaService;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author svargas
 */
@Path("registrar-metrica")
public class MetricaResource {

    @EJB
    RegistroMetricaService registroMetricaService;

    @Context
    HttpServletRequest request;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void createRegistroMetrica(RegistroMetrica registroMetrica) {
        registroMetricaService.createRegistroMetrica(registroMetrica, request.getLocale());
    }
}
