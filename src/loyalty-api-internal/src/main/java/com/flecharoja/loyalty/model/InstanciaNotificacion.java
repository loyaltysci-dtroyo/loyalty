package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "INSTANCIA_NOTIFICACION")
@IdClass(InstanciaNotificacionPK.class)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InstanciaNotificacion.countInstanciasByIdInstancia", query = "SELECT COUNT(i) FROM InstanciaNotificacion i WHERE i.idInstancia = :idInstancia"),
    @NamedQuery(name = "InstanciaNotificacion.groupIdInstancias", query = "SELECT i.idInstancia FROM InstanciaNotificacion i GROUP BY i.idInstancia"),
    @NamedQuery(name = "InstanciaNotificacion.findInstanciaByIdInstanciaIdMiembro", query = "SELECT i FROM InstanciaNotificacion i WHERE i.idInstancia = :idInstancia AND i.idMiembro = :idMiembro")
})
public class InstanciaNotificacion implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "ID_INSTANCIA")
    private String idInstancia;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "ID_NOTIFICACION")
    private String idNotificacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Column(name = "FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    
    @Column(name = "TEXTO")
    private String texto;

    public InstanciaNotificacion() {
    }
    
    public InstanciaNotificacion(String idMiembro, String idInstancia, String idNotificacion, Date fechaCreacion, String texto) {
        this.idNotificacion = idNotificacion;
        this.idInstancia = idInstancia;
        this.idMiembro = idMiembro;
        this.fechaCreacion = fechaCreacion;
        this.texto = texto;
    }
    
    public String getIdInstancia() {
        return idInstancia;
    }

    public void setIdInstancia(String idInstancia) {
        this.idInstancia = idInstancia;
    }

    public String getIdMiembro() {
        return idMiembro;
    }
    
    public void setIdMiembro(String idMiembro) {    
        this.idMiembro = idMiembro;
    }

    public String getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(String idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInstancia != null ? idInstancia.hashCode() : 0);
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstanciaNotificacion)) {
            return false;
        }
        InstanciaNotificacion other = (InstanciaNotificacion) object;
        if ((this.idInstancia == null && other.idInstancia != null) || (this.idInstancia != null && !this.idInstancia.equals(other.idInstancia))) {
            return false;
        }
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.InstanciaNotificacion[ idInstancia=" + idInstancia + ", idMiembro=" + idMiembro + " ]";
    }
    
}
