package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NOTIFICACION_LISTA_SEGMENTO")
@IdClass(NotificacionListaSegmentoPK.class)
@NamedQueries({
    @NamedQuery(name = "NotificacionListaSegmento.getIdSegmentosByIdNotificacion", query = "SELECT l.idSegmento FROM NotificacionListaSegmento l WHERE l.idNotificacion = :idNotificacion")
})
public class NotificacionListaSegmento implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_NOTIFICACION")
    private String idNotificacion;
    
    @Id
    @Column(name = "ID_SEGMENTO")
    private String idSegmento;
    
    public NotificacionListaSegmento() {
    }

    public String getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(String idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public String getIdSegmento() {
        return idSegmento;
    }

    public void setIdSegmento(String idSegmento) {
        this.idSegmento = idSegmento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotificacion != null ? idNotificacion.hashCode() : 0);
        hash += (idSegmento != null ? idSegmento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificacionListaSegmento)) {
            return false;
        }
        NotificacionListaSegmento other = (NotificacionListaSegmento) object;
        if ((this.idNotificacion == null && other.idNotificacion != null) || (this.idNotificacion != null && !this.idNotificacion.equals(other.idNotificacion))) {
            return false;
        }
        if ((this.idSegmento == null && other.idSegmento != null) || (this.idSegmento != null && !this.idSegmento.equals(other.idSegmento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.NotificacionListaSegmento[ idNotificacion=" + idNotificacion + ", idSegmento=" + idSegmento + " ]";
    }
    
}
