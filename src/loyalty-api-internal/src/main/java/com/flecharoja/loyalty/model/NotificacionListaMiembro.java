package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NOTIFICACION_LISTA_MIEMBRO")
@IdClass(NotificacionListaMiembroPK.class)
@NamedQueries({
    @NamedQuery(name = "NotificacionListaMiembro.getIdMiembrosByIdNotificacion", query = "SELECT l.idMiembro FROM NotificacionListaMiembro l WHERE l.idNotificacion = :idNotificacion")
})
public class NotificacionListaMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_NOTIFICACION")
    private String idNotificacion;
    
    @Id
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;
    
    public NotificacionListaMiembro() {
    }

    public String getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(String idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotificacion != null ? idNotificacion.hashCode() : 0);
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificacionListaMiembro)) {
            return false;
        }
        NotificacionListaMiembro other = (NotificacionListaMiembro) object;
        if ((this.idNotificacion == null && other.idNotificacion != null) || (this.idNotificacion != null && !this.idNotificacion.equals(other.idNotificacion))) {
            return false;
        }
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.NotificacionListaMiembro[ idNotificacion=" + idNotificacion + ", idMiembro=" + idMiembro + " ]";
    }
    
}
