package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MIEMBRO")
@NamedQueries({
    @NamedQuery(name = "MiembroData.getDocIdentificacionByIdMiembro", query = "SELECT m.docIdentificacion FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getFechaIngresoByIdMiembro", query = "SELECT m.fechaIngreso FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getFechaExpiracionByIdMiembro", query = "SELECT m.fechaExpiracion FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getDireccionByIdMiembro", query = "SELECT m.direccion FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getCiudadResidenciaByIdMiembro", query = "SELECT m.ciudadResidencia FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getEstadoResidenciaByIdMiembro", query = "SELECT m.estadoResidencia FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getPaisResidenciaByIdMiembro", query = "SELECT m.paisResidencia FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getCodigoPostalByIdMiembro", query = "SELECT m.codigoPostal FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getTelefonoMovilByIdMiembro", query = "SELECT m.telefonoMovil FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getFechaNacimientoByIdMiembro", query = "SELECT m.fechaNacimiento FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getIndGeneroByIdMiembro", query = "SELECT m.indGenero FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getIndEstadoCivilByIdMiembro", query = "SELECT m.indEstadoCivil FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getFrecuenciaCompraByIdMiembro", query = "SELECT m.frecuenciaCompra FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getIndEducacionByIdMiembro", query = "SELECT m.indEducacion FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getIngresoEconomicoByIdMiembro", query = "SELECT m.ingresoEconomico FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getIndHijosByIdMiembro", query = "SELECT m.indHijos FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getNombreByIdMiembro", query = "SELECT m.nombre FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getApellidoByIdMiembro", query = "SELECT m.apellido FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getApellido2ByIdMiembro", query = "SELECT m.apellido2 FROM MiembroData m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroData.getCorreoByIdMiembro", query = "SELECT m.correo FROM MiembroData m WHERE m.idMiembro = :idMiembro")
})
public class MiembroData implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;

    @Column(name = "DOC_IDENTIFICACION")
    private String docIdentificacion;

    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;

    @Column(name = "FECHA_EXPIRACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaExpiracion;

    @Column(name = "DIRECCION")
    private String direccion;

    @Column(name = "CIUDAD_RESIDENCIA")
    private String ciudadResidencia;

    @Column(name = "ESTADO_RESIDENCIA")
    private String estadoResidencia;

    @Column(name = "PAIS_RESIDENCIA")
    private String paisResidencia;

    @Column(name = "CODIGO_POSTAL")
    private String codigoPostal;

    @Column(name = "TELEFONO_MOVIL")
    private String telefonoMovil;

    @Column(name = "FECHA_NACIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;

    @Column(name = "IND_GENERO")
    private Character indGenero;

    @Column(name = "IND_ESTADO_CIVIL")
    private Character indEstadoCivil;

    @Column(name = "FRECUENCIA_COMPRA")
    private String frecuenciaCompra;

    @Column(name = "IND_EDUCACION")
    private Character indEducacion;

    @Column(name = "INGRESO_ECONOMICO")
    private Double ingresoEconomico;

    @Column(name = "IND_HIJOS")
    private Boolean indHijos;
    
    @Version
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "APELLIDO")
    private String apellido;

    @Column(name = "APELLIDO2")
    private String apellido2;

    @Column(name = "EMAIL")
    private String correo;

    public MiembroData() {
    }

    public MiembroData(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getDocIdentificacion() {
        return docIdentificacion;
    }

    public void setDocIdentificacion(String docIdentificacion) {
        this.docIdentificacion = docIdentificacion;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudadResidencia() {
        return ciudadResidencia;
    }

    public void setCiudadResidencia(String ciudadResidencia) {
        this.ciudadResidencia = ciudadResidencia;
    }

    public String getEstadoResidencia() {
        return estadoResidencia;
    }

    public void setEstadoResidencia(String estadoResidencia) {
        this.estadoResidencia = estadoResidencia;
    }

    public String getPaisResidencia() {
        return paisResidencia;
    }

    public void setPaisResidencia(String paisResidencia) {
        this.paisResidencia = paisResidencia;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Character getIndGenero() {
        return indGenero;
    }

    public void setIndGenero(Character indGenero) {
        this.indGenero = indGenero;
    }

    public Character getIndEstadoCivil() {
        return indEstadoCivil;
    }

    public void setIndEstadoCivil(Character indEstadoCivil) {
        this.indEstadoCivil = indEstadoCivil;
    }

    public String getFrecuenciaCompra() {
        return frecuenciaCompra;
    }

    public void setFrecuenciaCompra(String frecuenciaCompra) {
        this.frecuenciaCompra = frecuenciaCompra;
    }

    public Character getIndEducacion() {
        return indEducacion;
    }

    public void setIndEducacion(Character indEducacion) {
        this.indEducacion = indEducacion;
    }

    public Double getIngresoEconomico() {
        return ingresoEconomico;
    }

    public void setIngresoEconomico(Double ingresoEconomico) {
        this.ingresoEconomico = ingresoEconomico;
    }

    public Boolean getIndHijos() {
        return indHijos;
    }

    public void setIndHijos(Boolean indHijos) {
        this.indHijos = indHijos;
    }
    
    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }
    
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiembroData)) {
            return false;
        }
        MiembroData other = (MiembroData) object;
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Miembro[ idMiembro=" + idMiembro + " ]";
    }

}
