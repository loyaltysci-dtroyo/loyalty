package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "CONFIGURACION_GENERAL")
public class ConfiguracionGeneral implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID")
    private Integer id;
    
    @Column(name = "MSJ_EMAIL_DESDE")
    private String msjEmailDesde;

    public ConfiguracionGeneral() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMsjEmailDesde() {
        return msjEmailDesde;
    }

    public void setMsjEmailDesde(String msjEmailDesde) {
        this.msjEmailDesde = msjEmailDesde;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfiguracionGeneral)) {
            return false;
        }
        ConfiguracionGeneral other = (ConfiguracionGeneral) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ConfiguracionGeneral[ id=" + id + " ]";
    }
    
}
