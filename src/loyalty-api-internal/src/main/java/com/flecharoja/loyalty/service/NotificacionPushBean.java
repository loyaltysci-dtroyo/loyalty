package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.model.PeticionEnvioNotificacion;
import com.flecharoja.loyalty.model.PeticionEnvioNotificacionCustom;
import com.flecharoja.loyalty.model.TrabajosInternos;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author svargas
 */
@Stateless
public class NotificacionPushBean {
    
    private static final String API_KEY = "AAAANJFVhes:APA91bEwc9SpzH7hI72Iw2dGWNTNyU53pCtZ23KBR07ezNmqUodFz5rst2UoPNOcbC5CW3bJav3i_1TyXzpYrhMiagr6W7rXnUcRCB08D9lcFGwgyDnEHNTcTSY0j5l_TA_n-PBDB0DF";
    
    private enum ErroresFCM {
        Unavailable, InvalidRegistration, NotRegistered;
    }
    
    @EJB
    MiembroBean miembroBean;
    
    @EJB
    NotificacionTextoBean notificacionTextoBean;
    
    @EJB
    TrabajoInternoBean trabajoInternoBean;
    
    @EJB
    InstanciaNotificacionBean instanciaNotificacionBean; 

    @TransactionAttribute(TransactionAttributeType.NEVER)
    @Asynchronous
    public void enviarNuevaInstancia (PeticionEnvioNotificacion peticion, Notificacion notificacion, String idTrabajo) {
        System.out.println("Enviar push");
        String idNotificacion = peticion.getIdNotificacion();

        List<Miembro> miembros;
        try {
            miembros = miembroBean.getMiembrosNotificacion(idNotificacion, peticion.getMiembros());
        } catch (IOException ex) {
            System.out.println("Error encontrando miembros");
            trabajoInternoBean.cancelarTrabajoInterno(idTrabajo);
            return;
        }
        
        String idInstancia = instanciaNotificacionBean.getNewIdInstancia();
        
        Date fechaCreacion = new Date();
        instanciaNotificacionBean.insertInstanciaNotificacion(idInstancia, idNotificacion, miembros.stream().collect(Collectors.toMap(Miembro::getIdMiembro, (miembro) -> notificacionTextoBean.getTextoFinal(miembro.getIdMiembro(), notificacion.getTexto(), peticion.getParametros()))), fechaCreacion);
        
        JsonArrayBuilder respuesta = Json.createArrayBuilder();
        Map<String, Boolean> resultado = enviarPush(idInstancia, notificacion, miembros.stream().filter((miembro) -> miembro.getIndContactoNotificacion()!=null && miembro.getIndContactoNotificacion() && miembro.getFcmTokenRegistro()!=null).collect(Collectors.toList()), 0, 0, idTrabajo);
        resultado.putAll(miembros.stream().filter((miembro) -> miembro.getIndContactoNotificacion()==null || !miembro.getIndContactoNotificacion() || miembro.getFcmTokenRegistro()==null).collect(Collectors.toMap((miembro) -> miembro.getIdMiembro(), (miembro) -> Boolean.FALSE)));
        resultado.forEach((idMiembro, enviado) -> respuesta.add(Json.createObjectBuilder().add(idMiembro, enviado)));
        
        trabajoInternoBean.actualizarTrabajoInterno(idTrabajo, TrabajosInternos.Estados.COMPLETADO.getValue(), 100, Json.createObjectBuilder().add("idNotificacion", idNotificacion).add("respuesta", respuesta).build().toString());
    }
    
    private Map<String, Boolean> enviarPush (String idInstancia, Notificacion notificacion, List<Miembro> miembros, int numReintento, long msEspera, String idTrabajo) {
        System.out.println("Enviar Push, intento: " + numReintento);
        if (numReintento==5) {
            return miembros.stream().collect(Collectors.toMap((t) -> t.getIdMiembro(), (t) -> Boolean.FALSE));
        }
        
        try {
            Thread.sleep(msEspera);
        } catch (InterruptedException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
        }
        
        Map<String, Boolean> resultado = new HashMap<>();
        List<List<Miembro>> partition = Lists.partition(miembros, 800);
        List<Miembro> reenvios = new ArrayList<>();
        
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("https://fcm.googleapis.com/fcm/send");
        Invocation.Builder request = target.request();
        
        for (List<Miembro> eligibles : partition) {
            JsonArrayBuilder registrationIds = Json.createArrayBuilder();
            eligibles.forEach((eligible) -> {
                System.out.println("Se enviará notificación a: " + eligible.getIdMiembro() + " - " + eligible.getEmail());
                registrationIds.add(eligible.getFcmTokenRegistro());
            });
            
            request = request.header("Authorization", "key="+API_KEY);

            JsonObjectBuilder data = Json.createObjectBuilder();
            data.add("idNotificacion", idInstancia);
            data.add("encabezado", notificacion.getEncabezado().substring(0, notificacion.getEncabezado().length()>50?50:notificacion.getEncabezado().length())+"...");
            data.add("texto", notificacion.getTexto().substring(0, notificacion.getTexto().length()>100?100:notificacion.getTexto().length())+"...");
            
            JsonObjectBuilder notification = Json.createObjectBuilder();
            notification.add("title", notificacion.getEncabezado().length() > 50 ? notificacion.getEncabezado().substring(0, notificacion.getEncabezado().length()) + "..." : notificacion.getEncabezado());
            notification.add("body", notificacion.getTexto().length() > 50 ? notificacion.getTexto().substring(0, notificacion.getTexto().length()) + "..." : notificacion.getTexto());
            notification.add("sound", "default");

            JsonObjectBuilder body = Json.createObjectBuilder();
            body.add("data", data.build());
            body.add("notification", notification.build());
            body.add("registration_ids", registrationIds);
            body.add("content_available", true);
            body.add("priority", "high");

            Response response = request.post(Entity.entity(body.build(), MediaType.APPLICATION_JSON));
            Date fechaEnvio = response.getDate();
            
            if (response.getStatus()!=Response.Status.OK.getStatusCode()) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Estado de respuesta no esperado: ".concat(response.getStatusInfo().getReasonPhrase()).concat("\nReintentando enviar mensajes..."));
                reenvios.addAll(eligibles);
            } else {
                JsonObject responseJson = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();

                JsonArray jsonArray = responseJson.getJsonArray("results");
                for (int i = 0; i<jsonArray.size(); i++) {
                    JsonObject object = jsonArray.getJsonObject(i);
                    if (object.containsKey("error")) {
                        try {
                            ErroresFCM efcm = ErroresFCM.valueOf(object.getString("error"));
                            switch(efcm) {
                                case InvalidRegistration:
                                case NotRegistered: {
                                    Miembro miembro = eligibles.get(i);
                                    miembroBean.updateMiembroFCMToken(miembro.getIdMiembro(), null);
                                    resultado.put(miembro.getIdMiembro(), Boolean.FALSE);
                                    break;
                                }
                                case Unavailable: {
                                    reenvios.add(eligibles.get(i));
                                    break;
                                }
                            }
                        } catch (IllegalArgumentException e) {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Error no procesado: ".concat(object.getString("error")).concat("\nReintentando enviar mensaje..."));
                            reenvios.add(eligibles.get(i));
                        }
                    } else {
                        Miembro miembro = eligibles.get(i);
                        if (object.containsKey("registration_id")) {
                            miembroBean.updateMiembroFCMToken(miembro.getIdMiembro(), object.getString("registration_id"));
                        }
                        instanciaNotificacionBean.updateInstanciaNotificacionMiembro(idInstancia, miembro.getIdMiembro(), fechaEnvio);
                        resultado.put(miembro.getIdMiembro(), Boolean.TRUE);
                    }
                }
            }
        }
        
        if (!reenvios.isEmpty()) {
            resultado.putAll(enviarPush(idInstancia, notificacion, reenvios, numReintento+1, msEspera==0?1000:msEspera*2, idTrabajo));
        }
        
        return resultado;
    }
    
    @TransactionAttribute(TransactionAttributeType.NEVER)
    @Asynchronous
    public void enviarNuevaInstancia (PeticionEnvioNotificacionCustom peticion) {
        List<Miembro> miembros = miembroBean.getMiembrosNotificacion(peticion.getMiembros());
        
        enviarPushCustom(peticion, miembros.stream().filter((miembro) -> miembro.getFcmTokenRegistro()!=null).collect(Collectors.toList()), 0, 0);
    }
    
    private Map<String, Boolean> enviarPushCustom (PeticionEnvioNotificacionCustom peticion, List<Miembro> miembros, int numReintento, long msEspera) {
        if (numReintento==5) {
            return miembros.stream().collect(Collectors.toMap((t) -> t.getIdMiembro(), (t) -> Boolean.FALSE));
        }
        
        try {
            Thread.sleep(msEspera);
        } catch (InterruptedException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
        }
        
        Map<String, Boolean> resultado = new HashMap<>();
        List<Miembro> reenvios = new ArrayList<>();
        
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("https://fcm.googleapis.com/fcm/send");
        Invocation.Builder request = target.request();
        
        for (Miembro miembro : miembros) {
            System.out.println("Se enviará notificación a: " + miembro.getIdMiembro() + " - " + miembro.getEmail());
            
            JsonArrayBuilder registrationIds = Json.createArrayBuilder();
            registrationIds.add(miembro.getFcmTokenRegistro());
            
            request = request.header("Authorization", "key="+API_KEY);

            JsonObjectBuilder notification = Json.createObjectBuilder();
            notification.add("title", peticion.getTitulo());
            notification.add("body", notificacionTextoBean.getTextoFinal(miembro.getIdMiembro(), peticion.getCuerpo(), peticion.getParametros()));
            notification.add("sound", "default");
            
            JsonObjectBuilder data = Json.createObjectBuilder();
            data.addNull("idNotificacion");
            data.add("encabezado", peticion.getTitulo());
            data.add("texto", notificacionTextoBean.getTextoFinal(miembro.getIdMiembro(), peticion.getCuerpo(), peticion.getParametros()));
            
            JsonObjectBuilder body = Json.createObjectBuilder();
            body.add("notification", notification.build());
            body.add("data", data.build());
            body.add("registration_ids", registrationIds);
            body.add("content_available", true);
            body.add("priority", "high");

            Response response = request.post(Entity.entity(body.build(), MediaType.APPLICATION_JSON));
            
            if (response.getStatus()!=Response.Status.OK.getStatusCode()) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Estado de respuesta no esperado: ".concat(response.getStatusInfo().getReasonPhrase()).concat("\nReintentando enviar mensajes..."));
                reenvios.add(miembro);
            } else {
                JsonObject responseJson = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();

                JsonArray jsonArray = responseJson.getJsonArray("results");
                for (int i = 0; i<jsonArray.size(); i++) {
                    JsonObject object = jsonArray.getJsonObject(i);
                    if (object.containsKey("error")) {
                        try {
                            ErroresFCM efcm = ErroresFCM.valueOf(object.getString("error"));
                            switch(efcm) {
                                case InvalidRegistration:
                                case NotRegistered: {
                                    miembroBean.updateMiembroFCMToken(miembro.getIdMiembro(), null);
                                    resultado.put(miembro.getIdMiembro(), Boolean.FALSE);
                                    break;
                                }
                                case Unavailable: {
                                    reenvios.add(miembro);
                                    break;
                                }
                            }
                        } catch (IllegalArgumentException e) {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Error no procesado: ".concat(object.getString("error")).concat("\nReintentando enviar mensaje..."));
                            reenvios.add(miembro);
                        }
                    } else {
                        if (object.containsKey("registration_id")) {
                            miembroBean.updateMiembroFCMToken(miembro.getIdMiembro(), object.getString("registration_id"));
                        }
                        resultado.put(miembro.getIdMiembro(), Boolean.TRUE);
                    }
                }
            }
        }
        
        if (!reenvios.isEmpty()) {
            resultado.putAll(enviarPushCustom(peticion, reenvios, numReintento+1, msEspera==0?1000:msEspera*2));
        }
        
        return resultado;
    }
}
