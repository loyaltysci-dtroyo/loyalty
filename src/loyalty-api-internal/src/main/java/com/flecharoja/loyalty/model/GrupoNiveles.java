/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "GRUPO_NIVELES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GrupoNiveles.findAll", query = "SELECT g FROM GrupoNiveles g"),
    @NamedQuery(name = "GrupoNiveles.findByIdGrupoNivel", query = "SELECT g FROM GrupoNiveles g WHERE g.idGrupoNivel = :idGrupoNivel")
})
public class GrupoNiveles implements Serializable {

  
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_GRUPO_NIVEL")
    private String idGrupoNivel;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "GRUPO_NIVELES")
    @OrderBy(value = "metricaInicial")
    private List<NivelMetrica> nivelMetricaList;
    

    public GrupoNiveles() {
    }

    public GrupoNiveles(String idGrupoNivel) {
        this.idGrupoNivel = idGrupoNivel;
    }
    
    public String getIdGrupoNivel() {
        return idGrupoNivel;
    }

    public void setIdGrupoNivel(String idGrupoNivel) {
        this.idGrupoNivel = idGrupoNivel;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<NivelMetrica> getNivelMetricaList() {
        return nivelMetricaList;
    }

    public void setNivelMetricaList(List<NivelMetrica> nivelMetricaList) {
        this.nivelMetricaList = nivelMetricaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGrupoNivel != null ? idGrupoNivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupoNiveles)) {
            return false;
        }
        GrupoNiveles other = (GrupoNiveles) object;
        if ((this.idGrupoNivel == null && other.idGrupoNivel != null) || (this.idGrupoNivel != null && !this.idGrupoNivel.equals(other.idGrupoNivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.GrupoNiveles[ idGrupoNivel=" + idGrupoNivel + " ]";
    }  

}
