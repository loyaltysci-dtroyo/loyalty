/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "TABLA_POSICIONES")
@NamedQueries({
    @NamedQuery(name = "TablaPosiciones.findAll", query = "SELECT t FROM TablaPosiciones t"),
    @NamedQuery(name = "TablaPosiciones.findByIdTabla", query = "SELECT t FROM TablaPosiciones t WHERE t.idTabla = :idTabla"),
    @NamedQuery(name = "TablaPosiciones.findByIdMetrica", query = "SELECT t FROM TablaPosiciones t WHERE t.idMetrica.idMetrica = :idMetrica"),
    @NamedQuery(name = "TablaPosiciones.findGeneral", query = "SELECT t FROM TablaPosiciones t WHERE t.idMetrica.idMetrica = :idMetrica AND t.indTipo = :indTipo")
})
public class TablaPosiciones implements Serializable {
    
    public enum Calculo{
        SUMA('S'),
        PROMEDIO('P');
        
        private final char value;
        private static final Map<Character,Calculo> lookup = new HashMap<>();

        private Calculo(char value) {
            this.value = value;
        }
        
        static{
            for(Calculo calculo :values()){
                lookup.put(calculo.value, calculo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Calculo get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Tipo{
        MIEMBROS('U'),
        GRUPO('G'),
        GRUPO_ESPECIFICO('E');
        
        private final char value;
        private static final Map<Character, Tipo> lookup = new HashMap<>();

        private Tipo(char value) {
            this.value = value;
        }
        
        static{
            for(Tipo tipo:values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipo get(Character value){
            return value==null?null:lookup.get(value);
        }
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tablaPosiciones")
    private List<TabposMiembro> tabposMiembroList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tablaPosiciones")
    private List<TabposGrupomiembro> tabposGrupomiembroList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_TABLA")
    private String idTabla;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "IND_FORMATO")
    private String indFormato;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Column(name = "IND_GRUPAL_FORMA_CALCULO")
    private Character indGrupalFormaCalculo;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    private String imagen;
    
    @JoinColumn(name = "ID_GRUPO", referencedColumnName = "ID_GRUPO")
    @ManyToOne
    private Grupo idGrupo;
    
    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    private Metrica idMetrica;
    
    public TablaPosiciones() {
    }

    public TablaPosiciones(String idTabla) {
        this.idTabla = idTabla;
    }

    public String getIdTabla() {
        return idTabla;
    }

    public void setIdTabla(String idTabla) {
        this.idTabla = idTabla;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public String getIndFormato() {
        return indFormato;
    }

    public void setIndFormato(String indFormato) {
        this.indFormato = indFormato;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getIndGrupalFormaCalculo() {
        return indGrupalFormaCalculo;
    }

    public void setIndGrupalFormaCalculo(Character indGrupalFormaCalculo) {
        this.indGrupalFormaCalculo = indGrupalFormaCalculo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Grupo getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Grupo idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTabla != null ? idTabla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TablaPosiciones)) {
            return false;
        }
        TablaPosiciones other = (TablaPosiciones) object;
        if ((this.idTabla == null && other.idTabla != null) || (this.idTabla != null && !this.idTabla.equals(other.idTabla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.model.TablaPosiciones[ idTabla=" + idTabla + " ]";
    }

    public List<TabposMiembro> getTabposMiembroList() {
        return tabposMiembroList;
    }

    public void setTabposMiembroList(List<TabposMiembro> tabposMiembroList) {
        this.tabposMiembroList = tabposMiembroList;
    }

    public List<TabposGrupomiembro> getTabposGrupomiembroList() {
        return tabposGrupomiembroList;
    }

    public void setTabposGrupomiembroList(List<TabposGrupomiembro> tabposGrupomiembroList) {
        this.tabposGrupomiembroList = tabposGrupomiembroList;
    }

}
