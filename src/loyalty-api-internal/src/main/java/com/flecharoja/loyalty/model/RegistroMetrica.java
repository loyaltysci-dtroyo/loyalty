package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "REGISTRO_METRICA")
public class RegistroMetrica implements Serializable {
    
    public enum TiposGane{
        DESCONOCIDO("D"),
        MISION("M"),
        BIENVENIDA("B"),
        REFERENCIA("R"),
        TRANSACCION("T"),
        REVERSION("V"),
        BONIFICACION_EXTRA("E");
        
        private final String value;
        private static final Map<String,TiposGane> lookup = new HashMap<>();

        private TiposGane(String value) {
            this.value = value;
        }

        static{
            for(TiposGane ind : values()){
                lookup.put(ind.value, ind);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static TiposGane get(String value){
            return value==null?DESCONOCIDO:lookup.get(value);
        }
    }
    
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RegistroMetricaPK registroMetricaPK;
    
    @Column(name = "ID_METRICA")
    private String idMetrica;
    
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;
    
    @Column(name = "IND_TIPO_GANE")
    private String indTipoGane;

    @Column(name = "CANTIDAD")
    private Double cantidad;

    @Column(name = "ID_MISION")
    private String idMision;
    
    @Column(name = "ID_TRANSACCION")
    private String idTransaccion;
    
    @Column(name = "DISPONIBLE_ACTUAL")
    private Double disponibleActual;

    public RegistroMetrica() {
    }

    public RegistroMetrica(Date fecha, String idMetrica, String idMiembro, TiposGane tipoGane, Double cantidad, String idTransaccion, String idMision) {
        this.registroMetricaPK = new RegistroMetricaPK(fecha, null);
        this.idMetrica = idMetrica;
        this.idMiembro = idMiembro;
        this.indTipoGane = tipoGane.value;
        this.cantidad = cantidad;
        this.idTransaccion = idTransaccion;
        this.idMision = idMision;
    }

    public RegistroMetricaPK getRegistroMetricaPK() {
        return registroMetricaPK;
    }

    public void setRegistroMetricaPK(RegistroMetricaPK registroMetricaPK) {
        this.registroMetricaPK = registroMetricaPK;
    }

    public String getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(String idMetrica) {
        this.idMetrica = idMetrica;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIndTipoGane() {
        return indTipoGane;
    }

    public void setIndTipoGane(String indTipoGane) {
        this.indTipoGane = indTipoGane;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Double getDisponibleActual() {
        return disponibleActual;
    }

    public void setDisponibleActual(Double disponibleActual) {
        this.disponibleActual = disponibleActual;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (registroMetricaPK != null ? registroMetricaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegistroMetrica)) {
            return false;
        }
        RegistroMetrica other = (RegistroMetrica) object;
        if ((this.registroMetricaPK == null && other.registroMetricaPK != null) || (this.registroMetricaPK != null && !this.registroMetricaPK.equals(other.registroMetricaPK))) {
            return false;
        }
        return true;
    }
}