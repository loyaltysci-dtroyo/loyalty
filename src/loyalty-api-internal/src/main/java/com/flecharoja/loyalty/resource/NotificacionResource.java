package com.flecharoja.loyalty.resource;

import com.flecharoja.loyalty.model.PeticionEnvioNotificacion;
import com.flecharoja.loyalty.model.PeticionEnvioNotificacionCustom;
import com.flecharoja.loyalty.service.CorreoElectronicoBean;
import com.flecharoja.loyalty.service.NotificacionBean;
import javax.ejb.EJB;
import javax.json.Json;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author svargas
 */
@Path("envio-notificacion")
public class NotificacionResource {
    
    @Context
    HttpServletRequest request;
    
    @EJB
    NotificacionBean bean;
    
    @EJB
    CorreoElectronicoBean ceb;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response enviarNotificacion(PeticionEnvioNotificacion peticion) {
        return Response.accepted(Json.createObjectBuilder().add("idTrabajo", bean.enviarNotificacion(peticion, request.getLocale())).build()).build();
    }
    
    @POST
    @Path("personalizado")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void enviarNotificacion(PeticionEnvioNotificacionCustom peticion) {
        bean.enviarNotificacionCustom(peticion, request.getLocale());
    }
}
