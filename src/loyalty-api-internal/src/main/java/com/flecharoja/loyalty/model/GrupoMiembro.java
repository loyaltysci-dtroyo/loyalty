/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "GRUPO_MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GrupoMiembro.findAll", query = "SELECT g FROM GrupoMiembro g"),
    @NamedQuery(name = "GrupoMiembro.findGruposNotInMiembro", query = "SELECT g FROM Grupo g WHERE g.indVisible = 'V' AND  g.idGrupo NOT IN (SELECT s.grupoMiembroPK.idGrupo FROM GrupoMiembro s WHERE s.grupoMiembroPK.idMiembro = :idMiembro)"),
    @NamedQuery(name = "GrupoMiembro.findByIdMiembro", query = "SELECT g.grupo FROM GrupoMiembro g WHERE g.grupoMiembroPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "GrupoMiembro.findByIdGrupo", query = "SELECT g.miembro FROM GrupoMiembro g WHERE g.grupoMiembroPK.idGrupo = :idGrupo"),
    @NamedQuery(name = "GrupoMiembro.findByFechaCreacion", query = "SELECT g FROM GrupoMiembro g WHERE g.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "GrupoMiembro.findByUsuarioCreacion", query = "SELECT g FROM GrupoMiembro g WHERE g.usuarioCreacion = :usuarioCreacion")})
public class GrupoMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GrupoMiembroPK grupoMiembroPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @JoinColumn(name = "ID_GRUPO", referencedColumnName = "ID_GRUPO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Grupo grupo;
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;

    public GrupoMiembro() {
    }

    public GrupoMiembro(GrupoMiembroPK grupoMiembroPK) {
        this.grupoMiembroPK = grupoMiembroPK;
    }

    public GrupoMiembro(GrupoMiembroPK grupoMiembroPK, Date fechaCreacion, String usuarioCreacion) {
        this.grupoMiembroPK = grupoMiembroPK;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public GrupoMiembro(String idMiembro, String idGrupo) {
        this.grupoMiembroPK = new GrupoMiembroPK(idMiembro, idGrupo);
    }

    public GrupoMiembroPK getGrupoMiembroPK() {
        return grupoMiembroPK;
    }

    public void setGrupoMiembroPK(GrupoMiembroPK grupoMiembroPK) {
        this.grupoMiembroPK = grupoMiembroPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (grupoMiembroPK != null ? grupoMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupoMiembro)) {
            return false;
        }
        GrupoMiembro other = (GrupoMiembro) object;
        if ((this.grupoMiembroPK == null && other.grupoMiembroPK != null) || (this.grupoMiembroPK != null && !this.grupoMiembroPK.equals(other.grupoMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.GrupoMiembro[ grupoMiembroPK=" + grupoMiembroPK + " ]";
    }
    
}
