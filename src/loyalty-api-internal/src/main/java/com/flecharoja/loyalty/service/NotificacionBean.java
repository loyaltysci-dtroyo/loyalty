package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.exception.MyException;
import com.flecharoja.loyalty.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.model.PeticionEnvioNotificacion;
import com.flecharoja.loyalty.model.PeticionEnvioNotificacionCustom;
import com.flecharoja.loyalty.model.TrabajosInternos;
import java.util.Date;
import java.util.Locale;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author svargas
 */
@Stateless
public class NotificacionBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    private NotificacionPushBean notificacionPushBean;
    
    @EJB
    private CorreoElectronicoBean correoElectronicoBean;
    
    public String enviarNotificacion(PeticionEnvioNotificacion peticion, Locale locale) {
        
        Notificacion notificacion;
        try {
            notificacion = em.find(Notificacion.class, peticion.getIdNotificacion());
        } catch (IllegalArgumentException e) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "idNotificacion");
        }
        if (notificacion==null) {
            throw new MyException(MyException.ErrorRecursoNoEncontrado.ENTIDAD_NO_EXISTENTE, locale, "notification_not_found");
        }
        
        if (!notificacion.getIndEstado().equals(Notificacion.Estados.PUBLICADO.getValue())) {
            throw new MyException(MyException.ErrorAutorizacion.OPERACION_NO_PERMITIDA, locale, "notification_not_published");
        }
        
        String idTrabajo = null;
        switch (Notificacion.Tipos.get(notificacion.getIndTipo())) {
            case EMAIL: {
                if (peticion.getSender()==null) {
                    ConfiguracionGeneral configuracionGeneral = em.find(ConfiguracionGeneral.class, 0);
                    if (configuracionGeneral==null || configuracionGeneral.getMsjEmailDesde()==null) {
                        throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "sender_email_not_available");
                    }
                    peticion.setSender(configuracionGeneral.getMsjEmailDesde());
                }
                
                TrabajosInternos trabajo = new TrabajosInternos();
                em.persist(trabajo);
                em.flush();
                
                idTrabajo = trabajo.getIdTrabajo();
                
                correoElectronicoBean.enviarNuevaInstancia(peticion, notificacion, idTrabajo);
                break;
            }
            case NOTIFICACION_PUSH: {
                System.out.println("Notificacion Push");
                TrabajosInternos trabajo = new TrabajosInternos();
                em.persist(trabajo);
                em.flush();
                
                idTrabajo = trabajo.getIdTrabajo();
                
                notificacionPushBean.enviarNuevaInstancia(peticion, notificacion, idTrabajo);
                break;
            }
        }
        
        if (!Notificacion.Eventos.get(notificacion.getIndEvento()).equals(Notificacion.Eventos.DETONADOR)) {
            notificacion.setIndEstado(Notificacion.Estados.EJECUTADO.getValue());
        }
        
        notificacion.setFechaEjecucion(new Date());
        em.merge(notificacion);
        
        if (idTrabajo==null) {
            throw new MyException(MyException.ErrorSistema.ERROR_DESCONOCIDO, locale, "job_id_not_created");
        }
        
        return idTrabajo;
    }

    public void enviarNotificacionCustom(PeticionEnvioNotificacionCustom peticion, Locale locale) {
        if (peticion.getCuerpo()==null || peticion.getTitulo()==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "body, titulo");
        }
        
        if (peticion.getMiembros()==null || peticion.getMiembros().isEmpty()) {
            throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "no_objective_members");
        }
        
        PeticionEnvioNotificacionCustom.Tipos tipo = PeticionEnvioNotificacionCustom.Tipos.get(peticion.getTipoMsj());
        if (tipo==null) {
            throw new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, locale, "attributes_invalid", "tipoMsj");
        }
        switch (tipo) {
            case EMAIL: {
                if (peticion.getSender()==null) {
                    ConfiguracionGeneral configuracionGeneral = em.find(ConfiguracionGeneral.class, 0);
                    if (configuracionGeneral==null || configuracionGeneral.getMsjEmailDesde()==null) {
                        throw new MyException(MyException.ErrorSistema.OPERACION_FALLIDA, locale, "sender_email_not_available");
                    }
                    peticion.setSender(configuracionGeneral.getMsjEmailDesde());
                }
                
                correoElectronicoBean.enviarNuevaInstancia(peticion);
                break;
            }
            case NOTIFICACION_PUSH: {
                notificacionPushBean.enviarNuevaInstancia(peticion);
                break;
            }
        }
    }
}
