package com.flecharoja.loyalty.service;

import java.util.Map;
import java.util.regex.Matcher;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 * Clase de utilidad para el texto de notificacion
 *
 * @author svargas
 */
@Stateless
public class NotificacionTextoBean {

    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    /**
     * Metodo que reemplaza las entradas de parametros del texto de una notificacion por su valor correspondiente
     * 
     * @param idMiembro Identificador del miembro
     * @param texto Texto de la notificacion
     * @param parametros Entradas de parametros extras
     * @return Texto de la notificacion resultante
     */
    public String getTextoFinal (String idMiembro, String texto, Map<String, String> parametros) {
        if (texto.contains("${member.name}")) {
            String nombre;
            try {
                nombre = (String) em.createNamedQuery("MiembroData.getNombreByIdMiembro").setParameter("idMiembro", idMiembro).getSingleResult();
            } catch (NoResultException e) {
                nombre = "null";
            }
            texto = texto.replaceAll("\\$\\{member.name\\}", Matcher.quoteReplacement(nombre));
        }

        if (texto.contains("${member.lastname1}")) {
            String apellido1;
            try {
                apellido1 = (String) em.createNamedQuery("MiembroData.getApellidoByIdMiembro").setParameter("idMiembro", idMiembro).getSingleResult();
            } catch (NoResultException e) {
                apellido1 = "null";
            }
            texto = texto.replaceAll("\\$\\{member.lastname1\\}", Matcher.quoteReplacement(apellido1));
        }

        if (texto.contains("${member.lastname2}")) {
            String apellido2;
            try {
                apellido2 = (String) em.createNamedQuery("MiembroData.getApellido2ByIdMiembro").setParameter("idMiembro", idMiembro).getSingleResult();
            } catch (NoResultException e) {
                apellido2 = "null";
            }
            texto = texto.replaceAll("\\$\\{member.lastname2\\}", Matcher.quoteReplacement(apellido2));
        }

        if (texto.contains("${member.email}")) {
            String correo;
            try {
                correo = (String) em.createNamedQuery("MiembroData.getCorreoByIdMiembro").setParameter("idMiembro", idMiembro).getSingleResult();
            } catch (NoResultException e) {
                correo = "null";
            }
            texto = texto.replaceAll("\\$\\{member.email\\}", Matcher.quoteReplacement(correo));
        }
        
        if (parametros==null || parametros.isEmpty()) {
            return texto;
        }
        for (Map.Entry<String, String> param : parametros.entrySet()) {
            if (param.getValue()!=null) {
                texto = texto.replaceAll("\\$\\{other."+param.getKey()+"\\}", Matcher.quoteReplacement(param.getValue()));
            }
        }
        
        return texto;
    }
}
