package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.InstanciaNotificacion;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

/**
 *
 * @author svargas
 */
@Stateless
public class InstanciaNotificacionBean {
    
    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    public String getNewIdInstancia() {
        String idInstancia = UUID.randomUUID().toString();
        while ((long)em.createNamedQuery("InstanciaNotificacion.countInstanciasByIdInstancia").setParameter("idInstancia", idInstancia).getSingleResult()>0) {
            idInstancia = UUID.randomUUID().toString();
        }
        return idInstancia;
    }

    public void insertInstanciaNotificacion(String idInstancia, String idNotificacion, Map<String, String> miembrosData, Date fechaCreacion) throws PersistenceException {
        miembrosData.entrySet().forEach((data) -> {
            em.persist(new InstanciaNotificacion(data.getKey(), idInstancia, idNotificacion, fechaCreacion, data.getValue()));
        });
    }
    
    public void insertInstanciaNotificacion(String idInstancia, String idNotificacion, String idMiembro, Date fecha) throws PersistenceException {
        InstanciaNotificacion instanciaNotificacion = new InstanciaNotificacion(idMiembro, idInstancia, idNotificacion, fecha, null);
        instanciaNotificacion.setFechaEnvio(fecha);
        em.persist(instanciaNotificacion);
    }
    
    public void updateInstanciaNotificacionMiembro(String idInstancia, String idMiembro, Date fechaEnvio) {
        InstanciaNotificacion instanciaNotificacion = (InstanciaNotificacion) em.createNamedQuery("InstanciaNotificacion.findInstanciaByIdInstanciaIdMiembro")
                .setParameter("idInstancia", idInstancia).setParameter("idMiembro", idMiembro).getSingleResult();
        instanciaNotificacion.setFechaEnvio(fechaEnvio);
        em.merge(instanciaNotificacion);
    }
}
