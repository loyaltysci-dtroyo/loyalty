package com.flecharoja.loyalty.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author wtencio, svargas
 */
@Entity
@Table(name = "MIEMBRO")
public class Miembro implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;

    @Column(name = "IND_CONTACTO_EMAIL")
    private Boolean indContactoEmail;

    @Column(name = "IND_CONTACTO_SMS")
    private Boolean indContactoSms;

    @Column(name = "IND_CONTACTO_NOTIFICACION")
    private Boolean indContactoNotificacion;

    @Column(name = "IND_CONTACTO_ESTADO")
    private Boolean indContactoEstado;

    @Version
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    @Size(max = 200)
    @Column(name = "FCM_TOKEN_REGISTRO")
    private String fcmTokenRegistro;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMAIL")
    private String email;

    public Miembro() {
    }

    public Miembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public Boolean getIndContactoEmail() {
        return indContactoEmail;
    }

    public void setIndContactoEmail(Boolean indContactoEmail) {
        this.indContactoEmail = indContactoEmail;
    }

    public Boolean getIndContactoSms() {
        return indContactoSms;
    }

    public void setIndContactoSms(Boolean indContactoSms) {
        this.indContactoSms = indContactoSms;
    }

    public Boolean getIndContactoNotificacion() {
        return indContactoNotificacion;
    }

    public void setIndContactoNotificacion(Boolean indContactoNotificacion) {
        this.indContactoNotificacion = indContactoNotificacion;
    }

    public Boolean getIndContactoEstado() {
        return indContactoEstado;
    }

    public void setIndContactoEstado(Boolean indContactoEstado) {
        this.indContactoEstado = indContactoEstado;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getFcmTokenRegistro() {
        return fcmTokenRegistro;
    }

    public void setFcmTokenRegistro(String fcmTokenRegistro) {
        this.fcmTokenRegistro = fcmTokenRegistro;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Miembro)) {
            return false;
        }
        Miembro other = (Miembro) object;
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Miembro[ idMiembro=" + idMiembro + " ]";
    }

}
