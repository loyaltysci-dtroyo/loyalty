package com.flecharoja.loyalty.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NOTIFICACION")
public class Notificacion implements Serializable {
    
    public enum Estados {
        PUBLICADO('P'),
        EJECUTADO('E'),
        ARCHIVADO('A'),
        BORRADOR('B');
        
        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Estados get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Eventos {
        MANUAL('M'),
        CALENDARIZADO('C'),
        DETONADOR('D');
        
        private final char value;
        private static final Map<Character, Eventos> lookup = new HashMap<>();

        private Eventos(char value) {
            this.value = value;
        }
        
        static {
            for (Eventos estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Eventos get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Tipos {
        NOTIFICACION_PUSH('P'),
        EMAIL('E');
        
        private final char value;
        private static final Map<Character, Tipos> lookup = new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }
        
        static {
            for (Tipos estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipos get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_NOTIFICACION")
    private String idNotificacion;
    
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Column(name = "IND_EVENTO")
    private Character indEvento;
    
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Column(name = "FECHA_EJECUCION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEjecucion;
    
    @Column(name = "ENCABEZADO_ARTE")
    private String encabezado;
    
    @Column(name = "TEXTO")
    private String texto;

    public Notificacion() {
    }

    public Notificacion(String idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public String getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(String idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo!=null?Character.toUpperCase(indTipo):null;
    }

    public Character getIndEvento() {
        return indEvento;
    }

    public void setIndEvento(Character indEvento) {
        this.indEvento = indEvento!=null?Character.toUpperCase(indEvento):null;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado!=null?Character.toUpperCase(indEstado):null;
    }

    public Date getFechaEjecucion() {
        return fechaEjecucion;
    }

    public void setFechaEjecucion(Date fechaEjecucion) {
        this.fechaEjecucion = fechaEjecucion;
    }

    public String getEncabezado() {
        return encabezado;
    }

    public void setEncabezado(String encabezado) {
        this.encabezado = encabezado;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotificacion != null ? idNotificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notificacion)) {
            return false;
        }
        Notificacion other = (Notificacion) object;
        if ((this.idNotificacion == null && other.idNotificacion != null) || (this.idNotificacion != null && !this.idNotificacion.equals(other.idNotificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Notificacion[ idNotificacion=" + idNotificacion + " ]";
    }
    
}
