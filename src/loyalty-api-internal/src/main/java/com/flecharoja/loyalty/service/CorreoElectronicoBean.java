package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.Notificacion;
import com.flecharoja.loyalty.model.PeticionEnvioNotificacion;
import com.flecharoja.loyalty.model.PeticionEnvioNotificacionCustom;
import com.flecharoja.loyalty.model.TrabajosInternos;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.json.Json;
import javax.json.JsonArrayBuilder;

/**
 *
 * @author svargas
 */
@Stateless
public class CorreoElectronicoBean {
    
    private static final String URL_TRACKING_FILE = "https://api-loyalty.flecharoja.com/loyalty-tracking/image.gif";
    
    @EJB
    MiembroBean miembroBean;
    
    @EJB
    NotificacionTextoBean notificacionTextoBean;
    
    @EJB
    TrabajoInternoBean trabajoInternoBean;
    
    @EJB
    InstanciaNotificacionBean instanciaNotificacionBean;
    
    private static final String API_KEY = "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";
    
    @TransactionAttribute(TransactionAttributeType.NEVER)
    @Asynchronous
    public void enviarNuevaInstancia (PeticionEnvioNotificacion peticion, Notificacion notificacion, String idTrabajo) {
        List<Miembro> miembros;
        try {
            miembros = miembroBean.getMiembrosNotificacion(notificacion.getIdNotificacion(), peticion.getMiembros());
        } catch (IOException ex) {
            trabajoInternoBean.cancelarTrabajoInterno(idTrabajo);
            return;
        }
        
        String idInstancia = instanciaNotificacionBean.getNewIdInstancia();
        
        Map<String, String> map = miembros.stream().collect(Collectors.toMap((miembro) -> miembro.getIdMiembro(), (miembro) -> {
            if (miembro.getIndContactoEmail()==null || !miembro.getIndContactoEmail()) {
                return "";
            } else {
                return miembro.getEmail();
            }
        }));
        
        JsonArrayBuilder respuesta = Json.createArrayBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (!entry.getValue().isEmpty()) {
                Mail mail = new Mail();
                Personalization personalization = new Personalization();
                personalization.addTo(new Email(entry.getValue()));
                mail.addPersonalization(personalization);
                mail.setFrom(new Email(peticion.getSender()));
                mail.setSubject(notificacion.getEncabezado());
                String html;
                html = "<html>"
                            + "<body>"
                                + notificacionTextoBean.getTextoFinal(entry.getKey(), notificacion.getTexto(), peticion.getParametros())
                                + "<img src=\""+URL_TRACKING_FILE+"?idi="+idInstancia+"&idm="+entry.getKey()+"\"/>"
                            + "</body>"
                        + "</html>";
                mail.addContent(new Content("text/html", html));

                SendGrid sg = new SendGrid(API_KEY);
                Request request = new Request();
                try {
                    request.method = (Method.POST);
                    request.endpoint = ("mail/send");
                    request.body = (mail.build());
                    Response response = sg.api(request);
                    switch (response.statusCode) {
                        case 202: {
                            instanciaNotificacionBean.insertInstanciaNotificacion(idInstancia, notificacion.getIdNotificacion(), entry.getKey(), new Date());
                            respuesta.add(Json.createObjectBuilder().add(entry.getKey(), true));
                            break;
                        }
                        case 400: {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "The email send to member id: {0} (email: {1}) has failed, BAD REQUEST", new Object[]{entry.getKey(), entry.getValue()});
                            respuesta.add(Json.createObjectBuilder().add(entry.getKey(), false));
                            break;
                        }
                        case 401: {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "The email send to member id: {0} (email: {1}) has failed, UNAUTHORIZED", new Object[]{entry.getKey(), entry.getValue()});
                            respuesta.add(Json.createObjectBuilder().add(entry.getKey(), false));
                            break;
                        }
                        case 413: {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "The email send to member id: {0} (email: {1}) has failed, PAYLOAD TOO LARGE", new Object[]{entry.getKey(), entry.getValue()});
                            respuesta.add(Json.createObjectBuilder().add(entry.getKey(), false));
                            break;
                        }
                        default: {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "The email send to member id: {0} (email: {1}) has failed, response status not expected ({2})", new Object[]{entry.getKey(), entry.getValue(), response.statusCode});
                            respuesta.add(Json.createObjectBuilder().add(entry.getKey(), false));
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "The email send to member id: "+entry.getKey()+" (email: "+entry.getValue()+") has failed", ex);
                    respuesta.add(Json.createObjectBuilder().add(entry.getKey(), false));
                }
            } else {
                respuesta.add(Json.createObjectBuilder().add(entry.getKey(), false));
            }
        }
        
        trabajoInternoBean.actualizarTrabajoInterno(idTrabajo, TrabajosInternos.Estados.COMPLETADO.getValue(), 100, Json.createObjectBuilder().add("idNotificacion", notificacion.getIdNotificacion()).add("respuesta", respuesta).build().toString());
    }
    
    @TransactionAttribute(TransactionAttributeType.NEVER)
    @Asynchronous
    public void enviarNuevaInstancia (PeticionEnvioNotificacionCustom peticion) {
        List<Miembro> miembros = miembroBean.getMiembrosNotificacion(peticion.getMiembros());
        
        Map<String, String> map = miembros.stream().collect(Collectors.toMap((miembro) -> miembro.getIdMiembro(), (miembro) -> miembro.getEmail()));
        
        JsonArrayBuilder respuesta = Json.createArrayBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (!entry.getValue().isEmpty()) {
                Mail mail = new Mail();
                Personalization personalization = new Personalization();
                personalization.addTo(new Email(entry.getValue()));
                mail.addPersonalization(personalization);
                mail.setFrom(new Email(peticion.getSender()));
                mail.setSubject(peticion.getTitulo());
                String html;
                html = "<html>"
                            + "<body>"
                                + notificacionTextoBean.getTextoFinal(entry.getKey(), peticion.getCuerpo(), peticion.getParametros())
                            + "</body>"
                        + "</html>";
                mail.addContent(new Content("text/html", html));
                
                SendGrid sg = new SendGrid(API_KEY);
                Request request = new Request();
                try {
                    request.method = (Method.POST);
                    request.endpoint = ("mail/send");
                    request.body = (mail.build());
                    Response response = sg.api(request);
                    switch (response.statusCode) {
                        case 202: {
                            respuesta.add(Json.createObjectBuilder().add(entry.getKey(), true));
                            break;
                        }
                        case 400: {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "The email send to member id: {0} (email: {1}) has failed, BAD REQUEST", new Object[]{entry.getKey(), entry.getValue()});
                            respuesta.add(Json.createObjectBuilder().add(entry.getKey(), false));
                            break;
                        }
                        case 401: {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "The email send to member id: {0} (email: {1}) has failed, UNAUTHORIZED", new Object[]{entry.getKey(), entry.getValue()});
                            respuesta.add(Json.createObjectBuilder().add(entry.getKey(), false));
                            break;
                        }
                        case 413: {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "The email send to member id: {0} (email: {1}) has failed, PAYLOAD TOO LARGE", new Object[]{entry.getKey(), entry.getValue()});
                            respuesta.add(Json.createObjectBuilder().add(entry.getKey(), false));
                            break;
                        }
                        default: {
                            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "The email send to member id: {0} (email: {1}) has failed, response status not expected ({2})", new Object[]{entry.getKey(), entry.getValue(), response.statusCode});
                            respuesta.add(Json.createObjectBuilder().add(entry.getKey(), false));
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "The email send to member id: "+entry.getKey()+" (email: "+entry.getValue()+") has failed", ex);
                    respuesta.add(Json.createObjectBuilder().add(entry.getKey(), false));
                }
            } else {
                respuesta.add(Json.createObjectBuilder().add(entry.getKey(), false));
            }
        }
    }
}
