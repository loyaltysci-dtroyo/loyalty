package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.TrabajosInternos;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

/**
 *
 * @author svargas
 */
@Stateless
public class TrabajoInternoBean {
    
    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Actualiza los valores de estado, procentajeCompletado y resultado de un trabajo interno
     *
     * @param idTrabajo Identificador del trabajo interno
     * @param indEstado Indicador del estado de trabajo interno
     * @param porcentajeCompletado Numero entero representativo del porcentaje de complete
     * @param resultado Texto con el resultado actual del trabajo
     * @return Booleano sobre el hecho de que el trabajo es actualizable y se pudo actualizar (true = todo bien / false = algo mal)
     */
    public boolean actualizarTrabajoInterno(String idTrabajo, char indEstado, int porcentajeCompletado, String resultado) {
        TrabajosInternos ti = em.find(TrabajosInternos.class, idTrabajo);
        
        if (!ti.getIndEstado().equals(TrabajosInternos.Estados.PROCESANDO.getValue())) {
            return false;
        }
        
        ti.setFechaActualizacion(new Date());
        ti.setIndEstado(indEstado);
        ti.setPorcentajeCompletado(porcentajeCompletado);
        ti.setResultado(resultado);
        
        try {
            em.merge(ti);
            em.flush();
        } catch (PersistenceException e) {
            return false;
        }
        
        return true;
    }
    
    public void cancelarTrabajoInterno(String idTrabajo) {
        TrabajosInternos ti = em.find(TrabajosInternos.class, idTrabajo);
        
        if (!ti.getIndEstado().equals(TrabajosInternos.Estados.FALLIDO.getValue())) {
            ti.setFechaActualizacion(new Date());
            ti.setIndEstado(TrabajosInternos.Estados.FALLIDO.getValue());
            em.merge(ti);
        }
    }
}
