package com.flecharoja.loyalty.service;

import com.flecharoja.loyalty.model.Miembro;
import com.flecharoja.loyalty.model.MiembroData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author svargas
 */
@Stateless
public class MiembroBean {
    
    private final Configuration configuration;
    
    @PersistenceContext(unitName = "com.flecharoja_loyalty-api-client_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public MiembroBean() {
        this.configuration = new Configuration();
        this.configuration.addResource("conf/hbase/hbase-site.xml");
    }

    public List<Miembro> getMiembrosNotificacion (String idNotificacion, List<String> miembros) throws IOException {
        if (miembros==null || miembros.isEmpty()) {
            miembros = em.createNamedQuery("NotificacionListaMiembro.getIdMiembrosByIdNotificacion").setParameter("idNotificacion", idNotificacion).getResultList();
            miembros.addAll(getIdMiembrosEligibles(em.createNamedQuery("NotificacionListaSegmento.getIdSegmentosByIdNotificacion").setParameter("idNotificacion", idNotificacion).getResultList()));
        }
        return miembros.stream().distinct().map((idMiembro) -> em.find(Miembro.class, idMiembro)).filter((miembro) -> miembro!=null).collect(Collectors.toList());
    }
    
    public List<Miembro> getMiembrosNotificacion (List<String> miembros) {
        return miembros.stream().distinct().map((idMiembro) -> em.find(Miembro.class, idMiembro)).filter((miembro) -> miembro!=null).collect(Collectors.toList());
    }
    
    private List<String> getIdMiembrosEligibles(List<String> segmentos) throws IOException {
        List<String> miembros = new ArrayList<>();
        System.out.println("Segmentos: " + segmentos.size());
        if (!segmentos.isEmpty()) {
            try (Connection connection = ConnectionFactory.createConnection(configuration)) {
                Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "ELEGIBLES_SEGMENTO"));

                List<Get> gets = new ArrayList<>();
                segmentos.forEach((idSegmento) -> {
                    gets.add(new Get(Bytes.toBytes(idSegmento)));
                });

                for (Result result : table.get(gets)) {
                    System.out.println("result: " + result.toString());
                    if (!result.isEmpty()) {
                        result.getFamilyMap(Bytes.toBytes("MIEMBROS")).entrySet().stream().filter((entry) -> (Bytes.toBoolean(entry.getValue()))).forEach((entry) -> {
                            miembros.add(Bytes.toString(entry.getKey()));
                        });
                    }
                }
            }
        }
        
        return miembros;
    }
    
    public void updateMiembroFCMToken(String idMiembro, String fcmToken) {
        if (fcmToken!=null) {
            List<Miembro> miembrosPasados = em.createNamedQuery("Miembro.findByFcmTokenRegistro").setParameter("fcmTokenRegistro", fcmToken).getResultList();
            miembrosPasados.forEach((miembroPasado) -> {
                miembroPasado.setFcmTokenRegistro(null);
                em.merge(miembroPasado);
            });
        }
        Miembro miembro = em.find(Miembro.class, idMiembro);
        miembro.setFcmTokenRegistro(fcmToken);
        em.merge(miembro);
    }
}
