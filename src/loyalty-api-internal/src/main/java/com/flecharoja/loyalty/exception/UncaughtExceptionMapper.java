package com.flecharoja.loyalty.exception;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author svargas
 */
@Provider
public class UncaughtExceptionMapper implements ExceptionMapper<Throwable>{

    @Override
    public Response toResponse(Throwable exception) {
        if (exception instanceof WebApplicationException) {
            return ((WebApplicationException) exception).getResponse();
        }
        
        if (exception instanceof MyException) {
            return ((MyException) exception).getResponse();
        }
        
        if (exception instanceof javax.json.JsonException
                || exception instanceof com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException
                || exception instanceof org.codehaus.jackson.map.exc.UnrecognizedPropertyException) {
            Logger.getLogger(exception.getClass().getName()).log(Level.SEVERE, null, exception);
            return new MyException(MyException.ErrorPeticion.ENTIDAD_NO_VALIDA, Locale.US, "data_format_invalid").getResponse();
        }
        
        Logger.getLogger(exception.getClass().getName()).log(Level.SEVERE, null, exception);
        return new MyException(MyException.ErrorSistema.ERROR_DESCONOCIDO, Locale.US, "unknown_error").getResponse();
    }
    
}
