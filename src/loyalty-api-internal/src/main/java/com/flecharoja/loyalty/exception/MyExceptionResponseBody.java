package com.flecharoja.loyalty.exception;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MyExceptionResponseBody {
    
    private int code;
    private String message;
    private String lang;

    public MyExceptionResponseBody() {
    }

    public MyExceptionResponseBody(int code, String message) {
        this.code = code;
        this.message = message;
        this.lang = "en";
    }

    public MyExceptionResponseBody(int code, String message, String lang) {
        this.code = code;
        this.message = message;
        this.lang = lang;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
    
}
