package com.flecharoja.loyalty.storm.exception;

import com.flecharoja.loyalty.storm.util.CodigosErrores;
import javax.ejb.ApplicationException;

/**
 *
 * @author svargas
 */
@ApplicationException(rollback = true)
public class ErrorSistemaException extends Exception {
    
    public ErrorSistemaException(CodigosErrores.Sistema codigo) {
        super(CodigosErrores.ESTADO_ERROR_SISTEMA);
    }
    public ErrorSistemaException(CodigosErrores.Sistema codigo, String mensaje) {
        super(CodigosErrores.ESTADO_ERROR_SISTEMA+" "+mensaje);
    }
}
