package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas, wtencio
 */
@Entity
@Table(name = "NIVEL_METRICA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NivelMetrica.findAll", query = "SELECT n FROM NivelMetrica n"),
    @NamedQuery(name = "NivelMetrica.findNivelesByIdGrupo", query = "SELECT n FROM NivelMetrica n WHERE n.grupoNiveles.idGrupoNivel = :idGrupoNivel ORDER BY n.metricaInicial"),
    @NamedQuery(name = "NivelMetrica.countNivelGrupo", query = "SELECT COUNT(n) FROM NivelMetrica n WHERE n.grupoNiveles.idGrupoNivel = :idGrupoNivel AND n.metricaInicial = :metricaInicial")
})
public class NivelMetrica implements Serializable {

    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @OneToMany(mappedBy = "idNivelMinimo")
    private List<Producto> productoList;


    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "nivel_uuid")
    @GenericGenerator(name = "nivel_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_NIVEL")
    private String idNivel;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Size(min = 1, max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "METRICA_INICIAL")
    private BigInteger metricaInicial;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    
    @OneToMany(mappedBy = "nivelMetrica")
    private List<ReglaMiembroAtb> reglaMiembroAtbList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idNivel")
    private List<MiembroMetricaNivel> miembroMetricaNivelList;

    @JoinColumn(name = "GRUPO_NIVELES", referencedColumnName = "ID_GRUPO_NIVEL")
    @ManyToOne(optional = false)
    private GrupoNiveles grupoNiveles;  
  
    public NivelMetrica() {
    }

    public NivelMetrica(Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public String getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(String idNivel) {
        this.idNivel = idNivel;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getMetricaInicial() {
        return metricaInicial;
    }

    public void setMetricaInicial(BigInteger metricaInicial) {
        this.metricaInicial = metricaInicial;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }
    
    public GrupoNiveles getGrupoNiveles() {
        return grupoNiveles;
    }

    public void setGrupoNiveles(GrupoNiveles grupoNiveles) {
        this.grupoNiveles = grupoNiveles;
    }

    
    @XmlTransient
    public List<MiembroMetricaNivel> getMiembroMetricaNivelList() {
        return miembroMetricaNivelList;
    }

    public void setMiembroMetricaNivelList(List<MiembroMetricaNivel> miembroMetricaNivelList) {
        this.miembroMetricaNivelList = miembroMetricaNivelList;
    }

    
    @XmlTransient
    public List<ReglaMiembroAtb> getReglaMiembroAtbList() {
        return reglaMiembroAtbList;
    }

    public void setReglaMiembroAtbList(List<ReglaMiembroAtb> reglaMiembroAtbList) {
        this.reglaMiembroAtbList = reglaMiembroAtbList;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivel != null ? idNivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NivelMetrica)) {
            return false;
        }
        NivelMetrica other = (NivelMetrica) object;
        if ((this.idNivel == null && other.idNivel != null) || (this.idNivel != null && !this.idNivel.equals(other.idNivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Nivel[ idNivel=" + idNivel + " ]";
    }

    
    @XmlTransient
    public List<Producto> getProductoList() {
        return productoList;
    }

    public void setProductoList(List<Producto> productoList) {
        this.productoList = productoList;
    }
    
}
