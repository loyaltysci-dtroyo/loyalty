package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.exception.RecursoNoEncontradoException;
import com.flecharoja.loyalty.storm.model.Miembro;
import com.flecharoja.loyalty.storm.model.Promocion;
import com.flecharoja.loyalty.storm.model.PromocionListaMiembro;
import com.flecharoja.loyalty.storm.util.CodigosErrores;
import com.flecharoja.loyalty.storm.util.Indicadores;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * EJB encargado del manejo de datos para el mantenimiento de promociones
 *
 * @author svargas
 */
@Stateless
public class PromocionService {

    /**
     * Metodo que obtiene un listado de promociones en un rango de numero de
     * registros y opcionalmente por estado
     *
     * @param estado Texto con los estados deseados
     * @return Respuesta con un listado de promociones y el rango de registros
     * obtenidos
     */
    public List<Promocion> getPromociones(String estado) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultado = null;

        try {
            if (estado == null) {//en el caso de que no se haya establecido ningun estado, se recuperan todos
                resultado = em.createNamedQuery("Promocion.findAll").getResultList();
            } else {
                //se define el query
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery<Object> query = cb.createQuery();
                Root<Promocion> root = query.from(Promocion.class);

                //se forman los predicados con los estados deseados en la peticion
                List<Predicate> predicates = new ArrayList<>();
                for (String e : estado.split("-")) {
                    predicates.add(cb.equal(root.get("indEstado"), e.toUpperCase().charAt(0)));
                }
                //se forma la clausula WHERE con los predicados formados
                query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
                //se obtiene los registros en un rango valido
                resultado = em.createQuery(query.select(root)).getResultList();
            }
        } catch (IllegalArgumentException e) {//en el caso de argumentos validos (paginacion)
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        em.close();
        return resultado;
    }

    /**
     * Metodo que obtiene la informacion de una promocion por el identificador
     * de la misma
     *
     * @param idPromocion Identificador de la promocion
     * @return Respuesta con la informacion de la promocion encontrada
     */
    public Promocion getPromocionPorId(String idPromocion) throws RecursoNoEncontradoException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Promocion promocion = em.find(Promocion.class, idPromocion);
        //verificacion de que la entidad exista
        if (promocion == null) {
            throw new RecursoNoEncontradoException();
        }
        em.close();
        return promocion;
    }

    /**
     * Metodo que realiza una busqueda de promociones por palabras claves dentro
     * de "busqueda" y retorna un listado de promociones que concuerdan en un
     * rango y opcionalmente por estado(s)
     *
     * @param busqueda Variable de texto con las palabras claves
     * @param estado Estados de promocion deseados
     * @return Respuesta con el listado de promociones y el rango de los
     * registros
     */
    public List<Promocion> searchPromocion(String busqueda, String estado) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultado = null;
        //division del texto de busqueda en un arreglos de palabras claves (divididas por espacio)
        String[] palabrasClaves = busqueda.split(" ");

        //creacion del query
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Promocion> root = query.from(Promocion.class);

        //definicion de los predicados para la comparacion de palabras claves con una serie de columnas
        List<Predicate> predicates = new ArrayList<>();
        for (String palabraClave : palabrasClaves) {
            predicates.add(cb.like(cb.upper(root.get("nombre")), "%" + palabraClave.toUpperCase() + "%"));
            predicates.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + palabraClave.toUpperCase() + "%"));
            predicates.add(cb.like(cb.upper(root.get("descripcion")), "%" + palabraClave.toUpperCase() + "%"));
            predicates.add(cb.like(cb.upper(root.get("tags")), "%" + palabraClave.toUpperCase() + "%"));
        }
        //establecimiento de la clausula WHERE
        if (estado == null) {//en el caso de que no se establecio alguno estado
            query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
        } else {
            List<Predicate> predicates2 = new ArrayList<>();
            for (String e : estado.split("-")) {
                predicates2.add(cb.equal(root.get("indEstado"), e.toUpperCase().charAt(0)));
            }
            query.where(cb.or(predicates2.toArray(new Predicate[predicates2.size()])), cb.or(predicates.toArray(new Predicate[predicates.size()])));
        }
        try {
            //obtencion del resultado en un rango valido
            resultado = em.createQuery(query.select(root)).getResultList();
        } catch (IllegalArgumentException e) {//paginacion erronea
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        em.close();
        return resultado;
    }

    public void agregarMiembroElegibles(String idPromocion, String idMiembro) throws ErrorSistemaException {
        this.includeExcludeMiembro(idPromocion, idMiembro, Indicadores.INCLUIDO, idMiembro);
    }

    /**
     * Metodo que asigna (incluye o excluye) a un miembro de una promocion
     *
     * @param idPromocion Identificador de la promocion
     * @param idMiembro Identificador del miembro
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesion
     */
    public void includeExcludeMiembro(String idPromocion, String idMiembro, Character indAccion, String usuario) throws ErrorSistemaException {
        try {
            EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            //verificacion que los id's sean validos
            Promocion promocion = em.find(Promocion.class, idPromocion);
            em.getReference(Miembro.class, idMiembro).getIdMiembro();
            if (promocion == null) {
                LOG.log(Level.SEVERE, "La promocion es nula", new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA));
                em.getTransaction().commit();
                return;
            }
            //verificacion de entidad archivada
            if (promocion.getIndEstado().equals(Promocion.Estados.ARCHIVADO.getValue())) {
                LOG.log(Level.SEVERE, "La promocion esta archivada: " + promocion.getIdPromocion(), new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA));
                em.getTransaction().commit();
                return;
            }
            Date date = Calendar.getInstance().getTime();
            //verificacion que el indicador de accion sea uno valido
            if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
                PromocionListaMiembro promocionListaMiembro = new PromocionListaMiembro(idMiembro, idPromocion, indAccion, date, usuario);
                em.merge(promocionListaMiembro);
                em.flush();
                em.clear();
            } else {
                LOG.log(Level.SEVERE, "Excepcion en com.flecharoja.loyalty.storm.PromocionService.java", new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA));

            }
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al agregar miembro elegible a promocion", ex);
        }
    }
    private static final Logger LOG = Logger.getLogger(PromocionService.class.getName());

}
