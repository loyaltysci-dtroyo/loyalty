package com.flecharoja.loyalty.storm;

import com.flecharoja.loyalty.storm.filters.EventosSistemaFilter;
import com.flecharoja.loyalty.storm.functions.DisparadorTareasFunction;
import com.flecharoja.loyalty.storm.functions.EventosSistemaParserFunction;
import com.flecharoja.loyalty.storm.functions.ProcesadorTareasFunction;
import com.flecharoja.loyalty.storm.functions.ProcesamientoTareaParserFunction;
import com.flecharoja.loyalty.storm.mappers.ReglasTupleToKafkaMapper;
import com.flecharoja.loyalty.storm.mappers.TareasTupleToKafkaMapper;
import org.apache.storm.kafka.BrokerHosts;
import org.apache.storm.kafka.ZkHosts;
import org.apache.storm.kafka.trident.TridentKafkaConfig;
import com.flecharoja.loyalty.storm.util.Indicadores;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.kafka.trident.OpaqueTridentKafkaSpout;
import org.apache.storm.kafka.trident.TridentKafkaStateFactory;
import org.apache.storm.kafka.trident.TridentKafkaUpdater;
import org.apache.storm.kafka.trident.selector.DefaultTopicSelector;
import org.apache.storm.trident.Stream;
import org.apache.storm.trident.TridentTopology;
import org.apache.storm.tuple.Fields;

/**
 * Clase rincipal encargada de stablecer la configuracion del cluster de storm
 * asi como iniciar la ejecución del mismo
 *
 * @author faguilar
 */
public class Main {

    public static void main(String[] ars) {
        //LocalCluster cluster = new LocalCluster();
        Properties props = new Properties();//props para el producer
        Config config = new Config();//config de la topologia 

        BrokerHosts hosts = new ZkHosts(Indicadores.ZK_CONN_STRING);
        TridentTopology topologiaEventos = new TridentTopology();

        //config para los spouts de kafka
        TridentKafkaConfig kafkaConfigEventos = new TridentKafkaConfig(hosts, Indicadores.KAFKA_TOPIC_EVENTOS, "loyalty-storm-eventos");

        kafkaConfigEventos.startOffsetTime = -1;

        TridentKafkaConfig kafkaConfigTareas = new TridentKafkaConfig(hosts, Indicadores.KAFKA_TOPIC_WORKFLOW, "loyalty-storm-tareas");
        kafkaConfigTareas.startOffsetTime = -1;
        //config para el producer de kafka
        props.put("bootstrap.servers", Indicadores.BOOTSTRAP_SERVERS);
        props.put("request.required.acks", "1");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        //eventos
        TridentKafkaStateFactory eventosStateFactory = new TridentKafkaStateFactory()
                .withProducerProperties(props)
                .withKafkaTopicSelector(new DefaultTopicSelector(Indicadores.KAFKA_TOPIC_SPARK))
                .withTridentTupleToKafkaMapper(new ReglasTupleToKafkaMapper());

//        tareas
        TridentKafkaStateFactory tareasStateFactory = new TridentKafkaStateFactory()
                .withProducerProperties(props)
                .withKafkaTopicSelector(new DefaultTopicSelector(Indicadores.KAFKA_TOPIC_WORKFLOW))
                .withTridentTupleToKafkaMapper(new TareasTupleToKafkaMapper());
        //SPOUT EVENTOS
        OpaqueTridentKafkaSpout kafkaSpoutEventos = new OpaqueTridentKafkaSpout(kafkaConfigEventos);
        //SPOUT TAREAS
        OpaqueTridentKafkaSpout kafkaSpoutTareas = new OpaqueTridentKafkaSpout(kafkaConfigTareas);

        //PROCESAMIENTO DE REGLAS DE SEGMENTO
        Stream eventosValidados = topologiaEventos.newStream(Indicadores.KAFKA_TOPIC_EVENTOS, kafkaSpoutEventos)
                .each(new Fields("bytes"), new EventosSistemaFilter())
                .each(new Fields("bytes"), new EventosSistemaParserFunction(), new Fields("indEvento", "idMiembro", "idElemento"));

        Stream tareasEjecutar = eventosValidados
                .each(new Fields("indEvento", "idMiembro", "idElemento"), new DisparadorTareasFunction(), new Fields("idNodo"));
//        Stream asignacionInsignias  = eventosValidados
//                .each(new Fields("indEvento", "idMiembro", "idElemento"), new AsignacionBadgeFunction(), new Fields("idNodo"));
//
//        Stream reglasActualizar = eventosValidados
//                .each(new Fields("indEvento", "idMiembro", "idElemento"), new ActualizadorReglasFunction(), new Fields("indTipoRegla", "idRegla"));
        //envia a procesar las reglas
//        reglasActualizar
//                .partitionPersist(eventosStateFactory, new Fields("indTipoRegla", "idRegla", "idMiembro"), new TridentKafkaUpdater(), new Fields("indTipoRegla", "idRegla", "idMiembro"));
        //persiste los eventos de procesameinto de workflow generados a partir del disparador de tarea a un topic de kafka
        tareasEjecutar
                .partitionPersist(tareasStateFactory, new Fields("idNodo", "idMiembro"), new TridentKafkaUpdater(), new Fields("idNodo", "idMiembro"));
//        //PROCESAMIENTO DE WORKFLOW
        Stream streamProcesamientoTareas = topologiaEventos.newStream(Indicadores.KAFKA_TOPIC_WORKFLOW, kafkaSpoutTareas)
                .each(new Fields("bytes"), new ProcesamientoTareaParserFunction(), new Fields("idNodo", "idMiembro"))
                .each(new Fields("idNodo", "idMiembro"), new ProcesadorTareasFunction(), new Fields("idNodoSiguiente"));

        streamProcesamientoTareas
                .partitionPersist(tareasStateFactory, new Fields("idNodoSiguiente", "idMiembro"), new TridentKafkaUpdater(), new Fields("idNodo", "idMiembro"));
        StormTopology build = topologiaEventos.build();

        config.put(Config.TOPOLOGY_DEBUG, false);
        config.setDebug(false);
        config.setNumAckers(1);
        config.setNumWorkers(1);

        config.setNumEventLoggers(1);
        config.setMaxTaskParallelism(1);
        config.setTopologyWorkerMaxHeapSize(250000);
        //cluster.submitTopology("loyalty-storm", config, build);
        try {
            StormSubmitter.submitTopology("loyalty-storm", config, build);
        } catch (AlreadyAliveException | InvalidTopologyException | AuthorizationException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
