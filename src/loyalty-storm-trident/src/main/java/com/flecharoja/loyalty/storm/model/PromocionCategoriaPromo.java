package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PROMOCION_CATEGORIA_PROMO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PromocionCategoriaPromo.findAll", query = "SELECT c FROM PromocionCategoriaPromo c"),
    @NamedQuery(name = "PromocionCategoriaPromo.findByIdCategoriaIdPromocion", query = "SELECT c FROM PromocionCategoriaPromo c WHERE c.categoriaPromocionPK.idCategoria = :idCategoria AND c.categoriaPromocionPK.idPromocion = :idPromocion"),
    @NamedQuery(name = "PromocionCategoriaPromo.findByIdCategoria", query = "SELECT c FROM PromocionCategoriaPromo c WHERE c.categoriaPromocionPK.idCategoria = :idCategoria"),
    @NamedQuery(name = "PromocionCategoriaPromo.findByIdPromocion", query = "SELECT c FROM PromocionCategoriaPromo c WHERE c.categoriaPromocionPK.idPromocion = :idPromocion"),
    @NamedQuery(name = "PromocionCategoriaPromo.findCategoriasByIdPromocion", query = "SELECT c.categoria FROM PromocionCategoriaPromo c WHERE c.categoriaPromocionPK.idPromocion = :idPromocion"),
    @NamedQuery(name = "PromocionCategoriaPromo.findPromocionesByIdCategoria", query = "SELECT c.promocion FROM PromocionCategoriaPromo c WHERE c.categoriaPromocionPK.idCategoria = :idCategoria"),
    @NamedQuery(name = "PromocionCategoriaPromo.countByIdPromocion", query = "SELECT COUNT(c) FROM PromocionCategoriaPromo c WHERE c.categoriaPromocionPK.idPromocion = :idPromocion"),
    @NamedQuery(name = "PromocionCategoriaPromo.countByIdCategoria", query = "SELECT COUNT(c) FROM PromocionCategoriaPromo c WHERE c.categoriaPromocionPK.idCategoria = :idCategoria")})
public class PromocionCategoriaPromo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected PromocionCategoriaPromoPK categoriaPromocionPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_CATEGORIA", referencedColumnName = "ID_CATEGORIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CategoriaPromocion categoria;
    
    @JoinColumn(name = "ID_PROMOCION", referencedColumnName = "ID_PROMOCION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Promocion promocion;

    public PromocionCategoriaPromo() {
    }

    public PromocionCategoriaPromo(String idCategoria, String idPromocion, Date fechaCreacion, String usuarioCreacion) {
        this.categoriaPromocionPK = new PromocionCategoriaPromoPK(idCategoria, idPromocion);
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public PromocionCategoriaPromoPK getCategoriaPromocionPK() {
        return categoriaPromocionPK;
    }

    public void setCategoriaPromocionPK(PromocionCategoriaPromoPK categoriaPromocionPK) {
        this.categoriaPromocionPK = categoriaPromocionPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public CategoriaPromocion getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriaPromocion categoria) {
        this.categoria = categoria;
    }

    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoriaPromocionPK != null ? categoriaPromocionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromocionCategoriaPromo)) {
            return false;
        }
        PromocionCategoriaPromo other = (PromocionCategoriaPromo) object;
        if ((this.categoriaPromocionPK == null && other.categoriaPromocionPK != null) || (this.categoriaPromocionPK != null && !this.categoriaPromocionPK.equals(other.categoriaPromocionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CategoriaPromocion[ categoriaPromocionPK=" + categoriaPromocionPK + " ]";
    }
    
}
