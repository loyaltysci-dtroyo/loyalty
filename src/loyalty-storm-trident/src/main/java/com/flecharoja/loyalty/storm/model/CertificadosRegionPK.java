/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.metamodel.SingularAttribute;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author wtencio
 */
@Embeddable
public class CertificadosRegionPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_REGION")
    private String idRegion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "COD_CERTIFICADO")
    private String codCertificado;

    public CertificadosRegionPK() {
    }

    public CertificadosRegionPK(String idRegion, String codCertificado) {
        this.idRegion = idRegion;
        this.codCertificado = codCertificado;
    }

   

    public String getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(String idRegion) {
        this.idRegion = idRegion;
    }

    public String getCodCertificado() {
        return codCertificado;
    }

    public void setCodCertificado(String codCertificado) {
        this.codCertificado = codCertificado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegion != null ? idRegion.hashCode() : 0);
        hash += (codCertificado != null ? codCertificado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CertificadosRegionPK)) {
            return false;
        }
        CertificadosRegionPK other = (CertificadosRegionPK) object;
        if ((this.idRegion == null && other.idRegion != null) || (this.idRegion != null && !this.idRegion.equals(other.idRegion))) {
            return false;
        }
        if ((this.codCertificado == null && other.codCertificado != null) || (this.codCertificado != null && !this.codCertificado.equals(other.codCertificado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CertificadosComercioPK[ idRegion=" + idRegion + ", codCertificado=" + codCertificado + " ]";
    }
    
}
