package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.model.GrupoMiembro;
import com.flecharoja.loyalty.storm.model.Miembro;
import com.flecharoja.loyalty.storm.util.CodigosErrores;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia
 * relacionados a miembros
 *
 * @author svargas, wtencio
 */
public class MiembroService {
    /**
     * Metodo que obtiene todos los miembros almacenados
     *
     * @param estado Valores de los indicadores de estado a filtrar
     * @return Respuesta con el listado de todos los miembros encontrados y su
     * rango
     */
    public List<Miembro> getMiembros(String estado) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List<Miembro> resultado = null;
        Long total;

        //en el caso de que no se haya solicitado filtro por algun estado, se retornan todos
        if (estado == null) {
            total = ((Long) em.createNamedQuery("Miembro.countAll").getSingleResult());
            total -= total - 1 < 0 ? 0 : 1;

            //recuperacion de los miembros en un rango valido
            resultado = em.createNamedQuery("Miembro.findAll").getResultList();
        } else {
            //definicion del query
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<Miembro> root = query.from(Miembro.class);
            //creacion de los predicados segun los estados
            List<Predicate> predicates = new ArrayList<>();
            for (String e : estado.split("-")) {
                predicates.add(cb.equal(root.get("indEstadoMiembro"), e.toUpperCase().charAt(0)));

            }
            //asignacion de los predicados a la clausula de WHERE
            query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));

            //obtencion del total
            total = (Long) em.createQuery(query.select(cb.count(root))).getSingleResult();
            total -= total - 1 < 0 ? 0 : 1;

            //obtencion de los miembros en un rango valido
            List<Object> temp = em.createQuery(query.select(root))
                    .getResultList();
            //casteo de los resultados a Miembro
            resultado = temp.stream().map((Object t) -> (Miembro) t).collect(Collectors.toList());
        }
        em.close(); 
        return resultado;
    }

    /**
     * Metodo que realiza una busqueda de miembros a partir de una cadena de
     * texto conformada de palabras claves comparadas con ciertos atributos
     *
     * @param busqueda Cadena de texto que contiene las palabras claves a buscar
     * @param estado Estados de los miembros deseados
     * @return Respuesta con el listado de miembros encontrados
     */
    public List<Miembro> searchMiembros(String busqueda, String estado) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List<Miembro> resultado = null;

        //division de las palabras claves
        String[] palabrasClaves = busqueda.split(" ");

        //definicion del query
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Miembro> root = query.from(Miembro.class);
        //creacion de los predicados
        List<Predicate> predicates = new ArrayList<>();
        for (String palabraClave : palabrasClaves) {
            predicates.add(cb.like(cb.lower(root.get("nombre")), "%" + palabraClave.toLowerCase() + "%"));
            predicates.add(cb.like(cb.lower(root.get("apellido")), "%" + palabraClave.toLowerCase() + "%"));
            predicates.add(cb.like(cb.lower(root.get("apellido2")), "%" + palabraClave.toLowerCase() + "%"));
            predicates.add(cb.like(cb.lower(root.get("docIdentificacion")), "%" + palabraClave.toLowerCase() + "%"));
        }

        //verificacion de casos donde no se filtren por estado
        if (estado == null) {
            query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
        } else {
            List<Predicate> predicates2 = new ArrayList<>();
            for (String e : estado.split("-")) {
                predicates2.add(cb.equal(root.get("indEstadoMiembro"), e.toUpperCase().charAt(0)));
            }
            query.where(cb.or(predicates2.toArray(new Predicate[predicates2.size()])), cb.or(predicates.toArray(new Predicate[predicates.size()])));
        }

        //obtencion del resultado en un rango valido
        resultado = em.createQuery(query.select(root))
                .getResultList()
                .stream().map((Object t) -> (Miembro) t).collect(Collectors.toList());
        em.close();
        return resultado;
    }

    /**
     * Metodo que obtiene un miembro almacenados segun su id
     *
     * @param idMiembro Identificador de un miembro en la bases de datos
     * @return Respuesta con un miembro con toda su informacion
     */
    public Miembro getMiembro(String idMiembro) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Miembro miembro = em.find(Miembro.class, idMiembro);
        //verificacion que la entidad exista
        if (miembro == null) {
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        em.close();
        return miembro;
    }

    /**
     * Metodo que determina si un miembro pertenece a un grupo
     *
     * @param idGrupo id del grupo a consultar
     * @param idMiembro id del miembro a consultar
     * @return
     */
    public boolean miembroPerteneceGrupo(String idGrupo, String idMiembro) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List<GrupoMiembro> listaGrupoMiembro;
        listaGrupoMiembro = em.createNamedQuery("GrupoMiembro.findGruposByIdMiembroAndIdMiembro")
                .setParameter("idMiembro", idMiembro)
                .setParameter("idGrupo", idGrupo).getResultList();
        em.close();
        return listaGrupoMiembro != null && !listaGrupoMiembro.isEmpty();
        
    }

}
