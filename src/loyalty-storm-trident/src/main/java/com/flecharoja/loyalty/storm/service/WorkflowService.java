package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.exception.RecursoNoEncontradoException;
import com.flecharoja.loyalty.storm.model.Workflow;
import com.flecharoja.loyalty.storm.model.WorkflowNodo;
import com.flecharoja.loyalty.storm.model.WorkflowNodoEspera;
import com.flecharoja.loyalty.storm.model.WorkflowNodoEsperaPK;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * flujos de trabajo (workflows)
 *
 * @author faguilar
 */
@Stateless
public class WorkflowService {

    /**
     * Metodo que obtiene una lista de workflows almacenados por estado(s)
     *
     * @param estado Valores de los indicadores de estado a buscar
     * @return Respuesta con una lista de workflows
     */
    public List<Workflow> getWorkflows(String estado) {
        List resultado = null;
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        if (estado == null) {//en el caso de que no se haya establecido algun estado, se recuperan todos
            resultado = em.createNamedQuery("Workflow.findAll").getResultList();
        } else {
            //definicion del query
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<Workflow> root = query.from(Workflow.class);

            //creacion de los predicados comparando los estados con los almacenados
            List<Predicate> predicates = new ArrayList<>();
            for (String e : estado.split("-")) {
                if (!e.equals("")) {
                    predicates.add(cb.equal(root.get("indEstado"), e.toUpperCase().charAt(0)));
                }
            }
            //asignacion de los predicados a la clausula WHERE
            query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
            //recuperacion de la lista de workflows en un rango valido
            resultado = em.createQuery(query.select(root)).getResultList();
        }

        return resultado;
    }

    /**
     * Metodo que obtiene un workflow por su numero de identificacion
     *
     * @param idWorkflow Identificacion del workflow
     * @return Respuesta con la informacion del workflow
     */
    public Workflow getWorkflowPorId(String idWorkflow) throws RecursoNoEncontradoException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Workflow workflow = em.find(Workflow.class, idWorkflow);
        //verificacion que el workflow exista
        if (workflow == null) {
            throw new RecursoNoEncontradoException();
        }
        return workflow;
    }

    public WorkflowNodo getNodoEspera(String idNodo) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        return em.find(WorkflowNodo.class, idNodo);
    }

    /**
     * Metodo que realiza una busqueda en base a palabras claves sobre una serie
     * de atributos, opcionalmente por estados y en un rango de registros
     *
     * @param busqueda Cadena de texto con las palabras claves
     * @param estados Indicador de los estados de los workflows deseados
     * @return Listado de todos los workflows encontrados
     */
    public List<Workflow> searchWorkflows(String busqueda, String estados) throws ErrorSistemaException {
        List resultado;
        String[] palabrasClaves = busqueda.split(" ");
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        //definicion del query
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Workflow> root = query.from(Workflow.class);

        //definicion de los predicados de comparacion de las palabras claves con un numero de columnas seleccionadas
        List<Predicate> predicates = new ArrayList<>();
        for (String palabraClave : palabrasClaves) {
            predicates.add(cb.like(cb.upper(root.get("nombre")), "%" + palabraClave.toUpperCase() + "%"));
            predicates.add(cb.like(cb.upper(root.get("nombreInterno")), "%" + palabraClave.toUpperCase() + "%"));
        }
        //establecimiento de la clausula WHERE
        if (estados == null) {//en el caso de que no se hayan pasado algun estado, se ignora el filtrado por los mismos
            query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
        } else {
            //definicion de predicado adicional para el filtrado por estados
            List<Predicate> predicates2 = new ArrayList<>();
            for (String e : estados.split("-")) {
                predicates2.add(cb.equal(root.get("indEstado"), e.toUpperCase()));
            }
            query.where(cb.or(predicates2.toArray(new Predicate[predicates2.size()])), cb.or(predicates.toArray(new Predicate[predicates.size()])));
        }
        //recuperacion de la lista de resultados en un rango valido
        resultado = em.createQuery(query.select(root)).getResultList();
        em.close();
        return resultado;

    }

    /**
     * Metodo encargado de almacenar el estado de un nodo de workflow para ser
     * procesado una vez la senal de completado es recibida
     *
     * @param idNodo id del nodo a almacenar
     * @param idMiembro id del miembro asociado a la tarea en ejecucion
     * @throws com.flecharoja.loyalty.storm.exception.ErrorSistemaException
     */
    public void almacenarTareaWorkflow(String idNodo, String idMiembro) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        WorkflowNodoEspera nodo = em.find(WorkflowNodoEspera.class, new WorkflowNodoEsperaPK(idNodo, idMiembro));
        if (nodo == null) {
            WorkflowNodoEspera nodoEspera = new WorkflowNodoEspera(new WorkflowNodoEsperaPK(idNodo, idMiembro));
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            em.persist(nodoEspera);
            em.flush();
            em.getTransaction().commit();
            em.clear();
            em.close();
        }
    }
}
