/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "MISION_JUEGO_PREMIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MisionJuegoPremio.findAll", query = "SELECT m FROM MisionJuegoPremio m"),
    @NamedQuery(name = "MisionJuegoPremio.countAll", query = "SELECT COUNT(m) FROM MisionJuegoPremio m"),
    @NamedQuery(name = "MisionJuegoPremio.findByIdJuego", query = "SELECT m FROM MisionJuegoPremio m WHERE m.idJuego = :idJuego"),
    @NamedQuery(name = "MisionJuegoPremio.findByIdMision", query = "SELECT m FROM MisionJuegoPremio m WHERE m.idMision.idMision = :idMision")
})
public class MisionJuegoPremio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "mision_juego_uuid")
    @GenericGenerator(name = "mision_juego_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_JUEGO")
    private String idJuego;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_PREMIO")
    private Character indTipoPremio;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "PROBABILIDAD")
    private int probabilidad;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_RESPUESTA")
    private Character indRespuesta;
    
    @Column(name = "CANT_INTER_TOTAL")
    private BigInteger cantInterTotal;
    
    @Column(name = "IND_INTER_TOTAL")
    private Character indInterTotal;
    
    @Column(name = "CANT_INTER_MIEMBRO")
    private BigInteger cantInterMiembro;
    
    @Column(name = "IND_INTER_MIEMBRO")
    private Character indInterMiembro;
    
    @Column(name = "CANT_METRICA")
    private BigInteger cantMetrica;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @JoinColumn(name = "ID_MISION", referencedColumnName = "ID_MISION")
    @ManyToOne(optional = false)
    private Mision idMision;
    @JoinColumn(name = "ID_PREMIO", referencedColumnName = "ID_PREMIO")
    @ManyToOne
    private Premio idPremio;

    public MisionJuegoPremio() {
    }

    public MisionJuegoPremio(String idJuego) {
        this.idJuego = idJuego;
    }

    public MisionJuegoPremio(String idJuego, Character indTipoPremio, int probabilidad, Character indRespuesta, String usuarioCreacion, Date fechaCreacion, String usuarioModificacion, Date fechaModificacion, Long numVersion) {
        this.idJuego = idJuego;
        this.indTipoPremio = indTipoPremio;
        this.probabilidad = probabilidad;
        this.indRespuesta = indRespuesta;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaCreacion = fechaCreacion;
        this.usuarioModificacion = usuarioModificacion;
        this.fechaModificacion = fechaModificacion;
        this.numVersion = numVersion;
    }

    public String getIdJuego() {
        return idJuego;
    }

    public void setIdJuego(String idJuego) {
        this.idJuego = idJuego;
    }

    public Character getIndTipoPremio() {
        return indTipoPremio;
    }

    public void setIndTipoPremio(Character indTipoPremio) {
        this.indTipoPremio = indTipoPremio;
    }

    public int getProbabilidad() {
        return probabilidad;
    }

    public void setProbabilidad(int probabilidad) {
        this.probabilidad = probabilidad;
    }

    public Character getIndRespuesta() {
        return indRespuesta;
    }

    public void setIndRespuesta(Character indRespuesta) {
        this.indRespuesta = indRespuesta;
    }

    public BigInteger getCantInterTotal() {
        return cantInterTotal;
    }

    public void setCantInterTotal(BigInteger cantInterTotal) {
        this.cantInterTotal = cantInterTotal;
    }

    public Character getIndInterTotal() {
        return indInterTotal;
    }

    public void setIndInterTotal(Character indInterTotal) {
        this.indInterTotal = indInterTotal;
    }

    public BigInteger getCantInterMiembro() {
        return cantInterMiembro;
    }

    public void setCantInterMiembro(BigInteger cantInterMiembro) {
        this.cantInterMiembro = cantInterMiembro;
    }

    public Character getIndInterMiembro() {
        return indInterMiembro;
    }

    public void setIndInterMiembro(Character indInterMiembro) {
        this.indInterMiembro = indInterMiembro;
    }

    public BigInteger getCantMetrica() {
        return cantMetrica;
    }

    public void setCantMetrica(BigInteger cantMetrica) {
        this.cantMetrica = cantMetrica;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Mision getIdMision() {
        return idMision;
    }

    public void setIdMision(Mision idMision) {
        this.idMision = idMision;
    }

    public Premio getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(Premio idPremio) {
        this.idPremio = idPremio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idJuego != null ? idJuego.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionJuegoPremio)) {
            return false;
        }
        MisionJuegoPremio other = (MisionJuegoPremio) object;
        if ((this.idJuego == null && other.idJuego != null) || (this.idJuego != null && !this.idJuego.equals(other.idJuego))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionJuegoPremio[ idJuego=" + idJuego + " ]";
    }
    
}
