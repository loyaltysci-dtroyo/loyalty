/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.filters;

import org.apache.storm.trident.operation.BaseFilter;
import org.apache.storm.trident.tuple.TridentTuple;

/**
 *
 * @author faguilar
 */
public class TareasTerminadasFilter extends BaseFilter {

    @Override
    public boolean isKeep(TridentTuple tuple) { 
        return tuple.getStringByField("idNodo")==null;
    }
    
}
