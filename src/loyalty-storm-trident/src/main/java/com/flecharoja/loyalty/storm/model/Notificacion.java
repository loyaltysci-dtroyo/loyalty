package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NOTIFICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Notificacion.findAll", query = "SELECT n FROM Notificacion n"),
    @NamedQuery(name = "Notificacion.countAll", query = "SELECT COUNT(n) FROM Notificacion n"),
    @NamedQuery(name = "Notificacion.countByNombreInterno", query = "SELECT COUNT(n) FROM Notificacion n WHERE n.nombreInterno = :nombreInterno"),
    @NamedQuery(name = "Notificacion.findAllByIndEstado", query = "SELECT n FROM Notificacion n WHERE n.indEstado = :indEstado")
})
public class Notificacion implements Serializable {
    
    public enum Estados {
        PUBLICADO('P'),
        EJECUTADO('E'),
        ARCHIVADO('A'),
        BORRADOR('B');
        
        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Estados get (char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }
    
    public enum Eventos {
        MANUAL('M'),
        CALENDARIZADO('C'),
        DETONADOR('D');
        
        private final char value;
        private static final Map<Character, Eventos> lookup = new HashMap<>();

        private Eventos(char value) {
            this.value = value;
        }
        
        static {
            for (Eventos estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Eventos get (char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }
    
    public enum Tipos {
        NOTIFICACION_PUSH('P'),
        EMAIL('E');
        
        private final char value;
        private static final Map<Character, Tipos> lookup = new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }
        
        static {
            for (Tipos estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Tipos get (char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "notificacion_uuid")
    @GenericGenerator(name = "notificacion_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_NOTIFICACION")
    private String idNotificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;
   
    @Column(name = "TEXTO")
    private String texto;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Column(name = "IND_EVENTO")
    private Character indEvento;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_OBJETIVO")
    private Character indObjetivo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Column(name = "FECHA_EJECUCION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEjecucion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Version
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @Size(max = 500)
    @Column(name = "IMAGEN_ARTE")
    private String imagenArte;
    
    @Size(max = 150)
    @Column(name = "ENCABEZADO_ARTE")
    private String encabezadoArte;
    
    @Size(max = 40)
    @Column(name = "ID_ACTIVIDAD_OBJETIVO")
    private String idActividadObjetivo;
    
    @Size(max = 40)
    @Column(name = "ID_RECOMPENSA_OBJETIVO")
    private String idRecompensaObjetivo;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "notificacion")
    private List<NotificacionListaMiembro> notificacionListaMiembroList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "notificacion")
    private List<NotificacionListaSegmento> notificacionListaSegmentoList;
    
    @JoinColumn(name = "ID_PROMO_OBJETIVO", referencedColumnName = "ID_PROMOCION")
    @ManyToOne
    private Promocion promocion;

    public Notificacion() {
    }

    public Notificacion(String idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public String getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(String idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo!=null?Character.toUpperCase(indTipo):null;
    }

    public Character getIndEvento() {
        return indEvento;
    }

    public void setIndEvento(Character indEvento) {
        this.indEvento = indEvento!=null?Character.toUpperCase(indEvento):null;
    }

    public Character getIndObjetivo() {
        return indObjetivo;
    }

    public void setIndObjetivo(Character indObjetivo) {
        this.indObjetivo = indObjetivo!=null?Character.toUpperCase(indObjetivo):null;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado!=null?Character.toUpperCase(indEstado):null;
    }

    public Date getFechaEjecucion() {
        return fechaEjecucion;
    }

    public void setFechaEjecucion(Date fechaEjecucion) {
        this.fechaEjecucion = fechaEjecucion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getImagenArte() {
        return imagenArte;
    }

    public void setImagenArte(String imagenArte) {
        this.imagenArte = imagenArte;
    }

    public String getEncabezadoArte() {
        return encabezadoArte;
    }

    public void setEncabezadoArte(String encabezadoArte) {
        this.encabezadoArte = encabezadoArte;
    }

    public String getIdActividadObjetivo() {
        return idActividadObjetivo;
    }

    public void setIdActividadObjetivo(String idActividadObjetivo) {
        this.idActividadObjetivo = idActividadObjetivo;
    }

    public String getIdRecompensaObjetivo() {
        return idRecompensaObjetivo;
    }

    public void setIdRecompensaObjetivo(String idRecompensaObjetivo) {
        this.idRecompensaObjetivo = idRecompensaObjetivo;
    }
    
    
    @XmlTransient
    public List<NotificacionListaMiembro> getNotificacionListaMiembroList() {
        return notificacionListaMiembroList;
    }

    public void setNotificacionListaMiembroList(List<NotificacionListaMiembro> notificacionListaMiembroList) {
        this.notificacionListaMiembroList = notificacionListaMiembroList;
    }

    
    @XmlTransient
    public List<NotificacionListaSegmento> getNotificacionListaSegmentoList() {
        return notificacionListaSegmentoList;
    }

    public void setNotificacionListaSegmentoList(List<NotificacionListaSegmento> notificacionListaSegmentoList) {
        this.notificacionListaSegmentoList = notificacionListaSegmentoList;
    }

    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotificacion != null ? idNotificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notificacion)) {
            return false;
        }
        Notificacion other = (Notificacion) object;
        if ((this.idNotificacion == null && other.idNotificacion != null) || (this.idNotificacion != null && !this.idNotificacion.equals(other.idNotificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Notificacion[ idNotificacion=" + idNotificacion + " ]";
    }
    
}
