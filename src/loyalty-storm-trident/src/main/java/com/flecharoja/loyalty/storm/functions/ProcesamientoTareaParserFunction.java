/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.functions;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.apache.storm.trident.operation.BaseFunction;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;

/**
 *
 * @author faguilar
 * formato de la tupla: [idNodo:string, idMiembro:string]
 */
public class ProcesamientoTareaParserFunction extends BaseFunction  {

    @Override
    public void execute(TridentTuple tuple, TridentCollector collector) {
       try {
            String jsonInput = new String(tuple.getBinary(0));
            System.out.println("Parser Procesamiento Tareas:" +jsonInput);
            ObjectMapper m = new ObjectMapper();
            JsonNode rootNode = m.readTree(jsonInput);
            JsonNode idNodoNode = rootNode.path("idNodo");
            JsonNode idMiembroNode = rootNode.path("idMiembro");
            collector.emit(new Values(idNodoNode.asText(),idMiembroNode.asText()));
        } catch (Exception ex) {
           collector.reportError(ex);
//           throw new FailedException(ex);
        }
    }
    
}
