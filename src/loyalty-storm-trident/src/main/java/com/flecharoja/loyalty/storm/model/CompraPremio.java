
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "COMPRA_PREMIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompraPremio.findAll", query = "SELECT c FROM CompraPremio c"),
    @NamedQuery(name = "CompraPremio.findByIdTransaccion", query = "SELECT c FROM CompraPremio c WHERE c.idTransaccion = :idTransaccion")
})
public class CompraPremio implements Serializable {

    @JoinColumn(name = "ID_PREMIO", referencedColumnName = "ID_PREMIO")
    @ManyToOne(optional = false)
    private Premio idPremio;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_TRANSACCION")
    private String idTransaccion;
    
    @Size(max = 50)
    @Column(name = "CODIGO_CERTIFICADO")
    private String codigoCertificado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VALOR_EFECTIVO")
    private Double valorEfectivo;

    @JoinColumn(name = "ID_TRANSACCION", referencedColumnName = "ID_TRANSACCION", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private TransaccionCompra transaccionCompra;

    public CompraPremio() {
    }

    public CompraPremio(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public String getCodigoCertificado() {
        return codigoCertificado;
    }

    public void setCodigoCertificado(String codigoCertificado) {
        this.codigoCertificado = codigoCertificado;
    }

    public Double getValorEfectivo() {
        return valorEfectivo;
    }

    public void setValorEfectivo(Double valorEfectivo) {
        this.valorEfectivo = valorEfectivo;
    }


    public TransaccionCompra getTransaccionCompra() {
        return transaccionCompra;
    }

    public void setTransaccionCompra(TransaccionCompra transaccionCompra) {
        this.transaccionCompra = transaccionCompra;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTransaccion != null ? idTransaccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompraPremio)) {
            return false;
        }
        CompraPremio other = (CompraPremio) object;
        if ((this.idTransaccion == null && other.idTransaccion != null) || (this.idTransaccion != null && !this.idTransaccion.equals(other.idTransaccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CompraPremio[ idTransaccion=" + idTransaccion + " ]";
    }

    public Premio getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(Premio idPremio) {
        this.idPremio = idPremio;
    }

  
    
}
