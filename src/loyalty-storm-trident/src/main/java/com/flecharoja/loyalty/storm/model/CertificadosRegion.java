/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CERTIFICADOS_REGION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CertificadosRegion.findAll", query = "SELECT c FROM CertificadosRegion c"),
    @NamedQuery(name = "CertificadosRegion.countCodConfirmacion",query = " SELECT c FROM CertificadosRegion c WHERE c.codigoConfirmacion = :codConfirmacion")
    , @NamedQuery(name = "CertificadosRegion.findByIdRegion", query = "SELECT c FROM CertificadosRegion c WHERE c.certificadosRegionPK = :idRegion")
    , @NamedQuery(name = "CertificadosRegion.findByCodCertificado", query = "SELECT c FROM CertificadosRegion c WHERE c.certificadosRegionPK = :codCertificado")
    , @NamedQuery(name = "CertificadosRegion.findByFechaEntrega", query = "SELECT c FROM CertificadosRegion c WHERE c.fechaEntrega = :fechaEntrega")
    , @NamedQuery(name = "CertificadosRegion.findByUsuarioEntrega", query = "SELECT c FROM CertificadosRegion c WHERE c.usuarioEntrega = :usuarioEntrega")
    , @NamedQuery(name = "CertificadosRegion.findByIndFormaRegistrada", query = "SELECT c FROM CertificadosRegion c WHERE c.indFormaRegistrada = :indFormaRegistrada")})

public class CertificadosRegion implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CODIGO_CONFIRMACION")
    private String codigoConfirmacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_VERIFICADO")
    private Boolean indVerificado;

    public enum FormaEntrega{
        MANUAL('M'),
        AUTOMATICA('A');
        
        public final char value;
        public static final Map<Character,FormaEntrega> lookup= new HashMap<>();

        private FormaEntrega(char value) {
            this.value = value;
        }
        
        static{
            for(FormaEntrega formaEntrega : values()){
                lookup.put(formaEntrega.value, formaEntrega);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static FormaEntrega get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CertificadosRegionPK certificadosRegionPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_ENTREGA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEntrega;
    
    @Size(max = 45)
    @Column(name = "USUARIO_ENTREGA")
    private String usuarioEntrega;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_FORMA_REGISTRADA")
    private Character indFormaRegistrada;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO")
    @ManyToOne(optional = false)
    private Miembro idMiembro;
    
//    @JoinColumn(name = "COD_CERTIFICADO", referencedColumnName = "CODIGO", insertable = false, updatable = false)
//    @ManyToOne(optional = false)
//    private CodigoCertificado codigoCertificado;
    
    @JoinColumn(name = "ID_REGION", referencedColumnName = "ID_REGION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Region region;
    


    public CertificadosRegion() {
    }

    public CertificadosRegion(CertificadosRegionPK certificadosComercioPK) {
        this.certificadosRegionPK = certificadosComercioPK;
    }

    public CertificadosRegion(CertificadosRegionPK certificadosComercioPK, Date fechaEntrega, Character indFormaRegistrada) {
        this.certificadosRegionPK = certificadosComercioPK;
        this.fechaEntrega = fechaEntrega;
        this.indFormaRegistrada = indFormaRegistrada;
    }

    public CertificadosRegion(String idRegion, String codCertificado) {
        this.certificadosRegionPK = new CertificadosRegionPK(idRegion, codCertificado);
    }

    public CertificadosRegionPK getCertificadosRegionPK() {
        return certificadosRegionPK;
    }

    public void setCertificadosRegionPK(CertificadosRegionPK certificadosComercioPK) {
        this.certificadosRegionPK = certificadosComercioPK;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getUsuarioEntrega() {
        return usuarioEntrega;
    }

    public void setUsuarioEntrega(String usuarioEntrega) {
        this.usuarioEntrega = usuarioEntrega;
    }

    public Character getIndFormaRegistrada() {
        return indFormaRegistrada;
    }

    public void setIndFormaRegistrada(Character indFormaRegistrada) {
        this.indFormaRegistrada = indFormaRegistrada;
    }

    public Miembro getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(Miembro idMiembro) {
        this.idMiembro = idMiembro;
    }

//    public CodigoCertificado getCodigoCertificado() {
//        return codigoCertificado;
//    }
//
//    public void setCodigoCertificado(CodigoCertificado codigoCertificado) {
//        this.codigoCertificado = codigoCertificado;
//    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (certificadosRegionPK != null ? certificadosRegionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CertificadosRegion)) {
            return false;
        }
        CertificadosRegion other = (CertificadosRegion) object;
        if ((this.certificadosRegionPK == null && other.certificadosRegionPK != null) || (this.certificadosRegionPK != null && !this.certificadosRegionPK.equals(other.certificadosRegionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.CertificadosRegion[ certificadosComercioPK=" + certificadosRegionPK + " ]";
    }

    public String getCodigoConfirmacion() {
        return codigoConfirmacion;
    }

    public void setCodigoConfirmacion(String codigoConfirmacion) {
        this.codigoConfirmacion = codigoConfirmacion;
    }

    public Boolean getIndVerificado() {
        return indVerificado;
    }

    public void setIndVerificado(Boolean indVerificado) {
        this.indVerificado = indVerificado;
    }
    
}
