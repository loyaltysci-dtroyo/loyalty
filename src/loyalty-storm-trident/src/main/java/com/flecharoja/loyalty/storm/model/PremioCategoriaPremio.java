/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO_CATEGORIA_PREMIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PremioCategoriaPremio.findAll", query = "SELECT p FROM PremioCategoriaPremio p"),
    @NamedQuery(name = "PremioCategoriaPremio.findByIdCategoria", query = "SELECT p FROM PremioCategoriaPremio p WHERE p.premioCategoriaPremioPK.idCategoria = :idCategoria"),
    @NamedQuery(name = "PremioCategoriaPremio.findByIdPremio", query = "SELECT p FROM PremioCategoriaPremio p WHERE p.premioCategoriaPremioPK.idPremio = :idPremio"),
    @NamedQuery(name = "PremioCategoriaPremio.findByFechaCreacion", query = "SELECT p FROM PremioCategoriaPremio p WHERE p.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "PremioCategoriaPremio.findByUsuarioCreacion", query = "SELECT p FROM PremioCategoriaPremio p WHERE p.usuarioCreacion = :usuarioCreacion"),
    @NamedQuery(name = "PremioCategoriaPremio.findPremiosByIdCategoria", query = "SELECT c.premio FROM PremioCategoriaPremio c WHERE c.premioCategoriaPremioPK.idCategoria = :idCategoria"),
    @NamedQuery(name = "PremioCategoriaPremio.countByIdCategoria", query = "SELECT COUNT(c) FROM PremioCategoriaPremio c WHERE c.premioCategoriaPremioPK.idCategoria = :idCategoria"),
    @NamedQuery(name = "PremioCategoriaPremio.countByIdPremio", query = "SELECT COUNT(c) FROM PremioCategoriaPremio c WHERE c.premioCategoriaPremioPK.idPremio = :idPremio"),
    @NamedQuery(name = "PremioCategoriaPremio.findPremioNotInCategoria", query = "SELECT p FROM Premio p  WHERE   p.idPremio NOT IN (SELECT c.premioCategoriaPremioPK.idPremio FROM PremioCategoriaPremio c WHERE c.premioCategoriaPremioPK.idCategoria = :idCategoria)"),
    @NamedQuery(name = "PremioCategoriaPremio.findCategoriasByIdPremio", query = "SELECT c.categoriaPremio FROM PremioCategoriaPremio c WHERE c.premioCategoriaPremioPK.idPremio = :idPremio"),
    @NamedQuery(name = "PremioCategoriaPremio.findCategoriasNotInPremio", query = "SELECT c FROM CategoriaPremio c  WHERE c.idCategoria NOT IN (SELECT c.premioCategoriaPremioPK.idCategoria FROM PremioCategoriaPremio c WHERE c.premioCategoriaPremioPK.idPremio = :idPremio)"),    

})
public class PremioCategoriaPremio implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PremioCategoriaPremioPK premioCategoriaPremioPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_CATEGORIA", referencedColumnName = "ID_CATEGORIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CategoriaPremio categoriaPremio;
    
    @JoinColumn(name = "ID_PREMIO", referencedColumnName = "ID_PREMIO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Premio premio;

    public PremioCategoriaPremio() {
    }

    public PremioCategoriaPremio(PremioCategoriaPremioPK premioCategoriaPremioPK) {
        this.premioCategoriaPremioPK = premioCategoriaPremioPK;
    }

    public PremioCategoriaPremio(PremioCategoriaPremioPK premioCategoriaPremioPK, Date fechaCreacion, String usuarioCreacion) {
        this.premioCategoriaPremioPK = premioCategoriaPremioPK;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public PremioCategoriaPremio(String idCategoria, String idPremio,Date fechaCreacion, String usuarioCreacion) {
        this.premioCategoriaPremioPK = new PremioCategoriaPremioPK(idCategoria, idPremio);
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public PremioCategoriaPremioPK getPremioCategoriaPremioPK() {
        return premioCategoriaPremioPK;
    }

    public void setPremioCategoriaPremioPK(PremioCategoriaPremioPK premioCategoriaPremioPK) {
        this.premioCategoriaPremioPK = premioCategoriaPremioPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public CategoriaPremio getCategoriaPremio() {
        return categoriaPremio;
    }

    public void setCategoriaPremio(CategoriaPremio categoriaPremio) {
        this.categoriaPremio = categoriaPremio;
    }

    public Premio getPremio() {
        return premio;
    }

    public void setPremio(Premio premio) {
        this.premio = premio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (premioCategoriaPremioPK != null ? premioCategoriaPremioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PremioCategoriaPremio)) {
            return false;
        }
        PremioCategoriaPremio other = (PremioCategoriaPremio) object;
        if ((this.premioCategoriaPremioPK == null && other.premioCategoriaPremioPK != null) || (this.premioCategoriaPremioPK != null && !this.premioCategoriaPremioPK.equals(other.premioCategoriaPremioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.PremioCategoriaPremio[ premioCategoriaPremioPK=" + premioCategoriaPremioPK + " ]";
    }
    
}
