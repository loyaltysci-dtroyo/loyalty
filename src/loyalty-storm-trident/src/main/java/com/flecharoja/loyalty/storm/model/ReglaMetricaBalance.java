package com.flecharoja.loyalty.storm.model;

import static com.flecharoja.loyalty.storm.model.ReglaMetricaCambio.Balances.values;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "REGLA_METRICA_BALANCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReglaMetricaBalance.getAllReglasInSegmentoActivo", query = "SELECT r FROM ReglaMetricaBalance r WHERE r.idLista.idSegmento.indEstado='AC'")
    ,
    @NamedQuery(name = "ReglaMetricaBalance.findByIdRegla", query = "SELECT r FROM ReglaMetricaBalance r WHERE r.idRegla = :idRegla")
    ,
    @NamedQuery(name = "ReglaMetricaBalance.findByIdLista", query = "SELECT r FROM ReglaMetricaBalance r WHERE r.idLista.idLista = :idLista")})
public class ReglaMetricaBalance implements Serializable {

    private static final long serialVersionUID = 1L;

    public enum Balances {

        ACUMULADO('A'),
        EXPIRADO('B'),
        REDIMIDO('C'),
        DISPONIBLE('D');

        private final char value;
        private static final Map<Character, Balances> lookup = new HashMap<>();

        private Balances(char value) {
            this.value = value;
        }

        static {
            for (Balances estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Balances get(char value) {
            return value == Character.MIN_VALUE ? null : lookup.get(value);
        }
    }
    @Id
    @GeneratedValue(generator = "reglametricabalance_uuid")
    @GenericGenerator(name = "reglametricabalance_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_REGLA")
    private String idRegla;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_OPERADOR")
    private Character indOperador;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_BALANCE")
    private Character indBalance;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "VALOR_COMPARACION")
    private String valorComparacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "NOMBRE")
    private String nombre;

    @JoinColumn(name = "ID_LISTA", referencedColumnName = "ID_LISTA")
    @ManyToOne(optional = false)
    private ListaReglas idLista;

    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    private Metrica idMetrica;

    public ReglaMetricaBalance() {
    }

    public String getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(String idRegla) {
        this.idRegla = idRegla;
    }

    public Character getIndOperador() {
        return indOperador;
    }

    public void setIndOperador(Character indOperador) {
        this.indOperador = indOperador != null ? Character.toUpperCase(indOperador) : null;
    }

    public Character getIndBalance() {
        return indBalance;
    }

    public void setIndBalance(Character indBalance) {
        this.indBalance = indBalance != null ? Character.toUpperCase(indBalance) : null;
    }

    public String getValorComparacion() {
        return valorComparacion;
    }

    public void setValorComparacion(String valorComparacion) {
        this.valorComparacion = valorComparacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public ListaReglas getIdLista() {
        return idLista;
    }

    public void setIdLista(ListaReglas idLista) {
        this.idLista = idLista;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegla != null ? idRegla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReglaMetricaBalance)) {
            return false;
        }
        ReglaMetricaBalance other = (ReglaMetricaBalance) object;
        if ((this.idRegla == null && other.idRegla != null) || (this.idRegla != null && !this.idRegla.equals(other.idRegla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ReglaMetricaBalance[ idRegla=" + idRegla + " ]";
    }

}
