/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.functions;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.exception.RecursoNoEncontradoException;
import com.flecharoja.loyalty.storm.service.ReglaAsignacionBadgeService;
import com.flecharoja.loyalty.storm.service.InsigniasService;
import java.util.Map;
import org.apache.storm.trident.operation.BaseFunction;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.operation.TridentOperationContext;
import org.apache.storm.trident.tuple.TridentTuple;
import com.flecharoja.loyalty.storm.model.EventosSistema.Eventos;
import com.flecharoja.loyalty.storm.model.Mision;
import com.flecharoja.loyalty.storm.model.ReglaAsignacionBadge;
import com.flecharoja.loyalty.storm.model.TransaccionCompra;
import com.flecharoja.loyalty.storm.service.MisionService;
import com.flecharoja.loyalty.storm.service.TransaccionesService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import org.apache.storm.topology.ReportedFailedException;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.KeyOnlyFilter;
import org.apache.hadoop.hbase.filter.RegexStringComparator;
import org.apache.hadoop.hbase.filter.RowFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.storm.topology.ReportedFailedException;
import org.apache.storm.tuple.Values;
import org.hibernate.exception.JDBCConnectionException;

/**
 * En esta funcion se asignan los badge a los miembros segun las reglas que
 * hayan sido definidas
 *
 * @author faguilar
 */
public class AsignacionBadgeFunction extends BaseFunction {

    private InsigniasService insigniaService;
    private TransaccionesService transaccionService;
    private ReglaAsignacionBadgeService reglaService;
    private List<ReglaAsignacionBadge> listaReglas;
    private MisionService misionService;
    private Configuration configuration;
    private Connection connection;
    private static final Logger LOG = Logger.getLogger(AsignacionBadgeFunction.class.getName());

    /**
     *
     * @param conf
     * @param context
     */
    @Override
    public void prepare(Map conf, TridentOperationContext context) {

        super.prepare(conf, context);
        this.transaccionService = new TransaccionesService();
        this.insigniaService = new InsigniasService();
        this.misionService = new MisionService();
        this.reglaService = new ReglaAsignacionBadgeService();
        //se obtienen todas las reglas para las insignias activas
        this.configuration = new Configuration();
        this.configuration.addResource("conf/hbase/hbase-site.xml");
        try {
            this.connection = ConnectionFactory.createConnection(configuration);
        } catch (IOException ex) {
            throw new ReportedFailedException(ex);
        }
    }

    @Override
    public void execute(TridentTuple tt, TridentCollector tc) {
        this.listaReglas = this.reglaService.getListaReglasInsigniasActivas();
        try {
            String idMiembro = tt.getStringByField("idMiembro");
            int indEvento = tt.getIntegerByField("indEvento");
            String idTransaccion = tt.getStringByField("idElemento");
            LOG.log(Level.INFO, "Execute asignacion de badge idMiembro: {0} indEvento: {1}", new Object[]{idMiembro, indEvento});
            switch (Eventos.get(indEvento)) {
                case MIE_COMPRA_PRODUCTO: {
                    //check si hay reglas para ese tipo de evento
                    this.listaReglas.stream()
                            .filter(regla -> regla.getIndTipoRegla().charAt(0) == ReglaAsignacionBadge.TiposRegla.FRECUENCIA_COMPRAS.getValue()
                            || regla.getIndTipoRegla().charAt(0) == ReglaAsignacionBadge.TiposRegla.ACUMULADO_COMPRAS.getValue())
                            .forEach((regla) -> {
                                //para cada una de las reglas presentes para el tipo de evento
                                LOG.log(Level.INFO, "Regla: ", regla.getIdRegla());
                                LOG.log(Level.INFO, "Badge de regla: ", regla.getIdBadge());
                                //check si el miembro ya tiene la insignia que la regla otorga
                                if (!tieneInsignia(idMiembro, regla.getIdBadge())) {
                                    //si no, verificar si el miembro cumple con esta regla
                                    if (verificarCumplimientoReglas(idMiembro, regla, idTransaccion)) {
                                        //si la cumple, verificar el resto de reglas para esta misma insignia,si las cumple todas, asignar la insignia
                                        asignarInsignia(idMiembro, regla.getIdBadge());
                                    } else {
                                        LOG.log(Level.INFO, "Miembro no cumple con relgas de badge idMiembro: {0} idRegla: {1}", new Object[]{idMiembro, regla.getIdRegla()});
                                    }

                                } else {
                                    LOG.log(Level.INFO, "Miembro ya tiene badge idMiembro: {0} idRegla: {1}", new Object[]{idMiembro, regla.getIdRegla()});
                                }
                            });
                    break;
                }
                case MIE_ALCANZA_NIVEL_METRICA: {
                    //check si hay reglas para ese tipo de evento
                    this.listaReglas.stream()
                            .filter(regla -> regla.getIndTipoRegla().charAt(0) == ReglaAsignacionBadge.TiposRegla.ASCENDIO_NIVEL.getValue())
                            .forEach((regla) -> {//para cada una de las reglas presentes para el tipo de evento
                                //check si el miembro ya tiene la insignia que la regla otorga
                                if (!tieneInsignia(idMiembro, regla.getIdBadge())) {
                                    //si no, verificar si el miembro cumple con esta regla
                                    if (verificarCumplimientoReglas(idMiembro, regla, idTransaccion)) {
                                        //si la cumple, verificar el resto de reglas para esta misma insignia,si las cumple todas, asignar la insignia
                                        asignarInsignia(idMiembro, regla.getIdBadge());
                                    } else {
                                        LOG.log(Level.INFO, "Miembro no cumple con relgas de badge idMiembro: {0} idRegla: {1}", new Object[]{idMiembro, regla.getIdRegla()});
                                    }

                                } else {
                                    LOG.log(Level.INFO, "Miembro ya tiene badge idMiembro: {0} idRegla: {1}", new Object[]{idMiembro, regla.getIdRegla()});
                                }
                            });
                    break;
                }
                case MIE_APRUEBA_MISION: {
                    //check si hay reglas para ese tipo de evento
                    this.listaReglas.stream()
                            .filter(regla -> regla.getIndTipoRegla().charAt(0) == ReglaAsignacionBadge.TiposRegla.EJECUCION_RETOS.getValue())
                            .forEach((regla) -> {//para cada una de las reglas presentes para el tipo de evento
                                //check si el miembro ya tiene la insignia que la regla otorga
                                if (!tieneInsignia(idMiembro, regla.getIdBadge())) {
                                    //si no, verificar si el miembro cumple con esta regla
                                    if (verificarCumplimientoReglas(idMiembro, regla, idTransaccion)) {
                                        //si la cumple, verificar el resto de reglas para esta misma insignia,si las cumple todas, asignar la insignia
                                        asignarInsignia(idMiembro, regla.getIdBadge());
                                    } else {
                                        LOG.log(Level.INFO, "Miembro no cumple con relgas de badge idMiembro: {0} idRegla: {1}", new Object[]{idMiembro, regla.getIdRegla()});
                                    }

                                } else {
                                    LOG.log(Level.INFO, "Miembro ya tiene badge idMiembro: {0} idRegla: {1}", new Object[]{idMiembro, regla.getIdRegla()});
                                }
                            });
                    break;
                }
                case MIE_REFIERE_MIE: {
                    //check si hay reglas para ese tipo de evento
                    this.listaReglas.stream()
                            .filter(regla -> regla.getIndTipoRegla().charAt(0) == ReglaAsignacionBadge.TiposRegla.REFERIO_MIEMBRO.getValue())
                            .forEach((regla) -> {//para cada una de las reglas presentes para el tipo de evento
                                //check si el miembro ya tiene la insignia que la regla otorga
                                if (!tieneInsignia(idMiembro, regla.getIdBadge())) {
                                    //si no, verificar si el miembro cumple con esta regla
                                    if (verificarCumplimientoReglas(idMiembro, regla, idTransaccion)) {
                                        //si la cumple, verificar el resto de reglas para esta misma insignia,si las cumple todas, asignar la insignia
                                        asignarInsignia(idMiembro, regla.getIdBadge());
                                    } else {
                                        LOG.log(Level.INFO, "Miembro no cumple con relgas de badge idMiembro: {0} idRegla: {1}", new Object[]{idMiembro, regla.getIdRegla()});
                                    }
                                } else {
                                    LOG.log(Level.INFO, "Miembro ya tiene badge idMiembro: {0} idRegla: {1}", new Object[]{idMiembro, regla.getIdRegla()});
                                }
                            });
                    break;
                }
                default: {
                    LOG.log(Level.INFO, "Asignacion de badge: Cayo en el default");
                    break;
                }
            }
        } catch (Exception e) {
             if(e instanceof JDBCConnectionException){
                    throw new ReportedFailedException(e);
                }
            LOG.log(Level.INFO, "Fallo la asignacion de badge ", e);
            throw new ReportedFailedException(e);
        }
    }

    public boolean tieneInsignia(String idMiembro, String idInsignia) {
        return this.insigniaService.verificarInsignia(idMiembro, idInsignia);
    }

    private void asignarInsignia(String idMiembro, String idInsgnia) {
        try {
            this.insigniaService.asignarInsignia(idInsgnia, idMiembro);
            LOG.log(Level.INFO, "Insignia asignada idMiembro: {0} insignia: {1}", new Object[]{idMiembro, idInsgnia});
        } catch (ErrorSistemaException | RecursoNoEncontradoException ex) {
            LOG.log(Level.INFO, "Insignia no asignada idMiembro: {0} insignia: {1}", new Object[]{idMiembro, idInsgnia});
            throw new ReportedFailedException(ex);
        }
    }

    private boolean verificarCumplimientoReglas(String idMiembro, ReglaAsignacionBadge reglaInicial, String idTransaccion) {
        boolean result = true;
        List<ReglaAsignacionBadge> reglas = reglaService.getReglasLista(reglaInicial.getIdBadge());
        for (ReglaAsignacionBadge regla : reglas) {
            boolean resultRegla = true;
            switch (ReglaAsignacionBadge.TiposRegla.get(regla.getIndTipoRegla().charAt(0))) {
                //si es acumulado o frecuencia de compras se procesa igual
                case ACUMULADO_COMPRAS:
                case FRECUENCIA_COMPRAS: {
                    TransaccionCompra transaccion = transaccionService.getTransaccion(idTransaccion);
                    Stream<TransaccionCompra> transacciones = transaccionService.getTransacciones(idMiembro).stream();
                    resultRegla = checkAcumuladoOFrecuencia(regla, transaccion, transacciones);
                    break;
                }
                case EJECUCION_RETOS: {
                    resultRegla = checkEjecucionRetos(regla, idMiembro);
                    break;
                }
                case REFERIO_MIEMBRO: {
                    resultRegla = true;
                    break;
                }
                case ASCENDIO_NIVEL: {
                    resultRegla = true;
                    break;
                }
                default: {
                }
            }

            switch (ReglaAsignacionBadge.ValoresIndOperadorRegla.get(regla.getIndOperadorRegla())) {
                case AND: {
                    result = resultRegla && result;
                    break;
                }
                case OR: {
                    result = resultRegla || result;
                    break;
                }
            }

        }
        return result;
    }

    public boolean checkAcumuladoOFrecuencia(ReglaAsignacionBadge regla, TransaccionCompra transaccion, Stream<TransaccionCompra> transacciones) {
        boolean resultRegla = false;
        switch (ReglaAsignacionBadge.TiposFecha.get(regla.getFechaIndTipo().charAt(0))) {
            case ANO_ACTUAL: {
                transacciones = transacciones.filter((transaccionMiembro) -> {
                    return DateUtils.truncate(transaccion.getFecha(), Calendar.YEAR).equals(DateUtils.truncate(transaccionMiembro.getFecha(), Calendar.YEAR));
                });
                break;
            }
            case ANTERIOR: {
                switch (ReglaAsignacionBadge.TiposUltimaFecha.get(regla.getFechaIndUltimo().charAt(0))) {
                    case ANO: {
                        Date fecha = DateUtils.addYears(new Date(), -Integer.parseInt(regla.getFechaCantidad()));
                        transacciones = transacciones.filter((transaccionMiembro) -> {
                            boolean expr1 = DateUtils.truncate(fecha, Calendar.YEAR).equals(DateUtils.truncate(transaccionMiembro.getFecha(), Calendar.YEAR));
                            boolean expr2 = DateUtils.truncate(fecha, Calendar.YEAR).before(DateUtils.truncate(transaccionMiembro.getFecha(), Calendar.YEAR));
                            //si la transaccion ocurrio en el x # de annos anteriores pero no en el actual
                            return !expr1 && expr2;
                        });
                        break;
                    }
                    case DIA: {
                        Date fecha = DateUtils.addDays(new Date(), -Integer.parseInt(regla.getFechaCantidad()));
                        transacciones = transacciones.filter((transaccionMiembro) -> {
                            boolean expr1 = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH).equals(DateUtils.truncate(transaccionMiembro.getFecha(), Calendar.DAY_OF_MONTH));
                            boolean expr2 = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH).before(DateUtils.truncate(transaccionMiembro.getFecha(), Calendar.DAY_OF_MONTH));
                            //si la transaccion ocurrio en el x # de dias anteriores pero no en el actual
                            return !expr1 && expr2;
                        });
                        break;
                    }
                    case MES: {
                        Date fecha = DateUtils.addMonths(new Date(), -Integer.parseInt(regla.getFechaCantidad()));
                        transacciones = transacciones.filter((transaccionMiembro) -> {
                            boolean expr1 = DateUtils.truncate(fecha, Calendar.MONTH).equals(DateUtils.truncate(transaccionMiembro.getFecha(), Calendar.MONTH));
                            boolean expr2 = DateUtils.truncate(fecha, Calendar.MONTH).before(DateUtils.truncate(transaccionMiembro.getFecha(), Calendar.MONTH));
                            //si la transaccion ocurrio en el x # de dias anteriores pero no en el actual
                            return !expr1 && expr2;
                        });
                        break;
                    }
                }
                break;
            }
            case DESDE: {
                transacciones = transacciones.filter((transaccionMiembro) -> {
                    return transaccionMiembro.getFecha().compareTo(regla.getFechaInicio()) >= 0;
                });
                break;
            }
            case ENTRE: {
                transacciones = transacciones.filter((transaccionMiembro) -> {
                    return transaccionMiembro.getFecha().compareTo(regla.getFechaInicio()) >= 0 && transaccionMiembro.getFecha().compareTo(regla.getFechaFin()) <= 0;
                });
                break;
            }
            case HASTA: {
                transacciones = transacciones.filter((transaccionMiembro) -> {
                    return transaccionMiembro.getFecha().compareTo(regla.getFechaFin()) <= 0;
                });
                break;
            }
            case MES_ACTUAL: {
                transacciones = transacciones.filter((transaccionMiembro) -> {
                    return DateUtils.truncate(transaccion.getFecha(), Calendar.YEAR).equals(DateUtils.truncate(transaccionMiembro.getFecha(), Calendar.YEAR))
                            && DateUtils.truncate(transaccion.getFecha(), Calendar.MONTH).equals(DateUtils.truncate(transaccionMiembro.getFecha(), Calendar.MONTH));
                });
                break;
            }
            case ULTIMO: {
                switch (ReglaAsignacionBadge.TiposUltimaFecha.get(regla.getFechaIndUltimo().charAt(0))) {
                    case ANO: {
                        Date fecha = DateUtils.addYears(new Date(), -Integer.parseInt(regla.getFechaCantidad()));
                        transacciones = transacciones.filter((transaccionMiembro) -> {
                            return fecha.before(transaccionMiembro.getFecha()) || fecha.equals(transaccionMiembro.getFecha());
                        });
                        break;
                    }
                    case DIA: {
                        Date fecha = DateUtils.addDays(new Date(), -Integer.parseInt(regla.getFechaCantidad()));
                        transacciones = transacciones.filter((transaccionMiembro) -> {
                            return fecha.before(transaccionMiembro.getFecha()) || fecha.equals(transaccionMiembro.getFecha());
                        });
                        break;
                    }
                    case MES: {
                        Date fecha = DateUtils.addMonths(new Date(), -Integer.parseInt(regla.getFechaCantidad()));
                        transacciones = transacciones.filter((transaccionMiembro) -> {
                            return fecha.before(transaccionMiembro.getFecha()) || fecha.equals(transaccionMiembro.getFecha());
                        });
                        break;
                    }
                }
                break;
            }
        }
        ReglaAsignacionBadge.TiposRegla tipoRegla = ReglaAsignacionBadge.TiposRegla.get(regla.getIndTipoRegla().charAt(0));
        switch (tipoRegla) {
            case ACUMULADO_COMPRAS: {
                double resultado = transacciones.map((transaccionMiembro) -> transaccionMiembro.getTotal()).reduce(0d, (t, u) -> t + u);
                switch (ReglaAsignacionBadge.ValoresIndOperador.get(regla.getIndOperadorComparacion())) {
                    case IGUAL: {
                        resultRegla = resultado == Double.parseDouble(regla.getValorComparacion());
                        break;
                    }
                    case MAYOR: {
                        resultRegla = resultado > Double.parseDouble(regla.getValorComparacion());
                        break;
                    }
                    case MAYOR_IGUAL: {
                        resultRegla = resultado >= Double.parseDouble(regla.getValorComparacion());
                        break;
                    }
                    case MENOR: {
                        resultRegla = resultado < Double.parseDouble(regla.getValorComparacion());
                        break;
                    }
                    case MENOR_IGUAL: {
                        resultRegla = resultado <= Double.parseDouble(regla.getValorComparacion());
                        break;
                    }
                }
                break;
            }
            case FRECUENCIA_COMPRAS: {
                int resultado = transacciones.map((transaccionMiembro) -> transaccionMiembro.getTotal() > 0 ? 1 : -Integer.parseInt(regla.getFechaCantidad())).reduce(0, (t, u) -> t + u);
                switch (ReglaAsignacionBadge.ValoresIndOperador.get(regla.getIndOperadorComparacion())) {
                    case IGUAL: {
                        resultRegla = resultado == Integer.parseInt(regla.getValorComparacion());
                        break;
                    }
                    case MAYOR: {
                        resultRegla = resultado > Integer.parseInt(regla.getValorComparacion());
                        break;
                    }
                    case MAYOR_IGUAL: {
                        resultRegla = resultado >= Integer.parseInt(regla.getValorComparacion());
                        break;
                    }
                    case MENOR: {
                        resultRegla = resultado < Integer.parseInt(regla.getValorComparacion());
                        break;
                    }
                    case MENOR_IGUAL: {
                        resultRegla = resultado <= Integer.parseInt(regla.getValorComparacion());
                        break;
                    }
                }
                break;
            }
        }
        return resultRegla;
    }

    private boolean checkEjecucionRetos(ReglaAsignacionBadge regla, String idMiembro) {
        boolean resultRegla = false;
        List<Mision> misiones = null;
        try {
            misiones = this.misionService.getMisiones(String.valueOf(Mision.Estados.PUBLICADO.getValue()));
        } catch (ErrorSistemaException ex) {
            Logger.getLogger(AsignacionBadgeFunction.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            Table table = connection.getTable(TableName.valueOf(configuration.get("hbase.namespace"), "REGISTRO_MISION"));
            Scan scan = new Scan();
            String regexp = ".*&" + idMiembro + "&.*";//filtrado por id de miembro
            RegexStringComparator keyRegEx = new RegexStringComparator(regexp);
            RowFilter rowFilter = new RowFilter(CompareOp.EQUAL, keyRegEx);
            Filter filter = new FilterList(new KeyOnlyFilter(), rowFilter);
            scan.setFilter(filter);
            String comodinInicio = "00000000-0000-0000-0000-000000000000&00000000-0000-0000-0000-000000000000&";
            String comodin = "zzzzzzzz-zzzz-zzzz-zzzz-zzzzzzzzzzzz&zzzzzzzz-zzzz-zzzz-zzzz-zzzzzzzzzzzz&";
            //se establecen las condiciones para el filtrado en hbase
            switch (ReglaAsignacionBadge.TiposFecha.get(regla.getFechaIndTipo().charAt(0))) {
                case ANO_ACTUAL: {
                    scan.setStartRow(Bytes.toBytes(comodin + new Date().getTime()));
                    scan.setStopRow(Bytes.toBytes(comodin + DateUtils.truncate(new Date(), Calendar.YEAR).getTime()));
                    break;
                }
                case ANTERIOR: {
                    switch (ReglaAsignacionBadge.TiposUltimaFecha.get(regla.getFechaIndUltimo().charAt(0))) {
                        case ANO: {
                            scan.setStartRow(Bytes.toBytes(comodinInicio + new Date().getTime()));
                            scan.setStopRow(Bytes.toBytes(comodin + DateUtils.truncate(DateUtils.addYears(new Date(), -Integer.parseInt(regla.getFechaCantidad())), Calendar.YEAR).getTime()));
                            break;
                        }
                        case DIA: {
                            scan.setStartRow(Bytes.toBytes(comodinInicio + new Date().getTime()));
                            scan.setStopRow(Bytes.toBytes(comodin + DateUtils.truncate(DateUtils.addDays(new Date(), -Integer.parseInt(regla.getFechaCantidad())), Calendar.DAY_OF_MONTH).getTime()));
                            break;
                        }
                        case MES: {
                            scan.setStartRow(Bytes.toBytes(comodinInicio + new Date().getTime()));
                            scan.setStopRow(Bytes.toBytes(comodin + DateUtils.truncate(DateUtils.addMonths(new Date(), -Integer.parseInt(regla.getFechaCantidad())), Calendar.YEAR).getTime()));
                            break;
                        }
                    }
                    break;
                }
                case DESDE: {
                    scan.setStopRow(Bytes.toBytes(comodin + regla.getFechaInicio().getTime()));
                    break;
                }
                case ENTRE: {
                    scan.setStartRow(Bytes.toBytes(comodinInicio + regla.getFechaFin().getTime()));
                    scan.setStopRow(Bytes.toBytes(comodin + regla.getFechaInicio().getTime()));
                    break;
                }
                case HASTA: {
                    scan.setStartRow(Bytes.toBytes(comodinInicio + regla.getFechaFin().getTime()));
                    break;
                }
                case MES_ACTUAL: {
                    scan.setStartRow(Bytes.toBytes(comodinInicio + new Date().getTime()));
                    scan.setStopRow(Bytes.toBytes(comodin + DateUtils.truncate(new Date(), Calendar.MONTH).getTime()));
                    break;
                }
                case ULTIMO: {
                    switch (ReglaAsignacionBadge.TiposUltimaFecha.get(regla.getFechaIndUltimo().charAt(0))) {
                        case ANO: {
                            scan.setStartRow(Bytes.toBytes(comodinInicio + new Date().getTime()));
                            scan.setStopRow(Bytes.toBytes(comodin + DateUtils.addYears(new Date(), -Integer.parseInt(regla.getFechaCantidad())).getTime()));
                            break;
                        }
                        case DIA: {
                            scan.setStartRow(Bytes.toBytes(comodinInicio + new Date().getTime()));
                            scan.setStopRow(Bytes.toBytes(comodin + DateUtils.addDays(new Date(), -Integer.parseInt(regla.getFechaCantidad())).getTime()));
                            break;
                        }
                        case MES: {
                            scan.setStartRow(Bytes.toBytes(comodinInicio + new Date().getTime()));
                            scan.setStopRow(Bytes.toBytes(comodin + DateUtils.addMonths(new Date(), -Integer.parseInt(regla.getFechaCantidad())).getTime()));
                            break;
                        }
                    }
                    break;
                }
            }
            int cantMisionesEncuesta = 0;
            int cantMisionesJuego = 0;
            int cantMisionesPreferencias = 0;
            int cantMisionesPerfil = 0;
            int cantMisionesSocial = 0;
            int cantMisionesSubirContenido = 0;
            int cantMisionesVerContenido = 0;
            try (ResultScanner scanner = table.getScanner(scan)) {

                for (Result result = scanner.next(); result != null; result = scanner.next()) {
                    String key = Bytes.toString(result.getRow());
                    String[] row = key.split("&");
                    try {
                        Mision misionActual = misiones.stream().filter((mision) -> {
                            return ((row[0] != null) && (mision.getIdMision().equals(row[0])));
                        }).findFirst().get();

                        //mision => row[0] //miembro=> row[1] //fecha=> row[2]
                        if (misionActual != null) {
                            switch (Mision.Tipos.get(misionActual.getIndTipoMision())) {
                                case ENCUESTA: {
                                    cantMisionesEncuesta++;
                                    break;
                                }
                                case JUEGO_PREMIO: {
                                    cantMisionesJuego++;
                                    break;
                                }
                                case PREFERENCIAS: {
                                    cantMisionesPreferencias++;
                                    break;
                                }
                                case PERFIL: {
                                    cantMisionesPerfil++;
                                    break;
                                }
                                case RED_SOCIAL: {
                                    cantMisionesSocial++;
                                    break;
                                }
                                case SUBIR_CONTENIDO: {
                                    cantMisionesSubirContenido++;
                                    break;
                                }
                                case VER_CONTENIDO: {
                                    cantMisionesVerContenido++;
                                    break;
                                }
                            }
                        }
                    } catch (java.util.NoSuchElementException ex) {
                        throw new ReportedFailedException(ex);
                    }
                }
            }

            switch (ReglaAsignacionBadge.TiposValorComparacion.get(regla.getIndTipoValorComparacion().charAt(0))) {
                case ENCUESTA: {
                    switch (ReglaAsignacionBadge.ValoresIndOperador.get(regla.getIndOperadorComparacion())) {
                        case IGUAL: {
                            resultRegla = cantMisionesEncuesta == Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR: {
                            resultRegla = cantMisionesEncuesta > Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR_IGUAL: {
                            resultRegla = cantMisionesEncuesta >= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR: {
                            resultRegla = cantMisionesEncuesta < Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR_IGUAL: {
                            resultRegla = cantMisionesEncuesta <= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                    }
                    break;
                }
                case JUEGO_PREMIO: {
                    switch (ReglaAsignacionBadge.ValoresIndOperador.get(regla.getIndOperadorComparacion())) {
                        case IGUAL: {
                            resultRegla = cantMisionesJuego == Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR: {
                            resultRegla = cantMisionesJuego > Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR_IGUAL: {
                            resultRegla = cantMisionesJuego >= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR: {
                            resultRegla = cantMisionesJuego < Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR_IGUAL: {
                            resultRegla = cantMisionesJuego <= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                    }
                    break;
                }
                case PERFIL: {
                    switch (ReglaAsignacionBadge.ValoresIndOperador.get(regla.getIndOperadorComparacion())) {
                        case IGUAL: {
                            resultRegla = cantMisionesPerfil == Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR: {
                            resultRegla = cantMisionesPerfil > Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR_IGUAL: {
                            resultRegla = cantMisionesPerfil >= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR: {
                            resultRegla = cantMisionesPerfil < Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR_IGUAL: {
                            resultRegla = cantMisionesPerfil <= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                    }
                    break;
                }
                case PREFERENCIAS: {
                    switch (ReglaAsignacionBadge.ValoresIndOperador.get(regla.getIndOperadorComparacion())) {
                        case IGUAL: {
                            resultRegla = cantMisionesPreferencias == Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR: {
                            resultRegla = cantMisionesPreferencias > Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR_IGUAL: {
                            resultRegla = cantMisionesPreferencias >= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR: {
                            resultRegla = cantMisionesPreferencias < Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR_IGUAL: {
                            resultRegla = cantMisionesPreferencias <= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                    }
                    break;
                }
                case RED_SOCIAL: {
                    switch (ReglaAsignacionBadge.ValoresIndOperador.get(regla.getIndOperadorComparacion())) {
                        case IGUAL: {
                            resultRegla = cantMisionesSocial == Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR: {
                            resultRegla = cantMisionesSocial > Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR_IGUAL: {
                            resultRegla = cantMisionesSocial >= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR: {
                            resultRegla = cantMisionesSocial < Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR_IGUAL: {
                            resultRegla = cantMisionesSocial <= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                    }
                    break;
                }
                case SUBIR_CONTENIDO: {
                    switch (ReglaAsignacionBadge.ValoresIndOperador.get(regla.getIndOperadorComparacion())) {
                        case IGUAL: {
                            resultRegla = cantMisionesSubirContenido == Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR: {
                            resultRegla = cantMisionesSubirContenido > Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR_IGUAL: {
                            resultRegla = cantMisionesSubirContenido >= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR: {
                            resultRegla = cantMisionesSubirContenido < Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR_IGUAL: {
                            resultRegla = cantMisionesSubirContenido <= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                    }
                    break;
                }
                case VER_CONTENIDO: {
                    switch (ReglaAsignacionBadge.ValoresIndOperador.get(regla.getIndOperadorComparacion())) {
                        case IGUAL: {
                            resultRegla = cantMisionesVerContenido == Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR: {
                            resultRegla = cantMisionesVerContenido > Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MAYOR_IGUAL: {
                            resultRegla = cantMisionesVerContenido >= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR: {
                            resultRegla = cantMisionesVerContenido < Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                        case MENOR_IGUAL: {
                            resultRegla = cantMisionesVerContenido <= Integer.parseInt(regla.getValorComparacion());
                            break;
                        }
                    }
                    break;
                }
            }

        } catch (IOException e) {
            throw new ReportedFailedException(e);
        }
        return resultRegla;
    }

    @Override
    public void cleanup() {
        try {
            super.cleanup(); //To change body of generated methods, choose Tools | Templates.
            this.connection.close();
        } catch (IOException ex) {
            Logger.getLogger(AsignacionBadgeFunction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
