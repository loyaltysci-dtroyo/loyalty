package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.exception.RecursoNoEncontradoException;
import com.flecharoja.loyalty.storm.model.CodigoCertificado;
import com.flecharoja.loyalty.storm.model.ConfiguracionGeneral;
import com.flecharoja.loyalty.storm.model.ExistenciasUbicacion;
import com.flecharoja.loyalty.storm.model.ExistenciasUbicacionPK;
import com.flecharoja.loyalty.storm.model.HistoricoMovimientos;
import com.flecharoja.loyalty.storm.model.Miembro;
import com.flecharoja.loyalty.storm.model.Premio;
import com.flecharoja.loyalty.storm.model.PremioControl;
import com.flecharoja.loyalty.storm.model.PremioListaMiembro;
import com.flecharoja.loyalty.storm.model.PremioMiembro;
import com.flecharoja.loyalty.storm.util.CodigosErrores;
import com.flecharoja.loyalty.storm.util.Indicadores;
import com.flecharoja.loyalty.storm.util.MyKafkaUtils;
import com.google.common.primitives.Chars;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.TemporalType;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang.time.DateUtils;

/**
 * EJB con los metodos de negocio necesarios para el manejo de premios
 *
 * @author wtencio
 */
public class PremioService {

    private static final Logger LOG = Logger.getLogger(PremioService.class.getName());

    /**
     * Metodo que obtiene un listado de premios en un rango de numero de
     * registros y opcionalmente por estado
     *
     * @param estado Texto con los estados deseados
     * @return Respuesta con un listado de premios y el rango de registros
     * obtenidos
     * @throws com.flecharoja.loyalty.storm.exception.ErrorSistemaException
     */
    public List<Premio> getPremios(String estado) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Map<String, Object> respuesta = new HashMap<>();
        List resultado = null;
        try {
            resultado = em.createNamedQuery("Premio.findByIndEstado").setParameter("indEstado", estado).getResultList();
        } catch (IllegalArgumentException e) {//en el caso de argumentos validos (paginacion)
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        em.close();
        return resultado;
    }

    /**
     * Metodo que obtiene la informacion de un premio por el identificador de la
     * misma
     *
     * @param idPremio Identificador del premio
     * @return Respuesta con la informacion de la promocion encontrada
     * @throws
     * com.flecharoja.loyalty.storm.exception.RecursoNoEncontradoException
     */
    public Map<String, Object> getPremioPorId(String idPremio) throws RecursoNoEncontradoException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Map<String, Object> respuesta = new HashMap<>();
        Premio premio = em.find(Premio.class, idPremio);
        //verificacion de que la entidad exista
        if (premio == null) {
            throw new RecursoNoEncontradoException();
        }
        respuesta.put("resultado", premio);
        em.close();
        return respuesta;
    }

    /**
     * Agrega un miembro a la lista de elgibles para un premio
     *
     * @param idMiembro id del miembro a agregar
     * @param idPremio id del premio asociado
     * @throws ErrorSistemaException si la operacion es fallida
     */
    public void agregarMiembroElegibles(String idPremio, String idMiembro) throws ErrorSistemaException {
        this.includeExcludeMiembro(idPremio, idMiembro, Indicadores.INCLUIDO, idMiembro);
    }

    /**
     * Método que permite a un miembro redimir un premio
     *
     * @param idPremio identificador del premio
     * @param idMiembro identificador del miembro
     * @return
     * @throws com.flecharoja.loyalty.storm.exception.ErrorSistemaException
     */
    public String redimirPremio(String idPremio, String idMiembro) throws ErrorSistemaException {

        try {
            EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
            MyKafkaUtils mku = new MyKafkaUtils();

            CodigoCertificado codigoCertificado = null;
            Premio premio = em.find(Premio.class, idPremio);
            if (premio == null) {
                throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA, "premio == null");
            }
            Miembro miembro = em.find(Miembro.class, idMiembro);
            if (miembro == null) {
                throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA, "miembro == null");
            }

            //verificacion de la elegibilidad de redimir el premio
            //TODO verificar existencias y afectar existencias
            PremioControl premioControl = em.find(PremioControl.class, premio.getIdPremio());

            if (!premioControl.getIndEstado().equals(PremioControl.Estados.PUBLICADO.getValue())) {
                LOG.log(Level.WARNING, "Premio no activo, miembro: {0} , premio: {1}", new Object[]{idMiembro, premio.getIdPremio()});
            }
            if (!checkIntervalosRespuestaPremioMiembro(premioControl, idMiembro)) {
                LOG.log(Level.WARNING, "Intervalo de respuesta de miembro: 0} para el premio: {1}", new Object[]{idMiembro, premio.getIdPremio()});
            }
            if (!checkCalendarizacionPremio(premioControl)) {
                LOG.log(Level.WARNING, "Calendarizacion no valida, miembro: {0} ,premio: {1}", new Object[]{idMiembro, premio.getIdPremio()});
            }
            if (!existsExitenciaUbicacion(idPremio)) {
                LOG.log(Level.WARNING, "No hay existencias, miembro: {0} ,premio: {1}", new Object[]{idMiembro, premio.getIdPremio()});
            }

            PremioMiembro premioMiembro = new PremioMiembro();
            premioMiembro.setEstado(PremioMiembro.Estados.REDIMIDO_SIN_RETIRAR.getValue());
            premioMiembro.setFechaAsignacion(Calendar.getInstance().getTime());
            premioMiembro.setIdMiembro(miembro);
            premioMiembro.setIndMotivoAsignacion(PremioMiembro.Motivos.PREMIO.getValue());
            premioMiembro.setIdPremio(premio);
            if (premio.getIndTipoPremio().equals(Indicadores.PREMIO_IND_TIPO_CERTIFICADO)) {
                List<CodigoCertificado> certificados = em.createNamedQuery("CodigoCertificado.findByIdPremio")
                        .setParameter("idPremio", premio.getIdPremio())
                        .setParameter("indEstado", CodigoCertificado.Estados.DISPONIBLE.getValue())
                        .getResultList();
                if (!certificados.isEmpty()) {
                    codigoCertificado = certificados.get(0);
                    premioMiembro.setCodigoCertificado(codigoCertificado.getCodigoCertificadoPK().getCodigo());
                    codigoCertificado.setIndEstado(CodigoCertificado.Estados.OCUPADO.getValue());
                } else {
                    throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA, "certificados.isEmpty()");
                }
                em.merge(codigoCertificado);
            }
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            em.persist(premioMiembro);
            em.flush();
            this.afectarExistenciasHistorico(idPremio, idMiembro, premioMiembro.getIdPremioMiembro());
            em.getTransaction().commit();
            em.close();
            mku.notifyEvento(MyKafkaUtils.EventosSistema.REMIDIO_PREMIO, idPremio, idMiembro);
            if (codigoCertificado != null) {
                return codigoCertificado.getCodigoCertificadoPK().getCodigo();
            } else {
                return null;
            }

        } catch (IOException ex) {
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
    }

    /**
     * filtrado de premio por tiempo transcurrido entre intervalos de respuestas
     */
    private boolean checkIntervalosRespuestaPremioMiembro(PremioControl premio, String idMiembro) {
        if (premio.getIndRespuesta() != null && premio.getIndRespuesta()) {
            EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
            PremioControl.IntervalosTiempoRespuesta intervalo;
            boolean flag = true;//bandera indicando si se aceptan nuevas rediciones (por defecto true)

            //verificacion de intervalos de respuesta totales
            intervalo = PremioControl.IntervalosTiempoRespuesta.get(premio.getIndIntervaloTotal());
            if (intervalo != null) {
                flag = checkIntervaloRespuestaPremio(null, premio.getIdPremio(), intervalo, premio.getCantTotal());
            }

            //verificacion de intervalos de respuesta totales por miembro..
            intervalo = PremioControl.IntervalosTiempoRespuesta.get(premio.getIndIntervaloMiembro());
            if (intervalo != null && flag) {
                flag = checkIntervaloRespuestaPremio(idMiembro, premio.getIdPremio(), intervalo, premio.getCantTotalMiembro());
            }

            //verificacion de tiempo entre respuestas del miembro...
            if (intervalo != null && flag) {
                Date fechaUltima = (Date) em.createNamedQuery("PremioMiembro.findByUltimaFechaAsignacion")
                        .setParameter("idMiembro", idMiembro)
                        .setParameter("idPremio", premio.getIdPremio())
                        .getSingleResult();
                if (fechaUltima != null) {
                    switch (intervalo) {
                        case POR_DIA: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.DAY_OF_YEAR, -premio.getCantIntervaloRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                        case POR_HORA: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.HOUR_OF_DAY, -premio.getCantIntervaloRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                        case POR_MINUTO: {
                            Calendar fecha = Calendar.getInstance();
                            fecha.add(Calendar.MINUTE, -premio.getCantIntervaloRespuesta().intValue());
                            flag = fechaUltima.before(fecha.getTime());
                            break;
                        }
                    }
                }
            }
            em.close();
            return flag;
        }
        return true;
    }

    /**
     * true -> se aceptan nuevas respuestas false -> no se aceptan nuevas
     * respuestas
     */
    private boolean checkIntervaloRespuestaPremio(String idMiembro, String idPremio, PremioControl.IntervalosTiempoRespuesta intervalo, Long cantTotal) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        /*
        Se obtienen la cantidad de respuestas de registros de respuestas de premio en rangos de tiempos de fecha
         */
        long cantidadRespuestas = 0;
        switch (intervalo) {
            case POR_SIEMPRE: {
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasPremio").setParameter("idPremio", idPremio).getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasPremioMiembro").setParameter("idPremio", idPremio).setParameter("idMiembro", idMiembro).getSingleResult();
                }
                break;
            }
            case POR_MES: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_SEMANA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.WEEK_OF_YEAR, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_DIA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
            case POR_HORA: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.HOUR_OF_DAY, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();

                }

                break;
            }
            case POR_MINUTO: {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MINUTE, -1);
                if (idMiembro == null) {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremio")
                            .setParameter("idPremio", idPremio)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                } else {
                    cantidadRespuestas = (long) em.createNamedQuery("PremioMiembro.countRespuestasTiempoPremioMiembro")
                            .setParameter("idPremio", idPremio)
                            .setParameter("idMiembro", idMiembro)
                            .setParameter("fechaInicio", calendar.getTime(), TemporalType.TIMESTAMP)
                            .setParameter("fechaFin", new Date(), TemporalType.TIMESTAMP)
                            .getSingleResult();
                }
                break;
            }
        }
        em.close();
        //mientras que la cantidad de respuestas siga siendo menor a la cantidad de respuestas totales aceptables..
        return cantidadRespuestas < cantTotal;
    }

    /**
     * Método que verifica que el premio este entre las fechas correctas
     */
    private boolean checkCalendarizacionPremio(PremioControl premio) {
        //se establece el indicador de respuesta como verdadero(todo es permitido hasta que sea negado)
        boolean flag = true;
        //se obtiene la fecha actual
        Calendar hoy = Calendar.getInstance();

        //si el premio es calendarizado
        if (premio.getEfectividadIndCalendarizado().equals(PremioControl.Efectividad.CALENDARIZADO.getValue())) {
            //si la fecha de inicio esta establecida, se comprueba que la fecha sea pasada a hoy o que sea el mismo dia
            if (premio.getEfectividadFechaInicio() != null) {
                flag = hoy.getTime().after(premio.getEfectividadFechaInicio()) || DateUtils.isSameDay(hoy.getTime(), premio.getEfectividadFechaInicio());
            }
            //mientras siga siendo verdadero...
            //si la fecha de fin esta establecida, se comprueba que la fecha sea posterior a hoy o que sea el mismo dia
            if (flag && premio.getEfectividadFechaFin() != null) {
                flag = hoy.getTime().before(premio.getEfectividadFechaFin()) || DateUtils.isSameDay(hoy.getTime(), premio.getEfectividadFechaFin());
            }

            //mientras siga siendo verdadero...
            if (flag && premio.getEfectividadIndDias() != null) {
                //se obtiene el numero de dia de hoy
                int hoyDia = hoy.get(Calendar.DAY_OF_WEEK);
                //se recorre los indicadores de dias de recurrencia en busca de que se cumpla al menos una condicion
                flag = Chars.asList(premio.getEfectividadIndDias().toCharArray()).stream().anyMatch((t) -> {
                    //segun el indicador de dia de recurrencia, se verifica si el numero de dia de hoy concuerda con el numero de dia asociado al indicador
                    switch (t) {
                        case 'L': {
                            return Calendar.MONDAY == hoyDia;
                        }
                        case 'K': {
                            return Calendar.TUESDAY == hoyDia;
                        }
                        case 'M': {
                            return Calendar.WEDNESDAY == hoyDia;
                        }
                        case 'J': {
                            return Calendar.THURSDAY == hoyDia;
                        }
                        case 'V': {
                            return Calendar.FRIDAY == hoyDia;
                        }
                        case 'S': {
                            return Calendar.SATURDAY == hoyDia;
                        }
                        case 'D': {
                            return Calendar.SUNDAY == hoyDia;
                        }
                        default: {
                            return false;
                        }
                    }
                });
            }
            if (flag && premio.getEfectividadIndSemana() != null) {
                //comprobacion de semans
            }
        }
        return flag;
    }

    /**
     * Método que dice si existen existencias del producto en la ubicacion
     * central
     *
     * @param idPremio identificador del premio
     * @param locale
     * @return
     */
    public boolean existsExitenciaUbicacion(String idPremio) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List<ConfiguracionGeneral> configuracion = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
        ConfiguracionGeneral config = configuracion.get(0);

        if (config.getIntegracionInventarioPremios() == false) {
            return true;
        } else {
            if (config.getUbicacionPrincipal() == null) {
                throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
            }
            String idUbicacion = config.getUbicacionPrincipal().getIdUbicacion();

            ExistenciasUbicacion existencias = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(idUbicacion, idPremio, config.getMes(), config.getPeriodo()));
            em.close();
            if (existencias == null) {
                return false;
            }
            return existencias.getSaldoActual() >= 1;
        }
    }

    public void afectarExistenciasHistorico(String idPremio, String idMiembro, String idPremioMiembro) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List<ConfiguracionGeneral> configuracion = em.createNamedQuery("ConfiguracionGeneral.findAll").getResultList();
        ConfiguracionGeneral config = configuracion.get(0);
        if (config.getIntegracionInventarioPremios() == true) {
            if (config.getUbicacionPrincipal() == null) {
                throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
            }
            String idUbicacion = config.getUbicacionPrincipal().getIdUbicacion();

            Premio premio = em.find(Premio.class, idPremio);
            if (premio == null) {
                throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
            }

            ExistenciasUbicacion existencias = em.find(ExistenciasUbicacion.class, new ExistenciasUbicacionPK(idUbicacion, idPremio, config.getMes(), config.getPeriodo()));

            if (existencias == null) {
                throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
            }

            if (existencias.getSaldoActual() >= 1) {
                existencias.setSalidas(existencias.getSalidas() + 1);
                existencias.setSaldoActual((existencias.getSaldoInicial() + existencias.getEntradas()) - existencias.getSalidas());
            }

            try {
                if (!em.getTransaction().isActive()) {
                    em.getTransaction().begin();
                }
                em.merge(existencias);
                em.flush();
                em.getTransaction().commit();
            } catch (ConstraintViolationException e) {
                throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
            }
            HistoricoMovimientos historico;
            if (premio.getIndTipoPremio().equals(Premio.Tipo.CERTIFICADO.getValue())) {
                historico = new HistoricoMovimientos(null, em.find(Miembro.class, idMiembro), premio, existencias.getUbicacion(), null, 1L, HistoricoMovimientos.Tipos.REDENCION_AUTO.getValue(), new Date(), HistoricoMovimientos.Status.REDIMIDO_SIN_RETIRAR.getValue(), idPremioMiembro);
            } else {
                historico = new HistoricoMovimientos(premio.getPrecioPromedio(), em.find(Miembro.class, idMiembro), premio, existencias.getUbicacion(), null, 1L, HistoricoMovimientos.Tipos.REDENCION_AUTO.getValue(), new Date(), HistoricoMovimientos.Status.REDIMIDO_SIN_RETIRAR.getValue(), idPremioMiembro);
            }

            long totalPremios = (long) em.createNamedQuery("ExistenciasUbicacion.findByPremioByPeriodoByMes").setParameter("idPremio", idPremio).setParameter("mes", config.getMes()).setParameter("periodo", config.getPeriodo()).getSingleResult();
            premio.setTotalExistencias(totalPremios);
            try {
                em.persist(historico);
                em.merge(premio);
            } catch (ConstraintViolationException e) {
                throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
            } finally {
                em.close();
            }
        }
    }

    /**
     * Método que asigna (incluye o excluye) a un miembro de un premio
     *
     * @param idPremio identificador del premio
     * @param idMiembro identificador del miembro
     * @param indAccion tipo de accion(incluir o excluir)
     * @param usuario usuario en sesion
     * @throws com.flecharoja.loyalty.storm.exception.ErrorSistemaException
     */
    public void includeExcludeMiembro(String idPremio, String idMiembro, Character indAccion, String usuario) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Premio premio;
        premio = em.find(Premio.class, idPremio);
        em.getReference(Miembro.class, idMiembro).getIdMiembro();

        if (premio.getIndEstado().equals(Premio.Estados.ARCHIVADO.getValue())) {
            LOG.log(Level.SEVERE, "La Promocion es nula", new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA));
        }
        Date date = Calendar.getInstance().getTime();
        indAccion = Character.toUpperCase(indAccion);
        //verificacion que el indicador de accion sea una valido
        if (indAccion.compareTo(Indicadores.INCLUIDO) == 0 || indAccion.compareTo(Indicadores.EXCLUIDO) == 0) {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            PremioListaMiembro premioListaMiembro = new PremioListaMiembro(idMiembro, idPremio, indAccion, date, usuario);
            em.merge(premioListaMiembro);
            em.flush();
            em.getTransaction().commit();
            em.close();
        } else {
            em.close();
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }

    }

}
