package com.flecharoja.loyalty.storm.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@XmlRootElement
public class RegistroMetrica {
    
    public enum TiposGane{
        DESCONOCIDO("D"),
        MISION("M"),
        BIENVENIDA("B"),
        REFERENCIA("R"),
        TRANSACCION("T");
        
        private final String value;
        private static final Map<String,TiposGane> lookup = new HashMap<>();

        private TiposGane(String value) {
            this.value = value;
        }

        static{
            for(TiposGane ind : values()){
                lookup.put(ind.value, ind);
            }
        }

        public String getValue() {
            return value;
        }
        
        public static TiposGane get(String value){
            return value==null?null:lookup.get(value);
        }
    }
    
    @XmlRootElement
    public class RowKey {
        private Date fecha;
        private String idInstancia;

        public RowKey() {
            fecha = new Date();
            idInstancia = null;
        }

        public RowKey(Date fecha, String idInstancia) {
            this.fecha = fecha;
            this.idInstancia = idInstancia;
        }

        public Date getFecha() {
            return fecha;
        }

        public void setFecha(Date fecha) {
            this.fecha = fecha;
        }

        public String getIdInstancia() {
            return idInstancia;
        }

        public void setIdInstancia(String idInstancia) {
            this.idInstancia = idInstancia;
        }

        @Override
        public String toString() {
            return "RowKey{" + "fecha=" + fecha + ", idInstancia=" + idInstancia + '}';
        }
    }
    @XmlRootElement
    public class Data {
        private String metrica;
        private String miembro;
        private String indTipoGane;
        private double cantidad;
        private double disponible;
        private boolean vencido;

        public Data(String metrica, String miembro, String indTipoGane, double cantidad) {
            this.metrica = metrica;
            this.miembro = miembro;
            this.indTipoGane = indTipoGane;
            this.cantidad = cantidad;
            this.disponible = cantidad;
            this.vencido = false;
        }

        public Data(String metrica, String miembro, String indTipoGane, double cantidad, double disponible, boolean vencido) {
            this.metrica = metrica;
            this.miembro = miembro;
            this.indTipoGane = indTipoGane;
            this.cantidad = cantidad;
            this.disponible = disponible;
            this.vencido = vencido;
        }

        private Data(double disponible) {
            this.disponible = disponible;
        }

        public String getMetrica() {
            return metrica;
        }

        public void setMetrica(String metrica) {
            this.metrica = metrica;
        }

        public String getMiembro() {
            return miembro;
        }

        public void setMiembro(String miembro) {
            this.miembro = miembro;
        }

        public String getIndTipoGane() {
            return indTipoGane;
        }

        public void setIndTipoGane(String indTipoGane) {
            this.indTipoGane = indTipoGane;
        }

        public double getCantidad() {
            return cantidad;
        }

        public void setCantidad(double cantidad) {
            this.cantidad = cantidad;
        }

        public double getDisponible() {
            return disponible;
        }

        public void setDisponible(double disponible) {
            this.disponible = disponible;
        }

        public boolean isVencido() {
            return vencido;
        }

        public void setVencido(boolean vencido) {
            this.vencido = vencido;
        }

        @Override
        public String toString() {
            return "Data{" + "metrica=" + metrica + ", miembro=" + miembro + ", cantidad=" + cantidad + ", disponible=" + disponible + ", vencido=" + vencido + '}';
        }
    }
    @XmlRootElement
    public class Detalles {
        private String mision;
        private String transaccion;

        public Detalles() {
        }

        public Detalles(String mision, String transaccion) {
            this.mision = mision;
            this.transaccion = transaccion;
        }

        public String getMision() {
            return mision;
        }

        public void setMision(String mision) {
            this.mision = mision;
        }

        public String getTransaccion() {
            return transaccion;
        }

        public void setTransaccion(String transaccion) {
            this.transaccion = transaccion;
        }

        @Override
        public String toString() {
            return "Detalles{" + "mision=" + mision + ", transaccion=" + transaccion + '}';
        }
    }
    
    private RowKey rowKey;
    private Data data;
    private Detalles detalles;

    public RegistroMetrica(Date fecha, String instancia){
        this.rowKey = new RowKey(fecha, instancia);
    }
    public RegistroMetrica(String idMetrica, String idMiembro, String indTipoGane, double cantidad) {
        this.rowKey = new RowKey();
        this.data = new Data(idMetrica, idMiembro, indTipoGane, cantidad);
        this.detalles = new Detalles();
    }

    public RegistroMetrica(Date fecha, String idInstancia, String metrica, String miembro, String indTipoGane, double cantidad, double disponible, boolean vencido, String mision, String transaccion) {
        this.rowKey = new RowKey(fecha, idInstancia);
        this.data = new Data(metrica, miembro, indTipoGane, cantidad, disponible, vencido);
        this.detalles = new Detalles(mision, transaccion);
    }
    
    public RegistroMetrica(Date fecha, String idInstancia, double disponible) {
        this.rowKey = new RowKey(fecha, idInstancia);
        this.data = new Data(disponible);
    }

    public RowKey getRowKey() {
        return rowKey;
    }

    public void setRowKey(RowKey rowKey) {
        this.rowKey = rowKey;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Detalles getDetalles() {
        return detalles;
    }

    public void setDetalles(Detalles detalles) {
        this.detalles = detalles;
    }

    @Override
    public String toString() {
        return "RegistroMetrica{" + "rowKey=" + rowKey.toString() + ", data=" + data.toString() + ", detalles=" + detalles.toString() + '}';
    }
}
