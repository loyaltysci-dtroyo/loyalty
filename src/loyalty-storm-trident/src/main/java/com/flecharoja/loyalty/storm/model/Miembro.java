package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author wtencio, svargas
 */
@Entity
@Table(name = "MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Miembro.findAll", query = "SELECT m FROM Miembro m ORDER BY m.fechaCreacion DESC"),
    @NamedQuery(name = "Miembro.getAvatarFromMiembro", query = "SELECT m.avatar FROM Miembro m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "Miembro.findAllActivos", query = "SELECT m FROM Miembro m WHERE m.indEstadoMiembro = 'A' ORDER BY m.fechaCreacion DESC"),
    @NamedQuery(name = "Miembro.countAll", query = "SELECT COUNT(m.idMiembro) FROM Miembro m"),
    @NamedQuery(name = "Miembro.countAllByIndEstadoMiembro", query = "SELECT COUNT(m.idMiembro) FROM Miembro m WHERE m.indEstadoMiembro = :indEstadoMiembro"),
    @NamedQuery(name = "Miembro.countAllActivos", query = "SELECT COUNT(m.idMiembro) FROM Miembro m WHERE m.indEstadoMiembro = 'A' "),
    @NamedQuery(name = "Miembro.findAllByIdMiembro", query = "SELECT m FROM Miembro m WHERE m.idMiembro IN :lista ORDER BY m.fechaCreacion DESC"),
    @NamedQuery(name = "Miembro.findByIdMiembro", query = "SELECT m FROM Miembro m WHERE m.idMiembro = :idMiembro"),
    @NamedQuery(name = "Miembro.findByIndEstadoMiembro", query = "SELECT m FROM Miembro m WHERE m.indEstadoMiembro = :indEstadoMiembro ORDER BY m.fechaCreacion DESC")})
public class Miembro implements Serializable, Comparable<Miembro> {

    public enum ValoresIndGenero {
        FEMENINO('F'),
        MASCULINO('M');
        
        private final char value;
        private static final Map<Character,ValoresIndGenero> lookup =  new HashMap<>();

        private ValoresIndGenero(char value) {
            this.value = value;
        }
        
        static {
            for (ValoresIndGenero genero : values()) {
                lookup.put(genero.value, genero);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static ValoresIndGenero get(char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }
    
    public enum ValoresIndEstadoCivil {
        CASADO('C'),
        SOLTERO('S'),
        UNION_LIBRE('U'),
        NINGUNO('N');
        
        private final char value;
        private static final Map<Character,ValoresIndEstadoCivil> lookup =  new HashMap<>();

        private ValoresIndEstadoCivil(char value) {
            this.value = value;
        }
        
        static {
            for (ValoresIndEstadoCivil estadoCivil : values()) {
                lookup.put(estadoCivil.value, estadoCivil);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static ValoresIndEstadoCivil get(char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }
    
    public enum ValoresIndEducacion {
        PRIMARIA('P'),
        SECUNDARIA('S'),
        TECNICA('T'),
        UNIVERSITARIA('U'),
        GRADO('G'),
        POSTGRADO('R'),
        MASTER('M'),
        DOCTORADO('D');
        
        private final char value;
        private static final Map<Character,ValoresIndEducacion> lookup =  new HashMap<>();

        private ValoresIndEducacion(char value) {
            this.value = value;
        }
        
        static {
            for (ValoresIndEducacion educacion : values()) {
                lookup.put(educacion.value, educacion);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static ValoresIndEducacion get(char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }
    
    public enum Estados{
        ACTIVO('A'),
        INACTIVO('I');
        
        private final char value;
        private static final Map<Character,Estados> lookup =  new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static{
            for(Estados estado:values()){
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }
        public static Estados get(char value){
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }

    public enum Residencia{
        CIUDAD('C'),
        ESTADO('E');
         private final char value;
        private static final Map<Character,Residencia> lookup =  new HashMap<>();

        private Residencia(char value) {
            this.value = value;
        }
        
        static{
            for(Residencia residencia:values()){
                lookup.put(residencia.value, residencia);
            }
        }

        public char getValue() {
            return value;
        }
        public static Residencia get(char value){
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }
    
    
//    @Column(name = "IND_TIPO_DISPOSITIVO")
//    private Character indTipoDispositivo;
//    
//    @Column(name = "FECHA_ULTIMA_CONEXION")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date fechaUltimaConexion;
//    
//    @Size(max = 200)
//    @Column(name = "FCM_TOKEN_REGISTRO")
//    private String fcmTokenRegistro;
//    
//    @OneToMany(mappedBy = "miembroReferente")
//    private List<Miembro> miembroList;
//    
//    @JoinColumn(name = "MIEMBRO_REFERENTE", referencedColumnName = "ID_MIEMBRO")
//    @ManyToOne
//    private Miembro miembroReferente;

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DOC_IDENTIFICACION")
    private String docIdentificacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO_MIEMBRO")
    private Character indEstadoMiembro;

    @Column(name = "FECHA_EXPIRACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaExpiracion;

    @Size(max = 200)
    @Column(name = "DIRECCION")
    private String direccion;

    @Size(max = 100)
    @Column(name = "CIUDAD_RESIDENCIA")
    private String ciudadResidencia;

    @Size(max = 100)
    @Column(name = "ESTADO_RESIDENCIA")
    private String estadoResidencia;

    @Size(max = 3)
    @Column(name = "PAIS_RESIDENCIA")
    private String paisResidencia;

    @Size(max = 10)
    @Column(name = "CODIGO_POSTAL")
    private String codigoPostal;

    @Size(max = 15)
    @Column(name = "TELEFONO_MOVIL")
    private String telefonoMovil;

    @Column(name = "FECHA_NACIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;

    @Column(name = "IND_GENERO")
    private Character indGenero;

    @Column(name = "IND_ESTADO_CIVIL")
    private Character indEstadoCivil;

    @Size(max = 20)
    @Column(name = "FRECUENCIA_COMPRA")
    private String frecuenciaCompra;

    @Column(name = "FECHA_SUSPENSION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSuspension;

    @Size(max = 200)
    @Column(name = "CAUSA_SUSPENSION")
    private String causaSuspension;

    @Column(name = "IND_EDUCACION")
    private Character indEducacion;

    @Column(name = "INGRESO_ECONOMICO")
    private BigInteger ingresoEconomico;

    @Size(max = 500)
    @Column(name = "COMENTARIOS")
    private String comentarios;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    @Size(max = 300)
    @Column(name = "AVATAR")
    private String avatar;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NOMBRE")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "APELLIDO")
    private String apellido;

    @Size(max = 100)
    @Column(name = "APELLIDO2")
    private String apellido2;
    
     @Column(name = "IND_CONTACTO_EMAIL")
    private Boolean indContactoEmail;
    @Column(name = "IND_CONTACTO_SMS")
    private Boolean indContactoSms;
    @Column(name = "IND_CONTACTO_NOTIFICACION")
    private Boolean indContactoNotificacion;
    @Column(name = "IND_CONTACTO_ESTADO")
    private Boolean indContactoEstado;
    @Column(name = "IND_HIJOS")
    private Boolean indHijos;
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    

    @Transient
    private String contrasena;

    @Transient
    private String correo;

    @Transient
    private String nombreUsuario;

    @Transient
    private BigDecimal totalAcumulado;
    
    
    public Miembro() {
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getDocIdentificacion() {
        return docIdentificacion;
    }

    public void setDocIdentificacion(String docIdentificacion) {
        this.docIdentificacion = docIdentificacion;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Character getIndEstadoMiembro() {
        return indEstadoMiembro;
    }

    public void setIndEstadoMiembro(Character indEstadoMiembro) {
        this.indEstadoMiembro = indEstadoMiembro != null ? Character.toUpperCase(indEstadoMiembro) : null;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudadResidencia() {
        return ciudadResidencia;
    }

    public void setCiudadResidencia(String ciudadResidencia) {
        this.ciudadResidencia = ciudadResidencia;
    }

    public String getEstadoResidencia() {
        return estadoResidencia;
    }

    public void setEstadoResidencia(String estadoResidencia) {
        this.estadoResidencia = estadoResidencia;
    }

    public String getPaisResidencia() {
        return paisResidencia;
    }

    public void setPaisResidencia(String paisResidencia) {
        this.paisResidencia = paisResidencia;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Character getIndGenero() {
        return indGenero;
    }

    public void setIndGenero(Character indGenero) {
        this.indGenero = indGenero != null ? Character.toUpperCase(indGenero) : null;
    }

    public Boolean getIndContactoEmail() {
        return indContactoEmail;
    }

    public void setIndContactoEmail(Boolean indContactoEmail) {
        this.indContactoEmail = indContactoEmail;
    }

    public Boolean getIndContactoSms() {
        return indContactoSms;
    }

    public void setIndContactoSms(Boolean indContactoSms) {
        this.indContactoSms = indContactoSms;
    }

    public Boolean getIndContactoNotificacion() {
        return indContactoNotificacion;
    }

    public void setIndContactoNotificacion(Boolean indContactoNotificacion) {
        this.indContactoNotificacion = indContactoNotificacion;
    }

    public Boolean getIndContactoEstado() {
        return indContactoEstado;
    }

    public void setIndContactoEstado(Boolean indContactoEstado) {
        this.indContactoEstado = indContactoEstado;
    }

    public Character getIndEstadoCivil() {
        return indEstadoCivil;
    }

    public void setIndEstadoCivil(Character indEstadoCivil) {
        this.indEstadoCivil = indEstadoCivil != null ? Character.toUpperCase(indEstadoCivil) : null;
    }

    public String getFrecuenciaCompra() {
        return frecuenciaCompra;
    }

    public void setFrecuenciaCompra(String frecuenciaCompra) {
        this.frecuenciaCompra = frecuenciaCompra;
    }

    public Date getFechaSuspension() {
        return fechaSuspension;
    }

    public void setFechaSuspension(Date fechaSuspension) {
        this.fechaSuspension = fechaSuspension;
    }

    public String getCausaSuspension() {
        return causaSuspension;
    }

    public void setCausaSuspension(String causaSuspension) {
        this.causaSuspension = causaSuspension;
    }

    public Character getIndEducacion() {
        return indEducacion;
    }

    public void setIndEducacion(Character indEducacion) {
        this.indEducacion = indEducacion != null ? Character.toUpperCase(indEducacion) : null;
    }

    public BigInteger getIngresoEconomico() {
        return ingresoEconomico;
    }

    public void setIngresoEconomico(BigInteger ingresoEconomico) {
        this.ingresoEconomico = ingresoEconomico;
    }

    public Boolean getIndHijos() {
        return indHijos;
    }

    public void setIndHijos(Boolean indHijos) {
        this.indHijos = indHijos;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public BigDecimal getTotalAcumulado() {
        return totalAcumulado;
    }

    public void setTotalAcumulado(BigDecimal totalAcumulado) {
        this.totalAcumulado = totalAcumulado;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

  
    @Override
    public int compareTo(Miembro miembro) {

        return this.totalAcumulado.compareTo(miembro.getTotalAcumulado());

    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Miembro)) {
            return false;
        }
        Miembro other = (Miembro) object;
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        return true;
    }

//    @Override
//    public String toString() {
//        return "com.flecharoja.loyalty.model.Miembro[ idMiembro=" + idMiembro + " ]";
//    }

    @Override
    public String toString() {
        return idMiembro + ", " + docIdentificacion + ", " + fechaIngreso + ", " + indEstadoMiembro + ", " + fechaExpiracion + ", " + direccion + ", " + ciudadResidencia + ", " + estadoResidencia + ", " + paisResidencia + ", " + codigoPostal + ", " + telefonoMovil + ", " + fechaNacimiento + ", " + indGenero + ", " + indEstadoCivil + ", " + frecuenciaCompra + ", " + fechaSuspension + ", " + causaSuspension + ", " + indEducacion + ", " + ingresoEconomico + ", " + comentarios + ", " + fechaCreacion + ", " + usuarioCreacion + ", " + fechaModificacion + ", " + usuarioModificacion + ", " + avatar + ", " + nombre + ", " + apellido + ", " + apellido2 + ", " + contrasena + ", " + correo + ", " + nombreUsuario + ", " + indContactoEmail + ", " + indContactoSms + ", " + indContactoNotificacion + ", " + indContactoEstado + ", " + indHijos ;
    }
    
    

   

//    public Character getIndTipoDispositivo() {
//        return indTipoDispositivo;
//    }
//
//    public void setIndTipoDispositivo(Character indTipoDispositivo) {
//        this.indTipoDispositivo = indTipoDispositivo;
//    }
//
//    public Date getFechaUltimaConexion() {
//        return fechaUltimaConexion;
//    }
//
//    public void setFechaUltimaConexion(Date fechaUltimaConexion) {
//        this.fechaUltimaConexion = fechaUltimaConexion;
//    }
//
//    public String getFcmTokenRegistro() {
//        return fcmTokenRegistro;
//    }
//
//    public void setFcmTokenRegistro(String fcmTokenRegistro) {
//        this.fcmTokenRegistro = fcmTokenRegistro;
//    }
//
//    @ApiModelProperty(hidden = true)
//    @XmlTransient
//    public List<Miembro> getMiembroList() {
//        return miembroList;
//    }
//
//    public void setMiembroList(List<Miembro> miembroList) {
//        this.miembroList = miembroList;
//    }
//
//    public Miembro getMiembroReferente() {
//        return miembroReferente;
//    }
//
//    public void setMiembroReferente(Miembro miembroReferente) {
//        this.miembroReferente = miembroReferente;
//    }
}
