/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Premio.findAll", query = "SELECT p FROM Premio p ORDER BY p.fechaModificacion DESC")
    ,
    @NamedQuery(name = "Premio.findByIdPremio", query = "SELECT p FROM Premio p WHERE p.idPremio = :idPremio")
    ,
    @NamedQuery(name = "Premio.countByNombreInterno", query = "SELECT COUNT(p.idPremio) FROM Premio p WHERE p.nombreInterno = :nombreInterno")
    ,
    @NamedQuery(name = "Premio.countAll", query = "SELECT COUNT(p.idPremio) FROM Premio p")
    ,
    @NamedQuery(name = "Premio.findByNombre", query = "SELECT p FROM Premio p WHERE p.nombre = :nombre")
    ,
    @NamedQuery(name = "Premio.findByNombreInterno", query = "SELECT p FROM Premio p WHERE p.nombreInterno = :nombreInterno")
    ,
    @NamedQuery(name = "Premio.findByIndTipoPremio", query = "SELECT p FROM Premio p WHERE p.indTipoPremio = :indTipoPremio")
    ,
    @NamedQuery(name = "Premio.findByIndEnvio", query = "SELECT p FROM Premio p WHERE p.indEnvio = :indEnvio")
    ,
    @NamedQuery(name = "Premio.findByImagen", query = "SELECT p FROM Premio p WHERE p.imagenArte = :imagen")
    ,
    @NamedQuery(name = "Premio.findByIndEstado", query = "SELECT p FROM Premio p WHERE p.indEstado = :indEstado")
    ,
    @NamedQuery(name = "Premio.getIdPremioByIndEstado", query = "SELECT p.idPremio FROM Premio p WHERE p.indEstado = :indEstado")
    ,
    @NamedQuery(name = "Premio.countCodInterno", query = "SELECT COUNT(p) FROM Premio p WHERE p.codPremio = :codPremio")
})
public class Premio implements Serializable {


    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ENVIO")
    private Boolean indEnvio;

    @Basic(optional = false)
    @NotNull()
    @Column(name = "VALOR_MONEDA")
    private Double valorMoneda;

    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @Column(name = "IND_RESPUESTA")
    private Boolean indRespuesta;
    
    @Column(name = "CANT_TOTAL")
    private Long cantTotal;
    
    @Column(name = "CANT_TOTAL_MIEMBRO")
    private Long cantTotalMiembro;
    
    @Column(name = "CANT_INTERVALO_RESPUESTA")
    private Long cantIntervaloRespuesta;
    
    @Column(name = "TOTAL_EXISTENCIAS")
    private Long totalExistencias;

    @Column(name = "PRECIO_PROMEDIO")
    private Double precioPromedio;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codPremio")
    private List<HistoricoMovimientos> historicoMovimientosList;

    @Size(max = 40)
    @Column(name = "COD_PREMIO")
    private String codPremio;


    public enum Tipo {
        CERTIFICADO('C'),
        PRODUCTO('P');

        private final char value;
        private static final Map<Character, Tipo> lookup = new HashMap<>();

        private Tipo(char value) {
            this.value = value;
        }

        static {
            for (Tipo tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static Tipo get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum Efectividad {
        PERMANENTE('P'),
        CALENDARIZADO('C');

        private final char value;
        private static final Map<Character, Efectividad> lookup = new HashMap<>();

        private Efectividad(char value) {
            this.value = value;
        }

        static {
            for (Efectividad efectividad : values()) {
                lookup.put(efectividad.value, efectividad);
            }
        }

        public char getValue() {
            return value;
        }

        public static Efectividad get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum Estados {
        BORRADOR('B'),
        PUBLICADO('P'),
        ARCHIVADO('A');

        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }

        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Estados get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    public enum IntervalosTiempoRespuesta {
        POR_SIEMPRE('A'),
        POR_MINUTO('B'),
        POR_HORA('C'),
        POR_DIA('D'),
        POR_SEMANA('E'),
        POR_MES('F');

        private final char value;
        private static final Map<Character, IntervalosTiempoRespuesta> lookup = new HashMap<>();

        private IntervalosTiempoRespuesta(char value) {
            this.value = value;
        }

        static {
            for (IntervalosTiempoRespuesta intervalo : values()) {
                lookup.put(intervalo.value, intervalo);
            }
        }

        public char getValue() {
            return value;
        }

        public static IntervalosTiempoRespuesta get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "premio")
    private List<ExistenciasUbicacion> existenciasUbicacionList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "premio_uuid")
    @GenericGenerator(name = "premio_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_PREMIO")
    private String idPremio;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_PREMIO")
    private Character indTipoPremio;

    @Size(min = 1, max = 200)
    @Column(name = "DESCRIPCION")
    private String descripcion;


    @Column(name = "VALOR_EFECTIVO")
    private Double valorEfectivo;

    @Size(max = 100)
    @Column(name = "TRACKING_CODE")
    private String trackingCode;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;

    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;

    @Column(name = "FECHA_ARCHIVADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaArchivado;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    private Metrica idMetrica;

    @Size(max = 100)
    @Column(name = "SKU")
    private String sku;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN_ARTE")
    private String imagenArte;


    @Column(name = "IND_INTERVALO_TOTAL")
    private Character indIntervaloTotal;


    @Column(name = "IND_INTERVALO_MIEMBRO")
    private Character indIntervaloMiembro;

    @Column(name = "IND_INTERVALO_RESPUESTA")
    private Character indIntervaloRespuesta;

    @Basic(optional = false)
    @NotNull
    @Size(max = 150)
    @Column(name = "ENCABEZADO_ARTE")
    private String encabezadoArte;

    @Size(max = 300)
    @Column(name = "SUBENCABEZADO_ARTE")
    private String subencabezadoArte;

    @Basic(optional = false)
    @NotNull
    @Size(max = 500)
    @Column(name = "DETALLE_ARTE")
    private String detalleArte;

    @Column(name = "CANT_MIN_ACUMULADO")
    private Double cantMinAcumulado;

    @Basic(optional = false)
    @NotNull
    @Column(name = "EFECTIVIDAD_IND_CALENDARIZADO")
    private Character efectividadIndCalendarizado;

    @Column(name = "EFECTIVIDAD_FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date efectividadFechaInicio;

    @Column(name = "EFECTIVIDAD_FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date efectividadFechaFin;

    @Size(max = 7)
    @Column(name = "EFECTIVIDAD_IND_DIAS")
    private String efectividadIndDias;

    @Column(name = "EFECTIVIDAD_IND_SEMANA")
    private Integer efectividadIndSemana;

    @Transient
    private String codigoCertificado;

    @Transient
    private Date fechaRedencion;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPremio")
    private List<PremioMiembro> premioMiembroList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "premio")
    private List<CodigoCertificado> codigoCertificadoList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "premio")
    private List<PremioListaUbicacion> premioListaUbicacionList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "premio")
    private List<PremioListaMiembro> premioListaMiembroList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "premio")
    private List<PremioListaSegmento> premioListaSegmentoList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "premio")
    private List<PremioCategoriaPremio> premioCategoriaPremioList;

    public Premio() {
    }

    public Premio(String idPremio) {
        this.idPremio = idPremio;
    }

    public Premio(String idPremio, String nombre, String nombreInterno, Character indTipoPremio, Boolean indEnvio, String descripcion, Double valorMoneda, String imagenArte, Character indEstado, String usuarioCreacion, Date fechaCreacion, String usuarioModificacion, Date fechaModificacion, Long numVersion) {
        this.idPremio = idPremio;
        this.nombre = nombre;
        this.nombreInterno = nombreInterno;
        this.indTipoPremio = indTipoPremio;
        this.indEnvio = indEnvio;
        this.descripcion = descripcion;
        this.valorMoneda = valorMoneda;
        this.imagenArte = imagenArte;
        this.indEstado = indEstado;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaCreacion = fechaCreacion;
        this.usuarioModificacion = usuarioModificacion;
        this.fechaModificacion = fechaModificacion;
        this.numVersion = numVersion;
    }

    public String getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(String idPremio) {
        this.idPremio = idPremio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }

    public Character getIndTipoPremio() {
        return indTipoPremio;
    }

    public void setIndTipoPremio(Character indTipoPremio) {
        this.indTipoPremio = indTipoPremio == null ? null : Character.toUpperCase(indTipoPremio);
    }

    public Boolean getIndEnvio() {
        return indEnvio;
    }

    public void setIndEnvio(Boolean indEnvio) {
        this.indEnvio = indEnvio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getValorMoneda() {
        return valorMoneda;
    }

    public void setValorMoneda(Double valorMoneda) {
        this.valorMoneda = valorMoneda;
    }

    public Double getValorEfectivo() {
        return valorEfectivo;
    }

    public void setValorEfectivo(Double valorEfectivo) {
        this.valorEfectivo = valorEfectivo;
    }

    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Date getFechaArchivado() {
        return fechaArchivado;
    }

    public void setFechaArchivado(Date fechaArchivado) {
        this.fechaArchivado = fechaArchivado;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }

    @XmlTransient
    public List<PremioCategoriaPremio> getPremioCategoriaPremioList() {
        return premioCategoriaPremioList;
    }

    public void setPremioCategoriaPremioList(List<PremioCategoriaPremio> premioCategoriaPremioList) {
        this.premioCategoriaPremioList = premioCategoriaPremioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPremio != null ? idPremio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Premio)) {
            return false;
        }
        Premio other = (Premio) object;
        if ((this.idPremio == null && other.idPremio != null) || (this.idPremio != null && !this.idPremio.equals(other.idPremio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Premio{" + "indEnvio=" + indEnvio + ", numVersion=" + numVersion + ", indRespuesta=" + indRespuesta + ", cantIntervaloRespuesta=" + cantIntervaloRespuesta + ", idPremio=" + idPremio + ", nombre=" + nombre + ", nombreInterno=" + nombreInterno + ", indTipoPremio=" + indTipoPremio + ", descripcion=" + descripcion + ", valorMoneda=" + valorMoneda + ", valorEfectivo=" + valorEfectivo + ", trackingCode=" + trackingCode + ", indEstado=" + indEstado + ", fechaPublicacion=" + fechaPublicacion + ", fechaArchivado=" + fechaArchivado + ", usuarioCreacion=" + usuarioCreacion + ", fechaCreacion=" + fechaCreacion + ", usuarioModificacion=" + usuarioModificacion + ", fechaModificacion=" + fechaModificacion + ", idMetrica=" + idMetrica + ", sku=" + sku + ", imagenArte=" + imagenArte + ", cantTotal=" + cantTotal + ", indIntervaloTotal=" + indIntervaloTotal + ", cantTotalMiembro=" + cantTotalMiembro + ", indIntervaloMiembro=" + indIntervaloMiembro + ", indIntervaloRespuesta=" + indIntervaloRespuesta + ", encabezadoArte=" + encabezadoArte + ", subencabezadoArte=" + subencabezadoArte + ", detalleArte=" + detalleArte + ", cantMinAcumulado=" + cantMinAcumulado + ", efectividadIndCalendarizado=" + efectividadIndCalendarizado + ", efectividadFechaInicio=" + efectividadFechaInicio + ", efectividadFechaFin=" + efectividadFechaFin + ", efectividadIndDias=" + efectividadIndDias + ", efectividadIndSemana=" + efectividadIndSemana + '}';
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getImagenArte() {
        return imagenArte;
    }

    public void setImagenArte(String imagenArte) {
        this.imagenArte = imagenArte;
    }

    public Boolean getIndRespuesta() {
        return indRespuesta;
    }

    public void setIndRespuesta(Boolean indRespuesta) {
        this.indRespuesta = indRespuesta;
    }

    public Long getCantTotal() {
        return cantTotal;
    }

    public void setCantTotal(Long cantTotal) {
        this.cantTotal = cantTotal;
    }

    public Character getIndIntervaloTotal() {
        return indIntervaloTotal;
    }

    public void setIndIntervaloTotal(Character indIntervaloTotal) {
        this.indIntervaloTotal = indIntervaloTotal;
    }

    public Long getCantTotalMiembro() {
        return cantTotalMiembro;
    }

    public void setCantTotalMiembro(Long cantTotalMiembro) {
        this.cantTotalMiembro = cantTotalMiembro;
    }

    public Character getIndIntervaloMiembro() {
        return indIntervaloMiembro;
    }

    public void setIndIntervaloMiembro(Character indIntervaloMiembro) {
        this.indIntervaloMiembro = indIntervaloMiembro;
    }

    public Character getIndIntervaloRespuesta() {
        return indIntervaloRespuesta;
    }

    public void setIndIntervaloRespuesta(Character indIntervaloRespuesta) {
        this.indIntervaloRespuesta = indIntervaloRespuesta;
    }

    public String getEncabezadoArte() {
        return encabezadoArte;
    }

    public void setEncabezadoArte(String encabezadoArte) {
        this.encabezadoArte = encabezadoArte;
    }

    public String getSubencabezadoArte() {
        return subencabezadoArte;
    }

    public void setSubencabezadoArte(String subencabezadoArte) {
        this.subencabezadoArte = subencabezadoArte;
    }

    public String getDetalleArte() {
        return detalleArte;
    }

    public void setDetalleArte(String detalleArte) {
        this.detalleArte = detalleArte;
    }

    @XmlTransient
    public List<PremioListaUbicacion> getPremioListaUbicacionList() {
        return premioListaUbicacionList;
    }

    public void setPremioListaUbicacionList(List<PremioListaUbicacion> premioListaUbicacionList) {
        this.premioListaUbicacionList = premioListaUbicacionList;
    }

    @XmlTransient
    public List<PremioListaMiembro> getPremioListaMiembroList() {
        return premioListaMiembroList;
    }

    public void setPremioListaMiembroList(List<PremioListaMiembro> premioListaMiembroList) {
        this.premioListaMiembroList = premioListaMiembroList;
    }

    @XmlTransient
    public List<PremioListaSegmento> getPremioListaSegmentoList() {
        return premioListaSegmentoList;
    }

    public void setPremioListaSegmentoList(List<PremioListaSegmento> premioListaSegmentoList) {
        this.premioListaSegmentoList = premioListaSegmentoList;
    }

    public Double getCantMinAcumulado() {
        return cantMinAcumulado;
    }

    public void setCantMinAcumulado(Double cantMinAcumulado) {
        this.cantMinAcumulado = cantMinAcumulado;
    }

    public Character getEfectividadIndCalendarizado() {
        return efectividadIndCalendarizado;
    }

    public void setEfectividadIndCalendarizado(Character efectividadIndCalendarizado) {
        this.efectividadIndCalendarizado = efectividadIndCalendarizado == null ? null : Character.toUpperCase(efectividadIndCalendarizado);
    }

    public Date getEfectividadFechaInicio() {
        return efectividadFechaInicio;
    }

    public void setEfectividadFechaInicio(Date efectividadFechaInicio) {
        this.efectividadFechaInicio = efectividadFechaInicio;
    }

    public Date getEfectividadFechaFin() {
        return efectividadFechaFin;
    }

    public void setEfectividadFechaFin(Date efectividadFechaFin) {
        this.efectividadFechaFin = efectividadFechaFin;
    }

    public String getEfectividadIndDias() {
        return efectividadIndDias;
    }

    public void setEfectividadIndDias(String efectividadIndDias) {
        this.efectividadIndDias = efectividadIndDias;
    }

    public Integer getEfectividadIndSemana() {
        return efectividadIndSemana;
    }

    public void setEfectividadIndSemana(Integer efectividadIndSemana) {
        this.efectividadIndSemana = efectividadIndSemana;
    }

  

    public void setCodigoCertificadoList(List<CodigoCertificado> codigoCertificadoList) {
        this.codigoCertificadoList = codigoCertificadoList;
    }

    @XmlTransient
    public List<PremioMiembro> getPremioMiembroList() {
        return premioMiembroList;
    }

    public void setPremioMiembroList(List<PremioMiembro> premioMiembroList) {
        this.premioMiembroList = premioMiembroList;
    }

    public Long getCantIntervaloRespuesta() {
        return cantIntervaloRespuesta;
    }

    public void setCantIntervaloRespuesta(Long cantIntervaloRespuesta) {
        this.cantIntervaloRespuesta = cantIntervaloRespuesta;
    }

    public String getCodigoCertificado() {
        return codigoCertificado;
    }

    public void setCodigoCertificado(String codigoCertificado) {
        this.codigoCertificado = codigoCertificado;
    }

    public Date getFechaRedencion() {
        return fechaRedencion;
    }

    public void setFechaRedencion(Date fechaRedencion) {
        this.fechaRedencion = fechaRedencion;
    }

    @XmlTransient
    public List<ExistenciasUbicacion> getExistenciasUbicacionList() {
        return existenciasUbicacionList;
    }

    public void setExistenciasUbicacionList(List<ExistenciasUbicacion> existenciasUbicacionList) {
        this.existenciasUbicacionList = existenciasUbicacionList;
    }

  

    public String getCodPremio() {
        return codPremio;
    }

    public void setCodPremio(String codPremio) {
        this.codPremio = codPremio;
    }

    

    public Double getPrecioPromedio() {
        return precioPromedio;
    }

    public void setPrecioPromedio(Double precioPromedio) {
        this.precioPromedio = precioPromedio;
    }

    public Long getTotalExistencias() {
        return totalExistencias;
    }

    public void setTotalExistencias(Long totalExistencias) {
        this.totalExistencias = totalExistencias;
    }

    @XmlTransient
    public List<HistoricoMovimientos> getHistoricoMovimientosList() {
        return historicoMovimientosList;
    }

    public void setHistoricoMovimientosList(List<HistoricoMovimientos> historicoMovimientosList) {
        this.historicoMovimientosList = historicoMovimientosList;
    }

}
