/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.filters;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.storm.trident.operation.BaseFilter;
import org.apache.storm.trident.tuple.TridentTuple;
import com.flecharoja.loyalty.storm.util.Indicadores;
import java.io.IOException;

/**
 * Filtro encargado de la validacion de los mensajes provenientes del spout de
 * kafka, para lo cual se verifica que el indicador de evento se encuentre
 * dentro del rango valido y que los campos de la tupla no estén nulos, también
 * se verifica que el JSON de entrada esté formateado correctamente
 *
 * @author faguilar
 */
public class EventosSistemaFilter extends BaseFilter {

    /**
     * Metodo encargado de determinar si el elemento cumple con las validaciones
     * necesarias para permanecer en la topologia
     *
     * @param tuple
     * @return
     */
    @Override
    public boolean isKeep(TridentTuple tuple) {

        try {
            System.err.print("EventosSistemaFilter"+tuple.toString());
            if (!tuple.isEmpty()) {
                String jsonInput = new String(tuple.getBinary(0));
                ObjectMapper m = new ObjectMapper();
                JsonNode rootNode = m.readTree(jsonInput);
                JsonNode indEventoNode = rootNode.path("indEvento");
                //si el nodo existe
                int indEvento = indEventoNode.asInt();
                //si el id de evento se sale de indice
                if (indEvento < 0 || indEvento > Indicadores.IND_EVENTO_MAX_INDEX) {
                    System.out.println("Tupla aceptada: " + false + jsonInput);
                    return false;
                } else {
                    //si el id de evento no requiere que el mensaje tenga id de evento
                    if ((indEvento == 24 ||indEvento == 23 || indEvento == 25 || indEvento == 26)) {
                        JsonNode idMiembroNode = rootNode.path("idMiembro");
                        boolean valida = !(idMiembroNode.isMissingNode()) && idMiembroNode.asText().length() > 32 && idMiembroNode.asText().length() < 40;
//                      System.out.println("Tupla aceptada: " + valida +jsonInput);
                        System.out.println("Tupla aceptada: " + valida + jsonInput);
                        return valida;
                    } else {
                        JsonNode idMiembroNode = rootNode.path("idMiembro");
                        JsonNode idElementoNode = rootNode.path("idElemento");
                        boolean valida = (!(idElementoNode.isMissingNode()) && idElementoNode.asText().length() > 32 && idElementoNode.asText().length() < 40)
                                && (!(idMiembroNode.isMissingNode()) && idMiembroNode.asText().length() > 32 && idMiembroNode.asText().length() < 40);
//                      System.out.println("Tupla aceptada: " + valida +jsonInput);
                        System.out.println("Tupla aceptada: " + valida + jsonInput);
                        return valida;
                    }
                }
            }
            return true;
        } catch (IOException ex) {
            System.out.println("Tupla rechazada" + new String(tuple.getBinary(0)));
            //si hay un error al parsear el json, entonces se descarta la tupla
            return false;
        }
    }
}
