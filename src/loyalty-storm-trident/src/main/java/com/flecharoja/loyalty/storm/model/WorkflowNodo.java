/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "WORKFLOW_NODO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WorkflowNodo.findAll", query = "SELECT w FROM WorkflowNodo w")
    , @NamedQuery(name = "WorkflowNodo.findByIdNodo", query = "SELECT w FROM WorkflowNodo w WHERE w.idNodo = :idNodo")
    , @NamedQuery(name = "WorkflowNodo.findByIdSucesor", query = "SELECT w FROM WorkflowNodo w WHERE w.idSucesor.idSucesor = :idNodo")
    , @NamedQuery(name = "WorkflowNodo.findByNombreNodo", query = "SELECT w FROM WorkflowNodo w WHERE w.nombreNodo = :nombreNodo")
    , @NamedQuery(name = "WorkflowNodo.findByIndTipoTarea", query = "SELECT w FROM WorkflowNodo w WHERE w.indTipoTarea = :indTipoTarea")
    , @NamedQuery(name = "WorkflowNodo.findByReferencia", query = "SELECT w FROM WorkflowNodo w WHERE w.referencia = :referencia")
    , @NamedQuery(name = "WorkflowNodo.findByNumVersion", query = "SELECT w FROM WorkflowNodo w WHERE w.numVersion = :numVersion")
    , @NamedQuery(name = "WorkflowNodo.findByFechaCreacion", query = "SELECT w FROM WorkflowNodo w WHERE w.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "WorkflowNodo.findByFechaModificacion", query = "SELECT w FROM WorkflowNodo w WHERE w.fechaModificacion = :fechaModificacion")
    , @NamedQuery(name = "WorkflowNodo.findByUsuarioCreacion", query = "SELECT w FROM WorkflowNodo w WHERE w.usuarioCreacion = :usuarioCreacion")
    , @NamedQuery(name = "WorkflowNodo.findByUsuarioModificacion", query = "SELECT w FROM WorkflowNodo w WHERE w.usuarioModificacion = :usuarioModificacion")})
public class WorkflowNodo implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private long numVersion;
    @Column(name = "IS_NODO_MISION_ESPEC")
    private boolean isNodoMisionEspec;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "workflowNodo")
    private Collection<WorkflowNodoEspera> workflowNodoEsperaCollection;
    @OneToOne(mappedBy = "idSucesor")
    private WorkflowNodo workflowNodo;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_NODO")
    private String idNodo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "NOMBRE_NODO")
    private String nombreNodo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_TAREA")
    private Character indTipoTarea;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "REFERENCIA")
    private String referencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    @OneToMany(mappedBy = "idSucesor")
    private Collection<WorkflowNodo> workflowNodoCollection;
    @JoinColumn(name = "ID_SUCESOR", referencedColumnName = "ID_NODO")
    @ManyToOne
    private WorkflowNodo idSucesor;
    @OneToMany(mappedBy = "idSucesor")
    private Collection<Workflow> workflowCollection;
    @Basic(optional = true)
    @Column(name = "CANT_REFERENCIA")
    private Double cantReferencia;

    public WorkflowNodo(String idNodo, String nombreNodo, Character indTipoTarea, String referencia, Long numVersion, Date fechaCreacion, Date fechaModificacion, String usuarioCreacion, String usuarioModificacion, Collection<WorkflowNodo> workflowNodoCollection, WorkflowNodo idSucesor, Collection<Workflow> workflowCollection, Double cantReferencia) {
        this.idNodo = idNodo;
        this.nombreNodo = nombreNodo;
        this.indTipoTarea = indTipoTarea;
        this.referencia = referencia;
        this.numVersion = numVersion;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioCreacion = usuarioCreacion;
        this.usuarioModificacion = usuarioModificacion;
        this.workflowNodoCollection = workflowNodoCollection;
        this.idSucesor = idSucesor;
        this.workflowCollection = workflowCollection;
        this.cantReferencia = cantReferencia;
    }

    public Double getCantReferencia() {
        return cantReferencia;
    }

    public void setCantReferencia(Double cantReferencia) {
        this.cantReferencia = cantReferencia;
    }

    public String getIdNodo() {
        return idNodo;
    }

    public WorkflowNodo() {
    }

    public WorkflowNodo(String idNodo) {
        this.idNodo = idNodo;
    }

    public void setIdNodo(String idNodo) {
        this.idNodo = idNodo;
    }

    public String getNombreNodo() {
        return nombreNodo;
    }

    public void setNombreNodo(String nombreNodo) {
        this.nombreNodo = nombreNodo;
    }

    public Character getIndTipoTarea() {
        return indTipoTarea;
    }

    public void setIndTipoTarea(Character indTipoTarea) {
        this.indTipoTarea = indTipoTarea;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }
   
    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    @XmlTransient
    public Collection<WorkflowNodo> getWorkflowNodoCollection() {
        return workflowNodoCollection;
    }

    public void setWorkflowNodoCollection(Collection<WorkflowNodo> workflowNodoCollection) {
        this.workflowNodoCollection = workflowNodoCollection;
    }

    public WorkflowNodo getIdSucesor() {
        return idSucesor;
    }

    public void setIdSucesor(WorkflowNodo idSucesor) {
        this.idSucesor = idSucesor;
    }

    @XmlTransient
    public Collection<Workflow> getWorkflowCollection() {
        return workflowCollection;
    }

    public void setWorkflowCollection(Collection<Workflow> workflowCollection) {
        this.workflowCollection = workflowCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNodo != null ? idNodo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkflowNodo)) {
            return false;
        }
        WorkflowNodo other = (WorkflowNodo) object;
        if ((this.idNodo == null && other.idNodo != null) || (this.idNodo != null && !this.idNodo.equals(other.idNodo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.WorkflowNodo[ idNodo= " + idNodo +", nombre= "+nombreNodo+ " ]";
    }

    @XmlTransient
    public Collection<WorkflowNodoEspera> getWorkflowNodoEsperaCollection() {
        return workflowNodoEsperaCollection;
    }

    public void setWorkflowNodoEsperaCollection(Collection<WorkflowNodoEspera> workflowNodoEsperaCollection) {
        this.workflowNodoEsperaCollection = workflowNodoEsperaCollection;
    }

    public WorkflowNodo getWorkflowNodo() {
        return workflowNodo;
    }

    public void setWorkflowNodo(WorkflowNodo workflowNodo) {
        this.workflowNodo = workflowNodo;
    }


    public void setNumVersion(long numVersion) {
        this.numVersion = numVersion;
    }

    public boolean getIsNodoMisionEspec() {
        return isNodoMisionEspec;
    }

    public void setIsNodoMisionEspec(boolean isNodoMisionEspec) {
        this.isNodoMisionEspec = isNodoMisionEspec;
    }

}
