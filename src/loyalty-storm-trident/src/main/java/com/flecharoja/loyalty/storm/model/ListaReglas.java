package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "LISTA_REGLAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ListaReglas.countByIdSegmento", query = "SELECT COUNT(l.idSegmento) FROM ListaReglas l WHERE l.idSegmento.idSegmento = :idSegmento"),
    @NamedQuery(name = "ListaReglas.findByIdLista", query = "SELECT l FROM ListaReglas l WHERE l.idLista = :idLista"),
    @NamedQuery(name = "ListaReglas.findByIdSegmento", query = "SELECT l FROM ListaReglas l WHERE l.idSegmento.idSegmento = :idSegmento ORDER BY l.orden")})
public class ListaReglas implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "listareglas_uuid")
    @GenericGenerator(name = "listareglas_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_LISTA")
    private String idLista;
    
    @Basic(optional = false)
    @NotNull
    @Size(min=1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_OPERADOR")
    private Character indOperador;
    
    @Column(name = "ORDEN")
    private BigInteger orden;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @JoinColumn(name = "ID_SEGMENTO", referencedColumnName = "ID_SEGMENTO")
    @ManyToOne(optional = false)
    private Segmento idSegmento;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLista")
    private List<ReglaMiembroAtb> reglaMiembroAtbList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLista")
    private List<ReglaMetricaCambio> reglaMetricaCambioList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLista")
    private List<ReglaMetricaBalance> reglaMetricaBalanceList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "listaReglas")
    private List<ReglaAvanzada> reglaAvanzadaList;
    
    public ListaReglas() {
    }

    public String getIdLista() {
        return idLista;
    }

    public void setIdLista(String idLista) {
        this.idLista = idLista;
    }

    public Character getIndOperador() {
        return indOperador;
    }

    public void setIndOperador(Character indOperador) {
        this.indOperador = indOperador!=null?Character.toUpperCase(indOperador):null;
    }

    public BigInteger getOrden() {
        return orden;
    }

    public void setOrden(BigInteger orden) {
        this.orden = orden;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    
    @XmlTransient
    public Segmento getIdSegmento() {
        return idSegmento;
    }
    
    public void setIdSegmento(Segmento idSegmento) {
        this.idSegmento = idSegmento;
    }
    
    
    @XmlTransient
    public List<ReglaMiembroAtb> getReglaMiembroAtbList() {
        return reglaMiembroAtbList;
    }

    public void setReglaMiembroAtbList(List<ReglaMiembroAtb> reglaMiembroAtbList) {
        this.reglaMiembroAtbList = reglaMiembroAtbList;
    }

    
    @XmlTransient
    public List<ReglaMetricaCambio> getReglaMetricaCambioList() {
        return reglaMetricaCambioList;
    }

    public void setReglaMetricaCambioList(List<ReglaMetricaCambio> reglaMetricaCambioList) {
        this.reglaMetricaCambioList = reglaMetricaCambioList;
    }

    
    @XmlTransient
    public List<ReglaMetricaBalance> getReglaMetricaBalanceList() {
        return reglaMetricaBalanceList;
    }

    public void setReglaMetricaBalanceList(List<ReglaMetricaBalance> reglaMetricaBalanceList) {
        this.reglaMetricaBalanceList = reglaMetricaBalanceList;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    @XmlTransient
    public List<ReglaAvanzada> getReglaAvanzadaList() {
        return reglaAvanzadaList;
    }

    public void setReglaAvanzadaList(List<ReglaAvanzada> reglaAvanzadaList) {
        this.reglaAvanzadaList = reglaAvanzadaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLista != null ? idLista.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ListaReglas)) {
            return false;
        }
        ListaReglas other = (ListaReglas) object;
        if ((this.idLista == null && other.idLista != null) || (this.idLista != null && !this.idLista.equals(other.idLista))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ListaReglas[ idLista=" + idLista + " ]";
    }
    
}
