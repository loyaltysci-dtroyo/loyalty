/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "MISION_JUEGO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Juego.findAll", query = "SELECT j FROM Juego j"),
    @NamedQuery(name = "Juego.findImagen", query = "SELECT j.imagen FROM Juego j WHERE j.idMision = :idJuego"),
    @NamedQuery(name = "Juego.findIdMision", query = "SELECT j FROM Juego j WHERE j.idMision = :idMision")
})
public class Juego implements Serializable {

    @Version
    @Basic(optional = false)
    @NotNull()
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @Column(name = "TIEMPO")
    private Integer tiempo;
    
    @Column(name = "IMAGEN")
    private String imagen;
    
    @Column(name = "IMAGENES_ROMPECABEZAS")
    private String imagenesRompecabezas;

    public enum Tipo{
        RULETA('R'),
        RASPADITA('A'),
        ROMPECABEZAS('O');
        
        private final char value;
        private static final Map<Character,Tipo> lookup= new HashMap<>();

        private Tipo(char value) {
            this.value = value;
        }
        
        static{
            for(Tipo tipo : values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        public static Tipo get(Character  value){
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum Estrategia{
        RANDOM('R'),
        PROBABILIDAD('P');
        
        private final char value;
        private static final Map<Character,Estrategia> lookup= new HashMap<>();

        private Estrategia(char value) {
            this.value = value;
        }
        
        static{
            for(Estrategia estrategia : values()){
                lookup.put(estrategia.value, estrategia);
            }
        }

        public char getValue() {
            return value;
        }
        public static Estrategia get(Character  value){
            return value==null?null:lookup.get(value);
        }
    }
    
    
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO")
    private Character tipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTRATEGIA")
    private Character estrategia;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ID_MISION")
    private String idMision;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idJuego")
    private List<MisionJuego> misionJuegoList;
    
    @JoinColumn(name = "ID_MISION", referencedColumnName = "ID_MISION", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Mision mision;

    public Juego() {
    }

    public Juego(String idMision) {
        this.idMision = idMision;
    }

    public Juego(String idMision, Character tipo, Character estrategia, Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.idMision = idMision;
        this.tipo = tipo;
        this.estrategia = estrategia;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public Character getEstrategia() {
        return estrategia;
    }

    public void setEstrategia(Character estrategia) {
        this.estrategia = estrategia;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getIdMision() {
        return idMision;
    }

    public void setIdMision(String idMision) {
        this.idMision = idMision;
    }

    @XmlTransient
    @JsonIgnore
    public List<MisionJuego> getMisionJuegoList() {
        return misionJuegoList;
    }

    public void setMisionJuegoList(List<MisionJuego> misionJuegoList) {
        this.misionJuegoList = misionJuegoList;
    }

    public Mision getMision() {
        return mision;
    }

    public void setMision(Mision mision) {
        this.mision = mision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMision != null ? idMision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Juego)) {
            return false;
        }
        Juego other = (Juego) object;
        if ((this.idMision == null && other.idMision != null) || (this.idMision != null && !this.idMision.equals(other.idMision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Juego{" + "numVersion=" + numVersion + ", tipo=" + tipo + ", estrategia=" + estrategia + ", fechaCreacion=" + fechaCreacion + ", usuarioCreacion=" + usuarioCreacion + ", fechaModificacion=" + fechaModificacion + ", usuarioModificacion=" + usuarioModificacion + ", idMision=" + idMision + '}';
    }


    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }


    public Integer getTiempo() {
        return tiempo;
    }

    public void setTiempo(Integer tiempo) {
        this.tiempo = tiempo;
    }

    public String getImagenesRompecabezas() {
        return imagenesRompecabezas;
    }

    public void setImagenesRompecabezas(String imagenesRompecabezas) {
        this.imagenesRompecabezas = imagenesRompecabezas;
    }
   
    
}
