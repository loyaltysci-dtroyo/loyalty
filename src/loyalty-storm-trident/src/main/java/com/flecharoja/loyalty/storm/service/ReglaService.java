package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.model.ReglaAvanzada;
import com.flecharoja.loyalty.storm.model.ReglaMetricaBalance;
import com.flecharoja.loyalty.storm.model.ReglaMetricaCambio;
import com.flecharoja.loyalty.storm.model.ReglaMiembroAtb;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * Bean encargado de obtner definiciones de todos los tipos de reglas: Balance
 * de metrica, Atributo de miembro, Cambio de metrica y Regla avanzada
 *
 * @author faguilar
 */
public class ReglaService {

    public ReglaService() {

    }

    /**
     * Metodo que obtiene la lista de reglas para todos los segmentos
     *
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List<ReglaMetricaCambio> getReglasMetricaCambioSegmentosActivos() {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultList = em.createNamedQuery("ReglaMetricaCambio.getAllReglasInSegmentoActivo").getResultList();
        em.close();
        return resultList;

    }

    /**
     * Metodo que obtiene la lista de reglas para todos los segmentos
     *
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List<ReglaMiembroAtb> getReglasAtributoMiembroSegmentosActivos() {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultList = em.createNamedQuery("ReglaMiembroAtb.getAllReglasInSegmentoActivo").getResultList();
        em.close();
        return resultList;

    }

    /**
     * Metodo que obtiene la lista de reglas para todos los segmentos
     *
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List<ReglaMetricaBalance> getReglasMetricaBalanceSegmentosActivos() {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultList = em.createNamedQuery("ReglaMetricaBalance.getAllReglasInSegmentoActivo").getResultList();
        em.close();
        return resultList;

    }

    /**
     * Metodo que obtiene la lista de reglas para todos los segmentos
     *
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List<ReglaAvanzada> getReglasAvanzadasSegmentosActivos() {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultList = em.createNamedQuery("ReglaAvanzada.getAllReglasInSegmentoActivo").getResultList();
        em.close();
        return resultList;

    }
}
