/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PRODUCTO_CATEG_SUBCATEG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoCategSubcateg.findAll", query = "SELECT p FROM ProductoCategSubcateg p"),
    @NamedQuery(name = "ProductoCategSubcateg.findByIdProducto", query = "SELECT p FROM ProductoCategSubcateg p WHERE p.productoCategSubcategPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoCategSubcateg.findByIdSubcategoria", query = "SELECT p FROM ProductoCategSubcateg p WHERE p.productoCategSubcategPK.idSubcategoria = :idSubcategoria"),
    @NamedQuery(name = "ProductoCategSubcateg.findByFechaCreacion", query = "SELECT p FROM ProductoCategSubcateg p WHERE p.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "ProductoCategSubcateg.findByUsuarioCreacion", query = "SELECT p FROM ProductoCategSubcateg p WHERE p.usuarioCreacion = :usuarioCreacion"),
    @NamedQuery(name = "ProductoCategSubcateg.findByIdProductoByIdSubcategoria", query = "SELECT p FROM ProductoCategSubcateg p WHERE p.productoCategSubcategPK.idProducto = :idProducto AND p.productoCategSubcategPK.idSubcategoria = :idSubcategoria"),
    @NamedQuery(name = "ProductoCategSubcateg.findProductoNotInSubcategoria", query = "SELECT p FROM Producto p  WHERE   p.idProducto NOT IN (SELECT c.productoCategSubcategPK.idProducto FROM ProductoCategSubcateg c WHERE c.productoCategSubcategPK.idSubcategoria = :idSubcategoria)"),
    @NamedQuery(name = "ProductoCategSubcateg.findCategoriaNotInProducto", query = "SELECT c.idCategoria FROM SubcategoriaProducto c WHERE c.idSubcategoria NOT IN (SELECT p.productoCategSubcategPK.idSubcategoria FROM ProductoCategSubcateg p WHERE p.productoCategSubcategPK.idProducto = :idProducto)")
})
    

public class ProductoCategSubcateg implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoCategSubcategPK productoCategSubcategPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_CATEGORIA", referencedColumnName = "ID_CATEGORIA")
    @ManyToOne(optional = false)
    private CategoriaProducto idCategoria;
    
    @JoinColumn(name = "ID_PRODUCTO", referencedColumnName = "ID_PRODUCTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    
    @JoinColumn(name = "ID_SUBCATEGORIA", referencedColumnName = "ID_SUBCATEGORIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SubcategoriaProducto subcategoriaProducto;

    public ProductoCategSubcateg() {
    }

    public ProductoCategSubcateg(ProductoCategSubcategPK productoCategSubcategPK) {
        this.productoCategSubcategPK = productoCategSubcategPK;
    }

    public ProductoCategSubcateg(String idProducto, String idSubcategoria, CategoriaProducto categoria, Date fechaCreacion, String usuarioCreacion) {
        this.productoCategSubcategPK = new ProductoCategSubcategPK(idProducto, idSubcategoria);
        this.idCategoria = categoria;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public ProductoCategSubcateg(String idProducto, String idSubcategoria) {
        this.productoCategSubcategPK = new ProductoCategSubcategPK(idProducto, idSubcategoria);
    }

    public ProductoCategSubcategPK getProductoCategSubcategPK() {
        return productoCategSubcategPK;
    }

    public void setProductoCategSubcategPK(ProductoCategSubcategPK productoCategSubcategPK) {
        this.productoCategSubcategPK = productoCategSubcategPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public CategoriaProducto getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(CategoriaProducto idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public SubcategoriaProducto getSubcategoriaProducto() {
        return subcategoriaProducto;
    }

    public void setSubcategoriaProducto(SubcategoriaProducto subcategoriaProducto) {
        this.subcategoriaProducto = subcategoriaProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoCategSubcategPK != null ? productoCategSubcategPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoCategSubcateg)) {
            return false;
        }
        ProductoCategSubcateg other = (ProductoCategSubcateg) object;
        if ((this.productoCategSubcategPK == null && other.productoCategSubcategPK != null) || (this.productoCategSubcategPK != null && !this.productoCategSubcategPK.equals(other.productoCategSubcategPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ProductoCategSubcateg[ productoCategSubcategPK=" + productoCategSubcategPK + " ]";
    }
    
}
