/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "INSIGNIAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Insignias.findAll", query = "SELECT i FROM Insignias i"),
    @NamedQuery(name = "Insignias.countAll", query = "SELECT COUNT(i.idInsignia) FROM Insignias i"),
    @NamedQuery(name = "Insignias.countByNombreInterno", query = "SELECT COUNT(i.idInsignia) FROM Insignias i WHERE i.nombreInterno = :nombreInterno"),
    @NamedQuery(name = "Insignias.findByIdInsignia", query = "SELECT i FROM Insignias i WHERE i.idInsignia = :idInsignia"),
    @NamedQuery(name = "Insignias.findByNombreInsignia", query = "SELECT i FROM Insignias i WHERE i.nombreInsignia = :nombreInsignia"),
    @NamedQuery(name = "Insignias.findByDescripcion", query = "SELECT i FROM Insignias i WHERE i.descripcion = :descripcion"),
    @NamedQuery(name = "Insignias.findByTipo", query = "SELECT i FROM Insignias i WHERE i.tipo = :tipo"),
    @NamedQuery(name = "Insignias.findByImagen", query = "SELECT i FROM Insignias i WHERE i.imagen = :imagen"),
    @NamedQuery(name = "Insignias.findByEstado", query = "SELECT i FROM Insignias i WHERE i.estado = :estado")
})
public class Insignias implements Serializable {

   
     
   public enum Estados{
       BORRADOR('B'),
       PUBLICADO('P'),
       ARCHIVADO('A');
       
       private final char value;
       private static final Map<Character,Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
       
       static{
           for(Estados estado : values()){
               lookup.put(estado.value, estado);
           }
       }

        public char getValue() {
            return value;
        }
       
        public static Estados get (char value){
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
   }
   
   public enum Tipos{
       COLLECTOR('C'),
       STATUS('S');
       
       private final char value;
       private static final Map<Character,Tipos> lookup = new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }
       
       static{
           for(Tipos tipo : values()){
               lookup.put(tipo.value, tipo);
           }
       }

        public char getValue() {
            return value;
        }
       
        public static Tipos get(char  value){
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
   }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "insignias_uuid")
    @GenericGenerator(name = "insignias_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_INSIGNIA")
    private String idInsignia;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INSIGNIA")
    private String nombreInsignia;
    
    @Size(min = 1, max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO")
    private Character tipo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    private String imagen;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTADO")
    private Character estado;
    
    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;
    
    @Column(name = "FECHA_ARCHIVADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaArchivado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;
    
    @Version 
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
     
    
    
     
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insignias")
    private List<MiembroInsigniaNivel> miembroInsigniaNivelList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInsignia")
    private List<NivelesInsignias> nivelesInsigniasList;

    public Insignias() {
    }

    public Insignias(String idInsignia) {
        this.idInsignia = idInsignia;
    }

    public Insignias(String idInsignia, String nombreInsignia, String descripcion, Character tipo, String imagen, Character estado, Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.idInsignia = idInsignia;
        this.nombreInsignia = nombreInsignia;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.imagen = imagen;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public String getIdInsignia() {
        return idInsignia;
    }

    public void setIdInsignia(String idInsignia) {
        this.idInsignia = idInsignia;
    }

    public String getNombreInsignia() {
        return nombreInsignia;
    }

    public void setNombreInsignia(String nombreInsignia) {
        this.nombreInsignia = nombreInsignia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Date getFechaArchivado() {
        return fechaArchivado;
    }

    public void setFechaArchivado(Date fechaArchivado) {
        this.fechaArchivado = fechaArchivado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    
    @XmlTransient
    public List<NivelesInsignias> getNivelesInsigniasList() {
        return nivelesInsigniasList;
    }

    public void setNivelesInsigniasList(List<NivelesInsignias> nivelesInsigniasList) {
        this.nivelesInsigniasList = nivelesInsigniasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInsignia != null ? idInsignia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Insignias)) {
            return false;
        }
        Insignias other = (Insignias) object;
        if ((this.idInsignia == null && other.idInsignia != null) || (this.idInsignia != null && !this.idInsignia.equals(other.idInsignia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Insignias[ idInsignia=" + idInsignia + " ]";
    }

  

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }

   
    
    @XmlTransient
    public List<MiembroInsigniaNivel> getMiembroInsigniaNivelList() {
        return miembroInsigniaNivelList;
    }

    public void setMiembroInsigniaNivelList(List<MiembroInsigniaNivel> miembroInsigniaNivelList) {
        this.miembroInsigniaNivelList = miembroInsigniaNivelList;
    }   
}
