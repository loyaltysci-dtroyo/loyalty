package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.exception.RecursoNoEncontradoException;
import com.flecharoja.loyalty.storm.model.Miembro;
import com.flecharoja.loyalty.storm.model.Segmento;
import com.flecharoja.loyalty.storm.model.SegmentoListaMiembro;
import com.flecharoja.loyalty.storm.model.SegmentoListaMiembroPK;
import com.flecharoja.loyalty.storm.util.CodigosErrores;
import com.flecharoja.loyalty.storm.util.Indicadores;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * la lista de miembros excluidos / incluidos de un segmento
 *
 * @author svargas
 */
public class SegmentoListaMiembroService {

    /**
     * Metodo que obtiene un listado de los miembros excluidos e incluidos o
     * ambos
     *
     * @param idSegmento Identificador del segmento
     * @param indTipo Indicador del tipo de miembros incluidos/excluidos
     * @return Respuesta con una lista de miembros en un rango de resultados
     */
    public List<Miembro> getMiembrosPorSegmento(String idSegmento, Character indTipo) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        try {
            em.getReference(Segmento.class, idSegmento).getIdSegmento();
        } catch (EntityNotFoundException e) {
            em.close();
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        List<Miembro> resultado = null;
        try {
            if (indTipo == null) {//en el caso de que no se haya pasado algun indicador de tipo, se obtienen todos
                //obtencion de la lista en un rango valido
                resultado = em.createNamedQuery("SegmentoListaMiembro.findMiembrosByIdSegmento")
                        .setParameter("idSegmento", idSegmento)
                        .getResultList();
            } else {
                //obtencion de la lista en un rango valido
                resultado = em.createNamedQuery("SegmentoListaMiembro.findMiembrosByIdSegmentoIndTipo")
                        .setParameter("indTipo", indTipo)
                        .setParameter("idSegmento", idSegmento)
                        .getResultList();
            }
        } catch (IllegalArgumentException e) {
            em.close();
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        em.close();
        return resultado;
    }

    /**
     * Metodo que obtiene un listado de los miembros excluidos e incluidos o
     * ambos filtrados adicionamente por palabras claves
     *
     * @param idSegmento Identificador del segmento
     * @param indTipo Indicador del tipo de miembros incluidos/excluidos
     * @param busqueda Cadena de texto con palabras claves
     * @return Respuesta con una lista de miembros en un rango de resultados
     */
    public List<Miembro> searchMiembros(String idSegmento, Character indTipo, String busqueda) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List<Miembro> resultado = null;
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        String[] terminos = busqueda.split(" ");

        if (indTipo == null) {
            Root<SegmentoListaMiembro> root = query.from(SegmentoListaMiembro.class);
            List<Predicate> predicate0 = new ArrayList<>();
            for (String termino : terminos) {
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("idMiembro")), "%" + termino.toLowerCase() + "%"));
            }
            query.where(cb.or(predicate0.toArray(new Predicate[predicate0.size()])),
                    cb.and(cb.equal(root.get("segmento").get("idSegmento"), idSegmento)));
            try {
                resultado = em.createQuery(query.select(root.get("miembro")))
                        .getResultList()
                        .stream().map((Object t) -> (Miembro) t).collect(Collectors.toList());
            } catch (IllegalArgumentException e) {
                em.close();
                throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
            }
        } else {
            Root<SegmentoListaMiembro> root = query.from(SegmentoListaMiembro.class);
            List<Predicate> predicate0 = new ArrayList<>();
            for (String termino : terminos) {
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("idMiembro")), "%" + termino.toLowerCase() + "%"));
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("nombre")), "%" + termino.toLowerCase() + "%"));
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("apellido")), "%" + termino.toLowerCase() + "%"));
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("apellido2")), "%" + termino.toLowerCase() + "%"));
                predicate0.add(cb.like(cb.lower(root.get("miembro").get("docIdentificacion")), "%" + termino.toLowerCase() + "%"));
            }

            query.where(cb.or(predicate0.toArray(new Predicate[predicate0.size()])),
                    cb.and(cb.equal(root.get("segmento").get("idSegmento"), idSegmento)),
                    cb.and(cb.equal(root.get("indTipo"), indTipo)));
        }
        em.close();
        return resultado;
    }

    /**
     * Metodo que incluye o excluye en un segmento a un miembro
     *
     * @param idSegmento Identificador del segmento
     * @param idMiembro Identificador del miembro
     * @param accion Indicador de la accion deseada (incluir, excluir)
     * @param usuario Identificador del usuario creador
     */
    public void includeExcludeMiembro(String idSegmento, String idMiembro, Character accion, String usuario) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        //asignacion de valores por defecto
        Date date = Calendar.getInstance().getTime();
        //creacion de la entidad
        SegmentoListaMiembro segmentoListaMiembro = new SegmentoListaMiembro(idMiembro, idSegmento, accion, date, usuario);
        em.merge(segmentoListaMiembro);
        em.flush();
        em.close();
    }

    /**
     * Metodo que elimina de la inclusion o exclusion en un segmento a un
     * miembro
     *
     * @param idSegmento Identificador del segmento
     * @param idMiembro Identificador del miembro
     * @param usuario usuario en sesion
     */
    public void removeMiembro(String idSegmento, String idMiembro, String usuario) throws RecursoNoEncontradoException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        //eliminacion de la entidad (si es que existe)
        em.remove(em.getReference(SegmentoListaMiembro.class, new SegmentoListaMiembroPK(idMiembro, idSegmento)));
        em.flush();
        em.close();

    }

    /**
     * Metodo que incluye o excluye en un segmento a un miembro
     *
     * @param idSegmento Identificador del segmento
     * @param listaIdMiembro Identificadores de miembros
     * @param accion Indicador de la accion deseada (incluir, excluir)
     * @param usuario usuario sesión
     */
    public void includeExcludeBatchMiembro(String idSegmento, List<String> listaIdMiembro, Character accion, String usuario) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        //verificacion que los id's sean validos
        Segmento segmento = em.getReference(Segmento.class, idSegmento);

        listaIdMiembro.stream().forEach((idMiembro) -> {
            em.getReference(Miembro.class, idMiembro).getIdMiembro();
        });
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            em.close();
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        //conversion del valor a su par en mayuscula y comprobacion que su valor sea valido
        accion = Character.toUpperCase(accion);
        if (accion.compareTo(Indicadores.EXCLUIDO) != 0 && accion.compareTo(Indicadores.INCLUIDO) != 0) {
            em.close();
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        //asignacion de valores por defecto
        Date date = Calendar.getInstance().getTime();
        //creacion de la entidad
        for (String idMiembro : listaIdMiembro) {
            em.merge(new SegmentoListaMiembro(idMiembro, idSegmento, accion, date, usuario));
        }
        em.flush();
        em.close();
    }

    /**
     * Metodo que elimina de la inclusion o exclusion en un segmento a un
     * miembro
     *
     * @param idSegmento Identificador del segmento
     * @param listaIdMiembro Identificadores de miembros
     * @param usuario usuario en sesión
     */
    public void removeBatchMiembro(String idSegmento, List<String> listaIdMiembro, String usuario) throws ErrorSistemaException, RecursoNoEncontradoException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Segmento segmento = em.getReference(Segmento.class, idSegmento);

        listaIdMiembro.stream().forEach((idMiembro) -> {
            em.getReference(Miembro.class, idMiembro).getIdMiembro();
        });
        if (segmento.getIndEstado().equals(Segmento.Estados.ARCHIVADO.getValue())) {
            em.close();
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        listaIdMiembro.stream().forEach((idMiembro) -> {
            //eliminacion de la entidad (si es que existe)
            em.remove(em.getReference(SegmentoListaMiembro.class, new SegmentoListaMiembroPK(idMiembro, idSegmento)));
        });
        em.flush();
        em.close();
    }
}
