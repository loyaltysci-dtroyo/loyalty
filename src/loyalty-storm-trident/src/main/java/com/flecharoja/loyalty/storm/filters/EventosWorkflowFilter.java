/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.filters;

import org.apache.storm.trident.operation.BaseFilter;
import org.apache.storm.trident.tuple.TridentTuple;

/**
 * Encargado de la validacion de los mensajes de procesamiento de eventos 
 * concretamente se encarga de verificar que los mensajes cumplen con el formato
 * dado y que los datos estan en un formato correcto
 * @author faguilar
 */
public class EventosWorkflowFilter extends BaseFilter  {
    
    public EventosWorkflowFilter() {
    }


    @Override
    public boolean isKeep(TridentTuple tuple) {
      return true;
    }
    
}
