package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "REGLA_AVANZADA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReglaAvanzada.getAllReglasInSegmentoActivo", query = "SELECT r FROM ReglaAvanzada r WHERE r.listaReglas.idSegmento.indEstado='AC'"),
    @NamedQuery(name = "ReglaAvanzada.findByIdLista", query = "SELECT r FROM ReglaAvanzada r WHERE r.listaReglas.idLista = :idLista")
})
public class ReglaAvanzada implements Serializable {
    
    public enum TiposDispositivos {
        ANDROID('A'),
        IOS('I'),
        DESCONOCIDO('D');
        
        private final char value;
        private static final Map<Character, TiposDispositivos> lookup = new HashMap<>();

        private TiposDispositivos(char value) {
            this.value = value;
        }
        
        static {
            for (TiposDispositivos tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static TiposDispositivos get (char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }
    
    public enum TiposUltimaFecha {
        DIA('D'),
        MES('M'),
        ANO('A');
        
        private final char value;
        private static final Map<Character, TiposUltimaFecha> lookup = new HashMap<>();

        private TiposUltimaFecha(char value) {
            this.value = value;
        }
        
        static {
            for (TiposUltimaFecha tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static TiposUltimaFecha get (char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }
    
    public enum TiposRegla {
        UNIERON_PROGRAMA('A'),
        RECIBIERON_MENSAJE('B'),
        ABRIERON_MENSAJE('C'),
        REDIMIERON_PREMIO('D'),
        GASTARON_DINERO('F'),
        ULTIMA_CONEXION('G'),
        REFERENCIAS_MIEMBROS('H'),
        TIPO_DISPOSITIVO('I'),
        POSICION_TABLA_POS('J'),
        CANTIDAD_COMPRAS_UBICACION('K'),
        REDIMIERON_PROMOCION('L'),
        VIERON_PROMOCION('M'),
        COMPRARON_PRODUCTO('N'),
        VIERON_MISION('O'),
        COMPLETARON_MISION('P'),
        GANARON_MISION('Q'),
        TIENE_INSIGNIA('R'),
        PERTENECEN_GRUPO_MIE('S'),
        PERTENECEN_SEGMENTO('T'),
        CAMBIARON_NIVEL('U');
        
        private final char value;
        private static final Map<Character, TiposRegla> lookup = new HashMap<>();
        
        private TiposRegla(char value) {
            this.value = value;
        }
        
        static {
            for (TiposRegla tipo : TiposRegla.values()) {
                lookup.put(tipo.value, tipo);
            }
        }
        
        public char getValue() {
            return value;
        }
        
        public static TiposRegla get (char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "reglaavanzada_uuid")
    @GenericGenerator(name = "reglaavanzada_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_REGLA")
    private String idRegla;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_REGLA")
    private Character indTipoRegla;
    
    @Size(max = 40)
    @Column(name = "REFERENCIA")
    private String referencia;
    
    @Column(name = "IND_OPERADOR")
    private Character indOperador;
    
    @Size(max = 200)
    @Column(name = "VALOR_COMPARACION0")
    private String valorComparacion0;
    
    @Size(max = 200)
    @Column(name = "VALOR_COMPARACION1")
    private String valorComparacion1;
    
    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    
    @Column(name = "FECHA_FINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFinal;
    
    @Column(name = "FECHA_IND_ULTIMO")
    private Character fechaIndUltimo;
    
    @Column(name = "FECHA_CANTIDAD")
    private BigInteger fechaCantidad;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @JoinColumn(name = "ID_LISTA", referencedColumnName = "ID_LISTA")
    @ManyToOne(optional = false)
    private ListaReglas listaReglas;

    public ReglaAvanzada() {
    }

    public ReglaAvanzada(String idRegla) {
        this.idRegla = idRegla;
    }

    public ReglaAvanzada(String idRegla, Character indTipoRegla, String usuarioCreacion, Date fechaCreacion) {
        this.idRegla = idRegla;
        this.indTipoRegla = indTipoRegla;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(String idRegla) {
        this.idRegla = idRegla;
    }

    public Character getIndTipoRegla() {
        return indTipoRegla;
    }

    public void setIndTipoRegla(Character indTipoRegla) {
        this.indTipoRegla = indTipoRegla==null?null:Character.toUpperCase(indTipoRegla);
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public Character getIndOperador() {
        return indOperador;
    }

    public void setIndOperador(Character indOperador) {
        this.indOperador = indOperador==null?null:Character.toUpperCase(indOperador);
    }

    public String getValorComparacion0() {
        return valorComparacion0;
    }

    public void setValorComparacion0(String valorComparacion0) {
        this.valorComparacion0 = valorComparacion0;
    }

    public String getValorComparacion1() {
        return valorComparacion1;
    }

    public void setValorComparacion1(String valorComparacion1) {
        this.valorComparacion1 = valorComparacion1;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Character getFechaIndUltimo() {
        return fechaIndUltimo;
    }

    public void setFechaIndUltimo(Character fechaIndUltimo) {
        this.fechaIndUltimo = fechaIndUltimo==null?null:Character.toUpperCase(fechaIndUltimo);
    }

    public BigInteger getFechaCantidad() {
        return fechaCantidad;
    }

    public void setFechaCantidad(BigInteger fechaCantidad) {
        this.fechaCantidad = fechaCantidad;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @XmlTransient
    
    public ListaReglas getListaReglas() {
        return listaReglas;
    }

    public void setListaReglas(ListaReglas listaReglas) {
        this.listaReglas = listaReglas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegla != null ? idRegla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReglaAvanzada)) {
            return false;
        }
        ReglaAvanzada other = (ReglaAvanzada) object;
        if ((this.idRegla == null && other.idRegla != null) || (this.idRegla != null && !this.idRegla.equals(other.idRegla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ReglaAvanzada[ idRegla=" + idRegla + " ]";
    }

}
