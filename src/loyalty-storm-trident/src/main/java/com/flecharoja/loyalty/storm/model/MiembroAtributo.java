/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "MIEMBRO_ATRIBUTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MiembroAtributo.findAll", query = "SELECT m FROM MiembroAtributo m"),
    @NamedQuery(name = "MiembroAtributo.findMiembrosByIdAtributo", query = "SELECT m.miembro FROM MiembroAtributo m WHERE m.miembroAtributoPK.idAtributo = :idAtributo"),
    @NamedQuery(name = "MiembroAtributo.findAtributosByIdMiembro", query= "SELECT m.atributoDinamico FROM MiembroAtributo m WHERE m.miembroAtributoPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroAtributo.findByIdMiembro", query = "SELECT m FROM MiembroAtributo m WHERE m.miembroAtributoPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroAtributo.findByIdAtributo", query = "SELECT m FROM MiembroAtributo m WHERE m.miembroAtributoPK.idAtributo = :idAtributo"),
    @NamedQuery(name = "MiembroAtributo.findByFechaCreacion", query = "SELECT m FROM MiembroAtributo m WHERE m.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "MiembroAtributo.findByValor", query = "SELECT m FROM MiembroAtributo m WHERE m.valor = :valor"),
    @NamedQuery(name = "MiembroAtributo.findByIdAtributoIdMiembro", query = "SELECT m FROM MiembroAtributo m WHERE m.miembroAtributoPK.idAtributo = :idAtributo AND m.miembroAtributoPK.idMiembro = :idMiembro")
})
public class MiembroAtributo implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MiembroAtributoPK miembroAtributoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Size(max = 100)
    @Column(name = "VALOR")
    private String valor;
    
    @JoinColumn(name = "ID_ATRIBUTO", referencedColumnName = "ID_ATRIBUTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private AtributoDinamico atributoDinamico;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;

    public MiembroAtributo() {
    }

    public MiembroAtributo(MiembroAtributoPK miembroAtributoPK) {
        this.miembroAtributoPK = miembroAtributoPK;
    }

    public MiembroAtributo(MiembroAtributoPK miembroAtributoPK, Date fechaCreacion) {
        this.miembroAtributoPK = miembroAtributoPK;
        this.fechaCreacion = fechaCreacion;
      
    }

    public MiembroAtributo(String idMiembro, String idAtributo) {
        this.miembroAtributoPK = new MiembroAtributoPK(idMiembro, idAtributo);
    }

    
    public MiembroAtributoPK getMiembroAtributoPK() {
        return miembroAtributoPK;
    }

    public void setMiembroAtributoPK(MiembroAtributoPK miembroAtributoPK) {
        this.miembroAtributoPK = miembroAtributoPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }


    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public AtributoDinamico getAtributoDinamico() {
        return atributoDinamico;
    }

    public void setAtributoDinamico(AtributoDinamico atributoDinamico) {
        this.atributoDinamico = atributoDinamico;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (miembroAtributoPK != null ? miembroAtributoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiembroAtributo)) {
            return false;
        }
        MiembroAtributo other = (MiembroAtributo) object;
        if ((this.miembroAtributoPK == null && other.miembroAtributoPK != null) || (this.miembroAtributoPK != null && !this.miembroAtributoPK.equals(other.miembroAtributoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MiembroAtributo[ miembroAtributoPK=" + miembroAtributoPK + " ]";
    }
    
}
