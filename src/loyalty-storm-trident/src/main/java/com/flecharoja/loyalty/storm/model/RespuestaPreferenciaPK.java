/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author wtencio
 */
@Embeddable
public class RespuestaPreferenciaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "ID_MIEMBRO")
    private String idMiembro;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "ID_PREFERENCIA")
    private String idPreferencia;

    public RespuestaPreferenciaPK() {
    }

    public RespuestaPreferenciaPK(String idMiembro, String idPreferencia) {
        this.idMiembro = idMiembro;
        this.idPreferencia = idPreferencia;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getIdPreferencia() {
        return idPreferencia;
    }

    public void setIdPreferencia(String idPreferencia) {
        this.idPreferencia = idPreferencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiembro != null ? idMiembro.hashCode() : 0);
        hash += (idPreferencia != null ? idPreferencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaPreferenciaPK)) {
            return false;
        }
        RespuestaPreferenciaPK other = (RespuestaPreferenciaPK) object;
        if ((this.idMiembro == null && other.idMiembro != null) || (this.idMiembro != null && !this.idMiembro.equals(other.idMiembro))) {
            return false;
        }
        if ((this.idPreferencia == null && other.idPreferencia != null) || (this.idPreferencia != null && !this.idPreferencia.equals(other.idPreferencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.RespuestaPreferenciaPK[ idMiembro=" + idMiembro + ", idPreferencia=" + idPreferencia + " ]";
    }
    
}
