/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "MIEMBRO_PREMIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MiembroPremio.findAll", query = "SELECT m FROM MiembroPremio m"),
    @NamedQuery(name = "MiembroPremio.findByIdPremio", query = "SELECT m FROM MiembroPremio m WHERE m.miembroPremioPK.idPremio = :idPremio"),
    @NamedQuery(name = "MiembroPremio.findByIdMiembro", query = "SELECT m FROM MiembroPremio m WHERE m.miembroPremioPK.idMiembro = :idMiembro"),
    @NamedQuery(name = "MiembroPremio.findByIndEstado", query = "SELECT m FROM MiembroPremio m WHERE m.indEstado = :indEstado"),
    @NamedQuery(name = "MiembroPremio.findBySkuCodigo", query = "SELECT m FROM MiembroPremio m WHERE m.skuCodigo = :skuCodigo"),
    @NamedQuery(name = "MiembroPremio.findByFechaRedimido", query = "SELECT m FROM MiembroPremio m WHERE m.fechaRedimido = :fechaRedimido"),
    @NamedQuery(name = "MiembroPremio.findByCantProductos", query = "SELECT m FROM MiembroPremio m WHERE m.cantProductos = :cantProductos")})
public class MiembroPremio implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MiembroPremioPK miembroPremioPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "IND_ESTADO")
    private String indEstado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "SKU_CODIGO")
    private String skuCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_REDIMIDO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRedimido;
    @Column(name = "CANT_PRODUCTOS")
    private BigInteger cantProductos;
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;
    @JoinColumn(name = "ID_PREMIO", referencedColumnName = "ID_PREMIO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Premio premio;

    public MiembroPremio() {
    }

    public MiembroPremio(MiembroPremioPK miembroPremioPK) {
        this.miembroPremioPK = miembroPremioPK;
    }

    public MiembroPremio(MiembroPremioPK miembroPremioPK, String indEstado, String skuCodigo, Date fechaRedimido) {
        this.miembroPremioPK = miembroPremioPK;
        this.indEstado = indEstado;
        this.skuCodigo = skuCodigo;
        this.fechaRedimido = fechaRedimido;
    }

    public MiembroPremio(String idPremio, String idMiembro) {
        this.miembroPremioPK = new MiembroPremioPK(idPremio, idMiembro);
    }

    public MiembroPremioPK getMiembroPremioPK() {
        return miembroPremioPK;
    }

    public void setMiembroPremioPK(MiembroPremioPK miembroPremioPK) {
        this.miembroPremioPK = miembroPremioPK;
    }

    public String getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(String indEstado) {
        this.indEstado = indEstado;
    }

    public String getSkuCodigo() {
        return skuCodigo;
    }

    public void setSkuCodigo(String skuCodigo) {
        this.skuCodigo = skuCodigo;
    }

    public Date getFechaRedimido() {
        return fechaRedimido;
    }

    public void setFechaRedimido(Date fechaRedimido) {
        this.fechaRedimido = fechaRedimido;
    }

    public BigInteger getCantProductos() {
        return cantProductos;
    }

    public void setCantProductos(BigInteger cantProductos) {
        this.cantProductos = cantProductos;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public Premio getPremio() {
        return premio;
    }

    public void setPremio(Premio premio) {
        this.premio = premio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (miembroPremioPK != null ? miembroPremioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiembroPremio)) {
            return false;
        }
        MiembroPremio other = (MiembroPremio) object;
        if ((this.miembroPremioPK == null && other.miembroPremioPK != null) || (this.miembroPremioPK != null && !this.miembroPremioPK.equals(other.miembroPremioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MiembroPremio[ miembroPremioPK=" + miembroPremioPK + " ]";
    }
    
}
