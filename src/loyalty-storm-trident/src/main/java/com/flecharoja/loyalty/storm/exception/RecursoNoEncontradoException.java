package com.flecharoja.loyalty.storm.exception;

import com.flecharoja.loyalty.storm.util.CodigosErrores;
import javax.ejb.ApplicationException;

/**
 *
 * @author svargas
 */
@ApplicationException(rollback = true)
public class RecursoNoEncontradoException extends Exception {
    
    public RecursoNoEncontradoException() {
        super(CodigosErrores.ESTADO_ERROR_NO_RECURSO);
    }
}
