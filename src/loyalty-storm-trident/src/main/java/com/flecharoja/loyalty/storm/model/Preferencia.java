/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREFERENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Preferencia.findAll", query = "SELECT p FROM Preferencia p"),
    @NamedQuery(name = "Preferencia.countAll", query = "SELECT COUNT(p.idPreferencia) FROM Preferencia p"),
    @NamedQuery(name = "Preferencia.findByIdPreferencia", query = "SELECT p FROM Preferencia p WHERE p.idPreferencia = :idPreferencia"),
    @NamedQuery(name = "Preferencia.findByPregunta", query = "SELECT p FROM Preferencia p WHERE p.pregunta = :pregunta"),
    @NamedQuery(name = "Preferencia.findByRespuestas", query = "SELECT p FROM Preferencia p WHERE p.respuestas = :respuestas"),
    @NamedQuery(name = "Preferencia.findByIndTipoRespuesta", query = "SELECT p FROM Preferencia p WHERE p.indTipoRespuesta = :indTipoRespuesta"),
    @NamedQuery(name = "Preferencia.findByFechaCreacion", query = "SELECT p FROM Preferencia p WHERE p.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "Preferencia.findByUsuarioCreacion", query = "SELECT p FROM Preferencia p WHERE p.usuarioCreacion = :usuarioCreacion"),
    @NamedQuery(name = "Preferencia.findByFechaModificacion", query = "SELECT p FROM Preferencia p WHERE p.fechaModificacion = :fechaModificacion"),
    @NamedQuery(name = "Preferencia.findByUsuarioModificacion", query = "SELECT p FROM Preferencia p WHERE p.usuarioModificacion = :usuarioModificacion"),
    @NamedQuery(name = "Preferencia.findByNumVersion", query = "SELECT p FROM Preferencia p WHERE p.numVersion = :numVersion")})
public class Preferencia implements Serializable {

    

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "preferencia_uuid")
    @GenericGenerator(name = "preferencia_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_PREFERENCIA")
    private String idPreferencia;
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "PREGUNTA")
    private String pregunta;
    
 
    @Size(max = 300)
    @Column(name = "RESPUESTAS")
    private String respuestas;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "IND_TIPO_RESPUESTA")
    private String indTipoRespuesta;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @JoinColumn(name = "ID_ATRIBUTO", referencedColumnName = "ID_ATRIBUTO")
    @ManyToOne
    private AtributoDinamico idAtributo;

    public Preferencia() {
    }

    public Preferencia(String idPreferencia) {
        this.idPreferencia = idPreferencia;
    }

    public Preferencia(String idPreferencia, String pregunta, String indTipoRespuesta, Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.idPreferencia = idPreferencia;
        this.pregunta = pregunta;
        this.indTipoRespuesta = indTipoRespuesta;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public String getIdPreferencia() {
        return idPreferencia;
    }

    public void setIdPreferencia(String idPreferencia) {
        this.idPreferencia = idPreferencia;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(String respuestas) {
        this.respuestas = respuestas;
    }

    public String getIndTipoRespuesta() {
        return indTipoRespuesta;
    }

    public void setIndTipoRespuesta(String indTipoRespuesta) {
        this.indTipoRespuesta = indTipoRespuesta;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPreferencia != null ? idPreferencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Preferencia)) {
            return false;
        }
        Preferencia other = (Preferencia) object;
        if ((this.idPreferencia == null && other.idPreferencia != null) || (this.idPreferencia != null && !this.idPreferencia.equals(other.idPreferencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Preferencia[ idPreferencia=" + idPreferencia + " ]";
    }

   

    public AtributoDinamico getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(AtributoDinamico idAtributo) {
        this.idAtributo = idAtributo;
    }
    
}
