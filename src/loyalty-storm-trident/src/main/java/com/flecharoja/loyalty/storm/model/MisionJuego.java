/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO_MISION_JUEGO")
@XmlRootElement
@NamedQueries({
      @NamedQuery(name = "MisionJuego.findAll", query = "SELECT m FROM MisionJuego m")
    , @NamedQuery(name = "MisionJuego.findByIdMisionJuego", query = "SELECT m FROM MisionJuego m WHERE m.idMisionJuego = :idMisionJuego")
    , @NamedQuery(name = "MisionJuego.countByIdJuego", query = "SELECT COUNT(m) FROM MisionJuego m WHERE m.idJuego.idMision = :idMision")
    , @NamedQuery(name = "MisionJuego.findByIdJuego", query = "SELECT m FROM MisionJuego m WHERE m.idJuego.idMision = :idMision"),
      @NamedQuery(name = "MisionJuego.countAll", query = "SELECT COUNT(m) FROM MisionJuego m"),
      @NamedQuery(name = "MisionJuego.findGracias", query = "SELECT COUNT (m) FROM MisionJuego m WHERE m.idJuego.idMision = :idMision AND m.indTipoPremio = :tipoPremio")
  })
public class MisionJuego implements Serializable {

    @Basic(optional = false)
    @Column(name = "PROBABILIDAD")
    private Double probabilidad;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_RESPUESTA")
    private Boolean indRespuesta;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long  numVersion;
    @JoinColumn(name = "METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne
    private Metrica metrica;
    
    public enum Tipo{
        PREMIO('P'),
        METRICA('M'),
        GRACIAS('G');
        
        private final char value;
        private static final Map<Character,Tipo> lookup= new HashMap<>();

        private Tipo(char value) {
            this.value = value;
        }
        
        static{
            for(Tipo tipo : values()){
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        public static Tipo get(Character  value){
            return value==null?null:lookup.get(value);
        }
    }
    
     public enum IntervalosTiempoRespuesta {
        POR_SIEMPRE('A'),
        POR_MINUTO('B'),
        POR_HORA('C'),
        POR_DIA('D'),
        POR_SEMANA('E'),
        POR_MES('F');

        private final char value;
        private static final Map<Character, IntervalosTiempoRespuesta> lookup = new HashMap<>();

        private IntervalosTiempoRespuesta(char value) {
            this.value = value;
        }

        static {
            for (IntervalosTiempoRespuesta tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }

        public static IntervalosTiempoRespuesta get(Character value) {
            return value == null ? null : lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "mision_juego_uuid")
    @GenericGenerator(name = "mision_juego_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_MISION_JUEGO")
    private String idMisionJuego;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_PREMIO")
    private Character indTipoPremio;
    
    
    @Column(name = "CANT_INTER_TOTAL")
    private Long cantInterTotal;
    
    @Column(name = "IND_INTER_TOTAL")
    private Character indInterTotal;
    
    @Column(name = "CANT_INTER_MIEMBRO")
    private Long cantInterMiembro;
    
    @Column(name = "IND_INTER_MIEMBRO")
    private Character indInterMiembro;
    
    @Column(name = "CANT_METRICA")
    private Double cantMetrica;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    
    @JoinColumn(name = "ID_JUEGO", referencedColumnName = "ID_MISION")
    @ManyToOne(optional = false)
    private Juego idJuego;
    
    @JoinColumn(name = "ID_PREMIO", referencedColumnName = "ID_PREMIO")
    @ManyToOne
    private Premio idPremio;

    public MisionJuego() {
    }

    public MisionJuego(String idMisionJuego) {
        this.idMisionJuego = idMisionJuego;
    }

    public MisionJuego(String idMisionJuego, Character indTipoPremio, Double probabilidad, Boolean indRespuesta, String usuarioCreacion, Date fechaCreacion, String usuarioModificacion, Date fechaModificacion, Long numVersion) {
        this.idMisionJuego = idMisionJuego;
        this.indTipoPremio = indTipoPremio;
        this.probabilidad = probabilidad;
        this.indRespuesta = indRespuesta;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaCreacion = fechaCreacion;
        this.usuarioModificacion = usuarioModificacion;
        this.fechaModificacion = fechaModificacion;
        this.numVersion = numVersion;
    }

    public String getIdMisionJuego() {
        return idMisionJuego;
    }

    public void setIdMisionJuego(String idMisionJuego) {
        this.idMisionJuego = idMisionJuego;
    }

    public Character getIndTipoPremio() {
        return indTipoPremio;
    }

    public void setIndTipoPremio(Character indTipoPremio) {
        this.indTipoPremio = indTipoPremio;
    }


    public Boolean getIndRespuesta() {
        return indRespuesta;
    }

    public void setIndRespuesta(Boolean indRespuesta) {
        this.indRespuesta = indRespuesta;
    }

    public Long getCantInterTotal() {
        return cantInterTotal;
    }

    public void setCantInterTotal(Long cantInterTotal) {
        this.cantInterTotal = cantInterTotal;
    }

    public Character getIndInterTotal() {
        return indInterTotal;
    }

    public void setIndInterTotal(Character indInterTotal) {
        this.indInterTotal = indInterTotal;
    }

    public Long getCantInterMiembro() {
        return cantInterMiembro;
    }

    public void setCantInterMiembro(Long cantInterMiembro) {
        this.cantInterMiembro = cantInterMiembro;
    }

    public Character getIndInterMiembro() {
        return indInterMiembro;
    }

    public void setIndInterMiembro(Character indInterMiembro) {
        this.indInterMiembro = indInterMiembro;
    }

    public Double getCantMetrica() {
        return cantMetrica;
    }

    public void setCantMetrica(Double cantMetrica) {
        this.cantMetrica = cantMetrica;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Juego getIdJuego() {
        return idJuego;
    }

    public void setIdJuego(Juego idJuego) {
        this.idJuego = idJuego;
    }

    public Premio getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(Premio idPremio) {
        this.idPremio = idPremio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMisionJuego != null ? idMisionJuego.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionJuego)) {
            return false;
        }
        MisionJuego other = (MisionJuego) object;
        if ((this.idMisionJuego == null && other.idMisionJuego != null) || (this.idMisionJuego != null && !this.idMisionJuego.equals(other.idMisionJuego))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MisionJuego{" + "idMisionJuego=" + idMisionJuego + ", indTipoPremio=" + indTipoPremio + ", probabilidad=" + probabilidad + ", indRespuesta=" + indRespuesta + ", cantInterTotal=" + cantInterTotal + ", indInterTotal=" + indInterTotal + ", cantInterMiembro=" + cantInterMiembro + ", indInterMiembro=" + indInterMiembro + ", cantMetrica=" + cantMetrica + ", usuarioCreacion=" + usuarioCreacion + ", fechaCreacion=" + fechaCreacion + ", usuarioModificacion=" + usuarioModificacion + ", fechaModificacion=" + fechaModificacion + ", numVersion=" + numVersion + '}';
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }

    public Double getProbabilidad() {
        return probabilidad;
    }

    public void setProbabilidad(Double probabilidad) {
        this.probabilidad = probabilidad;
    }
    
    

   
    
}
