/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author faguilar
 */
public class EventosSistema {

    public enum Eventos {
        MIE_ENTRA_GRUPO(1),
        MIE_SALE_GRUPO(2),
        MIE_ES_ASIGN_INSIGNIA(3),
        MIE_ES_DESASIGN_INSIGNIA(4),
        MIE_ES_ASIGN_LEADERBOARD(5),
        MIE_ES_DESASIGN_LEADERBOARD(6),
        MIE_GANA_METRICA(7),
        MIE_REDIME_METRICA(8),
        MIE_EXPIRA_METRICA(9),
        MIE_ALCANZA_NIVEL_METRICA(10),
        MIE_DEJA_NIVEL_DE_METRICA(11),
        MIE_EJECUTA_MISION(12),
        MIE_APRUEBA_MISION(13),
        MIE_RERUEBA_MISION(14),
        MIE_RECIBE_NOTIFICACION(15),
        MIE_ABRE_NOTIFICACION(16),
        MIE_RECIBE_PREMIO(17),
        MIE_REDIME_PREMIO(18),
        MIE_COMPRA_PRODUCTO(19),
        MIE_AGREGA_PROMOCION(20),
        MIE_ENTRA_SEGMENTO(21),
        MIE_SALE_SEGMENTO(22),
        MIE_CUMPLE_ANOS(23),
        MIE_SE_REGISTRA_EN_EL_PROGRAMA(24),
        MIE_ELIMINA_SU_APP(25),
        MIE_INSTALA_SU_APP(26),
        MIE_VIO_PROMO(27),
        MIE_ACTUALIZA_PERFIL(28),
        MIE_VIO_MISION(29),
        EVENTO_CUSTOM(30),
        MIE_COMPRA_PRODUCTO_ESPECIFICO(31),
        MIE_CUMPLE_ANNIVERSARIO(32),
        MIE_REFIERE_MIE(33);

        private final int value;
        private static final Map<Integer, Eventos> lookup = new HashMap<>();

        private Eventos(int value) {
            this.value = value;
        }

        static {
            for (Eventos tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public static Eventos get(int value) {
            return lookup.get(value);
        }
    }

}
