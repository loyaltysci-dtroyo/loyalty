/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.functions;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.storm.topology.FailedException;
import org.apache.storm.trident.operation.BaseFunction;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.operation.TridentOperationContext;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;

/**
 * Funcion encargada de convertir los elementos JSON provenientes de eventos en
 * tuplas de storm, formateadas segun sus campos
 *
 * @author faguilar
 */
public class EventosSistemaParserFunction extends BaseFunction {

    private static final Logger LOG = Logger.getLogger(EventosSistemaParserFunction.class.getName());

    @Override
    public void cleanup() {
        super.cleanup();
    }   
   
    @Override
    public void prepare(Map conf, TridentOperationContext context) {
        super.prepare(conf, context); 
    }
    /**
     * Metodo encargado de generar tuplas de storm a partir de los mensajes de eventos que 
     * vienen en formato JSON desde el Filter de eventos (EventosSistemaFilter)
     * @param tuple
     * @param collector 
     */
    @Override
    public void execute(TridentTuple tuple, TridentCollector collector) {
        try {
            String jsonInput = new String(tuple.getBinary(0));
            ObjectMapper m = new ObjectMapper();
            JsonNode rootNode = m.readTree(jsonInput);
            JsonNode indEventoNode = rootNode.path("indEvento");
            int indEvento = indEventoNode.asInt();
            JsonNode idMiembroNode = rootNode.path("idMiembro");
            JsonNode idElementoNode = rootNode.path("idElemento");
//            LOG.log(Level.WARNING,"EventosSistemaParser - Tupla parseada: "+indEvento+" "+idMiembroNode.asText()+" "+idElementoNode.asText());
            collector.emit(new Values(indEvento,idMiembroNode.asText(),idElementoNode.asText()));
        } catch (Exception ex) {
           collector.reportError(ex);
           LOG.log(Level.SEVERE,"Error eventos sistema parser",ex);
           //throw new FailedException(ex);
        } 
       
    }

}
