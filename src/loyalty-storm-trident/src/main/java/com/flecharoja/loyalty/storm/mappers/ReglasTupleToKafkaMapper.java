/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.mappers;

import org.apache.storm.kafka.trident.mapper.TridentTupleToKafkaMapper;
import org.apache.storm.trident.tuple.TridentTuple;

/**
 *
 * @author faguilar
 */
public class ReglasTupleToKafkaMapper implements TridentTupleToKafkaMapper {
     @Override
    public Object getKeyFromTuple(TridentTuple tuple) {
        return "nodo";
    }

    @Override
    public Object getMessageFromTuple(TridentTuple tuple) {
        String result= "{\"indTipoRegla\": \"" + tuple.getString(0) + "\", \"idMiembro\": \"" + tuple.getString(2) + "\", \"idRegla\": \"" + tuple.getString(1) + "\"}";
        System.out.println("Escribiendo nodo en kafka:" + tuple.toString()+" "+result);
        return result;
    }
}
