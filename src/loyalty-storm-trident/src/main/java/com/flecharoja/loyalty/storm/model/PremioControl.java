/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "PREMIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PremioControl.findAll", query = "SELECT p FROM PremioControl p")

})
public class PremioControl implements Serializable {
   
    public enum Estados {
        PUBLICADO('P'),
        ARCHIVADO('A'),
        BORRADOR('B');
        
        private final char value;
        private static final Map<Character, Estados> lookup =  new HashMap<>();
        
        private Estados(char value){
            this.value = value;
        }
        
        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public char getValue(){
            return value;
        }
        
        public static Estados get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    
    public enum TiposPremio {
        CERTIFICADO('C'),
        PRODUCTO('P');
        
        private final char value;
        private static final Map<Character,TiposPremio> lookup = new HashMap<>();

        private TiposPremio(char value) {
            this.value = value;
        }
        
        static {
            for (TiposPremio tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static TiposPremio get (Character value){
            return value==null?null:lookup.get(value);
        }
    }
  
     public enum IntervalosTiempoRespuesta {
        POR_SIEMPRE('A'),
        POR_MINUTO('B'),
        POR_HORA('C'),
        POR_DIA('D'),
        POR_SEMANA('E'),
        POR_MES('F');
        
        private final char value;
        private static final Map<Character, IntervalosTiempoRespuesta> lookup = new HashMap<>();

        private IntervalosTiempoRespuesta(char value) {
            this.value = value;
        }
        
        static {
            for (IntervalosTiempoRespuesta tipo : values()) {
                lookup.put(tipo.value, tipo);
            }
        }
        
        public char getValue() {
            return value;
        }
        
        public static IntervalosTiempoRespuesta get (Character value) {
            return value==null?null:lookup.get(value);
        }
    }
     
     public enum Efectividad{
        PERMANENTE('P'),
        CALENDARIZADO('C'),
        RECURRENTE('R');
        
        private final char value;
        private static final Map<Character,Efectividad> lookup = new HashMap<>();

        private Efectividad(char value) {
            this.value = value;
        }
        
        static{
            for(Efectividad efectividad : values()){
                lookup.put(efectividad.value, efectividad);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Efectividad get(Character value){
            return value==null?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_PREMIO")
    private String idPremio;
       
    @Column(name = "IND_ESTADO")
    private Character indEstado;
        
    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    private Metrica idMetrica;
    
    @NotNull
    @Column(name = "IND_RESPUESTA")
    private Boolean indRespuesta;
     
    @Column(name = "CANT_TOTAL")
    private Long cantTotal;
    
    @Column(name = "IND_INTERVALO_TOTAL")
    private Character indIntervaloTotal;
    
    @Column(name = "CANT_TOTAL_MIEMBRO")
    private Long cantTotalMiembro;
    
    @Column(name = "IND_INTERVALO_MIEMBRO")
    private Character indIntervaloMiembro;
    
    @Column(name = "IND_INTERVALO_RESPUESTA")
    private Character indIntervaloRespuesta;
    
    @Column(name = "CANT_INTERVALO_RESPUESTA")
    private Long cantIntervaloRespuesta;
   
    @Basic(optional = false)
    @NotNull
    @Column(name = "EFECTIVIDAD_IND_CALENDARIZADO")
    private Character efectividadIndCalendarizado;
    
    @Column(name = "EFECTIVIDAD_FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date efectividadFechaInicio;
    
    @Column(name = "EFECTIVIDAD_FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date efectividadFechaFin;
    
    @Size(max = 7)
    @Column(name = "EFECTIVIDAD_IND_DIAS")
    private String efectividadIndDias;
    
    @Column(name = "EFECTIVIDAD_IND_SEMANA")
    private Integer efectividadIndSemana;
    
    public PremioControl() {
    }

    public String getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(String idPremio) {
        this.idPremio = idPremio;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPremio != null ? idPremio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PremioControl)) {
            return false;
        }
        PremioControl other = (PremioControl) object;
        if ((this.idPremio == null && other.idPremio != null) || (this.idPremio != null && !this.idPremio.equals(other.idPremio))) {
            return false;
        }
        return true;
    }

    public Boolean getIndRespuesta() {
        return indRespuesta;
    }

    public void setIndRespuesta(Boolean indRespuesta) {
        this.indRespuesta = indRespuesta;
    }

    public Long getCantTotal() {
        return cantTotal;
    }

    public void setCantTotal(Long cantTotal) {
        this.cantTotal = cantTotal;
    }

    public Character getIndIntervaloTotal() {
        return indIntervaloTotal;
    }

    public void setIndIntervaloTotal(Character indIntervaloTotal) {
        this.indIntervaloTotal = indIntervaloTotal;
    }

    public Long getCantTotalMiembro() {
        return cantTotalMiembro;
    }

    public void setCantTotalMiembro(Long cantTotalMiembro) {
        this.cantTotalMiembro = cantTotalMiembro;
    }

    public Character getIndIntervaloMiembro() {
        return indIntervaloMiembro;
    }

    public void setIndIntervaloMiembro(Character indIntervaloMiembro) {
        this.indIntervaloMiembro = indIntervaloMiembro;
    }

    public Character getIndIntervaloRespuesta() {
        return indIntervaloRespuesta;
    }

    public void setIndIntervaloRespuesta(Character indIntervaloRespuesta) {
        this.indIntervaloRespuesta = indIntervaloRespuesta;
    }
   
    public Character getEfectividadIndCalendarizado() {
        return efectividadIndCalendarizado;
    }

    public void setEfectividadIndCalendarizado(Character efectividadIndCalendarizado) {
        this.efectividadIndCalendarizado = efectividadIndCalendarizado;
    }

    public Date getEfectividadFechaInicio() {
        return efectividadFechaInicio;
    }

    public void setEfectividadFechaInicio(Date efectividadFechaInicio) {
        this.efectividadFechaInicio = efectividadFechaInicio;
    }

    public Date getEfectividadFechaFin() {
        return efectividadFechaFin;
    }

    public void setEfectividadFechaFin(Date efectividadFechaFin) {
        this.efectividadFechaFin = efectividadFechaFin;
    }

    public String getEfectividadIndDias() {
        return efectividadIndDias;
    }

    public void setEfectividadIndDias(String efectividadIndDias) {
        this.efectividadIndDias = efectividadIndDias;
    }

    public Integer getEfectividadIndSemana() {
        return efectividadIndSemana;
    }

    public void setEfectividadIndSemana(Integer efectividadIndSemana) {
        this.efectividadIndSemana = efectividadIndSemana;
    }  

    public Long getCantIntervaloRespuesta() {
        return cantIntervaloRespuesta;
    }

    public void setCantIntervaloRespuesta(Long cantIntervaloRespuesta) {
        this.cantIntervaloRespuesta = cantIntervaloRespuesta;
    }
    
    
}
