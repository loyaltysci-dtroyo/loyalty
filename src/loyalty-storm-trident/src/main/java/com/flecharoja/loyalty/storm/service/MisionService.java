package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.exception.RecursoNoEncontradoException;
import com.flecharoja.loyalty.storm.model.Miembro;
import com.flecharoja.loyalty.storm.model.Mision;
import com.flecharoja.loyalty.storm.model.MisionListaMiembro;
import com.flecharoja.loyalty.storm.model.MisionListaMiembroPK;
import com.flecharoja.loyalty.storm.util.CodigosErrores;
import com.flecharoja.loyalty.storm.util.Indicadores;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * EJB encargado del manejo de datos para el mantenimiento de misiones
 *
 * @author faguilar
 */
public class MisionService {

    private static final Logger LOG = Logger.getLogger(MisionService.class.getName());

    /**
     * Metodo que obtiene la lista de misiones en un rango de registros y
     * opcionalmente por estados
     *
     * @param estado
     * @return Respuesta con un listado de promociones y el rango de registros
     * @throws com.flecharoja.loyalty.storm.exception.ErrorSistemaException
     */
    public List<Mision> getMisiones(String estado) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultado = null;
        if (estado!=null && Mision.Estados.get(estado.charAt(0))!=null) {
            resultado = em.createNamedQuery("Mision.findAll").getResultList();
        } else {
            resultado = em.createNamedQuery("Mision.findByIndEstado").getResultList();
        }
       return resultado;
    }

    /**
     * Metodo que realiza una busqueda de misiones por palabras claves dentro de
     * " busqueda" y retorna un listado de promociones que concuerdan en un
     * rango y opcionalmente por estado(s)
     *
     * @param busqueda Variable de texto con las palabras claves
     * @param estados Estados de misiones deseados
     * @return Respuesta con el listado de misiones y el rango de los registros
     */
    public List<Mision> searchMisiones(String busqueda, List<String> estados) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultado = null;
        //division del texto de busqueda en un arreglos de palabras claves (divididas por espacio)
        String[] palabrasClaves = busqueda.split(" ");

        //se define el query
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> query = cb.createQuery();
        Root<Mision> root = query.from(Mision.class);

        //se forman los predicados con los estados deseados en la peticion
        List<Predicate> predicates = new ArrayList<>();

        for (String palabraClave : palabrasClaves) {
            predicates.add(cb.like(cb.upper(root.get("nombre")), "%" + palabraClave.toUpperCase() + "%"));
            predicates.add(cb.like(cb.upper(root.get("descripcion")), "%" + palabraClave.toUpperCase() + "%"));
            predicates.add(cb.like(cb.upper(root.get("tags")), "%" + palabraClave.toUpperCase() + "%"));
        }

        if (estados.isEmpty()) {
            query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
        } else {
            List<Predicate> predicates2 = new ArrayList<>();
            for (String e : estados) {
                predicates2.add(cb.equal(root.get("indEstado"), e.toUpperCase().charAt(0)));
            }
            //se forma la clausula WHERE con los predicados formados
            query.where(cb.or(predicates2.toArray(new Predicate[predicates2.size()])), cb.or(predicates.toArray(new Predicate[predicates.size()])));
        }

        //se obtiene los registros en un rango valido
        resultado = em.createQuery(query.select(root)).getResultList();
        em.close();
        return resultado;
    }

    /**
     * Metodo que obtiene la informacion de una mision por el identificador de
     * la misma
     *
     * @param idMision Identificador de la mision
     * @return Respuesta con la informacion de la mision encontrada
     */
    public Mision getMisionPorId(String idMision) throws RecursoNoEncontradoException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Mision mision = em.find(Mision.class, idMision);
        //verificacion de que la identidad exista
        if (mision == null) {
            throw new RecursoNoEncontradoException();
        }
        em.close();
        return mision;
    }

    /**
     * Metodo que obtiene un listado de miembros asignados a una mision en un
     * rango de respuesta y opcionalmente por el tipo de accion
     * (incluido/excluido)
     *
     * @param idMision Identificador de la mision
     * @param indAccion Indicador de tipo de accion deseado
     * @return Respuesta con el listado de miembros y su rango de respuesta
     */
    public List<Miembro> getMiembros(String idMision, Character indAccion) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List<Miembro> resultado = null;
        Long total;

        if (indAccion == null) {//en caso de que no se haya establecido el tipo de accion deseado, se recuperan todos
            total = ((Long) em.createNamedQuery("MisionListaMiembro.countMiembrosByIdMision").setParameter("idMision", idMision).getSingleResult());
            total -= total - 1 < 0 ? 0 : 1;

            resultado = em.createNamedQuery("MisionListaMiembro.findMiembrosByIdMision")
                    .setParameter("idMision", idMision).getResultList();

        } else {
            indAccion = Character.toUpperCase(indAccion);

            if (indAccion.compareTo(Indicadores.DISPONIBLES) == 0) {
                total = (Long) em.createNamedQuery("MisionListaMiembro.countMiembrosNotInListaByIdMision")
                        .setParameter("idMision", idMision)
                        .getSingleResult();
                total -= total - 1 < 0 ? 0 : 1;

                //obtencion de la lista en un rango valido
                resultado = em.createNamedQuery("MisionListaMiembro.findMiembrosNotInListaByIdMision")
                        .setParameter("idMision", idMision)
                        .getResultList();
            } else {
                total = ((Long) em.createNamedQuery("MisionListaMiembro.countMiembrosByIdMisionIndTipo")
                        .setParameter("idMision", idMision)
                        .setParameter("indTipo", indAccion)
                        .getSingleResult());
                total -= total - 1 < 0 ? 0 : 1;

                resultado = em.createNamedQuery("MisionListaMiembro.findMiembrosByIdMisionIndTipo")
                        .setParameter("idMision", idMision)
                        .setParameter("indTipo", indAccion)
                        .getResultList();

            }
        }
        em.close();
        return resultado;
    }

    /**
     * Metodo que asigna (incluye o excluye) a un miembro de una mision
     *
     * @param idMision Identificador de la mision
     * @param idMiembro Identificador del miembro
     * @param indAccion Tipo de accion (incluye/excluye)
     * @param usuario usuario en sesion
     */
    public void includeExcludeMiembro(String idMision, String idMiembro, Character indAccion, String usuario) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        Mision mision = em.find(Mision.class, idMision);
        if (mision == null) {
            LOG.log(Level.SEVERE, "La Mision es nula", new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA));
        }
        Date date = Calendar.getInstance().getTime();
        MisionListaMiembro promocionListaMiembro = new MisionListaMiembro(idMiembro, idMision, indAccion, date, usuario);
        em.merge(promocionListaMiembro);
        em.flush();
        em.clear();
        em.getTransaction().commit();
        em.close();
    }

    /**
     * Metodo que revoca la asignacion de un miembro a una mision
     *
     * @param idMision Identificador de la mision
     * @param idMiembro Identificador del miembro
     * @param usuario usuario en sesion
     */
    public void removeMiembro(String idMision, String idMiembro, String usuario) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        em.remove(em.getReference(MisionListaMiembro.class, new MisionListaMiembroPK(idMiembro, idMision)));
        em.flush();
        em.getTransaction().commit();
        em.close();
    }

    public void agregarMiembroElegibles(String idMision, String idMiembro) throws ErrorSistemaException {
        this.includeExcludeMiembro(idMision, idMiembro, Indicadores.INCLUIDO, idMiembro);
    }

}
