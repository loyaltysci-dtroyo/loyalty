
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "TRANSACCION_COMPRA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TransaccionCompra.findById", query = "SELECT t FROM TransaccionCompra t WHERE t.idTransaccion = :idTransaccion"),
    @NamedQuery(name = "TransaccionCompra.findByIdMiembro", query = "SELECT t FROM TransaccionCompra t WHERE t.miembro.idMiembro = :idMiembro"),
    @NamedQuery(name = "TransaccionCompra.countByIdMiembro", query = "SELECT COUNT(t) FROM TransaccionCompra t WHERE t.miembro.idMiembro = :idMiembro")
})
public class TransaccionCompra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "transaccion_uuid")
    @GenericGenerator(name = "transaccion_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_TRANSACCION")
    private String idTransaccion;
    
    @Size(max = 150)
    @Column(name = "ID_COMPRA")
    private String idCompra;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUBTOTAL")
    private Double subtotal;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IMPUESTO")
    private Double impuesto;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL")
    private Double total;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO")
    @ManyToOne(optional = false)

    private Miembro miembro;
    
    @JoinColumn(name = "ID_UBICACION", referencedColumnName = "ID_UBICACION")
    @ManyToOne
    private Ubicacion ubicacion;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "transaccionCompra")
    private CompraPremio compraPremio;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTransaccion", fetch = FetchType.EAGER)
    private List<CompraProducto> compraProductos;

    public TransaccionCompra() {
    }

    public TransaccionCompra(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public TransaccionCompra(String idTransaccion, Date fecha, Double subtotal, Double impuesto, Double total) {
        this.idTransaccion = idTransaccion;
        this.fecha = fecha;
        this.subtotal = subtotal;
        this.impuesto = impuesto;
        this.total = total;
    }

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public String getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(String idCompra) {
        this.idCompra = idCompra;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(Double impuesto) {
        this.impuesto = impuesto;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }


    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public CompraPremio getCompraPremio() {
        return compraPremio;
    }

    public void setCompraPremio(CompraPremio compraPremio) {
        this.compraPremio = compraPremio;
    }
    
    public List<CompraProducto> getCompraProductos() {
        return compraProductos;
    }

    public void setCompraProductos(List<CompraProducto> compraProductos) {
        this.compraProductos = compraProductos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTransaccion != null ? idTransaccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransaccionCompra)) {
            return false;
        }
        TransaccionCompra other = (TransaccionCompra) object;
        if ((this.idTransaccion == null && other.idTransaccion != null) || (this.idTransaccion != null && !this.idTransaccion.equals(other.idTransaccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.TransaccionCompra[ idTransaccion=" + idTransaccion + " ]";
    }
    
}
