package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.model.Workflow;
import com.flecharoja.loyalty.storm.model.WorkflowNodo;
import com.flecharoja.loyalty.storm.model.WorkflowNodoEspera;
import com.flecharoja.loyalty.storm.model.WorkflowNodoEsperaPK;
import com.flecharoja.loyalty.storm.util.CodigosErrores;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;

/**
 *
 * @author faguilar
 */
public class NodoService {

    private static final Logger LOG = Logger.getLogger(NodoService.class.getName());

    /**
     * Metodo que obtiene una lista de nodos del workflow para un id de workflow
     * y los retorna en una lista ordenada
     *
     * @param idWorkflow Valores de los indicadores de estado a buscar
     * @return Respuesta con una lista de workflows
     */
    public ArrayList<WorkflowNodo> getNodosPorIdWorkflow(String idWorkflow) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        if (idWorkflow == null || idWorkflow.equals("")) {
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        ArrayList<WorkflowNodo> resultado = new ArrayList<>();
        Workflow padre = em.find(Workflow.class, idWorkflow);
        WorkflowNodo nodoActual = padre.getIdSucesor();

        while (nodoActual != null) {
            resultado.add(nodoActual);
            nodoActual = nodoActual.getIdSucesor();
        }
        em.close();
        return resultado;
    }

    /**
     * Metodo que obtiene una lista de nodos del workflow para un id de workflow
     * y los retorna en una lista ordenada
     *
     * @return Respuesta con una lista de workflows
     */
    public List<WorkflowNodo> getAllNodos() {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List<WorkflowNodo> resultado = null;
        resultado = em.createNamedQuery("WorkflowNodo.findAll").getResultList();
        em.close();
        return resultado;
    }

    /**
     * Metodo que obtiene un nodo de wworkflow por su numero de identificacion
     *
     * @param idWorkflow Identificacion del workflow
     * @return Respuesta con la informacion del workflow
     */
    public WorkflowNodo getNodoPorId(String idWorkflow) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        if (idWorkflow == null || idWorkflow.equals("")) {
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        WorkflowNodo workflow = em.find(WorkflowNodo.class, idWorkflow);
        //verificacion que el workflow exista
        if (workflow == null) {
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        em.close();
        return workflow;
    }

    public WorkflowNodo getNodo(String idNodo) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        WorkflowNodo singleResult = em.createNamedQuery("WorkflowNodo.findByIdNodo", WorkflowNodo.class).setParameter("idNodo", idNodo).getSingleResult();
        em.close();
        return singleResult;
    }

    public void eliminarNodoEspera(WorkflowNodo nodoEliminar) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        WorkflowNodoEspera nodo = em.find(WorkflowNodoEspera.class, new WorkflowNodoEsperaPK(nodoEliminar.getIdNodo(), nodoEliminar.getIdNodo()));
        if (nodo != null) {
            em.remove(nodo);
//            LOG.log(Level.SEVERE, "El nodo es nulo", new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA));
        }
        em.flush();
        em.clear();
        em.getTransaction().commit();
        em.close();
    }
}
