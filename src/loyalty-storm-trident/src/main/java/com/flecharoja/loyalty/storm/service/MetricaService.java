package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.model.Metrica;
import com.flecharoja.loyalty.storm.model.Notificacion;
import com.flecharoja.loyalty.storm.util.CodigosErrores;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author faguilar
 */
public class MetricaService {

    private static final Logger LOG = Logger.getLogger(MetricaService.class.getName());

    private static final String RESOURCE_ID = "metrica/registrar-metrica";
    private static final String MESSAGE_API_ENDPOINT = "http://wfinterno:8080/loyalty-api-internal/webresources/v0/";

    public MetricaService() {

    }

    public void otorgarMetica(String idMiembro, String idMetrica, double cantidad) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Metrica metrica = em.find(Metrica.class, idMetrica);// notificacion almacenada
        if (metrica == null) {//si no se pudo encontrar la notificacion...
            LOG.log(Level.SEVERE, "La Metrica es nula", new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA));
        }
        //si la notificacion tiene un estado de archivado o ejecutado...
        if (metrica.getIndEstado().equals(Metrica.Estados.ARCHIVADO.getValue()) || metrica.getIndEstado().equals(Notificacion.Estados.BORRADOR.getValue())) {
            LOG.log(Level.SEVERE, "La Metrica es archivada o borrador", new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA));
        }

        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(MESSAGE_API_ENDPOINT).path(RESOURCE_ID);

        String json = "{"
                + " \"idMiembro\":\"" + idMiembro + "\","
                + " \"idMetrica\":\"" + idMetrica + "\","
                + " \"cantidad\":" + cantidad + ","
                + "  \"indTipoGane\":\"E\"}";
        LOG.log(Level.INFO, "Enviando solicitud al API de metricas con el siguiente valor:{0}", json);
        Response post = target.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));
        em.close();
        if (post.getStatus() != 200) {
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
    }
}
