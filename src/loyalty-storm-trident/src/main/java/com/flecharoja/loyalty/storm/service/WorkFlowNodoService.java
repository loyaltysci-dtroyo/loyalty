package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.exception.RecursoNoEncontradoException;
import com.flecharoja.loyalty.storm.model.Workflow;
import com.flecharoja.loyalty.storm.model.WorkflowCompletado;
import com.flecharoja.loyalty.storm.model.WorkflowCompletadoPK;
import com.flecharoja.loyalty.storm.model.WorkflowNodo;
import com.flecharoja.loyalty.storm.model.WorkflowNodoEspera;
import com.flecharoja.loyalty.storm.util.CodigosErrores;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Bean encargado de realizar operaciones de mantenimiento de datos de los
 * workflow
 *
 * @author faguilar
 */
@Stateless
public class WorkFlowNodoService {

    /**
     * Metodo que obtiene una lista de nodos del workflow para un id de workflow
     * y los retorna en una lista ordenada
     *
     * @param idWorkflow Valores de los indicadores de estado a buscar
     * @return Respuesta con una lista de workflows
     */
    public ArrayList<WorkflowNodo> getNodos(String idWorkflow) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        if (idWorkflow == null || idWorkflow.equals("")) {
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        ArrayList<WorkflowNodo> resultado = new ArrayList<>();
        Workflow padre = em.find(Workflow.class, idWorkflow);
        WorkflowNodo nodoActual = padre.getIdSucesor();
        while (nodoActual != null && nodoActual.getIdSucesor() != null) {
            resultado.add(nodoActual);
            nodoActual = nodoActual.getIdSucesor();
        }
        em.close();
        return resultado;
    }

    /**
     * Metodo que obtiene un nodo de wworkflow por su numero de identificacion
     *
     * @param idWorkflow Identificacion del workflow
     * @return Respuesta con la informacion del workflow
     */
    public WorkflowNodo getNodoPorId(String idWorkflow) throws ErrorSistemaException, RecursoNoEncontradoException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        if (idWorkflow == null || idWorkflow.equals("")) {
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        WorkflowNodo workflow = em.find(WorkflowNodo.class, idWorkflow);
        //verificacion que el workflow exista
        if (workflow == null) {
            em.close();
            throw new RecursoNoEncontradoException();
        }
        em.close();
        return workflow;
    }

    /**
     * Metodo que obtiene una lista de nodos del workflow en estado de espera y
     * los retorna en una lista ordenada
     *
     * Es utilizado para a la hora de disparar tareas, ya que al completar
     * mision aun pueden haer elementos en espera
     *
     * @return
     */
    public List<WorkflowNodoEspera> getAllNodosEspera() {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultList = em.createNamedQuery("WorkflowNodoEspera.findAll").getResultList();
        em.close();
        return resultList;
    }

    /**
     * Elimina un nodo en espera de la base de datos , debe usarse una vez que
     * el nodo representado por esta entidad se ha procesado
     *
     * @param idNodo
     */
    public void deleteNodoEspera(String idNodo, String idMiembro) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        em.createNamedQuery("WorkflowNodoEspera.deleteByID").setParameter("idMiembro", idMiembro).setParameter("idNodo", idNodo).executeUpdate();
        em.getTransaction().commit();
        em.close();
    }

    public void registrarCompleteNodo(WorkflowNodo workflowNodo, String idMiembro) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        em.persist(new WorkflowCompletado(new WorkflowCompletadoPK(idMiembro,workflowNodo.getIdNodo())));
        em.getTransaction().commit();
        em.close();
    }
}
