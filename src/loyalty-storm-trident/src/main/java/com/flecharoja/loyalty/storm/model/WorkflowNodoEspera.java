/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "WORKFLOW_NODO_ESPERA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WorkflowNodoEspera.findAll", query = "SELECT w FROM WorkflowNodoEspera w")
    ,@NamedQuery(name = "WorkflowNodoEspera.deleteByID", query = "DELETE FROM WorkflowNodoEspera w WHERE w.workflowNodoEsperaPK.idNodo=:idNodo AND w.workflowNodoEsperaPK.idMiembro=:idMiembro ")
    , @NamedQuery(name = "WorkflowNodoEspera.findByIdNodo", query = "SELECT w FROM WorkflowNodoEspera w WHERE w.workflowNodoEsperaPK.idNodo = :idNodo")
    , @NamedQuery(name = "WorkflowNodoEspera.findByIdMiembro", query = "SELECT w FROM WorkflowNodoEspera w WHERE w.workflowNodoEsperaPK.idMiembro = :idMiembro")})
public class WorkflowNodoEspera implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected WorkflowNodoEsperaPK workflowNodoEsperaPK;
    @JoinColumn(name = "ID_NODO", referencedColumnName = "ID_NODO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private WorkflowNodo workflowNodo;

    public WorkflowNodoEspera() {
    }

    public WorkflowNodoEspera(WorkflowNodoEsperaPK workflowNodoEsperaPK) {
        this.workflowNodoEsperaPK = workflowNodoEsperaPK;
    }

    public WorkflowNodoEspera(String idNodo, String idMiembro) {
        this.workflowNodoEsperaPK = new WorkflowNodoEsperaPK(idNodo, idMiembro);
    }

    public WorkflowNodoEsperaPK getWorkflowNodoEsperaPK() {
        return workflowNodoEsperaPK;
    }

    public void setWorkflowNodoEsperaPK(WorkflowNodoEsperaPK workflowNodoEsperaPK) {
        this.workflowNodoEsperaPK = workflowNodoEsperaPK;
    }

    public WorkflowNodo getWorkflowNodo() {
        return workflowNodo;
    }

    public void setWorkflowNodo(WorkflowNodo workflowNodo) {
        this.workflowNodo = workflowNodo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (workflowNodoEsperaPK != null ? workflowNodoEsperaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkflowNodoEspera)) {
            return false;
        }
        WorkflowNodoEspera other = (WorkflowNodoEspera) object;
        if ((this.workflowNodoEsperaPK == null && other.workflowNodoEsperaPK != null) || (this.workflowNodoEsperaPK != null && !this.workflowNodoEsperaPK.equals(other.workflowNodoEsperaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.storm.model.WorkflowNodoEspera[ workflowNodoEsperaPK=" + workflowNodoEsperaPK + " ]";
    }
    
}
