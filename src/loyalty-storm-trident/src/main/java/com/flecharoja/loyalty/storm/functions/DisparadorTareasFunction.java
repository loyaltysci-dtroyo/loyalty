package com.flecharoja.loyalty.storm.functions;

import com.flecharoja.loyalty.storm.model.Workflow;
import com.flecharoja.loyalty.storm.model.WorkflowNodoEspera;
import com.flecharoja.loyalty.storm.service.WorkFlowNodoService;
import com.flecharoja.loyalty.storm.service.WorkflowService;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.storm.topology.ReportedFailedException;
import org.apache.storm.trident.operation.BaseFunction;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.operation.TridentOperationContext;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;
import org.hibernate.exception.JDBCConnectionException;

/**
 * Flecha Roja Technologies Funcion encargada de procesar los mensajes de
 * ejecucion de tareas y determinar si existe alguna accion a realizar, en casos
 * especiales como cuando se requiere una confirmacion(misiones), se envian las
 * tareas a un almacenamiento temporal
 *
 * @author faguilar
 */
public class DisparadorTareasFunction extends BaseFunction {

    //indicadores de disparador que se encuentran en workflow
    private final char DIS_MIE_REGISTRA_PROGRAMA = 'A';
    private final char DIS_MIE_REDIME_PROMOCION = 'B';
    private final char DIS_MIE_APRUEBA_MISION = 'C';
    private final char DIS_MIE_REDME_PREMIO = 'D';
    private final char DIS_MIE_COMPRO_PRODUCTO_ESPECIFICO = 'E';
    private final char DIS_MIE_CUMPLE_ANNOS = 'F';
    private final char DIS_MIE_EVENTO_PERSONALIZADO = 'I';
    private final char DIS_MIE_COMPRO_PRODUCTO = 'H';
    private final char DIS_MIE_CUMPLE_ANNIVERSARIO = 'J';
    private final char DIS_SEC_MISIONES = 'M';

    private List<Workflow> listaWorkflow;
    private List<WorkflowNodoEspera> listaNodoWorkflowEspera;
    private WorkflowService workflowService;
    private WorkFlowNodoService workflowNodoService;

    //valores de disparador que vienen en el topic de eventos-sistema
    private final int MIE_REGISTRA_PROGRAMA = 24;
    private final int MIE_AGREGA_PROMO_BOOKMARK = 20;
    private final int MIE_APRUEBA_MISION = 13;
    private final int MIE_REDIMIO_PREMIO = 18;
    private final int MIE_COMPRA_PRODUCTO = 19;
    private final int MIE_CUMPLE_ANNOS = 23;
    private final int MIE_EVENTO_PERSONALIZADO = 30;
    private final int MIE_CUMPLE_ANNIVERSARIO = 32;

    private static final Logger LOG = Logger.getLogger(DisparadorTareasFunction.class.getName());

    /**
     * Libera recursos
     */
    @Override
    public void cleanup() {
        super.cleanup();
    }

    /**
     * Metodo encargado de obtener la definicion de los elementos de tarea
     * programada del sistema y cargarlos en memoria, para lo cual los obtiene
     * del DBMS una vez al ser inicializada la topologia de storm, y los
     * actualiza cuando sean actualizadas las definiciones de metadatos en el
     * DBMS, para lo cual se genera un evento especifico
     *
     * @param conf
     * @param context
     */
    @Override
    public void prepare(Map conf, TridentOperationContext context) {
        super.prepare(conf, context);
        System.out.println("com.flecharoja.loyalty.storm.functions.DisparadorTareasFunction.prepare()");
        workflowNodoService = new WorkFlowNodoService();
        workflowService = new WorkflowService();
        listaNodoWorkflowEspera = this.workflowNodoService.getAllNodosEspera();
        //se obtienen los workflow con estado activo

    }

    /**
     * Metodo encargado de verificar una tupla, es decir un evento que
     * potencialmente disparara una tarea, para lo cual revisa la definicion de
     * metadatos almacenados y en caso que el evento en efecto califique para
     * disparar una tarea este genera un mensaje en Kafka
     *
     * @param tuple tupla a procesar
     * @param collector Trident collector
     *
     * formato de la tupla [indEvento:int, idMiembro:string, idElemento:string]
     */
    @Override
    public void execute(TridentTuple tuple, TridentCollector collector) {
        try {
            this.listaWorkflow = this.workflowService.getWorkflows(String.valueOf(Workflow.Estados.ACTIVO.getValue()));
            this.listaNodoWorkflowEspera = this.workflowNodoService.getAllNodosEspera();
            /*
            Segun el tipo de mision, se filtran los workflow de la lista de workflow, y se envia un mensaje para disparar 
            la ejecucion de cada uno de ellos
             */
            System.out.println("Disparador Tareas: " + tuple.toString());

            //se otiene el id del evento de la tupla 
            switch (tuple.getIntegerByField("indEvento")) {
                case MIE_APRUEBA_MISION: {
                    List<Workflow> listaTemp = this.listaWorkflow.stream()
                            .filter((workflow) -> {
                                //si el disparador del workflow es igual a aprobar mision (o a secuenciacion de misiones) y la referencia es igual al id de elemento
                                return workflow.getIndDisparador() != null 
                                    && ((workflow.getIndDisparador().equals(DIS_MIE_APRUEBA_MISION)&& workflow.getReferenciaDisparador().equalsIgnoreCase(tuple.getStringByField("idElemento")))
                                        || (workflow.getIndDisparador().equals(DIS_SEC_MISIONES) && workflow.getIdSucesor().getReferencia().equalsIgnoreCase(tuple.getStringByField("idElemento"))));
                            }).collect(Collectors.toList());
                    listaTemp.forEach((workflow) -> {
                        collector.emit(new Values(workflow.getIdSucesor().getIdNodo(), tuple.getStringByField("idMiembro")));
                    });
                    //Cuando un miembro aprueba una mision tambien es necesario verificar si hay instancias de ejecucion de 
                    //workflow esperando que una mision sea aprobada
                    this.listaNodoWorkflowEspera.stream().filter((nodo) -> {
                                //se filtran los nodos de workflow especial
                                //si el miembro que detona el evento es igual al miembro que tiene en espera este nodo y la referencia del nodo en espera es igual al elemento del nodo
                                return (nodo.getWorkflowNodo().getIsNodoMisionEspec())
                                        && (nodo.getWorkflowNodoEsperaPK().getIdMiembro().equals(tuple.getStringByField("idMiembro")))
                                        && (nodo.getWorkflowNodo().getReferencia().equals(tuple.getStringByField("idElemento")));
                            }).collect(Collectors.toList()).forEach((nodo) -> {
                        //se registra que el miembro completo la secuencia del workflow
//                      this.workflowNodoService.registrarCompleteNodo(workflow.getWorkflowNodo(), tuple.getStringByField("idMiembro"));
                        System.out.println("Ejecutando nodo en espera: " + nodo.getWorkflowNodo().getIdNodo());
                        collector.emit(new Values(nodo.getWorkflowNodo().getIdSucesor().getIdNodo(), tuple.getStringByField("idMiembro")));
                        
                        //posteroirmente a emitir una tupla para continuar el procesamiento de los nodos
                        //debe eliminarse el registro de nodo en espera para que no vuelva a ser procesado
                        this.workflowNodoService.deleteNodoEspera(nodo.getWorkflowNodo().getIdNodo(), nodo.getWorkflowNodoEsperaPK().getIdMiembro());
                    });
                    break;
                }
                case MIE_REDIMIO_PREMIO: {
                    this.listaWorkflow.stream()
                            .filter((workflow) -> {
                                return workflow.getIndDisparador().equals(DIS_MIE_REDME_PREMIO)
                                        && workflow.getReferenciaDisparador().equalsIgnoreCase(tuple.getStringByField("idElemento"));
                            })
                            .collect(Collectors.toList()).forEach((workflow) -> {
                        collector.emit(new Values(workflow.getIdSucesor().getIdNodo(), tuple.getStringByField("idMiembro")));
                    });
                    break;
                }
                case MIE_COMPRA_PRODUCTO: {
                    this.listaWorkflow.stream()
                            .filter((workflow) -> {
                                return workflow.getIndDisparador().equals(DIS_MIE_COMPRO_PRODUCTO);
                            })
                            .collect(Collectors.toList()).forEach((workflow) -> {
                        System.out.println("Disparando tarea, workflow: " + workflow.getIdWorkflow() + " referencia:" + workflow.getReferenciaDisparador());
                        collector.emit(new Values(workflow.getIdSucesor().getIdNodo(), tuple.getStringByField("idMiembro")));
                    });
                    break;
                }
                case MIE_AGREGA_PROMO_BOOKMARK: {
                    List<Workflow> lista = this.listaWorkflow.stream()
                            .filter((workflow) -> {
                                return workflow.getIndDisparador().equals(DIS_MIE_REDIME_PROMOCION)
                                        && workflow.getReferenciaDisparador().equalsIgnoreCase(tuple.getStringByField("idElemento"));
                            })
                            .collect(Collectors.toList());
                    lista.forEach((workflow) -> {
                        collector.emit(new Values(workflow.getIdSucesor().getIdNodo(), tuple.getStringByField("idMiembro")));
                    });
                    break;
                }
                case MIE_CUMPLE_ANNOS: {
                    this.listaWorkflow.stream()
                            .filter((workflow) -> {
                                return workflow.getIndDisparador() == DIS_MIE_CUMPLE_ANNOS;
                            }).peek(System.out::print)
                            .collect(Collectors.toList()).forEach((workflow) -> {
                        collector.emit(new Values(workflow.getIdSucesor().getIdNodo(), tuple.getStringByField("idMiembro")));
                    });
                    break;
                }
                case MIE_REGISTRA_PROGRAMA: {
                    this.listaWorkflow.stream()
                            .filter((workflow) -> {
                                return workflow.getIndDisparador().equals(DIS_MIE_REGISTRA_PROGRAMA);
                            })
                            .collect(Collectors.toList()).forEach((workflow) -> {
                        collector.emit(new Values(workflow.getIdSucesor().getIdNodo(), tuple.getStringByField("idMiembro")));
                    });
                    break;
                }
                case MIE_EVENTO_PERSONALIZADO: {
                    this.listaWorkflow.stream()
                            .filter((workflow) -> {
                                return workflow.getIndDisparador().equals(DIS_MIE_EVENTO_PERSONALIZADO);
                            })
                            .collect(Collectors.toList()).forEach((workflow) -> {
                        collector.emit(new Values(workflow.getIdSucesor().getIdNodo(), tuple.getStringByField("idMiembro")));
                    });
                    break;
                }
                case MIE_CUMPLE_ANNIVERSARIO: {
                    this.listaWorkflow.stream()
                            .filter((workflow) -> {
                                return workflow.getIndDisparador().equals(DIS_MIE_CUMPLE_ANNIVERSARIO);
                            })
                            .collect(Collectors.toList()).forEach((workflow) -> {
                        collector.emit(new Values(workflow.getIdSucesor().getIdNodo(), tuple.getStringByField("idMiembro")));
                    });
                    break;
                }
                default: {
                    break;
                }
            }
        } catch (Exception e) {
            if (e instanceof JDBCConnectionException) {
                throw new ReportedFailedException(e);
            }
            LOG.log(Level.SEVERE, "Error en el disparador de tareas procesando la tupla: " + tuple.toString(), e);
        }
    }

}
