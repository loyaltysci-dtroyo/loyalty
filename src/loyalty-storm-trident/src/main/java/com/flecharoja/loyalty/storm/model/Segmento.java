package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "SEGMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Segmento.findAll", query = "SELECT s FROM Segmento s"),
    @NamedQuery(name = "Segmento.countAll", query = "SELECT COUNT(s.idSegmento) FROM Segmento s"),
    @NamedQuery(name = "Segmento.findByIdSegmento", query = "SELECT s FROM Segmento s WHERE s.idSegmento = :idSegmento"),
    @NamedQuery(name = "Segmento.countByNombreDespliegue", query = "SELECT COUNT(s.idSegmento) FROM Segmento s WHERE s.nombreDespliegue = :nombreDespliegue"),
    @NamedQuery(name = "Segmento.findByIndEstado", query = "SELECT s FROM Segmento s WHERE s.indEstado = :indEstado"),
    @NamedQuery(name = "Segmento.findByIdSegmentoInLista", query = "SELECT s FROM Segmento s WHERE s.idSegmento IN :lista")
})
public class Segmento implements Serializable {
    
    public enum Estados {
        ACTIVO("AC"),
        ARCHIVADO("AR"),
        BORRADOR("BO");
        
        private final String value;
        private static final Map<String, Estados> lookup = new HashMap<>();
        
        private Estados(String value) {
            this.value = value;
        }
        
        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public String getValue() {
            return value;
        }
        
        public static Estados get (String value) {
            return value==null?null:lookup.get(value);
        }
    }
    
    public enum TiposReglas {
        ATRIBUTO_MIEMBRO("C"),
        METRICA_CAMBIO("B"),
        METRICA_BALANCE("A"),
        AVANZADA("D");

        private final String value;
        public static final String OPERADORES_NUMERICOS = "VWXYZ";

        private TiposReglas(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "segmento_uuid")
    @GenericGenerator(name = "segmento_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_SEGMENTO")
    private String idSegmento;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_DESPLIEGUE")
    private String nombreDespliegue;
    
    @Size(min = 1, max = 255)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTATICO")
    private Character indEstatico;
    
    @Column(name = "FRECUENCIA_ACTUALIZACION")
    private BigInteger frecuenciaActualizacion;
    
    @Column(name = "ULTIMA_ACTUALIZACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimaActualizacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private String indEstado;
    
    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;
    
    @Column(name = "FECHA_ARCHIVADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaArchivado;
    
    @Size(min = 1, max = 500)
    @Column(name = "TAGS")
    private String tags;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segmento")
    private List<ProductoListaSegmento> productoListaSegmentoList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segmento")
    private List<PremioListaSegmento> premioListaSegmentoList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSegmento")
    private List<ListaReglas> listaReglasList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segmento")
    private List<SegmentoListaMiembro> listaMiembrosList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segmento")
    private List<PromocionListaSegmento> listaPromocionesList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segmento")
    private List<NotificacionListaSegmento> notificacionListaSegmentoList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segmento")
    private List<MisionListaSegmento> misionListaSegmentoList;

    public Segmento() {
    }

    public String getIdSegmento() {
        return idSegmento;
    }

    public void setIdSegmento(String idSegmento) {
        this.idSegmento = idSegmento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreDespliegue() {
        return nombreDespliegue;
    }

    public void setNombreDespliegue(String nombreDespliegue) {
        this.nombreDespliegue = nombreDespliegue;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getIndEstatico() {
        return indEstatico;
    }

    public void setIndEstatico(Character indEstatico) {
        this.indEstatico = indEstatico!=null?Character.toUpperCase(indEstatico):null;
    }

    public BigInteger getFrecuenciaActualizacion() {
        return frecuenciaActualizacion;
    }

    public void setFrecuenciaActualizacion(BigInteger frecuenciaActualizacion) {
        this.frecuenciaActualizacion = frecuenciaActualizacion;
    }

    public Date getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public void setUltimaActualizacion(Date ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    public String getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(String indEstado) {
        this.indEstado = indEstado!=null?indEstado.toUpperCase():null;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Date getFechaArchivado() {
        return fechaArchivado;
    }

    public void setFechaArchivado(Date fechaArchivado) {
        this.fechaArchivado = fechaArchivado;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    
    @XmlTransient
    public List<ListaReglas> getListaReglasList() {
        return listaReglasList;
    }

    public void setListaReglasList(List<ListaReglas> listaReglasList) {
        this.listaReglasList = listaReglasList;
    }

    
    @XmlTransient
    public List<SegmentoListaMiembro> getListaMiembrosList() {
        return listaMiembrosList;
    }

    public void setListaMiembrosList(List<SegmentoListaMiembro> listaMiembrosList) {
        this.listaMiembrosList = listaMiembrosList;
    }

    
    @XmlTransient
    public List<PromocionListaSegmento> getListaPromocionesList() {
        return listaPromocionesList;
    }

    public void setListaPromocionesList(List<PromocionListaSegmento> listaPromocionesList) {
        this.listaPromocionesList = listaPromocionesList;
    }
    
    
    @XmlTransient
    public List<NotificacionListaSegmento> getNotificacionListaSegmentoList() {
        return notificacionListaSegmentoList;
    }

    public void setNotificacionListaSegmentoList(List<NotificacionListaSegmento> notificacionListaSegmentoList) {
        this.notificacionListaSegmentoList = notificacionListaSegmentoList;
    }
    
    
    @XmlTransient
    public List<MisionListaSegmento> getMisionListaSegmentoList() {
        return misionListaSegmentoList;
    }

    public void setMisionListaSegmentoList(List<MisionListaSegmento> misionListaSegmentoList) {
        this.misionListaSegmentoList = misionListaSegmentoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSegmento != null ? idSegmento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Segmento)) {
            return false;
        }
        Segmento other = (Segmento) object;
        if ((this.idSegmento == null && other.idSegmento != null) || (this.idSegmento != null && !this.idSegmento.equals(other.idSegmento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Segmento[ idSegmento=" + idSegmento + " ]";
    }

    
    @XmlTransient
    public List<PremioListaSegmento> getPremioListaSegmentoList() {
        return premioListaSegmentoList;
    }

    public void setPremioListaSegmentoList(List<PremioListaSegmento> premioListaSegmentoList) {
        this.premioListaSegmentoList = premioListaSegmentoList;
    }

    
    @XmlTransient
    public List<ProductoListaSegmento> getProductoListaSegmentoList() {
        return productoListaSegmentoList;
    }

    public void setProductoListaSegmentoList(List<ProductoListaSegmento> productoListaSegmentoList) {
        this.productoListaSegmentoList = productoListaSegmentoList;
    }

}
