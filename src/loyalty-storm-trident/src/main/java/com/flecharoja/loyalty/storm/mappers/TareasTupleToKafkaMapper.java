/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.mappers;

import org.apache.storm.kafka.trident.mapper.TridentTupleToKafkaMapper;
import org.apache.storm.trident.tuple.TridentTuple;

/**
 * Mapper encargado de convertir tuplas de mensajeria de storm en mensajes de
 * kafka
 *
 * @author faguilar formato de la tupla: [idNodo:string, idMiembro:string]
 */
public class TareasTupleToKafkaMapper implements TridentTupleToKafkaMapper {

    @Override
    public Object getKeyFromTuple(TridentTuple tuple) {
        return "nodo";
    }

    @Override
    public Object getMessageFromTuple(TridentTuple tuple) {
        try {
            String result = "{\"idMiembro\": \"" + tuple.getString(1) + "\", \"idNodo\": \"" + tuple.getString(0) + "\"}";
            System.out.println("Escribiendo nodo en kafka:" + tuple.toString() + " " + result);
            return result;
        } catch (Exception ex) {
            return null;
        }
    }

}
