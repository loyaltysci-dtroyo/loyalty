package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.model.ReglaAsignacionBadge;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author faguilar
 */
public class ReglaAsignacionBadgeService {

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * @param idBadge
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List<ReglaAsignacionBadge> getReglasLista(String idBadge) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultList = em.createNamedQuery("ReglaAsignacionBadge.findByIdBadge").setParameter("idBadge", idBadge).getResultList();
         em.close();
        return resultList;
    }

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public List getListaReglasInsigniasActivas() {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultList = em.createNamedQuery("ReglaAsignacionBadge.findAll").getResultList();
         em.close();
        return resultList;
    }

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * @param idRegla Identificador de la lista de reglas
     * @return Respuesta con una lista de todas las reglas encontradas
     */
    public ReglaAsignacionBadge getRegla(String idRegla) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        ReglaAsignacionBadge name = (ReglaAsignacionBadge) em.createNamedQuery("ReglaAsignacionBadge.findByIdRegla").setParameter("idRegla", idRegla).getSingleResult();
         em.close();
        return name;
    }

}
