/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "MEDIOS_PAGO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MediosPago.findAll", query = "SELECT m FROM MediosPago m"),
    @NamedQuery(name = "MediosPago.countAll", query = "SELECT COUNT(m.idTipo) FROM MediosPago m"),
    @NamedQuery(name = "MediosPago.findByIdTipo", query = "SELECT m FROM MediosPago m WHERE m.idTipo = :idTipo"),
    @NamedQuery(name = "MediosPago.findByNombre", query = "SELECT m FROM MediosPago m WHERE m.nombre = :nombre"),
    @NamedQuery(name = "MediosPago.findByImagen", query = "SELECT m FROM MediosPago m WHERE m.imagen = :imagen")})
public class MediosPago implements Serializable {

    

    private static final long serialVersionUID = 1L;
      @Id
    @GeneratedValue(generator = "pago_uuid")
    @GenericGenerator(name = "pago_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_TIPO")
    private String idTipo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    private String imagen;
    
    @JoinColumn(name = "ID_CONFIGURACION", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private ConfiguracionGeneral idConfiguracion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    public MediosPago() {
    }

    public MediosPago(String idTipo) {
        this.idTipo = idTipo;
    }

    public MediosPago(String idTipo, String nombre, String imagen) {
        this.idTipo = idTipo;
        this.nombre = nombre;
        this.imagen = imagen;
    }

    public String getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(String idTipo) {
        this.idTipo = idTipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public ConfiguracionGeneral getIdConfiguracion() {
        return idConfiguracion;
    }

    public void setIdConfiguracion(ConfiguracionGeneral idConfiguracion) {
        this.idConfiguracion = idConfiguracion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipo != null ? idTipo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MediosPago)) {
            return false;
        }
        MediosPago other = (MediosPago) object;
        if ((this.idTipo == null && other.idTipo != null) || (this.idTipo != null && !this.idTipo.equals(other.idTipo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MediosPago[ idTipo=" + idTipo + " ]";
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
}
