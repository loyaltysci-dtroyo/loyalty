package com.flecharoja.loyalty.storm.util;
/**
 *
 * @author svargas
 */
public class CodigosErrores {

    public static final String ENCABEZADO_CODIGO_ERROR = "Error-Reason";
    public static final String ESTADO_ERROR_CLIENTE = "Error de cliente";
    public static final String ESTADO_ERROR_AUTORIZACION = "Error de autorizacion";
    public static final String ESTADO_ERROR_NO_RECURSO = "Recurso no encontrado";
    public static final String ESTADO_ERROR_SISTEMA = "Error de sistema";

    public enum Cliente {
        ARGUMENTOS_ERRONEOS(100),
        ARGUMENTOS_PAGINACION_ERRONEOS(101),
        ENTIDAD_NO_EXISTENTE(102),
        RESULTADO_NO_UNICO(103),
        ENTIDAD_NO_VALIDA(104),
        OPERACION_NO_PERMITIDA(105),
        IMAGEN_NO_VALIDA(106),
        ERROR_AUTENTICACION(107),
        SIN_AUTORIZACION(108),
        DOCUMENTO_ERRONEO(109);

        private final int value;

        private Cliente(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    public enum Sistema {
        TIEMPO_CONEXION_DB_AGOTADO(200),
        ENTIDAD_EXISTENTE(201),
        PROBLEMAS_HBASE(202),
        OPERACION_FALLIDA(203),
        ERROR_DESCONOCIDO(300);


        private final int value;

        private Sistema(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
