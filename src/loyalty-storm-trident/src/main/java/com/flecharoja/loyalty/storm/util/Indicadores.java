/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.util;

import org.apache.storm.kafka.ExponentialBackoffMsgRetryManager;

/**
 *Clase encargada de agrupar os indicadores en un unico punto para efectos de 
 * mantenibilidad
 * @author faguilar
 */
public class Indicadores {

    
    public final static int ZK_PORT=2181;
    public final static long IND_EVENTO_MAX_INDEX = 34;
    public final static String BOOTSTRAP_SERVERS = "spark.flecharoja.com:9092,spark.flecharoja.com:9093,spark.flecharoja.com:9094,spark.flecharoja.com:9095";
    public final static String ACKS = "all";
    public final static int RETRIES = 1;
    public final static int BATCH_SIZE = 16800;
    public final static int LINGES_MS = 1;
    public final static int BUFFER_MEMORY = 33554432;
    public final static String KEY_SERIALIZER = "org.apache.kafka.common.serialization.StringSerializer";
    public final static String VALUE_SERIALIZER= "org.apache.kafka.common.serialization.StringSerializer";
    public final static long STATE_UPDATE_INTERVAL_MS = 2000;// setting para determinar que tan seguido almacenar el offset de mensajes en el zookeeper
    public final static String FAILED_MSG_RETRY_MANAGER_CLASS = ExponentialBackoffMsgRetryManager.class.getName(); // Estrategia usada para reintentar mensajes fallidos
    
    // Configuracion especifica para el rtry exponencial, estas son usadas por el ExponentialBackoffMsgRetryManager para reintentar mensajes luego de que un bolt
    // llame a OutputCollector.fail(). Estas settings surten efecto solo si se esta usando el ExponentialBackoffMsgRetryManager.
    // Delay entre cada reintento sucesivo
    public final static long RETRY_INITIAL_DELAY_MS = 0;
    public final static double RETRY_DELAY_MULTIPLIER = 1.0;
    public final static long RETRY_DELAY_MAX_MS = 60 * 1000;// Delay maximo entre reintentos sucesivos
    public final static long START_OFFSET_TIME = -1; //offset de tiempo
    public final static int RETRY_LIMIT = -1;// Un mensaje fallido sera reintentado infinitamente si el retry limit es negativo. 
    public final static String ZK_CONN_STRING = "spark.flecharoja.com:2181";//string de conexion para el zookeeper, si se omite el puerto entonces se asume que se usara el puerto default

    /**
     * *** topics en los cuales se escuchara en kafka ****
     */
    public final static String KAFKA_TOPIC_EVENTOS = "eventos-sistema"; //Eventos
    public final static String KAFKA_TOPIC_SPARK = "calculo-reglas";//Spark
    public final static String KAFKA_TOPIC_WORKFLOW="procesamiento-tareas";
    
    /**
     * Indicadores para los valores usados en metadatos por el API
     */
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILED = "FAILED";
    
    public static final char USUARIO_IND_ESTADO_ACTIVO = 'B';
    public static final char USUARIO_IND_ESTADO_INACTIVO = 'I';
    
    public static final char MIEMBRO_IND_ESTADO_ACTIVO = 'A';
    public static final char MIEMBRO_IND_ESTADO_INACTIVO = 'I';
    
    public static final char INCLUIDO = 'I';
    public static final char EXCLUIDO = 'E';
    public static final char ASIGNADO = 'A';
    public static final char DISPONIBLES = 'D';
    
    public static final char METRICA_IND_ESTADO_PUBLICADO = 'P';
    public static final char METRICA_IND_ESTADO_ARCHIVADO = 'A';
    public static final char METRICA_IND_ESTADO_BORRADOR = 'B';
    
    public static final char PROMOCION_EFECTIVIDAD_PERMANENTE = 'P';
    
    public static final char MISION_ENCUESTAPREGUNTA_INDRESPUESTA_TODOS = 'T';
    
    public static final Character UBICACION_IND_ESTADO_DRAFT = 'D';
    public static final Character UBICACION_IND_ESTADO_PUBLICADO = 'P';
    public static final Character UBICACION_IND_ESTADO_ARCHIVADO = 'A';
    
    public static final Character UBICACION_EFECTIVIDAD_PERMANENTE = 'P';
    public static final Character UBICACION_EFECTIVIDAD_CALENDARIZADO = 'C';
    public static final Character UBICACION_EFECTIVIDAD_RECURRENTE  = 'R';
    
    public static final String PREFERENCIA_IND_TIPO_RESPUESTA_SELEC_UNICA = "SU";
    public static final String PREFERENCIA_IND_TIPO_RESPUESTA_RESP_MULTIPLE = "RM";
    public static final String PREFERENCIA_IND_TIPO_RESPUESTA_TEXTO = "TX";
    
    public static final char IND_ENTIDAD_MIEMBRO = 'M';
    public static final char IND_ENTIDAD_SEGMENTO = 'S';
    public static final char IND_ENTIDAD_UBICACION = 'U';
    
    public static final String MIEMBRO_IND_ESTADO_RESIDENCIA = "L";
    public static final String MIEMBRO_IND_CIUDAD_RESIDENCIA ="K";
    
    public static final String UBICACION_IND_DIR_ESTADO = "UE";
    public static final String UBICACION_IND_DIR_CIUDAD = "UC";
    
    public static final char TABLA_POSICIONES_IND_TIPO_CALCULO_SUMA = 'S';
    public static final char TABLA_POSICIONES_IND_TIPO_CALCULO_PROMEDIO = 'P';
    
    public static final String TABLA_POSICIONES_IND_TIPO_MIEMBROS = "U";
    public static final String TABLA_POSICIONES_IND_TIPO_GRUPOS = "G";
    public static final String TABLA_POSICIONES_IND_TIPO_GRUPO_ESPECIFICO = "E";
    
    public static final char ATRIBUTO_DINAMICO_IND_ESTADO_ACTIVO = 'A';
    public static final char ATRIBUTO_DINAMICO_IND_ESTADO_INACTIVO = 'I';
    
    public static final char GRUPO_IND_VISIBLE = 'V';
    public static final char GRUPO_IND_INVISIBLE = 'I';
    
    public static final char NOTIFICACION_IND_OBJECTIVO_PROMOCION = 'P';
    
    public static final char INSIGNIAS_ESTADO_ARCHIVADO = 'A';
    public static final char INSIGNIAS_ESTADO_PUBLICADO = 'P';
    public static final char INSIGNIAS_ESTADO_BORRADOR = 'B';
    public static final char INSIGNIAS_TIPO_COLLECTOR = 'C';
    public static final char INSIGNIAS_TIPO_STATUS = 'S';
    
    public static final char CONFIGURACION_IND_PROTOCOLO_SMTP_S = 'S';
    public static final char CONFIGURACION_IND_PROTOCOLO_SMTP_T = 'T';
    public static final char CONFIGURACION_IND_PROTOCOLO_SMTP_N = 'N';
    
    public static final char PREMIO_IND_ESTADO_BORRADOR = 'B';
    public static final char PREMIO_IND_ESTADO_PUBLICADO = 'P';
    public static final char PREMIO_IND_ESTADO_ARCHIVADO = 'A';
    
    public static final char PREMIO_MIEMBRO_ESTADO_ASIGNADO = 'A';
    public static final char PREMIO_MIEMBRO_ESTADO_RECLAMADO = 'R';
    public static final char PREMIO_MIEMBRO_ESTADO_CANCELADO = 'C';
    public static final String PREMIO_MIEMBRO_IND_MOTIVO_ASIGNACION_AWARD = "A";
    
    public static final char PREMIO_EFECTIVIDAD_PERMANENTE = 'P';
    public static final char PREMIO_EFECTIVIDAD_CALENDARIZADO = 'C';
    public static final char PREMIO_EFECTIVIDAD_RECURRENTE  = 'R';
    public static final char PREMIO_IND_TIPO_CERTIFICADO = 'C';
    public static final char PREMIO_IND_TIPO_PRODUCTO = 'P';
    
    public static final char CERTIFICADO_ESTADO_DISPONIBLE = 'D';
    public static final char CERTIFICADO_ESTADO_REDIMIDO = 'R';
    public static final char CERTIFICADO_ESTADO_CANCELADO  = 'C';
    
    public static final char PRODUCTO_EFECTIVIDAD_PERMANENTE = 'P';
    public static final char PRODUCTO_EFECTIVIDAD_CALENDARIZADO = 'C';
    public static final char PRODUCTO_EFECTIVIDAD_RECURRENTE  = 'R';
  
    public static final Character PRODUCTO_IND_ESTADO_BORRADOR = 'B';
    public static final Character PRODUCTO_IND_ESTADO_PUBLICADO = 'P';
    public static final Character PRODUCTO_IND_ESTADO_ARCHIVADO = 'A';
    public static final Character PRODUCTO_IND_ENTREGA_UBICACION = 'U';
    public static final Character PRODUCTO_IND_ENTREGA_DOMICILIO = 'D';
    
    public static final char ATRIBUTO_PRODUCTO_NUMERICO = 'N';
    public static final char ATRIBUTO_PRODUCTO_TEXTO = 'T';
    public static final char ATRIBUTO_PRODUCTO_OPCIONES = 'O';
    
    public static final char ATRIBUTO_PRODUCTO_ACTIVO = 'A';
    public static final char ATRIBUTO_PRODUCTO_INACTIVO = 'I';
    
    public static final char MISION_JUEGO_IND_TIPO_PREMIO_PREMIO = 'P';
    public static final char MISION_JUEGO_IND_TIPO_PREMIO_METRICA = 'M';

    
    public static final Character PREMIO_JUEGO_TIPO_METRICA = 'M';
    public static final Character PREMIO_JUEGO_TIPO_PREMIO = 'P';
    
    public static final String PAGINACION_RANGO_ACEPTADO = "Accept-Range";
    public static final int PAGINACION_RANGO_ACEPTADO_VALOR = 50;
    public static final String PAGINACION_RANGO_DEFECTO = "10";
    public static final String PAGINACION_RANGO_CONTENIDO = "Content-Range";
    
    public static final String URL_IMAGEN_PREDETERMINADA = "http://580dfadb669a3d46adbc-6dc2c0f932578b1114beed9c144d3102.r58.cf1.rackcdn.com/ee5cf60a-7d8e-4062-8c40-c600d52d4294.jpeg";
    public static String CONTRASENA_TEMPORAL_PREDETERMINADA = "1234abcd";
    
    
}
