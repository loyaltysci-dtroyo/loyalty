/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "CONFIGURACION_GENERAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConfiguracionGeneral.findAll", query = "SELECT c FROM ConfiguracionGeneral c"),
    @NamedQuery(name = "ConfiguracionGeneral.findBasic", query = "SELECT c.nombreEmpresa, c.logoEmpresa FROM ConfiguracionGeneral c"),
    @NamedQuery(name = "ConfiguracionGeneral.findById", query = "SELECT c FROM ConfiguracionGeneral c WHERE c.id = :id"),
    @NamedQuery(name = "ConfiguracionGeneral.findByNombreEmpresa", query = "SELECT c FROM ConfiguracionGeneral c WHERE c.nombreEmpresa = :nombreEmpresa"),
    @NamedQuery(name = "ConfiguracionGeneral.findByLogoEmpresa", query = "SELECT c.logoEmpresa FROM ConfiguracionGeneral c WHERE c.id = :id")})
public class ConfiguracionGeneral implements Serializable {

   
    public enum Protocolos{
        SMTP_S('S'),
        SMTP_T('T'),
        SMTP_N('N');
        
        private final char value;
        private static final Map<Character,Protocolos> lookup = new HashMap<>();

        private Protocolos(char value) {
            this.value = value;
        }
        
        static{
            for(Protocolos protocolo : values()){
                lookup.put(protocolo.value, protocolo);
            }
        }

        public char getValue() {
            return value;
        }
        public static Protocolos get(Character value){
            return value==null?null:lookup.get(value);
        }
    }
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_EMPRESA")
    private String nombreEmpresa;
    
    @Size(max = 300)
    @Column(name = "LOGO_EMPRESA")
    private String logoEmpresa;
    
    @Size(max = 300)
    @Column(name = "SITIO_WEB")
    private String sitioWeb;
    
    @Column(name = "BONO_ENTRADA")
    private Double bonoEntrada;
    
    @Column(name = "BONO_REFERENCIA")
    private Double bonoReferencia;
    
    @Column(name = "PORCENTAJE_COMPRA")
    private Double porcentajeCompra;
    
    @JoinColumn(name = "ID_METRICA_INICIAL", referencedColumnName = "ID_METRICA")
    @ManyToOne
    private Metrica idMetricaInicial;
    
    @Column(name = "PORCENTAJE_IMPUESTO")
    private Double porcentajeImpuesto;

    @Column(name = "CANT_DIAS_ENTREGA_PEDIDO")
    private Integer cantDiasEntregaPedido;
    
    @Size(max = 500)
    @Column(name = "POLITICAS_SEGURIDAD")
    private String politicasSeguridad;
    
    @JoinColumn(name = "UBICACION_PRINCIPAL", referencedColumnName = "ID_UBICACION")
    @ManyToOne
    private Ubicacion ubicacionPrincipal;
 
    @Column(name = "PERIODO")
    private int periodo;
    
    @Column(name = "MES")
    private int mes;
    
    @Column(name = "INTEGRACION_INVENTARIO_PREMIO")
    private Boolean integracionInventarioPremios;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idConfiguracion")
    private List<MediosPago> mediosPagoList;

    public ConfiguracionGeneral() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getLogoEmpresa() {
        return logoEmpresa;
    }

    public void setLogoEmpresa(String logoEmpresa) {
        this.logoEmpresa = logoEmpresa;
    }

    public Metrica getIdMetricaInicial() {
        return idMetricaInicial;
    }

    public void setIdMetricaInicial(Metrica idMetricaInicial) {
        this.idMetricaInicial = idMetricaInicial;
    }
    
    public String getSitioWeb() {
        return sitioWeb;
    }

    public void setSitioWeb(String sitioWeb) {
        this.sitioWeb = sitioWeb;
    }

    public Double getBonoEntrada() {
        return bonoEntrada;
    }

    public void setBonoEntrada(Double bonoEntrada) {
        this.bonoEntrada = bonoEntrada;
    }

    public Double getBonoReferencia() {
        return bonoReferencia;
    }

    public void setBonoReferencia(Double bonoReferencia) {
        this.bonoReferencia = bonoReferencia;
    }

    public Double getPorcentajeCompra() {
        return porcentajeCompra;
    }

    public void setPorcentajeCompra(Double porcentajeCompra) {
        this.porcentajeCompra = porcentajeCompra;
    }

    public Double getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(Double porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public Integer getCantDiasEntregaPedido() {
        return cantDiasEntregaPedido;
    }

    public void setCantDiasEntregaPedido(Integer cantDiasEntregaPedido) {
        this.cantDiasEntregaPedido = cantDiasEntregaPedido;
    }

    public List<MediosPago> getMediosPagoList() {
        return mediosPagoList;
    }

    public void setMediosPagoList(List<MediosPago> mediosPagoList) {
        this.mediosPagoList = mediosPagoList;
    }

    public String getPoliticasSeguridad() {
        return politicasSeguridad;
    }

    public void setPoliticasSeguridad(String politicasSeguridad) {
        this.politicasSeguridad = politicasSeguridad;
    }

    public Ubicacion getUbicacionPrincipal() {
        return ubicacionPrincipal;
    }

    public void setUbicacionPrincipal(Ubicacion ubicacionPrincipal) {
        this.ubicacionPrincipal = ubicacionPrincipal;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfiguracionGeneral)) {
            return false;
        }
        ConfiguracionGeneral other = (ConfiguracionGeneral) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ConfiguracionGeneral[ id=" + id + " ]";
    }

    public Boolean getIntegracionInventarioPremios() {
        return integracionInventarioPremios;
    }

    public void setIntegracionInventarioPremios(Boolean integracionInventarioPremios) {
        this.integracionInventarioPremios = integracionInventarioPremios;
    }
 
}
