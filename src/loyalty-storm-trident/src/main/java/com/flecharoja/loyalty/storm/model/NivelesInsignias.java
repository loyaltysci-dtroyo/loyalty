/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "NIVELES_INSIGNIAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NivelesInsignias.findAll", query = "SELECT n FROM NivelesInsignias n"),
    @NamedQuery(name = "NivelesInsignias.countByNombreInterno", query = "SELECT COUNT(n.idNivel) FROM NivelesInsignias n WHERE n.nombreInterno = :nombreInterno"),
    @NamedQuery(name = "NivelesInsignias.findByIdNivel", query = "SELECT n FROM NivelesInsignias n WHERE n.idNivel = :idNivel"),
    @NamedQuery(name = "NivelesInsignias.findNivelesByIdInsignia", query = "SELECT n FROM NivelesInsignias n WHERE n.idInsignia.idInsignia = :idInsignia")

})
public class NivelesInsignias implements Serializable {

   
    

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "nivelinsignia_uuid")
    @GenericGenerator(name = "nivelinsignia_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_NIVEL")
    private String idNivel;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_NIVEL")
    private String nombreNivel;
    
    @Size(min = 1, max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    private String imagen;
    
    
    @JoinColumn(name = "ID_INSIGNIA", referencedColumnName = "ID_INSIGNIA")
    @ManyToOne(optional = false)
    private Insignias idInsignia;
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Version 
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
     
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idNivel")
    private List<MiembroInsigniaNivel> miembroInsigniaNivelList;


    public NivelesInsignias() {
    }

    public NivelesInsignias(String idNivel) {
        this.idNivel = idNivel;
    }

    public NivelesInsignias(String idNivel, String nombreNivel, String descripcion, String nombreInterno, String imagen, Long numVersion) {
        this.idNivel = idNivel;
        this.nombreNivel = nombreNivel;
        this.descripcion = descripcion;
        this.nombreInterno = nombreInterno;
        this.imagen = imagen;
        this.numVersion = numVersion;
    }

    public String getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(String idNivel) {
        this.idNivel = idNivel;
    }

    public String getNombreNivel() {
        return nombreNivel;
    }

    public void setNombreNivel(String nombreNivel) {
        this.nombreNivel = nombreNivel;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Insignias getIdInsignia() {
        return idInsignia;
    }

    public void setIdInsignia(Insignias idInsignia) {
        this.idInsignia = idInsignia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivel != null ? idNivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NivelesInsignias)) {
            return false;
        }
        NivelesInsignias other = (NivelesInsignias) object;
        if ((this.idNivel == null && other.idNivel != null) || (this.idNivel != null && !this.idNivel.equals(other.idNivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.NivelesInsignias[ idNivel=" + idNivel + " ]";
    }

    

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    
    @XmlTransient
    public List<MiembroInsigniaNivel> getMiembroInsigniaNivelList() {
        return miembroInsigniaNivelList;
    }

    public void setMiembroInsigniaNivelList(List<MiembroInsigniaNivel> miembroInsigniaNivelList) {
        this.miembroInsigniaNivelList = miembroInsigniaNivelList;
    }
    
}
