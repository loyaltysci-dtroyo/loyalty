package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.model.TablaPosiciones;
import javax.persistence.EntityManager;
import java.util.List;

/**
 *
 * @author faguilar
 */
public class LeaderboardService {

   

    public LeaderboardService() {
        
    }

    public List<TablaPosiciones> getListaLeaderboardsPorId(List<String> listaId) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultList = em.createQuery("SELECT t FROM TablaPosiciones t WHERE t.idTabla in :ids").setParameter("ids", listaId).getResultList();
        em.close();
        return resultList;
    }

    public TablaPosiciones getLeaderboardPorId(String idTabla) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Object singleResult = em.createQuery("SELECT t FROM TablaPosiciones t WHERE t.idTabla = :id").setParameter("id", idTabla).getSingleResult();
        em.close();
        return (TablaPosiciones) singleResult;
    }

}
