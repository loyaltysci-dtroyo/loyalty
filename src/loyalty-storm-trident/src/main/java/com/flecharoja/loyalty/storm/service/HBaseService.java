/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.service;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.KeyOnlyFilter;
import org.apache.hadoop.hbase.filter.RegexStringComparator;
import org.apache.hadoop.hbase.filter.RowFilter;
import org.apache.hadoop.hbase.util.Bytes;

/**
 *
 * @author faguilar
 */
public class HBaseService {

    private final Configuration hbaseConf;

    public HBaseService() {
        this.hbaseConf = new Configuration();
        this.hbaseConf.addResource("conf/hbase/hbase-site.xml");
    }
    private static final Logger LOG = Logger.getLogger(HBaseService.class.getName());

    /**
     * Verifica si el miembro ha completado una mision 
     * @param idMision
     * @param idMiembro
     * @return
     * @throws IOException 
     */
    public boolean isMisionCompletada(String idMision, String idMiembro) throws IOException {
        try (Connection connection = ConnectionFactory.createConnection(hbaseConf)) {
            Table table = connection.getTable(TableName.valueOf(hbaseConf.get("hbase.namespace"), "REGISTRO_MISION"));

            //se define un escaner sobre files con un prefijo (regex) en el rowkey (que inicie con el identificador de mision + miembro), en un orden descendiente (se ordenan por el ultimo valor pendiente... fecha)
            Scan scan = new Scan();
            FilterList filterList = new FilterList(new RowFilter(CompareFilter.CompareOp.EQUAL, new RegexStringComparator(idMision + "&" + idMiembro + "&.*")), new KeyOnlyFilter());
            scan.setFilter(filterList);
            
            //se obtiene el primer resultado... si existe
            Result result;
            try (ResultScanner scanner = table.getScanner(scan)) {
                result = scanner.next();
                return !(result == null || result.isEmpty());
            }
        } catch (IOException e) {
           LOG.log(Level.SEVERE, "database_connection_failed", e);
           throw e;
        }
    }
}
