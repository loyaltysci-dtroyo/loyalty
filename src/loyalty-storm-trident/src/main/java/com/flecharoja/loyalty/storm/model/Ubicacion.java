/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "UBICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ubicacion.findAll", query = "SELECT u FROM Ubicacion u"),
    @NamedQuery(name = "Ubicacion.countAll", query = "SELECT COUNT(u.idUbicacion) FROM Ubicacion u"),
    @NamedQuery(name = "Ubicacion.findByIdUbicacion", query = "SELECT u FROM Ubicacion u WHERE u.idUbicacion = :idUbicacion"),
    @NamedQuery(name = "Ubicacion.findByNombre", query = "SELECT u FROM Ubicacion u WHERE u.nombre = :nombre"),
    @NamedQuery(name = "Ubicacion.findByNombreDespliegue", query = "SELECT u FROM Ubicacion u WHERE u.nombreDespliegue = :nombreDespliegue"),
    @NamedQuery(name = "Ubicacion.findByIndEstado", query = "SELECT u FROM Ubicacion u WHERE u.indEstado = :indEstado"),
    @NamedQuery(name = "Ubicacion.findByIndDirPais", query = "SELECT u FROM Ubicacion u WHERE u.indDirPais = :indDirPais"),
    @NamedQuery(name = "Ubicacion.findByIndDirEstado", query = "SELECT u FROM Ubicacion u WHERE u.indDirEstado = :indDirEstado"),
    @NamedQuery(name = "Ubicacion.findByIndDirCiudad", query = "SELECT u FROM Ubicacion u WHERE u.indDirCiudad = :indDirCiudad"),})
public class Ubicacion implements Serializable {

    @Version
    @Basic(optional = false)
    @NotNull()
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @OneToMany(mappedBy = "idUbicacion")
    private List<Producto> productoList;



    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "ubicacion_uuid")
    @GenericGenerator(name = "ubicacion_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_UBICACION")
    private String idUbicacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_DESPLIEGUE")
    private String nombreDespliegue;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;

    @Size(max = 3)
    @Column(name = "IND_DIR_PAIS")
    private String indDirPais;

    @Size(max = 100)
    @Column(name = "IND_DIR_ESTADO")
    private String indDirEstado;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "IND_DIR_CIUDAD")
    private String indDirCiudad;



    @Size(min = 1, max = 250)
    @Column(name = "DIRECCION")
    private String direccion;


    @Column(name = "DIR_PROYECCION")
    private Character dirProyeccion;


    @Size(min = 1, max = 150)
    @Column(name = "HORARIO_ATENCION")
    private String horarioAtencion;


    @Size(min = 1, max = 25)
    @Column(name = "TELEFONO")
    private String telefono;

    @Column(name = "IND_CALENDARIZACION")
    private Character indCalendarizacion;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;

    @Size(max = 7)
    @Column(name = "IND_DIA_RECURRENCIA")
    private String indDiaRecurrencia;

    @Size(max = 4)
    @Column(name = "IND_SEMANA_RECURRENCIA")
    private String indSemanaRecurrencia;

    @Column(name = "RANGO_DETECCION")
    private BigInteger rangoDeteccion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "DIR_LAT")
    private BigDecimal dirLat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DIR_LNG")
    private BigDecimal dirLng;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUbicacion")
    private List<Zona> zonaList;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ubicacion")
    private List<ProductoListaUbicacion> productoListaUbicacionList;

    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ubicacion")
    private List<RegionUbicacion> regionUbicacionList;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ubicacion")
    private List<PremioListaUbicacion> premioListaUbicacionList;

    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ubicacion")
    private List<MisionListaUbicacion> misionListaUbicacionList;

    public Ubicacion() {
    }

    public Ubicacion(String idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    public Ubicacion(String idUbicacion, String nombre, String nombreDespliegue, Character indEstado, String indDirCiudad, String direccion, Character dirProyeccion, String horarioAtencion, String telefono, Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion, BigDecimal dirLat, BigDecimal dirLng) {
        this.idUbicacion = idUbicacion;
        this.nombre = nombre;
        this.nombreDespliegue = nombreDespliegue;
        this.indEstado = indEstado;
        this.indDirCiudad = indDirCiudad;
        this.direccion = direccion;
        this.dirProyeccion = dirProyeccion;
        this.horarioAtencion = horarioAtencion;
        this.telefono = telefono;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
        this.dirLat = dirLat;
        this.dirLng = dirLng;
    }

    public String getIdUbicacion() {
        return idUbicacion;
    }

    public void setIdUbicacion(String idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreDespliegue() {
        return nombreDespliegue;
    }

    public void setNombreDespliegue(String nombreDespliegue) {
        this.nombreDespliegue = nombreDespliegue;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    public String getIndDirPais() {
        return indDirPais;
    }

    public void setIndDirPais(String indDirPais) {
        this.indDirPais = indDirPais;
    }

    public String getIndDirEstado() {
        return indDirEstado;
    }

    public void setIndDirEstado(String indDirEstado) {
        this.indDirEstado = indDirEstado;
    }

    public String getIndDirCiudad() {
        return indDirCiudad;
    }

    public void setIndDirCiudad(String indDirCiudad) {
        this.indDirCiudad = indDirCiudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Character getDirProyeccion() {
        return dirProyeccion;
    }

    public void setDirProyeccion(Character dirProyeccion) {
        this.dirProyeccion = dirProyeccion;
    }

    public String getHorarioAtencion() {
        return horarioAtencion;
    }

    public void setHorarioAtencion(String horarioAtencion) {
        this.horarioAtencion = horarioAtencion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Character getIndCalendarizacion() {
        return indCalendarizacion;
    }

    public void setIndCalendarizacion(Character indCalendarizacion) {
        this.indCalendarizacion = indCalendarizacion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getIndDiaRecurrencia() {
        return indDiaRecurrencia;
    }

    public void setIndDiaRecurrencia(String indDiaRecurrencia) {
        this.indDiaRecurrencia = indDiaRecurrencia;
    }

    public String getIndSemanaRecurrencia() {
        return indSemanaRecurrencia;
    }

    public void setIndSemanaRecurrencia(String indSemanaRecurrencia) {
        this.indSemanaRecurrencia = indSemanaRecurrencia;
    }

    public BigInteger getRangoDeteccion() {
        return rangoDeteccion;
    }

    public void setRangoDeteccion(BigInteger rangoDeteccion) {
        this.rangoDeteccion = rangoDeteccion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public BigDecimal getDirLat() {
        return dirLat;
    }

    public void setDirLat(BigDecimal dirLat) {
        this.dirLat = dirLat;
    }

    public BigDecimal getDirLng() {
        return dirLng;
    }

    public void setDirLng(BigDecimal dirLng) {
        this.dirLng = dirLng;
    }
    
    
    @XmlTransient
    public List<MisionListaUbicacion> getMisionListaUbicacionList() {
        return misionListaUbicacionList;
    }

    public void setMisionListaUbicacionList(List<MisionListaUbicacion> misionListaUbicacionList) {
        this.misionListaUbicacionList = misionListaUbicacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUbicacion != null ? idUbicacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ubicacion)) {
            return false;
        }
        Ubicacion other = (Ubicacion) object;
        if ((this.idUbicacion == null && other.idUbicacion != null) || (this.idUbicacion != null && !this.idUbicacion.equals(other.idUbicacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Ubicacion[ idUbicacion=" + idUbicacion + " ]";
    }

   
    
    @XmlTransient
    public List<PremioListaUbicacion> getPremioListaUbicacionList() {
        return premioListaUbicacionList;
    }

    public void setPremioListaUbicacionList(List<PremioListaUbicacion> premioListaUbicacionList) {
        this.premioListaUbicacionList = premioListaUbicacionList;
    }

   
    
    @XmlTransient
    public List<RegionUbicacion> getRegionUbicacionList() {
        return regionUbicacionList;
    }

    public void setRegionUbicacionList(List<RegionUbicacion> regionUbicacionList) {
        this.regionUbicacionList = regionUbicacionList;
    }

    
    @XmlTransient
    public List<ProductoListaUbicacion> getProductoListaUbicacionList() {
        return productoListaUbicacionList;
    }

    public void setProductoListaUbicacionList(List<ProductoListaUbicacion> productoListaUbicacionList) {
        this.productoListaUbicacionList = productoListaUbicacionList;
    }

    
    @XmlTransient
    public List<Zona> getZonaList() {
        return zonaList;
    }

    public void setZonaList(List<Zona> zonaList) {
        this.zonaList = zonaList;
    }

    
    @XmlTransient
    public List<Producto> getProductoList() {
        return productoList;
    }

    public void setProductoList(List<Producto> productoList) {
        this.productoList = productoList;
    }


}
