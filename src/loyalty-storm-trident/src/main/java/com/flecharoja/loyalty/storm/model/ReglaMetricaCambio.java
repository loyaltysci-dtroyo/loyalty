package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "REGLA_METRICA_CAMBIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReglaMetricaCambio.getAllReglasInSegmentoActivo", query = "SELECT r FROM ReglaMetricaCambio r WHERE r.idLista.idSegmento.indEstado='AC'")
    ,
    @NamedQuery(name = "ReglaMetricaCambio.findByIdRegla", query = "SELECT r FROM ReglaMetricaCambio r WHERE r.idRegla = :idRegla")
    ,
    @NamedQuery(name = "ReglaMetricaCambio.findByIdLista", query = "SELECT r FROM ReglaMetricaCambio r WHERE r.idLista.idLista = :idLista")})
public class ReglaMetricaCambio implements Serializable {

    private static final long serialVersionUID = 1L;

    public enum Balances {

        ACUMULADO('A'),
        EXPIRADO('B'),
        REDIMIDO('C'),
        DISPONIBLE('D');

        private final char value;
        private static final Map<Character, Balances> lookup = new HashMap<>();

        private Balances(char value) {
            this.value = value;
        }

        static {
            for (Balances estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Balances get(char value) {
            return value == Character.MIN_VALUE ? null : lookup.get(value);
        }
    }
    public enum Calendarizaciones {

        EN_EL_ULTIMO('B'),
        EN_EL_ANTERIOR('C'),
        DESDE('D'),
        HASTA('H'),
        ENTRE('E'),
        MES_ACTUAL('F'),
        ANNO_ACTUAL('G');

        private final char value;
        private static final Map<Character, Calendarizaciones> lookup = new HashMap<>();

        private Calendarizaciones(char value) {
            this.value = value;
        }

        static {
            for (Calendarizaciones estado : values()) {
                lookup.put(estado.value, estado);
            }
        }

        public char getValue() {
            return value;
        }

        public static Calendarizaciones get(char value) {
            return value == Character.MIN_VALUE ? null : lookup.get(value);
        }
    }

    @Id
    @GeneratedValue(generator = "reglametricacambio_uuid")
    @GenericGenerator(name = "reglametricacambio_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_REGLA")
    private String idRegla;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_CAMBIO")
    private Character indCambio;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;

    @Column(name = "IND_VALOR")
    private Character indValor;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "FECHA_FINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFinal;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @JoinColumn(name = "ID_LISTA", referencedColumnName = "ID_LISTA")
    @ManyToOne(optional = false)
    private ListaReglas idLista;

    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    private Metrica idMetrica;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_OPERADOR")
    private Character indOperador;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "VALOR_COMPARACION")
    private String valorComparacion;

    @Basic(optional = false)
    @NotNull
    @Size(max = 40)
    @Column(name = "NOMBRE")
    private String nombre;

    public ReglaMetricaCambio() {
    }

    public String getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(String idRegla) {
        this.idRegla = idRegla;
    }

    public Character getIndCambio() {
        return indCambio;
    }

    public void setIndCambio(Character indCambio) {
        this.indCambio = indCambio != null ? Character.toUpperCase(indCambio) : null;
    }

    public Character getIndOperador() {
        return indOperador;
    }

    public void setIndOperador(Character indOperador) {
        this.indOperador = indOperador != null ? Character.toUpperCase(indOperador) : null;
    }

    public String getValorComparacion() {
        return valorComparacion;
    }

    public void setValorComparacion(String valorComparacion) {
        this.valorComparacion = valorComparacion;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo != null ? Character.toUpperCase(indTipo) : null;
    }

    public Character getIndValor() {
        return indValor;
    }

    public void setIndValor(Character indValor) {
        this.indValor = indValor != null ? Character.toUpperCase(indValor) : null;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    @XmlTransient
    public ListaReglas getIdLista() {
        return idLista;
    }

    public void setIdLista(ListaReglas idLista) {
        this.idLista = idLista;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegla != null ? idRegla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReglaMetricaCambio)) {
            return false;
        }
        ReglaMetricaCambio other = (ReglaMetricaCambio) object;
        if ((this.idRegla == null && other.idRegla != null) || (this.idRegla != null && !this.idRegla.equals(other.idRegla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.ReglaMetricaCambio[ idRegla=" + idRegla + " ]";
    }

}
