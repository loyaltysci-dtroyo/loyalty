package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_LISTA_MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MisionListaMiembro.findMiembrosByIdMision", query = "SELECT m.miembro FROM MisionListaMiembro m WHERE m.misionListaMiembroPK.idMision = :idMision"),
    @NamedQuery(name = "MisionListaMiembro.countMiembrosByIdMision", query = "SELECT COUNT(m) FROM MisionListaMiembro m WHERE m.misionListaMiembroPK.idMision = :idMision"),
    @NamedQuery(name = "MisionListaMiembro.findMiembrosByIdMisionIndTipo", query = "SELECT m.miembro FROM MisionListaMiembro m WHERE m.misionListaMiembroPK.idMision = :idMision AND m.indTipo = :indTipo"),
    @NamedQuery(name = "MisionListaMiembro.countMiembrosByIdMisionIndTipo", query = "SELECT COUNT(m) FROM MisionListaMiembro m WHERE m.misionListaMiembroPK.idMision = :idMision AND m.indTipo = :indTipo"),
    @NamedQuery(name = "MisionListaMiembro.findMiembrosNotInListaByIdMision", query = "SELECT m FROM Miembro m WHERE m.idMiembro NOT IN (SELECT m.misionListaMiembroPK.idMiembro FROM MisionListaMiembro m WHERE m.misionListaMiembroPK.idMision = :idMision)"),
    @NamedQuery(name = "MisionListaMiembro.countMiembrosNotInListaByIdMision", query = "SELECT COUNT(m) FROM Miembro m WHERE m.idMiembro NOT IN (SELECT m.misionListaMiembroPK.idMiembro FROM MisionListaMiembro m WHERE m.misionListaMiembroPK.idMision = :idMision)")
})
public class MisionListaMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MisionListaMiembroPK misionListaMiembroPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;
    
    @JoinColumn(name = "ID_MISION", referencedColumnName = "ID_MISION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Mision mision;

    public MisionListaMiembro() {
    }

    public MisionListaMiembro(String idMiembro, String idMision, Character indTipo, Date fechaCreacion, String usuarioCreacion) {
        this.misionListaMiembroPK = new MisionListaMiembroPK(idMiembro, idMision);
        this.indTipo = indTipo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public MisionListaMiembro(String idMiembro, String idMision) {
        this.misionListaMiembroPK = new MisionListaMiembroPK(idMiembro, idMision);
    }

    public MisionListaMiembroPK getMisionListaMiembroPK() {
        return misionListaMiembroPK;
    }

    public void setMisionListaMiembroPK(MisionListaMiembroPK misionListaMiembroPK) {
        this.misionListaMiembroPK = misionListaMiembroPK;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo == null ? null : Character.toUpperCase(indTipo);
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public Mision getMision() {
        return mision;
    }

    public void setMision(Mision mision) {
        this.mision = mision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (misionListaMiembroPK != null ? misionListaMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionListaMiembro)) {
            return false;
        }
        MisionListaMiembro other = (MisionListaMiembro) object;
        if ((this.misionListaMiembroPK == null && other.misionListaMiembroPK != null) || (this.misionListaMiembroPK != null && !this.misionListaMiembroPK.equals(other.misionListaMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionListaMiembro[ misionListaMiembroPK=" + misionListaMiembroPK + " ]";
    }
    
}
