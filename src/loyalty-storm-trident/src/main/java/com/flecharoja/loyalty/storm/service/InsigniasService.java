package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.exception.RecursoNoEncontradoException;
import com.flecharoja.loyalty.storm.util.CodigosErrores;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.flecharoja.loyalty.storm.model.Insignias;
import com.flecharoja.loyalty.storm.model.Miembro;
import com.flecharoja.loyalty.storm.model.MiembroInsigniaNivel;
import com.flecharoja.loyalty.storm.model.NivelesInsignias;
import com.flecharoja.loyalty.storm.util.Indicadores;
import java.util.Calendar;
import java.util.Optional;

/**
 * Clase encargada de proveer metodos para el mantenimiento de la de la
 * informacion de insignias
 *
 * @author faguilar
 */
public class InsigniasService {

    public InsigniasService() {

    }

    /**
     * Método que devuelve la información de la insignia según su identificador
     *
     * @param idInsignia identificador único de la métrica
     * @return resultado de la operación
     */
    public Insignias getInsigniaById(String idInsignia) throws RecursoNoEncontradoException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Insignias insignia = em.find(Insignias.class, idInsignia);//se busca la entidad
        //verificacion que la insignia no sea nula (no se encontro)
        if (insignia == null) {
            throw new RecursoNoEncontradoException();
        }
        em.close();
        return insignia;
    }

    /**
     * Método que obtiene un listado de insignias según su estado, en un rango
     * de resultados según parametros
     *
     * @param estado indicador del estado deseado
     * @return listado de insignias y rango de la respuesta
     * @throws com.flecharoja.loyalty.storm.exception.ErrorSistemaException
     */
    public List<Insignias> getInsignias(String estado) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultado = null;//almacena el resultado
        if (estado == null) {
            resultado = em.createNamedQuery("Insignias.findAll").getResultList();
        } else {//en el caso de que si se manden valores de estado de insignias deseadas
            //se crea un contructor de peticion por criterio
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<Insignias> root = query.from(Insignias.class);
            //listado de predicados para la comparacion de estados
            List<Predicate> predicates = new ArrayList<>();
            for (String e : estado.split("-")) {//se recorre la cadena de estado, dividiendolo por el caracter "-"
                //se toma el primer caracter de cada elemento como un indicador de estado valido para su comparacion con el indicador de estado almacenado
                predicates.add(cb.equal(root.get("estado"), e.toUpperCase().charAt(0)));
            }
            //asignacion de los predicados en la clausula WHERE
            query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));

            //obtencion del listado en un rango
            resultado = em.createQuery(query.select(root)).getResultList();
        }
        em.close();
        return resultado;
    }

    /**
     * Método que asigna a un miembro un nivel de una insignia especifica,
     * **solo para insignias de tipo status**
     *
     * @param idMiembro identificador único del miembro
     * @param idNivel identificador único del nivel
     * @param idInsignia identificador de la insignia
     * @param usuario usuario en sesión
     * @param insignia insignia a agregar
     * @throws com.flecharoja.loyalty.storm.exception.ErrorSistemaException
     */
    public void assignMiembroInsigniaStatus(String idMiembro, String idNivel, String idInsignia, String usuario, Insignias insignia) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        MiembroInsigniaNivel miembroInsigniaNivel = new MiembroInsigniaNivel(idMiembro, idInsignia);
        Miembro miembro = em.find(Miembro.class, idMiembro);
        NivelesInsignias nivelInsignia = em.find(NivelesInsignias.class, idNivel);
        //verifica que la insignia no este en estado archivado o en borrador
        if (insignia.getIdInsignia().equals(nivelInsignia.getIdInsignia().getIdInsignia())) {
            miembroInsigniaNivel.setInsignias(insignia);
            miembroInsigniaNivel.setMiembro(miembro);
            miembroInsigniaNivel.setIdNivel(nivelInsignia);
            miembroInsigniaNivel.setFechaCreacion(Calendar.getInstance().getTime());
            miembroInsigniaNivel.setUsuarioCreacion(usuario);
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            em.persist(miembroInsigniaNivel);
            em.flush();
            em.clear();
            em.getTransaction().commit();

        } else {
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA, "El nivel de insignia no corresponde a la insignia");
        }
        em.close();
    }

    /**
     * Método que asigna a un miembro a una insignia collector
     *
     * @param idMiembro identificador único del miembro
     * @param insignia
     * @throws com.flecharoja.loyalty.storm.exception.ErrorSistemaException
     */
    public void assignMiembroInsigniaCollector(String idMiembro, Insignias insignia) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        MiembroInsigniaNivel miembroInsigniaNivel = new MiembroInsigniaNivel(idMiembro, insignia.getIdInsignia());
        Miembro miembro = em.find(Miembro.class, idMiembro);
        if (insignia.getEstado().equals(Indicadores.INSIGNIAS_ESTADO_BORRADOR) || insignia.getEstado().equals(Indicadores.INSIGNIAS_ESTADO_ARCHIVADO)) {
//            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        } else if (insignia.getTipo().equals(Indicadores.INSIGNIAS_TIPO_COLLECTOR)) {
            miembroInsigniaNivel.setInsignias(insignia);
            miembroInsigniaNivel.setMiembro(miembro);
            miembroInsigniaNivel.setIdNivel(null);
            miembroInsigniaNivel.setFechaCreacion(Calendar.getInstance().getTime());
            miembroInsigniaNivel.setUsuarioCreacion(idMiembro);
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            em.persist(miembroInsigniaNivel);
            em.flush();
            em.clear();
            em.getTransaction().commit();
        } else {
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        em.close();
    }

    /**
     * Metodo que obtiene un listado de niveles de insignias almacenadas
     *
     * @param idInsignia identificador unico de la insignia al que pertenece el
     * nivel
     * @return Respuesta con una coleccion de niveles encontrados
     */
    public List<NivelesInsignias> getNivelesInsignias(String idInsignia) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List<NivelesInsignias> niveles = em.createNamedQuery("NivelesInsignias.findNivelesByIdInsignia").setParameter("idInsignia", idInsignia).getResultList();
        em.close();
        return niveles;
    }

    /**
     * Asigna una insignia
     *
     * @param idInsignia insignia a asignar
     * @param idMiembro id del miembro al cual asignar la insignia
     * @throws ErrorSistemaException
     * @throws RecursoNoEncontradoException
     */

    public void asignarInsignia(String idInsignia, String idMiembro) throws ErrorSistemaException, RecursoNoEncontradoException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Insignias insignia = em.find(Insignias.class, idInsignia);
        if (insignia.getTipo().equals(Insignias.Tipos.COLLECTOR.getValue())) {
            this.assignMiembroInsigniaCollector(idMiembro, insignia);
        } else if (insignia.getTipo().equals(Insignias.Tipos.STATUS.getValue())) {
            //se obtienen todos los niveles de la insignia y se elige el primer nivel
            List<NivelesInsignias> niveles = (List<NivelesInsignias>) this.getNivelesInsignias(idInsignia);
            Optional<NivelesInsignias> optNivelInsignia = niveles.stream().findFirst();
            if (optNivelInsignia.isPresent()) {
                this.assignMiembroInsigniaStatus(idMiembro, optNivelInsignia.get().getIdNivel(), idInsignia, idMiembro, insignia);
            } else {
                throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
            }

        }
        em.close();
    }

    /**
     * Retorna true cuando el miembro ya tiene asignada una insignia
     *
     * @param idMiembro id del miembro a comprobar
     * @param idInsignia id de la insignia que se quiere comprobar
     * @return
     */
    public boolean verificarInsignia(String idMiembro, String idInsignia) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        //lista de insignias que tiene el miembro
        List<MiembroInsigniaNivel> minList = em.createNamedQuery("MiembroInsigniaNivel.findByIdMiembro").setParameter("idMiembro", idMiembro).getResultList();
        Optional<MiembroInsigniaNivel> findFirst = minList.stream().filter((min) -> {
            return min.getInsignias().getIdInsignia().equals(idInsignia); //To change body of generated lambdas, choose Tools | Templates.
        }).findFirst();
        em.close();
        return findFirst.isPresent();
    }

}
