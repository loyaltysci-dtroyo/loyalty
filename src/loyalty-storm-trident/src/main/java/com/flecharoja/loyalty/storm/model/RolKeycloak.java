/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;

/**
 *
 * @author wtencio
 */
public class RolKeycloak implements Serializable{
    
    
    private String id;
    private String nombre;
    private String descripcion;
    private String containerId;

    public RolKeycloak() {
    }

    public RolKeycloak(String id, String nombre, String descripcion, String containerId) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.containerId = containerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }
    
    
    
}
