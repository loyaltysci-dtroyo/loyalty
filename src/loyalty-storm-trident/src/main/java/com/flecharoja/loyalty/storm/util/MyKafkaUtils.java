package com.flecharoja.loyalty.storm.util;

import java.io.IOException;
import java.util.Properties;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author svargas
 */
public class MyKafkaUtils {
    
    public enum EventosSistema {
        ENTRA_GRUPO(1),
        SALE_GRUPO(2),
        ASIGNACION_INSIGNIA(3),
        DESASIGNACION_INSIGNIA(4),
        ASIGNACION_TABPOS(5),
        DESASIGNACION_TABPOS(6),
        GANA_METRICA(7),
        ALCANZA_NIVEL_METRICA(10),
        MIEMBRO_DEJA_NIVEL_METRICA(11),
        APROBO_MISION(13),
        REPROBO_MISION(14),
        RECIBIO_PREMIO(17),
        REMIDIO_PREMIO(18),
        CUMPLEANOS(23),
        REGISTRO_MIEMBRO(24),
        ANIVERSARIO_PROGRAMA(32),;

        private final int value;

        private EventosSistema(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    private final Properties properties;

    public MyKafkaUtils() throws IOException {
        this.properties = new Properties();
        this.properties.load(MyKafkaUtils.class.getResourceAsStream("/conf/kafka/kafka-producer.properties"));
    }
    
    public void notifyEvento (EventosSistema evento, String idElemento, String idMiembro) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("indEvento", evento.getValue());
        if (idElemento != null) {
            builder.add("idElemento", idElemento);
        }
        builder.add("idMiembro", idMiembro);
        try (KafkaProducer<Object, Object> producer = new KafkaProducer<>(properties)) {
            producer.send(new ProducerRecord<>("eventos-sistema", builder.build().toString()));
        }
    }
}
