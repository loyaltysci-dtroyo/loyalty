/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "ATRIBUTO_DINAMICO_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AtributoDinamicoProducto.findAll", query = "SELECT a FROM AtributoDinamicoProducto a"),
    @NamedQuery(name = "AtributoDinamicoProducto.countAll", query = "SELECT COUNT(a.idAtributo) FROM AtributoDinamicoProducto a"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByIdAtributo", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.idAtributo = :idAtributo"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByNombre", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByDescripcion", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.descripcion = :descripcion"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByIndTipo", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.indTipo = :indTipo"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByValorOpciones", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.valorOpciones = :valorOpciones"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByIndRequerido", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.indRequerido = :indRequerido"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByIndEstado", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.indEstado = :indEstado"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByFechaCreacion", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByUsuarioCreacion", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.usuarioCreacion = :usuarioCreacion"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByFechaModificacion", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.fechaModificacion = :fechaModificacion"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByUsuarioModificacion", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.usuarioModificacion = :usuarioModificacion"),
    @NamedQuery(name = "AtributoDinamicoProducto.findByNumVersion", query = "SELECT a FROM AtributoDinamicoProducto a WHERE a.numVersion = :numVersion")})
public class AtributoDinamicoProducto implements Serializable {

    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @Size(max = 300)
    @Column(name = "IMAGEN")
    private String imagen;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "atributo_producto_uuid")
    @GenericGenerator(name = "atributo_producto_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_ATRIBUTO")
    private String idAtributo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;
    
    @Size(max = 300)
    @Column(name = "VALOR_OPCIONES")
    private String valorOpciones;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_REQUERIDO")
    private Character indRequerido;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    
    @Transient
    private String valorAtributo;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "atributoDinamicoProducto")
    private List<ProductoAtributo> productoAtributoList;

    public AtributoDinamicoProducto() {
    }

    public AtributoDinamicoProducto(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public AtributoDinamicoProducto(String idAtributo, String nombre, Character indTipo, Character indRequerido, Character indEstado, Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.idAtributo = idAtributo;
        this.nombre = nombre;
        this.indTipo = indTipo;
        this.indRequerido = indRequerido;
        this.indEstado = indEstado;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public String getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public String getValorOpciones() {
        return valorOpciones;
    }

    public void setValorOpciones(String valorOpciones) {
        this.valorOpciones = valorOpciones;
    }

    public Character getIndRequerido() {
        return indRequerido;
    }

    public void setIndRequerido(Character indRequerido) {
        this.indRequerido = indRequerido;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public String getValorAtributo() {
        return valorAtributo;
    }

    public void setValorAtributo(String valorAtributo) {
        this.valorAtributo = valorAtributo;
    }
    
    

    
    @XmlTransient
    public List<ProductoAtributo> getProductoAtributoList() {
        return productoAtributoList;
    }

    public void setProductoAtributoList(List<ProductoAtributo> productoAtributoList) {
        this.productoAtributoList = productoAtributoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtributo != null ? idAtributo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtributoDinamicoProducto)) {
            return false;
        }
        AtributoDinamicoProducto other = (AtributoDinamicoProducto) object;
        if ((this.idAtributo == null && other.idAtributo != null) || (this.idAtributo != null && !this.idAtributo.equals(other.idAtributo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.AtributoDinamicoProducto[ idAtributo=" + idAtributo + " ]";
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    
}
