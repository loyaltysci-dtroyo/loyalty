package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p"),
    @NamedQuery(name = "Producto.countByNombreInterno", query = "SELECT COUNT(p.idProducto) FROM Producto p WHERE p.nombreInterno = :nombreInterno"),
    @NamedQuery(name = "Producto.countAll", query = "SELECT COUNT(p.idProducto) FROM Producto p"),
})
public class Producto implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_IMPUESTO")
    private Boolean indImpuesto;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "LIMITE_IND_RESPUESTA")
    private Boolean limiteIndRespuesta;
    
    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @Column(name = "CANT_MIN_METRICA")
    private BigInteger cantMinMetrica;
    
    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne
    private Metrica idMetrica;
    
    @JoinColumn(name = "ID_NIVEL_MINIMO", referencedColumnName = "ID_NIVEL")
    @ManyToOne
    private NivelMetrica idNivelMinimo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ENTREGA")
    private Character indEntrega;
    
    
    @JoinColumn(name = "ID_UBICACION", referencedColumnName = "ID_UBICACION")
    @ManyToOne
    private Ubicacion idUbicacion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private List<ProductoAtributo> productoAtributoList;

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "producto_uuid")
    @GenericGenerator(name = "producto_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_PRODUCTO")
    private String idProducto;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;
    
    @Size(max = 300)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    @Size(max = 100)
    @Column(name = "SKU")
    private String sku;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRECIO")
    private BigInteger precio;
    
    
    @Column(name = "MONTO_ENTREGA")
    private BigInteger montoEntrega;
    
    @Size(max = 300)
    @Column(name = "TAGS")
    private String tags;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "EFECTIVIDAD_IND_CALENDARIZADO")
    private Character efectividadIndCalendarizado;
    
    @Column(name = "EFECTIVIDAD_FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date efectividadFechaInicio;
    
    @Column(name = "EFECTIVIDAD_FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date efectividadFechaFin;
    
    @Size(max = 7)
    @Column(name = "EFECTIVIDAD_IND_DIAS")
    private String efectividadIndDias;
    
    @Column(name = "EFECTIVIDAD_IND_SEMANA")
    private BigInteger efectividadIndSemana;
    
    
    @Column(name = "LIMITE_CANT_TOTAL")
    private BigInteger limiteCantTotal;
    
    @Column(name = "LIMITE_IND_INTERVALO_TOTAL")
    private Character limiteIndIntervaloTotal;
    
    @Column(name = "LIMITE_CANT_TOTAL_MIEMBRO")
    private Character limiteCantTotalMiembro;
    
    @Column(name = "LIMITE_IND_INTERVALO_MIEMBRO")
    private Character limiteIndIntervaloMiembro;
    
    @Column(name = "LIMITE_IND_INTERVALO_RESPUESTA")
    private Character limiteIndIntervaloRespuesta;
    
    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;
    
    @Column(name = "FECHA_ARCHIVADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaArchivado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN_ARTE")
    private String imagenArte;
    
    
    @Size(max = 150)
    @Column(name = "ENCABEZADO_ARTE")
    private String encabezadoArte;
    
    @Size(max = 300)
    @Column(name = "SUBENCABEZADO_ARTE")
    private String subencabezadoArte;
    
    @Size(max = 500)
    @Column(name = "DETALLE_ARTE")
    private String detalleArte;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private List<ProductoListaMiembro> productoListaMiembroList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private List<ProductoCategSubcateg> productoCategSubcategList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private List<ProductoListaUbicacion> productoListaUbicacionList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private List<ProductoListaSegmento> productoListaSegmentoList;
    

    public Producto() {
    }

    
    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public BigInteger getPrecio() {
        return precio;
    }

    public void setPrecio(BigInteger precio) {
        this.precio = precio;
    }

    public Boolean getIndImpuesto() {
        return indImpuesto;
    }

    public void setIndImpuesto(Boolean indImpuesto) {
        this.indImpuesto = indImpuesto;
    }

    public BigInteger getMontoEntrega() {
        return montoEntrega;
    }

    public void setMontoEntrega(BigInteger montoEntrega) {
        this.montoEntrega = montoEntrega;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado == null ? null : Character.toUpperCase(indEstado);
    }

    public Character getEfectividadIndCalendarizado() {
        return efectividadIndCalendarizado;
    }

    public void setEfectividadIndCalendarizado(Character efectividadIndCalendarizado) {
        this.efectividadIndCalendarizado = efectividadIndCalendarizado == null ? null : Character.toUpperCase(efectividadIndCalendarizado);
    }

    public Date getEfectividadFechaInicio() {
        return efectividadFechaInicio;
    }

    public void setEfectividadFechaInicio(Date efectividadFechaInicio) {
        this.efectividadFechaInicio = efectividadFechaInicio;
    }

    public Date getEfectividadFechaFin() {
        return efectividadFechaFin;
    }

    public void setEfectividadFechaFin(Date efectividadFechaFin) {
        this.efectividadFechaFin = efectividadFechaFin;
    }

    public String getEfectividadIndDias() {
        return efectividadIndDias;
    }

    public void setEfectividadIndDias(String efectividadIndDias) {
        this.efectividadIndDias = efectividadIndDias;
    }

    public BigInteger getEfectividadIndSemana() {
        return efectividadIndSemana;
    }

    public void setEfectividadIndSemana(BigInteger efectividadIndSemana) {
        this.efectividadIndSemana = efectividadIndSemana;
    }

    public Character getIndEntrega() {
        return indEntrega;
    }

    public void setIndEntrega(Character indEntrega) {
        this.indEntrega = indEntrega;
    }


    public Boolean getLimiteIndRespuesta() {
        return limiteIndRespuesta;
    }

    public void setLimiteIndRespuesta(Boolean limiteIndRespuesta) {
        this.limiteIndRespuesta = limiteIndRespuesta;
    }

    public BigInteger getLimiteCantTotal() {
        return limiteCantTotal;
    }

    public void setLimiteCantTotal(BigInteger limiteCantTotal) {
        this.limiteCantTotal = limiteCantTotal;
    }

    public Character getLimiteIndIntervaloTotal() {
        return limiteIndIntervaloTotal;
    }

    public void setLimiteIndIntervaloTotal(Character limiteIndIntervaloTotal) {
        this.limiteIndIntervaloTotal = limiteIndIntervaloTotal == null ? null : Character.toUpperCase(limiteIndIntervaloTotal);
    }

    public Character getLimiteCantTotalMiembro() {
        return limiteCantTotalMiembro;
    }

    public void setLimiteCantTotalMiembro(Character limiteCantTotalMiembro) {
        this.limiteCantTotalMiembro = limiteCantTotalMiembro == null ? null : Character.toUpperCase(limiteCantTotalMiembro);
    }

    public Character getLimiteIndIntervaloMiembro() {
        return limiteIndIntervaloMiembro;
    }

    public void setLimiteIndIntervaloMiembro(Character limiteIndIntervaloMiembro) {
        this.limiteIndIntervaloMiembro = limiteIndIntervaloMiembro == null ? null : Character.toUpperCase(limiteIndIntervaloMiembro);
    }

    public Character getLimiteIndIntervaloRespuesta() {
        return limiteIndIntervaloRespuesta;
    }

    public void setLimiteIndIntervaloRespuesta(Character limiteIndIntervaloRespuesta) {
        this.limiteIndIntervaloRespuesta = limiteIndIntervaloRespuesta == null ? null : Character.toUpperCase(limiteIndIntervaloRespuesta);
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Date getFechaArchivado() {
        return fechaArchivado;
    }

    public void setFechaArchivado(Date fechaArchivado) {
        this.fechaArchivado = fechaArchivado;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Producto[ idProducto=" + idProducto + " ]";
    }

    public String getImagenArte() {
        return imagenArte;
    }

    public void setImagenArte(String imagenArte) {
        this.imagenArte = imagenArte;
    }

   

    public String getEncabezadoArte() {
        return encabezadoArte;
    }

    public void setEncabezadoArte(String encabezadoArte) {
        this.encabezadoArte = encabezadoArte;
    }

    public String getSubencabezadoArte() {
        return subencabezadoArte;
    }

    public void setSubencabezadoArte(String subencabezadoArte) {
        this.subencabezadoArte = subencabezadoArte;
    }

    public String getDetalleArte() {
        return detalleArte;
    }

    public void setDetalleArte(String detalleArte) {
        this.detalleArte = detalleArte;
    }

    
    @XmlTransient
    public List<ProductoListaMiembro> getProductoListaMiembroList() {
        return productoListaMiembroList;
    }

    public void setProductoListaMiembroList(List<ProductoListaMiembro> productoListaMiembroList) {
        this.productoListaMiembroList = productoListaMiembroList;
    }

    
    @XmlTransient
    public List<ProductoCategSubcateg> getProductoCategSubcategList() {
        return productoCategSubcategList;
    }

    public void setProductoCategSubcategList(List<ProductoCategSubcateg> productoCategSubcategList) {
        this.productoCategSubcategList = productoCategSubcategList;
    }

    
    @XmlTransient
    public List<ProductoListaUbicacion> getProductoListaUbicacionList() {
        return productoListaUbicacionList;
    }

    public void setProductoListaUbicacionList(List<ProductoListaUbicacion> productoListaUbicacionList) {
        this.productoListaUbicacionList = productoListaUbicacionList;
    }

    
    @XmlTransient
    public List<ProductoListaSegmento> getProductoListaSegmentoList() {
        return productoListaSegmentoList;
    }

    public void setProductoListaSegmentoList(List<ProductoListaSegmento> productoListaSegmentoList) {
        this.productoListaSegmentoList = productoListaSegmentoList;
    }

   
    
    @XmlTransient
    public List<ProductoAtributo> getProductoAtributoList() {
        return productoAtributoList;
    }

    public void setProductoAtributoList(List<ProductoAtributo> productoAtributoList) {
        this.productoAtributoList = productoAtributoList;
    }

    

    public Ubicacion getIdUbicacion() {
        return idUbicacion;
    }

    public void setIdUbicacion(Ubicacion idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    
    public BigInteger getCantMinMetrica() {
        return cantMinMetrica;
    }

    public void setCantMinMetrica(BigInteger cantMinMetrica) {
        this.cantMinMetrica = cantMinMetrica;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }

    public NivelMetrica getIdNivelMinimo() {
        return idNivelMinimo;
    }

    public void setIdNivelMinimo(NivelMetrica idNivelMinimo) {
        this.idNivelMinimo = idNivelMinimo;
    }

            
}
