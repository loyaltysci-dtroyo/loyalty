package com.flecharoja.loyalty.storm.functions;

import com.flecharoja.loyalty.storm.model.ReglaAvanzada;
import com.flecharoja.loyalty.storm.model.ReglaMetricaBalance;
import com.flecharoja.loyalty.storm.model.ReglaMetricaCambio;
import com.flecharoja.loyalty.storm.model.ReglaMiembroAtb;
import com.flecharoja.loyalty.storm.model.TablaPosiciones;
import com.flecharoja.loyalty.storm.service.InsigniasService;
import com.flecharoja.loyalty.storm.service.LeaderboardService;
import com.flecharoja.loyalty.storm.service.MiembroService;
import com.flecharoja.loyalty.storm.service.ReglaService;
import com.flecharoja.loyalty.storm.service.SegmentoService;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.storm.topology.ReportedFailedException;
import org.apache.storm.trident.operation.BaseFunction;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.operation.TridentOperationContext;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;
import org.hibernate.exception.JDBCConnectionException;

/**
 * Flecha Roja Technologies Funcion encargada de verificar las reglas que
 * deberan ser actualizadas, para lo cual verifica los registros de reglas de
 * segmento activo
 *
 * @author faguilar
 */
public class ActualizadorReglasFunction extends BaseFunction {

    private List<ReglaMiembroAtb> listaReglasMiembroAtributo;
    private List<ReglaMetricaBalance> listaReglasMetricaBalance;
    private List<ReglaMetricaCambio> listaReglasMetricaCambio;
    private List<ReglaAvanzada> listaReglasAvanzadas;
    private SegmentoService segmentoBean;
    private ReglaService reglaBean;
    private InsigniasService insigniaBean;
    private MiembroService miembroBean;
    private LeaderboardService leaderboardService;
    private final String REGLA_MIEMBRO_ATB = "C";
    private final String REGLA_METRICA_CAMBIO = "B";
    private final String REGLA_METRICA_BALANCE = "A";
    private final String REGLA_AVANZADA = "D";
    private static final Logger LOG = Logger.getLogger(ActualizadorReglasFunction.class.getName());

    /**
     * Encargado de liberar recursos previo a un shutdown de la topologia
     */
    @Override
    public void cleanup() {
        super.cleanup();
    }

    @Override
    public void prepare(Map conf, TridentOperationContext context) {
        
        super.prepare(conf, context);
        System.out.println("com.flecharoja.loyalty.storm.functions.ActualizadorReglasFunction.prepare()");
        obtenerMetadatosReglasSegmento();
    }

    /**
     *
     * REGLA AVANZADA - INDICADOR DE TIPO
     *
     * B Recibió un mensaje 
     * A Se unió al programa
     * C Abrió un mensaje 
     * D Redimió un premiO
     * E Gastó métrica
     * F Compras
     * G Última conexión
     * H Cantidad de referidos
     * I Tipo de dispositivo
     * J Rango de posiciones en leaderboard
     * K Cantidad de compras en una ubicacion 
     * L Aceptó una promoción 
     * M Vió una promoción 
     * N Compra de producto 
     * O Vió una misión 
     * P Completó una misión 
     * Q Sin aprobar una misión 
     * R Tiene una insignia 
     * S Pertenece a grupo 
     * T Pertenece a segmento
     *
     *
     */
    /**
     * Encagado de inicializar los reglaos necesarios para la operacion de esta
     * funcion, para lo cual carga la definicion de reglas para todos los
     * segmentos
     */
    private void obtenerMetadatosReglasSegmento() {
        try {
            this.leaderboardService = new LeaderboardService();
            this.segmentoBean = new SegmentoService();
            this.reglaBean = new ReglaService();
            this.insigniaBean = new InsigniasService();
            this.listaReglasMiembroAtributo = this.reglaBean.getReglasAtributoMiembroSegmentosActivos();
            this.listaReglasAvanzadas = this.reglaBean.getReglasAvanzadasSegmentosActivos();
            this.listaReglasMetricaBalance = this.reglaBean.getReglasMetricaBalanceSegmentosActivos();
            this.listaReglasMetricaCambio = this.reglaBean.getReglasMetricaCambioSegmentosActivos();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error al obtener metadatos: ", e);
//            throw new ReportedFailedException(e);
        }

    }

    /**
     * Encargado de ejecutar la logica de actualizacion de reglas basada en
     * eventos
     *
     * @param tuple tupla de trident en el formato
     * [indEvento:int,idMiembro:string,idElemento:string]
     * @param collector
     *
     * emite: tupla de trident en el formato
     * [indTipoRegla:string,idRegla:string,idMiembro:string]
     *
     *
     * los posibles valores para el atributo de "indTipoRegla", son: Atributo
     * Miembro (C) Metrica Cambio (B) Metrica Balance (A) Avanzada (D) NOTA: El
     * atributo de "idMiembro" indica que el re calculo de la regla es sobre el
     * miembro con el identificador igual al valor del atributo, este atributo
     * es opcional, ya que la ausencia del mismo se toma como una petición de re
     * calculo de la regla especificada sobre todos los miembros.
     *
     *
     */
    @Override
    public void execute(TridentTuple tuple, TridentCollector collector) {
        try{
        this.obtenerMetadatosReglasSegmento();
        switch (tuple.getInteger(0)) {
            case 0: {
                break;
            }
            case 1: {
                //Miembro entra a grupo
                this.listaReglasAvanzadas.stream()
                        .forEach((regla) -> {
                            if (regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.PERTENECEN_GRUPO_MIE.getValue() && regla.getReferencia().equals(tuple.getString(2))) {
                                collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                            }
                        });
                break;
            }
            case 2: {
                //Miembro sale de grupo
                this.listaReglasAvanzadas.stream()
                        .forEach((regla) -> {
                            if (regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.PERTENECEN_GRUPO_MIE.getValue() && regla.getReferencia().equals(tuple.getString(2))) {
                                collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                            }
                        });
                break;
            }
            case 3: {
                //Miembro se le asigna insignia
                this.listaReglasAvanzadas.stream()
                        .forEach((regla) -> {
                            if (regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.TIENE_INSIGNIA.getValue() && regla.getReferencia().equals(tuple.getString(2))) {
                                collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                            }
                        });
                break;
            }
            case 4: {
                //Miembro se le desasigna insignia
                this.listaReglasAvanzadas.stream()
                        .forEach((regla) -> {
                            if (regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.TIENE_INSIGNIA.getValue() && regla.getReferencia().equals(tuple.getString(2))) {
                                collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                            }
                        });
                break;
            }
            case 5: {
                //Miembro se le asigna a leaderboard
//                this.listaReglasAvanzadas.stream()
//                        .forEach((regla) -> {
//                            if (regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.TIENE_INSIGNIA.getValue() && regla.getReferencia().equals(tuple.getString(2))) {
//                                collector.emit(new Values(REGLA_MIEMBRO_ATB, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
//                            }
//                        });
                break;
            }
            case 6: {
                //Miembro se le desasina leaderboard

//                this.listaReglasAvanzadas.stream()
//                        .forEach((regla) -> {
//                            if (regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.TIENE_INSIGNIA.getValue() && regla.getReferencia().equals(tuple.getString(2))) {
//                                collector.emit(new Values(REGLA_MIEMBRO_ATB, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
//                            }
//                        });
                break;

            }
            case 7: {
                //Miembro gana metrica
                //al ganar metrica se recalculan las reglas de balance de metrica para la metrica
                //sobre la cual ocurre el cambio y el usuario
                this.listaReglasMetricaBalance.stream()
                        .forEach((regla) -> {
                            //si la metrica corresponde a la del evento y el balance es disponible
                            if (regla.getIdMetrica().getIdMetrica().equals(tuple.getString(2))
                                    && (regla.getIndBalance().equals(ReglaMetricaBalance.Balances.DISPONIBLE.getValue())
                                    || regla.getIndBalance().equals(ReglaMetricaBalance.Balances.ACUMULADO.getValue()))) {
                                collector.emit(new Values(REGLA_METRICA_BALANCE, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                            }
                        });
                //al redimir metrica se recalculan las reglas de cambio de metrica
                //sobre la cual ocurre el cambio y el usuario
                this.listaReglasMetricaCambio.stream()
                        .forEach((regla) -> {
                            //si la metrica de la regla corresponde a la del evento y el balance es disponible o acumulado
                            if (regla.getIdMetrica().getIdMetrica().equals(tuple.getString(2))
                                    && (regla.getIndCambio().equals(ReglaMetricaCambio.Balances.DISPONIBLE.getValue())
                                    || regla.getIndCambio().equals((ReglaMetricaCambio.Balances.ACUMULADO.getValue())))) {
                                //si la regla cumple con las restricciones de calendarizacion previo a ser enviada
                                if (regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.ANNO_ACTUAL.getValue())
                                        || regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.MES_ACTUAL.getValue())
                                        || regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.EN_EL_ULTIMO.getValue())
                                        || regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.EN_EL_ANTERIOR.getValue())) {
                                    collector.emit(new Values(REGLA_METRICA_CAMBIO, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                }
                                if (regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.DESDE.getValue())) {
                                    if (regla.getFechaInicio().getTime() <= System.currentTimeMillis()) {
                                        collector.emit(new Values(REGLA_METRICA_CAMBIO, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                    }
                                }
                                if (regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.HASTA.getValue())) {
                                    if (regla.getFechaFinal().getTime() >= System.currentTimeMillis()) {
                                        collector.emit(new Values(REGLA_METRICA_CAMBIO, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                    }
                                }
                                if (regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.ENTRE.getValue())) {
                                    if (regla.getFechaInicio().getTime() <= System.currentTimeMillis() && regla.getFechaFinal().getTime() >= System.currentTimeMillis()) {
                                        collector.emit(new Values(REGLA_METRICA_CAMBIO, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                    }
                                }
                            }
                        });
                //cuando se gana metrica se procesa el rango de posicion de leaderboards para el miembro
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getReferencia()!=null&&regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            //primero hay que obtener las reglas (si las hay) que tiene rango de posiciones de leaderboard como objetivo 
                            if (regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.POSICION_TABLA_POS.getValue()
                                    && regla.getReferencia().equals(tuple.getString(2))) {
                                TablaPosiciones leaderboard = this.leaderboardService.getLeaderboardPorId(regla.getReferencia());
                                //y que tienen la metrica del evento como metrica asociada al leaderboard 
                                if (leaderboard.getIdMetrica().getIdMetrica().equals(tuple.getStringByField("idElemento"))) {
                                    //luego de obtener el leaderboard si es tipo usuario o equipos se manda peticion
                                    if (leaderboard.getIndTipo() == TablaPosiciones.Tipos.USUARIO.getValue() || leaderboard.getIndTipo() == TablaPosiciones.Tipos.EQUIPOS.getValue()) {
                                        collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                        //si es grupal y si el miembro pertence al grupo mandar peticion
                                    } else if (leaderboard.getIndTipo() == TablaPosiciones.Tipos.GRUPAL.getValue()
                                            && miembroBean.miembroPerteneceGrupo(leaderboard.getIdGrupo().getIdGrupo(), tuple.getStringByField("idMiembro"))) {
                                        collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                    }
                                }
                            }
                        });
                break;
            }
            case 8: {
                //Miembro redime metrica
                //al redimir metrica se recalculan las reglas de balance de metrica 
                this.listaReglasMetricaBalance.stream()
                        .forEach((regla) -> {
                            //la regla debe aplicar sobre el balance redimido o disponible de la metrica que corresponde a la regla
                            if (regla.getIdMetrica().getIdMetrica().equals(tuple.getString(2))
                                    && (regla.getIndBalance().equals(ReglaMetricaBalance.Balances.DISPONIBLE.getValue())
                                    || regla.getIndBalance().equals(ReglaMetricaBalance.Balances.REDIMIDO.getValue()))) {
                                collector.emit(new Values(REGLA_METRICA_BALANCE, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                            }
                        });

                //al redimir metrica se recalculan las reglas de cambio de metrica
                //sobre la cual ocurre el cambio y el usuario
                this.listaReglasMetricaCambio.stream()
                        .forEach((regla) -> {
                            //si la metrica de la regla corresponde a la del evento y el balance es disponible o redimido
                            if (regla.getIdMetrica().getIdMetrica().equals(tuple.getString(2))
                                    && (regla.getIndCambio().equals(ReglaMetricaCambio.Balances.DISPONIBLE.getValue())
                                    || regla.getIndCambio().equals((ReglaMetricaCambio.Balances.REDIMIDO.getValue())))) {
                                //posteriormente se verifica que el la regla cumple con las restricciones de calendarizacion previo a ser enviada
                                if (regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.ANNO_ACTUAL.getValue())
                                        || regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.MES_ACTUAL.getValue())
                                        || regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.EN_EL_ULTIMO.getValue())
                                        || regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.EN_EL_ANTERIOR.getValue())) {
                                    collector.emit(new Values(REGLA_METRICA_CAMBIO, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                }
                                if (regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.DESDE.getValue())) {
                                    if (regla.getFechaInicio().getTime() <= System.currentTimeMillis()) {
                                        collector.emit(new Values(REGLA_METRICA_CAMBIO, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                    }
                                }
                                if (regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.HASTA.getValue())) {
                                    if (regla.getFechaFinal().getTime() >= System.currentTimeMillis()) {
                                        collector.emit(new Values(REGLA_METRICA_CAMBIO, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                    }
                                }
                                if (regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.ENTRE.getValue())) {
                                    if (regla.getFechaInicio().getTime() <= System.currentTimeMillis() && regla.getFechaFinal().getTime() >= System.currentTimeMillis()) {
                                        collector.emit(new Values(REGLA_METRICA_CAMBIO, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                    }
                                }
                            }
                        });
                break;
            }
            case 9: {
                //Miembro expira metrica

                //al expirar metrica se recalculan las reglas de balance de metrica para la metrica
                //sobre la cual ocurre el cambio y el usuario
                this.listaReglasMetricaBalance.stream()
                        .forEach((regla) -> {
                            //si la metrica corresponde a la del evento y el balance es disponible
                            if (regla.getIdMetrica().getIdMetrica().equals(tuple.getString(2))
                                    && (regla.getIndBalance().equals(ReglaMetricaBalance.Balances.DISPONIBLE.getValue())
                                    || regla.getIndBalance().equals(ReglaMetricaBalance.Balances.EXPIRADO.getValue())
                                    || regla.getIndBalance().equals(ReglaMetricaBalance.Balances.ACUMULADO.getValue()))) {
                                collector.emit(new Values(REGLA_METRICA_BALANCE, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                            }
                        });

                //al expirar metrica se recalculan las reglas de cambio de metrica
                //sobre la cual ocurre el cambio y el usuario
                this.listaReglasMetricaCambio.stream()
                        .forEach((regla) -> {
                            //si la metrica de la regla corresponde a la del evento y el balance es disponible o redimido
                            if (regla.getIdMetrica().getIdMetrica().equals(tuple.getString(2))
                                    && (regla.getIndCambio().equals(ReglaMetricaCambio.Balances.DISPONIBLE.getValue())
                                    || regla.getIndCambio().equals(ReglaMetricaBalance.Balances.EXPIRADO.getValue())
                                    || regla.getIndCambio().equals((ReglaMetricaCambio.Balances.ACUMULADO.getValue())))) {
                                //posteriormente se verifica que el la regla cumple con las restricciones de calendarizacion previo a ser enviada
                                if (regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.ANNO_ACTUAL.getValue())
                                        || regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.MES_ACTUAL.getValue())
                                        || regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.EN_EL_ULTIMO.getValue())
                                        || regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.EN_EL_ANTERIOR.getValue())) {
                                    collector.emit(new Values(REGLA_METRICA_CAMBIO, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                }
                                if (regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.DESDE.getValue())) {
                                    if (regla.getFechaInicio().getTime() <= System.currentTimeMillis()) {
                                        collector.emit(new Values(REGLA_METRICA_CAMBIO, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                    }
                                }
                                if (regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.HASTA.getValue())) {
                                    if (regla.getFechaFinal().getTime() >= System.currentTimeMillis()) {
                                        collector.emit(new Values(REGLA_METRICA_CAMBIO, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                    }
                                }
                                if (regla.getIndTipo().equals(ReglaMetricaCambio.Calendarizaciones.ENTRE.getValue())) {
                                    if (regla.getFechaInicio().getTime() <= System.currentTimeMillis() && regla.getFechaFinal().getTime() >= System.currentTimeMillis()) {
                                        collector.emit(new Values(REGLA_METRICA_CAMBIO, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                                    }
                                }
                            }
                        });

                break;
            }
            case 10: {
                //Miembro alcanza nivel de metrica
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.CAMBIARON_NIVEL.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 11: {
                //Miembro deja nivel de metrica
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.CAMBIARON_NIVEL.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 12: {
                //Miembro ejecuta mision
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.COMPLETARON_MISION.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 13: {
                //Miembro aprueba mision
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.GANARON_MISION.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 14: {//Miembro reprueba mision
//                throw new UnsupportedOperationException("NOT IMPLEMENTED");
//                this.listaReglasAvanzadas.stream()
//                        .forEach((regla) -> {
//                            if (regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.GANARON_MISION.getValue()
//                                    && regla.getReferencia().equals(tuple.getString(2))
//                                    && (regla.getFechaInicio().getTime() <= System.currentTimeMillis())
//                                    && (System.currentTimeMillis() <= regla.getFechaFinal().getTime())) {
//                                collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
//                            }
//                        });
//                break;
            }
            case 15: {
                //Miembro recibe notificacion
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.RECIBIERON_MENSAJE.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 16: {
                //Miembro abre notificacion
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.ABRIERON_MENSAJE.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 17: {
                //Miembro recibe premio

                break;
            }
            case 18: {
                //Miembro redime premio
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.REDIMIERON_PREMIO.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 19: {
                //Miembro compra producto
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.COMPRARON_PRODUCTO.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 20: {
                //Miembro agrega promocion a bookmarks
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.REDIMIERON_PROMOCION.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 21: {
                //Miembro entra a segmento
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.PERTENECEN_SEGMENTO.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 22: {
                //Miembro sale de segmento
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.PERTENECEN_SEGMENTO.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 23: {
                //Miembro cumple annos (reservado para disparador de tareas)
            }
            case 24: {
                //Miembro se registra en el programa
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.PERTENECEN_SEGMENTO.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 25: {
                //Miembro elimina su app
//                this.listaReglasAvanzadas.stream()
//                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.TIPO_DISPOSITIVO.getValue())
//                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
//                        .forEach((regla) -> {
//
//                                collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
//                        });
            }
            case 26: {
                //Miembro instala su app
//                this.listaReglasAvanzadas.stream()
//                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.TIPO_DISPOSITIVO.getValue())
//                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
//                        .forEach((regla) -> {
//                                collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
//                        });
            }
            case 27: {
                //Miembro vio promocion
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.VIERON_PROMOCION.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 28: {
                //Miembro actualiza perfil
                this.listaReglasMiembroAtributo.stream()
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_MIEMBRO_ATB, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            case 29: {
                //Miembro vio mision
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.VIERON_MISION.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
            //ID EVENTO CUSTOM
            case 30: {
                //reservado para procesamiento de tareas
            }
            case 31: {
                //Miembro compra producto
                this.listaReglasAvanzadas.stream()
                        .filter(regla -> regla.getIndTipoRegla() == ReglaAvanzada.TiposRegla.COMPRARON_PRODUCTO.getValue())
                        .filter(regla -> regla.getReferencia().equals(tuple.getString(2)))
                        .filter(regla -> isRangoValidoFechas(regla))
                        .forEach((regla) -> {
                            collector.emit(new Values(REGLA_AVANZADA, regla.getIdRegla(), tuple.getStringByField("idMiembro")));
                        });
                break;
            }
        }
        }catch(Exception e){
             if(e instanceof JDBCConnectionException){
                    throw new ReportedFailedException(e);
                }
            System.err.println("Error procesador de tareas " + tuple.toString());
        }
//        collector.emit(tuple);
    }

    /*
     * Encargado de validar el rango de fechas 
     * true = valido
     */
    private boolean isRangoValidoFechas(ReglaAvanzada regla) {
        if (regla.getFechaIndUltimo() != null) {
            return true;
//            if (regla.getFechaIndUltimo() == ReglaAvanzada.TiposUltimaFecha.ANO.getValue()) {
//                
//            }
//            if (regla.getFechaIndUltimo() == ReglaAvanzada.TiposUltimaFecha.DIA.getValue()) {
//
//            }
//            if (regla.getFechaIndUltimo() == ReglaAvanzada.TiposUltimaFecha.MES.getValue()) {
//
//            }
        } else {
            if (regla.getFechaInicio() != null && regla.getFechaFinal() == null) {
                return regla.getFechaInicio().getTime() <= System.currentTimeMillis();
            }
            if (regla.getFechaInicio() == null && regla.getFechaFinal() != null) {
                return regla.getFechaInicio().getTime() <= System.currentTimeMillis() && System.currentTimeMillis() <= regla.getFechaFinal().getTime();
            }
            if (regla.getFechaInicio() != null && regla.getFechaFinal() != null) {
                return regla.getFechaInicio().getTime() <= System.currentTimeMillis() && System.currentTimeMillis() <= regla.getFechaFinal().getTime();
            }
        }
        return false;
    }

}
