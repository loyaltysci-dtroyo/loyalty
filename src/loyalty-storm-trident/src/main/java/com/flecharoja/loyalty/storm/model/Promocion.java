package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "PROMOCION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Promocion.findAll", query = "SELECT p FROM Promocion p")
    ,
    @NamedQuery(name = "Promocion.getImagenArteFromPromocion", query = "SELECT p.imagenArte FROM Promocion p WHERE p.idPromocion = :idPromocion")
    ,
    @NamedQuery(name = "Promocion.countAll", query = "SELECT COUNT(p.idPromocion) FROM Promocion p")
    ,
    @NamedQuery(name = "Promocion.findByIdPromocion", query = "SELECT p FROM Promocion p WHERE p.idPromocion = :idPromocion")
    ,
    @NamedQuery(name = "Promocion.countByNombreInterno", query = "SELECT COUNT(p.idPromocion) FROM Promocion p WHERE p.nombreInterno = :nombreInterno")
    ,
    @NamedQuery(name = "Promocion.findByIndEstado", query = "SELECT p FROM Promocion p WHERE p.indEstado = :indEstado")})
public class Promocion implements Serializable {
    
    public enum Estados {
        PUBLICADO('A'),
        ARCHIVADO('B'),
        BORRADOR('C');
        
        private final char value;
        private static final Map<Character, Estados> lookup = new HashMap<>();

        private Estados(char value) {
            this.value = value;
        }
        
        static {
            for (Estados estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public char getValue() {
            return value;
        }
        
        public static Estados get (char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(generator = "promocion_uuid")
    @GenericGenerator(name = "promocion_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_PROMOCION")
    private String idPromocion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "NOMBRE")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_INTERNO")
    private String nombreInterno;

    @Size(min = 1, max = 500)
    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Size(max = 500)
    @Column(name = "TAGS")
    private String tags;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO_ACCION")
    private Character indTipoAccion;

    @Size(max = 500)
    @Column(name = "URL_OR_CODIGO")
    private String urlOrCodigo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ESTADO")
    private Character indEstado;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPublicacion;

    @Column(name = "FECHA_ARCHIVADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaArchivado;

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    @Basic(optional = false)
    @NotNull
    @Version
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "promocion")
    private List<PromocionListaMiembro> promocionListaMiembroList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "promocion")
    private List<PromocionListaUbicacion> promocionListaUbicacionList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "promocion")
    private List<PromocionCategoriaPromo> categoriaPromocionList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "promocion")
    private List<PromocionListaSegmento> promocionListaSegmentoList;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_RESPUESTA")
    private Boolean indRespuesta;

    @Column(name = "CANT_TOTAL")
    private BigInteger cantTotal;

    @Column(name = "IND_INTERVALO_TOTAL")
    private Character indIntervaloTotal;

    @Column(name = "CANT_TOTAL_MIEMBRO")
    private BigInteger cantTotalMiembro;

    @Column(name = "IND_INTERVALO_MIEMBRO")
    private Character indIntervaloMiembro;

    @Column(name = "IND_INTERVALO_RESPUESTA")
    private Character indIntervaloRespuesta;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IMAGEN_ARTE")
    @Size(max = 500)
    private String imagenArte;

    @Column(name = "ENCABEZADO_ARTE")
    @Size(max = 150)
    private String encabezadoArte;

    @Column(name = "SUBENCABEZADO_ARTE")
    @Size(max = 300)
    private String subencabezadoArte;

    @Column(name = "DETALLE_ARTE")
    @Size(max = 500)
    private String detalleArte;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_CALENDARIZACION")
    private Character indCalendarizacion;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;

    @Size(max = 7)
    @Column(name = "IND_DIA_RECURRENCIA")
    private String indDiaRecurrencia;

    @Column(name = "IND_SEMANA_RECURRENCIA")
    private BigInteger indSemanaRecurrencia;

    @OneToMany(mappedBy = "promocion")
    private List<Notificacion> notificacionList;

    public Promocion() {
    }

    public String getIdPromocion() {
        return idPromocion;
    }

    public void setIdPromocion(String idPromocion) {
        this.idPromocion = idPromocion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreInterno() {
        return nombreInterno;
    }

    public void setNombreInterno(String nombreInterno) {
        this.nombreInterno = nombreInterno;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Character getIndTipoAccion() {
        return indTipoAccion;
    }

    public void setIndTipoAccion(Character indTipoAccion) {
        this.indTipoAccion = indTipoAccion != null ? Character.toUpperCase(indTipoAccion) : null;
    }

    public String getUrlOrCodigo() {
        return urlOrCodigo;
    }

    public void setUrlOrCodigo(String urlOrCodigo) {
        this.urlOrCodigo = urlOrCodigo;
    }

    public Character getIndEstado() {
        return indEstado;
    }

    public void setIndEstado(Character indEstado) {
        this.indEstado = indEstado != null ? Character.toUpperCase(indEstado) : null;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Date getFechaArchivado() {
        return fechaArchivado;
    }

    public void setFechaArchivado(Date fechaArchivado) {
        this.fechaArchivado = fechaArchivado;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    
    @XmlTransient
    public List<PromocionListaMiembro> getPromocionListaMiembroList() {
        return promocionListaMiembroList;
    }

    public void setPromocionListaMiembroList(List<PromocionListaMiembro> promocionListaMiembroList) {
        this.promocionListaMiembroList = promocionListaMiembroList;
    }

    
    @XmlTransient
    public List<PromocionListaUbicacion> getPromocionListaUbicacionList() {
        return promocionListaUbicacionList;
    }

    public void setPromocionListaUbicacionList(List<PromocionListaUbicacion> promocionListaUbicacionList) {
        this.promocionListaUbicacionList = promocionListaUbicacionList;
    }

    
    @XmlTransient
    public List<PromocionCategoriaPromo> getCategoriaPromocionList() {
        return categoriaPromocionList;
    }

    public void setCategoriaPromocionList(List<PromocionCategoriaPromo> categoriaPromocionList) {
        this.categoriaPromocionList = categoriaPromocionList;
    }

    
    @XmlTransient
    public List<PromocionListaSegmento> getPromocionListaSegmentoList() {
        return promocionListaSegmentoList;
    }

    public void setPromocionListaSegmentoList(List<PromocionListaSegmento> promocionListaSegmentoList) {
        this.promocionListaSegmentoList = promocionListaSegmentoList;
    }

    public Boolean getIndRespuesta() {
        return indRespuesta;
    }

    public void setIndRespuesta(Boolean indRespuesta) {
        this.indRespuesta = indRespuesta;
    }

    public BigInteger getCantTotal() {
        return cantTotal;
    }

    public void setCantTotal(BigInteger cantTotal) {
        this.cantTotal = cantTotal;
    }

    public Character getIndIntervaloTotal() {
        return indIntervaloTotal;
    }

    public void setIndIntervaloTotal(Character indIntervaloTotal) {
        this.indIntervaloTotal = indIntervaloTotal != null ? Character.toUpperCase(indIntervaloTotal) : null;
    }

    public BigInteger getCantTotalMiembro() {
        return cantTotalMiembro;
    }

    public void setCantTotalMiembro(BigInteger cantTotalMiembro) {
        this.cantTotalMiembro = cantTotalMiembro;
    }

    public Character getIndIntervaloMiembro() {
        return indIntervaloMiembro;
    }

    public void setIndIntervaloMiembro(Character indIntervaloMiembro) {
        this.indIntervaloMiembro = indIntervaloMiembro != null ? Character.toUpperCase(indIntervaloMiembro) : null;
    }

    public Character getIndIntervaloRespuesta() {
        return indIntervaloRespuesta;
    }

    public void setIndIntervaloRespuesta(Character indIntervaloRespuesta) {
        this.indIntervaloRespuesta = indIntervaloRespuesta != null ? Character.toUpperCase(indIntervaloRespuesta) : null;
    }

    public String getImagenArte() {
        return imagenArte;
    }

    public void setImagenArte(String imagenArte) {
        this.imagenArte = imagenArte;
    }

    public String getEncabezadoArte() {
        return encabezadoArte;
    }

    public void setEncabezadoArte(String encabezadoArte) {
        this.encabezadoArte = encabezadoArte;
    }

    public String getSubencabezadoArte() {
        return subencabezadoArte;
    }

    public void setSubencabezadoArte(String subencabezadoArte) {
        this.subencabezadoArte = subencabezadoArte;
    }

    public String getDetalleArte() {
        return detalleArte;
    }

    public void setDetalleArte(String detalleArte) {
        this.detalleArte = detalleArte;
    }

    public Character getIndCalendarizacion() {
        return indCalendarizacion;
    }

    public void setIndCalendarizacion(Character indCalendarizacion) {
        this.indCalendarizacion = indCalendarizacion != null ? Character.toUpperCase(indCalendarizacion) : null;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getIndDiaRecurrencia() {
        return indDiaRecurrencia;
    }

    public void setIndDiaRecurrencia(String indDiaRecurrencia) {
        this.indDiaRecurrencia = indDiaRecurrencia != null ? indDiaRecurrencia.toUpperCase() : null;
    }

    public BigInteger getIndSemanaRecurrencia() {
        return indSemanaRecurrencia;
    }

    public void setIndSemanaRecurrencia(BigInteger indSemanaRecurrencia) {
        this.indSemanaRecurrencia = indSemanaRecurrencia;
    }

    
    @XmlTransient
    public List<Notificacion> getNotificacionList() {
        return notificacionList;
    }

    public void setNotificacionList(List<Notificacion> notificacionList) {
        this.notificacionList = notificacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPromocion != null ? idPromocion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Promocion)) {
            return false;
        }
        Promocion other = (Promocion) object;
        if ((this.idPromocion == null && other.idPromocion != null) || (this.idPromocion != null && !this.idPromocion.equals(other.idPromocion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.Promocion[ idPromocion=" + idPromocion + " ]";
    }

}
