/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author faguilar
 */
@Entity
@Table(name = "WORKFLOW_COMPLETADO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WorkflowCompletado.findAll", query = "SELECT w FROM WorkflowCompletado w")
    , @NamedQuery(name = "WorkflowCompletado.findByIdMiembro", query = "SELECT w FROM WorkflowCompletado w WHERE w.workflowCompletadoPK.idMiembro = :idMiembro")
    , @NamedQuery(name = "WorkflowCompletado.findByIdNodo", query = "SELECT w FROM WorkflowCompletado w WHERE w.workflowCompletadoPK.idNodo = :idNodo")
    , @NamedQuery(name = "WorkflowCompletado.findByFechaCreacion", query = "SELECT w FROM WorkflowCompletado w WHERE w.fechaCreacion = :fechaCreacion")})
public class WorkflowCompletado implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected WorkflowCompletadoPK workflowCompletadoPK;
    
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    public WorkflowCompletado() {
    }

    public WorkflowCompletado(WorkflowCompletadoPK workflowCompletadoPK) {
        this.workflowCompletadoPK = workflowCompletadoPK;
        this.fechaCreacion= new Date();
    }

    public WorkflowCompletado(String idMiembro, String idNodo) {
        this.workflowCompletadoPK = new WorkflowCompletadoPK(idMiembro, idNodo);
        this.fechaCreacion= new Date();
    }

    public WorkflowCompletadoPK getWorkflowCompletadoPK() {
        return workflowCompletadoPK;
    }

    public void setWorkflowCompletadoPK(WorkflowCompletadoPK workflowCompletadoPK) {
        this.workflowCompletadoPK = workflowCompletadoPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (workflowCompletadoPK != null ? workflowCompletadoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkflowCompletado)) {
            return false;
        }
        WorkflowCompletado other = (WorkflowCompletado) object;
        if ((this.workflowCompletadoPK == null && other.workflowCompletadoPK != null) || (this.workflowCompletadoPK != null && !this.workflowCompletadoPK.equals(other.workflowCompletadoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.storm.model.WorkflowCompletado[ workflowCompletadoPK=" + workflowCompletadoPK + " ]";
    }
    
}
