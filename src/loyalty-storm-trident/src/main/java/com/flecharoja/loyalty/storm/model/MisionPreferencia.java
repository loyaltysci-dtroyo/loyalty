package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_PREFERENCIA")
@NamedQueries({
    @NamedQuery(name = "MisionPreferencia.getPreferenciaByIdMision", query = "SELECT m.preferencia FROM MisionPreferencia m WHERE m.misionPreferenciaPK.idMision = :idMision"),
    @NamedQuery(name = "MisionPreferencia.getPreferenciaNotInByIdMision", query = "SELECT p FROM Preferencia p WHERE p.idPreferencia NOT IN (SELECT m.misionPreferenciaPK.idPreferencia FROM MisionPreferencia m WHERE m.misionPreferenciaPK.idMision = :idMision)"),
    @NamedQuery(name = "MisionPreferencia.countByIdMision", query = "SELECT COUNT(m) FROM MisionPreferencia m WHERE m.misionPreferenciaPK.idMision = :idMision")
})
public class MisionPreferencia implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MisionPreferenciaPK misionPreferenciaPK;
    
    @JoinColumn(name = "ID_PREFERENCIA", referencedColumnName = "ID_PREFERENCIA", insertable = false, updatable = false)
    @ManyToOne
    private Preferencia preferencia;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    public MisionPreferencia() {
    }
    
    public MisionPreferencia(String idMision, String idPreferencia, Date fecha, String usuario) {
        misionPreferenciaPK = new MisionPreferenciaPK(idMision, idPreferencia);
        fechaCreacion = fecha;
        usuarioCreacion = usuario;
    }

    public MisionPreferenciaPK getMisionPreferenciaPK() {
        return misionPreferenciaPK;
    }

    public void setMisionPreferenciaPK(MisionPreferenciaPK misionPreferenciaPK) {
        this.misionPreferenciaPK = misionPreferenciaPK;
    }

    public Preferencia getPreferencia() {
        return preferencia;
    }

    public void setPreferencia(Preferencia preferencia) {
        this.preferencia = preferencia;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (misionPreferenciaPK != null ? misionPreferenciaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionPreferencia)) {
            return false;
        }
        MisionPreferencia other = (MisionPreferencia) object;
        if ((this.misionPreferenciaPK == null && other.misionPreferenciaPK != null) || (this.misionPreferenciaPK != null && !this.misionPreferenciaPK.equals(other.misionPreferenciaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionPreferencia[ misionPreferenciaPK=" + misionPreferenciaPK + " ]";
    }
    
}
