package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.model.Miembro;
import com.flecharoja.loyalty.storm.model.Notificacion;
import com.flecharoja.loyalty.storm.model.NotificacionListaMiembro;
import com.flecharoja.loyalty.storm.util.CodigosErrores;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author faguilar
 */
public class NotificacionService {

    private static final Logger LOG = Logger.getLogger(NotificacionService.class.getName());

    private static final String RESOURCE_ID = "envio-notificacion";
    private static final String MESSAGE_API_ENDPOINT = "http://wildflyinterno:8080/loyalty-api-internal/webresources/v0/";


    public void enviarMensajeMiembro(String idMensaje, String idMiembro) throws ErrorSistemaException {
//        this.assignMiembroNotificacion(idMensaje, idMiembro, idMiembro);
        this.sendMessage(idMensaje, idMiembro);
    }

    /**
     * Metodo que asigna un miembro a una notificacion
     *
     * @param idNotificacion Identificador de la notificacion
     * @param idMiembro Identificador del miembro
     * @param usuarioCreacion Identificador del usuario creador
     */
    public void assignMiembroNotificacion(String idNotificacion, String idMiembro, String usuarioCreacion) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Notificacion notificacion = em.find(Notificacion.class, idNotificacion);// notificacion almacenada
        if (notificacion == null) {//si no se pudo encontrar la notificacion...
            LOG.log(Level.SEVERE, "La Notificacion es nula", new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA));

        }
        em.getReference(Miembro.class, idMiembro).getIdMiembro();//se busca la referencia de un miembro y se detona su busqueda

        //si la notificacion tiene un estado de archivado o ejecutado...
        if (notificacion.getIndEstado().equals(Notificacion.Estados.ARCHIVADO.getValue()) || notificacion.getIndEstado().equals(Notificacion.Estados.EJECUTADO.getValue())) {
            LOG.log(Level.SEVERE, "La Notificacion es archivada o ejecutada", new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA));
        }

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        //se registra la asignacion y la accion en la bitacora
        em.persist(new NotificacionListaMiembro(idNotificacion, idMiembro, Calendar.getInstance().getTime(), usuarioCreacion));

        em.flush();
        em.getTransaction().commit();
        em.clear();
        em.close();
    }

    private void sendMessage(String idMensaje, String idMiembro) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(MESSAGE_API_ENDPOINT).path(RESOURCE_ID);
        
        String json="{\"idNotificacion\":\""+idMensaje+"\",\"miembros\":[\""+idMiembro+"\"]}";
        LOG.log(Level.INFO, "Enviando mensaje al API de mensajes con el siguiente valor:{0}", json);
        Response post = target.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));
        em.close();
        if (post.getStatus() < 200 ||post.getStatus() > 299) {
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
    }

}
