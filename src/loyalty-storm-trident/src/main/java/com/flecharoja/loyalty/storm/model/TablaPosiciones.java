/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.model;

import static com.flecharoja.loyalty.storm.model.Promocion.Estados.values;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author wtencio
 */
@Entity
@Table(name = "TABLA_POSICIONES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TablaPosiciones.findAll", query = "SELECT t FROM TablaPosiciones t"),
    @NamedQuery(name = "TablaPosiciones.countAll", query = "SELECT COUNT(t.idTabla) FROM TablaPosiciones t"),
    @NamedQuery(name = "TablaPosiciones.findByIdTabla", query = "SELECT t FROM TablaPosiciones t WHERE t.idTabla = :idTabla"),
    @NamedQuery(name = "TablaPosiciones.findByNombre", query = "SELECT t FROM TablaPosiciones t WHERE t.nombre = :nombre"),
    @NamedQuery(name = "TablaPosiciones.findByIndTipo", query = "SELECT t FROM TablaPosiciones t WHERE t.indTipo = :indTipo"),
    @NamedQuery(name = "TablaPosiciones.findByIndFormato", query = "SELECT t FROM TablaPosiciones t WHERE t.indFormato = :indFormato"),
    @NamedQuery(name = "TablaPosiciones.findByDescripcion", query = "SELECT t FROM TablaPosiciones t WHERE t.descripcion = :descripcion"),
    @NamedQuery(name = "TablaPosiciones.findByIndGrupalFormaCalculo", query = "SELECT t FROM TablaPosiciones t WHERE t.indGrupalFormaCalculo = :indGrupalFormaCalculo"),
    @NamedQuery(name = "TablaPosiciones.getImagenFromTabla", query = "SELECT t.imagen FROM TablaPosiciones t WHERE t.idTabla = :idTabla")
})
public class TablaPosiciones implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(generator = "tablaposiciones_uuid")
    @GenericGenerator(name = "tablaposiciones_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_TABLA")
    private String idTabla;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_TIPO")
    private Character indTipo;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "IMAGEN")
    private String imagen;

    @Size(min = 1, max = 500)
    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "IND_GRUPAL_FORMA_CALCULO")
    private Character indGrupalFormaCalculo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 36, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "IND_FORMATO")
    private String indFormato;

    @Version
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_VERSION")
    private Long numVersion;

    @JoinColumn(name = "ID_GRUPO", referencedColumnName = "ID_GRUPO")
    @ManyToOne(optional = false)
    private Grupo idGrupo;

    @JoinColumn(name = "ID_METRICA", referencedColumnName = "ID_METRICA")
    @ManyToOne(optional = false)
    private Metrica idMetrica;
    
    public enum Tipos {
        USUARIO('A'),
        GRUPAL('B'),
        EQUIPOS('C');
        
        private final char value;
        private static final Map<Character, Tipos> lookup = new HashMap<>();

        private Tipos(char value) {
            this.value = value;
        }
        
        static {
            for (Tipos estado : values()) {
                lookup.put(estado.value, estado);
            }
        }
        
        public char getValue() {
            return value;
        }
        
        public static Tipos get (char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }
    
    public TablaPosiciones() {
    }

    public TablaPosiciones(Date fechaCreacion, String usuarioCreacion, Date fechaModificacion, String usuarioModificacion, Long numVersion) {
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioModificacion = usuarioModificacion;
        this.numVersion = numVersion;
    }

    public String getIdTabla() {
        return idTabla;
    }

    public void setIdTabla(String idTabla) {
        this.idTabla = idTabla;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Character getIndTipo() {
        return indTipo;
    }

    public void setIndTipo(Character indTipo) {
        this.indTipo = indTipo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }


    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getIndGrupalFormaCalculo() {
        return indGrupalFormaCalculo;
    }

    public void setIndGrupalFormaCalculo(Character indGrupalFormaCalculo) {
        this.indGrupalFormaCalculo = indGrupalFormaCalculo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }
    
    public String getIndFormato() {
        return indFormato;
    }

    public void setIndFormato(String indFormato) {
        this.indFormato = indFormato;
    }

    public Grupo getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Grupo idGrupo) {
        this.idGrupo = idGrupo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTabla != null ? idTabla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TablaPosiciones)) {
            return false;
        }
        TablaPosiciones other = (TablaPosiciones) object;
        if ((this.idTabla == null && other.idTabla != null) || (this.idTabla != null && !this.idTabla.equals(other.idTabla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TablaPosiciones{" + "idTabla=" + idTabla + ", nombre=" + nombre + ", indTipo=" + indTipo + ", imagen=" + imagen + ", descripcion=" + descripcion + ", indGrupalFormaCalculo=" + indGrupalFormaCalculo + ", fechaCreacion=" + fechaCreacion + ", usuarioCreacion=" + usuarioCreacion + ", fechaModificacion=" + fechaModificacion + ", usuarioModificacion=" + usuarioModificacion + ", indFormato=" + indFormato + ", numVersion=" + numVersion + ", idGrupo=" + idGrupo + ", idMetrica=" + idMetrica + '}';
    }

   

    

}
