package com.flecharoja.loyalty.storm.model;


import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "MISION_PERFIL_ATRIBUTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MisionPerfilAtributo.findAllByIdMision", query = "SELECT m FROM MisionPerfilAtributo m WHERE m.idMision.idMision = :idMision"),
    @NamedQuery(name = "MisionPerfilAtributo.countAllByIdMision", query = "SELECT COUNT(m) FROM MisionPerfilAtributo m WHERE m.idMision.idMision = :idMision"),
    @NamedQuery(name = "MisionPerfilAtributo.countAllByIdMisionIndAtributo", query = "SELECT COUNT(m) FROM MisionPerfilAtributo m WHERE m.idMision.idMision = :idMision AND m.indAtributo = :indAtributo"),
    @NamedQuery(name = "MisionPerfilAtributo.countAllByIdMisionIdAtributoDinamico", query = "SELECT COUNT(m) FROM MisionPerfilAtributo m WHERE m.idMision.idMision = :idMision AND m.idAtributoDinamico.idAtributo = :idAtributoDinamico")
})
public class MisionPerfilAtributo implements Serializable {
    
    public enum Atributos {
        FECHA_NACIMIENTO('A'),
        IND_GENERO('B'),
        IND_ESTADO_CIVIL('C'),
        FRECUENCIA_COMPRA('D'),
        NOMBRE('E'),
        APELLIDO('F'),
        CIUDAD_RESIDENCIA('K'),
        ESTADO_RESIDENCIA('L'),
        PAIS_RESIDENCIA('M'),
        CODIGO_POSTAL('N'),
        IND_EDUCACION('O'),
        INGRESO_ECONOMICO('P'),
        IND_HIJOS('Q'),
        ATRIBUTO_DINAMICO('R'),
        APELLIDO2('S');
        
        private final char value;
        private static final Map<Character, Atributos> lookup = new HashMap<>();

        private Atributos(char value) {
            this.value = value;
        }
        
        static {
            for (Atributos atributo : values()) {
                lookup.put(atributo.value, atributo);
            }
        }

        public char getValue() {
            return value;
        }
        
        public static Atributos get (char value) {
            return value==Character.MIN_VALUE?null:lookup.get(value);
        }
    }

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "mision_perfil_atributo_uuid")
    @GenericGenerator(name = "mision_perfil_atributo_uuid", strategy = "uuid2")
    @Size(min = 36, max = 40)
    @Column(name = "ID_ATRIBUTO")
    private String idAtributo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_REQUERIDO")
    private Boolean indRequerido;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IND_ATRIBUTO")
    private Character indAtributo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    
    @Version
    @Column(name = "NUM_VERSION")
    private Long numVersion;
    
    @JoinColumn(name = "ID_ATRIBUTO_DINAMICO", referencedColumnName = "ID_ATRIBUTO")
    @ManyToOne
    private AtributoDinamico idAtributoDinamico;
    
    @JoinColumn(name = "ID_MISION", referencedColumnName = "ID_MISION")
    @ManyToOne(optional = false)
    private Mision idMision;

    public MisionPerfilAtributo() {
    }

    public String getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public Boolean getIndRequerido() {
        return indRequerido;
    }

    public void setIndRequerido(Boolean indRequerido) {
        this.indRequerido = indRequerido;
    }

    public Character getIndAtributo() {
        return indAtributo;
    }

    public void setIndAtributo(Character indAtributo) {
        this.indAtributo = indAtributo == null ? null : Character.toUpperCase(indAtributo);
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }
    
    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Long getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    public AtributoDinamico getIdAtributoDinamico() {
        return idAtributoDinamico;
    }

    public void setIdAtributoDinamico(AtributoDinamico idAtributoDinamico) {
        this.idAtributoDinamico = idAtributoDinamico;
    }

    
    @XmlTransient
    public Mision getIdMision() {
        return idMision;
    }

    public void setIdMision(Mision idMision) {
        this.idMision = idMision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtributo != null ? idAtributo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MisionPerfilAtributo)) {
            return false;
        }
        MisionPerfilAtributo other = (MisionPerfilAtributo) object;
        if ((this.idAtributo == null && other.idAtributo != null) || (this.idAtributo != null && !this.idAtributo.equals(other.idAtributo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.MisionPerfilAtributo[ idAtributo=" + idAtributo + " ]";
    }
    
}
