/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.model.TransaccionCompra;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;

/**
 *
 * @author faguilar
 */
public class TransaccionesService {

    private static final Logger LOG = Logger.getLogger(PremioService.class.getName());

    public List getTransacciones(String idMiembro) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultList = em.createNamedQuery("TransaccionCompra.findByIdMiembro").setParameter("idMiembro", idMiembro).getResultList();
        em.close();
        return resultList;
    }

    public TransaccionCompra getTransaccion(String idTransaccion) {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        TransaccionCompra find = em.find(TransaccionCompra.class, idTransaccion);
        em.close();
        return find;
    }
}
