package com.flecharoja.loyalty.storm.service;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.exception.RecursoNoEncontradoException;
import com.flecharoja.loyalty.storm.model.Segmento;
import com.flecharoja.loyalty.storm.util.CodigosErrores;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * EJB encargado de ejecutar reglas de negocio y acciones de persistencia sobre
 * segmentos
 *
 * @author faguilar
 */
@Stateless
public class SegmentoService {

    /**
     * Metodo que obtiene una lista de segmentos almacenados por estado(s) en un
     * rango de registros
     *
     * @param estado Valores de los indicadores de estado a buscar
     * @return Respuesta con una lista de segmentos
     */
    public List<Segmento> getSegmentos(String estado) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List<Segmento> resultado = null;
        try {
            //recuperacion de la lista de segmentos en un rango valido
            resultado = em.createNamedQuery("Segmento.findByIndEstado").setParameter("indEstado", estado).getResultList();
        } catch (IllegalArgumentException e) {//paginacion erronea
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        }
        em.close();
        return resultado;
    }

    /**
     * Metodo que obtiene un segmento por su numero de identificacion
     *
     * @param idSegmento Identificacion del segmento
     * @return Respuesta con la informacion del segmento
     */
    public Segmento getSegmentoPorId(String idSegmento) throws RecursoNoEncontradoException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        Segmento segmento = em.find(Segmento.class, idSegmento);
        //verificacion que el segmento exista
        if (segmento == null) {
            throw new RecursoNoEncontradoException();
        }
        em.close();
        return segmento;
    }

    /**
     * Metodo que realiza una busqueda en base a palabras claves sobre una serie
     * de atributos, opcionalmente por estados y en un rango de registros
     *
     * @param busqueda Cadena de texto con las palabras claves
     * @param estados Indicador de los estados de los segmentos deseados
     * @return Listado de todos los segmentos encontrados
     */
    public List<Segmento> searchSegmentos(String busqueda, String estados) throws ErrorSistemaException {
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        List resultado;
        String[] palabrasClaves = busqueda.split(" ");
        try {
            //definicion del query
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Object> query = cb.createQuery();
            Root<Segmento> root = query.from(Segmento.class);

            //definicion de los predicados de comparacion de las palabras claves con un numero de columnas seleccionadas
            List<Predicate> predicates = new ArrayList<>();
            for (String palabraClave : palabrasClaves) {
                predicates.add(cb.like(cb.upper(root.get("nombre")), "%" + palabraClave.toUpperCase() + "%"));
                predicates.add(cb.like(cb.upper(root.get("nombreDespliegue")), "%" + palabraClave.toUpperCase() + "%"));
                predicates.add(cb.like(cb.upper(root.get("descripcion")), "%" + palabraClave.toUpperCase() + "%"));
                predicates.add(cb.like(cb.upper(root.get("tags")), "%" + palabraClave.toUpperCase() + "%"));
            }
            //establecimiento de la clausula WHERE
            if (estados == null) {//en el caso de que no se hayan pasado algun estado, se ignora el filtrado por los mismos
                query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));
            } else {
                //definicion de predicado adicional para el filtrado por estados
                List<Predicate> predicates2 = new ArrayList<>();
                for (String e : estados.split("-")) {
                    predicates2.add(cb.equal(root.get("indEstado"), e.toUpperCase()));
                }
                query.where(cb.or(predicates2.toArray(new Predicate[predicates2.size()])), cb.or(predicates.toArray(new Predicate[predicates.size()])));
            }

            //recuperacion de la lista de resultados en un rango valido
            resultado = em.createQuery(query.select(root)).getResultList();
        } catch (IllegalArgumentException e) {//paginacion erronea
            throw new ErrorSistemaException(CodigosErrores.Sistema.OPERACION_FALLIDA);
        } finally {
            em.close();
        }
        return resultado;
    }
}
