package com.flecharoja.loyalty.storm.functions;

import com.flecharoja.loyalty.storm.exception.ErrorSistemaException;
import com.flecharoja.loyalty.storm.exception.RecursoNoEncontradoException;
import com.flecharoja.loyalty.storm.model.WorkflowNodo;
import com.flecharoja.loyalty.storm.service.HBaseService;
import com.flecharoja.loyalty.storm.service.MisionService;
import com.flecharoja.loyalty.storm.service.NodoService;
import com.flecharoja.loyalty.storm.service.NotificacionService;
import com.flecharoja.loyalty.storm.service.PremioService;
import com.flecharoja.loyalty.storm.service.PromocionService;
import com.flecharoja.loyalty.storm.service.InsigniasService;
import com.flecharoja.loyalty.storm.service.MetricaService;
import com.flecharoja.loyalty.storm.service.WorkflowService;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.storm.topology.ReportedFailedException;
import org.apache.storm.trident.operation.BaseFunction;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.operation.TridentOperationContext;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;
import org.hibernate.exception.JDBCConnectionException;

/**
 *
 * Funcion encargada de procesar las tareas, dada una tupla de ejecucion de
 * tarea el algoritmo determina el procedimiento a ejecutar, una vez se ejecuta
 * el proce dimiento respectivo, se consulta el elemento siguiente en el flujo
 * de trabajo y se envia un nuevo mensaje al mismo topic en el que el escucha
 *
 * @author faguilar
 */
public class ProcesadorTareasFunction extends BaseFunction {

    private WorkflowNodo nodoActual;
    private NodoService nodoService;
    private MisionService misionService;
    private NotificacionService notificacionService;
    private PromocionService promocionService;
    private PremioService premioService;
    private InsigniasService insigniaService;
    private WorkflowService workflowService;
    private MetricaService metricaService;
    private HBaseService hBaseService;

    private final char ENVIAR_PROMO_ACC = 'A';
    private final char ENVIAR_MENSAJE_ACC = 'B';
    private final char ASIGNAR_INSINIA_ACC = 'C';
    private final char ASIGNAR_PREMIO_ACC = 'D';
    private final char ENVIAR_MISION_ACC = 'E';
    private final char ASIGNAR_METRICA_ACC = 'F';

    private static final Logger LOG = Logger.getLogger(ProcesadorTareasFunction.class.getName());

    @Override
    public void prepare(Map conf, TridentOperationContext context) {
        super.prepare(conf, context);
        this.nodoService = new NodoService();
        this.notificacionService = new NotificacionService();
        this.misionService = new MisionService();
        this.premioService = new PremioService();
        this.insigniaService = new InsigniasService();
        this.promocionService = new PromocionService();
        this.nodoActual = new WorkflowNodo();
        this.workflowService = new WorkflowService();
        this.hBaseService = new HBaseService();
    }

    private void enviarPromo(WorkflowNodo nodo, String idMiembro) throws ErrorSistemaException {
        this.promocionService.agregarMiembroElegibles(nodo.getReferencia(), idMiembro);
    }

    private void asignarInsignia(WorkflowNodo nodo, String idMiembro) throws ErrorSistemaException, RecursoNoEncontradoException {
        this.insigniaService.asignarInsignia(nodo.getReferencia(), idMiembro);
    }

    private void enviarMensaje(WorkflowNodo nodo, String idMiembro) {
        try {
            this.notificacionService.enviarMensajeMiembro(nodo.getReferencia(), idMiembro);
        } catch (ErrorSistemaException ex) {
            LOG.log(Level.SEVERE, "Imposible enviar mensaje a idMiembro:{0} idMensaje:{1}", new Object[]{idMiembro, nodo.getReferencia()});
        }
    }

    private void asignarPremio(WorkflowNodo nodo, String idMiembro) throws ErrorSistemaException {
        this.premioService.redimirPremio(nodo.getReferencia(), idMiembro);
    }

    private void enviarMison(WorkflowNodo nodo, String idMiembro) throws ErrorSistemaException {
        this.misionService.agregarMiembroElegibles(nodo.getReferencia(), idMiembro);
    }

    /**
     * Almacena una mision en una tabla temporal, para esperar que el usuario
     * apruebe la mision y proseguir con el flujo de accciones
     *
     * Si existe el registro lo actualiza
     */
    private void almacenarNodoTemporalmente(String idNodo, String idMiembro) throws ErrorSistemaException {
        this.workflowService.almacenarTareaWorkflow(idNodo, idMiembro);
    }

    /**
     * Asigna metrica a un membro como resultado de una tarea de asignar metrica
     *
     * @param nodo
     * @param idMiembro
     */
    private void asignarMetrica(WorkflowNodo nodo, String idMiembro) throws ErrorSistemaException {
        this.metricaService.otorgarMetica(idMiembro, nodo.getReferencia(), nodo.getCantReferencia());
    }

    /*
        Estructura de la tupla
        [idNodo,idMiembro]
     */
    @Override
    public void execute(TridentTuple tuple, TridentCollector collector) {
        System.out.println("com.flecharoja.loyalty.storm.functions.ProcesadorTareasFunction.execute()");
        try {
            System.out.println("Procesando nodo: " + tuple.toString());
            this.nodoActual = nodoService.getNodo(tuple.getStringByField("idNodo"));
            switch (nodoActual.getIndTipoTarea()) {
                case ENVIAR_PROMO_ACC: {
                    enviarPromo(nodoActual, tuple.getStringByField("idMiembro"));
                    break;
                }
                case ASIGNAR_INSINIA_ACC: {
                    asignarInsignia(nodoActual, tuple.getStringByField("idMiembro"));
                    break;
                }
                case ENVIAR_MENSAJE_ACC: {
                    enviarMensaje(nodoActual, tuple.getStringByField("idMiembro"));
                    break;
                }
                case ASIGNAR_PREMIO_ACC: {
                    asignarPremio(nodoActual, tuple.getStringByField("idMiembro"));
                    break;
                }
                case ENVIAR_MISION_ACC: {
                    enviarMison(nodoActual, tuple.getStringByField("idMiembro"));
                    break;
                }
                case ASIGNAR_METRICA_ACC: {
                    asignarMetrica(nodoActual, tuple.getStringByField("idMiembro"));
                    break;
                }
            }
            //emite una tupla 
            if (nodoActual.getIdSucesor() != null && !nodoActual.getIsNodoMisionEspec()) {
                System.out.println("Emitiendo nodo: " + tuple.toString());
                collector.emit(new Values(nodoActual.getIdSucesor().getIdNodo(), tuple.getStringByField("idMiembro")));
            } else if (nodoActual.getIdSucesor() != null && nodoActual.getIndTipoTarea().equals(ENVIAR_MISION_ACC) && nodoActual.getIsNodoMisionEspec()) {
                WorkflowNodo nodoAlmacenar = nodoActual;
                //mientras el miembro haya completado las misiones de la secuencia, se actualiza el nodo a almacenar
                while (nodoAlmacenar != null && hBaseService.isMisionCompletada(nodoAlmacenar.getReferencia(), tuple.getStringByField("idMiembro"))) {
                    // si ya completo la mision se prosigue con el nodo sucesor
                    nodoAlmacenar = nodoAlmacenar.getIdSucesor();
                }
                if (nodoAlmacenar != null) {
                    LOG.log(Level.SEVERE, "almacenarNodoTemporalmente", nodoActual.toString());
                    almacenarNodoTemporalmente(nodoAlmacenar.getIdNodo(), tuple.getStringByField("idMiembro"));
                    this.nodoService.eliminarNodoEspera(nodoActual);
                }
            }
        } catch (Exception e) {
            if (e instanceof JDBCConnectionException) {
                throw new ReportedFailedException(e);
            }
            LOG.log(Level.SEVERE, "Error procesador de tareas {0}", tuple.toString());
            LOG.log(Level.SEVERE, "Error en el procesador de tareas procesando la tupla: " + tuple.toString(), e);
        }
    }
}
