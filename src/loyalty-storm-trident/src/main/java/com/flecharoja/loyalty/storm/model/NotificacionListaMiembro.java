package com.flecharoja.loyalty.storm.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svargas
 */
@Entity
@Table(name = "NOTIFICACION_LISTA_MIEMBRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotificacionListaMiembro.findAll", query = "SELECT n FROM NotificacionListaMiembro n"),
    @NamedQuery(name = "NotificacionListaMiembro.findMiembrosByIdNotificacion", query = "SELECT n.miembro FROM NotificacionListaMiembro n WHERE n.notificacionListaMiembroPK.idNotificacion = :idNotificacion"),
    @NamedQuery(name = "NotificacionListaMiembro.findMiembrosNotInNotificacionByIdNotificacion", query = "SELECT m FROM Miembro m WHERE m.idMiembro NOT IN (SELECT n.notificacionListaMiembroPK.idMiembro FROM NotificacionListaMiembro n WHERE n.notificacionListaMiembroPK.idNotificacion = :idNotificacion)"),
    @NamedQuery(name = "NotificacionListaMiembro.countAll", query = "SELECT COUNT(n) FROM NotificacionListaMiembro n"),
    @NamedQuery(name = "NotificacionListaMiembro.countMiembrosByIdNotificacion", query = "SELECT COUNT(n) FROM NotificacionListaMiembro n WHERE n.notificacionListaMiembroPK.idNotificacion = :idNotificacion"),
    @NamedQuery(name = "NotificacionListaMiembro.countMiembrosNotInNotificacionByIdNotificacion", query = "SELECT COUNT(m) FROM Miembro m WHERE m.idMiembro NOT IN (SELECT n.notificacionListaMiembroPK.idMiembro FROM NotificacionListaMiembro n WHERE n.notificacionListaMiembroPK.idNotificacion = :idNotificacion)")
})
public class NotificacionListaMiembro implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected NotificacionListaMiembroPK notificacionListaMiembroPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    
    @JoinColumn(name = "ID_MIEMBRO", referencedColumnName = "ID_MIEMBRO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Miembro miembro;
    
    @JoinColumn(name = "ID_NOTIFICACION", referencedColumnName = "ID_NOTIFICACION", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Notificacion notificacion;

    public NotificacionListaMiembro() {
    }

    public NotificacionListaMiembro(String idNotificacion, String idMiembro, Date fechaCreacion, String usuarioCreacion) {
        this.notificacionListaMiembroPK = new NotificacionListaMiembroPK(idNotificacion, idMiembro);
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
    }

    public NotificacionListaMiembroPK getNotificacionListaMiembroPK() {
        return notificacionListaMiembroPK;
    }

    public void setNotificacionListaMiembroPK(NotificacionListaMiembroPK notificacionListaMiembroPK) {
        this.notificacionListaMiembroPK = notificacionListaMiembroPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public Notificacion getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(Notificacion notificacion) {
        this.notificacion = notificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notificacionListaMiembroPK != null ? notificacionListaMiembroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificacionListaMiembro)) {
            return false;
        }
        NotificacionListaMiembro other = (NotificacionListaMiembro) object;
        if ((this.notificacionListaMiembroPK == null && other.notificacionListaMiembroPK != null) || (this.notificacionListaMiembroPK != null && !this.notificacionListaMiembroPK.equals(other.notificacionListaMiembroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flecharoja.loyalty.model.NotificacionListaMiembro[ notificacionListaMiembroPK=" + notificacionListaMiembroPK + " ]";
    }
    
}
