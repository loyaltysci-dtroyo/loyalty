import { LoyaltyAdminPage } from './app.po';

describe('loyalty-admin App', function() {
  let page: LoyaltyAdminPage;

  beforeEach(() => {
    page = new LoyaltyAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
