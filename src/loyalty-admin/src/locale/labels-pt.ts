import { Injectable } from '@angular/core';

@Injectable()
export class LabelsPT {
  data: any;
  constructor() {
    this.data = {
      "breadcrumbs": {
        "dashboard": "Dashboard",
        "usuarios": "Usuários",
        "insertarUsuario": "Adicionar Usuário",
        "listaUsuarios": "Lista de usuários",
        "detalleUsuario": "Detalhe do usuário",
        "detalleAtributos": "Detalhe do atributos",
        "detalleRoles": "Detalhe de Funções",
        "insertarRoles": "Adicionar Função",
        "listaRoles": "Lista de Funções",
        "detalleRol": "Detalhe da Função",
        "tipoMiembro": "Cliente",
        "miembros": "Clientes",
        "insertarMiembro": "Adicionar Cliente",
        "detalleMiembro": "Detalhe de Cliente",
        "perfil": "Perfil",
        "niveles": "Níveis",
        "grupos": "Grupos",
        "atributosDinamicos": "Atributos Dinâmicos",
        "insignias": "Badges",
        "preferencias": "Preferências",
        "segmentos": "Segmentos",
        "redenciones": "Resgates",
        "premios": "Prêmios",
        "retos": "Desafios",
        "editarMiembro": "Editar membro",
        "listaMiembros": "Clientes",
        "ubicaciones": "Locais",
        "listaUbicaciones": "Lista de Locais",
        "insertarUbicacion": "Adicionar Local",
        "detalleUbicacion": "Detalhes do Local",
        "efectividad": "Efetividade da localização",
        "mapa": "Mapa",
        "metricas": "Métricas",
        "insertarMetrica": "Adicionar Métrica",
        "metricaLista": "Lista de Métricas",
        "metricaDetalle": "Detalhe da Métrica",
        "detalleNivel": "Detalhe de Níveis",
        "gruposNivel ": "Grupos de Níveis",
        "detalle": "Detalhe do Grupos de Níveis",
        "leaderboards": "Quadro Classificatório",
        "listaLeaderboards": "Lista do Quadro Classificatório",
        "detalleLeaderboard": "Detalhe do Quadro Classificatório",
        "insertarLeaderboards": "Adicionar Quadro Classificatório",
        "insertarSegmento": "Adicionar segmento",
        "listaSegmentos": "Lista de segmentos",
        "detalleSegmento": "Detalhe de segmento",
        "atributos": "Detalhe do atibuto",
        "reglas": "Lista da Regras",
        "detalleReglas": "Detalhe da Regra",
        "elegibles": "Elegibilidade",
        "promociones": "Oferta",
        "listaPromos": "Lista de Ofertas",
        "insertarPromo": "Adicionar Oferta",
        "insertarCategoria": "Adicionar Categoria",
        "listaCategorias": "Lista de Categorias",
        "detallePromo": "Detalhe de Oferta",
        "common": "Atributos",
        "arte": "Arte",
        "alcance": "Performance",
        "limites": "Limites",
        "categorias": "Categorias",
        "detalleCategorias": "Promociones por categoría",
        "misiones": "Desafios",
        "insertarMision": "Adicionar Desafio",
        "detalleMision": "Detalhe do Desafio",
        "listaMisiones": "Lista dos Desafios",
        "quien-quiere-ser-millonario": "Quien quiere ser millonario",
        "productos": "Produtos",
        "detalleProducto": "Detalhe do Produto",
        "listaProductos": "Lista da Produtos",
        "insertarProducto": "Adicionar Produtos",
        "informacion": "Geral",
        "display": "Mostrar",
        "listaCategoriasProducto": "Categorias de Produto",
        "insertarCategProducto": "Adicionar Categoria de produtos",
        "subcategorias": "Subcategoria",
        "calendarizacion": "Agendar",
        "recompensas": "Recompensas",
        "notificaciones": "Notificações",
        "listaNotificaciones": "Lista de Notificações",
        "insertarNotificaciones": "Adicionar Notificações",
        "detalleCampana": "Detalhes de Notificações",
        "insertar": "Adicionar Grupo",
        "detalleGrupo": "Detalhe do Grupo",
        "listaPreferencias": "Lista das Preferências",
        "insertarPreferencia": "Adicionar Preferências",
        "detallePreferencias": "Detalhes das Preferência",
        "listaInsignias": "Lista do Badges",
        "insertarInsignia": "Adicionar Badge",
        "detalleInsignia": "Detalhe do Badge",
        "listaNivel": "Adicionar Níveis",
        "asignarMiembro": "Atribuição de cliente",
        "listaAtributos": "Lista de Atributos",
        "detalleAtributo": "Detalhe",
        "configuracionSistema": "Configurações do Programa",
        "general": "Geral",
        "medios": "Métodos de Pagamento",
        "politica": "Políticas",
        "parametros": "Parâmetros do sistema",
        "detallePremio": "Detalhe de Prêmios",
        "requerido": "Informação Obrigatória",
        "insertarPremio": "Adicionar Prêmios",
        "redimido": "Resgatado",
        "limite": "Límite",
        "listaCategoriasPremio": "Lista de categorias",
        "detalleCategoria": "Detalhes das Categorias",
        "asignar": "Atribuir categoria",
        "certificados": "Certificados",
        "workflows": "Workflows",
        "listaWorkflow": "Lista de Workflows",
        "insertarWorkflow": "Adicionar Workflow",
        "detalleWorkflow": "Detalhe de Workflows",
        "inventario": "Inventário de Prêmios",
        "listaDocumentos": "Lista de documentos",
        "insertarDocumento": "Inserir Documento",
        "infoDocumento": "Informação de Documento",
        "proveedor": "Lista de Fornecedores",
        "dashboard-general-metrica": "Dashboard Métricas",
        "dashboard-general-mision": "Dashboard Desafios",
        "dashboard-general-notificacion": "Dashboard Notificações",
        "dashboard-general-premio": "Dashboard Prêmios",
        "dashboard-general-promocion": "Dashboard Ofertas",
        "reportes": "Relatórios",
        "historico": "Histórico",
        "compras": "Compras",
        "importacion": "Importação",
        "bitacora": "Log",
        "apiExternal": "Log - APIs Externas",
        "apiAdministrativo": "Log - APIs Admin",
        "noticia": "Notícia",
        "listaNoticias": "Listas de notícias",
        "categoriasNoticia": "Categoria de Notícias",
        "aprobacion":"Gestão das respostas"
      },
      "tabs": {},
      "menu": {
        "dashboard-miembros": "Dashboard Clientes",
        "dashboard-segmentos": "Dashboard Segmentos",
        "dashboard-programa": "Dashboard Programa",
        "dashboard-actividad": "Dashboard Atividade",
        "dashboard-metrica": "Dashboard Métrica",
        "ubicaciones": "Locais",
        "metricas": "Métricas",
        "grupos-tiers": "Grupo de Níveis",
        "adm-programa": "Configurações do Programa",
        "listado-usuarios": "Lista de usuários",
        "listado-roles": "Lista de Funções",
        "usuarios": "Usuários",
        "miembros": "Clientes",
        "listado-miembros": "Lista de Clientes",
        "preferencias": "Preferências",
        "atributos-dinamicos": "Atributos Dinâmicos",
        "grupos-miembro": "Grupo de Cliente",
        "leaderboards": "Quadro Classificatório",
        "segmentos": "Segmentos",
        "promociones": "Ofertas",
        "listado-promociones": "Lista de Ofertas",
        "actividad-promociones": "Atividade da oferta",
        "listado-categorias": "Lista de Categorias",
        "insignias": "Badges",
        "premios": "Prêmios",
        "listado-premios": "Lista de Prêmios",
        "actividad-premios": "Atividades de Prêmios",
        "categorias-premio": "Categorias de Prêmios",
        "unidades-medida": "Unidade de medida",
        "notificaciones-listado": "Lista de Notificações",
        "actividad-notificaciones": "Atividade de Notificações",
        "notificaciones": "Notificações",
        "misiones": "Desafios",
        "listado-misiones": "Lista de Desafios",
        "actividad-misiones": "Atividades de Desafio",
        "categorias-mision": "Categorias de desafio",
        "productos": "Produtos",
        "listado-productos": "Lista de Produtos",
        "categorias-productos": "Categorias de Produto",
        "atributos-producto": "Atributo do Produto",
        "workflow": "Workflow",
        "documentos-listado": "Listado de Documentos",
        "proveedores-listado": "Listado de Proveedores",
        "inventario-premio": "Inventario de Prêmios",
        "dashboard-general-metrica": "Dashboard Métricas",
        "dashboard-general-mision": "Dashboard Desafios",
        "dashboard-general-notificacion": "Dashboard Notificações",
        "dashboard-general-premio": "Dashboard Prêmios",
        "dashboard-general-promocion": "Dashboard Ofertas",
        "reportes": "Relatórios",
        "historico": "Histórico",
        "compras": "Compras",
        "importacion": "Importação",
        "bitacora": "Log",
        "apiExternal": "Log - APIs Externas",
        "apiAdministrativo": "Log - APIs Admin",
        "noticia": "Notícia",
        "listaNoticias": "Listas de notícias",
        "categoriasNoticia": "Categoria de Notícias",
        "aprobacion":"Gestão das respostas"
      },
      "general-limites-periodo": [
        {
          "value": "A",
          "label": "Sempre"
        },
        {
          "value": "B",
          "label": "Minuto"
        },
        {
          "value": "C",
          "label": "Hora"
        },
        {
          "value": "D",
          "label": "Dia"
        },
        {
          "value": "E",
          "label": "Semana"
        },
        {
          "value": "F",
          "label": "Mês"
        }
      ],
      "general-calendar": {
        "firstDayOfWeek": 0,
        "dayNames": [
          "Domingo",
          "Lunes",
          "Martes",
          "Miercoles",
          "Jueves",
          "Viernes",
          "Sabado"
        ],
        "dayNamesShort": [
          "Dom",
          "Lun",
          "Mar",
          "Mie",
          "Jue",
          "Vie",
          "Sab"
        ],
        "dayNamesMin": [
          "D",
          "L",
          "K",
          "M",
          "J",
          "V",
          "S"
        ],
        "monthNames": [
          "Enero",
          "Febrero",
          "Marzo",
          "Abril",
          "Mayo",
          "Junio",
          "Julio",
          "Agosto",
          "Septiembre",
          "Octubre",
          "Noviembre",
          "Diciembre"
        ],
        "monthNamesShort": [
          "Ene",
          "Feb",
          "Mar",
          "Abr",
          "May",
          "Jun",
          "Jul",
          "Ago",
          "Sep",
          "Oct",
          "Nov",
          "Dic"
        ]
      },
      "general-actividad-periodos": [
        {
          "label": "1 mês",
          "value": "1M"
        },
        {
          "label": "2 mêses",
          "value": "2M"
        },
        {
          "label": "3 mêses",
          "value": "3M"
        },
        {
          "label": "6 mêses",
          "value": "6M"
        },
        {
          "label": "1 ano",
          "value": "1Y"
        },
        {
          "label": "2 ano",
          "value": "2Y"
        }
      ],
      "general-itemsIndExclusion": [
        {
          "value": "I",
          "label": "Incluir"
        },
        {
          "value": "E",
          "label": "Excluir"
        }
      ],
      "segmentos": "",
      "segmentos-reglas-avanzadas": [
        {
          "value": "B",
          "label": "Recebeu uma mensagem"
        },
        {
          "value": "A",
          "label": "Entrou no programa em"
        },
        {
          "value": "C",
          "label": "Abriu uma mensagem"
        },
        {
          "value": "D",
          "label": "Resgatar um prêmio"
        },
        {
          "value": "F",
          "label": "Compras"
        },
        {
          "value": "G",
          "label": "Último login"
        },
        {
          "value": "H",
          "label": "Referências"
        },
        {
          "value": "I",
          "label": "Tipo de dispositivo"
        },
        {
          "value": "J",
          "label": "Posição no ranking"
        },
        {
          "value": "K",
          "label": "Quantidade de compras em um endereço"
        },
        {
          "value": "L",
          "label": "Aceitou uma promoção"
        },
        {
          "value": "M",
          "label": "Viu uma promoção"
        },
        {
          "value": "N",
          "label": "Compra de produto"
        },
        {
          "value": "O",
          "label": "Viu um desafio"
        },
        {
          "value": "P",
          "label": "Completou um desafio"
        },
        {
          "value": "Q",
          "label": "Ganhou um desafio"
        },
        {
          "value": "R",
          "label": "Tem um badge"
        },
        {
          "value": "S",
          "label": "Pertence a um grupo"
        },
        {
          "value": "T",
          "label": "Pertenece a um segmento"
        }
      ],
      "segmentos-listaElementosReciente": [
        {
          "value": "D",
          "label": "Dia"
        },
        {
          "value": "A",
          "label": "Ano"
        },
        {
          "value": "M",
          "label": "Mês"
        }
      ],
      "segmentos-listaDispositivos": [
        {
          "value": "A",
          "label": "Android"
        },
        {
          "value": "I",
          "label": "IOS"
        }
      ],
      "segmentos-listaOpcionesBoolean": [
        {
          "value": "true",
          "label": "Verdade"
        },
        {
          "value": "false",
          "label": "Falso"
        }
      ],
      "segmentos-listaEstadosCiviles": [
        {
          "label": "Casado",
          "value": "C"
        },
        {
          "label": "Solteiro",
          "value": "S"
        },
        {
          "label": "União livre",
          "value": "U"
        }
      ],
      "segmentos-listaEducacionMiembro": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Escola",
          "value": "P"
        },
        {
          "label": "Colegial",
          "value": "S"
        },
        {
          "label": "Tecnico",
          "value": "T"
        },
        {
          "label": "Universitaria",
          "value": "U"
        },
        {
          "label": "Grado",
          "value": "G"
        },
        {
          "label": "Pós-graduação",
          "value": "R"
        }
      ],
      "segmentos-listaEstadosMiembro": [
        {
          "value": "A",
          "label": "Ativo"
        },
        {
          "value": "I",
          "label": "Inativo"
        }
      ],
      "segmentos-listaGenerosMiembro": [
        {
          "label": "Masculino",
          "value": "M"
        },
        {
          "label": "Fêmea",
          "value": "F"
        }
      ],
      "segmentos-listaAtributos": [
        {
          "value": "B",
          "label": "Gênero"
        },
        {
          "value": "C",
          "label": "Estado civil"
        },
        {
          "value": "D",
          "label": "Frequência de compra"
        },
        {
          "value": "E",
          "label": "Nome"
        },
        {
          "value": "F",
          "label": "Nome do meio"
        },
        {
          "value": "W",
          "label": "Sobrenome"
        },
        {
          "value": "X",
          "label": "Email"
        },
        {
          "value": "H",
          "label": "Status"
        },
        {
          "value": "I",
          "label": "Nível inicial"
        },
        {
          "value": "J",
          "label": "Nível de progresso"
        },
        {
          "value": "K",
          "label": "Cidade"
        },
        {
          "value": "L",
          "label": "Estado(residência)"
        },
        {
          "value": "M",
          "label": "País"
        },
        {
          "value": "N",
          "label": "Código Postal"
        },
        {
          "value": "A",
          "label": "Data de nascimento"
        },
        {
          "value": "O",
          "label": "Educação"
        },
        {
          "value": "P",
          "label": "Faturamento"
        },
        {
          "value": "Q",
          "label": "Filhos"
        },
        {
          "value": "R",
          "label": "Atributos Dinâmicos"
        }
        /*{
          "value": "G",
          "label": "Miembro del sistema"
        }*/
      ],
      "segmentos-operadoresTexto": [
        {
          "value": "A",
          "label": "É"
        },
        {
          "value": "B",
          "label": "Não é"
        },
        {
          "value": "E",
          "label": "Começa com"
        },
        {
          "value": "F",
          "label": "Termina com"
        },
        {
          "value": "G",
          "label": "Contém"
        },
        {
          "value": "H",
          "label": "Não contém"
        },
        {
          "value": "I",
          "label": "Vazio"
        },
        {
          "value": "J",
          "label": "Não está vazio"
        }
      ],
      "segmentos-operadoresFecha": [
        {
          "value": "O",
          "label": "Antes de"
        },
        {
          "value": "P",
          "label": "A partir de"
        },
        {
          "value": "Q",
          "label": "Igual a"
        }
      ],
      "segmentos-operadoresNumericos": [
        {
          "value": "V",
          "label": "Igual"
        },
        {
          "value": "W",
          "label": "Maior que"
        },
        {
          "value": "X",
          "label": "Maior ou igual a"
        },
        {
          "value": "Y",
          "label": "Menor que"
        },
        {
          "value": "Z",
          "label": "Menor ou igual a"
        }
      ],
      "segmentos-listaBalancesMetrica": [
        {
          "value": "A",
          "label": "Acumulado"
        },
        {
          "value": "B",
          "label": "Expirado"
        },
        {
          "value": "C",
          "label": "Resgatado"
        },
        {
          "value": "D",
          "label": "Disponível"
        }
      ],
      "segmentos-listaBalancesMetricaBalance": [
        {
          "value": "B",
          "label": "Expirado"
        },
        {
          "value": "C",
          "label": "Resgatado"
        },
        {
          "value": "D",
          "label": "Disponível"
        }
      ],
      "segmentos-listaPeriodoTiempo": [
        {
          "value": "B",
          "label": "Último "
        },
        {
          "value": "C",
          "label": "Anterior"
        },
        {
          "value": "D",
          "label": "De"
        },
        {
          "value": "H",
          "label": "Para"
        },
        {
          "value": "E",
          "label": "Entre"
        },
        {
          "value": "F",
          "label": "Mês atual"
        },
        {
          "value": "G",
          "label": "Ano atual"
        }
      ],
      "segmentos-listaCalendarizacionReglasAvanzadas": [
        {
          "value": "B",
          "label": "No último"
        },
        {
          "value": "D",
          "label": "De"
        },
        {
          "value": "H",
          "label": "Para"
        },
        {
          "value": "E",
          "label": "Entre"
        },
        {
          "value": "F",
          "label": "Mês atual"
        },
        {
          "value": "G",
          "label": "Ano atual"
        }
      ], 
      "segmentos-listaTipoRegla": [
        {
          "value": "AT",
          "label": "Atributo do Membro"
        },
        {
          "value": "MB",
          "label": "Saldo de Métrica"
        },
        {
          "value": "MC",
          "label": "Mudança de Métrica"
        },
        {
          "value": "AV",
          "label": "Regra avançada"
        },
        {
          "value": "EP",
          "label": "Preferência"
        },
        {
          "value": "EE",
          "label": "Pesquisa"
        }
      ],
      "segmento-encuesta-operadores-selecc-multiple": [
        {
          "label": "Exatamente os itens selecionados",
          "value": "U"
        },
        {
          "label": "Qualquer dos itens para seleção",
          "value": "K"
        }
      ],
      "segmento-encuesta-operadores-selecc-unica": [
        {
          "label": "Qualquer dos itens para seleção",
          "value": "K"
        }
      ],
      "segmento-regla-operador-fijo": [
        {
          "value": "A",
          "label": "É"
        },
      ],
      "preguntas-respuestas": [
        {
          "label": "Respuesta 1",
          "value": "1"
        },
        {
          "label": "Respuesta 2",
          "value": "2"
        },
        {
          "label": "Respuesta 3",
          "value": "3"
        },
        {
          "label": "Respuesta 4",
          "value": "4"
        }
      ],
      "preguntas-itemsPuntos": [
        {
          "label": "100",
          "value": "100"
        },
        {
          "label": "200",
          "value": "200"
        },
        {
          "label": "300",
          "value": "300"
        },
        {
          "label": "400",
          "value": "400"
        },
        {
          "label": "500",
          "value": "500"
        },
        {
          "label": "750",
          "value": "750"
        },
        {
          "label": "1000",
          "value": "1000"
        },
        {
          "label": "1500",
          "value": "1500"
        },
        {
          "label": "2000",
          "value": "2000"
        },
        {
          "label": "3000",
          "value": "3000"
        },
        {
          "label": "5000",
          "value": "5000"
        },
        {
          "label": "7500",
          "value": "7500"
        },
        {
          "label": "10000",
          "value": "10000"
        },
        {
          "label": "15000",
          "value": "15000"
        },
        {
          "label": "30000",
          "value": "30000"
        }
      ],
      "notificaciones": [],
      "notificaciones-elegibles": [
        {
          "label": "Segmentos",
          "value": "S"
        },
        {
          "label": "Clientes",
          "value": "M"
        }
      ],
      "notificaciones-tipo": [
        {
          "value": "E",
          "label": "Email"
        },
        {
          "value": "P",
          "label": "Enviar notificações"
        }
      ],
      "notificaciones-objetivos": [
        {
          "value": null,
          "label": "Nenhum"
        },
        {
          "value": "P",
          "label": "Oferta"
        },
        {
          "value": "T",
          "label": "Tipo de prêmio"
        }
      ],
      "notificaciones-disparador": [
        {
          "value": "M",
          "label": "Manual"
        },
        {
          "value": "D",
          "label": "Enviado"
        }
      ],
      "misiones-elegibles-itemsIndComponente": [
        {
          "value": "S",
          "label": "Segmentos"
        },
        {
          "value": "M",
          "label": "Clientes"
        }
      ],
      "misiones-itemsTipoMision": [
        {
          "value": null,
          "label": "Escolha uma opção"
        },
        {
          "value": "E",
          "label": "Pesquisa"
        },
        {
          "value": "P",
          "label": "Perfil"
        },
        {
          "value": "V",
          "label": "Ver conteúdo"
        },
        {
          "value": "S",
          "label": "Subir conteúdo"
        },
        {
          "value": "R",
          "label": "Desafio social"
        },
        {
          "value": "J",
          "label": "Jogo"
        },
        {
          "value": "A",
          "label": "Preferência"
        },
        {
          "value": "B",
          "label": "Realidad Aumentada"
        }
      ],
      "misiones-itemsAprobacion": [
        {
          "value": null,
          "label": "Escolha uma opção"
        },
        {
          "value": "A",
          "label": "Automática"
        },
        {
          "value": "B",
          "label": "Manual"
        }
      ],
      "misiones-contenido-optionsTiposContenido": [
        {
          "value": null,
          "label": "Escolha uma opção"
        },
        {
          "label": "Imagem",
          "value": "I"
        },
        {
          "label": "Vídeo",
          "value": "V"
        }
      ],
      "misiones-perfil-atributos": [
        {
          "indAtributo": "A",
          "indRequerido": false,
          "label": "Data de nascimento"
        },
        {
          "indAtributo": "B",
          "indRequerido": false,
          "label": "Gênero"
        },
        {
          "indAtributo": "C",
          "indRequerido": false,
          "label": "Estado civil"
        },
        {
          "indAtributo": "D",
          "indRequerido": false,
          "label": "Frequência de compra"
        },
        {
          "indAtributo": "E",
          "indRequerido": false,
          "label": "Nome"
        },
        {
          "indAtributo": "F",
          "indRequerido": false,
          "label": "Nome do meio"
        },
        {
          "indAtributo": "S",
          "indRequerido": false,
          "label": "Sobrenome"
        },
        {
          "indAtributo": "K",
          "indRequerido": false,
          "label": "Cidade"
        },
        {
          "indAtributo": "L",
          "indRequerido": false,
          "label": "Estado(residência)"
        },
        {
          "indAtributo": "M",
          "indRequerido": false,
          "label": "País"
        },
        {
          "indAtributo": "N",
          "indRequerido": false,
          "label": "Código Postal"
        },
        {
          "indAtributo": "O",
          "indRequerido": false,
          "label": "Educação"
        },
        {
          "indAtributo": "P",
          "indRequerido": false,
          "label": "Faturamento"
        },
        {
          "indAtributo": "Q",
          "indRequerido": false,
          "label": "Filhos"
        }
      ],
      "misiones-juego-intervalos": [
        {
          "label": "Escolha um intervalo",
          "value": ""
        },
        {
          "label": "Sempre",
          "value": "A"
        },
        {
          "label": "Minuto",
          "value": "B"
        },
        {
          "label": "Hora",
          "value": "C"
        },
        {
          "label": "Dia",
          "value": "D"
        },
        {
          "label": "Semana",
          "value": "E"
        },
        {
          "label": "Mês",
          "value": "F"
        }
      ],
      "misiones-juego-itemstipo": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Roleta",
          "value": "R"
        },
        {
          "label": "Raspadinha",
          "value": "A"
        },
        {
          "label": "Enigma",
          "value": "O"
        },
        {
          "label": "Quien quiere ser millonario",
          "value": "M"
        }
      ],
      "misiones-juego-estrategiaItems": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Probabilidade",
          "value": "P"
        },
        {
          "label": "Random",
          "value": "R"
        }
      ],
      "misiones-juego-tipoPremioItems": [
        {
          "label": "Prêmio",
          "value": "P"
        },
        {
          "label": "Métrica",
          "value": "M"
        },
        {
          "label": "Agradecimiento",
          "value": "G"
        }
      ],
      "misiones-contenido-itemsRespuestaCorrectaMultiple": [
        {
          "label": "Todas são corretas",
          "value": "T"
        },
        {
          "label": "Exatamente as respostas selecionadas",
          "value": "G"
        },
        {
          "label": "Qualquer uma das respostas selecionadas",
          "value": "C"
        }
      ],
      "misiones-contenido-itemsRespuestaCorrecta": [
        {
          "label": "Todas são corretas",
          "value": "T"
        },
        {
          "label": "Qualquer uma das respostas selecionadas",
          "value": "C"
        }
      ],
      "misiones-contenido-itemsTipoPregunta": [
        {
          "label": "Pesquisa",
          "value": "E"
        },
        {
          "label": "Rating",
          "value": "C"
        }
      ],
      "misiones-contenido-itemsTipoRespuesta": [
        {
          "label": "Múltipla escolha",
          "value": "RM"
        },
        {
          "label": "Escolha única",
          "value": "RU"
        },
        {
          "label": "Resposta aberta - texto",
          "value": "RT"
        },
        {
          "label": "Resposta aberta - numérica",
          "value": "RN"
        }
      ],
      "misiones-contenido-itemsRespuestaCorrectaCalificacion": [
        {
          "label": "Menor ou igual a",
          "value": "D"
        },
        {
          "label": "Igual",
          "value": "E"
        },
        {
          "label": "Menor ou igual a",
          "value": "F"
        }
      ],
      "misiones-contenido-itemsTipo": [
        {
          "label": "Url",
          "value": "U"
        },
        {
          "label": "Video",
          "value": "V"
        },
        {
          "label": "Imagem",
          "value": "I"
        }
      ],
      "misiones-actividad-social-optionsTiposActividad": [
        {
          "label": "Compartilhar (link)",
          "value": "L"
        },
        {
          "label": "Compartilhar (mensagem)",
          "value": "M"
        },
        {
          "label": "Curtir",
          "value": "G"
        },
        {
          "label": "Tweet",
          "value": "A"
        },
        {
          "label": "Twitter Likes",
          "value": "B"
        },
        {
          "label": "Instagram Likes",
          "value": "C"
        }
      ],
      "promociones": [],
      "promociones-detalle-itemsDetalle": "Remover",
      "promociones-alcance-vistas-totales": "Visualizações totais",
      "promociones-alcance-vistas-unicas": "Visualizações únicas",
      "promociones-alcance-marcas-promedio": "Marcas (média)",
      "promociones-alcance-elegibles-promedio": "Elegíveis (média)",
      "promociones-items-codigo": [
        {
          "value": null,
          "label": "Nenhum"
        },
        {
          "value": "A",
          "label": "URL"
        },
        {
          "value": "B",
          "label": "Código QR"
        },
        {
          "value": "C",
          "label": "ITF"
        },
        {
          "value": "D",
          "label": "EAN"
        },
        {
          "value": "E",
          "label": "Código 128"
        },
        {
          "value": "F",
          "label": "Código 39"
        }
      ],
      "promociones-itemsIndComponente": [
        {
          "value": "S",
          "label": "Segmentos"
        },
        {
          "value": "M",
          "label": "Clientes"
        }
      ],
      "promociones-items-calendarizacion": [
        {
          "label": "Permanente",
          "value": "P"
        },
        {
          "label": "Agendadas",
          "value": "C"
        }
      ],
      "tarea-inicio": "Start",
      "tarea-enviar-promocion": "Enviar oferta",
      "tarea-enviar-mensaje": "Enviar mensagem",
      "tarea-enviar-insignia": "Enviar badge",
      "tarea-enviar-premio": "Enviar prêmio",
      "tarea-enviar-mision": "Enviar desafio",
      "tarea-enviar-metrica": "Dar métrica",
      "workflow-items-codigo": [
        {
          "value": "A",
          "label": "Registrar-se no programa"
        },
        {
          "value": "B",
          "label": "Resgatou uma oferta"
        },
        {
          "value": "C",
          "label": "Completou o desafio"
        },
        {
          "value": "D",
          "label": "Resgatou um prêmio"
        },
        {
          "value": "E",
          "label": "Comprou um produto específico"
        },
        {
          "value": "H",
          "label": "Comprou produtos"
        },
        {
          "value": "F",
          "label": "Aniversário de Adesão"
        },
        {
          "value": "G",
          "label": "Comprou produtos"
        },
        {
          "value": "I",
          "label": "Evento Customizado"
        },
        {
          "value": "J",
          "label": "Join  the program"
        }
      ],
      "workflow-tipo-tarea": [
        {
          "label": "Enviar oferta",
          "value": "A"
        },
        {
          "label": "Enviar mensagem",
          "value": "B"
        },
        {
          "label": "Conceder Badge",
          "value": "C"
        },
        {
          "label": "Prêmios presente",
          "value": "D"
        },
        {
          "label": "Enviar desafio",
          "value": "E"
        },
        {
          "label": "Otorgar Métrica",
          "value": "F"
        },
        {
          "value": "M",
          "label": "Secuencia de desafio"
        }
      ],
      "leaderboard-itemsTipoFormato": [
        {
          "label": "Nome",
          "value": "N"
        },
        {
          "label": "Iniciais",
          "value": "I"
        }
      ],
      "leaderboard-itemsTipoTabla": [
        {
          "label": "Grupo",
          "value": "E"
        },
        {
          "label": "Equipes",
          "value": "G"
        },
        {
          "label": "Membros",
          "value": "U"
        }
      ],
      "leaderboard-indGrupalFormaCalculo": [
        {
          "label": "Soma",
          "value": "S"
        },
        {
          "label": "Média",
          "value": "P"
        }
      ],
      "general-default-select": {
        "value": null,
        "label": "Escolha uma opção"
      },
      "atributo-miembro-tipoItems": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Numérico",
          "value": "N"
        },
        {
          "label": "Texto",
          "value": "T"
        },
        {
          "label": "Boleano",
          "value": "B"
        },
        {
          "label": "Data",
          "value": "F"
        }
      ],
      "configuracion-protocolo-smtp": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Protocolo SSL",
          "value": "S"
        },
        {
          "label": "Protocolo TLS",
          "value": "T"
        },
        {
          "label": "Sem protocolo",
          "value": "N"
        }
      ],
      "insignia-tipo": [
        {
          "label": "Escolha uma opção",
          "value": ""
        },
        {
          "label": "Status",
          "value": "S"
        },
        {
          "label": "Collector",
          "value": "C"
        }
      ],
      "documento-inventario-tipo": [
        {
          "label": "Compras",
          "value": "C"
        },
        {
          "label": "Ajuste mais",
          "value": "A"
        },
        {
          "label": "Ajuste menos",
          "value": "M"
        },
        {
          "label": "Transferência entre locais",
          "value": "I"
        },
        {
          "label": "Resgates",
          "value": "R"
        }
      ],
      "booleano-general": [
        {
          "label": "Verdade",
          "value": "true"
        },
        {
          "label": "Falso",
          "value": "false"
        }
      ],
      "miembro-educacion": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Escola",
          "value": "P"
        },
        {
          "label": "Colegial",
          "value": "S"
        },
        {
          "label": "Tecnico",
          "value": "T"
        },
        {
          "label": "Universitaria",
          "value": "U"
        },
        {
          "label": "Grado",
          "value": "G"
        },
        {
          "label": "Mestre",
          "value": "M"
        },
        {
          "label": "Pós-graduação",
          "value": "R"
        },
        {
          "label": "Doutorado",
          "value": "D"
        }
      ],
      "miembro-genero": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Fêmea",
          "value": "F"
        },
        {
          "label": "Masculino",
          "value": "M"
        }
      ],
      "miembro-estado-civil": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Casado",
          "value": "C"
        },
        {
          "label": "Solteiro",
          "value": "S"
        },
        {
          "label": "União livre",
          "value": "U"
        }
      ],
      "premio-tipoPremio": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Certificado",
          "value": "C"
        },
        {
          "label": "Produto físico",
          "value": "P"
        }
      ],
      "elegibles-general": [
        {
          "label": "Escolha uma opção",
          "value": ""
        },
        {
          "label": "Segmentos",
          "value": "S"
        },
        {
          "label": "Members",
          "value": "M"
        }
      ],
      "tipos-elegibles-general": [
        {
          "label": "Disponíveis",
          "value": "D"
        },
        {
          "label": "Incluídas",
          "value": "I"
        },
        {
          "label": "Excluídos",
          "value": "E"
        }
      ],
      "accion-elegibles-general": [
        {
          "label": "Incluir",
          "value": "I"
        },
        {
          "label": "Excluir",
          "value": "E"
        }
      ],
      "dias-efectividad-general": [
        {
          "label": "Segunda-feira",
          "value": "L"
        },
        {
          "label": "Terça-feira",
          "value": "K"
        },
        {
          "label": "Quarta-feira",
          "value": "M"
        },
        {
          "label": "Quinta-feira",
          "value": "J"
        },
        {
          "label": "Sexta-feira",
          "value": "V"
        },
        {
          "label": "Sábado",
          "value": "S"
        },
        {
          "label": "Domingo",
          "value": "D"
        }
      ],
      "tipo-efectividad-general": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Permanente",
          "value": "P"
        },
        {
          "label": "Agendada",
          "value": "C"
        }
      ],
      "atributo-producto-tipoItems": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Numérico",
          "value": "N"
        },
        {
          "label": "Texto",
          "value": "T"
        },
        {
          "label": "Múltiple",
          "value": "M"
        }
      ],
      "metrica-medida-tipo": [
        {
          "label": "Métrica baseada em...",
          "value": ""
        },
        {
          "label": "Pontos",
          "value": "P"
        },
        {
          "label": "Visitas",
          "value": "V"
        },
        {
          "label": "Atividade",
          "value": "A"
        },
        {
          "label": "Duração",
          "value": "U"
        },
        {
          "label": "Distância",
          "value": "D"
        },
        {
          "label": "Gastos",
          "value": "G"
        }
      ],
      "estado-redencion-premio": [
        {
          "label": "Pendente",
          "value": "S"
        },
        {
          "label": "Retirado",
          "value": "R"
        },
        {
          "label": "Cancelado",
          "value": "A"
        }
      ],
      "trabajo-interno-tipo": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Notificação",
          "value": "N"
        },
        {
          "label": "Importação",
          "value": "I"
        },
        {
          "label": "Exportação",
          "value": "E"
        }
      ],
      "trabajo-interno-estado": [
        {
          "label": "Escolha uma opção",
          "value": null
        },
        {
          "label": "Procesando",
          "value": "P"
        },
        {
          "label": "Falhou",
          "value": "I"
        },
        {
          "label": "Concluído",
          "value": "E"
        }
      ],
      "id-miembro-importacion": [
        {
          "label": "UUID",
          "value": "idMiembro"
        },
        {
          "label": "Identificação",
          "value": "docIdentificacion"
        }
      ],
      "importacion-miembro-segmento": [
        {
          "label": "UUID",
          "value": "ID_MIEMBRO"
        },
        {
          "label": "Identificação",
          "value": "DOC_IDENTIFICACION"
        }
      ],
      "atributo-tipo":[
        {
          "label": "Estático",
          "value": "E"
        },
        {
          "label": "Dinâmicos",
          "value": "D"
        }
      ],
      "atributos-estaticos": [
        {
          "value": "NOMBRE",
          "label": "Nome"
        },
        {
          "value": "APELLIDO",
          "label": "Nome do meio"
        },
        {
          "value": "APELLIDO2",
          "label": "Sobrenome"
        },
        {
          "value": "FECHA_NACIMIENTO",
          "label": "Data de nascimento"
        },
        {
          "value": "CIUDAD_RESIDENCIA",
          "label": "Cidade (residência)"
        },
        {
          "value": "DIRECCION",
          "label": "Direção"
        },
        {
          "value": "ESTADO_RESIDENCIA",
          "label": "Estado (residência)"
        },
        {
          "value": "PAIS_RESIDENCIA",
          "label": "País (residência)"
        },
        {
          "value": "CODIGO_POSTAL",
          "label": "Código Postal"
        },
        {
          "value": "CORREO",
          "label": "Email"
        },
        {
          "value": "CONTRASENA",
          "label": "Senha"
        },
        {
          "value": "IND_EDUCACION",
          "label": "Educação"
        },
        {
          "value": "IND_ESTADO_CIVIL",
          "label": "Estado civil"
        },
        {
          "value": "IND_GENERO",
          "label": "Gênero"
        },   
        {
          "value": "IND_HIJOS",
          "label": "Filhos"
        },
        {
          "value": "INGRESO_ECONOMICO",
          "label": "Faturamento"
        },
        {
          "value": "TELEFONO_MOVIL",
          "label": "Telefone"
        },
        {
          "value": "IND_CONTACTO_EMAIL",
          "label": "Contato por email"
        },
        {
          "value": "IND_CONTACTO_ESTADO",
          "label": "Contato por situação"
        },
        {
          "value": "IND_CONTACTO_NOTIFICACION",
          "label": "Contato por push"
        },
        {
          "value": "IND_CONTACTO_SMS",
          "label": "Contato por SMS"
        }
      ],
      "mision-aprobacion-estado":[
        {
          "label": "Adquirido",
          "value": "G"
        },
        {
          "label": "Falha",
          "value": "F"
        },
        {
          "label": "Pendente",
          "value": "P"
        }
      ],
      "respuesta-preferencia-tipo":[
        {
          "label": "Escolha única",
          "value": "SU"
        },
        {
          "label": "Múltipla escolha",
          "value": "RM"
        },
        {
          "label": "Texto",
          "value": "TX"
        }
      ],
      "miembro-suspencion": {
        "severity": "success",
        "summary": "Suspendido",
        "detail": "Membro de programa foi suspenso."
      },
      "miembro-inactivo": {
        "severity": "info",
        "summary": "Inactivo",
        "detail": "Membro de programa foi inativado."
      },
      "usuarios-error-username": {
        "severity": "error",
        "summary": "Erro",
        "detail": "O nome de usuário já existe"
      },
      "usuario-correo": {
        "severity": "error",
        "summary": "Error",
        "detail": "O e-mail já existe"
      },
      "segmentos-info-procesando": {
        "severity": "info",
        "summary": "Processando",
        "detail": "A seleção está sendo processada, por favor aguarde"
      },
      "segmentos-enviado-procesamiento": {
        "severity": "info",
        "summary": "Enviado",
        "detail": "O pedido foi enviado ao servidor"
      },
      "segmentos-miembros-select": {
        "detail": "Escolha um ou mais usuários",
        "severity": "info",
        "summary": ""
      },
      "general-lista-vacia": { 
        "severity": "info", 
        "summary": "Info", 
        "detail": "Nenhum registro localizado" 
      },
      "general-insercion": {
        "detail": "Os dados foram inseridos com sucesso",
        "severity": "success",
        "summary": "Inserido"
      },
      "general-actualizacion": {
        "detail": "Os dados foram atualizados com sucesso",
        "severity": "success",
        "summary": "Atualizado"
      },
      "general-eliminacion": {
        "detail": "Os dados foram excluídos com sucesso",
        "severity": "success",
        "summary": "Excluído"
      },
      "info-uploading": {
        "detail": "Carregando imagem",
        "severity": "info",
        "summary": "Espere"
      },
      "general-inclusion-simple": {
        "detail": "Os dados foram adicionados com sucesso",
        "severity": "success",
        "summary": "Adicionado"
      },
      "general-inclusion-multiple": {
        "detail": "Os itens foram adicionados com sucesso",
        "severity": "success",
        "summary": "Adicionado"
      },
      "general-exclusion-multiple": {
        "detail": "Os itens foram excluídos com sucesso",
        "severity": "success",
        "summary": "Excluidos"
      },
      "general-exclusion-simple": {
        "detail": "O item foi excluído com sucesso",
        "severity": "success",
        "summary": "Excluido"
      },
      "general-eliminacion-multiple": {
        "detail": "Os itens foram excluídos com sucesso",
        "severity": "success",
        "summary": "Excluído"
      },
      "general-eliminacion-simple": {
        "detail": "O item foi excluído com sucesso",
        "severity": "success",
        "summary": "Excluído"
      },
      "general-reporte-creacion": {
        "detail": "O relatório foi criado com sucesso",
        "severity": "success",
        "summary": "Adicionado"
      },
      "general-actualizando-datos": {
        "detail": "Atualizando dados",
        "severity": "info",
        "summary": "Espere"
      },
      "general-archivar": {
        "detail": "Os dados foram gravados com sucesso",
        "severity": "success",
        "summary": "Arquivado"
      },
      "general-publicar": {
        "detail": "Os item foi publicado com sucesso",
        "severity": "success",
        "summary": "Publicado"
      },
      "general-activar": {
        "detail": "Membro foi ativado corretamente.",
        "severity": "success",
        "summary": "Activado"
      }, 
      "general-desactivar": {
        "detail": "O item foi desativado corretamente",
        "severity": "success",
        "summary": "Inactivo"
      },
      "reporte-espera-generar": {
        "detail": "Você deve aguardar a conclusão da geração de certificado anterior, tente novamente mais tarde.",
        "severity": "info",
        "summary": "Información"
      },
      "documento-cerrado":{
        "detail": "O documento está fechado.",
        "severity": "success",
        "summary": "Cerrado"
      },
      "cierre-mes":{
        "detail": "The closing of the month was made..",
        "severity": "success",
        "summary": "Cerrado"
      },
      "respuesta-mision-aprobar":{
        "detail": "A resposta do membro foi aprovada.",
        "severity": "success",
        "summary":"Gano"
      },
      "respuesta-mision-rechazar":{
        "detail": "A resposta do membro foi recusada.",
        "severity": "success",
        "summary":"Fallo"
      },
      "procesando-importacion":{
        "detail": "A importação está sendo processada..",
        "severity": "info",
        "summary":""
      },
      "error-datos-incorrectos": {
        "detail": "Você deve incluir ao menos uma pergunta",
        "severity": "error",
        "summary": "Dados incorretos"
      },
      "error-conexion": {
        "detail": "Ocorreu um erro de conexão",
        "severity": "error",
        "summary": "Erro"
      },
      "error-nombre-interno": {
        "detail": "O nome interno já existe",
        "severity": "error",
        "summary": "Dados incorretos"
      },
      "error-url-invalido": {
        "detail": "O formato da URL não é válido",
        "severity": "error",
        "summary": "Erro"
      },
      "error-notificacion-texto": {
        "detail": "Você deve especificar o texto da notificação primeiro",
        "severity": "error",
        "summary": "Erro"
      },
      "error-misiones-pregunta": {
        "detail": "Você deve incluir ao menos uma pergunta",
        "severity": "error",
        "summary": "Dados incorretos"
      },
      "error-leaderboard-grupo": {
        "detail": "Selecione um grupo...",
        "severity": "error",
        "summary": "Erro"
      },
      "error-codigo-premio":{
        "detail": "El código de premio no es válido",
        "severity": "error",
        "summary": "Error"
      },
      "warn-misiones-premio-juego": {
        "detail": "Selecione um prêmio.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-metrica-juego": {
        "detail": "Insira a quantidade a ser enviada.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-tipo-juego": {
        "detail": "Selecione um tipo.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-estrategia": {
        "detail": "Selecione uma estratégia.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-imagen": {
        "detail": "Selecione uma imagem.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-rompecabeza": {
        "detail": "Quantidade de prêmios excedida. Elimine prêmios para alterar o Quebra-Cabeças.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-raspadita": {
        "detail": "Quantidade de prêmios excedida. Elimine prêmios para alterar o desafio Raspadinha.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-cambio": {
        "detail": "Alterado o estadao da Missão, não é possível entrar novo prêmio.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-tiempo": {
        "detail": "O tempo limite deve ser maior que 10 segundos.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-cantidadPiezas": {
        "detail": "O número de peças deve estar entre 6 e 16.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-probabilidad": {
        "detail": "Probabilidade do Prêmio - Requerido.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-repoert-periodo": {
        "detail": "Período fora do intervalo permitido, verifique e tente novamente",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-repoert-mes": {
        "detail": "Mês fora do intervalo permitido, verifique e tente novamente.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-atributo-valor": {
        "detail": "Verifique o valor do atributo para seguir.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-atributo-respuesta": {
        "detail": "Entre pelo menos duas (2) repostas.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-resultado": {
        "detail": "Resultados não localizados.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-ubicacion-geocoder": {
        "detail": "O serviço de geolocalização falhou: ",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-ubicacion-fecha": {
        "detail": "Verifique o intervalo de datas selecionado",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-ubicacion-semana": {
        "detail": "Indique a semana e dias de repetição para continuar.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-ubicacion-destino": {
        "detail": "Selecione uma localidade de Destino.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-origen": {
        "detail": "Selecione uma localidade de Origem.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-igual": {
        "detail": "A localidade de destino e origem deve ser diferente.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-proveedor": {
        "detail": "Seleccione um proveedor.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-workflow-disparador": {
        "detail": "Selecione o item para o envio/disparo segundo o tipo de ação.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-dato-valido": {
        "detail": "Entre com as informações solicitadas para continuar",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-cantidad": {
        "detail": "Selecione pelo menos um item para continuar",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-precio-premio": {
        "detail": "Insira o preço do prêmio.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-cantidad-premio": {
        "detail": "Digite a quantidade de prêmios.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "pipe-calendarizacion": {
        "Permanente": "Permanente",
        "Calendarizado": "Agendada",
        "Recurrente": "Recorrente"
      },
      "pipe-estado-campana": {
        "Borrador": "Rascunho",
        "Publicado": "Publicado",
        "Archivado": "Arquivado",
        "Ejecutado": "Executando"
      },
      "pipe-tipo-segmento": {
        "Estático": "Estático",
        "Dinamico": "Dinâmica"
      },
      "pipe-status-trabajo": {
        "RUNNING": "Executando",
        "SUBMITTED": "Em fila",
        "FAILED": "Falhou",
        "FINISHED": "Concluído",
        "KILLED": "Cancelado",
        "STOPPED": "Parado"
      },
      "pipe-evento-campana": {
        "Manual": "Manual",
        "Calendarizado": "Agendada",
        "Trigger": "Trigger"
      },
      "pipe-ind-operador": {
        "AND": "AND",
        "OR": "OR",
        "NOTIN": "NOT IN"
      },
      "pipe-tipo-insignia": {
        "Status": "Status",
        "Collector": "Collector"
      },
      "pipe-regla-tipo-cambio": {
        "A": "Ganhar",
        "B": "Expirar",
        "C": "Resgatar"
      },
      "pipe-regla-avanzada-ind-ultimo": {
        "D": "Dia(s)",
        "A": "Ano(s)",
        "M": "Mês(es)"
      },
      "pipe-tipo-notificacion": {
        "P": "Enviar notificações",
        "E": "Email"
      },
      "pipe-tipo-leaderboard": {
        "E": "Grupo",
        "U": "Membros",
        "G": "Equipes"
      },
      "pipe-estado-segmento": {
        "AR": "Arquivado",
        "BO": "Rascunho",
        "AC": "Ativo"
      },
      "pipe-estado-workflow": {
        "A": "Ativo",
        "B": "Arquivado",
        "C": "Rascunho"
      },
      "pipe-estado-usuario": {
        "B": "Ativo",
        "I": "Inativo"
      },
      "pipe-balance-metrica": {
        "A": "acumulado",
        "B": "vencido",
        "C": "resgatado",
        "D": "disponível"
      },
      "pipe-atributo-tipoDato": {
        "T": "Texto",
        "F": "Data",
        "B": "Boleano",
        "N": "Numérico",
        "M": "Múltiplo"
      },
      "pipe-atributo-visibilidad": {
        "true": "Administradores e app mobile",
        "false": "Somente administradores"
      },
      "pipe-cambio-estado": {
        "A": "Ativo",
        "B": "Rascunho",
        "I": "Inativo"
      },
      "pipe-certificado-estado": {
        "D": "Disponível",
        "A": "Cancelado",
        "O": "Ocupado"
      },
      "pipe-dato-booleano": {
        "true": "Verdade",
        "false": "Falso"
      },
      "pipe-documento-estado": {
        "true": "Fechar",
        "false": "Aberto"
      },
      "pipe-documento-tipo": {
        "C": "Compra",
        "R": "Redenção",
        "A": "Ajuste mais",
        "M": "Ajuste menos",
        "I": "Transferência entre locais"
      },
      "pipe-estado-elemento": {
        "B": "Rascunho",
        "P": "Publicado",
        "A": "Arquivado",
        "I": "Publicado",
        "D": "Rascunho"
      },
      "pipe-metrica-medida": {
        "P": "Pontos",
        "V": "Visitas",
        "A": "Atividade",
        "U": "Duração",
        "D": "Distância",
        "G": "Gastos"
      },
      "pipe-miembro-educacion": {
        "P": "Escola",
        "S": "Colegial",
        "T": "Tecnico",
        "U": "Universitaria",
        "G": "Grado",
        "M": "Mestre",
        "R": "Pós-graduação",
        "D": "Doutorado"
      },
      "pipe-miembro-estadoCivil": {
        "C": "Casado",
        "S": "Solteiro",
        "U": "União livre"
      },
      "pipe-miembro-estado": {
        "A": "Ativo",
        "I": "Inativo"
      },
      "pipe-miembro-genero": {
        "M": "Masculino",
        "F": "Fêmea"
      },
      "pipe-objetivo-campana": {
        "M": "Desafio",
        "P": "Oferta",
        "R": "Prêmio"
      },
      "pipe-tipo-premioJuego": {
        "P": "Prêmio",
        "M": "Métrica",
        "G": "Agradecimiento"
      },
      "pipe-premio-tipo": {
        "P": "Produto físico",
        "C": "Certificado"
      },
      "pipe-tipo-juegoMision": {
        "R": "Roleta",
        "A": "Raspadinha",
        "O": "Enigma"
      },
      "pipe-estado-redencion": {
        "S": "Pendente",
        "R": "Retirado",
        "A": "Cancelado"
      },
      "pipe-estado-trabajo-interno": {
        "P": "Procesando",
        "F": "Fallido",
        "C": "Completado"
      },
      "pipe-tipo-trabajo-interno": {
        "N": "Notificación",
        "E": "Exportação",
        "I": "Importação"
      },
      "pipe-tipo-pregunta": {
        "E": "Pesquisa",
        "C": "Calificación"
      },
      "pipe-tipo-red-social": {
        "L": "Link",
        "M": "Mensagem",
        "G": "Curtir",
        "A": "Tweet",
        "B": "Twitter Likes",
        "C": "Instagram Likes"
      },
      "reglas-asignacion-badge-tipo-regla": [
        {
          "label": "Execução de Missão",
          "value": "A"
        },
        {
          "label": "Quantidade de Referências",
          "value": "B"
        },
        {
          "label": "Incremento de Nível",
          "value": "C"
        },
        {
          "label": "Compra Acumulada",
          "value": "D"
        },
        {
          "label": "Quantidade de compras",
          "value": "E"
        }
      ],
      "reglas-asignacion-premio-tipo-regla": [
        {
          "label": "Frequência de compra",
          "value": "A"
        }, 
        {
          "label": "Quantidade de Compra",
          "value": "B"
        }, 
        {
          "label": "Compra Acumulada",
          "value": "C"
        }
      ],
      "reglas-asignacion-premio-fecha-calendarizacion": [
        {
          "label": "Dia(s)",
          "value": "D"
        },
        {
          "label": "Semana(s)",
          "value": "S"
        },
        {
          "label": "Mes(s)",
          "value": "M"
        }
      ],
      "reglas-listaPeriodoTiempo": [
        {
          "value": "B",
          "label": "Posterior "
        },
        {
          "value": "C",
          "label": "Anterior"
        },
        {
          "value": "D",
          "label": "De"
        },
        {
          "value": "H",
          "label": "Para"
        },
        {
          "value": "E",
          "label": "Entre"
        },
        {
          "value": "F",
          "label": "Mês actual"
        },
        {
          "value": "G",
          "label": "Ano actual"
        }
      ],
      "bitacora-accion": [
        {
          "label": "Inserido.",
          "value": "A"
        },
        {
          "label": "Atualizado",
          "value": "B"
        },
        {
          "label": "Removido",
          "value": "C"
        },
        {
          "label": "Publicado",
          "value": "D"
        },
        {
          "label": "Arquivado",
          "value": "E"
        },
        {
          "label": "Desativado",
          "value": "F"
        },
        {
          "label": "Ativo",
          "value": "G"
        },
        {
          "label": "Rascunho",
          "value": "H"
        }
      ],
      "confirmacion-pregunta": {
        "A": "Deseja arquivar o item?",
        "B": "Deseja remover o item?",
        "C": "Deseja ativar este item?",
        "D": "Deseja suspender este membro?",
        "E": "Deseja suspender este usuário?",
        "F": "Deseja inativar este item?",
        "G": "Deseja excluir este item?",
        "H": "Deseja excluir a lista de itens??"
      },
      "metricas-dashboard-cant-acumulada": "Quantidade acumulada",
      "metricas-dashboard-cant-redimida": "Quantidade vencida",
      "metricas-dashboard-cant-vencida": "Quantidade resgatada",
      "misiones-dashboard-cant-respuestas": "Quantidade de respostas",
      "misiones-dashboard-cant-respuestas-aprobadas": "Quantidade de respostas aprovadas",
      "misiones-dashboard-cant-respuestas-rechazadas": "Quantidade de respostas rejeitadas",
      "misiones-dashboard-cant-elegibles-promedio": "Média de membros elegíveis",
      "misiones-dashboard-cant-cant-vistas-totales": "Número total de visualizações",
      "misiones-dashboard-cant-vistas-unicas": "Número total de visitantes únicos",
      "notificaciones-dashboard-cant-respuestas": "Quantidade de respostas",
      "notificaciones-dashboard-cant-msjs-enviados": "Número de mensagens enviadas",
      "notificaciones-dashboard-cant-msjs-abiertos": "Número de mensagens abertas",
      "notificaciones-dashboard-cant-miembros-enviados": "Número de membros enviados",
      "premios-dashboard-cant-redenciones": "Número de resgates",
      "premios-dashboard-cant-elegibles-promedio": "Média de membros elegíveis"
    };
  }
}