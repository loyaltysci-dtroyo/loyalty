import { Injectable } from '@angular/core';

@Injectable()
export class LabelsEN {
  data: any;
  constructor() {
    this.data = {
      "breadcrumbs": {
        "dashboard": "Dashboard",
        "usuarios": "Users",
        "insertarUsuario": "Insert User",
        "listaUsuarios": "User List",
        "detalleUsuario": "User Detail",
        "detalleAtributos": "Attribute Detail",
        "detalleRoles": "Role Detail",
        "insertarRoles": "Insert Role",
        "listaRoles": "Role List",
        "detalleRol": "Role Detail",
        "tipoMiembro": "Member Type",
        "miembros": "Members",
        "insertarMiembro": "Insert Member",
        "detalleMiembro": "Member Detail",
        "perfil": "Profile",
        "niveles": "Levels",
        "grupos": "Groups",
        "atributosDinamicos": "Dynamic Attributes",
        "insignias": "Badges",
        "preferencias": "Preferences",
        "segmentos": "Segments",
        "redenciones": "Redeems",
        "premios": "Rewards",
        "retos": "Challenges",
        "editarMiembro": "Edit Member",
        "listaMiembros": "Member List",
        "ubicaciones": "Locations",
        "listaUbicaciones": "Location List",
        "insertarUbicacion": "Insert Location",
        "detalleUbicacion": "Location Detail",
        "efectividad": "Location Effectiveness",
        "mapa": "Map",
        "metricas": "Metrics",
        "insertarMetrica": "Insert Métric",
        "metricaLista": "Metric List",
        "metricaDetalle": "Metric Detail",
        "detalleNivel": "Level Detail",
        "gruposNivel ": "Tier Group List",
        "detalle": "Tier Group Detail",
        "leaderboards": "Leaderboards",
        "listaLeaderboards": "Leaderboard List",
        "detalleLeaderboard": "Leaderboard Detail",
        "insertarLeaderboards": "Insert Leaderboard",
        "insertarSegmento": "Insert Segment",
        "listaSegmentos": "Segment List",
        "detalleSegmento": "Segment Detail",
        "atributos": "Attribute Detail",
        "reglas": "Rule List",
        "detalleReglas": "Rule Detail",
        "elegibles": "Elegibility",
        "promociones": "Offers",
        "listaPromos": "Offer List",
        "insertarPromo": "Insert Offer",
        "insertarCategoria": "Insert Category",
        "listaCategorias": "Category List",
        "detallePromo": "Offer Detail",
        "common": "Attributes",
        "arte": "Display",
        "alcance": "Reach",
        "limites": "Limits",
        "categorias": "Categories",
        "detalleCategorias": "Category Details",
        "misiones": "Challenges",
        "insertarMision": "Insert Challenges",
        "detalleMision": "Challenges Details",
        "listaMisiones": "Challenges List",
        "juegos": "Games",
        "productos": "Product",
        "detalleProducto": "Product Detail",
        "listaProductos": "Product List",
        "insertarProducto": "Insert Product",
        "informacion": "General",
        "display": "Display",
        "listaCategoriasProducto": "Product Categories",
        "insertarCategProducto": "Insert Product Category",
        "subcategorias": "Sub-Categories",
        "calendarizacion": "Schelduling",
        "recompensas": "Rewards",
        "notificaciones": "Notifications",
        "listaNotificaciones": "Notifications List",
        "insertarNotificaciones": "Insert Notification",
        "detalleCampana": "Notification Detail",
        "insertar": "Insert Group",
        "detalleGrupo": "Group Detail",
        "listaPreferencias": "Preferences List",
        "insertarPreferencia": "Insert Preference",
        "detallePreferencias": "Preference Detail",
        "listaInsignias": "Badge List",
        "insertarInsignia": "Insert Badge",
        "detalleInsignia": "Bade Details",
        "listaNivel": "Insert Level",
        "asignarMiembro": "Member Assign",
        "listaAtributos": "Attribute List",
        "detalleAtributo": "Attribute Detail",
        "configuracionSistema": "System Configuration",
        "general": "General",
        "medios": "Payment Methods",
        "politica": "Policies",
        "parametros": "System Configuration",
        "detallePremio": "Reward Detail",
        "requerido": "Required Info",
        "insertarPremio": "Insert Reward",
        "redimido": "Redeemed",
        "limite": "Limits",
        "listaCategoriasPremio": "Categories List",
        "detalleCategoria": "Category Detail",
        "asignar": "Assign Category",
        "certificados": "Certificates",
        "workflows": "Workflows",
        "listaWorkflow": "Workflow List",
        "insertarWorkflow": "Insert Workflow",
        "detalleWorkflow": "Workflow Detail",
        "inventario": "Inventory Management",
        "listaDocumentos": "Document List",
        "insertarDocumento": "Insert Document",
        "infoDocumento": "Document Info",
        "proveedor": "Provider List",
        "dashboard-general-metrica": "Metrics Dashboard",
        "dashboard-general-mision": "Challenges Dashboard",
        "dashboard-general-notificacion": "Notifications Dashboard",
        "dashboard-general-premio": "Rewards Dashboard",
        "dashboard-general-promocion": "Offers Dashboard",
        "reportes": "Reports",
        "historico": "History",
        "compras": "Purchases",
        "importacion": "Import",
        'bitacoras': "Log",
        "apiExternal": "Log - External API",
        "apiAdministrativo": "Log - Admin API",
        "noticia": "Newsfeed",
        "listaNoticias": "Newsfeed List",
        "categoriasNoticia": "Newsfeed Categories",
        "aprobacion":"Response management"
      },
      "tabs": {},
      "menu": {
        "dashboard-miembros": "Member Dashboard",
        "dashboard-segmentos": "Segment Dashboard",
        "dashboard-programa": "Program Dashboard",
        "dashboard-actividad": "Activity Dashboard",
        "dashboard-metrica": "Metrics Dashboard",
        "ubicaciones": "Locations",
        "metricas": "Metrics",
        "grupos-tiers": "Tier Groups",
        "adm-programa": "Program Management",
        "listado-usuarios": "User List",
        "listado-roles": "Role List",
        "usuarios": "Users",
        "miembros": "Members",
        "listado-miembros": "Member List ",
        "preferencias": "Preferences",
        "atributos-dinamicos": "Dynamic Attributes",
        "grupos-miembro": "Member Groups",
        "leaderboards": "Leaderboards",
        "segmentos": "Segments",
        "promociones": "Offers",
        "listado-promociones": "Offer List",
        "actividad-promociones": "Offer Activity",
        "listado-categorias": "Offer Categories",
        "insignias": "Badges",
        "premios": "Rewards",
        "listado-premios": "Rewards List",
        "actividad-premios": "Rewards Activity",
        "categorias-premio": "Rewards Categories",
        "unidades-medida": "Measuring Units",
        "notificaciones-listado": "Notifications List",
        "actividad-notificaciones": "Notifications Activity",
        "notificaciones": "Notifications",
        "misiones": "Challenges",
        "listado-misiones": "Challenge List",
        "actividad-misiones": "Challenge Activity",
        "categorias-mision": "Challenge Categories",
        "productos": "Products",
        "listado-productos": "Product List",
        "categorias-productos": "Product Categories",
        "atributos-producto": "Product Attributes",
        "workflow": "Workflows",
        "documentos-listado": "Document List",
        "proveedores-listado": "Provider List",
        "inventario-premio": "Inventory Management",
        "dashboard-general-metrica": "Metrics Dashboard",
        "dashboard-general-mision": "Challenges Dashboard",
        "dashboard-general-notificacion": "Notifications Dashboard",
        "dashboard-general-premio": "Rewards Dashboard",
        "dashboard-general-promocion": "Offers Dashboard",
        "reportes": "Reports",
        "historico": "History",
        "compras": "Purchase",
        "importacion": "Import",
        'bitacoras': "Log",
        "apiExternal": "Log - External API",
        "apiAdministrativo": "Log - Admin API",
        "noticia": "Newsfeed",
        "listaNoticias": "Newsfeed List",
        "categoriasNoticia": "Newsfeed Categories",
        "aprobacion":"Response management"
      },
      "general-limites-periodo": [
        {
          "value": "A",
          "label": "Always"
        },
        {
          "value": "B",
          "label": "Minute"
        },
        {
          "value": "C",
          "label": "Hour"
        },
        {
          "value": "D",
          "label": "Day"
        },
        {
          "value": "E",
          "label": "Week"
        },
        {
          "value": "F",
          "label": "Month"
        }
      ],
      "general-calendar": {
        "firstDayOfWeek": 0,
        "dayNames": [
          "Domingo",
          "Lunes",
          "Martes",
          "Miercoles",
          "Jueves",
          "Viernes",
          "Sabado"
        ],
        "dayNamesShort": [
          "Dom",
          "Lun",
          "Mar",
          "Mie",
          "Jue",
          "Vie",
          "Sab"
        ],
        "dayNamesMin": [
          "D",
          "L",
          "K",
          "M",
          "J",
          "V",
          "S"
        ],
        "monthNames": [
          "Enero",
          "Febrero",
          "Marzo",
          "Abril",
          "Mayo",
          "Junio",
          "Julio",
          "Agosto",
          "Septiembre",
          "Octubre",
          "Noviembre",
          "Diciembre"
        ],
        "monthNamesShort": [
          "Ene",
          "Feb",
          "Mar",
          "Abr",
          "May",
          "Jun",
          "Jul",
          "Ago",
          "Sep",
          "Oct",
          "Nov",
          "Dic"
        ]
      },
      "general-actividad-periodos": [
        {
          "label": "1 month",
          "value": "1M"
        },
        {
          "label": "2 months",
          "value": "2M"
        },
        {
          "label": "3 months",
          "value": "3M"
        },
        {
          "label": "6 months",
          "value": "6M"
        },
        {
          "label": "1 year",
          "value": "1Y"
        },
        {
          "label": "2 years",
          "value": "2Y"
        }
      ],
      "general-itemsIndExclusion": [
        {
          "value": "I",
          "label": "Include"
        },
        {
          "value": "E",
          "label": "Exclude"
        }
      ],
      "segmentos": "",
      "segmentos-reglas-avanzadas": [
        {
          "value": "B",
          "label": "Received a Message"
        },
        {
          "value": "A",
          "label": "Joined the Program"
        },
        {
          "value": "C",
          "label": "Opened a Message"
        },
        {
          "value": "D",
          "label": "Redeemed a Reward"
        },
        {
          "value": "F",
          "label": "Purchases"
        },
        {
          "value": "G",
          "label": "Last Login"
        },
        {
          "value": "H",
          "label": "Referrals"
        },
        {
          "value": "I",
          "label": "Device"
        },
        {
          "value": "J",
          "label": "Ranking Position"
        },
        {
          "value": "K",
          "label": "Number of purchase in the Location "
        },
        {
          "value": "L",
          "label": "Accepted the Offer"
        },
        {
          "value": "M",
          "label": "Looked at the Offer"
        },
        {
          "value": "N",
          "label": "Purchase Product"
        },
        {
          "value": "O",
          "label": "Looked at the  Challenge"
        },
        {
          "value": "P",
          "label": "Challenge Completed"
        },
        {
          "value": "Q",
          "label": "Ganaron una misión"
        },
        {
          "value": "R",
          "label": "Has a Badge"
        },
        {
          "value": "S",
          "label": "Belongs to a Group"
        },
        {
          "value": "T",
          "label": "Belongs to an Audience"
        }
      ],
      "segmentos-listaElementosReciente": [
        {
          "value": "D",
          "label": "Day"
        },
        {
          "value": "A",
          "label": "Year"
        },
        {
          "value": "M",
          "label": "Month"
        }
      ],
      "segmentos-listaDispositivos": [
        {
          "value": "A",
          "label": "Android"
        },
        {
          "value": "I",
          "label": "IOS"
        }
      ],
      "segmentos-listaOpcionesBoolean": [
        {
          "value": "true",
          "label": "True"
        },
        {
          "value": "false",
          "label": "False"
        }
      ],
      "segmentos-listaEstadosCiviles": [
        {
          "label": "Married",
          "value": "C"
        },
        {
          "label": "Single",
          "value": "S"
        },
        {
          "label": "Free union",
          "value": "U"
        }
      ],
      "segmentos-listaEducacionMiembro": [
        {
          "label": "Choose an option",
          "value": null
        },
        {
          "label": "School",
          "value": "P"
        },
        {
          "label": "High school",
          "value": "S"
        },
        {
          "label": "Technical",
          "value": "T"
        },
        {
          "label": "Graduate",
          "value": "U"
        },
        {
          "label": "Degree",
          "value": "G"
        },
        {
          "label": "Postgraduate",
          "value": "R"
        }
      ],
      "segmentos-listaEstadosMiembro": [
        {
          "value": "A",
          "label": "Active"
        },
        {
          "value": "I",
          "label": "Inactive"
        }
      ],
      "segmentos-listaGenerosMiembro": [
        {
          "value": "M",
          "label": "Male"
        },
        {
          "value": "F",
          "label": "Female"
        }
      ],
      "segmentos-listaAtributos": [
        {
          "value": "B",
          "label": "Gender"
        },
        {
          "value": "C",
          "label": "Civil status"
        },
        {
          "value": "D",
          "label": "Purchase frequency"
        },
        {
          "value": "E",
          "label": "Name"
        },
        {
          "value": "F",
          "label": "Last name"
        },
        {
          "value": "W",
          "label": "Surname "
        },
        {
          "value": "X",
          "label": "Email"
        },
        {
          "value": "H",
          "label": "Status"
        },
        {
          "value": "I",
          "label": "Start tier"
        },
        {
          "value": "J",
          "label": "Tier in progress"
        },
        {
          "value": "K",
          "label": "City"
        },
        {
          "value": "L",
          "label": "State(residencia)"
        },
        {
          "value": "M",
          "label": "Country"
        },
        {
          "value": "N",
          "label": "ZIP Code"
        },
        {
          "value": "A",
          "label": "Birthdate"
        },
        {
          "value": "O",
          "label": "Education"
        },
        {
          "value": "P",
          "label": "Income"
        },
        {
          "value": "Q",
          "label": "Children"
        },
        {
          "value": "R",
          "label": "Dynamic Attributes"
        },
        {
          "value": "G",
          "label": "Miembro del sistema"
        }
      ],
      "segmentos-operadoresTexto": [
        {
          "value": "A",
          "label": "Is"
        },
        {
          "value": "B",
          "label": "Is not"
        },
        {
          "value": "E",
          "label": "Start with"
        },
        {
          "value": "F",
          "label": "Finishes with"
        },
        {
          "value": "G",
          "label": "Has"
        },
        {
          "value": "H",
          "label": "Doesn´t have"
        },
        {
          "value": "I",
          "label": "Empty"
        },
        {
          "value": "J",
          "label": "Is not empty"
        }
      ],
      "segmentos-operadoresFecha": [
        {
          "value": "O",
          "label": "Before"
        },
        {
          "value": "P",
          "label": "Start on"
        },
        {
          "value": "Q",
          "label": "Equal to"
        }
      ],
      "segmentos-operadoresNumericos": [
        {
          "value": "V",
          "label": "Equal"
        },
        {
          "value": "W",
          "label": "Greater than"
        },
        {
          "value": "X",
          "label": "Greater or Equal to"
        },
        {
          "value": "Y",
          "label": "Less than"
        },
        {
          "value": "Z",
          "label": "Less or Equal to"
        }
      ],
      "segmentos-listaBalancesMetrica": [
        {
          "value": "A",
          "label": "Accumulated"
        },
        {
          "value": "B",
          "label": "Expired"
        },
        {
          "value": "C",
          "label": "Redeemed"
        },
        {
          "value": "D",
          "label": "Available"
        }
      ],
      "segmentos-listaBalancesMetricaBalance": [
        {
          "value": "B",
          "label": "Expired"
        },
        {
          "value": "C",
          "label": "Redeemed"
        },
        {
          "value": "D",
          "label": "Available"
        }
      ],
      "segmentos-listaPeriodoTiempo": [
        {
          "value": "B",
          "label": "Last"
        },
        {
          "value": "C",
          "label": "Previous"
        },
        {
          "value": "D",
          "label": "From"
        },
        {
          "value": "H",
          "label": "To"
        },
        {
          "value": "E",
          "label": "Between"
        },
        {
          "value": "F",
          "label": "Current month"
        },
        {
          "value": "G",
          "label": "Current year"
        }
      ],
      "segmentos-listaCalendarizacionReglasAvanzadas": [
        {
          "value": "B",
          "label": "In the last"
        },
        {
          "value": "D",
          "label": "From"
        },
        {
          "value": "H",
          "label": "To"
        },
        {
          "value": "E",
          "label": "Between"
        },
        {
          "value": "F",
          "label": "Current month"
        },
        {
          "value": "G",
          "label": "Current year"
        }
      ],
      "segmentos-listaTipoRegla": [
        {
          "value": "AT",
          "label": "Member Attribute"
        },
        {
          "value": "MB",
          "label": "Metric Balance"
        },
        {
          "value": "MC",
          "label": "Metric Change"
        },
        {
          "value": "AV",
          "label": "Advanced Rule"
        },
        {
          "value": "EP",
          "label": "Preference"
        },
        {
          "value": "EE",
          "label": "Survey Response"
        }
      ],
      "segmento-encuesta-operadores-selecc-multiple": [
        {
          "label": "Exactly",
          "value": "U"
        },
        {
          "label": "Any",
          "value": "K"
        }
      ],
      "segmento-encuesta-operadores-selecc-unica": [
        {
          "label": "Any",
          "value": "K"
        }
      ],
      "segmento-regla-operador-fijo": [
        {
          "value": "A",
          "label": "Is"
        },
      ],
      "preguntas-respuestas": [
        {
          "label": "Respuesta 1",
          "value": "1"
        },
        {
          "label": "Respuesta 2",
          "value": "2"
        },
        {
          "label": "Respuesta 3",
          "value": "3"
        },
        {
          "label": "Respuesta 4",
          "value": "4"
        }
      ],
      "preguntas-itemsPuntos": [
        {
          "label": "100",
          "value": "100"
        },
        {
          "label": "200",
          "value": "200"
        },
        {
          "label": "300",
          "value": "300"
        },
        {
          "label": "400",
          "value": "400"
        },
        {
          "label": "500",
          "value": "500"
        },
        {
          "label": "750",
          "value": "750"
        },
        {
          "label": "1000",
          "value": "1000"
        },
        {
          "label": "1500",
          "value": "1500"
        },
        {
          "label": "2000",
          "value": "2000"
        },
        {
          "label": "3000",
          "value": "3000"
        },
        {
          "label": "5000",
          "value": "5000"
        },
        {
          "label": "7500",
          "value": "7500"
        },
        {
          "label": "10000",
          "value": "10000"
        },
        {
          "label": "15000",
          "value": "15000"
        },
        {
          "label": "30000",
          "value": "30000"
        }
      ],
      "notificaciones": [],
      "notificaciones-elegibles": [
        {
          "label": "Segments",
          "value": "S"
        },
        {
          "label": "Members",
          "value": "M"
        }
      ],
      "notificaciones-tipo": [
        {
          "value": "E",
          "label": "E-Mail"
        },
        {
          "value": "P",
          "label": "Push Notification"
        }
      ],
      "notificaciones-objetivos": [
        {
          "value": null,
          "label": "None"
        },
        {
          "value": "P",
          "label": "Offer"
        },
        {
          "value": "T",
          "label": "Reward type"
        }
      ],
      "notificaciones-disparador": [
        {
          "value": "M",
          "label": "Manual"
        },
        {
          "value": "D",
          "label": "Triggered"
        }
      ],
      "misiones-elegibles-itemsIndComponente": [
        {
          "value": "S",
          "label": "Segments"
        },
        {
          "value": "M",
          "label": "Members"
        }
      ],
      "misiones-itemsTipoMision": [
        {
          "value": null,
          "label": "Choose an option"
        },
        {
          "value": "E",
          "label": "Survey"
        },
        {
          "value": "P",
          "label": "Profile"
        },
        {
          "value": "V",
          "label": "Watch Content"
        },
        {
          "value": "S",
          "label": "Upload Content"
        },
        {
          "value": "R",
          "label": "Social Challenge"
        },
        {
          "value": "J",
          "label": "Game"
        },
        {
          "value": "A",
          "label": "Preference"
        },
        {
          "value": "B",
          "label": "Augmented Reality"
        }
      ],
      "misiones-itemsAprobacion": [
        {
          "value": null,
          "label": "Choose an option"
        },
        {
          "value": "A",
          "label": "Automatic"
        },
        {
          "value": "B",
          "label": "Manual"
        }
      ],
      "misiones-contenido-optionsTiposContenido": [
        {
          "value": null,
          "label": "Choose an option"
        },
        {
          "label": "Picture",
          "value": "I"
        },
        {
          "label": "Video",
          "value": "V"
        }
      ],
      "misiones-perfil-atributos": [
        {
          "indAtributo": "A",
          "indRequerido": false,
          "label": "Birth Date"
        },
        {
          "indAtributo": "B",
          "indRequerido": false,
          "label": "Gender"
        },
        {
          "indAtributo": "C",
          "indRequerido": false,
          "label": "Civil State"
        },
        {
          "indAtributo": "D",
          "indRequerido": false,
          "label": "Purchase Frequency"
        },
        {
          "indAtributo": "E",
          "indRequerido": false,
          "label": "Name"
        },
        {
          "indAtributo": "F",
          "indRequerido": false,
          "label": "Last Name"
        },
        {
          "indAtributo": "S",
          "indRequerido": false,
          "label": "Surname"
        },
        {
          "indAtributo": "K",
          "indRequerido": false,
          "label": "City"
        },
        {
          "indAtributo": "L",
          "indRequerido": false,
          "label": "State"
        },
        {
          "indAtributo": "M",
          "indRequerido": false,
          "label": "Country"
        },
        {
          "indAtributo": "N",
          "indRequerido": false,
          "label": "ZIP Code"
        },
        {
          "indAtributo": "O",
          "indRequerido": false,
          "label": "Education"
        },
        {
          "indAtributo": "P",
          "indRequerido": false,
          "label": "Income"
        },
        {
          "indAtributo": "Q",
          "indRequerido": false,
          "label": "Children"
        }
      ],
      "misiones-juego-intervalos": [
        {
          "label": "Choose an Interval",
          "value": ""
        },
        {
          "label": "Always",
          "value": "A"
        },
        {
          "label": "Minute",
          "value": "B"
        },
        {
          "label": "Hour",
          "value": "C"
        },
        {
          "label": "Day",
          "value": "D"
        },
        {
          "label": "Week",
          "value": "E"
        },
        {
          "label": "Month",
          "value": "F"
        }
      ],
      "misiones-juego-itemstipo": [
        {
          "label": "Choose an option",
          "value": null
        },
        {
          "label": "Roulette",
          "value": "R"
        },
        {
          "label": "Scratch-it",
          "value": "A"
        },
        {
          "label": "Puzzle",
          "value": "O"
        },
        {
          "label": "Who Wants to Be a Millionaire",
          "value": "M"
        }
      ],
      "misiones-juego-estrategiaItems": [
        {
          "label": "Choose an option",
          "value": null
        },
        {
          "label": "Probability",
          "value": "P"
        },
        {
          "label": "Random",
          "value": "R"
        }
      ],
      "misiones-juego-tipoPremioItems": [
        {
          "label": "Reward",
          "value": "P"
        },
        {
          "label": "Metric",
          "value": "M"
        },
        {
          "label": "Thanks",
          "value": "G"
        }
      ],
      "misiones-contenido-itemsRespuestaCorrectaMultiple": [
        {
          "label": "All are correct",
          "value": "T"
        },
        {
          "label": "Exactly the selected answers",
          "value": "G"
        },
        {
          "label": "Any of the selected answers",
          "value": "C"
        }
      ],
      "misiones-contenido-itemsRespuestaCorrecta": [
        {
          "label": "All are correct",
          "value": "T"
        },
        {
          "label": "Any of the selected answers",
          "value": "C"
        }
      ],
      "misiones-contenido-itemsTipoPregunta": [
        {
          "label": "Survey",
          "value": "E"
        },
        {
          "label": "Rating",
          "value": "C"
        }
      ],
      "misiones-contenido-itemsTipoRespuesta": [
        {
          "label": "Multiple Choice",
          "value": "RM"
        },
        {
          "label": "Single Choice",
          "value": "RU"
        },
        {
          "label": "Text - Open",
          "value": "RT"
        },
        {
          "label": "Numeric - Open",
          "value": "RN"
        }
      ],
      "misiones-contenido-itemsRespuestaCorrectaCalificacion": [
        {
          "label": "Less or equal than",
          "value": "D"
        },
        {
          "label": "Equal",
          "value": "E"
        },
        {
          "label": "Less or equal than",
          "value": "F"
        }
      ],
      "misiones-contenido-itemsTipo": [
        {
          "label": "Url",
          "value": "U"
        },
        {
          "label": "Video",
          "value": "V"
        },
        {
          "label": "Picture",
          "value": "I"
        }
      ],
      "misiones-actividad-social-optionsTiposActividad": [
        {
          "label": "Share (link)",
          "value": "L"
        },
        {
          "label": "Share (message)",
          "value": "M"
        },
        {
          "label": "Like",
          "value": "G"
        },
        {
          "label": "Tweet",
          "value": "A"
        },
        {
          "label": "Twitter Likes",
          "value": "B"
        },
        {
          "label": "Instagram Likes",
          "value": "C"
        }
      ],
      "promociones": [],
      "promociones-detalle-itemsDetalle": "Delete",
      "promociones-alcance-vistas-totales": "Total Views",
      "promociones-alcance-vistas-unicas": "Unique Views",
      "promociones-alcance-marcas-promedio": "Likes",
      "promociones-alcance-elegibles-promedio": "Targets(average)",
      "promociones-items-codigo": [
        {
          "value": null,
          "label": "None"
        },
        {
          "value": "A",
          "label": "URL"
        },
        {
          "value": "B",
          "label": "Code QR"
        },
        {
          "value": "C",
          "label": "ITF"
        },
        {
          "value": "D",
          "label": "EAN"
        },
        {
          "value": "E",
          "label": "Code 128"
        },
        {
          "value": "F",
          "label": "Code 39"
        }
      ],
      "promociones-itemsIndComponente": [
        {
          "value": "S",
          "label": "Segments"
        },
        {
          "value": "M",
          "label": "Members"
        }
      ],
      "promociones-items-calendarizacion": [
        {
          "label": "Permanent",
          "value": "P"
        },
        {
          "label": "Scheduled",
          "value": "C"
        }
      ],
      "tarea-inicio": "Start",
      "tarea-enviar-promocion": "Send offer",
      "tarea-enviar-mensaje": "Send message",
      "tarea-enviar-insignia": "Send badge",
      "tarea-enviar-premio": "Send award",
      "tarea-enviar-mision": "Send challenge",
      "tarea-enviar-metrica": "Grant metric",
      "workflow-items-codigo": [
        {
          "value": "A",
          "label": "Join the program"
        },
        {
          "value": "B",
          "label": "Redeemed offer"
        },
        {
          "value": "C",
          "label": "Challenge completed"
        },
        {
          "value": "D",
          "label": "Reward redeemed"
        },
        {
          "value": "E",
          "label": "Bought an specific product"
        },
        {
          "value": "H",
          "label": "Bought products"
        },
        {
          "value": "F",
          "label": "Birthday"
        },
        {
          "value": "G",
          "label": "Bought products"
        },
        {
          "vFalue": "I",
          "label": "Customized event"
        },
        {
          "value": "J",
          "label": "Anniversary of joining the program"
        },
        {
          "value": "M",
          "label": "Mission sequence"
        }
      ],
      "workflow-tipo-tarea": [
        {
          "label": "Send offer",
          "value": "A"
        },
        {
          "label": "Send message",
          "value": "B"
        },
        {
          "label": "Grant badge",
          "value": "C"
        },
        {
          "label": "Gift award",
          "value": "D"
        },
        {
          "label": "Send challenge",
          "value": "E"
        },
        {
          "label": "Grant Metric",
          "value": "F"
        }
      ],
      "leaderboard-itemsTipoFormato": [
        {
          "label": "Name",
          "value": "N"
        },
        {
          "label": "Initials",
          "value": "I"
        }
      ],
      "leaderboard-itemsTipoTabla": [
        {
          "label": "Group",
          "value": "E"
        },
        {
          "label": "Team",
          "value": "G"
        },
        {
          "label": "Members",
          "value": "U"
        }
      ],
      "leaderboard-indGrupalFormaCalculo": [
        {
          "label": "Sum",
          "value": "S"
        },
        {
          "label": "Average",
          "value": "P"
        }
      ],
      "general-default-select": {
        "value": null,
        "label": "Choose an Option"
      },
      "atributo-miembro-tipoItems": [
        {
          "label": "Choose an option",
          "value": null
        },
        {
          "label": "Numeric",
          "value": "N"
        },
        {
          "label": "Text",
          "value": "T"
        },
        {
          "label": "Boolean",
          "value": "B"
        },
        {
          "label": "Date",
          "value": "F"
        }
      ],
      "configuracion-protocolo-smtp": [
        {
          "label": "Choose a Protocol",
          "value": null
        },
        {
          "label": "SSL Protocol",
          "value": "S"
        },
        {
          "label": "TLS Protocol",
          "value": "T"
        },
        {
          "label": "No Protocol",
          "value": "N"
        }
      ],
      "insignia-tipo": [
        {
          "label": "Choose an option",
          "value": ""
        },
        {
          "label": "Status",
          "value": "S"
        },
        {
          "label": "Collector",
          "value": "C"
        }
      ],
      "documento-inventario-tipo": [
        {
          "label": "Purchase",
          "value": "C"
        },
        {
          "label": "Negative adjustment",
          "value": "A"
        },
        {
          "label": "Positive adjustment",
          "value": "M"
        },
        {
          "label": "Inter-location transfer",
          "value": "I"
        },
        {
          "label": "Redemption",
          "value": "R"
        }
      ],
      "booleano-general": [
        {
          "label": "True",
          "value": "true"
        },
        {
          "label": "False",
          "value": "false"
        }
      ],
      "miembro-educacion": [
        {
          "label": "Choose an option",
          "value": null
        },
        {
          "label": "School",
          "value": "P"
        },
        {
          "label": "High school",
          "value": "S"
        },
        {
          "label": "Technical",
          "value": "T"
        },
        {
          "label": "Graduate",
          "value": "U"
        },
        {
          "label": "Degree",
          "value": "G"
        },
        {
          "label": "Master",
          "value": "M"
        },
        {
          "label": "Postgraduate",
          "value": "R"
        },
        {
          "label": "Doctorate",
          "value": "D"
        }
      ],
      "miembro-genero": [
        {
          "label": "Choose an option",
          "value": null
        },
        {
          "label": "Female",
          "value": "F"
        },
        {
          "label": "Male",
          "value": "M"
        }
      ],
      "miembro-estado-civil": [
        {
          "label": "Choose an option",
          "value": null
        },
        {
          "label": "Married",
          "value": "C"
        },
        {
          "label": "Single",
          "value": "S"
        },
        {
          "label": "Free union",
          "value": "U"
        }
      ],
      "premio-tipoPremio": [
        {
          "label": "Choose an option",
          "value": null
        },
        {
          "label": "Certificate",
          "value": "C"
        },
        {
          "label": "Physical Product",
          "value": "P"
        }
      ],
      "elegibles-general": [
        {
          "label": "Choose an option",
          "value": ""
        },
        {
          "label": "Segments",
          "value": "S"
        },
        {
          "label": "Members",
          "value": "M"
        }
      ],
      "tipos-elegibles-general": [
        {
          "label": "Available",
          "value": "D"
        },
        {
          "label": "Included",
          "value": "I"
        },
        {
          "label": "Excluded",
          "value": "E"
        }
      ],
      "accion-elegibles-general": [
        {
          "label": "Include",
          "value": "I"
        },
        {
          "label": "Exclude",
          "value": "E"
        }
      ],
      "dias-efectividad-general": [
        {
          "label": "Monday",
          "value": "L"
        },
        {
          "label": "Tuesday",
          "value": "K"
        },
        {
          "label": "Wednesday",
          "value": "M"
        },
        {
          "label": "Thursday",
          "value": "J"
        },
        {
          "label": "Friday",
          "value": "V"
        },
        {
          "label": "Saturday",
          "value": "S"
        },
        {
          "label": "Sunday",
          "value": "D"
        }
      ],
      "tipo-efectividad-general": [
        {
          "label": "Choose an option",
          "value": null
        },
        {
          "label": "Permanent",
          "value": "P"
        },
        {
          "label": "Scheduled",
          "value": "C"
        }
      ],
      "atributo-producto-tipoItems": [
        {
          "label": "Choose an option",
          "value": null
        },
        {
          "label": "Numeric",
          "value": "N"
        },
        {
          "label": "Text",
          "value": "T"
        },
        {
          "label": "Multiple",
          "value": "M"
        }
      ],
      "metrica-medida-tipo": [
        {
          "label": "Metric based on ...",
          "value": ""
        },
        {
          "label": "Points",
          "value": "P"
        },
        {
          "label": "Visits",
          "value": "V"
        },
        {
          "label": "Activity",
          "value": "A"
        },
        {
          "label": "Duration",
          "value": "U"
        },
        {
          "label": "Distance",
          "value": "D"
        },
        {
          "label": "Spending",
          "value": "G"
        }
      ],
      "estado-redencion-premio": [
        {
          "label": "Pending",
          "value": "S"
        },
        {
          "label": "Retired",
          "value": "R"
        },
        {
          "label": "Canceled",
          "value": "A"
        }
      ],
      "trabajo-interno-tipo": [
        {
          "label": "Choose an option",
          "value": null
        },
        {
          "label": "Notification",
          "value": "N"
        },
        {
          "label": "Import",
          "value": "I"
        },
        {
          "label": "Export",
          "value": "E"
        }
      ],
      "trabajo-interno-estado": [
        {
          "label": "Choose an option",
          "value": null
        },
        {
          "label": "Procesando",
          "value": "P"
        },
        {
          "label": "Failed",
          "value": "I"
        },
        {
          "label": "Completed",
          "value": "E"
        }
      ],
      "id-miembro-importacion":[
        {
          "label": "UUID",
          "value": "idMiembro"
        },
        {
          "label": "Identification",
          "value": "docIdentificacion"
        }
      ],
      "importacion-miembro-segmento": [
        {
          "label": "UUID",
          "value": "ID_MIEMBRO"
        },
        {
          "label": "Identification",
          "value": "DOC_IDENTIFICACION"
        }
      ],
      "atributo-tipo":[
        {
          "label": "Static",
          "value": "E"
        },
        {
          "label": "Dynamic",
          "value": "D"
        }
      ],
      "atributos-estaticos": [
        {
          "value": "NOMBRE",
          "label": "Name"
        },
        {
          "value": "APELLIDO",
          "label": "Last name"
        },
        {
          "value": "APELLIDO2",
          "label": "Surname"
        },
        {
          "value": "FECHA_NACIMIENTO",
          "label": "Birthdate"
        },
        {
          "value": "CIUDAD_RESIDENCIA",
          "label": "City"
        },
        {
          "value": "DIRECCION",
          "label": "Address"
        },
        {
          "value": "ESTADO_RESIDENCIA",
          "label": "State(residencia)"
        },
        {
          "value": "PAIS_RESIDENCIA",
          "label": "Country"
        },
        {
          "value": "CODIGO_POSTAL",
          "label": "ZIP Code"
        },
        {
          "value": "CORREO",
          "label": "Email"
        },
        {
          "value": "CONTRASENA",
          "label": "Password"
        },
        {
          "value": "IND_EDUCACION",
          "label": "Education"
        },
        {
          "value": "IND_ESTADO_CIVIL",
          "label": "Civil state"
        },
        {
          "value": "IND_GENERO",
          "label": "Gender"
        },   
        {
          "value": "IND_HIJOS",
          "label": "Children"
        },
        {
          "value": "INGRESO_ECONOMICO",
          "label": "Income"
        },
        {
          "value": "TELEFONO_MOVIL",
          "label": "Telephone"
        },
        {
          "value": "IND_CONTACTO_EMAIL",
          "label": "Email suscribe"
        },
        {
          "value": "IND_CONTACTO_ESTADO",
          "label": "Statement Suscribe"
        },
        {
          "value": "IND_CONTACTO_NOTIFICACION",
          "label": "Push notification Suscribed"
        },
        {
          "value": "IND_CONTACTO_SMS",
          "label": "SMS Suscribed"
        }
      ],
      "mision-aprobacion-estado":[
        {
          "label": "Gained",
          "value": "G"
        },
        {
          "label": "Failed",
          "value": "F"
        },
        {
          "label": "Pendant",
          "value": "P"
        }
      ],
      "respuesta-preferencia-tipo":[
        {
          "label": "Single Choice",
          "value": "SU"
        },
        {
          "label": "Multiple Choice",
          "value": "RM"
        },
        {
          "label": "Text",
          "value": "TX"
        }
      ],
      "miembro-suspencion": {
        "severity": "success",
        "summary": "Suspendido",
        "detail": "The member has been suspended."
      },
      "miembro-inactivo": {
        "severity": "info",
        "summary": "Inactivo",
        "detail": "El miembro ha sido suspendido."
      },
      "usuarios-error-username": {
        "severity": "error",
        "summary": "Error",
        "detail": "The user name already exists"
      },
      "usuario-correo": {
        "severity": "error",
        "summary": "Error",
        "detail": "The email already exists"
      },
      "segmentos-info-procesando": {
        "severity": "info",
        "summary": "Processing",
        "detail": "The segment is being processed, please wait"
      },
      "segmentos-enviado-procesamiento": {
        "severity": "info",
        "summary": "Sent",
        "detail": "The request has been sent to the server"
      },
      "segmentos-miembros-select": {
        "detail": "Choose one or more users",
        "severity": "info",
        "summary": ""
      },
      "general-lista-vacia": {
        "severity": "info",
        "summary": "Info",
        "detail": "No records found"
      },
      "general-insercion": {
        "detail": "The data has been successfully inserted",
        "severity": "success",
        "summary": "Inserted"
      },
      "general-actualizacion": {
        "detail": "The data has been successfully updated",
        "severity": "success",
        "summary": "Updated"
      },
      "general-eliminacion": {
        "detail": "The data has been successfuly deleted",
        "severity": "success",
        "summary": "Deleted"
      },
      "info-uploading": {
        "detail": "Loading image",
        "severity": "info",
        "summary": "Wait"
      },
      "general-inclusion-simple": {
        "detail": "The item has been successfully added",
        "severity": "success",
        "summary": "Added"
      },
      "general-inclusion-multiple": {
        "detail": "The items has been successfully added",
        "severity": "success",
        "summary": "Added"
      },
      "general-exclusion-multiple": {
        "detail": "The items have been successfully deleted",
        "severity": "success",
        "summary": "Deleted"
      },
      "general-exclusion-simple": {
        "detail": "The item has been successfully deleted",
        "severity": "success",
        "summary": "Deleted"
      },
      "general-eliminacion-multiple": {
        "detail": "The items have been successfully deleted",
        "severity": "success",
        "summary": "Deleted"
      },
      "general-eliminacion-simple": {
        "detail": "The item has been successfully deleted",
        "severity": "success",
        "summary": "Deleted"
      },
      "general-reporte-creacion": {
        "detail": "The report has been successfully created",
        "severity": "success",
        "summary": "Created"
      },
      "general-actualizando-datos": {
        "detail": "Updating data",
        "severity": "info",
        "summary": "Wait"
      },
      "general-archivar": {
        "detail": "The items have been successfully arcived",
        "severity": "success",
        "summary": "Archived"
      },
      "general-publicar": {
        "detail": "The item have been published successfully",
        "severity": "success",
        "summary": "Published"
      },
      "general-activar": {
        "detail": "The member have been activated.",
        "severity": "success",
        "summary": "Active"
      }, 
      "general-desactivar": {
        "detail": "The element was deactivated correctly.",
        "severity": "success",
        "summary": "Inactivo"
      },
      "reporte-espera-generar": {
        "detail": "You must wait for the previous certificate generation to complete, please try again later.",
        "severity": "info",
        "summary": "Información"
      },
      "documento-cerrado":{
        "detail": "The document is closed.",
        "severity": "success",
        "summary": "Cerrado"
      },
      "cierre-mes":{
        "detail": "O fechamento do mês foi feito.",
        "severity": "success",
        "summary": "Cerrado"
      },
      "respuesta-mision-aprobar":{
        "detail": "La respuesta del miembro fue aprobada.",
        "severity": "success",
        "summary":"Gano"
      },
      "respuesta-mision-rechazar":{
        "detail": "La respuesta del miembro fue rechazada.",
        "severity": "success",
        "summary":"Fallo"
      },
      "procesando-importacion":{
        "detail": "The import is being processed.",
        "severity": "info",
        "summary":"Information"
      },
      "error-datos-incorrectos": {
        "detail": "You must include at least one question",
        "severity": "error",
        "summary": "Invalid data"
      },
      "error-conexion": {
        "detail": "A connection error has occurred",
        "severity": "error",
        "summary": "Error"
      },
      "error-nombre-interno": {
        "detail": "Internal name already exists",
        "severity": "error",
        "summary": "Invalid data"
      },
      "error-url-invalido": {
        "detail": "The URL format is not valid",
        "severity": "error",
        "summary": "Error"
      },
      "error-notificacion-texto": {
        "detail": "You must specify the notification text before",
        "severity": "error",
        "summary": "Error"
      },
      "error-misiones-pregunta": {
        "detail": "You must include at least one question",
        "severity": "error",
        "summary": "Invalid data"
      },
      "error-leaderboard-grupo": {
        "detail": "Select a group...",
        "severity": "error",
        "summary": "Error"
      },
      "error-codigo-premio":{
        "detail": "The reward code isn't valid",
        "severity": "error",
        "summary": "Error"
      },
      "warn-misiones-premio-juego": {
        "detail": "Select a reward.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-misiones-metrica-juego": {
        "detail": "Provide the quantity to be delivered.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-misiones-tipo-juego": {
        "detail": "Select a type.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-misiones-juego-estrategia": {
        "detail": "Select a strategy.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-misiones-juego-imagen": {
        "detail": "Select an image.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-misiones-juego-rompecabeza": {
        "detail": "Rewards Qty was exceeded, delete rewards to change the challenge.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-misiones-juego-raspadita": {
        "detail": "Rewards Qty was exceeded, delete rewards to change the challenge.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-misiones-juego-cambio": {
        "detail": "The challenge status has changed, its not possible to enter a reward.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-misiones-juego-tiempo": {
        "detail": "The time limit must be greater than 10 seconds.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-misiones-juego-cantidadPiezas": {
        "detail": "The number of pieces must be in the range of 6 to 16.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-probabilidad": {
        "detail": "The probability value must be indicated.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-repoert-periodo": {
        "detail": "The period is out of range, try again.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-repoert-mes": {
        "detail": "The mounth is out of range, try again.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-atributo-valor": {
        "detail": "Verify the attribute value to continue.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-atributo-respuesta": {
        "detail": "Debe ingresar al menos dos respuestas.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-resultado": {
        "detail": "No results found",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-ubicacion-geocoder": {
        "detail": "Geocoder faild due to: ",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-ubicacion-fecha": {
        "detail": "Verifique el rango de fechas seleccionado para continuar con la actualización.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-ubicacion-semana": {
        "detail": "Debe indicar la semana y los días de recurrencia para continuar.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-ubicacion-destino": {
        "detail": "Select a location of destiny.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-origen": {
        "detail": "Select a location of origin.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-igual": {
        "detail": "Location of origin and destiny, should be different.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-proveedor": {
        "detail": "Select a provider.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-workflow-disparador": {
        "detail": "Debe seleccionar el elemento para el disparador según el tipo de acción.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-dato-valido": {
        "detail": "Debe ingresar el dato solicitado para continuar.",
        "severity": "warn",
        "summary": "Alert"
      },
      "warn-cantidad": {
        "detail": "Debe seleccionar al menos 1 elemento para continuar.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-precio-premio": {
        "detail": "Enter the price of the prize.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-cantidad-premio": {
        "detail": "Enter the amount of rewards.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "pipe-calendarizacion": {
        "Permanente": "Permanent",
        "Calendarizado": "Scheduled",
        "Recurrente": "Recurrent"
      },
      "pipe-estado-campana": {
        "Borrador": "Draft",
        "Publicado": "Published",
        "Archivado": "Archived",
        "Ejecutado": "Executing"
      },
      "pipe-tipo-segmento": {
        "Estático": "Static",
        "Dinamico": "Dynamic"
      },
      "pipe-status-trabajo": {
        "RUNNING": "Executing",
        "SUBMITTED": "Queued",
        "FAILED": "Failed",
        "FINISHED": "Completed",
        "KILLED": "Canceled",
        "STOPPED": "Stopped"
      },
      "pipe-evento-campana": {
        "Manual": "Manual",
        "Calendarizado": "Scheduled",
        "Trigger": "Trigger"
      },
      "pipe-ind-operador": {
        "AND": "AND",
        "OR": "OR",
        "NOTIN": "NOT IN"
      },
      "pipe-tipo-insignia": {
        "Status": "Status",
        "Collector": "Collector"
      },
      "pipe-regla-tipo-cambio": {
        "A": "Get",
        "B": "Expire",
        "C": "Redeem"
      },
      "pipe-regla-avanzada-ind-ultimo": {
        "D": "Day(s)",
        "A": "Year(s)",
        "M": "Month(es)"
      },
      "pipe-tipo-notificacion": {
        "P": "Push Notification",
        "E": "Email"
      },
      "pipe-tipo-leaderboard": {
        "E": "Group",
        "U": "User",
        "G": "Team"
      },
      "pipe-estado-segmento": {
        "AR": "Archived",
        "BO": "Draft",
        "AC": "Active"
      },
      "pipe-estado-workflow": {
        "A": "Active",
        "B": "Archived",
        "C": "Draft"
      },
      "pipe-estado-usuario": {
        "B": "Active",
        "I": "Inactive"
      },
      "pipe-balance-metrica": {
        "A": "accumulated",
        "B": "expired",
        "C": "redeemed",
        "D": "available"
      },
      "pipe-atributo-tipoDato": {
        "T": "Text",
        "F": "Date",
        "B": "Boolean",
        "N": "Numeric",
        "M": "Multiple"
      },
      "pipe-atributo-visibilidad": {
        "true": "Administrators and mobile app",
        "false": "Administrators only"
      },
      "pipe-cambio-estado": {
        "A": "Active",
        "B": "Draft",
        "I": "Inactive"
      },
      "pipe-certificado-estado": {
        "D": "Available",
        "A": "Cancelled",
        "O": "Occupied"
      },
      "pipe-dato-booleano": {
        "true": "True",
        "false": "False"
      },
      "pipe-documento-estado": {
        "true": "Closed",
        "false": "Open"
      },
      "pipe-documento-tipo": {
        "C": "Purchase",
        "R": "Redemption",
        "A": "Positive adjustment",
        "M": "Negative adjustment",
        "I": "Inter-location transfer"
      },
      "pipe-estado-elemento": {
        "B": "Draft",
        "P": "Published",
        "A": "Archived",
        "I": "Published",
        "D": "Draft"
      },
      "pipe-metrica-medida": {
        "P": "Points",
        "V": "Visits",
        "A": "Activity",
        "U": "Duration",
        "D": "Distance",
        "G": "Spending"
      },
      "pipe-miembro-educacion": {
        "P": "School",
        "S": "High school",
        "T": "Technical",
        "U": "Graduate",
        "G": "Degree",
        "M": "Master",
        "R": "Postgraduate",
        "D": "Doctorate"
      },
      "pipe-miembro-estadoCivil": {
        "C": "Married",
        "S": "Single",
        "U": "Free union"
      },
      "pipe-miembro-estado": {
        "A": "Active",
        "I": "Inactive"
      },
      "pipe-miembro-genero": {
        "M": "Male",
        "F": "Female"
      },
      "pipe-objetivo-campana": {
        "M": "Challenge",
        "P": "Offer",
        "R": "Reward"
      },
      "pipe-tipo-premioJuego": {
        "P": "Reward",
        "M": "Metric",
        "G": "Gratitude"
      },
      "pipe-premio-tipo": {
        "P": "Physical Product",
        "C": "Certificate"
      },
      "pipe-tipo-juegoMision": {
        "R": "Roulette",
        "A": "Scratch-it",
        "O": "Puzzle"
      },
      "pipe-estado-redencion": {
        "S": "Pending",
        "R": "Retired",
        "A": "Canceled"
      },
      "pipe-estado-trabajo-interno": {
        "P": "Processing",
        "F": "Failed",
        "C": "Completed"
      },
      "pipe-tipo-trabajo-interno": {
        "N": "Notification",
        "E": "Export",
        "I": "Import"
      },
      "pipe-tipo-pregunta": {
        "E": "Survey",
        "C": "Rating"
      },
      "pipe-tipo-red-social": {
        "L": "Link",
        "M": "Message",
        "G": "Like",
        "A": "Tweet",
        "B": "Twitter Likes",
        "C": "Instagram Likes"
      },
      "reglas-asignacion-badge-tipo-regla": [
        {
          "label": "Challenge responses",
          "value": "A"
        },
        {
          "label": "Referals",
          "value": "B"
        },
        {
          "label": "Tier",
          "value": "C"
        },
        {
          "label": "Accumulated purchases",
          "value": "D"
        },
        {
          "label": "Purchase frequency",
          "value": "E"
        }
      ],
      "reglas-asignacion-premio-tipo-regla": [
        {
          "label": "Purchase frequency",
          "value": "A"
        },
        {
          "label": "Purchase amount",
          "value": "B"
        },
        {
          "label": "Accumulated purchases",
          "value": "C"
        }
      ],
      "reglas-asignacion-premio-fecha-calendarizacion": [
        {
          "label": "Day(s)",
          "value": "D"
        },
        {
          "label": "Week(s)",
          "value": "S"
        },
        {
          "label": "Month(s)",
          "value": "M"
        }
      ],
      "reglas-listaPeriodoTiempo": [
        {
          "value": "B",
          "label": "In the last"
        },
        {
          "value": "C",
          "label": "In the previous"
        },
        {
          "value": "D",
          "label": "From"
        },
        {
          "value": "H",
          "label": "To"
        },
        {
          "value": "E",
          "label": "Between"
        },
        {
          "value": "F",
          "label": "Current month"
        },
        {
          "value": "G",
          "label": "Current year"
        }
      ],
      "bitacora-accion": [
        {
          "label": "Inserted.",
          "value": "A"
        },
        {
          "label": "Updated",
          "value": "B"
        },
        {
          "label": "Deleted",
          "value": "C"
        },
        {
          "label": "Published",
          "value": "D"
        },
        {
          "label": "Archived",
          "value": "E"
        },
        {
          "label": "Desactivated",
          "value": "F"
        },
        {
          "label": "Active",
          "value": "G"
        },
        {
          "label": "Draft",
          "value": "H"
        }
      ],
      "confirmacion-pregunta": {
        "A": "Do you want to archive this item?",
        "B": "Do you want to delete this item?",
        "C": "Do you want to activate this item?",
        "D": "Do you want to suspend this member?",
        "E": "Do you want to suspend this user?",
        "F": "Desea inactivar este elemento?",
        "G": "¿Desea excluir este elemento?",
        "H": "¿Desea excluir esta lista de elementos?"
      },
      "metricas-dashboard-cant-acumulada": "Earned",
      "metricas-dashboard-cant-redimida": "Expired",
      "metricas-dashboard-cant-vencida": "Redeemed",
      "misiones-dashboard-cant-respuestas": "Qty of Responses",
      "misiones-dashboard-cant-respuestas-aprobadas": "Qty of Approved Responses",
      "misiones-dashboard-cant-respuestas-rechazadas": "Qty of Rejected Responses",
      "misiones-dashboard-cant-elegibles-promedio": "Members average",
      "misiones-dashboard-cant-cant-vistas-totales": "Total Views",
      "misiones-dashboard-cant-vistas-unicas": "Unique Views",
      "notificaciones-dashboard-cant-respuestas": "Qty of Responses",
      "notificaciones-dashboard-cant-msjs-enviados": "Sent Messages",
      "notificaciones-dashboard-cant-msjs-abiertos": "Open Messages",
      "notificaciones-dashboard-cant-miembros-enviados": "Sent members",
      "premios-dashboard-cant-redenciones": "Redemtions",
      "premios-dashboard-cant-elegibles-promedio": "Members average",
    };
  }
}