import { Injectable } from '@angular/core';

@Injectable()
export class LabelsES {
  public data: any;
  constructor() {
    this.data = {
      "breadcrumbs": {
        "dashboard": "Dashboard",
        "usuarios": "Usuarios",
        "insertarUsuario": "Insertar usuario",
        "listaUsuarios": "Lista de usuarios",
        "detalleUsuario": "Detalle de usuario",
        "detalleAtributos": "Detalle de atributos",
        "detalleRoles": "Detalle de roles",
        "insertarRoles": "Insertar roles",
        "listaRoles": "Lista de roles",
        "detalleRol": "Detalle de rol",
        "tipoMiembro": "Tipo de cliente",
        "miembros": "Clientes",
        "insertarMiembro": "Insertar cliente",
        "detalleMiembro": "Detalle de cliente",
        "perfil": "Perfil",
        "niveles": "Niveles",
        "grupos": "Grupos",
        "atributosDinamicos": "Atributos Dinámicos",
        "insignias": "Insignias",
        "preferencias": "Preferencias",
        "segmentos": "Segmentos",
        "redenciones": "Redenciones",
        "premios": "Premios",
        "retos": "Retos",
        "editarMiembro": "Editar cliente",
        "listaMiembros": "Lista de miembros",
        "ubicaciones": "Ubicaciones",
        "listaUbicaciones": "Lista ubicaciones",
        "insertarUbicacion": "Insertar ubicación",
        "detalleUbicacion": "Detalle de ubicación",
        "efectividad": "Efectividad de ubicación",
        "mapa": "Mapa",
        "metricas": "Metricas",
        "insertarMetrica": "Insertar métrica",
        "metricaLista": "Lista de métricas",
        "metricaDetalle": "Detalle de métrica",
        "detalleNivel": "Detalle de nivel",
        "gruposNivel ": "Lista Grupo de Tiers",
        "detalle": "Detalle del Grupo de Tiers",
        "leaderboards": "Leaderboards",
        "listaLeaderboards": "Lista de Leaderboards",
        "detalleLeaderboard": "Detalle de Leaderboards",
        "insertarLeaderboards": "Insertar Leaderboards",
        "insertarSegmento": "Insertar segmento",
        "listaSegmentos": "Lista de segmentos",
        "detalleSegmento": "Detalle de segmento",
        "atributos": "Detalle de atibutos",
        "reglas": "Lista de reglas",
        "detalleReglas": "Detalle de reglas",
        "elegibles": "Elegibles",
        "promociones": "Promociones",
        "listaPromos": "Lista de promociones",
        "insertarPromo": "Insertar promociones",
        "insertarCategoria": "Insertar categoría",
        "listaCategorias": "Lista categorías",
        "detallePromo": "Detalle de promoción",
        "common": "Atributos",
        "arte": "Arte",
        "alcance": "Desempeño",
        "limites": "Limites",
        "categorias": "Categorías",
        "detalleCategorias": "Detalle de categoría",
        "misiones": "Misiones",
        "insertarMision": "Insertar Misión",
        "detalleMision": "Detalle de Misión",
        "listaMisiones": "Lista de Misiones",
        "productos": "Productos",
        "detalleProducto": "Detalle Producto",
        "listaProductos": "Lista de productos",
        "insertarProducto": "Insertar producto",
        "informacion": "General",
        "display": "Despliegue",
        "listaCategoriasProducto": "Categorías de producto",
        "insertarCategProducto": "Insertar categoría de productos",
        "subcategorias": "Subcategorías",
        "calendarizacion": "Calendarización",
        "recompensas": "Recompensas",
        "notificaciones": "Notificaciones",
        "listaNotificaciones": "Lista de Notificaciones",
        "insertarNotificaciones": "Insertar Notificaciones",
        "detalleCampana": "Detalle Notificaciones",
        "insertar": "Insertar grupos",
        "detalleGrupo": "Detalle de grupos",
        "listaPreferencias": "Lista de preferencias",
        "insertarPreferencia": "Insertar preferencias",
        "detallePreferencias": "Detalle de preferencias",
        "listaInsignias": "Lista de insignias",
        "insertarInsignia": "Insertar insignia",
        "detalleInsignia": "Detalle insignia",
        "listaNivel": "Nivel inserción",
        "asignarMiembro": "Asignar a miembro",
        "listaAtributos": "Lista de Atributos",
        "detalleAtributo": "Detalle de atributo",
        "configuracionSistema": "Configuración del Sistema",
        "general": "General",
        "medios": "Medios de pago",
        "politica": "Política",
        "parametros": "Parámetros del sistema",
        "evertec": "Parámetros evertec del sisteme",
        "detallePremio": "Detalle Premio",
        "requerido": "Informacion requerida",
        "insertarPremio": "Insertar premio",
        "redimido": "Redimidos",
        "limite": "Límite",
        "listaCategoriasPremio": "Lista de categorías",
        "detalleCategoria": "Detalle de categoría",
        "asignar": "Asignar categoría",
        "certificados": "Certificados",
        "workflows": "Tareas Programadas",
        "listaWorkflow": "Lista de Tareas",
        "insertarWorkflow": "Insertar Tarea",
        "detalleWorkflow": "Detalle de Tarea",
        "inventario": "Inventario de Premio",
        "listaDocumentos": "Lista de documentos",
        "insertarDocumento": "Insertar documento",
        "infoDocumento": "Información de documento",
        "proveedor": "Lista de proveedores",
        "dashboard-general-metrica": "Dashboard Métricas",
        "dashboard-general-mision": "Dashboard Misiones",
        "dashboard-general-notificacion": "Dashboard Notificaciones",
        "dashboard-general-premio": "Dashboard Premios",
        "dashboard-general-promocion": "Dashboard Promociones",
        "reportes": "Reportes",
        "historico": "Histórico de movimientos",
        "compras": "Compras",
        "importacion": "Importación",
        "bitacora": "Bitácoras",
        "apiExternal": "Bitácora Api Externo",
        "apiAdministrativo": "Bitácora Api Administrativa",
        "noticia": "Noticias",
        "listaNoticias": "Lista de Noticias",
        "categoriasNoticia": "Categorías de Noticias",
        "aprobacion":"Gestion de Respuestas"
      },
      "tabs": {},
      "menu": {
        "juegos": "Juegos",
        "quien-quiere-ser-millonario": "Quien quiere ser millonario",
        "dashboard-miembros": "Dashboard Miembros",
        "dashboard-segmentos": "Dashboard Segmentos",
        "dashboard-programa": "Dashboard Programa",
        "dashboard-actividad": "Dashboard Actividad",
        "dashboard-metrica": "Dashboard Métrica",
        "ubicaciones": "Ubicaciones",
        "metricas": "Métricas",
        "grupos-tiers": "Grupos de Tiers",
        "adm-programa": "Adm. de Programa",
        "listado-usuarios": "Listado de Usuarios",
        "listado-roles": "Listado de Roles",
        "usuarios": "Usuarios",
        "miembros": "Miembros",
        "listado-miembros": "Listado de Miembros",
        "preferencias": "Preferencias",
        "atributos-dinamicos": "Atributos Dinámicos",
        "grupos-miembro": "Grupos de Miembro",
        "leaderboards": "Leaderboards",
        "segmentos": "Segmentos",
        "promociones": "Promociones",
        "listado-promociones": "Listado de Promociones",
        "actividad-promociones": "Actividad de Promociones",
        "listado-categorias": "Listado de Categorías",
        "insignias": "Insignias",
        "premios": "Premios",
        "listado-premios": "Listado de Premios",
        "actividad-premios": "Actividad de Premios",
        "categorias-premio": "Categorías de Premios",
        "unidades-medida": "Unidades de Medida",
        "notificaciones-listado": "Listado de Notificaciones",
        "actividad-notificaciones": "Actividad de Notificiaciones",
        "notificaciones": "Notificaciones",
        "misiones": "Misiones",
        "listado-misiones": "Listado de Misiones",
        "actividad-misiones": "Actividad de Misiones",
        "categorias-mision": "Categorías de Misión",
        "productos": "Productos",
        "listado-productos": "Listado de Productos",
        "categorias-productos": "Categorías de Producto",
        "atributos-producto": "Atributos de Producto",
        "workflow": "Tareas Programadas",
        "documentos-listado": "Listado de Documentos",
        "proveedores-listado": "Listado de Proveedores",
        "inventario-premio": "Inventario de Premio",
        "dashboard-general-metrica": "Dashboard Métricas",
        "dashboard-general-mision": "Dashboard Misiones",
        "dashboard-general-notificacion": "Dashboard Notificaciones",
        "dashboard-general-premio": "Dashboard Premio",
        "dashboard-general-promocion": "Dashboard Promociones",
        "reportes": "Reportes",
        "historico": "Histórico de movimientos",
        "compras": "Compras",
        "importacion": "Importación",
        "bitacora": "Bitácoras",
        "apiExternal": "Bitácora Api Externo",
        "apiAdministrativo": "Bitácora Api Administrativa",
        "noticia": "Noticias",
        "listaNoticias": "Lista de Noticias",
        "categoriasNoticia": "Categorías de Noticias",
        "aprobacion":"Gestion de Respuestas"
      },
      "general-limites-periodo": [
        {
          "value": "A",
          "label": "Siempre"
        },
        {
          "value": "B",
          "label": "Minuto"
        },
        {
          "value": "C",
          "label": "Hora"
        },
        {
          "value": "D",
          "label": "Día"
        },
        {
          "value": "E",
          "label": "Semana"
        },
        {
          "value": "F",
          "label": "Mes"
        }
      ],
      "general-calendar": {
        "firstDayOfWeek": 0,
        "dayNames": [
          "Domingo",
          "Lunes",
          "Martes",
          "Miercoles",
          "Jueves",
          "Viernes",
          "Sabado"
        ],
        "dayNamesShort": [
          "Dom",
          "Lun",
          "Mar",
          "Mie",
          "Jue",
          "Vie",
          "Sab"
        ],
        "dayNamesMin": [
          "D",
          "L",
          "K",
          "M",
          "J",
          "V",
          "S"
        ],
        "monthNames": [
          "Enero",
          "Febrero",
          "Marzo",
          "Abril",
          "Mayo",
          "Junio",
          "Julio",
          "Agosto",
          "Septiembre",
          "Octubre",
          "Noviembre",
          "Diciembre"
        ],
        "monthNamesShort": [
          "Ene",
          "Feb",
          "Mar",
          "Abr",
          "May",
          "Jun",
          "Jul",
          "Ago",
          "Sep",
          "Oct",
          "Nov",
          "Dic"
        ]
      },
      "general-actividad-periodos": [
        {
          "label": "1 mes",
          "value": "1M"
        },
        {
          "label": "2 meses",
          "value": "2M"
        },
        {
          "label": "3 meses",
          "value": "3M"
        },
        {
          "label": "6 meses",
          "value": "6M"
        },
        {
          "label": "1 año",
          "value": "1Y"
        },
        {
          "label": "2 años",
          "value": "2Y"
        }
      ],
      "general-itemsIndExclusion": [
        {
          "value": "I",
          "label": "Incluir"
        },
        {
          "value": "E",
          "label": "Excluir"
        }
      ],
      "segmentos": "",
      "segmentos-reglas-avanzadas": [
        {
          "value": "B",
          "label": "Recibió un mensaje"
        },
        {
          "value": "A",
          "label": "Se unió al programa"
        },
        {
          "value": "C",
          "label": "Abrió un mensaje"
        },
        {
          "value": "D",
          "label": "Redimió un premio"
        },
        {
          "value": "F",
          "label": "Compras"
        },
        {
          "value": "G",
          "label": "Última conexión"
        },
        {
          "value": "H",
          "label": "Cantidad de referidos"
        },
        {
          "value": "I",
          "label": "Tipo de dispositivo"
        },
        {
          "value": "J",
          "label": "Rango de posiciones en leaderboard"
        },
        {
          "value": "K",
          "label": "Cantidad de compras en una ubicacion"
        },
        {
          "value": "L",
          "label": "Aceptó una promoción"
        },
        {
          "value": "M",
          "label": "Vió una promoción"
        },
        {
          "value": "N",
          "label": "Compra de producto"
        },
        {
          "value": "O",
          "label": "Vió una misión"
        },
        {
          "value": "P",
          "label": "Completó una misión"
        },
        {
          "value": "Q",
          "label": "Ganaron una misión"
        },
        {
          "value": "R",
          "label": "Tiene una insignia"
        },
        {
          "value": "S",
          "label": "Pertenece a grupo"
        },
        {
          "value": "T",
          "label": "Pertenece a segmento"
        }
      ],
      "segmentos-listaElementosReciente": [
        {
          "value": "D",
          "label": "Día"
        },
        {
          "value": "A",
          "label": "Año"
        },
        {
          "value": "M",
          "label": "Mes"
        }
      ],
      "segmentos-listaDispositivos": [
        {
          "value": "A",
          "label": "Android"
        },
        {
          "value": "I",
          "label": "IOS"
        }
      ],
      "segmentos-listaOpcionesBoolean": [
        {
          "value": "true",
          "label": "True"
        },
        {
          "value": "false",
          "label": "False"
        }
      ],
      "segmentos-listaEstadosCiviles": [
        {
          "label": "Casado",
          "value": "C"
        },
        {
          "label": "Soltero",
          "value": "S"
        },
        {
          "label": "Unión libre",
          "value": "U"
        }
      ],
      "segmentos-listaEducacionMiembro": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Primaria",
          "value": "P"
        },
        {
          "label": "Secundaria",
          "value": "S"
        },
        {
          "label": "Técnica",
          "value": "T"
        },
        {
          "label": "Pregrado",
          "value": "U"
        },
        {
          "label": "Grado",
          "value": "G"
        },
        {
          "label": "Posgrado",
          "value": "R"
        }
      ],
      "segmentos-listaEstadosMiembro": [
        {
          "value": "A",
          "label": "Activo"
        },
        {
          "value": "I",
          "label": "Inactivo"
        }
      ],
      "segmentos-listaGenerosMiembro": [
        {
          "value": "M",
          "label": "Masculino"
        },
        {
          "value": "F",
          "label": "Femenino"
        }
      ],
      "segmentos-listaAtributos": [
        {
          "value": "B",
          "label": "Género"
        },
        {
          "value": "C",
          "label": "Estado civil"
        },
        {
          "value": "D",
          "label": "Frecuencia de compra"
        },
        {
          "value": "E",
          "label": "Nombre"
        },
        {
          "value": "F",
          "label": "Apellido"
        },
        {
          "value": "W",
          "label": "Segundo Apellido"
        },
        {
          "value": "X",
          "label": "Email"
        },
        {
          "value": "H",
          "label": "Estado"
        },
        {
          "value": "I",
          "label": "Nivel inicial"
        },
        {
          "value": "J",
          "label": "Nivel en progreso"
        },
        {
          "value": "K",
          "label": "Ciudad"
        },
        {
          "value": "L",
          "label": "Estado(residencia)"
        },
        {
          "value": "M",
          "label": "País"
        },
        {
          "value": "N",
          "label": "Código Postal"
        },
        {
          "value": "A",
          "label": "Fecha de nacimiento"
        },
        {
          "value": "O",
          "label": "Educación"
        },
        {
          "value": "P",
          "label": "Ingreso"
        },
        {
          "value": "Q",
          "label": "Hijos"
        },
        {
          "value": "R",
          "label": "Atributo Dinámico"
        },
        {
          "value": "G",
          "label": "Miembro del sistema"
        }
      ],
      "segmentos-operadoresTexto": [
        {
          "value": "A",
          "label": "Es"
        },
        {
          "value": "B",
          "label": "No es"
        },
        {
          "value": "E",
          "label": "Inicia con"
        },
        {
          "value": "F",
          "label": "Termina con"
        },
        {
          "value": "G",
          "label": "Contiene"
        },
        {
          "value": "H",
          "label": "No contiene"
        },
        {
          "value": "I",
          "label": "Está vacío"
        },
        {
          "value": "J",
          "label": "No está vacío"
        }
      ],
      "segmentos-operadoresFecha": [
        {
          "value": "O",
          "label": "Antes de"
        },
        {
          "value": "P",
          "label": "A partir de"
        },
        {
          "value": "Q",
          "label": "Igual a"
        }
      ],
      "segmentos-operadoresNumericos": [
        {
          "value": "V",
          "label": "Igual"
        },
        {
          "value": "W",
          "label": "Mayor que"
        },
        {
          "value": "X",
          "label": "Mayor o igual"
        },
        {
          "value": "Y",
          "label": "Menor que"
        },
        {
          "value": "Z",
          "label": "Menor o igual a"
        }
      ],
      "segmentos-listaBalancesMetrica": [
        {
          "value": "A",
          "label": "Acumulado"
        },
        {
          "value": "B",
          "label": "Vencido"
        },
        {
          "value": "C",
          "label": "Redimido"
        },
        {
          "value": "D",
          "label": "Disponible"
        }
      ],
      "segmentos-listaBalancesMetricaBalance": [
        {
          "value": "B",
          "label": "Vencido"
        },
        {
          "value": "C",
          "label": "Redimido"
        },
        {
          "value": "D",
          "label": "Disponible"
        }
      ],
      "segmentos-listaPeriodoTiempo": [
        {
          "value": "B",
          "label": "En el último "
        },
        {
          "value": "C",
          "label": "En el anterior"
        },
        {
          "value": "D",
          "label": "Desde"
        },
        {
          "value": "H",
          "label": "Hasta"
        },
        {
          "value": "E",
          "label": "Entre"
        },
        {
          "value": "F",
          "label": "Mes actual"
        },
        {
          "value": "G",
          "label": "Año actual"
        }
      ],
      "segmentos-listaCalendarizacionReglasAvanzadas": [
        {
          "value": "B",
          "label": "Desde hace"
        },
        {
          "value": "D",
          "label": "Desde"
        },
        {
          "value": "H",
          "label": "Hasta"
        },
        {
          "value": "E",
          "label": "Entre"
        },
        {
          "value": "F",
          "label": "Mes actual"
        },
        {
          "value": "G",
          "label": "Año actual"
        }
      ],
      "segmentos-listaTipoRegla": [
        {
          "value": "AT",
          "label": "Atributo de miembro"
        },
        {
          "value": "MB",
          "label": "Balance de métrica"
        },
        {
          "value": "MC",
          "label": "Cambio de métrica"
        },
        {
          "value": "AV",
          "label": "Regla avanzada"
        },
        {
          "value": "EP",
          "label": "Preferencia"
        },
        {
          "value": "EE",
          "label": "Encuesta"
        }
      ],
      "segmento-encuesta-operadores-selecc-multiple": [
        {
          "label": "Exactamente las seleccionadas",
          "value": "U"
        },
        {
          "label": "Cualquiera de las selecionadas",
          "value": "K"
        }
      ],
      "segmento-encuesta-operadores-selecc-unica": [
        {
          "label": "Cualquiera de las selecionadas",
          "value": "K"
        }
      ],
      "segmento-regla-operador-fijo": [
        {
          "value": "A",
          "label": "Es"
        },
      ],
      "preguntas-respuestas": [
        {
          "label": "Respuesta 1",
          "value": "1"
        },
        {
          "label": "Respuesta 2",
          "value": "2"
        },
        {
          "label": "Respuesta 3",
          "value": "3"
        },
        {
          "label": "Respuesta 4",
          "value": "4"
        }
      ],
      "preguntas-itemsPuntos": [
        {
          "label": "100",
          "value": "100"
        },
        {
          "label": "200",
          "value": "200"
        },
        {
          "label": "300",
          "value": "300"
        },
        {
          "label": "400",
          "value": "400"
        },
        {
          "label": "500",
          "value": "500"
        },
        {
          "label": "750",
          "value": "750"
        },
        {
          "label": "1000",
          "value": "1000"
        },
        {
          "label": "1500",
          "value": "1500"
        },
        {
          "label": "2000",
          "value": "2000"
        },
        {
          "label": "3000",
          "value": "3000"
        },
        {
          "label": "5000",
          "value": "5000"
        },
        {
          "label": "7500",
          "value": "7500"
        },
        {
          "label": "10000",
          "value": "10000"
        },
        {
          "label": "15000",
          "value": "15000"
        },
        {
          "label": "30000",
          "value": "30000"
        }
      ],
      "notificaciones": [],
      "notificaciones-elegibles": [
        {
          "label": "Segmentos",
          "value": "S"
        },
        {
          "label": "Miembros",
          "value": "M"
        }
      ],
      "notificaciones-tipo": [
        {
          "value": "E",
          "label": "Correo electrónico"
        },
        {
          "value": "P",
          "label": "Push Notification"
        }
      ],
      "notificaciones-objetivos": [
        {
          "value": null,
          "label": "Ninguno"
        },
        {
          "value": "P",
          "label": "Promocion"
        },
        {
          "value": "T",
          "label": "Tipo premio"
        }
      ],
      "notificaciones-disparador": [
        {
          "value": "M",
          "label": "Manual"
        },
        {
          "value": "D",
          "label": "Disparado"
        }
      ],
      "misiones-elegibles-itemsIndComponente": [
        {
          "value": "S",
          "label": "Segmentos"
        },
        {
          "value": "M",
          "label": "Miembros"
        }
      ],
      "misiones-itemsTipoMision": [
        {
          "value": null,
          "label": "Seleccione una opción"
        },
        {
          "value": "E",
          "label": "Encuesta"
        },
        {
          "value": "P",
          "label": "Perfil"
        },
        {
          "value": "V",
          "label": "Ver contenido"
        },
        {
          "value": "S",
          "label": "Subir contenido"
        },
        {
          "value": "R",
          "label": "Reto social"
        },
        {
          "value": "J",
          "label": "Juego"
        },
        {
          "value": "A",
          "label": "Preferencia"
        },
        {
          "value": "B",
          "label": "Realidad Aumentada"
        }
      ],
      "misiones-itemsAprobacion": [
        {
          "value": null,
          "label": "Seleccione una opción"
        },
        {
          "value": "A",
          "label": "Automático"
        },
        {
          "value": "B",
          "label": "Manual"
        }
      ],
      "misiones-contenido-optionsTiposContenido": [
        {
          "value": null,
          "label": "Seleccione una opción"
        },
        {
          "label": "Foto",
          "value": "I"
        },
        {
          "label": "video",
          "value": "V"
        }
      ],
      "misiones-perfil-atributos": [
        {
          "indAtributo": "A",
          "indRequerido": false,
          "label": "Fecha de nacimiento"
        },
        {
          "indAtributo": "B",
          "indRequerido": false,
          "label": "Género"
        },
        {
          "indAtributo": "C",
          "indRequerido": false,
          "label": "Estado civil"
        },
        {
          "indAtributo": "D",
          "indRequerido": false,
          "label": "Frecuencia de compra"
        },
        {
          "indAtributo": "E",
          "indRequerido": false,
          "label": "Nombre"
        },
        {
          "indAtributo": "F",
          "indRequerido": false,
          "label": "Apellido"
        },
        {
          "indAtributo": "S",
          "indRequerido": false,
          "label": "Segundo Apellido"
        },
        {
          "indAtributo": "K",
          "indRequerido": false,
          "label": "Ciudad"
        },
        {
          "indAtributo": "L",
          "indRequerido": false,
          "label": "Estado(residencia)"
        },
        {
          "indAtributo": "M",
          "indRequerido": false,
          "label": "País"
        },
        {
          "indAtributo": "N",
          "indRequerido": false,
          "label": "Código Postal"
        },
        {
          "indAtributo": "O",
          "indRequerido": false,
          "label": "Educación"
        },
        {
          "indAtributo": "P",
          "indRequerido": false,
          "label": "Ingreso"
        },
        {
          "indAtributo": "Q",
          "indRequerido": false,
          "label": "Hijos"
        }
      ],
      "misiones-juego-intervalos": [
        {
          "label": "Seleccione un intervalo",
          "value": ""
        },
        {
          "label": "Siempre",
          "value": "A"
        },
        {
          "label": "Minuto",
          "value": "B"
        },
        {
          "label": "Hora",
          "value": "C"
        },
        {
          "label": "Día",
          "value": "D"
        },
        {
          "label": "Semana",
          "value": "E"
        },
        {
          "label": "Mes",
          "value": "F"
        }
      ],
      "misiones-juego-itemstipo": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Ruleta",
          "value": "R"
        },
        {
          "label": "Raspadita",
          "value": "A"
        },
        {
          "label": "Rompecabezas",
          "value": "O"
        },
        {
          "label": "Quien quiere ser millonario",
          "value": "M"
        }
      ],
      "misiones-juego-estrategiaItems": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Probabilidad",
          "value": "P"
        },
        {
          "label": "Random",
          "value": "R"
        }
      ],
      "misiones-juego-tipoPremioItems": [
        {
          "label": "Premio",
          "value": "P"
        },
        {
          "label": "Métrica",
          "value": "M"
        },
        {
          "label": "Agradecimiento",
          "value": "G"
        }
      ],
      "misiones-contenido-itemsRespuestaCorrectaMultiple": [
        {
          "label": "Todas son correctas",
          "value": "T"
        },
        {
          "label": "Exactamente las seleccionadas",
          "value": "G"
        },
        {
          "label": "Cualquiera de las selecionadas",
          "value": "C"
        }
      ],
      "misiones-contenido-itemsRespuestaCorrecta": [
        {
          "label": "Todas son correctas",
          "value": "T"
        },
        {
          "label": "Cualquiera de las selecionadas",
          "value": "C"
        }
      ],
      "misiones-contenido-itemsTipoPregunta": [
        {
          "label": "Encuesta",
          "value": "E"
        },
        {
          "label": "Calificación",
          "value": "C"
        }
      ],
      "misiones-contenido-itemsTipoRespuesta": [
        {
          "label": "Selección múltiple",
          "value": "RM"
        },
        {
          "label": "Selección única",
          "value": "RU"
        },
        {
          "label": "Respuesta abierta - texto",
          "value": "RT"
        },
        {
          "label": "Respuesta abierta - numérico",
          "value": "RN"
        }
      ],
      "misiones-contenido-itemsRespuestaCorrectaCalificacion": [
        {
          "label": "Menor o igual",
          "value": "D"
        },
        {
          "label": "Igual",
          "value": "E"
        },
        {
          "label": "Menor o igual",
          "value": "F"
        }
      ],
      "misiones-contenido-itemsTipo": [
        {
          "label": "Url",
          "value": "U"
        },
        {
          "label": "Video",
          "value": "V"
        },
        {
          "label": "Imagen",
          "value": "I"
        }
      ],
      "misiones-actividad-social-optionsTiposActividad": [
        {
          "label": "Compartir (enlace)",
          "value": "L"
        },
        {
          "label": "Compartir (mensaje sin enlace)",
          "value": "M"
        },
        {
          "label": "Me gusta",
          "value": "G"
        },
        {
          "label": "Tweet",
          "value": "A"
        },
        {
          "label": "Twitter Likes",
          "value": "B"
        },
        {
          "label": "Instagram Likes",
          "value": "C"
        }
      ],
      "promociones": [],
      "promociones-detalle-itemsDetalle": "Eliminar",
      "promociones-alcance-vistas-totales": "Vistas Totales",
      "promociones-alcance-vistas-unicas": "Vistas Únicas",
      "promociones-alcance-marcas-promedio": "Marcas(promedio)",
      "promociones-alcance-elegibles-promedio": "Elegibles(promedio)",
      "promociones-items-codigo": [
        {
          "value": null,
          "label": "Ninguno"
        },
        {
          "value": "A",
          "label": "URL"
        },
        {
          "value": "B",
          "label": "Código QR"
        },
        {
          "value": "C",
          "label": "ITF"
        },
        {
          "value": "D",
          "label": "EAN"
        },
        {
          "value": "E",
          "label": "Code 128"
        },
        {
          "value": "F",
          "label": "Code 39"
        }
      ],
      "promociones-itemsIndComponente": [
        {
          "value": "S",
          "label": "Segmentos"
        },
        {
          "value": "M",
          "label": "Miembros"
        }
      ],
      "promociones-items-calendarizacion": [
        {
          "label": "Permanente",
          "value": "P"
        },
        {
          "label": "Calendarizado",
          "value": "C"
        }
      ],
      "tarea-inicio": "Start",
      "tarea-enviar-promocion": "Enviar promoción",
      "tarea-enviar-mensaje": "Enviar mensaje",
      "tarea-enviar-insignia": "Enviar insignia",
      "tarea-enviar-premio": "Enviar premio",
      "tarea-enviar-mision": "Enviar misión",
      "tarea-enviar-metrica": "Otorgar métrica",
      "workflow-items-codigo": [
        {
          "value": "A",
          "label": "Se registra en el programa"
        },
        {
          "value": "B",
          "label": "Redimió Promoción"
        },
        {
          "value": "C",
          "label": "Completó Misión"
        },
        {
          "value": "D",
          "label": "Redimió Premio"
        },
        {
          "value": "E",
          "label": "Compró Producto Específico"
        },
        {
          "value": "H",
          "label": "Compró Producto"
        },
        {
          "value": "F",
          "label": "Cumplió Años"
        },
        {
          "value": "G",
          "label": "Compró Producto"
        },
        {
          "vFalue": "I",
          "label": "Evento Personalizado"
        },
        {
          "value": "J",
          "label": "Aniversario de entrada al programa"
        },
        {
          "value": "M",
          "label": "Secuencia de misiones"
        }
      ],
      "workflow-tipo-tarea": [
        {
          "label": "Enviar promoción",
          "value": "A"
        },
        {
          "label": "Enviar mensaje",
          "value": "B"
        },
        {
          "label": "Asignar insignia",
          "value": "C"
        },
        {
          "label": "Asignar premio",
          "value": "D"
        },
        {
          "label": "Enviar misión",
          "value": "E"
        },
        {
          "label": "Otorgar Métrica",
          "value": "F"
        }
      ],
      "leaderboard-itemsTipoFormato": [
        {
          "label": "Nombre",
          "value": "N"
        },
        {
          "label": "Iniciales",
          "value": "I"
        }
      ],
      "leaderboard-itemsTipoTabla": [
        {
          "label": "Grupos",
          "value": "E"
        },
        {
          "label": "Equipos",
          "value": "G"
        },
        {
          "label": "Miembros",
          "value": "U"
        }
      ],
      "leaderboard-indGrupalFormaCalculo": [
        {
          "label": "Suma",
          "value": "S"
        },
        {
          "label": "Promedio",
          "value": "P"
        }
      ],
      "general-default-select": {
        "value": null,
        "label": "Seleccione una opción"
      },
      "atributo-miembro-tipoItems": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Numérico",
          "value": "N"
        },
        {
          "label": "Texto",
          "value": "T"
        },
        {
          "label": "Booleano",
          "value": "B"
        },
        {
          "label": "Fecha",
          "value": "F"
        }
      ],
      "configuracion-protocolo-smtp": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Protocolo SSL",
          "value": "S"
        },
        {
          "label": "Protocolo TLS",
          "value": "T"
        },
        {
          "label": "Sin protocolo",
          "value": "N"
        }
      ],
      "insignia-tipo": [
        {
          "label": "Seleccione una opción",
          "value": ""
        },
        {
          "label": "Status",
          "value": "S"
        },
        {
          "label": "Collector",
          "value": "C"
        }
      ],
      "documento-inventario-tipo": [
        {
          "label": "Compras",
          "value": "C"
        },
        {
          "label": "Ajuste más",
          "value": "A"
        },
        {
          "label": "Ajuste menos",
          "value": "M"
        },
        {
          "label": "Inter ubicación",
          "value": "I"
        },
        {
          "label": "Redención",
          "value": "R"
        }
      ],
      "booleano-general": [
        {
          "label": "Verdadero",
          "value": "true"
        },
        {
          "label": "Falso",
          "value": "false"
        }
      ],
      "miembro-educacion": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Primaria",
          "value": "P"
        },
        {
          "label": "Secundaria",
          "value": "S"
        },
        {
          "label": "Técnica",
          "value": "T"
        },
        {
          "label": "Universitaria",
          "value": "U"
        },
        {
          "label": "Grado",
          "value": "G"
        },
        {
          "label": "Master",
          "value": "M"
        },
        {
          "label": "Postgrado",
          "value": "R"
        },
        {
          "label": "Doctorado",
          "value": "D"
        }
      ],
      "miembro-genero": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Femenino",
          "value": "F"
        },
        {
          "label": "Masculino",
          "value": "M"
        }
      ],
      "miembro-estado-civil": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Casado",
          "value": "C"
        },
        {
          "label": "Soltero",
          "value": "S"
        },
        {
          "label": "Unión Libre",
          "value": "U"
        }
      ],
      "premio-tipoPremio": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Certificado",
          "value": "C"
        },
        {
          "label": "Producto físico",
          "value": "P"
        }
      ],
      "elegibles-general": [
        {
          "label": "Seleccione una opción",
          "value": ""
        },
        {
          "label": "Segmentos",
          "value": "S"
        },
        {
          "label": "Miembros",
          "value": "M"
        }
      ],
      "tipos-elegibles-general": [
        {
          "label": "Disponibles",
          "value": "D"
        },
        {
          "label": "Incluidos",
          "value": "I"
        },
        {
          "label": "Excluidos",
          "value": "E"
        }
      ],
      "accion-elegibles-general": [
        {
          "label": "Incluir",
          "value": "I"
        },
        {
          "label": "Excluir",
          "value": "E"
        }
      ],
      "dias-efectividad-general": [
        {
          "label": "Lunes",
          "value": "L"
        },
        {
          "label": "Martes",
          "value": "K"
        },
        {
          "label": "Miércoles",
          "value": "M"
        },
        {
          "label": "Jueves",
          "value": "J"
        },
        {
          "label": "Viernes",
          "value": "V"
        },
        {
          "label": "Sábado",
          "value": "S"
        },
        {
          "label": "Domingo",
          "value": "D"
        }
      ],
      "tipo-efectividad-general": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Permanente",
          "value": "P"
        },
        {
          "label": "Calendarizado",
          "value": "C"
        }
      ],
      "atributo-producto-tipoItems": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Numérico",
          "value": "N"
        },
        {
          "label": "Texto",
          "value": "T"
        },
        {
          "label": "Múltiple",
          "value": "M"
        }
      ],
      "metrica-medida-tipo": [
        {
          "label": "Métrica basada en...",
          "value": ""
        },
        {
          "label": "Puntos",
          "value": "P"
        },
        {
          "label": "Visitas",
          "value": "V"
        },
        {
          "label": "Actividad",
          "value": "A"
        },
        {
          "label": "Duración",
          "value": "U"
        },
        {
          "label": "Distancia",
          "value": "D"
        },
        {
          "label": "Gastos",
          "value": "G"
        }
      ],
      "estado-redencion-premio": [
        {
          "label": "Sin retirar",
          "value": "S"
        },
        {
          "label": "Retirado",
          "value": "R"
        },
        {
          "label": "Anulado",
          "value": "A"
        }
      ],
      "trabajo-interno-tipo": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Notificación",
          "value": "N"
        },
        {
          "label": "Importación",
          "value": "I"
        },
        {
          "label": "Exportación",
          "value": "E"
        }
      ],
      "trabajo-interno-estado": [
        {
          "label": "Seleccione una opción",
          "value": null
        },
        {
          "label": "Procesando",
          "value": "P"
        },
        {
          "label": "Fallido",
          "value": "I"
        },
        {
          "label": "Completado",
          "value": "E"
        }
      ],
      "id-miembro-importacion": [
        {
          "label": "UUID",
          "value": "idMiembro"
        },
        {
          "label": "Identificación",
          "value": "docIdentificacion"
        }
      ],
      "importacion-miembro-segmento": [
        {
          "label": "UUID",
          "value": "ID_MIEMBRO"
        },
        {
          "label": "Identificación",
          "value": "DOC_IDENTIFICACION"
        }
      ],
      "atributo-tipo": [
        {
          "label": "Estático",
          "value": "E"
        },
        {
          "label": "Dinámico",
          "value": "D"
        }
      ],
      "atributos-estaticos": [
        {
          "value": "NOMBRE",
          "label": "Nombre"
        },
        {
          "value": "APELLIDO",
          "label": "Apellido"
        },
        {
          "value": "APELLIDO2",
          "label": "Segundo Apellido"
        },
        {
          "value": "FECHA_NACIMIENTO",
          "label": "Fecha de nacimiento"
        },
        {
          "value": "CIUDAD_RESIDENCIA",
          "label": "Ciudad de residencia"
        },
        {
          "value": "DIRECCION",
          "label": "Dirección"
        },
        {
          "value": "ESTADO_RESIDENCIA",
          "label": "Estado de residencia"
        },
        {
          "value": "PAIS_RESIDENCIA",
          "label": "País de residencia"
        },
        {
          "value": "CODIGO_POSTAL",
          "label": "Código Postal"
        },
        {
          "value": "CORREO",
          "label": "Email"
        },
        {
          "value": "CONTRASENA",
          "label": "Contraseña"
        },
        {
          "value": "IND_EDUCACION",
          "label": "Educación"
        },
        {
          "value": "IND_ESTADO_CIVIL",
          "label": "Estado civil"
        },
        {
          "value": "IND_GENERO",
          "label": "Género"
        },
        {
          "value": "IND_HIJOS",
          "label": "Hijos"
        },
        {
          "value": "INGRESO_ECONOMICO",
          "label": "Ingreso"
        },
        {
          "value": "TELEFONO_MOVIL",
          "label": "Teléfono"
        },
        {
          "value": "IND_CONTACTO_EMAIL",
          "label": "Contacto por email"
        },
        {
          "value": "IND_CONTACTO_ESTADO",
          "label": "Contacto estado"
        },
        {
          "value": "IND_CONTACTO_NOTIFICACION",
          "label": "Contacto por notificación"
        },
        {
          "value": "IND_CONTACTO_SMS",
          "label": "Contacto por SMS"
        }
      ],
      "mision-aprobacion-estado": [
        {
          "label": "Ganado",
          "value": "G"
        },
        {
          "label": "Fallido",
          "value": "F"
        },
        {
          "label": "Pendiente",
          "value": "P"
        }
      ],
      "respuesta-preferencia-tipo": [
        {
          "label": "Selección única",
          "value": "SU"
        },
        {
          "label": "Respuesta múltiple",
          "value": "RM"
        },
        {
          "label": "Texto",
          "value": "TX"
        }
      ],
      "miembro-suspencion": {
        "severity": "success",
        "summary": "Suspendido",
        "detail": "El miembro ha sido suspendido."
      },
      "miembro-inactivo": {
        "severity": "info",
        "summary": "Inactivo",
        "detail": "El miembro ha sido suspendido."
      },
      "usuarios-error-username": {
        "severity": "error",
        "summary": "Error",
        "detail": "El nombre de usuario ya existe"
      },
      "usuario-correo": {
        "severity": "error",
        "summary": "Error",
        "detail": "El correo ya existe"
      },
      "segmentos-info-procesando": {
        "severity": "info",
        "summary": "Procesando",
        "detail": "El segmento se encuentra en proceso en este momento, por favor espere"
      },
      "segmentos-enviado-procesamiento": {
        "severity": "info",
        "summary": "Enviado",
        "detail": "La petición ha sido enviada al servidor"
      },
      "segmentos-miembros-select": {
        "detail": "Elija uno o más usuarios ",
        "severity": "info",
        "summary": ""
      },
      "general-lista-vacia": {
        "severity": "info",
        "summary": "Info",
        "detail": "No se encontraron elementos"
      },
      "general-insercion": {
        "detail": "Los datos se han insertado correctamente",
        "severity": "success",
        "summary": "Insertado"
      },
      "general-actualizacion": {
        "detail": "Los datos se han actualizado correctamente",
        "severity": "success",
        "summary": "Actualizado"
      },
      "general-eliminacion": {
        "detail": "Los datos se han eliminado correctamente",
        "severity": "success",
        "summary": "Eliminado"
      },
      "info-uploading": {
        "detail": "Subiendo imagen",
        "severity": "info",
        "summary": "Espere"
      },
      "general-inclusion-simple": {
        "detail": "El elemento se ha agregado correctamente",
        "severity": "success",
        "summary": "Agregado"
      },
      "general-inclusion-multiple": {
        "detail": "Los elementos se han agregado correctamente",
        "severity": "success",
        "summary": "Agregados"
      },
      "general-exclusion-multiple": {
        "detail": "Los elementos se han excluido correctamente",
        "severity": "success",
        "summary": "Excluidos"
      },
      "general-exclusion-simple": {
        "detail": "El elemento se ha excluido correctamente",
        "severity": "success",
        "summary": "Excluido"
      },
      "general-eliminacion-multiple": {
        "detail": "Los elementos se han eliminado correctamente",
        "severity": "success",
        "summary": "Eliminados"
      },
      "general-eliminacion-simple": {
        "detail": "El elemento se ha eliminado correctamente",
        "severity": "success",
        "summary": "Eliminado"
      },
      "general-reporte-creacion": {
        "detail": "El reporte se ha creado correctamente",
        "severity": "success",
        "summary": "Creado"
      },
      "general-actualizando-datos": {
        "detail": "Actualizando datos",
        "severity": "info",
        "summary": "Espere"
      },
      "general-archivar": {
        "detail": "El elemento se ha archivado correctamente",
        "severity": "success",
        "summary": "Archivado"
      },
      "general-publicar": {
        "detail": "El elemento se publicó correctamente",
        "severity": "success",
        "summary": "Publicado"
      },
      "general-activar": {
        "detail": "El elemento se ha activado correctamente.",
        "severity": "success",
        "summary": "Activado"
      }, 
      "general-desactivar": {
        "detail": "El elemento se ha desactivado correctamente",
        "severity": "success",
        "summary": "Inactivo"
      },
      "reporte-espera-generar": {
        "detail": "Debe esperar a que concluya la generación de certificados anterior, intente más tarde.",
        "severity": "info",
        "summary": "Información"
      },
      "documento-cerrado":{
        "detail": "El documento se ha cerrado.",
        "severity": "success",
        "summary": "Cerrado"
      },
      "cierre-mes":{
        "detail": "Se realizo el cierre de mes.",
        "severity": "success",
        "summary": "Cerrado"
      },
      "respuesta-mision-aprobar":{
        "detail": "La respuesta del miembro fue aprobada.",
        "severity": "success",
        "summary":"Gano"
      },
      "respuesta-mision-rechazar":{
        "detail": "La respuesta del miembro fue rechazada.",
        "severity": "success",
        "summary":"Fallo"
      },
      "procesando-importacion":{
        "detail": "La importación se esta procesando.",
        "severity": "info",
        "summary":"Información"
      },
      "error-datos-incorrectos": {
        "detail": "Debes insertar al menos una pregunta",
        "severity": "error",
        "summary": "Datos incorrectos"
      },
      "error-conexion": {
        "detail": "Ha ocurrido un error de conexión",
        "severity": "error",
        "summary": "Error"
      },
      "error-nombre-interno": {
        "detail": "El nombre ya existe",
        "severity": "error",
        "summary": "Datos incorrectos"
      },
      "error-url-invalido": {
        "detail": "El formato del URL es incorrecto",
        "severity": "error",
        "summary": "Error"
      },
      "error-notificacion-texto": {
        "detail": "Debes específicar el texto de la notificacion primero",
        "severity": "error",
        "summary": "Error"
      },
      "error-misiones-pregunta": {
        "detail": "Debes insertar al menos una pregunta",
        "severity": "error",
        "summary": "Datos incorrectos"
      },
      "error-leaderboard-grupo": {
        "detail": "Debes seleccionar un grupo...",
        "severity": "error",
        "summary": "Error"
      },
      "error-codigo-premio":{
        "detail": "El código de premio no es válido",
        "severity": "error",
        "summary": "Error"
      },
      "warn-misiones-premio-juego": {
        "detail": "Debe seleccionar un premio.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-metrica-juego": {
        "detail": "Debe ingresar la cantidad de metrica a entregar.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-tipo": {
        "detail": "Debe seleccionar el tipo de juego.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-estrategia": {
        "detail": "Debe seleccionar una estrategia.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-imagen": {
        "detail": "Debe seleccionar una imagen.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-rompecabeza": {
        "detail": "Cantidad de premios excedida, elimine premios para cambiar a Rompecabezas.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-raspadita": {
        "detail": "Cantidad de premios excedida, elimine premios para cambiar a Raspadita.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-cambio": {
        "detail": "El estado de la misión ha cambiado, no es posible ingresar otro premio de juego.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-tiempo": {
        "detail": "El tiempo límite debe ser mayor a 10 segundos.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-cantidadPiezas": {
        "detail": "La cantidad debe estrar entre 6 y 16 piezas de rompecabezas.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-misiones-juego-probabilidad": {
        "detail": "La probabilidad del premio no puede estar vacía.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-repoert-periodo": {
        "detail": "El período ingresado no está dentro del rango permitido, intentelo de nuevo.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-repoert-mes": {
        "detail": "El mes ingresado no está dentro del rango permitido, intentelo de nuevo.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-atributo-valor": {
        "detail": "Verifique el valor del atributo para continuar.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-atributo-respuesta": {
        "detail": "Debe ingresar al menos dos respuestas.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-resultado": {
        "detail": "No se encontraron resultados",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-geocoder": {
        "detail": "El servicio de geocodificación fallo debido a: ",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-fecha": {
        "detail": "Verifique el rango de fechas seleccionado para continuar con la actualización.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-semana": {
        "detail": "Debe indicar la semana y los días de recurrencia para continuar.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-destino": {
        "detail": "Seleccione la ubicación de destino.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-origen": {
        "detail": "Seleccione la ubicación de origen.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-ubicacion-igual": {
        "detail": "La ubicación de origen y destino deben ser diferentes.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-proveedor": {
        "detail": "Seleccione un proveedor.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-workflow-disparador": {
        "detail": "Debe seleccionar el elemento para el disparador según el tipo de acción.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-dato-valido": {
        "detail": "Debe ingresar el dato solicitado para continuar.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-cantidad": {
        "detail": "Debe seleccionar al menos 1 elemento para continuar.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-precio-premio": {
        "detail": "Ingrese el precio del premio.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "warn-cantidad-premio": {
        "detail": "Ingrese la cantidad de premios.",
        "severity": "warn",
        "summary": "Alerta"
      },
      "pipe-calendarizacion": {
        "Permanente": "Permanente",
        "Calendarizado": "Calendarizado",
        "Recurrente": "Recurrente"
      },
      "pipe-estado-campana": {
        "Borrador": "Borrador",
        "Publicado": "Publicado",
        "Archivado": "Archivado",
        "Ejecutado": "Ejecutado"
      },
      "pipe-tipo-segmento": {
        "E": "Estático",
        "D": "Dinámico"
      },
      "pipe-status-trabajo": {
        "RUNNING": "Ejecutando",
        "SUBMITTED": "En cola",
        "FAILED": "Fallido",
        "FINISHED": "Completado",
        "KILLED": "Cancelado",
        "STOPPED": "Detenido"
      },
      "pipe-evento-campana": {
        "Manual": "Manual",
        "Calendarizado": "Calendarizado",
        "Trigger": "Trigger"
      },
      "pipe-ind-operador": {
        "AND": "AND",
        "OR": "OR",
        "NOTIN": "NOT IN"
      },
      "pipe-tipo-insignia": {
        "Status": "Status",
        "Collector": "Collector"
      },
      "pipe-regla-tipo-cambio": {
        "A": "Ganar",
        "B": "Expirar",
        "C": "Redimir"
      },
      "pipe-regla-avanzada-ind-ultimo": {
        "D": "Día(s)",
        "A": "Año(s)",
        "M": "Mes(es)"
      },
      "pipe-tipo-notificacion": {
        "P": "Notificaciones Push",
        "E": "Correo Electronico"
      },
      "pipe-tipo-leaderboard": {
        "E": "Grupos",
        "U": "Miembros",
        "G": "Equipos"
      },
      "pipe-estado-segmento": {
        "AR": "Archivado",
        "BO": "Borrador",
        "AC": "Activo"
      },
      "pipe-estado-workflow": {
        "A": "Activo",
        "B": "Archivado",
        "C": "Borrador"
      },
      "pipe-estado-usuario": {
        "B": "Activo",
        "I": "Inactivo"
      },
      "pipe-balance-metrica": {
        "A": "acumulado",
        "B": "vencido",
        "C": "redimido",
        "D": "disponible"
      },
      "pipe-atributo-tipoDato": {
        "T": "Texto",
        "F": "Fecha",
        "B": "Booleano",
        "N": "Numerico",
        "M": "Múltiple"
      },
      "pipe-atributo-visibilidad": {
        "true": "Administradores y app móvil",
        "false": "Solo administradores"
      },
      "pipe-cambio-estado": {
        "A": "Activo",
        "B": "Borrador",
        "I": "Inactivo"
      },
      "pipe-certificado-estado": {
        "D": "Disponible",
        "A": "Anulado",
        "O": "Ocupado"
      },
      "pipe-dato-booleano": {
        "true": "Verdadero",
        "false": "Falso"
      },
      "pipe-documento-estado": {
        "true": "Cerrado",
        "false": "Abierto"
      },
      "pipe-documento-tipo": {
        "C": "Compra",
        "R": "Redención",
        "A": "Ajuste más",
        "M": "Ajuste menos",
        "I": "Inter ubicación"
      },
      "pipe-estado-elemento": {
        "B": "Borrador",
        "P": "Publicado",
        "A": "Archivado",
        "I": "Publicado",
        "D": "Borrador"
      },
      "pipe-metrica-medida": {
        "P": "Puntos",
        "V": "Visitas",
        "A": "Actividad",
        "U": "Duracion",
        "D": "Distancia",
        "G": "Gasto"
      },
      "pipe-miembro-educacion": {
        "P": "Primaria",
        "S": "Secundaria",
        "T": "Técnica",
        "U": "Universitaria",
        "G": "Grado",
        "M": "Master",
        "R": "Postgrado",
        "D": "Doctorado"
      },
      "pipe-miembro-estadoCivil": {
        "C": "Casado",
        "S": "Soltero",
        "U": "Unión libre"
      },
      "pipe-miembro-estado": {
        "A": "Activo",
        "I": "Inactivo"
      },
      "pipe-miembro-genero": {
        "M": "Masculino",
        "F": "Femenino"
      },
      "pipe-objetivo-campana": {
        "M": "Misión",
        "P": "Promoción",
        "R": "Premio"
      },
      "pipe-tipo-premioJuego": {
        "P": "Premio",
        "M": "Métrica",
        "G": "Agradecimiento"
      },
      "pipe-premio-tipo": {
        "P": "Producto físico",
        "C": "Certificado"
      },
      "pipe-tipo-juegoMision": {
        "R": "Ruleta",
        "A": "Raspadita",
        "O": "Rompecabezas"
      },
      "pipe-estado-redencion": {
        "S": "Sin retirar",
        "R": "Retirado",
        "A": "Anulado"
      },
      "pipe-estado-trabajo-interno": {
        "P": "Procesando",
        "F": "Fallido",
        "C": "Completado"
      },
      "pipe-tipo-trabajo-interno": {
        "N": "Notificación",
        "E": "Exportación",
        "I": "Importación"
      },
      "pipe-tipo-pregunta": {
        "E": "Encuesta",
        "C": "Calificación"
      },
      "pipe-tipo-red-social": {
        "L": "Enlace",
        "M": "Mensaje",
        "G": "Me gusta",
        "A": "Tweet",
        "B": "Twitter Likes",
        "C": "Instagram Likes"
      },
      "reglas-asignacion-badge-tipo-regla": [
        {
          "label": "Ejecución de misión",
          "value": "A"
        },
        {
          "label": "Cantidad de referidos",
          "value": "B"
        },
        {
          "label": "Ascendió al nivel",
          "value": "C"
        },
        {
          "label": "Acumulado de compras",
          "value": "D"
        },
        {
          "label": "Cantidad de compras",
          "value": "E"
        }
      ],
      "reglas-asignacion-premio-tipo-regla": [
        {
          "label": "Frecuencia de Compra",
          "value": "A"
        },
        {
          "label": "Monto de compra",
          "value": "B"
        },
        {
          "label": "Acumulado de compra",
          "value": "C"
        }
      ],
      "reglas-asignacion-premio-fecha-calendarizacion": [
        {
          "label": "Día(s)",
          "value": "D"
        },
        {
          "label": "Semana(s)",
          "value": "S"
        },
        {
          "label": "Mes(es)",
          "value": "M"
        }
      ],
      "reglas-listaPeriodoTiempo": [
        {
          "value": "B",
          "label": "En el(los) último(s) "
        },
        {
          "value": "C",
          "label": "En el(los) anterior(es)"
        },
        {
          "value": "D",
          "label": "Desde"
        },
        {
          "value": "H",
          "label": "Hasta"
        },
        {
          "value": "E",
          "label": "Entre"
        },
        {
          "value": "F",
          "label": "Mes actual"
        },
        {
          "value": "G",
          "label": "Año actual"
        }
      ],
      "bitacora-accion": [
        {
          "label": "Insertada.",
          "value": "A"
        },
        {
          "label": "Actualizada",
          "value": "B"
        },
        {
          "label": "Eliminada",
          "value": "C"
        },
        {
          "label": "Publicada",
          "value": "D"
        },
        {
          "label": "Archivada",
          "value": "E"
        },
        {
          "label": "Desactivada",
          "value": "F"
        },
        {
          "label": "Activada",
          "value": "G"
        },
        {
          "label": "Borrador",
          "value": "H"
        }
      ],
      "confirmacion-pregunta": {
        "A": "¿Desea archivar este elemento?",
        "B": "¿Desea eliminar este elemento?",
        "C": "¿Desea activar este elemento?",
        "D": "¿Desea suspender a este miembro?",
        "E": "¿Desea suspender a este usuario?",
        "F": "¿Desea inactivar este elemento?",
        "G": "¿Desea excluir este elemento?",
        "H": "¿Desea excluir esta lista de elementos?"
      },
      "metricas-dashboard-cant-acumulada": "Cantidad Acumulada",
      "metricas-dashboard-cant-redimida": "Cantidad Vencida",
      "metricas-dashboard-cant-vencida": "Cantidad Redimida",
      "misiones-dashboard-cant-respuestas": "Cantidad de Respuestas",
      "misiones-dashboard-cant-respuestas-aprobadas": "Cantidad de Respuestas Aprobadas",
      "misiones-dashboard-cant-respuestas-rechazadas": "Cantidad de Respuestas Rechazadas",
      "misiones-dashboard-cant-elegibles-promedio": "Cantidad de Elegibles Promedio",
      "misiones-dashboard-cant-cant-vistas-totales": "Cantidad de Vistas Totales",
      "misiones-dashboard-cant-vistas-unicas": "Cantidad de Vistas Únicas",
      "notificaciones-dashboard-cant-msjs-enviados": "Cantidad de mensajes enviados",
      "notificaciones-dashboard-cant-msjs-abiertos": "Cantidad de mensajes abiertos",
      "notificaciones-dashboard-cant-miembros-enviados": "Cantidad de miembros enviados",
      "notificaciones-dashboard-cant-respuestas": "Cantidad de Respuestas",
      "premios-dashboard-cant-redenciones": "Cantidad de Redenciones",
      "premios-dashboard-cant-elegibles-promedio": "Cantidad de Elegibles Promedio"
    };
  }
}