import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({ name: 'visibilidad' })
export class VisibilidadPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: boolean): string {
        if (value) {
            return this.i18nService.getLabels("pipe-atributo-visibilidad")["true"];
        }else{
            return this.i18nService.getLabels("pipe-atributo-visibilidad")["false"];
        }
    }
}