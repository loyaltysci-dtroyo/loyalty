import { I18nService } from '../service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'estadoNotificacion'
})

export class EstadoNotificacionPipe implements PipeTransform {

    constructor(private i18nService: I18nService) {

    }
    transform(value: string, args: any[]): any {

        let values =this.i18nService.getLabels("pipe-estado-campana");
            switch (value) {
            case "B": {
                return values["Borrador"];
            }
            case "P": {
                return values["Publicado"];
            }
            case "A": {
                return values["Archivado"];
            }
            case "E": {
                return values["Ejecutado"];
            }
        }
    }
}
