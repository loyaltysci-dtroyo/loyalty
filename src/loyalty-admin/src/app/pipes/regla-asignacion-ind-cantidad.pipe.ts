import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'indAsignacionPeriodoUltimo'
})

export class ReglaAsignacionIndCantidadPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: any, args: any[]): any {
        return this.i18nService.getLabels("reglas-listaPeriodoTiempo").find((element)=>{return element.value==value}).label || "";
    }
}