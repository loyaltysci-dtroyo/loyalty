import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'eventoCampana'
})

export class EventoCampanaPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string, args: any[]): any {
        //M=mision P=promocion R=recompensa
         switch(value){
            case "M":{
                return this.i18nService.getLabels("pipe-objetivo-campana")["M"];
            }
            case "P":{
                return this.i18nService.getLabels("pipe-objetivo-campana")["P"];
            }
            case "R":{
                 return this.i18nService.getLabels("pipe-objetivo-campana")["R"];
            }
            default:
                return "";
        }
    }
}