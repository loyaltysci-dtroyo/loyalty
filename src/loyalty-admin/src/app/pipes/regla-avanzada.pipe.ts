import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'reglaAvanzada'
})

export class ReglaAvanzadaPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: any, args: any[]): any {
        return this.i18nService.getLabels("segmentos-reglas-avanzadas").filter(element => {
            return element.value==value;
        }).map(element => {
            return element.label
        });
    }
}