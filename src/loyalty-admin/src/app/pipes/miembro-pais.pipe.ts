import { Pipe, PipeTransform } from '@angular/core';
import { Pais } from '../model';
import { I18nService, PaisService, KeycloakService } from '../service';
@Pipe({
    name: 'miembroPais'
})

export class MiembroPaisPipe implements PipeTransform {
    constructor(
        public kc: KeycloakService,
        private i18nService: I18nService,
        private paisService: PaisService
    ) {

    }
    transform(value: string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.paisService.token = tkn;
                return this.paisService.obtenerPaisPorNombre(value).map(
                    (data: Pais) => {
                        return data.nombrePais;
                    }
                ).toPromise();
            });

    }
}