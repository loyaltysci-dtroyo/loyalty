import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'tipoSegmento'
})

export class TipoSegmentoPipe implements PipeTransform {
    constructor(
        private i18nService: I18nService
    ) {
    }
    transform(value: string): string {
        return this.i18nService.getLabels("pipe-tipo-segmento")[value]||"";
    }
}