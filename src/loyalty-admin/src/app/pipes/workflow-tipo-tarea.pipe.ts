import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'indTipoTarea'
})

export class WorkflowTipoTareaPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string, args: any[]): any {
        return this.i18nService.getLabels("workflow-items-codigo").filter(element => {
            return element.value == value;
        }).map(element => {
            return element.label
        });
    }
}