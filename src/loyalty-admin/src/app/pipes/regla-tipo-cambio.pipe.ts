import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'indReglaTipoCambio'
})

export class ReglaTipoCambioPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string, args: any[]): any {
        switch(value){
            case"A":{
                return this.i18nService.getLabels("pipe-regla-tipo-cambio")["A"];
            }
            case"B":{
                return this.i18nService.getLabels("pipe-regla-tipo-cambio")["B"];
            }
            case"C":{
                return this.i18nService.getLabels("pipe-regla-tipo-cambio")["C"];
            }
        }
    }
}