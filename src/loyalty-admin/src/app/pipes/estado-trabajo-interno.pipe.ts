import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({ name: 'estadoTrabajo' })
export class EstadoTrabajoInternoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string): string {
        switch(value){
            case "P":{
                return this.i18nService.getLabels("pipe-estado-trabajo-interno")["P"];
            }
            case "F":{
                return this.i18nService.getLabels("pipe-estado-trabajo-interno")["F"];
            }
            case "C":{
                 return this.i18nService.getLabels("pipe-estado-trabajo-interno")["C"];
            }
        }
    }
}