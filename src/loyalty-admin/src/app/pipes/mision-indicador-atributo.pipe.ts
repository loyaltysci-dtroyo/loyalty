import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'indAtributo'
})

export class MisionIndicadorAtributoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){}
    transform(value: any, args: any[]): any {
        return this.i18nService.getLabels("misiones-perfil-atributos").filter(element => {
            return element.indAtributo == value;
        }).map(element => {
            return element.label
        });
    }
}