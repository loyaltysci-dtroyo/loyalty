import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'estadoUsuario'
})

export class EstadoUsuarioPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string): string {
       return this.i18nService.getLabels("pipe-estado-usuario")[value]||"";
    }
}