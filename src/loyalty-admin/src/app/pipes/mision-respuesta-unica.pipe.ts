import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'indRespUnica'
})

export class RespuestaUnicaPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){}
    transform(value: string, args: any[]): any {
         return this.i18nService.getLabels("misiones-contenido-itemsRespuestaCorrecta").filter(element => {
            return element.value == value;
        }).map(element => {
            return element.label
        });
    }
}