import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';

@Pipe({
    name: 'documentoTipo'
})

export class DocumentoTipoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string, args: any[]): any {
        switch (value) {
            case "C": {
                return this.i18nService.getLabels("pipe-documento-tipo")["C"];
            }
            case "R": {
                return this.i18nService.getLabels("pipe-documento-tipo")["R"];
            }
            case "A": {
                return this.i18nService.getLabels("pipe-documento-tipo")["A"];
            }
            case "M": {
                return this.i18nService.getLabels("pipe-documento-tipo")["M"];
            }
            case "I": {
                return this.i18nService.getLabels("pipe-documento-tipo")["I"];
            }
            default:{
                return "";
            }
        }
    }
}