import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'educacionMiembro'
})

export class EducacionMiembroPipe implements PipeTransform {
    constructor(
        private i18nService: I18nService
    ) { }

    transform(value: any, ...args: any[]): any {
        switch (value) {
            case "P": {
                return this.i18nService.getLabels("pipe-miembro-educacion")["P"];
            }
            case "S": {
                return this.i18nService.getLabels("pipe-miembro-educacion")["S"];
            }
            case "T": {
                return this.i18nService.getLabels("pipe-miembro-educacion")["T"];
            }
            case "U": {
                return this.i18nService.getLabels("pipe-miembro-educacion")["U"];
            }
            case "G": {
                return this.i18nService.getLabels("pipe-miembro-educacion")["G"];
            }
            case "M": {
                return this.i18nService.getLabels("pipe-miembro-educacion")["M"];
            }
            case "R": {
                return this.i18nService.getLabels("pipe-miembro-educacion")["R"];
            }
            case "D": {
                return this.i18nService.getLabels("pipe-miembro-educacion")["D"];
            }
            default: {
                return '';
            }
        }
    }
}