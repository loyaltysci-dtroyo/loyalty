import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({ name: 'tipoTrabajo' })
export class TipoTrabajoInternoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string): string {
        switch(value){
            case "N":{
                return this.i18nService.getLabels("pipe-tipo-trabajo-interno")["N"];
            }
            case "E":{
                return this.i18nService.getLabels("pipe-tipo-trabajo-interno")["E"];
            }
            case "I":{
                 return this.i18nService.getLabels("pipe-tipo-trabajo-interno")["I"];
            }
        }
    }
}