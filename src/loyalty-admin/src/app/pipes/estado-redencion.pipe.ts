import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'estadoRedencion'
})

export class EstadoRedencionPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string, args: any[]): any {
        //S=Sin retirar R=Retirado A=Anulado
         switch(value){
            case "S":{
                return this.i18nService.getLabels("pipe-estado-redencion")["S"];
            }
            case "R":{
                return this.i18nService.getLabels("pipe-estado-redencion")["R"];
            }
            case "A":{
                 return this.i18nService.getLabels("pipe-estado-redencion")["A"];
            }
            default:
                return "";
        }
    }
}