import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({ name: 'fechaAtributo' })
export class FechaAtributoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string): Date {
        let fecha = new Date(value);
        return fecha;
    }
}