import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'statusTrabajoSegmento'
})

export class StatusTrabajoSegmentoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string, args: any[]): any {
        switch(value){
            case "RUNNING":{
                return this.i18nService.getLabels("pipe-status-trabajo")["RUNNING"];
            }
            case "SUBMITTED":{
                return this.i18nService.getLabels("pipe-status-trabajo")["SUBMITTED"];
            }
            case "FAILED":{
                return this.i18nService.getLabels("pipe-status-trabajo")["FAILED"];
            }
            case "FINISHED":{
                return this.i18nService.getLabels("pipe-status-trabajo")["FINISHED"];
            }
             case "KILLED":{
                return this.i18nService.getLabels("pipe-status-trabajo")["KILLED"];
            }
            case"STOPPED":{
                return this.i18nService.getLabels("pipe-status-trabajo")["STOPPED"];
            }
            default:{
             
            }
        }
    }
}