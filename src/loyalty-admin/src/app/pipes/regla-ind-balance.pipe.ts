import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'indReglaBalance'
})

export class ReglaIndBalancePipe implements PipeTransform {
    constructor(
        private i18nService: I18nService
    ) {

    }
    transform(value: string): string {
        return this.i18nService.getLabels("segmentos-listaBalancesMetrica").filter(element => {
            return element.value == value;
        }).map(element => {
            return element.label
        });
    }
}