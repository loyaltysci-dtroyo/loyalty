import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'indAsignacionUltimo'
})

export class ReglaAsignacionIndUltimoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: any, args: any[]): any {
        return this.i18nService.getLabels("pipe-regla-avanzada-ind-ultimo")[value];
    }
}