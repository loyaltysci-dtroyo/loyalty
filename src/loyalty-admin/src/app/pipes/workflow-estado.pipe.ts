import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'workflowEstadoPipe'
})

export class WorkflowEstadoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    
    transform(value: any, args: any[]): any {
        return this.i18nService.getLabels("pipe-estado-workflow")[value].label;
    }
}