import { I18nService } from '../service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'calendarizacion' })
export class CalendarizacionPipe implements PipeTransform {
    constructor(private i18nService: I18nService) {

    }
    transform(value: string): any {
        switch (value) {
            case "P": {
                return this.i18nService.getLabels("pipe-calendarizacion")["Permanente"];
            } 
            case "C": {
                return this.i18nService.getLabels("pipe-calendarizacion")["Calendarizado"];
            } 
            default: { 
                return "";
            }
        }
    }
}