import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';

@Pipe({ name: 'datoBolean' })
export class DatoBoleanPipe implements PipeTransform {
  constructor(
    private i18nService: I18nService
  ) {

  }
  transform(value: boolean): string {
    switch (value) {
      case true: {
        return this.i18nService.getLabels("pipe-dato-booleano")["true"];
      }
      case false: {
        return this.i18nService.getLabels("pipe-dato-booleano")["false"];
      }
      default: {
        return "";
      }
    }
  }
}