import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';

@Pipe({ name: 'cambioEstado' })
export class CambioEstadoPipe implements PipeTransform {
    constructor(
        private i18nService: I18nService
    ) {

    }
    transform(value: string): string {
        switch (value) {
            case "A": {
                return this.i18nService.getLabels("pipe-cambio-estado")["A"];
            } 
            case "B": {
                return this.i18nService.getLabels("pipe-cambio-estado")["B"];
            } 
            case "I": {
                return this.i18nService.getLabels("pipe-cambio-estado")["I"];
            }
            default: { 
                return "";
            }
        }
    }
}