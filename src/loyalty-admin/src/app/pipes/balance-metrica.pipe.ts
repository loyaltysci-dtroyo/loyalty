import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'indBalanceMetrica'
})

export class IndBalanceMetricaPipe implements PipeTransform {
    constructor(
        private i18nService: I18nService
    ) {

    }
    transform(value: string, args: any[]): string {
        switch (value) {
            case "A": {
                return this.i18nService.getLabels("pipe-balance-metrica")["A"];
            }
            case "B": {
                return this.i18nService.getLabels("pipe-balance-metrica")["B"];
            }
            case "C": {
                return this.i18nService.getLabels("pipe-balance-metrica")["C"];
            }
            case "D": {
                return this.i18nService.getLabels("pipe-balance-metrica")["D"];
            }
            default: { 
                return "" 
            }
        }
    }
}