import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'estadoMiembro'
})

export class EstadoMiembroPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){ }
    transform(value: any, ...args: any[]): any {
        switch(value){
            case "A":{
                return this.i18nService.getLabels("pipe-miembro-estado")["A"];
            }
            case "I":{
                return this.i18nService.getLabels("pipe-miembro-estado")["I"];
            }
            default:{
                return '';
            }
    }
    }
}