import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';

@Pipe({ name: 'tipoInsignia' })
export class TipoInsigniaPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string): string {
        if (value == 'S') {
            return this.i18nService.getLabels("pipe-tipo-insignia")["Status"];
        }else{
            return this.i18nService.getLabels("pipe-tipo-insignia")["Collector"];
        }
    }
}