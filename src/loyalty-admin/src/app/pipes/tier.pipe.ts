import { Pipe, PipeTransform } from '@angular/core';
import { I18nService, NivelService,KeycloakService} from '../service';

import { Nivel} from '../model';
@Pipe({ name: 'tier' })
export class TierPipe implements PipeTransform {
    constructor(
        private nivelService:NivelService,
        private i18nService: I18nService,
        public kc: KeycloakService
    ) {

    }
    transform(value:string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.nivelService.token = tkn;
              
            });
             return this.nivelService.obtenertNivel(value).map((nivel:Nivel)=>{return nivel.nombre}).toPromise();
    }
}