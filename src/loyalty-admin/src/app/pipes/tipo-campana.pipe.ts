import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'tipoNotificacion'
})

export class TipoNotificacionPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string, args: any[]): any {
       let values=this.i18nService.getLabels("pipe-tipo-notificacion");
       switch (value) {
            case "P": {
                return values.P;
            }
            case "E": {
                return values.E;
            }
            default:{
                return "";
            }
        }
    }
}