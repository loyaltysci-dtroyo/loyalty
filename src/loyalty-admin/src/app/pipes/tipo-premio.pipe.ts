import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'tipoPremio'
})

export class TipoPremioPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: any, ...args: any[]): any {
        switch (value) {
            case "P": {
                return this.i18nService.getLabels("pipe-premio-tipo")["P"];
            }
            case "C": {
                return this.i18nService.getLabels("pipe-premio-tipo")["C"];
            }
            default: {
                return '';
            }
        }
    }
}