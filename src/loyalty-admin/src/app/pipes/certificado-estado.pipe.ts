import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';

@Pipe({ name: 'certificadoEstado' })
export class CertificadoEstadoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string): string {
        switch (value) {
            case "D": {
                return this.i18nService.getLabels("pipe-certificado-estado")["D"];
            }
            case "A": {
                return this.i18nService.getLabels("pipe-certificado-estado")["A"];
            }
            case "O": {
                return this.i18nService.getLabels("pipe-certificado-estado")["O"];
            } 
            default: { 
                return "";
            }
        }
    }
}