import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'tipoJuegoMision'
})

export class TipoJuegoMisionPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: any, ...args: any[]): any {
        switch (value) {
            case "R": {
                return this.i18nService.getLabels("pipe-premio-tipo")["P"];
            }
            case "A": {
                return this.i18nService.getLabels("pipe-premio-tipo")["C"];
            }
            case "O": {
                return this.i18nService.getLabels("pipe-premio-tipo")["C"];
            }
            default: {
                return '';
            }
        }
    }
}