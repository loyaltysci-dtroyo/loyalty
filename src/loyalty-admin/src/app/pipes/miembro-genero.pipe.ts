import { I18nService } from '../service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'generoMiembro'
})

export class GeneroMiembroPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: any, ...args: any[]): any {
        switch (value) {
            case "F": {
                return this.i18nService.getLabels("pipe-miembro-genero")["F"];
            }
            case "M": {
                return this.i18nService.getLabels("pipe-miembro-genero")["M"];
            }
            default: {
                return '';
            }
        }
    }
}