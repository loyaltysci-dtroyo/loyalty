import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'eventoNotificacion'
})

export class EventoNotificacionPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string, args: any[]): any {
        //M=mision P=promocion R=recompensa
         switch(value){
            case "M":{
                return this.i18nService.getLabels("pipe-evento-campana")["Manual"];
            }
            case "C":{
                return this.i18nService.getLabels("pipe-evento-campana")["Calendarizado"];
            }
            case "D":{
                 return this.i18nService.getLabels("pipe-evento-campana")["Trigger"];
            }
        }
    }
}