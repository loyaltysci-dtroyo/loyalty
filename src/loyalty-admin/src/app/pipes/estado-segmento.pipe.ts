import { I18nService } from '../service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'estadoSegmento'
})

export class EstadoSegmentoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string): any {
        return this.i18nService.getLabels("pipe-estado-segmento")[value]||"";  
    }
}