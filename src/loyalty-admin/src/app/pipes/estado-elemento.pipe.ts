import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({ name: 'estadoElemento' })
export class EstadoElementoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string): string {
        switch (value) {
            case "P": {
                return this.i18nService.getLabels("pipe-estado-elemento")["P"];
            }
            case "B": {
                return this.i18nService.getLabels("pipe-estado-elemento")["B"];
            }
            case "A": {
                return this.i18nService.getLabels("pipe-estado-elemento")["A"];
            }
            case "D": {
                return this.i18nService.getLabels("pipe-estado-elemento")["D"];
            }
            case "I": {
                return this.i18nService.getLabels("pipe-estado-elemento")["I"];
            }
            default:{
                return "";
            }
        }
    }
}