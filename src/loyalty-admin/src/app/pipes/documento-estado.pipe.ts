import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';

@Pipe({
    name: 'documentoEstado'
})

export class DocumentoEstadoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: boolean, args: any[]): any {
        switch (value) {
            case true: {
                return this.i18nService.getLabels("pipe-documento-estado")["true"];
            }
            case false: {
                return this.i18nService.getLabels("pipe-documento-estado")["false"];
            }
            default:{
                return "";
            }
        }
    }
}