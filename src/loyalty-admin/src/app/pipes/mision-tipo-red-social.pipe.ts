import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'tipoRedSocial'
})

export class MisionTipoRedSocialPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){}
    transform(value: string, args: any[]): any {
         return this.i18nService.getLabels("pipe-tipo-red-social")[value];
    }
}