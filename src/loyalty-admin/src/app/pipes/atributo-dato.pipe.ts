import { I18nService } from '../service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'tipoDato' })
export class TipoDatoPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string): string {
        switch(value){
            case 'T': {
                return this.i18nService.getLabels("pipe-atributo-tipoDato")["T"];
            };
            case 'F': {
                return this.i18nService.getLabels("pipe-atributo-tipoDato")["F"];
            };
            case 'B': {
                return this.i18nService.getLabels("pipe-atributo-tipoDato")["B"];
            };
            case 'N': {
                return this.i18nService.getLabels("pipe-atributo-tipoDato")["N"];
            };
            case 'M': {
                return this.i18nService.getLabels("pipe-atributo-tipoDato")["M"];
            };
            default: {
                 return "";
            };
        }
    }
}