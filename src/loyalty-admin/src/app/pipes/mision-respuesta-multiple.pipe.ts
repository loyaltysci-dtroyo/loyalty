import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'indRespMultiple'
})

export class RespuestaMultiplePipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){}
    transform(value: string, args: any[]): any {
         return this.i18nService.getLabels("misiones-contenido-itemsRespuestaCorrectaMultiple").filter(element => {
            return element.value == value;
        }).map(element => {
            return element.label
        });
    }
}