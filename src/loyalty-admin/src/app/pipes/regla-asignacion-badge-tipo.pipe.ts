import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';

@Pipe({
    name: 'reglaAsignacionBadgeTipo'
})

export class ReglaAsignacionBadgeTipoPipe implements PipeTransform {
     constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string, args: any[]): any {
        return this.i18nService.getLabels("reglas-asignacion-badge-tipo-regla").find((element)=>{return element.value==value}).label || "";
        }
}