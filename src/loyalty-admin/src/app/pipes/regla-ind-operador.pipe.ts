import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'indReglaOperador'
})

export class ReglaIndicadorOperadorPipe implements PipeTransform {
    constructor(
        private i18nService: I18nService
    ) {

    }
    transform(value: string): any {
        let operadoresTexto: any[] = this.i18nService.getLabels("segmentos-operadoresTexto");
        let operadoresNumericos: any[] = this.i18nService.getLabels("segmentos-operadoresNumericos");
        let operadoresSeleccionMultiple: any[] = this.i18nService.getLabels("segmento-encuesta-operadores-selecc-multiple");
        let operadoresSeleccionUnica: any[] = this.i18nService.getLabels("segmento-encuesta-operadores-selecc-unica");

        let operadores=operadoresTexto.concat(operadoresNumericos).concat(operadoresSeleccionMultiple).concat(operadoresSeleccionMultiple);
        let txt= operadores.find(element => {
            return element.value == value;
        });
    
        console.log("transforming", value, txt, operadores);
        if(txt){
            return txt.label;
        }else{
            return "";
        }
    }
}