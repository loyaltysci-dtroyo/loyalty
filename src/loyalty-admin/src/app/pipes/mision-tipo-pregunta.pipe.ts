import { I18nService } from '../service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'tipoPregunta' })
export class TipoPreguntaPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){}
    transform(value: any, ...args: any[]): any {
        switch (value) {
            case "E": {
                return this.i18nService.getLabels("pipe-tipo-pregunta")["E"];
            }
            case "C": {
                return this.i18nService.getLabels("pipe-tipo-pregunta")["C"];
            }
            default: {
                return '';
            }
        }
    }
}