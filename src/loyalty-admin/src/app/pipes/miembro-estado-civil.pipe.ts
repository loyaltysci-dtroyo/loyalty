import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'estadoCivilMiembro'
})

export class EstadoCivilMiembroPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: any, ...args: any[]): any {
        switch (value) {
            case "C": {
                return this.i18nService.getLabels("pipe-miembro-estadoCivil")["C"];
            } 
            case "S": {
                return this.i18nService.getLabels("pipe-miembro-estadoCivil")["S"];
            }
            case "U": {
                return this.i18nService.getLabels("pipe-miembro-estadoCivil")["U"];
            }
            case "N": {
                return this.i18nService.getLabels("pipe-miembro-estadoCivil")["O"];
            }
            default: {
                return '';
            }
            
        }
    }
}