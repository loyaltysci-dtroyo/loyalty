import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'tipoPremioJuego'
})
export class TipoPremioJuegoPipe implements PipeTransform {

    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string, args: any[]): any {
        switch (value) {
            case "P": {
                return this.i18nService.getLabels("pipe-tipo-premioJuego")["P"];
            }
            case "M": {
                return this.i18nService.getLabels("pipe-tipo-premioJuego")["M"];
            }
            case "G": {
                return this.i18nService.getLabels("pipe-tipo-premioJuego")["G"];
            }
        }
    }
}