import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({
    name: 'indOperador'
})

export class IndOperadorPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string): string {
        switch(value){
            case "A":{
                return this.i18nService.getLabels("pipe-ind-operador")["AND"];
            }
            case "O":{
                 return this.i18nService.getLabels("pipe-ind-operador")["OR"];
            }
            case "N":{
                 return this.i18nService.getLabels("pipe-ind-operador")["NOTIN"];
            }
        }
    }
}