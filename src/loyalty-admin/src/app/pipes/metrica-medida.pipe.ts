import { Pipe, PipeTransform } from '@angular/core';
import { I18nService } from '../service';
@Pipe({ name: 'metricaTipoMedida' })
export class MetricaTipoMedidaPipe implements PipeTransform {
    constructor(
        private i18nService:I18nService
    ){

    }
    transform(value: string): string {
        switch (value) {
            case "P": {
                return this.i18nService.getLabels("pipe-metrica-medida")["P"];
            }
            case "V": {
                return this.i18nService.getLabels("pipe-metrica-medida")["V"];
            }
            case "A": {
                return this.i18nService.getLabels("pipe-metrica-medida")["A"];
            }
            case "U": {
                return this.i18nService.getLabels("pipe-metrica-medida")["U"];
            }
            case "D":{
                return this.i18nService.getLabels("pipe-metrica-medida")["D"];
            }
            case "Gasto":{
                return this.i18nService.getLabels("pipe-metrica-medida")["G"];
            }
            default: {
                return ""
            }
        }
    }
}