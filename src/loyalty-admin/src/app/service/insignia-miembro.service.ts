import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Insignia, InsigniaNivel, Miembro } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';


/**
 * Jaylin Centeno López 
 * Servicio utilizado para manejar las insignias de un miembro
 * Consume los servicio web disponibles en MiembroInsigniaNivelResource del API REST
 */
@Injectable()
export class InsigniaMiembroService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/insignias`;
    //Representa los headers que se necesitan para la petición
    public headers: Headers;
    public token:string;

    constructor(
        public _http: Http
    ) {
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* Método para asignar un nivel de insignia a un miembro 
    Recibe: el id de nivel, insignia y miembro
    Retorna: resultado de la transacción*/
    public asingnarNivelMiembro(idMiembro: string, idInsignia: string, idNivel: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idInsignia + "/miembro/" + idMiembro +"/nivel/"+idNivel;
        let MiembroJson = JSON.stringify(idNivel);
        return this._http.post(url, MiembroJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para asignar una insignia a un miembro 
    Recibe: el id de insignia y miembro
    Retorna: resultado de la transacción*/
    public asingnarInsigniaMiembro(idMiembro: string, idInsignia: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idInsignia + "/miembro/" + idMiembro;
        let MiembroJson = JSON.stringify(idInsignia);
        return this._http.post(url, MiembroJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para remover la asignacion de un nivel con un miembro
    Recibe: el id de nivel, insignia y miembro
    Retorna: resultado de la transacción*/
    public removerNivelMiembro(idMiembro: string, idInsignia: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idInsignia + "/miembro/" + idMiembro; 
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para obtener los niveles de insignia asociados a un miembro
    Recibe: el id del miembro, el id de la insignia
    Retorna: la lista de los miembros del grupo //insignias status */
    public obtenerNivelesMiembro(idMiembro: string, idInsignia: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idInsignia + "/miembro/" + idMiembro;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <InsigniaNivel[]>response.json())
            .catch(this.handleError);
    }


    //Método que permite controlar los errores de las transacciones
    public handleError(error: Response) {
        return Observable.throw(error.json().error || 'Server error');
    }

}
