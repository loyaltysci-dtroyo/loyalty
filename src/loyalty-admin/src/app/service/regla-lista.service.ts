import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { ReglaMetricaBalance, ListaReglas } from '../model/index';
import { AppConfig } from '../app.config';

/*  Flecha Roja Technologies
 *  Autor:Fernando Aguilar
 *  Service encargado de administrar las listas de reglas (grupos principales de reglas) de un segmento
 */
@Injectable()
export class ListaReglasService {

    actionUrl: string = `${AppConfig.API_ENDPOINT}/segmento`;
public token:string;
    public headers: Headers;
    /* Constructor, encargado de inicializar los headers que se usaran para realizar los request de http
     * Recibe: una instancia de objeto de tipo http que ingresa a traves de angular2 DI
     * Retorna: un observable con las ocurrencias de regla de este tipo para ese segmento
     */
    constructor(public http: Http) {
         this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);

    }

    /* Retorna todas las listas maestras de reglas asociadas al segmento proporcionado
     * URL: get /segmento/{id_segmento}/reglas
     * Recibe: el id del segmento
     * Retorna: un observable con las ocurrencias de regla de este tipo para ese segmento
     */
    public getListasReglas(idSegmento: string) {
         this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idSegmento + "/reglas", { headers: this.headers })
            .map((response: Response) => <ListaReglas[]>response.json())
            .catch(this.handleError);
    }
    /* Retorna todas las listas maestras de reglas asociadas al segmento proporcionado
      * URL: get /segmento/{id_segmento}/reglas
      * Recibe: el id del segmento
      * Retorna: un observable con las ocurrencias de regla de este tipo para ese segmento
      */
    public getSingleListaReglas(idSegmento: string, idRegla: string) {
         this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idSegmento + "/reglas/" + idRegla, { headers: this.headers })
            .map((response: Response) => <ListaReglas>response.json())
            .catch(this.handleError);
    }

    /* Agrega una lista maestra de reglas asociada al segmento proporcionado
     * URL: post /segmento/{id_segmento}/reglas
     * Recibe: el id del segmento y la lista a insertar
     * Retorna: un observable con las ocurrencias de regla de este tipo para ese segmento
     */
    public addListaReglas(idSegmento: string, listaReglas: ListaReglas) {
         this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let listaJSON = JSON.stringify(listaReglas);
        return this.http.post(this.actionUrl + "/" + idSegmento + "/reglas", listaJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Intercambia el orden de las listas de reglas 
         * URL: get /segmento/{id_segmento}/reglas/{id_lista}
         * Recibe: el id del segmento y un array con dos grupos de reglas los cuales cambiaran de orden
         * Retorna: un observable con las ocurrencias de regla de este tipo para ese segmento
         */
    public intercambiarListaReglas(idSegmento: string, listaReglas: ListaReglas[]) {
         this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let listaJSON = JSON.stringify(listaReglas);
        return this.http.put(this.actionUrl + "/" + idSegmento + "/reglas/intercambiar", listaJSON, { headers: this.headers })
            .catch(this.handleError);

    }

    /* Retorna todas las listas maestras de reglas asociadas al segmento proporcionado
     * URL: get /segmento/{id_segmento}/reglas/{id_lista}
     * Recibe: el id del segmento y el id de la lista de reglas a eliminar
     * Retorna: un observable con las ocurrencias de regla de este tipo para ese segmento
     */
    public removeListaReglas(idSegmento: string, idListaReglas: string) {
         this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idSegmento + "/reglas/" + idListaReglas, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Metodo encargado de manejar los errores 
     * Recibe: el error en formato Response
     * Retorna: un error de observable con el error producido en formato JSON
     */
    public handleError(error: Response) {
        console.log(error);
        return Observable.throw(error);
    }

}