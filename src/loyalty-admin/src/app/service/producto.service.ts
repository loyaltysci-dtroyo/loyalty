import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Producto } from '../model/index';
import { AppConfig } from '../app.config';
declare var URI: any;
/**
 * Jaylin Centeno López 
 * Servicio que permite la manipulación de los productos y sus atributos dinámicos y elegibles
 * Consume los servicios web disponibles en ProductoResource del API REST
 */
@Injectable()

export class ProductoService {

    //Contiene la ruta en el que se encuentra el API REST
    actionUrl: string = `${AppConfig.API_ENDPOINT}/producto`;
    public headers: Headers;
    public token;

    constructor(
        public _http: Http, public producto: Producto
    ) {
        //Son los headers que necesita tener la petición
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* Método que obtiene un producto específico 
    Recibe: el id del producto
    Retorna: el producto buscado*/
    public obtenerProductoPorId(idProducto: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idProducto;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Producto>response.json())
            .catch(this.handleError);
    }

    /*Método que obtiene todos los producto (por estado) 
    Recibe: estado del producto (Borrador/Publicado/Archivado)
    Retorna: una lista con todos los productos disponibles*/
    public busquedaProducto(estado: string[], cantidad: number, filaDesplazamiento: number, palabrasClaves?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl;
        let uri = URI(url);
         if(estado && estado.length > 0){
            estado.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if(palabrasClaves){
            uri.addSearch("busqueda", palabrasClaves)
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para agregar un nuevo producto 
    Recibe: el producto a insertar
    Retorna: retorna un json con el id del producto*/
    public insertarProducto(producto: Producto) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let ProductoJSON = JSON.stringify(producto);
        return this._http.post(this.actionUrl, ProductoJSON, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para actualizar un producto 
    Recibe: producto con los valores actualizados
    Retorna: respuesta de la transacción */
    public editarProducto(producto: Producto) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let ProductoJSON = JSON.stringify(producto);
        return this._http.put(this.actionUrl, ProductoJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para remover/archivar un producto 
    Recibe: id del producto
    Retorna: el resultado de la transacción*/
    public eliminarProducto(idProducto: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idProducto;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para verificar si existe el nombre interno del producto
    Recibe: nombre interno
    Retorna: resultado de la transacción (true/false) */
    public verificarNombreInterno(nombreInterno: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/comprobar-nombre/" + nombreInterno;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    /***************** Atributos dinámicos *************/
    /* Método para obtener los atributos que tiene asignado un producto específico
    Recibe: el id del producto
    Retorna: la lista de los atributos que tiene el producto */
    public obtenerAtributosPorProducto(idProducto: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl +"/"+ idProducto+ "/atributos/" ;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para obtener los atributos que no están asociados a un producto
    Recibe: el id del producto, la cantidad de filas y la fila de desplazamiento
    Retorna: la lista de los atributos no asociados*/
    obtenerAtributosNoProducto(idProducto: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl +"/"+ idProducto + '/atributos-desligados?cantidad=' + cantidad + '&registro=' + filaDesplazamiento;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /********************************************************************/
    /*------------------------Manejo de elegibles-----------------------*/

    /* Elegibles por Miembros*/

    /* Obtiene la lista de los miembros elegibles para el producto 
    Recibe: id del producto, cantidad de filas, fila de desplazamiento y la acción (I/E/D)
    Retorna la lista de los miembros elegibles según la acción*/
    obtenerMiembrosElegiblesProducto(idProducto: string, accion: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url;
        if (cantidad == 0 && filaDesplazamiento == 0) {
            url = this.actionUrl + "/" + idProducto + "/miembro?accion=" + accion;
        } else {
            url = this.actionUrl + "/" + idProducto + "/miembro?accion=" + accion + '&cantidad=' + cantidad + '&registro=' + filaDesplazamiento;
        }
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para ingresar una lista de miembros, para incluir o excluir
    Recibe: id del producto, lista de ids, accion (I/E/D)
    Retorna: resultado de la transacción (true/false) */
    agregarListaMiembros(idProducto: string, idlistaMiembro: string[], accion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idProducto + "/miembro/?accion=" + accion;
        return this._http.post(url, idlistaMiembro, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para remover una lista de miembros de la lista de elgibles de un producto
    Recibe: la id del producto y una lista de ids de miembros a eliminar a la lista de elegibles
    Retorna: un objeto Response con el id de la ubicacion*/
    eliminarListaMiembros(idProducto: string, idlistaMiembro: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idProducto + "/miembro/remover-lista";
        return this._http.put(url, idlistaMiembro, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para agregar un miembro a la lista de elgibles de un producto
    Recibe: el miembro a agrgar a la lista de elegibles y la accion(I/E)
    Retorna: un objeto Response con el id del miembro*/
    agregarMiembro(idProducto: string, idMiembro: string, accion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url =this.actionUrl + "/" + idProducto + "/miembro/" + idMiembro + "?accion="+accion;
        return this._http.post(url, "", { headers: this.headers })
            .catch(this.handleError);
    }
    /* Método para elimina un miembro a la lista de elgibles de un producto
    Recibe: el miembro a excluir de la lista de elegibles
    Retorna: resultado de la transacción*/
    eliminarMiembro(idProducto: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url =this.actionUrl + "/" + idProducto + "/miembro/" + idMiembro;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /**************************************************************/

    /* Elegibles por segmentos */

    /* Obtiene la lista de los segmentos elegibles para el producto 
    Recibe: id del producto, cantidad de filas, fila de desplazamiento y la acción (I/E/D)
    Retorna la lista de los segmentos elegibles según la acción*/
    obtenerSegmentoElegiblesProducto(idProducto: string, action: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url;
        if (cantidad == 0 && filaDesplazamiento == 0) { 
            url = this.actionUrl + "/" + idProducto + "/segmento?accion=" + action;
        } else {
            url = this.actionUrl + "/" + idProducto + "/segmento?accion=" + action + '&cantidad=' + cantidad + '&registro=' + filaDesplazamiento;
        }
        return this._http.get(url, { headers: this.headers })
           .catch(this.handleError);
    }

    /* Método para ingresar una lista de segmentos, para incluir o excluir
    Recibe: id del producto, lista de id de segmentos, acción (I/E)
    Retorna: resultado de la transacción (true/false) */
    agregarListaSegmentos(idProducto: string, idlistaSegmento: string[], accion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idProducto + "/segmento/?accion=" + accion;
        return this._http.post(url, idlistaSegmento, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para remover una lista de ids de segmentos de la lista de elgibles de un producto
    Recibe: la id del producto y una lista de ids de segmentos a eliminar a la lista de elegibles
    Retorna: resultado de la trasacción*/
    eliminarListaSegmentos(idProducto: string, idlistaProducto: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idProducto + "/segmento/remover-lista";
        return this._http.put(url, idlistaProducto, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para agregar un segmento a la lista de elgibles de un producto
    Recibe: el segmento a agrgar a la lista de elegibles
    Retorna: resultado de la transacción*/
    agregarSegmento(idProducto: string, idSegmento: string, accion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url =this.actionUrl + "/" + idProducto + "/segmento/" + idSegmento + "?accion="+accion;
        return this._http.post(url, "", { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para eliminar un segmento a la lista de elgibles de un producto
    Recibe: el id del segmento a excluir de la lista de elegibles
    Retorna: resultado de la transacción*/
    eliminarSegmento(idProducto: string, idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url =this.actionUrl + "/" + idProducto + "/segmento/" + idSegmento;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /**************************************************************/

    /* Elegibles por ubicación */

    /* Obtiene la lista de las ubicaciones elegibles para el producto 
    Recibe: id del producto, cantidad de filas, fila de desplazamiento y la acción (I/E/D)
    Retorna la lista de los ubicaciones elegibles según la acción*/
    obtenerUbicacionElegiblesProducto(idProducto: string, action: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url;
        if (cantidad == 0 && filaDesplazamiento == 0) {
            url = this.actionUrl + "/" + idProducto + "/ubicacion?accion=" + action;
        } else {
            url = this.actionUrl + "/" + idProducto + "/ubicacion?accion=" + action + '&cantidad=' + cantidad + '&registro=' + filaDesplazamiento;
        }
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para ingresar una lista de ubicaciones, para incluir o excluir
    Recibe: id del producto, lista de id de ubicaciones, accion (I/E)
    Retorna: resultado de la transacción (true/false) */
    agregarListaUbicaciones(idProducto: string, idlistaUbicaciones: string[], accion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idProducto + "/ubicacion/?accion=" + accion;
        return this._http.post(url, idlistaUbicaciones, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para remover una lista de ids de ubicaciones de la lista de elgibles de un producto
    Recibe: la id del producto y una lista de ids de ubicaciones a eliminar a la lista de elegibles
    Retorna: resultado de la trasacción*/
    eliminarListaUbicacion(idProducto: string, idlistaUbicaciones: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idProducto + "/ubicacion/remover-lista";
        return this._http.put(url, idlistaUbicaciones, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para agregar una ubucación a la lista de elgibles de un producto
    Recibe: el id de la ubicación a agrgar a la lista de elegibles
    Retorna: resultado de la transacción*/
    agregarUbicacion(idProducto: string, idUbicacion: string, accion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url =this.actionUrl + "/" + idProducto + "/ubicacion/" + idUbicacion + "?accion="+accion;
        return this._http.post(url, "", { headers: this.headers })
            .catch(this.handleError);
    }
    /* Método para eliminar un segmento a la lista de elgibles de un producto
    Recibe: el id del segmento a excluir de la lista de elegibles
    Retorna: resultado de la transacción*/
    eliminarUbicacion(idProducto: string, idUbicacion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url =this.actionUrl + "/" + idProducto + "/ubicacion/" + idUbicacion;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}