import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { CategoriaProducto, SubCategoriaProducto } from '../model/index';
import { AppConfig } from '../app.config';
declare var URI: any;

@Injectable()
/**
 * Jaylin Centeno López 
 * Servicio utilizado para manejar las subcategorias del producto
 * Consume los servicio web disponibles en CategoriaProductoResource del API REST
 */
export class SubCategoriaProductoService {

    //Contiene la ruta en el que se encuentra el servicio REST
    actionUrl: string = `${AppConfig.API_ENDPOINT}/categoria-producto`;
    public headers: Headers;
    public token;

    constructor(
        public _http: Http
    ) {
        //Son los headers que necesita tener la petición
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* Método para agregar una nueva categoría 
    Recibe: categoría a insertar
    Retorna: retorna un json con el id de la categoría*/
    public insertarSubCategoria(idCategoria: string, subcategoria: SubCategoriaProducto) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let CategoriaJSON = JSON.stringify(subcategoria);
        let url = this.actionUrl + "/" + idCategoria + "/subcategoria";
        return this._http.post(url, CategoriaJSON, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para actualizar una subcategoría 
    Recibe: subcategoría con los valores actualizados
    Retorna: respuesta de la transacción */
    public editarSubCategoria(idCategoria: string, subcategoria: SubCategoriaProducto) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let SubcategoriaJSON = JSON.stringify(subcategoria);
        let url = this.actionUrl + "/" + idCategoria + "/subcategoria";
        return this._http.put(url, SubcategoriaJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para remover una categoría del sistema
    Recibe: id de la categoría
    Retorna: el resultado de la transacción*/
    public eliminarSubCategoria(idCategoria: string, idSubcategoria: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idCategoria + "/subcategoria/" + idSubcategoria;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método que obtiene una categoría en específico 
    Recibe: el id categoría
    Retorna: la categoría que coincide con el id*/
    public obtenerSubCategoriaPorId(idCategoria: string, idSubcategoria: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idCategoria + "/subcategoria/" + idSubcategoria;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <SubCategoriaProducto>response.json())
            .catch(this.handleError);
    }

    /* Método para obtener lista de subcategorías que coincidan con la búsqueda 
    Recibe: id de la categoría, los criterios de búsqueda, cantidad de filas, la fila de desplazamiento
    //cantidad y filaDesplazamiento son para la paginación
    Retorna: lista con las categorías que cumplen los criterios de búsqueda*/
    public busquedaSubCategorias(idCategoria: string, cantidad: number, filaDesplazamiento: number, busqueda?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idCategoria + "/subcategoria/buscar/";
        let uri = URI(url);
        if (busqueda) {
            uri.addSearch("busqueda", busqueda)
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para verificar si existe el nombreInterno de la categoría
   Recibe: el nombre 
   Retorna: resultado de la transacción (true/false) */
    public verificarNombre(idCategoria: string, nombreInterno: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/subcategoria/exists/" + nombreInterno;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <boolean>response.json())
            .catch(this.handleError);
    }

    /* ----------------------------------------------------------------------------*/
    /* ---------- Asignación de categoría y subcategoría a un prodcuto ------------*/

    /* Método para asingar una categoría y subcategoría a un producto
    Recibe: categoría a insertar
    Retorna: retorna un json con el id de la categoría*/
    public asignarSubCategoriaProducto(idCategoria: string, idSubcategoria: string, idProducto: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        //let CategoriaJSON = JSON.stringify(subcategoria);
        let url = this.actionUrl + "/" + idCategoria + "/subcategoria/" + idSubcategoria + "/producto/" + idProducto;
        return this._http.post(url, "", { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para remover una asignación de un producto con una categoría y subcategoría
    Recibe: el id del producto, de la categoría y subcategoría
    Retorna: el resultado de la transacción */
    public removerAsignacionSubCategoria(idCategoria: string, idSubcategoria: string, idProducto: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/subcategoria/" + idSubcategoria + "/producto/" + idProducto;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para agregar una lista de productos a una categoría
    Recibe: la lista de miembros (contiene solo el id del miembro)
    Retorna: resultado de la transacción*/
    public agregarListaProductos(listaIds: string[], idCategoria: string, idSubcategoria: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idCategoria + "/subcategoria/" + idSubcategoria + "/producto";
        let ProductosJson = JSON.stringify(listaIds);
        return this._http.post(url, ProductosJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para remover miembros de un grupo
    Recibe: la lista de miembros a remover
    Retorna: resultado de la transacción*/
    public removerListaProductos(listaIds: string[], idSubcategoria: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idSubcategoria + "/prodcuto/remover-lista";
        let ProductosJson = JSON.stringify(listaIds);
        return this._http.put(url, ProductosJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /* ----------------------------------------------------------------------------*/
    /* ---------- Obtener categorias/subcategorias, y productos ligados ------------*/

    /*Método que obtiene todas las subcategorías asignadas al producto
    Recibe:id de la categoria, cantidad de registro, la fila de desplazamiento
    //cantidad y filaDesplazamiento son para la paginación
    Retorna: una lista con todos las categorías disponibles*/
    public obtenerSubCategoriaProducto(idProducto: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url;
        if (cantidad == 0 && filaDesplazamiento == 0) {
            url = this.actionUrl + "/producto/" + idProducto;
        }
        else {
            url = this.actionUrl + "/producto/" + idProducto + "?cantidad=" + cantidad + "&registro=" + filaDesplazamiento;
        }
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /** Método para obtener las subcategorías no asignadas al producto
     * Recibe el id de categoía y producto
     * Retorna una lista con las subcategorias disponibles
     */
    public obtenerSubCategoriasDisponibles(idProducto: string, idCategoria: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/producto/" + idProducto + "/categoria/" + idCategoria + "/subcategoria-disponible";
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método que obtiene las categorias que no estan asociadas al producto 
    Recibe: id del producto
    Retorna: una lista con las categorias/subcategorías no asignadas*/
    public obtenerNoSubCategoriaProducto(idProducto: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/producto/" + idProducto + "/categoria-desligado";
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método que obtiener los productos que no pertenecen a esa categoría
    Recibe:id de la categoria, cantidad de registro, la fila de desplazamiento
    //cantidad y filaDesplazamiento son para la paginación
    Retorna: una lista con todos las categorías disponibles*/
    public obtenerProductosNoCategoria(idSubcategoria: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url;
        if (cantidad == 0 && filaDesplazamiento == 0) {
            url = this.actionUrl + "/" + idSubcategoria + "/producto-desligado";
        }
        else {
            url = this.actionUrl + "/" + idSubcategoria + "/producto-desligado?cantidad=" + cantidad + "&registro=" + filaDesplazamiento;
        }
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    

    /*Método para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}
