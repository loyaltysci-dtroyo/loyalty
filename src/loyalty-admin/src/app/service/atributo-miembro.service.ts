import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { AtributoDinamico, Miembro } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';

@Injectable()

/**
 * Jaylin Centeno López 
 * Se utiliza este servicio para la asignacion de los atributos a un miembro
 * Consume los servicio web disponibles en AtributoMiembroResource del API REST
 */
export class AtributoMiembroService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/atributo`;
    //Representa los headers que se necesitan para la petición
    public headers: Headers;
    public token: string;

    constructor(
        public atributo: AtributoDinamico,
        public miembro: Miembro,
        public _http: Http
    ) {
        //Headers para realizar las peticiones
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);

    }

    /* Método para obtener los miembros que tiene asignado un atributo específico
    Recibe: el id del atributo
    Retorna: la lista de los miembros con ese atributo */
    public obtenerMiembrosAtributo(idAtributo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/atributo/" + idAtributo;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Miembro[]>response.json())
            .catch(this.handleError);
    }

    /* Método para asociar un miembro a un atributo
    Recibe: el id de miembro y de atributo
    Retorna: resultado de la transacción*/
    public asignarAtributo(idMiembro: string, atributo: AtributoDinamico) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + atributo.idAtributo + "/miembro/" + idMiembro;
        return this._http.post(url, atributo.valorAtributoMiembro, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para actualizar el valor del atributo
    Recibe: el id de miembro y del atributo
    Retorna: resultado de la transacción*/
    public editarAtributo(idMiembro: string, atributo: AtributoDinamico) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', ' text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + atributo.idAtributo + "/miembro/" + idMiembro;
        return this._http.put(url, atributo.valorAtributoMiembro, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para remover la relación entre un atributo y un miembro
    Recibe: el id del atributo y del miembro
    Retorna: resultado de la transacción*/
    public removerAtributo(idMiembro: string, idAtributo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idAtributo + "/miembro/" + idMiembro;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Metodo para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }

}