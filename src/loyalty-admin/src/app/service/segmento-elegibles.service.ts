import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { AppConfig } from '../app.config';

/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar Morales
 * Service encargado de manipular ciertas tareas del back-end relativas al calculo de segmentos bajo peticion
 * recuperacion de estadisticas entre otros
 * Path: segmento/{idSegmento}/eligibles
 */
@Injectable()
export class SegmentoElegiblesService {

    actionUrl: string = `${AppConfig.API_ENDPOINT}/segmento`;
    public headers: Headers;
    public token:string;
    constructor(public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /*GET
     * Realiza un get al API y para recuperar todos los elegibles de un determinado segmento
     * Path: segmento/{idSegmento}/eligibles/miembros
     */
    getListaEligibles(idSegmento: string, cantRegistros: number, rowOffset: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + `/${idSegmento}/eligibles/miembros?cantidad=${cantRegistros}&registro=${rowOffset}`, { headers: this.headers })
        .catch(this.handleError);
    }

    /* GET
     * Realiza un get al API y recupera info de los elegibles y del calculo del segmento
     * Path: segmento/{idSegmento}/eligibles/info
     */
    getInfoEligibles(idSegmento: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + `/${idSegmento}/eligibles/info`, { headers: this.headers }).map((element) => { return element.json() }).catch(this.handleError);
    }

    /* POST
    * Realiza un post al API para solicitar un recalculo de segmento en particular
    * Path: segmento/{idSegmento}/eligibles/recalcular
    */
    recalculateEligibles(idSegmento: string) {
        this.headers.set('Accept', '*');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + `/${idSegmento}/eligibles/recalcular`, "", { headers: this.headers }).catch(this.handleError);
    }

    /* GET
     * Obtiene el estado del id de trabajo en ejecucion
     * Path: segmento/{idSegmento}/eligibles/recalcular/{idTrabajo}
     */
    statusRecalculateJob(idSegmento: string, idTrabajo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + `/${idSegmento}/eligibles/recalcular/${idTrabajo}`, { headers: this.headers }).catch(this.handleError);
    }

    /* PUT
     * Realiza un put al API para solicitar que se cancele el recalculo de segmento, en dado caso todos los cambios seran revertidos
     * Path: segmento/{idSegmento}/eligibles/recalcular/{idTrabajo}/cancelar
     */
    cancelRecalculateJob(idSegmento: string, idTrabajo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + `/${idSegmento}/eligibles/recalcular/${idTrabajo}/cancelar`, "", { headers: this.headers }).map((element) => { return element.json() }).catch(this.handleError);
    }

    /*
    *Se encarga de manejar errores, y luego lo lanza hacia la capa de presentacion
    *para darle tratamiento
    */
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}