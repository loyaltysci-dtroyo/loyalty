import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Configuracion } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';

/**
 * Jaylin Centeno López 
 * Se utiliza este servicio para manejar la configuración general del sistema
 * Consume los servicio web disponibles en ConfiguracionResource del API REST
 */
@Injectable()
export class ConfiguracionService{


    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/configuracion`;
    //Representa los headers que se necesitan para la petición
    public headers: Headers;
    public token:string;

    constructor(
        public _http: Http 
    ){

        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* Método para obtener la configuracion inicial
    Recibe: ningun valor
    Retorna:informacion de la configuracion general*/
    public obtenerConfiguracion() {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl, { headers: this.headers })
            .map((response: Response) => <Configuracion>response.json())
            .catch(this.handleError);
    }

    /*Método para actualizar la configuracion general
    Recibe: la información actualizar
    Retorna: el resultado de la transacción*/
    public actualizarConfiguracion(configuracion: Configuracion) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let ConfiguracionJson = JSON.stringify(configuracion);
        let url = this.actionUrl+"/general";
        return this._http.put(url, ConfiguracionJson, { headers: this.headers })
            .catch(this.handleError);
    }

    //Método que permite controlar los errores de las transacciones
    public handleError(error: Response) {
        return Observable.throw(error);
    }

}
