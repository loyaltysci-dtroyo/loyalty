import { KeycloakService } from './index';
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Segmento } from '../model/index';
import { AppConfig } from '../app.config';
declare var URI: any;
@Injectable()
export class SegmentoService {
    //TODO: recordar actualizar el url desde la clase de configuracion
    actionUrl: string = `${AppConfig.API_ENDPOINT}/segmento`;
    public headers: Headers;
    public token: string;

    constructor(public Segmento: Segmento, public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    /*
    * verifica que el nombre de despliegue del segmento sea unico
    * GET /segmento/comprobar-nombre/{nombreInterno}
    * Retorna: un observable que contiene los segmentos
    */
    public validarNombre(nombre: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/comprobar-nombre/" + nombre, { headers: this.headers })
            .map((response: Response) => <string>response.text())
            .catch(this.handleError);
    }
    /*
    * Guarda una instancia de segmento 
    * URL: get /segmento/
    * Retorna: un observable que contiene los segmentos
    */
    public getListaSegmentos() {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http
            .get(this.actionUrl, { headers: this.headers })
            .map((response: Response) => <Segmento[]>response.json())
            .catch(this.handleError);
    }
    /*
   * Encuentra los segmentos que correspondan con los terminos de busqueda
   * URL:/segmento/buscar/{busqueda}
   * Retorna: un observable que contiene los segmentos
   */
    public buscarSegmentos(busqueda: string[], estados: string[], cantRegistros: number, rowOffset: number, tipos?: string[], filtros?: string[], tipoOrden?: string, campoOrden?: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);

        let query = this.actionUrl + "?busqueda=";
        if (busqueda != null) {
            query += busqueda.join(" ");
        }

        let uri = URI(query);

        if (estados.length > 0) {
            estados.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (tipos && tipos.length > 0) {
            tipos.forEach(element => {
                uri.addSearch("tipo", element);
            });
        }
        if (filtros && filtros.length > 0) {
            filtros.forEach(element => {
                uri.addSearch("filtro", element);
            });
        }
        if (tipoOrden) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (campoOrden) {
            uri.addSearch("campo-orden", campoOrden);
        }

        if (cantRegistros >= 0) {
            uri.addSearch("cantidad", cantRegistros);
        }
        if (rowOffset >= 0) {
            uri.addSearch("registro", rowOffset);
        }
        // console.log(uri);
        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }
    /*
    * Guarda una instancia de segmento 
    * URL: get /segmento/
    * Retorna: un observable que contiene los segmentos
    */
    public getListaSegmentosArchivados() {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/archive", { headers: this.headers })
            .map((response: Response) => <Segmento[]>response.json()).catch(this.handleError);
    }
    /*
    * Guarda una instancia de segmento 
    * URL: get /segmento/{id_segmento}
    * Recibe: una instancia con el segmento a almacenar
    * Retorna: un observable que contiene el segmento
    */
    public getSegmento(id: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + id, { headers: this.headers })
            .map((response: Response) => <Segmento>response.json()).catch(this.handleError);
    }
    /*
    * Guarda una instancia de segmento 
    * URL: post /segmento/
    * Recibe: una instancia con el segmento a almacenar
    * Retorna: un observable que proporciona información sobre la transacción
    */
    public addSegmento(Segmento: Segmento) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl, JSON.stringify(Segmento), { headers: this.headers })
            .catch(this.handleError);
    }
    /*
   * Actualiza una instancia de segmento 
   * URL: put /segmento/
   * Recibe: una instancia con el segmento a actualizar
   * Retorna: un observable que proporciona el segmento, o el error generado
   */

    public updateSegmento(Segmento: Segmento) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/", JSON.stringify(Segmento), { headers: this.headers })
            .catch(this.handleError);

    }
    /*
   * Elimina una instancia de segmento 
   * URL: delete /segmento/{id_segmento}
   * Recibe: una instancia con el segmento a Eliminar
   * Retorna: un observable que proporciona informacion sobre la transacción, o el error generado
   */
    public deleteSegmento(Segmento: Segmento) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + Segmento.idSegmento, { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Metodo encargado de manejar el error, retorna un error de observable con una representacion del error en JSON
     * 
     */
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}