import { RegistroActividad } from '../model/registro-actividad';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, Request, XHRConnection, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Miembro, Usuario, Segmento, Categoria, Ubicacion } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from './';
import { Observer } from 'rxjs/Rx';
@Injectable()
export class EstadisticaService {
    public actionUrl: string = `${AppConfig.API_ENDPOINT}/estadisticas`;
    public headers: Headers;
    public token: string;

    constructor(public http: Http, public kc: KeycloakService) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    // GET /estadisticas/metrica
    // Obtener las estadisticas de todas las metricas
    public getestadisticasPromociones(): Observable<any> {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/promocion", { headers: this.headers }).map((response: Response) => { return response.json() });
    }

    //  Obtener las estadisticas de todas las promociones
    public getEstadisticasMisiones() {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/mision", { headers: this.headers }).map((response: Response) => { return response.json() });
    }

    //  Obtener las estadisticas de todas las metricas
    public getEstadisticasMetricas() {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/metrica", { headers: this.headers }).map((response: Response) => { return response.json() });
    }

    //  Obtener las estadisticas de todas las notificaciones
    public getEstadisticasNotificaciones() {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/notificacion", { headers: this.headers }).map((response: Response) => { return response.json() });
    }

    //  Obtener las estadisticas de todos los premios
    public getEstadisticasPremios() {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/premio", { headers: this.headers }).map((response: Response) => { return response.json() });
    }

    // GET /estadisticas/metrica/ { idMetrica }
    // Obtener las estadisticas de una metrica
    public getestadisticasPromocion(idPromocion: string): Observable<any> {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/promocion/" + idPromocion, { headers: this.headers }).map((response: Response) => { return response.json() });
    }

    //  Obtener las estadisticas de una promocion
    public getEstadisticasMision(idMision: string) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/mision/" + idMision, { headers: this.headers }).map((response: Response) => { return response.json() });
    }

    //  Obtener las estadisticas de una metrica
    public getEstadisticasMetrica(idMetrica: string) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/metrica/" + idMetrica, { headers: this.headers }).map((response: Response) => { return response.json() });
    }

    //  Obtener las estadisticas de una notificacion
    public getEstadisticasNotificacion(idNotificacion: string) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/notificacion/" + idNotificacion, { headers: this.headers }).map((response: Response) => { return response.json() });
    }

    //  GET /estadisticas/premio/ 
    //  Obtener las estadisticas de una mision
    public getEstadisticasPremio(idPremio: string) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/premio/" + idPremio, { headers: this.headers }).map((response: Response) => { return response.json() });
    }

    //Miembros nuevos para todos los segmentos
    // GET /segmentos/miembros-nuevos
    public getEstadisticasSegmentoMiembrosNuevos() {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/segmentos/miembros-nuevos/", { headers: this.headers }).map((response: Response) => { return response.json() });
    }
    //Miembros segmentados vs no segmentados
    // GET /miembros/segmentados
    public getEstadisticasMiembrosSegmentados() {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/miembros/segmentados", { headers: this.headers }).map((response: Response) => { return response.json() });
    }
    //Distribución de edades por miembro
    // GET /miembros/distribucion-edades 
    public getEstadisticasMiembrosEdades() {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/miembros/distribucion-edades", { headers: this.headers }).map((response: Response) => { return response.json() });
    }

    //Miembros mas activos (top 10 estadisticas de completado de mision en orden ascendente)
    //GET /miembros/activos
    public getEstadisticasMiembrosActivos() {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/miembros/activos", { headers: this.headers }).map((response: Response) => { return response.json() });
    }
    //     Top de Miembros ganadores de premio 
    // @GET
    // @Path("/miembros/ganadores")
    // @Produces(MediaType.APPLICATION_JSON)
    public getEstadisticasMiembrosGanadores() {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/miembros/ganadores", { headers: this.headers }).map((response: Response) => { return response.json() });
    }

    // @GET
    // @Path("/mision/general")
    // @Produces(MediaType.APPLICATION_JSON)
    public getEstadisticasMisionGeneral() {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/mision/general", { headers: this.headers }).map((response: Response) => { return response.json() });
    }
}