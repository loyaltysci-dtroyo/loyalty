import 'rxjs/Rx';
import { Headers, Http, Request, RequestOptionsArgs, Response, XHRConnection } from '@angular/http';
import { AppConfig } from '../app.config';
import { Notificacion } from '../model/index';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

declare var URI: any;
/**
* Flecha Roja Technologies oct-2016
* Fernando Aguilar
* Servicio encargado de realizar operaciones de gestion de datos de notificaciones publicitarias interactuando directamente con el API
*/
@Injectable()
export class NotificacionService {
    public actionUrl: string = `${AppConfig.API_ENDPOINT}/notificacion`;
    public urlListaMiembros: string = `/miembro`;
    public urlListaPromociones: string = `/promocion`;
    public urlListaSegmentos: string = `/segmento`;
    public headers: Headers;
    public token: string;

    constructor(public notificacion: Notificacion, public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);

    }

    /********************OPERACIONES DE DATOS DE CAMPANAS**********************/
    /*
    * verifica que el nombre de despliegue de la notificacion no este en la base de datos 
    * URL: get /comprobar-nombre/{nombre}
    * Retorna: un observable que contiene los Notificacions
    */
    public validarNombre(nombre: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/comprobar-nombre/" + nombre, { headers: this.headers }).map((response: Response) => <string>response.text()).catch(this.handleError);
    }

    /*
     * Encuentra las Misiones que correspondan con los terminos de busqueda
     * Recibe:
     *        -busqueda: termino de busqueda
     *        -estados: array con los estados a tomar en cuenta para la busqueda (C,B,A)
     *        -rowOffset: offset de row inicial para la paginacion
     *        -cantRegistros: cantidad de registros a traer del API
     *        -tipos-filtros
     * 
     * URL:/Promocion/buscar/{busqueda}
     * Retorna: un observable que contiene los Promocions
     */
    public buscarNotificaciones(busqueda: string[], estados: string[], rowOffset: number, cantRegistros: number, tipos?: string[], filtros?: string[], eventos?: string[], objetivos?: string[], tipoOrden?: string, campoOrden?: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);

        let query = this.actionUrl;
        if (busqueda != null) {
            query += "?busqueda=";
            query += busqueda.join(" ");
        }
        let uri = URI(query);

        if (estados.length > 0) {
            estados.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (tipos && tipos.length > 0) {
            tipos.forEach(element => {
                uri.addSearch("tipo", element);
            });
        }
        if (filtros && filtros.length > 0) {
            filtros.forEach(element => {
                uri.addSearch("filtro", element);
            });
        }
        if (eventos && eventos.length > 0) {
            eventos.forEach(element => {
                uri.addSearch("evento", element);
            });
        }
        if (objetivos && objetivos.length > 0) {
            objetivos.forEach(element => {
                uri.addSearch("objetivo", element);
            });
        }
        if (tipoOrden && tipoOrden.length > 0) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (campoOrden && campoOrden.length > 0) {
            uri.addSearch("campo-orden", campoOrden);
        }
        if (cantRegistros >= 0) {
            uri.addSearch("cantidad", cantRegistros);
        }
        if (rowOffset >= 0) {
            uri.addSearch("registro", rowOffset);
        }
        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /*
    * Obtiene una instancia de notificacion 
    * URL: get /notificacion/{id_Notificacion}
    * Recibe: una instancia con la campaña a almacenar
    * Retorna: un observable que contiene la Notificacion
    */
    public getNotificacion(id: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + id, { headers: this.headers }).map((response: Response) => <Notificacion>response.json()).catch(this.handleError);
    }

    /*
    * Guarda una instancia de Notificacion 
    * URL: post /notificacion/
    * Recibe: una instancia con la campaña a almacenar
    * Retorna: un observable que proporciona información sobre la transacción
    */
    public addNotificacion(notificacion: Notificacion) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl, JSON.stringify(notificacion), { headers: this.headers }).catch(this.handleError);
    }
    /*
   * Actualiza una instancia de Notificacion 
   * URL: put /Notificacion/
   * Recibe: una instancia con la campaña a actualizar
   * Retorna: un observable que proporciona el Notificacion, o el error generado
   */
    public updateNotificacion(notificacion: Notificacion) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl, JSON.stringify(notificacion), { headers: this.headers }).catch(this.handleError);
    }
    /*
   * Elimina una instancia de Notificacion 
   * URL: delete /Notificacion/{id_Notificacion}
   * Recibe: una instancia con la campañaa Eliminar
   * Retorna: un observable que proporciona informacion sobre la transacción, o el error generado
   */
    public deleteNotificacion(notificacion: Notificacion) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + notificacion.idNotificacion, { headers: this.headers }).catch(this.handleError);
    }

    /************OPERACIONES DE OBTENER MIEMBROS/SEGMENTOS ELEGIBLES*************/
    
    /**
    * Retorna la lista de segmentos elegibles de la campaña segun el tipo, sea disponibles o asignados
    * Retorna: lista conteniendo los segmentos elegibles
    * Recibe: el id de la campaña 
    */
    listarSegmentosNotificacion(idNotificacion: string, tipo: string, estados: string[], busqueda: string[], filaDesplazamiento: number, cantRegistros: number, tipos?: string[], filtros?: string[], tipoOrden?: string, campoOrden?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let query = this.actionUrl + "/" + idNotificacion + "/segmento";
        if (busqueda != null) {
            query += "?busqueda=";
            query += busqueda.join(" ");
        }
        let uri = URI(query);

        if (estados.length > 0) {
            estados.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (tipos && tipos.length > 0) {
            tipos.forEach(element => {
                uri.addSearch("tipos", element);
            });
        }
        if (tipo) {
            uri.addSearch("tipoLista", tipo);
        }
        if (filtros && filtros.length > 0) {
            filtros.forEach(element => {
                uri.addSearch("filtro", element);
            });
        }
        if (tipoOrden && tipoOrden.length > 0) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (campoOrden && campoOrden.length > 0) {
            uri.addSearch("campo-orden", campoOrden);
        }
        if (cantRegistros >= 0) {
            uri.addSearch("cantidad", cantRegistros);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this.http.get( uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /**
    * Retorna la lista de segmentos elegibles de la campaña
    * Retorna: lista conteniendo los segmentos elegibles
    * Recibe: el id de la campaña 
    */
    listarSegmentosElegiblesNotificacion(idNotificacion: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idNotificacion + "/segmento", { headers: this.headers }).map((response: Response) => <Notificacion>response.json())
            .catch(this.handleError);
    }
    /**
    * Retorna la lista de segmentos elegibles de la campaña
    * Retorna: lista conteniendo los segmentos disponibles
    * Recibe: el id de la campaña 
    */
    listarSegmentosDisponiblesNotificacion(idNotificacion: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idNotificacion + "/segmento?tipo=D", { headers: this.headers }).map((response: Response) => <Notificacion>response.json())
            .catch(this.handleError);
    }

    /**
    * Retorna la lista de miembros elegibles de la campaña
    * Retorna: lista conteniendo los miembros elegibles
    * Recibe: el id de la campaña 
    */
    listarMiembrosElegiblesNotificacion(idNotificacion: string, rowOffset: number, cantRegistros: number, busqueda: string) {
        let estadoMiembro = "A";
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);

        let query = this.actionUrl + "/" + idNotificacion + "/miembro";
        let uri = URI(query);

        if (busqueda) {
            uri.addSearch("busqueda", busqueda)
        }
        if (cantRegistros >= 0) {
            uri.addSearch("cantidad", cantRegistros);
        }
        if (rowOffset >= 0) {
            uri.addSearch("registro", rowOffset);
        }
        uri.addSearch("estado", estadoMiembro);


        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }
    /**
   * Retorna la lista de miembros elegibles de la campaña
   * Retorna: lista conteniendo los miembros elegibles
   * Recibe: el id de la campaña 
   */
    listarMiembrosDisponiblesNotificacion(idNotificacion: string, rowOffset: number, cantRegistros: number, busqueda: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let estadoMiembro = "A";
        let query = this.actionUrl + "/" + idNotificacion + "/miembro?tipo=D";
        let uri = URI(query);

        if (busqueda) {
            uri.addSearch("busqueda", busqueda)
        }
        if (cantRegistros >= 0) {
            uri.addSearch("cantidad", cantRegistros);
        }
        if (rowOffset >= 0) {
            uri.addSearch("registro", rowOffset);
        }
        uri.addSearch("estado", estadoMiembro);

        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }
    /**
    * Retorna la lista de miembros elegibles de la campaña
    * Retorna: lista conteniendo los miembros elegibles
    * Recibe: el id de la campaña 
    */
    listarPromocionesElegiblesNotificacion(idNotificacion: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idNotificacion + "/promocion", { headers: this.headers }).map((response: Response) => <Notificacion>response.json())
            .catch(this.handleError);
    }
    /**
   * Retorna la lista de miembros elegibles de la campaña
   * Retorna: lista conteniendo los miembros elegibles
   * Recibe: el id de la campaña 
   */
    listarPromocionesDisponiblesNotificacion(idNotificacion: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idNotificacion + "/promocion?tipo=D", { headers: this.headers }).map((response: Response) => <Notificacion>response.json())
            .catch(this.handleError);
    }

    /************OPERACIONES DE INCLUSION/EXCLUSION DE MIEMBROS/SEGMENTOS ELEGIBLES*************/

    /**Incluye un miembro a la lista de elgibles de una notificacion publicitaria
     * Recibe: el miembro a agrgar a la lista de elegibles
     * Retorna: un objeto Response con el id del miembro
     */
    incluirMiembro(idNotificacion: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idNotificacion + "/miembro/" + idMiembro, "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye una lista de miembros a la lista de elgibles de una notificacion publicitaria
    * Recibe: el miembro a agrgar a la lista de elegibles
    * Retorna: un objeto Response con el id del miembro
    */
    incluirListaMiembros(idNotificacion: string, idsMiembro: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idNotificacion + "/miembro/", idsMiembro, { headers: this.headers })
            .catch(this.handleError);
    }

    /**Elimina un miembro a la lista de elgibles de una notificacion publicitaria
     * Recibe: el miembro a excluir de la lista de elegibles
     * Retorna: un objeto Response con el id del miembro
     */
    eliminarMiembro(idNotificacion: string, idMiembro: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idNotificacion + "/miembro/" + idMiembro, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Elimina una lista de miembros a la lista de elgibles de una notificacion publicitaria
     * Recibe: el miembro a excluir de la lista de elegibles
     * Retorna: un objeto Response con el id del miembro
     */
    eliminarListaMiembros(idNotificacion: string, idsMiembro: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idNotificacion + "/miembro/remover-lista", idsMiembro, { headers: this.headers })
            .catch(this.handleError);
    }

    /**Incluye un segmento a la lista de elgibles de una notificacion publicitaria
    * Recibe: el segmento a agrgar a la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    incluirSegmento(idNotificacion: string, idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idNotificacion + "/segmento/" + idSegmento, "", { headers: this.headers })
            .catch(this.handleError);
    }

    /**Incluye una lista de segmentos a la lista de elgibles de una notificacion publicitaria
    * Recibe: el segmento a agrgar a la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    incluirListaSegmento(idNotificacion: string, idsSegmento: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idNotificacion + "/segmento/", idsSegmento, { headers: this.headers })
            .catch(this.handleError);
    }

    /**Elimina un segmento a la lista de elgibles de una notificacion publicitaria
    * Recibe: el segment a excluir de la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    eliminarSegmento(idNotificacion: string, idSegmento: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idNotificacion + "/segmento/" + idSegmento, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Elimina una lista de segmentos a la lista de elgibles de una notificacion publicitaria
    * Recibe: el segment a excluir de la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    eliminarListaSegmento(idNotificacion: string, idsSegmento: string[]) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idNotificacion + "/segmento/remover-lista", idsSegmento, { headers: this.headers })
            .catch(this.handleError);
    }

    /**Incluye una promocion a la lista de elgibles de una notificacion publicitaria
   * Recibe: la promocion a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    incluirPromocion(idNotificacion: string, idPromo: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idNotificacion + "/promocion/" + idPromo, "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye un listado de promocion a la lista de elgibles de una notificacion publicitaria
* Recibe: la promocion a agrgar a la lista de elegibles
* Retorna: un objeto Response con el id de la ubicacion
*/
    incluirListaPromocion(idNotificacion: string, idsPromo: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idNotificacion + "/promocion/", idsPromo, { headers: this.headers })
            .catch(this.handleError);
    }

    /**Elimina una promocion a la lista de elgibles de un segmento
    * Recibe: el segment a excluir de la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    eliminarPromocion(idNotificacion: string, idPromo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idNotificacion + "/promocion/" + idPromo, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Elimina un listado de promocion de la lista de elgibles de un segmento
* Recibe: el segment a excluir de la lista de elegibles
* Retorna: un objeto Response con el id de la ubicacion
*/
    eliminarListaPromocion(idNotificacion: string, idsPromo: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idNotificacion + "/promocion/remover-lista", idsPromo, { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Metodo encargado de manejar el error, retorna un error de observable 
     * con una representacion del error en JSON
     */
    public handleError(error: Response) {
        return Observable.throw(error);
    }




}