import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { ReglaMetricaBalance, ListaReglas } from '../model/index';
import { AppConfig } from '../app.config';

/**Flecha Roja Technologies
 * Autor:Fernando Aguilar 
 * Service encargado de realizar mantenimineto de metadatos de reglas de balance de metrica 
 */
@Injectable()
export class ReglaMetricaBalanceService {

    actionUrl: string = `${AppConfig.API_ENDPOINT}/segmento`;
    public headers: Headers; public token: string;

    constructor(public ReglaMetricaBalance: ReglaMetricaBalance, public _http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /*
    * Retorna una lista  de reglas de metrica-balance asociadas al segmento proporcionado según su identificador
    * URL:get /segmento/{id_segmento}/reglas/{id_lista}/metrica-balance/
    * Recibe: el id del segmento y el id de la lista de reglas
    * Retorna: un observable con las ocurrencias de regla de este tipo para ese segmento salvo errores
    */
    public getReglaMetricaBalanceList(idSegmento: string, idLista: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/" + idSegmento + "/reglas/" + idLista + "/metrica-balance", { headers: this.headers })
            .map((response: Response) => <ReglaMetricaBalance[]>response.json())
            .catch(this.handleError);
    }

    /*
    * Almacena una instancia de regla asociada al segmento y el id de lista de reglas proporcionado 
    * URL:post /segmento/{id_segmento}/reglas/{id_lista}/metrica-balance/
    * Recibe: el id del segmento, id de lista de reglas y la instancia de reglaMetricaBalance a insertar
    * Retorna: un observable con datos sobre la transacción
    */
    public addReglaMetricaBalance(idSegmento: string, idListaReglas: string, reglaMetricaBalance: ReglaMetricaBalance) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let reglaJSON = JSON.stringify(reglaMetricaBalance);
        return this._http.post(this.actionUrl + "/" + idSegmento + "/reglas/" + idListaReglas + "/metrica-balance", reglaJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /*
    * Retorna una instancia de regla asociada al segmento, el id de lista y el id de regla roporcionados 
    * URL:get /segmento/{id_segmento}/reglas/{id_lista}/metrica-balance/{id_regla}
    * Recibe: el id del segmento, id de lista de reglas y el id de regla a traer
    * Retorna: un observable que proporciona la métrica, o el error generado
    */
    public getReglaMetricaBalance(idSegmento: string, idLista: string, idReglaMetricaBalance: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/" + idSegmento + "/reglas/" + idLista + "/metrica-balance/" + idReglaMetricaBalance, { headers: this.headers })
            .map((response: Response) => <ReglaMetricaBalance>response.json())
            .catch(this.handleError);
    }


    /*
    * Elimina una instancia de regla asociada al segmento, el id de lista y el id de regla roporcionados 
    * URL:delete /segmento/{id_segmento}/reglas/{id_lista}/metrica-balance/{id_regla}
    * Recibe: el id del segmento, id de lista de reglas y el id de regla a traer
    * Retorna: un observable que proporciona la métrica, o el error generado
    */
    public deleteReglaMetricaBalance(idSegmento: string, idListaReglas: string, idReglaMetricaBalance: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.delete(this.actionUrl + "/" + idSegmento + "/reglas/" + idListaReglas + "/metrica-balance/" + idReglaMetricaBalance, { headers: this.headers })
            .catch(this.handleError);
    }

    /*
    * Encargado de manejar los errores 
    * Recibe: un response con el error que ocurrió
    */
    public handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json() || 'Server error');
    }
}