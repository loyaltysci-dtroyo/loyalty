import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Categoria } from '../model/index';
import { AppConfig } from '../app.config';
/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Servicio encargado de darle mantenimiento a las categorias de promocion 
 * */
@Injectable()
export class PromocionCategoriaService {

    actionUrl: string = `${AppConfig.API_ENDPOINT}/categoria-promocion/`;
    public headers: Headers; public token: string;
    constructor(public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    /**
     * GET
     * Obtiene una categoria del API
     * Recibe: el id de categoria a obtener
     */
    obtenerCategoria(idCategoria: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + idCategoria, { headers: this.headers }).map((response: Response) => <Categoria>response.json()).catch(this.handleError);
    }
    /**
     * GET
     * Retorna todas las categorias
     * Retorna:un observble con las categorias
     */
    obtenerCategorias() {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl, { headers: this.headers }).map((response: Response) => <Categoria[]>response.json()).catch(this.handleError);
    }
    /*
    * GET
    * Verifica si el nombre interno de categoria existe en la base de datos 
    * Retorna:un observble con las categorias
    */
    verificarNombreInterno(nombre: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "comprobar-nombre/" + nombre, { headers: this.headers }).catch(this.handleError);
    }

    /**
     * GET
     * Obtiene las categorias que estan asociadas a una promocion
     * Recibe:el id de Categoria
     * Retorna:un observble con las categorias
     */
    obtenerPromocionesPorCategoria(idCategoria: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + idCategoria + "/promocion", { headers: this.headers })
            .map((response: Response) => <Categoria>response.json())
            .catch(this.handleError);
    }
    /**
     * POST
     * Inserta una categoria
     * Params:categoria - Categoria a insertar
     */
    insertarCategoria(categoria: Categoria) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl, JSON.stringify(categoria), { headers: this.headers })
            .map((response: Response) => { }).catch(this.handleError);
    }
    /**
     * PUT
    * Modifica una categoria
    * Params:categoria - Categoria a modificar
    */
    modificarCategoria(categoria: Categoria) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl, JSON.stringify(categoria), { headers: this.headers })
            .catch(this.handleError);
    }
    /**
     * DELETE
    * Elimina una categoria
    * Params:idCategoria - id de la categoria a eliminar
    */
    eliminarCategoria(idCategoria: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + idCategoria, { headers: this.headers });
    }
    /**
     * POST
     * Asocia una promocion a una categoria
     * Params: idPromocion - promocion a la cual asociar la categoria
     *         idCategoria - categoria a asociar a la promo
     */
    asociarPromocionCategoria(idCategoria: string, idPromocion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + idCategoria + "/promocion/" + idPromocion,"", { headers: this.headers })
            .map((response: Response) => { }).catch(this.handleError);

    }
    /**
     * DELETE
     * Revoca la asociacion de una categoria a una promo
     * Params: idPromocion - promocion a  la cual revocar la categoria
     *         idCategoria - categoria a revocar de la promo
     */
    removerPrmocionCategoria(idCategoria: string, idPromocion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + idCategoria + "/promocion/" + idPromocion, { headers: this.headers });
    }
    /**
     * Metodo encargado de manejar el error, retorna un error de observable 
     * con una representacion del error en JSON
     */
    public handleError(error: Response) {

        return Observable.throw(error);
    }
}