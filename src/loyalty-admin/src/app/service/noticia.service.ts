import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Noticia, NoticiaComentario } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;

@Injectable()

/**
 * Jaylin Centeno López 
 * Servicio que permite manipular la entidad de noticias
 * Consume los servicio web disponibles en NoticiaResource
 */

/* ------------------------------------------------------*/
export class NoticiaService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/noticia`;
    public token: string;
    public headers: Headers;

    constructor(
        public _http: Http,
        public noticia: Noticia
    ) {
        //Headers para realizar las peticiones
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**
     * @GET
     * Obtiene el listado de noticias
     */
    public obtenerListaNoticias(estado: string[],cantidad: number, filaDesplazamiento: number, busqueda?: string, filtro?: string[], ordenTipo?:string, ordenCampo?:string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl);
        if (estado && estado.length > 0) {
            estado.forEach(element => uri.addSearch("estado", element));
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (busqueda && busqueda.length > 0) {
            uri.addSearch("busqueda", busqueda)
        }
        if (filtro && filtro.length > 0) {
            filtro.forEach(element => uri.addSearch("filtro", element));
        }
        if (ordenTipo) {
            uri.addSearch("tipo-orden", ordenTipo);
        }
        if (ordenCampo) {
            uri.addSearch("campo-orden", ordenCampo);
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /** 
     * @GET
     * Obtiene la noticia que corresponda al id que se pasa por parametros
     */
    public obtenerNoticiaId(id: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/" + id, { headers: this.headers }).catch(this.handleError);
    }

    /**
     * @POST
     * Agrega una nueva noticia
     */
    public insertarNoticia(noticia: Noticia) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl, JSON.stringify(noticia), {headers: this.headers}).catch(this.handleError);
    }

    /**
     * @PUT
     * Modifica una entidad de noticia
     */
    public modificarNoticia(noticia: Noticia) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.put(this.actionUrl, JSON.stringify(noticia), {headers: this.headers}).catch(this.handleError);
    }

    /** 
     * @DELETE
     * Remueve la noticia que corresponda al id que se pasa por parametros
     */
    public removernoticiaId(id: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.delete(this.actionUrl + "/" + id, { headers: this.headers }).catch(this.handleError);
    }

    /**
     * @GET
     * Obtiene el listado de las comentarios de una noticia  
     */
    public obtenerListaComentariosNoticia(idNoticia:string, since: number , until: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl+'/'+idNoticia+'/comentario');
        if (until) {
            uri.addSearch("until", until);
        }
        if (since) {
            uri.addSearch("since", since);
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /**
     * @GET
     * Obtiene el listado de las comentarios de un comentario  
     */
    public obtenerListaComentariosDeComentario(idNoticia:string, idComentario:string, until: number,since: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl+'/'+idNoticia+'/comentario/'+idComentario+'/comentario');
        if (until) {
            uri.addSearch("until", until);
        }
        if (since) {
            uri.addSearch("since", since);
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /**
     * @POST
     * Agrega un nuevo comentario
     */
    public insertarComentario(idNoticia: string, comentario: NoticiaComentario) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl+'/'+idNoticia+'/comentario', JSON.stringify(comentario), {headers: this.headers}).catch(this.handleError);
    }

    /**
     * @PUT
     * Modifica una entidad de comentario
     */
    public modificarComentario(idNoticia: string, comentario: NoticiaComentario) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.put(this.actionUrl+'/'+idNoticia+'/comentario', JSON.stringify(comentario), {headers: this.headers}).catch(this.handleError);
    }

    /** 
     * @DELETE
     * Remueve un comentario
     */
    public removerComentario(idNoticia: string, idComentario: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.delete(this.actionUrl +'/'+idNoticia+'/comentario/'+idComentario, { headers: this.headers }).catch(this.handleError);
    }

    /**
     * @POST
     * Asignación de una categoría a una noticia
     */
    public asignarCategoria(idCategoria: string, idNoticia: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl+"/"+idNoticia+"/categoria/"+idCategoria, '', {headers: this.headers}).catch(this.handleError);
    }
    /**
     * @DELETE
     * Remover asignación de una noticia a noticia
     */
    public removerAsignacionCategoria(idCategoria: string, idNoticia: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.delete(this.actionUrl+"/"+idNoticia+"/categoria/"+idCategoria, {headers: this.headers}).catch(this.handleError);
    }

    /*Metodo para controlar los errores que se dan durante las peticiones al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}