import { AppConfig } from '../app.config';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx'

declare var Keycloak: any;
declare var KeycloakAuthorization: any;

@Injectable()
/**
* Flecha Roja Technologies 
* Fernando Aguilar
* Servicio encargadAo de administrar la autenticacion y autorizacion con el identity provider 
*/
export class KeycloakService {
    static auth: any = {};
    static authz: any = {};
    static clientID = AppConfig.AUTHZ_API_CLIENT;
    static rpt: string;

    constructor() {

    }

    /** 
     * Metodo que inicializa el runtime de keycloak, es llamado desde antes de hacer el boostraping de la aplicacion
     * y no debería ser llamado en ningun otro lado de la aplicacion ya que para eso se expone este servicio desde el 
     * app module
    */
    static init(): Promise<any> {
        let keycloakAuth = new Keycloak(AppConfig.KC_CONF);
        KeycloakService.auth.loggedIn = false;
        return new Promise((resolve, reject) => {
            keycloakAuth.init({ onLoad: 'login-required' })
                .success(() => {
                    KeycloakService.auth.loggedIn = true;
                    KeycloakService.auth.auth = keycloakAuth;
                    KeycloakService.authz = new KeycloakAuthorization(keycloakAuth);
                    KeycloakService.auth.auth.updateToken(5).success(function (refreshed: any) {
                        KeycloakService.authz.authorize();
                        KeycloakService.authz.entitlement(KeycloakService.clientID).then((rpt: any) => KeycloakService.rpt = rpt);
                        resolve("OK");
                    }).error(function () {
                        reject("ERR");
                    });
                })
                .error(() => {
                    reject("ERR");
                });
        });
    }

    /**
     * Encargado de proveer un RPT actualizado, decodificado y listo para su uso
     * 
     */
    getRPT(): Promise<string> {
        return new Promise<string>((resolve) => {
            KeycloakService.authz.entitlement(KeycloakService.clientID).then((rpt: any) => {
                KeycloakService.rpt = rpt;
                var base64Url = rpt.split('.')[1];
                var base64 = base64Url.replace('-', '+').replace('_', '/');
                resolve(<string>JSON.parse(window.atob(base64)));
            })
        });

    }

    /**Metodo encargado de cerrar la sesion de un usuario */
    logout() {
        KeycloakService.auth.auth.logout();
        KeycloakService.auth = null;
        KeycloakService.authz = null;
    }
    /**Metodo encargado de crear un URL para redireccionar al usuario 
     *  a la pagina de amdinistracion de su cuneta enkeycloak donde puede 
     * cambiar su clave entre otros parametros */
    accountMAnagement() {
        return KeycloakService.auth.auth.createAccountUrl();
    }

    /** 
      * Metodo encagado de generar un access token para el usuario logueado en ese momento 
      *El token de acceso tiene una vida util finita, por lo tanto necefsitamos refrescar el 
      *token de acceso si este esta expirado antes de enviar requests al API, de locontrario el API
      *va a rechazar todo request
      */
    getToken(): Promise<string> {
        return new Promise<string>((resolver) => {
            //console.log(KeycloakService.auth)
            if (KeycloakService.auth.auth.isTokenExpired(4)) {
                KeycloakService.auth.auth.updateToken(5)
                    .then(
                    (refreshed: any) => {
                        //console.log("token obtenido");
                        resolver(<string>KeycloakService.auth.auth.token);
                    },() => {
                          console.log("Error obteniendo token");
                          //sesio expirada, iniciando nuevamente
                          KeycloakService.init();
                    });
            } else {
                // console.log("usando token existente");
                resolver(<string>KeycloakService.auth.auth.token);
            }
        });
    }
    /*
    *Metodo que se encarga de obtener el id del usuario que se encuentra actualmente en sesion 
    */
    getUsuario() {
        return new Promise<string>((resolve, reject) => {
            KeycloakService.auth.auth.updateToken(5)
                .success(
                () => { resolve(<string>KeycloakService.auth.auth.subject) }
                )
                .error(
                () => reject('Failed ')
                );
        });

    }
    getUserProfile() {
        KeycloakService.auth.loadUserProfile();
    }
}