import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { JuegoMillonario } from '../model/index';
import { AppConfig } from '../app.config';
declare var URI: any;

/**
 * Jaylin Centeno López 
 * Servicio utilizado para manipular las métricas
 * Consume los servicio web disponibles en MetricaResource del API REST
 */

@Injectable()
export class QuienQuiereSerMillonarioService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/juego-millonario`;
    //Representa los headers que se necesitan para la peticion
    public headers: Headers;
    public token: string;

    constructor(
        public JuegoMillonario: JuegoMillonario,
        public http: Http) {
        this.headers = new Headers();
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    /*
     * Encuentra los juegos que correspondan con los terminos de busqueda
     * Recibe:
     *        -busqueda: termino de busqueda
     *        -rowOffset: offset de row inicial para la paginacion
     *        -cantRegistros: cantidad de registros a traer del API
     *        -tipos-filtros
     * 
     * URL:/juego-millonario
     * Retorna: un observable que contiene los juegos
     */
    public buscarJuegos(
        busqueda: string[],
        rowOffset: number,
        cantRegistros: number,
        filtros?: string[],
        tipoOrden?: string,
        campoOrden?: string
    ) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);

        let query = this.actionUrl + "?busqueda=";
        if (busqueda != null) {
            query += busqueda.join(" ");
        }

        let uri = URI(query);

        if (filtros && filtros.length > 0) {
            filtros.forEach(element => {
                uri.addSearch("filtro", element);
            });
        }
        if (tipoOrden && tipoOrden.length > 0) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (campoOrden && campoOrden.length > 0) {
            uri.addSearch("campo-orden", campoOrden);
        }

        if (cantRegistros >= 0) {
            uri.addSearch("cantidad", cantRegistros);
        }
        if (rowOffset >= 0) {
            uri.addSearch("registro", rowOffset);
        }
        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /*
    * Guarda una instancia de Quien quiere ser millonario 
    * URL: post /juego-millonario
    * Recibe: una instancia con el Juego a almacenar
    * Retorna: un observable que proporciona información sobre la transacción
    */
    public addJuego(juegoMillonario: JuegoMillonario) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl, JSON.stringify(juegoMillonario), { headers: this.headers })
            .catch(this.handleError);
    }

    /*
    * Obtiene una instancia de Promocion 
    * URL: get /juego-millonario/{id_juego}
    * Recibe: una instancia con el Promocion a almacenar
    * Retorna: un observable que contiene el Promocion
    */
    public getJuego(id: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + id, { headers: this.headers }).map((response: Response) => <JuegoMillonario>response.json()).catch(this.handleError);
    }

    /*
   * Actualiza una instancia de Tema millonario 
   * URL: put /juego-millonario/
   * Recibe: una instancia con el Tema a actualizar
   * Retorna: un observable que proporciona el Tema, o el error generado
   */
  public updateJuego(juego: JuegoMillonario) {
    this.headers.set('Content-Type', 'application/json');
    this.headers.set('Accept', 'application/json');
    this.headers.set('Authorization', 'bearer ' + this.token);
    let JuegoJSON = JSON.stringify(juego);
    return this.http.put(this.actionUrl, JuegoJSON, { headers: this.headers })
        .catch(this.handleError);

}

    /**
     * Metodo encargado de manejar el error, retorna un error de observable 
     * con una representacion del error en JSON
     */
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}
