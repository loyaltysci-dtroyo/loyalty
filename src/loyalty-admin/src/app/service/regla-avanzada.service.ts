import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { ReglaAvanzada } from '../model/index';
import { AppConfig } from '../app.config';

@Injectable()
export class ReglaAvanzadaService {
    public headers:Headers;
    public token:string;
    public actionUrl: string = `${AppConfig.API_ENDPOINT}/segmento`;
    constructor(public _http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**
     * Metodo que obtiene la lista de reglas de este tipo en una lista
     * especifica
     *
     * Params: idLista Identificador de la lista de reglas
     * Retorna: Respuesta con una lista de todas las reglas encontradas
     * 
     * Path: GET /segmento/{idSegmento}/reglas/{idLista}/avanzada
     */
    public getReglasAvanzadasLista( idSegmento:string,idLista:string){
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/" + idSegmento + "/reglas/" + idLista + "/avanzada", { headers: this.headers })
            .map((response: Response) => <ReglaAvanzada[]>response.json())
            .catch(this.handleError);
    }

     /**
     * Metodo que crea una nueva regla en una lista especifica
     *
     * Params: idSegmento Identificador de segmento
     *         idLista Identificador de la lista de reglas
     *         reglaAvanzada Objeto con la informacion de la regla
     *         param usuario usuario en sesion
     * Retorna:Respuesta con el identificador de la regla creada
     * 
     * POST: /segmento/{idSegmento}/reglas/{idLista}/avanzada
     */
    public createReglaAvanzada( idSegmento:string,  idLista:string,  reglaAvanzada:ReglaAvanzada) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl + "/" + idSegmento + "/reglas/" + idLista + "/avanzada",JSON.stringify(reglaAvanzada), { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Metodo que elimina de una lista especifica una regla
     *
     *  Params: idSegmento Identificador de segmento
     *          idLista Identificador de la lista de reglas
     *          idRegla Identificador de la regla
     *  Retorna: Usuario usuario en sesión
     * 
     * DELETE:/segmento/{idSegmento}/reglas/{idLista}/avanzada/{idRegla}
     */
    public deleteReglaAvanzada( idSegmento:string,  idLista:string,  idRegla:string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.delete(this.actionUrl + "/" + idSegmento + "/reglas/" + idLista + "/avanzada/"+idRegla, { headers: this.headers })
            .catch(this.handleError);
    }
    
    /**
     * Encargado del manejo de errores, simplemente lanza una secuencia de Observable que terminara en error
     */
    public handleError(error:Response){
        return Observable.throw(error);
    }
}