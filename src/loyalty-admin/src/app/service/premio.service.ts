import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Premio, Miembro, Segmento, Ubicacion, PremioCertificado } from '../model/index';
import { AppConfig } from '../app.config';
declare var URI: any;

/**
 * Jaylin Centeno López 
 * Servicio utilizado para manejar los premios y elegibles
 * Consume los servicio web disponibles en PremioResource del API REST
 */
@Injectable()

export class PremioService {

    //Contiene la ruta en el que se encuentra el API REST
    actionUrl: string = `${AppConfig.API_ENDPOINT}/premio`;
    public headers: Headers;
    public token:string;

    constructor(
        public _http: Http, public premio: Premio
    ) {
        //Son los headers que necesita tener la petición
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* Método para agregar un nuevo premio 
    Recibe: el premio a insertar
    Retorna: retorna un json con el id del premio*/
    public insertarPremio(premio: Premio) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let PremioJSON = JSON.stringify(premio);
        return this._http.post(this.actionUrl, PremioJSON, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para actualizar un premio 
    Recibe: premio con los valores actualizados
    Retorna: respuesta de la transacción */
    public editarPremio(premio: Premio) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let PremioJSON = JSON.stringify(premio);
        return this._http.put(this.actionUrl, PremioJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para remover/archivar un premio 
    Recibe: id del premio
    Retorna: el resultado de la transacción*/
    public eliminarPremio(idPremio: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idPremio;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método que obtiene un premio específico 
    Recibe: el id del premio
    Retorna: el premio buscado*/
    public obtenerPremioPorId(idPremio: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idPremio;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Premio>response.json())
            .catch(this.handleError);
    }

    /*Método que obtiene todos los premio (por estado) 
    Recibe: estado del premio (Borrador/Publicado/Archivado)
    Retorna: una lista con todos los premios disponibles*/
    public obtenerListaPremios(estado: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url:any;
        if (cantidad == 0 && filaDesplazamiento == 0) {
            url = this.actionUrl + "?estado=" + estado;
        } else {
            url = this.actionUrl + "?estado=" + estado + '&cantidad=' + cantidad + '&registro=' + filaDesplazamiento;
        }
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método que obtiene todos los premio (por estado) 
    Recibe: estado del premio (Borrador/Publicado/Archivado)
    Retorna: una lista con todos los premios disponibles*/
    public obtenerPremiosUbicacionCentral(busqueda: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/ubicacion-central?busqueda="+busqueda;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para obtener lista de premios que coincidan con la búsqueda 
    Recibe: los criterios de búsqueda y el estado de los premios
    Retorna: lista con los premios que cumplen los criterios de búsqueda*/
    public busquedaPremios(cantidad: number, filaDesplazamiento: number, tipos?: string[], estado?: string[], palabrasClaves?: string, calendarizacion?: string[], envio?: string, respuesta?: string, categoria?: string, accion?: string, filtro?: string[], tipoOrden?: string, campoOrden?: string ) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if(estado && estado.length > 0){
            estado.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (tipos && tipos.length > 0) {
            tipos.forEach(element => {
                uri.addSearch("tipo", element);
            });
        }
        if (filtro && filtro.length > 0) {
            filtro.forEach(element => {
                uri.addSearch("filtro", element);
            });
        }
        if(palabrasClaves){
            uri.addSearch("busqueda", palabrasClaves)
        }
        if (calendarizacion && calendarizacion.length > 0) {
            calendarizacion.forEach(element => {
                uri.addSearch("calendarizacion", element);
            });
        }
        if(envio){
            uri.addSearch("envio", envio)
        }
        if(respuesta){
            uri.addSearch("respuesta", respuesta)
        }
        if(categoria){
            uri.addSearch("categoria", categoria)
        }
        if(accion){
            uri.addSearch("accion", accion)
        }
        if (tipoOrden) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (campoOrden) {
            uri.addSearch("campo-orden", campoOrden);
        }

        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para verificar si existe el nombre interno del premio
    Recibe: nombre interno
    Retorna: resultado de la transacción (true/false) */
    public verificarNombreInterno(nombreInterno: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url :any= this.actionUrl + "/exists/" + nombreInterno;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    /********************************************************************/
    /*------------------------Manejo de elegibles-----------------------*/

    /* Elegibles por Miembros*/

    /* Obtiene la lista de los miembros elegibles para el premio 
    Recibe: id del premio, cantidad de filas, fila de desplazamiento y la acción (I/E/D)
    Retorna la lista de los miembros elegibles según la acción*/
    obtenerMiembrosElegiblesPremio(idPremio: string, action: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url:any;
        if (cantidad == 0 && filaDesplazamiento == 0) {
            url = this.actionUrl + "/" + idPremio + "/miembro?accion=" + action;
        } else {
            url = this.actionUrl + "/" + idPremio + "/miembro?accion=" + action + '&cantidad=' + cantidad + '&registro=' + filaDesplazamiento;
        }
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para ingresar una lista de miembros, para incluir o excluir
    Recibe: id del premio, lista de ids, accion (I/E/D)
    Retorna: resultado de la transacción (true/false) */
    agregarListaMiembros(idPremio: string, idlistaMiembro: string[], accion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url :any= this.actionUrl + "/" + idPremio + "/miembro/?accion=" + accion;
        return this._http.post(url, idlistaMiembro, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para remover una lista de miembros de la lista de elgibles de un premio
    Recibe: la id del premio y una lista de ids de miembros a eliminar a la lista de elegibles
    Retorna: un objeto Response con el id de la ubicacion*/
    eliminarListaMiembros(idPremio: string, idlistaMiembro: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url :any= this.actionUrl + "/" + idPremio + "/miembro/remover-lista";
        return this._http.put(url, idlistaMiembro, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para agregar un miembro a la lista de elgibles de un premio
    Recibe: el miembro a agrgar a la lista de elegibles y la accion(I/E)
    Retorna: un objeto Response con el id del miembro*/
    agregarMiembro(idPremio: string, idMiembro: string, accion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url =this.actionUrl + "/" + idPremio + "/miembro/" + idMiembro + "?accion="+accion;
        return this._http.post(url, "", { headers: this.headers })
            .catch(this.handleError);
    }
    /* Método para elimina un miembro a la lista de elgibles de un premio
    Recibe: el miembro a excluir de la lista de elegibles
    Retorna: resultado de la transacción*/
    eliminarMiembro(idPremio: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url =this.actionUrl + "/" + idPremio + "/miembro/" + idMiembro;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /**************************************************************/

    /* Elegibles por segmentos */

    /* Obtiene la lista de los segmentos elegibles para el premio 
    Recibe: id del premio, cantidad de filas, fila de desplazamiento y la acción (I/E/D)
    Retorna la lista de los segmentos elegibles según la acción*/
    obtenerSegmentoElegiblesPremio(idPremio: string, action: string, cantidad: number, filaDesplazamiento: number, busqueda: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl+"/"+idPremio+"/segmento";
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (busqueda && busqueda.length > 0) {
            uri.addSearch("busqueda", busqueda)
        }
        if (action) {
            uri.addSearch("accion", action);
        }
        // .map((response: Response) => <Segmento>response.json())
        return this._http.get(uri.toString(), { headers: this.headers })
           .catch(this.handleError);
    }

    /* Método para ingresar una lista de segmentos, para incluir o excluir
    Recibe: id del premio, lista de id de segmentos, acción (I/E)
    Retorna: resultado de la transacción (true/false) */
    agregarListaSegmentos(idPremio: string, idlistaSegmento: string[], accion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url :any= this.actionUrl + "/" + idPremio + "/segmento/?accion=" + accion;
        return this._http.post(url, idlistaSegmento, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para remover una lista de ids de segmentos de la lista de elgibles de un premio
    Recibe: la id del premio y una lista de ids de segmentos a eliminar a la lista de elegibles
    Retorna: resultado de la trasacción*/
    eliminarListaSegmentos(idPremio: string, idlistaPremio: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url:any = this.actionUrl + "/" + idPremio + "/segmento/remover-lista";
        return this._http.put(url, idlistaPremio, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para agregar un segmento a la lista de elgibles de un premio
    Recibe: el segmento a agrgar a la lista de elegibles
    Retorna: resultado de la transacción*/
    agregarSegmento(idPremio: string, idSegmento: string, accion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url:any =this.actionUrl + "/" + idPremio + "/segmento/" + idSegmento + "?accion="+accion;
        return this._http.post(url, "", { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para eliminar un segmento a la lista de elgibles de un premio
    Recibe: el id del segmento a excluir de la lista de elegibles
    Retorna: resultado de la transacción*/
    eliminarSegmento(idPremio: string, idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url:any =this.actionUrl + "/" + idPremio + "/segmento/" + idSegmento;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /**************************************************************/

    /* Elegibles por ubicación */

    /* Obtiene la lista de las ubicaciones elegibles para el premio 
    Recibe: id del premio, cantidad de filas, fila de desplazamiento y la acción (I/E/D)
    Retorna la lista de los ubicaciones elegibles según la acción*/
    obtenerUbicacionElegiblesPremio(idPremio: string, action: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url:any;
        if (cantidad == 0 && filaDesplazamiento == 0) {
            url = this.actionUrl + "/" + idPremio + "/ubicacion?accion=" + action;
        } else {
            url = this.actionUrl + "/" + idPremio + "/ubicacion?accion=" + action + '&cantidad=' + cantidad + '&registro=' + filaDesplazamiento;
        }
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
            //.map((response: Response) => <Ubicacion>response.json())
    }

    /* Método para ingresar una lista de ubicaciones, para incluir o excluir
    Recibe: id del premio, lista de id de ubicaciones, accion (I/E)
    Retorna: resultado de la transacción (true/false) */
    agregarListaUbicaciones(idPremio: string, idlistaUbicaciones: string[], accion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url :any= this.actionUrl + "/" + idPremio + "/ubicacion/?accion=" + accion;
        return this._http.post(url, idlistaUbicaciones, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para remover una lista de ids de ubicaciones de la lista de elgibles de un premio
    Recibe: la id del premio y una lista de ids de ubicaciones a eliminar a la lista de elegibles
    Retorna: resultado de la trasacción*/
    eliminarListaUbicacion(idPremio: string, idlistaUbicaciones: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url:any = this.actionUrl + "/" + idPremio + "/ubicacion/remover-lista";
        return this._http.put(url, idlistaUbicaciones, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para agregar una ubucación a la lista de elgibles de un premio
    Recibe: el id de la ubicación a agrgar a la lista de elegibles
    Retorna: resultado de la transacción*/
    agregarUbicacion(idPremio: string, idUbicacion: string, accion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url:any =this.actionUrl + "/" + idPremio + "/ubicacion/" + idUbicacion + "?accion="+accion;
        return this._http.post(url, "", { headers: this.headers })
            .catch(this.handleError);
    }
    /* Método para eliminar un segmento a la lista de elgibles de un premio
    Recibe: el id del segmento a excluir de la lista de elegibles
    Retorna: resultado de la transacción*/
    eliminarUbicacion(idPremio: string, idUbicacion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url :any=this.actionUrl + "/" + idPremio + "/ubicacion/" + idUbicacion;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*----------------Manejo de certificado------------------*/

    /* Método para agregar un nuevo certificado de premio 
    Recibe: un certificado 
    Retorna: retorna un json con el id del certificado*/
    public insertarCertificado(idPremio: string, codigo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        //let CertificadoJSON = JSON.stringify(codigo);
        let url =this.actionUrl +"/"+idPremio+"/certificado";
        return this._http.post(url, codigo, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para generar un badge de certificados de premio 
    Recibe: cantidad a generar*/
    public generarCertificados(idPremio: string, cantidad: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url =this.actionUrl +"/"+idPremio+"/certificado/generar?cantidad="+ cantidad;
        return this._http.post(url, "", { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*Método para eliminar un certificado, cuando el estado del premio es BORRADOR
    Recibe: id del certificado
    Retorna: el resultado de la transacción*/
    public eliminarCertificado(idPremio: string, idCertificado: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url= this.actionUrl + "/" + idPremio+"/certificado/"+idCertificado;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*Método para anular un certificado, cuando el estado del premio es PUBLICADO
    Recibe: id del certificado
    Retorna: el resultado de la transacción*/
    public anularCertificado(idPremio: string, idCertificado: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url= this.actionUrl + "/" + idPremio+"/certificado/"+idCertificado+"/anular";
        return this._http.put(url,'', { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*Método que obtiene todos los certificados (por estado) 
    Recibe: estado del premio (Borrador/Publicado/Archivado)
    Retorna: una lista con todos los certificados disponibles*/
    public obtenerListaCertificados(idPremio: string, estado: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idPremio+"/certificado"
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if(estado != null && estado != "null"){
            uri.addSearch("estado", estado);
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /*Método para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}