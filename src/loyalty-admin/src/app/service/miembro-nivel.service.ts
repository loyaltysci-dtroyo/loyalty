import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Nivel, MiembroNivel } from '../model/index';
import { AppConfig } from '../app.config';

/**
 * Jaylin Centeno López 
 * Servicio utilizado para los niveles del miembro
 * Consume los servicio web disponibles en MiembroNivelResource del API REST
 */

@Injectable()
export class MiembroNivelService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/miembro`;
    public headers: Headers;
    public token: string;

    constructor(
        public nivel: Nivel,
        public _http: Http) {

        //Headers para realizar las peticiones
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* Método que obtiene los niveles asociados a un miembro
    Recibe: el id del miembro
    Retorna: lista de niveles 
    */
    public obtenerNivelesMiembro(idMiembro: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMiembro + "/metricas?cantidad="+cantidad+"&registro="+filaDesplazamiento;;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para asignar un nivel al miembro 
    Recibe: el id del miembro y del nivel a asociar
    Retorna: retorna el resultado de la transacción */
    public asignarNivelMiembro(idMiembro: string, idMetrica: string, idNivel: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMiembro + "/metrica/"+idMetrica+"/nivel/"+idNivel;
        let JsonNuevo = {"idMetrica":idMetrica, "idMiembro":idMiembro} ;
        return this._http.post(url, JsonNuevo, { headers: this.headers }).catch(this.handleError);
    }

    /*Método para desligar un nivel de un miembro 
    Recibe: el id del miembro y del nivel a asociar
    Retorna: el resultado de la transacción*/
    public desasignarNivelMiembro(idMiembro: string, idMetrica: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMiembro + "/metrica/" + idMetrica;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*Método para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}
