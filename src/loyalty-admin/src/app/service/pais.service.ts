import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Pais } from '../model/index';
import { AppConfig } from '../app.config';

/*Este servicio consume la API REST de UtilResource*/
/*---------------------------------------------------------------------------*/

@Injectable()

export class PaisService{

    //Contiene la ruta en el que se encuentra el API REST
    actionUrl: string = `${AppConfig.API_ENDPOINT}/util`;
    public headers: Headers;
    public token:string;
    constructor( public _http: Http, public pais: Pais) {
        //Son los headers que necesita tener la peticion
        this.headers = new Headers;
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

     /* Metodo que obtiene la lista con los países 
    Recibe: ningun valor
    Retorna: una lista con los países*/
    public obtenerPaises(){
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/paises", { headers: this.headers })
            .map((response: Response) => <Pais[]>response.json())
            .catch(this.handleError);
    }

    /* Metodo que obtiene el un país
    Recibe: ningun valor
    Retorna: Observable*/
    public obtenerPaisPorNombre(alfa3: string) :Observable<any>{
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/pais/"+alfa3, { headers: this.headers })
            .map((response: Response) => <Pais>response.json()).catch(this.handleError);
    }

     /*
    Metodo que permite controlar los errores 
    */
    public handleError(error: Response) {
        console.log(error.text());
        return Observable.throw(error)
        //return Observable.throw(error.json().error || 'Server error');
    }


}