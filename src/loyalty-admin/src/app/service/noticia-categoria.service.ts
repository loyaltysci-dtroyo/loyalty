import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { CategoriaNoticia, Noticia } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;

@Injectable()

/**
 * Jaylin Centeno López 
 * Servicio que permite manipular la entidad de categoria de noticia
 * Consume los servicio web disponibles en CategoriaNoticiaResource
 */

export class CategoriaNoticiaService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/categoria-noticia`;
    public token: string;
    public headers: Headers;

    constructor(
        public _http: Http,
        public categoria: CategoriaNoticia
    ) {
        //Headers para realizar las peticiones
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**
     * @GET
     * Obtiene el listado de las categorías disponibles en el sistema
     */
    public obtenerListaCategorias(cantidad: number, filaDesplazamiento: number, busqueda?: string, tipoOrden?: string, campoOrden?:string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (busqueda && busqueda.length > 0) {
            uri.addSearch("busqueda", busqueda);
        }
        if (tipoOrden) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (campoOrden) {
            uri.addSearch("campo-orden", campoOrden);
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /** 
     * @GET
     * Obtiene la categoría que corresponda al id que se pasa por parametros
     */
    public obtenerCategoriaId(id: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/" + id, { headers: this.headers }).catch(this.handleError);
    }

    /**
     * @POST
     * Agrega una nueva categoría
     */
    public insertarCategoria(categoria: CategoriaNoticia) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl, JSON.stringify(categoria), {headers: this.headers}).catch(this.handleError);
    }

    /**
     * @PUT
     * Modifica una entidad de categoría
     */
    public modificarCategoria(categoria: CategoriaNoticia) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.put(this.actionUrl, JSON.stringify(categoria), {headers: this.headers}).catch(this.handleError);
    }

    /** 
     * @DELETE
     * Remueve la categoría que corresponda al id que se pasa por parametros
     */
    public removerCategoriaId(id: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.delete(this.actionUrl + "/" + id, { headers: this.headers }).catch(this.handleError);
    }

    /**
     * @GET
     * Obtiene el listado de las noticias de una categoría  
     */
    public obtenerListaNoticiasPorCateg(id:string, indAccion: string, estado: string[], cantidad: number, filaDesplazamiento: number, filtro?: string[]) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl+'/'+id+'/noticia');
        if(indAccion){
            uri.addSearch("ind-accion-lista", indAccion);
        }
        if(estado && estado.length > 0){
            estado.forEach(element => uri.addSearch("estado", element));
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (filtro && filtro.length >= 0) {
            filtro.forEach(elemento => uri.addSearch("filtro", elemento));
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /**
     * @POST
     * Asignación de lista de noticias a categoría de noticia
     */
    public asignarCategoriaListaNoticias(id: string, lista: string[]) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl+"/"+id+"/noticia", lista, {headers: this.headers}).catch(this.handleError);
    }

    /**
     * @PUT
     * Elimina la asignación de lista de noticias a una categoría de noticia
     */
    public removerAsignacionListaNoticia(id: string, lista: string[]) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.put(this.actionUrl+"/"+id+"/noticia/remover-lista", lista, {headers: this.headers}).catch(this.handleError);
    }

    /**
     * @POST
     * Asignación de una noticia con una categoría de noticia
     */
    public asignarCategoria(idCategoria: string, idNoticia: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl+"/"+idCategoria+"/noticia/"+idNoticia, '', {headers: this.headers}).catch(this.handleError);
    }
    /**
     * @DELETE
     * Remover asignación de una categoría a noticia
     */
    public removerAsignacionCategoria(idCategoria: string, idNoticia: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.delete(this.actionUrl+"/"+idCategoria+"/noticia/"+idNoticia, {headers: this.headers}).catch(this.handleError);
    }

    /*Metodo para controlar los errores que se dan durante las peticiones al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}