import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { AtributoDinamicoProducto, Producto } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;
@Injectable()

/**
 * Jaylin Centeno López 
 * Se utiliza este servicio para manipular los atributos dinámicos de los productos
 * Consume los servicio web disponibles en AtributoDinamicoProductoResource del API REST
 */
export class AtributoDinamicoProductoService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/atributo-producto`;
    public token: string;
    public headers: Headers;

    constructor(
        public atributo: AtributoDinamicoProducto,
        public _http: Http
    ) {
        //Headers para realizar las peticiones/respuestas
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    /** @GET
     * Método para obtener un atributo de producto por id
     * Recibe: el id del atributo
     * Retorna: el atributo seleccionado
     */
    public obtenerAtributoId(idAtributo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idAtributo;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <AtributoDinamicoProducto>response.json())
            .catch(this.handleError);
    }
    /**
     * @GET
     * Método para obtener una lista de atributos dinámicos por el estado del atributo
     * Recibe: el estado, cantidad y fila
     * Retorna: los atributo dinámico del tipo de dato
     */
    public busquedaAtributosProducto(estado: string[], cantidad: number, filaDesplazamiento: number, tipoDato?: string[], busqueda?: string, requerido?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl;
        let uri = URI(url);
        if (estado && estado.length > 0) {
            estado.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        
        if (tipoDato && tipoDato.length > 0) {
            tipoDato.forEach(element => {
                uri.addSearch("tipo-dato", element);
            });
        }
        if(busqueda){
            uri.addSearch("busqueda", busqueda)
        }
        if(requerido){
            uri.addSearch("requerido", requerido)
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }
    /**
     * @POST
     * Método para insertar un atributo dinámico de producto
     * Recibe: el atributo a insertar
     * Retorna: el resultado de la transacción
     */
    public agregarAtributo(atributo: AtributoDinamicoProducto) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let AtributoJson = JSON.stringify(atributo);
        return this._http.post(this.actionUrl, AtributoJson, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }
    /**
     * @PUT
     * Método para actualizar un atributo dinámico 
     * Recibe: el atributo con la información actualizada
     * Retorna: el resultado de la transacción
     */
    public editarAtributo(atributo: AtributoDinamicoProducto) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let AtributoJSON = JSON.stringify(atributo);
        return this._http.put(this.actionUrl, AtributoJSON, { headers: this.headers })
            .catch(this.handleError);
    }
    /**
     * @DELETE
     * Metodo para remover un atributo dinámico 
     * Recibe: id del atributo dinámico
     * Retorna: resultado de la operación
     */
    public eliminarAtributo(idAtributo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idAtributo;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /********************************************************************/
    /* Servicio para asociar/remover un atributo dinámico a un producto */

    /* Método para asignar un atributo a un producto
    Recibe: el id del producto y del atributo
    Retorna: resultado de la transacción*/
    public asignarAtributoProducto(idProducto: string, idAtributo: string, valor: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idAtributo + "/producto/" + idProducto;
        //let valorJson = JSON.stringify(valor); 
        return this._http.post(url, valor, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para actualizar el valor del atributo del producto
    Recibe: el id de producto y del atributo
    Retorna: resultado de la transacción*/
    public actualizarAtributoProducto(idProducto: string, idAtributo: string, valor: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        //let valorJson = JSON.stringify(valor);
        let url = this.actionUrl + "/" + idAtributo + "/producto/" +idProducto;
        return this._http.put(url, valor, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para remover la relación entre un atributo y un producto
    Recibe: el id del atributo y del producto
    Retorna: resultado de la transacción*/
    public removerAsignacionAtributo(idProducto: string, idAtributo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idAtributo + "/producto/" + idProducto;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Metodo para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}