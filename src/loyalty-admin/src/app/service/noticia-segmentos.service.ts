import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { CategoriaNoticia, Noticia } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;

@Injectable()

/**
 * Jaylin Centeno López 
 * Servicio que permite manipular los segmentos utilizados en noticia
 * Consume los servicio web disponibles en NoticiaListaSegmentoResource
 */

export class NoticiaListaSegmentoService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/noticia`;
    public token: string;
    public headers: Headers;

    constructor(
        public _http: Http,
    ) {
        //Headers para realizar las peticiones
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**
     * @GET
     * Obtiene el listado de los segmentos de noticia
     * indAccion = incluido/excluido/disponibles
     * estados = estado de los segmentos AC = activo por defecto
     * tipos = tipo de segmento estático/dinámico
     */
    public obtenerListaSegmentosNoticia(idNoticia:string, indAccion:string, cantidad: number, filaDesplazamiento: number, busqueda?: string, tipos?:string[], filtros?: string[], tipoOrden?:string, campoOrden?:string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl+'/'+idNoticia+'/segmento');
        if (indAccion) {
            uri.addSearch("accion", indAccion);
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (busqueda && busqueda.length > 0) {
            uri.addSearch("busqueda", busqueda)
        }
        if (tipos && tipos.length > 0) {
            tipos.forEach(element => uri.addSearch("tipo", element));
        }
        if (filtros && filtros.length > 0) {
            filtros.forEach(element => uri.addSearch("filtro", element));
        }
        if (tipoOrden) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (campoOrden) {
            uri.addSearch("campo-orden", campoOrden);
        }
        uri.addSearch("estado", 'AC');
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /**
     * @POST
     * Asignación de lista de segmentos a noticia
     */
    public asignarListaSegmentosNoticia(id: string, lista: string[], accion:string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl+"/"+id+"/segmento?accion="+accion, lista, {headers: this.headers}).catch(this.handleError);
    }

    /**
     * @PUT
     * Elimina la asignación de lista de segmentos a una noticia
     */
    public removerAsignacionListaNoticia(id: string, lista: string[]) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.put(this.actionUrl+"/"+id+"/segmento/remover-lista", lista, {headers: this.headers}).catch(this.handleError);
    }

    /**
     * @POST
     * Asignación de un segmento a una noticia
     */
    public asignarSegmento(idSegmento: string, idNoticia: string, accion:string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl+"/"+idNoticia+"/segmento/"+idSegmento+"?accion="+accion, '', {headers: this.headers}).catch(this.handleError);
    }
    /**
     * @DELETE
     * Remover asignación de un segmento a una noticia
     */
    public removerAsignacioSegmento(idNoticia: string, idSegmento: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.delete(this.actionUrl+"/"+idNoticia+"/segmento/"+idSegmento, {headers: this.headers}).catch(this.handleError);
    }

    /*Metodo para controlar los errores que se dan durante las peticiones al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}