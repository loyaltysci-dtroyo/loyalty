import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Grupo } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;

/**
 * Jaylin Centeno López 
 * Servicio utilizado para manipular grupos del sistema
 * Consume los servicio web disponibles en GrupoResource del API REST
 */
@Injectable()
export class GrupoService{


    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/grupo`;
    //Representa los headers que se necesitan para la petición
    public headers: Headers;
    public token:string;

    constructor(
        public _http: Http 
    ){

        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /*Método para buscar un grupo por id
    Recibe: el id del grupo
    Retorna: el grupo buscado*/
    public obtenerGrupoPorId(idGrupo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupo;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Grupo>response.json())
            .catch(this.handleError);
    }

    /*Método para insertar un nuevo grupo
    Recibe: el grupo a insertar
    Retorna: el resultado de la transacción*/
    public insertarGrupo(grupo: Grupo) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let GrupoJson = JSON.stringify(grupo);
        return this._http.post(this.actionUrl, GrupoJson, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*Método para actualizar un grupo
    Recibe: el grupo a actualizar
    Retorna: el resultado de la transacción*/
    public actualizarGrupo(grupo: Grupo) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let GrupoJson = JSON.stringify(grupo);
        return this._http.put(this.actionUrl, GrupoJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para eliminar un grupo
    Recibe: el id del grupo a eliminar
    Retorna: el resultado de la transacción*/
    public removerGrupo(idGrupo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupo;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para cambiar la visibilidad del grupo
    Recibe: id del grupo, estado de visibilidad
    Retorna: el resultado de la transacción*/
    //V = visible I= invisible
    public visibilidadGrupo(idGrupo: string, visible: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/visible/" + idGrupo + "?visible=" + visible;
        return this._http.put(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para realizar búsquedas de grupos
    Recibe: palabras claves, indicador del grupo (visibilidad)
    Retorna: el resultado de la búsqueda */
    public buscarGrupo(cantidad: number, filaDesplazamiento: number,palabrasClaves?: string, indicador?: string[]) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl;
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if(palabrasClaves){
            uri.addSearch("busqueda", palabrasClaves)
        }
        if (indicador && indicador.length > 0) {
            indicador.forEach(element => {
                uri.addSearch("visibilidad", element);
            });
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    //Método que permite controlar los errores de las transacciones
    public handleError(error: Response) {
        return Observable.throw(error);
    }

}
