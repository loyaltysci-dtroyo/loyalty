import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Rol, Usuario } from '../model/index';
import { AppConfig } from '../app.config';
declare var URI: any;
/**
 * Flecha Roja Technologies
 * Autor:Fernnado Aguilar 
 * Service encargado de realiar mantenimiento de datos y busquedas de roles
 */
@Injectable()
export class RolService {
    actionUrl: string = `${AppConfig.API_ENDPOINT}/rol`;
    public headers: Headers;
    public token: string;
    constructor(public Rol: Rol, public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**
     * GET
     * Retorna la lista de roles  presentes en el API
     * Path: rol/
     * Retorna:Rol[]
     */
    public getListaRoles() {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl, { headers: this.headers }).map((response: Response) => <Rol[]>response.json()).catch(this.handleError);
    }
    /**
    * GET
    * Realiza una consulta para obtener los roles disponibles para asociar a un miembro determinado
    * Path: disponibles/{idUsuario}
    * Retorna:Rol[]
    */
    getRolesDisponiblesPorUsuario(idUsuario: string, busqueda: string[], rowOffset: number, rowsPerPage: number) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let terminoBusqueda = "";
        let url = this.actionUrl + '/disponibles/' + idUsuario + '/?cantidad=' + rowsPerPage + "&registro=" + rowOffset;
        let uri = URI(url);
        if (busqueda.length > 0) {
            busqueda.forEach(termino => {
                 uri.addSearch("busqueda", termino.trim());
                //terminoBusqueda += " " + termino.trim();
            });
        }
        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);

    }
    /**
    * GET
    * Realiza una busqueda entre los roles presentes en el API
    * Path: rol/buscar/{nombreRol}
    * Retorna:Rol[]
    */
    public busquedaRoles(nombreRol: string, filaDesplazamiento: number, cantidad: number) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl;
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if(nombreRol.length > 0){
            uri.addSearch("busqueda", nombreRol);
        }
        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }
    /*
    *GET
    *Path: rol/{idrol}
    *Retorna el rol a partir del id 
    *Retorna:Rol
    */
    public getRolPorNombre(id: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + id, { headers: this.headers }).map((response: Response) => <Rol>response.json()).catch(this.handleError);
    }
    /*
    *GET
    *Path: usuarios-rol/{nombreRol}
    *Obtiene una lista de usuarios asociados a un rol
    *Retorna:Usuario[]
    */
    public getUsuariosPorRol(nombreRol: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/usuarios-rol/";
        if(nombreRol.length > 0){
            url+=nombreRol;
        }
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /*
     *POST
     *Path: rol/
     *Ingresa un nuevo rol a traves de API
     *Retorna:response con informacion sobre,a transaccion
     */
    public insertRol(Rol: Rol) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl, JSON.stringify(Rol), { headers: this.headers }).catch(this.handleError);
    }
    /*
    *PUT
    *Path: rol/
    *Ingresa un nuevo rol a traves de API
    *Retorna:response con informacion sobre,a transaccion
    */
    public editRol(Rol: Rol) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl, JSON.stringify(Rol), { headers: this.headers }).catch(this.handleError);
    }
    /*
    *DELETE
    *Path: rol/{idRol}
    *Elimina un rol a traves de API
    *Retorna:response con informacion sobre,a transaccion
    */
    public deleteRol(rol: Rol) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + rol.nombre, { headers: this.headers }).catch(this.handleError);
    }
    /*
    *Se encarga de manejar errores, y luego lo lanza hacia la capa de presentacion
    *para darle tratamiento
    */
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}