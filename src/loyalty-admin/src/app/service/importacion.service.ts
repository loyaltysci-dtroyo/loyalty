import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';


/**
 * Jaylin Centeno López 
 * Servicio utilizado para manejar las importaciones de miembros, miembros a segmentos y atributos dinámicos de miembro
 * Consume los servicio web disponibles en ImportResource del API REST
 */
declare var URI: any;
@Injectable()
export class ImportacionService {

    actionUrl: string = `${AppConfig.API_ENDPOINT}/importar`; //Ruta en el que se encuentra el servicio web
    public headers: Headers; //Headers que se necesitan para la petición
    public token: string;

    constructor(
        public _http: Http
    ) {
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**
     * Importación de miembros a un segmento estático
     * Recibe: id segmento, atributo llave (doc. id o id. miembro), accion(incluir, excluir ) lista miembros base64
     * Retorna: respuesta de la transacción
     */
     public importarMiembrosSegmento(idSegmento: string, clave: string, accion: string, lista: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl+"/miembro/segmento/"+idSegmento);
        if (clave) {
            uri.addSearch("atributo-clave", clave);
        }
        if (accion) {
            uri.addSearch("accion-miembro", accion);
        }
        return this._http.post(uri.toString(), lista, { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Importacion de miembros
     * Recibe: lista miembros base64
     * Retorna: respuesta de la transacción
     */
     public importarMiembros(lista: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl+"/miembro", lista, { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Importacion de informacion de atributos para actualizar en miembros
     * Recibe: lista miembros base64
     * Retorna: respuesta de la transacción
     */
     public importarInfoMiembros(identificacion: string, atributo: string, tipo: string,lista: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl+"/miembro/actualizar");
        if (identificacion) {
            uri.addSearch("identificacion", identificacion);
        }
        if (atributo) {
            uri.addSearch("atributo", atributo);
        }
        if (tipo) {
            uri.addSearch("tipo-atributo", tipo);
        }
        return this._http.put(uri.toString(), lista, { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Importacion de códigos de certificados para un premio
     * Recibe: lista códigos base64
     * Retorna: respuesta de la transacción
     */
    public importarCertificados(premio: string, lista: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl+"/certificados");
        if (premio) {
            uri.addSearch("premio", premio);
        }
        return this._http.post(uri.toString(), lista, { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Importacion de numCertificado de certificados para una categoria producto
     * Recibe: lista numCertificado base64
     * Retorna: respuesta de la transacción
     */
    public importarCertificadosCategoriaProducto(categoria: string, lista: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl+"/certificadosCategoriaProducto");
        if (categoria) {
            uri.addSearch("categoria", categoria);
        }
        return this._http.post(uri.toString(), lista, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}