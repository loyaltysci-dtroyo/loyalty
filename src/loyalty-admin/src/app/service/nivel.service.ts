import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Nivel, GrupoNivel } from '../model/index';
import { AppConfig } from '../app.config';
declare var URI: any;
/**
 * Jaylin Centeno López 
 * Servicio utilizado para los niveles y grupos de niveles
 * Consume los servicio web disponibles en NivelMetricaResource del API REST
 */

@Injectable()

export class NivelService {

    //Contiene la ruta en el que se encuentra el API REST
    actionUrl: string = `${AppConfig.API_ENDPOINT}/grupo-nivel`;
    public headers: Headers;
    public token:string;

    constructor(
        public _http: Http, public Nivel: Nivel
    ) {
        //Son los headers que necesita tener la petición
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**************************************************************/
    /*---------------------------Grupos niveles--------------------------*/

    /*Método que obtiene la información de un grupo de niveles 
    Recibe: id del grupo
    Retorna: el grupo buscado*/
    public obtenertGrupoNivelPorId(idGrupoNivel: string) {
        //this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupoNivel;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <GrupoNivel>response.json())
            .catch(this.handleError);
    }

    /*Método que obtiene todos los grupos nivel disponibles
    Recibe: ningún valor
    Retorna:lista de grupos de nivel*/
    public obtenertListaGrupoNivel() {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl, { headers: this.headers })
            .map((response: Response) => <GrupoNivel[]>response.json())
            .catch(this.handleError);
    }

    /*Método para realizar búsquedas de grupos
    Recibe: palabras claves (" ")
    Retorna: el resultado de la búsqueda */
    public buscarGrupoNivel(palabrasClaves: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/buscar/";
        if(palabrasClaves != ""){
            url += palabrasClaves;
        }
         let uri = URI(url);
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para agregar un grupo de niveles 
    Recibe: el nombre del grupo
    Retorna: id del nivel*/
    public agregarGrupoNivel(nombreGrupo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        //let GrupoNivelJSON = JSON.stringify(nombreGrupo);
        return this._http.post(this.actionUrl,nombreGrupo , { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*Método para actualizar un grupo de niveles 
    Recibe: el grupo a actualizar
    Retorna: resultado de la transaccion*/
    public editarGrupo(grupoNivel: GrupoNivel) {
        //this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let GrupoNivelJSON = JSON.stringify(grupoNivel);
        return this._http.put(this.actionUrl, GrupoNivelJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para verificar si existe el nombre del grupo
    Recibe: id del grupo
    Retorna: resultado de la transacción (true/false) */
    public verificarNombre(nombreGrupo: string) {
        //this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/exists/" + nombreGrupo;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <boolean>response.json())
            .catch(this.handleError);
    }

    /* Método para eliminar un grupo de niveles
    Recibe: el id del grupo
    Retorna: el resultado de la trasacción */
    public removerGrupoNivel(idGrupo: string) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupo;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /**************************************************************/
    /*---------------------------Niveles--------------------------*/

    /*Método que obtiene todos todos los niveles asociados a un grupo
    Recibe: id del grupo
    Retorna: un lista con todos los niveles*/
    public obtenerListaNiveles(idGrupo: string) {
        //this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupo + "/niveles";
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Nivel[]>response.json())
            .catch(this.handleError);
    }

    /*Método que obtiene la información de un nivel 
    Recibe: id del nivel
    Retorna: el nivel buscado*/
    public obtenertNivelPorId(idGrupo: string, idNivel: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupo + "/niveles/" + idNivel;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Nivel>response.json())
            .catch(this.handleError);
    }
    /*Método que obtiene la información de un nivel 
    Recibe: id del nivel
    Retorna: el nivel buscado*/
    public obtenertNivel(idNivel: string) :Observable<any> {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/niveles/" + idNivel;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Nivel>response.json());
    }

    /*Método para agregar un nivel 
    Recibe: el nivel a asociar
    Retorna: id del nivel*/
    public agregarNivel(idGrupo: string, Nivel: Nivel) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let NivelJSON = JSON.stringify(Nivel);
        let url = this.actionUrl + "/" + idGrupo + "/niveles";
        return this._http.post(url, NivelJSON, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*Método para actualizar un nivel 
    Recibe: el nivel a actualizar, el id del grupo
    Retorna: resultado de la transaccion*/
    public editarNivel(idGrupo: string, Nivel: Nivel) {
        //this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let NivelJSON = JSON.stringify(Nivel);
        let url = this.actionUrl + "/" + idGrupo + "/niveles";
        return this._http.put(url, NivelJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Elimina el nivel 
    Recibe: el id del nivel a eliminar 
    Retorna: resultado de la transaccion*/
    public removerNivel(idGrupo: string, idNivel: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupo + "/niveles/" + idNivel;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /**************************************************************/
    /*---------------------------Niveles Métrica--------------------------*/

    /* Método que obtiene todas las métricas por nivel 
    Recibe: el id del nivel
    Retorna: lista de las metricas asociadas*/

    public obtenerMetricasNivel(idNivel: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idNivel + "/metrica";
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Nivel[]>response.json())
            .catch(this.handleError);
    }

    /*Método para asignar un nivel a metricas 
    Recibe: el id del nivel y una lista de metricas
    Retorna: lista de las metricas asociadas*/

    public asignarNivelMetricas(idNivel: string, listaMetrica: string[]) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idNivel + "/metrica";
        let ListaJson = JSON.stringify(listaMetrica);
        return this._http.post(url, ListaJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para desasignar métricas de un nivel
    Recibe: el id del nivel y una lista de metricas
    Retorna: lista de las metricas asociadas*/

    public removerNivelMetricas(idNivel: string, listaMetrica: string[]) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idNivel + "/metrica/remover-lista";
        let ListaJson = JSON.stringify(listaMetrica);
        return this._http.put(url, ListaJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para asignar una metrica a un nivel 
    Recibe: el id de nivel y de metricas
    Retorna: respuesta de la transacción*/
    public asignarNivelMetrica(idNivel: string, idMetrica: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idNivel + "/metrica/" + idMetrica;
        return this._http.post(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para desasignar una metrica de un nivel
    Recibe: el id de nivel y de metricas
    Retorna: respuesta de la transacción*/

    public removerNivelMetrica(idNivel: string, idMetrica: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idNivel + "/metrica/" + idMetrica;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método que permite controlar los errores durante las peticiones*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}