import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Leaderboard } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;

@Injectable()
/**
 * Flecha Roja Technologies 21-oct-2016
 * Fernando Aguilar
 * Service encargado de insteractuar con el API RESTful
 */
export class LeaderboardService {
    //TODO: recordar actualizar el url desde la clase de configuracion
    public actionUrl: string = `${AppConfig.API_ENDPOINT}/tablaposiciones`;
    public headers: Headers;
    public token:string;

    constructor(public Leaderboard: Leaderboard, public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    /**Encargado de traer las leaderboards del API */
    public busqueda(terminos: string, leaderboardsBusqueda: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/buscar/";
        let uri = URI(url);
        if (leaderboardsBusqueda && leaderboardsBusqueda.length > 0) {
            leaderboardsBusqueda.forEach(element => {
                uri.addSearch("busqueda", element);
            });
        }
        if (terminos) {
            uri.addSearch("tipo", terminos);
        }
        return this.http.get(uri.toString(), { headers: this.headers })
            .map((response: Response) => <Leaderboard[]>response.json())
            .catch(this.handleError);
    }

    /**
     * Retorna la lista de Leaderboards del API 
     * */
    public getListaLeaderboards() {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl, { headers: this.headers })
            .map((response: Response) => <Leaderboard[]>response.json()).catch(this.handleError);
    }

    public getLeaderboard(id: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + id, { headers: this.headers })
            .map((response: Response) => <Leaderboard>response.json()).catch(this.handleError);
    }
    /*
    * Guarda una instancia de Leaderboard 
    * URL: post /Leaderboard/
    * Retorna: un observable que contiene los Leaderboards
    */
    public addLeaderboard(Leaderboard: Leaderboard) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl, JSON.stringify(Leaderboard), { headers: this.headers }).catch(this.handleError);
    }
    /*
   * Actualiza una instancia de Leaderboard 
   * URL: put /Leaderboard/
   * Recibe: una instancia con el Leaderboard a actualizar
   * Retorna: un observable que proporciona el Leaderboard, o el error generado
   */
    public updateLeaderboard(Leaderboard: Leaderboard) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl, JSON.stringify(Leaderboard), { headers: this.headers })
            .catch(this.handleError);
    }
    /*
     * Elimina una instancia de Leaderboard 
     * URL: delete /Leaderboard/{id_Leaderboard}
     * Recibe: una instancia con el Leaderboard a Eliminar
     * Retorna: un observable que proporciona informacion sobre la transacción, o el error generado
     */
    public deleteLeaderboard(Leaderboard: Leaderboard) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + Leaderboard.idTabla, { headers: this.headers })
            .catch(this.handleError);
    }

    public handleError(error: Response) {
        return Observable.throw(error);
    }
}