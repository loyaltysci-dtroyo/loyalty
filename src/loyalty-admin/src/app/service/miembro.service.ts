import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Miembro, InsigniaNivel, Insignia } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';

/**
 * Jaylin Centeno López 
 * Servicio utilizado para manejar la información de los miembros y la relación con otros elementos del sistema
 * Consume los servicio web disponibles en MiembroResource del API REST
 */
declare var URI: any;
@Injectable()
export class MiembroService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/miembro`;
    public token: string;
    public headers: Headers;

    constructor(
        public Miembro: Miembro,
        public _http: Http) {

        //Headers para realizar las peticiones
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* Método que obtiene todos los miembros de la aplicación (activos/inactivos) 
    Recibe: ningún elemento
    Retorna: una lista con los miembros*/
    public obtenerTodosLosMiembros() {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Miembro[]>response.json())
            .catch(this.handleError);
    }

    /* Método para obtener lista de miembros que coincidan con la búsqueda 
    Recibe: los criterios de búsqueda y el estado de los miembros
    Retorna: lista con los miembros que cumplen los criterios de búsqueda*/
    public busquedaMiembros(estado: string[], rowOffset: number, cantRegistros: number, busqueda?: string, filtros?: string[], generos?: string[], estadosCiviles?: string[], educaciones?: string[], indEmail?: boolean, indSMS?: boolean, indNotificacion?: boolean, indHijos?: boolean, tipoOrden?: string, campoOrden?: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let query = this.actionUrl + "?busqueda=" + busqueda;
        let uri = URI(query);
        if (estado && estado.length > 0) {
            estado.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (generos && generos.length > 0) {
            generos.forEach(element => {
                uri.addSearch("genero", element);
            });
        }
        if (filtros && filtros.length > 0) {
            filtros.forEach(element => {
                uri.addSearch("filtro", element);
            });
        }
        if (estadosCiviles && estadosCiviles.length > 0) {
            estadosCiviles.forEach(element => {
                uri.addSearch("estado-civil", element);
            });
        }
        if (educaciones && educaciones.length > 0) {
            educaciones.forEach(element => {
                uri.addSearch("educacion", element);
            });
        }

        if (indEmail === true) {
            uri.addSearch("contacto-email", "TRUE");
        } else if (indEmail === false) {
            uri.addSearch("contacto-email", "FALSE");
        }
        if (indSMS === true) {
            uri.addSearch("contacto-sms", "TRUE");
        } else if (indSMS === false) {
            uri.addSearch("contacto-sms", "FALSE");
        }
        if (indNotificacion === true) {
            uri.addSearch("contacto-notificacion", "TRUE");
        } else if (indNotificacion === false) {
            uri.addSearch("contacto-notificacion", "FALSE");
        }
        if (indHijos === true) {
            uri.addSearch("tiene-hijos", "TRUE");
        } else if (indHijos === false) {
            uri.addSearch("tiene-hijos", "FALSE");
        }
        if (campoOrden && campoOrden.length > 0) {
            uri.addSearch("campo-orden", campoOrden);
        }

        if (cantRegistros >= 0) {
            uri.addSearch("cantidad", cantRegistros);
        }
        if (rowOffset >= 0) {
            uri.addSearch("registro", rowOffset);
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /* Método que obtiene un miembro específico 
    Recibe: estado del miembro y el id del miembro
    Retorna: el miembro buscado*/
    public obtenerMiembroPorId(idMiembro: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMiembro;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Miembro>response.json())
            .catch(this.handleError);
    }

    /* Método para agregar un nuevo miembro 
    Recibe: el miembro a insertar
    Retorna: retorna un json con el id del miembro*/
    public insertarMiembro(Miembro: Miembro) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let MiembroJSON = JSON.stringify(Miembro);
        return this._http.post(this.actionUrl, MiembroJSON, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para actualizar un miembro 
    Recibe: miembro con los valores actualizados
    Retorna: respuesta de la transacción */
    public editarMiembro(Miembro: Miembro) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let MiembroJSON = JSON.stringify(Miembro);
        return this._http.put(this.actionUrl, MiembroJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Método para suspender a un miembro
     * Recibe: id del miembro y la causa de suspención
     * Retorna: la respuesta de la transacción 
     */
    public suspenderMiembro(idMiembro: string, suspension: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMiembro + "/suspender";
        return this._http.put(url, suspension, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*Método para suspender un miembro 
    Recibe: id del miembro, la causa de suspencion
    Retorna: el resultado de la transacción*/
    public eliminarMiembro(idMiembro: string) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMiembro + "/suspender";
        return this._http.delete(url).catch(this.handleError);
    }

    /*Método para activar un miembro 
    Recibe: id del miembro
    Retorna: el resultado de la transacción*/
    public activarMiembro(idMiembro: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMiembro + "/activar";
        return this._http.put(url, '', { headers: this.headers }).catch(this.handleError);
    }


    /* Método para verificar si existe el nombre de usuario
    Recibe: nombre usuario
    Retorna: resultado de la transacción (true/false) */
    public existenciaNombreUsuario(nombreUsuario: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/comprobar-nombre/" + nombreUsuario;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <boolean>response.json())
            .catch(this.handleError);
    }

    /* Método para verificar si existe el correo de cliente
    Recibe: correo
    Retorna: resultado de la transacción (true/false) */
    public existenciaCorreo(correo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/comprobar-email/" + correo;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <boolean>response.json())
            .catch(this.handleError);
    }

    /*******************Segmentos********************/

    /*
    * Método que permite obtener la lista de los segmentos en los que el 
    * miembro es elegible.
    * Recibe el id del miembro, cantidad y resultados
    * Retorna la lista de los segmentos
    */
    public obtenerSegmentosMiembro(idMiembro: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMiembro + "/segmento";
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*************** Atributos *************/

    /* Método para obtener los atributos asociados a un miembro
    Recibe: el id del miembro, la cantidad de filas y la fila de desplazamiento
    Retorna: la lista de los atributos que tiene asociado*/
    obtenerAtributosPorMiembro(idMiembro: string, cantidad: number, filaDesplazamiento: number, estados: string[], tipoDatos?: string[], busqueda?: string, requerido?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let query = this.actionUrl + "/" + idMiembro + "/atributos";
        let uri = URI(query);

        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (busqueda) {
            uri.addSearch("busqueda", busqueda)
        }
        if (estados && estados.length > 0) {
            estados.forEach(element => {
                uri.addSearch("estado", estados);
            });
        }
        if (tipoDatos && tipoDatos.length > 0) {
            tipoDatos.forEach(element => {
                uri.addSearch("tipo-dato", tipoDatos);
            });
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /**************  Insignias  ***********/

    /*Método para obtener las insignias que están y no están relacionadas con el miembro
    Recibe: id del miembro, cantidad, fila de desplazamiento, estado, tipo, tipo de insignia y busqueda
    Retorna: la lista de insignias del miembro //insignias collector*/
    public obtenerInsigniasMiembro(idMiembro: string, cantidad: number, filaDesplazamiento: number, estados: string[], tipo: string, tipoInsignia: string[], busqueda?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let query = this.actionUrl + "/" + idMiembro + "/insignias";
        let uri = URI(query);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (estados && estados.length > 0) {
            estados.forEach(element => {
                uri.addSearch("estado", estados);
            });
        }
        if (tipo) {
            uri.addSearch("tipo", tipo);
        }
        if (busqueda) {
            uri.addSearch("busqueda", busqueda)
        }
        if (tipoInsignia && tipoInsignia.length > 0) {
            tipoInsignia.forEach(element => {
                uri.addSearch("tipo-insignia", tipoInsignia);
            });
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para obtener los grupos en los que participa un miembro
    Recibe: el id del miembro
    Retorna: la lista de los grupos a los que pertenece*/
    public obtenerGruposPorMiembro(idMiembro: string, cantidad: number, filaDesplazamiento: number, tipo: string, busqueda?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMiembro + "/grupos";
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (tipo) {
            uri.addSearch("tipo", tipo);
        }
        if (busqueda) {
            uri.addSearch("busqueda", busqueda)
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /********Actividad metrica*******/
    public obtenerActividadMetrica(idMiembro: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + '/' + idMiembro + '/actividad/metrica', { headers: this.headers })
            .catch(this.handleError);
    }

    /*********actividad promociones ********/
    public obtenerActividadPromociones(idMiembro: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + '/' + idMiembro + '/actividad/promocion', { headers: this.headers })
            .catch(this.handleError);
    }
    /************** Premios ************/
    public obtenerActividadPremios(idMiembro: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + '/' + idMiembro + '/actividad/premios-recompensas', { headers: this.headers })
            .catch(this.handleError);
    }
    /************* Misiones ***********/
    public obtenerActividadMisiones(idMiembro: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + '/' + idMiembro + '/actividad/mision', { headers: this.headers }).catch(this.handleError);
    }
    /********** Notificaciones *********/
    public obtenerActividadNotificaciones(idMiembro: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + '/' + idMiembro + '/actividad/notificacion', { headers: this.headers }).catch(this.handleError);
    }

    /********* Balances de métrica ************/
    /**
     * Obtiene el balance de todas las métricas del miembro 
     * Recibe: id del miembro
     * Retorna: lista de las metricas
     */
    public obtenerBalancesMiembro(idMiembro: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + '/' + idMiembro + '/balance', { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Obtiene el balance de métricas del miembro 
     * Recibe: id del miembro y id de la metrica
     * Retorna: Balance
     */
    public obtenerBalanceMiembroMetrica(idMiembro: string, idMetrica: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + '/' + idMiembro + '/balance/' + idMetrica, { headers: this.headers }).catch(this.handleError);
    }

    /********* Referals ***********/
    /**
     * Obtiene los miembros que han sido referidos
     * Recibe: id del miembro
     * Retorna: lista con los miembros
     */
    public obtenerReferals(idMiembro: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + '/' + idMiembro + '/referenciados';
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /************* Transacciones ***********/
    /**
     * Obtiene las transacciones realizadas por el miembro
     * Recibe: id del miembro
     * Retorna: lista con las transacciones del miembro
     */
    public obtenerTransacciones(idMiembro: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + '/' + idMiembro + '/transacciones';
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /************ Cantidad de miembros ***********/
    public obtenerEstadisticasMiembrosEstado() {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + '/cantidad-estado';
        return this._http.get(url, { headers: this.headers }).catch(this.handleError);
    }

    public obtenerCantidadMiembrosNuevos() {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + '/cantidad-nuevos';
        return this._http.get(url, { headers: this.headers }).map((response: Response) => response.text()).catch(this.handleError);
    }

    /*Método para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}
