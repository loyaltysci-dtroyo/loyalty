import { AppConfig } from '../app.config';
import { RegistroActividad } from '../model/registro-actividad';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { LabelsPT } from "../../locale/labels-pt";
import { LabelsEN } from "../../locale/labels-en";
import { LabelsES } from "../../locale/labels-es";



@Injectable()
export class I18nService {
    // private url = AppConfig.LOCAL_DOMAIN + "/locale/labels-" + AppConfig.LOCALE + ".json";
    public labels: any
    public labelspt:LabelsPT;
    public labelses:LabelsPT;
    public labelsen:LabelsPT;

    constructor(public http: Http) {
        this.labelsen = new LabelsEN();
        this.labelses = new LabelsES();
        this.labelspt = new LabelsPT();
        
    }
    public initialize() {
        
    }   
    public getLabels(key: string) {
        switch(AppConfig.LOCALE){
            case 'es':{
                return this.labelses.data[key];
            }
            case 'en':{
                return this.labelsen.data[key];
            }
            case 'pt':{
                return this.labelspt.data[key];
            }   
        }
            
    }
}