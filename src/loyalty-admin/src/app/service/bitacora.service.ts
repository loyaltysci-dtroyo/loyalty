import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;

/**
 * Jaylin Centeno López 
 * Se utiliza este servicio para obtener información de las bitacoras del sistema
 * Consume los servicio web disponibles en BitacoraResource del API REST
 */

@Injectable()
export class BitacoraService{

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/bitacora`;
    //Representa los headers que se necesitan para la petición
    public headers: Headers;
    public token:string;

    constructor(
        public _http: Http 
    ){
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**
     * Método para obtener los registros de bitacora del api administrativo 
     * Parametros: acción, cantidad de filas, fila desplazamiento 
     * Retorna: resultado de la transacción
     */
    public obtenerBitacoraAdmin(accion: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + '/admin';
        let uri = URI(url);
        if (accion != null) {
            uri.addSearch("accion", accion);
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Método para obtener los registros de bitacora del api external 
     * Parametros: ninguno
     * Retorna: resultado de la transacción
     */
    public obtenerBitacoraExternal(periodo:string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/external"+"?periodo="+periodo, { headers: this.headers }).catch(this.handleError);
    }

    //Método que permite controlar los errores de las transacciones
    public handleError(error: Response) {
        return Observable.throw(error);
    }

}
