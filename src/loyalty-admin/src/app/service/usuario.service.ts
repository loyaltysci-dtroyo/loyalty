import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Usuario, Rol } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;
/**
 * Flecha Roja Technologies
 * Service encargado de realiar mantenimientos de datos de Usuarios
 */
@Injectable()
export class UsuarioService {
    public actionUrl: string = `${AppConfig.API_ENDPOINT}/usuario`;
    public headers: Headers;
    public token: string;

    constructor(public usuario: Usuario, public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    /**
     * Realiza una busqueda de los usuarios en el sistema
     * Parametros: terminos - terminos de busqueda 
     *             inactivos y activos- flags que refinan la busqueda por estados
     *             cantRegistros - Cantidad de registros a incluir en la pagina
     *             rowOffset - offset (# row actual) de la pagina
     */
    public busqueda(terminos: string[], estado: string[], cantRegistros: number, rowOffset: number, filtros?: string[]) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl);
        if (cantRegistros >= 0) {
            uri.addSearch("cantidad", cantRegistros);
        }
        if (rowOffset >= 0) {
            uri.addSearch("registro", rowOffset);
        }
        if (terminos && terminos.length > 0) {
            terminos.forEach(element => {
                uri.addSearch("busqueda", element.trim());
            });
        }
        if (estado && estado.length > 0) {
            estado.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (filtros && filtros.length > 0) {
            filtros.forEach(element => {
                uri.addSearch("filtro", element);
            });
        }
        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }
    /**
     * Obtiene la lista de usuarios en el sistema
     * Parametros: cantRegistros - Cantidad de registros a incluir en la pagina
     *             rowOffset - offset (# row actual) de la pagina
     */
    /*public getListaUsuarios(cantRegistros: number, rowOffset: number) {
        let url = this.actionUrl + "";
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        if (cantRegistros >= 0) { url += "?cantidad=" + cantRegistros; }
        if (rowOffset >= 0) { url += "&registro=" + rowOffset; }
        return this.http.get(url, { headers: this.headers }).catch(this.handleError);
    }*/
    /**
     * Obtiene un usuario a partir de su id
     * Parametros: id - el uuid el usuario 
     */
    public getUsuario(id: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + id, { headers: this.headers }).map((response: Response) => <Usuario>response.json()).catch(this.handleError);
    }
    /*
    * Guarda una instancia de usuario 
    * URL: post /usuario/
    * Retorna: un observable que contiene los Usuarios
    */
    public addUsuario(usuario: Usuario) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl, JSON.stringify(usuario), { headers: this.headers }).map((response: Response) => response.text()).catch(this.handleError);
    }
    /**
     * Determina si el nombre de usuario que recibe mantiene la unicidad en el sistema
     * Parametros: nombreUsuario - el nombre de usuario a consultar
     */

    public validarNombre(nombreUsuario: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/exists/" + nombreUsuario, { headers: this.headers }).map((response: Response) => <string>response.text()).catch(this.handleError);
    }

    /*
   * Actualiza una instancia de Usuario 
   * URL: put /usuario/
   * Recibe: una instancia con el Usuario a actualizar
   * Retorna: un observable que proporciona el Usuario, o el error generado
   */
    public updateUsuario(usuario: Usuario) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl, JSON.stringify(usuario), { headers: this.headers }).catch(this.handleError);
    }
    
    /*
   * Cambia de estado activo a inactivo a un Usuario 
   * URL: put /usuario/eliminar/:id
   * Recibe: el id del usuario a inactivar
   * Retorna: un observable que proporciona el resultado
   */
    public inactivarUsuario(idUsuario: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl+"/eliminar/"+idUsuario,'', { headers: this.headers }).catch(this.handleError);
    }

    /*
   * Cambia de estado inactivo a activo a un Usuario 
   * URL: put /usuario/activar/:id
   * Recibe: el id del usuario para activar
   * Retorna: un observable que proporciona el resultado
   */
    public activarUsuario(idUsuario: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl+"/activar/"+idUsuario,'', { headers: this.headers }).catch(this.handleError);
    }

    /*
     * Elimina una instancia de Usuario 
     * URL: delete /usuario/{id_Usuario}
     * Recibe: una instancia con el Usuario a Eliminar
     * Retorna: un observable que proporciona informacion sobre la transacción, o el error generado
     */
    public deleteUsuario(usuario: Usuario) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/eliminar/" + usuario.idUsuario, { headers: this.headers }).catch(this.handleError);
    }

    /* -----------   ROLES  -----------*/
    /**
     * Revoca el rol del usuario
     * DELETE /usuario/{idUsuario}/rol/{nombreRol}
     */
    public removerRol(usuario: Usuario, rol: Rol) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + usuario.idUsuario + "/remover-roles", { headers: this.headers, body: "[" + JSON.stringify(rol) + "]" }).catch(this.handleError);
    }


    /**
     * Agrega un rol a un usuario
     * PUT /usuario/{idUsuario}/rol/{nombreRol}
    */
    public agregarRol(usuario: Usuario, rol: Rol) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + usuario.idUsuario + "/roles/", "[" + JSON.stringify(rol) + "]", { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Remueve una lista de roles para el usuario
     * PUT /usuario/{idUsuario}/remover-roles
     */
    public removerListaRol(usuario: Usuario, roles: Rol[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let ids = roles.map((element) => { return element.nombre });
        return this.http.put(this.actionUrl + "/" + usuario.idUsuario + "/remover-roles", ids, { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Obtiene la lista de roles para el usuario
     * GET /usuario/{idUsuario}/roles
     */
    public getListaRolesAsociados(usuario: Usuario, rowOffset: number, rowsPerPage: number) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + usuario.idUsuario + "/roles?cantidad=" + rowsPerPage + "&registro=" + rowOffset, { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Asigna una lista de roles al usuario
     * PUT /usuario/{idUsuario}/roles
     */
    public agregarListaRol(usuario: Usuario, roles: Rol[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let ids = roles.map((element) => { return element.nombre });
        return this.http.put(this.actionUrl + "/" + usuario.idUsuario + "/roles", ids, { headers: this.headers }).catch(this.handleError);
    }

    public obtenerPerfilUsuario() {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/perfil", { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Maneja los errores, simplemente lanza el error (agregar cualquier manejo adicional de errores a nivel local aqui)
     */
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}