import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { HistoricoMovimientos } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';

/**
 * Jaylin Centeno López 
 * Servicio utilizado para manipular los historicos de movimientos
 * Consume los servicio web disponibles en HistoricoResource del API REST
 */
declare var URI: any;
@Injectable()
export class HistoricoService{

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/historico`;
    //Representa los headers que se necesitan para la petición
    public headers: Headers;
    public token:string;

    constructor(
        public _http: Http 
    ){
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* Método para actualizar un movimiento de tipo redención
    Recibe: movimiento a editar
    Retorna: respuesta de la transacción */
    public editarHistorico(movimiento: HistoricoMovimientos) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let MovimientoJSON = JSON.stringify(movimiento);
        return this._http.put(this.actionUrl, MovimientoJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Lista de movimientos historicos 
    Recibe: los criterios de búsqueda 
    Retorna: lista con los movimientos*/
    public busquedaHistoricos(cantRegistros: number, filaDesplazamiento: number, tipoMovimiento: string, estado?: string[], miembro?: string, premio?: string, ubicacion?: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl);
        if (estado.length > 0) {
            estado.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (cantRegistros >= 0) {
            uri.addSearch("cantidad", cantRegistros);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (tipoMovimiento) {
            uri.addSearch("movimiento", tipoMovimiento);
        }
        if (miembro) {
            uri.addSearch("miembro", miembro);
        }
        if (premio) {
            uri.addSearch("premio", premio);
        }
        if (ubicacion) {
            uri.addSearch("ubicacion", ubicacion);
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /*Método para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}