import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Proveedor } from '../model/index';
import { AppConfig } from '../app.config';

declare var URI: any;

@Injectable()

/**
 * Jaylin Centeno López 
 * Servicio que permite la manipulación de los proveedores
 * Consume los servicio web disponibles en ProveedorResource del API REST
 */
export class ProveedorService {

    //Contiene la ruta en el que se encuentra el API REST
    actionUrl: string = `${AppConfig.API_ENDPOINT}/proveedor`;
    public headers: Headers;
    public token: string;

    constructor(
        public _http: Http, public Proveedor: Proveedor
    ) {
        //Son los headers que necesita tener la petición
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /*Método que obtiene la información de un grupo de niveles 
    Recibe: id del grupo
    Retorna: el grupo buscado*/
    public obtenertProveedorPorId(idProveedor: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idProveedor;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método que obtiene todos los grupos nivel disponibles
    Recibe: ningún valor
    Retorna:lista de grupos de nivel*/
    public obtenerProveedores(busqueda: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let query = this.actionUrl + "?busqueda=" + busqueda;
        let uri = URI(query);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /*
    *Método para agregar un proveedor
    *Recibe: proveedor
    *Retorna: id del proveedor
    */
    public agregarProveedor(proveedor: Proveedor) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let proveedorJson = JSON.stringify(proveedor);
        return this._http.post(this.actionUrl, proveedorJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para actualizar un proveedor
    Recibe: el proveedor a actualizar
    Retorna: resultado de la transaccion*/
    public editarProveedor(proveedor: Proveedor) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let proveedorJson = JSON.stringify(proveedor);
        return this._http.put(this.actionUrl, proveedorJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para eliminar un proveedor
    Recibe: el id del grupo
    Retorna: el resultado de la trasacción */
    public eliminarProveedor(idProveedor: string) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idProveedor;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método que permite controlar los errores durante las peticiones*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}