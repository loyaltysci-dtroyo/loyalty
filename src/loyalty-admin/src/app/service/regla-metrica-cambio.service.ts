import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { ReglaMetricaCambio, ListaReglas } from '../model/index';
import { AppConfig } from '../app.config';
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Service encargado de realizar mantenimiento de datos de reglas de cambio 
 * de metrica (usuario gana, redime o expira alguna metrica)
 */
@Injectable()
export class ReglaMetricaCambioService {

    actionUrl: string = `${AppConfig.API_ENDPOINT}/segmento`;
    public headers: Headers; public token: string;

    constructor(public _http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /*Retorna todas las listas de listas de reglas asociadas al segmento proporcionado
    * URL: get /segmento/{id_segmento}/reglas
    * Recibe: el id del segmento
    * Retorna: un observable con las ocurrencias de regla de este tipo para ese segmento
    */
    public getListasReglas(idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/" + idSegmento + "/reglas", { headers: this.headers })
            .map((response: Response) => <ListaReglas[]>response.json())
            .catch(this.handleError);
    }
    /*
    * Retorna una lista  de reglas de metrica-balance asociadas al segmento proporcionado según su identificador
    * URL:get /segmento/{id_segmento}/reglas/{id_lista}/metrica-cambio/
    * Recibe: el id del segmento y el id de la lista de reglas
    * Retorna: un observable con las ocurrencias de regla de este tipo para ese segmento salvo errores
    */
    public getReglaMetricaCambioList(idSegmento: string, idLista: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/" + idSegmento + "/reglas/" + idLista + "/metrica-cambio", { headers: this.headers })
            .map((response: Response) => <ReglaMetricaCambio[]>response.json())
            .catch(this.handleError);
    }

    /*
    * Almacena una instancia de regla asociada al segmento y el id de lista de reglas proporcionado 
    * URL:post /segmento/{id_segmento}/reglas/{id_lista}/metrica-cambio/
    * Recibe: el id del segmento, id de lista de reglas y la instancia de reglaMetricaCambio a insertar
    * Retorna: un observable con datos sobre la transacción
    */
    public addReglaMetricaCambio(idSegmento: string, idListaReglas: string, reglaMetricaCambio: ReglaMetricaCambio) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let reglaJSON = JSON.stringify(reglaMetricaCambio);
        return this._http.post(this.actionUrl + "/" + idSegmento + "/reglas/" + idListaReglas + "/metrica-cambio", reglaJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /*
    * Retorna una instancia de regla asociada al segmento, el id de lista y el id de regla roporcionados 
    * URL:get /segmento/{id_segmento}/reglas/{id_lista}/metrica-cambio/{id_regla}
    * Recibe: el id del segmento, id de lista de reglas y el id de regla a traer
    * Retorna: un observable que proporciona la métrica, o el error generado
    */
    public getReglaMetricaCambio(idSegmento: string, idLista: string, idReglaMetricaCambio: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/" + idSegmento + "/reglas/" + idLista + "/metrica-cambio/" + idReglaMetricaCambio, { headers: this.headers })
            .map((response: Response) => <ReglaMetricaCambio>response.json())
            .catch(this.handleError);
    }


    /*
    * Elimina una instancia de regla asociada al segmento, el id de lista y el id de regla roporcionados 
    * URL:delete /segmento/{id_segmento}/reglas/{id_lista}/metrica-cambio/{id_regla}
    * Recibe: el id del segmento, id de lista de reglas y el id de regla a traer
    * Retorna: un observable que proporciona la métrica, o el error generado
    */
    public deleteReglaMetricaCambio(idSegmento: string, idListaReglas: string, idReglaMetricaCambio: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.delete(this.actionUrl + "/" + idSegmento + "/reglas/" + idListaReglas + "/metrica-cambio/" + idReglaMetricaCambio, { headers: this.headers })
            .catch(this.handleError);
    }

    /*
    * Encargado de manejar los errores 
    * Recibe: un response con el error que ocurrió
    */
    public handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}