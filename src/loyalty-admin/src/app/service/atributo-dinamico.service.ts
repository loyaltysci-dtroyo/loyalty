import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { AtributoDinamico } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;

@Injectable()

/**
 * Jaylin Centeno López 
 * Se utiliza este servicio para manipular los atributos dinámicos
 * Consume los servicio web disponibles en AtributoDinamicoResource del API REST
 */
export class AtributoDinamicoService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/atributo`;
    public token:string;
    public headers: Headers;

    constructor(
        public atributo: AtributoDinamico,
        public _http: Http
    ) {
        //Headers para realizar las peticiones/respuestas
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /*Método para obtener un atributo dinámico por id
    Recibe: el id del atributo
    Retorna: el atributo dinámico seleccionado*/
    public obtenerAtributoId(idAtributo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idAtributo;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <AtributoDinamico>response.json())
            .catch(this.handleError);
    }
   
    /**
     * Método para obtener una lista de atributos dinámicos que coincidan con la búsqueda 
     * Retorna la lista con los atributos que cumplen los criterios de búsqueda
     * Params: cantidad de filas, fila desplazamiento, estado, busqueda,
     *  tipo: F - fecha
              T - fecha
              N - fecha
              B - fecha
    */
    public busquedaAtributosDinamicos(cantidad: number, filaDesplazamiento: number, estado:string[],tipo?: string[], busqueda?: string, visible?: string, requerido?: string, filtro?: string[], tipoOrden?: string, campoOrden?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl;
        let uri = URI(url);

        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (estado && estado.length > 0) {
            estado.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (tipo && tipo.length > 0) {
            tipo.forEach(element => {
                uri.addSearch("tipo-dato", element);
            });
        }
        if (filtro && filtro.length > 0) {
            filtro.forEach(element => {
                uri.addSearch("filtro", element);
            });
        }
        if(busqueda){
            uri.addSearch("busqueda", busqueda)
        }
        if(visible){
            uri.addSearch("visible", visible);
        }
        if(requerido){
            uri.addSearch("requerido", requerido)
        }
        if(tipoOrden){
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if(campoOrden){
            uri.addSearch("campo-orden", campoOrden);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para insertar un atributo dinámico
    Recibe: el atributo a insertar
    Retorna: el resultado de la transacción*/
    public agregarAtributo(atributo: AtributoDinamico) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let AtributoJson = JSON.stringify(atributo);
        return this._http.post(this.actionUrl, AtributoJson, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para actualizar un atributo dinámico 
    Recibe: el atributo con la información actualizada
    Retorna: el resultado de la transacción */
    public editarAtributo(atributo: AtributoDinamico) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let AtributoJSON = JSON.stringify(atributo);
        return this._http.put(this.actionUrl, AtributoJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Metodo para remover un atributo dinámico 
    Recibe: id del atributo dinámico
    Retorna: resultado de la operación */
    public eliminarAtributo(idAtributo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idAtributo;
        let idJson = JSON.stringify(idAtributo);
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Metodo para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}