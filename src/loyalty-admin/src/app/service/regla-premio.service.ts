import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { ReglaAvanzada, ReglaPremioCompra } from '../model/index';
import { AppConfig } from '../app.config';

@Injectable()
export class ReglaPremioService {
    public headers: Headers;
    public token: string;
    public actionUrl: string = `${AppConfig.API_ENDPOINT}/reglas-premio`;
    constructor(public _http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    public getListaReglasPremio(): Observable<any> {
        this.headers.set('Authorization','Bearer: '+this.token);
        return this._http.get(this.actionUrl, {headers:this.headers});
    }

    public agregarReglaPremio(reglaPremio:ReglaPremioCompra): Observable<any> {
        this.headers.set('Authorization','Bearer: '+this.token);
        return this._http.post(this.actionUrl, JSON.stringify(reglaPremio), {headers:this.headers});
    }

    public removerReglaPremio(idRegla:string): Observable<any> {
        this.headers.set('Authorization','Bearer: '+this.token);
        return this._http.delete(this.actionUrl+'/'+idRegla, {headers:this.headers});
    }

    public editarReglaPremio(reglaPremio:ReglaPremioCompra): Observable<any> {
        this.headers.set('Authorization','Bearer: '+this.token);
        return this._http.put(this.actionUrl+'/'+JSON.stringify(reglaPremio), {headers:this.headers});
    }
}