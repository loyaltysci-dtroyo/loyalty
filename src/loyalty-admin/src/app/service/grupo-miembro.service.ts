import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Grupo, Miembro } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';

declare var URI: any;

@Injectable()

/**
 * Jaylin Centeno López 
 * Servicio utilizado para manipular los miembros de un grupo
 * Consume los servicio web disponibles en GrupoMiembroResource del API REST
 */
export class GrupoMiembroService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/grupo`;
    //Representa los headers que se necesitan para la petición
    public headers: Headers;
    public token: string;

    constructor(
        public _http: Http
    ) {
        //Headers para realizar las peticiones
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* Método para obtener los miembros de un grupo
    Recibe: el id del grupo
    Retorna: la lista de los miembros del grupo */
    public obtenerMiembrosGrupo(idGrupo: string, cantidad: number, filaDesplazamiento: number, estado: string, busqueda: string, tipo?:string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupo + "/miembros";
        let uri = URI(url);
        if (busqueda) {
            uri.addSearch("busqueda", busqueda)
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (tipo) {
            uri.addSearch("tipo", tipo)
        }
        uri.addSearch("estado", estado);

        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para obtener la cantidad de miembros de un grupo
    Recibe: el id del grupo
    Retorna: la cantidad de miembros*/
    public cantidadMiembros(idGrupo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupo + "/cantidad-miembros";
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para agregar una lista de miembros a un grupo
    Recibe: la lista de miembros (contiene solo el id del miembro)
    Retorna: resultado de la transacción*/
    public agregarListaMiembro(listaMiembro: string[], idGrupo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupo;
        let GrupoMiembroJson = JSON.stringify(listaMiembro);
        return this._http.post(url, GrupoMiembroJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para agregar un miembro a un grupo
    Recibe: el id de grupo y de miembro
    Retorna: resultado de la transacción*/
    public agregarMiembro(idMiembro: string, idGrupo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupo + "/miembro/" + idMiembro;
        let GrupoMiembroJson = JSON.stringify(idMiembro);
        return this._http.post(url, GrupoMiembroJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para remover miembros de un grupo
    Recibe: la lista de miembros a remover
    Retorna: resultado de la transacción*/
    public removerListaMiembro(listaMiembro: string[], idGrupo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupo + "/remover-miembros";
        let GrupoMiembroJson = JSON.stringify(listaMiembro);
        return this._http.put(url, GrupoMiembroJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para remover un miembro de un grupo
    Recibe: el id del grupo y del miembro
    Retorna: resultado de la transacción*/
    public removerMiembro(idMiembro: string, idGrupo: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idGrupo + "/miembro/" + idMiembro;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }

}
