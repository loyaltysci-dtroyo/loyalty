import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Preferencia } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;

@Injectable()
/**
 * Flecha Roja Technologies 21-oct-2016
 * Fernando Aguilar
 * Service encargado de insteractuar con el API RESTful
 */
export class PreferenciaService {
    //TODO: recordar actualizar el url desde la clase de configuracion
    public actionUrl: string = `${AppConfig.API_ENDPOINT}/preferencia`;
    public headers: Headers;
    public token:string;

    constructor(public Preferencia: Preferencia, public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    /**Encargado de traer las preferencias del API c */
    public busqueda(terminos: string, preferenciasBusqueda: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/buscar/";
        let uri = URI(url);
         if(terminos){
            uri.addSearch("busqueda", terminos)
        }
        if (preferenciasBusqueda && preferenciasBusqueda.length > 0) {
            preferenciasBusqueda.forEach(element => {
                uri.addSearch("respuesta", element);
            });
        }
        return this.http.get(uri.toString(), { headers: this.headers })
            .map((response: Response) => <Preferencia[]>response.json())
            .catch(this.handleError);
    }

    /**
     * Retorna la lista de Preferencias del API 
     * */
    public getListaPreferencias() {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl, { headers: this.headers })
            .map((response: Response) => <Preferencia[]>response.json())
            .catch(this.handleError);
    }

    public getPreferencia(id: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + id, { headers: this.headers })
            .map((response: Response) => <Preferencia>response.json())
            .catch(this.handleError);
    }
    /*
    * Guarda una instancia de Preferencia 
    * URL: post /Preferencia/
    * Retorna: un observable que contiene los Preferencias
    */
    public addPreferencia(Preferencia: Preferencia) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let PreferenciaJSON = JSON.stringify(Preferencia);

        return this.http.post(this.actionUrl, PreferenciaJSON, { headers: this.headers })
            .catch(this.handleError);
    }


    /*
   * Actualiza una instancia de Preferencia 
   * URL: put /Preferencia/
   * Recibe: una instancia con el Preferencia a actualizar
   * Retorna: un observable que proporciona el Preferencia, o el error generado
   */
    public updatePreferencia(Preferencia: Preferencia) {
        let usuarioJSON = JSON.stringify(Preferencia);
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl, usuarioJSON, { headers: this.headers })
            .catch(this.handleError);
    }
    /*
     * Elimina una instancia de Preferencia 
     * URL: delete /Preferencia/{id_Preferencia}
     * Recibe: una instancia con el Preferencia a Eliminar
     * Retorna: un observable que proporciona informacion sobre la transacción, o el error generado
     */
    public deletePreferencia(Preferencia: Preferencia) {
        this.headers = new Headers();
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + Preferencia.idPreferencia, { headers: this.headers })
            .catch(this.handleError);
    }

    public handleError(error: Response) {
        return Observable.throw(error);
    }
}