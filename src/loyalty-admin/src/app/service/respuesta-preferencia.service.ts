import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Miembro, Preferencia, RespuestaPreferencia } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
/**
 * Autor: Jaylin Centeno López 
 */

//Servicio consume la API REST de Preferencias para administrar las respuestas del miembro*/
declare var URI: any;

@Injectable()
export class RespuestaPreferenciaService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/preferencia`;
    public token: string;
    public headers: Headers;

    constructor(
        public miembro: Miembro,
        public preferencia: Preferencia,
        public _http: Http) {

        //Headers para realizar las peticiones
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**
     * Obtiene las respuestas del miembro 
     * Recibe: id del miembro
     * Retorna: respuesta de la transacción
     */
    public busquedaRespuestasMiembro(idMiembro: string, cantidad: number, filaDesplazamiento: number, tipo?: string[],busqueda?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/miembro/" + idMiembro;
        let uri = URI(url);

        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (tipo && tipo.length > 0) {
            tipo.forEach(element => {
                uri.addSearch("tipo-respuesta", element);
            });
        }
        if(busqueda){
            uri.addSearch("busqueda", busqueda)
        }
        return this._http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Insertar la respuesta de un miembro  
     * Recibe: id de la preferencia, id del miembro y la respuesta
     * Retorna: respuesta de la transacción
     */
    public insertarRespuestaPreferencia(idPreferencia: string, idMiembro: string, respuesta: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idPreferencia + "/miembro/" + idMiembro;
        let RespuestaJson = JSON.stringify(respuesta);
        return this._http.post(url, respuesta, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);

    }
    /**
     * Remueve la respuesta de un miembro
     * Recibe: id de la preferencia y el id del miembro
     * Retorna: respuesta de la transacción
     */
    public removerRespuesta(idPreferencia: string, idMiembro: string) {
        //this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idPreferencia + "/miembro/" + idMiembro;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Actualiza las respuestas de un miembro 
     * Recibe: id de la preferencia, id del miembro y la respuesta
     * Retorna: respuesta de la transacción
     */
    public actualizarRespuesta(idPreferencia: string, idMiembro: string, respuesta: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idPreferencia + "/miembro/" + idMiembro;
        let RespuestaJson = JSON.stringify(respuesta);
        return this._http.put(url, respuesta, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /**
     * Manejo de errores
     */
    public handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}