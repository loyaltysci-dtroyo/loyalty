import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Metrica, Nivel } from '../model/index';
import { AppConfig } from '../app.config';
declare var URI: any;

/**
 * Jaylin Centeno López 
 * Servicio utilizado para manipular las métricas
 * Consume los servicio web disponibles en MetricaResource del API REST
 */

@Injectable()
export class MetricaService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/metrica`;
    //Representa los headers que se necesitan para la peticion
    public headers: Headers;
    public token: string;

    constructor(
        public Metrica: Metrica,
        public _http: Http) {
        this.headers = new Headers();
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* Método que obtiene todas las métricas segun el estado
    Recibe: el estado de las métricas
    Retorna: la lista de métricas publicadas */
    public getListaMetrica(estado: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let estados = estado.split('');
        let params = "";
        estados.forEach(element => {
            if(params == ""){
                params += "?estado="+element;
            }else{
                params += "&&estado="+element;
            }
        });
        let url = this.actionUrl + params;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Metrica[]>response.json())
            .catch(this.handleError);
    }

    /* Método que obtiene todas las métricas disponibles 
    Recibe: ningun elemento
    Retorna: una lista con todas las métricas disponibles*/
    public getListaMetricas() {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl, { headers: this.headers })
            .map((response: Response) => <Metrica[]>response.json())
            .catch(this.handleError);
    }

    /* Método que obtiene una métrica específica mediante un id
    Recibe: id de la métrica
    Retorna: métrica especifica */
    public getMetrica(id: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + id;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Metrica>response.json())
            .catch(this.handleError);
    }

    /* Método para obtener lista de miembros que coincidan con la búsqueda 
    Recibe: los criterios de búsqueda y el estado de los miembros
    Retorna: lista con los miembros que cumplen los criterios de búsqueda */
    public buscarMetricas(estado: string[],cantidad?: number, filaDesplazamiento?: number, palabrasClaves?: string, expiracion?: string,  filtro?: string[], tipoOrden?: string, campoOrden?: string ) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl;
        let uri = URI(url);
        if(estado && estado.length > 0){
            estado.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if(palabrasClaves){
            uri.addSearch("busqueda", palabrasClaves)
        }
        if (filtro && filtro.length > 0) {
            filtro.forEach(element => {
                uri.addSearch("filtro", element);
            });
        }
        if (tipoOrden) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (expiracion) {
            uri.addSearch("expiracion", expiracion);
        }
        if (campoOrden) {
            uri.addSearch("campo-orden", campoOrden);
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }

        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para verificar si existe el nombre interno de la métrica
    Recibe: id de la métrica
    Retorna: resultado de la transacción (true/false) */
    public verificarNombreInterno(nombreInterno: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/exists/" + nombreInterno;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <boolean>response.json())
            .catch(this.handleError);
    }

    /* Método que agrega una nueva métrica 
    Recibe: métrica
    Retorna: retorna el id de la métrica (text plain) */
    public addMetrica(Metrica: Metrica) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let MetricaJSON = JSON.stringify(Metrica);
        return this._http.post(this.actionUrl, MetricaJSON, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para actualizar una métrica 
    Recibe: métrica con valores actualizados
    Retorna: id de la métrica */
    public updateMetrica(Metrica: Metrica) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let MetricaJSON = JSON.stringify(Metrica);
        return this._http.put(this.actionUrl, MetricaJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para eliminar una métrica 
    Recibe: el id de la métrica a eliminar
    Retorna: resultado de la transacción*/
    public deleteMetrica(idMetrica: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMetrica;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método que permite controlar los errores durante las peticiones*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}
