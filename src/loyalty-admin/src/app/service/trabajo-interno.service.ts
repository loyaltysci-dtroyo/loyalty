import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';

/**
 * Jaylin Centeno López 
 * Servicio que permite obtener los trabajos internos en el sistema
 * Consume los servicio web disponibles en TrabajoInternoResource del API REST
 */
declare var URI: any;
@Injectable()
export class TrabajoInternoService {

    actionUrl: string = `${AppConfig.API_ENDPOINT}/trabajo`; //Ruta en el que se encuentra el servicio web
    public headers: Headers; //Headers que se necesitan para la petición
    public token: string;

    constructor(
        public _http: Http
    ) {
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**@GET
     * Busqueda de trabajos internos
     * Recibe: tipo de trabajo, estados, tipo orden, campo orden, cantidad y registro
     * Retorna: respuesta de la transacción
     */
    public obtenerTrabajos(tipo: string[], estado: string[], cantidad: number, filaDesplazamiento: number, tipoOrden?: string, campoOrden?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl);
        if (tipo && tipo.length > 0) {
            tipo.forEach(element => {
                uri.addSearch("tipo", element);
            });
        }
        if (estado && estado.length > 0) {
            estado.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (tipoOrden) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (campoOrden) {
            uri.addSearch("campo-orden", campoOrden);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}