import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { PreguntaMillonario, JuegoMillonario } from '../model/index';
import { AppConfig } from '../app.config';
declare var URI: any;

/**
 * Jaylin Centeno López 
 * Servicio utilizado para manipular las métricas
 * Consume los servicio web disponibles en MetricaResource del API REST
 */

@Injectable()
export class PreguntaQuienQuiereSerMillonarioService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/pregunta-millonario`;
    //Representa los headers que se necesitan para la peticion
    public headers: Headers;
    public token: string;

    constructor(
        public PreguntaMillonario: PreguntaMillonario,
        public http: Http) {
        this.headers = new Headers();
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    /*
     * Encuentra los juegos que correspondan con los terminos de busqueda
     * Recibe:
     *        -rowOffset: offset de row inicial para la paginacion
     *        -cantRegistros: cantidad de registros a traer del API
     * 
     * URL:/pregunta-millonario
     * Retorna: un observable que contiene los juegos
     */
    public buscarPreguntas(
        juegoMillonario: JuegoMillonario
    ) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        const url = this.actionUrl + '/' + juegoMillonario.idJuego;
        return this.http.get(url, { headers: this.headers }).catch(this.handleError);
    }

    /*
    * Guarda una instancia de Quien quiere ser millonario 
    * URL: post /pregunta-millonario
    * Recibe: una instancia con el Juego a almacenar
    * Retorna: un observable que proporciona información sobre la transacción
    */
    public addPregunta(preguntaMillonario: PreguntaMillonario) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl, JSON.stringify(preguntaMillonario), { headers: this.headers })
            .catch(this.handleError);
    }

    /*
    * Obtiene una instancia de Promocion 
    * URL: get /pregunta-millonario/{id_pregunta}
    * Recibe: una instancia con el Promocion a almacenar
    * Retorna: un observable que contiene el Promocion
    */
    public getPregunta(id: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/pregunta/" + id, { headers: this.headers }).map((response: Response) => <PreguntaMillonario>response.json()).catch(this.handleError);
    }

    /*
   * Actualiza una instancia de Tema millonario 
   * URL: put /pregunta-millonario/
   * Recibe: una instancia con el Tema a actualizar
   * Retorna: un observable que proporciona el Tema, o el error generado
   */
  public updatePregunta(preguntaMillonario: PreguntaMillonario) {
    this.headers.set('Content-Type', 'application/json');
    this.headers.set('Accept', 'application/json');
    this.headers.set('Authorization', 'bearer ' + this.token);
    let preguntaJSON = JSON.stringify(preguntaMillonario);
    return this.http.put(this.actionUrl, preguntaJSON, { headers: this.headers })
        .catch(this.handleError);
}

/*
   * Actualiza una instancia de Tema millonario 
   * URL: put /pregunta-millonario/
   * Recibe: una instancia con el Tema a actualizar
   * Retorna: un observable que proporciona el Tema, o el error generado
   */
  public deletePregunta(preguntaMillonario: PreguntaMillonario) {
    this.headers.set('Content-Type', 'application/json');
    this.headers.set('Accept', 'application/json');
    this.headers.set('Authorization', 'bearer ' + this.token);

    return this.http.delete(this.actionUrl + '/' + preguntaMillonario.idPregunta, { headers: this.headers })
        .catch(this.handleError);
}

    /**
     * Metodo encargado de manejar el error, retorna un error de observable 
     * con una representacion del error en JSON
     */
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}
