import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { AppConfig } from '../app.config';

/*Este servicio consume la API REST de UtilResource*/
/*---------------------------------------------------------------------------*/

@Injectable()

export class TransaccionesService {

    //Contiene la ruta en el que se encuentra el API REST
    actionUrl: string = `${AppConfig.API_ENDPOINT}/util`;
    public headers: Headers;
    public token: string;

    constructor(public _http: Http) {
        //Son los headers que necesita tener la peticion
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    obtenerListadoTransacciones(idMiembro: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl, { headers: this.headers })
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }
     /*Metodo para controlar los errores que se dan durante las peticiones 
    al web service */
    public handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}