import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { UnidadMedida } from '../model/index';
import { AppConfig } from '../app.config';

@Injectable()

/**
 * Jaylin Centeno
 * Este servicio consume del API REST UnidadesMedidaResource
 * Mantenimiento de Unidades de medida utilizadas en los premios
 */
export class UnidadMedidaService {

    //Contiene la ruta en el que se encuentra el API REST
    actionUrl: string = `${AppConfig.API_ENDPOINT}/unidades-medida`;
    public headers: Headers;
    public token: string;

    constructor(
        public _http: Http
    ) {
        //Son los headers que necesita tener la petición
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* @POST
    * Método para agregar una nuevo unidad de medida para premio 
    * Recibe: la unidad de medida a insertar
    * Retorna: el id de la unidad de medida
    */
    public insertarUnidad(unidad: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl, unidad, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*@PUT
    * Método para actualizar las unidades de medida 
    * Recibe: unidad con los valores actualizados
    * Retorna: respuesta de la transacción 
    */
    public editarUnidad(unidad: UnidadMedida) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let UnidadJSON = JSON.stringify(unidad);
        return this._http.put(this.actionUrl, UnidadJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /*@DELETE
    * Método para remover una unidad
    * Recibe: id de la unidad
    * Retorna: el resultado de la transacción
    */
    public eliminarUnidad(idUnidad: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idUnidad;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*@GET
    * Método que obtiene una unidad de medida específica 
    * Recibe: id de la unidad
    * Retorna: la unidad de medida buscada
    */
    public obtenerUnidadPorId(idMedida: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMedida;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

   /**@GET
    * Método que obtiene todas las unidades de medida
    * @param cantidad 
    * @param filaDesplazamiento 
    * Retorna: una lista con todas las unidades disponibles
    */
    public obtenerListaUnidades(cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "?cantidad=" + cantidad + "&registro=" + filaDesplazamiento;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

   /**@GET
    * Obtiene la lista de unidades de medida que coincidan con la búsqueda
    * @param busqueda 
    * @param cantidad 
    * @param filaDesplazamiento 
    * Retorna: lista con las unidades que cumplan los criterios de búsqueda
    */
    public busquedaUnidades(busqueda: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/buscar/" + busqueda + "?cantidad=" + cantidad + "&registro=" + filaDesplazamiento;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Método para controlar los errores que se dan durante las peticiones al web service
     * @param error 
     */
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}