import { ReglaAsignacionBadge } from '../model/index';
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { AppConfig } from '../app.config';

@Injectable()
export class ReglaAsignacionBadgeService {
    public headers: Headers;
    public token: string;
    public actionUrl: string = `${AppConfig.API_ENDPOINT}/insignias`;
    constructor(public _http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**
     * Metodo que obtiene la lista de reglas de este tipo 
     * Retorna: Respuesta con una lista de todas las reglas encontradas
     * Path: GET /premio/{idBadge}/regla-asignacion
     */
    public getReglasAsignacionBadge(idBadge: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/" + idBadge + "/regla-asignacion/", { headers: this.headers })
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    /**
    * Metodo que crea una nueva regla 
    * Params:
    *         ReglaAsignacionBadge Objeto con la informacion de la regla
    *         idBadge:premio para el cual aplica la asignacion
    * 
    * POST: /premio/{idBadge}/regla-asignacion
    */
    public createReglaAsignacionBadge(idBadge: string, ReglaAsignacionBadge: ReglaAsignacionBadge) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl + "/" + idBadge + "/regla-asignacion/", JSON.stringify(ReglaAsignacionBadge), { headers: this.headers })
            .catch(this.handleError);
    }

    /**
    * Metodo que actualiza una regla de asignacion de premio
    * Params:
    *         ReglaAsignacionBadge Objeto con la informacion de la regla
    *         idBadge:premio para el cual aplica la asignacion
    * 
    * PUT: /premio/{idBadge}/regla-asignacion
    */
    public updateReglaAsignacionBadge(idBadge: string, ReglaAsignacionBadge: ReglaAsignacionBadge) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.put(this.actionUrl + "/" + idBadge + "/regla-asignacion/", JSON.stringify(ReglaAsignacionBadge), { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Metodo que elimina de una lista especifica una regla
     *
     *  Params: idBadge Identificador de segmento
     *          idLista Identificador de la lista de reglas
     *          idRegla Identificador de la regla
     *  Retorna: Usuario usuario en sesión
     * 
     * DELETE:/premio/{idBadge}/regla-asignacion/
     */
    public deleteReglaAsignacionBadge(idBadge: string, idRegla: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.delete(this.actionUrl + "/" + idBadge + "/regla-asignacion/" + idRegla , { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Encargado del manejo de errores, simplemente lanza una secuencia de Observable que terminara en error
     */
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}