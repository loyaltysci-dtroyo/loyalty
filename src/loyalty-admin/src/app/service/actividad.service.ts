import { RegistroActividad } from '../model/registro-actividad';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, Request, XHRConnection, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Miembro, Usuario, Segmento, Categoria, Ubicacion } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from './';
import { Observer } from 'rxjs/Rx';
@Injectable()
export class ActividadService {
    public actionUrl: string = `${AppConfig.API_ENDPOINT}/actividades`;
    public headers: Headers;
    public token: string;

    constructor(public http: Http, public kc: KeycloakService) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    // GET /actividades/metrica
    // Obtener las actividades de todas las metricas
    public getActividadesMetricas(periodo:string): Observable<RegistroActividad[]> {

        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/metrica"+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });
    }

    // GET /actividades/metrica/ { idMetrica }
    // Obtener las actividades de una metrica
    public getActividadesMetrica(idMetrica: string, periodo:string): Observable<RegistroActividad[]> {

        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/metrica/" + idMetrica+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });
    }

    // GET /actividades/miembro/ { idMiembro } / metrica
    // Obtener las actividades del miembro en metricas
    public getActividadesMetricaPorMiembro(idMiembro: string, periodo:string): Observable<RegistroActividad[]> {

        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/miembro/" + idMiembro + "/metrica"+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });
    }

    // GET /actividades/miembro/ { idMiembro } / mision
    // Obtener las actividades del miembro en misiones
    public getActividadesMisionPorMiembro(idMiembro: string, periodo:string): Observable<RegistroActividad[]> {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/miembro/" + idMiembro + "/mision"+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });
    }

    // GET /actividades/miembro/ { idMiembro } / notificacion
    // Obtener las actividades del miembro en notificaciones
    public getActividadesNotificacionPorMiembro(idMiembro: string, periodo:string): Observable<RegistroActividad[]> {

        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/miembro/" + idMiembro + "/notificacion"+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });
    }

    // GET /actividades/miembro/ { idMiembro } / premio
    // Obtener las actividades del miembro en premios
    public getActividadesPremiosPorMiembro(idMiembro: string,periodo:string): Observable<RegistroActividad[]> {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/miembro/" + idMiembro + "/premio"+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => {
            return <RegistroActividad[]>response.json()
        });
    }

    // GET /actividades/miembro/ { idMiembro } / promocion
    // Obtener las actividades del miembro en promociones
    public getActividadesPromocionPorMiembro(idMiembro: string, periodo:string): Observable<RegistroActividad[]> {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/miembro/" + idMiembro + "/promocion"+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });

    }

    // GET /actividades/mision
    // Obtener las actividades de todas las misiones
    public getActividadesMisiones(periodo:string): Observable<RegistroActividad[]> {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/mision"+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });
    }

    // GET /actividades/mision/ { idMision }
    // Obtener las actividades de una mision
    public getActividadesMision(idMision: string, periodo:string): Observable<RegistroActividad[]> {

        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/mision/" + idMision+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });
    }

    // GET /actividades/notificacion
    // Obtener las actividades de todas las notificaciones
    public getActividadesNotificaciones(periodo:string): Observable<RegistroActividad[]> {

        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/notificacion"+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });

    }

    // GET /actividades/notificacion/ { idNotificacion }
    // Obtener las actividades de una mision
    public getActividadesNotificacion(idNotificacion: string,periodo:string): Observable<RegistroActividad[]> {

        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/notificacion/" + idNotificacion+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });
    }

    // GET /actividades/premio
    // Obtener las actividades de todas los premios
    public getActividadesPremios(periodo:string): Observable<RegistroActividad[]> {

        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/premio"+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });
    }

    // GET /actividades/premio/ { idPremio }
    // Obtener las actividades de un premio
    public getActividadesPremio(idPremio: string,periodo:string): Observable<RegistroActividad[]> {

        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/premio/" + idPremio+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });
    }

    // GET /actividades/promocion
    // Obtener las actividades de todas las promociones
    public getActividadesPromociones(periodo:string): Observable<RegistroActividad[]> {

        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/promocion"+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });
    }

    // GET /actividades/promocion/ { idPromocion }
    // Obtener las actividades de una promocion
    public getActividadesPromocion(idPromocion,periodo:string): Observable<RegistroActividad[]> {

        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/promocion/" + idPromocion+"?periodo="+periodo, { headers: this.headers }).map((response: Response) => { return <RegistroActividad[]>response.json() });
    }

}