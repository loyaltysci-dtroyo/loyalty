import { Injectable } from '@angular/core';
import { Http, Response, Headers, Request, XHRConnection, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Promocion, Miembro, Usuario, Segmento, Categoria, Ubicacion } from '../model/index';
import { AppConfig } from '../app.config';
declare var URI:any;
/**
* Flecha Roja Technologies oct-2016
* Fernando Aguilar
* Servicio encargado de realizar operaciones de gestion de datos de promocion interactuando directamente con el API
*/
@Injectable()
export class PromocionService {
    //TODO: recordar actualizar el url desde la clase de configuracion
    actionUrl: string = `${AppConfig.API_ENDPOINT}/promocion`;
    public headers: Headers; public token: string;

    constructor(public Promocion: Promocion, public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    /********************OPERACIONES DE DATOS DE PROMOCIONES**********************/
    /*
    * verifica que el nombre de despliegue del Promocion sea unico
    * URL: get /Promocion/
    * Retorna: un observable que contiene los Promocions
    */
    public validarNombre(nombre: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/exists/" + nombre, { headers: this.headers })
            .map((response: Response) => <string>response.text())
            .catch(this.handleError);
    }
    // /*
    // * Obtiene la lista de promociones
    // * URL: get /Promocion/
    // * Retorna: un observable que contiene los Promocions
    // */
    // public getListaPromociones() {
    //     this.headers.set('Content-Type', 'application/json');
    //     this.headers.set('Accept', 'application/json');
    //     this.headers.set('Authorization', 'bearer ' + this.token);
    //     return this.http.get(this.actionUrl, { headers: this.headers }).catch(this.handleError);
    // }


 /*
     * Encuentra los Promociones que correspondan con los terminos de busqueda
     * Recibe:
     *        -busqueda: termino de busqueda
     *        -estados: array con los estados a tomar en cuenta para la busqueda (C,B,A)
     *        -rowOffset: offset de row inicial para la paginacion
     *        -cantRegistros: cantidad de registros a traer del API
     *        -tipos-filtros
     * 
     * URL:/Promocion/buscar/{busqueda}
     * Retorna: un observable que contiene los Promocions
     */
    public buscarPromociones(busqueda:string[],estados: string[],rowOffset:number,cantRegistros:number,tipos?:string[],filtros?:string[], tipoOrden?:string,campoOrden?: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);

        let query = this.actionUrl + "?busqueda=";
        if (busqueda != null) {
            query += busqueda.join(" ");
        }

        let uri = URI(query);

        if (estados.length > 0) {
            estados.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (tipos && tipos.length > 0) {
            tipos.forEach(element => {
                uri.addSearch("accion", element);
            });
        }
        if (filtros && filtros.length > 0) {
            filtros.forEach(element => {
                uri.addSearch("filtro", element);
            });
        }
        if (tipoOrden&&tipoOrden.length>0) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (campoOrden&&campoOrden.length>0) {
            uri.addSearch("campo-orden", campoOrden);
        }

        if (cantRegistros >= 0) {
            uri.addSearch("cantidad", cantRegistros);
        }
        if (rowOffset >= 0) {
            uri.addSearch("registro", rowOffset);
        }
        // console.log(uri);
        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /*
    * Obtiene una instancia de Promocion 
    * URL: get /promocion/{id_Promocion}
    * Recibe: una instancia con el Promocion a almacenar
    * Retorna: un observable que contiene el Promocion
    */
    public getPromocion(id: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + id, { headers: this.headers }).map((response: Response) => <Promocion>response.json()).catch(this.handleError);
    }
    /*
    * Guarda una instancia de Promocion 
    * URL: post /promocion/
    * Recibe: una instancia con el Promocion a almacenar
    * Retorna: un observable que proporciona información sobre la transacción
    */
    public addPromocion(promocion: Promocion) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl, JSON.stringify(promocion), { headers: this.headers })
            .catch(this.handleError);
    }


    /*
   * Actualiza una instancia de Promocion 
   * URL: put /Promocion/
   * Recibe: una instancia con el Promocion a actualizar
   * Retorna: un observable que proporciona el Promocion, o el error generado
   */
    public updatePromocion(promocion: Promocion) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let PromocionJSON = JSON.stringify(promocion);
        return this.http.put(this.actionUrl, PromocionJSON, { headers: this.headers })
            .catch(this.handleError);

    }
    /*
   * Elimina una instancia de Promocion 
   * URL: delete /Promocion/{id_Promocion}
   * Recibe: una instancia con el Promocion a Eliminar
   * Retorna: un observable que proporciona informacion sobre la transacción, o el error generado
   */
    public deletePromocion(promocion: Promocion) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + promocion.idPromocion, { headers: this.headers }).catch(this.handleError);
    }
    /************OPERACIONES DE OBTENER MIEMBROS/SEGMENTOS/UBICACIONES ELEGIBLES*************/
    /**
    * Retorna la lista de segmentos elegibles de la promo
    * Retorna: lista conteniendo los segmentos elegibles
    * Recibe: el id de la promo y el acton: I=incluir E=excluir
    */
    listarSegmentosElegiblesPromo(idPromocion: string, action: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idPromocion + "/segmento?accion=" + action, { headers: this.headers })
            .map((response: Response) => <Promocion>response.json()).catch(this.handleError);
    }
    /**
    * Retorna la lista de ubicaciones elegibles de la promo
    * Retorna: lista conteniendo lsd ubicaciones elegibles
    * Recibe: el id de la promo y el acton: I=incluir E=excluir
    */
    listarUbicacionesElegiblesPromo(idPromocion: string, action: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idPromocion + "/ubicacion?accion=" + action, { headers: this.headers })
            .map((response: Response) => <Promocion>response.json()).catch(this.handleError);
    }
    /**
    * Retorna la lista de miembros elegibles de la promo
    * Retorna: lista conteniendo los miembros elegibles
    * Recibe: el id de la promo y el acton: I=incluir E=excluir
    */
    listarMiembrosElegiblesPromo(idPromocion: string, action: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idPromocion + "/miembro?accion=" + action, { headers: this.headers })
            .map((response: Response) => <Promocion>response.json()).catch(this.handleError);
    }

    /************OPERACIONES DE INCLUSION/EXCLUSION DE MIEMBROS/SEGMENTOS/UBICACIONES ELEGIBLES*************/

    /**Incluye un miembro a la lista de elgibles de una promocion
     * Recibe: el miembro a agrgar a la lista de elegibles
     * Retorna: un objeto Response con el id del miembro
     */
    incluirMiembro(idPromocion: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idPromocion + "/miembro/" + idMiembro + "?accion=I", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye un miembro a la lista de elgibles de una promocion
     * Recibe: el miembro a excluir de la lista de elegibles
     * Retorna: un objeto Response con el id del miembro
     */
    excluirMiembro(idPromocion: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idPromocion + "/miembro/" + idMiembro + "?accion=E", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Elimina un miembro a la lista de elgibles de una promocion
     * Recibe: el miembro a excluir de la lista de elegibles
     * Retorna: un objeto Response con el id del miembro
     */
    eliminarMiembro(idPromocion: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idPromocion + "/miembro/" + idMiembro, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de miembros de los elegibles de la promocion
    * Recibe: la id de la promocion y una lista de id de miembro a agregar a la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */

    incluirListaMiembros(idPromocion: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idPromocion + "/miembro/?accion=I", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de miembros de la lista de elgibles de una promocion
   * Recibe: la id de la promocion y una lista de id de miembro a excluir a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    excluirListaMiembros(idPromocion: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idPromocion + "/miembro/?accion=E", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de miembros de la lista de elgibles de una promocion
   * Recibe: la id de la promocion y una lista de id de miembro a eliminar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    eliminarListaMiembros(idPromocion: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idPromocion + "/miembro/remover-lista", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye una ubicacion a la lista de elgibles de una promocion
     * Recibe: la ubicacion a agrgar a la lista de elegibles
     * Retorna: un objeto Response con el id de la ubicacion
     */
    incluirUbicacion(idPromocion: string, idUbicacion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idPromocion + "/ubicacion/" + idUbicacion + "?accion=I", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una ubicacion de la lista de elgibles de una promocion
    * Recibe: el id de la promocion y un id de ubicacion a excluir de la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    excluirUbicacion(idPromocion: string, idUbicacion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idPromocion + "/ubicacion/" + idUbicacion + "?accion=E", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Elimina una ubicacion de la lista de elgibles de una promocion
     * Recibe: la ubicacion a excluir de la lista de elegibles
     * Retorna: un objeto Response con el id de la ubicacion
     */
    eliminarUbicacion(idPromocion: string, idUbicacion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idPromocion + "/ubicacion/" + idUbicacion, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye una lista de ubicaciones en la lista de elgibles de una promocion
       * Recibe: la ubicacion a agrgar a la lista de elegibles
       * Retorna: un objeto Response con el id de la ubicacion
       */
    incluirListaUbicaciones(idPromocion: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idPromocion + "/ubicacion/?accion=I", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de ubicaciones de la lista de elgibles de una promocion
   * Recibe: el segmento a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    excluirListaUbicaciones(idPromocion: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idPromocion + "/ubicacion/?accion=E", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de promociones de la lista de elgibles de una promocion
   * Recibe: el segmento a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    eliminarListaUbicaciones(idPromocion: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idPromocion + "/ubicacion/remover-lista", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye un segmento en la lista de elgibles de una promocion
    * Recibe: el segmento a agrgar a la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    incluirSegmento(idPromocion: string, idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idPromocion + "/segmento/" + idSegmento + "?accion=I", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una promocion a la lista de elgibles de una promocion
    * Recibe: el segmento a agrgar a la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    incluirListaSegmentos(idPromocion: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idPromocion + "/segmento/?accion=I", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una promocion a la lista de elgibles de una promocion
   * Recibe: el segmento a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    excluirListaSegmentos(idPromocion: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idPromocion + "/segmento/?accion=E", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una promocion a la lista de elgibles de una promocion
   * Recibe: el segmento a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    eliminarListaSegmentos(idPromocion: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idPromocion + "/segmento/remover-lista", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye una promocion a la lista de elgibles de una promocion
   * Recibe: el segment a excluir de la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    excluirSegmento(idPromocion: string, idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idPromocion + "/segmento/" + idSegmento + "?accion=E", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Elimina una promocion a la lista de elgibles de una promocion
    * Recibe: el segment a excluir de la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    eliminarSegmento(idPromocion: string, idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idPromocion + "/segmento/" + idSegmento, { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Retorna una categoria asociada a una promocion
     * Params:idCategoria - Id de la categoria
     * Retorna:un observble con la categoria
     */
    obtenerCategoriaAsociada(idPromocion: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idPromocion + "/categoria", { headers: this.headers })
            .map((response: Response) => <Categoria[]>response.json()).catch(this.handleError);
    }
    /**
     * Metodo encargado de manejar el error, retorna un error de observable 
     * con una representacion del error en JSON
     */
    public handleError(error: Response) {
        return Observable.throw(error);
    }




}