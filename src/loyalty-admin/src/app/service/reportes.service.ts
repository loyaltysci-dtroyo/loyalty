import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;

/**
 * Jaylin Centeno López 
 * Servicio que permite el manejo y creación de los reportes
 * Consume los servicio web disponibles en ReportesResource del API REST
 */

@Injectable()
export class ReporteService {


    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/reporte`;
    //Representa los headers que se necesitan para la petición
    public headers: Headers;
    public token: string;

    constructor(
        public _http: Http
    ) {

        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* 
     * Se obtiene el reporte de existencias de uno o varios premios 
     * Recibe: periodo, mes, id ubicación y id premio (opcionales)
     * */
    public reporteExistenciasPremio(periodo: number, mes: number, idUbicacion?: string, idPremio?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/existencias";
        let uri = URI(url);
        if (periodo) {
            uri.addSearch("periodo", periodo);
        }
        if (mes) {
            uri.addSearch("mes", mes);
        }
        if (idUbicacion != undefined && idUbicacion != null && idUbicacion.length >0) {
            uri.addSearch("ubicacion", idUbicacion);
        }
        if (idPremio != undefined && idPremio != null && idPremio.length >0) {
            uri.addSearch("premio", idPremio);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* 
     * Se obtiene el reporte de un documento de inventario 
     * Recibe: id del documento
     * */
    public reporteDocumentoInventario(idDocumento: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/documento-inventario?documento="+idDocumento;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /* 
     * Se obtiene el reporte de movimientos de premios 
     * Recibe: periodo, mes, id ubicación y id premio (opcionales)
     * */
    public reporteMovimientos(fechaInicio: number, fechaFin: number, tipoMovimiento?: string, idUbicacion?: string, idPremio?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/movimientos";
        let uri = URI(url);
        if (fechaInicio) {
            uri.addSearch("fecha-inicio", fechaInicio);
        }
        if (fechaFin) {
            uri.addSearch("fecha-fin", fechaFin);
        }
        if (tipoMovimiento) {
            uri.addSearch("tipo-movimiento", tipoMovimiento);
        }
        if (idUbicacion != undefined && idUbicacion != null && idUbicacion.length > 0) {
            uri.addSearch("ubicacion", idUbicacion);
        }
        if (idPremio != undefined && idPremio != null && idPremio.length > 0) {
            uri.addSearch("premio", idPremio);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* 
     * Se obtiene el reporte de carritos
     * Recibe: idCategoria (opcional)
     * */
    public reporteCarritos(fechaInicio: number, fechaFin: number, idCategoria?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/carritos";
        let uri = URI(url);
        if (fechaInicio) {
            uri.addSearch("fecha-inicio", fechaInicio);
        }
        if (fechaFin) {
            uri.addSearch("fecha-fin", fechaFin);
        }
        if (idCategoria) {
            uri.addSearch("idCategoria", idCategoria);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* 
     * Se obtiene el reporte del cátalogo de premios
     * */
    public reporteCatalogoPremios() {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/catalogo-premios";
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }
    /**return this._http.get(url, { responseType: ResponseContentType.Blob }).map(
        (res) => { return new Blob([res.blob()], { type: 'application/pdf' })}) */

    //Método que permite controlar los errores de las transacciones
    public handleError(error: Response) {
        console.error(error);
        return Observable.throw(error);
    }
}