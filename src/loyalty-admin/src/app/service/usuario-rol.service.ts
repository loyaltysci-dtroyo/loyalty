import { Injectable } from '@angular/core';
import { Http, Response,RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { UsuarioRol } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';

/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Service encargado de realizar mantenimientos de datos de la asociacion de roles a usuarios
 */
@Injectable()
export class UsuarioRolService {
    //TODO: recordar actualizar el url desde la clase de configuracion
    public actionUrl: string =`${AppConfig.API_ENDPOINT}/usuario`;
    public headers: Headers;
    public token:string;

    constructor(public usuarioUsuarioRol:UsuarioRol,public http: Http) {

     }
     
     /**
     * GET
     * Retorna la lista de UsuarioRoles  presentes en el API
     * Path:UsuarioRolKeycloak/
     * Retorna:UsuarioRol[]
     */
    public getListaUsuarioRoles () {
        this.headers = new Headers();
        this.headers.append('Authorization', 'bearer '+this.token);
        return this.http.get(this.actionUrl, {headers:this.headers})
            .map((response: Response) => <UsuarioRol[]>response.json())
            .catch(this.handleError);
    }
    /*
    *GET
    *Path: UsuarioRolKeycloak/{idUsuarioRol}
    *Retorna el UsuarioRol a partir del id 
    *Retorna:UsuarioRol
    */
    public getUsuarioRolPorID (id: number){
        return this.http.get(this.actionUrl + "/" + id)
            .map((response: Response) => <UsuarioRol>response.json())
            .catch(this.handleError);
    }
   /*
    *POST
    *Path: UsuarioRolKeycloak/
    *Ingresa un nuevo UsuarioRol a traves de API
    *Retorna:response con informacion sobre,a transaccion
    */
    public insertUsuarioRol (UsuarioRol: UsuarioRol){
         this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        let UsuarioRolJSON = JSON.stringify(UsuarioRol);
        return this.http.post(this.actionUrl, UsuarioRolJSON, { headers: this.headers })
            .catch(this.handleError);
    }
    /*
    *PUT
    *Path: UsuarioRolKeycloak/
    *Ingresa un nuevo UsuarioRol a traves de API
    *Retorna:response con informacion sobre,a transaccion
    */
    public editUsuarioRol (UsuarioRol: UsuarioRol){
         this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
         return this.http.put(this.actionUrl , JSON.stringify(UsuarioRol), { headers: this.headers })
            .map((response: Response) => <UsuarioRol>response.json())
            .catch(this.handleError);
    }
    /*
    *DELETE
    *Path: UsuarioRolKeycloak/{idUsuarioRol}
    *Elimina un UsuarioRol a traves de API
    *Retorna:response con informacion sobre,a transaccion
    */
    public deleteUsuarioRol (UsuarioRol: UsuarioRol){
        return this.http.delete(this.actionUrl + "/" )//TODO FALTA ID
            .catch(this.handleError);
    }
    /*
    *Se encarga de manejar errores, y luego lo lanza hacia la capa de presentacion
    *para darle tratamiento
    */
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}