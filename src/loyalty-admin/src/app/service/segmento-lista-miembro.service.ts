import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Segmento, Miembro } from '../model/index';
import { AppConfig } from '../app.config';
/**
 * Flecha Roja Technologies
 * Autor:Fernnaod Aguilar Morales
 * Service encargado de realizar mantenimiento de datos de los elegibles estáticos de segmento
 * es decir de los miembros definisod por 'override' y no por reglas
 */
@Injectable()
export class SegmentoListaMiembroService {
    actionUrl: string = `${AppConfig.API_ENDPOINT}/segmento/`;
    public headers: Headers;
    public token: string;
    constructor(public Segmento: Segmento, public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    /* 
    * Metodos encargados de traer los datos de las listas blanca y negra desde el API
    *  
    */
    getMiembrosListaBlanca(idSegmento: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + idSegmento + "/miembro?accion=I", { headers: this.headers }).map((response: Response) => <Miembro[]>response.json()).catch(this.handleError);
    }
    getMiembrosListaNegra(idSegmento: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + idSegmento + "/miembro?accion=E", { headers: this.headers }).map((response: Response) => <Miembro[]>response.json()).catch(this.handleError);
    }
    /**
     * Retorna una lista de miembros disponibles para la insercion, los cuales no estan incluidos ya sea en miembros 
     * a incluir o en miembros a excluir
     */
    getMiembrosDisponibles(idSegmento: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + idSegmento + "/miembro?accion=D", { headers: this.headers }).map((response: Response) => <Miembro[]>response.json()).catch(this.handleError);
    }
    
 /**Retorna una lista de miembros disponibles para la insercion, los cuales no estan incluidos ya sea en miembros 
     * a incluir o en miembros a excluir
     */
    searchMiembrosDisponibles(terminos:string[], idSegmento: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + idSegmento + "/miembro?accion=D&busqueda="+terminos.join(" "), { headers: this.headers }).map((response: Response) => <Miembro[]>response.json()).catch(this.handleError);
    }
    /*  
     * Metodos encargados de agregar un miembro a la lista negra y lista blanca de MiembrosComponent respectivamente
     *  Recibe: el miembro a agregar a la lista blanca
     */
    agregarMiembroListaBlanca(idSegmento: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + idSegmento + "/miembro/" + idMiembro + "?accion=I", "", { headers: this.headers }).catch(this.handleError);
    }
    agregarMiembroListaNegra(idSegmento: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + idSegmento + "/miembro/" + idMiembro + "?accion=E", "", { headers: this.headers }).catch(this.handleError);
    }
    /**
     * Metodos que agregan una lista de miembros a una lista negra o lista blanca de miembros
     */
    agregarListaMiembrosListaNegra(miembros: Miembro[], idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let listaId: String[] = miembros.map((element) => { return element.idMiembro });
        return this.http.post(this.actionUrl + idSegmento + "/miembro/?accion=E", listaId, { headers: this.headers }).catch(this.handleError);
    }
    agregarListaMiembrosListaBlanca(miembros: Miembro[], idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let listaId: String[] = miembros.map((element) => { return element.idMiembro });
        return this.http.post(this.actionUrl + idSegmento + "/miembro/?accion=I", listaId, { headers: this.headers }).catch(this.handleError);
    }

    /*  Metodo encargado de eliminar un miembro de las listas negra o lista blanca de MiembrosComponent 
    *   Recibe: el miembro a agregar a la lista blanca
    */

    eliminarMiembroListas(idSegmento: string, idMiembro: string) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + idSegmento + "/miembro/" + idMiembro, { headers: this.headers }).catch(this.handleError);
    }
    /**
     * Elimina una lista de miembros de los miembros a incluir/excluir de un segmento 
     */
    eliminarListaMiembroListas(miembros: Miembro[], idSegmento: string) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        let listaId: String[] = miembros.map((element) => { return element.idMiembro });
        return this.http.put(this.actionUrl + idSegmento + "/miembro/remover-lista", listaId, { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Metodo encargado de manejar el error, retorna un error de observable con una representacion del error en JSON
     * 
     */
    public handleError(error:any) {
        return Observable.throw(error);
    }
}