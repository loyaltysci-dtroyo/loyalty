import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { MedioPago, Configuracion } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';

/**
 * Jaylin Centeno López 
 * Servicio utilizado para los medios de pago
 * Consume los servicio web disponibles en MediosPagosResource del API REST
 */
@Injectable()
export class MedioPagoService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/medios-pago`;
    //Representa los headers que se necesitan para la petición
    public headers: Headers;
    public token:string;

    constructor(
        public _http: Http
    ) {
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /*Método que obtiene todos los medio de pago 
    Recibe: cantidad de filas y la fila de dezplazamiento
    Retorna: una lista con todos los medio de pago disponibles*/
    public obtenerListaMedios(cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url:any;
        if (cantidad == 0 && filaDesplazamiento == 0) {
            url = this.actionUrl;
        } else {
            url = this.actionUrl + "?cantidad=" + cantidad + '&registro=' + filaDesplazamiento;
        }
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para agregar un medio de pago 
    Recibe: medio de pago*/
    public insertarMedioPago(medio: MedioPago) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let MedioJSON = JSON.stringify(medio);
        return this._http.post(this.actionUrl, MedioJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para remover un medio de pago 
    Recibe: id del medio de pago
    Retorna: el resultado de la transacción*/
    public eliminarMedio(idPago: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idPago;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*Método que permite controlar los errores durante las peticiones*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }

}