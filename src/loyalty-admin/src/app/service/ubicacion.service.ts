import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Ubicacion } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;

@Injectable()

/**
 * Jaylin Centeno López 
 * Servicio que permite manipular la entidad de ubicación
 * Consume los servicio web disponibles en UbicacionResource
 */

/* ------------------------------------------------------*/
export class UbicacionService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/ubicacion`;
    public token: string;
    public headers: Headers;

    constructor(
        public _http: Http,
        public ubicacion: Ubicacion
    ) {
        //Headers para realizar las peticiones
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /** @GET
     * Método para obtener una ubicación por id
     * Recibe: el id de una ubicación 
     * Retorna: ubicación
     */
    public obtenerUbicacionId(idUbicacion: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idUbicacion;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Ubicacion>response.json())
            .catch(this.handleError);
    }

    /** @POST
     * Método para agregar una ubicación
     * Recibe: ubicación a insertar
     * Retorna: el resultado de la transacción
     */
    public agregarUbicacion(ubicacion: Ubicacion) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let UbicacionJson = JSON.stringify(ubicacion);
        return this._http.post(this.actionUrl, UbicacionJson, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /** @PUT 
     * Método para actualizar una ubicación
     * Recibe: ubicación a actualizar
     * Retorna: el resultado de la transacción
     */
    public editarUbicacion(ubicacion: Ubicacion) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let UbicacionJson = JSON.stringify(ubicacion);
        return this._http.put(this.actionUrl, UbicacionJson, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /** @PUT
     * Método para el cambio de estado de una ubicacion
     * Recibe: el id y el estado de la ubicacion 
     * Retorna: el resultado de la transacción
     * Publicado - Archivado - Draft
     */
    public cambiarEstado(idUbicacion: string, estado: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/cambiar-estado/" + idUbicacion;
        let JsonEstado = JSON.stringify(estado);
        return this._http.put(url, JsonEstado, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /** @DELETE
     * Método para remover una ubicación, siempre y cuando este en Draft
     * Recibe: id de la ubicación a eliminar
     * Retorna: resultado de la transacción
     */
    public removerUbicacion(idUbicacion: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/eliminar/" + idUbicacion;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /** @GET
     * Método para obtener las ubicaciones que coincidan con la búsqueda 
     * Recibe: los criterios de búsqueda y el estado 
     * Retorna: lista de las ubicaciones que cumplen los criterios de búsqueda
     */
    public busquedaUbicacion(estado: string[], cantidad: number, filaDesplazamiento: number, busqueda?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl;
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (busqueda && busqueda.length > 0) {
            uri.addSearch("busqueda", busqueda)
        }
        if (estado && estado.length > 0) {
            estado.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /*Metodo para controlar los errores que se dan durante las peticiones al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}