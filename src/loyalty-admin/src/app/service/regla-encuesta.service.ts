import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { ReglaAvanzada } from '../model/index';
import { AppConfig } from '../app.config';

@Injectable()
export class ReglaEncuestaService {
    public headers: Headers;
    public token: string;
    public actionUrl: string = `${AppConfig.API_ENDPOINT}/segmento`;
    constructor(public _http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**
     * obtiene la lista de reglas de encuesta asociadas a un segmento   
     * @param idSegmento id del segmento asociado  
     * @param idLista id de la lista de reglas asociada 
     */
    public getReglasEncuesta(idSegmento: string, idLista: string): Observable<Response> {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/" + idSegmento + "/reglas/"+idLista+"/encuesta", { headers: this.headers })
            .catch(this.handleError);
    }
    /**
     * 
     * @param idLista el id de la lista de reglas a asociar la regla
     * @param idSegmento el id del segmento a asociar la regla
     */
    public agregarReglaEncuesta(idSegmento: string, idLista: string, reglaEncuesta: any): Observable<Response> {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl + "/" + idSegmento + "/reglas/"+idLista+"/encuesta",JSON.stringify(reglaEncuesta), { headers: this.headers })
            .catch(this.handleError);
    }
    /**
     * 
     * @param idRegla id de la regla a remover
     */
    public removerReglaEncuesta(idSegmento: string, idLista: string, idRegla: string): Observable<Response> {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.delete(this.actionUrl + "/" + idSegmento + "/reglas/"+idLista+"/encuesta/"+idRegla, { headers: this.headers })
            .catch(this.handleError);
    }
    /**
   * Encargado del manejo de errores, simplemente lanza una secuencia de Observable que terminara en error
   */
    public handleError(error: Response) {
        return Observable.throw(error);
    }

}