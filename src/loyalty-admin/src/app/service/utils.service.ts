import { Injectable } from '@angular/core';
import { Http, Response,RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { AppConfig } from '../app.config';

/*Este servicio consume la API REST de UtilResource*/
/*---------------------------------------------------------------------------*/

@Injectable()

export class UtilsService {

    //Contiene la ruta en el que se encuentra el API REST
    actionUrl: string = `${AppConfig.API_ENDPOINT}/util`;
    public headers: Headers;
    public token:string;

    constructor(public _http: Http) {
        //Son los headers que necesita tener la peticion
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* 
    * Método que obtiene la cantidad de miembros según rangos de edad
    * Recibe un indicador de estado
    * Retorna: un json con la información por rangos de edad
    */
    public obtenerRangosEdad(indicador: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/rango-edad?indicador="+indicador;
        return this._http.get(url, { headers: this.headers })
        //.map((response: Response) => response.json())
            .catch(this.handleError);
    }

    /* 
    * Método que obtiene la cantidad de miembros por genero
    * Recibe un indicador de estado
    * Retorna un json con la información por rango de genero
    */
    public obtenerRangosGenero(indicador: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/rango-genero?indicador="+indicador;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*
   Metodo que permite controlar los errores 
   */
    public handleError(error: Response) {
        console.log(error.text());
        return Observable.throw(error);
    }


}