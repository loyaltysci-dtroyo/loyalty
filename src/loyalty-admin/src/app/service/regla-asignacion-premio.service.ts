import { ReglaAsignacionPremio } from '../model/index';
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { AppConfig } from '../app.config';

@Injectable()
export class ReglaAsignacionPremioService {
    public headers: Headers;
    public token: string;
    public actionUrl: string = `${AppConfig.API_ENDPOINT}/premio`;
    constructor(public _http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**
     * Metodo que obtiene la lista de reglas de este tipo 
     *
     * Retorna: Respuesta con una lista de todas las reglas encontradas
     * 
     * Path: GET /premio/{idPremio}/regla-asignacion
     */
    public getReglasAsignacionPremio(idPremio: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl + "/" + idPremio + "/regla-asignacion/", { headers: this.headers })
            .map((response: Response) => <ReglaAsignacionPremio[]>response.json())
            .catch(this.handleError);
    }

    /**
    * Metodo que crea una nueva regla 
    * Params:
    *         ReglaAsignacionPremio Objeto con la informacion de la regla
    *         idPremio:premio para el cual aplica la asignacion
    * 
    * POST: /premio/{idPremio}/regla-asignacion
    */
    public createReglaAsignacionPremio(idPremio: string, ReglaAsignacionPremio: ReglaAsignacionPremio) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.post(this.actionUrl + "/" + idPremio + "/regla-asignacion/", JSON.stringify(ReglaAsignacionPremio), { headers: this.headers })
            .catch(this.handleError);
    }

    /**
    * Metodo que actualiza una regla de asignacion de premio
    * Params:
    *         ReglaAsignacionPremio Objeto con la informacion de la regla
    *         idPremio:premio para el cual aplica la asignacion
    * 
    * PUT: /premio/{idPremio}/regla-asignacion
    */
    public updateReglaAsignacionPremio(idPremio: string, ReglaAsignacionPremio: ReglaAsignacionPremio) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.put(this.actionUrl + "/" + idPremio + "/regla-asignacion/", JSON.stringify(ReglaAsignacionPremio), { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Metodo que elimina de una lista especifica una regla
     *
     *  Params: idPremio Identificador de segmento
     *          idLista Identificador de la lista de reglas
     *          idRegla Identificador de la regla
     *  Retorna: Usuario usuario en sesión
     * 
     * DELETE:/premio/{idPremio}/regla-asignacion/
     */
    public deleteReglaAsignacionPremio(idPremio: string, idRegla: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.delete(this.actionUrl + "/" + idPremio + "/regla-asignacion/" + idRegla , { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Encargado del manejo de errores, simplemente lanza una secuencia de Observable que terminara en error
     */
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}