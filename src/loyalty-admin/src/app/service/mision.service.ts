import { Preferencia } from '../model';
import { Observable, Observer } from 'rxjs/Rx';
import { Categoria, Miembro, MisionEncuestaPregunta, Mision, MisionAtrPerfil, MisionVerContenido, Segmento, Ubicacion, Usuario, MisionSocial, MisionSubirContenido, PremioMisionJuego, MisionJuego } from '../model/index';
import { Headers, Http, Request, RequestOptionsArgs, Response, XHRConnection } from '@angular/http';
import { AppConfig } from '../app.config';
import { Injectable } from '@angular/core';
import { MisionRealidadAumentada } from 'app/model/mision-realidad-aumentada';

/**
* Flecha Roja Technologies oct-2016
* Fernando Aguilar
* Servicio encargado de realizar operaciones de gestion de datos de mision interactuando directamente con el API
*/
declare var URI: any;
@Injectable()
export class MisionService {
    //TODO: recordar actualizar el url desde la clase de configuracion
    actionUrl: string = `${AppConfig.API_ENDPOINT}/mision`;
    actionUrlCategoria: string = `${AppConfig.API_ENDPOINT}/categoria-mision`;
    public headers: Headers; public token: string;

    constructor(public Mision: Mision, public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    /********************OPERACIONES DE DATOS DE PROMOCIONES**********************/
    /*
    * verifica que el nombre de despliegue de la Mision sea unico
    * URL: get /Mision/
    * Retorna: un observable que contiene los Misions
    */
    public validarNombre(nombre: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/exists/" + nombre, { headers: this.headers })
            .map((response: Response) => <string>response.text())
            .catch(this.handleError);
    }
    /*
     * Encuentra los Promociones que correspondan con los terminos de busqueda
     * Recibe:
     *        -busqueda: termino de busqueda
     *        -estados: array con los estados a tomar en cuenta para la busqueda (C,B,A)
     *        -rowOffset: offset de row inicial para la paginacion
     *        -cantRegistros: cantidad de registros a traer del API
     *        -tipos-filtros
     * 
     * URL:/Promocion/buscar/{busqueda}
     * Retorna: un observable que contiene los Promocions
     */
    public buscarMisiones(busqueda: string[], estados: string[], rowOffset: number, cantRegistros: number, tipos?: string[], aprobaciones?: string[], filtros?: string[], tipoOrden?: string, campoOrden?: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);

        let query = this.actionUrl + "?busqueda=";
        if (busqueda != null) {
            query += busqueda.join(" ");
        }

        let uri = URI(query);

        if (estados.length > 0) {
            estados.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (tipos && tipos.length > 0) {
            tipos.forEach(element => {
                uri.addSearch("tipo", element);
            });
        }
        if (aprobaciones && aprobaciones.length > 0) {
            aprobaciones.forEach(element => {
                uri.addSearch("aprobacion", element);
            });
        }
        if (filtros && filtros.length > 0) {
            filtros.forEach(element => {
                uri.addSearch("filtro", element);
            });
        }
        if (tipoOrden && tipoOrden.length > 0) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (campoOrden && campoOrden.length > 0) {
            uri.addSearch("campo-orden", campoOrden);
        }

        if (cantRegistros >= 0) {
            uri.addSearch("cantidad", cantRegistros);
        }
        if (rowOffset >= 0) {
            uri.addSearch("registro", rowOffset);
        }
        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    // GET /mision/{idMision}/encuesta-preferencia
    public obtenerListaPreferenciasDisponibles(idMision: string): Observable<any> {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/encuesta-preferencia?tipo=D", { headers: this.headers }).map((response: Response) => <Preferencia[]>response.json()).catch(this.handleError);
    }

    // GET /mision/{idMision}/encuesta-preferencia
    public obtenerListaPreferenciasAsociadas(idMision: string): Observable<any> {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/encuesta-preferencia?tipo=A", { headers: this.headers }).map((response: Response) => <Preferencia[]>response.json()).catch(this.handleError);
    }

    // PUT /mision/{idMision}/encuesta-preferencia
    public asociarPreferencias(idsPreferencia: string[], idMision: any) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/encuesta-preferencia", JSON.stringify(idsPreferencia), { headers: this.headers }).map((response: Response) => <Preferencia[]>response.json()).catch(this.handleError);
    }
    // PUT /mision/{idMision}/encuesta-preferencia/remover
    public removerPreferencias(idsPreferencia: string[], idMision: any) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/encuesta-preferencia/remover", JSON.stringify(idsPreferencia), { headers: this.headers }).map((response: Response) => <Preferencia[]>response.json()).catch(this.handleError);
    }
    public actualizarRetoPreferencia(): Observable<any> {
        //TODO
        return new Observable();
    }



    /*
    * Obtiene una instancia de Mision 
    * URL: get /mision/{id_Mision}
    * Recibe: una instancia con el Mision a almacenar
    * Retorna: un observable que contiene el Mision
    */
    public getMision(id: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + id, { headers: this.headers }).map((response: Response) => <Mision>response.json()).catch(this.handleError);
    }
    /*
    * Guarda una instancia de Mision 
    * URL: post /mision/
    * Recibe: una instancia con el Mision a almacenar
    * Retorna: un observable que proporciona información sobre la transacción
    */
    public addMision(mision: Mision) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl, JSON.stringify(mision), { headers: this.headers })
            .catch(this.handleError);
    }


    /*
   * Actualiza una instancia de Mision 
   * URL: put /Mision/
   * Recibe: una instancia con el Mision a actualizar
   * Retorna: un observable que proporciona el Mision, o el error generado
   */
    public updateMision(mision: Mision) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let MisionJSON = JSON.stringify(mision);
        return this.http.put(this.actionUrl, MisionJSON, { headers: this.headers })
            .catch(this.handleError);

    }
    /*
   * Elimina una instancia de Mision 
   * URL: delete /Mision/{id_Mision}
   * Recibe: una instancia con el Mision a Eliminar
   * Retorna: un observable que proporciona informacion sobre la transacción, o el error generado
   */
    public deleteMision(mision: Mision) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + mision.idMision, { headers: this.headers }).catch(this.handleError);
    }
    /************OPERACIONES DE OBTENER MIEMBROS/SEGMENTOS/UBICACIONES ELEGIBLES*************/
    /**
    * Retorna la lista de segmentos elegibles de la mision
    * Retorna: lista conteniendo los segmentos elegibles
    * Recibe: el id de la mision y el acton: I=incluir E=excluir
    */
    listarSegmentosElegiblesMision(idMision: string, action: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/segmento?accion=" + action, { headers: this.headers })
            .map((response: Response) => <Segmento[]>response.json()).catch(this.handleError);
    }
    /**
    * Retorna la lista de ubicaciones elegibles de la mision
    * Retorna: lista conteniendo lsd ubicaciones elegibles
    * Recibe: el id de la mision y el acton: I=incluir E=excluir
    */
    listarUbicacionesElegiblesMision(idMision: string, action: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/ubicacion?accion=" + action, { headers: this.headers })
            .map((response: Response) => <Ubicacion[]>response.json()).catch(this.handleError);
    }
    /**
    * Retorna la lista de miembros elegibles de la mision
    * Retorna: lista conteniendo los miembros elegibles
    * Recibe: el id de la mision y el acton: I=incluir E=excluir
    */
    listarMiembrosElegiblesMision(idMision: string, action: string, cantidad: number, registro: number) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl+ "/" + idMision + "/miembro?accion=" + action);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (registro >= 0) {
            uri.addSearch("registro", registro);
        }
        return this.http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError); //.map((response: Response) => <Miembro[]>response.json())
    }

    /************OPERACIONES DE INCLUSION/EXCLUSION DE MIEMBROS/SEGMENTOS/UBICACIONES ELEGIBLES*************/

    /**Incluye un miembro a la lista de elgibles de una mision
     * Recibe: el miembro a agrgar a la lista de elegibles
     * Retorna: un objeto Response con el id del miembro
     */
    incluirMiembro(idMision: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/miembro/" + idMiembro + "?accion=I", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye un miembro a la lista de elgibles de una mision
     * Recibe: el miembro a excluir de la lista de elegibles
     * Retorna: un objeto Response con el id del miembro
     */
    excluirMiembro(idMision: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/miembro/" + idMiembro + "?accion=E", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Elimina un miembro a la lista de elgibles de una mision
     * Recibe: el miembro a excluir de la lista de elegibles
     * Retorna: un objeto Response con el id del miembro
     */
    eliminarMiembro(idMision: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idMision + "/miembro/" + idMiembro, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de miembros de los elegibles de la mision
    * Recibe: la id de la mision y una lista de id de miembro a agregar a la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */

    incluirListaMiembros(idMision: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/miembro/?accion=I", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de miembros de la lista de elgibles de una mision
   * Recibe: la id de la mision y una lista de id de miembro a excluir a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    excluirListaMiembros(idMision: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/miembro/?accion=E", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de miembros de la lista de elgibles de una mision
   * Recibe: la id de la mision y una lista de id de miembro a eliminar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    eliminarListaMiembros(idMision: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/miembro/remover-lista", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye una ubicacion a la lista de elgibles de una mision
     * Recibe: la ubicacion a agrgar a la lista de elegibles
     * Retorna: un objeto Response con el id de la ubicacion
     */
    incluirUbicacion(idMision: string, idUbicacion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/ubicacion/" + idUbicacion + "?accion=I", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una ubicacion de la lista de elgibles de una mision
    * Recibe: el id de la mision y un id de ubicacion a excluir de la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    excluirUbicacion(idMision: string, idUbicacion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/ubicacion/" + idUbicacion + "?accion=E", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Elimina una ubicacion de la lista de elgibles de una mision
     * Recibe: la ubicacion a excluir de la lista de elegibles
     * Retorna: un objeto Response con el id de la ubicacion
     */
    eliminarUbicacion(idMision: string, idUbicacion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idMision + "/ubicacion/" + idUbicacion, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye una lista de ubicaciones en la lista de elgibles de una mision
       * Recibe: la ubicacion a agrgar a la lista de elegibles
       * Retorna: un objeto Response con el id de la ubicacion
       */
    incluirListaUbicaciones(idMision: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/ubicacion/?accion=I", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de ubicaciones de la lista de elgibles de una mision
   * Recibe: el segmento a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    excluirListaUbicaciones(idMision: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/ubicacion/?accion=E", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de misiones de la lista de elgibles de una mision
   * Recibe: el segmento a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    eliminarListaUbicaciones(idMision: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/ubicacion/remover-lista", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye un segmento en la lista de elgibles de una mision
    * Recibe: el segmento a agrgar a la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    incluirSegmento(idMision: string, idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/segmento/" + idSegmento + "?accion=I", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una mision a la lista de elgibles de una mision
    * Recibe: el segmento a agrgar a la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    incluirListaSegmentos(idMision: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/segmento/?accion=I", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una mision a la lista de elgibles de una mision
   * Recibe: el segmento a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    excluirListaSegmentos(idMision: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/segmento/?accion=E", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una mision a la lista de elgibles de una mision
   * Recibe: el segmento a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    eliminarListaSegmentos(idMision: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/segmento/remover-lista", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye una mision a la lista de elgibles de una mision
   * Recibe: el segment a excluir de la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    excluirSegmento(idMision: string, idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/segmento/" + idSegmento + "?accion=E", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Elimina una mision a la lista de elgibles de una mision
    * Recibe: el segment a excluir de la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    eliminarSegmento(idMision: string, idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idMision + "/segmento/" + idSegmento, { headers: this.headers })
            .catch(this.handleError);
    }

    /**
     * Verifica que el nombre interno de la categoria no exista en la base de datos previamente
     * esto para mantener la integridad de los datos y evitar que se produzca un error ya que el 
     * campo es unico
     */
    verificarNombreInternoCategoria(nombre: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrlCategoria + "/comprobar-nombre/" + nombre, { headers: this.headers })
            .catch(this.handleError);

    }

    /**
     * Retorna la lista de categorias de mision
     * Params:idMision - Id de la mision sobre la cual se obtienen las categorias
     * Retorna:un observble con la categoria
     */
    obtenerListaCategorias(rowOffset: number, cantRegistros: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrlCategoria + "?cantidad=" + cantRegistros + "&registro=" + rowOffset, { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Metodo encargado de obtener una categoria de mision del sistema,
     * para lo cual interactua con el servicio web que expone el api
     * GET categoria-mision
     */
    obtenerCategoria(idCategoria: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrlCategoria + '/' + idCategoria, { headers: this.headers })
            .map((response: Response) => response.json()).catch(this.handleError);
    }

    /**
     * Metodo encargado de insertar una categoria de mision en el sistema,
     * para lo cual interactua con el servicio web que expone el api
     * POST categoria-mision
     */
    insertarCategoria(categoria: Categoria) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrlCategoria, JSON.stringify(categoria), { headers: this.headers }).catch(this.handleError);
    }
    /**
     * Metodo encargado de modificar una categoria de mision en el sistema,
     * para lo cual interactua con el servicio web que expone el api
     * PUT 
     */
    modificarCategoria(categoria: Categoria) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrlCategoria, JSON.stringify(categoria), { headers: this.headers }).catch(this.handleError);
    }
    /**
     * Metodo encargado de eliminar una categoria de mision en el sistema,
     * para lo cual interactua con el servicio web que expone el api
     * DELETE 
     */
    eliminarCategoria(idCategoria: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrlCategoria + "/" + idCategoria, { headers: this.headers }).catch(this.handleError);
    }
    /**
    * Retorna la lista de categorias asociadas a una mision
    * Params:idMision - Id de la mision sobre la cual se obtienen las categorias
    * Retorna:un observble con la categoria
    */
    obtenerCategoriasAsociadas(idMision: string, rowOffset: number, cantRegistros: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/categoria?cantidad=" + cantRegistros + "&registro=" + rowOffset, { headers: this.headers })
            .map((response: Response) => <Categoria[]>response.json()).catch(this.handleError);
    }

    /**
     * Obtiene las misiones que estan asociadas a una categoria, para lo cual toma como parametro el id de la
     * categoria
     */
    obtenerMisionesPorCategoria(idCategoria: string, rowOffset: number, cantRegistros: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrlCategoria + "/" + idCategoria + "/mision?cantidad=" + cantRegistros + "&registro=" + rowOffset, { headers: this.headers }).catch(this.handleError);
    }
    /**
    * Retorna una categoria asociada a una mision
    * Params:idCategoria - Id de la categoria
    * Retorna:un observble con la categoria
    * POST {idCategoria}/mision/{idMision}
    */
    asignarCategoriaMision(idCategoria: string, idMision: string): Observable<Categoria[]> {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrlCategoria + "/" + idCategoria + "/mision/" + idMision, "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**
     * Retorna una categoria asociada a una mision
     * Params:idCategoria - Id de la categoria
     * Retorna:un observble con la categoria
     * DELETE {idCategoria}/mision/{idMision}
     */
    removerCategoriaMision(idCategoria: string, idMision: string): Observable<Categoria[]> {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrlCategoria + "/" + idCategoria + "/mision/" + idMision, { headers: this.headers })
            .catch(this.handleError);
    }

    /***********OPERACIONES Mision encuesta*************************/
    /**
     * Obtiene la lista de pregutnas de encuesta
     * GET /mision/{idMision}/encuesta
     */
    obtenerPreguntasEncuesta(idMision: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/encuesta", { headers: this.headers }).catch(this.handleError);

    }
    /**
    * Obtiene la lista de pregutnas de encuesta
    * GET /mision/{idMision}/encuesta
    */
    obtenerPreguntaEncuestaPorId(idMision: string, idPregunta: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/encuesta/" + idPregunta, { headers: this.headers }).catch(this.handleError);

    }
    /**
     * Crea una pregunta de encuesta
     * POST /mision/{idMision}/encuesta
     */
    crearPreguntaEncuesta(idMision: string, pregunta: MisionEncuestaPregunta) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/encuesta", JSON.stringify(pregunta), { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Modifica una pregunta de encuesta existente
     * PUT /mision/{idMision}/encuesta
     */
    editarPreguntasEncuesta(idMision: string, pregunta: MisionEncuestaPregunta) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/encuesta", JSON.stringify(pregunta), { headers: this.headers }).catch(this.handleError);

    }
    /**
     * Elimina una pregunta de encuesta
     * DELETE /mision/{idMision}/encuesta/{idPregunta}
     */
    eliminarPreguntasEncuesta(idMision: string, idPregunta: string) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idMision + "/encuesta/" + idPregunta, { headers: this.headers }).catch(this.handleError);
    }

    /***********OPERACIONES Mision ver contenido********************/
    /**
     * Obtiene la definicion de la mision de contenido para una mision en especifico cuando la mision es de tipo ver contenido
     * GET /mision/{idMision}/ver-contenido
     */
    obtenerContenido(idMision: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/ver-contenido", { headers: this.headers }).map((response: Response) => <MisionVerContenido>response.json()).catch(this.handleError);
    }

    /**
    * Almacena los datos de configuracion de reto para una mision en concreto cuando la mision es de ver contenido
    * PUT /mision/{idMision}/ver-contenido
    * Params:
    *  idMision - id de la mision a la cual agregar el contenido
    *  misionVerContenido - metadatos del reto para la mision de ver contenido
    */
    actualizarContenido(idMision: string, misionVerContenido: MisionVerContenido) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/ver-contenido", JSON.stringify(misionVerContenido), { headers: this.headers }).catch(this.handleError);
    }

    /***********OPERACIONES Mision subir contenido****************/
    /**
     * Obtiene la definicion del reto para las misiones de subir contenido, en caso de queel reto no haya sido definido entonces retornara uncodigo de error
     * 404, el cual debe ser caturado  para crear una nueva definicin en blanco
     */
    obtenerRetoSubirContenido(idMision: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/subir-contenido", { headers: this.headers }).map((element) => { return <MisionSubirContenido>element.json() }).catch(this.handleError);
    }
    /**
     * Crea un reto de subir contenido para la mision 
     */
    crearRetoSubirContenido(idMision: string, reto: MisionSubirContenido) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/subir-contenido", JSON.stringify(reto), { headers: this.headers }).catch(this.handleError);
    }

    /**
    * actualiza un reto de subir contenido para la mision 
    */
    actualizarRetoSubirContenido(idMision: string, reto: MisionSubirContenido) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/subir-contenido", JSON.stringify(reto), { headers: this.headers }).catch(this.handleError);
    }
    /**
    * elimina un reto de subir contenido para la mision 
    */
    eliminarRetoSubirContenido(idMision: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idMision + "/subir-contenido", { headers: this.headers }).catch(this.handleError);
    }
    /***********OPERACIONES Mision red social****************/

    /**
        * Obtiene la definicion del reto para las misiones de subir contenido, en caso de que el reto no haya sido definido entonces retornara uncodigo de error
        * 404, el cual debe ser caturado  para crear una nueva definicin en blanco
        */

    obtenerRetoRedSocial(idMision: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/red-social", { headers: this.headers }).map((element) => { return <MisionSocial>element.json() }).catch(this.handleError);
    }
    /**
    * Crea un reto de red scial para la mision 
    */
    crearRetoRedSocial(idMision: string, reto: MisionSocial) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/red-social", JSON.stringify(reto), { headers: this.headers }).catch(this.handleError);
    }
    /**
     * modifica un reto de red scial para la mision 
     */
    modificarRetoRedSocial(idMision: string, reto: MisionSocial) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/red-social", JSON.stringify(reto), { headers: this.headers }).catch(this.handleError);
    }
    /**
     * elimina un reto de red scial para la mision 
     */
    eliminarRetoRedSocial(idMision: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idMision + "/subir-contenido", { headers: this.headers }).catch(this.handleError);
    }

      /***********OPERACIONES Mision realidad aumentada****************/

    /**
        * Obtiene la definicion del reto para las misiones de realidad aumentada, en caso de que el reto no haya sido definido entonces retornara uncodigo de error
        * 404, el cual debe ser caturado  para crear una nueva definicin en blanco
        */

        obtenerRetoRealidadAumentada(idMision: string) {
            this.headers.set('Content-Type', 'application/json');
            this.headers.set('Accept', 'application/json');
            this.headers.set('Authorization', 'bearer ' + this.token);
            return this.http.get(this.actionUrl + "/" + idMision + "/realidad-aumentada", { headers: this.headers }).map((element) => { return <MisionSocial>element.json() }).catch(this.handleError);
        }
        /**
        * Crea un reto de red scial para la mision 
        */
        crearRetoRealidadAumentada(idMision: string, reto: MisionRealidadAumentada) {
            this.headers.set('Content-Type', 'application/json');
            this.headers.set('Accept', 'application/json');
            this.headers.set('Authorization', 'bearer ' + this.token);
            return this.http.post(this.actionUrl + "/" + idMision + "/realidad-aumentada", JSON.stringify(reto), { headers: this.headers }).catch(this.handleError);
        }
        /**
         * modifica un reto de red scial para la mision 
         */
        modificarRetoRealidadAumentada(idMision: string, reto: MisionRealidadAumentada) {
            this.headers.set('Content-Type', 'application/json');
            this.headers.set('Accept', 'application/json');
            this.headers.set('Authorization', 'bearer ' + this.token);
            return this.http.put(this.actionUrl + "/" + idMision + "/realidad-aumentada", JSON.stringify(reto), { headers: this.headers }).catch(this.handleError);
        }
        /**
         * elimina un reto de red scial para la mision 
         */
        eliminarRetoRealidadAumentada(idMision: string) {
            this.headers.set('Content-Type', 'application/json');
            this.headers.set('Accept', 'application/json');
            this.headers.set('Authorization', 'bearer ' + this.token);
            return this.http.delete(this.actionUrl + "/" + idMision + "/realidad-aumentada", { headers: this.headers }).catch(this.handleError);
        }
    /***********OPERACIONES Mision actualizar perfil****************/
    /**
     * Obtiene la definicion de la mision de contenido para una mision en especifico cuando la mision es de tipo ver contenido
     * GET /mision/{idMision}/actualizar-perfil
     */
    obtenerAtributosMisionPerfil(idMision: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/actualizar-perfil", { headers: this.headers }).map((response: Response) => <MisionAtrPerfil>response.json()).catch(this.handleError);

    }
    /**
     * Almacena los datos de configuracion de reto para una mision en concreto cuando la mision es de ver contenido
     * POST /mision/{idMision}/actualizar-perfil
     * Params:
     *  idMision - id de la mision a la cual agregar el contenido
     *  misionAtrPerfil - metadatos del reto para la mision de actualizar perfil
     */
    crearAtributoMisionPerfil(idMision: string, misionAtrPerfil: MisionAtrPerfil) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/actualizar-perfil", JSON.stringify(misionAtrPerfil), { headers: this.headers }).catch(this.handleError);
    }

    /**
    * Almacena los datos de configuracion de reto para una mision en concreto cuando la mision es de ver contenido
    * PUT /mision/{idMision}/actualizar-perfil/remover-atributo
    */
    removerAtributoMisionPerfil(idMision: string, misionAtrPerfil: MisionAtrPerfil) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idMision + "/actualizar-perfil/" + misionAtrPerfil.idAtributo, { headers: this.headers }).catch(this.handleError);
    }
    /**
     * Almacena los datos de configuracion de reto para una mision en concreto cuando la mision es de ver contenido
     * POST /mision/{idMision}/actualizar-perfil
     * Params:
     *  idMision - id de la mision a la cual agregar el contenido
     *  misionAtrPerfil - metadatos del reto para la mision de actualizar perfil
     */
    crearListaAtributosMisionPerfil(idMision: string, misionAtrPerfil: MisionAtrPerfil[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/actualizar-perfil", JSON.stringify(misionAtrPerfil), { headers: this.headers }).catch(this.handleError);
    }

    /**
    * Almacena los datos de configuracion de reto para una mision en concreto cuando la mision es de ver contenido
    * PUT /mision/{idMision}/actualizar-perfil/remover-atributo
    */
    removerListaAtributosMisionPerfil(idMision: string, misionAtrPerfil: MisionAtrPerfil[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let ids: string[];
        misionAtrPerfil.forEach((element) => {
            ids.push(element.idAtributo);
        })
        //misionAtrPerfil.idAtributo
        return this.http.delete(this.actionUrl + "/" + idMision + "/actualizar-perfil/", { headers: this.headers }).catch(this.handleError);
    }

    /***********OPERACIONES de Juego****************/

    /**
     * Crea un juego
     * POST /mision/{idMision}/juego
     */
    agregarJuego(idMision: string, juego: MisionJuego) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/juego", JSON.stringify(juego), { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Actualiza el juego
     * PUT /mision/{idMision}/juego
     */
    actualizarJuego(idMision: string, juego: MisionJuego) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/juego", JSON.stringify(juego), { headers: this.headers }).catch(this.handleError);
    }

    /**
    * Obtiene el juego por id de la misión
    * GET /mision/{idMision}/encuesta
    */
    obtenerJuegoPorId(idMision: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/juego", { headers: this.headers }).map((response: Response) => <MisionJuego>response.json()).catch(this.handleError);
    }

    /***********OPERACIONES de Mision juego (premios)****************/

    /**
     * Registrar un premio de una misión de tipo juego
     * POST /mision/{idMision}/juego/premio
     */
    registrarPremioJuego(idMision: string, premioJuego: PremioMisionJuego) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idMision + "/juego/premio", JSON.stringify(premioJuego), { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Actualizar un premio de una misión de tipo juego
     * PUT /mision/{idMision}/juego/premio
     */
    editarPremioJuego(idMision: string, premioJuego: PremioMisionJuego) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/juego/premio", JSON.stringify(premioJuego), { headers: this.headers }).catch(this.handleError);
    }

    /**
    * Obtiene el premio del juego por id
    * GET /mision/{idMision}/juego/premio
    */
    obtenerPremioJuegoPorId(idMision: string, premioJuego: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idMision + "/juego/premio/" + premioJuego, { headers: this.headers }).catch(this.handleError);
    }

    /**
    * Remueve los premios de una misión de tipo juego
    * DELETE /mision/{idMision}/juego/premio
    */
    removerPremioJuegoMision(idMision: string, premioJuego: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idMision + "/juego/premio/" + premioJuego, { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Obtiene la lista de los premios del juego
     * GET /mision/{idMision}/actualizar-perfil
     */
    obtenerPremiosMisionJuego(idMision: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMision + "/juego/premio";
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }

    /************************Respuestas de mision **************************************************/
    /**
     * Obtiene las respuestas que estan pendientes de calificar por un administrador
     */
    public getRespuestasPendientes(idMision: string, estado: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idMision + "/respuestas";
        let uri = URI(url);
        if (estado) {
            uri.addSearch("estado", estado)
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this.http.get(uri.toString(), { headers: this.headers }).catch(this.handleError);
    }
    /**
     * Aprueba/reprueba la respuesta de la mision para un miembro
     * @param aprobacion objeto con info sobre la aprobacion/no aprobacion
     * @param idMision id de la mision a aprobar
     */
    public enviarAprobacion(aprobacion: any, idMision: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idMision + "/respuestas/", JSON.stringify(aprobacion), { headers: this.headers }).catch(this.handleError);
    }

    /**
     * Metodo encargado de manejar el error, retorna un error de observable 
     * con una representacion del error en JSON
     */
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}