import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { DocumentoInventario, DetalleDocumento } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;
/**
 * Jaylin Centeno López 
 * Servicio utilizado para manejar los documentos de inventarios
 * Consume los servicio web disponibles en DocumentoInventarioResource del API REST
 */
@Injectable()
export class DocumentoInventarioService {


    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/documento`;
    //Representa los headers que se necesitan para la petición
    public headers: Headers;
    public token: string;

    constructor(
        public _http: Http
    ) {

        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /********************Búsqueda de documentos********************/
    /*------------------------------------------------------------*/

    /* &&
    * Método que obtiene un documento por su identificador
    * Recibe: el id del documento
    * Retorna: el documento buscado
    */
    public obtenerDocumentoPorId(idDocumento: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idDocumento;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <DocumentoInventario>response.json())
            .catch(this.handleError);
    }

    /* &&
    * Método para obtener lista de documentos utilizando distintos parametros 
    * Recibe: el tipo de documento, la fila de desplazamiento y la cantidad de filas
    * Retorna: lista con los documentos del mismo tipo
    */
    public obtenerDocumentosPorTerminos(cantidad: number, filaDesplazamiento: number,tipos?: string[], ubicacion?: string, cerrado?: string, tipoOrden?: string, campoOrden?: string,  ) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl;
        let uri = URI(url);

        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }

        if (tipos && tipos.length > 0) {
            tipos.forEach(element => {
                uri.addSearch("tipo", element);
            });
        }

        if(ubicacion != undefined && ubicacion != null && ubicacion.length >0){
            uri.addSearch("ubicacion", ubicacion);
        }

        if(cerrado){
            uri.addSearch("cerrado", cerrado)
        }

        if (tipoOrden) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (campoOrden) {
            uri.addSearch("campo-orden", campoOrden);
        }

        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* &&
    * Método para obtener lista de documentos por tipo y ubicacion 
    * Recibe: el tipo de documento, el id de la ubicación, la fila de desplazamiento y la cantidad de filas
    * Retorna: lista con los documentos del mismo tipo, de una sola ubicación
    */
    public obtenerDocumentosUbicacionTipo(idUbicacion: string, tipo: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let tipos = tipo.split('');
        let params = "";
        tipos.forEach(element => {
            if(params == ""){
                params += "?tipo="+element;
            }else{
                params += "&tipo="+element;
            }
        });
        let url = this.actionUrl + "/ubicacion/" + idUbicacion;
        if (cantidad == 0 && filaDesplazamiento == 0) {
            if (tipo != "") {
                url += params;
            }
        } else {
            if (tipo != "") {
                url += params + '&cantidad=' + cantidad + '&registro=' + filaDesplazamiento;
            } else {
                url += "?cantidad=" + cantidad + '&registro=' + filaDesplazamiento;
            }
        }
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /********************* Manejo de documentos *******************/
    /*------------------------------------------------------------*/

    /* &&
    * Método que registra un nuevo documento de inventario
    * Recibe: documento a insertar
    * Retorna: retorna el id del documento
    */
    public insertarDocumento(documento: DocumentoInventario) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let DocumentoJSON = JSON.stringify(documento);
        return this._http.post(this.actionUrl, DocumentoJSON, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* &&
    * Método para actualizar un documento 
    * Recibe: documento con los valores actualizados
    * Retorna: respuesta de la transacción 
    */
    public editarDocumento(documento: DocumentoInventario) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let DocumentoJSON = JSON.stringify(documento);
        return this._http.put(this.actionUrl, DocumentoJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /* &&
    * Método para remover un documento de inventario, siempre y cuando no tenga detalles relacionados
    * Recibe: id del documento
    * Retorna: el resultado de la transacción
    */
    public eliminarDocumento(idDocumento: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idDocumento;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* &&
    * Método para cerrar un documento
    * Recibe: el número del documento
    * Retorna: respuesta de la transacción 
    */
    public cerrarDocumento(numDocumento: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        //let DocumentoJSON = JSON.stringify(documento);
        let url = this.actionUrl + "/" + numDocumento + "/cerrar";
        return this._http.put(url, "", { headers: this.headers })
            .catch(this.handleError);
    }

    /* &&
    * Método para hacer un cierre de mes
    * Párametros: id de la ubicación
    * Respuesta: resultado de la transacción 
    */
    public cerrarMes(){
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/cerrar-mes";
        return this._http.put(url, "", { headers: this.headers })
            .catch(this.handleError);
    }

    /************* Disponibilidad de premios en ubicaciones ************/
    /*-----------------------------------------------------------------*/

    /* &&
    * Obtener los premios disponibles en una ubicación 
    * Recibe: id de la ubicación, la fila de desplazamiento y la cantidad de filas
    * Retorna: los premios disponibles
    */
    public obtenerPreimosPorUbicacion(idUbicacion: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/ubicacion/" + idUbicacion + "/premios-disponibles";
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        /*if (cantidad == 0 && filaDesplazamiento == 0) {
            url = this.actionUrl + "/ubicacion/" + idUbicacion + "/premios-disponibles";
        } else {
            url = this.actionUrl + "/ubicacion/" + idUbicacion + "/premios-disponibles?cantidad=" + cantidad + '&registro=' + filaDesplazamiento;
        }*/
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /********************* Detalle del documento *************************/
    /*-----------------------------------------------------------------*/

    /* &&
    * Método que registra un nuevo detalle del documento de inventario
    * Recibe: número de documento y el detalle a insertar
    * Retorna: retorna el id del detalle
    */
    public insertarDetalle(numDocumento: string, detalle: DetalleDocumento) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + '/' + numDocumento + '/detalle';
        let DetalleJSON = JSON.stringify(detalle);
        return this._http.post(url, DetalleJSON, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* &&
    * Método para actualizar un detalle 
    * Recibe: detalle con los valores actualizados
    * Retorna: respuesta de la transacción 
    */
    public editarDetalle(numDocumento: string, detalle: DetalleDocumento) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + '/' + numDocumento + '/detalle';
        let DetalleJSON = JSON.stringify(detalle);
        return this._http.put(url, DetalleJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /*&&
    * Método para remover un detalle de un documento de inventario
    * Recibe: id del documento
    * Retorna: el resultado de la transacción
    */
    public eliminarDetalle(numDocumento: string, numDetalle: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + '/' + numDocumento + '/detalle/' + numDetalle;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* 
    * Método que obtiene una lista con los detalles del documento 
    * Recibe: el id del documento
    * Retorna: lista de detalles
    */
    public obtenerDetallesDocumento(numDocumento: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl;
        if (cantidad != 0) {
            url = this.actionUrl +'/'+ numDocumento + '/detalle?cantidad=' + cantidad + '&registro=' + filaDesplazamiento;
        }
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /* 
    * Método que obtiene un documento usando el id 
    * Recibe: el id del documento
    * Retorna: el documento buscado
    */
    public obtenerDetallePorId(numDocumento: string, numDetalle: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + numDocumento + '/detalle/' + numDetalle;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    //Método que permite controlar los errores de las transacciones
    public handleError(error: Response) {
        return Observable.throw(error);
    }

}
