import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Premio, CategoriaPremio } from '../model/index';
import { AppConfig } from '../app.config';
declare var URI: any;

/**
 * Jaylin Centeno López 
 * Servicio utilizado para manejar las categorías de premio
 * Consume los servicio web disponibles en PremioResource del API REST
 */
@Injectable()

export class CategoriaPremioService {

    //Contiene la ruta en el que se encuentra el servicio REST
    actionUrl: string = `${AppConfig.API_ENDPOINT}/categoria-premio`;
    public headers: Headers;
    public token: string;

    constructor(
        public _http: Http, public premio: Premio
    ) {
        //Son los headers que necesita tener la petición
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* Método para agregar una nueva categoría 
    Recibe: categoría a insertar
    Retorna: retorna un json con el id de la categoría*/
    public insertarCategoriaPremio(categoria: CategoriaPremio) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let CategoriaJSON = JSON.stringify(categoria);
        return this._http.post(this.actionUrl, CategoriaJSON, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para actualizar una categoría 
    Recibe: categoría con los valores actualizados
    Retorna: respuesta de la transacción */
    public editarCategoriaPremio(categoria: CategoriaPremio) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let CategoriaJSON = JSON.stringify(categoria);
        return this._http.put(this.actionUrl, CategoriaJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para remover una categoría del sistema
    Recibe: id de la categoría
    Retorna: el resultado de la transacción*/
    public eliminarCategoriaPremio(idCategoria: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idCategoria;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método que obtiene una categoría en específico 
    Recibe: el id categoría
    Retorna: la categoría que coincide con el id*/
    public obtenerCategoriaPorId(idCategoria: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idCategoria;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <CategoriaPremio>response.json())
            .catch(this.handleError);
    }

    /* Método para obtener lista de categorías que coincidan con la búsqueda 
    Recibe: los criterios de búsqueda, cantidad de filas, la fila de desplazamiento
    //cantidad y filaDesplazamiento son para la paginación
    Retorna: lista con las categorías que cumplen los criterios de búsqueda*/
    public busquedaCategorias(cantidad: number, filaDesplazamiento: number, busqueda?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl;
        
        if (busqueda) {
            url+=busqueda;
        }
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para obtener el listado de los premios asignados a una categoría
    Recibe: el id de la categoría, cantidad de filas, la fila de desplazamiento
    //cantidad y filaDesplazamiento son para la paginación
    Retorna: lista con las categorías que cumplen los criterios de búsqueda*/
    public obtenerPremiosCategoria(idCategoria: string, cantidad: number, filaDesplazamiento: number, estado?: string[], tipo?: string[], busqueda?: string, accion?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idCategoria + "/premio";
        let uri = URI(url);
        if (busqueda) {
            uri.addSearch("busqueda", busqueda)
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        if (estado.length > 0) {
            estado.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (tipo && tipo.length > 0) {
            tipo.forEach(element => {
                uri.addSearch("tipo", element);
            });
        }
        if (accion) {
            uri.addSearch("accion", accion);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para obtener el listado de categorías asignadas a un premio*/
    public obtenerCategoriasPremio(idPremio: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/premio/" + idPremio;
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para obtener el listado de categorías asignadas a un premio*/
    public obtenerCategoriasNoPremio(idPremio: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/premio/" + idPremio + "/categorias-desligadas";
        let uri = URI(url);
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para verificar si existe el nombre de la categoría
    Recibe: el nombre 
    Retorna: resultado de la transacción (true/false) */
    public verificarNombre(nombre: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/exists/" + nombre;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <boolean>response.json())
            .catch(this.handleError);
    }


    /*********************   Asignar categorías a Premios   ****************/

    /* Método para asignar una categoría a una lista de premios
    Recibe: el id de categoría y lista de premios
    Retorna: resultado de la transacción*/
    public asignarLista(idCategoria: string, premios: string[]) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let ListaJson = JSON.stringify(premios);
        let url = this.actionUrl + "/" + idCategoria + "/premio";
        return this._http.post(url, ListaJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para remover una asignación de categoría a una lista de premios 
    Recibe: id de cateogoria y la lista de ids de premios a remover
    Retorna: resultado de la transacción*/
    public removerLista(premios: string[], idCategoria: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idCategoria + "/premio/remover-lista";
        let ListaJson = JSON.stringify(premios);
        return this._http.put(url, ListaJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para asignar una categoría a un premio
    Recibe: el id de la categoría y del premio
    Retorna: resultado de la transacción*/
    public asignarPremio(idCategoria: string, idPremio: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idCategoria + "/premio/" + idPremio;
        return this._http.post(url, "", { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para remover una asignación de una categoría a de un premio
    Recibe: el id de categoría y del premio
    Retorna: resultado de la transacción*/
    public removerPremio(idCategoria: string, idPremio: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idCategoria + "/premio/" + idPremio;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}