import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { CategoriaProducto } from '../model/index';
import { AppConfig } from '../app.config';

declare var URI: any;
/**
 * Jaylin Centeno López 
 * Servicio utilizado para manejar las categorías del producto
 * Consume los servicio web disponibles en CategoriaProductoResource del API REST
 */
@Injectable()

export class CategoriaProductoService {

    //Contiene la ruta en el que se encuentra el servicio REST
    actionUrl: string = `${AppConfig.API_ENDPOINT}/categoria-producto`;
    public headers: Headers;
    public token;

    constructor(
        public _http: Http,
    ) {
        //Son los headers que necesita tener la petición
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /* Método para agregar una nueva categoría 
    Recibe: categoría a insertar
    Retorna: retorna un json con el id de la categoría*/
    public insertarCategoriaProducto(categoria: CategoriaProducto) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let CategoriaJSON = JSON.stringify(categoria);
        return this._http.post(this.actionUrl, CategoriaJSON, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para actualizar una categoría 
    Recibe: categoría con los valores actualizados
    Retorna: respuesta de la transacción */
    public editarCategoriaProducto(categoria: CategoriaProducto) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let CategoriaJSON = JSON.stringify(categoria);
        return this._http.put(this.actionUrl, CategoriaJSON, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para remover una categoría del sistema
    Recibe: id de la categoría
    Retorna: el resultado de la transacción*/
    public eliminarCategoriaProducto(idCategoria: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idCategoria;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método que obtiene una categoría en específico 
    Recibe: el id categoría
    Retorna: la categoría que coincide con el id*/
    public obtenerCategoriaPorId(idCategoria: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idCategoria;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <CategoriaProducto>response.json())
            .catch(this.handleError);
    }

    /*Método que obtiene todas las categorías de producto
    Recibe: cantidad de registro y fila de desplazamiento
    //cantidad y filaDesplazamiento son para la paginación
    Retorna: una lista con todos las categorías disponibles*/
    public obtenerListaCategorias(cantidad: number, filaDesplazamiento: number, busqueda?: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let uri = URI(this.actionUrl);
        if (busqueda) {
            uri.addSearch("busqueda", busqueda)
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para verificar si existe el nombreInterno de la categoría
    Recibe: el nombre 
    Retorna: resultado de la transacción (true/false) */
    public verificarNombre(nombreInterno: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/exists/" + nombreInterno;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    /*----------------Manejo de certificado------------------*/

    /* Método para agregar un nuevo certificado de una categoria
    Recibe: un certificado 
    Retorna: retorna un json con el id del certificado*/
    public insertarCertificado(idCategoria: string, numCertificado: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url =this.actionUrl +"/"+idCategoria+"/certificado";
        return this._http.post(url, numCertificado, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /* Método para generar un badge de certificados de premio 
    Recibe: cantidad a generar*/
    /*public generarCertificados(idPremio: string, cantidad: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'text/plain');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url =this.actionUrl +"/"+idPremio+"/certificado/generar?cantidad="+ cantidad;
        return this._http.post(url, "", { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }*/

    /*Método para eliminar un certificado, cuando el estado del premio es BORRADOR
    Recibe: id del certificado
    Retorna: el resultado de la transacción*/
    /*public eliminarCertificado(idPremio: string, idCertificado: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url= this.actionUrl + "/" + idPremio+"/certificado/"+idCertificado;
        return this._http.delete(url, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }*/

    /*Método que obtiene todos los certificados (por estado) 
    Recibe: idCategoria
    Retorna: una lista con todos los certificados disponibles*/
    public obtenerListaCertificados(idCategoriaProducto: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        const url = this.actionUrl + "/" + idCategoriaProducto + "/certificado"
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}