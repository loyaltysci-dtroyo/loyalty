import { WorkflowNodo } from '../model';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, Request, XHRConnection, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Workflow, Miembro, Usuario, Segmento, Categoria, Ubicacion } from '../model/index';
import { AppConfig } from '../app.config';
declare var URI: any;
/**
* Flecha Roja Technologies oct-2016
* Fernando Aguilar
* Servicio encargado de realizar operaciones de gestion de datos de workflow interactuando directamente con el API
*/
@Injectable()
export class WorkflowService {
    public actionUrl: string = `${AppConfig.API_ENDPOINT}/workflow`;
    public headers: Headers;
    public token: string;

    constructor(public Workflow: Workflow, public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }
    /********************OPERACIONES DE DATOS DE workflows**********************/

    /*
    * Obtiene la lista de workflows
    * URL: get /Workflow/
    * Retorna: un observable que contiene los Workflows
    */
    public getListaWorkflows(indPagina: number, cantRegistros: number) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl, { headers: this.headers }).catch(this.handleError);
    }
    /*
     * Encuentra los Workflows que correspondan con los terminos de busqueda
     * Recibe:
     *        -busqueda: termino de busqueda
     *        -estados: array con los estados a tomar en cuenta para la busqueda (C,B,A)
     *        -rowOffset: offset de row inicial para la paginacion
     *        -cantRegistros: cantidad de registros a traer del API
     * URL:/Workflow/{busqueda}
     * Retorna: un observable que contiene los Workflows
     */
    public buscarWorkflows(busqueda: string[], estados: string[], rowOffset: number, cantRegistros: number, tipos?: string[], filtros?: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        
        let query = URI(this.actionUrl + "?");
        query.addSearch("busqueda", busqueda.join(" "));

        if (estados&&estados.length > 0) {
            estados.forEach(estado => {
                query.addSearch("estado", estado);
            });
        }
        if (tipos&&tipos.length > 0) {
            tipos.forEach(tipo => {
                query.addSearch("tipo", tipo);
            });
        }
         if (filtros&&filtros.length > 0) {
            filtros.forEach(filtro => {
                query.addSearch("filtro", filtro);
            });
        }
        if (cantRegistros >= 0) {
            query.addSearch("cantidad", cantRegistros);
        }
        if (rowOffset >= 0) {
            query.addSearch("registro", rowOffset);
        }
        let hdrs = new Headers();
        return this.http.get(query.toString(), { headers: this.headers }).catch(this.handleError);
    }
    /*
    * valida el nombre interno de un Workflow 
    * URL: get /workflow/{id_Workflow}
    * Recibe: una instancia con el Workflow a almacenar
    * Retorna: un observable que contiene el Workflow
    * Path: /comprobar-nombre/{nombreInterno}
    */
    public validarNombre(nombre: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/comprobar-nombre/" + nombre, { headers: this.headers }).map((response: Response) => response.text()).catch(this.handleError);
    }

    /*
    * Obtiene una instancia de Workflow 
    * URL: get /workflow/{id_Workflow}
    * Recibe: una instancia con el Workflow a almacenar
    * Retorna: un observable que contiene el Workflow
    */
    public getWorkflow(id: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + id, { headers: this.headers }).map((response: Response) => <Workflow>response.json()).catch(this.handleError);
    }
    /*
    * Guarda una instancia de Workflow 
    * URL: post /workflow/
    * Recibe: una instancia con el Workflow a almacenar
    * Retorna: un observable que proporciona información sobre la transacción
    */
    public addWorkflow(workflow: Workflow) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl, JSON.stringify(workflow), { headers: this.headers })
            .catch(this.handleError);
    }

    /*
   * Actualiza una instancia de Workflow 
   * URL: put /Workflow/
   * Recibe: una instancia con el Workflow a actualizar
   * Retorna: un observable que proporciona el Workflow, o el error generado
   */
    public updateWorkflow(workflow: Workflow) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl, JSON.stringify(workflow), { headers: this.headers })
            .catch(this.handleError);

    }
    /*
   * Elimina una instancia de Workflow 
   * URL: delete /Workflow/{id_Workflow}
   * Recibe: una instancia con el Workflow a Eliminar
   * Retorna: un observable que proporciona informacion sobre la transacción, o el error generado
   */
    public deleteWorkflow(workflow: Workflow) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + workflow.idWorkflow, { headers: this.headers }).catch(this.handleError);
    }
    /************OPERACIONES DE CRUD NODOS*************/
    /** 
    * Obtiene especificamente un nodo de workflow con un id especifico
    * Retorna: un elemento nodo
    * Recibe:el id del nodo a obtener
    */
    obtenerNodoWorkflow(idNodo: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/nodo/" + idNodo, { headers: this.headers })
            .map((response: Response) => <WorkflowNodo>response.json()).catch(this.handleError);
    }
    /**
    * Retorna la lista de nodos para un workflow en el orden establecido
    * Retorna: la lista de nodos 
    * Recibe: el id del workflow raiz
    * GET /lista/{idWorkflow}
    */
    listarNodosPorIdWorkflow(idWorkflow: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/nodo/lista/" + idWorkflow, { headers: this.headers })
            .map((response: Response) => <WorkflowNodo>response.json()).catch(this.handleError);
    }
    /**
    * Retorna la lista de miembros elegibles de la promo
    * Retorna: lista conteniendo los miembros elegibles
    * 
    */
    insertarNodoWorkflow(idPadre: string, nodo: WorkflowNodo) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/nodo/" + idPadre, JSON.stringify(nodo), { headers: this.headers })
            .map((response: Response) => <Workflow>response.json()).catch(this.handleError);
    }
    /**
   * Retorna la lista de miembros elegibles de la promo
   * Retorna: lista conteniendo los miembros elegibles
   * Recibe: el id de la promo y el acton: I=incluir E=excluir
   */
    pushNodoWorkflow(idWorkflow: string, nodo: WorkflowNodo) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/nodo/push/" + idWorkflow, JSON.stringify(nodo), { headers: this.headers })
            .catch(this.handleError);
    }
    /**
    * Retorna la lista de miembros elegibles de la promo
    * Retorna: lista conteniendo los miembros elegibles
    * Recibe: el id de la promo y el acton: I=incluir E=excluir
    */
    editarNodoWorkflow(nodo: WorkflowNodo) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/nodo/", JSON.stringify(nodo), { headers: this.headers })
            .catch(this.handleError);
    }
    /**
    * Retorna la lista de miembros elegibles de la promo
    * Retorna: lista conteniendo los miembros elegibles
    * Recibe: el id de la promo y el acton: I=incluir E=excluir
    */
    eliminarNodoWorkflow(idNodo: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/nodo/" + idNodo, { headers: this.headers })
            .catch(this.handleError);
    }


      /************OPERACIONES DE INCLUSION/EXCLUSION DE MIEMBROS/SEGMENTOS/UBICACIONES ELEGIBLES*************/
      /**Estas operaciones son usadas cuando elworkflow es de tipo secuencia de workflows*/

    /**Incluye un miembro a la lista de elgibles de un workflow
     * Recibe: el miembro a agrgar a la lista de elegibles
     * Retorna: un objeto Response con el id del miembro
     */
    incluirMiembro(idWorkflow: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idWorkflow + "/miembro/" + idMiembro + "?accion=I", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye un miembro a la lista de elgibles de un workflow
     * Recibe: el miembro a excluir de la lista de elegibles
     * Retorna: un objeto Response con el id del miembro
     */
    excluirMiembro(idWorkflow: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idWorkflow + "/miembro/" + idMiembro + "?accion=E", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Elimina un miembro a la lista de elgibles de un workflow
     * Recibe: el miembro a excluir de la lista de elegibles
     * Retorna: un objeto Response con el id del miembro
     */
    eliminarMiembro(idWorkflow: string, idMiembro: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idWorkflow + "/miembro/" + idMiembro, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de miembros de los elegibles del workflow
    * Recibe: la id del workflow y una lista de id de miembro a agregar a la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */

    incluirListaMiembros(idWorkflow: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idWorkflow + "/miembro/?accion=I", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de miembros de la lista de elgibles de un workflow
   * Recibe: la id del workflow y una lista de id de miembro a excluir a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    excluirListaMiembros(idWorkflow: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idWorkflow + "/miembro/?accion=E", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de miembros de la lista de elgibles de un workflow
   * Recibe: la id del workflow y una lista de id de miembro a eliminar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    eliminarListaMiembros(idWorkflow: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idWorkflow + "/miembro/remover-lista", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye una ubicacion a la lista de elgibles de un workflow
     * Recibe: la ubicacion a agrgar a la lista de elegibles
     * Retorna: un objeto Response con el id de la ubicacion
     */
    incluirUbicacion(idWorkflow: string, idUbicacion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idWorkflow + "/ubicacion/" + idUbicacion + "?accion=I", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una ubicacion de la lista de elgibles de un workflow
    * Recibe: el id del workflow y un id de ubicacion a excluir de la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    excluirUbicacion(idWorkflow: string, idUbicacion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idWorkflow + "/ubicacion/" + idUbicacion + "?accion=E", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Elimina una ubicacion de la lista de elgibles de un workflow
     * Recibe: la ubicacion a excluir de la lista de elegibles
     * Retorna: un objeto Response con el id de la ubicacion
     */
    eliminarUbicacion(idWorkflow: string, idUbicacion: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idWorkflow + "/ubicacion/" + idUbicacion, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye una lista de ubicaciones en la lista de elgibles de un workflow
       * Recibe: la ubicacion a agrgar a la lista de elegibles
       * Retorna: un objeto Response con el id de la ubicacion
       */
    incluirListaUbicaciones(idWorkflow: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idWorkflow + "/ubicacion/?accion=I", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de ubicaciones de la lista de elgibles de un workflow
   * Recibe: el segmento a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    excluirListaUbicaciones(idWorkflow: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idWorkflow + "/ubicacion/?accion=E", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye una lista de workflows de la lista de elgibles de un workflow
   * Recibe: el segmento a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    eliminarListaUbicaciones(idWorkflow: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idWorkflow + "/ubicacion/remover-lista", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye un segmento en la lista de elgibles de un workflow
    * Recibe: el segmento a agrgar a la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    incluirSegmento(idWorkflow: string, idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idWorkflow + "/segmento/" + idSegmento + "?accion=I", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye un workflow a la lista de elgibles de un workflow
    * Recibe: el segmento a agrgar a la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    incluirListaSegmentos(idWorkflow: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idWorkflow + "/segmento/?accion=I", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye un workflow a la lista de elgibles de un workflow
   * Recibe: el segmento a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    excluirListaSegmentos(idWorkflow: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idWorkflow + "/segmento/?accion=E", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Excluye un workflow a la lista de elgibles de un workflow
   * Recibe: el segmento a agrgar a la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    eliminarListaSegmentos(idWorkflow: string, ids: string[]) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.put(this.actionUrl + "/" + idWorkflow + "/segmento/remover-lista", ids, { headers: this.headers })
            .catch(this.handleError);
    }
    /**Incluye un workflow a la lista de elgibles de un workflow
   * Recibe: el segment a excluir de la lista de elegibles
   * Retorna: un objeto Response con el id de la ubicacion
   */
    excluirSegmento(idWorkflow: string, idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.post(this.actionUrl + "/" + idWorkflow + "/segmento/" + idSegmento + "?accion=E", "", { headers: this.headers })
            .catch(this.handleError);
    }
    /**Elimina un workflow a la lista de elgibles de un workflow
    * Recibe: el segment a excluir de la lista de elegibles
    * Retorna: un objeto Response con el id de la ubicacion
    */
    eliminarSegmento(idWorkflow: string, idSegmento: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.delete(this.actionUrl + "/" + idWorkflow + "/segmento/" + idSegmento, { headers: this.headers })
            .catch(this.handleError);
    }


       /************OPERACIONES DE OBTENER MIEMBROS/SEGMENTOS/UBICACIONES ELEGIBLES*************/
    /**
    * Retorna la lista de segmentos elegibles de el workflow
    * Retorna: lista conteniendo los segmentos elegibles
    * Recibe: el id de el workflow y el acton: I=incluir E=excluir
    */
    listarSegmentosElegiblesWorkflow(idWorkflow: string, action: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idWorkflow + "/segmento?accion=" + action, { headers: this.headers })
            .map((response: Response) => <Segmento[]>response.json()).catch(this.handleError);
    }
    /**
    * Retorna la lista de ubicaciones elegibles de el workflow
    * Retorna: lista conteniendo lsd ubicaciones elegibles
    * Recibe: el id de el workflow y el acton: I=incluir E=excluir
    */
    listarUbicacionesElegiblesWorkflow(idWorkflow: string, action: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idWorkflow + "/ubicacion?accion=" + action, { headers: this.headers })
            .map((response: Response) => <Ubicacion[]>response.json()).catch(this.handleError);
    }
    /**
    * Retorna la lista de miembros elegibles de el workflow
    * Retorna: lista conteniendo los miembros elegibles
    * Recibe: el id de el workflow y el acton: I=incluir E=excluir
    */
    listarMiembrosElegiblesWorkflow(idWorkflow: string, action: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Accept', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this.http.get(this.actionUrl + "/" + idWorkflow + "/miembro?accion=" + action, { headers: this.headers })
            .map((response: Response) => <Miembro[]>response.json()).catch(this.handleError);
    }



    /**
     * Metodo encargado de manejar el error, retorna un error de observable 
     * con una representacion del error en JSON
     */
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}