import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';


/*-------------Este servicio consume la API REST de ExportResource------------*/
/*---------------------------------------------------------------------------*/
declare var URI: any;
@Injectable()
export class ExportacionService {

    actionUrl: string = `${AppConfig.API_ENDPOINT}/exportar`; //Ruta en el que se encuentra el servicio web
    public headers: Headers; //Headers que se necesitan para la petición
    public token: string;

    constructor(
        public _http: Http
    ) {
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /**
     * Exportación de la información de miembros 
     * Recibe: id segmento
     * Retorna: lista miembros base64
     */
     public exportarMiembrosSegmento(idSegmento: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        return this._http.get(this.actionUrl+"/segmento/"+idSegmento,  { headers: this.headers })
            .catch(this.handleError);
    }


    /*Método para controlar los errores que se dan durante las peticiones 
    al web service*/
    public handleError(error: Response) {
        return Observable.throw(error);
    }
}