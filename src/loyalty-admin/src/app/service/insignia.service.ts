import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { Insignia, InsigniaNivel, Miembro } from '../model/index';
import { AppConfig } from '../app.config';
import { KeycloakService } from '../service/index';
declare var URI: any;

/**
 * Jaylin Centeno López 
 * Servicio utilizado para manejar las insignias y sus respectivos niveles
 * Consume los servicio web disponibles en InsigniaResource del API REST
 */
@Injectable()
export class InsigniaService {

    //Contiene la ruta en el que se encuentra el servicio web
    actionUrl: string = `${AppConfig.API_ENDPOINT}/insignias`;
    //Representa los headers que se necesitan para la petición
    public headers: Headers;
    public token:string;

    constructor(
        public _http: Http
    ) {
        this.headers = new Headers;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', 'bearer ' + this.token);
    }

    /*Método para insertar una insignia
    Recibe: la insignia a insertar
    Retorna: el resultado de la transacción*/
    public insertarInsignia(insignia: Insignia) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let insigniaJson = JSON.stringify(insignia);
        return this._http.post(this.actionUrl, insigniaJson, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*Método para actualizar una inisgnia
    Recibe: la insignia a actualizar
    Retorna: el resultado de la transacción*/
    public actualizarInsignia(insignia: Insignia) {
        //this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let insigniaJson = JSON.stringify(insignia);
        return this._http.put(this.actionUrl, insigniaJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para eliminar una insignia
    Recibe: el id de la insignia a eliminar
    Retorna: el resultado de la transacción*/
    public removerInsignia(idInsignia: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idInsignia;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para obtener la lista de todas las insignias según el estado
    Recibe: estado
    Retorna: lista de insignias*/
    public obtenerInsigniasEstado(estado: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "?estado=" + estado+'&cantidad=' + cantidad + '&registro=' + filaDesplazamiento;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para buscar una insignia por id
   Recibe: el id de la insignia
   Retorna: la insignia buscado*/
    public obtenerInsigniaPorId(idInsignia: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idInsignia;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <Insignia>response.json())
            .catch(this.handleError);
    }

    /*Método para realizar búsquedas de insignias
    Recibe: palabras claves
    Retorna: el resultado de la búsqueda */
    public buscarInsignias(palabrasClaves: string, cantidad: number, filaDesplazamiento: number) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/buscar/" + palabrasClaves;
        return this._http.get(url, { headers: this.headers })
            .catch(this.handleError);
    }
    /*Método para realizar búsquedas de insignias
    Recibe: palabras claves
    Retorna: el resultado de la búsqueda */
    public buscarInsigniasTemp(estado: string[],cantidad: number, filaDesplazamiento: number, tipos?: string[],palabrasClaves?: string, filtro?: string[], tipoOrden?: string, campoOrden?: string ) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl;
        let uri = URI(url);

        if(estado && estado.length > 0){
            estado.forEach(element => {
                uri.addSearch("estado", element);
            });
        }
        if (tipos && tipos.length > 0) {
            tipos.forEach(element => {
                uri.addSearch("tipo-insignia", element);
            });
        }
        if(palabrasClaves){
            uri.addSearch("busqueda", palabrasClaves)
        }
        if (filtro && filtro.length > 0) {
            filtro.forEach(element => {
                uri.addSearch("filtro", element);
            });
        }
        if (tipoOrden) {
            uri.addSearch("tipo-orden", tipoOrden);
        }
        if (campoOrden) {
            uri.addSearch("campo-orden", campoOrden);
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        //let url = this.actionUrl + "/buscar/" + palabrasClaves+'?cantidad=' + cantidad + '&registro=' + filaDesplazamiento;
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para verificar si existe el nombre interno de la insignia
    Recibe: nombre de la insignia
    Retorna: resultado de la transacción (true/false) */
    public verificarNombreInterno(nombreInterno: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/exists/" + nombreInterno;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }


    /***************************************************************/

    /*Método para insertar un nivel de insignia
   Recibe: nivel a insertar
   Retorna: el resultado de la transacción*/
    public insertarNivel(nivel: InsigniaNivel, idInsignia: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idInsignia + "/niveles";
        let NivelJson = JSON.stringify(nivel);
        return this._http.post(url, NivelJson, { headers: this.headers })
            .map((response: Response) => response.text())
            .catch(this.handleError);
    }

    /*Método para actualizar un nivel
    Recibe: el nivel a actualizar
    Retorna: el resultado de la transacción*/
    public actualizarNivel(nivel: InsigniaNivel, idInsignia: string) {
        //this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let NivelJson = JSON.stringify(nivel);
        let url = this.actionUrl + "/" + idInsignia + "/niveles";
        return this._http.put(url, NivelJson, { headers: this.headers })
            .catch(this.handleError);
    }

    /*Método para eliminar un nivel
    Recibe: el id del nivel a eliminar
    Retorna: el resultado de la transacción*/
    public removerNivel(idInsignia: string, idNivel: string) {
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idInsignia + "/niveles/" + idNivel;
        return this._http.delete(url, { headers: this.headers })
            .catch(this.handleError);
    }

    /* Método para verificar si existe el nombre interno de un nivel
    Recibe: nombre de la insignia
    Retorna: resultado de la transacción (true/false) */
    public verificarNivelNombreInterno(nombreInterno: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/niveles/exists/" + nombreInterno;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <boolean>response.json())
            .catch(this.handleError);
    }

    /*Método para buscar una insignia por id
   Recibe: el id de la insignia
   Retorna: la insignia buscado*/
    public obtenerNivelPorId(idInsignia: string, idNivel: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idInsignia + "/niveles/" + idNivel;
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <InsigniaNivel>response.json())
            .catch(this.handleError);
    }

    /*Método para buscar una insignia por id
   Recibe: el id de la insignia
   Retorna: la insignia buscado*/
    public obtenerListaNiveles(idInsignia: string) {
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idInsignia + "/niveles";
        return this._http.get(url, { headers: this.headers })
            .map((response: Response) => <InsigniaNivel[]>response.json())
            .catch(this.handleError);
    }

    /*Método para obtener los miembros asociados a una insignia
    Recibe:
    Retorna*/
    obtenerMiembrosParaInsignia(idInsignia: string, cantidad: number, filaDesplazamiento: number, tipo: string, busqueda?: string){
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'bearer ' + this.token);
        let url = this.actionUrl + "/" + idInsignia + "/miembros";
        let uri = URI(url);
        let estado = "A";
        uri.addSearch("estado", estado);
        uri.addSearch("tipo", tipo);
        if(busqueda){
            uri.addSearch("busqueda", busqueda)
        }
        if (cantidad >= 0) {
            uri.addSearch("cantidad", cantidad);
        }
        if (filaDesplazamiento >= 0) {
            uri.addSearch("registro", filaDesplazamiento);
        }
        return this._http.get(uri.toString(), { headers: this.headers })
            .catch(this.handleError);
    }

    //Método que permite controlar los errores de las transacciones
    public handleError(error: Response) {
        return Observable.throw(error);
    }

}
