import {
    PromocionesDashboardGeneralComponent
} from '../component/promociones/promociones-dashboard-general.component';
import {
    NotificacionDashboardGeneralComponent
} from '../component/notificaciones/notificacion-dashboard-general.component';
import { MisionesDashboardGeneralComponent } from '../component/misiones/misiones-dashboard-general.component';
import { MetricaDashboardGeneralComponent } from '../component/metrica/metrica-dashboard-general.component';
import { PremioDashboardGeneralComponent } from '../component/premio/premio-dashboard-general.component';
import {
    DashboardClienteComponent,
    DashboardComponent,
    DashboardSegmentoComponent,
    DashVacioComponent
} from '../component/dashboards';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';
import {  DashboardRoutingModule} from './dashboard-routing.module';

@NgModule({
    imports: [GeneralModule, DashboardRoutingModule],
    exports: [],
    declarations: [
        //DASHBOARD
        DashboardComponent,
        DashboardClienteComponent,
        DashboardSegmentoComponent,
        DashVacioComponent,
        PremioDashboardGeneralComponent,
        MetricaDashboardGeneralComponent,
        MisionesDashboardGeneralComponent,
        NotificacionDashboardGeneralComponent,
        PromocionesDashboardGeneralComponent
    ],
    providers: [],
})
export class DashboardModule { }
