import { UsuariosRoutingModule } from './usuarios-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';
import * as Componentes from '../component/usuarios/index';

@NgModule({
    imports: [GeneralModule, UsuariosRoutingModule],
    exports: [],
    declarations: [
        Componentes.UsuariosComponent,
        Componentes.RolListaComponent,
        Componentes.RolInsertarComponent,
        Componentes.RolDetalleComponent,
        Componentes.UsuarioInsertarComponent,
        Componentes.UsuarioListaComponent,
        Componentes.UsuarioDetalleComponent,
        Componentes.UsuarioDetalleAtributosComponent,
        Componentes.UsuarioDetalleRolesComponent,
    ],
    providers: [],
})
export class UsuariosModule { }
