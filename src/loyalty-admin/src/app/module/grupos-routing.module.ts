import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import * as Componentes from '../component/grupos/index';
@NgModule({
    imports: [RouterModule.forChild([
        { path: '', component: Componentes.GrupoListaComponent },
        { path: 'insertar', component: Componentes.GrupoInsertarComponent },
        {
            path: 'detalleGrupo/:id', component: Componentes.DetalleComponent,
            children: [
                 { path: '', redirectTo: 'general', pathMatch: 'full' },
                { path: 'general', component: Componentes.GrupoDetalleComponent },
                { path: 'miembros', component: Componentes.GrupoMiembroComponent }
            ]
        },
    ])],
    exports: [RouterModule],
    providers: [],
})
export class GruposRoutingModule { }
