import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';

import * as Componentes from '../component/inventario';
@NgModule({
    imports: [RouterModule.forChild([
       { path: '', redirectTo: 'listaDocumentos', pathMatch: 'full' },
            { path: 'listaDocumentos', component: Componentes.DocumentoListaComponent },
            { 
                path: 'detalleDocumento/:id', component: Componentes.DocumentoInfoComponent, 
                children:[
                    { path: '', redirectTo: 'detalles', pathMatch: 'full' },
                    { path: 'detalles', component: Componentes.DetalleDocumentoComponent }
                ] },
            { path: 'insertarDocumento', component: Componentes.DocumentoInsertarComponent },
            { path: 'proveedor', component: Componentes.ProveedorComponent },
            { path: 'historico', component: Componentes.MovimientoHistoricoComponent}
    ])],
    exports: [RouterModule],
    providers: [],
})
export class InventariosRoutingModule { }
