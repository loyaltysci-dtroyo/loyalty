import { PromocionesRoutingModule } from './promociones-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';

import * as Componentes from '../component/promociones/index';

@NgModule({
    imports: [GeneralModule, PromocionesRoutingModule],
    exports: [],
    declarations: [
        Componentes.PromocionesActividadComponent,
        Componentes.DetallePromoActividadComponent,
        Componentes.PromocionesComponent,
        Componentes.InsertarPromoComponent,
        Componentes.InsertarCategoriaPromoComponent,
        Componentes.ListaCategoriasPromoComponent,
        Componentes.DetallePromoAlcanceComponent,
        Componentes.DetallePromoCommonComponent,
        Componentes.ListaPromoComponent,
        Componentes.DetalleCategoriaComponent,
        Componentes.DetallePromoCategoriasComponent,
        Componentes.DetallePromoArteComponent,
        Componentes.DetallePromoElegiblesComponent,
        Componentes.DetallePromoComponent
    ],
    providers: [],
})
export class PromocionesModule { }
