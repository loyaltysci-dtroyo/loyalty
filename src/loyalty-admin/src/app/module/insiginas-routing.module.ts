import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';

import * as Componentes from '../component/insignias';

@NgModule({
    imports: [RouterModule.forChild([
       { path: '', component: Componentes.InsigniaListaComponent },
            { path: 'insertarInsignia', component: Componentes.InsigniaInsertarComponent },
            {
                path: 'detalleInsignia/:id', component: Componentes.InsigniaDetalleComponent,
                children: [
                    { path: '', redirectTo: 'informacion', pathMatch: 'full' },
                    { path: 'informacion', component: Componentes.InsigniaInformacionComponent },
                    { path: 'listaNivel', component: Componentes.InsigniaNivelListaComponent },
                    { path: 'asignarMiembro', component: Componentes.InsigniaMiembroComponent },
                    { path: 'asignacionAutomatica', component: Componentes.InsigniaDetalleReglasAsignacionComponent }
                ]
            }
    ])],
    exports: [RouterModule],
    providers: [],
})
export class InsigniasRoutingModule { }
