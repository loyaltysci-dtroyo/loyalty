import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import * as Componentes from '../component/reportes/reportes.component';
@NgModule({
    imports: [
        RouterModule.forChild([
            { path: 'reportes', component: Componentes.ReporteComponent, pathMatch: 'full' },
        ])],
    exports: [RouterModule],
    providers: [],
})
export class ReportesRoutingModule { }


