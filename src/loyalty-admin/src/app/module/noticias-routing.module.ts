import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import * as Componentes from '../component/noticias/index';
@NgModule({
    imports: [RouterModule.forChild([
        { path: '', component: Componentes.NoticiaListaComponent },
        { path: 'insertarNoticia', component: Componentes.NoticiaInsertarComponent },
        {
            path: 'detalleNoticia/:id', component: Componentes.NoticiaDetalleComponent,
            children: [
                { path: '', redirectTo: 'general', pathMatch: 'full' },
                { path: 'general', component: Componentes.NoticiaDetalleGeneralComponent },
                { path: 'audiencia', component: Componentes.NoticiaSegmentoElegiblesComponent }
            ]
        },
        { path: 'listaCategoriasNoticia', component: Componentes.CategoriaNoticiaListaComponent },
        { path: 'detalleCategoriaNoticia/:id', component: Componentes.CategoriaNoticiaDetalleComponent },
    ])],
    exports: [RouterModule],
    providers: [],
})
export class NoticiasRoutingModule { }
