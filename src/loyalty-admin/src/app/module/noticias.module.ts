import { NoticiasRoutingModule } from './noticias-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import * as Componentes from '../component/noticias/index';

@NgModule({
    imports: [GeneralModule, NoticiasRoutingModule, InfiniteScrollModule],
    exports: [],
    declarations: [
        Componentes.NoticiaComponent,
        Componentes.NoticiaListaComponent,
        Componentes.NoticiaInsertarComponent,
        Componentes.NoticiaDetalleComponent,
        Componentes.NoticiaDetalleGeneralComponent,
        Componentes.NoticiaSegmentoElegiblesComponent,
        Componentes.CategoriaNoticiaListaComponent,
        Componentes.CategoriaNoticiaDetalleComponent,
        Componentes.NoticiaComentarioComponent
    ],
    providers: [],
})
export class NoticiasModule {

 }
