import { SegmentosRoutingModule } from './segmentos-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import * as Componentes from '../component/segmentos';
@NgModule({
    imports: [CommonModule, GeneralModule, SegmentosRoutingModule],
    exports: [],
    declarations: [
        Componentes.SegmentosComponent, 
        Componentes.DetalleAtributosSegmentoComponent,
        Componentes.SegmentoDetalleReglasComponent,
        Componentes.SegmentosDetalleMiembros,
        Componentes.SegmentosListaReglasComponent,
        Componentes.DetalleSegmentoComponent,
        Componentes.ListaSegmentosComponent,
        Componentes.InsertarSegmentoComponent,
        Componentes.SegmentosDetalleElegiblesComponent,
        Componentes.SegmentoInsertarReglaEncuestaComponent,
        Componentes.SegmentosInsertarReglaPreferenciaComponent,
        Componentes.SegmentoImportExportComponent
     ],
    providers: [],
})
export class SegmentosModule { }
