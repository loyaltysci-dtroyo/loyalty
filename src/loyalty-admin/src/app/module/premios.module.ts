import { PremiosRoutingModule } from './premios-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';

import * as Componentes from '../component/premio';

@NgModule({
    imports: [GeneralModule, PremiosRoutingModule],
    exports: [],
    declarations: [
        Componentes.PremioComponent,
        Componentes.PremioListaComponent,
        Componentes.PremioInsertarComponent,
        Componentes.PremioDetalleComponent,
        Componentes.PremioInformacionComponent,
        Componentes.PremioDisplayComponent,
        Componentes.PremioElegiblesComponent,
        Componentes.PremioLimiteComponent,
        Componentes.PremioCategoriaComponent,
        Componentes.PremioMiembroElegiblesComponent,
        Componentes.PremioSegmentoElegiblesComponent,
        Componentes.CategoriaPremioListaComponent,
        Componentes.CategoriaPremioInsertarComponent,
        Componentes.CategoriaPremioDetalleComponent,
        Componentes.CategoriaPremioAsignarComponent,
        Componentes.CategoriaPremioComponent,
        Componentes.PremioCertificadoComponent,
        Componentes.UnidadMedidaComponent,
        Componentes.PremiosActividadComponent,
        Componentes.PremioDetalleActividadComponent,
        Componentes.PremioDetalleDashboardComponent,
        Componentes.PremiosDetalleReglasAsignacionComponent
    ],
    providers: [],
})
export class PremiosModule { }
