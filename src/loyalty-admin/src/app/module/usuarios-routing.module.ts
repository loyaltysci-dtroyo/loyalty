import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import * as Componentes from '../component/usuarios/index';
@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', redirectTo: 'listaUsuarios', pathMatch: 'full' },
            { path: 'insertarUsuario', component: Componentes.UsuarioInsertarComponent, },
            { path: 'listaUsuarios', component: Componentes.UsuarioListaComponent },
            {
                path: 'detalleUsuario/:id', component: Componentes.UsuarioDetalleComponent,
                children: [
                    { path: '', redirectTo: 'detalleAtributos', pathMatch: 'full' },
                    { path: 'detalleAtributos', component: Componentes.UsuarioDetalleAtributosComponent },
                    { path: 'detalleRoles', component: Componentes.UsuarioDetalleRolesComponent }
                ]
            },
            { path: 'insertarRoles', component: Componentes.RolInsertarComponent },
            { path: 'listaRoles', component: Componentes.RolListaComponent },
            { path: 'detalleRol/:id', component: Componentes.RolDetalleComponent }
        ])],
    exports: [RouterModule],
    providers: [],
})
export class UsuariosRoutingModule { }


