import { GruposRoutingModule } from './grupos-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';

import * as Componentes from '../component/grupos/index';

@NgModule({
    imports: [GeneralModule, GruposRoutingModule],
    exports: [],
    declarations: [
        //GRUPOS
        Componentes.GrupoComponent,
        Componentes.GrupoInsertarComponent,
        Componentes.GrupoListaComponent,
        Componentes.GrupoDetalleComponent,
        Componentes.GrupoMiembroComponent,
        Componentes.DetalleComponent,
    ],
    providers: [],
})
export class GruposModule {

 }
