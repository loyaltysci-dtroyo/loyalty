import { ReportesRoutingModule } from './reportes-routing.module';
import { GeneralModule } from './general.module';
import { OtrosModule } from './otros.module';
import { NgModule } from '@angular/core';
import * as Componentes from '../component/reportes/reportes.component';
import {CommonModule} from '@angular/common';
@NgModule({
    imports: [GeneralModule, ReportesRoutingModule, OtrosModule],
    exports: [],
    declarations: [
        Componentes.ReporteComponent
    ],
    providers: [],
})
export class ReportesModule { }
