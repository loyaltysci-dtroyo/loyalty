import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import * as Components from '../component/metrica/index';
import {GrupoNivelDetalleComponent} from '../component/nivel/grupo-nivel-detalle.component';
import {GrupoNivelListaComponent} from '../component/nivel/grupo-nivel-lista.component';

@NgModule({
     imports: [RouterModule.forChild([
        { path: '', redirectTo: 'metricaLista', pathMatch: 'full' },
            { path: 'insertarMetrica', component: Components.MetricaInsertarComponent },
            { path: 'metricaLista', component: Components.MetricaListaComponent },
            { path: 'metricaDetalle/:id', component: Components.MetricaDetalleGeneralComponent, children:[
                { path: '', component: Components.MetricaDetalleComponent },
                { path: 'general', component: Components.MetricaDetalleComponent },
                { path: 'actividad', component: Components.MetricaActividadComponent },
                { path: 'dashboard', component: Components.MetricaDashboardComponent }
            ] },
            {
                path: 'gruposNiveles',
                children: [
                    { path: '', component: GrupoNivelListaComponent },
                    { path: 'detalle/:id', component: GrupoNivelDetalleComponent },
                ]
            },
    ])],
    exports: [RouterModule],
    providers: [],
})
export class MetricasRoutingModule { }
