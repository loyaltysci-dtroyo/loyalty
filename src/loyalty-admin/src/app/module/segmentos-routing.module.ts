import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import * as Componentes from '../component/segmentos';
@NgModule({
     imports: [RouterModule.forChild([
       { path: '', redirectTo: 'listaSegmentos', pathMatch: 'full' },
            { path: 'insertarSegmento', component: Componentes.InsertarSegmentoComponent },
            { path: 'listaSegmentos', component: Componentes.ListaSegmentosComponent },
            {
                path: 'detalleSegmento/:id', component: Componentes.DetalleSegmentoComponent,
                children: [
                    { path: '', redirectTo: 'atributos', pathMatch: 'full' },
                    { path: 'atributos', component: Componentes.DetalleAtributosSegmentoComponent },
                    { path: 'reglas', component: Componentes.SegmentosListaReglasComponent },
                    { path: 'detalleReglas/:id', component: Componentes.SegmentoDetalleReglasComponent },
                    { path: 'miembros', component: Componentes.SegmentosDetalleMiembros },
                    { path: 'elegibles', component: Componentes.SegmentosDetalleElegiblesComponent },
                    { path: 'import-export', component: Componentes.SegmentoImportExportComponent}
                ]
            }
    ])],
    exports: [RouterModule],
    providers: [],
})
export class SegmentosRoutingModule { }
