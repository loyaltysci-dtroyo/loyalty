import { InsigniasRoutingModule } from './insiginas-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';

import * as Componentes from '../component/insignias';

@NgModule({
    imports: [GeneralModule, InsigniasRoutingModule],
    exports: [],
    declarations: [
        Componentes.InsigniaComponent,
        Componentes.InsigniaListaComponent,
        Componentes.InsigniaInsertarComponent,
        Componentes.InsigniaDetalleComponent,
        Componentes.InsigniaNivelListaComponent,
        Componentes.InsigniaMiembroComponent,
        Componentes.InsigniaInformacionComponent,
        Componentes.InsigniaDetalleReglasAsignacionComponent
    ],
    providers: [],
})
export class InsigniasModule { }
