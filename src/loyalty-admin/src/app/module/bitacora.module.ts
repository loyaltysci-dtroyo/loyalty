import { NgModule } from '@angular/core';
import { GeneralModule } from './general.module';
import { BitacoraRoutingModule} from './bitacora-routing.module'
import * as Componentes from '../component/bitacoras/index';
@NgModule({
    imports: [GeneralModule,BitacoraRoutingModule],
    exports: [],
    declarations: [
        Componentes.BitacoraComponent,
        Componentes.BitacoraApiExternalComponent,
        Componentes.BitacoraApiAdministrativaComponent
    ],
    providers: [],
}) 
export class BitacoraModule { }
 