import { OtrosModule } from './otros.module';
import { MiembrosRoutingModule } from './miembros-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';
import { PreferenciasInsertarComponent } from '../component/miembros/preferencias/preferencias-insertar.component';
import { DetallePreferenciaComponent } from '../component/miembros/preferencias/preferencias-detalle.component';
import { PreferenciasListaComponent } from '../component/miembros/preferencias/preferencias-lista.component';
import {  SeleccionPromocionesComponent,SeleccionMiembrosComponent,SeleccionMetricaComponent,SeleccionSegmentosComponent,SeleccionUbicacionesComponent } from '../component/common/';
import * as ComponentesMiembro from '../component/miembros/index';
import {
    AtributoComponent, AtributoListaComponent, AtributoInsertarComponent,
    AtributoDetalleComponent
} from '../component/atributos/index';
@NgModule({
    imports: [GeneralModule, MiembrosRoutingModule, OtrosModule ],
    exports: [],
    declarations: [
        AtributoComponent,
        AtributoListaComponent,
        AtributoInsertarComponent,
        AtributoDetalleComponent,
        PreferenciasInsertarComponent,
        DetallePreferenciaComponent,
        ComponentesMiembro.MiembroActividadNotificacionesComponent,
        ComponentesMiembro.MiembroPreferenciaComponent,
        ComponentesMiembro.MiembrosComponent,
        ComponentesMiembro.MiembroListaComponent,
        ComponentesMiembro.DetallePreferenciaComponent,
        ComponentesMiembro.MiembroActividadAwardsComponent,
        ComponentesMiembro.MiembroActividadMetricaComponent,
        ComponentesMiembro.MiembroActividadMisionesComponent,
        ComponentesMiembro.MiembroActividadPremiosComponent,
        ComponentesMiembro.MiembroActividadPromocionesComponent,
        ComponentesMiembro.MiembroAtributoComponent,
        ComponentesMiembro.MiembroDetalleComponent,
        ComponentesMiembro.MiembroEditarComponent,
        ComponentesMiembro.MiembroGrupoComponent,
        ComponentesMiembro.MiembroInsertarComponent,
        ComponentesMiembro.MiembroInsigniaComponent,
        ComponentesMiembro.MiembroListaComponent,
        ComponentesMiembro.MiembroNivelComponent,
        ComponentesMiembro.MiembroPerfilComponent,
        ComponentesMiembro.MiembroPreferenciaComponent,
        ComponentesMiembro.MiembrosComponent,
        ComponentesMiembro.MiembroSegmentoComponent,
        ComponentesMiembro.PreferenciasInsertarComponent,
        ComponentesMiembro.PreferenciasListaComponent,
        ComponentesMiembro.MiembroImportacionComponent,
        ComponentesMiembro.MiembroComprasComponent,
        ComponentesMiembro.MiembroReferalsComponent,
        ComponentesMiembro.MiembroTransaccionesComponent],
    providers: [],
})
export class MiembrosModule { }
