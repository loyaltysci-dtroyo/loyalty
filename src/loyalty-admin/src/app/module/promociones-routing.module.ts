import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import * as Componentes from '../component/promociones/index';
@NgModule({
    imports: [RouterModule.forChild([
        { path: '', redirectTo: 'listaPromos', pathMatch: 'full' },
        { path: 'actividad', component: Componentes.PromocionesActividadComponent },
        { path: 'listaPromos', component: Componentes.ListaPromoComponent },
        { path: 'insertarPromo', component: Componentes.InsertarPromoComponent },
        { path: 'insertarCategoria', component: Componentes.InsertarCategoriaPromoComponent },
        { path: 'listaCategorias', component: Componentes.ListaCategoriasPromoComponent },
        { path: 'detalleCategorias/:id', component: Componentes.DetalleCategoriaComponent },
        {
            path: 'detallePromo/:id', component: Componentes.DetallePromoComponent,
            children: [
                { path: '', redirectTo: 'common', pathMatch: 'full' },
                { path: 'actividad', component: Componentes.DetallePromoActividadComponent },
                { path: 'common', component: Componentes.DetallePromoCommonComponent },
                { path: 'arte', component: Componentes.DetallePromoArteComponent },
                { path: 'alcance', component: Componentes.DetallePromoAlcanceComponent },
                { path: 'elegibles', component: Componentes.DetallePromoElegiblesComponent },
                { path: 'categorias', component: Componentes.DetallePromoCategoriasComponent }
            ]
        },
    ])],
    exports: [RouterModule],
    providers: [],
})
export class PromocionesRoutingModule { }
