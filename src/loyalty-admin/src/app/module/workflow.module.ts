import { WorkflowRoutingModule } from './workflow-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';

import * as Component from '../component/workflows/index';

@NgModule({
    imports: [GeneralModule, WorkflowRoutingModule],
    exports: [],
    declarations: [
        Component.WorkflowComponent,
        Component.WorkflowDetalleComponent,
        Component.WorkflowListaComponent,
        Component.WorkflowDetalleCommonComponent,
        Component.WorkflowDetalleDefinicionComponent,
        Component.WorkflowInsertarComponent,
        Component.WorkflowDetalleElegiblesComponent,
        Component.WorkflowDetalleMiembrosComponent,
        Component.WorkflowDetalleSegmentosComponent,
        Component.WorkflowDetalleUbicacionesComponent,
    ],
    providers: [],
})
export class WorkflowModule { }
