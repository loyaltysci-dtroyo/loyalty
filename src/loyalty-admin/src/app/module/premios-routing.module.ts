import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import * as Componentes from '../component/premio';
@NgModule({
    imports: [RouterModule.forChild([
        { path: '', component: Componentes.PremioListaComponent },
        { path: 'actividad', component: Componentes.PremiosActividadComponent },
        { path: 'insertarPremio', component: Componentes.PremioInsertarComponent },
        {
            path: 'detallePremio/:id', component: Componentes.PremioInformacionComponent,
            children: [
                { path: '', redirectTo: 'requerido', pathMatch: 'full' },
                { path: 'dashboard', component: Componentes.PremioDetalleDashboardComponent },
                { path: 'actividad', component: Componentes.PremioDetalleActividadComponent },
                { path: 'requerido', component: Componentes.PremioDetalleComponent },
                { path: 'display', component: Componentes.PremioDisplayComponent },
                { path: 'elegibles', component: Componentes.PremioElegiblesComponent },
                { path: 'limite', component: Componentes.PremioLimiteComponent },
                { path: 'categorias', component: Componentes.PremioCategoriaComponent },
                { path: 'miembros', component: Componentes.PremioMiembroElegiblesComponent },
                { path: 'segmentos', component: Componentes.PremioSegmentoElegiblesComponent },
                { path: 'certificados', component: Componentes.PremioCertificadoComponent },
                { path: 'reglasAsignacion', component: Componentes.PremiosDetalleReglasAsignacionComponent }
            ]
        }, 
        { path: 'listaCategoriasPremio', component: Componentes.CategoriaPremioListaComponent },
        { path: 'insertarCategoria', component: Componentes.CategoriaPremioInsertarComponent },
        {
            path: 'detalleCategoria/:id', component: Componentes.CategoriaPremioComponent,
            children: [
                { path: '', redirectTo: 'informacion', pathMatch: 'full' },
                { path: 'informacion', component: Componentes.CategoriaPremioDetalleComponent },
                { path: 'asignar', component: Componentes.CategoriaPremioAsignarComponent },
            ]
        },
        { path: 'unidadMedida', component: Componentes.UnidadMedidaComponent }
    ])],
    exports: [RouterModule],
    providers: [],
})
export class PremiosRoutingModule { }
