import {
    PromocionesDashboardGeneralComponent
} from '../component/promociones/promociones-dashboard-general.component';
import {
    NotificacionDashboardGeneralComponent
} from '../component/notificaciones/notificacion-dashboard-general.component';
import { MisionesDashboardGeneralComponent } from '../component/misiones/misiones-dashboard-general.component';
import { MetricaDashboardGeneralComponent } from '../component/metrica/metrica-dashboard-general.component';
import { PremioDashboardGeneralComponent } from '../component/premio/premio-dashboard-general.component';
import { DashboardClienteComponent, DashboardSegmentoComponent, DashVacioComponent } from '../component/dashboards';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import * as Componentes from '../component/configuracion/';
@NgModule({
    imports: [RouterModule.forChild([
          { path: '', redirectTo: 'miembros', pathMatch: 'full' },
            { path: 'miembros', component: DashboardClienteComponent },
            { path: 'segmentos', component: DashboardSegmentoComponent },
            { path: 'premios', component: PremioDashboardGeneralComponent },
            { path: 'metricas', component: MetricaDashboardGeneralComponent },
            { path: 'misiones', component: MisionesDashboardGeneralComponent },
            { path: 'notificaciones', component: NotificacionDashboardGeneralComponent },
            { path: 'promociones', component: PromocionesDashboardGeneralComponent },
            { path: 'vacio', component: DashVacioComponent }      
    ])],
    exports: [RouterModule],
    providers: [],
})
export class DashboardRoutingModule { }
