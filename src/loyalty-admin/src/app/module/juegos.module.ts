import { JuegosRoutingModule } from './juegos-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';
import * as Componentes from '../component/juegos';
@NgModule({
    imports: [GeneralModule, JuegosRoutingModule],
    exports: [],
    declarations: [
        Componentes.QuienQuiereSerMillonarioComponent,
        Componentes.QuienQuiereSerMillonarioListaComponent,
        Componentes.QuienQuiereSerMillonarioInsertarComponent,
        Componentes.QuienQuiereSerMillonarioDetalleComponent,
        Componentes.QuienQuiereSerMillonarioDetalleCommonComponent,
        Componentes.PreguntasMillonarioListaComponent,
        Componentes.PreguntasMillonarioDetalleComponent
    ],
    providers: [],
})
export class JuegosModule {
    
}
