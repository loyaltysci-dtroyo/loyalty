import { TierPipe } from '../pipes/tier.pipe';
import { ReglaAsignacionTipoPipe, ReglaAsignacionBadgeTipoPipe } from '../pipes/index';
import { MisionesModule } from './misiones.module';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    BreadcrumbModule, ButtonModule, CalendarModule, ChartModule, RatingModule,
    CheckboxModule, ConfirmDialogModule, SharedModule, DataGridModule, DataListModule,
    DataScrollerModule, DataTableModule, DialogModule, DropdownModule, EditorModule, FieldsetModule,
    GrowlModule, InputMaskModule, InputSwitchModule, InputTextModule, TriStateCheckboxModule,
    InputTextareaModule, ListboxModule, MessagesModule, OrderListModule,MultiSelectModule, OverlayPanelModule,
    PaginatorModule, PanelModule, PanelMenuModule, PasswordModule,PickListModule, ProgressBarModule, RadioButtonModule, AutoCompleteModule,
    ScheduleModule, SelectButtonModule,SliderModule, SpinnerModule, SplitButtonModule, TabMenuModule, TabViewModule,ToggleButtonModule, ChipsModule
} from 'primeng/primeng';
import{BrowserModule} from '@angular/platform-browser';
import { ControlMessagesComponent } from '../utils/index';

// pipes
import {
    IndOperadorPipe,EstadoSegmentoPipe, IndBalanceMetricaPipe, TipoPremioPipe,ReglaAsignacionIndCantidadPipe,
    TipoSegmentoPipe, TipoLeaderboardPipe,IndicadorReglaPeriodoPipe, EventoNotificacionPipe, ReglaAvanzadaIndUltimoPipe,
    ReglaTipoCambioPipe, StatusTrabajoSegmentoPipe,EstadoUsuarioPipe, TipoRespuestaPipe, EstadoElementoPipe, DocumentoTipoPipe,
    ReglaIndicadorAtributoPipe, EventoCampanaPipe, ReglaIndBalancePipe, DocumentoEstadoPipe, ReglaAsignacionIndUltimoPipe, MisionIndicadorAtributoPipe,
    ReglaIndicadorOperadorPipe, EstadoNotificacionPipe, TipoNotificacionPipe, EstadoRedencionPipe, EstadoTrabajoInternoPipe, TipoTrabajoInternoPipe,
    DatoBoleanPipe, CambioEstadoPipe, VisibilidadPipe, TipoDatoPipe, ReglaAvanzadaPipe, TipoJuegoMisionPipe, RespuestaUnicaPipe, RespuestaCorrectaPipe,
    CalendarizacionPipe, TipoInsigniaPipe, CertificadoEstadoPipe, FechaAtributoPipe, MiembroPaisPipe, TipoPreguntaPipe, RespuestaMultiplePipe,
    WorkflowTipoTareaPipe, WorkflowEstadoPipe, MetricaTipoMedidaPipe, EstadoMiembroPipe, EducacionMiembroPipe, EstadoCivilMiembroPipe, GeneroMiembroPipe, 
    TipoPremioJuegoPipe, MisionTipoRedSocialPipe, BitacoraAccionPipe, PromoTipoAccionPipe, ReglaValorComparacionPipe
} from '../pipes/index';

import { CommonModule } from '@angular/common';

@NgModule({
    imports: [FormsModule, ReactiveFormsModule, RouterModule, CommonModule,HttpModule],
    exports: [
        //modulos propios de angular
        FormsModule, ReactiveFormsModule, RouterModule, HttpModule,CommonModule,
        //modulos de PrimeNG
        BreadcrumbModule, ButtonModule, CalendarModule, AutoCompleteModule,TierPipe,
        ChartModule, OrderListModule, CheckboxModule, RatingModule, ChipsModule, ConfirmDialogModule, SharedModule, DataGridModule,
        DataListModule, DataScrollerModule, DataTableModule, DialogModule, DropdownModule, EditorModule, FieldsetModule, GrowlModule, TriStateCheckboxModule,
        InputMaskModule, InputSwitchModule, InputTextModule, InputTextareaModule, ListboxModule, MessagesModule, MultiSelectModule, OverlayPanelModule, PaginatorModule,
        PanelModule, PasswordModule, PickListModule, ProgressBarModule, RadioButtonModule, SelectButtonModule, SliderModule, SpinnerModule,
        SplitButtonModule, TabMenuModule, TabViewModule, ToggleButtonModule,
        //pipes (se emplean a traves de todos los componentes de la aplicacion)
        IndOperadorPipe, TipoSegmentoPipe, EstadoMiembroPipe, MiembroPaisPipe, EducacionMiembroPipe,ReglaAsignacionTipoPipe, EstadoCivilMiembroPipe, GeneroMiembroPipe,ReglaAsignacionBadgeTipoPipe,
        WorkflowTipoTareaPipe, WorkflowEstadoPipe, TipoRespuestaPipe, IndBalanceMetricaPipe , ReglaAvanzadaPipe, ReglaAvanzadaIndUltimoPipe,EstadoSegmentoPipe,
        EstadoNotificacionPipe, TipoInsigniaPipe, EstadoUsuarioPipe, TipoLeaderboardPipe, EventoCampanaPipe, ReglaIndBalancePipe, ReglaIndicadorAtributoPipe,
        TipoNotificacionPipe, EventoNotificacionPipe, StatusTrabajoSegmentoPipe, EstadoRedencionPipe, RespuestaUnicaPipe, RespuestaMultiplePipe, RespuestaCorrectaPipe,
        DatoBoleanPipe, CambioEstadoPipe, VisibilidadPipe, TipoDatoPipe, CalendarizacionPipe, CertificadoEstadoPipe, EstadoElementoPipe, FechaAtributoPipe,
        MetricaTipoMedidaPipe, TipoPremioPipe,DocumentoTipoPipe, TipoPremioJuegoPipe, TipoJuegoMisionPipe, ReglaAsignacionIndUltimoPipe, MisionTipoRedSocialPipe,
        DocumentoEstadoPipe,EstadoSegmentoPipe,ReglaAvanzadaPipe,ReglaAvanzadaIndUltimoPipe,ReglaIndicadorOperadorPipe,ReglaTipoCambioPipe,IndicadorReglaPeriodoPipe,
        ControlMessagesComponent,ReglaAsignacionIndCantidadPipe, EstadoTrabajoInternoPipe, TipoTrabajoInternoPipe, TipoPreguntaPipe, MisionIndicadorAtributoPipe, 
        BitacoraAccionPipe, PromoTipoAccionPipe, ReglaValorComparacionPipe
    ],
    declarations: [
       IndOperadorPipe, TipoSegmentoPipe, EstadoMiembroPipe, MiembroPaisPipe, EducacionMiembroPipe, EstadoCivilMiembroPipe, GeneroMiembroPipe,
        WorkflowTipoTareaPipe, WorkflowEstadoPipe, TipoRespuestaPipe, IndBalanceMetricaPipe, ReglaAvanzadaPipe, ReglaAvanzadaIndUltimoPipe,EstadoSegmentoPipe,
        EstadoNotificacionPipe, TipoInsigniaPipe, EstadoUsuarioPipe, TipoLeaderboardPipe, EventoCampanaPipe, ReglaIndBalancePipe, ReglaIndicadorAtributoPipe,
        TipoNotificacionPipe, EventoNotificacionPipe, StatusTrabajoSegmentoPipe, EstadoRedencionPipe, ReglaAsignacionIndUltimoPipe, ReglaAsignacionBadgeTipoPipe,
        DatoBoleanPipe, CambioEstadoPipe, VisibilidadPipe, TipoDatoPipe, CalendarizacionPipe, CertificadoEstadoPipe, EstadoElementoPipe, FechaAtributoPipe,
        MetricaTipoMedidaPipe, TipoPremioPipe,DocumentoTipoPipe, TipoPremioJuegoPipe, TipoJuegoMisionPipe,ReglaAsignacionTipoPipe, MisionIndicadorAtributoPipe,
        DocumentoEstadoPipe,EstadoSegmentoPipe,ReglaAvanzadaPipe,ReglaAvanzadaIndUltimoPipe,ReglaIndicadorOperadorPipe,ReglaTipoCambioPipe,IndicadorReglaPeriodoPipe,
        ReglaAsignacionIndCantidadPipe, EstadoTrabajoInternoPipe, TipoTrabajoInternoPipe, TipoPreguntaPipe, RespuestaUnicaPipe, RespuestaMultiplePipe, RespuestaCorrectaPipe,
        ControlMessagesComponent, MisionTipoRedSocialPipe, TierPipe, BitacoraAccionPipe, PromoTipoAccionPipe, ReglaValorComparacionPipe
    ],
    providers: [],
})
export class GeneralModule { }
