import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import * as Componentes from '../component/notificaciones';
@NgModule({
    imports: [RouterModule.forChild([
        { path: '', redirectTo: 'listaNotificaciones', pathMatch: 'full' },
        { path: 'listaNotificaciones', component: Componentes.NotificacionesListaComponent },
        { path: 'actividad', component: Componentes.NotificacionActividadComponent },
        { path: 'insertarNotificaciones', component: Componentes.NotificacionesInsertarComponent },
        {
            path: 'detalleNotificacion/:id', component: Componentes.NotificacionesDetalleComponent,
            children: [
                { path: '', redirectTo: 'common', pathMatch: 'full' }, 
                { path: 'dashboard', component: Componentes.NotificacionDetalleDashboardComponent },
                { path: 'actividad', component: Componentes.NotificacionDetalleActividadComponent },
                { path: 'common', component: Componentes.NotificacionesDetalleAtributosComponent },
                { path: 'arte', component: Componentes.NotificacionesDetalleArteComponent },
                { path: 'miembros', component: Componentes.NotificacionesDetalleMiembrosComponent },
                { path: 'segmentos', component: Componentes.NotificacionesDetalleSegmentoComponent }
            ]
        }
    ])],
    exports: [RouterModule],
    providers: [],
})
export class NotificacionesRoutingModule { }
