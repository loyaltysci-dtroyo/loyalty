import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import * as Componentes from '../component/configuracion/';
@NgModule({
    imports: [RouterModule.forChild([
        { path: '', redirectTo: 'general', pathMatch: 'full' },
                { path: 'general', component: Componentes.ConfiguracionGeneralComponent},
                { path: 'medios', component: Componentes.ConfiguracionMediosComponent },
                { path: 'politica', component: Componentes.ConfiguracionPoliticaComponent },
                { path: 'parametros', component: Componentes.ConfiguracionParamsComponent },
                { path: 'evertec', component: Componentes.ConfiguracionEvertecComponent }
    ])],
    exports: [RouterModule],
    providers: [],
})
export class ConfigRoutingModule { }
