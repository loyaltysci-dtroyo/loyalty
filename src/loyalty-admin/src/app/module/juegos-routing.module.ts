import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import * as Componentes from '../component/juegos';
@NgModule({
    imports: [RouterModule.forChild([
        { path: '', redirectTo: 'listaJuegosMillonario', pathMatch: 'full' },
        { path: 'listaJuegosMillonario', component: Componentes.QuienQuiereSerMillonarioListaComponent },
        { path: 'insertarJuegoMillonario', component: Componentes.QuienQuiereSerMillonarioInsertarComponent },
        { path: 'detallePregunta/:id', component: Componentes.PreguntasMillonarioDetalleComponent },
        {
            path: 'detalleJuegoMillonario/:id', component: Componentes.QuienQuiereSerMillonarioDetalleComponent,
            children: [
                { path: '', redirectTo: 'common', pathMatch: 'full' },
                { path: 'common', component: Componentes.QuienQuiereSerMillonarioDetalleCommonComponent },
                { path: 'preguntas', component: Componentes.PreguntasMillonarioListaComponent }
            ]
        },
    ])],
    exports: [RouterModule],
    providers: [],
})
export class JuegosRoutingModule { }