import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import * as Compoentes from '../component/leaderboards/'
@NgModule({
     imports: [RouterModule.forChild([
         { path: '', redirectTo: 'listaLeaderboards', pathMatch: 'full' },
            { path: 'listaLeaderboards', component: Compoentes.LeaderboardListaComponent },
            { path: 'detalleLeaderboard/:id', component: Compoentes.LeaderboardDetalleComponent },
            { path: 'insertarLeaderboards', component: Compoentes.LeaderboardInsertarComponent }
    ])],
    exports: [RouterModule],
    providers: [],
})
export class LeaderboardsRoutingModule { }
