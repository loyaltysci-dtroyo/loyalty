import { OtrosModule } from './otros.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';
import { ConfigRoutingModule} from './config-routing.module'
import * as Componentes from '../component/configuracion/';
@NgModule({
    imports: [GeneralModule, ConfigRoutingModule, OtrosModule],
    exports: [],
    declarations: [
           //CONFIGURACION GENERAL
        Componentes.ConfiguracionComponent,
        Componentes.ConfiguracionGeneralComponent,
        Componentes.ConfiguracionMediosComponent,
        Componentes.ConfiguracionPoliticaComponent,
        Componentes.ConfiguracionParamsComponent,
        Componentes.ConfiguracionEvertecComponent
    ],
    providers: [],
})
export class ConfigModule { }
