import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import * as Componentes from '../component/ubicaciones';
@NgModule({
    imports: [RouterModule.forChild([
        { path: '', redirectTo: 'listaUbicaciones', pathMatch: 'full' },
        { path: 'listaUbicaciones', component: Componentes.UbicacionListaComponent },
        { path: 'insertarUbicacion', component: Componentes.UbicacionInsertarComponent },
        {
            path: 'detalleUbicacion/:id', component: Componentes.UbicacionDetalleComponent,
            children: [
                { path: '', redirectTo: 'informacion', pathMatch: 'full' },
                { path: 'informacion', component: Componentes.UbicacionInformacionComponent },
                { path: 'efectividad', component: Componentes.UbicacionEfectividadComponent },
                { path: 'mapa', component: Componentes.UbicacionMapaComponent },
                { path: 'beacons', component: Componentes.BeaconComponent }
            ]
        }
    ])],
    exports: [RouterModule],
    providers: [],
})
export class UbicacionesRoutingModule { }
