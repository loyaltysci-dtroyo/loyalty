import { MetricasRoutingModule } from './metricas-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';
import * as Components from '../component/metrica/index';
import {GrupoNivelDetalleComponent} from '../component/nivel/grupo-nivel-detalle.component';
import {GrupoNivelListaComponent} from '../component/nivel/grupo-nivel-lista.component';

@NgModule({
    imports: [GeneralModule, MetricasRoutingModule],
    exports: [],
    declarations: [
        Components.MetricaDashboardComponent,
        Components.MetricaInsertarComponent,
        Components.MetricaListaComponent,
        Components.MetricaDetalleComponent,
        Components.MetricasComponent,
        Components.MetricaActividadComponent,
        Components.MetricaDetalleGeneralComponent,
        GrupoNivelListaComponent,
        GrupoNivelDetalleComponent
    ],
    providers: [],
})
export class MetricasModule { }
