import { MisionesRoutingModule } from './misiones-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';
import * as Componentes from '../component/misiones';
@NgModule({
    imports: [GeneralModule, MisionesRoutingModule],
    exports: [],
    declarations: [
        Componentes.MisionesComponent,
        Componentes.DetalleCategoriaMisionComponent,
        Componentes.InsertarCategoriaMisionComponent,
        Componentes.ListaCategoriasMisionComponent,
        Componentes.MisionesActividadSocialComponent,
        Componentes.MisionesContenidoComponent,
        Componentes.MisionesDetalleArteComponent,
        Componentes.MisionesDetalleCategoriasComponent,
        Componentes.MisionesDetalleCommmonComponent,
        Componentes.MisionesDetalleComponent,
        Componentes.MisionesDetalleElegiblesComponent,
        Componentes.MisionesDetalleLimitesComponent,
        Componentes.MisionesDetalleMiembrosComponent,
        Componentes.MisionesDetalleRetoComponent,
        Componentes.MisionesDetalleSegmentosComponent,
        Componentes.MisionesDetalleUbicacionesComponent,
        Componentes.MisionesEncuestaComponent,
        Componentes.MisionesListaComponent,
        Componentes.MisionesInsertarComponent,
        Componentes.MisionesSubirContenidoComponent,
        Componentes.MisionesDetalleAprobacionComponent,
        Componentes.MisionesPerfilComponent,
        Componentes.MisionesDetalleActividadComponent, 
        Componentes.MisionesActividadComponent,
        Componentes.MisionesJuegoComponent,
        Componentes.MisionesDetalleDashboardComponent,
        Componentes.MisionesPreferenciaComponent,
        Componentes.MisionesRealidadAumentada
        ],
    providers: [],
})
export class MisionesModule {

}
