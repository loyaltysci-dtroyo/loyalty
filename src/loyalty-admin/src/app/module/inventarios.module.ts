import { InventariosRoutingModule } from './inventarios-routing.module';
import { GeneralModule } from './general.module';
import { OtrosModule } from './otros.module';
import { NgModule } from '@angular/core';

import * as Componentes from '../component/inventario';
@NgModule({
    imports: [GeneralModule, InventariosRoutingModule, OtrosModule],
    exports: [],
    declarations: [
        Componentes.InventarioComponent,
        Componentes.DocumentoListaComponent,
        Componentes.DocumentoInfoComponent,
        Componentes.DocumentoInsertarComponent,
        Componentes.ProveedorComponent,
        Componentes.DetalleDocumentoComponent,
        Componentes.MovimientoHistoricoComponent
       
    ],
    providers: [],
})
export class InventariosModule { }
