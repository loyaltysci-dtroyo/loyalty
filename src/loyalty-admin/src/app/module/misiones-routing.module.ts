import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import * as Componentes from '../component/misiones';
@NgModule({
    imports: [RouterModule.forChild([
       { path: '', redirectTo: 'listaMisiones', pathMatch: 'full' },
            { path: 'insertarMision', component: Componentes.MisionesInsertarComponent },
            { path: 'actividad', component: Componentes.MisionesActividadComponent },
            { path: 'listaMisiones', component: Componentes.MisionesListaComponent },
            { path: 'aprobacion/:id', component: Componentes.MisionesDetalleAprobacionComponent },
            { path: 'insertarCategoria', component: Componentes.InsertarCategoriaMisionComponent },
            { path: 'listaCategorias', component: Componentes.ListaCategoriasMisionComponent },
            { path: 'detalleCategorias/:id', component: Componentes.DetalleCategoriaMisionComponent },
            {
                path: 'detalleMision/:id', component: Componentes.MisionesDetalleComponent,
                children: [
                    { path: '', redirectTo: 'common', pathMatch: 'full' },
                    { path: 'dashboard', component: Componentes.MisionesDetalleDashboardComponent },
                    { path: 'actividades', component: Componentes.MisionesDetalleActividadComponent },
                    { path: 'common', component: Componentes.MisionesDetalleCommmonComponent },
                    { path: 'reto', component: Componentes.MisionesDetalleRetoComponent },
                    { path: 'elegibles', component: Componentes.MisionesDetalleElegiblesComponent },
                    { path: 'limites', component: Componentes.MisionesDetalleLimitesComponent },
                    { path: 'categorias', component: Componentes.MisionesDetalleCategoriasComponent },
                    { path: 'arte', component: Componentes.MisionesDetalleArteComponent },
                    { path: 'aprobacion', component: Componentes.MisionesDetalleAprobacionComponent }
                ]
            },

    ])],
    exports: [RouterModule],
    providers: [],
})
export class MisionesRoutingModule { }
