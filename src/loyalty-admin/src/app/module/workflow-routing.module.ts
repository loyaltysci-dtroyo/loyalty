import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import * as Component from '../component/workflows/index';
@NgModule({
    imports: [RouterModule.forChild([
        { path: '', redirectTo: 'listaWorkflow', pathMatch: 'full' },
            { path: 'listaWorkflow', component: Component.WorkflowListaComponent },
            { path: 'insertarWorkflow', component: Component.WorkflowInsertarComponent },
            {
                path: 'detalleWorkflow/:id', component: Component.WorkflowDetalleComponent,
                children: [
                    { path: '', redirectTo: 'common', pathMatch: 'full' },
                    { path: 'common', component: Component.WorkflowDetalleCommonComponent },
                    { path: 'definicion', component: Component.WorkflowDetalleDefinicionComponent},
                    { path: 'elegibles', component: Component.WorkflowDetalleElegiblesComponent}
                ]
            },
    ])],
    exports: [RouterModule],
    providers: [],
})
export class WorkflowRoutingModule { }
