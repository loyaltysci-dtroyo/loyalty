import { UbicacionesRoutingModule } from './ubicaciones-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';

import * as Componentes from '../component/ubicaciones';

@NgModule({
    imports: [GeneralModule, UbicacionesRoutingModule],
    exports: [],
    declarations: [
        Componentes.UbicacionesComponent,
        Componentes.UbicacionDetalleComponent,
        Componentes.UbicacionEfectividadComponent,
        Componentes.UbicacionInformacionComponent,
        Componentes.UbicacionInsertarComponent,
        Componentes.UbicacionListaComponent,
        Componentes.UbicacionMapaComponent,
        Componentes.BeaconComponent
        ],
    providers: [],
})
export class UbicacionesModule { }
