import { NotificacionesRoutingModule } from './notificaciones-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';

import * as Componentes from '../component/notificaciones';

@NgModule({
    imports: [GeneralModule, NotificacionesRoutingModule],
    exports: [],
    declarations: [
        Componentes.NotificacionesComponent,
        Componentes.NotificacionesDetalleArteComponent,
        Componentes.NotificacionDetalleDashboardComponent,
        Componentes.NotificacionesDetalleAtributosComponent,
        Componentes.NotificacionesDetalleComponent,
        Componentes.NotificacionesDetalleMiembrosComponent,
        Componentes.NotificacionesDetalleElegiblesComponent,
        Componentes.NotificacionesDetallePromoComponent,
        Componentes.NotificacionesDetalleSegmentoComponent,
        Componentes.NotificacionesInsertarComponent,
        Componentes.NotificacionesListaComponent,
        Componentes.NotificacionActividadComponent, 
        Componentes.NotificacionDetalleActividadComponent
    ],
    providers: [],
})
export class NotificacionesModule { }
