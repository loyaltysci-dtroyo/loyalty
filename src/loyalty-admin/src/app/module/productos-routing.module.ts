import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import * as Componentes from '../component/index';
@NgModule({
     imports: [RouterModule.forChild([
        { path: '', redirectTo: 'listaProductos', pathMatch: 'full' },
            { path: 'listaProductos', component: Componentes.ProductoListaComponent },
            { path: 'insertarProducto', component: Componentes.ProductoInsertarComponent },
            {
                path: 'detalleProducto/:id', component: Componentes.ProductoDetalleComponent,
                children: [
                    { path: '', redirectTo: 'informacion', pathMatch: 'full' },
                    { path: 'informacion', component: Componentes.ProductoInformacionComponent },
                    { path: 'display', component: Componentes.ProductoDisplayComponent },
                    { path: 'elegibles', component: Componentes.ProductoElegiblesComponent },
                    { path: 'limite', component: Componentes.ProductoLimiteComponent },
                    { path: 'categorias', component: Componentes.ProductoCategoriaComponent },
                    { path: 'miembros', component: Componentes.ProductoMiembroElegiblesComponent },
                    { path: 'segmentos', component: Componentes.ProductoSegmentoElegiblesComponent },
                    { path: 'atributos', component: Componentes.ProductoAtributoComponent },
                    { path: 'calendarizacion', component: Componentes.ProductoCalendarizacionComponent }
                ]
            },
            { path: 'listaCategoriasProducto', component: Componentes.CategoriaProductoListaComponent },
            { path: 'insertarCategProducto', component: Componentes.CategoriaProductoInsertarComponent },
            {
                path: 'detalleCategProducto/:id', component: Componentes.CategoriaProductoComponent,
                children: [
                    { path: '', redirectTo: 'informacion', pathMatch: 'full' },
                    { path: 'informacion', component: Componentes.CategoriaProductoDetalleComponent },
                    { path: 'subcategorias', component: Componentes.CategoriaProductoSubCategoriaComponent },
                    { path: 'certificadosCategoriaProducto', component: Componentes.CertificadoCategoriaProducto },
                ]
            },
            { path: 'listaAtributosProducto', component: Componentes.AtributoProductoListaComponent },
            { path: 'insertarAtributoProducto', component: Componentes.AtributoProdcutoInsertarComponent },
            {
                path: 'detalleAtributoProducto/:id', component: Componentes.AtributoProductoDetalleComponent
            },

    ])],
    exports: [RouterModule],
    providers: [],
})
export class ProductosRoutingModule { }
