import { OtrosRoutingModule } from './otros-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';
import {  SeleccionPromocionesComponent,SeleccionMiembrosComponent,SeleccionMetricaComponent,SeleccionSegmentosComponent,SeleccionUbicacionesComponent, SeleccionPremiosComponent, SeleccionCategoriaProductosComponent } from '../component/common/';
@NgModule({
    imports: [
        GeneralModule,
        OtrosRoutingModule
    ],
    exports: [
        SeleccionMiembrosComponent,
        SeleccionSegmentosComponent,
        SeleccionUbicacionesComponent,  
        SeleccionMetricaComponent,
        SeleccionPromocionesComponent,
        SeleccionPremiosComponent,
        SeleccionCategoriaProductosComponent
    ],
    declarations: [
        SeleccionMiembrosComponent,
        SeleccionSegmentosComponent,
        SeleccionUbicacionesComponent,  
        SeleccionMetricaComponent,
        SeleccionPromocionesComponent,
        SeleccionPremiosComponent,
        SeleccionCategoriaProductosComponent
    ],
    providers: [],
})
export class OtrosModule { }
