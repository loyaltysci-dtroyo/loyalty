import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import * as Componentes from '../component/bitacoras/index';
@NgModule({
    imports: [RouterModule.forChild([
        { path: '', redirectTo: 'apiExternal', pathMatch: 'full' },
        { path: 'apiExternal', component: Componentes.BitacoraApiExternalComponent, pathMatch: 'full' },
        { path: 'apiAdministrativa', component: Componentes.BitacoraApiAdministrativaComponent, pathMatch: 'full'}
    ])],
    exports: [RouterModule],
    providers: [],
})
export class BitacoraRoutingModule { }
