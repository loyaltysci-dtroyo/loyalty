import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import { PreferenciasInsertarComponent } from '../component/miembros/preferencias/preferencias-insertar.component';
import { DetallePreferenciaComponent } from '../component/miembros/preferencias/preferencias-detalle.component';
import { PreferenciasListaComponent } from '../component/miembros/preferencias/preferencias-lista.component';
import * as ComponentesMiembro from '../component/miembros/index';
import {
    AtributoComponent, AtributoListaComponent, AtributoInsertarComponent,
    AtributoDetalleComponent
} from '../component/atributos/index';
@NgModule({
     imports: [RouterModule.forChild([
        { path: '', component: ComponentesMiembro.MiembroListaComponent },
            { path: 'insertarMiembro', component: ComponentesMiembro.MiembroInsertarComponent },
            {
                path: 'detalleMiembro/:id', component: ComponentesMiembro.MiembroDetalleComponent,
                children: [
                    { path: '', redirectTo: 'perfil', pathMatch: 'full' },
                    { path: 'niveles', component: ComponentesMiembro.MiembroNivelComponent },
                    { path: 'grupos', component: ComponentesMiembro.MiembroGrupoComponent },
                    { path: 'atributosDinamicos', component: ComponentesMiembro.MiembroAtributoComponent },
                    { path: 'insignias', component: ComponentesMiembro.MiembroInsigniaComponent },
                    { path: 'preferencias', component: ComponentesMiembro.MiembroPreferenciaComponent },
                    { path: 'perfil', component: ComponentesMiembro.MiembroPerfilComponent },
                    { path: 'segmentos', component: ComponentesMiembro.MiembroSegmentoComponent },
                    { path: 'redenciones', component: ComponentesMiembro.MiembroActividadAwardsComponent },
                    { path: 'misiones', component: ComponentesMiembro.MiembroActividadMisionesComponent },
                    { path: 'metrica', component: ComponentesMiembro.MiembroActividadMetricaComponent },
                    { path: 'promociones', component: ComponentesMiembro.MiembroActividadPromocionesComponent },
                    { path: 'notificaciones', component: ComponentesMiembro.MiembroActividadNotificacionesComponent },
                    { path: 'premios', component: ComponentesMiembro.MiembroActividadPremiosComponent },
                    { path: 'compras', component: ComponentesMiembro.MiembroComprasComponent },
                    { path: 'referals', component: ComponentesMiembro.MiembroReferalsComponent},
                    { path: 'transacciones', component: ComponentesMiembro.MiembroTransaccionesComponent}
                ]
            },
            { path: 'editarMiembro/:id', component: ComponentesMiembro.MiembroEditarComponent },
            { path: 'listaMiembros', component: ComponentesMiembro.MiembroListaComponent },
            { path: 'detallePreferencias/:id', component: DetallePreferenciaComponent },
            { path: 'listaPreferencias', component: PreferenciasListaComponent },
            { path: 'insertarPreferencia', component: PreferenciasInsertarComponent },
            { path: 'importacion', component:ComponentesMiembro.MiembroImportacionComponent}
    ])],
    exports: [RouterModule],
    providers: [],
})
export class MiembrosRoutingModule { }
