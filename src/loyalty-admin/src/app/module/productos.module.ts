import { ProductosRoutingModule } from './productos-routing.module';
import { GeneralModule } from './general.module';
import { OtrosModule } from './otros.module';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import * as Componentes from '../component/productos/index';

@NgModule({
    imports: [GeneralModule, OtrosModule, ProductosRoutingModule],
    exports: [],
    declarations: [
        Componentes.ProductoComponent,
        Componentes.ProductoListaComponent,
        Componentes.ProductoInsertarComponent,
        Componentes.ProductoDetalleComponent,
        Componentes.ProductoInformacionComponent,
        Componentes.ProductoElegiblesComponent,
        Componentes.ProductoCategoriaComponent,
        Componentes.ProductoMiembroElegiblesComponent,
        Componentes.ProductoSegmentoElegiblesComponent,
        Componentes.ProductoDisplayComponent,
        Componentes.ProductoLimiteComponent,
        Componentes.ProductoAtributoComponent,
        Componentes.ProductoCalendarizacionComponent,
        //CATEGORIAS PRODUCTOS
        Componentes.CategoriaProductoComponent,
        Componentes.CategoriaProductoListaComponent,
        Componentes.CategoriaProductoInsertarComponent,
        Componentes.CategoriaProductoDetalleComponent,
        Componentes.CategoriaProductoSubCategoriaComponent,
        Componentes.CertificadoCategoriaProducto,
        //ATRIBUTOS PRODUCTOS
        Componentes.AtributoProductoComponent,
        Componentes.AtributoProductoListaComponent,
        Componentes.AtributoProdcutoInsertarComponent,
        Componentes.AtributoProductoDetalleComponent,
    ],
    providers: [],
})
export class ProductosModule { }
