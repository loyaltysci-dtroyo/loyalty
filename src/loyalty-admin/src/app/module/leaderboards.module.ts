import { LeaderboardsRoutingModule } from './leaderboards-routing.module';
import { GeneralModule } from './general.module';
import { NgModule } from '@angular/core';

import * as Compoentes from '../component/leaderboards/';

@NgModule({
    imports: [GeneralModule, LeaderboardsRoutingModule],
    exports: [],
    declarations: [
        Compoentes.LeaderboardDetalleComponent, 
        Compoentes.LeaderboardListaComponent, 
        Compoentes.LeaderboardInsertarComponent, 
        Compoentes.LeaderBoardsComponent],
    providers: [],
})
export class LeaderboardsModule { }
