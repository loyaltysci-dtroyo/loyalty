import { WorkflowComponent } from './component/workflows/index';
import { NgModule } from '@angular/core';
import { AuthGuard } from './component/common/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { DashboardComponent, DashVacioComponent, DashboardClienteComponent, DashboardSegmentoComponent } from './component/dashboards/index';
import { LeaderBoardsComponent } from './component/leaderboards/index';
import { MetricasComponent } from './component/metrica/index';
import { MiembrosComponent } from './component/miembros/index';
import { QuienQuiereSerMillonarioComponent } from './component/juegos/index';
import { MisionesComponent } from './component/misiones/index';
import { PromocionesComponent } from './component/promociones/index';
import { NotificacionesComponent } from './component/notificaciones/index';
import { SegmentosComponent } from './component/segmentos/index';
import { UbicacionesComponent } from './component/ubicaciones/index';
import { UsuariosComponent } from './component/usuarios/index';
import { GrupoNivelDetalleComponent, GrupoNivelListaComponent } from './component/nivel/index';
import { GrupoComponent } from './component/grupos/index';
import {
    AtributoComponent, AtributoListaComponent, AtributoInsertarComponent,
    AtributoDetalleComponent
} from './component/atributos/index';
import { InsigniaComponent } from './component/insignias/index';
import {
    ConfiguracionComponent, ConfiguracionGeneralComponent,
    ConfiguracionMediosComponent, ConfiguracionPoliticaComponent, ConfiguracionParamsComponent
} from './component/configuracion/index';
import { PremioComponent } from './component/premio/index';
import { ProductoComponent } from './component/productos/index';
import { InventarioComponent, DocumentoListaComponent, DocumentoInfoComponent, DocumentoInsertarComponent, ProveedorComponent, DetalleDocumentoComponent } from './component/inventario/index';
import { TrabajoInternoComponent } from './component/configuracion/trabajos-internos.component';
import { BitacoraComponent } from './component/bitacoras/index';
import { NoticiaComponent } from './component/noticias/index';
export const routes: Routes = [
    { path: '', redirectTo: '/dashboard/miembros', pathMatch: 'full' },
    {
        path: 'dashboard', component: DashboardComponent,loadChildren: 'app/module/dashboard.module#DashboardModule'
    },
    {
        path: 'usuarios', component: UsuariosComponent, canActivate: [AuthGuard], loadChildren: 'app/module/usuarios.module#UsuariosModule'
    },
    {
        path: 'miembros', component: MiembrosComponent, canActivate: [AuthGuard],
        loadChildren: 'app/module/miembros.module#MiembrosModule'
    },
    {
        path: 'ubicaciones', component: UbicacionesComponent, canActivate: [AuthGuard],
        loadChildren: 'app/module/ubicaciones.module#UbicacionesModule'
    },
    {
        path: 'metricas', component: MetricasComponent, canActivate: [AuthGuard],
        loadChildren: 'app/module/metricas.module#MetricasModule'
    },
    {
        path: 'leaderboards', component: LeaderBoardsComponent, canActivate: [AuthGuard],
        loadChildren: 'app/module/leaderboards.module#LeaderboardsModule'
    },
    {
        path: 'segmentos', component: SegmentosComponent, canActivate: [AuthGuard],
        loadChildren: 'app/module/segmentos.module#SegmentosModule'
    },
    {
        path: 'promociones', component: PromocionesComponent, canActivate: [AuthGuard],
        loadChildren: 'app/module/promociones.module#PromocionesModule'
    },
    {
        path: 'misiones', component: MisionesComponent, canActivate: [AuthGuard],
        loadChildren: 'app/module/misiones.module#MisionesModule'
    },
    {
        path: 'juegos', component: QuienQuiereSerMillonarioComponent,
        loadChildren: 'app/module/juegos.module#JuegosModule'
    },
    {
        path: 'productos', component: ProductoComponent, canActivate: [AuthGuard],
        loadChildren: 'app/module/productos.module#ProductosModule'
    },
    { path: 'recompensas', component: PromocionesComponent, canActivate: [AuthGuard] },
    {
        path: 'notificaciones', component: NotificacionesComponent, canActivate: [AuthGuard],
        loadChildren: 'app/module/notificaciones.module#NotificacionesModule'
    },
    {
        path: 'grupos', component: GrupoComponent, canActivate: [AuthGuard],
        loadChildren: 'app/module/grupos.module#GruposModule'
    },
    {
        path: 'atributosDinamicos', component: AtributoComponent, canActivate: [AuthGuard],
        children: [
            { path: '', component: AtributoListaComponent },
            { path: 'insertar', component: AtributoInsertarComponent },
            {
                path: 'detalleAtributo/:id',
                children: [
                    { path: '', component: AtributoDetalleComponent }
                ]
            }
        ]
    },
    {
        path: 'insignias', component: InsigniaComponent,
        loadChildren: 'app/module/insignias.module#InsigniasModule'
    },
    {
        path: 'configuracionSistema', component: ConfiguracionComponent,
        loadChildren: 'app/module/config.module#ConfigModule'
    },
    {
        path: 'premios', component: PremioComponent,
        loadChildren: 'app/module/premios.module#PremiosModule'
    },
    {
        path: 'workflows', component: WorkflowComponent,
        loadChildren: 'app/module/workflow.module#WorkflowModule'
    },
    {
        path: 'inventario', component: InventarioComponent,
        loadChildren: 'app/module/inventarios.module#InventariosModule'
    },
    { path: 'trabajos', component: TrabajoInternoComponent },
    {
        path: 'bitacoras', component: BitacoraComponent,
        loadChildren: 'app/module/bitacora.module#BitacoraModule'
    },
    {
        path: 'noticia', component: NoticiaComponent,
        loadChildren: 'app/module/noticias.module#NoticiasModule'
    }
];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
