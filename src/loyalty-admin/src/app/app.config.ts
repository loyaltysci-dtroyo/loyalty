import { Injectable } from '@angular/core';

@Injectable()
export class AppConfig {
  public static get BACK_DOMAIN(): string { return "https://api-demos.loyaltysci.com"; }
  public static get IDP_DOMAIN(): string { return "https://idp-demos.loyaltysci.com"; }
  public static get AUTHZ_API_CLIENT(): string { return 'loyalty-api' }
  
  //usado para cargar los elementos de internacionalizacion
  public static get LOCALE(): string { /*return navigator.language;*/ return "es" }
  // usado para realizar llamados al backend
  public static get TRANS_API_ENDPOINT(): string { return this.API_ENDPOINT; }
  // usado para realizar llamados al backend
  public static get API_ENDPOINT(): string { return AppConfig.BACK_DOMAIN + '/loyalty-api/webresources/v0'; }
  // usado para setear la imagen por default
  public static get DEFAULT_IMG_URL(): string { return './resources/img/generic0.jpg'; }

  public static get KC_CONF() {
    return {
      url: AppConfig.IDP_DOMAIN+'/auth',
      realm: 'loyalty-admin',
      clientId: 'loyalty-angular'
    }
  }
}