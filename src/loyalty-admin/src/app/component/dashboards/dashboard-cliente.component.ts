import { Component, ViewChild, OnInit, AfterContentChecked, AfterViewChecked } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Message, UIChart } from 'primeng/primeng';
import { Miembro, EstadisticaMiembroGeneral } from '../../model/index';
import { MiembroService, UtilsService, KeycloakService, EstadisticaService, I18nService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { PERM_LECT_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'dashboard-cliente-component',
    templateUrl: 'dashboard-cliente.component.html',
    providers: [Miembro, MiembroService, UtilsService, EstadisticaService]

})

/**
 * Componente hijo, muestra la información y grafica relacionada con los miembros
 */
export class DashboardClienteComponent implements OnInit {
    
    @ViewChild("chartEstado") chartEstado: UIChart;

    public totalActivos: number = 0; //cantidad de miembros activos
    public totalInactivos: number = 0; //cantidad de miembros inactivos    
    public total: number = 0;//Reresenta la cantidad de miembros en el sistema
    public nuevos: number = 0; //cantidad de miembros nuevos
    public data: any; //Representa los datos del pie chart de miembros inactivos/activos
    public options: any;
    public statsDataRangoEdades: EstadisticaMiembroGeneral;
    public statsDataMiembrosActivos: any[];
    public statsDataMiembrosGanadores: any[];
    public chartDataRangoEdades: any;
    public lectura: boolean; //Permisos de lectura sobre cliente
    public cargandoDatos: boolean = true;

    constructor(
        public kc: KeycloakService,
        private i18nService: I18nService,
        public estadisticaService: EstadisticaService,
        public route: ActivatedRoute,
        private router: Router,
        private keycloakService: KeycloakService,
        private miembroService: MiembroService,
        private utilService: UtilsService,
        private msgService: MessagesService,
        public authGuard: AuthGuard
    ) {
        //Datos utilizados para el gráfico de miembros del sistema
        this.data = {
            labels: ['Activos', 'Inactivos'],
            datasets: [
                {
                    data: [this.totalActivos, this.totalInactivos],
                    backgroundColor: [
                        "#388E3C",
                        "#1B5E20",
                    ],
                    hoverBackgroundColor: [
                        "#388E3C",
                        "#1B5E20",
                    ]
                }]
        };
        this.chartDataRangoEdades = undefined;
        //Son opciones para los chart, en este caso la posición de las leyendas
        this.options = {
            legend: {
                position: 'bottom'
            }
        };

        //Permite saber si el usuario tiene permisos
        this.authGuard.canWrite(PERM_LECT_MIEMBROS).subscribe((permiso: boolean) => {
            this.lectura = permiso;
            this.getEstadisticasMiembrosActivos();
            this.getEstadisticasMiembrosEdades();
            this.getEstadisticasMiembrosGanadores();
            this.getEstadisticasMiembrosEstado();
        });
    }

    ngOnInit() {
        this.getEstadisticasMiembrosActivos();
        this.getEstadisticasMiembrosEdades();
        this.getEstadisticasMiembrosGanadores();
        this.getEstadisticasMiembrosEstado();
        this.getMiembrosNuevos();
        this.cargandoDatos = false;
    }

    //Método utilizado para actualizar los datos del chart de miembros del sistema
    actualizarPie() {
        //Datos a utilizar
        this.data.datasets[0].data = [this.totalActivos, this.totalInactivos];
        this.chartEstado.refresh();
        this.total = this.totalActivos + this.totalInactivos;
    }

    /**
     * Obtiene la cantidad de miembros del sistema, activos o inactivos
     */
    getEstadisticasMiembrosEstado(){
        this.kc.getToken().then(
            (token: string) =>{
                this.miembroService.token = token;
                let sub = this.miembroService.obtenerEstadisticasMiembrosEstado().subscribe(
                    (response: Response) =>{
                        let miembroCantidad = response.json();
                        this.totalInactivos = miembroCantidad.cantMiembrosInactivos;  
                        this.totalActivos = miembroCantidad.cantMiembrosActivos;   
                        this.actualizarPie();
                    },
                    (error) =>{this.manejaError(error);},
                    () => {sub.unsubscribe();}
                )
            }
        )
    }
    /**
     * Obtiene los miembros nuevos del sistema según la fecha de ingreso
     */
    getMiembrosNuevos(){
        this.kc.getToken().then(
            (token: string) =>{
                this.miembroService.token = token;
                let sub = this.miembroService.obtenerCantidadMiembrosNuevos().subscribe(
                    (data) =>{
                        this.nuevos = data;
                    },
                    (error) =>{this.manejaError(error);},
                    () => {sub.unsubscribe();}
                )
            }
        )
    }

    /**
     * Obtiene las estadisticas para todos los miembros, como ranking de ganadores, etc
     */
    getEstadisticasMiembrosEdades() {
        if(this.lectura){
        //--Miembros por rango de edad
        this.kc.getToken().then(
            (tkn: string) => {
                this.estadisticaService.token = tkn;
                let sub = this.estadisticaService.getEstadisticasMiembrosEdades().subscribe(
                    (responseData: any) => {
                        this.statsDataRangoEdades = responseData;
                        //Se componen los datos usados en el charr de rango por edad
                        this.chartDataRangoEdades = {
                            labels: ['18-25', '25-35', '35-45', '45-55', '55+'],
                            datasets: [{
                                label: '',
                                data: [this.statsDataRangoEdades.cantMiembrosEdad1825, this.statsDataRangoEdades.cantMiembrosEdad2535, this.statsDataRangoEdades.cantMiembrosEdad3545, this.statsDataRangoEdades.cantMiembrosEdad4555, this.statsDataRangoEdades.cantMiembrosEdadMayor55],
                                backgroundColor: [
                                    '#003923',
                                    '#0E563B',
                                    '#267356',
                                    '#489075',
                                    '#73AC96'
                                ]
                            }
                            ]
                        };
                    }, 
                    (error) => { this.manejaError(error);}, 
                    () => {sub.unsubscribe();}
                );
            });
        }
    }

    //Tabla de Miembros mas activos (top de total de misiones completadas desglosadas por tipo)
    getEstadisticasMiembrosActivos() {
        if(this.lectura){
        this.kc.getToken().then(
            (tkn: string) => {
                this.estadisticaService.token = tkn;
                let sub = this.estadisticaService.getEstadisticasMiembrosActivos().subscribe(
                    (responseData: any) => {
                        this.statsDataMiembrosActivos = responseData;

                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
        }
    }

    //--Ranking de ganadores (miembrros con mas premios redimidos ordenados)
    getEstadisticasMiembrosGanadores() {
        if(this.lectura){
        this.kc.getToken().then(
            (tkn: string) => {
                this.estadisticaService.token = tkn;
                let sub = this.estadisticaService.getEstadisticasMiembrosGanadores().subscribe(
                    (responseData: any) => {
                        this.statsDataMiembrosGanadores = responseData;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
        }
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
  */
    manejaError(error: any) {
        // this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}