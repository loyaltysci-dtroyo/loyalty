import { Component, ViewChild, OnInit, AfterContentChecked, AfterViewChecked } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Message, UIChart } from 'primeng/primeng';
import { Segmento, EstadisticaSegmentoIndividual, EstadisticaSegmentoGeneral } from '../../model/index';
import { SegmentoService, SegmentoElegiblesService, KeycloakService, I18nService, EstadisticaService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { PERM_LECT_SEGMENTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index'

@Component({
    moduleId: module.id,
    selector: 'dashboard-segmentos-component',
    templateUrl: 'dashboard-segmentos.component.html',
    providers: [Segmento, SegmentoElegiblesService, EstadisticaSegmentoIndividual, SegmentoService, EstadisticaService]

})

export class DashboardSegmentoComponent implements OnInit {
    @ViewChild("charSegmentos") charSegmentos: UIChart;

    public listaSegmento: Segmento[] = []; //Lista de Segmentos
    public nombresSegmento: string[] = [];//Lista para guardar los nombres del segmento
    public cantidadMiembros: string[] = []; //Lista para guardar la cantidad de miembros del segmento
    public infoElegibles: any = {};//object con la info de elegibles (estadisticas)
    public segmentoProcesado: boolean;//indica que el procesamiento de reglas del segmento fue disparado alguna vez en el pasado

    public totalRecords: number; //Total de segmentos en el sistema
    public cantidad: number = 50; //Cantidad de rows
    public filaDesplazamiento: number = 0;//Desplazamiento de la primera fila
    public listaVacia: boolean; //Para verificar si la lista de segmentos esta vacia
    public totalActivos: number = 0; //cantidad de segmentos activos

    public data: any; //Representa los datos del pie chart de miembros inactivos/activos
    public draft: number = 0;
    public terminosBusqueda: string[];// el contenido de la barra de busquedas, queda null pues no es utilizado
    public preferenciasBusqueda: string[] = ["AC"];//almacena los indicadores de estado de segmento spara busqueda avanzada

    public chartDataMiembrosSegmentados: any;
    public chartDataMiembrosNuevos: any;
    public options;
    public lectura: boolean = false; //Permisos de lectura sobre notificacion
    public cargandoDatos: boolean = true;

    public dataset: any; //Para guardar la info del dataset para el chart datamiembronuevo

    constructor(
        public kc: KeycloakService,
        private i18nService: I18nService,
        public estadisticaService: EstadisticaService,
        public route: ActivatedRoute,
        public router: Router,
        public keycloakService: KeycloakService,
        public segmentoService: SegmentoService,
        public segmentoElegiblesService: SegmentoElegiblesService,
        public msgService: MessagesService,
        public authGuard: AuthGuard
    ) {
        //Permite saber si el usuario tiene permisos
        this.authGuard.canWrite(PERM_LECT_SEGMENTOS).subscribe((permiso: boolean) => {
            this.lectura = permiso;
            this.buscarSegmentos();
            this.getEstadisticasMiembrosNuevos();
            this.getEstadisticasMiembrosSegmentados();
        });
        //Datos utilizados para el gráfico de segmentos
        this.data = {
            labels: this.nombresSegmento,
            datasets: [
                {
                    label: 'Segmentos',
                    data: this.cantidadMiembros,
                    backgroundColor: "#3F51B5",
                    borderColor: "#1F5A1F",

                }]
        };
        this.options = {
            legend: {
                position: 'bottom'
            },
            //responsive:false,
            maintainAspectRatio: false
        };
    }

    ngOnInit() {
        this.buscarSegmentos();
        this.getEstadisticasMiembrosNuevos();
        this.getEstadisticasMiembrosSegmentados();
    }

    actualizarPie() {
        this.data.labels = this.nombresSegmento;
        this.data.datasets[0].data = this.cantidadMiembros;
        this.charSegmentos.refresh();
    }

    //Busca los segmentos disponibles en el sistema
    buscarSegmentos() {
        if (this.lectura) {
            let rango;
            this.keycloakService.getToken().then((tkn: string) => {
                this.segmentoService.token = tkn
                let sub = this.segmentoService.buscarSegmentos(this.terminosBusqueda, this.preferenciasBusqueda, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {

                        this.listaSegmento = <Segmento[]>response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalActivos = +response.headers.get("Content-Range").split("/")[1];
                            this.totalActivos++;
                            rango = response.headers.get("Content-Range").split("/")[0];
                            this.filaDesplazamiento = parseInt(rango.split("-")[1]);
                            this.obtenerInfoEligibles();
                        }
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => sub.unsubscribe());
            });
        }
    }

    //Permite obtenr todos los segmentos del sistema
    obtenerInfoSegmento() {
        //En caso de que se cumpla, vuelve a llamar  
        if (this.filaDesplazamiento < this.totalActivos) {
            this.filaDesplazamiento++;
            this.buscarSegmentos();
        }
        if (this.filaDesplazamiento >= this.totalActivos) {
            this.actualizarPie();
        }
    }

    //Obtiene la información de los elegibles
    obtenerInfoEligibles() {
        if (this.lectura) {
            this.keycloakService.getToken().then(
                (tkn: string) => {
                    this.segmentoElegiblesService.token = tkn;
                    this.listaSegmento.forEach(segmento => {
                        let sub = this.segmentoElegiblesService.getInfoEligibles(segmento.idSegmento).subscribe(
                            (data) => {
                                this.infoElegibles = data;
                                this.nombresSegmento = [...this.nombresSegmento, segmento.nombre];
                                this.cantidadMiembros = [...this.cantidadMiembros, this.infoElegibles.cantidadMiembros];
                                this.actualizarPie();
                            },
                            (error: Response) => {
                                //Si devuelve un 404 significa que el segmento no esta calculado
                                if (error.status == 404) {
                                    return;
                                }
                                this.manejaError(error);
                            },
                            () => {
                                sub.unsubscribe();
                            }
                        );
                    });
                    this.obtenerInfoSegmento();
                });
        }
    }

    /**
     * Obtiene las estadisticas para miembros nuevos por segmento
     */
    getEstadisticasMiembrosNuevos() {
        if (this.lectura) {
            //--Miembros por rango de edad
            this.kc.getToken().then(
                (tkn: string) => {
                    this.estadisticaService.token = tkn;
                    let sub = this.estadisticaService.getEstadisticasSegmentoMiembrosNuevos().subscribe(
                        (response: EstadisticaSegmentoIndividual[]) => {
                            this.chartDataMiembrosNuevos = {
                                labels: response.map((element) => { return element.segmento.nombre }),
                                datasets: [{
                                    label: response.map((element) => { return element.segmento.nombre }),
                                    data: response.map((element) => { return element.miembrosNuevos }),
                                    backgroundColor: response.map((element) => { '#' + (Math.random() * 0xFFFFFF << 0).toString(16) })
                                }
                                ]
                            };
                        }, (error) => {
                            this.manejaError(error);
                        }, () => {
                            sub.unsubscribe();
                        }
                    );
                });
        }
    }
    getEstadisticasMiembrosSegmentados() {
        if (this.lectura) {
            // --Tabla de Miembros mas activos (top de total de misiones completadas desglosadas por tipo)
            this.kc.getToken().then(
                (tkn: string) => {
                    this.estadisticaService.token = tkn;
                    let sub = this.estadisticaService.getEstadisticasMiembrosSegmentados().subscribe(
                        (response: EstadisticaSegmentoGeneral) => {
                            this.chartDataMiembrosSegmentados = {
                                labels: ['Segmentados', 'No Segmentados'],
                                datasets: [{
                                    data: [response.cantMiembrosSegmentados, response.cantMiembrosNoSegmentados],
                                    backgroundColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                                }
                                ]
                            };
                        }, (error) => {
                            this.manejaError(error);
                        }, () => {
                            sub.unsubscribe();
                        }
                    );
                });
        }
    }
    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error: any) {
        // this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
