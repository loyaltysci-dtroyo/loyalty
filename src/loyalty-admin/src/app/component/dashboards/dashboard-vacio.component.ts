import { Component, OnInit } from '@angular/core';
import { Response, Headers } from '@angular/http';
import { ConfirmationService } from 'primeng/primeng';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Miembro, Segmento } from '../../model/index';
import { MiembroService, KeycloakService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'dashboard-vacio-component',
    templateUrl: 'dashboard-vacio.component.html',
    providers: [Miembro, Segmento, MiembroService, ConfirmationService]
})

/*Esta clase representa al componente que permite mostrar los segmentos en los que el miembro es elegible*/
/*-------------------------------------------------------------------------------------------------------*/
export class DashVacioComponent {


    public id: string; // Id del miembro pasado por la url
    public totalRecords: number; // Total de miembros en el sistema
    public cantidad: number = 0; // Cantidad de filas
    public filaDesplazamiento: number = 0; // Desplazamiento de la primera fila
    public listaSegmentos: Segmento[] = []; // Contendrá la lista de los segmentos
    public listaVacia: boolean = true; // Para validar si la lista de segmentos viene vacía

    constructor(
        public miembroService: MiembroService,
        public router: Router,
        public route: ActivatedRoute,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public confirmationService: ConfirmationService,

    ) {
        //Permite capturar el id del miembro que viene en la url
        this.route.parent.params.subscribe(params => { this.id = params["id"]; });
    }


    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}