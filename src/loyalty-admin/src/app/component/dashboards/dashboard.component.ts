import {Component}  from '@angular/core'; 

@Component({
    moduleId:module.id,
    selector:'dashboard-component',
    templateUrl:'dashboard.component.html',
     
})

/**
 * Jaylin Centeno
 * Componente padre, muestra el dashboard
 */
export class DashboardComponent  {
  
}