import { I18nService, ProductoService } from '../../service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MenuItem, Message, SelectItem } from 'primeng/primeng';
import { PERM_LECT_PRODUCTOS, PERM_LECT_PROMOCIONES, PERM_LECT_MISIONES, PERM_LECT_PREMIOS, PERM_ESCR_WORKFLOW } from '../common/auth.constants';
import { RT_TAREA_DETALLE } from '../../utils/rutas';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Component, OnInit } from '@angular/core';
import { WorkflowService, KeycloakService, PremioService, MisionService, PromocionService } from '../../service/index';
import { Workflow, WorkflowNodo, Promocion, Mision, Premio, Producto, Metrica } from '../../model/index';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Response } from '@angular/http';
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de realizar mantenimiento de datos sobre la entidad de 
 * workflow, realiza mantenimiento sobre los datos generales
 */
@Component({
    moduleId: module.id,
    selector: 'workflow-detalle-common',
    templateUrl: 'workflow-detalle-common.component.html',
    providers: [MisionService, ProductoService, PromocionService, Metrica, PremioService, Promocion, Mision, Producto, Premio]
})
export class WorkflowDetalleCommonComponent implements OnInit {

    public nombreValido: boolean; //  true si el nombre es valido
    public idWorkflowInsertada: string; //  id que retorna el api cuado la workflow es insertada
    public itemsCodigo: SelectItem[] = []; //  items para poblar el select de codigo de workflow
    public cantRowsPorPagina: number;
    public rowOffset: number;
    public listaMisionesDisponibles: Mision[] = [];
    public listaPromocionesDisponibles: Promocion[] = [];
    public listaPremiosDisponibles: Premio[] = [];
    public listaProductosDisponibles: Producto[] = [];
    public terminosBusqueda: string[] = [];
    public cantRows: number; //  cant de rows totales para paginacion
    public nombreElementoSeleccionado: string;
    public displayModalSeleccionarElemento = false;

    public tienePermisosEscritura: boolean; //  true si el usuario tiene permisos para escribir en este recurso del sistema
    public lecturaProducto: boolean; // Permiso de lectura sobre producto
    public lecturaPremio: boolean; // Permiso de lectura sobre premio
    public lecturaMision: boolean; // Permiso de lectura sobre mision
    public lecturaPromocion: boolean; // Permiso de lectura sobre promocion 

    constructor(
        public workflow: Workflow,
        public router: Router, public kc: KeycloakService,
        public workflowService: WorkflowService,
        public premioService: PremioService,
        public misionService: MisionService,
        public promocionService: PromocionService,
        public productoService: ProductoService,
        public authGuard: AuthGuard,
        private i18nService: I18nService,
        public msgService: MessagesService) {
        //  poblar los items de codigo
        this.rowOffset = 0;
        this.cantRowsPorPagina = 10;
    }

    /**
     * Restablece los campos de referencia  nombre dle elemento seleccionado
     */
    resetFields() {
        this.workflow.referenciaDisparador = undefined;
        this.nombreElementoSeleccionado = undefined;
    }
    /**
     * Carga de manera perezosa los elementos para la referencia
     */
    lazyLoadElementos(event) {
        this.rowOffset = event.first;
        this.cantRows = event.rows;
    }

    ngOnInit() {
        this.authGuard.canWrite(PERM_ESCR_WORKFLOW).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.authGuard.canWrite(PERM_LECT_MISIONES).subscribe((result: boolean) => {
            this.lecturaMision = result;
        });
        this.authGuard.canWrite(PERM_LECT_PREMIOS).subscribe((result: boolean) => {
            this.lecturaPremio = result;
        });
        this.authGuard.canWrite(PERM_LECT_PRODUCTOS).subscribe((result: boolean) => {
            this.lecturaProducto = result;
        });
        this.authGuard.canWrite(PERM_LECT_PROMOCIONES).subscribe((result: boolean) => {
            this.lecturaPromocion = result;
        });
        this.obtenerWorkflow();
        this.itemsCodigo = this.i18nService.getLabels("workflow-items-codigo");
    }

    /**
   * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para
   * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes,
   * esto permite mantener un unico puntero al
   * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
   * no siempre sea necesario llamar al api para actualizar los datos
   */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
     * Obtiene los datos del workflow
     */
    obtenerWorkflow() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn; //  actualiza el token de authenticacion
                let sub = this.workflowService.getWorkflow(this.workflow.idWorkflow).subscribe(
                    (data: Workflow) => {
                        this.copyValuesOf(this.workflow, data);
                        this.obtenerNombreElementoSelecionado(this.workflow);
                    }, (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**
     * Establece el nombre interno para el workflow de manera automatica de maneja que el usuario no tenga que manipularlo
     */
    establecerNombres() {
        if (this.workflow.nombreWorkflow) {
            let temp = this.workflow.nombreWorkflow.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(' ', 'g'), '_');
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, '');
            this.workflow.nombreInternoWorkflow = temp;
        }
    }
    /** 
     * Inserta una promcion en el sistema, emplea los datos que contiene la variable global this.workflow
     */
    editarWorkflow() {
        this.kc.getToken().then(
            (tkn: string) => {

                this.workflowService.token = tkn; //  actualiza el token de authenticacion
                let sub = this.workflowService.updateWorkflow(this.workflow).subscribe(
                    (response: Response) => {
                        this.obtenerWorkflow();
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                    }, (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /* 
     * Metodo que reririge al detalle del segmento que recibe por parámetros
     * Recibe: el segmento al cual redirigir al detalle
     */
    gotoDetail(idWorkflow: string) {
        let link = [RT_TAREA_DETALLE + idWorkflow];
        this.router.navigate(link);
    }
    /**
     * 
     */
    validarNombre() {
        if (this.workflow.nombreInternoWorkflow == '') { this.nombreValido = false; return }
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;
                let sub = this.workflowService.validarNombre(this.workflow.nombreInternoWorkflow).subscribe(
                    (existeNombre: string) => {
                        this.nombreValido = (existeNombre == 'false');
                    }, () => {
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Agrega un elemento como comparador para la lista de tareas
     * este elemento puede ser de tipo mision, 
     * 
     * typeof es necesario por que el instanceof no funciona para este tipo de objeto
     * y algunas entidades (premio por ejemplo) tienen atributos con nombre igual (premio.idMetrica), por lo tanto no basta
     * con chequear la existencia de un atributo
     */
    agregarElementoNodoInsertar(elemento: any) {

        // promocion
        if (this.workflow.indDisparador == 'B') {
            this.workflow.referenciaDisparador = (elemento as Promocion).idPromocion;
            this.nombreElementoSeleccionado = (elemento as Promocion).nombre;
        }
        // mision
        if (this.workflow.indDisparador == 'C') {
            this.workflow.referenciaDisparador = (elemento as Mision).idMision;
            this.nombreElementoSeleccionado = (elemento as Mision).nombre;
        }
        // premio
        if (this.workflow.indDisparador == 'D') {
            this.workflow.referenciaDisparador = (elemento as Premio).idPremio;
            this.nombreElementoSeleccionado = (elemento as Premio).nombre;
        }
        // producto
        if (this.workflow.indDisparador == 'E') {
            this.workflow.referenciaDisparador = (elemento as Producto).idProducto;
            this.nombreElementoSeleccionado = (elemento as Producto).nombre;
        }
        this.displayModalSeleccionarElemento = false;
    }

    /***
     * Obtiene el listado de elementos necesarios para la referencia de tarea
     */
    listarElementosDisponibles() {
        switch (this.workflow.indDisparador) {
            case 'B': {
                if (this.lecturaPromocion) { this.obtenerListaPromociones();}
                break;
            }
            case 'C': {
                if (this.lecturaMision) {this.obtenerListaMisiones();}
                break;
            }
            case 'D': {
                if(this.lecturaPremio){ this.obtenerListaPremios();}
                break;
            }
            case 'E': {
                if(this.lecturaProducto){ this.obtenerListaProductos(); }
                break;
            }

        }
    }


    /**
      * Obtiene la lista de productos para usarlas a la hora de definir tareas
      */
    obtenerListaProductos() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.productoService.token = tkn;
                let sub = this.productoService.busquedaProducto(['A'], this.cantRowsPorPagina, this.rowOffset).subscribe(
                    (data: Response) => {
                        this.listaProductosDisponibles = <Producto[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }


    /**
          * Obtiene la lista de premios para usarlas a la hora de definir tareas
          */
    obtenerListaPremios() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.premioService.token = tkn;
                let sub = this.premioService.obtenerListaPremios("P", this.cantRowsPorPagina, this.rowOffset).subscribe(
                    (data: Response) => {
                        this.listaPremiosDisponibles = <Premio[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
      * Obtiene la lista de promociones para definir tareas
      */
    obtenerListaPromociones() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.buscarPromociones(this.terminosBusqueda, ['A'],
                    this.rowOffset, this.cantRowsPorPagina).subscribe(
                    (data: Response) => {
                        this.listaPromocionesDisponibles = <Promocion[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                    );
            });

    }
    /**
     * Metodo encargado de obtener el listado de misiones en elsistema, esto para la definicon de tareas sobre mision
     */
    obtenerListaMisiones() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.buscarMisiones(this.terminosBusqueda, ['A'], this.rowOffset, this.cantRowsPorPagina).subscribe(
                    (data: Response) => {
                        this.listaMisionesDisponibles = <Mision[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }

    /**
    * Obtiene el nombre del elemento seleccionado
    */
    obtenerNombreElementoSelecionado(elemento: Workflow) {
        switch (elemento.indDisparador) {
            case "B": {
                this.obtenerPromocion(elemento.referenciaDisparador);
                break;
            }
            case "C": {
                this.obtenerMision(elemento.referenciaDisparador);
                break;
            }
            case "D": {
                this.obtenerPremio(elemento.referenciaDisparador);
                break;
            }
            case "E": {
                this.obtenerProducto(elemento.referenciaDisparador);
                break;
            }
        }
    }
    obtenerProducto(idProducto: string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.productoService.token = tkn;
                let sub = this.productoService.obtenerProductoPorId(idProducto).subscribe(
                    (data: Promocion) => {
                        this.nombreElementoSeleccionado = data.nombre;
                    },
                    (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Obtiene una promocion a partir del id de promocion
     */
    obtenerPromocion(idPromo: string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.getPromocion(idPromo).subscribe(
                    (data: Promocion) => {
                        this.nombreElementoSeleccionado = data.nombre;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
    * Obtiene un premio a partir del id de premio
    */
    obtenerPremio(idPremio: string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.premioService.token = tkn;
                let sub = this.premioService.obtenerPremioPorId(idPremio).subscribe(
                    (data: Premio) => {
                        this.nombreElementoSeleccionado = data.nombre;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
    * Obtiene una mision a partir del id de mision
    */
    obtenerMision(idMision: string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.getMision(idMision).subscribe(
                    (data: Mision) => {
                        this.nombreElementoSeleccionado = data.nombre;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
       * Actualiza los registros de offset de 
       */
    actualizarOffset(response: Response) {
        if (response.headers.has('Content-Range')) {
            this.cantRows = +response.headers.get('Content-Range').split('/')[1];
        }
    }
    /*
     * Muestra un mensaje de error al usuario con los datos de la transaccion
     * Recibe: una instancia de request con la informacion de error
     * Retorna: un observable que proporciona información sobre la transaccion
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /**
     * 
     */
    confirm() {
        this.gotoDetail(this.idWorkflowInsertada);
    }
}