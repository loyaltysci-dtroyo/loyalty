import { Component, OnInit } from '@angular/core';
import { Workflow } from '../../model/index';
import { WorkflowService } from '../../service/index';
import { WorkflowNodo } from 'app/model/workflow-nodo';
/**
 * Flecha Roja Tecchnologies
 * Fernando Aguilar 
 * Componente raiz del arbol de componentes
 */
@Component({
    moduleId: module.id,
    selector: 'workflow-component',
    templateUrl: 'workflow.component.html',
    providers: [Workflow,WorkflowNodo,WorkflowService]
})
export class WorkflowComponent implements OnInit {
    constructor(    ) { }

    ngOnInit() { }
}