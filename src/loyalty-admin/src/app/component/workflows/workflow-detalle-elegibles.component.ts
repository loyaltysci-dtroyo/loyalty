import { Component, OnInit } from '@angular/core';
import { I18nService } from 'app/service/i18n.service';
import { SelectItem } from 'primeng/components/common/selectitem';
import { MiembroService } from 'app/service/miembro.service';

@Component({
    selector: 'workflow-detalle-elegibles',
    templateUrl: 'workflow-detalle-elegibles.component.html',
    providers:[]
})

export class WorkflowDetalleElegiblesComponent implements OnInit {
    public itemsIndExclusion: SelectItem[];
    public itemsIndComponente: SelectItem[];//items de select para el componente actual
    public indComponente: string;//indicador para seleccionar entre el componente de la mision
    public indExclusion: string;//indicador para seleccionar entre la inclusion o exclusion de elementos 
    public indInsertar: boolean;//indicador para seleccionar si inserta o se elimina 
    public displayInsertar: boolean;
    constructor(public i18nService: I18nService) {
        this.itemsIndComponente = [];
        this.itemsIndExclusion = [];
        this.indComponente = "S";
        this.indExclusion = "I";
        this.indInsertar = false;

        this.itemsIndExclusion = this.i18nService.getLabels("general-itemsIndExclusion");

        this.itemsIndComponente = this.i18nService.getLabels("misiones-elegibles-itemsIndComponente");
    }
    ngOnInit() {

    }
}