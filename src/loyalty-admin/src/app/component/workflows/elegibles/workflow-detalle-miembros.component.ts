
import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { MenuItem, SelectItem } from 'primeng/primeng';
import { Miembro, Workflow, Segmento, Ubicacion } from '../../../model/index';
import { MiembroService, KeycloakService, SegmentoService, WorkflowService } from '../../../service/index';

import { Router } from '@angular/router';
import { PERM_ESCR_PROMOCIONES } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de realizar mantenimiento de datos de los miembros elegibles de el workflow, agrega los miembros de la lista de disponibles a las listas de incluir/excluir
 * y/o elimina los mismos de las listas
 */
@Component({
    moduleId: module.id,
    selector: 'workflow-detalle-miembros',
    templateUrl: 'workflow-detalle-miembros.component.html',
    providers:[MiembroService, Miembro, WorkflowService]
})
export class WorkflowDetalleMiembrosComponent implements OnChanges, OnInit {
    public tienePermisosEscritura = true;
    public listaMiembrosDisponibles: Miembro[];//miembros disponibles
    public listaMiembros: Miembro[];//lista de miembros elegibles para el workflow
    public busquedaElegibles: string;//campo de busqueda para elegibles en el caso de miembros
    public busquedaDisponibles: string;//campo de busqueda para segmentos/usuarios/ubicaciones disponibles para agregar 
    public seleccMultipleAgregar: any[];//lista de miembros actualmente seleccionados en la seleccion multiple de agregar
    public seleccMultipleEliminar: any[];//lista de segmentos actualmente seleccionados en la seleccion multiple de eliminar
    public displayEliminar: boolean = false;//muestra el modal de eliminar
    @Input('incluir') indIncluir: string;//indica si se debe trabajar con los miembros a incluir o excluir
    @Input('insertar') displayInsertar: boolean = false;//indica si se debe mostrar los miembros a insertar

    constructor(
        public workflow: Workflow, public msgService: MessagesService,
        public miembroService: MiembroService,
        public authGuard: AuthGuard,
        public kc: KeycloakService,
        public workflowService: WorkflowService) {
    }

    ngOnInit() {
        this.displayInsertar = false;
        this.listarMiembrosElegibles();
    }

    /**
     * Usado para detectar cambios en los valores de los input del Componente
     * se usa para actualizar los listados dependiendo del valor y la variable
     * que fueron actualizados
     */
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        for (let propName in changes) {
            if (propName == "indIncluir") {
                if (this.displayInsertar == true) {
                    this.listarMiembrosDisponibles();
                }
                if (this.displayInsertar == false) {
                    this.listarMiembrosElegibles();
                }
            }
            if (propName == "displayInsertar") {
                let changedProp = changes[propName];
                if (changedProp.currentValue == true) {
                    this.listarMiembrosDisponibles();
                }
                if (changedProp.currentValue == false) {
                    this.listarMiembrosElegibles();
                }
            }

        }
    }
    /**Lista los miembros asociados a una promocion, 
         * usa el indicador de inclusion directamente en el llamdo al service
         * para traer solo los miembros incluidos/excluidos */
    listarMiembrosElegibles() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;
                let sub = this.workflowService.listarMiembrosElegiblesWorkflow(
                    this.workflow.idWorkflow, this.indIncluir).subscribe(
                    (data: Miembro[]) => {
                        this.listaMiembros = data;
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe() }
                    );
            });

    }

    //lista miembros disponibles para agregar
    listarMiembrosDisponibles() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;
                let sub = this.workflowService.listarMiembrosElegiblesWorkflow(this.workflow.idWorkflow, "D").subscribe(
                    (data: Miembro[]) => {
                        this.listaMiembrosDisponibles = data;
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    //-------------OPERACIONES DE MIEMBROS----------------
    /**
     * Incluye un miembro a la lista de elegibles para el workflow
     * Recibe: el miembroa insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    incluirMiembro(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;
                let sub = this.workflowService.incluirMiembro(this.workflow.idWorkflow, miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "El miembro ha sido incluido");
                        this.listarMiembrosDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
    * Incluye una lista de miembros a la lista d elgibles de la promcoion
    * Emplea la variable seleccionMultipleAgregar para tomar el listado de miembros que debe agregar
    * Llama al metodo manejaError en caso de excepcion
    */
    incluirListaMiembros(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;
                let idMiembros = this.seleccMultipleAgregar.map((elem: Miembro) => { return elem.idMiembro });
                let sub = this.workflowService.incluirListaMiembros(this.workflow.idWorkflow, idMiembros).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "Los miembros han sido incluidos");
                        this.listarMiembrosDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Excluye una lista de miembros de la lista de elegibles para el workflow
     * Recibe: el miembro a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    excluirListaMiembros(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;
                let idMiembros = this.seleccMultipleAgregar.map((elem: Miembro) => { return elem.idMiembro });
                let sub = this.workflowService.excluirListaMiembros(this.workflow.idWorkflow, idMiembros).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "Los miembros han sido excluidos");
                        this.listarMiembrosDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Excluye una lista de miembros de la lista de elegibles para el workflow
     * Recibe: el miembro a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarListaMiembro(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;
                let idMiembros = this.seleccMultipleEliminar.map((elem: Miembro) => { return elem.idMiembro });
                let sub = this.workflowService.eliminarListaMiembros(this.workflow.idWorkflow, idMiembros).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "Los miembros han sido eliminados");
                        this.listarMiembrosElegibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
    * Excluye un miembro de la lista de elegibles para el workflow
    * Recibe: el miembro a excluir de la lista de elegibles
    * Llama al metodo manejaError en caso de excepcion
    */
    excluirMiembro(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;
                let sub = this.workflowService.excluirMiembro(this.workflow.idWorkflow, miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "El miembro ha sido excluido");
                        this.listarMiembrosDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Elimina un miembro de la lista de elegibles para el workflow
     * Recibe: el miembro a eliminar de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarMiembroListas(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;
                let sub = this.workflowService.eliminarMiembro(this.workflow.idWorkflow, miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "El miembro ha sido eliminado");
                        this.listarMiembrosElegibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}