import { I18nService } from '../../service';
import { InsigniaService, MisionService, PromocionService, SegmentoService, NotificacionService, PremioService, MetricaService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Component, OnInit } from '@angular/core';
import { WorkflowService, KeycloakService } from '../../service/index';
import {
    Workflow, Metrica, WorkflowNodo, InsigniaNivel, Insignia, Mision, Segmento,
    Notificacion, Promocion, Premio
} from '../../model/index';
import { PERM_LECT_PRODUCTOS, PERM_LECT_PROMOCIONES, PERM_LECT_MISIONES, PERM_LECT_PREMIOS, 
    PERM_LECT_METRICAS, PERM_LECT_INSIGNIAS, PERM_LECT_CAMPANAS, PERM_ESCR_WORKFLOW } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
declare var flowchart: any;
declare var $: any;
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de definir las tareas que conformaran el workflow
 * despliega la lista de tareas y habilita operaciones de mantenimiento de
 * datos sobre ellas
 */

@Component({
    moduleId: module.id,
    selector: 'workflow-detalle-definicion',
    templateUrl: 'workflow-detalle-definicion.component.html',
    providers: [InsigniaNivel, Insignia, Metrica, Mision, Segmento, Notificacion, Promocion, Premio,
        InsigniaService, MisionService, PromocionService, SegmentoService, MetricaService, NotificacionService, PremioService]
})
export class WorkflowDetalleDefinicionComponent implements OnInit {

    public displayModalSeleccionarElemento = false;// flag para mostrar el modal de seleccion de elemento de referencia
    public displayModalInsertar: boolean; // flag para mostrar el modal de insertar tarea
    public displayModalEditar: boolean; // flag para mostrar el modal de editar tarea
    public listaNodos: WorkflowNodo[]; // lista de nodos para el workflow actual
    public terminosBusqueda: string[]; // termino de busqueda
    public rowOffset: number; // offset de row actual para paginacion
    public cantRowsPorPagina: number; // cantidad de row para paginacion
    public cantRowsTotal: number; // cantidad de row para paginacion
    public listaMisionesDisponibles: Mision[]; // listado de misiones disponibles para elegir como referencia para la tabla
    public listaSegmentosDisponibles: Segmento[]; // listado de segmentos disponibles para elegir como referencia para la tabla
    public listaMensajesDisponibles: Notificacion[]; // listado de mensajes disponibles para elegir como referencia para la tabla
    public listaMetricasDisponibles: Metrica[];
    public listaInsigniasDisponibles: Insignia[]; // listado de insignias disponibles para elegir como referencia para la tabla
    public listaPromocionesDisponibles: Promocion[];// listado de promociones disponibles para elegir como referencia para la tabla
    public listaPremiosDisponibles: Premio[]; // listado de premios disponibles para elegir como referencia para la tabla
    public nombreElementoSeleccionado: string;
    public optionsTipoTarea: SelectItem[];
    public cargando = false;
    public edicion = false;
    public displayModalEliminar: boolean; // habilita el cuadro de dialogo para eliminar un nodo

    public tienePermisosEscritura: boolean; //  true si el usuario tiene permisos para escribir en este recurso del sistema
    public lecturaProducto: boolean; // Permiso de lectura sobre producto
    public lecturaPremio: boolean; // Permiso de lectura sobre premio
    public lecturaMision: boolean; // Permiso de lectura sobre mision
    public lecturaPromocion: boolean; // Permiso de lectura sobre promocion 
    public lecturaInsignia: boolean; // Permiso de lectura sobre insignia 
    public lecturaMetrica: boolean; // Permiso de lectura sobre metrica 
    public lecturaNotificacion: boolean; // Permiso de lectura sobre notificacion 

    constructor(
        public workflow: Workflow,
        public workflowService: WorkflowService,
        public nodoInsertar: WorkflowNodo,
        public nodoEditar: WorkflowNodo,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public misionService: MisionService,
        public segmentoService: SegmentoService,
        public insigniaService: InsigniaService,
        public promocionService: PromocionService,
        public premioService: PremioService,
        public notificacionService: NotificacionService,
        public metricaService: MetricaService,
        private i18nService: I18nService,
        public authGuard: AuthGuard
    ) {
        this.cantRowsTotal = 0;
        this.cantRowsPorPagina = 4;
        this.rowOffset = 0;
        this.terminosBusqueda = [];
        this.displayModalEditar = this.displayModalInsertar = false;
        this.listaNodos = [];
        this.optionsTipoTarea = [];
        this.listaMisionesDisponibles = [];
        this.listaSegmentosDisponibles = [];
        this.listaInsigniasDisponibles = [];
        this.listaPromocionesDisponibles = [];
        this.listaPremiosDisponibles = [];
        this.listaMensajesDisponibles = [];
        this.listaMetricasDisponibles = [];
    }

    ngOnInit() {
        this.authGuard.canWrite(PERM_ESCR_WORKFLOW).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.authGuard.canWrite(PERM_LECT_MISIONES).subscribe((result: boolean) => {
            this.lecturaMision = result;
        });
        this.authGuard.canWrite(PERM_LECT_PREMIOS).subscribe((result: boolean) => {
            this.lecturaPremio = result;
        });
        this.authGuard.canWrite(PERM_LECT_PRODUCTOS).subscribe((result: boolean) => {
            this.lecturaProducto = result;
        });
        this.authGuard.canWrite(PERM_LECT_PROMOCIONES).subscribe((result: boolean) => {
            this.lecturaPromocion = result;
        });
        this.authGuard.canWrite(PERM_LECT_INSIGNIAS).subscribe((result: boolean) => {
            this.lecturaInsignia = result;
        });
        this.authGuard.canWrite(PERM_LECT_CAMPANAS).subscribe((result: boolean) => {
            this.lecturaNotificacion = result;
        });
        this.authGuard.canWrite(PERM_LECT_METRICAS).subscribe((result: boolean) => {
            this.lecturaMetrica = result;
        });
        this.actualizarDisplayNodos();
        this.obtenerWorkflow();
    }
    /**
     * Restablece los campos de referencia  nombre dle elemento seleccionado
     */
    resetFields() {
        this.nodoEditar.referencia = undefined;
        this.nodoInsertar.referencia = undefined;
        this.nombreElementoSeleccionado = undefined;
    }
    /**
     * Carga de manera perezosa los elementos para la referencia
     */
    lazyLoadElementos(event) {
        this.rowOffset = event.first;
        this.cantRowsTotal = event.rows;
        this.listarElementosDisponibles();
    }

    /**
     * Agrega un elemento como comparador para la lista de tareas
     * este elemento puede ser de tipo mision, 
     * 
     * typeof es necesario por que el instanceof no funciona para este tipo de objeto
     * y algunas entidades (premio por ejemplo) tienen atributos con nombre igual (premio.idMetrica), por lo tanto no basta
     * con chequear la existencia de un atributo
     */
    agregarElementoNodoInsertar(elemento: any) {
        // promocion
        if (this.nodoInsertar.indTipoTarea == "A") {
            this.nodoInsertar.referencia = (elemento as Promocion).idPromocion;
            this.nodoEditar.referencia = (elemento as Promocion).idPromocion;
            this.nombreElementoSeleccionado = (elemento as Promocion).nombre;
        }
        // mensaje
        if (this.nodoInsertar.indTipoTarea == "B") {
            this.nodoInsertar.referencia = (elemento as Notificacion).idNotificacion;
            this.nodoEditar.referencia = (elemento as Notificacion).idNotificacion;
            this.nombreElementoSeleccionado = (elemento as Notificacion).nombre;
        }
        // insignia
        if (this.nodoInsertar.indTipoTarea == "C") {
            this.nodoInsertar.referencia = (elemento as Insignia).idInsignia;
            this.nodoEditar.referencia = (elemento as Insignia).idInsignia;
            this.nombreElementoSeleccionado = (elemento as Insignia).nombreInsignia;
        }
        // premio
        if (this.nodoInsertar.indTipoTarea == "D") {
            this.nodoInsertar.referencia = (elemento as Premio).idPremio;
            this.nodoEditar.referencia = (elemento as Premio).idPremio;
            this.nombreElementoSeleccionado = (elemento as Premio).nombre;
        }
        // mision
        if (this.nodoInsertar.indTipoTarea == "E") {
            this.nodoInsertar.referencia = (elemento as Mision).idMision;
            this.nodoEditar.referencia = (elemento as Mision).idMision;
            this.nombreElementoSeleccionado = (elemento as Mision).nombre;
        }
        // metrica
        if (this.nodoInsertar.indTipoTarea == "F") {
            this.nodoInsertar.referencia = (elemento as Metrica).idMetrica;
            this.nodoEditar.referencia = (elemento as Metrica).idMetrica;
            this.nombreElementoSeleccionado = (elemento as Metrica).nombre;
        }
        this.displayModalSeleccionarElemento = false;
    }
    /***
     * Obtiene el listado de elementos necesarios para la referencia de tarea
     */
    listarElementosDisponibles() {
        this.edicion = false;
        //console.log("elementos disponibles de", this.nodoInsertar.indTipoTarea)
        switch (this.nodoInsertar.indTipoTarea) {
            case "A": {
                this.obtenerListaPromociones();
                break;
            }
            case "B": {
                this.obtenerListaMensajes();
                break;
            }
            case "C": {
                this.obtenerListaInsignias();
                break;
            }
            case "D": {
                this.obtenerListaPremios();
                break;
            }
            case "E": {
                this.obtenerListaMisiones();
                break;
            }
            case "F": {
                this.obtenerListaMetricas();
                break;
            }
        }
    }

    /**
     * Actualiza los registros de offset de 
     */
    actualizarOffset(response: Response) {
        if (response.headers.has("Content-Range")) {
            this.cantRowsTotal = +response.headers.get("Content-Range").split("/")[1];
        }
    }

    /**
     * Obtiene la lista de metricas en el sistema
     */
    obtenerListaMetricas() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.metricaService.token = tkn;
                let sub = this.metricaService.buscarMetricas(["P"], this.cantRowsPorPagina, this.rowOffset, this.terminosBusqueda.join(" ")).subscribe(
                    (data: Response) => {
                        this.listaMetricasDisponibles = <Metrica[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /**
     * Metodo encargado de obtener el listado de misiones en elsistema, esto para la definicon de tareas sobre mision
     */
    obtenerListaMisiones() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;

                let sub = this.misionService.buscarMisiones(this.terminosBusqueda, ["A"], this.rowOffset, this.cantRowsPorPagina).subscribe(
                    (data: Response) => {
                        this.listaMisionesDisponibles = <Mision[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * TODO:Obtener los mensajes de tipo triggered
    * Obtiene la lista de notificaciones para usarlas a la hora de definir tareas
    */
    obtenerListaMensajes() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.buscarNotificaciones(this.terminosBusqueda, ["P"], this.rowOffset, this.cantRowsPorPagina,["T"],[],["D"]).subscribe(
                    (data: Response) => {
                        this.listaMensajesDisponibles = <Notificacion[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
      * Obtiene la lista de premios para usarlas a la hora de definir tareas
      */
    obtenerListaPremios() {
        let busqueda = "";
        if (this.terminosBusqueda.length > 0) {
            this.terminosBusqueda.forEach(element => {
                busqueda += element + " ";
            });
            busqueda = busqueda.trim();
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.premioService.token = tkn;
                let sub = this.premioService.busquedaPremios(this.cantRowsPorPagina, this.rowOffset,[],["P"],busqueda ).subscribe(
                    (data: Response) => {
                        this.listaPremiosDisponibles = <Premio[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
      * Obtiene la lista de promociones para definir tareas
      */
    obtenerListaPromociones() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.buscarPromociones(this.terminosBusqueda, ["A"], this.rowOffset, this.cantRowsPorPagina).subscribe(
                    (data: Response) => {
                        this.listaPromocionesDisponibles = <Promocion[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
     * Metodo encargado de obtener la lista de insignias para la definicion de tareas
     */
    obtenerListaInsignias() {
        let busqueda = "";
        if (this.terminosBusqueda.length > 0) {
            this.terminosBusqueda.forEach(element => {
                busqueda += element + " ";
            });
            busqueda = busqueda.trim();
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.insigniaService.token = tkn;
                let sub = this.insigniaService.buscarInsigniasTemp(["P"], this.cantRowsPorPagina, this.rowOffset, [],busqueda).subscribe(
                    (data: Response) => {
                        this.listaInsigniasDisponibles = <Insignia[]>data.json();
                        this.displayModalSeleccionarElemento = true;
                        this.actualizarOffset(data);
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
     * Obtiene la lista de segmentos para ser usados como valor de copracion para las tareas
     */
    obtenerListaSegmentos() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoService.token = tkn;

                let sub = this.segmentoService.buscarSegmentos(this.terminosBusqueda, ["AC"], this.cantRowsPorPagina, this.rowOffset)
                    .subscribe(
                    (data: Response) => {
                        this.listaSegmentosDisponibles = <Segmento[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                    );
            });

    }

    /**
   * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
   * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, 
   * esto permite mantener un unico puntero al 
   * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
   * no siempre sea necesario llamar al api para actualizar los datos
   */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
     * Metodo encargado de obtener la lista de nodos para la tarea actual
     * para lo cual invoca la capa de servicio 
     */
    public obtenerWorkflow() {
        this.kc.getToken().then((token) => {
            this.workflowService.token == token
            let subWorkflow = this.workflowService.getWorkflow(this.workflow.idWorkflow).subscribe(
                (data: Workflow) => {
                    this.workflow = data
                    if(this.workflow.isWorkflowMisionEspec){
                        this.optionsTipoTarea = this.i18nService.getLabels("workflow-tipo-tarea").filter((element)=>{return element.value=='E'});
                    }else{
                        this.optionsTipoTarea = this.i18nService.getLabels("workflow-tipo-tarea");
                    };
                    this.nodoInsertar.indTipoTarea=this.optionsTipoTarea[0].value;
                    let subNodos = this.workflowService.listarNodosPorIdWorkflow(this.workflow.idWorkflow).subscribe(
                        (data: WorkflowNodo[]) => { this.listaNodos = data; },
                        (error: Response) => { this.manejaError(error) },
                        () => { subWorkflow.unsubscribe(); }
                    );
                }, (error: Response) => { this.manejaError(error) }, () => { subWorkflow.unsubscribe(); }
            );
        });

    }
    /**
     * Metodo encargado de insertar un nodo para la tarea actual justo despuesd el 
     * nodo especificado como padre, el cual recibe como parametro
     * para lo cual invoca la capa de servicio 
     */
    public insertarNodo(nodoPadre: WorkflowNodo) {
        this.kc.getToken().then((token) => {
            this.workflowService.token == token
            let subWorkflow = this.workflowService.insertarNodoWorkflow(nodoPadre.idNodo, this.nodoInsertar).subscribe(
                (data) => {
                    this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                    this.actualizarDisplayNodos();
                    this.nodoInsertar = new WorkflowNodo();
                    this.displayModalInsertar = false
                }, (error: Response) => { this.manejaError(error) }, () => { subWorkflow.unsubscribe(); }
            );
        });
    }
    /**
     * Metodo encargado eliminar el nodo indicado  para la tarea actual
     * para lo cual invoca la capa de servicio 
     * recibe como parametro el id del nodo a eliminar
     */
    public eliminarNodo() {
        this.kc.getToken().then((token) => {
            this.workflowService.token == token
            let subWorkflow = this.workflowService.eliminarNodoWorkflow(this.nodoEditar.idNodo).subscribe(
                (data) => {
                    this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                    this.actualizarDisplayNodos();
                    this.displayModalEliminar = false;
                    this.displayModalEditar = false
                }, (error: Response) => { this.manejaError(error) }, () => { subWorkflow.unsubscribe(); }
            );
        });
    }
    /**
     * Envia una peticion para actualizar la instancia de nodo que este almacenada en 
     * this.nodoEditar
     */
    public actualizarNodo() {
        this.kc.getToken().then((token) => {
            this.workflowService.token == token
            let subWorkflow = this.workflowService.editarNodoWorkflow(this.nodoEditar).subscribe(
                (data) => {
                    this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                    this.actualizarDisplayNodos();
                    this.displayModalEditar = false
                }, (error: Response) => { this.manejaError(error) }, () => { subWorkflow.unsubscribe(); }
            );
        });
    }
    /**
     * Metodo encargado de obtener la lista de nodos para la tarea actual
     * para lo cual invoca la capa de servicio 
     */
    public pushNodo() {
        this.kc.getToken().then((token) => {
            this.workflowService.token == token
            let subWorkflow = this.workflowService.pushNodoWorkflow(this.workflow.idWorkflow, this.nodoInsertar).subscribe(
                (data) => {
                    this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                    this.actualizarDisplayNodos();
                    this.nodoInsertar = new WorkflowNodo();
                    this.nombreElementoSeleccionado = undefined;
                    this.displayModalInsertar = false;
                    this.displayModalSeleccionarElemento = false;
                }, (error: Response) => { this.manejaError(error) }, () => { subWorkflow.unsubscribe(); }
            );
        });
    }

    /**
     * metodo encargado de actualizar el componente graficco que muestra los nodos (acciones) de la tarea programada
     * para lo cual obtiene primero la lista de nodos para la tarea, luego procede a crear la estructura de las tareas
     * segun su definicon y orden respectivo
     */
    public actualizarDisplayNodos() {
        let strDefElementos: string = "st=>start: " + this.i18nService.getLabels("tarea-inicio") + "\ne=>end: Fin";// string para declarar los elementos que formaran parte del workflow
        let strDefFlujo: string = "st";// string para la definicion del flujo de elementos
        this.kc.getToken().then((token) => {
            this.workflowService.token == token;// token para validacion
            let subWorkflow = this.workflowService.getWorkflow(this.workflow.idWorkflow).subscribe(
                (data: Workflow) => {// se obtienen los datos del nodo raiz
                    this.copyValuesOf(this.workflow, data);
                    this.kc.getToken().then((token) => {
                        this.workflowService.token == token;
                        // se obtiene el listado de nodos para la raiz obtenida
                        let subWorkflow = this.workflowService.listarNodosPorIdWorkflow(data.idWorkflow).subscribe(
                            (data: WorkflowNodo[]) => {
                                this.listaNodos = data;
                                data.forEach(nodo => {
                                    // por cada nodo:
                                    // se agrega el id de elemento y su nombre a la declaracion de la lista de tareas del flujo
                                    let tipoElemento = "";
                                    switch (nodo.indTipoTarea) {
                                        case "A": {
                                            strDefElementos += "\n" + nodo.idNodo +
                                                "=>operation: " + this.i18nService.getLabels("tarea-enviar-promocion") + ": " + nodo.nombreNodo + "|promocion"; break;
                                        }
                                        case "B": {
                                            strDefElementos += "\n" + nodo.idNodo +
                                                "=>operation: " + this.i18nService.getLabels("tarea-enviar-mensaje") + ": " + nodo.nombreNodo + "|mensaje"; break;
                                        }
                                        case "C": {
                                            strDefElementos += "\n" + nodo.idNodo +
                                                "=>operation: " + this.i18nService.getLabels("tarea-enviar-insignia") + ": " + nodo.nombreNodo + "|insignia"; break;
                                        }
                                        case "D": {
                                            strDefElementos += "\n" + nodo.idNodo +
                                                "=>operation: " + this.i18nService.getLabels("tarea-enviar-premio") + ": " + nodo.nombreNodo + "|premio"; break;
                                        }
                                        case "E": {
                                            strDefElementos += "\n" + nodo.idNodo +
                                                "=>operation: " + this.i18nService.getLabels("tarea-enviar-mision") + ": " + nodo.nombreNodo + "|mision"; break;
                                        }
                                        case "F": {
                                            strDefElementos += "\n" + nodo.idNodo +
                                                "=>operation: " + this.i18nService.getLabels("tarea-enviar-metrica") + ": " + nodo.nombreNodo + "|metrica"; break;
                                        }
                                    }
                                    // se agrega el id de elemento al flujo de tareas
                                    strDefFlujo += "->" + nodo.idNodo;
                                });
                                strDefFlujo += "->e";// nodo final
                                // se elimina el contenido del div que contiene el diagrama
                                $("[id=diagram]").empty();
                                // se parsea el contenido del string de formato
                                var diagram = flowchart.parse(strDefElementos + "\n" + strDefFlujo);
                                // se pinta el diagrama en el elemento que tiene como id diagram
                                diagram.drawSVG('diagram', {
                                    'x': 0, 'y': 0, 'font-weight': 'bold',
                                    'line-width': 3, 'line-length': 50, 'text-margin': 10,
                                    'font-size': 14, 'font-color': 'black', 'line-color': 'black',
                                    'element-color': 'black', 'fill': 'white',
                                    'yes-text': 'si', 'no-text': 'no',
                                    'arrow-end': 'block', 'scale': 1,
                                    // flowstate
                                    'flowstate': {
                                        'promocion': { 'fill': '#2D8CDE' },
                                        'mensaje': { 'fill': '#DAEDE2' },
                                        'insignia': { 'fill': '#77C4D3' },
                                        'premio': { 'fill': '#01A2A6' },
                                        'mision': { 'fill': '#3D9970' },
                                        'metrica': { 'fill': 'A3F4970' }
                                    }
                                });
                                //  una vez los elementos forman parte del DOM, por cada tarea 
                                this.listaNodos.forEach(element => {
                                    let idNodoSeleccionado: string = "";
                                    //  se agrega un listener para los clicks, para desplegar el modal de edicion para cada uno de ellos
                                    $("[id^=" + element.idNodo.substring(0, element.idNodo.length - 1) + "]").click(
                                        (event) => {
                                            // se obtiene el id de elemento clickeado
                                            // si el elemento es un rectangulo, o su texto (el texto tiene el mismo id del rect pero con una t al final)
                                            if (event.target.parentElement.tagName == "text") {
                                                idNodoSeleccionado = event.target.parentElement.id.substring(0, element.idNodo.length);
                                            } else {
                                                idNodoSeleccionado = event.target.id.toString();
                                            }
                                            //console.log(event.target.parentElement.tagName, event.target.parentElement.id.substring(0, element.idNodo.length))
                                            // se obtienen los datos del workflow para mostrarlos en el modal de edicion
                                            this.workflowService.obtenerNodoWorkflow(idNodoSeleccionado).subscribe(
                                                (data: WorkflowNodo) => {
                                                    this.nodoEditar = data;
                                                    //console.log(this.nodoEditar);
                                                    this.obtenerNombreElementoSelecionado(this.nodoEditar);
                                                    this.displayModalEditar = true;// deslega el modals
                                                }, (error: Response) => { this.manejaError(error); }, () => { }
                                            );
                                        }
                                        // finalmente se agrega css para que el pntero sea pointer sobre el texto
                                    ).css("cursor", "pointer");
                                    this.cargando = true;// se actualiza el flag de cargado para mostrar elementos de ui
                                });
                            }
                        );
                    });
                }, (error: Response) => { this.manejaError(error) }, () => { subWorkflow.unsubscribe(); }
            );
        });
    }
    /**
     * Obtiene el nombre del elemento seleccionado
     */
    obtenerNombreElementoSelecionado(elemento: WorkflowNodo) {
        //console.log(elemento);

        switch (elemento.indTipoTarea) {
            case "A": {
                if(this.lecturaPromocion){this.obtenerPromocion(elemento.referencia);}
                break;
            }
            case "B": {
                if(this.lecturaNotificacion){this.obtenerMensaje(elemento.referencia);}
                break;
            }   
            case "C": {
                if(this.lecturaInsignia){this.obtenerInsignia(elemento.referencia);}
                break;
            }
            case "D": {
                if(this.lecturaPremio){this.obtenerPremio(elemento.referencia);}
                break;
            }
            case "E": {
                if(this.lecturaMision){this.obtenerMision(elemento.referencia);}
                break;
            }
            case "F": {
                if(this.lecturaMetrica){this.obtenerMetrica(elemento.referencia);}
                break;
            }
        }
    }
    /**
     * Obtiene una promocion a partir del id de promocion
     */
    obtenerPromocion(idPromo: string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.getPromocion(idPromo).subscribe(
                    (data: Promocion) => {
                        this.nombreElementoSeleccionado = data.nombre;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
    * Obtiene un mensaje a partir del id de mensaje
    */
    obtenerMensaje(idMensaje: string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.getNotificacion(idMensaje).subscribe(
                    (data: Notificacion) => {
                        this.nombreElementoSeleccionado = data.nombre;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    obtenerMetrica(idMensaje: string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.metricaService.token = tkn;
                let sub = this.metricaService.getMetrica(idMensaje).subscribe(
                    (data: Metrica) => {
                        this.nombreElementoSeleccionado = data.nombre;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
   * Obtiene una insignia a partir del id de insignia
   */
    obtenerInsignia(idInsignia: string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.insigniaService.token = tkn;
                let sub = this.insigniaService.obtenerInsigniaPorId(idInsignia).subscribe(
                    (data: Insignia) => {
                        this.nombreElementoSeleccionado = data.nombreInsignia;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
    * Obtiene un premio a partir del id de premio
    */
    obtenerPremio(idPremio: string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.premioService.token = tkn;
                let sub = this.premioService.obtenerPremioPorId(idPremio).subscribe(
                    (data: Premio) => {
                        this.nombreElementoSeleccionado = data.nombre;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
    * Obtiene una mision a partir del id de mision
    */
    obtenerMision(idMision: string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.getMision(idMision).subscribe(
                    (data: Mision) => {
                        this.nombreElementoSeleccionado = data.nombre;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Maneja los errores que ocuren producto de request http fallidos al api
     * escala el error al servicio de manejo de errores
     */
    public manejaError(response: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(response));
    }
}