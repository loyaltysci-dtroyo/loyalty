import { I18nService } from '../../service';
import { MessagesService, ErrorHandlerService } from '../../utils/index';
import { Component, OnInit } from "@angular/core";
import { WorkflowService, KeycloakService } from "../../service/index";
import { Workflow, WorkflowNodo } from "../../model/index";
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PERM_ESCR_WORKFLOW } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import * as Rutas from '../../utils/rutas'
/**
 * Componente padre del arbol de componentes de detalle de tareas programadas
 * provee enrutamiento e inyeccion de dependencias a el mismo
 */
@Component({
    moduleId: module.id,
    selector: 'workflow-detalle',
    templateUrl: 'workflow-detalle.component.html',
    providers: []
})
export class WorkflowDetalleComponent implements OnInit {
    // tipos de workflow que se desplegaran A=Activo, B=Archivado, C=Borrador
    public cargando: boolean = true;
    public displayModalArchivar: boolean;
    public displayModalEliminar: boolean;
    public escritura: boolean; // Permiso de escritura sobre el workflow 

    constructor(
        public route: ActivatedRoute,
        public workflow: Workflow,
        public msgService: MessagesService,
        public kc: KeycloakService, private i18nService: I18nService,
        public workflowService: WorkflowService,
        public authGuard: AuthGuard,
        public router: Router
    ) {
        this.displayModalArchivar = false;
    }
    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un 
      * unico puntero al * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y 
      * tambien para que al iterar entre ellos no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
       
    }

    ngOnInit() {
        this.authGuard.canWrite(PERM_ESCR_WORKFLOW).subscribe((result: boolean) => {
            this.escritura = result;
        });
        this.route.params.forEach((params: Params) => { this.workflow.idWorkflow = params['id'] });
        this.obtenerWorkflow();
    }

    /**
      * Obtiene los datos del workflow
      */
    obtenerWorkflow() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;//actualiza el token de authenticacion
                let sub = this.workflowService.getWorkflow(this.workflow.idWorkflow).subscribe(
                    (data: Workflow) => {
                        this.copyValuesOf(this.workflow, data);
                        this.cargando = false;
                    }, (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    activarWorflow() {
        this.workflow.indEstado = "A";
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;//actualiza el token de authenticacion
                let sub = this.workflowService.updateWorkflow(this.workflow).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                    }, (error) => { this.manejaError(error); },
                    () => { this.obtenerWorkflow(); sub.unsubscribe(); }
                );
            });
    }
    archivarWorkflow() {
        this.workflow.indEstado = "B";
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;//actualiza el token de authenticacion
                let sub = this.workflowService.updateWorkflow(this.workflow).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.displayModalArchivar = false;

                    }, (error) => { this.manejaError(error); },
                    () => { this.obtenerWorkflow(); sub.unsubscribe(); }
                );
            });
    }
    eliminarWorkflow() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;//actualiza el token de authenticacion
                let sub = this.workflowService.deleteWorkflow(this.workflow).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"))
                        this.displayModalArchivar = false;
                        this.router.navigate([Rutas.RT_TAREA_LISTA]);

                    }, (error) => { this.obtenerWorkflow(); this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**
     *  Método que permite regresar en la navegación
     */
    goBack() {
        //window.history.back();   
        this.router.navigate([Rutas.RT_TAREA_LISTA]);
    }
    /**
     * Maneja el error a nivel de componente
     */
    public manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}