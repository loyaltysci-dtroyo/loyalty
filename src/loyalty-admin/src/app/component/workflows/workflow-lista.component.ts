import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Workflow } from '../../model/index';
import { WorkflowService } from '../../service/index';
import { MenuItem } from 'primeng/primeng';
import { Response } from '@angular/http';
import { KeycloakService } from '../../service/index';
import { AuthGuard } from '../common/index';
import { PERM_ESCR_WORKFLOW } from '../common/auth.constants';
/**
* Flecha Roja Technologies oct-2016
* Fernando Aguilar
* Componente encargado de mostra la lista de tareas programadas y
* brindar la posibilidad de navegar hacia el detalle de la misma y
* navegar al componente de isnercion de tareas programadas 
*/

@Component({
    moduleId: module.id,
    selector: 'workflow-lista',
    templateUrl: 'workflow-lista.component.html'
})
export class WorkflowListaComponent implements OnInit {

    public escritura: boolean; //Permiso de escritura (true/false)
    public listaWorkflows: Workflow[];// lista de workflows 
    public workflowEliminar: Workflow;// variable para almacenar la workflow a eliminar
    public terminosBusqueda: string[]=[];//  termino de busqueda
    public displayModalEliminar = false;// true= se muestra el modal
    public displayModalMensaje = false;// true= se muestra el modal
    public preferenciasBusqueda: string[] = [];// tipos de workflow que se desplegaran A=Activo, B=Archivado, C=Borrador
    public displayModalConfirm: boolean;// se muestra el modal de confirmacion cuando este valor es true
    //public itemsBusqueda: MenuItem[] = [];// usado para poblar el split button de terminos avanzados de busqueda
    public busquedaAvanzada = false;// usado para desplegar el div de busqueda avanzada
    public rowOffset: number;
    public cantRegistros: number;// cantidad de registros que se va a solicitar al api
    public cantTotalRegistros: number;// cantidad total de registros en el sistema para los terminos de busqueda establecidos
    public tipos:string[]=[];
    constructor( public workflow: Workflow, 
        public cdRef: ChangeDetectorRef,
        public router: Router, 
        public kc: KeycloakService,
        public workflowService: WorkflowService,
        public msgService: MessagesService,
        public authGuard: AuthGuard
    ) {
        //Permite saber si el usuario tiene permisos de escritura sobre workflows
        this.authGuard.canWrite(PERM_ESCR_WORKFLOW).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        /*this.itemsBusqueda = [
            { label: 'Avanzado', icon: 'ui-icon-build', command: () => { this.busquedaAvanzada = !this.busquedaAvanzada } }
        ];*/
     }
    ngOnInit() { 
        this.rowOffset=0;
        this.cantRegistros=10;
        this.cargarWorkflows();
    }

     selectWorkflowEliminar(workflow: Workflow) {
        this.workflowEliminar = workflow;
    }

    /*Metodo que reririge al detalle del segmento que recibe por parámetros
   * Recibe: el segmento al cual redirigir al detalle
   */
    gotoDetail(workflow: Workflow) {
        let link = ['workflows/detalleWorkflow', workflow.idWorkflow];
        this.router.navigate(link);
    }

    /**Usado para cargar de manera perezosa los elementos de la lista de workflows */
    loadData(event) {
        // event.first = First row offset
        // event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantRegistros = event.rows;
        this.cargarWorkflows();
    }

    /**Metodo encargado de cargar las workflows de la base de datos
     * Utiliza el string de busqueda que fue seteado por el campo de busqueda,
     * en este caso se obtiene el response completo para efectos de obtener los headers para la paginacion
     */
    cargarWorkflows() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.workflowService.token = tkn;
                let sub = this.workflowService.buscarWorkflows(this.terminosBusqueda, this.preferenciasBusqueda, this.rowOffset, this.cantRegistros,this.tipos).subscribe(
                    (data: Response) => {
                        this.cantTotalRegistros = +data.headers.get("Content-Range").split("/")[1];
                        this.listaWorkflows = <Workflow[]>data.json();// se obtiene los datos del body del request
                        this.cdRef.detectChanges();
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /*
      * Muestra un mensaje de error al usuario con los datos de la transaccion
      * Recibe: una instancia de request con la informacion de error
      * Retorna: un observable que proporciona información sobre la transaccion
      */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}