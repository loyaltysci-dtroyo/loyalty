import { I18nService } from '../../service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MenuItem, Message, SelectItem } from 'primeng/primeng';
import { PERM_LECT_PRODUCTOS, PERM_LECT_PROMOCIONES, PERM_LECT_MISIONES, PERM_LECT_PREMIOS, PERM_ESCR_WORKFLOW } from '../common/auth.constants';
import { RT_TAREA_DETALLE } from '../../utils/rutas';
import { AuthGuard } from '../common/index';
import { Component, OnInit } from '@angular/core';
import { Workflow, Promocion, Metrica, Mision, Producto, Premio, Notificacion } from '../../model/index';
import { WorkflowService, KeycloakService, MisionService, ProductoService, PromocionService, PremioService } from '../../service/index';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
import * as Rutas from '../../utils/rutas'
/**
 * Flecha Roja Technologies
 * Fernando Aguilar 
 * Componente encargado de insertar los metadatos de las tareas programadas, insertar
 * el nodo raiz, que contiene la informacion de 
 */
@Component({
    moduleId: module.id,
    selector: 'workflow-insertar',
    templateUrl: 'workflow-insertar.component.html',
    providers: [MisionService, ProductoService, PromocionService, Metrica, PremioService, Promocion, Mision, Producto, Premio, Notificacion]
})
export class WorkflowInsertarComponent implements OnInit {

    public nombreValido: boolean; // true si el nombre es valido
    public idWorkflowInsertada: string; // id que retorna el api cuado la workflow es insertada
    public itemsCodigo: SelectItem[] = []; // items para poblar el select de codigo de workflow
    public displayModalSeleccionarElemento: boolean;
    public listaMisionesDisponibles: Mision[] = [];
    public listaPromocionesDisponibles: Promocion[] = [];
    public listaPremiosDisponibles: Premio[] = [];
    public listaProductosDisponibles: Producto[] = [];
    public cantRowsPorPagina: number;
    public rowOffset: number;
    public terminosBusqueda: string[] = [];
    public cantRows: number; // cant de rows totales para paginacion
    public nombreElementoSeleccionado: string;

    public tienePermisosEscritura: boolean; // true si el usuario tiene permisos para esrcrbir en este recurso del sistema
    public lecturaProducto: boolean; // Permiso de lectura sobre producto
    public lecturaPremio: boolean; // Permiso de lectura sobre premio
    public lecturaMision: boolean; // Permiso de lectura sobre mision
    public lecturaPromocion: boolean; // Permiso de lectura sobre promocion 

    constructor(
        public workflow: Workflow,
        public router: Router,
        public kc: KeycloakService,
        public workflowService: WorkflowService,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public premioService: PremioService,
        public misionService: MisionService,
        public promocionService: PromocionService,
        public productoService: ProductoService,
        private i18nService: I18nService
    ) {
        // poblar los items de codigo
        this.itemsCodigo = this.i18nService.getLabels("workflow-items-codigo");
        this.rowOffset = 0;
        this.cantRowsPorPagina = 5;
        this.cantRows = 5;
    }

    ngOnInit() {
        this.authGuard.canWrite(PERM_ESCR_WORKFLOW).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.authGuard.canWrite(PERM_LECT_MISIONES).subscribe((result: boolean) => {
            this.lecturaMision = result;
        });
        this.authGuard.canWrite(PERM_LECT_PREMIOS).subscribe((result: boolean) => {
            this.lecturaPremio = result;
        });
        this.authGuard.canWrite(PERM_LECT_PRODUCTOS).subscribe((result: boolean) => {
            this.lecturaProducto = result;
        });
        this.authGuard.canWrite(PERM_LECT_PROMOCIONES).subscribe((result: boolean) => {
            this.lecturaPromocion = result;
        });
    }

    establecerNombres() {
        if (this.workflow.nombreWorkflow) {
            let temp = this.workflow.nombreWorkflow.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(' ', 'g'), '_');
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, '');
            this.workflow.nombreInternoWorkflow = temp;
        }
    }
    /**
         * Agrega un elemento como comparador para la lista de tareas
         * este elemento puede ser de tipo mision
         * typeof es necesario por que el instanceof no funciona para este tipo de objeto
         * y algunas entidades (premio por ejemplo) tienen atributos con nombre igual (premio.idMetrica), por lo tanto no basta
         * con chequear la existencia de un atributo
         */
    agregarElementoNodoInsertar(elemento: any) {

        // promocion
        if (this.workflow.indDisparador == 'B') {
            this.workflow.referenciaDisparador = (elemento as Promocion).idPromocion;
            this.nombreElementoSeleccionado = (elemento as Promocion).nombre;
        }
        // mision
        if (this.workflow.indDisparador == 'C') {
            this.workflow.referenciaDisparador = (elemento as Mision).idMision;
            this.nombreElementoSeleccionado = (elemento as Mision).nombre;
        }
        // premio
        if (this.workflow.indDisparador == 'D') {
            this.workflow.referenciaDisparador = (elemento as Premio).idPremio;
            this.nombreElementoSeleccionado = (elemento as Premio).nombre;
        }
        // producto
        if (this.workflow.indDisparador == 'E') {
            this.workflow.referenciaDisparador = (elemento as Producto).idProducto;
            this.nombreElementoSeleccionado = (elemento as Producto).nombre;
        }
        this.displayModalSeleccionarElemento = false;
    }

    /**
         * Restablece los campos de referencia  nombre dle elemento seleccionado
         */
    resetFields() {
        this.workflow.referenciaDisparador = undefined;
        this.nombreElementoSeleccionado = undefined;
    }
    /**
     * Carga de manera perezosa los elementos para la referencia
     */
    lazyLoadElementos(event) {
        this.rowOffset = event.first;
        this.cantRows = event.rows;
        this.listarElementosDisponibles();
    }
    /** 
     * Inserta una promcion en el sistema, emplea los datos que contiene la variable global this.workflow
     */
    insertarWorkflow() {

        if ((this.workflow.indDisparador == "B" || this.workflow.indDisparador == "C" || this.workflow.indDisparador == "D" || this.workflow.indDisparador == "E")
            && this.workflow.referenciaDisparador == null) {
            this.msgService.showMessage(this.i18nService.getLabels("warn-workflow-disparador"));
            return;
        }
        if (this.workflow.indDisparador == "A" || this.workflow.indDisparador == "H" || this.workflow.indDisparador == "F" || this.workflow.indDisparador == "G" || this.workflow.indDisparador == "I" || this.workflow.indDisparador == "J") {
            this.workflow.referenciaDisparador == null
        }
        this.workflow.indEstado = 'C';
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn; // actualiza el token de authenticacion
                let sub = this.workflowService.addWorkflow(this.workflow).subscribe(
                    (response: Response) => {
                        this.idWorkflowInsertada = response.text(); // obtiene el id de la workflow insertada a partir del body del response
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.gotoDetail(this.idWorkflowInsertada);
                    }, (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /* 
     * Metodo que reririge al detalle del segmento que recibe por parámetros
     * Recibe: el segmento al cual redirigir al detalle
     */
    gotoDetail(idWorkflow: string) {
        let link = [RT_TAREA_DETALLE + '/' + idWorkflow];
        this.router.navigate(link);
    }
    validarNombre() {
        if (this.workflow.nombreInternoWorkflow == '') { this.nombreValido = false; return; }
        this.kc.getToken().then(
            (tkn: string) => {
                this.workflowService.token = tkn;
                let sub = this.workflowService.validarNombre(this.workflow.nombreInternoWorkflow).subscribe(
                    (existeNombre: string) => {
                        this.nombreValido = (existeNombre == 'false');
                    }, () => {
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /*
     * Muestra un mensaje de error al usuario con los datos de la transaccion
     * Recibe: una instancia de request con la informacion de error
     * Retorna: un observable que proporciona información sobre la transaccion
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /**
     * 
 */
    confirm() {
        this.gotoDetail(this.idWorkflowInsertada);
    }

    /***
     * Obtiene el listado de elementos necesarios para la referencia de tarea
     */
    listarElementosDisponibles() {
        switch (this.workflow.indDisparador) {
            case 'B': {
                this.obtenerListaPromociones();
                break;
            }
            case 'C': {
                this.obtenerListaMisiones();
                break;
            }
            case 'D': {
                this.obtenerListaPremios();
                break;
            }
            case 'E': {
                this.obtenerListaProductos();
                break;
            }

        }
    }


    /**
      * Obtiene la lista de productos para usarlas a la hora de definir tareas
      */
    obtenerListaProductos() {
        let estado = ['A'];
        this.kc.getToken().then(
            (tkn: string) => {
                this.productoService.token = tkn;
                let sub = this.productoService.busquedaProducto(estado, this.cantRowsPorPagina, this.rowOffset).subscribe(
                    (data: Response) => {
                        this.listaProductosDisponibles = <Producto[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }


    /**
          * Obtiene la lista de premios para usarlas a la hora de definir tareas
          */
    obtenerListaPremios() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.premioService.token = tkn;
                let sub = this.premioService.obtenerListaPremios('P', this.cantRowsPorPagina, this.rowOffset).subscribe(
                    (data: Response) => {
                        this.listaPremiosDisponibles = <Premio[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
      * Obtiene la lista de promociones para definir tareas
      */
    obtenerListaPromociones() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.buscarPromociones(this.terminosBusqueda, ['A'], this.rowOffset, this.cantRowsPorPagina).subscribe(
                    (data: Response) => {
                        this.listaPromocionesDisponibles = <Promocion[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
     * Metodo encargado de obtener el listado de misiones en elsistema, esto para la definicon de tareas sobre mision
     */
    obtenerListaMisiones() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;

                let sub = this.misionService.buscarMisiones(this.terminosBusqueda, ['A'], this.rowOffset, this.cantRowsPorPagina).subscribe(
                    (data: Response) => {
                        this.listaMisionesDisponibles = <Mision[]>data.json();
                        this.actualizarOffset(data);
                        this.displayModalSeleccionarElemento = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
       * Actualiza los registros de offset de 
       */
    actualizarOffset(response: Response) {
        if (response.headers.has('Content-Range')) {
            this.cantRows = +response.headers.get('Content-Range').split('/')[1];
        }
    }
    /**
     *  Método que permite regresar en la navegación
     */
    goBack() {
        //window.history.back();   
        this.router.navigate([Rutas.RT_TAREA_LISTA]);
    }
}