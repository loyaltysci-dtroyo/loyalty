import { Component, OnInit } from '@angular/core';
import { GrupoNivel, Nivel } from '../../model/index';
import { NivelService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Response } from '@angular/http';
import { PERM_ESCR_METRICAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
@Component({
    moduleId: module.id,
    selector: 'grupo-nivel-lista-component',
    templateUrl: 'grupo-nivel-lista.component.html',
    providers: [NivelService, Nivel, GrupoNivel]
})
/**
 * Jaylin Centeno
 * Componente en el que se listan todos los grupos de niveles existentes 
 */
export class GrupoNivelListaComponent implements OnInit {

    public listaGrupos: GrupoNivel[]; //Array que contendra la lista de los grupos existentes
    public idGrupoNivel: string; //Valores del nivel seleccionado
    public listaVacia: boolean; //Para verificar si la lista de niveles esta vacia
    public display: boolean = false; //Para desplegar el modal de informacion
    public nuevo: boolean; //Para verificar el ingreso de una identificacion
    public busqueda: string = ""; //Terminos de búsqueda
    public listaBusqueda: string[] = [];
    public cargandoDatos: boolean = true; //Muestra la carga de datos
    public eliminar: boolean; //Habilita el cuadro de dialogo para eliminar un grupo
    public escritura: boolean;

    constructor(
        public grupoNivel: GrupoNivel,
        public grupoNivelService: NivelService,
        public router: Router,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService,
        public authGuard: AuthGuard
    ) { }

    //Inicialización
    ngOnInit() {
        this.authGuard.canWrite(PERM_ESCR_METRICAS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.obtenerListaGrupoNivel();
    }

    /**
     * Recibe el id del grupo y la acción a ejecutar sobre el grupo
     */
    accion(idGrupo: string, accion: string) {
        this.idGrupoNivel = idGrupo;
        if (accion == "detalle") {
            this.gotoDetail(idGrupo);
        } else {
            this.eliminar = true;
        }
    }

    /**
     * Se obtienen los grupos de niveles existentes
     */
    obtenerListaGrupoNivel() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.grupoNivelService.token = token;
                this.grupoNivelService.obtenertListaGrupoNivel().subscribe(
                    (data) => {
                        this.listaGrupos = data;
                        if (this.listaGrupos.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Se buscan los grupos de acuerdo a varios criterios de búsqueda
     */
    busquedaGrupos() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.grupoNivelService.token = token;
                this.grupoNivelService.buscarGrupoNivel(this.busqueda).subscribe(
                    (data: Response) => {
                        this.listaGrupos = data.json();
                        if (this.listaGrupos.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) =>{this.manejaError(error); this.cargandoDatos = false;}
                );
            }
        );
    }

    /**
     * Método encargado de validar la existencia del nombre de un grupo
     */
    validarNombre() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.grupoNivelService.token = token;
                this.grupoNivelService.verificarNombre(this.grupoNivel.nombre).subscribe(
                    (data) => {
                        if (!data) {
                            this.agregarGrupo();
                        } else {
                            this.msgService.showMessage(this.i18nService.getLabels("error-nombre-interno"));
                        }
                    }
                );
            }
        );
    }

    /**
     * Permite insertar la definición de un nuevo grupo
     */
    agregarGrupo() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.grupoNivelService.token = token;
                this.grupoNivelService.agregarGrupoNivel(this.grupoNivel.nombre).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.gotoDetail(data);
                    },
                    (error) => {
                        this.manejaError(error)
                    }
                )
            }
        );
    }

    /**
     * Se redirecciona al detalle del grupo de nivel
     */
    gotoDetail(idGrupoNivel: string) {
        let link = ['metricas/gruposNiveles/detalle', idGrupoNivel];
        this.router.navigate(link);
    }

    /**
     * Permite remover un grupo de nivel
     */
    removerGrupoNivel() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.grupoNivelService.token = token;
                this.grupoNivelService.removerGrupoNivel(this.idGrupoNivel).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-archivar"));
                        this.obtenerListaGrupoNivel();
                        this.eliminar = false;
                    },
                    (error) => {
                        this.manejaError(error);
                        this.eliminar = false;
                    }
                );
            }
        );
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
