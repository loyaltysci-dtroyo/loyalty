import { Component, OnInit } from '@angular/core';
import { Nivel, GrupoNivel } from '../../model/index';
import { NivelService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { PERM_ESCR_METRICAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'grupo-nivel-detalle-component',
    templateUrl: 'grupo-nivel-detalle.component.html',
    providers: [Nivel, NivelService, GrupoNivel]
})

/**
 * Componente encargado de mostrar el detalle del grupo y sus niveles
 */
export class GrupoNivelDetalleComponent implements OnInit {

    //Representa el id enviado por parametro
    public id: string;
    //Lista de los niveles del grupo
    public listaNiveles: Nivel[];
    //Validar si la lista esta vacia
    public listaVacia: boolean = true;
    //Id del nivel seleccionado
    public idNivel: string;
    //Form group para validar el nivel
    public formNivel: FormGroup;
    public formNivelDetalle: FormGroup;
    //manejo de errores
    public error: boolean;
    public nuevo: boolean = false;
    public nivelEditar: boolean = false;
    public editar: boolean = false;
    public estado: string = "Agregar";
    public nombreTemporal: string;
    public escritura: boolean;
    public cargandoDatos: boolean = true;
    public elemento: string;
    public eliminar: boolean;

    constructor(
        public router: Router,
        public route: ActivatedRoute,
        public nivelNuevo: Nivel,
        public nivel: Nivel,
        public grupoNivel: GrupoNivel,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public nivelService: NivelService,
        public i18nService: I18nService
    ) {

        //El id del nivel se inicializa con el valor introducido por la url
        this.route.params.forEach((params: Params) => { this.id = params['id'] });

        //Validacion del form
        this.formNivel = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
            'metricaInicial': new FormControl('', Validators.required),
            'multiplicador': new FormControl('')
        });

        //Validacion del form
        this.formNivelDetalle = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
            'metricaInicial': new FormControl('', Validators.required),
            'multiplicador': new FormControl('')
        });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_METRICAS).subscribe(
            (permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    ngOnInit() {
        this.mostrarDetalle();
    }

    //Método para mostrar el dialogo de confirmación
    confirmar(elemento: string, idNivel?: string) {
        this.elemento = elemento;
        if(this.elemento == "nivel"){
            this.idNivel = idNivel;
        }
        this.eliminar = true;
    }

    //Se encarga de obtener los grupos de nivel por id
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.nivelService.token = token;
                this.nivelService.obtenertGrupoNivelPorId(this.id).subscribe(
                    (data) => {
                        this.grupoNivel = data;
                        this.nombreTemporal = this.grupoNivel.nombre;
                        this.obtenerListaNiveles();
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    edtiarGrupo(){
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.nivelService.token = token;
                this.nivelService.editarGrupo(this.grupoNivel).subscribe(
                    (data) => {
                        this.editar = false;
                        this.msgService.showMessage({ detail: "El grupo se actualizo correctamente", severity: "success", summary: "" });
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    //Método encargado de validar la existencia del nombre de un grupo
    validarNombre() {
        if (this.grupoNivel.nombre === this.nombreTemporal) {
            this.edtiarGrupo();
        } else {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.nivelService.token = token;
                this.nivelService.verificarNombre(this.grupoNivel.nombre).subscribe(
                    (data) => {
                        if (!data) {
                            this.edtiarGrupo();
                        } else {
                            this.msgService.showMessage({ detail: "El nombre del grupo ya existe.", severity: "warn", summary: "" });
                        }
                    }
                );
            }
        );
        }
    }

    nivelSeleccionado(nivel: Nivel, accion: string) {
        this.nivel = nivel;
        this.estado = "Editar"
        this.nivelEditar = true;
    }


    obtenerListaNiveles() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.nivelService.token = token;
                this.nivelService.obtenerListaNiveles(this.id).subscribe(
                    (data) => {
                        this.listaNiveles = data;
                        if (this.listaNiveles.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => {
                        this.manejaError(error);
                    }
                );
            }
        );
    }

    //Método para agregar un nivel 
    agregarNivel() {
        this.nivelNuevo.grupoNiveles = this.grupoNivel;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.nivelService.token = token;
                this.nivelService.agregarNivel(this.id, this.nivelNuevo).subscribe(
                    (data) => {
                        this.msgService.showMessage({ detail: "El tier se ingreso correctamente", severity: "success", summary: "" });
                        this.nuevo = false;
                        this.obtenerListaNiveles();
                        this.formNivel.reset();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.formNivel.reset();
                    }
                )
            }
        );
    }

    editarNivel() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.nivelService.token = token;
                this.nivelService.editarNivel(this.id, this.nivel).subscribe(
                    (data) => {
                        this.msgService.showMessage({ detail: "El tier se actualizo correctamente", severity: "success", summary: "" });
                        this.nivelEditar = false;
                        this.obtenerListaNiveles();
                    },
                    (error) => {
                        this.manejaError(error);
                    }
                )
            }
        );
    }

    removerNivel() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.nivelService.token = token;
                this.nivelService.removerNivel(this.id, this.idNivel).subscribe(
                    (data) => {
                        this.msgService.showMessage({ detail: "El tier ha sido removido", severity: "success", summary: "" });
                        this.obtenerListaNiveles();
                        this.eliminar = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    removerGrupo() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.nivelService.token = token;
                this.nivelService.removerGrupoNivel(this.id).subscribe(
                    (data) => {
                        this.goBack();
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    //Metodo que permite navegar
    goBack() {
        let link = ['metricas/gruposNiveles'];
        this.router.navigate(link);
    }

    // Metodo para manejar los errores que se provocan en las transacciones
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}