import { Component, OnInit } from '@angular/core';
import { Metrica } from '../../model/metrica';
import { MetricaService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Pagination, PaginatedResult } from '../../model/paginacion';
import { Response } from "@angular/http";
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { PERM_ESCR_METRICAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'metrica-lista-component',
    templateUrl: 'metrica-lista.component.html',
    providers: [Metrica, MetricaService]

})

/**
 * Jaylin Centeno
 * Componente que lista todas las metricas existentes 
 */
export class MetricaListaComponent implements OnInit {

    public escritura: boolean;//Permite verificar permisos (true/false)
    public cargandoDatos: boolean = true;
    public eliminar: boolean;//Habilita el cuadro de dialogo para eliminar
    public accion: string; //Guarda el tipo de accion a realizar sobre una metrica

    // ------ Métricas ------ //
    public listaMetricas: Metrica[]; //lista de metricas existentes
    public listaVacia: boolean; //Verifica si la lista de metricas esta vacia
    public cantidad: number = 10; //Cantidad de filas
    public totalRecords: number; //Total de productos en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public idMetrica: string; //Id de la métrica seleccionada

    // ------ Busqueda ------ //
    public avanzada: boolean = false; //Para mostrar la búsqueda avanzada
    public busqueda: string; //Palabras clave de búsqueda
    public listaBusqueda: string[] = [];
    public estadoMetrica: string[] = [];

    constructor(
        public metrica: Metrica,
        public metricaService: MetricaService,
        public router: Router,
        public msgService: MessagesService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_METRICAS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.estadoMetrica = ['P', 'B'];
    }

    //Inicializacion
    ngOnInit() {
        this.busquedaMetricas();
    }

    //Método para mostrar el dialogo de confirmación
    confirmar(idMetrica: string) {
        this.idMetrica = idMetrica;
        this.eliminar = true;
    }

    //Método para ir a la página de insertar un cliente
    nuevaMetrica() {
        let link = ['metricas/insertarMetrica'];
        this.router.navigate(link);
    }

    //Método que obtiene la metrica seleccionada para ejecutar una acción
    //Recibe la metrica y la acción a ejecutar
    metricaSeleccionada(idMetrica: string) {
        let link = ['metricas/metricaDetalle', idMetrica];
        this.router.navigate(link);
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaMetricas();
    }

    //Método encargado de realizar la búsqueda de métricas
    busquedaMetricas() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.metricaService.token = token;
                this.metricaService.buscarMetricas(this.estadoMetrica, this.cantidad, this.filaDesplazamiento, this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaMetricas = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaMetricas.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => {
                        this.manejaError(error);
                        this.cargandoDatos = false;
                    }
                );
            }
        );
    }

    //Método que se encarga de archivar/remover la métrica
    removerMetrica() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.metricaService.token = token || "";
                this.metricaService.deleteMetrica(this.idMetrica).subscribe(
                    (data) => {
                        if (this.accion == "archivar") {
                            this.msgService.showMessage(this.i18nService.getLabels('general-archivar'));
                        } else {
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                        }
                        this.eliminar = false;
                        this.busquedaMetricas();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.eliminar = false;
                    }
                );
            }
        );

    }

    //Metodo que permite navegar en la historia de navegación
    goBack() {
        window.history.back();
    }

    // Metodo para manejar los errores que se provocan en las transacciones
    manejaError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}