import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MetricaService, EstadisticaService } from '../../service/index';
import { Metrica } from '../../model/index';
import { KeycloakService } from '../../service/index';
import { PERM_LECT_METRICAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'metrica-dashboard-general',
    templateUrl: 'metrica-dashboard-general.component.html',
    providers: [Metrica, MetricaService, EstadisticaService]
})

/**
 * Componente que maneja información general de las métricas y las muestra en charts
 */
export class MetricaDashboardGeneralComponent implements OnInit {
    //Propiedades del componente
    public statsData: [{
        idMetrica: string,
        nombreMetrica: string,
        nombreInternoMetrica: string,
        cantAcumulada: number,
        cantRedimida: number,
        cantVencida: number,
        periodos: [
            {
                mes: string,
                cantAcumulada: number,
                cantRedimida: number,
                cantVencida: number
            },
            {
                mes: string,
                cantAcumulada: number,
                cantRedimida: number,
                cantVencida: number
            },
            {
                mes: string,
                cantAcumulada: number,
                cantRedimida: number,
                cantVencida: number
            },
            {
                mes: string,
                cantAcumulada: number,
                cantRedimida: number,
                cantVencida: number
            }
        ]
    }];
    public chartCantAcumulada: any;
    public chartCantRedimida: any;
    public chartCantVencida: any;

    public options: any;

    public cantRedimida: number;
    public cantVencida: number;
    public cantAcumulada: number;
    public lectura: boolean; //Permisos de lectura sobre notificacion
    public cargandoDatos: boolean = true;

    constructor(
        public metrica: Metrica,
        public kc: KeycloakService,
        public metricaService: MetricaService,
        private i18nService: I18nService,
        public estadisticaService: EstadisticaService,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService,
        public authGuard: AuthGuard
    ) {
        this.options = {
            legend: {
                position: 'bottom'
            }
        };
        this.cantAcumulada = this.cantVencida = this.cantRedimida = 0;
        //Permite saber si el usuario tiene permisos
        this.authGuard.canWrite(PERM_LECT_METRICAS).subscribe((permiso: boolean) => {
            this.lectura = permiso;
            this.obtenerDatosEstadistica();
        });

    }

    ngOnInit() {
    }

    //Se llenan los graficos con los datos obtenidos del sistema
    poblarGraficos() {
        this.chartCantAcumulada = {
            labels: this.statsData[0].periodos.map((element => element.mes)),
            datasets: this.statsData.map((element) => {
                return {
                    label: element.nombreMetrica,
                    data: element.periodos.map((element) => { return element.cantAcumulada }),
                    fill: false,
                    borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                }
            })
        }

        this.chartCantRedimida = {
            labels: this.statsData[0].periodos.map((element => element.mes)),
            datasets: this.statsData.map((element) => {
                return {
                    label: element.nombreMetrica,
                    data: element.periodos.map((element) => { return element.cantRedimida }),
                    fill: false,
                    borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                }
            })
        }
        this.chartCantVencida = {
            labels: this.statsData[0].periodos.map((element => element.mes)),
            datasets: this.statsData.map((element) => {
                return {
                    label: element.nombreMetrica,
                    data: element.periodos.map((element) => { return element.cantVencida }),
                    fill: false,
                    borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                }
            })
        }
        this.statsData.forEach((element) => {
            this.cantAcumulada += element.cantAcumulada;
            this.cantVencida += element.cantVencida;
            this.cantRedimida += element.cantRedimida;
        });
    }

    //Método encargado de obtener las estadisticas de las métricas
    obtenerDatosEstadistica() {
        if (this.lectura) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.estadisticaService.token = tkn;
                let sub = this.estadisticaService.getEstadisticasMetricas().subscribe(
                    (responseData: any) => {
                        this.statsData = responseData;
                        this.poblarGraficos();
                        this.cargandoDatos = false;
                    }, (error) => {
                        this.manejaError(error);
                        this.cargandoDatos = false;
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
        }else{ this.cargandoDatos = false; }
    }

    //Metodo llamado cada vez que se seleccionan datos de los charts
    selectData(event) {

    }
    /**
   * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
   * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
   * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
   * no siempre sea necesario llamar al api para actualizar los datos
   */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}