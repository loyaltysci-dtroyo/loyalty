import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService, MessagesService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { ActividadService, KeycloakService } from '../../service/index';
import { Metrica, RegistroActividad } from '../../model/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { PERM_LECT_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'metrica-actividad',
    templateUrl: 'metrica-actividad.component.html',
    providers: [ActividadService]
})

/**
 * Componente utilizado para mostrar la actividad de Métrica -->
 */
export class MetricaActividadComponent implements OnInit {
    public listaActividades: any[];
    /**Atributos para sorting/busqueda */
    public indPeriodo: string = '1M';
    public periodos: SelectItem[] = [];
    public lecturaMiembro: boolean; //Permiso de lectura sobre miembro

    constructor(
        public metrica: Metrica,
        public actividadService: ActividadService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public route: ActivatedRoute,
        public i18nService: I18nService,
        public authGuard: AuthGuard,
        public router: Router
    ) {
        //Permite saber si el usuario tiene permisos de lectura sobre miembro
        this.authGuard.canWrite(PERM_LECT_MIEMBROS).subscribe((permiso: boolean) => {
            this.lecturaMiembro = permiso;
        });
    }

    ngOnInit() {
        //Se obtienen los valores predeterminados de los períodos de actividad
        this.periodos = this.i18nService.getLabels("general-actividad-periodos");
        this.route.parent.params.subscribe((params) => {
            if (params['id']) {
                this.metrica.idMetrica = params['id'];
                this.obtenerActividad();
            }
        });
    }

    /**
     * Obtiene los registros de actividad para el metrica actual
     */
    obtenerActividad() {
        this.kc.getToken().then((tkn) => {
            this.actividadService.token = tkn;
            let sub = this.actividadService.getActividadesMetrica(this.metrica.idMetrica, this.indPeriodo).subscribe(
                (response: RegistroActividad[]) => {
                    this.listaActividades = response;
                }, (error: Response) => {
                    this.handleError(error);
                }, () => {
                    sub.unsubscribe();
                }
            );
        }).catch();
    }
    /**
     * Redirige al administrador a la pantalla de dashboard para el elemento seleccionado
     */
    gotoDetail(idMiembro: string) {
        this.router.navigate(['/miembros/detalleMiembro', idMiembro]);
    }
    /**
     * Maneja el erro a nivel de componente
     */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}