import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MetricaService, EstadisticaService } from '../../service/index';
import { Metrica } from '../../model/index';
import { KeycloakService } from '../../service/index';

@Component({
    moduleId: module.id,
    selector: 'metrica-dashboard',
    templateUrl: 'metrica-dashboard.component.html',
    providers: [EstadisticaService]
})

/**
 * Componente utilizado para generar el dashboard de una métrica específica
 */
export class MetricaDashboardComponent implements OnInit {
    public statsData: {
        idMetrica: string,
        nombreMetrica: string,
        nombreInternoMetrica: string,
        cantAcumulada: number,
        cantRedimida: number,
        cantVencida: number,
        periodos: [
            {
                mes: string,
                cantAcumulada: number,
                cantRedimida: number,
                cantVencida: number
            },
            {
                mes: string,
                cantAcumulada: number,
                cantRedimida: number,
                cantVencida: number
            },
            {
                mes: string,
                cantAcumulada: number,
                cantRedimida: number,
                cantVencida: number
            },
            {
                mes: string,
                cantAcumulada: number,
                cantRedimida: number,
                cantVencida: number
            }
        ]
    };
    public chartVistasData: any;
    public options: any;

    constructor(
        public metrica: Metrica,
        public kc: KeycloakService,
        public metricaService: MetricaService,
        private i18nService: I18nService,
        public estadisticaService: EstadisticaService,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService) {

    }

    ngOnInit() {
        this.obtenerMetrica();

    }

    //Se llenan los graficos con los datos obtenidos
    poblarGraficos() {
        this.statsData.periodos.reverse();
        this.chartVistasData = {
            labels: this.statsData.periodos.map((element => element.mes)),
            datasets: [
                {
                    label: this.i18nService.getLabels("metricas-dashboard-cant-acumulada"),
                    data: this.statsData.periodos.map((element => element.cantAcumulada)),
                    fill: false,
                    borderColor: '#4bc0c0'
                },
                {
                    label: this.i18nService.getLabels("metricas-dashboard-cant-redimida"),
                    data: this.statsData.periodos.map((element => element.cantRedimida)),
                    fill: false,
                    borderColor: '#afc0c0'
                },
                {
                    label: this.i18nService.getLabels("metricas-dashboard-cant-vencida"),
                    data: this.statsData.periodos.map((element => element.cantVencida)),
                    fill: false,
                    borderColor: '#f3c0c0'
                },
            ]
        }

    }

    //Método que obtiene las estadisticas de las métricas
    obtenerDatosEstadistica() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.estadisticaService.token = tkn;
                let sub = this.estadisticaService.getEstadisticasMetrica(this.metrica.idMetrica).subscribe(
                    (responseData: any) => {
                        this.statsData = responseData;
                        this.poblarGraficos();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /**Obtiene una metrica a traves del API */
    obtenerMetrica() {
        this.route.parent.params.subscribe((params) => {
            if (params.id) {
                this.kc.getToken().then(
                    (tkn: string) => {
                        this.metricaService.token = tkn;
                        let sub = this.metricaService.getMetrica(params.id).subscribe(
                            (chartData: Metrica) => {
                                this.copyValuesOf(this.metrica, chartData);
                                this.obtenerDatosEstadistica();
                            }, (error) => {
                                this.manejaError(error);
                                this.router.navigate(["/metricas"]);
                            }, () => {
                                sub.unsubscribe();
                            }
                        );
                    });
            } else {
                this.router.navigate(["/metricas"]);
            }
        })

    }

    selectData(event) {

    }
    /**
   * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
   * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
   * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
   * no siempre sea necesario llamar al api para actualizar los datos
   */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }


}