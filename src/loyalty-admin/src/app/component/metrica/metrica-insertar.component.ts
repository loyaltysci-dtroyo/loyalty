import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Metrica, GrupoNivel, Nivel } from '../../model/index';
import { MetricaService, NivelService, KeycloakService, I18nService } from '../../service/index';
import { SelectItem } from 'primeng/primeng';
import { PERM_ESCR_METRICAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'metrica-insertar-component',
    templateUrl: 'metrica-insertar.component.html',
    providers: [Metrica, GrupoNivel, NivelService, Nivel, MetricaService]
})

/** 
 * Jaylin Centeno
 * Componente que permite insertar métricas
 */
export class MetricaInsertarComponent implements OnInit {

    public escritura: boolean; //Permite verificar si el usuario tiene permisos (true/false)
    public formMetrica: FormGroup; //Form usado para validar y asignar valores por default
    public vencimiento: number; //Cantidad de días de vencimiento de la métrica
    public tipoMetrica: SelectItem[]; //Listas con los tipos de métrica
    public nombreExiste: boolean; //Validación del nombre interno
    
    public grupoItem: SelectItem[]; //Lista con los grupos a mostrar en el Dropdown
    public listaGruposNiveles: GrupoNivel[]; //Lista de los grupos existentes en el sistema
    public listaVacia: boolean = false; //Verifica si la lista esta vacia
    public grupoNivel: GrupoNivel; //Grupo de niveles seleccionado
    public idGrupo: string; //Id del grupo
  

    constructor(
        public metrica: Metrica,
        public nivelService: NivelService,
        public metricaService: MetricaService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_METRICAS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //inicialización del form group
        this.formMetrica = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'nombreInterno': new FormControl('', Validators.required),
            'medida': new FormControl('', Validators.required),
            'radioExpiracion': new FormControl('false', Validators.required),
            'diasVencimiento': new FormControl('0', Validators.required),
            'grupoNivel': new FormControl('', Validators.required)
        });
        this.tipoMetrica = [];
        this.tipoMetrica = this.i18nService.getLabels("metrica-medida-tipo");
    }

    ngOnInit() {
        this.obtenerGruposNiveles();
    }

    //Método que se encarga de insertar una  métrica
    agregarMetrica() {
        if (this.nombreExiste == true) {
            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
            return;
        } else {
            if (!this.metrica.indExpiracion) {
                this.metrica.diasVencimiento = 0;
            } else {
                this.metrica.diasVencimiento = this.vencimiento;
            }
            this.metrica.grupoNiveles = this.grupoNivel;
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.metricaService.token = token || "";
                    this.metricaService.addMetrica(this.metrica).subscribe(
                        (data) => {
                            let link = ['metricas/metricaDetalle', data];
                            this.router.navigate(link);
                        },
                        (error) => {
                            this.manejaError(error)
                        }
                    );
                }
            );
        }
    }

    //Establece el nombre interno a partir del nombre de la métrica
    establecerNombres() {
        if (this.metrica.nombre != "" && this.metrica.nombre != null) {
            let temp = this.metrica.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.metrica.nombreInterno = temp;
            this.validarNombre();
        }
    }

    //Método encargado de validar la existencia del nombre interno de un producto
    validarNombre() {
        if (this.metrica.nombreInterno == null && this.metrica.nombreInterno.trim() == "") {
            this.nombreExiste = true;
            return;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.metricaService.token = token;
                this.metricaService.verificarNombreInterno(this.metrica.nombreInterno.trim()).subscribe(
                    (data) => {
                        this.nombreExiste = data;
                        if (this.nombreExiste == true) {
                            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
                            return;
                        }
                    }
                );
            }
        );
    }

    //Se obtiene los grupos de niveles disponibles 
    obtenerGruposNiveles() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.nivelService.token = token || "";
                this.nivelService.obtenertListaGrupoNivel().subscribe(
                    (data) => {
                        this.listaGruposNiveles = data;
                        this.grupoItem = [];
                        this.grupoItem=[...this.grupoItem,this.i18nService.getLabels('general-default-select')];
                        if (this.listaGruposNiveles.length > 0) {
                            this.listaVacia = false;
                            this.listaGruposNiveles.forEach(grupo => {
                                this.grupoItem=[...this.grupoItem,{ label: grupo.nombre, value: grupo }];
                            });
                        } else {
                            this.listaVacia = true;
                        }
                    }
                )
            }
        );
    }

    //redirecciona a la lista de metricas
    goBack() {
        let link = ['metricas'];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
