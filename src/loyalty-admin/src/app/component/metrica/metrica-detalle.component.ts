import { Component, OnInit } from '@angular/core';
import { Metrica, Nivel, GrupoNivel } from '../../model/index';
import { MetricaService, NivelService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { PERM_ESCR_METRICAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'metrica-detalle-component',
    templateUrl: 'metrica-detalle.component.html',
    providers: [MetricaService, NivelService]
})

/**
 * Jaylin Centeno
 * Componente encargado de mostrar el detalle de la métrica*/
export class MetricaDetalleComponent implements OnInit {

    //Variable temporal del nombre interno de la métrica
    public nombreTemporal: string;
    // Form group para validar la métrica
    public formMetrica: FormGroup;
    //--Grupos de niveles --//
    public listaGruposNiveles: GrupoNivel[];
    public listaVacia: boolean = true;
    public grupoItem: SelectItem[];
    public accion: string;
    public estado: string;
    public grupoNivel: GrupoNivel;
    public nombreGrupo: string;
    public vencimiento: number;
    public escritura: boolean;//Permiso del usuario
    //Estado de la métrica
    public publicado: boolean;
    public borrador: boolean;
    public archivado: boolean;
    //Validación del nombre interno
    public nombreExiste: boolean;
    public cargandoDatos: boolean = true;
    public eliminar: boolean;

    constructor(
        public router: Router,
        public route: ActivatedRoute,
        public metrica: Metrica,
        public metricaService: MetricaService,
        public authGuard: AuthGuard,
        public nivelService: NivelService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        //El id de la métrica se inicializa con el valor introducido por la url
        this.route.parent.params.forEach((params: Params) => { this.metrica.idMetrica = params['id'] });

        //Validacion del formMetrica
        this.formMetrica = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'nombreInterno': new FormControl('', Validators.required),
            'radioExpiracion': new FormControl('', Validators.required),
            'diasVencimiento': new FormControl('0', Validators.required),
            'grupo': new FormControl('', Validators.required)
        });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_METRICAS).subscribe(
            (permiso: boolean) => {
                this.escritura = permiso;
            });
    }

     /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    //Inicialización
    ngOnInit() {
        this.mostrarDetalle();
        this.obtenerGruposNiveles();
    }

    //Método que se encarga traer el detalle de la métrica
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.metricaService.token = token || "";
                this.metricaService.getMetrica(this.metrica.idMetrica).subscribe(
                    (data) => {
                        this.copyValuesOf(this.metrica, data);
                        this.vencimiento = this.metrica.diasVencimiento;
                        this.nombreTemporal = this.metrica.nombreInterno;
                        this.estado = this.metrica.indEstado;
                        this.grupoNivel = this.metrica.grupoNiveles;
                        if (this.metrica.indEstado == 'P') {
                            this.publicado = true;
                        } else if (this.metrica.indEstado == 'B') {
                            this.borrador = true;
                        } else {
                            this.archivado = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    //Obtiene los grupos de niveles disponibles
    obtenerGruposNiveles() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.nivelService.token = token || "";
                this.nivelService.obtenertListaGrupoNivel().subscribe(
                    (data) => {
                        this.listaGruposNiveles = data;
                        this.grupoItem = [];
                        this.grupoItem=[...this.grupoItem,this.i18nService.getLabels('general-default-select')];
                        if (this.listaGruposNiveles.length > 0) {
                            this.listaVacia = false;
                            this.listaGruposNiveles.forEach(grupo => {
                                this.grupoItem=[...this.grupoItem,{ label: grupo.nombre, value: grupo }];
                            });
                        } else {
                            this.listaVacia = true;
                        }
                    }
                )
            }
        );
    }

    
    //establece el nombre interno a partir del nombre de la metrica
    establecerNombres() {
        if (this.metrica.nombre != "" || this.metrica.nombre != null) {
            let temp = this.metrica.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.metrica.nombreInterno = temp;
            this.validarNombre();
        }
    }

    //Método encargado de validar la existencia del nombre interno de una métrica
    validarNombre() {
        if (this.metrica.nombreInterno == "" && this.metrica.nombreInterno == null) {
            this.nombreExiste = true;
            return;
        }
        //Si el nombre sigue siendo el mismo no se llama al servicio
        if (this.metrica.nombreInterno != this.nombreTemporal) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.metricaService.token = token || "";
                    this.metricaService.verificarNombreInterno(this.metrica.nombreInterno).subscribe(
                        (data) => {
                            this.nombreExiste = data;
                             if (this.nombreExiste == true) {
                                this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
                                return;
                            }
                        },
                        (error) => {
                            this.manejaError(error);
                        }
                    );
                }
            );
        }
    }

    //Método que se encarga de remover/archivar la métrica
    removerMetrica() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.metricaService.token = token || "";
                this.metricaService.deleteMetrica(this.metrica.idMetrica).subscribe(
                    (data) => {
                        if (this.accion == "archivar") {
                            this.msgService.showMessage(this.i18nService.getLabels('general-archivar'));
                            this.archivado = true;
                            this.publicado = this.borrador = false;
                        } else {
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                            this.goBack();
                        }
                    },
                    (error) => {
                        this.manejaError(error);
                    }
                );
            }
        );
    }

    //Método que se encarga de actualizar la métrica
    actualizarMetrica() {
        if (this.nombreExiste == true) {
            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
            return;
        } else {
            if (!this.metrica.indExpiracion) {
                this.metrica.diasVencimiento = 0;
            }
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.metricaService.token = token || "";
                    this.metricaService.updateMetrica(this.metrica).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                            this.mostrarDetalle();
                        },
                        (error) => {
                            this.manejaError(error);
                        }
                    );
                }
            );
        }
    }

    //Permite publicar la metrica siempre y cuando se encuentre en estado borrador
    publicarMetrica() {
        if (this.nombreExiste == true) {
            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
            return;
        } else {
            this.metrica.indEstado = 'P';
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.metricaService.token = token || "";
                    this.metricaService.updateMetrica(this.metrica).subscribe(
                        (data) => {
                            this.publicado = true;
                            this.archivado = this.borrador = false;
                            this.msgService.showMessage(this.i18nService.getLabels('general-publicar'));
                            this.mostrarDetalle();
                        },
                        (error) => {
                            this.manejaError(error);
                        }
                    );
                }
            );
        }
    }

    //Método que permite regresar en la navegación
    goBack() {
        let link = ['metricas'];
        this.router.navigate(link);
    }

    // Método para manejar los errores que se provocan en las transacciones
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}

