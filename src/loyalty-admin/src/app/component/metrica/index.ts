export { MetricasComponent } from './metricas.component';
export { MetricaListaComponent } from './metrica-lista.component';
export { MetricaInsertarComponent } from './metrica-insertar.component';
export { MetricaDetalleComponent } from './metrica-detalle.component';
export { MetricaDashboardComponent } from './metrica-dashboard.component';
export { MetricaActividadComponent } from './metrica-actividad.component';
export { MetricaDetalleGeneralComponent } from './metrica-detalle-general.component';
export { MetricaDashboardGeneralComponent } from './metrica-dashboard-general.component';