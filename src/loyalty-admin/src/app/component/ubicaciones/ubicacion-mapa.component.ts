import { Component, OnInit } from '@angular/core';
import { Ubicacion } from '../../model/index';
import { UbicacionService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { PERM_ESCR_UBICACIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

declare var google: any;
@Component({
    moduleId: module.id,
    selector: 'ubicacion-mapa-component',
    styleUrls: ['mapas.css'],
    templateUrl: 'ubicacion-mapa.component.html',
})
/**
 * Jaylin Centeno
 * Componente que muestra el mapa y la dirección de la ubicación
 * Para más información del Geocoder https://developers.google.com/maps/documentation/javascript/geocoding
 * Para utilizar el autocompletado Google Places API Web Service debe estar habilitado en la Google API Console (en el mismo proyecto que se configuro para la Google Maps JavaScript API).
 * El servicio Places es una biblioteca independiente, separada del código principal Maps JavaScript API. 
 * Para usar la funcionalidad contenida en la biblioteca, primero debe cargarla usando el parámetro libraries=placces en la URL de Maps API del index.html
 * Idioma de las direcciones que devuelve el geocodificador: 
 * idioma preferida del navegador o el especificado al cargar el código de JavaScript de la API mediante el parámetro language
 */
export class UbicacionMapaComponent implements OnInit {

    public escritura: boolean; //Permiso de escritura (true/false)
    public formUbicacion: FormGroup; //Formulario de la ubicación    
    public geocoder: any; //Representa el servicio de geocodificación
    public map: any; //Representa el mapa a desplegar
    public infoWindow: any; //Representa la ventana de informacion del mapa
    public autocomplete: any; //Representa el input de autocompletar
    public marker: any; //Representa el marcador a desplegar en el mapa
    public latlng = {
        lat: 0,
        lng: 0,
    } //Objeto que contiene la atitud y longitud de la ubicación, se le pasa al geocode()
    public componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'short_name',
        postal_code: 'short_name'
    }; //Contiene la información de la dirección buscada

    constructor(
        public ubicacion: Ubicacion,
        public ubicacionService: UbicacionService,
        public msgService: MessagesService,
        public router: Router,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,
        public i18nService: I18nService,
        public keycloakService: KeycloakService
    ) {
        this.formUbicacion = new FormGroup({
            'indDirCiudad': new FormControl('', Validators.required),
            'indDirEstado': new FormControl(''),
            'indDirPais': new FormControl(''),
            'direccion': new FormControl('', Validators.required),
        });
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_UBICACIONES).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.geocoder = new google.maps.Geocoder; //Se inicializa el servicio de geocodificación
        this.latlng = { lat: 0, lng: 0 }; //Posición en la que se va ubicar el mapa
        this.infoWindow = new google.maps.InfoWindow; //Representa ventana de información desplegada en el mapa
    }

    //Inicializacion
    ngOnInit() {
        //Opciones del mapa
        let options = {
            center: this.latlng,
            mapTypeId: google.maps.MapTypeId.terrain,
            zoom: 25
        };
        //Se inicializa la carga del mapa con las coordenadas dadas
        this.map = new google.maps.Map(document.getElementById('map'), options);
        this.mostrarDetalle();
        this.marcadorAutocompletar();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
     * Obtiene la definición de la ubicación
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                let sub = this.ubicacionService.obtenerUbicacionId(this.ubicacion.idUbicacion).subscribe(
                    (data) => {
                        this.copyValuesOf(this.ubicacion, data);
                        this.latlng = { lat: this.ubicacion.dirLat, lng: this.ubicacion.dirLng } //contiene la atitud y longitud de la ubicación
                        this.geocodeLatLng(this.geocoder, this.map);//Se pasa por parámetros el objeto de geocoder y el map
                    },
                    (error) => {
                        this.manejaError(error)
                    },
                    () => { sub.unsubscribe(); }
                );
            }
        );
    }
    /**
     * Permite editar la definición de la ubicación
     */
    editarUbicacion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                this.ubicacionService.editarUbicacion(this.ubicacion).subscribe(
                    (data) => {
                        this.marker.setIcon(null); //Se remueven los marcadores
                        this.infoWindow.close(); //Se cierra la ventana de información
                        this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                        this.mostrarDetalle();
                    },
                    (error) => { this.manejaError(error);}
                );
            }
        );
    }
    /**
     * Método para crear marcadores en el mapa
     * Un marcador identifica una posición en el mapa
     * google.maps.Marker es el constructor del marcador
     */
    mapMarker(location) {
        //Propiedades del marcador
        let options = {
            position: location, // (obligatorio) especifica un objeto LatLng que identifica la ubicación inicial del marcador. 
            map: this.map //(opcional) especifica el Map en el cual debe ubicarse el marcador, sino se utiliza el método setMap()
        };
        this.marker = new google.maps.Marker(options);
        //this.marker.setMap(this.map) // No hace falta utilizarlo ya que se en options se especifica el Map
    }

    /**
     * Método que hace uso del servicio de geocodificación
     * Utilizado para convertir direcciones en coordenadas geográficas (aquí son usadas para poner los marcadores y posicionar en el mapa)
     * Estado devuelto en el servicio -> status:OK, ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED, INVALID_REQUEST, UNKNOW_ERROR
     */
    geocodeLatLng(geocoder, map) {
        //inicia una solicitud dirigida al servicio de geocodificación
        //location es un parametro obligatorio
        //se debe ejecutar el metodo callback al recuperar los resultados, los parametros son results y status 
        geocoder.geocode({ 'location': this.latlng }, (results, status) => {
            if (status == 'OK') {
                //results[1] = partial_match
                if (results[1]) {
                    //Se crea el mensaje a desplegar en la ventana de informacion del mapa
                    var contentString = '<div id="content"><div id="siteNotice">' +
                        '</div>' + '<h3 id="firstHeading" class="firstHeading">' + this.ubicacion.nombre + '</h3>' +
                        '<div id="bodyContent">' +
                        '<p><b>Dirección: </b>' + this.ubicacion.direccion + '</p>' +
                        '</div></div>';

                    //geometry.location devuelve un objeto LatLn utilizado para centrar el Map 
                    map.setCenter(results[0].geometry.location);
                    map.setZoom(17);
                    this.mapMarker(results[0].geometry.location);
                    this.infoWindow.setContent(contentString);
                    this.infoWindow.open(map, this.marker);

                } else {
                    this.msgService.showMessage(this.i18nService.getLabels('warn-ubicacion-resultado'));
                }
            } else {
                let mensaje = this.i18nService.getLabels('warn-ubicacion-geocoder');
                mensaje.detail = + status;
                this.msgService.showMessage(mensaje);
            }
        });
    }
    /**
     * El autocompletado es una función de la biblioteca de Places de la Google Maps JavaScript API. 
     * Se utiliza para dar el comportamiento de escritura anticipada en el campo de búsqueda de Google Maps. 
     * Cuando un usuario comienza a escribir una dirección, la función de autocompletado termina la tarea.
     */
    marcadorAutocompletar() {
        var input = document.getElementById('pac-input'); //Se obtiene el valor del pac-input
        this.autocomplete = new google.maps.places.Autocomplete(input); // Se crea un objeto autocomplete y se asocia con el control input
        this.autocomplete.bindTo('bounds', this.map); //El método bindTo() es para restringir los resultados al viewport del mapa, aún cuando este cambie.

        //El servicio autocomplete (al ser seleccionada una de las predicciones) activa un evento place_changed.
        this.autocomplete.addListener('place_changed', () => {
            this.infoWindow.close(); //se cierra la ventana de información
            this.marker.setVisible(false); //Se oculta el marcador
            var place = this.autocomplete.getPlace(); // Al llamar el método getPlace() en el objeto Autocomplete permite recuperar un objeto PlaceResult.
            //place.geometry da información relacionada con geométricos
            /** location proporciona la latitud y longitud
             * viewport define el viewport preferido en el mapa al visualizar el sitio*/
            if (!place.geometry) {
                // El usuario ingreso un nombre de lugar que no tiene sugerencias y presiono Enter
                // o la peticion para obtener el Place Details fallo
                this.msgService.showMessage(this.i18nService.getLabels("warn-ubicacion-resultado"));
                return;
            }

            // Si el lugar tiene una geometría lo presenta en el mapa
            if (place.geometry.viewport) {
                this.map.fitBounds(place.geometry.viewport);
            }

                this.map.setCenter(place.geometry.location);
                this.map.setZoom(17);
            
            //Llamado al método para mostrar el marcador
            this.mapMarker(place.geometry.location);
            this.marker.setVisible(true);

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (this.componentForm[addressType]) {
                    var val = place.address_components[i][this.componentForm[addressType]];
                    switch (addressType) {
                        case "locality": this.ubicacion.indDirCiudad = val;
                            break;
                        case "administrative_area_level_1": this.ubicacion.indDirEstado = val;
                            break;
                        case "country": this.ubicacion.indDirPais = val;
                            break;
                        default: break;
                    }
                }
            }
            this.ubicacion.direccion = place.formatted_address;
            this.ubicacion.dirLat = place.geometry.location.lat();
            this.ubicacion.dirLng = place.geometry.location.lng();
            var contentString = '<div id="content"><div id="siteNotice">' +
                '</div>' + '<h3 id="firstHeading" class="firstHeading">' + this.ubicacion.nombre + '</h3>' +
                '<div id="bodyContent">' +
                '<p><b>Dirección: </b>' + this.ubicacion.direccion + '</p>' +
                '</div></div>';
            //Se muestra el nombre y la dirección del lugar
            this.infoWindow.setContent(contentString);
            this.infoWindow.open(this.map, this.marker);
        });
    }
    /** 
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error)); 7
    }
}