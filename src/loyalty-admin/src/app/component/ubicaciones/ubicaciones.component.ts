import { Beacon } from '../../model/beacon';
import {Component}  from '@angular/core';

@Component({
    moduleId: module.id,
    selector:'ubicaciones-component',
    templateUrl:'ubicaciones.component.html',
    providers:[Beacon]
})
/**
 * Jaylin Centeno
 * Componente principal de ubicaciones
 */
export class UbicacionesComponent  {

    constructor(){}
    
}