export { UbicacionesComponent } from './ubicaciones.component';
export { UbicacionListaComponent } from './ubicacion-lista.component';
export { UbicacionInsertarComponent } from './ubicacion-insertar.component';
export { UbicacionDetalleComponent} from './ubicacion-detalle.component';
export { UbicacionInformacionComponent} from './ubicacion-informacion.component';
export { UbicacionEfectividadComponent } from './ubicacion-efectividad.component';
export { UbicacionMapaComponent } from './ubicacion-mapa.component';
export { BeaconComponent } from  './beacons/beacons.component';