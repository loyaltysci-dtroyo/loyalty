import { Component, OnInit } from '@angular/core';
import { Ubicacion } from '../../model/index';
import { UbicacionService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PERM_ESCR_UBICACIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Response } from '@angular/http'
import * as Rutas from '../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'ubicacion-lista-component',
    templateUrl: 'ubicacion-lista.component.html',
    providers: [Ubicacion, UbicacionService]
})
/**
 * Jaylin Centeno
 * Componente que muestra la lista de ubicaciones disponibles y permite ir al detalle de las mismas
 */
export class UbicacionListaComponent {

    public escritura: boolean = true; //Permiso de escritura (true/false)
    public eliminar: boolean; //Habilita el cuadro de dialogo para remover/archivar
    public publicada: boolean; //Maneja el tipo de estado de la ubicacion seleccionada
    public cargandoDatos: boolean = true; //Muestra la carga de datos
    public remover: string; //Mensaje dialogo confirmacion
    public archivar: string; //Mensaje dialogo confirmacion

    // --- Ubicaciones --- //
    public idUbicacion: string; //Id de la ubicación seleccionada
    public listaUbicaciones: Ubicacion[]; //Lista de ubicaciones existentes
    public listaVacia: boolean = true; //Para verificar si la lista esta vacia
    public cantidad: number = 10; //Cantidad de filas
    public totalRecords: number; //Total de ubicaciones en el sistema
    public filaDesplazamiento: number; //Desplazamiento de la primera fila

    // --- Búsqueda --- //
    public busqueda: string;
    public listaBusqueda: string[] = [];
    public avanzada: boolean = false;
    public estado: string[] = ['P', 'D'];

    constructor(
        public ubicacion: Ubicacion,
        public ubicacionService: UbicacionService,
        public router: Router,
        public authGuard: AuthGuard,
        public i18nService: I18nService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_UBICACIONES).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        this.remover = this.i18nService.getLabels("confirmacion-pregunta")["B"];
        this.archivar = this.i18nService.getLabels("confirmacion-pregunta")["A"];
    }

    //Inicializacion
    ngOnInit() {
        this.busquedaUbicaciones();
    }

    /**
     * Método utilizado para la carga de datos de manera perezosa
     */
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaUbicaciones();
    }

    /** 
     * Método para mostrar el dialogo de confirmación
     */
    dialogoConfirmar(idUbicacion: string, estado: string) {
        if (estado == "P" || estado == "I") {
            this.publicada = true;
        } else {
            this.publicada = false;
        }
        this.idUbicacion = idUbicacion;
        this.eliminar = true;
    }

    /**
     * Facilita la busqueda de las ubicaciones que cumplan con los parámetros proporcionados
     * Estado = publicado, inactivo, borrador o archivado 
     * Y los terminos de busqueda 
     */
    busquedaUbicaciones() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                this.ubicacionService.busquedaUbicacion(this.estado, this.cantidad, this.filaDesplazamiento, this.busqueda).subscribe(
                    (response: Response) => {
                        this.cargandoDatos = false;
                        this.listaUbicaciones = <Ubicacion[]>response.json();
                        this.totalRecords = +response.headers.get("Content-Range").split("/")[1];
                        if (this.listaUbicaciones.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                    },
                    (error) => { this.manejaError(error); this.cargandoDatos = false; }
                );
            }
        );
    }

    /**
     * Método para ir a la pantalla de ingreso
     */ 
    nuevaUbicacion() {
        this.router.navigate([Rutas.RT_UBICACIONES_INSERTAR]);
    }
    /**
     * Redirecciona al detalle de la ubicación seleccionada
     */
    ubicacionSeleccionada(idUbicacion: string) {
        this.router.navigate([Rutas.RT_UBICACIONES_DETALLE, idUbicacion]);
    }

    /**
     * Remueve o archiva una ubicación según el estado
     */
    removerUbicacion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                this.ubicacionService.removerUbicacion(this.idUbicacion).subscribe(
                    (data) => {
                        let index = this.listaUbicaciones.findIndex(element => element.idUbicacion == this.idUbicacion );
                        this.listaUbicaciones.splice(index, 1);
                        this.listaUbicaciones=[...this.listaUbicaciones];
                        //this.busquedaUbicaciones();
                        this.eliminar = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));

                    },
                    (error) => { this.manejaError(error); this.eliminar = false; }
                );
            }
        );
    }

    /**
     * Metodo para manejar los errores que se provocan en las transacciones
     */ 
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}