import { Beacon } from '../../../model/beacon';
import { Component, OnInit } from '@angular/core';
import { MessagesService } from '../../../utils';
import { I18nService } from '../../../service';
@Component({
    selector: 'beacons-component',
    templateUrl: './beacons.component.html'
})
export class BeaconComponent implements OnInit {
    public terminosBusqueda:string="";
    public displayModalInsertar = false;
    public displayModalEditar = false;
    public listaBeacons: Beacon[]=[]

    constructor(public beacon:Beacon, public msgService:MessagesService,public i18nService:I18nService ) {

    }

    ngOnInit() {

    }

    public buscarBeacons(){
    
    }
    public getBeacons() {
        return this.listaBeacons;
    }

    public insertarBeacon(beacon: Beacon) {
        this.listaBeacons= [beacon,...this.listaBeacons];
        this.beacon=new Beacon();
        this.displayModalInsertar=false;
        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
    }

    public eliminarBeacon(beacon: Beacon) {
        this.listaBeacons = this.listaBeacons.splice(0, 1, beacon);
        this.beacon=new Beacon();
    }

    public modificarBeacon(beacon: Beacon) {
        this.listaBeacons = this.listaBeacons.map((b: Beacon) => {
            if (b.getUUID() == beacon.getUUID()) {
                return beacon;
            }
            else {
                return b;
            }
        });
        this.beacon=new Beacon();
        this.displayModalEditar=false;
        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
    }

}
