import { Component, OnInit } from '@angular/core';
import { Ubicacion } from '../../model/index';
import { UbicacionService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MenuItem } from 'primeng/primeng';
import { PERM_ESCR_UBICACIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';

declare var google: any;
@Component({
    moduleId: module.id,
    selector: 'ubicacion-detalle-component',
    styleUrls: ['mapas.css'],
    templateUrl: 'ubicacion-detalle.component.html',
    providers: [Ubicacion, UbicacionService],

})

/**
 * Jaylin Centeno
 * Componente que muestra y permite modificar la información general de la ubicación
 */
export class UbicacionDetalleComponent implements OnInit {

    public escritura: boolean = true; //Permite de escritura
    public cargandoDatos: boolean = true; //Muestra la carga de datos
    public verModal: boolean; //Habilita el cuadro de dialogo para remover o cambiar de estado
    public eliminar: boolean = false; //Permite definir la acción sobre la ubicación 
    public ubicActiva: boolean; //valida si se desea activar o inactiva la ubicacion
    public estado: string; //Se guarda el estado de la ubicación
    public tipo: string; //Activa o Inactiva
    //Según el estado, permite manejar el estilo del div
    public publicado: boolean;
    public inactivo: boolean;
    public draft: boolean;
    public archivado: boolean;
    //Items del menú
    public informacion = Rutas.RT_UBICACIONES_DETALLE_INFORMACION; //Ruta dinámica
    public efectividad = Rutas.RT_UBICACIONES_DETALLE_EFECTIVIDAD; //Ruta dinámica
    public mapa = Rutas.RT_UBICACIONES_DETALLE_MAPA; //Ruta dinámica

    constructor(
        public ubicacion: Ubicacion,
        public ubicacionService: UbicacionService,
        public router: Router,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,
        public i18nService: I18nService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService
    ) {
        //El id de la ubicación pasado por url
        this.route.params.forEach((params: Params) => { this.ubicacion.idUbicacion = params['id'] });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_UBICACIONES).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    //Inicializacion
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /** 
     * Método para mostrar el dialogo de confirmación
     */
    confirmar(accion?: string){
        if(accion == "remover"){
            this.eliminar = true;
        }
        this.verModal = true;
    }
    /**
     * Obtiene la definición de la ubicación
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                let sub = this.ubicacionService.obtenerUbicacionId(this.ubicacion.idUbicacion).subscribe(
                    (data) => {
                        this.copyValuesOf(this.ubicacion, data);
                        this.estado = this.ubicacion.indEstado;
                        if (this.ubicacion.indEstado == 'P') {
                            this.publicado = true;
                            this.ubicActiva = true;
                            this.tipo ='Activo';
                        } else if (this.ubicacion.indEstado == 'D') {
                            this.draft = true;
                        } else if (this.ubicacion.indEstado == 'I') {
                            this.inactivo = true;
                            this.ubicActiva = false;
                            this.tipo ='Inactivo';
                        } else {
                            this.archivado = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => { this.manejaError(error); this.cargandoDatos = false;},
                    () => { sub.unsubscribe(); this.cargandoDatos = false;}
                );
            }
        );
    }

    /**
     * Para remover o archivar una ubicación según el estado
     */
    removerUbicacion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                this.ubicacionService.removerUbicacion(this.ubicacion.idUbicacion).subscribe(
                    (data) => {
                        this.verModal = false;
                        if (this.estado == 'P' || this.estado == 'I') {
                            this.archivado = true;
                            this.publicado = this.inactivo = false;
                            this.ubicacion.indEstado = "A";
                            this.msgService.showMessage(this.i18nService.getLabels("general-archivar"));
                            this.eliminar = false;
                        } else {
                            this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                            this.goBack();
                        }

                    },
                    (error) => {
                        this.manejaError(error);
                        this.ubicacion.indEstado = this.estado;
                        if (this.estado == 'P') {
                            this.publicado = true;
                        } else if (this.estado = 'I') {
                            this.inactivo = true;
                        }
                        this.archivado = false;
                    }
                );
            }
        );
    }

    /**
     * Se realiza el cambio del estado de una ubicación
     */
    estadoActivo() {
        if (this.ubicActiva) {
            this.ubicacion.indEstado = 'I'
        } else {
            this.ubicacion.indEstado = 'P'
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                this.ubicacionService.editarUbicacion(this.ubicacion).subscribe(
                    (data) => {
                        if (this.estado == "P") {
                            this.msgService.showMessage({ detail: "La ubicación se ha inactivado.", severity: "success", summary: "" });
                            this.publicado = false;
                            this.inactivo = true;
                        }
                        if (this.estado == "I") {
                            this.msgService.showMessage(this.i18nService.getLabels("general-activar"));
                            this.publicado = true;
                            this.inactivo = false;
                        }
                        this.verModal = false;
                        this.mostrarDetalle();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.ubicacion.indEstado = this.estado;
                    });
            });
    }

    /**
     * Método para publicar la ubicacion en estado borrador
     */
    publicarUbicacion() {
        this.ubicacion.indEstado = "P";
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                this.ubicacionService.editarUbicacion(this.ubicacion).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-publicar"));
                        this.publicado = true;
                        this.draft = false;
                        this.mostrarDetalle();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.ubicacion.indEstado = this.estado;
                        this.publicado = false;
                        this.draft = true;
                    });
            });
    }

    /** 
     * Método para volver a la página anterior 
     */
    goBack() {
        this.router.navigate([Rutas.RT_UBICACIONES_LISTA]);
    }
    /** 
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}