import { Component, OnInit } from '@angular/core';
import { Ubicacion } from '../../model/index';
import { UbicacionService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { PERM_ESCR_UBICACIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import * as Rutas from '../../utils/rutas';

declare var google: any;
@Component({
    moduleId: module.id,
    selector: 'ubicacion-insertar-component',
    styleUrls: ['mapas.css'],
    templateUrl: 'ubicacion-insertar.component.html',
    providers: [Ubicacion, UbicacionService],
})

/**
 * Jaylin Centeno
 * Componente que muestra el mapa para localizar una ubicación
 * El servicio Places es una biblioteca independiente, separada del código principal Maps JavaScript API. 
 * Para usar la funcionalidad contenida en la biblioteca, primero debe cargarla usando el parámetro libraries=placces en la URL de Maps API del index.html
 * Idioma de las direcciones que devuelve el geocodificador: 
 * idioma preferida del navegador o el especificado al cargar el código de JavaScript de la API mediante el parámetro language
 */
export class UbicacionInsertarComponent implements OnInit {

    public setUbicacion: boolean = false; //Permite mostrar los campos de direccion
    public escritura: boolean; //Permiso de escritura (true/false)
    public formUbicacion: FormGroup; //Formulario de la ubicación
    public map: any; //Representa el mapa a desplegar
    public infoWindow: any; //Representa la información a mostrar en el mapa
    public autocomplete: any; //Contiene la dirección buscada
    public marker: any; //Representa el marcador a desplegar en el mapa
    public latlng = {
        lat: 9.935697,
        lng: -84.1483645,
    } //Objeto que contiene la atitud y longitud de la ubicación, se le pasa al geocode()
    public componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'short_name',
        postal_code: 'short_name'
    };//Contiene la información de la dirección buscada

    constructor(
        public ubicacion: Ubicacion,
        public ubicacionService: UbicacionService,
        public router: Router,
        public i18nService: I18nService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public authGuard: AuthGuard,
    ) {
        this.formUbicacion = new FormGroup({
            'codigo': new FormControl('', Validators.required),
            'nombre': new FormControl('', Validators.required),
            'nombreDespliegue': new FormControl('', Validators.required),
            'indDirCiudad': new FormControl('', Validators.required),
            'indDirEstado': new FormControl(''),
            'indDirPais': new FormControl(''),
            'direccion': new FormControl('', Validators.required),
            'fechaInicio': new FormControl(''),
            'fechaFin': new FormControl(''),
            'diaRecurrencia': new FormControl(''),
            'semanaRecurrencia': new FormControl('')
        });
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_UBICACIONES).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.infoWindow = new google.maps.InfoWindow; //Representa ventana de información desplegada en el mapa
    }

    //Inicializacion
    ngOnInit() {
        //Opciones del mapa
        let options = {
            center: this.latlng,
            mapTypeId: google.maps.MapTypeId.terrain,
            zoom: 13
        };
        //Se inicializa la carga del mapa con las coordenadas dadas
        this.map = new google.maps.Map(document.getElementById('map'), options);
        this.mapMarker(this.latlng); //Se hace el llamado al método para agregar un marcador
        this.infoWindow.setPosition(this.latlng);//Se le asigna a la ventana una posición en el mapa
        this.infoWindow.setContent('Ubicación encontrada'); //Contenido de la ventana
        this.infoWindow.open(this.map, this.marker);//Se agrega la ventana al mapa, junto con el marcador
        this.marcadorAutocompletar();
    }
    /**
     * Método para crear marcadores en el mapa
     * Un marcador identifica una posición en el mapa
     * google.maps.Marker es el constructor del marcador
     */
    mapMarker(location) {
        //Propiedades del marcador
        let options = {
            position: location, // (obligatorio) especifica un objeto LatLng que identifica la ubicación inicial del marcador. 
            map: this.map //(opcional) especifica el Map en el cual debe ubicarse el marcador, sino se utiliza el método setMap()
        };
        this.marker = new google.maps.Marker(options);
    }

    /**
     * Permite insertar la definición de la ubicación
     */
    agregarUbicacion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                this.ubicacionService.agregarUbicacion(this.ubicacion).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-insercion'));
                        this.router.navigate([Rutas.RT_UBICACIONES_DETALLE, data]);
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }
    /**
     * Permite establecer el nombre de la ubicación
     */
    establecerNombre() {
        if (this.ubicacion.nombre != "" || this.ubicacion.nombre != null) {
            let temp = this.ubicacion.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.ubicacion.nombreDespliegue = temp;
        }
    }

    /**
     * El autocompletado es una función de la biblioteca de Places de la Google Maps JavaScript API. 
     * Se utiliza para dar el comportamiento de escritura anticipada en el campo de búsqueda de Google Maps. 
     * Cuando un usuario comienza a escribir una dirección, la función de autocompletado termina la tarea.
     */
    marcadorAutocompletar() {
        var input = (document.getElementById('pac-input')); //Se obtiene el valor del pac-input//Locación ingresada
        this.autocomplete = new google.maps.places.Autocomplete(input); // Se crea un objeto autocomplete y se asocia con el control input
        this.autocomplete.bindTo('bounds', this.map); //El método bindTo() es para restringir los resultados al viewport del mapa, aún cuando este cambie.

        //El servicio autocomplete (al ser seleccionada una de las predicciones) activa un evento place_changed.
        this.autocomplete.addListener('place_changed', () => {
            this.infoWindow.close(); //se cierra la ventana de información
            this.marker.setVisible(false); //El marcador oculta
            var place = this.autocomplete.getPlace(); // Al llamar el método getPlace() en el objeto Autocomplete permite recuperar un objeto PlaceResult.
            /* place.geometry da información relacionada con geométricos
             * location proporciona la latitud y longitud
             * viewport define el viewport preferido en el mapa al visualizar el sitio*/
            if (!place.geometry) {
                // El usuario ingreso un nombre de lugar que no tiene sugerencias y presiono Enter
                // o la peticion para obtener el Place Details fallo
                let mensaje = this.i18nService.getLabels("warn-ubicacion-resultado");
                this.msgService.showMessage(mensaje);
                return;
            }
            // Si el lugar tiene una geometría lo presenta en el mapa
            if (place.geometry.viewport) {
                this.map.fitBounds(place.geometry.viewport);
            } else {
                this.map.setCenter(place.geometry.location);
                this.map.setZoom(17);
            }
            //Llamado al método para mostrar el marcador
            this.mapMarker(place.geometry.location);
            this.marker.setVisible(true);

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];

                if (this.componentForm[addressType]) {
                    var val = place.address_components[i][this.componentForm[addressType]];
                    switch (addressType) {
                        case "locality": this.ubicacion.indDirCiudad = val;
                            break;
                        case "administrative_area_level_1": this.ubicacion.indDirEstado = val;
                            break;
                        case "country": this.ubicacion.indDirPais = val;
                            break;
                        default: break;
                    }
                }
            }
            this.ubicacion.direccion = place.formatted_address;
            this.ubicacion.dirLat = place.geometry.location.lat();
            this.ubicacion.dirLng = place.geometry.location.lng();
            this.setUbicacion = true;
            var contentString = '<div id="content"><div id="siteNotice">' +
                '</div>' + '<h3 id="firstHeading" class="firstHeading">' + this.ubicacion.nombre + '</h3>' +
                '<div id="bodyContent">' +
                '<p><b>Dirección: </b>' + this.ubicacion.direccion + '</p>' +
                '</div></div>';
            //Se muestra el nombre y la dirección del lugar
            this.infoWindow.setContent(contentString);
            this.infoWindow.open(this.map, this.marker);
        });
    }

    /**
     *  Método que permite regresar en la navegación
     */
    goBack() {
        //window.history.back();
        let link = ['ubicaciones'];
        this.router.navigate(link);
    }
    /**
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}