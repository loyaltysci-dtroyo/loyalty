import { Component, OnInit } from '@angular/core';
import { Ubicacion } from '../../model/index';
import { UbicacionService, KeycloakService, I18nService } from '../../service/index';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { PERM_ESCR_UBICACIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import * as Rutas from '../../utils/rutas';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

declare var google: any;
@Component({
    moduleId: module.id,
    selector: 'ubicacion-efectividad-component',
    styleUrls: ['mapas.css'],
    templateUrl: 'ubicacion-efectividad.component.html',
    providers: [UbicacionService],

})
/**
 * Jaylin Centeno
 * Componente que muestra y permite cambiar la efectividad de la ubicacion
 */
export class UbicacionEfectividadComponent implements OnInit {

    public formEfectividad: FormGroup; //Formulario para la efectividad de ubicación
    public escritura: boolean = true; // Permiso de escritura (true/false)
    public fecFin: Date; //Representa la fecha de fin en la calendarización
    public fecInicio: Date; //Representa la fecha de inicio en la calendarización
    public dias: SelectItem[] = []; //Contiene los días de la semana
    public diasSelec: string[] = []; //Representa los días seleccionados para la calendarización recurrente
    public efectividadItem: SelectItem[]; //Lista tipos efectividad
    public esRecurrente: boolean = false; //Se maneja el estado de recurrencia

    constructor(
        public ubicacion: Ubicacion,
        public ubicacionService: UbicacionService,
        public router: Router,
        public authGuard: AuthGuard,
        public i18nService: I18nService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
    ) {
        //Valida los permisos de escritura del usuario
        this.authGuard.canWrite(PERM_ESCR_UBICACIONES).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.formEfectividad = new FormGroup({
            'indCalendarizacion': new FormControl('true', Validators.required),
            "fechaInicio": new FormControl(''),
            "fechaFin": new FormControl(''),
            "indDiaRecurrencia": new FormControl(''),
            "indSemanaRecurrencia": new FormControl(''),
        });
    }

    //Inicializacion
    ngOnInit() {
        this.efectividadItem = this.i18nService.getLabels("tipo-efectividad-general")
        this.dias = this.i18nService.getLabels("dias-efectividad-general");
        this.mostrarDetalle();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
     * Obtiene la definición de la ubicación
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                let sub = this.ubicacionService.obtenerUbicacionId(this.ubicacion.idUbicacion).subscribe(
                    (data) => {
                        this.copyValuesOf(this.ubicacion, data);
                        //Se convierten las fechas para poder ser utilizadas en el p-calendar
                        if (this.ubicacion.indCalendarizacion == 'C') {
                            this.fecFin = new Date(this.ubicacion.fechaFin);
                            this.fecInicio = new Date(this.ubicacion.fechaInicio);
                            if (this.ubicacion.indDiaRecurrencia != null) {
                                this.diasSelec = this.ubicacion.indDiaRecurrencia.split('');
                                this.esRecurrente = true;
                            }
                        }
                    },
                    (error) => { this.manejaError(error);},
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**
     * Permite actualizar la definición de la ubicación
     */
    actualizarUbicacion() {
        if (this.ubicacion.indCalendarizacion == 'C') {
            if (this.ubicacion.fechaInicio == null || this.ubicacion.fechaFin == null) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-ubicacion-fecha"));
                return;
            }
            if (this.esRecurrente) {
                if (this.diasSelec.length < 1 || this.ubicacion.indSemanaRecurrencia == null) {
                    this.msgService.showMessage(this.i18nService.getLabels("warn-ubicacion-semana"));
                    return;
                }
                this.ubicacion.indDiaRecurrencia = "";
                for (let i = 0; i < this.diasSelec.length; i++) {
                    this.ubicacion.indDiaRecurrencia += this.diasSelec[i];
                }
            }
            if (!this.esRecurrente) {
                this.ubicacion.indSemanaRecurrencia = null;
                this.ubicacion.indDiaRecurrencia = null;
            }
        } else {
            this.ubicacion.fechaFin = null;
            this.ubicacion.fechaInicio = null;
            this.fecFin = null;
            this.fecInicio = null;
            this.ubicacion.indSemanaRecurrencia = null;
            this.ubicacion.indDiaRecurrencia = null;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                this.ubicacionService.editarUbicacion(this.ubicacion).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.mostrarDetalle();
                    },
                    (error) => {
                        this.manejaError(error);
                    });
            });
    }
    /** 
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error)); 7
    }
}