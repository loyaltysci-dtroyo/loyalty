import { Component, OnInit } from '@angular/core';
import { Ubicacion } from '../../model/index';
import { UbicacionService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PERM_ESCR_UBICACIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

declare var google: any;
@Component({
    moduleId: module.id,
    selector: 'ubicacion-informacion-component',
    styleUrls: ['mapas.css'],
    templateUrl: 'ubicacion-informacion.component.html',
})

/**
 * Jaylin Centeno
 * Componente que muestra la información general de la ubicación
 * Permite cambiar el estado y remover una ubicación
 */
export class UbicacionInformacionComponent implements OnInit {
    
    public formUbicacion: FormGroup; //Formulario de la ubicación
    public escritura: boolean; //Permiso de escritura (true/false)
    public cargandoDatos: boolean = true; //Muestra la carga de datos

    constructor(
        public ubicacion: Ubicacion,
        public ubicacionService: UbicacionService,
        public router: Router,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,
        public i18nService: I18nService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService
    ) {
        this.formUbicacion = new FormGroup({
            'codigo': new FormControl('', Validators.required),
            'nombre': new FormControl('', Validators.required),
            'nombreDespliegue': new FormControl('', Validators.required),
            'horario': new FormControl(''),
            'telefono': new FormControl(''),
        });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_UBICACIONES).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    //Inicializacion
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
     * Obtiene la definición de la ubicación
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                let sub = this.ubicacionService.obtenerUbicacionId(this.ubicacion.idUbicacion).subscribe(
                    (data) => {
                        this.copyValuesOf(this.ubicacion, data);
                        this.cargandoDatos = false;
                    },
                    (error) => { this.manejaError(error); this.cargandoDatos = false; },
            () => { sub.unsubscribe(); this.cargandoDatos = false; }
                );
            }
        );
    }
    /**
     * Permite establecer el nombre de la ubicación
     */
    establecerNombre() {
        if (this.ubicacion.nombre != "" || this.ubicacion.nombre != null) {
            let temp = this.ubicacion.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.ubicacion.nombreDespliegue = temp;
        }
    }
    /**
     * Permite editar la definición de la ubicación
     */
    editarUbicacion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.ubicacionService.token = token;
                this.ubicacionService.editarUbicacion(this.ubicacion).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                        this.mostrarDetalle();
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }
    /** 
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));7
    }
}