import {Component}  from '@angular/core'; 
import {Leaderboard,Metrica,Grupo} from '../../model/index';
import {LeaderboardService, MetricaService,GrupoService} from '../../service/index'
@Component({
    moduleId: module.id,
    selector:'leaderboards-component',
    templateUrl:'leaderboards.component.html',
    providers: [Leaderboard, LeaderboardService, Metrica,MetricaService,Grupo, GrupoService]
})

export class LeaderBoardsComponent  {
}