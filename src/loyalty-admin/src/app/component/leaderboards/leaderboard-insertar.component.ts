import { I18nService } from '../../service';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Leaderboard, Metrica, Grupo } from '../../model/index';
import { LeaderboardService, KeycloakService, GrupoService } from '../../service/index';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { MetricaService } from '../../service/metrica.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import * as Routes from '../../utils/rutas';
import { PERM_ESCR_LEADERBOARDS, PERM_LECT_GRUPOS, PERM_LECT_METRICAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { Response } from '@angular/http';
import { AppConfig } from '../../app.config';

@Component({
    moduleId: module.id,
    selector: 'leaderboard-insertar',
    templateUrl: 'leaderboard-insertar.component.html',
    providers: [Leaderboard, LeaderboardService, Metrica, MetricaService, Grupo, GrupoService]
})
export class LeaderboardInsertarComponent implements OnInit {
    public form: FormGroup;
    public itemsTipoTabla: SelectItem[];//para el select de tipo de tabla
    public itemsTipoFormato: SelectItem[];//par aselecionar el formato en el cual sedeslegara la tabla
    public itemsGrupos: SelectItem[];//para seleccionar los grupos a la hora de determinar tablas grupales
    public indGrupalFormaCalculo: SelectItem[];//metodo dde calculo de tablas gurpales suma o promedio
    public file_src = `${AppConfig.DEFAULT_IMG_URL}`;;
    public file: any;
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public lecturaGrupo: boolean; //Permiso de lectura sobre grupo
    public lecturaMetrica: boolean; //Permiso de lectura sobre metrica
    public subiendo: boolean = false;

    //--- Metrica ----//
    public itemsMetricas: SelectItem[];
    public idMetricaSeleccionada: string;
    public metrica: Metrica;
    public listaMetricas: Metrica[]; //Lista de las métricas disponibles
    public metricaVacia: boolean; //Maneja si las metricas estan vacías
    public listaBusqueda: string[] = [];
    public totalRecordsMetrica: number;
    public cantidadMetricas: number = 5;
    public filaMetricas: number = 0;
    public nombreMetrica: string;
    public displayDialogMetrica: boolean;

    public metricaBusqueda;
    public totalRecordsMetricas;

    constructor(
        public i18nService: I18nService,
        public router: Router,
        public metricaService: MetricaService,
        public leaderboard: Leaderboard,
        public leaderboardService: LeaderboardService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        public grupoService: GrupoService
    ) {
        this.authGuard.canWrite(PERM_ESCR_LEADERBOARDS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.authGuard.canWrite(PERM_LECT_GRUPOS).subscribe((result: boolean) => {
            this.lecturaGrupo = result;
            this.poblarSelectGrupos();
        });
        this.authGuard.canWrite(PERM_LECT_METRICAS).subscribe((result: boolean) => {
            this.lecturaMetrica = result;
            this.cargarMetricas();
        });

        this.itemsGrupos = [];
        this.itemsTipoFormato = this.i18nService.getLabels("leaderboard-itemsTipoFormato");
        this.itemsTipoTabla = this.i18nService.getLabels("leaderboard-itemsTipoTabla");
        this.indGrupalFormaCalculo = this.i18nService.getLabels("leaderboard-indGrupalFormaCalculo");
        this.itemsMetricas = [];
        this.form = new FormGroup({
            "nombre": new FormControl("", Validators.required),
            "descripcion": new FormControl("", Validators.required),
            "grupo": new FormControl(""),
            "formato": new FormControl("", Validators.required),
            "tipoTabla": new FormControl("", Validators.required),
            "formaCalculo": new FormControl("", Validators.required),
            "indMetrica": new FormControl("", Validators.required)
        });
    }

    ngOnInit() {
        this.cargarMetricas();
        this.poblarSelectGrupos();
    }

    loadDataMetrica(event: any) {
        this.filaMetricas = event.first;
        this.cantidadMetricas = event.rows;
        this.cargarMetricas();
    }

    seleccionarMetrica(metrica: Metrica) {
        this.metrica = metrica;
        this.nombreMetrica = metrica.nombre;
        this.displayDialogMetrica = false;
    }

    /**Metod encargado de poblar la lista de metricas a partir del API
   * Recibe:nada
   * Retorna: void, pero actualiza la lista de metricas (items Metricas)
   */
    cargarMetricas() {
        let busqueda = "";
        busqueda = this.listaBusqueda.join(" ")
        if (this.lecturaMetrica) {
            this.kc.getToken().then(
                (tkn) => {
                    this.metricaService.token = tkn;
                    this.metricaService.buscarMetricas(['P'], this.cantidadMetricas, this.filaMetricas, busqueda).subscribe(
                        (response: Response) => {
                            this.listaMetricas = response.json();
                            if (response.headers.get("Content-Range") != null) {
                                this.totalRecordsMetrica = + response.headers.get("Content-Range").split("/")[1];
                            }
                            if (this.listaMetricas.length > 0) {
                                this.metricaVacia = false;
                            } else {
                                this.metricaVacia = true;
                            }
                        },
                        (error: any) => this.manejaError(error)
                    );
                }
            )
        }
    }

    /**
     * Encargado de isnertar la leaderboard a traves del service que conecta con el API
     * Emplea la variable global llamada leaderboard para almacenar lso datos introducidos por el usuario
     *  */
    insertarLeaderboard() {

        if (this.leaderboard.indTipo == 'E' && !this.leaderboard.idGrupo.idGrupo) {
            this.msgService.showMessage(this.i18nService.getLabels("error-leaderboard-grupo"));
            return;
        }

        if (this.leaderboard.indTipo != 'E') {
            this.leaderboard.idGrupo = null;
        }

        this.kc.getToken().then((tkn) => {
            this.leaderboardService.token = tkn;
            this.metricaService.token = tkn;
            this.subiendo = true;
            this.leaderboard.idMetrica = this.metrica;
            let sub = this.leaderboardService.addLeaderboard(this.leaderboard).subscribe(
                (data: Leaderboard) => {
                    this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                    this.router.navigate([Routes.RT_LEADERBOARDS_LISTA]);
                },
                (error) => {
                    this.manejaError(error);
                },
                () => {
                    sub.unsubscribe();
                    this.subiendo = false;
                }
            );

        }).catch((error: any) => { this.manejaError(error) });
    }
    /**
     * Desplega un mensaj de erro ral usuario 
     */
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /**
     * Encargado de poblar el select de grupospara seleccionar grupo en caso de que la tabla de prosiciones sea de tipo grupal
     */
    poblarSelectGrupos() {
        let cantidad = 50; //Cantidad de rows
        let filaDesplazamiento = 0; //Desplazamiento de la primera fila
        if (this.lecturaGrupo) {
            this.itemsGrupos = [];
            this.itemsGrupos = [...this.itemsGrupos, { label: '--', value: null }];
            this.kc.getToken().then(
                (tkn) => {
                    this.grupoService.token = tkn;
                    this.grupoService.buscarGrupo(cantidad, filaDesplazamiento, '', ['V']).subscribe(
                        (data: Response) => {
                            let lista = data.json();
                            lista.forEach((grp: Grupo) => {
                                this.itemsGrupos = [...this.itemsGrupos, { label: grp.nombre, value: grp }];
                            });
                        },
                        (error) => this.manejaError(error)
                    );
                }
            )
        }
    }
    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input: any) {

        this.file = input.files[0];

        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);

        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event: any) => {
            // Obtener el (base64 de la imagen)
            img.src = event.target.result;
            // Redimensionar la imagen
            this.leaderboard.imagen = event.target.result.split(",")[1];
            this.file_src = this.resize(img);
        }, false);

        reader.readAsDataURL(input.files[0]);


    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img: any, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");

        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");

        ctx.drawImage(img, 0, 0, width, height);

        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        return dataUrl
    }
    goBack() {
        let link = ['leaderboards'];
        this.router.navigate(link);
    }
}