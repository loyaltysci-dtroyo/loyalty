import { Leaderboard } from '../../model/leaderboard';
import { Component, OnInit } from '@angular/core';
import { Categoria, Promocion, Metrica, Grupo } from '../../model/index';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LeaderboardService, GrupoService, MetricaService, KeycloakService, I18nService } from '../../service/index'
import { MessagesService, ErrorHandlerService } from "../../utils/index";
import { SelectItem } from 'primeng/primeng';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as Routes from '../../utils/rutas';
import { PERM_ESCR_LEADERBOARDS, PERM_LECT_GRUPOS, PERM_LECT_METRICAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { Response } from '@angular/http';
@Component({
    moduleId: module.id,
    selector: 'leaderboard-detalle',
    templateUrl: 'leaderboard-detalle.component.html'

})
/**
 * Flecha Roja Technologies 24-oct-2016
 * Fernando Aguilar
 * Compoennte encargado de mostrar el detalle de leaderboards y de editar leaderboards
 */
export class LeaderboardDetalleComponent implements OnInit {
    public metricas: Metrica[];//lista de tablas de posicion 
    public itemsMetricas: SelectItem[];//para poblar el drop de metricas 
    public idMetricaSeleccionada: string;//id de la metrica seleccionada en el drop de metricas seleccionadas
    public itemsTipoTabla: SelectItem[];//items para especificar eltipo de tabla (grupal equipos o usuario)
    public itemsTipoFormato: SelectItem[];//para poblar el dropdown de formato del leaderboard( nombre o iniciales)
    public indGrupalFormaCalculo: SelectItem[];//para poblar el dropdown de forma de calculo global (suma o promoedio)
    public itemsGrupos: SelectItem[];//para poblar el dropdown de grupos
    public file_src: any;
    public file: any;
    public form: FormGroup;//validacion y controles del form de editar leaderboard
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public subiendo: boolean = false;
    public lecturaGrupo: boolean;
    public lecturaMetrica: boolean;
    public cantidad: number = 50; //Cantidad de rows
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public gruposLista : Grupo[];
    public grupoSelecc: string;

    //--- Metrica ----//
    public metrica: Metrica;
    public listaMetricas: Metrica[]; //Lista de las métricas disponibles
    public metricaVacia: boolean; //Maneja si las metricas estan vacías
    public listaBusqueda: string[] = [];
    public totalRecordsMetrica: number;
    public cantidadMetricas: number = 5;
    public filaMetricas: number = 0;
    public nombreMetrica: string;
    public displayDialogMetrica: boolean;

    public metricaBusqueda;
    public totalRecordsMetricas;
    constructor(
        public leaderboard: Leaderboard,
        public leaderboardService: LeaderboardService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public grupoService: GrupoService,
        public metricaService: MetricaService,
        public i18nService: I18nService
    ) {
        /**Verificacion de los permisos de escritura del usuario en sesion */
        this.authGuard.canWrite(PERM_ESCR_LEADERBOARDS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.authGuard.canWrite(PERM_LECT_GRUPOS).subscribe((result: boolean) => {
            this.lecturaGrupo = result;
            this.poblarSelectGrupos();
        });
        this.authGuard.canWrite(PERM_LECT_METRICAS).subscribe((result: boolean) => {
            this.lecturaMetrica = result;
            this.cargarMetricas()
        });

        this.route.params.forEach((params: Params) => { this.leaderboard.idTabla = params['id'] });
        this.metricas = [];
        this.itemsMetricas = [];
        this.form = new FormGroup({
            "nombre": new FormControl("", Validators.required),
            "descripcion": new FormControl(""),
            "grupo": new FormControl(""),
            "formato": new FormControl("", Validators.required),
            "tipoTabla": new FormControl("", Validators.required),
            "formaCalculo": new FormControl("", Validators.required),
            "indMetrica": new FormControl("", Validators.required)
        });
        /**Mapeo de los elementos de los dropdown */
        this.itemsTipoFormato = this.i18nService.getLabels("leaderboard-itemsTipoFormato");
        // E = Grupo especifico, G = grupo general, U = miembros
        this.itemsTipoTabla = this.i18nService.getLabels("leaderboard-itemsTipoTabla");
        this.indGrupalFormaCalculo = this.i18nService.getLabels("leaderboard-indGrupalFormaCalculo");
        this.itemsMetricas = [];
    }

    ngOnInit() {
        //this.leaderboard.indGrupalFormaCalculo = "S";
        this.leaderboard.idGrupo = new Grupo();
        this.obtenerLeaderboard();//se obtienen los datos del leaderboard 
        this.cargarMetricas();//poblar el select de metricas 
        this.poblarSelectGrupos();//sepobla el select de grupos en caso de que se quiera cambiar de grupo

    }

    loadDataMetrica(event: any) {
        this.filaMetricas = event.first;
        this.cantidadMetricas = event.rows;
        this.cargarMetricas();
    }

    seleccionarMetrica(metrica: Metrica) {
        this.metrica= metrica;
        this.nombreMetrica = metrica.nombre;
        this.displayDialogMetrica = false;
    }
    /**
     * Obtiene el detalle de leaderboard del API
     * */
    obtenerLeaderboard() {
        this.kc.getToken().then((tkn: string) => {
            this.leaderboardService.token = tkn;
            let sub = this.leaderboardService.getLeaderboard(this.leaderboard.idTabla).subscribe(
                (data: Leaderboard) => {
                    this.leaderboard = data;
                    this.file_src = this.leaderboard.imagen;
                    if(this.leaderboard.idMetrica != undefined){
                        this.metrica = this.leaderboard.idMetrica;
                        this.nombreMetrica = this.metrica.nombre;
                    }
                    if(this.leaderboard.indTipo == 'E'){
                        this.grupoSelecc = this.leaderboard.idGrupo.idGrupo;
                    }
                },
                (error: any) => {
                    this.manejaError(error);
                },
                () => {
                    sub.unsubscribe();
                }
            );
        }).catch((error: any) => { this.manejaError(error) });
    }

    /**
     * Encargado de manejo de errores
     */
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    /**Metodo encargado de poblar la lista de metricas a partir del API
      * Recibe:nada
      * Retorna: void, pero actualiza la lista de metricas (items Metricas)
      */
    cargarMetricas() {
        let busqueda = "";
        busqueda = this.listaBusqueda.join(" ")
        if (this.lecturaMetrica) {
            //Se carga la lista con los valores que provee el servicio
            this.kc.getToken().then(
                (tkn) => {
                    this.metricaService.token = tkn;
                    this.metricaService.buscarMetricas(['P'], this.cantidadMetricas, this.filaMetricas, busqueda).subscribe(
                        (response: Response) => {
                            this.listaMetricas = response.json();
                                if (response.headers.get("Content-Range") != null) {
                                    this.totalRecordsMetrica = + response.headers.get("Content-Range").split("/")[1];
                                }
                                if (this.listaMetricas.length > 0) {
                                    this.metricaVacia = false;
                                } else {
                                    this.metricaVacia = true;
                                }
                        },
                        (error: any) => this.manejaError(error)
                    );
                });
        }
    }

    /**
       * Encargado de poblar el select de grupospara seleccionar grupo en caso de que la tabla de prosiciones sea de tipo grupal
       */
    poblarSelectGrupos() {
        if (this.lecturaGrupo) {
            this.itemsGrupos = [];
            this.itemsGrupos=[...this.itemsGrupos,{ label: '--', value: null }];
            this.kc.getToken().then(
                (tkn) => {
                    this.grupoService.token = tkn;
                    this.grupoService.buscarGrupo(this.cantidad, this.filaDesplazamiento, '', ['V']).subscribe(
                        (data: Response) => {
                            let lista = data.json();
                            lista.forEach((grp: Grupo) => {
                                this.itemsGrupos=[...this.itemsGrupos,{ label: grp.nombre, value: grp.idGrupo }];
                            });
                            this.gruposLista = lista;
                        },
                        (error: any) => this.manejaError(error)
                    );
                }
            )
        }
    }

    /**
     * Encargado de isnertar la leaderboard a traves del service que conecta con el API
     * Emplea la variable global llamada leaderboard para almacenar lso datos introducidos por el usuario
     *  */
    editarLeaderboard() {
        if (!(this.leaderboard.indTipo == "E")) {
            this.leaderboard.idGrupo = null;
        }else{
            this.leaderboard.idGrupo = this.gruposLista.find(element => element.idGrupo == this.grupoSelecc);
        }
        this.msgService.showMessage(this.i18nService.getLabels("general-actualizando-datos"));
        this.kc.getToken().then((tkn) => {
            this.leaderboardService.token = tkn;
            this.metricaService.token = tkn;
            this.subiendo = true;
            /*let subs = this.metricaService.getMetrica(this.leaderboard.idMetrica.idMetrica).subscribe(
                (data: Metrica) => {
                    this.leaderboard.idMetrica = data;*/
                    if(this.metrica != undefined){
                        this.leaderboard.idMetrica = this.metrica;
                    }
                    let sub = this.leaderboardService.updateLeaderboard(this.leaderboard).subscribe(
                        (data: Leaderboard) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                            this.obtenerLeaderboard();
                            //this.router.navigate([Routes.RT_LEADERBOARDS_DETALLE, data.idTabla]);
                        },
                        (error: any) => {
                            this.manejaError(error)
                        },
                        () => {
                            sub.unsubscribe();
                            this.subiendo = false;
                        }
                    );
                },
                (error: any) => this.manejaError(error)
            );

        //}).catch((error: any) => { this.manejaError(error) });
    }
    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input: any) {

        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);

        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event: any) => {
            // Obtener el (base64 de la imagen)
            img.src = event.target.result;
            this.leaderboard.imagen = event.target.result.split(",")[1];
            // Redimensionar la imagen
            this.file_src = this.resize(img);
        }, false);
        reader.readAsDataURL(input.files[0]);


    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img: any, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");

        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");

        ctx.drawImage(img, 0, 0, width, height);

        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        return dataUrl
    }

    //Regresar a la lista
    goBack() {
        this.router.navigate([Routes.RT_LEADERBOARDS]);
    }
}