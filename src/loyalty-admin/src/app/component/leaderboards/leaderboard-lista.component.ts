import * as Routes from '../../utils/rutas';
import { RT_LEADERBOARDS_INSERTAR } from '../../utils/rutas';
import { Leaderboard } from '../../model/leaderboard';
import { Component, OnInit } from '@angular/core';
import { Categoria, Promocion, Metrica, Grupo } from '../../model/index';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LeaderboardService, KeycloakService, GrupoService, I18nService } from '../../service/index'
import { MessagesService, ErrorHandlerService } from "../../utils/index";
import { MenuItem } from 'primeng/primeng';
import { PERM_ESCR_LEADERBOARDS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'leaderboard-lista',
    templateUrl: 'leaderboard-lista.component.html',
    providers: [LeaderboardService]
})
/**
 * Flecha Roja Technologies 24-oct-2016
 * Fernando Aguilar
 * Compoennte encargado de listar y eliminar Leaderboard del sistema
 */
export class LeaderboardListaComponent implements OnInit {
    public leaderboards: Leaderboard[];
    public leaderboardEliminar: Leaderboard;
    public terminosBusqueda: string[];
    public displayBusquedaAvanzada: boolean;
    public itemsBusqueda: MenuItem[];
    public displayModalEliminar: boolean;
    public preferenciasBusqueda: string; //string[];//contiene las preferencia de busquda ingresadas por el usuario en los checks de tipo
    public rtInsertarLeaderboards = RT_LEADERBOARDS_INSERTAR;
    public escritura: boolean; //Permite verificar permiso de escritura (true/false)
    public questBorrar: string;
    public metricaBusqueda;
    public totalRecordsMetricas;

    constructor(
        public leaderboard: Leaderboard,
        public router: Router, 
        public leaderboardService: LeaderboardService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService,
        public authGuard: AuthGuard
    ) {
        this.authGuard.canWrite(PERM_ESCR_LEADERBOARDS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        //this.preferenciasBusqueda = ['G', 'U', 'E'];
        this.displayModalEliminar = false;

        this.itemsBusqueda = [
            { label: 'Avanzado', icon: 'ui-icon-build', command: () => { this.displayBusquedaAvanzada = !this.displayBusquedaAvanzada } }
        ];
    }

    ngOnInit() {
        this.buscarLeaderboards();
        this.questBorrar = this.i18nService.getLabels("confirmacion-pregunta")["B"];
    }
    /**
     * Busca las leaderboards en el API basandodse en los atributos de busqueda introducidos por el usuario
     * Emplea: el arreglo de leaderboards de busqueda y el termino de busueda introdcido
     */
    buscarLeaderboards() {
        this.keycloakService.getToken().then((tkn: string) => {
            this.leaderboardService.token = tkn;
            let sub = this.leaderboardService.busqueda(this.preferenciasBusqueda, this.terminosBusqueda ).subscribe(
                (data: Leaderboard[]) => {
                    this.leaderboards = data;
                },
                (error: any) => { this.manejaError(error); },
                () => { sub.unsubscribe(); }
            );
        }).catch((error: any) => { this.manejaError(error) });


    }

    /**Obtiene todas las leaderboards del API */
    getAllLeaderboards() {
        this.keycloakService.getToken().then((tkn: string) => {
            this.leaderboardService.token = tkn;
            let sub = this.leaderboardService.getListaLeaderboards().subscribe(
                (data: Leaderboard[]) => {
                    this.leaderboards = data;
                },
                (error) => { this.manejaError(error); },
                () => { sub.unsubscribe(); }
            );
        }).catch((error) => { this.manejaError(error) });

    }
    /**navega hacia el detalle de una preferencia en concreto */
    gotoDetail(preferencia: Leaderboard) {
        let link = Routes.RT_LEADERBOARDS_DETALLE;
        this.router.navigate([link, preferencia.idTabla]);
    }
    /**elimina la preferencia del sistema, es invocado por el modal de confirmacion */
    eliminarLeaderboard() {
        this.keycloakService.getToken().then((tkn: string) => {
            this.leaderboardService.token = tkn;
            let sub = this.leaderboardService.deleteLeaderboard(this.leaderboardEliminar).subscribe(
                () => {
                    this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                    this.buscarLeaderboards();
                    this.displayModalEliminar = false;
                },
                (error: any) => { this.manejaError(error); },
                () => { sub.unsubscribe(); }
            );
        }).catch((error: any) => { this.manejaError(error) });
    }
    /**
     * Encargado de manejo de errores
     */
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /**
     * Muestra el modal de eliminar del leaderboard y ademas almacena el leaderboard a eliminara
     * para luego eliminarla una vez que el usuario confirma la accion
     */
    mostrarModalEliminarLeaderboard(leadeboard: Leaderboard) {
        this.leaderboardEliminar = leadeboard;
        this.displayModalEliminar = true;
    }
}
