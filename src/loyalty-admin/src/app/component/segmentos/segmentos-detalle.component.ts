import { I18nService } from '../../service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Segmento } from '../../model/index';
import { SegmentoService } from '../../service/index';
import { Subscription } from 'rxjs';
import { MenuItem } from 'primeng/primeng';
import { KeycloakService } from '../../service/index';
import * as Rutas from '../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'detalle-segmento-component',
    templateUrl: 'segmentos-detalle.component.html',
    providers: [Segmento, SegmentoService]
})
/**
 * Flecha Roja Technologies oct-2016
 * Fernando Aguilar
 * Componente encargado de permitir al usuario cambiar los estados de el segmento, permite
 * mostrar el titulo de detalle de segmento, asi como de servir como padre para los elementos
 *  de detalle de segmento, provee enrutamiento a nivel de los elementos de detalle de segmento
 */
export class DetalleSegmentoComponent {

    public items: MenuItem[];
    public displayModalArchivar: boolean;//despliega el modal de confirmacion de archivado
    public displayModalEliminar: boolean;//despliega el modal de confirmacion de eliminado
    public update: boolean;

    constructor(
        public segmento: Segmento,//instancia de segmento usada para almacenar los datos del segmento actual
        public segmentoService: SegmentoService,//encargado del manejo de datos de segmentos
        public route: ActivatedRoute,//ruta actual
        public msgService: MessagesService,
        public kc: KeycloakService,
        public router: Router,
        private i18nService: I18nService
    ) {
        this.route.params.forEach((params: Params) => { this.segmento.idSegmento = params['id'] });
        this.items = [
            { label: 'Atributos', icon: 'ui-icon-reorder', routerLink: [`/segmentos/detalleSegmento/${this.segmento.idSegmento}/atributos`] },
            { label: 'Reglas', icon: 'ui-icon-toc', routerLink: [`/segmentos/detalleSegmento/${this.segmento.idSegmento}/reglas`] },
            { label: 'Miembros', icon: 'ui-icon-group', routerLink: [`/segmentos/detalleSegmento/${this.segmento.idSegmento}/miembros`] }
        ];
        this.obtenerSegmento();
    }
    /**
  * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
  * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
  * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
  * no siempre sea necesario llamar al api para actualizar los datos
  */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Obtiene un segmento del API, setea los valores del segmento obtenido para evitar perder el puntero al segmento que fue iyectado al arbol de dependencias
     * por angular y permitir que los hijos del arbol de componentes compartan dicho puntero para sincronizar datos del detalle de segmento
     */
    obtenerSegmento() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoService.token = tkn;
                let sub = this.segmentoService.getSegmento(this.segmento.idSegmento).subscribe(
                    (data: Segmento) => {
                        this.copyValuesOf(this.segmento, data)
                    },
                    (error) => {

                        this.manejaError(error)
                    },
                    () => sub.unsubscribe()
                );
            });
    }
    /**
     * Encargado de activar un segmento, siempre y cuando el segmento se encuentre en estado borrador
     */
    activarSegmento() {
        if (this.update) {
            this.obtenerSegmento();
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoService.token = tkn;
                this.segmento.indEstado = "AC";
                let sub = this.segmentoService.updateSegmento(this.segmento).subscribe(
                    (data: Segmento) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.obtenerSegmento();
                        this.update = true;
                    },
                    (error) => {
                        this.obtenerSegmento();
                        this.manejaError(error);
                        this.update = false;
                    },
                    () => sub.unsubscribe()
                );
            });
    }
    /**
     * Encargado de eliminar un segmento, siempre y cuando el segmento se encuentre en estado borrador
     */
    eliminarSegmento() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoService.token = tkn;
                let sub = this.segmentoService.deleteSegmento(this.segmento).subscribe(
                    (data: Segmento) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                        let link = [Rutas.RT_SEGMENTOS_LISTA_SEGMENTOS];
                        this.router.navigate(link);
                    },
                    (error) => { this.manejaError(error) },
                    () => sub.unsubscribe()
                );
            });

    }
    /**
     * Encargado de archivar un segmento, siempre y cuando el segmento se encuentre en estado activado
     */
    archivarSegmento() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoService.token = tkn;
                this.segmento.indEstado = "AR";
                let sub = this.segmentoService.deleteSegmento(this.segmento).subscribe(
                    (data: Segmento) => {
                        this.obtenerSegmento();
                        this.displayModalArchivar = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-archivar"));
                    },
                    (error) => { this.manejaError(error); },
                    () => sub.unsubscribe()
                );
            });
    }

    // Método para regresar a la lista de productos
    goBack() {
        let link = ['segmentos'];
        this.router.navigate(link);
    }
    /*
  * Muestra un mensaje de error al usuario con los datos de la transaccion
  * Recibe: una instancia de request con la informacion de error
  * Retorna: un observable que proporciona información sobre la transaccion
  */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}