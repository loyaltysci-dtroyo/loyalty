import { IndicadorReglaPeriodoPipe } from '../../pipes';
import { ReglaPreferenciaService } from '../../service/regla-preferencia.service';
import { ReglaEncuestaService } from '../../service/regla-encuesta.service';
import { I18nService } from '../../service';
import { Response } from "@angular/http"
import { KeycloakService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Component, OnInit } from '@angular/core';
import {
    AtributoDinamico, ListaReglas, Pais, Segmento, ReglaMetricaCambio, Ubicacion, ReglaAvanzada, ReglaEncuesta, ReglaPreferencia,
    ReglaMiembroAtb, ReglaMetricaBalance, Nivel, GrupoNivel, Grupo, Notificacion, Premio, Metrica, Leaderboard, Promocion, Producto, Mision, Insignia
} from '../../model/index';
import {
    AtributoDinamicoService, ReglaAvanzadaService, MetricaService, PaisService, ReglaMetricaBalanceService, NotificacionService, PremioService, UbicacionService, NivelService, MisionService,
    ReglaMiembroAtributoService, ReglaMetricaCambioService, SegmentoService, PromocionService, GrupoService, LeaderboardService, ProductoService, InsigniaService
} from '../../service/index';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { SelectItem, LazyLoadEvent } from 'primeng/primeng';
import { PERM_ESCR_SEGMENTOS } from '../common/auth.constants'
import { AuthGuard } from '../common/index';
import { Observable } from 'rxjs/Rx';
@Component({
    moduleId: module.id,
    selector: 'segmentos-detalle-reglas-component',
    templateUrl: 'segmentos-detalle-reglas.component.html',
    providers: [
        Ubicacion, ReglaAvanzada, ReglaEncuesta, ReglaPreferencia, AtributoDinamico, Nivel, GrupoNivel, Producto, MisionService, Mision, Insignia, Metrica, Notificacion, Premio, Leaderboard, Promocion, Grupo, UbicacionService, ProductoService, ReglaMetricaBalance, ReglaMetricaCambio, ReglaMiembroAtb, ReglaMetricaBalanceService, NotificacionService, PremioService, LeaderboardService, InsigniaService, ReglaEncuestaService, ReglaPreferenciaService,
        ReglaMiembroAtributoService, ReglaAvanzadaService, GrupoService, PromocionService, SegmentoService, MetricaService, AtributoDinamicoService, ReglaMetricaCambioService, NivelService, Pais, PaisService]
})

/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Componente encargado de mostrar el detalle de reglas de un determinado grupo de reglas de un segmento
 */
export class SegmentoDetalleReglasComponent implements OnInit {
    /**
     * Form groups usados para validar la entrada de reglas en sus respectivos formatos 
     * dependiendo del atributo selecionado
     */
    public modalGrpGRL: FormGroup;//general
    public modalGrpAT: FormGroup;//atributo
    public modalGrpNivel: FormGroup;//para reglas de atributo usando nivel de metrica
    public modalGrpATNumeric: FormGroup;//atributo de tipo numerico
    public modalGrpATText: FormGroup;//atributo de tipo texto
    public modalGrpMB: FormGroup;//metrica balance
    public modalGrpMC: FormGroup;//metrica cambio
    public modalGrpAV: FormGroup;//form para reglas avanzadas
    public reglaMetricaBalanceList: ReglaMetricaBalance[];//lista de reglas de balance de metrica
    public reglaMetricaCambioList: ReglaMetricaCambio[];//lista de reglas de cambio de métrica
    public reglaMiembroAtbList: ReglaMiembroAtb[];//lista de reglas de atributos de miembro
    public reglaAvanzadaList: ReglaAvanzada[];//lista de reglas avanzadas
    public reglaEncuestaList: ReglaEncuesta[];//lista de reglas avanzadas
    public reglaPreferenciaList: ReglaPreferencia[];//lista de reglas avanzadas

    public tipoRegla: string;//modificado por el select de tipo de regla, usado por el form del modal para alternar entre los distintos forms dependiendo de los tipos de regla
    public reglaEliminar: any;//guarda temporalmente la regla a eliminar
    public tipoReglaEliminar: string;//guarda el tipo de regla a eliminar para determinar cual service debe ser llamado
    public mostrarModalInsertar: boolean;//cuando es true se muestr el modal de insertar reglas
    public mostrarModalEliminar: boolean;//cuando es true se muestra el modal de confirmacion de eliminar reglas

    /*** Select items: elementos para poblar los drop down, que en este caso son empleados para la definicion de reglas*/
    public operadoresFecha: SelectItem[];//operadores para definir reglas cuando el valor de tipo de datos es fecha 
    public operadorFijo: SelectItem[]; //operador fijo para los valores de genero, estado civil, estado miembro, educacion, pais
    public operadoresTexto: SelectItem[];//operadores para definir reglas cuando el valor de tipo de datos es texto 
    public operadoresNumericos: SelectItem[];//operadores para definir reglas cuando el valor de tipo de datos es numerico 
    public listaTipoRegla: SelectItem[];//lista de tipos de regla para usuarla en dropdoen
    public listaOpcionesBoolean: SelectItem[];//lista de tipos de regla para usuarla en dropdoen
    public listaAtributos: SelectItem[];//lista usada para poblar el select de atributos con los atributos
    public listaBalancesMetrica: SelectItem[];//paratipo de cambio = ganar expirar o redimir
    public listaPeriodoTiempo: SelectItem[];
    public listaMetricas: SelectItem[];//para poblar las metricas del select para insertar reglas de metrica cambio y metrica balance
    public listaMetricasNivel: SelectItem[];//para poblar las metricas del select para insertar reglas de atributo a partir de el nivel de metrica seleccionado
    public listaGrupos: SelectItem[];//para poblar el select de grupos de niveles de metrica
    public listaNiveles: SelectItem[];//poblar el select de niveles de metrica
    public listaBalancesMetricaBalance: SelectItem[];//indicadores de balance de metrica especiales para reglas de balance de metrica
    public optionsReglasAvanzadas: SelectItem[];//contiene la lista de reglas avanzadas para poblar el drop de reglas en reglas avanzadas
    public listaEstadosCiviles: SelectItem[];//usados para poblar el drop de estados civiles al insertar una regla de atributo 
    public listaEstadosMiembro: SelectItem[];//opciones de activo e inactivo
    public listaEducacionMiembro: SelectItem[];//niveles de educacion, 
    public listaGenerosMiembro: SelectItem[];//opciones de genero para las reglas
    public listaPaisesMiembro: SelectItem[];//opciones de los paises, eventualmente sera poblado a partir del catalogo de paises

    public tipoDato: string[] = ["N", "T", "B", "F"];//N:numerico,T:string,F:fecha,B:bool
    public busquedaAtributo: string = "";//termino de busqueda para atributos
    public displaySeleccAtrDinamico: boolean = false;//cuando es true se despliega el modal para la seleccion de atributos dinamicos
    public listaAtributosDinamicos: AtributoDinamico[];//lista con los atributos dinamicos que se despliegan para que el usuario seleccione en caso de reglas de atributo
    public es: any; // locales para los calendar input en español
    public tienePermisosEscritura: boolean;//cuando es true se despliegan las opciones de escritura en la pantalla
    /**Atributos especificos para la defincion de reglas avanzadas */
    public mostrarModalInsertarReglaAvanzada: boolean;
    public terminoBusquedaReglasAvanzadas: string;
    public rowOffsetReglasAvanzadas: number;//para paginar los elementos de referencia
    public cantRowsReglasAvanzadas: number;//para paginacion de los elementos de referencia
    public cantRowsPorPaginaReglasAvanzadas = 5;
    public indPeriodoReglaAvanzada: string;//temporal para determinar la calendarizacion de las reglas avanzadas
    public listaMensajesDisponibles: Notificacion[];
    public listaPremiosDisponibles: Premio[];
    public listaUbicacionesDisponibles: Ubicacion[];
    public optionsDispositivosDisponibles: SelectItem[];
    public listaLeaderboardsDisponibles: Leaderboard[];
    public listaPromocionesDisponibles: Promocion[];
    public listaProductosDisponibles: Producto[];
    public listaMisionesDisponibles: Mision[];
    public listaInsigniasDisponibles: Insignia[];
    public listaMetricasDisponibles: Metrica[];
    public listaGruposDisponibles: Grupo[];
    public listaGruposNivelDisponibles: GrupoNivel[];
    public listaNivelesDisponibles: SelectItem[];
    public listaSegmentosDisponibles: Segmento[];
    public listaDispositivos: SelectItem[];
    public listaCalendarizacionReglasAvanzadas: SelectItem[];
    public listaElementosReciente: SelectItem[];//en el ultimo dia/semana/mes
    public idElementoSeleccionado: string//TEMPORAL
    public nombreElementoSeleccionado: string//TEMPORAL
    public indOperador: string;//TEMPORAL
    public listaPaises: Pais[];
    public listaBusqueda: string[] = [];
    // ---- Atributos dinámicos ---- //
    public cantRowsAtributos: number; //Total de atributos dinámicos
    public cantRowsPorPaginaAtributo: number = 10; //Cantidad de filas
    public rowOffsetAtributos: number = 0; //Desplazamiento de la primera filas

    // ---- Metricas -----//
    public totalRecordsMetrica: number;
    public cantidadMetricas: number = 5;
    public filaMetricas: number = 0;
    public nombreMetrica: string;
    public displayDialogMetrica: boolean;
    public metricasCambio: Metrica[] =[];
    public metricaBusqueda: string[] = [];

    constructor(
        public atributoService: AtributoDinamicoService,
        public segmento: Segmento,
        public nivelService: NivelService,
        private reglaMetricaBalanceService: ReglaMetricaBalanceService,
        private reglaMetricaCambioService: ReglaMetricaCambioService,
        private reglaMiembroAtributoService: ReglaMiembroAtributoService,
        private reglaEncuestaService: ReglaEncuestaService,
        private reglaPreferenciaService: ReglaPreferenciaService,
        public metricaService: MetricaService,
        public kc: KeycloakService,
        public grupoSeleccionado: GrupoNivel,//el nivel de grupo seleccionado para las reglas de atributo cuando se elige nivel de metrica
        public reglaMetricaBalance: ReglaMetricaBalance,
        public reglaMetricaCambio: ReglaMetricaCambio,
        public reglaMiembroAtb: ReglaMiembroAtb,
        public listaReglas: ListaReglas,
        public notificacionService: NotificacionService,
        public premioService: PremioService,
        public misionService: MisionService,
        public promocionService: PromocionService,
        public grupoService: GrupoService,
        public leaderboardService: LeaderboardService,
        public insigniaService: InsigniaService,
        public segmentoService: SegmentoService,
        public ubicacionService: UbicacionService,
        public productoService: ProductoService,
        public reglaAvanzadaService: ReglaAvanzadaService,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,
        public paisService: PaisService,
        public msgService: MessagesService,
        public reglaAvanzada: ReglaAvanzada,
        public formBuilder: FormBuilder,
        private i18nService: I18nService) {

        this.es = {
            firstDayOfWeek: 1,
            dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
            dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
            dayNamesMin: ["D", "L", "K", "M", "J", "V", "S"],
            monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
        };

        this.route.params.forEach((params: Params) => { this.listaReglas.idLista = params['id'] });

        /*Form groups con los datos de validacion y valores por default del modal de insercion de reglas*/
        this.modalGrpGRL = new FormGroup({
            "tipoRegla": new FormControl('AT', [Validators.required]),
        });
        //form group para validar las reglas de atributo de miembro
        this.modalGrpAT = new FormGroup({
            "selectAtributo": new FormControl('', [Validators.required]),
            "txtNombreRegla": new FormControl('', [Validators.required])
        });
        //form group para validar las reglas de atributo de miembro pero con atributo de tipo texto
        this.modalGrpATText = new FormGroup({
            "selectOperadorTxt": new FormControl('', [Validators.required]),
            "txtValor": new FormControl('', [Validators.required]),
        });
        //form group para validar las reglas de atributo de miembro pero con atributo de tipo numerico
        this.modalGrpATNumeric = new FormGroup({
            "selectOperadorNmb": new FormControl('', [Validators.required]),
            "nmbValor": new FormControl('', [Validators.required]),
        });
        //form group para validar las reglas de balance de metrica
        this.modalGrpMB = new FormGroup({
            "txtNombreRegla": new FormControl('', [Validators.required]),
            "selectIdMetrica": new FormControl('', [Validators.required]),
            "selectOperadorTxt": new FormControl('', [Validators.required]),
            "nmbValor": new FormControl('', [Validators.required]),
            "indBalance": new FormControl('', [Validators.required])
        });
        //form group para validar las reglas de cambio de metrica
        this.modalGrpMC = new FormGroup({
            "txtNombreRegla": new FormControl('AT', [Validators.required]),
            "selectIdMetrica": new FormControl('', [Validators.required]),
            "selectTipoCambio": new FormControl('', [Validators.required]),
            "selectOperadorMetrica": new FormControl('', [Validators.required]),
            "nmbValor": new FormControl('', [Validators.required]),
            "selectPeriodoMetrica": new FormControl('', [Validators.required])
        });

        this.modalGrpNivel = new FormGroup({
            // "selectGrupo": new FormControl('', [Validators.required]),
            "selectMetrica": new FormControl('', [Validators.required]),
            "selectNivel": new FormControl('', [Validators.required]),
        });

        this.modalGrpAV = new FormGroup({
            "txtNombreRegla": new FormControl('', [Validators.required]),
            "selectTipoRegla": new FormControl('', [Validators.required])
        });
        this.onChangeSelectAtributo();

        //inicializaciones varias
        this.tipoRegla = "AT";
        this.reglaMetricaCambio.indTipo = "B";
        this.reglaMetricaBalance.indOperador = "V";
        this.reglaMetricaBalance.indBalance = "A"
        this.reglaMiembroAtb.indOperador = "A";
        this.reglaMiembroAtb.indAtributo = "B";
        this.reglaMiembroAtb.indOperador = "V";

        //reglas normales
        this.listaGrupos = [];
        this.listaBalancesMetricaBalance = [];
        // this.reglaMetricaBalanceList = [];
        // this.reglaMetricaCambioList = [];
        // this.reglaMiembroAtbList = [];
        this.listaOpcionesBoolean = [];
        this.operadoresNumericos = [];
        this.operadoresFecha = [];
        this.operadoresTexto = [];
        this.operadorFijo = [];
        this.listaMetricas = [];
        this.listaNiveles = [];
        this.listaAtributos = [];
        this.listaEstadosCiviles = [];
        this.listaEstadosMiembro = [];
        this.listaEducacionMiembro = [];
        this.listaAtributosDinamicos = [];
        this.listaBalancesMetrica = [];
        this.listaPeriodoTiempo = [];
        this.listaTipoRegla = [];
        this.optionsReglasAvanzadas = [];
        this.listaGenerosMiembro = [];
        this.listaPaisesMiembro = [];

        //reglas avanzadas
        // this.reglaAvanzadaList = [];
        this.reglaAvanzada.indTipoRegla = 'A';
        this.indPeriodoReglaAvanzada = "B";
        this.reglaAvanzada.fechaIndUltimo = "A";
        this.reglaAvanzada.fechaCantidad = 1;
        this.terminoBusquedaReglasAvanzadas = "";
        this.rowOffsetReglasAvanzadas = 0;
        this.cantRowsReglasAvanzadas = 0;
        this.listaGruposDisponibles = [];
        this.optionsDispositivosDisponibles = [];
        this.listaGruposNivelDisponibles = [];
        this.listaInsigniasDisponibles = [];
        this.listaLeaderboardsDisponibles = [];
        this.listaMensajesDisponibles = [];
        this.listaMetricasDisponibles = [];
        this.listaMisionesDisponibles = [];
        this.listaPromocionesDisponibles = [];
        this.listaSegmentosDisponibles = [];
        this.listaProductosDisponibles = [];
        this.listaPremiosDisponibles = [];
        this.listaUbicacionesDisponibles = [];
        this.listaElementosReciente = [];
        this.listaCalendarizacionReglasAvanzadas = [];
        this.listaDispositivos = [];

        this.mapeoSelects();//encagado de poblar los dropdown
        this.authGuard.canWrite(PERM_ESCR_SEGMENTOS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });

    }
    /**
     * Metodo para cargar solamente los valores necesarios para el modal de seleccion de elemento de referencia
     */
    displayModalSelectElementoReglasAvanzadas() {
        this.mapeoElementosReglasAvanzadas();
        this.rowOffsetReglasAvanzadas = 0;
        this.cantRowsReglasAvanzadas = 0;
    }
    /**
     * Restablee los valores por defecto cuando el dropdown de reglas avanzadas cambia de valor
     */
    resetFieldsReglasAvanzadas() {
        this.listaBusqueda = [];
        this.cantRowsReglasAvanzadas = 0;
        this.rowOffsetReglasAvanzadas = 0;
        this.indPeriodoReglaAvanzada = "B";
        this.nombreElementoSeleccionado = undefined;
        this.reglaAvanzada.referencia = undefined;
        this.reglaAvanzada.fechaIndUltimo = "A";
        this.mostrarModalInsertarReglaAvanzada = false;
    }

    // Encargado de procesar el llamado de paginación de atributo dinamico
    loadDataAtributosDinamicos(event: LazyLoadEvent) {
        this.rowOffsetAtributos = event.first;
        this.cantRowsPorPaginaAtributo = event.rows;
        this.busquedaAtributos();
    }

    /**
     * Encargado de procesar los llamados para paginacion
     */
    loadDataReglasAvanzadas(event: LazyLoadEvent) {
        this.rowOffsetReglasAvanzadas = event.first;
        this.cantRowsReglasAvanzadas = event.rows;
        switch (this.reglaAvanzada.indTipoRegla) {
            //abrio o vio un mensaje
            case "B": {
                this.obtenerListaMensajes();
                break;
            }
            case "C": {
                this.obtenerListaMensajes();
                break;
            }
            case "D": {//redimio un premio
                this.obtenerListaPremios();
                break;
            }
            case "E": {//asto de metrica
                this.obtenerListaMetricas();
                break;
            }
            case "I": {//tipo de dispositivo
                this.optionsDispositivosDisponibles = [...this.optionsDispositivosDisponibles, { label: "IOS", value: "A" }];
                this.optionsDispositivosDisponibles = [...this.optionsDispositivosDisponibles, { label: "Android", value: "B" }];
                break;
            }
            case "J": {//posocion en leaderboard
                this.obtenerListaLeaderboards();
                break;
            }
            case "K": {//compras por tienda (ubicacion)
                this.obtenerListaUbicaciones();
                break;
            }
            case "L": {
                this.obtenerListaPromociones();
                break;
            }
            case "M": {//redimio o vio una promocion
                this.obtenerListaPromociones();
                break;
            }
            case "N": {//compro producto especifico
                this.obtenerListaProductos();
                break;
            }
            //finalizo/completo/aprobo/una mision
            case "Q": {
                this.obtenerListaMisiones();
                break;
            }
            case "R": {
                this.obtenerListaMisiones();
                break;
            }
            case "O": {
                this.obtenerListaMisiones();
                break;
            }
            case "P": {
                this.obtenerListaMisiones();
                break;
            }
            case "R": {//tiene una insignia
                this.obtenerListaInsignias();
                break;
            }
            case "S": { //pertenece a grupo
                this.obtenerListaGrupos();
                break;
            }
            case "T": { //pertenece a semgento
                this.obtenerListaSegmentos();
                break;
            }
            /*case "U": { //pertenece a semgento
                this.obtenerListaGrupos();
                break;
            }*/
            default: {
                break;
            }
        }

    }

    loadDataMetrica(event: any) {
        this.filaMetricas = event.first;
        this.cantidadMetricas = event.rows;
        this.mapeoMetricas();
    }

    seleccionarMetrica(metrica: Metrica) {
        if(this.tipoRegla=='MC'){
            this.reglaMetricaCambio.idMetrica.idMetrica= metrica.idMetrica;
        }
        if(this.tipoRegla=='MB'){
            this.reglaMetricaBalance.idMetrica.idMetrica = metrica.idMetrica;
        }
        this.reglaMetricaCambio.idMetrica.idMetrica= metrica.idMetrica;
        this.nombreMetrica = metrica.nombre;
        this.displayDialogMetrica = false;
    }

    /**
     * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
     * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
     * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
     * no siempre sea necesario llamar al api para actualizar los datos
     */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Obtiene un segmento del API, setea los valores del segmento obtenido para evitar perder el puntero al segmento que fue iyectado al arbol de dependencias
     * por angular y permitir que los hijos del arbol de componentes compartan dicho puntero para sincronizar datos del detalle de segmento
     */
    obtenerSegmento() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoService.token = tkn;
                let sub = this.segmentoService.getSegmento(this.segmento.idSegmento).subscribe(
                    (data: Segmento) => {
                        this.copyValuesOf(this.segmento, data)
                    },
                    (error) => {
                        this.manejaError(error)
                    },
                    () => sub.unsubscribe()
                );
            });
    }

    /**
     * Selecciona un atributo dinamico para usarlo en una regla de atributo de miembro
     */
    seleccionarAtributo(atributo: AtributoDinamico) {
        this.displaySeleccAtrDinamico = false;
        this.reglaMiembroAtb.atributoDinamico = atributo;
        if (atributo.indTipoDato == 'B') {
            this.reglaMiembroAtb.valorComparacion = true;
        }
    }
    /** Encargado de limpoar los valores de memoria para que el form se limpie 
     * en caso que el usuario se arrepienta y seleccione un nuevo tipo de regla
     */
    clearAtributo() {
        this.reglaMiembroAtb.indAtributo = "";
        this.reglaMiembroAtb.indOperador = "";
        this.reglaMiembroAtb.valorComparacion = "";
    }
    /** Encargado de limpoar los valores de memoria para que el form se limpie 
    * en caso que el usuario se arrepienta y seleccione un nuevo tipo de regla
    */
    clearBalance() {
        this.reglaMetricaBalance.indOperador = "";
        this.reglaMetricaBalance.valorComparacion = "";
    }
    /** Encargado de limpoar los valores de memoria para que el form se limpie 
    * en caso que el usuario se arrepienta y seleccione un nuevo tipo de regla
    */
    clearMetricaCambio() {
        this.reglaMetricaCambio.indValor = "D";
        this.reglaMetricaCambio.fechaInicio = new Date;
        this.reglaMetricaCambio.fechaFinal = new Date;
    }

    /** 
     * Metodo init: encargado de incializaciones, actualiza los datos de las listas de metricas, grupo de reglas en cuestion, 
     * y listas de treglas pertenecientes a dicho grupo
    */
    ngOnInit() {
        this.onChangeSelectAtributo();
        this.obtenerSegmento();
        this.actualizarListaReglas();
    }
    /**
     * Actualiza los valores de las listas de reglas dependiendo del indicador de tipo de regla activo en ese momento
     */
    actualizarListaReglas() {
        this.reglaAvanzadaList = undefined;
        this.reglaMiembroAtbList = undefined;
        this.reglaMetricaBalanceList = undefined;
        this.reglaMetricaCambioList = undefined;

        if (this.tipoRegla == "AT") {
            this.actualizarListaMiembroAtributo();
        }
        if (this.tipoRegla == "MB") {
            this.actualizarListasMetricabalance();
        }
        if (this.tipoRegla == "MC") {
            this.actualizarListasMetricaCambio();
        }
        if (this.tipoRegla == "AV") {
            this.actualizarListaReglasAvanzadas();
        }
        if (this.tipoRegla == "EP") {
            this.actualizarListaReglasPreferencia();
        }
        if (this.tipoRegla == "EE") {
            this.actualizarListaReglasEncuesta();
        }
    }
    /**
     * obtiene la lista de reglas de encuesta asociadas a la lista de regla
     */
    actualizarListaReglasEncuesta() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaEncuestaService.token = tkn;

                let sub = this.reglaEncuestaService.getReglasEncuesta(this.segmento.idSegmento, this.listaReglas.idLista).subscribe(
                    (data: Response) => {
                        this.reglaEncuestaList = data.json();

                    },
                    (error) => this.manejaError(error),
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**
     * Obtiene el listado de reglas de preferencia asociadas a la lista de reglas
     */
    actualizarListaReglasPreferencia() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaPreferenciaService.token = tkn;

                let sub = this.reglaPreferenciaService.getReglasPreferencia(this.segmento.idSegmento, this.listaReglas.idLista).subscribe(
                    (data: Response) => {
                        this.reglaPreferenciaList = data.json();
                    },
                    (error) => this.manejaError(error),
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**
     * Encargado de sincronizar el contenido de la lista de reglas de balance de metrica
     */
    actualizarListasMetricabalance() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaMetricaBalanceService.token = tkn;

                let sub = this.reglaMetricaBalanceService.getReglaMetricaBalanceList(this.segmento.idSegmento, this.listaReglas.idLista).subscribe(
                    (data) => {
                        this.reglaMetricaBalanceList = data;

                    },
                    (error) => this.manejaError(error),
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**
      * Encargado de sincronizar el contenido de la lista de reglas de atributo de miembro
      */
    actualizarListaMiembroAtributo() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaMiembroAtributoService.token = tkn;
                let sub = this.reglaMiembroAtributoService.getReglaMiembroAtributoList(this.segmento.idSegmento, this.listaReglas.idLista).subscribe(
                    (data: ReglaMiembroAtb[]) => {
                        this.reglaMiembroAtbList = data;
                        this.reglaMiembroAtbList.forEach(
                            (element: ReglaMiembroAtb) => {
                                if (element.indAtributo == "R" && element.atributoDinamico.indTipoDato == 'F') {
                                    element.valorComparacion = new Date(element.valorComparacion).toLocaleString().split(" ")[0]
                                }
                            }
                        );
                    },
                    (error) => this.manejaError(error),
                    () => { sub.unsubscribe(); }
                )
            });
    }
    /**
      * Encargado de sincronizar el contenido de la lista de reglas de cambio de metrica
      */
    actualizarListasMetricaCambio() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaMetricaCambioService.token = tkn;

                let sub = this.reglaMetricaCambioService.getReglaMetricaCambioList(this.segmento.idSegmento, this.listaReglas.idLista).subscribe(
                    (data) => {
                        this.reglaMetricaCambioList = data;
                    },
                    (error) => this.manejaError(error),
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**
     * encargado de sincronizar el contenido de la lista de reglas avanzadas
     * 
     */
    actualizarListaReglasAvanzadas() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaAvanzadaService.token = tkn;
                let sub = this.reglaAvanzadaService.getReglasAvanzadasLista(this.segmento.idSegmento, this.listaReglas.idLista).subscribe(
                    (data) => {
                        this.reglaAvanzadaList = data;
                        this.reglaAvanzadaList.forEach((regla: ReglaAvanzada) => {
                            this.kc.getToken().then((tkn: string) => {
                                switch (regla.indTipoRegla) {
                                    case "B": {//abrio mensaje
                                        this.notificacionService.token = tkn;
                                        let sub = this.notificacionService.getNotificacion(regla.referencia).subscribe(
                                            (data: Notificacion) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "C": {//vio un mensaje
                                        this.notificacionService.token = tkn;
                                        let sub = this.notificacionService.getNotificacion(regla.referencia).subscribe(
                                            (data: Notificacion) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "D": {//redimio un premio
                                        this.premioService.token = tkn;
                                        let sub = this.premioService.obtenerPremioPorId(regla.referencia).subscribe(
                                            (data: Premio) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "E": {//redimio un premio
                                        this.metricaService.token = tkn;
                                        let sub = this.metricaService.getMetrica(regla.referencia).subscribe(
                                            (data: Metrica) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "I": {//tipo de dispositivo
                                        this.optionsDispositivosDisponibles = [...this.optionsDispositivosDisponibles, { label: "IOS", value: "A" }];
                                        this.optionsDispositivosDisponibles = [...this.optionsDispositivosDisponibles, { label: "Android", value: "B" }];
                                        ///this.mostrarModalInsertarReglaAvanzada = true;
                                        break;
                                    }
                                    case "J": {//posocion en leaderboard
                                        this.leaderboardService.token = tkn;
                                        let sub = this.leaderboardService.getLeaderboard(regla.referencia).subscribe(
                                            (data: Leaderboard) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "K": {//compras por tienda (ubicacion)
                                        this.ubicacionService.token = tkn;
                                        let sub = this.ubicacionService.obtenerUbicacionId(regla.referencia).subscribe(
                                            (data: Ubicacion) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "L": {//redimio o vio una promocion
                                        this.promocionService.token = tkn;
                                        let sub = this.promocionService.getPromocion(regla.referencia).subscribe(
                                            (data: Promocion) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    } case "M": {
                                        this.promocionService.token = tkn;
                                        let sub = this.promocionService.getPromocion(regla.referencia).subscribe(
                                            (data: Promocion) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "N": {//compro producto especifico
                                        this.productoService.token = tkn;
                                        let sub = this.productoService.obtenerProductoPorId(regla.referencia).subscribe(
                                            (data: Producto) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "O": {
                                        this.misionService.token = tkn;
                                        let sub = this.misionService.getMision(regla.referencia).subscribe(
                                            (data: Mision) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "P": { //finalizo/completo/aprobo/una mision
                                        this.misionService.token = tkn;
                                        let sub = this.misionService.getMision(regla.referencia).subscribe(
                                            (data: Mision) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "Q": {
                                        this.misionService.token = tkn;
                                        let sub = this.misionService.getMision(regla.referencia).subscribe(
                                            (data: Mision) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "R": {//tiene insignia
                                        this.insigniaService.token = tkn;
                                        let sub = this.insigniaService.obtenerInsigniaPorId(regla.referencia).subscribe(
                                            (data: Insignia) => { regla.nombreReferencia = data.nombreInsignia }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "S": { //pertenece a grupo
                                        this.grupoService.token = tkn;
                                        let sub = this.grupoService.obtenerGrupoPorId(regla.referencia).subscribe(
                                            (data: Grupo) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "T": { //pertenece a semgento
                                        this.segmentoService.token = tkn;
                                        let sub = this.segmentoService.getSegmento(regla.referencia).subscribe(
                                            (data: Segmento) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    case "U": { //pertenece a semgento
                                        this.segmentoService.token = tkn;
                                        let sub = this.segmentoService.getSegmento(regla.referencia).subscribe(
                                            (data: Segmento) => { regla.nombreReferencia = data.nombre }, (error) => { this.manejaError }, () => { sub.unsubscribe() }
                                        );
                                        break;
                                    }
                                    default: {
                                        break;
                                    }
                                }
                            });
                        });
                    },
                    (error) => this.manejaError(error),
                    () => { sub.unsubscribe(); }
                );
            });
    }


    /**
      * Encargado de el manejo de errores
      */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error))
    }

    /**
      * Encargado de agregar una regla de atributo de miembro, la cual esta previamente almacenada en
      * la variable reglaMiembroAtb
      */
    agregarReglaMiembroAtributo() {

        /**Si la regla no es de tipo nivel de metrica, entonces la metrica y el nivel deben ir como no definidos para ser ignorados al parsear el objeto */
        if (!(this.reglaMiembroAtb.indAtributo == "J" || this.reglaMiembroAtb.indAtributo == "I")) {
            this.reglaMiembroAtb.metrica = undefined;
            this.reglaMiembroAtb.nivelMetrica = undefined;
        }
        /**Si el atributo no es atributo dinamico entonces el atributo dinamico debe ir como no definido para ser ignorado al parsear el objeto */
        if (!(this.reglaMiembroAtb.indAtributo == "R")) {
            this.reglaMiembroAtb.atributoDinamico = undefined;
        }

        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaMiembroAtributoService.token = tkn;
                let sub = this.reglaMiembroAtributoService.addReglaMiembroAtributo(
                    this.segmento.idSegmento, this.listaReglas.idLista, this.reglaMiembroAtb).subscribe(
                    (data) => {
                        this.reglaMiembroAtb = new ReglaMiembroAtb();
                        this.reglaMiembroAtb.indOperador = "A";
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.mostrarModalInsertar = false;
                        this.actualizarListaMiembroAtributo();
                        /**Si la regla es de tipo nivel de metrica */
                        if ((this.reglaMiembroAtb.indAtributo == "J" || this.reglaMiembroAtb.indAtributo == "I")) {
                            this.reglaMiembroAtb.metrica = undefined;
                            this.reglaMiembroAtb.nivelMetrica = undefined;
                        }
                        /**Si el atributo es atributo dinamico */
                        if (this.reglaMiembroAtb.indAtributo == "R") {
                            this.reglaMiembroAtb.atributoDinamico = undefined;
                        }
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => {
                        sub.unsubscribe();
                    }
                    );
            });
    }
    /**
      * Encargado de agregar una regla de balance de metrica, la cual esta previamente almacenada en
      * la variable reglaMetricaBalance
      */
    agregarReglaMetricaBalance() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaMetricaBalanceService.token = tkn;
                let sub = this.reglaMetricaBalanceService.addReglaMetricaBalance(
                    this.segmento.idSegmento, this.listaReglas.idLista, this.reglaMetricaBalance).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.mostrarModalInsertar = false;
                        this.actualizarListasMetricabalance();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.mostrarModalInsertar = false;
                    },
                    () => {
                        sub.unsubscribe()
                    }
                    );
            });
    }
    /**
      * Encargado de agregar una regla de cambio de metrica, la cual esta previamente almacenada en
      * la variable reglaMetricaCambio
      */
    agregarReglaMetricaCambio() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaMetricaCambioService.token = tkn;
                let sub = this.reglaMetricaCambioService.addReglaMetricaCambio(
                    this.segmento.idSegmento, this.listaReglas.idLista, this.reglaMetricaCambio).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.mostrarModalInsertar = false;
                        this.actualizarListasMetricaCambio();

                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                    );
            });
    }
    /**
      * Encargado de agregar una regla avanzada, la cual esta previamente almacenada en
      * la variable reglaAvanzada
      */
    agregarReglaAvanzada() {
        //chequeos de datos
        if (this.indPeriodoReglaAvanzada == "B") {
            this.reglaAvanzada.fechaFinal = undefined;
            this.reglaAvanzada.fechaInicio = undefined;

        } else if (this.indPeriodoReglaAvanzada == "D") {
            this.reglaAvanzada.fechaFinal = undefined;
            this.reglaAvanzada.fechaCantidad = undefined;
            this.reglaAvanzada.fechaIndUltimo = undefined;
        }
        else if (this.indPeriodoReglaAvanzada == "H") {
            this.reglaAvanzada.fechaInicio = undefined;
            this.reglaAvanzada.fechaCantidad = undefined;
            this.reglaAvanzada.fechaIndUltimo = undefined;
        }
        else if (this.indPeriodoReglaAvanzada = "E") {
            this.reglaAvanzada.fechaCantidad = undefined;
            this.reglaAvanzada.fechaIndUltimo = undefined;
        }
        else if (this.indPeriodoReglaAvanzada == "F") {
            this.reglaAvanzada.fechaFinal = undefined;
            this.reglaAvanzada.fechaCantidad = undefined;
            this.reglaAvanzada.fechaIndUltimo = undefined;
        }
        if (this.indPeriodoReglaAvanzada == "G") {
            this.reglaAvanzada.fechaFinal = undefined;
            this.reglaAvanzada.fechaInicio = undefined;
            this.reglaAvanzada.fechaCantidad = undefined;
        }
        if (this.reglaAvanzada.referencia == "") { this.reglaAvanzada.referencia = undefined; }
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaAvanzadaService.token = tkn;
                let sub = this.reglaAvanzadaService.createReglaAvanzada(
                    this.segmento.idSegmento, this.listaReglas.idLista, this.reglaAvanzada).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.mostrarModalInsertar = false;
                        this.actualizarListaReglasAvanzadas();
                        // this.reglaAvanzada = new ReglaAvanzada();
                        this.resetFieldsReglasAvanzadas();
                        this.reglaAvanzada.indTipoRegla = "A";
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                    );
            });

    }
    /**
      * Encargado de eliminar una regla de cualquier tipo 
      * Nota: en el momento de desarrollo el operador instanceof no estaba funcionando correctamente,por lo 
      * tanto fue necesario utilizar un workaround para determinar el tipo de regla, es decir usar una variable
      * tipo string para controlar el tipo
      *
      * Muestra entonces el modal para que el usuario confirme la accion de eliminar una regla del segmento
      */
    selectReglaEliminar(regla: any, tipoRegla: string) {
        this.tipoReglaEliminar = tipoRegla;
        this.reglaEliminar = regla;
    }
    /**
      * Encargado de eliminar una regla del segmento, es disparado por el boton de confirmacion del modal
      * Realiza el llamado correspondiente al API para eliminar la regla del segmento segun su tipo
      */
    confirmAction() {
        switch (this.tipoReglaEliminar) {
            //regla de tipo balance de metrica
            case 'MB': {
                let regla = this.reglaEliminar as ReglaMetricaBalance;
                this.kc.getToken().then(
                    (tkn: string) => {
                        this.reglaMetricaBalanceService.token = tkn;
                        let sub = this.reglaMetricaBalanceService.deleteReglaMetricaBalance(
                            this.segmento.idSegmento, this.listaReglas.idLista, regla.idRegla).subscribe(
                            () => {

                                this.actualizarListasMetricabalance();
                            },
                            (error) => { this.manejaError(error) },
                            () => { sub.unsubscribe(); }
                            );
                    });
                break;
            }
            //regla de tipo cambio de metrica
            case 'MC': {
                let regla = this.reglaEliminar as ReglaMetricaCambio;
                this.kc.getToken().then(
                    (tkn: string) => {
                        this.reglaMetricaCambioService.token = tkn;
                        let sub = this.reglaMetricaCambioService.deleteReglaMetricaCambio(
                            this.segmento.idSegmento, this.listaReglas.idLista, regla.idRegla).subscribe(
                            () => {
                                this.actualizarListasMetricaCambio();
                            },
                            (error) => { this.manejaError(error) },
                            () => { sub.unsubscribe(); }
                            );
                    });
                break;
            }
            //regla de atributo de usuario
            case 'AT': {
                this.kc.getToken().then(
                    (tkn: string) => {
                        this.reglaMiembroAtributoService.token = tkn;
                        let sub = this.reglaMiembroAtributoService.deleteReglaMiembroAtributo(
                            this.segmento.idSegmento, this.listaReglas.idLista, this.reglaEliminar.idRegla).subscribe(
                            () => {
                                this.actualizarListaMiembroAtributo();
                            },
                            (error) => { this.manejaError(error); },
                            () => { sub.unsubscribe(); }
                            );
                    });
                break;
            }
            //regla de atributo de usuario
            case 'AV': {
                this.kc.getToken().then(
                    (tkn: string) => {
                        this.reglaAvanzadaService.token = tkn;
                        let sub = this.reglaAvanzadaService.deleteReglaAvanzada(
                            this.segmento.idSegmento, this.listaReglas.idLista, this.reglaEliminar.idRegla).subscribe(
                            () => {
                                this.actualizarListaReglasAvanzadas();
                            },
                            (error) => { this.manejaError(error); },
                            () => { sub.unsubscribe(); }
                            );
                    });
                break;
            }
            //regla de atributo de usuario
            case 'EE': {
                this.kc.getToken().then(
                    (tkn: string) => {
                        this.reglaEncuestaService.token = tkn;
                        let sub = this.reglaEncuestaService.removerReglaEncuesta(
                            this.segmento.idSegmento, this.listaReglas.idLista, this.reglaEliminar.idRegla).subscribe(
                            () => {
                                this.actualizarListaReglas();
                            },
                            (error) => { this.manejaError(error); },
                            () => { sub.unsubscribe(); }
                            );
                    });
                break;
            }
            //regla de atributo de usuario
            case 'EP': {
                this.kc.getToken().then(
                    (tkn: string) => {
                        this.reglaPreferenciaService.token = tkn;
                        let sub = this.reglaPreferenciaService.removerReglaPreferencia(
                            this.segmento.idSegmento, this.listaReglas.idLista, this.reglaEliminar.idRegla).subscribe(
                            () => {
                                this.actualizarListaReglas();
                            },
                            (error) => { this.manejaError(error); },
                            () => { sub.unsubscribe(); }
                            );
                    });
                break;
            }
        }
        this.mostrarModalEliminar = false;
    }
    /**
     * Despleiga el modal de leiminar regla para un dato tipo de regla y su indicador
     */
    displayModalEliminar(regla: any, indTipoRegla: string) {
        this.selectReglaEliminar(regla, indTipoRegla);
        this.mostrarModalEliminar = true;
    }

    /**Metodo que obtiene los atributos dinamicos para que sean seleccionados
     * en caso de una regla de atributos sobre el atributo Dinámico, es invocado
     * por el boton de agregar atributo dinamico y abre el dialog de seleccion
     * de atributos automaticamente*/
    busquedaAtributos() {
        this.busquedaAtributo = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busquedaAtributo += element + " ";
            });
            this.busquedaAtributo = this.busquedaAtributo.trim();
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.atributoService.token = tkn;
                let sub = this.atributoService.busquedaAtributosDinamicos(this.cantRowsPorPaginaAtributo, this.rowOffsetAtributos, ["P"], this.tipoDato, this.busquedaAtributo).subscribe(
                    (data: Response) => {
                        if (data.headers.has("Content-Range")) {
                            this.cantRowsAtributos = +data.headers.get("Content-Range").split("/")[1];
                        }
                        this.displaySeleccAtrDinamico = true;
                        this.listaAtributosDinamicos = data.json();
                    },
                    (error) => this.manejaError(error),
                    () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Actualiza los registros de offset de 
     */
    actualizarOffset(response: Response) {
        if (response.headers.has("Content-Range")) {
            this.cantRowsReglasAvanzadas = +response.headers.get("Content-Range").split("/")[1];
        }
    }
    /**
     * Obtiene la lista de notificaciones para usarlas a la hora de definir reglas avanzadas
     */
    obtenerListaMensajes() {
        this.terminoBusquedaReglasAvanzadas = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.terminoBusquedaReglasAvanzadas += element + " ";
            });
            this.terminoBusquedaReglasAvanzadas = this.terminoBusquedaReglasAvanzadas.trim();
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;

                let sub = this.notificacionService.buscarNotificaciones([this.terminoBusquedaReglasAvanzadas], ["P", "E"], this.rowOffsetReglasAvanzadas, this.cantRowsPorPaginaReglasAvanzadas).subscribe(
                    (data: Response) => {
                        this.listaMensajesDisponibles = <Notificacion[]>data.json();
                        this.actualizarOffset(data);
                        this.mostrarModalInsertarReglaAvanzada = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
 * Obtiene la lista de premios para usarlas a la hora de definir reglas avanzadas
 */
    obtenerListaPremios() {
        this.terminoBusquedaReglasAvanzadas = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.terminoBusquedaReglasAvanzadas += element + " ";
            });
            this.terminoBusquedaReglasAvanzadas = this.terminoBusquedaReglasAvanzadas.trim();
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.premioService.token = tkn;
                let sub = this.premioService.busquedaPremios(this.cantRowsPorPaginaReglasAvanzadas, this.rowOffsetReglasAvanzadas, [], ['P'], this.terminoBusquedaReglasAvanzadas).subscribe(
                    (data: Response) => {
                        this.listaPremiosDisponibles = <Premio[]>data.json();
                        this.actualizarOffset(data);
                        this.mostrarModalInsertarReglaAvanzada = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Obtiene la lista de leaderboards para usarlas a la hora de definir reglas avanzadas
     */
    obtenerListaMetricas() {
        this.terminoBusquedaReglasAvanzadas = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.terminoBusquedaReglasAvanzadas += element + " ";
            });
            this.terminoBusquedaReglasAvanzadas = this.terminoBusquedaReglasAvanzadas.trim();
        }
        let estado = ['P'];
        this.kc.getToken().then(
            (tkn: string) => {
                this.metricaService.token = tkn;
                let sub = this.metricaService.buscarMetricas(estado, this.cantRowsPorPaginaReglasAvanzadas, this.rowOffsetReglasAvanzadas, this.terminoBusquedaReglasAvanzadas).subscribe(
                    (response: Response) => {
                        this.actualizarOffset(response);
                        this.listaMetricasDisponibles = response.json();
                        this.mostrarModalInsertarReglaAvanzada = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }

    // /**
    //  * Obtiene la lista de nveles para ser usados a la hora de definir relas avanzadas
    //  */
    // obtenerListaNiveles(idGrupo: string) {
    //     this.kc.getToken().then(
    //         (tkn:string) => {
    //             this.nivelService.token = tkn;
    //             let sub = this.nivelService.obtenerListaNiveles("").subscribe(
    //                 (data: Nivel[]) => {
    //                     this.listaNivelesDisponibles = data;
    //                     this.mostrarModalInsertarReglaAvanzada = true;
    //                 },
    //                 (error) => {
    //                     this.manejaError;
    //                 },
    //                 () => {
    //                     sub.unsubscribe();
    //                 });
    //         }
    //     )

    // }

    /**
     * Obtiene la lista de nveles para ser usados a la hora de definir relas avanzadas
     */
    obtenerListaGruposNivel(idGrupo: string) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.nivelService.token = tkn;
                let sub = this.nivelService.obtenertListaGrupoNivel().subscribe(
                    (data: GrupoNivel[]) => {
                        this.listaGruposNivelDisponibles = data;
                        this.mostrarModalInsertarReglaAvanzada = true;
                    },
                    (error) => {
                        this.manejaError;
                    },
                    () => {
                        sub.unsubscribe();
                    });
            }
        )

    }
    /**
 * Obtiene la lista de leaderboards para usarlas a la hora de definir reglas avanzadas
 */
    obtenerListaLeaderboards() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.leaderboardService.token = tkn;

                let sub = this.leaderboardService.getListaLeaderboards().subscribe(
                    (data: Leaderboard[]) => {
                        this.listaLeaderboardsDisponibles = data;
                        this.mostrarModalInsertarReglaAvanzada = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Obtiene la lista de ubicaciones para usarlas a la hora de definir reglas avanzadas
     */
    obtenerListaUbicaciones() {
        this.terminoBusquedaReglasAvanzadas = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.terminoBusquedaReglasAvanzadas += element + " ";
            });
            this.terminoBusquedaReglasAvanzadas = this.terminoBusquedaReglasAvanzadas.trim();
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.ubicacionService.token = tkn;
                let sub = this.ubicacionService.busquedaUbicacion(["P"], this.cantRowsPorPaginaReglasAvanzadas, this.rowOffsetReglasAvanzadas, this.terminoBusquedaReglasAvanzadas).subscribe(
                    (response: Response) => {
                        this.actualizarOffset(response);
                        this.listaUbicacionesDisponibles = response.json();
                        this.mostrarModalInsertarReglaAvanzada = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Obtiene la lista de promociones para definir reglas avanzadas
     */
    obtenerListaPromociones() {
        /*this.terminoBusquedaReglasAvanzadas = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.terminoBusquedaReglasAvanzadas += element + " ";
            });
            this.terminoBusquedaReglasAvanzadas = this.terminoBusquedaReglasAvanzadas.trim();
        }*/
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.buscarPromociones(this.listaBusqueda, ["A"], this.rowOffsetReglasAvanzadas, this.cantRowsPorPaginaReglasAvanzadas).subscribe(
                    (data: Response) => {
                        this.listaPromocionesDisponibles = <Promocion[]>data.json();
                        this.actualizarOffset(data);
                        this.mostrarModalInsertarReglaAvanzada = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
     * Obtiene la lista de productos para la definicon de reglas avanzadas
     */
    obtenerListaProductos() {
        this.terminoBusquedaReglasAvanzadas = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.terminoBusquedaReglasAvanzadas += element + " ";
            });
            this.terminoBusquedaReglasAvanzadas = this.terminoBusquedaReglasAvanzadas.trim();
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.productoService.token = tkn;

                let sub = this.productoService.busquedaProducto(["P"], this.cantRowsPorPaginaReglasAvanzadas, this.rowOffsetReglasAvanzadas, this.terminoBusquedaReglasAvanzadas).subscribe(
                    (data: Response) => {
                        this.listaProductosDisponibles = <Producto[]>data.json();
                        this.actualizarOffset(data);
                        this.mostrarModalInsertarReglaAvanzada = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
     * Metodo encargado de obtener el listado de misiones en elsistema, esto para la definicon de reglas avanzadas sobre mision
     */
    obtenerListaMisiones() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;

                let sub = this.misionService.buscarMisiones(this.listaBusqueda, ["A"], this.rowOffsetReglasAvanzadas, this.cantRowsPorPaginaReglasAvanzadas).subscribe(
                    (data: Response) => {
                        this.listaMisionesDisponibles = <Mision[]>data.json();
                        this.actualizarOffset(data);
                        this.mostrarModalInsertarReglaAvanzada = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
     * Metodo encargado de obtener la lista de insignias para la definicion de reglas avanzadas
     */
    obtenerListaInsignias() {
        this.terminoBusquedaReglasAvanzadas = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.terminoBusquedaReglasAvanzadas += element + " ";
            });
            this.terminoBusquedaReglasAvanzadas = this.terminoBusquedaReglasAvanzadas.trim();
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.insigniaService.token = tkn;
                let sub = this.insigniaService.buscarInsigniasTemp(['P'], this.cantRowsPorPaginaReglasAvanzadas, this.rowOffsetReglasAvanzadas, [], this.terminoBusquedaReglasAvanzadas).subscribe(
                    (data: Response) => {
                        this.listaInsigniasDisponibles = <Insignia[]>data.json();
                        this.actualizarOffset(data);
                        this.mostrarModalInsertarReglaAvanzada = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
     * Obtiene la lista de los grupos de miemroe para ser usados como comparadores en la definicion de reglas avanzadas
     */
    obtenerListaGrupos() {
        this.terminoBusquedaReglasAvanzadas = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.terminoBusquedaReglasAvanzadas += element + " ";
            });
            this.terminoBusquedaReglasAvanzadas = this.terminoBusquedaReglasAvanzadas.trim();
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.grupoService.token = tkn;
                let sub = this.grupoService.buscarGrupo(this.cantRowsPorPaginaReglasAvanzadas, this.rowOffsetReglasAvanzadas, this.terminoBusquedaReglasAvanzadas).subscribe(
                    (data: Response) => {
                        this.listaGruposDisponibles = data.json();
                        this.mostrarModalInsertarReglaAvanzada = true;
                        this.actualizarOffset(data);
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
     * Obtiene la lista de segmentos para ser usados como valor de copracion para las reglas avanzadas
     */
    obtenerListaSegmentos() {
        this.terminoBusquedaReglasAvanzadas = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.terminoBusquedaReglasAvanzadas += element + " ";
            });
            this.terminoBusquedaReglasAvanzadas = this.terminoBusquedaReglasAvanzadas.trim();
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoService.token = tkn;

                let sub = this.segmentoService.buscarSegmentos([this.terminoBusquedaReglasAvanzadas], ["AC"], this.cantRowsPorPaginaReglasAvanzadas, this.rowOffsetReglasAvanzadas).subscribe(
                    (data: Response) => {
                        this.listaSegmentosDisponibles = <Segmento[]>data.json();
                        this.actualizarOffset(data);
                        this.mostrarModalInsertarReglaAvanzada = true;
                    },
                    (error) => {
                        this.manejaError(error)
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }

    /**
     * Agrega un elemento como comparador para la lista de reglas avanzadas
     * este elemento puede ser de tipo mision, 
     * 
     * typeof es necesario por que el instanceof no funciona para este tipo de objeto
     * y algunas entidades (premio por ejemplo) tienen atributos con nombre igual (premio.idMetrica), por lo tanto no basta
     * con chequear la existencia de un atributo
     */
    agregarElementoReglaAvanzada(elemento: any) {
        //notificacion
        if ((this.reglaAvanzada.indTipoRegla == "B" || this.reglaAvanzada.indTipoRegla == "C") && elemento.idNotificacion && typeof elemento.idNotificacion == "string") {
            this.reglaAvanzada.referencia = elemento.idNotificacion;
            this.nombreElementoSeleccionado = elemento.nombre;
        }
        //premio
        if (this.reglaAvanzada.indTipoRegla == "D" && elemento.idPremio && typeof elemento.idPremio == "string") {
            this.reglaAvanzada.referencia = elemento.idPremio;
            this.nombreElementoSeleccionado = elemento.nombre;
        }
        //promo
        if ((this.reglaAvanzada.indTipoRegla == "L" || this.reglaAvanzada.indTipoRegla == "M") && elemento.idPromocion && typeof elemento.idPromocion == "string") {
            this.reglaAvanzada.referencia = elemento.idPromocion;
            this.nombreElementoSeleccionado = elemento.nombre;
        }
        //ubicacion
        if (this.reglaAvanzada.indTipoRegla == "K" && elemento.idUbicacion && typeof elemento.idUbicacion == "string") {
            this.reglaAvanzada.referencia = elemento.idUbicacion;
            this.nombreElementoSeleccionado = elemento.nombre;
        }
        //leaderboard
        if (this.reglaAvanzada.indTipoRegla == "J" && elemento.idTabla && typeof elemento.idTabla == "string") {
            this.reglaAvanzada.referencia = elemento.idTabla;
            this.nombreElementoSeleccionado = elemento.nombre;
        }
        //productos
        if (this.reglaAvanzada.indTipoRegla == "N" && elemento.idProducto && typeof elemento.idProducto == "string") {
            this.reglaAvanzada.referencia = elemento.idProducto;
            this.nombreElementoSeleccionado = elemento.nombre;
        }
        //mision
        if ((this.reglaAvanzada.indTipoRegla == "O" || this.reglaAvanzada.indTipoRegla == "P" || this.reglaAvanzada.indTipoRegla == "Q") && elemento.idMision && typeof elemento.idMision == "string") {
            this.reglaAvanzada.referencia = elemento.idMision;
            this.nombreElementoSeleccionado = elemento.nombre;
        }
        //insignia
        if (this.reglaAvanzada.indTipoRegla == "R" && elemento.idInsignia && typeof elemento.idInsignia == "string") {
            this.reglaAvanzada.referencia = elemento.idInsignia;
            this.nombreElementoSeleccionado = elemento.nombreInsignia;
        }
        //Metrica
        if (this.reglaAvanzada.indTipoRegla == "E" && elemento.idMetrica && typeof elemento.idMetrica == "string") {
            this.reglaAvanzada.referencia = elemento.idMetrica;
            this.nombreElementoSeleccionado = elemento.nombre;
        }
        //grupo
        if (this.reglaAvanzada.indTipoRegla == "S" && elemento.idGrupo && typeof elemento.idGrupo == "string") {
            this.reglaAvanzada.referencia = elemento.idGrupo;
            this.nombreElementoSeleccionado = elemento.nombre;
        }
        //nivel
        if (this.reglaAvanzada.indTipoRegla == "U" && elemento.idNivel && typeof elemento.idNivel == "string") {
            this.reglaAvanzada.referencia = elemento.idNivel;
            this.nombreElementoSeleccionado = elemento.nombre;
        }
        //segmento
        if (this.reglaAvanzada.indTipoRegla == "T" && elemento.idSegmento && typeof elemento.idSegmento == "string") {
            this.reglaAvanzada.referencia = elemento.idSegmento;
            this.nombreElementoSeleccionado = elemento.nombre;
        }
        this.mostrarModalInsertarReglaAvanzada = false;

    }
    /**
     * Encargado de mapear los elementos de nivel de metrica en elementos del selecitem
     * a partir del id de grupo que ingresa como parametro
     */
    mapeoNivelesGrupo(idGrupo: string) {
        this.listaNivelesDisponibles = [];
        let sub = this.nivelService.obtenerListaNiveles(idGrupo).subscribe(
            (data: Nivel[]) => {
                data.forEach(nivel => {
                    this.listaNivelesDisponibles = [...this.listaNivelesDisponibles, { value: nivel.idNivel, label: nivel.nombre }]
                });
                if (this.listaNivelesDisponibles.length > 0) {
                    this.reglaMiembroAtb.valorComparacion = this.listaNivelesDisponibles[0].value;
                }
            }, (error) => { this.manejaError(error) }, () => { sub.unsubscribe() }
        );
    }

    /**
     * Encargado de determinar cuales datos son cargados en los dropdown a partir del elemento seleccionado en el dropdown de 
     * tipo de accion para la regla avanzada, ayuda a reducir la carga ya que trae los datos solamente del elemento para el cual la regla
     * aplica
     * !! llamado desde el HTML, drop de tipo de accion en regla avanzada !!
     */
    mapeoElementosReglasAvanzadas() {
        //se liberan recursos
        this.listaMensajesDisponibles = [];
        this.listaUbicacionesDisponibles = [];
        this.listaProductosDisponibles = [];
        this.listaMisionesDisponibles = [];
        this.listaPremiosDisponibles = [];
        this.listaPromocionesDisponibles = [];
        this.listaProductosDisponibles = [];
        this.listaInsigniasDisponibles = [];
        this.optionsDispositivosDisponibles = [];
        this.listaLeaderboardsDisponibles = [];
        this.listaMisionesDisponibles = [];
        this.listaGruposDisponibles = [];
        this.listaSegmentosDisponibles = [];

        switch (this.reglaAvanzada.indTipoRegla) {
            case "B": {
                this.obtenerListaMensajes();
                break;
            }
            case "C": {//abrio o vio un mensaje
                this.obtenerListaMensajes();
                break;
            }
            case "D": {//redimio un premio
                this.obtenerListaPremios();
                break;
            }
            case "E": {//redimio un premio
                this.obtenerListaMetricas();
                break;
            }
            case "I": {//tipo de dispositivo
                this.optionsDispositivosDisponibles = [...this.optionsDispositivosDisponibles, { label: "IOS", value: "A" }];
                this.optionsDispositivosDisponibles = [...this.optionsDispositivosDisponibles, { label: "Android", value: "B" }];
                this.mostrarModalInsertarReglaAvanzada = true;
                break;
            }
            case "J": {//posocion en leaderboard
                this.obtenerListaLeaderboards();
                break;
            }
            case "K": {//compras por tienda (ubicacion)
                this.obtenerListaUbicaciones();
                break;
            }
            case "L": {//redimio o vio una promocion
                this.obtenerListaPromociones();
                break;
            } case "M": {
                this.obtenerListaPromociones();
                break;
            }
            case "N": {//compro producto especifico
                this.obtenerListaProductos();
                break;
            }
            case "O": {
                this.obtenerListaMisiones();
                break;
            }
            case "P": { //finalizo/completo/aprobo/una mision
                this.obtenerListaMisiones();
                break;
            }
            case "Q": {
                this.obtenerListaMisiones();
                break;
            }
            case "R": {//tiene una insignia
                this.obtenerListaInsignias();
                break;
            }
            case "S": { //pertenece a grupo
                this.obtenerListaGrupos();
                break;
            }
            case "T": { //pertenece a semgento
                this.obtenerListaSegmentos();
                break;
            }
            // case "U": { //cuando ccambia a un nivel especifico
            //     this.obtenerListaGruposNivel();
            //     break;
            // }
            default: {
                break;
            }
        }
    }


    /**
     * Metodo encargado de mapear los elementos de el select de niveles de metrica 
     * con los niveles del grupo de niveles seleccionado, obtiene los niveles basandose
     * en el id del grupo de nivel seleccionado
     */
    mapeoSelectNivelesMetrica() {
        this.listaNiveles = [];
        this.reglaMiembroAtb.atributoDinamico
        // indicador de nivel de metrica
        this.kc.getToken().then(
            (tkn: string) => {
                this.metricaService.token = tkn; this.nivelService.token = tkn;
                let sub = this.metricaService.getMetrica(this.reglaMiembroAtb.metrica.idMetrica).subscribe(
                    (data: Metrica) => {
                        this.reglaMiembroAtb.metrica = data;
                        let subs = this.nivelService.obtenerListaNiveles(this.reglaMiembroAtb.metrica.grupoNiveles.idGrupoNivel).subscribe(
                            (data: Nivel[]) => {
                                this.listaNiveles = [...this.listaNiveles, this.i18nService.getLabels("general-default-select")];
                                data.forEach(nivel => {
                                    this.listaNiveles = [...this.listaNiveles, { value: nivel.idNivel, label: nivel.nombre }];
                                });
                            },
                            (error) => this.manejaError(error),
                            () => { subs.unsubscribe(); }
                        );
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); })


            });
    }

    /**Metodo encargado de poblar el drop de los grupos de nivel en reglas de atributo 
     * cuando el atributo es nivel inicial, o nivel en progreso, obtiene las metricas a partir del 
     * id del nivel seleccionado 
     * !! Llamado en el html al insertar reglas !!
     
    mapeoSelectGruposMetrica() {
        this.listaNiveles = [];
        // indicador de nivel de metrica
        this.kc.getToken().then(

            reglaMiembroAtb.metrica.idMetrica

            (tkn:string) => {
                this.nivelService.token = tkn;
                let subs = this.nivelService.ob().subscribe(
                    (data: GrupoNivel[]) => {
                        data.forEach(nivel => {
                            this.listaGrupos=[...this.listaGrupos,{ value: nivel.idGrupoNivel, label: nivel.nombre }];
                        });
                    },
                    (error) => this.manejaError(error),
                    () => { subs.unsubscribe(); }
                );
            });
    }*/
    /**
     * Metodo encargado de poblar el drop de grupos de metrica dependiendo del id de metrica asociada que se seleccione
     * 
     * !! Llamado en el html al insertar reglas de atributo de miembro!!
    
    mapeoSelectMetrica() {
        this.listaNiveles = [];
        // indicador de nivel de metrica
        this.kc.getToken().then(
            (tkn:string) => {
                this.nivelService.token = tkn;
                let subs = this.nivelService.obtenerMetricasNivel(this.reglaMiembroAtb.nivelMetrica.idNivel).subscribe(
                    (data: Metrica[]) => {
                        data.forEach(metrica => {
                            this.listaGrupos=[...this.listaGrupos,{ value: metrica.idMetrica, label: metrica.nombre }];
                        });
                    },
                    (error) => this.manejaError(error),
                    () => { subs.unsubscribe(); }
                );
            });
    }*/

    /**
     * Encargado de poblar el dropdown de metricas para las reglas de cambio de metrica y las reglas de balance de metrica
     */
    mapeoMetricas() {
        let busqueda = "";
        busqueda = this.listaBusqueda.join(" ")
        let estado = ['P'];
        this.kc.getToken().then(
            (tkn: string) => {
                this.metricaService.token = tkn;
                let sub = this.metricaService.buscarMetricas(estado, this.cantidadMetricas, this.filaMetricas, busqueda).subscribe(
                    (response: Response) => {
                        let data = response.json();
                        this.metricasCambio = response.json();
                        this.totalRecordsMetrica = +response.headers.get("Content-Range").split("/")[1];
                        /*this.listaMetricas = [...this.listaMetricas, this.i18nService.getLabels("general-default-select")];
                        data.forEach(metrica => {
                            this.listaMetricas = [...this.listaMetricas, { value: metrica.idMetrica, label: metrica.nombreInterno }];
                        });*/
                        if (data.length > 0) {
                            this.reglaMetricaBalance.idMetrica.idMetrica = data[0].idMetrica;
                            this.reglaMetricaCambio.idMetrica.idMetrica = data[0].idMetrica;
                        }
                    },
                    (error) => this.manejaError(error),
                    () => { sub.unsubscribe(); }
                );
            });
    }

    //Método para obtener la lista de países
    mapeoPaises() {
        this.kc.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.listaPaises = [];
                this.paisService.token = token || "";
                //this.listaPaisesMiembro = this.i18nService.getLabels("general-default-select");
                this.paisService.obtenerPaises().subscribe(
                    (paises: Pais[]) => {
                        this.listaPaises = paises;
                        if (this.listaPaises.length > 0) {
                            this.listaPaises.forEach((pais: Pais) => {
                                this.listaPaisesMiembro = [...this.listaPaisesMiembro, { label: pais.nombrePais, value: pais.alfa3 }];
                            });
                            // this.reglaMiembroAtb.valorComparacion+=paises[0].alfa2;
                        }
                    },
                    (error) =>
                        this.manejaError(error)
                );
            });
    }

    /**
     * Metodo encargado de inciializar los valores que emplearan los dropdown para la seleccion de reglas */
    mapeoSelects() {
        this.mapeoMetricas();//se cargan las metricas para reglas de cambio y balance de metrica
        // this.mapeoSelectGruposMetrica();//se cargan los grupos de niveles para poder seleccionar los niveles en reglas de atributo de  miembro
        // this.mapeoPaises();
        //mapeo de las reglas avanzadas

        this.optionsReglasAvanzadas = this.i18nService.getLabels("segmentos-reglas-avanzadas");

        //Valor fijo
        this.operadorFijo = this.i18nService.getLabels("segmento-regla-operador-fijo");
        //periodos
        this.listaElementosReciente = this.i18nService.getLabels("segmentos-listaElementosReciente");

        this.listaDispositivos = this.i18nService.getLabels("segmentos-listaDispositivos");

        //indicadores de valor booleano
        this.listaOpcionesBoolean = this.i18nService.getLabels("segmentos-listaOpcionesBoolean");

        //estados civiles
        this.listaEstadosCiviles = this.i18nService.getLabels("segmentos-listaEstadosCiviles");

        //niveles de educacion
        this.listaEducacionMiembro = this.i18nService.getLabels("segmentos-listaEducacionMiembro");

        //estados de miembro
        this.listaEstadosMiembro = this.i18nService.getLabels("segmentos-listaEstadosMiembro");

        //generos de miembro
        this.listaGenerosMiembro = this.i18nService.getLabels("segmentos-listaGenerosMiembro");

        //indicadores de atributo
        this.listaAtributos = this.i18nService.getLabels("segmentos-listaAtributos");

        //Mapeo de los valores para los select de operadores de texto
        this.operadoresTexto = this.i18nService.getLabels("segmentos-operadoresTexto");

        //Mapeo de los valores para los select de operadores de fecha
        this.operadoresFecha = this.i18nService.getLabels("segmentos-operadoresFecha");

        //Mapeo de los valores para los select de operadores numericos
        this.operadoresNumericos = this.i18nService.getLabels("segmentos-operadoresNumericos");

        //Mapeo de los valores de tipos de cambio de metrica
        this.listaBalancesMetrica = this.i18nService.getLabels("segmentos-listaBalancesMetrica");

        //Mapeo de los valores de tipos de cambio de metrica
        this.listaBalancesMetricaBalance = this.i18nService.getLabels("segmentos-listaBalancesMetricaBalance");

        //Mapeo de los valores de tiempo
        this.listaPeriodoTiempo = this.i18nService.getLabels("segmentos-listaPeriodoTiempo");

        //Mapeo de los valores de tiempo
        this.listaCalendarizacionReglasAvanzadas = this.i18nService.getLabels("segmentos-listaCalendarizacionReglasAvanzadas");

        //tipos generales de regla
        this.listaTipoRegla = this.i18nService.getLabels("segmentos-listaTipoRegla");
    }

    onChangeSelectAtributo() {

        switch (this.reglaMiembroAtb.indAtributo) {
            case "A": {
                this.reglaMiembroAtb.valorComparacion = new Date();
                break;
            }
            case "B": {
                this.reglaMiembroAtb.valorComparacion = "M";
                break;
            }
            case "H": {
                this.reglaMiembroAtb.valorComparacion = "A";
                break;
            }
            case "C": {
                this.reglaMiembroAtb.valorComparacion = "S";
                break;
            }
            case "O": {
                this.reglaMiembroAtb.valorComparacion = "P";
                break;
            }
            case "M": {
                this.mapeoPaises();
                break;
            }
            default: {
                this.reglaMiembroAtb.valorComparacion = undefined;
                break;
            }
        }
    }
}