import { I18nService } from '../../service';
import { KeycloakService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { SegmentoService } from '../../service/segmento.service';
import { Segmento } from '../../model/index';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/primeng';
import * as Rutas from '../../utils/rutas';
import { PERM_ESCR_SEGMENTOS } from '../common/auth.constants'
import { AuthGuard } from '../common/index';
import { Response } from '@angular/http'
/**
* Flecha Roja Technologies 
* Fernando Aguilar
 * Componente encargado de mostrar la lista general de segmentos, 
 * provee acceso al detalle de los segmentos para su edicion
 */
@Component({
    moduleId: module.id,
    selector: 'lista-segmentos',
    providers: [Segmento, SegmentoService],
    templateUrl: 'segmentos-lista.component.html'
})
export class ListaSegmentosComponent implements OnInit {
    public segmentos: Segmento[];//array con la lista de segmentos correspondientes a la pagina actual de la vista
    public segmentoEliminar: Segmento;//instancia temporal de segmento usada para eliminar segmentos
    public displayModalMensaje: boolean;//true= se muestra el modal

    public displayModalConfirm: boolean;//true= despliega modal de confirmacion eliminar segmento
    public itemsBusqueda: MenuItem[];// para el split button de busqueda avanzada
    public itemsDetalle: MenuItem[];//para el split button de detalle/eliminar segmento
    public busquedaAvanzada: boolean;//true= despliega el form de busqueda avanzada
    public tienePermisosEscritura: boolean;//true si el usuario tiene permiso de escritura en este recurso

    /**Parametros para busqueda */
    public terminosBusqueda: string[]; //terminos que se introducen en el campo de busqueda 
    public tipos: string[] = []; // tipos selecciondos para busqueda avanzada
    public filtros: string[] = []; // aafiltros para la busqueda avanzada
    public preferenciasBusqueda: string[] = ["AC", "BO"]; // almacena los indicadores de estado de segmento spara busqueda avanzada
    public rowOffset: number; // offset del row actual, usado para paginacion
    public cantRegistros: number; // cantidad de registros totales de 
    public cantPorPagina: number; // cantidad de registros por pagina

    constructor(
        public segmentoService: SegmentoService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public cdRef: ChangeDetectorRef,
        public kc: KeycloakService,
        private i18nService:I18nService
    ) {
        this.cantPorPagina = 10;
        this.itemsBusqueda = [];//para el split button
        this.itemsDetalle = [];
        this.terminosBusqueda = [];
    }
    //inicializaciones
    ngOnInit() {
        this.itemsBusqueda = [
            { label: 'Avanzado', icon: 'ui-icon-build', command: () => this.toggleBusquedaAvanzada() }
        ];
        this.buscarSegmentos();
        this.authGuard.canWrite(PERM_ESCR_SEGMENTOS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }

    /**Usado para cargar de manera perezosa los elementos de la lista de promociones */
    loadData(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantPorPagina = event.rows;
        this.buscarSegmentos();
    }

    /**
     * Metodos que realizan toggle de variables boolean, son invocados por de los controles de GUI
     *  */

    toggleBusquedaAvanzada() {
        this.busquedaAvanzada = !this.busquedaAvanzada;
    }
    /**
     * Metodo encargado de mostrar los elementos archivados
     * Es ejecutado al pulsar al check de elementos archivados
     */
    buscarSegmentos() {
        this.kc.getToken().then((tkn: string) => {
            this.segmentoService.token = tkn
            let sub = this.segmentoService.buscarSegmentos(this.terminosBusqueda, this.preferenciasBusqueda, this.cantPorPagina, this.rowOffset, this.tipos, this.filtros).subscribe(
                (response: Response) => {
                    this.cantRegistros = +response.headers.get("Content-Range").split("/")[1];
                    this.segmentos = <Segmento[]>response.json();
                },
                (error) => {
                    this.manejaError(error);
                },
                () => sub.unsubscribe());
        });
    }

    /*
    * Metodo que reririge al detalle del segmento que recibe por parámetros
    * Recibe: el segmento al cual redirigir al detalle
    */
    gotoDetail(segmento: Segmento) {
        let link = [Rutas.RT_SEGMENTOS_DETALLE_SEGMENTO, segmento.idSegmento];
        this.router.navigate(link);
    }
    /*
    *Redirige a la lista general de segmentos
    */
    gotoListaSegmentos() {
        let link = [Rutas.RT_SEGMENTOS];
        this.router.navigate(link);
    }
    /*
    * Redirige a el formuario de insertar segmentos
    */
    gotoInsertarSegmentos() {
        let link = [Rutas.RT_SEGMENTOS_INSERTAR];
        this.router.navigate(link);
    }
    /*Metodo que se encarga de manejar los errores 
   * Recibe: una instancia de error, puede ser un ProgressEvent, o un Response
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /* Metodo que muestra un mensaje al usuario para informarlo sobre algun acontecimiento
     * Recibe: modalHeader: el mensaje que mostraráen el header del modal 
     *         modalMessage: el mensaje que mostrara en el cuerpo del modal
     */
    mostrarModalMensaje(modalHeader: string, modalMessage: string) {
        this.displayModalMensaje = true;
    }

    /* Metodo que elimina unsegmento a traves de la capa de servicios 
    * Recibe: segmento: el segmento a eliminar
    * Retorna: void
    */
    eliminarSegmento(segmento: Segmento) {
        this.kc.getToken().then((tkn: string) => {
            this.segmentoService.token = tkn
            let sub = this.segmentoService.deleteSegmento(segmento).subscribe(
                () => {
                    this.buscarSegmentos();
                    this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"))
                },
                (error) => { this.manejaError(error); },
                () => { sub.unsubscribe(); }
            );
        });
    }
    /* Metodo que muestra un modal especial para confirmar la eliminacion de un segmento
     * Retorna:void
     */
    mostrarModalEliminarSegmento(segmento: Segmento) {
        this.segmentoEliminar = segmento;
        this.displayModalConfirm = true;
    }
    /* 
    * Redirige al usuario a la pagina anterior
    */
    goBack() {
        window.history.back();
    }
}