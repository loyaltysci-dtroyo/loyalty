import { I18nService } from '../../service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { KeycloakService } from '../../service/index';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SegmentoService, ListaReglasService } from '../../service/index';
import { Segmento, ListaReglas } from '../../model/index';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import * as Rutas from '../../utils/rutas';
import { PERM_ESCR_SEGMENTOS } from '../common/auth.constants'
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'segmento-insertar-component',
    templateUrl: 'segmentos-insertar.component.html'
})
/*
* Flecha Roja Technologies
* Autor:Fernando Aguilar
* Componente encargado de representar el componente para insertar segmentos
*/
export class InsertarSegmentoComponent {
    public modal: FormGroup;
    public form: FormGroup;
    public error: boolean;
    public nombreValido: boolean;
    public validandoNombre: boolean = false;
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    constructor(
        private i18nService: I18nService,
        public segmento: Segmento,//instancia de segmento usada para almacenar los datos del segmento actual
        public segmentoService: SegmentoService,//encargado del manejo de datos de segmentos
        public router: Router,
        public kc: KeycloakService,
        public msgService: MessagesService,
        public authGuard: AuthGuard
    ) {
        this.form = new FormGroup({
            "nombre": new FormControl('', [Validators.required]),
            "descripcion": new FormControl(''),
            "nombreDespliegue": new FormControl({ value: "", disabled: true }, [Validators.required]),
            "tag": new FormControl(''),
            "radioTipo": new FormControl('', Validators.required),
            "frecActualizacion": new FormControl('1', Validators.required)
        });
        this.modal = new FormGroup({
            "nombreLista": new FormControl('', Validators.required),
            "operadorLista": new FormControl('', Validators.required),
        });

        this.segmento = new Segmento();
        this.segmento.indEstado = "BO";
        this.segmento.indEstatico = "D"
        this.segmento.frecuenciaActualizacion = 1;
        this.error = false;

        this.authGuard.canWrite(PERM_ESCR_SEGMENTOS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }



    /**
     * Metodo encargado de insertar un segmento y redirigir al usuario a la pagina de detalle de segmento
     * Recibe: Utiliza la instancia de segmento (con ese mismo nombre) que contiene los datos ingresados en el form
     */
    insertarSegmento() {
        if (this.nombreValido) {
            this.msgService.showMessage({ detail: "El nombre interno ya existe. Digite un nuevo nombre interno.", severity: "warn", summary: "" });
        } else {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoService.token = tkn;
                let sub = this.segmentoService.addSegmento(this.segmento).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        let link = [`${Rutas.RT_SEGMENTOS_DETALLE_SEGMENTO}${data.text()}${Rutas.RT_SEGMENTOS_DETALLE_ATRIBUTOS}`];//url usado para redirigir
                        this.router.navigate(link);//redirige al detallde de segmento
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => sub.unsubscribe()//evita memory leak
                );
            });
        }
    }
    establecerNombres() {
        this.segmento.descripcion = this.segmento.nombre;
        let temp = this.segmento.nombre.trim();
        temp = temp.toLowerCase();
        temp = temp.replace(new RegExp(" ", 'g'), "_");
        temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
        this.segmento.nombreDespliegue = temp;
        if(this.segmento.nombreDespliegue != '' && this.segmento.nombreDespliegue != null){
            this.validarNombre();
        }
    }

    /**
     * Metodo auxiliar para ayudar a la validacion de el nombre publico del segmento, el cual debe ser unico
     * Recibe: Un string con el nombre atributo nombre que usa para validar
     * Retorna: Boolean: true=valido, false=invalido 
     */
    validarNombre() {
        if (this.segmento.nombreDespliegue == null || this.segmento.nombreDespliegue == "") {
            this.nombreValido = false;
            return;
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.validandoNombre = true;
                this.segmentoService.token = tkn;
                let sub = this.segmentoService.validarNombre(this.segmento.nombreDespliegue).subscribe(
                    (data: string) => {
                        if(data == "true"){
                            this.nombreValido = true;
                            this.msgService.showMessage({ detail: "El nombre interno ya existe. Digite un nuevo nombre interno.", severity: "warn", summary: "" });
                        }else{
                            this.nombreValido = false;

                        }
                        this.validandoNombre = false;
                    },
                    (error) => {
                        this.validandoNombre = false;
                    },
                    () => { sub.unsubscribe(); this.validandoNombre = false; }
                );
            });
    }

    /**
     * Metodo encargado de manejo de errores
     * 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    goBack() {
        window.history.back();
    }
}