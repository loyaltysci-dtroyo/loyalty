import {Component}  from '@angular/core';
import { ListaReglas, Segmento, ReglaMetricaBalance} from '../../model/index';
import { SegmentoService, ListaReglasService, ReglaMetricaCambioService, ReglaMiembroAtributoService} from '../../service/index';


@Component({
    moduleId: module.id,
    selector:'segmentos-component',
    templateUrl:'segmentos.component.html',
    providers:[Segmento, SegmentoService,ListaReglas, ReglaMetricaBalance]
})
/**
 * Componente que sirve como padre del arbol de componentes de segmento
 * 
 */
export class SegmentosComponent  {
    constructor(public segmento:Segmento){
        
    }
}