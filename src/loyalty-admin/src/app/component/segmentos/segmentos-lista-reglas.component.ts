import { I18nService } from '../../service';
import { KeycloakService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Component, OnInit } from '@angular/core';
import { ListaReglas, Segmento } from '../../model/index';
import { SegmentoService, ListaReglasService } from '../../service/index';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import * as Routes from '../../utils/rutas';
import { PERM_ESCR_SEGMENTOS } from '../common/auth.constants'
import { AuthGuard } from '../common/index';
/* 
   * Flecha Roja Technologies
   * Autor: Fernando Aguilar
   * Descripcion: Componente encargado de Agregar y Excluir grupos de reglas de un segmento
   */
@Component({
    moduleId: module.id,
    selector: 'segmentos-lista-reglas-component',
    templateUrl: 'segmentos-lista-reglas.component.html',
    providers: [ListaReglasService]
})
export class SegmentosListaReglasComponent implements OnInit {
    public modalGrp: FormGroup;
    public empty: boolean; //indicador para mostrar mensajes de error en la GUI
    public mostraModalInsertar: boolean;
    public operadores: SelectItem[];
    public tienePermisosEscritura:boolean;
    public loading:boolean=false;
    public mostraModalConfirm:boolean=false;
    constructor(
        public segmento: Segmento,
        public kc: KeycloakService,
        public listaReglasService: ListaReglasService,//encargado de manejo de datos de lista de reglas
        public listaReglas: ListaReglas,//variable temporal
        public segmentoService: SegmentoService,//encargado del manejo de datos de segmentos
        public msgService: MessagesService,
        private i18nService:I18nService,
        public authGuard: AuthGuard,
        public router: Router
    ) {
        /*Form group con los datos de validacion y valores por default del modal de insercion de reglas*/
        this.modalGrp = new FormGroup({
            "nombreLista": new FormControl(" ", Validators.required),
            "operadorLista": new FormControl(" ", Validators.required),
        });

        this.operadores = [];
        this.operadores=[...this.operadores,{ label: "AND", value: "A" }];
        this.operadores=[...this.operadores,{ label: "OR", value: "O" }];
        this.operadores=[...this.operadores,{ label: "NOT IN", value: "N" }];
        this.authGuard.canWrite(PERM_ESCR_SEGMENTOS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        //this.itemsDetalle=[...this.itemsDetalle,{label: 'Eliminar', icon: 'ui-icon-delete', command: ()=>this.eliminarRegla()}]
    }
    mostrarModalInsertar() {
        this.mostraModalInsertar = true;
        this.listaReglas = new ListaReglas();
    }

    /*  Metodo encargado de agregar una lista de reglas, obtene la informacion del modal de la GUI
      * Recibe: no recibe nada, los datos de la lista de reglas son un atributo de esta clase 
      * y se llenan con los datos provenientes del modal
      * Retorna: void, el tratamiento de errores se da en esta clase
      */
    agregarListaReglas() {
        if (this.segmento.listaReglasList.length > 0) {
            this.listaReglas.orden = this.segmento.listaReglasList.length + 1;
        } else {
            this.listaReglas.orden = 1;
            this.listaReglas.indOperador = "O";
        }
        this.kc.getToken().then(
            (tkn:string) => {
                this.listaReglasService.token = tkn;
                var sub = this.listaReglasService.addListaReglas(this.segmento.idSegmento, this.listaReglas).subscribe(
                    (data) => {
                        //si tuvo exito al agregar la lista de reglas entonces la actualiza
                        let sub = this.listaReglasService.getListasReglas(this.segmento.idSegmento).subscribe(
                            (data: ListaReglas[]) => {
                                this.segmento.listaReglasList = data;//actualiza los datos
                                this.empty = false;//muestra la lista de reglas
                                this.mostraModalInsertar = false;//cierra el modal
                                this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                            },
                            (error) => {
                                this.manejaError(error);//informa al usuario
                            },
                            () => {sub.unsubscribe();}
                        );
                    },
                    (error) => {
                        this.manejaError(error);//informa al usuario
                    },
                    () => sub.unsubscribe()
                );
            });
    }
    //Actualiza la lista de reglas agregadas a el segmento
    actualizarListareglas() {
        this.loading=true
        this.kc.getToken().then(
            (tkn:string) => {
                this.listaReglasService.token = tkn;

                let sub = this.listaReglasService.getListasReglas(this.segmento.idSegmento).subscribe(
                    (data: ListaReglas[]) => {
                        this.loading=false;
                        this.segmento.listaReglasList = data;
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => sub.unsubscribe()
                );
            });
    }
    /*Metodo que se encarga de manejar los errores 
   * Recibe: una instancia de error, puede ser un ProgressEvent, o un Response
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /**Elimina un grupo de reglas de la lista de reglas del segmento */
    actualizarSegmento() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.segmentoService.token = tkn;

                var sub = this.segmentoService.updateSegmento(this.segmento).subscribe(
                    (data: Segmento) => {
                         this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => sub.unsubscribe()
                );
            });
    }
    /**Sube de nivel una regla si el usuario tiene permisos */
    subirNivel(reglaList: ListaReglas) {
        if (reglaList.orden <= 1) { return }
        let idx = this.segmento.listaReglasList.indexOf(reglaList);
        let listaTemp = this.segmento.listaReglasList[idx - 1];//elemento a bajar de nivel
        this.kc.getToken().then(
            (tkn:string) => {
                this.listaReglasService.token = tkn;

                let sub = this.listaReglasService.intercambiarListaReglas(this.segmento.idSegmento, [listaTemp, reglaList]).subscribe(
                    () => { this.actualizarListareglas(); },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**Baja de ivel una regla si el usuario tiene permisos */
    bajarNivel(reglaList) {
        if (reglaList.orden == this.segmento.listaReglasList.length) { return }
        let idx = this.segmento.listaReglasList.indexOf(reglaList);
        let listaTemp = this.segmento.listaReglasList[idx + 1];//elemento a subir 
        this.kc.getToken().then(
            (tkn:string) => {
                this.listaReglasService.token = tkn;

                let sub = this.listaReglasService.intercambiarListaReglas(this.segmento.idSegmento, [listaTemp, reglaList]).subscribe(
                    () => { this.actualizarListareglas(); },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe() }
                );
            });
        this.actualizarListareglas();
    }
    /**Navega hacia el detalle de reglas para el grupo selecconado*/
    gotoDetail(reglaList: ListaReglas) {
        this.listaReglas = reglaList;
        let link = [Routes.RT_SEGMENTOS_DETALLE_SEGMENTO + this.segmento.idSegmento + Routes.RT_SEGMENTOS_DETALLE_DETALLE_REGLAS + reglaList.idLista];
        this.router.navigate(link);
    }
    /**Obtiene los grupos de reglas del segmento */
    ngOnInit() {
        this.loading=true
        this.kc.getToken().then(
            (tkn:string) => {
                this.listaReglasService.token = tkn;

                let sub = this.listaReglasService.getListasReglas(this.segmento.idSegmento).subscribe(
                    (data: ListaReglas[]) => {
                        this.loading=false;
                        this.segmento.listaReglasList = data;
                        if (this.segmento.listaReglasList.length == 0)
                        { this.empty = true; }
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => sub.unsubscribe()
                );
            });
    }
    /**
     * Muestra una confirmacion al usuario antes de eliminar una regla 
     */
    mostrarModalEliminarRegla(regla: ListaReglas) {
        this.listaReglas = regla;
    }

    /**
     * Elimina un grupo de reglas de la lista de reglas del segmento 
     */
    eliminarRegla(regla: ListaReglas) {
        var idx = this.segmento.listaReglasList.findIndex((value: ListaReglas) => {
            return value.idLista == regla.idLista;
        });
        this.loading=true
        this.kc.getToken().then(
            (tkn:string) => {
                this.listaReglasService.token = tkn;

                let sub = this.listaReglasService.removeListaReglas(this.segmento.idSegmento, this.segmento.listaReglasList[idx].idLista).subscribe(
                    () => {
                        this.loading=false;
                        this.actualizarListareglas();
                         this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); }

                );
            });
    }
}