import { I18nService } from '../../service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { SegmentoListaMiembroService } from '../../service/segmento-lista-miembro.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Segmento, Miembro } from '../../model/index';
import { SegmentoService, MiembroService, KeycloakService } from '../../service/index';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { PERM_ESCR_SEGMENTOS } from '../common/auth.constants'
import { AuthGuard } from '../common/index';
@Component({
    moduleId: module.id,
    selector: 'segmentos-detalle-miembros',
    templateUrl: 'segmentos-detalle-miembros.component.html',
    providers: [Miembro, MiembroService, SegmentoListaMiembroService]
})

/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Componente encargado de mostrar, insertar y eliminar los miembros asociados a un segmento
 */
export class SegmentosDetalleMiembros implements OnInit {

    public busquedaMiembros: string[];//campo de busqueda para mibmros incluidos/excluidos
    public busquedaDisponibles: string;//campo de busqueda de miembros disponibles a agregar
    public elementosSelectIncluir: SelectItem[] = [];
    public displayInsertarMiembros = false;//muestra y esconde el modal de insertar miembros
    public listaNegraMiembros: Miembro[] = [];//lista de miembros a incluir
    public listaBlancaMiembros: Miembro[] = []; //lista d emiembros a excluir
    public listaMiembrosDisponibles: Miembro[] = [];//lista de miembros disponibles para insertar;
    public listaVacia: boolean; //Permite verificar si la lista está vacia
    public indExcluir: string;//indicador para incluir o excluir los valores de la lista de miembros de segmento
    public displayModalEliminar: boolean;
    public seleccMultipleAgregar: Miembro[];//lista de miembros seleccionados para agregar a lista blanca o negra
    public seleccMultipleRemover: Miembro[];//lista de miembros seleccionados para ser eliminados de lista blanca o negra
    public displayModalEliminarLista: boolean;//true= muestra la lista de eliminar
    public tienePermisosEscritura: boolean;
    public displayModalAgregarLista: boolean = false;
    constructor(
        public segmento: Segmento,
        private router: Router,
        private kc: KeycloakService,
        private segmentoListaMiembroService: SegmentoListaMiembroService,
        private authGuard: AuthGuard,
        private msgService: MessagesService,
        private i18nService: I18nService
    ) {
        this.busquedaMiembros=[];
        this.busquedaDisponibles = "";
        this.elementosSelectIncluir = this.i18nService.getLabels("general-itemsIndExclusion");
        this.authGuard.canWrite(PERM_ESCR_SEGMENTOS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }

    // TODO queda pendiente traer los miembros segun el indicador de estado, 
    // guardar los miembros por segmento en la bd y darle estilo a todo esto

    ngOnInit() {
        this.indExcluir = "I";
        this.actualizarListas();
    }
    /**
     * Metodo encargado de cargar las listas de miembros incluidos o excluidos, es invocado cada vez que se desea actualizar las listas
     */
    actualizarListas() {
        // console.log("actualizarlistas", this.indExcluir)
        if (this.indExcluir == "I") {
            //traer lista de miembros a incluir
            this.actualizarListaBlanca();
        } else {
            //traer lista de miembro a excluir
            this.actualizarListaNegra();
        }
    }

    buscarMiembros(){
         this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoListaMiembroService.token = tkn;
                let sub = this.segmentoListaMiembroService.searchMiembrosDisponibles(this.busquedaMiembros, this.segmento.idSegmento).subscribe(
                    (data: Miembro[]) => {
                        this.listaMiembrosDisponibles = data;
                        if(this.listaMiembrosDisponibles.length > 0){
                            this.listaVacia = false;
                        }else{
                            this.listaVacia = true;
                        }
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /**
     * Despliega el listado de miembros para elegir entre si icluir/excluir una lista de ellos o realizarlo individualmente 
     */
    mostrarModalInsertar() {
        try {
            this.getListaMiembrosDisponibles();
            this.msgService.showMessage(this.i18nService.getLabels("segmentos-miembros-select"));
            this.displayInsertarMiembros = true;
        } catch (e) {
            console.log(e);
        }

    }
    /**
     * Encargado de listar los miembros disponibles para la agregar a el segmento 
     */
    getListaMiembrosDisponibles() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoListaMiembroService.token = tkn;
                let sub = this.segmentoListaMiembroService.getMiembrosDisponibles(this.segmento.idSegmento).subscribe(
                    (data: Miembro[]) => {
                        this.listaMiembrosDisponibles = data;
                        if(this.listaMiembrosDisponibles.length > 0){
                            this.listaVacia = false;
                        }else{
                            this.listaVacia = true;
                        }
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }


    /**Asocia un miembro al segmento en cuestión
     * Recibe: una instancia de miembro con los datos del miembro a agregar, del cual se necesita solamente el id
     */
    agregarMiembro(miembro: Miembro) {
        if (this.indExcluir == "I") {
            this.agregarMiembroListaBlanca(miembro);

        } else if (this.indExcluir == "E") {
            this.agregarMiembroListaNegra(miembro);
        }
        //this.actualizarListas();
        this.getListaMiembrosDisponibles();
    }
    /**
     * Agrega una lista de miembros a miembros a incluir/excluir del segmento dependiendo de el indicador de exclusion 
     */
    agregarListaMiembros() {
        if (this.indExcluir == "I") {
            this.agregarListaMiembrosListaBlanca();

        } else if (this.indExcluir == "E") {
            this.agregarListaMiembrosListaNegra();
        }
        this.actualizarListas();
        // this.getListaMiembrosDisponibles();
    }

    /**Metodos que agregan una determinada lista de miembros a la lista de elegibles de segmento, 
     * ya sea lista negra o lista blanca 
     * Utilizan las lista de selecicon multiple que fueron llenadas previamente por llos data table */
    agregarListaMiembrosListaBlanca() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoListaMiembroService.token = tkn;
                let sub = this.segmentoListaMiembroService.agregarListaMiembrosListaBlanca(this.seleccMultipleAgregar, this.segmento.idSegmento).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-multiple"));
                        this.displayInsertarMiembros = false;
                        this.actualizarListas();
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); }

                );
            });

    }
    agregarListaMiembrosListaNegra() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoListaMiembroService.token = tkn;
                let sub = this.segmentoListaMiembroService.agregarListaMiembrosListaNegra(this.seleccMultipleAgregar, this.segmento.idSegmento).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-exclusion-multiple"));
                        this.displayInsertarMiembros = false;
                        this.actualizarListas();
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); }

                );
            });
    }


    /*  Metodos encargados de agregar un miembro a la lista negra o lista blanca de MiembrosComponent respectivamente
    *   Recibe: el miembro a agregar a la lista blanca
    */
    agregarMiembroListaBlanca(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoListaMiembroService.token = tkn;
                let sub = this.segmentoListaMiembroService.agregarMiembroListaBlanca(this.segmento.idSegmento, miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                        this.actualizarListas();
                    }, (error) => {
                        this.manejaError(error);
                        this.actualizarListas();
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    agregarMiembroListaNegra(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoListaMiembroService.token = tkn;
                let sub = this.segmentoListaMiembroService.agregarMiembroListaNegra(this.segmento.idSegmento, miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-exclusion-simple"));
                        this.actualizarListas();
                    }, (error) => {
                        this.manejaError(error);
                        this.actualizarListas();
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /*  Metodo encargado de eliminar un miembro de las listas negra o lista blanca de MiembrosComponent 
    *   Recibe: el miembro a agregar a la lista blanca
    */
    eliminarMiembro(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoListaMiembroService.token = tkn;
                let sub = this.segmentoListaMiembroService.eliminarMiembroListas(
                    this.segmento.idSegmento, miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.displayModalEliminar = false;
                        this.actualizarListas();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                    );
            });

    }

    /*  Metodo encargado de eliminar una serie de miembros de las listas negra o lista blanca de MiembrosComponent 
       *   Recibe: el miembro a agregar a la lista blanca
       */
    eliminarListaMiembros() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoListaMiembroService.token = tkn;
                let sub = this.segmentoListaMiembroService.eliminarListaMiembroListas(this.seleccMultipleRemover, this.segmento.idSegmento).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-multiple"));
                        this.actualizarListas();
                        this.displayModalEliminar = false;
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); }

                );
            });


    }

    /*  Metodos encargados de traer los datos (recuperar informacion) de las listas blanca y negra desde el API
     *  actualiza el 
     */
    actualizarListaBlanca() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoListaMiembroService.token = tkn;
                let sub = this.segmentoListaMiembroService.getMiembrosListaBlanca(this.segmento.idSegmento).subscribe(
                    (data: Miembro[]) => {
                        this.listaBlancaMiembros = data;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    actualizarListaNegra() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.segmentoListaMiembroService.token = tkn;
                let sub = this.segmentoListaMiembroService.getMiembrosListaNegra(this.segmento.idSegmento).subscribe(
                    (data: Miembro[]) => {
                        this.listaNegraMiembros = data;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    //limpia las listas de miembros
    clearListaMiembros() {
        this.listaBlancaMiembros = [];
        this.listaNegraMiembros = []
    }
    //Encargado de informar al usuario si acontecio un error
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
