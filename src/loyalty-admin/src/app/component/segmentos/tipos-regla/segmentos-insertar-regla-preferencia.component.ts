import { RT_MIEMBROS_PREFERENCIAS_DETALLE } from '../../../utils/rutas';
import { ReglaPreferenciaService,PreferenciaService } from '../../../service/';
import { ErrorHandlerService } from '../../../utils/error-handler.service';
import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { ListaReglas, Preferencia, ReglaPreferencia, Segmento } from '../../../model/';
import { SegmentoService, I18nService, KeycloakService } from '../../../service/';
import { MessagesService } from '../../../utils/';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
@Component({
    moduleId: module.id,
    selector: 'segmentos-insertar-regla-preferencia',
    templateUrl: 'segmentos-insertar-regla-preferencia.component.html',
    providers:[PreferenciaService, ReglaPreferencia,Preferencia]
})

export class SegmentosInsertarReglaPreferenciaComponent implements OnInit {
    public listaPreferencias: Preferencia[];
    public terminosBusqueda: string[];
    public rowOffset: number;
    public cantRegistros: number;
    public cantTotalRegistros: number;
    public listaOperadores: SelectItem[];
    public reglaInsertar: ReglaPreferencia;
    public displaySelectorPreferencias: boolean = false;
    public preferenciaSeleccionada:Preferencia;
    public itemsPreguntaSeleccion: SelectItem[];
    public itemsRespuestaSeleccion: SelectItem[];
    public respuestasSeleccionadas: string[] = [];
    @Output() success = new EventEmitter<boolean>();

    constructor(
        private msgService: MessagesService,
        private segmento: Segmento,
        private listaReglas: ListaReglas,
        private relglaSevice: ReglaPreferenciaService,
        private i18nService: I18nService,
        private preferenciaService: PreferenciaService,
        private segmentoService: SegmentoService,
        private kc: KeycloakService) {

        this.itemsPreguntaSeleccion = [];
        this.itemsRespuestaSeleccion = [];
        this.preferenciaSeleccionada = new Preferencia();
        this.terminosBusqueda = [];
        this.listaPreferencias = [];
        this.cantTotalRegistros = 10;
        this.reglaInsertar = new ReglaPreferencia();
    }

    ngOnInit() {
        // this.obtenerPreferenciaes();

        this.listaOperadores = [this.i18nService.getLabels("general-default-select")];

    }

    /**
     * Inicializa los operadores cada vez que el usuario selecciona una nueva preferencia
     */
    resetOperadores() {
        if (this.preferenciaSeleccionada) {
            switch (this.preferenciaSeleccionada.indTipoRespuesta) {
                case Preferencia.TIPO_PREF_TEXTO: {
                    this.listaOperadores = this.i18nService.getLabels("segmentos-operadoresTexto");
                    break;
                }
                case Preferencia.TIPO_PREF_RESP_MULTIPLE: {
                    this.listaOperadores = this.i18nService.getLabels("segmento-encuesta-operadores-selecc-multiple");
                    break;
                }
                case Preferencia.TIPO_PREF_RESP_UNICA: {
                    this.listaOperadores = this.i18nService.getLabels("segmento-encuesta-operadores-selecc-unica");
                    break;
                }
                default: {
                    break;
                }
            }
        } 
        this.reglaInsertar.indOperador = this.listaOperadores[0].value;
    }
    /**
     * Maneja el lazy loading
     * @param evento 
     */
    onLazyLoad($event) {
        this.rowOffset = $event.first;
        this.cantTotalRegistros = $event.rows;
    }
    /**
     * Encargado de actualizar la lista de respuestas cada vez que el usuario seleciona una pregutna de la encuesta para la mision
     */
    poblarRespuestasSeleccion() {
        if (this.preferenciaSeleccionada && this.preferenciaSeleccionada.indTipoRespuesta == Preferencia.TIPO_PREF_RESP_MULTIPLE
                || this.preferenciaSeleccionada.indTipoRespuesta == Preferencia.TIPO_PREF_RESP_UNICA) {
            this.itemsRespuestaSeleccion = this.preferenciaSeleccionada.respuestas
                .split("\n")
                .map((element: string) => { return { "value": element, "label": element } });
        }
        this.resetOperadores();
    }
    /**
     * Registra la seleccion de la mision y actualiza la lista de preferencias
     */
    seleccionarPreferencia(preferencia: Preferencia) {
        this.preferenciaSeleccionada = preferencia;
        this.poblarRespuestasSeleccion();
        
        this.displaySelectorPreferencias = false;
    }
    /**
     * Obtiene la lista de preferencias para seleccionar una
     */
    obtenerPreferencias() {
        this.kc.getToken().then((tkn) => {
            this.preferenciaService.token = tkn;
            let sub = this.preferenciaService.busqueda(this.terminosBusqueda.join(" "), ["TX","SU","RM"]).subscribe(
                (data: any) => {
                    this.listaPreferencias =data;
                    this.displaySelectorPreferencias = true;
                    // this.cantTotalRegistros = + data.headers.get("Content-Range").split("/")[1];
                }, (err: Response) => {
                    this.handleError(err);
                }, () => {
                    sub.unsubscribe();
                }
            );
        });
    }
   
    /**
     * Inserta una regla de encuesta
     */
    insertarRegla() {
        //formateo del valor de comparacion
        this.reglaInsertar.preferencia = this.preferenciaSeleccionada;
        if (this.preferenciaSeleccionada.indTipoRespuesta == Preferencia.TIPO_PREF_RESP_MULTIPLE
                || this.preferenciaSeleccionada.indTipoRespuesta == Preferencia.TIPO_PREF_RESP_UNICA) {
            this.reglaInsertar.valorComparacion = this.respuestasSeleccionadas
                .map((element) => { return this.preferenciaSeleccionada.respuestas.split("\n").findIndex((resp) => { return resp == element }) + 1 }).join("\n");
        }
        this.kc.getToken().then((tkn) => {
            this.relglaSevice.token = tkn;
            let sub = this.relglaSevice.agregarReglaPreferencia(this.segmento.idSegmento, this.listaReglas.idLista, this.reglaInsertar).subscribe(
                (data: any) => {
                    this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                    this.success.emit(true);
                }, (err: Response) => {
                    this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(err));
                }, () => {
                    sub.unsubscribe();
                }
            );
        });
    }

    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }


}