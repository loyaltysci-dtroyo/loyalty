import { ErrorHandlerService } from '../../../utils/error-handler.service';
import { ReglaEncuestaService } from '../../../service/regla-encuesta.service';
import { MisionService } from '../../../service/mision.service';
import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Mision, MisionEncuestaPregunta, Segmento, ListaReglas, ReglaEncuesta } from '../../../model/';
import { SegmentoService, I18nService, KeycloakService } from '../../../service/';
import { MessagesService } from '../../../utils/';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
@Component({
    moduleId: module.id,
    selector: 'segmentos-insertar-regla-encuesta',
    templateUrl: 'segmentos-insertar-regla-encuesta.component.html',
    providers: [ReglaEncuestaService]
})

export class SegmentoInsertarReglaEncuestaComponent implements OnInit {

    public listaMisiones: Mision[]
    public listaPreguntasMision: MisionEncuestaPregunta[];
    public terminosBusqueda: string[];
    public rowOffset: number;
    public cantRegistros: number;
    public cantTotalRegistros: number;
    public listaOperadores: SelectItem[];
    public reglaInsertar: ReglaEncuesta;
    public displaySelectorMisiones: boolean = false;
    public misionSeleccionada: Mision;

    public preguntaSeleccionada: MisionEncuestaPregunta;
    public itemsPreguntaSeleccion: SelectItem[];
    public itemsRespuestaSeleccion: SelectItem[];
    public respuestaCalificacion: number;
    public respuestasSeleccionadas: string[] = [];
    @Output() success = new EventEmitter<boolean>();

    constructor(
        private msgService: MessagesService,
        private segmento: Segmento,
        private listaReglas: ListaReglas,
        private relglaSevice: ReglaEncuestaService,
        private i18nService: I18nService,
        private misionService: MisionService,
        private segmentoService: SegmentoService,
        private kc: KeycloakService) {

        this.itemsPreguntaSeleccion = [];
        this.itemsRespuestaSeleccion = [];
        this.misionSeleccionada = new Mision();
        this.misionSeleccionada.nombre = "";
        this.terminosBusqueda = [];
        this.listaMisiones = [];
        this.listaPreguntasMision = [];
        this.cantTotalRegistros = 10;
        this.reglaInsertar = new ReglaEncuesta();
    }

    ngOnInit() {
        // this.obtenerMisiones();

        this.listaOperadores = [this.i18nService.getLabels("general-default-select")];

    }

    /**
     * Inicializa los operadores cada vez que el usuario selecciona una nueva pregunta
     */
    resetOperadores() {
        if (this.preguntaSeleccionada && (this.preguntaSeleccionada.indTipoPregunta == MisionEncuestaPregunta.TIP_PREG_ENCUESTA)) {
            switch (this.preguntaSeleccionada.indTipoRespuesta) {
                case MisionEncuestaPregunta.TIP_RESP_RESPUESTA_ABIERTA_TEXTO: {
                    this.listaOperadores = this.i18nService.getLabels("segmentos-operadoresTexto");
                    break;
                }
                case MisionEncuestaPregunta.TIP_RESP_RESPUESTA_ABIERTA_NUMERICO: {
                    this.listaOperadores = this.i18nService.getLabels("segmentos-operadoresNumericos");
                    break;
                }
                case MisionEncuestaPregunta.TIP_RESP_SELECCION_MULTIPLE: {
                    this.listaOperadores = this.i18nService.getLabels("segmento-encuesta-operadores-selecc-multiple");
                    break;
                }
                case MisionEncuestaPregunta.TIP_RESP_SELECCION_UNICA: {
                    this.listaOperadores = this.i18nService.getLabels("segmento-encuesta-operadores-selecc-unica");
                    break;
                }
                default: {
                    break;
                }
            }
        } else if (this.preguntaSeleccionada && (this.preguntaSeleccionada.indTipoPregunta == MisionEncuestaPregunta.TIP_PREG_CALIFICACION)) {
            this.listaOperadores = this.i18nService.getLabels("segmentos-operadoresNumericos");
        }
        this.reglaInsertar.indOperador = this.listaOperadores[0].value;
    }
    /**
     * Maneja el lazy loading
     * @param evento 
     */
    onLazyLoad($event) {
        this.rowOffset = $event.first;
        this.cantTotalRegistros = $event.rows;
    }
    /**
     * Encargado de actualizar la lista de respuestas cada vez que el usuario seleciona una pregutna de la encuesta para la mision
     */
    poblarRespuestasSeleccion() {
        if (this.preguntaSeleccionada && this.preguntaSeleccionada.indTipoPregunta == MisionEncuestaPregunta.TIP_PREG_ENCUESTA
            && (this.preguntaSeleccionada.indTipoRespuesta == MisionEncuestaPregunta.TIP_RESP_SELECCION_MULTIPLE
                || this.preguntaSeleccionada.indTipoRespuesta == MisionEncuestaPregunta.TIP_RESP_SELECCION_UNICA)) {
            this.itemsRespuestaSeleccion = this.misionSeleccionada.misionEncuestaPreguntaList
                .find((element: MisionEncuestaPregunta) => { return element.idPregunta == this.preguntaSeleccionada.idPregunta; })
                .respuestas
                .split("\n")
                .map((element: string) => { return { "value": element, "label": element } });
        } else if (this.preguntaSeleccionada.indTipoPregunta == MisionEncuestaPregunta.TIP_PREG_CALIFICACION) {

        }
        this.resetOperadores();
    }
    /**
     * Registra la seleccion de la mision y actualiza la lista de preguntas
     */
    seleccionarMision(mision: Mision) {
        this.misionSeleccionada = mision;
        this.getPreguntasMision();
        // this.poblarRespuestasSeleccion();
        this.displaySelectorMisiones = false;

    }
    /**
     * Obtiene la lista de misiones para seleccionar una para luego seleccionar una prgunta asociada
     */
    obtenerMisiones() {
        this.kc.getToken().then((tkn) => {
            this.misionService.token = tkn;
            let sub = this.misionService.buscarMisiones(this.terminosBusqueda, ["A"], this.rowOffset, this.cantRegistros, ["E"]).subscribe(
                (data: any) => {
                    this.listaMisiones = <Mision[]>data.json();
                    this.displaySelectorMisiones = true;
                    this.cantTotalRegistros = + data.headers.get("Content-Range").split("/")[1];
                }, (err: Response) => {
                    this.handleError(err);
                }, () => {
                    sub.unsubscribe();
                }
            );
        });
    }
    /**
     * Obtiene la lista de preguntas asociadas a una mision de encuesta
     */
    getPreguntasMision() {
        this.kc.getToken().then((tkn) => {
            this.misionService.token = tkn;
            let sub = this.misionService.obtenerPreguntasEncuesta(this.misionSeleccionada.idMision).subscribe(
                (data: Response) => {
                    this.listaPreguntasMision = <MisionEncuestaPregunta[]>data.json();
                    this.misionSeleccionada.misionEncuestaPreguntaList = <MisionEncuestaPregunta[]>data.json();
                    this.itemsPreguntaSeleccion = this.misionSeleccionada.misionEncuestaPreguntaList
                        .map((element: MisionEncuestaPregunta) => { return { "value": element, "label": element.pregunta }; });
                    this.preguntaSeleccionada = this.misionSeleccionada.misionEncuestaPreguntaList[0];
                    this.resetOperadores();
                    this.poblarRespuestasSeleccion();
                }, (err: Response) => {
                    this.handleError(err);
                }, () => {
                    sub.unsubscribe();
                }
            );
        });
    }
    /**
     * Inserta una regla de encuesta
     */
    insertarRegla() {
        //formateo del valor de comparacion
        this.reglaInsertar.pregunta = this.preguntaSeleccionada;
        if (this.preguntaSeleccionada.indTipoPregunta == MisionEncuestaPregunta.TIP_PREG_ENCUESTA
            && (this.preguntaSeleccionada.indTipoRespuesta == MisionEncuestaPregunta.TIP_RESP_SELECCION_MULTIPLE
                || this.preguntaSeleccionada.indTipoRespuesta == MisionEncuestaPregunta.TIP_RESP_SELECCION_UNICA)) {
            this.reglaInsertar.valorComparacion = this.respuestasSeleccionadas
                .map((element) => { return this.preguntaSeleccionada.respuestas.split("\n").findIndex((resp) => { return resp == element }) + 1 }).join("\n");
        }
        this.kc.getToken().then((tkn) => {
            this.relglaSevice.token = tkn;
            let sub = this.relglaSevice.agregarReglaEncuesta(this.segmento.idSegmento, this.listaReglas.idLista, this.reglaInsertar).subscribe(
                (data: any) => {
                    this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                    this.success.emit(true);
                }, (err: Response) => {
                    this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(err));
                }, () => {
                    sub.unsubscribe();
                }
            );
        });
    }

    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}