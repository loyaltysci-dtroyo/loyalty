import { I18nService } from '../../service';
import { Component, OnInit } from '@angular/core';
import { ListaReglas, Segmento } from '../../model/index';
import { SegmentoService, ListaReglasService } from '../../service/index';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';
import { AuthGuard } from '../common/index';
import { PERM_ESCR_SEGMENTOS } from '../common/auth.constants'
import { KeycloakService } from '../../service/index';
/**
* Flecha Roja Technologies set-2016
* Fernando Aguilar
* Componente encargado del manejo de segmentos y sus agrupaciones de reglas
*/
@Component({
    moduleId: module.id,
    selector: 'detalle-segmento-component',
    templateUrl: 'segmentos-detalle-atributos.component.html',
    providers: [ListaReglasService]
})
export class DetalleAtributosSegmentoComponent implements OnInit {
    public form: FormGroup; //formgroup usado para validaciones y valores por defecto de los componentes del form   
    public error: boolean;//indicador usado para marcar si hubo error
    public displayActualizar: boolean;
    public tienePermisosEscritura:boolean;
    public nombreValido:boolean;
    constructor(
        public segmento: Segmento,//instancia de segmento usada para almacenar los datos del segmento actual
        public segmentoService: SegmentoService,//encargado del manejo de datos de segmentos
        public msgService: MessagesService, 
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        private i18nService:I18nService,
    ) {
        this.displayActualizar = false;
        this.authGuard.canWrite(PERM_ESCR_SEGMENTOS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }
    /*
    * Metodo init: encargado de incializar el form group para efectos de validacion y valores por default
    * ademas de obtener los segmentos de la API y las listas de reglas asociadas a este segmento
    */
    ngOnInit() {
        /*Form group con los datos de validacion y valores por default del form principal*/
        this.form = new FormGroup({
            "nombre": new FormControl('', Validators.required),
            "descripcion": new FormControl('', Validators.required),
            "nombreDespliegue": new FormControl({ value: '', disabled: true }, Validators.required),
            "tag": new FormControl(''),
            "frecActualizacion": new FormControl(1)
        });
        this.obtenerSegmento();

    }

    /**
  * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
  * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
  * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
  * no siempre sea necesario llamar al api para actualizar los datos
  */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Obtiene un segmento del API, setea los valores del segmento obtenido para evitar perder el puntero al segmento que fue iyectado al arbol de dependencias
     * por angular y permitir que los hijos del arbol de componentes compartan dicho puntero para sincronizar datos del detalle de segmento
     */
    obtenerSegmento() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.segmentoService.token = tkn;
                let sub = this.segmentoService.getSegmento(this.segmento.idSegmento).subscribe(
                    (data: Segmento) => {
                        this.copyValuesOf(this.segmento, data)
                    },
                    (error) => {

                        this.manejaError(error)
                    },
                    () => sub.unsubscribe()
                );
            });
    }

    /* Metodo encargado de actualizar los datos del segmento a traves del API
    * Recibe: no recibe nada , los datos del segmento son un atributo de esta clase 
    * y se llenan con los datos provenientes de la GUI
    * Retorna: void, el tratamiento de errores se da en esta clase
    */
    actualizarSegmento() {
        if (this.form.touched) {
            this.kc.getToken().then(
                (tkn:string) => {
                    this.segmentoService.token = tkn;
                    let sub = this.segmentoService.updateSegmento(this.segmento).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                            this.obtenerSegmento();
                        },
                        (error) => {
                            this.manejaError(error);
                            this.obtenerSegmento();
                        },
                        () => sub.unsubscribe()
                    );
                });
        }
    }

    /**
     * Muestra modal de confirm para actualizar un segmento
     */
    mostrarModalActualizarSegmento() {
        this.displayActualizar = true;
    }
    /**
     * Maneja el error a nivel de componente
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /**
     * Invocado por el confirmar accion del dialog de actualizacion de segmento
     */
    confirmAction() {
        this.actualizarSegmento();
    }
    goBack() {
        window.history.back();
    }

    validarNombre(){
        //TODO
    }

}