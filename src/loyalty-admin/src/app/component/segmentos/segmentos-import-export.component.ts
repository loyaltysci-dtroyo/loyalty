import { Component } from '@angular/core';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ImportacionService, ExportacionService, KeycloakService, I18nService } from '../../service/index';
import * as Routes from '../../utils/rutas';
import { Segmento } from '../../model/index';
import { PERM_ESCR_MIEMBROS, PERM_ESCR_SEGMENTOS, PERM_LECT_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { SelectItem } from 'primeng/primeng';

@Component({
    moduleId: module.id,
    selector: 'segmentos-import-export',
    templateUrl: 'segmentos-import-export.component.html',
    providers: [ImportacionService, ExportacionService],
})

export class SegmentoImportExportComponent {

    public ayuda: boolean; //Habilita el dialogo de ayuda
    public importar: boolean; //Manipula el dom para mostrar la importación
    public exportar: boolean; //Manipula el dom para mostrar la exportacion
    public escrituraMiembro: boolean; //Permiso de escritura sobre miembro (true/false)
    public escrituraSegmento: boolean; //Permiso de escritura sobre segmento (true/false)
    public lecturaMiembro: boolean; //Permiso de lectura sobre miembro (true/false)

    public idSegmento: string; //Guarda el id enviado por parametros
    public listaBase64: string; //Contiene el base64 del archivo subido
    public nombreArchivo: string = ""; //Para mostrar el nombre del archivo seleccionado

    //Importacion de miembros
    public accion: string = "I"; // Guarda el tipo de acción
    public identificacion: string = "idMiembro"; //Guarda el tipo de identif. por el que se va a reconocer el miembro  
    public idItems: SelectItem[] = []; //Lista de los tipos de documento (uuid - doc. identif.)
    public accionItems: SelectItem[] = []; //Lista de la acción a realizar (incluir - excluir)

    constructor(
        public segmento: Segmento,
        public exportacionService: ExportacionService,
        public importacionService: ImportacionService,
        public route: ActivatedRoute,//ruta actual
        public router: Router,
        public authGuard: AuthGuard,
        public i18nService: I18nService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escrituraMiembro = permiso;
        });
        this.authGuard.canWrite(PERM_ESCR_SEGMENTOS).subscribe((permiso: boolean) => {
            this.escrituraSegmento = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_MIEMBROS).subscribe((permiso: boolean) => {
            this.lecturaMiembro = permiso;
        });
        //this.route.params.forEach((params: Params) => { this.idSegmento = params['id'] });

        this.importar = true;
        this.idItems = this.i18nService.getLabels("importacion-miembro-segmento");
        this.accionItems = this.i18nService.getLabels("general-itemsIndExclusion");
    }

    importarMiembros() {
        if (this.escrituraMiembro && this.escrituraSegmento) {
            if (this.listaBase64 != "" && this.listaBase64 != undefined) {
                this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                    (token: string) => {
                        this.importacionService.token = token;
                        this.importacionService.importarMiembrosSegmento(this.segmento.idSegmento, this.identificacion, this.accion, this.listaBase64).subscribe(
                            (data) => {
                                this.msgService.showMessage({ detail: "La importación de miembros esta siendo procesada.", severity: "info", summary: "" });
                                this.listaBase64 = "";
                                this.nombreArchivo = "";
                            },
                            (error) => this.manejarError(error)
                        );
                    }
                );
            } else {
                this.msgService.showMessage({ detail: "Debe seleccionar un archivo para continuar.", severity: "warn", summary: "" });
            }
        }
    }

    exportarMiembros() {
        if (this.lecturaMiembro) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.exportacionService.token = token;
                    this.exportacionService.exportarMiembrosSegmento(this.segmento.idSegmento).subscribe(
                        (data) => {
                            this.msgService.showMessage({ detail: "La exportación esta siendo procesada.", severity: "info", summary: "" });
                            this.listaBase64 = "";
                            this.nombreArchivo = "";
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
        }
    }

    //Método para subir un archivo csv con los codigos
    subirArchivo() {
        let archivoCsv: any;
        let nuevoCsv: any;
        let urlBase64: string[];
        archivoCsv = document.getElementById("archivo"); //Se obtiene el archivo subido por el usuario
        this.nombreArchivo = archivoCsv.files[0].name;
        let reader = new FileReader();
        reader.readAsDataURL(archivoCsv.files[0]);
        reader.addEventListener("load", (event) => {
            let archivo = reader.result;
            urlBase64 = archivo.split(",");
            this.listaBase64 = urlBase64[1];
        }, false);
    }


    /* Método para manejar los errores que se provocan en las transacciones*/
    manejarError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
