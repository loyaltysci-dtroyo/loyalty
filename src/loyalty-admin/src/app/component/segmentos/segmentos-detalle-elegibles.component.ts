import { I18nService } from '../../service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Segmento, Miembro } from '../../model/index';
import { SegmentoElegiblesService, SegmentoService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';
import { KeycloakService } from '../../service/index';
import { Response } from "@angular/http";
import { Message} from 'primeng/primeng';
const PROCESAMIENTO_ACTIVO:boolean=true;
const PROCESAMIENTO_INACTIVO:boolean=false;
const POLL_RATE:number=3000;
/**
* Flecha Roja Technologies set-2016
* Fernando Aguilar
* Componente encargado de mostrar los miembros que fueron autogenerados por back-end con base en reglas (segmento ACTIVO)
* ademas muestra estadisticas generales sobre los trabajos actualmente ejecutandose en el back end y permite disparar trabajos nuevos
*/
@Component({
    moduleId: module.id,
    selector: 'segmentos-detalle-elegibles',
    templateUrl: 'segmentos-detalle-elegibles.component.html',
    providers: [SegmentoElegiblesService,MessagesService, SegmentoService, Miembro]
})
export class SegmentosDetalleElegiblesComponent implements OnInit {
    public terminoBusqueda: string;
    public msgs:Message[];
    public listaMiembros: Miembro[];
    public showInfo: boolean = false;//true=se muestra las estadisticas del segmento
    public idTrabajo: string;//id del trabajo de spark, se almacena para tener la posibilidad de cancelarlo luego
    public statusTrabajo: string;// Estado del  trabajo en ejecucion
    public infoElegibles: any;//object con la info de elegibles (estadisticas)
    public trabajoCancelado: boolean;//status proveniente del API, true= el trabajo ha sido terminado, false= el torabajo no pudo ser terminado o ya fue cancelado anteriormente
    public segmentoProcesado: boolean;//indica que el procesamiento de reglas del segmento fue disparado alguna vez en el pasado
    public indRecalculo: boolean;//usado para cuando el segmento se encuentra en proceso de recalculo, se activa con el metodo de recalcular y se desactiva al completar o cancelar el recalculo
    public rowOffset: number;//offset del row para paginacion
    public cantRegistros: number;//cantidad de registros a pedir al api para la paginacion
    public cantRegistrosTotal: number;//cantidad total de registros de miembro elegibles, es usado para la paginacion
    public mostrarInfoMiembros: boolean = false;//flag, cuando es true se despliega la consulta de los miembros elegibles a partie del procesamiento de reglas
    public POLL_RATE: number=3000;//cantidad en ms que indica la frecuencia de refresco de la informacion de elegibles
    
    constructor(
        public segmentoElegiblesService: SegmentoElegiblesService,
        public miembro: Miembro,
        public segmentoService: SegmentoService,
        public segmento: Segmento,
        public router: Router,
        public kc: KeycloakService,
        public msgService: MessagesService,
        private i18nService:I18nService
    ) {
        this.statusTrabajo = "STOPPED";
        this.listaMiembros = [];
        this.segmentoProcesado = false;
        this.indRecalculo = PROCESAMIENTO_INACTIVO;//inicia con status inactivo
        this.cantRegistros = 10; 
        this.rowOffset = 0;
        this.mostrarInfoMiembros = false;
        setInterval(() => { this.statusRecalculateJob() }, 3000);
        this.msgs=[];
    }

    ngOnInit() {
        this.getSegmento();//se obtiene la info del segmento para determinar el estado del segmento y mostrar un mensaje en caso de que el segmento no este en estado activo
    }
    obtenerSegmento(){

    }

    /**
     * Usado para cargar 'lazy' los elementos de la lista de promociones 
     */
    loadData(event:any) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantRegistros = event.rows;
        this.getListaEligibles();
    }
    /*
    * Navega hacia el detalle del miembro, es invocado desde la lista de miembros en caso de que el usuario desee navegar hacia el detalle
    */
    gotoDetailMiembro(miembro:Miembro) {
        let link = ["/miembros/detalleMiembro", miembro.idMiembro]
        this.router.navigate(link);
    }
    /*
    * Obtiene los datos de un segmento, usado para mostrar los detalles del segmento, una vez obtenidos los datos del segmento se procede a obtener los datos de elgibles si el 
    * segmento esta en estado activo
    */
    getSegmento() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.segmentoService.token = tkn;
                let sub = this.segmentoService.getSegmento(this.segmento.idSegmento).subscribe(
                    (data: Segmento) => {
                        this.segmento = data;
                        if (this.segmento.indEstado == "AC") {
                            this.getInfoEligibles();//se obtiene la informacion de los elegibles en caso de que el segmento tenga el estado activo
                            this.getListaEligibles();//se obtiene la lista de miembros elegibles en caso de que el segmento este en estado activo
                        }
                    },
                    (error) => { this.manejaError(error); }, () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Metodo encargado de recuperar la informacion de miembros disponibles del sistema
     * Pobla la variable listaMiembros con los valores que provee el API
     */
    getListaEligibles() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.segmentoElegiblesService.token = tkn;
                let sub = this.segmentoElegiblesService.getListaEligibles(this.segmento.idSegmento, this.cantRegistros, this.rowOffset).subscribe(
                    (data:Response) => {
                        this.listaMiembros =data.json();
                        if(data.headers.has("Content-Range")){
                             this.cantRegistrosTotal= +data.headers.get("Content-Range").split("/")[1];
                        }
                       
                        this.segmentoProcesado = true;//si el segmento tiene lista de elegibles entonces quiere decir que ha sido procesado
                    }, (error) => {
                        this.manejaError(error);

                    },
                    () => {
                        sub.unsubscribe();
                    }
                )
            });
    }

    /**
     * Obtiene las estadisticas del recalculo del segmento si es que el recalculo del semgneot se ha ejecutado alguna vez en el pasado, en caso de que 
     * no haya informacion, esto quiere decir que el segmento nunca ha sido calculado por la tarea programada del sistema, 
     * obtiene la info como cantidad de usuarios en el segmento, reglas fallidas, etc. Almacena esos datos en la variable infoElegible para ser desplegados en la GUI
     */
    getInfoEligibles() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.segmentoElegiblesService.token = tkn;
                let sub = this.segmentoElegiblesService.getInfoEligibles(this.segmento.idSegmento).subscribe(
                    (data) => {
                        this.infoElegibles = data;//actualia la info del recalculo de reglas
                        this.segmentoProcesado = true;//si el segmento tiene info de elegibles entonces quiere decir que ha sido procesado
                    }, (error:any) => {
                        this.manejaError(error);
                    },
                    () => {
                        sub.unsubscribe();
                    }
                )
            });
    }

    /**
     * Envia una señal al API para disparar un proceso de ejecucion de calculo de elegibles de segmento, el API responde con un mensaje de codigo de trabajo, el cual
     * es obtenido y almacenado, la bandera de recalculo se activa para disparar el continuo chequeo de reglas del segmento, en caso de que el API retorne error (spark inaccesible, down,  etc)
     * informa al usuario del error y actualiza la bandera de recalculo para dejar de solicitar informacion al servidor sobre el estado de la peticion
     */
    recalculateEligibles() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.segmentoElegiblesService.token = tkn;
                let sub = this.segmentoElegiblesService.recalculateEligibles(this.segmento.idSegmento).subscribe(
                    (data: Response) => {
                        this.msgs=[...this.msgs,this.i18nService.getLabels("segmentos-info-procesando")];
                        this.indRecalculo = PROCESAMIENTO_ACTIVO;
                        this.statusTrabajo = "RUNNING";//se actualiza el valor de el status de trabajo a running
                        this.idTrabajo = data.text();//se obtiene el id de trabajo a partir del body del mensaje
                        this.msgService.showMessage(this.i18nService.getLabels("segmentos-enviado-procesamiento"));
                    }, (error:any) => {
                        this.manejaError(error);
                        this.indRecalculo = PROCESAMIENTO_INACTIVO;//en caso de error se actualiza el flag
                    },
                    () => {
                        sub.unsubscribe();
                    }
                )
            });
    }
    /**
     * Realiza un polling al servidor cada Consulta el estado del proceso de recalculado del segmento, es llamado constantemente una vez el usuario dispara el calculo de miembros
     * para actualizar la informacion en pantalla del estado del calculo
     */
    statusRecalculateJob() {
        if (this.indRecalculo) {
            this.kc.getToken().then(
                (tkn:string) => {
                    this.segmentoElegiblesService.token = tkn;
                    let sub = this.segmentoElegiblesService.statusRecalculateJob(this.segmento.idSegmento, this.idTrabajo).subscribe(
                        (data) => {

                            this.statusTrabajo = data.text();
                            if (this.statusTrabajo == "KILLED" || this.statusTrabajo == "FAILED" || this.statusTrabajo == "SUCCESS" || this.statusTrabajo == "FINISHED") {
                                this.indRecalculo = PROCESAMIENTO_INACTIVO;
                                this.msgs=[];
                                this.getInfoEligibles();//se obtiene la informacion de los elegibles en caso de que el segmento tenga el estado activo
                                this.getListaEligibles();//se obtiene la lista de miembros elegibles en caso de que el segmento este en estado activo
                            }
                        }, (error:any) => {
                            this.manejaError(error);
                        },
                        () => {
                            sub.unsubscribe();
                        }
                    )
                });
        }
    }
    /**
     * Cancela el recalculado de un segmento, en caso de cancelar el recalculo del segmento los cambios seran revertidos
     * actualiza la bandera de recalculo para evitar que el se siga enviando peticiones de solicitud de estado al servidor
     */
    cancelRecalculateJob(idTrabajo: string) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.segmentoElegiblesService.token = tkn;
                let sub = this.segmentoElegiblesService.cancelRecalculateJob(this.segmento.idSegmento, this.idTrabajo).subscribe(
                    (data) => {
                        this.indRecalculo = PROCESAMIENTO_INACTIVO;
                        this.trabajoCancelado = data;
                    }, (error:any) => {
                        this.manejaError(error);
                    },
                    () => {
                        sub.unsubscribe();
                    }
                )
            });
    }



    /**
     * Navega hacia el detalle de miembro en el caso que se seleccione el detalle de miembro en la lista
     */
    gotoDetail(miembro: Miembro) {
        let link = [Rutas.RT_MIEMBROS_DETALLE, miembro.idMiembro];
        this.router.navigate(link);
    }
    /**
     * Maneja el error, en este caso informa al usuario con un mensaje de notificacion
     */
    manejaError(error:any) {
        if (error.status == 404) {
            this.segmentoProcesado = false;// si el segmento retorna 404, quiere decir que nunca fue procesado (lista de elegibles no existente)
        }
        else {
            this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
        }
    }
}