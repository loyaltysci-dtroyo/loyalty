import { Component, OnInit } from '@angular/core';
import { CategoriaPromocion } from '../../model';
import { AppConfig } from '../../app.config';
import { PromocionService, PromocionCategoriaService, KeycloakService, I18nService } from '../../service/index';
import { Promocion, Categoria } from '../../model/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MessagesService, ErrorHandlerService } from "../../utils/index";
import * as Routes from '../../utils/rutas';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PERM_ESCR_CATEGORIAS_PROMOCION, PERM_ESCR_PROMOCIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
@Component({
    moduleId: module.id,
    selector: 'detalle-categoria',
    templateUrl: 'detalle-categoria.component.html',
    providers: [Categoria, PromocionCategoriaService, Promocion, PromocionService]
})
/**
 * Flecha Roja Technologies oct-2016
 * Fernando Aguilar
 * Componente encargado de mostrar el detalle y editar la categoría de promocion
 */
export class DetalleCategoriaComponent implements OnInit {

    public promociones: Promocion[];
    public displayModalEliminar: boolean = false;
    public promoEliminar: Promocion;
    public displayPromociones: boolean = false;
    public form: FormGroup;//validaciones
    public file_src: any;//campo del source de la imagen, contiene el url de la imagen
    public file: any;
    public tienePermisosEscritura: boolean;
    public permisoEcrituraPromo: boolean;
    public nombreExiste: boolean//true cuando el nombre es invalido
    public nombreInterno: string;
    constructor(
        public promo: Promocion,
        public categoria: Categoria, public i18nService: I18nService,
        public categoriaService: PromocionCategoriaService,
        public promocionService: PromocionService,
        public route: ActivatedRoute, public authGuard: AuthGuard,
        public router: Router, public kc: KeycloakService,
        public msgService: MessagesService
    ) {
        this.promociones = [];
        this.form = new FormGroup({
            "nombre": new FormControl('', Validators.required),
            "nombreInterno": new FormControl('', Validators.required),
            "descripcion": new FormControl('', Validators.required)
        });
        this.file_src = `${AppConfig.DEFAULT_IMG_URL}`;

        this.route.params.forEach((params: Params) => { this.categoria.idCategoria = params['id'] });

        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS_PROMOCION).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.authGuard.canWrite(PERM_ESCR_PROMOCIONES).subscribe((result: boolean) => {
            this.permisoEcrituraPromo = result;
        });
    }

    ngOnInit() {
        this.listarPromociones();
        this.getCategoria();

    }
    /**
     * Obtiene una determinada categoria del api y almacena dicha categoria en la variable de categoria correspondiente
     * 
     */
    getCategoria() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.categoriaService.token = tkn;
                let sub = this.categoriaService.obtenerCategoria(this.categoria.idCategoria).subscribe(
                    (categoria) => {
                        this.categoria = categoria;
                        this.file_src = this.categoria.imagen//actualizo el url de la imagen
                        this.nombreInterno = this.categoria.nombreInterno;
                    },
                    (error) => {
                        this.manejaError(error)//en caso de error se reenvia el error a la capa de manejo de errores
                    },
                    () => {
                        sub.unsubscribe();//evita memory leak
                    }
                );
            }
        );
    }
    /**
    * Edita los valores de la cateogria, emplea la variable this.categoria para la insercion
    */
    editarCatetoria() {
        if (this.tienePermisosEscritura) {
            if (!this.nombreExiste) {
                this.kc.getToken().then(
                    (tkn: string) => {
                        this.categoriaService.token = tkn;
                        //this.categoria.imagen = this.categoria.imagen.split(",")[1];//se almacenan el valor de la imagen, el split es necesario debido al formato del base64 generado
                        let sub = this.categoriaService.modificarCategoria(this.categoria).subscribe(
                            (data) => {
                                this.getCategoria();//actualiza el valor de la categoria
                                this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                            },
                            (error) => {
                                this.manejaError(error) //en caso de error se reenvia el error a la capa de manejo de errores
                            },
                            () => {
                                sub.unsubscribe();//evita memory leak
                            });
                    });
            } else {
                this.msgService.showMessage({ detail: "El nombre interno ya existe. Digite un nuevo nombre interno.", severity: "warn", summary: "" });
            }
        }

    }

    establecerNombres() {
        if (this.categoria.nombre != "" && this.categoria.nombre != null) {
            let temp = this.categoria.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.categoria.nombreInterno = temp;
            this.verificarNombreInterno();
        }
    }

    verificarNombreInterno() {
        if (this.categoria.nombreInterno == null || this.categoria.nombreInterno == "") {
            this.nombreExiste = false;
            return;
        }
        if (this.nombreInterno == this.categoria.nombreInterno) {
            this.nombreExiste = false;
            return;
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.validarNombre(this.categoria.nombreInterno).subscribe(
                    (data) => {
                        this.nombreExiste = data;
                        if (this.nombreExiste) {
                                this.msgService.showMessage({ detail: "El nombre interno ya existe. Digite un nuevo nombre interno.", severity: "warn", summary: "" });
                            }
                    }, (error) => { this.manejaError(error) }, () => { sub.unsubscribe() }
                );
            });
    }
    /**
    * Revoca la asociación entre la categoria y la promocion
    */
    eliminarPromo() {
        if (this.tienePermisosEscritura && this.permisoEcrituraPromo) {
            this.kc.getToken().then(
                (tkn: string) => {
                    this.categoriaService.token = tkn;
                    let sub = this.categoriaService.removerPrmocionCategoria(this.categoria.idCategoria, this.promoEliminar.idPromocion).subscribe(
                        () => {
                            this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                            this.listarPromociones();//se listan las promociones asociadas a esta categoria nuevamente
                        }, (error) => {
                            this.manejaError(error);//en caso de error se reenvia el error a la capa de manejo de errores
                        }, () => {
                            sub.unsubscribe();//evita memory leak
                        }
                    )
                });
        }

    }
    /**
     * Metodo encargado de listar las promociones asociadas a la categoria en cuestion 
     * */
    listarPromociones() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.categoriaService.token = tkn;
                let sub = this.categoriaService.obtenerPromocionesPorCategoria(this.categoria.idCategoria).subscribe(
                    (data: Promocion[]) => {
                        this.promociones = data;
                    }, (error) => {
                        this.manejaError(error);//en caso de error se reenvia el error a la capa de manejo de errores
                    }, () => {
                        sub.unsubscribe();//evita memory leak
                    }
                )
            });
    }
    /**
    * Encargado de manejo de errores
    */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /*
     * Metodo encargado de mostrar un modal de confirmacion antes de revcar una promocion de la lista d epromociones asociadas a la CategoriaPromocion,
     * es invocado desde el template de este componente
    */
    mostrarModalEliminarPromo(promo: Promocion) {
        this.promoEliminar = promo;
        this.displayModalEliminar = true;
    }
    /**
     * Navega hacia el detalle de la promocion, este Metodo
   *  Es invocado desde el template de este componente 
     */
    gotoDetailPromo(promo) {
        let link = [Routes.RT_PROMOCIONES_DETALLE_PROMOCION + promo.idPromocion];
        this.router.navigate(link);
    }
    /* Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form, este Metodo
    *  Es invocado desde el template de este componente 
    */
    fileChange(input) {

        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            // Obtener el (base64 de la imagen)
            img.src = this.categoria.imagen = event.target.result;
            // Redimensionar la imagen
            this.file_src = this.resize(img);
            let urlBase64 = this.file_src.split(",");
            this.categoria.imagen = urlBase64[1];
        }, false);

        reader.readAsDataURL(input.files[0]);


    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");


        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");

        ctx.drawImage(img, 0, 0, width, height);

        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        return dataUrl
    }

    //Regresar a la lista
    goBack() {
        this.router.navigate([Routes.RT_PROMOCIONES_LISTA_CATEGORIAS]);
    }

}