import { I18nService } from '../../service/i18n.service';
import { PromocionService, PromocionCategoriaService } from '../../service/index';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Categoria, Promocion } from '../../model/index';
import { Router } from '@angular/router';
import { MessagesService, ErrorHandlerService } from "../../utils/index";
import { KeycloakService } from '../../service/index';
import { PERM_ESCR_PROMOCIONES } from '../common/auth.constants'
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'detalle-promo-categorias',
    templateUrl: 'detalle-promo-categoria.component.html',
    providers: [Categoria, PromocionCategoriaService, ]
})
/**
 * Flecha Roja Technologies oct-2016
 * Fernando Aguilar
 * Componente encargado de mostrar las categorias de promo ademas de asociar y revocar las categorias a la promocion
 */

export class DetallePromoCategoriasComponent implements OnInit {

    public categoriasDisponibles: Categoria[];//Lista de categorias que pueden ser insertadas en la promocion
    public categorias: Categoria[];//lista de categorias que la promocion tiene asignadas actualmente
    public displayInsertar = false;
    public tienePermisosEscritura:boolean;//Indica si el usuario tiene permiso para escribir en el componente actual
    public cantRegistros:number;
    public rowOffset:number;
    public rowOffsetDisponibles:number;
    public listaVacia: boolean = false;
    constructor(public categoria: Categoria,
        public promocionService: PromocionService,
        public promocionCategoriaService:PromocionCategoriaService,
        public authGuard:AuthGuard,
        public promocion: Promocion, 
        public kc: KeycloakService,private i18nService:I18nService,
        public router: Router,
        public msgService: MessagesService) {

        this.categorias = [];
        this.categoriasDisponibles = [];
        if (this.promocion.indEstado == "BO" || this.promocion.indEstado == "AR") {
            this.tienePermisosEscritura = false;
        } else {
            this.authGuard.canWrite(PERM_ESCR_PROMOCIONES).subscribe((result: boolean) => {
                this.tienePermisosEscritura = result;
            });
        }
    }

    ngOnInit() {
        this.listarCategoriasAsociadas();
    }
    /**
     * Lista las categorias asociadas a la promocion
     */
    listarCategoriasAsociadas() {
        this.categorias = []
        this.kc.getToken().then(
            (tkn:string) => {
                this.promocionService.token=tkn;
                let sub = this.promocionService.obtenerCategoriaAsociada(this.promocion.idPromocion).subscribe(
                    (data: Categoria[]) => {
                        this.categorias = data
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Lista las categorias disponibles en el API
     */
    listarcategoriasDisponibles() {
        this.categoriasDisponibles = [];
        this.kc.getToken().then(
            (tkn:string) => {
                this.promocionCategoriaService.token=tkn;
                let sub = this.promocionCategoriaService.obtenerCategorias().subscribe(
                    (data: Categoria[]) => {
                        let existe;
                        if(data.length > 0){
                            this.listaVacia = false;
                        }else{ this.listaVacia = true;}
                        data.forEach((categoriaData) => {
                            existe = this.categorias.find((categoria) => { return categoria.idCategoria == categoriaData.idCategoria });
                            if (existe == null || existe == undefined) {
                                this.categoriasDisponibles=[...this.categoriasDisponibles,categoriaData];
                            }
                        })

                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Asocia una categoría a la promocion
     */
    asociarCategoria(categoria: Categoria) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.promocionCategoriaService.token=tkn;
                let sub = this.promocionCategoriaService.asociarPromocionCategoria(categoria.idCategoria, this.promocion.idPromocion).subscribe(
                    () => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.listarCategoriasAsociadas();
                        this.displayInsertar = false;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                )
            });
    }
    /**
     * Revoca la asociación entre la categoria y la promocion
     */
    revocarCategoria(categoria: Categoria) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.promocionCategoriaService.token = tkn;
                let sub = this.promocionCategoriaService.removerPrmocionCategoria(categoria.idCategoria, this.promocion.idPromocion).subscribe(
                    () => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                        this.listarCategoriasAsociadas();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                )
            });
    }
    /**Metodo que muestr el modal de categorias disponibles y carga la lista de categorias disponibles */
    displayModalInsertar() {
        this.displayInsertar = true;
        this.listarcategoriasDisponibles();
    }

    /**
     * Encargado de manejo de errores
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}