import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Promocion } from '../../model/index';
import { PromocionService } from '../../service/index';
import { MenuItem } from 'primeng/primeng';
import { Response } from '@angular/http';
import { KeycloakService } from '../../service/index';
import { PERM_ESCR_PROMOCIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
@Component({
    moduleId: module.id,
    selector: 'lista-promo-component',
    templateUrl: 'lista-promo.component.html',
    providers: [Promocion, PromocionService]
})

/**
* Flecha Roja Technologies oct-2016
* Fernando Aguilar
* Componente encargado de mostra la lista de promociones y
* brindar la posibilidad de navegar hacia el detalle de la promo 
*/
export class ListaPromoComponent implements OnInit {
    public listaPromos: Promocion[];//lista de promociones 
    public promocionEliminar: Promocion;//variable para almacenar la promo a eliminar

    public displayModalEliminar: boolean = false;//true= se muestra el modal
    public displayModalMensaje: boolean = false;//true= se muestra el modal

    public displayModalConfirm: boolean;//se muestra el modal de confirmacion cuando este valor es true
    public itemsBusqueda: MenuItem[] = [];//usado para poblar el split button de terminos avanzados de busqueda
    public busquedaAvanzada: boolean = false;//usadopara desplegar el div de busqueda avanzada
    public rowOffset: number;
    public cantRegistros: number;//cantidad de registros que se va a solicitar al api
    public cantTotalRegistros: number;//cantidad total de registros en el sistema para los terminos de busqueda establecidos

    /** Parametros de busqueda avanzada*/
    public terminosBusqueda: string[]=[]; // termino de busqueda
    public estados: string[] = ["A", "C"];//estados de promocion que se desplegaran
    public camposFiltro: string[]=[]; // atributos de la busqueda avanzada
    public acciones: string[]=[]; // url o codigos
    public tipos:string[]=[];
    public calendarizacion: string[]=[]; // refinar por calendarizacion
    public tipoOrden: string; // tipo de orden (asc o desc)
    public campoOrden: string; // campo sobre el cual ordenar

    public permisoEscritura: boolean; //Guarda el valor del permiso

    constructor(
        public promo: Promocion,
        public router: Router, public kc: KeycloakService,
        public promocionService: PromocionService,
        public msgService: MessagesService,
        public authGuard: AuthGuard
        ) {

        this.rowOffset = 0;
        this.cantRegistros = 10;
        this.cantTotalRegistros = 0;
        this.terminosBusqueda = [];
        this.itemsBusqueda = [
            { label: 'Avanzado', icon: 'ui-icon-build', command: () => { this.busquedaAvanzada = !this.busquedaAvanzada } }
        ];
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PROMOCIONES).subscribe((permiso: boolean) => {
            this.permisoEscritura = permiso;
        });
    }
    ngOnInit() {
        this.cargarPromos();
    }

    selectPromoEliminar(promo: Promocion) {
        this.promocionEliminar = promo;
    }

    /*Metodo que reririge al detalle del segmento que recibe por parámetros
   * Recibe: el segmento al cual redirigir al detalle
   */
    gotoDetail(promo: Promocion) {
        let link = ['promociones/detallePromo', promo.idPromocion];
        this.router.navigate(link);
    }

    /**Usado para cargar de manera perezosa los elementos de la lista de promociones */
    loadData(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantRegistros = event.rows;
        this.cargarPromos();
    }

    /**Metodo encargado de cargar las promos de la base de datos
     * Utiliza el string de busqueda que fue seteado por el campo de busqueda,
     * en este caso se obtiene el response completo para efectos de obtener los headers para la paginacion
     */
    cargarPromos() {

        this.kc.getToken().then(
            (tkn) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.buscarPromociones(this.terminosBusqueda, this.estados, this.rowOffset, this.cantRegistros,this.tipos, this.camposFiltro, this.tipoOrden, this.campoOrden).subscribe(
                    (data: Response) => {
                        this.cantTotalRegistros = +data.headers.get("Content-Range").split("/")[1];
                        this.listaPromos = <Promocion[]>data.json();//se obtiene los datos del body del request

                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /*
      * Muestra un mensaje de error al usuario con los datos de la transaccion
      * Recibe: una instancia de request con la informacion de error
      * Retorna: un observable que proporciona información sobre la transaccion
      */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}