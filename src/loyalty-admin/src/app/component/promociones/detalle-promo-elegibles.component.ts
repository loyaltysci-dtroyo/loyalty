import { I18nService } from '../../service/i18n.service';
import { Component, OnInit, Renderer } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { MenuItem, SelectItem } from 'primeng/primeng';
import { Miembro, Promocion, Segmento, Ubicacion } from '../../model/index';
import { MiembroService, PromocionService, SegmentoService } from '../../service/index';

import { KeycloakService } from '../../service/index';
import { Router } from '@angular/router';
import { PERM_ESCR_PROMOCIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar 
 * Componente encargado de administrar los miembros que seran elegibles para la promocion, 
 * por lo tanto determinando el publico meta para la promocion
 */
@Component({
    moduleId: module.id,
    selector: 'detalle-promo-elegibles-component',
    templateUrl: 'detalle-promo-elegibles.component.html',
    providers: [MiembroService, SegmentoService, Miembro, Segmento, Ubicacion]
})
export class DetallePromoElegiblesComponent implements OnInit {
    public listaSegmentosDisponibles: Segmento[];//segmentos disponibles
    public listaUbicacionesDisponibles: Ubicacion[];//lista de ubicaciones
    public listaMiembrosDisponibles: Miembro[];//miembros disponibles
    public listaSegmentos: Segmento[];//lista de segmentos elegibles para la promo
    public listaUbicaciones: Ubicacion[];//lista de ubicaciones elegibles para la promo
    public listaMiembros: Miembro[];//lista de miembros elegibles para la promo
    public indExclusion: string;//indicador para determinar su cargar la lista de incluir o excluir
    public indComponente: string;//indicador para determinar si desplegar segmentos o usuarios o ubicaciones
    public busquedaElegibles: string;//campo de busqueda para elegibles en el caso de miembros
    public busquedaDisponibles: string;//campo de busqueda para segmentos/usuarios/ubicaciones disponibles para agregar 
    public seleccMultipleAgregar: any[];//lista de miembros actualmente seleccionados en la seleccion multiple de agregar
    public seleccMultipleEliminar: any[];//lista de segmentos actualmente seleccionados en la seleccion multiple de eliminar

    public displayInsertar: boolean = false;//muestra el modal de insertar
    public displayEliminar: boolean = false;//muestra el modal de eliminar
    public itemsIndComponente: SelectItem[] = [];//items de select para poblar el drop de componente: segmento, miembro o ubicacion
    public itemstindSegmentos: SelectItem[] = [];//items de select para poblar el drop de incluir/excluir
    public itemsDetalle: MenuItem[] = [];//items de menu para poblar el split button de busqueda avanzada
    public elementoEliminar: any;//variable usada para almacenar el elemento a eliminar de una menera temporal
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema

    constructor(public promo: Promocion,
        private i18nService: I18nService,
        public segmentoService: SegmentoService,
        public miembroService: MiembroService,
        public authGuard: AuthGuard,
        public kc: KeycloakService,
        public promocionService: PromocionService,
        public router: Router,
        public msgService: MessagesService) {

        this.indExclusion = "I";
        this.indComponente = "S";
        this.itemsIndComponente = this.i18nService.getLabels("promociones-itemsIndComponente");
        this.itemstindSegmentos = this.i18nService.getLabels("general-itemsIndExclusion");
        this.itemsDetalle = [{ label: this.i18nService.getLabels("promociones-detalle-itemsDetalle"), icon: 'ui-icon-delete', command: () => { this.displayEliminar = true } }];

        /**Se determina si el usuario posee permisos para escribir en este recurso */
        if (this.promo.indEstado == "BO" || this.promo.indEstado == "AR") { this.tienePermisosEscritura = false; }
        else {
            this.authGuard.canWrite(PERM_ESCR_PROMOCIONES).subscribe((result: boolean) => {
                this.tienePermisosEscritura = result;
            });
        }
    }
    ngOnInit() {
        this.listarElegiblesPromo();
    }
    //almacena temporalmente el elemento a eliminar posteriormente
    selectElementoEliminar(elemento: any) {
        this.elementoEliminar = elemento;
    }
    //eliminar el elemento seleccionado, notese que utiliza el indicador de componente para realizar la operacion
    eliminarElemento() {
        if (this.indComponente == "S") {
            this.eliminarSegmento(this.elementoEliminar);
        }
        else if (this.indComponente == "M") {
            this.eliminarMiembroListas(this.elementoEliminar);
        }
        else if (this.indComponente == "U") {
            this.eliminarUbicacionListas(this.elementoEliminar);
        }
        this.displayInsertar = false;

    }
    //invoca al metodo correspondiente para cargar los elementos disponibles con base en el indicador del modal 
    listarElementosDisponibles() {

        if (this.indComponente == "S") {
            this.listarSegmentosDisponibles();
        }
        else if (this.indComponente == "M") {
            this.listarMiembrosDisponibles();
        }
        else if (this.indComponente == "U") {
            this.listarUbicacionesDisponibles();
        }
        this.displayInsertar = true;
    }
    //lista miembros disponibles para agregar
    listarMiembrosDisponibles() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.listarMiembrosElegiblesPromo(this.promo.idPromocion, "D").subscribe(
                    (data: Miembro[]) => {
                        this.listaMiembrosDisponibles = data;
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    //lista segmentos disponibles para agregar a la lista de elegibles
    listarSegmentosDisponibles() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.listarSegmentosElegiblesPromo(this.promo.idPromocion, "D").subscribe(
                    (data: Segmento[]) => {
                        this.listaSegmentosDisponibles = data;//se asignan los datos provenientes del api al listado de segmentos
                    },
                    (error) => {
                        this.manejaError(error);//se informa al usuario si hubo algun error
                    },
                    () => {
                        sub.unsubscribe(); //evita memory leak
                    }
                );
            });
    }
    //lista las ubicaciones disponibles para agregar a la lista de elegibles
    listarUbicacionesDisponibles() {
        this.listaUbicacionesDisponibles = [];
        for (let i = 1; i < 4; i++) {
            let ubicacionTemp = new Ubicacion();
            ubicacionTemp.nombre = "lorem";
            ubicacionTemp.nombreDespliegue = "dolor";
            this.listaUbicacionesDisponibles=[...this.listaUbicacionesDisponibles,ubicacionTemp];
        }
    }

    //-------------OPERACIONES DE MIEMBROS----------------
    /**
     * Incluye un miembro a la lista de elegibles para la promocion
     * Recibe: el miembroa insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    incluirMiembro(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.incluirMiembro(this.promo.idPromocion, miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
    * Incluye una lista de miembros a la lista d elgibles de la promcoion
    * Emplea la variable seleccionMultipleAgregar para tomar el listado de miembros que debe agregar
    * Llama al metodo manejaError en caso de excepcion
    */
    incluirListaMiembros(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let idMiembros = this.seleccMultipleAgregar.map((elem: Miembro) => { return elem.idMiembro });
                let sub = this.promocionService.incluirListaMiembros(this.promo.idPromocion, idMiembros).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-multiple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Excluye una lista de miembros de la lista de elegibles para la promocion
     * Recibe: el miembro a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    excluirListaMiembro(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let idMiembros = this.seleccMultipleAgregar.map((elem: Miembro) => { return elem.idMiembro });
                let sub = this.promocionService.excluirListaMiembros(this.promo.idPromocion, idMiembros).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-exclusion-multiple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Excluye una lista de miembros de la lista de elegibles para la promocion
     * Recibe: el miembro a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarListaMiembro(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let idMiembros = this.seleccMultipleEliminar.map((elem: Miembro) => { return elem.idMiembro });
                let sub = this.promocionService.eliminarListaMiembros(this.promo.idPromocion, idMiembros).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-multiple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
    * Excluye un miembro de la lista de elegibles para la promocion
    * Recibe: el miembro a excluir de la lista de elegibles
    * Llama al metodo manejaError en caso de excepcion
    */
    excluirMiembro(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.excluirMiembro(this.promo.idPromocion, miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-exclusion-multiple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Elimina un miembro de la lista de elegibles para la promocion
     * Recibe: el miembro a eliminar de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarMiembroListas(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.eliminarMiembro(this.promo.idPromocion, miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }

    //-------------OPERACIONES DE UBICACION----------------
    /**
     * Incluye una ubicaicon a la lista de elegibles para la promocion
     * Recibe: el ubicaicon a insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    incluirUbicacion(ubicacion: Ubicacion) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.incluirUbicacion(this.promo.idPromocion, ubicacion.idUbicacion + "").subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });

    }
    /**
     * Excluye una ubicacion de la lista de elegibles para la promocion
     * Recibe: la ubicacion a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    excluirUbicacion(ubicacion: Ubicacion) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.excluirUbicacion(this.promo.idPromocion, ubicacion.idUbicacion + "").subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-exclusion-simple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }


    /**
     * Elimina una ubicacion de la lista de elegibles para la promocion
     * Recibe: la ubicacion a eliminar de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarUbicacionListas(ubicacion: Ubicacion) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.eliminarUbicacion(this.promo.idPromocion, ubicacion.idUbicacion + "").subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }


    /**
     * Incluye una ubicaicon a la lista de elegibles para la promocion
     * Recibe: el ubicaicon a insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    incluirListaUbicacion(ubicacion: Ubicacion) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let idUbicaciones = this.seleccMultipleAgregar.map((elem: Ubicacion) => { return elem.idUbicacion });
                let sub = this.promocionService.incluirListaUbicaciones(this.promo.idPromocion, idUbicaciones).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-multiple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });

    }
    /**
     * Excluye una ubicacion de la lista de elegibles para la promocion
     * Recibe: la ubicacion a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    excluirListaUbicacion(ubicacion: Ubicacion) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let idUbicaciones = this.seleccMultipleAgregar.map((elem: Ubicacion) => { return elem.idUbicacion });
                let sub = this.promocionService.excluirListaUbicaciones(this.promo.idPromocion, idUbicaciones).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-exclusion-multiple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }


    /**
     * Elimina una ubicacion de la lista de elegibles para la promocion
     * Recibe: la ubicacion a eliminar de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarListaUbicacionListas(ubicacion: Ubicacion) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let idUbicaciones = this.seleccMultipleEliminar.map((elem: Ubicacion) => { return elem.idUbicacion });
                let sub = this.promocionService.eliminarListaUbicaciones(this.promo.idPromocion, idUbicaciones).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-multiple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }

    //-------------OPERACIONES DE SEGMENTOS----------------
    /**
     * Incluye un segmento a la lista de elegibles para la promocion
     * Recibe: el segmento a insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    incluirSegmento(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.incluirSegmento(this.promo.idPromocion, segmento.idSegmento).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Excluye un segmento de la lista de elegibles para la promocion
     * Recibe: el segmento a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    excluirSegmento(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.excluirSegmento(this.promo.idPromocion, segmento.idSegmento).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-exclusion-simple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Elimina un segmento de la lista de elegibles para la promocion
     * Recibe: El segmento a eliminar de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarSegmento(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.eliminarSegmento(this.promo.idPromocion, segmento.idSegmento).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }

    /**
     * Incluye un segmento a la lista de elegibles para la promocion
     * Recibe: el segmento a insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    incluirListaSegmentos(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let idSegmentos = this.seleccMultipleAgregar.map((elem: Segmento) => { return elem.idSegmento });
                let sub = this.promocionService.incluirListaSegmentos(this.promo.idPromocion, idSegmentos).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-multiple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Excluye un segmento de la lista de elegibles para la promocion
     * Recibe: el segmento a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    excluirListaSegmentos(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let idSegmentos = this.seleccMultipleAgregar.map((elem: Segmento) => { return elem.idSegmento });
                let sub = this.promocionService.excluirListaSegmentos(this.promo.idPromocion, idSegmentos).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-exclusion-multiple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Elimina un segmento de la lista de elegibles para la promocion
     * Recibe: El segmento a eliminar de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarListaSegmento(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let idSegmentos = this.seleccMultipleAgregar.map((elem: Segmento) => { return elem.idSegmento });
                let sub = this.promocionService.eliminarListaSegmentos(this.promo.idPromocion, idSegmentos).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-multiple"));
                        this.listarElegiblesPromo();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }


    //--------------------ELEGIBLES PROMO------------------------
    /**Metodo que lista los elementos elegible para la promo 
     * basandose en el indicador de componente que el usuario hay aselecionado 
     * */
    listarElegiblesPromo() {
        this.listaSegmentos = [];
        this.listaUbicaciones = [];
        this.listaMiembros = [];
        if (!this.displayInsertar) {//si no esta en modo de insercion
            //se listan los elegibles de la promo
            if (this.indComponente == "S") {
                this.listarSegmentosElegiblesPromo();
            }
            else if (this.indComponente == "M") {
                this.listarMiembrosElegiblesPromo();
            }
            else if (this.indComponente == "U") {
                this.listarUbicacionesElegiblesPromo();
            }
        } else {//si esta en modo de insercion
            //se listan los disponibles
            if (this.indComponente == "S") {
                this.listarSegmentosDisponibles();
            }
            else if (this.indComponente == "M") {
                this.listarMiembrosDisponibles();
            }
            else if (this.indComponente == "U") {
                this.listarUbicacionesDisponibles();
            }
        }
    }
    /**Lista los miembros asociados a una promocion, 
     * usa el indicador de inclusion directamente en el llamdo al service
     * para traer solo los miembros incluidos/excluidos */
    listarMiembrosElegiblesPromo() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.listarMiembrosElegiblesPromo(
                    this.promo.idPromocion, this.indExclusion).subscribe(
                    (data: Miembro[]) => {
                        this.listaMiembros = data;
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe() }
                    );
            });

    }
    /**Lista los segmentos asociados a una promocion, 
     * usa el indicador de inclusion directamente en el llamdo al service
     * para traer solo los segmentos incluidos/excluidos */
    listarSegmentosElegiblesPromo() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.listarSegmentosElegiblesPromo(
                    this.promo.idPromocion, this.indExclusion).subscribe(
                    (data: Segmento[]) => {
                        this.listaSegmentos = data;
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); }
                    );
            });
    }
    /**Lista ubicaciones asociadas a una promocion, 
     * usa el indicador de inclusion directamente en el llamdo al service
     * para traer solo las ubicaciones incluidos/excluidos */
    listarUbicacionesElegiblesPromo() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.listarUbicacionesElegiblesPromo(
                    this.promo.idPromocion, this.indExclusion).subscribe(
                    (data: Ubicacion[]) => {
                        this.listaUbicaciones = data;
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); }
                    );
            });
    }

    /*
    * Muestra un mensaje de error al usuario con los datos de la transaccion
    * Recibe: una instancia de request con la informacion de error
    * Retorna: un observable que proporciona información sobre la transaccion
    */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    /**Metodo encargado de redirigir hacia el detalle del elemento seleccionado 
     * para brindar una mejor experiencia de usuario y permitirle verificar los datos*/
    gotoDetail(elemento) {
        let link;
        if (this.indComponente == 'S') {
            link = ["/segmentos/detalleSegmento", (elemento as Segmento).idSegmento]
            this.router.navigate(link);
        }
        if (this.indComponente == 'M') {
            link = ["/miembros/detalleMiembro", (elemento as Miembro).idMiembro]
            this.router.navigate(link);
        }
        if (this.indComponente == 'U') {
            // link=["/ubicaciones/detalleUbicacion", (elemento as Ubicacion).idUbicacion]
            // this.router.navigate(link);
        }
    }

}