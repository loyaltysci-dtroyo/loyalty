import { I18nService } from '../../service';
import { ErrorHandlerService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';
import { MessagesService } from '../../utils/index'
import { PromocionService } from '../../service/index';
import { Promocion } from '../../model/index';
import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { FormGroup } from '@angular/forms';
import { KeycloakService } from '../../service/index';
import { PERM_ESCR_PROMOCIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { Router } from '@angular/router';
@Component({
    moduleId: module.id,
    selector: 'detalle-promo-component',
    templateUrl: 'detalle-promo.component.html',
    providers: [Promocion, PromocionService]
})
/**
 * Flecha Roja Technologies set-2016
 * Autor: Fernando Aguilar
 * Componente encargado de mostrar el menu y funcionar de padre para los componente de promocion
 * entre ellos insertar promocion, detalle de promocion y categorias de promocion asi como proveer
 * enrutamiento entre ellos, permite activar y desactivar promociones 
 */
export class DetallePromoComponent {
    public itemsCalendarizacion: SelectItem[];//items de menu para la calendarizacion de la promocion
    public form: FormGroup;//validaciones
    public rtCommon = Rutas.RT_PROMOCIONES_DETALLE_COMMON;//rutas dinamicas
    public rtElegibles = Rutas.RT_PROMOCIONES_DETALLE_ELEGIBLES;//rutas dinamicas
    public rtArte = Rutas.RT_PROMOCIONES_DETALLE_ARTE;//rutas dinamicas
    public rtAlcance = Rutas.RT_PROMOCIONES_DETALLE_ALCANCE;//rutas dinamicas
    public rtCategoria = Rutas.RT_PROMOCIONES_DETALLE_CATEGORIAS;//rutas dinamicas
    public rtActividad = Rutas.RT_PROMOCIONES_DETALLE_ACTIVIDAD;
    public isRecurrente: any;//flag de recurrencia de la promo true=recurrente
    public es: any; // locales para los calendar input en español
    public diasSeleccionados: string[];//dias seleccionados para recurrencia

    public minDate;//fecha minima a partir de la cual se permite al usuario definir la fecha inicial
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public displayModalEliminar: boolean = false;
    public displayModalEditar: boolean = false;
    public displayModalArchivar: boolean = false;

    public publicado: boolean;
    public borrador: boolean;
    public archivado: boolean;
    public inactivo: boolean;

    //Se guarda las fechas si es calendarizado
    public fecInicio: Date = null;
    public fecFin: Date = null;

    constructor(
        public promo: Promocion,
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,//ruta actual
        public promocionService: PromocionService,
        public msgService: MessagesService,
        public router: Router,
        private i18nService: I18nService
    ) {
        this.diasSeleccionados = [];
        this.itemsCalendarizacion = [];
        this.route.params.forEach((params: Params) => { this.promo.idPromocion = params['id'] });
        this.promo.indSemanaRecurrencia = 1;
        this.itemsCalendarizacion = this.i18nService.getLabels('promociones-items-calendarizacion');
        this.minDate = new Date(); //fecha minima para la calendarizacion
        this.es = this.i18nService.getLabels("general-calendar");
        this.obtenerPromo();
        this.authGuard.canWrite(PERM_ESCR_PROMOCIONES).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
     *Obtiene los datos de la promocion para desplegar el estado de la promocion y el nombr een la barra de encabezado
     */
    obtenerPromo() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.getPromocion(this.promo.idPromocion).subscribe(
                    (data: Promocion) => {
                        this.copyValuesOf(this.promo, data);
                        // this.promo = data;
                        /* se transforma el string de dias de recurrencia y semanas de recurrencia para desplegar la promo en caso de que esta sea 
                         * de tipo activado
                         */
                        if (this.promo.indDiaRecurrencia && this.promo.indDiaRecurrencia != "") {
                            for (let i = 0; i < this.promo.indDiaRecurrencia.length; i++) {
                                this.diasSeleccionados = [...this.diasSeleccionados, this.promo.indDiaRecurrencia[i]];
                            }
                        }
                        switch(this.promo.indEstado ){
                            case 'A': this.publicado = true;
                            break;
                            case 'B': this.archivado = true;
                            break;
                            case 'D': this.inactivo = true;
                            break;
                            default: this.borrador = true;
                        }

                        if (this.promo.fechaInicio != null) {
                            this.fecInicio = new Date(this.promo.fechaInicio);
                        }
                        if (this.promo.fechaFin != null) {
                            this.fecFin = new Date(this.promo.fechaFin);
                        }

                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); }
                )
            });
    }
    /**Vuelve a establecer valores por defecto cuando el usuario seleccion una opcion diferente en el select*/
    onChangeDropCalendarizacion(event) {
        if (event.value == "C") {
            this.promo.fechaInicio = new Date();
            this.promo.fechaFin = new Date();
            this.promo.indDiaRecurrencia = "";
            this.promo.indSemanaRecurrencia = 1;
        }
    }
    /**Vuelve a establecer valores por defecto cuando el usuario cambia el check de recurrencia*/
    onChangeRecurrencia() {
        if (!this.isRecurrente) {
            this.promo.indDiaRecurrencia = "";
            this.promo.indSemanaRecurrencia = 1;
        }
    }


    /**
     * Metodo invocado al publicar una promocion
     * Establece el estado de la promocion a activo y define el metadata para la activacion de la misma
     */
    publicarPromocion() {
        let indDias = "";
        this.diasSeleccionados.forEach((element) => {
            indDias += element;
        });
        this.promo.indDiaRecurrencia = indDias;
        this.diasSeleccionados = []

        let indSemanas = "";

        if (this.promo.indCalendarizacion != "C") {
            this.promo.fechaInicio = null;
            this.promo.fechaFin = null;
            this.promo.indDiaRecurrencia = null;
            this.promo.indSemanaRecurrencia = null;
        }
        this.promo.indEstado = "A";
        if (this.promo.indDiaRecurrencia == "") { this.promo.indDiaRecurrencia = null }

        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.updatePromocion(this.promo).subscribe((data) => {
                    this.msgService.showMessage(this.i18nService.getLabels("general-publicar"));
                    this.obtenerPromo();
                },
                    (error) => { this.manejaError(error) });
                () => { sub.unsubscribe(); this.obtenerPromo(); }
            });

    }
    /**
     * Cancela la publicacion de la promocion y la devuelve al estado de archivado
     */
    arhivarPromo() {
        this.promo.indEstado = "B";
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.updatePromocion(this.promo).subscribe(
                    (data) => {
                        this.obtenerPromo();
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.displayModalArchivar = false;
                    }, (error) => { this.manejaError(error) }
                );
            });
    }

    /**
  * Cancela la publicacion de la promocion y la devuelve al estado de archivado
  */
    eliminarPromo() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.deletePromocion(this.promo).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                        this.router.navigate(['/promociones']);
                    }, (error) => { this.manejaError(error) }
                );
            });
    }

    //Método que permite regresar en la navegación
    goBack() {
        let link = [Rutas.RT_PROMOCIONES_LISTA_PROMOCIONES];
        this.router.navigate(link);
    }

    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}