import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { Categoria } from '../../model/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { RT_CATEGORIAS_DETALLE, RT_CATEGORIAS_INSERTAR } from '../../utils/rutas';
import { PromocionCategoriaService, I18nService, KeycloakService } from '../../service/index';
import { AuthGuard } from '../common/index';
import { PERM_ESCR_CATEGORIAS_PROMOCION } from '../common/auth.constants';

@Component({
    moduleId: module.id,
    selector: 'lista-categorias-component',
    templateUrl: 'lista-categorias.component.html',
    providers: [Categoria, PromocionCategoriaService]
})
/**
 * Flecha Roja Technologies set-2016
 * Fernando Aguilar
 * Componente encargado de listar las categorias de la promocion
 */
export class ListaCategoriasPromoComponent implements OnInit {

    public categorias: Categoria[];//lista de ctegorias
    public displayModalEliminar: boolean;//muesta el modal con el form para inertar
    public categoriaEliminar: Categoria;//almacena la categoria a eliminar
    public rutaInsertar = RT_CATEGORIAS_INSERTAR;//para efectos de forwarding
    public escritura: boolean; //Permite verificar permisos (true/false)

    constructor(public categoria: Categoria,
        public kc: KeycloakService,
        public categoriaService: PromocionCategoriaService,
        public router: Router,
        private i18nService: I18nService,
        public msgService: MessagesService,
        public authGuard: AuthGuard
    ) {
        this.categorias = [];
        //Permite saber si el usuario tiene permisos de escritura sobre este elemento
        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS_PROMOCION).subscribe(
            (permiso: boolean) => { this.escritura = permiso; });
    }

    ngOnInit() {
        this.getListaCategorias();
    }
    /**
     * Obtiene todas las categorias del API
     */
    getListaCategorias() {
        this.kc.getToken().then(
            (tkn) => {
                this.categoriaService.token = tkn;
                let sub = this.categoriaService.obtenerCategorias().subscribe(
                    (data: Categoria[]) => {
                        this.categorias = data;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
     * Metoo encargado de manejar errores, recibe una instancia de response y la redirige al service de manejo
     * de errores
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /**
     * Metodo encargado de mistra un mensaje de confimacion al usuario paa el iminar la categoria
     *  */
    mostrarModalEliminarCategoria(categoria: Categoria) {
        this.displayModalEliminar = true;
        this.categoriaEliminar = categoria;
    }
    /**Metodo encagado de eliminar la categoria dela base de datos */
    eliminarCategoria() {
        if (this.escritura) {
            this.displayModalEliminar = false;
            this.kc.getToken().then(
                (tkn) => {
                    this.categoriaService.token = tkn;
                    let sub = this.categoriaService.eliminarCategoria(this.categoriaEliminar.idCategoria).subscribe(
                        (data) => {
                            this.getListaCategorias();
                            this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"))
                        },
                        (error) => { this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error)) },
                        () => { sub.unsubscribe() }
                    );
                });
        }
    }

    gotoDetail(categoria: Categoria) {
        let link = RT_CATEGORIAS_DETALLE;
        this.router.navigate([link, categoria.idCategoria]);
    }

}