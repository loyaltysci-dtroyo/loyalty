import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PromocionService, EstadisticaService } from '../../service/index';
import { Promocion } from '../../model/index';
import { KeycloakService } from '../../service/index';

@Component({
    moduleId: module.id,
    selector: 'detalle-promo-alcance-component',
    templateUrl: 'detalle-promo-alcance.component.html',
    providers:[EstadisticaService]
})
/**
 * Flecha Roja Technologies oct-2016
 * Fernando Aguilar
 * Componente encargado de mostrar el detalle de la promocion y editar la promocion
 */
export class DetallePromoAlcanceComponent implements OnInit {
    public statsData: {
        idPromocion: string,
        nombrePromocion: string,
        nombreInternoPromocion: string,
        cantMiembrosElegibles: number,
        cantVistasUnicasElegibles: number,
        cantVistasTotales: number,
        periodos: [
            {
                mes: string,
                cantMarcasPromedio: number,
                cantElegiblesPromedio: number,
                cantVistasTotales: number,
                cantVistasUnicas: number
            },
            {
                mes: string,
                cantMarcasPromedio: number,
                cantElegiblesPromedio: number,
                cantVistasTotales: number,
                cantVistasUnicas: number
            },
            {
                mes: string,
                cantMarcasPromedio: number,
                cantElegiblesPromedio: number,
                cantVistasTotales: number,
                cantVistasUnicas: number
            },
            {
                mes: string,
                cantMarcasPromedio: number,
                cantElegiblesPromedio: number,
                cantVistasTotales: number,
                cantVistasUnicas: number
            }
        ]
    };
    public chartVistasData: any;
    public chartElegibilidadData: any;
    public options: any;

    constructor(public promo: Promocion, public kc: KeycloakService,
        public promocionService: PromocionService,private i18nService:I18nService,
        public estadisticaService: EstadisticaService,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService) { 

        }

    ngOnInit() {
        this.obtenerPromo();
        
    }

    // this.statsData.periodos.map((element=>element.mes))
    poblarGraficos() {
        this.chartVistasData = {
            labels: this.statsData.periodos.map((element => element.mes)),
            datasets: [
                {
                    label: this.i18nService.getLabels("promociones-alcance-vistas-totales") ,
                    data: this.statsData.periodos.map((element => element.cantVistasTotales)),
                    fill: false,
                    borderColor: '#4bc0c0'
                },
                {
                    label: this.i18nService.getLabels("promociones-alcance-vistas-unicas"),
                    data: this.statsData.periodos.map((element => element.cantVistasUnicas)),
                    fill: false,
                    borderColor: '#afc0c0'
                },
                {
                    label: this.i18nService.getLabels("promociones-alcance-marcas-promedio"),
                    data: this.statsData.periodos.map((element => element.cantMarcasPromedio)),
                    fill: false,
                    borderColor: '#f3c0c0'
                },
            ]
        }
        this.chartElegibilidadData = {
            labels: this.statsData.periodos.map((element => element.mes)),
            datasets: [
                {
                    label: this.i18nService.getLabels("promociones-alcance-elegibles-promedio"),
                    data: this.statsData.periodos.map((element => element.cantElegiblesPromedio)),
                    fill: false,
                    borderColor: '#4bc0c0'
                }
            ]
        }
    }

    obtenerDatosEstadistica() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.estadisticaService.token = tkn;
                let sub = this.estadisticaService.getestadisticasPromocion(this.promo.idPromocion).subscribe(
                    (responseData: any) => {
                        this.statsData = responseData;
                        this.poblarGraficos();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /**Obtiene una promo a traves del API */
    obtenerPromo() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.getPromocion(this.promo.idPromocion).subscribe(
                    (chartData: Promocion) => {
                        this.copyValuesOf(this.promo, chartData);
                        this.obtenerDatosEstadistica();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    selectData(event) {

    }
    /**
   * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
   * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
   * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
   * no siempre sea necesario llamar al api para actualizar los datos
   */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}