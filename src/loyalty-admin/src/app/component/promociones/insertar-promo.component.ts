import { I18nService } from '../../service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MenuItem, Message, SelectItem } from 'primeng/primeng';
import { PERM_ESCR_PRODUCTOS, PERM_ESCR_PROMOCIONES } from '../common/auth.constants';
import { RT_PROMOCIONES_DETALLE_COMMON, RT_PROMOCIONES_DETALLE_PROMOCION } from '../../utils/rutas';
import * as Routes from '../../utils/rutas';
import { AuthGuard } from '../common/index';
import { Component } from '@angular/core';
import { KeycloakService } from '../../service/index';
import { Promocion } from '../../model/index';
import { PromocionService } from '../../service/index';
import { Response } from '@angular/http';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'insertar-promo-component',
    templateUrl: 'insertar-promo.component.html',
    providers: [Promocion, PromocionService]
})
/**
 * Flecha Roja Technologies set-2016
 * Fernando Aguilar
 * Componente encargado de insertar promociones al sistema
 */
export class InsertarPromoComponent {
    public tags: string[];
    public nombreExiste: boolean;//true si el nombre es valido
    public idPromoInsertada: string;//id que retorna el api cuado la promo es insertada
    public itemsCodigo: SelectItem[] = [];//items para poblar el select de codigo de promo
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public insertando: boolean = false;
    constructor(public promo: Promocion,
        public router: Router, 
        private i18nService:I18nService,
        public kc: KeycloakService,
        public promocionService: PromocionService,
        public authGuard: AuthGuard,
        public msgService: MessagesService) {


        //poblar los items de codigo
        this.itemsCodigo=this.i18nService.getLabels("promociones-items-codigo");

        this.promo.urlOrCodigo = null;

        this.authGuard.canWrite(PERM_ESCR_PROMOCIONES).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }
    establecerNombres() {
        try {
            this.promo.descripcion = this.promo.nombre;
            let temp = this.promo.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.promo.nombreInterno = temp;
        } catch (e) { }
    }
    /** 
     * Inserta una promcion en el sistema, emplea los datos que contiene la variable global this.promo
     */
    insertarPromo() {
        this.promo.indEstado = 'C';
        this.kc.getToken().then(
            (tkn) => {
                this.promocionService.token = tkn;//actualiza el token de authenticacion
                this.insertando = true;
                let sub = this.promocionService.addPromocion(this.promo).subscribe(
                    (response: Response) => {
                        try {
                            this.promo.tags = this.tags.join("\n");
                        } catch (e) { };
                        this.idPromoInsertada = response.text();//obtiene el id de la promocion insertada a partir del body del response
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"))
                        this.gotoDetail(this.idPromoInsertada);

                    }, (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); this.insertando = false; }
                );
            });
    }
    /*Metodo que reririge al detalle del segmento que recibe por parámetros
  * Recibe: el segmento al cual redirigir al detalle
  */
    gotoDetail(idPromo: string) {
        let link = [RT_PROMOCIONES_DETALLE_PROMOCION + idPromo];
        this.router.navigate(link);
    }
    validarNombre() {
        if (this.promo.nombreInterno == "") { this.nombreExiste = false; return }
        this.kc.getToken().then(
            (tkn) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.validarNombre(this.promo.nombreInterno).subscribe(
                    (existeNombre: string) => {
                        if(existeNombre == "true"){
                            this.nombreExiste = true;
                        }else{
                            this.nombreExiste = false;
                        }
                    }, () => {

                    }, () => { sub.unsubscribe(); }
                );
            });
    }
    /*
  * Muestra un mensaje de error al usuario con los datos de la transaccion
  * Recibe: una instancia de request con la informacion de error
  * Retorna: un observable que proporciona información sobre la transaccion
  */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    confirm() {
        this.gotoDetail(this.idPromoInsertada);
    }

    //Regresar a la lista
    goBack() {
        this.router.navigate([Routes.RT_PROMOCIONES]);
    }
}