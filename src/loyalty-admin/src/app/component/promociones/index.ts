export { PromocionesComponent } from './promociones.component'
export { DetallePromoComponent } from './detalle-promo.component'
export { DetallePromoAlcanceComponent } from './detalle-promo-alcance.component'
export { DetallePromoArteComponent } from './detalle-promo-arte.component'
export { DetallePromoCommonComponent } from './detalle-promo-common.component'
export { DetallePromoElegiblesComponent } from './detalle-promo-elegibles.component'
export { ListaPromoComponent } from './lista-promo.component'
export { InsertarPromoComponent } from './insertar-promo.component'
export { InsertarCategoriaPromoComponent } from './insertar-categoria.component'
export { ListaCategoriasPromoComponent } from './lista-categorias.component'
export { DetallePromoCategoriasComponent} from './detalle-promo-categorias.component';
export { DetalleCategoriaComponent} from './detalle-categoria.component';
export { DetallePromoActividadComponent} from './detalle-promo-actividad.component';
export { PromocionesActividadComponent} from './promociones-actividad.component';
export { PromocionesDashboardGeneralComponent} from './promociones-dashboard-general.component';
