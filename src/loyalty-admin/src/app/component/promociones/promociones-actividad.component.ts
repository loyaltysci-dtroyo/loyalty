import { I18nService } from '../../service';
import { ErrorHandlerService, MessagesService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { ActividadService, KeycloakService } from '../../service/index';
import { Miembro, RegistroActividad } from '../../model/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { PERM_LECT_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'promociones-actividad',
    templateUrl: 'promociones-actividad.component.html',
    providers: [Miembro, ActividadService]
})

export class PromocionesActividadComponent implements OnInit {

    /**Atributos para sorting/busqueda */
    public indPeriodo: string = '1M';
    public periodos: SelectItem[] = [];
    public listaActividades: any[];
    public lecturaMiembro: boolean; //Permiso de lectura sobre miembro

    constructor(
        public miembro: Miembro,
        public actividadService: ActividadService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public route: ActivatedRoute,
        public router: Router,
        private i18nService: I18nService,
        public authGuard: AuthGuard
    ) {
        //Permite saber si el usuario tiene permisos de lectura sobre miembro
        this.authGuard.canWrite(PERM_LECT_MIEMBROS).subscribe((permiso: boolean) => {
            this.lecturaMiembro = permiso;
        });

    }

    ngOnInit() {
        this.periodos = this.i18nService.getLabels("general-actividad-periodos");
        this.obtenerActividad();
    }

    /**
     * Obtiene los registros de actividad para el miembro actual
     */
    obtenerActividad() {
        this.kc.getToken().then((tkn) => {
            this.actividadService.token = tkn;
            let sub = this.actividadService.getActividadesPromociones(this.indPeriodo).subscribe(
                (response: RegistroActividad[]) => {
                    this.listaActividades = response;
                }, (response: Response) => {
                    this.handleError(response);
                }, () => {
                    sub.unsubscribe();
                }
            );
        }).catch()
    }
    /**
     * Redirige al administrador a la pantalla de dashboard para el elemento seleccionado
     */
    gotoPromocion(idElemento: string) {
        this.router.navigate(['/promociones/detallePromo', idElemento]);
    }

    gotoMiembro(idElemento: string) {
        this.router.navigate(['/miembros/detalleMiembro/', idElemento]);
    }
    /**
     * Maneja el erro a nivel de componente
     */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}