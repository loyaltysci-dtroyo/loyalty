import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessagesService, ErrorHandlerService } from '../../utils/index';
import { RT_PROMOCIONES, RT_PROMOCIONES_LISTA_CATEGORIAS } from '../../utils/rutas';
import { PromocionCategoriaService, KeycloakService, I18nService } from '../../service/index';
import { Categoria } from '../../model/index';
import { AppConfig } from '../../app.config';
import { AuthGuard } from '../common/index';
import { PERM_ESCR_CATEGORIAS_PROMOCION } from '../common/auth.constants';

@Component({
    moduleId: module.id,
    selector: 'insertar-categoria',
    templateUrl: 'insertar-categoria.component.html',
    providers: [Categoria, PromocionCategoriaService]
})
/**
 * Flecha Roja Technologies oct-2016
 * Fernando Aguilar
 * Componente iencargado de isnertar categorias de mision
 */
export class InsertarCategoriaPromoComponent {

    public form: FormGroup;//validaciones
    public file_src: any;
    public file: any;
    public nombreValido: boolean//true cuando el nombre es valido
    public tienePermisosEscritura: boolean;
    constructor(
        public categoria: Categoria,
        public promoCategoriaService: PromocionCategoriaService,
        public router: Router,
        public authGuard: AuthGuard,
        public kc: KeycloakService,
        public msgService: MessagesService,
        private i18nService: I18nService
    ) {
        this.form = new FormGroup({
            "nombre": new FormControl('', Validators.required),
            "nombreInterno": new FormControl('', Validators.required),
            "descripcion": new FormControl('', Validators.required)
        });

        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS_PROMOCION).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.file_src = `${AppConfig.DEFAULT_IMG_URL}`;
    }

    mostrarModalEliminarCategoria(categoria) {
        if (this.tienePermisosEscritura) {
            this.kc.getToken().then(
                (tkn) => {
                    this.promoCategoriaService.token = tkn;
                    let sub = this.promoCategoriaService.eliminarCategoria(categoria.idCategoria).subscribe(
                        (data) => { }, (error) => { this.manejaError(error) }, () => { sub.unsubscribe(); }
                    );
                });
        }
    }
    /*
   * Inserta una categoría
   * Recibe: nada, toma la instancia de categoria que fuen llenada por el form 
   * Retorna: void, se encarga de insertar y reireccionar a la pagina e lista de categorias
   */
    insertarCategoria() {
        if (this.tienePermisosEscritura) {
            if (this.nombreValido) {
                this.categoria.fechaRegistro = new Date();
                if (this.categoria.imagen != null) {
                    this.categoria.imagen = this.categoria.imagen.split(",")[1];
                }
                this.msgService.showMessage({ detail: "Esta acción puede tardar unos segundos...", severity: "info", summary: "" });
                this.kc.getToken().then(
                    (tkn) => {
                        this.promoCategoriaService.token = tkn;
                        let sub = this.promoCategoriaService.insertarCategoria(this.categoria).subscribe(
                            () => {
                                this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                                let link = [RT_PROMOCIONES_LISTA_CATEGORIAS];
                                this.router.navigate(link);
                            }, (error) => {
                                this.manejaError(error);
                            },
                            () => { sub.unsubscribe(); }
                        );
                    });
            } else {

            }
        }
    }

    goBack() {
        this.router.navigate([RT_PROMOCIONES_LISTA_CATEGORIAS]);
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    establecerNombres() {
        if (this.categoria.nombre != "" && this.categoria.nombre != null) {
            let temp = this.categoria.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.categoria.nombreInterno = temp;
            this.verificarNombreInterno();
        }
    }

    verificarNombreInterno() {
        if (this.categoria.nombreInterno == null || this.categoria.nombreInterno == "") {
            this.nombreValido = false;
            return;
        }
        this.kc.getToken().then(
            (tkn) => {
                this.promoCategoriaService.token = tkn;
                let sub = this.promoCategoriaService.verificarNombreInterno(this.categoria.nombreInterno).subscribe(
                    (data) => {
                        this.nombreValido = data.text() == "false";
                        if (this.nombreValido == false) {
                            this.msgService.showMessage({ detail: "El nombre interno ya existe. Digite un nuevo nombre interno.", severity: "warn", summary: "" });
                        }
                    }, (error) => { this.manejaError(error) }, () => { sub.unsubscribe() }
                );
            });
    }

    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {

        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            // Obtener el (base64 de la imagen)
            img.src = this.categoria.imagen = event.target.result;
            // Redimensionar la imagen
            this.file_src = this.resize(img);
        }, false);

        reader.readAsDataURL(input.files[0]);


    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");

        // console.log("Size Before: " + img.src.length + " bytes");

        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");

        ctx.drawImage(img, 0, 0, width, height);

        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        // console.log("Size After:  " + dataUrl.length + " bytes");
        return dataUrl
    }
}