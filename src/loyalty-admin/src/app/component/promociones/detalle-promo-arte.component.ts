import { I18nService } from '../../service/i18n.service';
import * as Rutas from '../../utils/rutas';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { PromocionService } from '../../service/index';
import { Promocion } from '../../model/index';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { KeycloakService } from '../../service/index';
import { PERM_ESCR_PROMOCIONES } from '../common/auth.constants'
import { AuthGuard } from '../common/index';
/**
 * Flecha Roja Technologies oct-2016
 * Fernando Aguilar
 * Componente encargado de mostrar el editor para editar el arte(display) de la promocion 
 */
@Component({
    moduleId: module.id,
    selector: 'detalle-promo-arte-component',
    templateUrl: 'detalle-promo-arte.component.html'
})
export class DetallePromoArteComponent implements OnInit {
    public file_src: any;
    public file: any;
    public form: FormGroup;
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public subiendoImagen: boolean;
    constructor(public promo: Promocion, public kc: KeycloakService,private i18nService:I18nService,
        public promocionService: PromocionService, public authGuard: AuthGuard,
        public msgService: MessagesService) {
        this.form = new FormGroup({
            "encabezado": new FormControl("", Validators.required),
            "sub-encabezado": new FormControl("", Validators.required),
            "detalle": new FormControl("", Validators.required),
            "imagen": new FormControl("", Validators.required)
        });
        if (this.promo.indEstado == "BO" || this.promo.indEstado == "AR") {
            this.tienePermisosEscritura = false;
        } else {
            this.authGuard.canWrite(PERM_ESCR_PROMOCIONES).subscribe((result: boolean) => {
                this.tienePermisosEscritura = result;
            });
        }
        this.subiendoImagen = false;
    }

    ngOnInit() {
        this.cargarPromo();
    }
    /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
                objDestino[property] = objFuente[property];
        }
    }
    /**
     * Obtiene una promocion del sistema, almacena los datos de dicha promocion en la variable global this.promo
     */
    cargarPromo() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.getPromocion(this.promo.idPromocion).subscribe(
                    (data: Promocion) => {
                        this.copyValuesOf(this.promo,data)
                        this.file_src = this.promo.imagenArte;
                    },
                    (error) => { this.manejaError(error); },
                    () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /**
     * Encargado de actualizar la promocion, es usado para guardar los datos de display actuales en la base de datos
     * No recibe parametros, pero utiliza la instancia en this.promo para actualizar los datos de la promo
     */
    guardarDatos() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.promocionService.token = tkn;
                if(this.promo.imagenArte.split(",")[1]!=null){
                this.promo.imagenArte = this.promo.imagenArte.split(",")[1];
                }
                this.subiendoImagen = true;
                this.msgService.showMessage(this.i18nService.getLabels("info-uploading"));
                let sub = this.promocionService.updatePromocion(this.promo).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.cargarPromo();
                    }, (error) => {
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); this.subiendoImagen = false; }
                );
            });
    }
    /**
     * Maneja los errores, en este caso despliega un mensaje al usuario 
     * Toma como parametro un Response de http y lo envia al Service que maneja los errores
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {
        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            // Obtener el (base64 de la imagen)
            img.src =  this.file_src= this.promo.imagenArte = event.target.result;
            // Redimensionar la imagen
            // = this.resize(img);
        }, false);

        reader.readAsDataURL(input.files[0]);
    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");
        // console.log("Size Before: " + img.src.length + " bytes");
        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        // console.log("Size After:  " + dataUrl.length + " bytes");
        return dataUrl
    }
}
