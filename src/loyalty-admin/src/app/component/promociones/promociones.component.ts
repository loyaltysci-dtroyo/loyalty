import {Component}  from '@angular/core'; 
@Component({
    moduleId: module.id,
    selector:'promociones-component',
    templateUrl:'promociones.component.html'
})
/**
 * Flecha Roja Technologies set-2016
 * Fernando Aguilar
 * Componente general de promociones, es el padre de los componentes de 
 * detalle/insertar/lista y permite entre otros la comunicacion entre hijos
 */
export class PromocionesComponent  {
    constructor(){
        
    }
}