import { I18nService } from '../../service/i18n.service';
import * as Rutas from '../../utils/rutas';

import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthGuard } from '../common/index';
import { KeycloakService } from '../../service/index';
import { PERM_ESCR_PROMOCIONES } from '../common/auth.constants'
import { Promocion } from '../../model/index';
import { PromocionService } from '../../service/index';
import { SelectItem } from 'primeng/primeng';

/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar 
 * Componente encargado de mostrar los detalles de la promocion, permite la edicion de los atributos generales de la promo
 * si el usuario tiene permiso para escribir en este recurso
 */
@Component({
    moduleId: module.id,
    selector: 'detalle-promo-common-component',
    templateUrl: 'detalle-promo-common.component.html'
})
export class DetallePromoCommonComponent implements OnInit {
    public tags: string[];
    public form: FormGroup;//validaciones
    public itemsCodigo: SelectItem[] = [];//items de seleccion para 
    public nombreExiste: boolean;//true si el nombre interno de la promo es valido (unico)
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema

    constructor(
        public promo: Promocion, private i18nService: I18nService,
        public promocionService: PromocionService,
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService) {
        this.form = new FormGroup({
            "nombre": new FormControl('', Validators.required),
            "descripcion": new FormControl('', Validators.required),
            "nombreInterno": new FormControl({ value: '', disabled: true }, Validators.required),
            "tags": new FormControl(''),
            "urlOrCodigo": new FormControl('', Validators.required),
            "valorUrlOrCodigo": new FormControl('', Validators.required),
            "selectRespuesta": new FormControl('', Validators.required)
        });
        this.itemsCodigo=[...this.itemsCodigo,{ value: null, label: "Ninguno" }];
        this.itemsCodigo=[...this.itemsCodigo,{ value: "A", label: "URL" }];
        this.itemsCodigo=[...this.itemsCodigo,{ value: "B", label: "Código QR" }];
        this.itemsCodigo=[...this.itemsCodigo,{ value: "C", label: "ITF" }];
        this.itemsCodigo=[...this.itemsCodigo,{ value: "D", label: "EAN" }];
        this.itemsCodigo=[...this.itemsCodigo,{ value: "E", label: "Code 128" }];
        this.itemsCodigo=[...this.itemsCodigo,{ value: "F", label: "Code 39" }];
        this.tags = [];
        if (this.promo.indEstado == "BO" || this.promo.indEstado == "AR") {
            this.tienePermisosEscritura = false;
        } else {
            this.authGuard.canWrite(PERM_ESCR_PROMOCIONES).subscribe((result: boolean) => {
                this.tienePermisosEscritura = result;
            });
        }
    }
    ngOnInit() {
        this.obtenerPromo();//se obtienen los datos de la promocion 
    }

    /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    establecerNombres() {
        try {
            this.promo.descripcion = this.promo.nombre;
            let temp = this.promo.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.promo.nombreInterno = temp;
        } catch (e) { }
    }

    validarNombre() {
        if (this.promo.nombreInterno == "") { this.nombreExiste = false; return }
        this.kc.getToken().then(
            (tkn) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.validarNombre(this.promo.nombreInterno).subscribe(
                    (existeNombre: string) => {
                        if(existeNombre == "true"){
                            this.nombreExiste = true;
                        }else{
                            this.nombreExiste = false;
                        }
                    }, () => {

                    }, () => { sub.unsubscribe(); }
                );
            });
    }

    /**
     * Metodo encargado de guardar los detalles de la promocion en el sistema
     * Utiliza la instancia de promocion almacenada en this.promo
     */
    actualizarPromo() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                this.promo.tags = this.tags.join("\n");
                let sub = this.promocionService.updatePromocion(this.promo).subscribe(
                    () => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.obtenerPromo();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Obtiene los detalles de la promocion en el sistema
     */
    obtenerPromo() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.getPromocion(this.promo.idPromocion).subscribe(
                    (data: Promocion) => {
                        this.copyValuesOf(this.promo, data);
                        if (this.promo && this.promo.tags) {
                            this.tags = this.promo.tags.split("\n");
                        }
                        // this.promo = data;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }


}