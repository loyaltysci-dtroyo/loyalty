import { Component, OnInit, Renderer } from '@angular/core';
import { MenuItem, SelectItem } from 'primeng/primeng';
import { Response } from '@angular/http';
import { Miembro, Premio } from '../../model/index';
import { MiembroService, PremioService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_PREMIOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'premio-elegibles-miembro-component',
    templateUrl: 'premio-elegibles-miembros.component.html',
    providers: [MiembroService, Miembro]
})
/**
 * Jaylin Centeno
 * Componente que muestra el detalle de los miembros elegibles del premio
 */
export class PremioMiembroElegiblesComponent implements OnInit {

    public id: string; //Contiene el id del premio
    public listaMiembrosDisponibles: Miembro[]; //Lista con los miembros disponibles en el sistema
    public listaVacia: boolean = true; //Valida que la lista de disponibles no esta vacía
    public indAccion: string = "D"; //Permite determinar la acción, ya sea incluir o excluir
    public elegible: string = "I"; //Indica cual es el elemento elegible a mostrar
    public busqueda: string; //Búsqueda de miembros, guarda las palabras para realizar las búsquedas
    public listaSeleccion: Miembro[] = []; //Lista que contendra la seleccion de los miembros, para asociar o remover
    public listaRemover: Miembro[] = [];
    public lista: string[] = []; //Lista que contendra el id de los miembros, para asociar o remover del grupo
    public itemsAccion: SelectItem[] = []; //Items del dropdown, muestra las acciones de incluir o excluir 
    public itemsElegibles: SelectItem[] = []; //Items del dropdown, muestra los elementos de elegibles
    public idMiembro: string; //Contendra el id del miembro a eliminar
    public escritura: boolean; //Permite verificar permisos (true/false)
    public totalRecords: number; //Total de miembros en el sistema
    public cantidad: number = 0; //Cantidad de rows
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public cargandoDatos: boolean = true;
    public indicador: string;
    public eliminar: boolean;

    constructor(
        public premio: Premio,
        public miembroService: MiembroService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public premioService: PremioService,
        public router: Router,
        public msgService: MessagesService,
        public route: ActivatedRoute,
        public i18nService: I18nService
    ) {
        //Se obtienen las listas predeterminadas en el sistema 
        this.itemsElegibles = this.i18nService.getLabels("accion-elegibles-general");
        this.itemsAccion = this.i18nService.getLabels("tipos-elegibles-general");
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    /**
     * Método que se inicia al entrar por primera vez al componente
     */
    ngOnInit() {
        this.obtenerMiembrosDisponibles();
    }

    /**
     * Método que muestra un cuadro de dialogo para pedir una confirmación 
     * @param idMiembro 
     */
    dialogoConfirmar(idMiembro: string) {
        this.idMiembro = idMiembro;
        this.eliminar = true;
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerMiembrosDisponibles();
    }

    /**
     * Método para obtener la lista de los miembros según la acción 
     * Si la acción es incluir o excluir se obtendran los miembros que ya estan en los elegibles 
     * Si la acción es disponibles se obtendran los miembros que no están en los elegibles
     */
    obtenerMiembrosDisponibles() {
        if (this.indicador != this.indAccion) {
            this.cargandoDatos = true;
            this.indicador = this.indAccion;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.premioService.token = token;
                this.premioService.obtenerMiembrosElegiblesPremio(this.premio.idPremio, this.indAccion, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaMiembrosDisponibles = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaMiembrosDisponibles.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                )
            }
        );
    }

    /**
     * Método para agregar un miembro a los elegibles del premio 
     * Ya sea para incluirlo o excluirlo de los elegibles
     * @param idMiembro 
     */
    agregarMiembro(idMiembro: string) {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.premioService.token = token;
                let sub = this.premioService.agregarMiembro(this.premio.idPremio, idMiembro, this.elegible).subscribe(
                    (data) => {
                        if (this.elegible == "E") {
                            this.msgService.showMessage(this.i18nService.getLabels('general-exclusion-simple'));
                        } else {
                            this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                        }
                        this.obtenerMiembrosDisponibles();
                        if (this.listaSeleccion.length > 0) {
                            let index = this.listaSeleccion.findIndex(element => element.idMiembro == idMiembro);
                            this.listaSeleccion.splice(index, 1);
                        }
                    },
                    (error) => { this.manejaError(error); },
                );
            });
    }

    /**
     * Método para agregar la lista de miembros a los elegibles de los premios 
     * Ya sea para incluir o excluir la lista de los elegibles
     */
    agregarListaMiembros() {
        if (this.listaSeleccion.length >= 1) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.premioService.token = token;
                    this.listaSeleccion.forEach(miembro => {
                        this.lista = [...this.lista, miembro.idMiembro]
                    })
                    this.premioService.agregarListaMiembros(this.premio.idPremio, this.lista, this.elegible).subscribe(
                        (data) => {
                            if (this.elegible == "E") {
                                this.msgService.showMessage(this.i18nService.getLabels('general-exclusion-multiple'));
                            } else {
                                this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-multiple'));
                            }
                            this.listaSeleccion = [];
                            this.lista = [];
                            this.obtenerMiembrosDisponibles();
                        },
                        (error) => { this.manejaError(error); },
                    );
                }
            );
        } else {
            this.msgService.showMessage(this.i18nService.getLabels('warn-cantidad'));
        }

    }

    /**
     * Permite remover una lista de miembros de los incluidos/excluidos
     */
    removerListaMiembros() {
        if (this.listaRemover.length >= 1) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.premioService.token = token;
                    this.listaRemover.forEach(miembro => {
                        this.lista = [...this.lista, miembro.idMiembro]
                    })
                    this.premioService.eliminarListaMiembros(this.premio.idPremio, this.lista).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-multiple'));
                            this.obtenerMiembrosDisponibles();
                            this.listaRemover = [];
                            this.lista = [];
                        },
                        (error) => { this.manejaError(error); },
                    );
                }
            );
        } else {
            this.msgService.showMessage(this.i18nService.getLabels('warn-cantidad'));
        }
    }

    /**
     * Método para eliminar un miembro de la lista de los elegibles de un premio
     */
    eliminarMiembroLista() {
        this.eliminar = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (tkn: string) => {
                this.premioService.token = tkn;
                let sub = this.premioService.eliminarMiembro(this.premio.idPremio, this.idMiembro).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                        this.obtenerMiembrosDisponibles();
                        if (this.listaRemover.length > 0) {
                            let index = this.listaRemover.findIndex(element => element.idMiembro == this.idMiembro);
                            this.listaRemover.splice(index, 1);
                        }
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }

    /**
     * Redirección a componente padre de los elegibles
     */
    goBack() {
        let ruta = "premios/detallePremio/" + this.premio.idPremio + "/elegibles";
        let link = [ruta]
        this.router.navigate(link);
    }

    /**
     * Manejo de error en las respuestas 
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}