import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { Premio, Metrica, UnidadMedida } from '../../model/index';
import { PremioService, MetricaService, UnidadMedidaService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_PREMIOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'premio-detalle-component',
    templateUrl: 'premio-detalle.component.html',
    providers: [Metrica, UnidadMedida, UnidadMedidaService, MetricaService]
})

/**
 * Componente PremioDetalle para ver el detalle del premio
 */
export class PremioDetalleComponent implements OnInit {

    
    public id: string; //Id del premio
    public tipoPremioItem: SelectItem[]; //Lista para Dropdown del tipo de premio//
    public cargandoDatos: boolean = true;
    public noMostrar: boolean = false; //Validación del nombre interno
    public nombreExiste: boolean;
    public nombreTemp: string; //Guarda el nombre interno del premio
    public metricaBusqueda;
    public totalRecordsMetricas;
    public formPremio: FormGroup; // Form group para validar el formulario de premio
    public estado: string; //Contendra los estados del premio
    public escritura: boolean; //Permite verificar permisos (true/false)

    //----Unidades de medida -------//
    public listaUnidades: UnidadMedida[]; //Lista de las unidades de medida disponibles
    public unidadVacia: boolean; //Maneja si la lista de unidades esta vacía
    public verUnidades: boolean = false; //Permite desplegar el modal de unidades de medida
    public totalRecords: number; //Total de de categorías en el sistema
    public cantidad: number; //Cantidad de filas
    public filaDesplazamiento: number; //Desplazamiento de la primera fila    
    
    //------ Metricas ------//
    public listaMetricas: Metrica[]; //Lista de las métricas disponibles
    public metricaVacia: boolean; //Maneja si las metricas estan vacías
    public metricaItems: SelectItem[] = []; //Lista para desplegar en el Dropdown
    public listaBusqueda: string[] = [];
    public totalRecordsMetrica: number;
    public cantidadMetricas: number =5;
    public filaMetricas: number = 0;
    public nombreMetrica: string;
    public displayDialogMetrica: boolean;

    constructor(
        public premio: Premio,
        public unidad: UnidadMedida,
        public premioService: PremioService,
        public metricaService: MetricaService,
        public unidadService: UnidadMedidaService,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Validacion del formulario y datos por defecto
        this.formPremio = new FormGroup({
            'codPremio': new FormControl('', Validators.required),
            'nombre': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
            'despliegue': new FormControl('', Validators.required),
            'tipoPremio': new FormControl('', Validators.required),
            'metrica': new FormControl('', Validators.required),
            'valorMoneda': new FormControl('', Validators.required),
            'envio': new FormControl('', Validators.required),
            'sku': new FormControl(''),
            'code': new FormControl(''),
            'expira': new FormControl('', Validators.required)
        });

        //Se llena la lista para el tipo de premio
        this.tipoPremioItem = [];
        this.tipoPremioItem = this.i18nService.getLabels("premio-tipoPremio");
    }

    /**
     * Método que se inicia al entrar por primera vez al componente
     */
    ngOnInit() {
        this.mostrarDetalle();
        this.obtenerMetricas();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerUnidades();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadDataMetrica(event: any) {
        this.filaMetricas = event.first;
        this.cantidadMetricas = event.rows;
        this.obtenerMetricas();
    }

    /**
     * Llamado al seleccionar una métrica, asigna los valores y cierra el cuadro de dialogo
     * @param metrica 
     */
    seleccionarMetrica(metrica: Metrica){
        this.premio.idMetrica = metrica;
        this.nombreMetrica = metrica.nombre;
        this.displayDialogMetrica = false;
    }

    /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Método que se encarga traer el detalle del premio
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.premioService.token = token;
                this.premioService.obtenerPremioPorId(this.premio.idPremio).subscribe(
                    (data) => {
                        this.copyValuesOf(this.premio, data);
                        if (this.premio.unidadMedida != null) {
                            this.unidad = this.premio.unidadMedida;
                        }
                        this.nombreMetrica = this.premio.idMetrica.nombre;
                        this.nombreTemp = this.premio.nombreInterno;
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Se encarga de obtener las métricas solicitadas
     */
    obtenerMetricas() {
        let busqueda = "";
        busqueda = this.listaBusqueda.join(" ")
        let estado = ["P"];
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.metricaService.token = token;
                this.metricaService.buscarMetricas(estado,this.cantidadMetricas, this.filaMetricas, busqueda).subscribe(
                    (response: Response) => {
                        this.listaMetricas = response.json();
                        if (response.headers.get("Content-Range") != null) {
                                this.totalRecordsMetrica = + response.headers.get("Content-Range").split("/")[1];
                            }
                        if (this.listaMetricas.length > 0) {
                            this.metricaVacia = false;
                        } else {
                            this.metricaVacia = true;
                        }
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Se obtienen las unidades de medida que se encuentran en el sistema
     */
    obtenerUnidades() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.unidadService.token = token;
                let sub = this.unidadService.obtenerListaUnidades(this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaUnidades = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaUnidades.length > 0) {
                            this.unidadVacia = false;
                        } else {
                            this.unidadVacia = true;
                        }
                    },
                    (error) => this.manejaError(error),
                    () => sub.unsubscribe()
                )
            }
        )
    }

    /**
     * Llamado cuando se selecciona una medida 
     * @param unidad 
     */
    unidadSeleccionada(unidad: UnidadMedida) {
        this.premio.unidadMedida = unidad;
        this.verUnidades = false;
    }

    /**
     * Establece el nombre interno usando el nombre del premio
     */
    establecerNombres() {
        if (this.premio.nombre != "" && this.premio.nombre != null) {
            let temp = this.premio.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.premio.nombreInterno = temp;
            this.nombreTemp = temp;
            this.validarNombreInterno();
        }
    }

    /**
     * Método encargado de validar la existencia del nombre interno de premio
     */
    validarNombreInterno() {
        if (this.premio.nombreInterno == null || this.premio.nombreInterno.trim() == "") {
            this.nombreExiste = true;
            return;
        }
        if (this.premio.nombreInterno != this.nombreTemp) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.premioService.token = token;
                    this.premioService.verificarNombreInterno(this.premio.nombreInterno.trim()).subscribe(
                        (data) => {
                            this.nombreExiste = data;
                            if (this.nombreExiste) {
                                this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
                            }
                        }
                    );
                }
            );
        }
    }

    /**
     * Método que se encarga de actualizar la informacion del premio
     */
    actualizarPremio() {
        this.noMostrar = true;
        if (this.nombreExiste) {
            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
        } else {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.premioService.token = token;
                    this.premioService.editarPremio(this.premio).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                            this.mostrarDetalle();
                            this.noMostrar = false;
                        },
                        (error) => {
                            this.manejaError(error);
                            this.noMostrar = false;
                        }
                    );
                }
            );
        }
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
