import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Rutas from '../../../utils/rutas';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { CategoriaPremio,Premio } from '../../../model/index';
import { CategoriaPremioService, KeycloakService, I18nService } from '../../../service/index';
import { PERM_ESCR_PREMIO_CATEGORIA } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'categoria-premio-component',
    templateUrl: 'categoria-premio.component.html',
    providers: [CategoriaPremio, CategoriaPremioService,Premio]
})
/**
 * Jaylin Centeno
 * Componente utilizado para ver/manipular los detalles de la categoría 
 */
export class CategoriaPremioComponent implements OnInit {

    public id: string; //Id de la categoría de premio
    public items: MenuItem[]; //Representa los items del tabmenu
    public escritura: boolean; //verificación de permisos (true/false)
    public eliminar: boolean; //Habilita el cuadro de dialogo para eliminar la categoría
    public detalle = Rutas.RT_PREMIOS_CATEGORIA_INFORMACION; //Contendra la ruta para ver el detalle de la categoría
    public asignar = Rutas.RT_PREMIOS_CATEGORIA_ASINGAR; //Contendra la ruta para ver los premios que contiene esa categoría

    constructor(
        public categoria: CategoriaPremio,
        public categoriaService: CategoriaPremioService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Representa el id de la categoría pasado por la url 
        this.route.params.forEach((params) => { this.id = params['id'] });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIO_CATEGORIA).subscribe(
            (permiso: boolean) => {
                this.escritura = permiso;
            }
        );
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
     * Se obtiene el detalle de la categoría
     */ 
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.categoriaService.token = token;
                this.categoriaService.obtenerCategoriaPorId(this.id).subscribe(
                    (data) => {
                        this.categoria = data;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Se remueve una categoría de premio
     */ 
    removerCategoriaPremio() {
        this.eliminar = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.categoriaService.token = token;
                this.categoriaService.eliminarCategoriaPremio(this.id).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.goBack();
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    /**
     * Se redirecciona a la lista de categorías
     */ 
    goBack() {
        this.router.navigate([Rutas.RT_PREMIOS_CATEGORIA_LISTA]);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
