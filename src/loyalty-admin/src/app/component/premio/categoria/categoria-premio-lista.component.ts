import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoriaPremio, Premio } from '../../../model/index';
import { CategoriaPremioService, KeycloakService, I18nService } from '../../../service/index';
import { Http, Response } from '@angular/http';
import { PERM_ESCR_PREMIO_CATEGORIA } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import * as Rutas from '../../../utils/rutas'

@Component({
    moduleId: module.id,
    selector: 'categoria-premio-lista-component',
    templateUrl: 'categoria-premio-lista.component.html',
    providers: [CategoriaPremio, CategoriaPremioService, Premio]
})
/**
 * Jaylin Centeno
 * Componente que muestra los tipos de categorías de un premio 
 */
export class CategoriaPremioListaComponent implements OnInit {

    public eliminar: boolean; // Habilita el cuadro de dialogo para remover 
    public escritura: boolean; //Permite verificar permisos (true/false)
    public cargandoDatos : boolean = true; //Muestra la carga de datos

    //  --- Categoría ---  //
    public idCategoria: string; //Id de la categoría seleccionada
    public listaCategoria: CategoriaPremio[]; //Lista de categorías
    public listaVacia: boolean = true; //Para verificar si la lista de obtenida esta vacía
    public cantidad: number = 10; //Cantidad de rows
    public totalRecords: number; //Total de categorías de premios en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera filaDesplazamiento

    public busqueda: string = ""; //Palabras clave para la búsqueda
    public listaBusqueda: string[] = [];
    
    constructor(
        public categoria: CategoriaPremio,
        public categoriaService: CategoriaPremioService,
        public router: Router,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) { 
        //Permite saber si el usuario tiene permisos de escritura sobre este elemento
        this.authGuard.canWrite(PERM_ESCR_PREMIO_CATEGORIA).subscribe(
            (permiso: boolean) => { this.escritura = permiso; });
    }
    
    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.busquedaCategorias();
    }

    /**
     * Método para mostrar el cuadro de dialogo para eliminar un elemento
     */ 
    confirmar(idCategoria: string) {
        this.idCategoria = idCategoria;
        this.eliminar = true;
    }

    /**
     * Método utilizado para la carga de los elementos del datatable
     * onLazyLoad, permite la carga de los elementos en segmentos
     */
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaCategorias();
    }

    /**
     * Búsqueda de las categorías de premio según palabras claves
     */
    busquedaCategorias() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.categoriaService.token = token;
                this.categoriaService.busquedaCategorias(this.cantidad, this.filaDesplazamiento, this.busqueda.trim()).subscribe(
                    (response: Response) => {
                        this.listaCategoria = response.json();
                        if(response.headers.get("Content-Range") != null){
                           this.totalRecords =+ response.headers.get("Content-Range").split("/")[1];
                        }
                        //Se pregunta si la lista esta vacía
                        if (this.listaCategoria.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Método para navegar hasta el detalle de la categoría seleccionada
     */
    detalleInsignia(idCategoria: string) {
        this.router.navigate([Rutas.RT_PREMIOS_CATEGORIA_DETALLE, idCategoria]);
    }

    /**
     * Redirige a la pantalla de inserción de categoría
     */ 
    ir() {
        this.router.navigate([Rutas.RT_PREMIOS_CATEGORIA_INSERTAR]);
    }

    /**
     * Permite remover la categoría seleccionada
     */ 
    removerCategoria() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.categoriaService.token = token;
                this.categoriaService.eliminarCategoriaPremio(this.idCategoria).subscribe(
                    (data) => {
                        this.eliminar = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.filaDesplazamiento = 0;
                        let index = this.listaCategoria.findIndex(element => element.idCategoria == this.idCategoria);
                        this.listaCategoria.splice(index, 1);
                        this.listaCategoria = [...this.listaCategoria];
                    },
                    (error) => {
                        this.eliminar = false;
                        this.manejaError(error)
                    }
                );
            }
        );
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}