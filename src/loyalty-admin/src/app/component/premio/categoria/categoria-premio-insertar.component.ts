import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CategoriaPremio, Premio } from '../../../model/index';
import { CategoriaPremioService, KeycloakService, I18nService } from '../../../service/index';
import { PERM_ESCR_PREMIO_CATEGORIA } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import * as Rutas from '../../../utils/rutas'
import { AppConfig } from '../../../app.config';

@Component({
    moduleId: module.id,
    selector: 'categoria-insertar-component',
    templateUrl: 'categoria-premio-insertar.component.html',
    providers: [CategoriaPremio, Premio, CategoriaPremioService]
})

/**
 * Jaylin Centeno
 * Componente que permite insertar las categorías de un premio 
 */
export class CategoriaPremioInsertarComponent {

    public imagen: any; //Para manipular el archivo subido por el usuario
    public imagenCateg: any; //Guarda la dirección de la imagen por defecto
    public escritura: boolean; //Permite verificar permisos (true/false)
    public nombreExiste: boolean; //Validación del nombre interno
    public formCategoria: FormGroup; //Formulario
    public subiendoImagen: boolean = false; //Indica si existe una carga de imagen

    constructor(
        public categoria: CategoriaPremio,
        public categoriaService: CategoriaPremioService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Validaciones
        this.formCategoria = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'nombreInterno': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
        });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIO_CATEGORIA).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        this.imagenCateg = `${AppConfig.DEFAULT_IMG_URL}`;
    }

    /**
     * Se inserta una nueva categoría de premio
     */
    insertarCategoria() {
        if (this.nombreExiste) {
            this.msgService.showMessage(this.i18nService.getLabels("error-nombre-interno"));
        } else {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.categoriaService.token = token;
                    this.subiendoImagen = true;
                    let sub = this.categoriaService.insertarCategoriaPremio(this.categoria).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                            this.router.navigate([Rutas.RT_PREMIOS_CATEGORIA_DETALLE, data]);
                        },
                        (error) => {
                            this.manejaError(error);
                            this.subiendoImagen = false;
                        },
                        () => { sub.unsubscribe(); this.subiendoImagen = false; }
                    );
                }
            );
        }
    }

    /**
     * Permite establecer el nombre interno de la categoría
     */
    establecerNombres() {
        if (this.categoria.nombre != "" && this.categoria.nombre != null) {
            let temp = this.categoria.nombre;
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.categoria.nombreInterno = temp;
            this.validarNombre();
        }
    }

    /**
     * Valida la existencia del nombre interno
     */ 
    validarNombre() {
        if (this.categoria.nombreInterno == null && this.categoria.nombreInterno == "") {
            this.nombreExiste = true;
            return;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.verificarNombre(this.categoria.nombreInterno.trim()).subscribe(
                    (data) => {
                        this.nombreExiste = data;
                        if (this.nombreExiste) {
                            this.msgService.showMessage(this.i18nService.getLabels("error-nombre-interno"));
                        }
                    }
                );
            });
    }

    /**
     * Método para subir una imagen que representa la categoría
     */ 
    subirImagen() {
        //se obtiene el archivo subido
        this.imagen = document.getElementById("imagenCateg");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.imagenCateg = reader.result;
            let urlBase64 = this.imagenCateg.split(",");
            this.categoria.imagen = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /**
     * Método que permite regresar en la navegación
     */ 
    goBack() {
        this.router.navigate([Rutas.RT_PREMIOS_CATEGORIA_LISTA]);
    }
    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}