import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CategoriaPremio, Premio } from '../../../model/index';
import { CategoriaPremioService, PremioService, KeycloakService, I18nService } from '../../../service/index';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { PERM_ESCR_PREMIO_CATEGORIA, PERM_ESCR_PREMIOS, PERM_LECT_PREMIO_CATEGORIA } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'categoria-premio-asignar-component',
    templateUrl: 'categoria-premio-asignar.component.html',
    providers: [Premio, PremioService, CategoriaPremio, CategoriaPremioService]
})

/**
 * Jaylin Centeno
 * Componente utilizado para ver, remover y asociar las categorías con los premios
 */
export class CategoriaPremioAsignarComponent implements OnInit {

    public id: string; //Id de la categoría pasado por la url
    public escritura: boolean; //Permite verificar permisos (true/false)
    public escrituraPremio: boolean; //Permite verificar permisos (true/false)
    public lecturaCategoria: boolean; // Verifica permiso de lectura
    public cargandoDatos: boolean = true;
    public idPremio: string; //Id del premio seleccionado
    public listaSelec: boolean; //Permite saber si la seleccion es de tipo lista o no
    public eliminar: boolean; //Habilita el cuadro de dialogo para eliminar

    // --- Premios asignados --- //
    public listaPremios: Premio[] = []; //Lista de los premios con la categoría asignada
    public listaVacia: boolean = true; //Para verificar que la lista no este vacía
    public cantidad: number = 10; //Cantidad de rows
    public totalRecords: number; //Total de de premios asignados a esa categoría
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    // --- Premios no asignados --- //
    public listaTotalPremios: Premio[] = []; //Lista con los premios no asociados a la categorías
    public listaTotalVacia: boolean = true; //Para verificar si existen premios disponibles
    public cantidadPremios: number = 10; //Cantidad de filas
    public totalRecordsPremio: number; //Total de de premios disponibles
    public filaDesplazPremio: number = 0; //Desplazamiento de la primera fila

    //Lista que contendra la seleccion de los datatable (asociar o remover)
    public listaSeleccion: Premio[] = [];
    public listaRemover: Premio[] = [];
    public lista: string[] = []; //Lista que contendra el id de los premios, para asociar o remover de la categoría

    //Busqueda
    public estado: string[] = ["P"]; //Estado de los premios a mostrar P = publicado
    public busqueda: string = ""; //Palabras clave para la búsqueda
    public listaBusqueda: string[] = [];

    constructor(
        public premio: Premio,
        public premioService: PremioService,
        public categoria: CategoriaPremio,
        public categoriaService: CategoriaPremioService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Representa el id del grupo pasado por la url 
        this.route.parent.params.forEach((params: Params) => { this.id = params['id'] });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIO_CATEGORIA).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_ESCR_PREMIOS).subscribe((permiso: boolean) => {
            this.escrituraPremio = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_PREMIO_CATEGORIA).subscribe((permiso: boolean) => {
            this.lecturaCategoria = permiso;
            this.obtenerPremiosCategoria();
            this.obtenerPremiosNoCategoria();
        });
    }

    //Método que se ejecuta al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
        this.obtenerPremiosCategoria();
        this.obtenerPremiosNoCategoria();
    }
    /**
     * Permite acción según el elemento
     */
    premioSeleccionado(elemento: string, idPremio?: string) {
        if (elemento == 'true') {
            this.listaSelec = true;
        } else {
            this.idPremio = idPremio;
            this.listaSelec = false;
            
        }
        this.eliminar = true;
    }

    //Método para mostrar el detalle de la categoría
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.obtenerCategoriaPorId(this.id).subscribe(
                    (data) => {
                        this.categoria = data;
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Método utilizado para la carga de los elementos del datatable
    //onLazyLoad, permite la carga de los elementos en segmentos
    //paginación
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerPremiosCategoria();
    }

    loadPremio(event: any) {
        this.filaDesplazPremio = event.first;
        this.cantidadPremios = event.rows;
        this.obtenerPremiosNoCategoria();
    }

    //Método para obtener los premios que pertenecen a la categoría seleccionado
    obtenerPremiosCategoria() {
        if (this.lecturaCategoria) {
            this.busqueda = "";
            if (this.listaBusqueda.length > 0) {
                this.listaBusqueda.forEach(element => {
                    this.busqueda += element + " ";
                });
            }
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.categoriaService.token = token;
                    this.categoriaService.obtenerPremiosCategoria(this.id, this.cantidad, this.filaDesplazamiento, this.estado, [], this.busqueda.trim(), 'A').subscribe(
                        (response: Response) => {
                            //Se obtiene la lista de los premios disponibles
                            this.listaPremios = response.json();
                            //Se recibe el total de las categorías disponibles
                            if (response.headers.get("Content-Range") != null) {
                                this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                            }
                            if (this.listaPremios.length > 0) {
                                this.listaVacia = false;
                            }
                            else {
                                this.listaVacia = true;
                            }
                        },
                        (error) => this.manejarError(error)
                    )
                }
            );
        }
    }

    //Método que se encarga de traer los premios que no tienen asignada la categoría
    //Por el momento se obtienen todos los premios existentes
    obtenerPremiosNoCategoria() {
        if (this.lecturaCategoria) {
            this.busqueda = "";
            if (this.listaBusqueda.length > 0) {
                this.listaBusqueda.forEach(element => {
                    this.busqueda += element + " ";
                });
            }
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.categoriaService.token = token;
                    this.categoriaService.obtenerPremiosCategoria(this.id, this.cantidadPremios, this.filaDesplazPremio, this.estado, [], this.busqueda.trim(), 'D').subscribe(
                        (response: Response) => {
                            //Se obtiene la lista de premios
                            this.listaTotalPremios = response.json();
                            //Se recibe el total de las categorías disponibles
                            if (response.headers.get("Content-Range") != null) {
                                this.totalRecordsPremio = + response.headers.get("Content-Range").split("/")[1];
                            }
                            if (this.listaTotalPremios.length > 0) {
                                this.listaTotalVacia = false;
                            } else {
                                this.listaTotalVacia = true;
                            }
                        },
                        (error) =>
                            this.manejarError(error)
                    );
                });
        }
    }

    //Método para crear una lista con ids de premios para asignarles una categoría  
    crearListaSeleccion() {
        //Lista selección es la lista generada cuando hay una selección multiple de premios en la tabla
        if (this.listaSeleccion.length > 0) {
            this.listaSeleccion.forEach(premio => {
                this.lista = [...this.lista, premio.idPremio]
            })
        }
    }

    //Método para crear una lista con ids de premios para remover una categoría
    crearListaRemover() {
        //Lista remover es la lista generada cuando hay una selección multiple de premios en la tabla
        if (this.listaRemover.length > 0) {
            this.listaRemover.forEach(premio => {
                this.lista = [...this.lista, premio.idPremio]
            })
        }
    }

    //Método que permite asignar la categoría a un solo premio
    asignarCategoriaPremio(idPremio: string) {
        if (this.escritura && this.escrituraPremio) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.categoriaService.token = token;
                    this.categoriaService.asignarPremio(this.id, idPremio).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                            this.obtenerPremiosNoCategoria();
                            this.obtenerPremiosCategoria();
                            this.sacarPremiosNoCategoria("premio");
                            if (this.listaSeleccion.length > 0) {
                                let index = this.listaSeleccion.findIndex(element => element.idPremio == idPremio);
                                this.listaSeleccion.splice(index, 1);
                            }
                        },
                        (error) => this.manejarError(error)
                    );
                });
        }
    }

    //Método para asignar a una lista de premios una categoría 
    agregarListaPremios() {
        if (this.escritura && this.escrituraPremio) {
            //Es necesario preguntar si se selecciono algún cliente, para evitar que la lista vaya nula
            if (this.listaSeleccion.length > 0) {
                this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                    (token: string) => {
                        this.categoriaService.token = token;
                        this.crearListaSeleccion();
                        this.categoriaService.asignarLista(this.id, this.lista).subscribe(
                            (data) => {
                                this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-multiple"));
                                this.sacarPremiosNoCategoria("lista");
                                this.lista = [];
                                this.listaSeleccion = [];
                                this.obtenerPremiosNoCategoria();
                                this.obtenerPremiosCategoria();
                            },
                            (error) => this.manejarError(error)
                        );
                    }
                );
            } else {
                this.msgService.showMessage(this.i18nService.getLabels("warn-cantidad"));
            }
        }
    }

    //Método para remover la asignación de una categoría a un premio
    removerPremio() {
        if (this.escritura && this.escrituraPremio) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.categoriaService.token = token;
                    this.categoriaService.removerPremio(this.id, this.idPremio).subscribe(
                        (data) => {
                            this.sacarPremiosCategoria("premio");
                            this.msgService.showMessage(this.i18nService.getLabels("general-exclusion-simple"));
                            if (this.listaRemover.length > 0) {
                                let index = this.listaRemover.findIndex(element => element.idPremio == this.idPremio);
                                this.listaRemover.splice(index, 1);
                            }
                            this.obtenerPremiosCategoria();
                            this.obtenerPremiosNoCategoria();
                            this.eliminar = false;
                        },
                        (error) => { this.manejarError(error); this.eliminar = false; }
                    );
                }
            );
        }
    }

    //Método para remover la asignación de una categoría a una lista de premios 
    removerListaPremios() {
        if (this.escritura && this.escrituraPremio) {
            //Es necesario preguntar si se selecciono algún cliente, para evitar que la lista vaya nula
            if (this.listaRemover.length > 0) {
                this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                    (token: string) => {
                        this.crearListaRemover();
                        this.categoriaService.token = token;
                        this.categoriaService.removerLista(this.lista, this.id).subscribe(
                            (data) => {
                                this.msgService.showMessage(this.i18nService.getLabels("general-exclusion-multiple"));
                                this.sacarPremiosCategoria("lista");
                                this.lista = [];
                                this.listaRemover = [];
                                this.eliminar = false;
                                this.obtenerPremiosCategoria();
                                this.obtenerPremiosNoCategoria();
                            },
                            (error) => { this.manejarError(error); this.eliminar = false; }
                        );
                    }
                );
            } else {
                this.msgService.showMessage(this.i18nService.getLabels("warn-cantidad"));
            }
        }
    }

    //Método que remueve de la tabla de premios sin categoría los premios seleccionados
    // para asignarles la categoría 
    sacarPremiosNoCategoria(tipo: string) {
        let listaTemporal = this.listaTotalPremios;
        let posicion;
        if (this.listaTotalPremios.length >= 0) {
            if (tipo == "lista") {
                this.lista.forEach(premio => {
                    let index = this.listaTotalPremios.findIndex(element => element.idPremio == premio)
                    this.listaTotalPremios.splice(index, 1);
                });
            } else {
                let index = this.listaTotalPremios.findIndex(element => element.idPremio == this.idPremio)
                this.listaTotalPremios.splice(index, 1);
            }
        }
    }

    //Método que remueve de la tabla de premios con categoría los premios seleccionados
    // para removerles la categoría 
    sacarPremiosCategoria(tipo: string) {
        let listaTemporal = this.listaPremios;
        let posicion;
        if (this.listaPremios.length >= 0) {
            if (tipo == "lista") {
                this.lista.forEach(premio => {
                    let index = this.listaPremios.findIndex(element => element.idPremio == premio)
                    this.listaPremios.splice(index, 1);
                });
            } else {
                let index = this.listaPremios.findIndex(element => element.idPremio == this.idPremio)
                this.listaPremios.splice(index, 1);
            }
        }
    }

    // Método para regresar al detalle de la categoría 
    goBack() {
        let link = ['premios/detalleCategoria', this.id];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}