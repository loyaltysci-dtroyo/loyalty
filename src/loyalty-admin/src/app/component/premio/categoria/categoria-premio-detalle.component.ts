import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CategoriaPremio, Premio } from '../../../model/index';
import { CategoriaPremioService, KeycloakService, I18nService } from '../../../service/index';
import { PERM_ESCR_PREMIO_CATEGORIA } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'categoria-premio-detalle-component',
    templateUrl: 'categoria-premio-detalle.component.html',
    providers: [Premio, CategoriaPremio, CategoriaPremioService]
})
/**
 * Jaylin Centeno
 * Componente utilizado para ver/manipular la información de la categoría 
 */
export class CategoriaPremioDetalleComponent implements OnInit {

    public escritura: boolean; //Permite verificar permisos (true/false)
    public nombreExiste: boolean; //Validación del nombre interno
    public subiendoImagen: boolean = false; //Indica si existe una carga de imagen
    public cargandoDatos: boolean = true;

    public id: string; //Id de la categoría recibida como parametro en la url
    public formCategoria: FormGroup; //Formulario
    public imagen: any; //Contendra el archivo subido por el usuario
    public imagenCateg: any; //Contendra la dirección
    public nombreInterno: string; //Nombre interno actual de la categoría

    constructor(
        public categoria: CategoriaPremio,
        public categoriaServicio: CategoriaPremioService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Representa el id del grupo pasado por la url 
        this.route.parent.params.forEach((params: Params) => { this.id = params['id'] });
        //Validación del formulario
        this.formCategoria = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'nombreInterno': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
        });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIO_CATEGORIA).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    //Método que se inicia solo una vez, al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
     * Método que llama al servicio para obtener el detalle de la categoría
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.categoriaServicio.token = token;
                this.categoriaServicio.obtenerCategoriaPorId(this.id).subscribe(
                    (data) => {
                        this.categoria = data;
                        this.imagenCateg = this.categoria.imagen;
                        this.nombreInterno = this.categoria.nombreInterno;
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    /**
     * Método para subir una imagen que representa la categoría
     */
    subirImagen() {
        //se obtiene el archivo subido
        this.imagen = document.getElementById("imagenCateg");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.imagenCateg = reader.result;
            let urlBase64 = this.imagenCateg.split(",");
            this.categoria.imagen = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }
    /**
     * Permite establecer el nombre interno de la categoría
     */
    establecerNombres() {
        if (this.categoria.nombre != "" && this.categoria.nombre != null) {
            let temp = this.categoria.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.categoria.nombreInterno = temp;
            this.validarNombreInterno();
        }
    }

    /**
     * Valida la existencia del nombre interno
     */ 
    validarNombreInterno() {
        if (this.categoria.nombreInterno == "" || this.categoria.nombreInterno == null) {
            this.nombreExiste = true;
            return;
        }
        if (this.nombreInterno == this.categoria.nombreInterno) {
            this.actualizarCategoria();
        } else {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.categoriaServicio.token = token;
                    this.categoriaServicio.verificarNombre(this.categoria.nombreInterno.trim()).subscribe(
                        (data) => {
                            this.nombreExiste = data;
                            if (this.nombreExiste) {
                                this.msgService.showMessage(this.i18nService.getLabels("error-nombre-interno"));
                            }
                        }
                    );
                }
            );
        }
    }

    /**
     * Permite modificar la entidad de categoría de premio
     */
    actualizarCategoria() {
        if (this.nombreExiste) {
            this.msgService.showMessage(this.i18nService.getLabels("error-nombre-interno"));
        } else {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.categoriaServicio.token = token;
                    this.subiendoImagen = true;
                    let sub = this.categoriaServicio.editarCategoriaPremio(this.categoria).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                            this.mostrarDetalle();
                        },
                        (error) => {
                            this.manejarError(error);
                            this.subiendoImagen = false;
                        },
                        () => { sub.unsubscribe(); this.subiendoImagen = false; }
                    );
                }
            );
        }
    }

    // Método para regresar a la lista de categorías
    goBack() {
        let link = ['/premios/listaCategoriasPremio'];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}