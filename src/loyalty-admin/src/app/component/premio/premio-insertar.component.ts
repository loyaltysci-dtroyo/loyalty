import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';

import { Premio, Metrica } from '../../model/index';
import { PremioService, MetricaService, KeycloakService, I18nService } from '../../service/index';

import { PERM_ESCR_PREMIOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'premio-insertar-component',
    templateUrl: 'premio-insertar.component.html',
    providers: [Premio, PremioService, Metrica, MetricaService]

})

/**
 * Componente que permite la inserción de nuevos premios
 */
export class PremioInsertarComponent implements OnInit {

    //Listas para Dropdown//
    public tipoPremioItem: SelectItem[];
    // Form group para validar el formulario de premio
    public formPremio: FormGroup;
    //Permite verificar permisos (true/false)
    public escritura: boolean;
    //Validación del nombre interno
    public nombreExiste: boolean;
    public metricaBusqueda;
    public totalRecordsMetricas;

    //------ Metricas ------//
    public metrica: Metrica;
    public listaMetricas: Metrica[]; //Lista de las métricas disponibles
    public metricaVacia: boolean; //Maneja si las metricas estan vacías
    public metricaItems: SelectItem[] = []; //Lista para desplegar en el Dropdown
    public listaBusqueda: string[] = []; //Guarda los críterios de búsqueda
    public totalRecordsMetrica: number; //Total de elementos
    public cantidadMetricas: number = 5; //Cantidad de rows
    public filaMetricas: number = 0; //Desplazamiento de la primera fila
    public nombreMetrica: string; //Nombre a mostrar
    public displayDialogMetrica: boolean; //Usado para mostrar/ocultar el cuadro de dialog

    constructor(
        public premio: Premio,
        public premioService: PremioService,
        public metricaService: MetricaService,
        public router: Router,
        public msgService: MessagesService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Validación del formulario y datos por defecto
        this.formPremio = new FormGroup({
            'codPremio': new FormControl('', Validators.required),
            'nombre': new FormControl('', Validators.required),
            'despliegue': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
            'tipoPremio': new FormControl('', Validators.required),
            'metrica': new FormControl('', Validators.required),
            'valorMoneda': new FormControl('', Validators.required),
            'envio': new FormControl('false', Validators.required),
            'expira': new FormControl('', Validators.required)
        });

        this.tipoPremioItem = [];
        //Obtiene la lista predeterminada de tipo de premio
        this.tipoPremioItem = this.i18nService.getLabels("premio-tipoPremio");
    }

    //Se llama al iniciarse el componente
    ngOnInit() {
        this.obtenerMetricas();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadDataMetrica(event: any) {
        this.filaMetricas = event.first;
        this.cantidadMetricas = event.rows;
        this.obtenerMetricas();
    }

    /**
     * Llamado cuando se selecciona una métrica
     * Asigna los valores necesarios y oculta el cuadro de dialogo
     * @param metrica 
     */
    seleccionarMetrica(metrica: Metrica) {
        this.premio.idMetrica = metrica;
        this.nombreMetrica = metrica.nombre;
        this.displayDialogMetrica = false;
    }

    /**
     * Método que obtienen la lista de todas las metricas según los críterios de búsqueda
     */
    obtenerMetricas() {
        let busqueda = "";
        busqueda = this.listaBusqueda.join(" ")
        let estado = ["P"];
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.metricaService.token = token;
                this.metricaService.buscarMetricas(estado, this.cantidadMetricas, this.filaMetricas, busqueda).subscribe(
                    (response: Response) => {
                        this.listaMetricas = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecordsMetrica = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaMetricas.length > 0) {
                            this.metricaVacia = false;
                        } else {
                            this.metricaVacia = true;
                        }
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Establece el nombre interno del premio utilizando el nombre
     */
    establecerNombres() {
        if (this.premio.nombre != "" && this.premio.nombre != null) {
            let temp = this.premio.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.premio.nombreInterno = temp;
            this.validarNombreInterno();
        }
    }

    /**
     * Método encargado de validar la existencia del nombre interno del premio
     */
    validarNombreInterno() {
        if (this.premio.nombreInterno == null && this.premio.nombreInterno.trim() == "") {
            this.nombreExiste = true;
            return;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.premioService.token = token;
                this.premioService.verificarNombreInterno(this.premio.nombreInterno.trim()).subscribe(
                    (data) => {
                        this.nombreExiste = data;
                        if (this.nombreExiste) {
                            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
                        }
                    }
                );
            }
        );
    }

    /**
     * Hace el llamado al servicio para guardar el premio configurado
     */
    insertarPremio() {
        //Se pregunta por la existencia del nombre, hasta que lo cambie se puede proceder
        if (this.nombreExiste) {
            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
        } else {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.premioService.token = token;
                    this.premioService.insertarPremio(this.premio).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                            this.premio.idPremio = data;
                            //Redirección al detalle del premio
                            let link = ['premios/detallePremio', this.premio.idPremio];
                            this.router.navigate(link);
                        },
                        (error) => {
                            this.manejaError(error);
                        }
                    );
                }
            );
        }
    }

    /**
     * Redirección a la página de lista de premios
     */
    goBack() {
        let link = ['premios'];
        this.router.navigate(link);
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
