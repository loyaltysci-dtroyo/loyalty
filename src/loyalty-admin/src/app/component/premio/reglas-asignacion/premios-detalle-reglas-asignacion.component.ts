import { ReglaAsignacionPremioService } from '../../../service/regla-asignacion-premio.service';
import { ReglaAsignacionPremio, Metrica, Nivel, GrupoNivel } from '../../../model/index';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { KeycloakService, I18nService, NivelService } from '../../../service/index';
import { AuthGuard } from '../../common/index';
import { MessagesService, ErrorHandlerService } from '../../../utils/index';
import { PERM_ESCR_PREMIOS } from '../../common/auth.constants';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';

@Component({
    moduleId: module.id,
    selector: 'premios-detalle-reglas-asignacion',
    templateUrl: 'premios-detalle-reglas-asignacion.component.html',
    providers: [ReglaAsignacionPremioService, NivelService, Nivel]
})

/**
 * Componente utilizado para crear reglas de asignación de premio
 */
export class PremiosDetalleReglasAsignacionComponent implements OnInit {

    public regla: ReglaAsignacionPremio;
    private idPremio: string;
    public permisoEscritura: boolean;
    public listaReglas: ReglaAsignacionPremio[];
    public metrica: Metrica;
    public grupoSeleccionado: GrupoNivel;
    public displayModalEliminar: boolean;
    public itemsGruposTier: SelectItem[];
    public itemsNiveles: SelectItem[];
    public itemsOperador: SelectItem[];
    public itemsOperadoresRegla: SelectItem[];
    public itemsTiposRegla: SelectItem[];
    public itemsCalendarizacion: SelectItem[];
    public itemsFechaIndUltimo: SelectItem[];
    public displayModal: boolean;
    public indAccion: string;

    constructor(
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        private activatedroute: ActivatedRoute,
        public kc: KeycloakService,
        private i18nService: I18nService,
        private reglaAsignacionPremioService: ReglaAsignacionPremioService,
        private nivelService: NivelService
    ) {

    }

    ngOnInit() {
        this.displayModal = false;
        this.itemsOperadoresRegla = [];
        //Valores predeterminados en el sistema
        this.itemsOperadoresRegla=[...this.itemsOperadoresRegla,{ "label": "AND", "value": "A" }, { "label": "OR", "value": "O" }];
        this.itemsCalendarizacion = this.i18nService.getLabels("segmentos-listaPeriodoTiempo");
        this.itemsTiposRegla = this.i18nService.getLabels("reglas-asignacion-premio-tipo-regla");
        this.itemsOperador = this.i18nService.getLabels("segmentos-operadoresNumericos");
        this.itemsFechaIndUltimo = this.i18nService.getLabels("reglas-asignacion-premio-fecha-calendarizacion");
        this.regla = new ReglaAsignacionPremio();

        //Obtiene el id de la ruta padre
        this.activatedroute.parent.params.subscribe((params: { [key: string]: any }) => {
            this.idPremio = params["id"];
        });
        // Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIOS).subscribe((permiso: boolean) => {
            this.permisoEscritura = permiso;
        });
        this.obtenerReglasAsignacion();

    }
    resetGrupoSeleccionado() {
        this.grupoSeleccionado = undefined;
    }
    /**
     * Obtiene la lista de grupos de tier para poblar el drop
     */
    obtenerGruposTier() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.nivelService.token = tkn;
                let sub = this.nivelService.obtenertListaGrupoNivel().subscribe(
                    (data: GrupoNivel[]) => {
                        this.itemsGruposTier = data.map((element: GrupoNivel) => { return { label: element.nombre, value: element.idGrupoNivel } });
                        this.itemsGruposTier=[...this.itemsGruposTier,{ label: "Ninguno", value: undefined }];
                    },
                    (error) => {
                        this.handleError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
   * Obtiene la lista de tiers para un nivel dado, es llamado cuando el drop de grupo de tier cambia
   */
    obtenerListaTier(event) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.nivelService.token = tkn;
                let sub = this.nivelService.obtenerListaNiveles(event.value ? event.value : event).subscribe(
                    (data: Nivel[]) => {
                        this.itemsNiveles = data.map((element: Nivel) => { return { label: element.nombre, value: element.idNivel } });
                    },
                    (error) => {
                        this.handleError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /**
     * Obtiene la lista de reglas de asignacion de premio
     */
    obtenerReglasAsignacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaAsignacionPremioService.token = tkn;
                let sub = this.reglaAsignacionPremioService.getReglasAsignacionPremio(this.idPremio).subscribe(
                    (data) => {
                        this.listaReglas = data;
                        this.displayModalEliminar = false;
                    },
                    (error) => {
                        this.handleError(error);
                        this.displayModalEliminar = false;
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * inserta una regla de asignacion de premio
     */
    insertarReglasAsignacion(regla: ReglaAsignacionPremio) {
        if (this.regla.tier && !this.regla.tier.idNivel) {
            this.regla.tier = undefined;
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaAsignacionPremioService.token = tkn;
                let sub = this.reglaAsignacionPremioService.createReglaAsignacionPremio(this.idPremio, this.regla).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.regla = new ReglaAsignacionPremio();
                        this.obtenerReglasAsignacion();
                        this.displayModal = false;
                        this.regla = new ReglaAsignacionPremio();
                    },
                    (error) => {
                        this.regla = new ReglaAsignacionPremio();
                        this.obtenerReglasAsignacion();
                        this.handleError(error);
                        this.displayModal = false;
                        this.regla = new ReglaAsignacionPremio();
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Actualiza una relga de asignacion de premio
     */
    actualizarReglaAsignacion(regla: ReglaAsignacionPremio) {
        if (this.regla.tier && !this.regla.tier.idNivel) {
            this.regla.tier = undefined;
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaAsignacionPremioService.token = tkn;
                let sub = this.reglaAsignacionPremioService.updateReglaAsignacionPremio(this.idPremio, this.regla).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.regla = new ReglaAsignacionPremio();
                        this.obtenerReglasAsignacion();
                        this.displayModal = false;
                        this.regla = new ReglaAsignacionPremio();
                    },
                    (error) => {
                        this.regla = new ReglaAsignacionPremio();
                        this.obtenerReglasAsignacion();
                        this.handleError(error);
                        this.displayModal = false;
                        this.regla = new ReglaAsignacionPremio();
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
    * Actualiza una relga de asignacion de premio
    */
    eliminarReglaAsignacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaAsignacionPremioService.token = tkn;
                let sub = this.reglaAsignacionPremioService.deleteReglaAsignacionPremio(this.idPremio, this.regla.idRegla).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                        this.obtenerReglasAsignacion();
                        this.regla = new ReglaAsignacionPremio();
                    },
                    (error) => {
                        this.handleError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    mostrarModalEliminar(regla: ReglaAsignacionPremio) {
        this.regla = regla;
        this.displayModalEliminar = true;
    }
    mostrarModalInsertar() {
        this.obtenerGruposTier();
        this.indAccion = "I";
        this.displayModal = true;
    }
    mostrarModalEditar(regla: ReglaAsignacionPremio) {
        this.obtenerGruposTier();
        this.regla = regla;
        if (regla && regla.tier) {
            this.grupoSeleccionado = regla.tier.grupoNiveles;
            this.obtenerListaTier(this.grupoSeleccionado.idGrupoNivel);
        } else {
            this.grupoSeleccionado = undefined;
        }
        this.indAccion = "E";
        this.displayModal = true;
    }
    /**
        * Maneja el erro a nivel de componente
        */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}