import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';

import { Premio } from '../../model/index';
import { PremioService, KeycloakService, I18nService } from '../../service/index';

import { PERM_ESCR_PREMIOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'premio-elegibles-component',
    templateUrl: 'premio-elegibles.component.html',
})

/**
 * Componente padre para los elegibles de premio
 */
export class PremioElegiblesComponent {

    public id: string; //Contiene el id del premio
    public elegible: string; //Indica cual es el elemento elegible a mostrar
    public itemsElegibles: SelectItem[]; //Items del dropdown, muestra los elementos de elegibles

    constructor(
        public premio: Premio,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public premioService: PremioService,
        public router: Router,
        public msgService: MessagesService,
        public route: ActivatedRoute,
        public i18nService: I18nService
    ) {
        this.itemsElegibles = [];
        //Se obtiene la lista predeterminada de los elegibles
        this.itemsElegibles = this.i18nService.getLabels("elegibles-general");
    }

    /**
     * Método que muestra la definición de elegibles según el elementos seleccionado
     */
    definicionElegibles() {
        let ruta = "premios/detallePremio/" + this.premio.idPremio;
        if (this.elegible == 'M') {
            ruta += "/miembros";
        }
        else if (this.elegible == 'S') {
            ruta += "/segmentos";
        }

        let link = [ruta];
        this.router.navigate(link);
    }
    /**
     * Manejo de error en las transacciones
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }


}