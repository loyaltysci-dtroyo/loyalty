import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Response, Headers } from '@angular/http';
import { ConfirmationService } from 'primeng/primeng';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { UnidadMedida } from '../../../model/index';
import { UnidadMedidaService, KeycloakService, I18nService } from '../../../service/index';
import * as Rutas from '../../../utils/rutas';
import { AuthGuard } from '../../common/index';
import { PERM_ESCR_PREMIOS } from '../../common/auth.constants';

@Component({
    moduleId: module.id,
    selector: 'unidad-medida.component',
    templateUrl: 'unidad-medida.component.html',
    providers: [UnidadMedida, UnidadMedidaService, ConfirmationService],
})
/**
 * Jaylin Centeno
 * Componente que permite crear, ver y eliminar de las unidades de medida
 */
export class UnidadMedidaComponent {

    public escritura: boolean; //Permiso de escritura sobre premio
    public editar: boolean = false; //Permite mostrar el modal para agregar/editar unidad de medida
    public accion: string; //Acción sobre la unidad, agregar o editar
    public cargandoDatos: boolean = true; //Muestra la carga de datos
    public eliminar: boolean; //Habilita el cuadro de dialogo para eliminar
    public subiendoImagen: boolean = false;
    public formUnidad: FormGroup; //Form group para validar la inserción/edición de una unidad de medida

    public listaUnidades: UnidadMedida[] = []; //Lista de las unidades de medida existentes
    public listaVacia: boolean = true; //Para verificar si la lista esta vacia 
    public idUnidad: string; //Valores de la metrica seleccionada
    public cantidad: number = 10; //Cantidad de rows
    public totalRecords: number; //Total de unindades en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    public busqueda: string = ""; //Palabras clave de búsqueda
    public listaBusqueda: string[] = [];

    constructor(
        public unidad: UnidadMedida,
        public unidadService: UnidadMedidaService,
        public router: Router,
        public keycloakService: KeycloakService,
        public confirmationService: ConfirmationService,
        public msgService: MessagesService,
        public i18nService: I18nService,
        public authGuard: AuthGuard
    ) {

        //Validación del forms
        this.formUnidad = new FormGroup({
            'descripcion': new FormControl('', Validators.required)
        });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    /**
     * Método que se inicia al entrar por primera vez al componente
     */
    ngOnInit() {
        this.obtenerListaUnidades();
    }

    /**
     * Método llamado para la carga la lista de manera perezosa
     */
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
    }

    /**
     * Método para mostrar el dialogo de confirmación
     */
    confirmar(idUnidad: string) {
        this.idUnidad = idUnidad;
        this.eliminar = true;
    }

    /**
     * Recibe el id de la unidad y la acción a ejecutar
     */
    unidadSeleccionada(unidad: UnidadMedida) {
        this.unidad = unidad;
        this.formUnidad.setValue({ descripcion: this.unidad.descripcion });
        this.accion = "editar";
        this.editar = true;
    }
    /**
     * Según la acción se invoca al método correspondiente
     */
    accionUnidad() {
        if (this.accion == "editar") {
            this.editarUnidad();
        } else {
            this.agregarUnidad();
        }
    }

    /**
     * Se cancela la edicion de la unidad y se hace un reset
     */
    cancelarAccion() {
        this.editar = false;
        this.formUnidad.reset();
        this.accion = "insertar"
    }
    /**
     * Se obtiene las unidades de medidas
     */
    obtenerListaUnidades() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.unidadService.token = token;
                //Se carga la lista con los valores que provee el servicio
                let sub = this.unidadService.obtenerListaUnidades(this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaUnidades = response.json();
                        if (this.listaUnidades.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error),
                    () => sub.unsubscribe()
                );
            }
        );
    }

    /**
     * Se realiza la búsqueda de unidades de medida usando varios criterios
     */
    busquedaUnidades() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.unidadService.token = token;
                this.unidadService.busquedaUnidades(this.busqueda, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaUnidades = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaUnidades.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    /**
     * Se edita los datos de la entidad de medida
     */
    editarUnidad() {
        if (this.formUnidad.get('descripcion').value.trim() != "") {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.unidadService.token = token;
                    this.unidad.descripcion = this.formUnidad.get('descripcion').value;
                    let sub = this.unidadService.editarUnidad(this.unidad).subscribe(
                        (data) => {
                            this.editar = false;
                            this.accion = "insertar";
                            this.formUnidad.reset();
                            this.obtenerListaUnidades();
                            this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        },
                        (error) => {
                            this.manejaError(error);
                            this.editar = false;
                            this.accion = "insertar";
                            this.formUnidad.reset();
                        },
                        () => { sub.unsubscribe(); }
                    );
                }
            );
        } else { this.msgService.showMessage(this.i18nService.getLabels("warn-dato-valido")); }
    }

    /**
     * Se inserta una nueva unidad de medida en el sistema
     */
    agregarUnidad() {
        if (this.formUnidad.get('descripcion').value.trim() != "") {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.unidadService.token = token;
                    this.unidad.descripcion = this.formUnidad.get('descripcion').value;
                    let sub = this.unidadService.insertarUnidad(this.unidad.descripcion).subscribe(
                        (data) => {
                            this.editar = false;
                            this.accion = "insertar";
                            this.formUnidad.reset();
                            this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                            this.obtenerListaUnidades();
                        }, (error) => {
                            this.manejaError(error);
                            this.editar = false;
                            this.accion = "insertar";
                            this.formUnidad.reset();
                        },
                        () => { sub.unsubscribe(); }
                    );
                }
            );
        } else { this.msgService.showMessage(this.i18nService.getLabels("warn-dato-valido")); }
    }

    /**
     * Se remueven las unidades de medida
     */
    eliminarUnidad() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.unidadService.token = token;
                this.unidadService.eliminarUnidad(this.idUnidad).subscribe(
                    (data) => {
                        this.eliminar = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.sacarUnidad();
                    }, (error) => {
                        this.eliminar = false;
                        this.manejaError(error)
                    }
                );
            }
        );
    }
    
    /**
     * Se remueve la unidad seleccionada
     */
    sacarUnidad() {
        if (this.listaUnidades.length >= 0) {
            let index = this.listaUnidades.findIndex(element => element.idUnidad == this.idUnidad);
            this.listaUnidades.splice(index, 1);
            this.listaUnidades = [...this.listaUnidades];
        }
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}