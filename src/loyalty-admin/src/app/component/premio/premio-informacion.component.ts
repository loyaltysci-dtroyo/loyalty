import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Rutas from '../../utils/rutas';
import { MenuItem, SelectItem } from 'primeng/primeng';

import { Premio } from '../../model/index';
import { PremioService, KeycloakService, I18nService } from '../../service/index';

import { PERM_ESCR_PREMIOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'premio-informacion-component',
    templateUrl: 'premio-informacion.component.html',
    providers: [Premio, PremioService]
})

/**
 * Componente que permite mostrar los detalles y opciones de un premio
 */
export class PremioInformacionComponent implements OnInit {

    
    public id: string; //Id del premio
    public estado: string; //Contendra los estados del premio
    public items: MenuItem[]; //Representa la lista con los items del menú 
    //Items del menú y las rutas asignadas
    public detalle = Rutas.RT_PREMIOS_DETALLE_REQUERIDO;
    public elegibles = Rutas.RT_PREMIOS_DETALLE_ELEGIBLES;
    public limites = Rutas.RT_PREMIOS_DETALLE_LIMITE;
    public display = Rutas.RT_PREMIOS_DETALLE_DISPLAY;
    public redimidos = Rutas.RT_PREMIOS_DETALLE_REDIMIDO;
    public categorias = Rutas.RT_PREMIOS_DETALLE_CATEGORIA;
    public certificados = Rutas.RT_PREMIOS_DETALLE_CERTIFICADO;
    //Permite verificar permisos (true/false)
    public escritura: boolean;
    //Se guarda las fechas si es calendarizado
    public fecInicio: Date = null;
    public fecFin: Date = null;
    //Semanas seleccionadas
    public semanas: number;
    //Array con la lista de los días de la semana
    public dias: SelectItem[];
    public diasSelec: string[];
    public efectividadItem: SelectItem[]; //Muestra la efectividad, si es recurrente/ calendarizado/ permanente
    //Permite el uso del ngClass según el estado, será la clase utilizada para dar formato al widget
    public publicado: boolean;
    public borrador: boolean;
    public archivado: boolean;
    public esRecurrente: boolean = false;//flag de recurrencia de la ubicacion true=recurrente
    public cargandoDatos: boolean = true;
    public eliminar: boolean;
    public accion: string;

    constructor(
        public premio: Premio,
        public premioService: PremioService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //El id del premio se inicializa con el valor introducido por la url
        this.route.params.forEach((params) => { this.premio.idPremio = params['id'] });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.diasSelec = [];
    }

    /**
     * Llamado solo una vez
     */
    ngOnInit() {
        this.mostrarDetalle();
        this.dias = [];
        this.dias = this.i18nService.getLabels("dias-efectividad-general")
        this.efectividadItem = [];
        this.efectividadItem = this.i18nService.getLabels("tipo-efectividad-general");
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Habilita el cuadro de dialogo
     * @param accion 
     */
    dialogoEliminar(accion: string) {
        this.accion = accion;
        this.eliminar = true;
    }

    /**
     * Método que se encarga traer el detalle del premio
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.premioService.token = token;
                this.premioService.obtenerPremioPorId(this.premio.idPremio).subscribe(
                    (data) => {
                        this.copyValuesOf(this.premio, data);
                        if (this.premio.efectividadFechaInicio != null) {
                            this.fecInicio = new Date(this.premio.efectividadFechaInicio);
                        }
                        if (this.premio.efectividadFechaFin != null) {
                            this.fecFin = new Date(this.premio.efectividadFechaFin);
                        }
                        if (this.premio.indEstado == 'P') {
                            this.publicado = true;
                        } else if (this.premio.indEstado == 'B') {
                            this.borrador = true;
                        } else {
                            this.archivado = true;
                        }
                        if (this.premio.efectividadIndDias || this.premio.efectividadIndSemana) {
                            this.esRecurrente = true;
                        }
                        if (this.premio.efectividadIndDias != "" && this.premio.efectividadIndDias != null) {
                            this.diasSelec = this.premio.efectividadIndDias.split('');
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Método que se encarga de publicar,usado solo cuando el premio es tipo borrador
     */
    publicarPremio() {
        if (this.premio.efectividadIndCalendarizado == 'C') {
            if (!this.esRecurrente) {
                this.premio.efectividadIndSemana = null;
                this.premio.efectividadIndDias = null;
            } else {
                this.premio.efectividadIndDias = "";
                if (this.diasSelec.length > 0 && this.diasSelec != null) {
                    let diasS = ""
                    this.diasSelec.forEach((element) => {
                        diasS += element;
                    });
                    this.premio.efectividadIndDias = diasS;
                }
            }
        } else {
            this.premio.efectividadFechaFin = null;
            this.premio.efectividadFechaInicio = null;
            this.premio.efectividadIndSemana = null;
            this.premio.efectividadIndDias = null;
        }
        this.premio.indEstado = "P";
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.premioService.token = token;
                this.premioService.editarPremio(this.premio).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-publicar"));
                        this.publicado = true;
                        this.archivado = this.borrador = false;
                        this.mostrarDetalle();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.premio.indEstado = this.estado;
                        this.publicado = false;
                        this.borrador = true;
                    }
                );
            }
        );
    }

    //Método que se encarga de actualizar un premio
    actualizarPremio() {
        if (this.premio.efectividadIndCalendarizado == 'C') {
            if (!this.esRecurrente) {
                this.premio.efectividadIndSemana = null;
                this.premio.efectividadIndDias = null;
            } else {
                this.premio.efectividadIndDias = "";
                for (let i = 0; i < this.diasSelec.length; i++) {
                    this.premio.efectividadIndDias += this.diasSelec[i];
                }
            }
        } else {
            this.premio.efectividadFechaFin = null;
            this.premio.efectividadFechaInicio = null;
            this.premio.efectividadIndSemana = null;
            this.premio.efectividadIndDias = null;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.premioService.token = token;
                this.premioService.editarPremio(this.premio).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.mostrarDetalle();
                    },
                    (error) => {
                        this.manejaError(error);
                    }
                );
            }
        );
    }

    /**
     * Utilizado para remover/archivar un premio
     */
    removerPremio() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.premioService.token = token;
                this.premioService.eliminarPremio(this.premio.idPremio).subscribe(
                    (data) => {
                        if (this.estado == 'P') {
                            this.archivado = true;
                            this.premio.indEstado = "A";
                            this.publicado = this.borrador = false;
                            this.msgService.showMessage(this.i18nService.getLabels("general-archivar"));
                        } else {
                            this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                            this.goBack();
                        }
                    },
                    (error) => {
                        this.manejaError(error);
                        if (this.estado == 'P') {
                            this.publicado = true;
                            this.archivado = false;
                        }
                    }
                );
            }
        );
    }

    /**
     * Redirecciona a la lista de premios
     */
    goBack() {
        let link = ['premios'];
        this.router.navigate(link);
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
