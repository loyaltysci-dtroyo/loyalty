import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Premio } from '../../model/index';
import { PremioService, KeycloakService, I18nService } from '../../service/index';

import { PERM_ESCR_PREMIOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'premio-display.component',
    templateUrl: 'premio-display.component.html',
})

/**
 * Jaylin Centeno
 * Componente utilizado para el display de los premios
 */
export class PremioDisplayComponent implements OnInit {

    
    public id: string; //Id del premio
    public formDisplay: FormGroup; // Form group para validar el formulario de premio
    public imagen: any;//Contendra el archivo subido por el usuario
    public avatar: any; //Guarda la dirección de la imagen
    public urlBase64: string[]; //Se guarda el array de la imagen

    public escritura: boolean; //Permite verificar permisos (true/false)
    public subiendoImagen: boolean = false;
    public vistaPrevia: boolean = false;
    public cargandoDatos: boolean = true;
    public noMostrar: boolean = false;

    constructor(
        public premio: Premio,
        public premioService: PremioService,
        public router: Router,
        public route: ActivatedRoute,
        public msgService: MessagesService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Validacion del formulario y datos por defecto
        this.formDisplay = new FormGroup({
            'encabezado': new FormControl('', Validators.required),
            'subEncabezado': new FormControl(''),
            'detalle': new FormControl('', Validators.required)
        });
    }

    /**
     * Método que se inicia al entrar por primera vez al componente
     */
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Obtiene el detalle de premio
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.premioService.token = token;
                this.premioService.obtenerPremioPorId(this.premio.idPremio).subscribe(
                    (data) => {
                        this.copyValuesOf(this.premio, data);
                        this.avatar = this.premio.imagenArte;
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Actualización de la informacion del premio
     */
    actualizarPremio() {
        this.noMostrar = true;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.premioService.token = token;
                if (this.premio.imagenArte != null) {
                    this.subiendoImagen = true;
                }
                this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
                let sub = this.premioService.editarPremio(this.premio).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                        this.mostrarDetalle();
                    },
                    (error) => {
                        this.manejaError(error)
                        this.subiendoImagen = false;
                    },
                    () => { sub.unsubscribe(); this.subiendoImagen = false; this.noMostrar = false; }
                );
            }
        );
    }

    /**
     * Obtiene la imagen subida por el usuario y la pasa a base64
     */
    subirImagen() {
        this.imagen = document.getElementById("avatar");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.avatar = reader.result;
            let urlBase64 = this.avatar.split(",");
            this.premio.imagenArte = urlBase64[1];
            
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}