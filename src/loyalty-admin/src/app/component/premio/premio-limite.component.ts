import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';

import { Premio } from '../../model/index';
import { PremioService, KeycloakService, I18nService } from '../../service/index';

import { PERM_ESCR_PREMIOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'premio-limite-component',
    templateUrl: 'premio-limite.component.html'
})

/**
 * Jaylin Centeno
 * Componente utilizado para asignar límites a los premios
 */
export class PremioLimiteComponent implements OnInit {

    public id: string; //Id del premio
    public formLimite: FormGroup; // Form group para validar el formulario de premio
    public estado: string;//Contendra los estados del premio
    public itemsRespuesta: SelectItem[] = []; //Contiene los elementos del intervalo de respuesta
    public escritura: boolean;//Permite verificar permisos (true/false)

    constructor(
        public premio: Premio,
        public premioService: PremioService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Permite obtener si el usuario cuenta o no con el permiso
        this.authGuard.canWrite(PERM_ESCR_PREMIOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Validacion del formulario y datos por defecto
        this.formLimite = new FormGroup({
            "cantTotal": new FormControl(Validators.required),
            "cantTotalMiembro": new FormControl(Validators.required),
            "cantMinimoAcumulado": new FormControl(Validators.required),
            "indIntervaloTotal": new FormControl(Validators.required),
            "indRespuesta": new FormControl(Validators.required),
            "cantIntervaloRespuesta": new FormControl(Validators.required),
            "indIntervaloMiembro": new FormControl('A', Validators.required),
            "indIntervaloRespuesta": new FormControl('A', Validators.required)
        });

        this.itemsRespuesta = this.i18nService.getLabels("general-limites-periodo");
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Trae el detalle del premio
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.premioService.token = token;
                this.premioService.obtenerPremioPorId(this.premio.idPremio).subscribe(
                    (data) => {
                        this.copyValuesOf(this.premio, data);
                        if(this.premio.indIntervaloTotal == null){
                            this.premio.indIntervaloTotal = 'A'
                        }
                        if(this.premio.indIntervaloMiembro == null){
                            this.premio.indIntervaloMiembro = 'A'
                        }
                        if(this.premio.indIntervaloRespuesta == null){
                            this.premio.indIntervaloRespuesta = 'A'
                        }
                        
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Método que se encarga de actualizar la informacion del premio
     */
    actualizarPremio() {
        //Si se selecciona que el premio tiene respuesta se deben evaluar que se llenarán los valores necesarios
        if (this.premio.indRespuesta) {
            if (this.premio.cantTotal != null && this.premio.cantTotalMiembro != null && this.premio.cantMinAcumulado != null && this.premio.indIntervaloTotal.trim() != "" && this.premio.cantIntervaloRespuesta != null && this.premio.indIntervaloMiembro.trim() != "" && this.premio.indIntervaloRespuesta.trim() != "") {
                this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                    (token: string) => {
                        this.premioService.token = token;
                        this.premioService.editarPremio(this.premio).subscribe(
                            (data) => {
                                this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                                this.mostrarDetalle();
                            },
                            (error) => {
                                this.manejaError(error)
                            }
                        );
                    }
                );
            } else {
                this.msgService.showMessage(this.i18nService.getLabels('warn-dato-valido'));
            }

        } else {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.premioService.token = token;
                    this.premioService.editarPremio(this.premio).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                        },
                        (error) => {
                            this.manejaError(error)
                        }
                    );
                }
            );
        }
    }

    /**
     * Método para manejar los errores que se producen en las transacciones
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}