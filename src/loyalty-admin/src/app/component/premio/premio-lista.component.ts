import { Component, OnInit, ViewChild, Renderer, ElementRef, OnChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Premio, CategoriaPremio } from '../../model/index';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { PremioService, CategoriaPremioService, KeycloakService, I18nService } from '../../service/index';
import * as Routes from '../../utils/rutas';
import { PERM_ESCR_PREMIOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'premio-lista-component',
    templateUrl: 'premio-lista.component.html',
    providers: [Premio, CategoriaPremio, PremioService, CategoriaPremioService],

})

/**
 * Jaylin Centeno
 * Componente hijo, muestra la lista de los premios disponibles en el sistema
 */
export class PremioListaComponent implements OnInit {

    public escritura: boolean; //Representa el permiso de escritura (true/false)
    public cargandoDatos : boolean = true; //Utilizado para mostrar la carga de datos
    public eliminar: boolean; //Habilita el cuadro de dialogo para remover

    //------ Premios ------//
    public listaPremios: Premio[]; //Lista de los premio del sistema
    public listaVacia: boolean = true; //Permite verificar si la lista de premios esta vacía
    public totalRecords: number; //Total de premios en el sistema
    public cantidad: number=10; //Cantidad de rows
    public filaDesplazamiento: number=0; //Desplazamiento de la primera fila
    public idPremio: string; //Id del premio seleccionado

    //------- Búsqueda-------//
    public avanzada: boolean = false; //Permite activar/desactivar la búsqueda avanzada
    public busqueda: string; //Palabras claves de búsqueda
    public listaBusqueda: string[] = []; //Palabras de búsqueda, ChipModule
    public estadoPremio: string[] = []; //Almacena los indicadores de estado del premio para busqueda avanzada
    public tipoPremio: string[] = []; //Almacena los indicadores del tipo de premio para busqueda avanzada

    constructor(
        public premio: Premio,
        public premioService: PremioService,
        public categoria: CategoriaPremioService,
        public router: Router,
        public renderer: Renderer,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) { 
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        //Estados seleccionados para traer la lista de premios según los estados predeterminados
        this.estadoPremio = ['P', 'B'];
    }

    /**
     * Método que se inicia al entrar por primera vez al componente
     */
    ngOnInit() {
        this.busquedaPremios()
    }

    /**
     * Permite mostrar el cuadro de dialogo para remover el premio seleccionado
     * @param idPremio 
     */
    dialogoConfirmar(idPremio: string) {
        this.idPremio = idPremio;
        this.eliminar = true;
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event:any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaPremios();
    }

    /**
     * Método que permite activar o desactivar la búsqueda avanzada
     */ 
    busquedaAvanzada(){
        this.avanzada = !this.avanzada;
    }

    /**
     * Método encargado de realizar la búsqueda de premios según criterios de búsqueda
     * cantidad, filaDesplazamiento, tipoPremio, estadoPremio, busqueda
     */
    busquedaPremios() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.premioService.token = token;
                this.premioService.busquedaPremios(this.cantidad, this.filaDesplazamiento, this.tipoPremio, this.estadoPremio, this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaPremios = response.json();
                        if(response.headers.get("Content-Range") != null){
                           this.totalRecords =+ response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaPremios.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => {
                        this.manejaError(error);
                        this.cargandoDatos = false;
                    }
                );
            }
        );
    }

    /**
     * Redirecciona a la página para insertar un cliente
     */
    nuevoPremio() {
        let link = ['premios/insertarPremio'];
        this.router.navigate(link);
    }

    /**
     * Método para remover un premio de la lista
     */
    removerPremio(){
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.premioService.token = token;
                this.premioService.eliminarPremio(this.idPremio).subscribe(
                    (data) => {
                        if(this.premio.indEstado == 'P'){
                            this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        }else{
                            this.msgService.showMessage(this.i18nService.getLabels("general-archivar"));
                        }
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    /**
     * Redirecciona al detalle del premio
     * @param premio 
     */
    gotoDetail(premio:Premio) {
        let link = ['premios/detallePremio', premio.idPremio];
        this.router.navigate(link);
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
