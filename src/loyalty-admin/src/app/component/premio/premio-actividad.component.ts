import { ErrorHandlerService, MessagesService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { ActividadService, KeycloakService, I18nService } from '../../service/index';
import { Miembro, RegistroActividad } from '../../model/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { PERM_LECT_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'premio-actividad',
    templateUrl: 'premio-actividad.component.html',
    providers: [ActividadService]
})

/**
 * Componente utilizado para mostrar toda la actividad relacionada con los premios
 */
export class PremiosActividadComponent implements OnInit {

    public listaActividades: any[];
    public lecturaMiembro: boolean; //Permiso de lectura sobre miembro
    /**Atributos para sorting/busqueda */
    public indPeriodo: string = '1M';
    public periodos: SelectItem[] = [
        { label: '1 mes', value: '1M' },
        { label: '2 meses', value: '2M' },
        { label: '3 meses', value: '3M' },
        { label: '6 meses', value: '6M' },
        { label: '1 año', value: '1Y' },
        { label: '2 años', value: '2Y' }];

    constructor(
        public actividadService: ActividadService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard
    ) {
        //Permite saber si el usuario tiene permisos de lectura sobre miembro
        this.authGuard.canWrite(PERM_LECT_MIEMBROS).subscribe((permiso: boolean) => {
            this.lecturaMiembro = permiso;
        });
    }

    ngOnInit() {
        this.obtenerActividad();
    }

    /**
     * Obtiene los registros de actividad para el miembro actual
     */
    obtenerActividad() {
        this.kc.getToken().then((tkn) => {
            this.actividadService.token = tkn;
            let sub = this.actividadService.getActividadesPremios(this.indPeriodo).subscribe(
                (response: RegistroActividad[]) => {
                    this.listaActividades = response;
                }, (response: Response) => {
                    this.handleError(response);
                }, () => {
                    sub.unsubscribe();
                }
            );
        }).catch()
    }
    /**
     * Redirige al administrador al detalle del miembro
     */
    gotoMiembro(idElemento: string) {
        this.router.navigate(['/miembros/detalleMiembro/', idElemento]);
    }
    /**
    * Redirige al administrador a la pantalla de dashboard para el elemento seleccionado
    */
    gotoPremio(idElemento: string) {
        this.router.navigate(['/premios/detallePremio', idElemento]);
    }
    /**
     * Maneja el erro a nivel de componente
     */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}