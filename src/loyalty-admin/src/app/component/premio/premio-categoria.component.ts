import { Component, Renderer, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CategoriaPremio, Premio } from '../../model/index';
import { CategoriaPremioService, PremioService, KeycloakService } from '../../service/index';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { PERM_ESCR_PREMIO_CATEGORIA, PERM_ESCR_PREMIOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'premio-categoria-component',
    templateUrl: 'premio-categoria.component.html',
    providers: [CategoriaPremio, CategoriaPremioService]
})

/**
 * Componete para ver, remover y asociar premios a categorías
 */
export class PremioCategoriaComponent implements OnInit {

    public idCategoria: string; // Id de la categoría seleccionado
    public escritura: boolean; // Permite verificar permisos (true/false)
    public escrituraPremio: boolean; //Permiso
    public asignar: boolean = false;
    public cargandoDatos: boolean = true;
    public cargandoTotal: boolean = true;
    public eliminar: boolean;

    // -- Categ. Asignadas -- //
    public listaCategorias: CategoriaPremio[] = []; // Lista de las categorías que tiene el premio
    public listaVacia: boolean = true; // Para verificar existen categorías asignadas a premios
    public cantidad: number = 10; // Cantidad de filas
    public totalRecords: number; // Total de de categorías en el sistema
    public filaDesplazamiento: number = 0; // Desplazamiento de la primera fila

    // -- Categ. NO Asignadas -- //
    public listaTotalCategorias: CategoriaPremio[] = []; // Lista con las categorías no asignadas
    public listaTotalVacia: boolean = true; // Para verificar si existen categorías en la aplicacion
    public cantidadTotal: number = 10; // Cantidad de filas
    public totalRecordsTotal: number; // Total de de categorías en el sistema
    public filaDesplazamientoTotal: number = 0; // Desplazamiento de la primera fila

    constructor(
        public premio: Premio,
        public premioService: PremioService,
        public categoria: CategoriaPremio,
        public categoriaService: CategoriaPremioService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService
    ) {
        // Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PREMIO_CATEGORIA).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_ESCR_PREMIOS).subscribe((permiso: boolean) => {
            this.escrituraPremio = permiso;
        });
    }

    // Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.obtenerCategoriasPremio();
    }

    // Método para guardar el id de la categoría seleccionada
    categoriaSeleccionada(idCategoria: string) {
        this.idCategoria = idCategoria;
        this.eliminar = true;
    }

    // Método utilizado para la carga de los elementos del datatable
    // onLazyLoad, permite la carga de los elementos en segmentos
    // paginación
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerCategoriasPremio();
    }

    loadDataTotal(event) {
        this.filaDesplazamientoTotal = event.first;
        this.cantidadTotal = event.rows;
        this.obtenerCategoriasNoAsignadas();
    }

    asignarCategoria() {
        this.obtenerCategoriasNoAsignadas();
        this.asignar = true;
    }

    // Método para obtener las categorías asignadas al premio
    obtenerCategoriasPremio() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.obtenerCategoriasPremio(this.premio.idPremio, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        // Se obtiene la lista de las categorías del premio
                        this.listaCategorias = response.json();
                        // Se recibe el total de las categorías disponibles
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaCategorias.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejarError(error)
                )
            }
        );
    }

    // Método que se encarga de traer las categorías que no están asignadas al premio
    // Por el momento se obtienen todos las categorías existentes
    obtenerCategoriasNoAsignadas() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.obtenerCategoriasNoPremio(this.premio.idPremio, this.cantidadTotal, this.filaDesplazamientoTotal).subscribe(
                    (response: Response) => {
                        // Se obtiene la lista de categorías
                        this.listaTotalCategorias = response.json();
                        // Se recibe el total de las categorías disponibles
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecordsTotal = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaTotalCategorias.length > 0) {
                            this.listaTotalVacia = false;
                        } else {
                            this.listaTotalVacia = true;
                        }
                        this.cargandoTotal = false;
                    },
                    (error) =>
                        this.manejarError(error)
                );
            });
    }


    // Método que permite asignar la categoría a un solo premio
    asignarCategoriaPremio(idCategoria: string) {
        if (this.escritura && this.escrituraPremio) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token) => {
                this.categoriaService.token = token || '';
                this.categoriaService.asignarPremio(idCategoria, this.premio.idPremio).subscribe(
                    (data) => {
                        this.idCategoria = idCategoria;
                        this.msgService.showMessage({ detail: "La asociación se realizo correctamente.", severity: "success", summary: "" });
                        this.sacarCategoriaNoAsignada();
                        this.obtenerCategoriasPremio();
                        this.obtenerCategoriasNoAsignadas();
                    },
                    (error) => this.manejarError(error)
                );
            });
        }
    }

    // Método para remover la asignación de una categoría a un premio
    removerCategoriaPremio() {
        if (this.escritura && this.escrituraPremio) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.removerPremio(this.idCategoria, this.premio.idPremio).subscribe(
                    (data) => {
                        this.sacarCategoriasPremio();
                        this.obtenerCategoriasPremio();
                        this.obtenerCategoriasNoAsignadas();
                        this.eliminar = false;
                        this.msgService.showMessage({ detail: "El premio ya no pertenece a esta categoría.", severity: "success", summary: "" });
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
        }
    }

    // Método que remueve de la tabla de categorías no asignasdas las categoría seleccionada
    sacarCategoriaNoAsignada() {
        let index = this.listaTotalCategorias.findIndex(element => element.idCategoria == this.idCategoria);
        this.listaTotalCategorias.splice(index, 1);
    }

    // Método que remueve de la tabla de categorías asignadas la categoría seleccionada
    sacarCategoriasPremio() {
        let index = this.listaCategorias.findIndex(element => element.idCategoria == this.idCategoria);
        this.listaCategorias.splice(index, 1);
    }

    //  Método para regresar al detalle de la categoría 
    goBack() {
        let link = ['premios/detalleCategoria', this.premio.idPremio];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}