import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PremioService, EstadisticaService } from '../../service/index';
import { Premio } from '../../model/index';
import { KeycloakService } from '../../service/index';

@Component({
    moduleId: module.id,
    selector: 'premio-detalle-dashboard',
    templateUrl: 'premio-detalle-dashboard.component.html',
    providers: [EstadisticaService]
})

/**
 * Componente que muestra los detalles de estadisticas del premio
 */
export class PremioDetalleDashboardComponent implements OnInit {
    public statsData: {
        idPremio: string,
		nombrePremio: string,
		nombreInternoPremio: string,
		cantRedenciones: number,
		cantIntentario: number,
		cantMiembrosElegibles: number,
		cantRedencionesElegibles: number,
		periodos: [
			{
				mes: string,
				cantRedenciones: number,
				cantElegiblesPromedio: number
			},
			{
				mes: string,
				cantRedenciones: number,
				cantElegiblesPromedio: number
			},
			{
				mes: string,
				cantRedenciones: number,
				cantElegiblesPromedio: number
			},
			{
				mes: string,
				cantRedenciones: number,
				cantElegiblesPromedio: number
			}
		]
    };
    public chartVistasData: any;
    public chartElegibilidadData: any;
    public options: any;
    public data: any;

    constructor(public premio: Premio,
        public kc: KeycloakService,
        public premioService: PremioService,
        private i18nService: I18nService,
        public estadisticaService: EstadisticaService,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService) {
            this.statsData=undefined;
    }

    /**
     * Método llamado al inicializar el componente
     */
    ngOnInit() {
        this.obtenerPremio();
        this.obtenerDatosEstadistica();
    }

    /**
     * Se poblan los gráficos con los datos obtenidos
     */
    poblarGraficos() {
        this.statsData.periodos.reverse();
        this.chartElegibilidadData = {
            labels: this.statsData.periodos.map((element => element.mes)),
            datasets: [
                {
                    label: this.i18nService.getLabels("premios-dashboard-cant-redenciones"),
                    data: this.statsData.periodos.map((element => element.cantRedenciones)),
                    fill: false,
                    borderColor: '#4bc0c0'
                }
            ]
        };
        this.chartVistasData = {
            labels: this.statsData.periodos.map((element => element.mes)),
            datasets: [
                
                {
                    label: this.i18nService.getLabels("premios-dashboard-cant-elegibles-promedio"),
                    data: this.statsData.periodos.map((element => element.cantElegiblesPromedio)),
                    fill: false,
                    borderColor: '#afc0c0'
                }
            ]
        };
    }

    /**
     * Se encarga de obtener los datos de las estadisticas para poblar los gráficos
     */
    obtenerDatosEstadistica() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.estadisticaService.token = tkn;
                let sub = this.estadisticaService.getEstadisticasPremio(this.premio.idPremio).subscribe(
                    (responseData: any) => {
                        this.statsData = responseData;
                        this.poblarGraficos();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /**
     * Obtiene el premio al que pertencen las estadisticas
     */
    obtenerPremio() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.premioService.token = tkn;
                let sub = this.premioService.obtenerPremioPorId(this.premio.idPremio).subscribe(
                    (chartData: Premio) => {
                        this.copyValuesOf(this.premio, chartData);
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    selectData(event) {

    }
    /**
   * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
   * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
   * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
   * no siempre sea necesario llamar al api para actualizar los datos
   */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}