import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Component, OnInit, } from '@angular/core';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { NotificacionService, PromocionService, MisionService } from '../../service/index';
import { Response } from '@angular/http';
import { Notificacion, Promocion, Mision } from '../../model/index';
import { KeycloakService } from '../../service/index';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as Rutas from '../../utils/rutas';
import { PERM_ESCR_CAMPANAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { AppConfig } from '../../app.config';
/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Componente en el cual se permite la insercion de nuevas notificaciones publicitarias, 
 * se permite la definicion de los parametros generales de la notificacion y luego
 * se redirige al usuario al componente de detalle de notificacion
 */
@Component({
    moduleId: module.id,
    selector: 'notificaciones-insertar',
    templateUrl: 'notificacion-insertar.component.html',
    providers: [Notificacion, PromocionService, Promocion, Mision, MisionService]
})
export class NotificacionesInsertarComponent implements OnInit {
    
    public nombreElementoSeleccionado: string;
    public nombreValido: boolean;//true si el nombre es valido
    public optionsTipo: SelectItem[];//drop de tipo de notificacion (email o push)
    public optionsEvento: SelectItem[];//drop de opciones de evento (manual, calendarizado, trigger)
    public optionsObjetivo: SelectItem[];//drop de tipos de objetivo (mision, promocion o recompensa)
    public rtInsertarNotificaciones = Rutas.RT_CAMPANAS_INSERTAR;
    public idNotificacionInsertada: string;
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public file_src: any;//src del avatar del usuario
    public file: any;//imagen
    public cargando: boolean = false;//usado para mostrar una animacion al usuario 
    public displayInsertar: boolean = false;
    public listaPromocionesDisponibles: Promocion[];
    public cantRegistros: number;//para paginar la seleccion de promo objetivo
    public cantTotal: number;//para paginar la seleccion de promo objetivo
    public rowOffset: number;//para paginar la seleccion de promo objetivo
    public busquedaPromos: string[];//para la seleccion de promo objetivo
    public listaMisiones: Mision[];//lista de misiones 

    constructor(public notificacion: Notificacion,
        public notificacionService: NotificacionService,
        public promocionService: PromocionService,
        public kc: KeycloakService, public i18nService:I18nService,
        public router: Router, public authGuard: AuthGuard,
        public misionService:MisionService,
        public msgService: MessagesService) {
        this.listaPromocionesDisponibles = [];
        this.listaMisiones=[];
    }

    ngOnInit() {
        //poblado de options para los dropdown
        this.optionsTipo = [];
        this.optionsObjetivo = [];
        this.optionsEvento=[];

        this.notificacion.indEvento = 'M';
        this.notificacion.indTipo = 'E';
        this.notificacion.indObjetivo = 'P';

        this.optionsTipo=this.i18nService.getLabels("notificaciones-tipo");
        this.optionsObjetivo= this.i18nService.getLabels("notificaciones-objetivos");
        this.file_src = `${AppConfig.DEFAULT_IMG_URL}`;;
        this.authGuard.canWrite(PERM_ESCR_CAMPANAS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }


    seleccionarElemento() {

    }
    /**Metodo encargado de cargar las promos de la base de datos
     * Utiliza el string de busqueda que fue seteado por el campo de busqueda,
     * en este caso se obtiene el response completo para efectos de obtener los headers para la paginacion
     */
    cargarMisiones() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.buscarMisiones(this.busquedaPromos, ["A"], this.rowOffset, this.cantRegistros).subscribe(
                    (data: Response) => {
                        this.cantTotal = +data.headers.get("Content-Range").split("/")[1];
                        this.listaMisiones = <Mision[]>data.json();//se obtiene los datos del body del request
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**Metodo encargado de cargar las promos de la base de datos
     * Utiliza el string de busqueda que fue seteado por el campo de busqueda,
     * en este caso se obtiene el response completo para efectos de obtener los headers para la paginacion
     */
    agregarMision(){

    }

    /**
     * Establece automaticamente el nombre itnerno de los elementos a desplegar
     */
    establecerNombres() {
        let temp = this.notificacion.nombre.trim();
        temp = temp.toLowerCase();
        temp = temp.replace(new RegExp(" ", 'g'), "_");
        temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
        this.notificacion.nombreInterno = temp;
    }

    /**Usado para cargar de manera perezosa los elementos de la lista de promociones */
    loadData(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantRegistros = event.rows;
        this.listarPromocionesElegibles();
    }
    /**
   * Se encarga de traer la lista de promociones que pueden ser seleccionadas como ojetivo para la notificacion
   */
    listarPromocionesElegibles() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.buscarPromociones(this.busquedaPromos, ['A'], this.rowOffset, this.cantRegistros).subscribe(
                    (data: Response) => {
                        this.displayInsertar = true;
                        this.cantTotal = +data.headers.get("Content-Range").split("/")[1];
                        this.listaPromocionesDisponibles = <Promocion[]>data.json();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    /**
     * Inserta una notificacion en el sistema, emplea la variable global notificacion para obtener los datos de la notificacion a insertar
     * la variable notificacion se llena automaticamente a traves de las directivas de ngmodel de los componentes del form
     */
    insertarNotificacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                if (this.nombreValido) {
                    let sub = this.notificacionService.addNotificacion(this.notificacion).subscribe(
                        (response: Response) => {
                            this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                            this.idNotificacionInsertada = response.text();
                            this.gotoDetail(this.idNotificacionInsertada);
                        }, (error) => { this.manejaError(error); },
                        () => { sub.unsubscribe(); }
                    );
                } else {
                    this.msgService.showMessage(this.i18nService.getLabels("error-nombre-interno"));
                }
            });
    }
    /**
     *Metodo que reririge al detalle del segmento que recibe por parámetros
     * Recibe: el segmento al cual redirigir al detalle
     */
    gotoDetail(idNotificacion: string) {
        let link = [Rutas.RT_CAMPANAS_DETALLE, idNotificacion];
        this.router.navigate(link);
    }
    /**
     * Valida que el nombre interno de la notificacion sea unico y se evite tener que enviar un request al API que terminara en error
     * 
     */
    validarNombre() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                if (this.notificacion.nombreInterno == "") { this.nombreValido = false; return }
                let sub = this.notificacionService.validarNombre(this.notificacion.nombreInterno).subscribe(
                    (existeNombre: string) => {
                        this.nombreValido = (existeNombre == "false");
                    }, () => {

                    }, () => { sub.unsubscribe(); }
                );
            });
    }
    /**
     * Selecciona la promocion de objetivo para la notificacion
     */
    agregarPromocion(promo: Promocion) {
        this.displayInsertar = false;
        this.notificacion.promocion = promo;
        this.nombreElementoSeleccionado = promo.nombre;
    }
    /*
  * Muestra un mensaje de error al usuario con los datos de la transaccion
  * Recibe: una instancia de request con la informacion de error
  * Retorna: un observable que proporciona información sobre la transaccion
  */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    confirm() {
        this.gotoDetail(this.idNotificacionInsertada);
    }
    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {
        this.file = input.files[0];
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        let reader: any
        let target: EventTarget;
        reader = new FileReader(); // Crear un FileReader
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            img.src = event.target.result; // Obtener el (base64 de la imagen)
            this.notificacion.imagenArte = img.src.split(',')[1];
            this.file_src = this.resize(img); // Redimensionar la imagen
        }, false);
        reader.readAsDataURL(input.files[0]);
    }
    /*
     * Metodo encargado de redimensionar una imagen
     * Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     * estan por defecto en 900*900
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");
        let width = img.width;
        let height = img.height;
        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        let dataUrl = canvas.toDataURL('image/jpeg'); // IMPORTANT: 'jpeg' NOT 'jpg'
        return dataUrl
    }

    goBack() {
        this.router.navigate([Rutas.RT_CAMPANAS_LISTA]);
    }
}