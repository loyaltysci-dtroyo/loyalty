import { I18nService } from '../../service/i18n.service';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Notificacion, Segmento, Miembro } from '../../model/index';
import { NotificacionService } from '../../service/index';
import { KeycloakService } from '../../service/index';
import { SelectItem } from 'primeng/primeng';
import { PERM_ESCR_CAMPANAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Componente encargado de alternar entre los componentes que sleccionaran los miembros elegibles de la notificacion,
 * para el establecimiento de los elegibles hay un componente individual para cada tipo de elegible
 */
@Component({
    moduleId: module.id,
    selector: 'notificaciones-detalle-elegibles',
    templateUrl: 'notificacion-detalle-elegibles.component.html'
})
export class NotificacionesDetalleElegiblesComponent implements OnInit {


    public busquedaElegibles: string;//campo de busqueda para elegibles en el caso de miembros
    public busquedaDisponibles: string;//campo de busqueda para segmentos/usuarios/ubicaciones disponibles para agregar 
    public showElegibles: boolean;//boolean para desplegar los elementos de promo(false) o  de overrides(true)
    public optionsElegibles: SelectItem[] = [];
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public indTarget: string;//indicador de objetivo
    public indElegibles: string;//indicador para determinar si desplegar segmentos o usuarios 

    constructor(public notificacion: Notificacion,
        public notificacionService: NotificacionService,
        public authGuard: AuthGuard,
        public kc: KeycloakService,
        public i18nService:I18nService,
        public msgService: MessagesService) {
        this.showElegibles = false;
        this.indElegibles = "S";
        this.optionsElegibles=this.i18nService.getLabels("notificaciones-elegibles");
        this.authGuard.canWrite(PERM_ESCR_CAMPANAS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }
    ngOnInit() {
        this.obtenerNotificacion();

    }
    /**
    * Obtiene los detalles de la notificacion en el sistema
    */
    obtenerNotificacion() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.getNotificacion(this.notificacion.idNotificacion).subscribe(
                    (data: Notificacion) => {
                        this.notificacion = data;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    mostrarMiembros() {
        this.showElegibles = true;
        this.indElegibles = "M";
    }
    mostrarSegmentos() {
        this.showElegibles = true;
        this.indElegibles = "S";
    }

    /*
  * Muestra un mensaje de error al usuario con los datos de la transaccion
  * Recibe: una instancia de request con la informacion de error
  * Retorna: un observable que proporciona información sobre la transaccion
  */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}