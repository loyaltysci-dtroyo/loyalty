import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MetricaService, EstadisticaService } from '../../service/index';
import { Metrica } from '../../model/index';
import { KeycloakService } from '../../service/index'; import { Component, OnInit } from '@angular/core';
import { PERM_LECT_CAMPANAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'notificacion-dashboard-general',
    templateUrl: 'notificacion-dashboard-general.component.html',
    providers: [EstadisticaService]
})

export class NotificacionDashboardGeneralComponent implements OnInit {
    public statsData: [{
        idNotificacion: string,
        nombreNotificacion: string,
        nombreInternoNotificacion: string,
        cantMsjsEnviados: number,
        cantMsjsAbiertos: number,
        cantMiembrosEnviados: number,
        cantMiembrosObjetivo: number,
        periodos: [
            {
                mes: string,
                cantMsjsEnviados: number,
                cantMsjsAbiertos: number,
                cantMiembrosEnviados: number
            },
            {
                mes: string,
                cantMsjsEnviados: number,
                cantMsjsAbiertos: number,
                cantMiembrosEnviados: number
            },
            {
                mes: string,
                cantMsjsEnviados: number,
                cantMsjsAbiertos: number,
                cantMiembrosEnviados: number
            },
            {
                mes: string,
                cantMsjsEnviados: number,
                cantMsjsAbiertos: number,
                cantMiembrosEnviados: number
            }
        ]
    }];
    public chartCantMiembrosEnviados: any;
    public chartCantMensajesAbiertos: any;
    public chartCantMensajesEnviados: any;

    public chartCantVistasUnicas: any;
    public options: any;
    public lectura: boolean; //Permisos de lectura sobre notificacion
    public cargandoDatos: boolean = true;

    constructor(
        public kc: KeycloakService,
        private i18nService: I18nService,
        public estadisticaService: EstadisticaService,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService,
        public authGuard: AuthGuard
    ) {
        //Permite saber si el usuario tiene permisos
        this.authGuard.canWrite(PERM_LECT_CAMPANAS).subscribe((permiso: boolean) => {
            this.lectura = permiso;
            this.obtenerDatosEstadistica();
        });
    }

    ngOnInit() {
    }

    // this.statsData.periodos.map((element=>element.mes))
    poblarGraficos() {
        this.chartCantMiembrosEnviados = {
            labels: this.statsData[0].periodos.map((element => element.mes)),
            datasets: this.statsData.map((element) => {
                return {
                    label: element.nombreNotificacion,
                    data: element.periodos.map((element) => { return element.cantMiembrosEnviados }),
                    fill: false,
                    borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                }
            })
        }
        this.chartCantMensajesAbiertos = {
            labels: this.statsData[0].periodos.map((element => element.mes)),
            datasets: this.statsData.map((element) => {
                return {
                    label: element.nombreNotificacion,
                    data: element.periodos.map((element) => { return element.cantMsjsAbiertos }),
                    fill: false,
                    borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                }
            })
        }
        this.chartCantMensajesEnviados = {
            labels: this.statsData[0].periodos.map((element => element.mes)),
            datasets: this.statsData.map((element) => {
                return {
                    label: element.nombreNotificacion,
                    data: element.periodos.map((element) => { return element.cantMsjsEnviados }),
                    fill: false,
                    borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                }
            })
        }

    }

    obtenerDatosEstadistica() {
        if (this.lectura) {
            this.kc.getToken().then(
                (tkn: string) => {
                    this.estadisticaService.token = tkn;
                    let sub = this.estadisticaService.getEstadisticasNotificaciones().subscribe(
                        (responseData: any) => {
                            this.statsData = responseData;
                            this.poblarGraficos();
                            this.cargandoDatos = false;
                        }, (error) => {
                            this.manejaError(error);
                            this.cargandoDatos = false;
                        }, () => {
                            sub.unsubscribe();
                        }
                    );
                });
        }else{ this.cargandoDatos = false; }
    }

    selectData(event) {

    }
    /**
   * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
   * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
   * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
   * no siempre sea necesario llamar al api para actualizar los datos
   */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}