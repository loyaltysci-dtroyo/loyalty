import { I18nService } from '../../service/i18n.service';
import { PERM_ESCR_CAMPANAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Notificacion, Promocion } from '../../model/index';
import { NotificacionService } from '../../service/index';
import { KeycloakService, PromocionService } from '../../service/index';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';

/*
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Componente encargado de habilitar la edicion de atributos de las notificaciones publicitarias, 
 * en este componente el usuario puede establecer parametros generales
 */
@Component({
    moduleId: module.id,
    selector: 'notificaciones-detalle-atributos',
    templateUrl: 'notificacion-detalle-atributos.component.html',
    providers: [PromocionService, Promocion]
})
export class NotificacionesDetalleAtributosComponent implements OnInit {
    
    public form: FormGroup;//validaciones
    public nombreValido: boolean;//true si el nombre es valido
    public optionsTipo: SelectItem[];//drop de tipo de notificacion (email o push)
    // public optionsEvento: SelectItem[];//drop de opciones de evento (manual, calendarizado, trigger)
    public optionsObjetivo: SelectItem[];//drop de tipos de objetivo (mision, promocion o recompensa)
    public tienePermisosEscritura: boolean;
    public listaPromocionesDisponibles: Promocion[];//promociones disponibles que se muestran al elegir promocion objetivo
    public busquedaPromos: string[];//terminos de busqueda para objetivo
    public displayInsertar: boolean;//despliega el modal para seleccionar el objetivo 
    public cantTotal: number;//cantidad total de registros para paginacion, usado en el objtivo
    public rowOffset: number;//offset de row para la paginacion del selector de objetivos
    public cantRegistros: number;//cantidad de registros que se le pide al API para el selector de objetivos

    constructor(
        public i18nService:I18nService,
        public notificacion: Notificacion, 
        public campanaService: NotificacionService,
        public authGuard: AuthGuard,
        public router: Router, 
        public kc: KeycloakService, 
        public route: ActivatedRoute,
        public promocionService: PromocionService,
        public msgService: MessagesService) {
        this.form = new FormGroup({
            "nombre": new FormControl('', [Validators.required]),
            "nombreInterno": new FormControl({ value: "", disabled: true }, Validators.required),
            "drpNotificacion": new FormControl({ value: '', disabled: true }),
            "drpObjetivo": new FormControl(''),
            "promo": new FormControl('')
        });
        this.authGuard.canWrite(PERM_ESCR_CAMPANAS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.listaPromocionesDisponibles = [];

        //poblado de options para los dropdown
        this.optionsTipo = [];
        this.optionsObjetivo = [];
        // this.optionsEvento = [];

        this.optionsTipo=this.i18nService.getLabels("notificaciones-tipo");

        this.optionsObjetivo=this.i18nService.getLabels("notificaciones-objetivos");
    }

    validarNombre() {
        //TODO
    }

    ngOnInit() {
        this.obtenerNotificacion();
    }

    /**Usado para cargar lazy los elementos de la lista de promociones elegibles*/
    loadData(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantRegistros = event.rows;
        this.listarPromocionesDisponibles();
    }
    /**
    * Selecciona la promocion de objetivo para la notificacion
    */
    agregarPromocion(promo: Promocion) {
        this.displayInsertar = false;
        this.notificacion.promocion = promo;
    }
    /**
 * Se encarga de traer la lista de promociones que pueden ser seleccionadas como ojetivo para la notificacion
 */
    listarPromocionesDisponibles() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.buscarPromociones(this.busquedaPromos, ['A'], this.rowOffset, this.cantRegistros).subscribe(
                    (data: Response) => {
                        this.displayInsertar = true;
                        this.cantTotal = +data.headers.get("Content-Range").split("/")[1];
                        this.listaPromocionesDisponibles = <Promocion[]>data.json();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**
     * Metodo encargado de guardar los detalles de la notificacion en el sistema
     * Utiliza la instancia de notificacion almacenada en this.promo
     */
    actualizarNotificacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.campanaService.token = tkn;
                let sub = this.campanaService.updateNotificacion(this.notificacion).subscribe(
                    () => {
                        this.obtenerNotificacion();
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Obtiene los detalles de la notificacion en el sistema y los almacena en la variable global llamada notificacion
     */
    obtenerNotificacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.campanaService.token = tkn;
                let sub = this.campanaService.getNotificacion(this.notificacion.idNotificacion).subscribe(
                    (data: Notificacion) => {
                        this.notificacion = data;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}