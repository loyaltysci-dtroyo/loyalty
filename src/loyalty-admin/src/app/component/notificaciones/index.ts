
export {NotificacionesComponent} from './notificacion.component';
export {NotificacionesListaComponent} from './notificacion-lista.component';
export {NotificacionesInsertarComponent} from './notificacion-insertar.component';
export {NotificacionesDetalleComponent} from './notificacion-detalle.component';
export {NotificacionesDetalleArteComponent} from './notificacion-detalle-arte.component';
export {NotificacionesDetalleAtributosComponent} from './notificacion-detalle-atributos.component';
export {NotificacionesDetalleElegiblesComponent} from './notificacion-detalle-elegibles.component';
export {NotificacionesDetalleMiembrosComponent} from './elegibles/notificacion-detalle-miembros.component';
export {NotificacionesDetallePromoComponent} from './elegibles/notificacion-detalle-promo.component';
export {NotificacionesDetalleSegmentoComponent} from './elegibles/notificacion-detalle-segmento.component';
export {NotificacionDetalleDashboardComponent} from './notificacion-detalle-dashboard.component';
export {NotificacionDetalleActividadComponent} from './notificacion-detalle-actividad.component';
export {NotificacionActividadComponent} from './notificacion-actividad.component';
export {NotificacionDashboardGeneralComponent} from './notificacion-dashboard-general.component';