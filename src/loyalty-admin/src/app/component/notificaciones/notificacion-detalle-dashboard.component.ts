import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NotificacionService, EstadisticaService } from '../../service/index';
import { Notificacion } from '../../model/index';
import { KeycloakService } from '../../service/index';

@Component({
    moduleId: module.id,
    selector: 'notificacion-detalle-dashboard',
    templateUrl: 'notificacion-detalle-dashboard.component.html',
    providers: [EstadisticaService]
})
export class NotificacionDetalleDashboardComponent implements OnInit {
    public statsData: {
        idNotificacion: string,
        nombreNotificacion: string,
        nombreInternoNotificacion: string,
        cantMsjsEnviados: number,
        cantMsjsAbiertos: number,
        cantMiembrosEnviados: number,
        cantMiembrosObjetivo: number,
        periodos: [
            {
                mes: string,
                cantMsjsEnviados: number,
                cantMsjsAbiertos: number,
                cantMiembrosEnviados: number
            },
            {
                mes: string,
                cantMsjsEnviados: number,
                cantMsjsAbiertos: number,
                cantMiembrosEnviados: number
            },
            {
                mes: string,
                cantMsjsEnviados: number,
                cantMsjsAbiertos: number,
                cantMiembrosEnviados: number
            },
            {
                mes: string,
                cantMsjsEnviados: number,
                cantMsjsAbiertos: number,
                cantMiembrosEnviados: number
            }
        ]
    };
    public chartVistasData: any;
    public chartElegibilidadData: any;
    public options: any;
    public data: any;

    constructor(public notificacion: Notificacion,
        public kc: KeycloakService,
        public notificacionService: NotificacionService,
        private i18nService: I18nService,
        public estadisticaService: EstadisticaService,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService) {

    }

    ngOnInit() {
        this.obtenerPromo();
        this.obtenerDatosEstadistica();
    }

    // this.statsData.periodos.map((element=>element.mes))
    poblarGraficos() {
        this.statsData.periodos.reverse();
        this.chartElegibilidadData = {
            labels: this.statsData.periodos.map((element => element.mes)),
            datasets: [
                {
                    label: this.i18nService.getLabels("notificaciones-dashboard-cant-msjs-enviados"),
                    data: this.statsData.periodos.map((element => element.cantMsjsEnviados)),
                    fill: false,
                    borderColor: '#4bc0c0'
                },
                {
                    label: this.i18nService.getLabels("notificaciones-dashboard-cant-msjs-abiertos"),
                    data: this.statsData.periodos.map((element => element.cantMsjsAbiertos)),
                    fill: false,
                    borderColor: '#afc0c0'
                }
            ]
        };
        this.chartVistasData = {
            labels: this.statsData.periodos.map((element => element.mes)),
            datasets: [
                {
                    label: this.i18nService.getLabels("notificaciones-dashboard-cant-miembros-enviados"),
                    data: this.statsData.periodos.map((element => element.cantMiembrosEnviados)),
                    fill: false,
                    borderColor: '#f3c0c0'
                }
            ]
        };
        this.data = {
            
            labels: [this.i18nService.getLabels("notificaciones-dashboard-cant-msjs-enviados"), this.i18nService.getLabels("notificaciones-dashboard-cant-msjs-abiertos")],
            datasets: [
                {
                    data: [this.statsData.cantMsjsEnviados, this.statsData.cantMsjsAbiertos],
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB"
                    ]
                }]
        };
    }

    obtenerDatosEstadistica() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.estadisticaService.token = tkn;
                let sub = this.estadisticaService.getEstadisticasNotificacion(this.notificacion.idNotificacion).subscribe(
                    (responseData: any) => {
                        this.statsData = responseData;
                        this.poblarGraficos();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /**Obtiene una notificacion a traves del API */
    obtenerPromo() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.getNotificacion(this.notificacion.idNotificacion).subscribe(
                    (chartData: Notificacion) => {
                        this.copyValuesOf(this.notificacion, chartData);
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    selectData(event) {

    }
    /**
   * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
   * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
   * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
   * no siempre sea necesario llamar al api para actualizar los datos
   */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}