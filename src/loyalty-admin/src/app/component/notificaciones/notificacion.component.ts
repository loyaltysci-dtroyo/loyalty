import { NotificacionService } from '../../service/index';
import { Notificacion,Promocion } from '../../model/index';
import { Component, OnInit } from '@angular/core';

/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Componente padre del arbol de componentes de notificacions publicitarias
 */

@Component({
    moduleId: module.id,
    selector: 'notificacion-component',
    templateUrl: 'notificacion.component.html',
    providers:[Notificacion,NotificacionService,Promocion]
})
export class NotificacionesComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
    
}
