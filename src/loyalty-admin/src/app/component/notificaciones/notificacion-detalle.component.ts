import { I18nService } from '../../service/i18n.service';
import { NotificacionService } from '../../service/index';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Response } from '@angular/http';
import { Notificacion } from '../../model/index';
import { KeycloakService } from '../../service/index';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as Rutas from '../../utils/rutas';
import { ActivatedRoute, Params } from '@angular/router'
import { PERM_ESCR_CAMPANAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Componente padre del arbol de notificaciones publicitarias, en el se muestran los detalles generales de la notificacion seleccionada
 */
@Component({
    moduleId: module.id,
    selector: 'notificaciones-detalle',
    templateUrl: 'notificacion-detalle.component.html',
    providers: [Notificacion, NotificacionService]
})
export class NotificacionesDetalleComponent implements OnInit {
    public frmActivar: FormGroup;
    public form: FormGroup;
    public optionsEvento: SelectItem[];//drop de tipos de objetivo (mision, notificacioncion o recompensa)
    public minDate: Date;
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public displayModalEliminar: boolean = false;
    constructor(public notificacion: Notificacion,
        public notificacionService: NotificacionService,
        public router: Router,
        public kc: KeycloakService,
        public i18nService: I18nService,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,
        public msgService: MessagesService) {
        this.obtenerNotificacion();
        this.route.params.forEach((params: Params) => { this.notificacion.idNotificacion = params['id'] });
        this.form = new FormGroup({
            "nombre": new FormControl('', [Validators.required]),
            "nombreInterno": new FormControl('', Validators.required),
            "drpNotificacion": new FormControl('', [Validators.required]),
            "drpEvento": new FormControl('', Validators.required),
            "drpObjetivo": new FormControl('', Validators.required)
        });
        this.frmActivar = new FormGroup({
            "drpEvento": new FormControl('', Validators.required),
            "fechaInicio": new FormControl('', Validators.required)
        });

        this.minDate = new Date();
        this.optionsEvento = (this.i18nService.getLabels("notificaciones-disparador"));
        this.authGuard.canWrite(PERM_ESCR_CAMPANAS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }

    ngOnInit() {

    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
   * Obtiene los detalles de la notificacion en el sistema
   */
    obtenerNotificacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.getNotificacion(this.notificacion.idNotificacion).subscribe(
                    (data: Notificacion) => {
                        this.copyValuesOf(this.notificacion, data);
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    cancelarNotificacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                this.notificacion.indEstado = "A";
                let sub = this.notificacionService.updateNotificacion(this.notificacion).subscribe(
                    (data: Notificacion) => {
                        this.obtenerNotificacion();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        this.obtenerNotificacion(); sub.unsubscribe();
                    }
                );
            });
    }

    eliminarNotificacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.deleteNotificacion(this.notificacion).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.router.navigate(['/notificaciones']);
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    archivarNotificacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                this.notificacion.indEstado = "C";
                let sub = this.notificacionService.updateNotificacion(this.notificacion).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.router.navigate(['/notificaciones']);
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    actualizarNotificacion() {
        if (this.notificacion.texto == "") {
            this.msgService.showMessage(this.i18nService.getLabels("error-notificacion-texto"));
        } else {
            this.kc.getToken().then(
                (tkn: string) => {
                    this.notificacionService.token = tkn;
                    this.notificacion.indEstado = "P";
                    let sub = this.notificacionService.updateNotificacion(this.notificacion).subscribe(
                        (data: Notificacion) => {
                            this.obtenerNotificacion();
                            this.msgService.showMessage(this.i18nService.getLabels("general-publicar"));
                        }, (error) => {
                            this.manejaError(error);
                        }, () => {
                            this.obtenerNotificacion(); sub.unsubscribe();
                        }
                    );
                });
        }
    }
    //Regresar a la lista
    goBack() {
        this.router.navigate([Rutas.RT_CAMPANAS_LISTA]);
    }
    /*
 * Muestra un mensaje de error al usuario con los datos de la transaccion
 * Recibe: una instancia de request con la informacion de error
 * Retorna: un observable que proporciona información sobre la transaccion
 */
    manejaError(error) {
        this.obtenerNotificacion();
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}