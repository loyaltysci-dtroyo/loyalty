import { ErrorHandlerService, MessagesService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { ActividadService, KeycloakService, I18nService } from '../../service/index';
import { Miembro, RegistroActividad } from '../../model/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { PERM_LECT_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'notificacion-actividad',
    templateUrl: 'notificacion-actividad.component.html',
    providers: [ActividadService]
})

export class NotificacionActividadComponent implements OnInit {
    public listaActividades: any[];

    /**Atributos para sorting/busqueda */
    public indPeriodo: string = '1M';
    public periodos: SelectItem[] = [];
    public lecturaMiembro: boolean; //Permiso de lectura sobre miembro

    constructor(
        public actividadService: ActividadService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public route: ActivatedRoute,
        public i18nService: I18nService,
        public router: Router,
        public authGuard: AuthGuard
    ) {
        //Permite saber si el usuario tiene permisos de lectura sobre miembro
        this.authGuard.canWrite(PERM_LECT_MIEMBROS).subscribe((permiso: boolean) => {
            this.lecturaMiembro = permiso;
        });
    }

    ngOnInit() {
        this.obtenerActividad();
        this.periodos = this.i18nService.getLabels("general-actividad-periodos");
    }

    /**
     * Obtiene los registros de actividad para el miembro actual
     */
    obtenerActividad() {
        this.kc.getToken().then((tkn) => {
            this.actividadService.token = tkn;
            let sub = this.actividadService.getActividadesNotificaciones(this.indPeriodo).subscribe(
                (response: RegistroActividad[]) => {
                    this.listaActividades = response;
                }, (response: Response) => {
                    this.handleError(response);
                }, () => {
                    sub.unsubscribe();
                }
            );
        }).catch()
    }
    /**
     * Redirige al administrador al detalle del miembro
     */
    gotoMiembro(idElemento: string) {
        this.router.navigate(['/miembros/detalleMiembro/', idElemento]);
    }
    /**
    * Redirige al administrador a la pantalla de dashboard para el elemento seleccionado
    */
    gotoNotificacion(idElemento: string) {
        this.router.navigate(['/notificacion/detalleNotificacion', idElemento]);
    }
    /**
     * Maneja el erro a nivel de componente
     */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}