import { ActivatedRoute, Router } from '@angular/router';
import { Notificacion, Segmento } from '../../../model/index';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { Response } from '@angular/http';

import { AuthGuard } from '../../common/index';
import { NotificacionService } from '../../../service/index';
import { KeycloakService } from '../../../service/index';
import { PERM_ESCR_CAMPANAS } from '../../common/auth.constants';

/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Componente encargado del despliegue y la asociacion/revocacion de segmentos asociados a una notificacion publicitaria
 * los cuales tienen precedencia sobre el publico meta de la segmento
 */
@Component({
    moduleId: module.id,
    selector: 'notificaciones-detalle-segmento',
    templateUrl: 'notificacion-detalle-segmento.component.html'
})
export class NotificacionesDetalleSegmentoComponent implements OnInit {
    public listaSegmentosDisponibles: Segmento[];//lista de segmentos disponibles para agregar a la notificacion
    public listaSegmentos: Segmento[];//lista de segmentos agregados para la notificacion
    public seleccMultipleAgregar: Segmento[];//seleccion multiple de la lista de segmentos agregados
    public seleccMultipleEliminar: Segmento[];//seleccion multiple de la lista de segmentos disponibles
    public segmentoSeleccionado: Segmento;//para cuando se elimina un segmento de manera individual
    public displayInsertar: boolean = false;//true=muestra el div de insertar segmentos
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema

    public busqueda: string; //Palabras claves de búsqueda
    public listaBusqueda: string[] = []; //Palabras de búsqueda, ChipModule

    //Elegibles
    public fila: number = 0;//offset del row actual para efectos de paginacion
    public cantRegistros: number = 9;//cantidad de registros por pagina que seran solicitados
    public cantTotalRegistros: number;//para efectos de paginacion
    //Disponibles
    public fila2: number = 0;//offset del row actual para efectos de paginacion
    public cantRegistros2: number = 9;//cantidad de registros por pagina que seran solicitados
    public cantTotalRegistros2: number;//para efectos de paginacion

    constructor(public notificacion: Notificacion,
        public notificacionService: NotificacionService,
        public kc: KeycloakService, public authGuard: AuthGuard,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService) {
        this.listaSegmentos = [];
        this.listaSegmentosDisponibles = [];
        this.seleccMultipleAgregar = [];
        this.seleccMultipleEliminar = [];
        this.displayInsertar = false;
        this.authGuard.canWrite(PERM_ESCR_CAMPANAS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }

    ngOnInit() {
        this.listarSegmentosElegibles();
        this.msgService.msgs = [];
        this.msgService.mostrarInfo("", "Por favor seleccione los segmentos que desea agregar");

    }
    /**Usado para cargar de manera perezosa los elementos de la lista de miembros disponibles */
    loadData(event) {
        this.fila = event.first;
        this.cantRegistros = event.rows;
        this.listarSegmentosElegibles();
    }

    loadData2(event) {
        this.fila2 = event.first;
        this.cantRegistros2 = event.rows;
        this.listarSegmentosDisponibles();
    }

    /**
     * Se encarga de traer la lista de segmentos que fueron agregadas como elegibles de la notificacion ppublicitaria
     */
    listarSegmentosElegibles() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.listarSegmentosNotificacion(this.notificacion.idNotificacion, "A", ['AC'], this.listaBusqueda, this.fila, this.cantRegistros).subscribe(
                    (data: Response) => {
                        if(data.headers.get("Content-Range") != null){
                           this.cantTotalRegistros =+ data.headers.get("Content-Range").split("/")[1];
                        }
                        this.displayInsertar = false;
                        this.listaSegmentos = data.json();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    //lista la segmentos disponibles para agregar
    listarSegmentosDisponibles() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.listarSegmentosNotificacion(this.notificacion.idNotificacion, "D", ['AC'], this.listaBusqueda, this.fila, this.cantRegistros).subscribe(
                    (data: Response) => {
                        if(data.headers.get("Content-Range") != null){
                           this.cantTotalRegistros =+ data.headers.get("Content-Range").split("/")[1];
                        }
                        this.displayInsertar = true;
                        this.listaSegmentosDisponibles = data.json();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    /**
     * Incluye una segmento a la lista de elegibles para la notificacion
     * Recibe: la segmento a insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    agregarSegmento(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.incluirSegmento(this.notificacion.idNotificacion, segmento.idSegmento).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("Insertado", "Los datos han sido insertados");
                        this.listarSegmentosElegibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Excluye una segmento de la lista de elegibles para la notificacion
     * Recibe: la segmento a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarSegmento(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.eliminarSegmento(this.notificacion.idNotificacion, segmento.idSegmento).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("Eliminado", "Los datos han sido eliminados");
                        this.listarSegmentosElegibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Agrega una coleccion de segmentos a la lista de elegibles para la notificacion
     * Recibe: la segmento a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    agregarMultipleSegmento() {
        if (this.seleccMultipleAgregar.length > 0) {
            this.kc.getToken().then(
                (tkn: string) => {
                    this.notificacionService.token = tkn;
                    let ids = this.seleccMultipleAgregar.map((elem) => { return elem.idSegmento });

                    let sub = this.notificacionService.incluirListaSegmento(this.notificacion.idNotificacion, ids).subscribe(
                        (data) => {
                            this.msgService.mostrarInfo("Insertado", "Los datos han sido insertados");
                            this.listarSegmentosElegibles();
                            this.seleccMultipleAgregar = [];
                        },
                        (error) => { this.manejaError(error); },
                        () => { sub.unsubscribe() }
                    );
                });
        }
    }
    /**
     * Agrega una coleccion de segmentos a la lista de elegibles para la notificacion
     * Recibe: la segmento a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarMultipleSegmento() {
        if (this.seleccMultipleEliminar.length > 0) {
            this.kc.getToken().then(
                (tkn: string) => {
                    this.notificacionService.token = tkn;
                    let ids = this.seleccMultipleEliminar.map((elem) => { return elem.idSegmento });
                    let sub = this.notificacionService.eliminarListaSegmento(this.notificacion.idNotificacion, ids).subscribe(
                        (data) => {
                            this.msgService.mostrarInfo("Eliminado", "Los datos han sido eliminados");
                            this.listarSegmentosElegibles();
                            this.seleccMultipleEliminar = [];
                        },
                        (error) => { this.manejaError(error); },
                        () => { sub.unsubscribe() }
                    );
                });
        }
    }

    /*
    * Muestra un mensaje de error al usuario con los datos de la transaccion
    * Recibe: una instancia de request con la informacion de error
    * Retorna: un observable que proporciona información sobre la transaccion
    */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}