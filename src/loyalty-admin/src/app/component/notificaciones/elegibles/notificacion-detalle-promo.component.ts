import { ActivatedRoute, Router } from '@angular/router';
import { Notificacion, Promocion } from '../../../model/index';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { AuthGuard } from '../../common/index';
import { NotificacionService } from '../../../service/index';
import { KeycloakService } from '../../../service/index';
import { PERM_ESCR_CAMPANAS } from '../../common/auth.constants';

/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Componente encargado exlclusivamente del despliegue y la asociacion/revocacion de promociones ligadas a una camana publicitaria
 */
@Component({
    moduleId: module.id,
    selector: 'notificaciones-detalle-promo',
    templateUrl: 'notificacion-detalle-promo.component.html'
})
export class NotificacionesDetallePromoComponent implements OnInit {
    public listaPromocionesDisponibles: Promocion[];
    public promocionSelecionada: Promocion;
    public listaPromociones: Promocion[];
    public seleccMultipleAgregar: Promocion[];//seleccion multiple de la lista de promociones agregados
    public seleccMultipleEliminar: Promocion[];//seleccion multiple de la lista de promociones disponibles
    public displayInsertar: boolean;//true=muestra el div de insertar promociones
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema

    constructor(public notificacion: Notificacion,
        public notificacionService: NotificacionService,
        public kc: KeycloakService,
        public route: ActivatedRoute, public authGuard: AuthGuard,
        public router: Router,
        public msgService: MessagesService) {
        this.displayInsertar = false;
        this.listaPromocionesDisponibles = [];
        this.listaPromociones = [];
        this.seleccMultipleAgregar = [];
        this.seleccMultipleEliminar = [];

        this.authGuard.canWrite(PERM_ESCR_CAMPANAS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }

    ngOnInit() {
        this.listarPromocionesElegibles();

    }
    /**
     * Se encarga de traer la lista de promociones que fueron agregadas como elegibles de la notificacion ppublicitaria
     */
    listarPromocionesElegibles() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.listarPromocionesElegiblesNotificacion(this.notificacion.idNotificacion).subscribe(
                    (data: Promocion[]) => {
                        this.displayInsertar = false;
                        this.listaPromociones = data;
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    /**lista la promociones disponibles para agregar
     * a la lista de elegibles de la campaña
    */
    listarPromocionesDisponibles() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.listarPromocionesDisponiblesNotificacion(this.notificacion.idNotificacion).subscribe(
                    (data: Promocion[]) => {
                        this.displayInsertar = true;
                        this.listaPromocionesDisponibles = data;
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    /**
     * Incluye una promocion a la lista de elegibles para la notificacion
     * Recibe: la promocion a insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    agregarPromocion(promocion: Promocion) {
      
            this.kc.getToken().then(
                (tkn:string) => {
                    this.notificacionService.token = tkn;
                    let sub = this.notificacionService.incluirPromocion(this.notificacion.idNotificacion, promocion.idPromocion).subscribe(
                        (data) => {
                            this.msgService.mostrarInfo("Insertado", "Los datos han sido insertados");
                            this.listarPromocionesDisponibles();
                        },
                        (error) => { this.manejaError(error); },
                        () => { sub.unsubscribe() }
                    );
                });
       
    }
    /**
     * Excluye una promocion de la lista de elegibles para la notificacion
     * Recibe: la promocion a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarPromocion(promocion: Promocion) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.eliminarPromocion(this.notificacion.idNotificacion, promocion.idPromocion).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("Eliminado", "Los datos han sido eliminados");
                        this.listarPromocionesElegibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Agrega una coleccion de promociones a la lista de elegibles para la notificacion
     * Recibe: la segmento a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    agregarMultiplePromocion() {
        if (this.seleccMultipleAgregar.length > 0) {
            this.kc.getToken().then(
                (tkn:string) => {
                    this.notificacionService.token = tkn;
                    let ids = this.seleccMultipleAgregar.map((elem) => { return elem.idPromocion });
                    let sub = this.notificacionService.incluirListaPromocion(this.notificacion.idNotificacion, ids).subscribe(
                        (data) => {
                            this.msgService.mostrarInfo("Insertado", "Los datos han sido insertados");
                            this.listarPromocionesElegibles();
                            this.seleccMultipleAgregar = [];

                        },
                        (error) => { this.manejaError(error); },
                        () => { sub.unsubscribe() }
                    );
                });
        }
    }
    /**
     * Agrega una coleccion de promociones a la lista de elegibles para la notificacion
     * Recibe: la segmento a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarMultiplePromocion() {
        if (this.seleccMultipleEliminar.length > 0) {
            this.kc.getToken().then(
                (tkn:string) => {
                    this.notificacionService.token = tkn;
                    let ids = this.seleccMultipleEliminar.map((elem) => { return elem.idPromocion });
                    let sub = this.notificacionService.eliminarListaPromocion(this.notificacion.idNotificacion, ids).subscribe(
                        (data) => {
                            this.msgService.mostrarInfo("Eliminado", "Los datos han sido eliminados");
                            this.listarPromocionesElegibles();
                            this.seleccMultipleEliminar = [];
                        },
                        (error) => { this.manejaError(error); },
                        () => { sub.unsubscribe() }
                    );
                });
        }
    }

    /*
    * Muestra un mensaje de error al usuario con los datos de la transaccion
    * Recibe: una instancia de request con la informacion de error
    * Retorna: un observable que proporciona información sobre la transaccion
    */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}