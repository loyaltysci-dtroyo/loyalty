import { ActivatedRoute, Router } from '@angular/router';
import { Notificacion, Miembro } from '../../../model/index';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { Response } from '@angular/http';

import { AuthGuard } from '../../common/index';
import { NotificacionService } from '../../../service/index';
import { KeycloakService } from '../../../service/index';
import { PERM_ESCR_CAMPANAS } from '../../common/auth.constants';

/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Componente encargado exclusivamente del despliegue y la asociacion/revocacion de miembros asociados a una Notificacion publicitaria
 * los cuales tienen precedencia sobre el publico meta de la miembro
 */
@Component({
    moduleId: module.id,
    selector: 'notificaciones-detalle-miembros',
    templateUrl: 'notificacion-detalle-miembros.component.html'
})
export class NotificacionesDetalleMiembrosComponent implements OnInit {
    public listaMiembrosDisponibles: Miembro[];//lista de miembros disponibles para agregar a la Notificacion
    public listaMiembros: Miembro[];//lista de miembros agregados para la Notificacion
    public miembroSeleccionado: Miembro;
    public seleccMultipleAgregar: Miembro[];//seleccion multiple de la lista de miembros agregados
    public seleccMultipleEliminar: Miembro[];//seleccion multiple de la lista de miembros disponibles
    public displayInsertar: boolean;//true=muestra el div de insertar miembros
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    
    //Elegibles
    public rowOffset: number = 0;//offset del row actual para efectos de paginacion
    public cantRegistros: number = 9;//cantidad de registros por pagina que seran solicitados
    public cantTotalRegistros: number;//para efectos de paginacion
    //Disponibles
    public rowOffset2: number = 0;//offset del row actual para efectos de paginacion
    public cantRegistros2: number = 9;//cantidad de registros por pagina que seran solicitados
    public cantTotalRegistros2: number;//para efectos de paginacion

    public busqueda: string; //Palabras claves de búsqueda
    public listaBusqueda: string[] = []; //Palabras de búsqueda, ChipModule

    constructor(public Notificacion: Notificacion,
        public notificacionService: NotificacionService,
        public kc: KeycloakService, public authGuard: AuthGuard,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService) {
        this.listaMiembros = []; this.displayInsertar = false;
        this.listaMiembrosDisponibles = [];
        this.seleccMultipleAgregar = [];
        this.seleccMultipleEliminar = [];
        this.authGuard.canWrite(PERM_ESCR_CAMPANAS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }

    ngOnInit() {
        this.listarMiembrosElegibles();
        this.msgService.msgs=[];
        this.msgService.mostrarInfo("", "Por favor seleccione los miembros que desea agregar");

    }

    /**Usado para cargar de manera perezosa los elementos de la lista de miembros disponibles */
    loadData(event) {
        this.rowOffset = event.first;
        this.cantRegistros = event.rows;
        this.listarMiembrosElegibles();
    }

    loadData2(event) {
        this.rowOffset2 = event.first;
        this.cantRegistros2 = event.rows;
        this.listarMiembrosDisponibles();
    }

    /**
     * Se encarga de traer la lista de miembros que fueron agregadas como elegibles de la Notificacion ppublicitaria
     */
    listarMiembrosElegibles() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.kc.getToken().then(
            (tkn:string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.listarMiembrosElegiblesNotificacion(this.Notificacion.idNotificacion, this.rowOffset, this.cantRegistros,this.busqueda ).subscribe(
                    (response: Response) => {
                        this.listaMiembros = response.json();
                        if(response.headers.get("Content-Range") != null){
                           this.cantTotalRegistros =+ response.headers.get("Content-Range").split("/")[1];
                        }
                        this.displayInsertar = false;
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    //lista la miembros disponibles para agregar
    listarMiembrosDisponibles() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.kc.getToken().then(
            (tkn:string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.listarMiembrosDisponiblesNotificacion(this.Notificacion.idNotificacion, this.rowOffset2, this.cantRegistros2,this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaMiembrosDisponibles = response.json();
                        if(response.headers.get("Content-Range") != null){
                           this.cantTotalRegistros2 =+ response.headers.get("Content-Range").split("/")[1];
                        }
                        this.displayInsertar = true;
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    /**
     * Incluye una miembro a la lista de elegibles para la Notificacion
     * Recibe: la miembro a insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    agregarMiembro(miembro: Miembro) {
        if (this.tienePermisosEscritura) {
            this.kc.getToken().then(
                (tkn:string) => {
                    this.notificacionService.token = tkn;
                    let sub = this.notificacionService.incluirMiembro(this.Notificacion.idNotificacion, miembro.idMiembro).subscribe(
                        (data) => {
                            this.msgService.mostrarInfo("Insertado", "Los datos han sido insertados");
                            this.listarMiembrosElegibles();
                        },
                        (error) => { this.manejaError(error); },
                        () => { sub.unsubscribe() }
                    );
                });
        }
    }
    /**
     * Excluye una miembro de la lista de elegibles para la Notificacion
     * Recibe: la miembro a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarMiembro(miembro: Miembro) {
        if (this.tienePermisosEscritura) {
            this.kc.getToken().then(
                (tkn:string) => {
                    this.notificacionService.token = tkn;
                    let sub = this.notificacionService.eliminarMiembro(this.Notificacion.idNotificacion, miembro.idMiembro).subscribe(
                        (data) => {
                            this.msgService.mostrarInfo("Eliminado", "Los datos han sido eliminados");
                            this.listarMiembrosElegibles();
                        },
                        (error) => { this.manejaError(error); },
                        () => { sub.unsubscribe() }
                    );
                });


        }
    }
    /**
     * Agrega una coleccion de miembros a la lista de elegibles para la Notificacion
     * Recibe: la segmento a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    agregarMultipleMiembro() {
        if (this.tienePermisosEscritura) {
            if (this.seleccMultipleAgregar.length > 0) {
                this.kc.getToken().then(
                    (tkn:string) => {
                        this.notificacionService.token = tkn;
                        let ids = this.seleccMultipleAgregar.map((elem) => { return elem.idMiembro });
                        let sub = this.notificacionService.incluirListaMiembros(this.Notificacion.idNotificacion, ids).subscribe(
                            (data) => {
                                this.msgService.mostrarInfo("Insertado", "Los datos han sido insertados");
                                this.listarMiembrosElegibles();
                                this.seleccMultipleAgregar = [];;
                            },
                            (error) => { this.manejaError(error); },
                            () => { sub.unsubscribe() }
                        );
                    });
            }
        }
    }
    /**
     * Agrega una coleccion de miembros a la lista de elegibles para la Notificacion
     * Recibe: la segmento a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarMultipleMiembro() {
        if (this.tienePermisosEscritura) {
            if (this.seleccMultipleEliminar.length > 0) {
                this.kc.getToken().then(
                    (tkn:string) => {
                        this.notificacionService.token = tkn;
                        let ids = this.seleccMultipleEliminar.map((elem) => { return elem.idMiembro });
                        let sub = this.notificacionService.eliminarListaMiembros(this.Notificacion.idNotificacion, ids).subscribe(
                            (data) => {
                                this.msgService.mostrarInfo("Eliminado", "Los datos han sido eliminados");
                                this.listarMiembrosElegibles();
                                this.seleccMultipleEliminar = [];
                            },
                            (error) => { this.manejaError(error); },
                            () => { sub.unsubscribe() }
                        );
                    });
            }
        }
    }

    /*
    * Muestra un mensaje de error al usuario con los datos de la transaccion
    * Recibe: una instancia de request con la informacion de error
    * Retorna: un observable que proporciona información sobre la transaccion
    */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}