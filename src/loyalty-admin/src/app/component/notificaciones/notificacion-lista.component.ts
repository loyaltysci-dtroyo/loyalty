import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { NotificacionService } from '../../service/index';
import { Component, OnInit, } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/primeng';
import { Response } from '@angular/http';
import { KeycloakService } from '../../service/index';
import { Notificacion } from '../../model/index';
/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Componente encargado de desplegar la lista de notificaciones publicitarias en el sistema e identificarlas con un indicador visual de estado
 */
@Component({
    moduleId: module.id,
    selector: 'notificaciones-lista',
    templateUrl: 'notificacion-lista.component.html'
})
export class NotificacionesListaComponent implements OnInit {
    public listaNotificaciones: Notificacion[];//lista de notificaciones
    public notificacionEliminar: Notificacion;//variable para almacenar la notificacion a eliminar
    public terminosBusqueda: string[] = [];// termino de busqueda
    public displayModalEliminar: boolean = false;//true= se muestra el modal
    public displayModalMensaje: boolean = false;//true= se muestra el modal
    public estados: string[] = ["B", "P", "E"];//notificacions que se desplegaran segun el estado
    public displayModalConfirm: boolean;//se muestra el modal de confirmacion cuando este valor es true
    public itemsBusqueda: MenuItem[] = [];//usado para poblar el split button de terminos avanzados de busqueda
    public busquedaAvanzada: boolean = false;//usadopara desplegar el div de busqueda avanzada
    public rowOffset: number;//offset del row actual para efectos de paginacion
    public cantRegistros: number;//cantidad de registros por pagina que seran solicitados
    public cantTotalRegistros: number;//para efectos de paginacion
    public idNotificacion: string;
    public accion: string; //Accion a realizar sobre la notificacion seleccionada
    /**
     * Parametros para busqueda avanzada
     */
     public tipos:string[]=[];
     public filtros:string[]=[];
     public eventos:string[]=[];
     public objetivos:string[]=[];
     public tipoOrden:string;
     public campoOrden:string;

    constructor(public router: Router,
        public kc: KeycloakService,public i18nService:I18nService,
        public msgService: MessagesService,
        public notificacionService: NotificacionService
    ) {
        this.itemsBusqueda = [
            { label: 'Avanzado', icon: 'ui-icon-build', command: () => { this.busquedaAvanzada = !this.busquedaAvanzada } }
        ];
    }

    ngOnInit() {
        this.buscarNotificaciones();
    }

    selectNotificacionEliminar(notificacion: Notificacion) {
        this.notificacionEliminar = notificacion;
    }

    confirmar(notificacion: Notificacion, accion: string) {
        this.notificacionEliminar = notificacion;
        this.accion = accion;
        this.displayModalEliminar = true;
    }

    /**Elimina una notificacion del sistema */
    eliminarNotificacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                let sub = this.notificacionService.deleteNotificacion(this.notificacionEliminar).subscribe(
                    () => {
                        this.displayModalEliminar = false;
                        if(this.accion == 'eliminar'){
                            this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        }else{
                            this.msgService.showMessage(this.i18nService.getLabels("general-archivar"));
                        }
                        this.buscarNotificaciones();
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe() }
                );
            });
    }

    /*Metodo que reririge al detalle del segmento que recibe por parámetros
   * Recibe: el segmento al cual redirigir al detalle
   */
    gotoDetail(notificacion: Notificacion) {
        let link = ['notificaciones/detalleNotificacion', notificacion.idNotificacion];
        this.router.navigate(link);

    }


    /**Usado para cargar de manera perezosa los elementos de la lista de Notificaciones */
    loadData(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantRegistros = event.rows;
    }

    /**Metodo encargado de cargar las Notificaciones de la base de datos
     * Utiliza el string de busqueda que fue seteado por el campo de busqueda
     */
    buscarNotificaciones() {
        this.listaNotificaciones = null;
        if (this.terminosBusqueda) {
            this.kc.getToken().then(
                (tkn: string) => {
                    this.notificacionService.token = tkn;
                    let sub = this.notificacionService.buscarNotificaciones(this.terminosBusqueda, this.estados, this.rowOffset, this.cantRegistros, this.tipos, this.filtros, this.eventos, this.objetivos, this.tipoOrden, this.campoOrden).subscribe(
                        (data: Response) => {
                            this.listaNotificaciones = <Notificacion[]>data.json();
                            data.headers.get("content-range")
                        },
                        (error) => {
                            this.manejaError(error);
                        },
                        () => { sub.unsubscribe(); }
                    );
                });
        }
    }

    /*
      * Muestra un mensaje de error al usuario con los datos de la transaccion
      * Recibe: una instancia de request con la informacion de error
      * Retorna: un observable que proporciona información sobre la transaccion
      */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}