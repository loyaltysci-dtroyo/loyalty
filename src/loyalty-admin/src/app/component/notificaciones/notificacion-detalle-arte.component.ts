import { I18nService } from '../../service/i18n.service';
import { AppConfig } from '../../app.config';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Response } from '@angular/http';
import { Notificacion } from '../../model/index'; import { KeycloakService, NotificacionService } from '../../service/index';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as Rutas from '../../utils/rutas';
import { PERM_ESCR_CAMPANAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ViewChild } from '@angular/core';
import { Editor } from 'primeng/primeng';
declare var grapesjs: any;
declare var CKEDITOR: any;
/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Componente encargado de mostrar el arte de las notificacions publicitarias,
 * en este componente el usuario puede definir el mensaje que se enviara por correo 
 * o en su defecto un nombre y un texto qe se desplegara en las push notifications
 */
@Component({
    moduleId: module.id,
    selector: 'notificaciones-detalle-arte',
    templateUrl: 'notificacion-detalle-arte.component.html'
})
export class NotificacionesDetalleArteComponent implements OnInit {

    public file_src: any;//src (url o base64) de la imagen, es el campo que va en el source de la imagen
    public file: any;//temporal para el tratamiento de la imagen
    public formEmail: FormGroup;//validacion en caso de que sea una notificacion por correo
    public formNotificacion: FormGroup;//validacion en caso de que sea notificacion push
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public subiendoContenido: boolean = false;
    public displayContenido: boolean = false;

    constructor(
        public i18nService: I18nService,
        public notificacion: Notificacion,
        public notificacionService: NotificacionService,
        public router: Router,
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,
        public msgService: MessagesService) {
        this.formEmail = new FormGroup({
            "encabezadoArte": new FormControl('', [Validators.required]),
            "texto": new FormControl('', [Validators.required])
        });
        this.formNotificacion = new FormGroup({
            "texto": new FormControl('', [Validators.required]),
            "encabezadoArte": new FormControl('', [Validators.required])
        });
        this.authGuard.canWrite(PERM_ESCR_CAMPANAS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }
    ngOnInit() {
        this.obtenerNotificacion();

    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
  * Obtiene los detalles de la notificacion en el sistema, se usa para obtener el texto de la notificacion
  */
    obtenerNotificacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                let sub = this.notificacionService.getNotificacion(this.notificacion.idNotificacion).subscribe(
                    (data: Notificacion) => {
                        this.copyValuesOf(this.notificacion, data);
                        if (this.notificacion.imagenArte) {
                            this.file_src = this.notificacion.imagenArte;
                        } else {
                            this.file_src = AppConfig.DEFAULT_IMG_URL;
                        }
                        if (this.notificacion.indTipo == 'E') {
                            //this.notificacion.texto = "";
                            this.initEditor();


                        }
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Encargado de actualizar el valor del despliegue de la notificacion
     * emplea la variable global notificacion para obtener los datos de la notificacion
     */
    actualizarNotificacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.notificacionService.token = tkn;
                this.subiendoContenido = true;
                this.msgService.showMessage(this.i18nService.getLabels("info-uploading"));
                let sub = this.notificacionService.updateNotificacion(this.notificacion).subscribe(
                    (data: Notificacion) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.obtenerNotificacion();
                        this.subiendoContenido = true;
                    }, (error) => {
                        this.manejaError(error);
                        this.subiendoContenido = false;
                    }, () => {
                        this.subiendoContenido = false;
                        sub.unsubscribe();
                    }
                );
            });
    }

    initEditor() {
        var editor = grapesjs.init({
            container: '#gjs',
            components: ``,
            style: '.txt-red{color: red}',
            plugins: ['gjs-preset-newsletter', 'gjs-plugin-ckeditor'],
            commands: {
                defaults: [{
                    id: 'storeData',
                    run: function (editor, senderBtn) {
                        editor.store();
                        console.log(editor, senderBtn);
                    },
                }]
            },
            assetManager: {
                storageType: '',
                upload: 'http://localhost/uploadImage.php',
                storeOnChange: true,
                storeAfterUpload: true
            },
            pluginsOpts: {
                'gjs-preset-newsletter': {
                    modalLabelImport: 'Ingrese el código en el siguiente campo de texto y seleccione "Importar"',
                    modalLabelExport: 'Copy the code and use it wherever you want',
                    codeViewerTheme: 'material',
                    //defaultTemplate: templateImport,
                    importPlaceholder: '',
                    cellStyle: {
                        'font-size': '12px',
                        'font-weight': 300,
                        'vertical-align': 'top',
                        color: 'rgb(111, 119, 125)',
                        margin: 0,
                        padding: 0,
                    }
                },
                'gjs-plugin-ckeditor': {
                    position: 'center',
                    options: {
                        startupFocus: true,
                        extraAllowedContent: '*(*);*{*}', // Allows any class and any inline style
                        allowedContent: true, // Disable auto-formatting, class removing, etc.
                        enterMode: CKEDITOR.ENTER_BR,
                        toolbar: [
                            { name: 'styles', items: ['Font', 'FontSize'] },
                            ['Bold', 'Italic', 'Underline', 'Strike'],
                            { name: 'paragraph', items: ['NumberedList', 'BulletedList'] },
                            { name: 'links', items: ['Link', 'Unlink'] },
                            { name: 'colors', items: ['TextColor', 'BGColor'] },
                        ],
                    }
                }
            }

        });
        var mdlClass = 'gjs-mdl-dialog-sm';
        var pnm = editor.Panels;
        var cmdm = editor.Commands;
        var md = editor.Modal;
        var infoContainer = document.getElementById("info-cont");
        console.log(md, localStorage);
        pnm.addButton('options', {
            id: 'clear-all',
            className: 'fa fa-trash icon-blank',
            attributes: { title: 'Empty canvas' },
            command: {
                run: function (editor, sender) {
                    sender && sender.set('active', false);
                    if (confirm('Are you sure to clean the canvas?')) {
                        editor.DomComponents.clear();
                        setTimeout(function () {
                            localStorage.clear()
                        }, 0)
                    }
                }
            }
        });

    }

    /*
    * Muestra un mensaje de error al usuario con los datos de la transaccion
    * Recibe: una instancia de request con la informacion de error
    * Retorna: un observable que proporciona información sobre la transaccion
    */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {
        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            // Obtener el (base64 de la imagen)
            img.src = event.target.result;
            this.notificacion.imagenArte = img.src.split(",")[1];
            // Redimensionar la imagen
            this.file_src = this.resize(img);
        }, false);

        reader.readAsDataURL(input.files[0]);
    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");
        // console.log("Size Before: " + img.src.length + " bytes");
        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        // console.log("Size After:  " + dataUrl.length + " bytes");
        return dataUrl
    }
}

