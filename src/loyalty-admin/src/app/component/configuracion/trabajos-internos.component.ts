import { AppConfig } from '../../app.config';
import { MessagesService, ErrorHandlerService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { Http, RequestOptionsArgs, Headers, Response } from '@angular/http';
import { TrabajoInterno } from '../../model/index';
import { TrabajoInternoService, KeycloakService, I18nService } from '../../service/index';
import { Message, SelectItem } from "primeng/primeng";
import { PERM_ESCR_CONFIGURACION } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'trabajos-internos',
    templateUrl: 'trabajos-internos.component.html',
    providers: [TrabajoInterno, TrabajoInternoService],
})

/**
 * Jaylin Centeno
 * Componente para visualizar el estado y tipo de los trabajos internos
 */
export class TrabajoInternoComponent implements OnInit {

    public tipo: string[] = [];
    public estado: string[] = [];
    public ordenTipo: string;
    public ordenCampo: string;
    public itemsTipo: SelectItem[] = []; //Lista con los tipos de trabajo
    public itemsEstado: SelectItem[] = []; //Lista con los estados de trabajo
    public cantidad: number = 10; //Cantidad de filas a mostrar en la tabla
    public totalRecords: number; //Total de trabajos en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public cargandoDatos: boolean;
    public listaVacia: boolean = true;
    public listaTrabajos: TrabajoInterno[];
    public permisoConfig: boolean;

    constructor(
        public trabajoService: TrabajoInternoService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService,
        public authGuard: AuthGuard,
    ) {
        //Tipos y estados seleccionados por default
        this.tipo = ['N', 'E', 'I'];
        this.estado = ['P', 'C', 'F'];
        //Se obtienen las listas creadas con valores predeterminados
        this.itemsTipo = this.i18nService.getLabels("trabajo-interno-tipo");
        this.itemsEstado = this.i18nService.getLabels("trabajo-interno-estado");

        //Se verifica si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_CONFIGURACION).subscribe((permiso: boolean) => {
            this.permisoConfig = permiso;
            this.obtenerTrabajos();
        });
    }

    //LLamado una sola vez
    ngOnInit() {
        this.obtenerTrabajos();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerTrabajos();
    }

    // Método para obtener los trabajos internos
    obtenerTrabajos() {
        if (this.permisoConfig) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.trabajoService.token = token;
                    this.trabajoService.obtenerTrabajos(this.tipo, this.estado, this.cantidad, this.filaDesplazamiento, this.ordenTipo, this.ordenCampo).subscribe(
                        (response: Response) => {
                            this.listaTrabajos = response.json();
                            if (response.headers.get("Content-Range") != null) {
                                this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                            }
                            if (this.listaTrabajos.length > 0) {
                                this.listaVacia = false;
                            } else {
                                this.listaVacia = true;
                            }
                            this.cargandoDatos = false;
                        },
                        (error) => {
                            this.manejaError(error);
                            this.cargandoDatos = false;
                        });
                });
        }
    }

    // Método para manejar los errores que se provocan en las transacciones
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}