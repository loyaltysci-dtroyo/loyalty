import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Configuracion } from '../../model/index';
import { ConfiguracionService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_CONFIGURACION } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import * as Rutas from '../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'configuracion-politica-component',
    templateUrl: 'configuracion-politica.component.html',
    providers: [Configuracion, ConfiguracionService],
})

/**
 * Jaylin Centeno
 * Componente que permite subir un archivo con las políticas del sistema
 */
export class ConfiguracionPoliticaComponent {

    public escritura: boolean; //Permite verificar permisos (true/false)
    public subiendoDatos: boolean = false;
    public cargandoDatos: boolean = true;
    public imagen: any;
    public politicas: any;
    public nombreArchivo: string = ""; //Para mostrar el nombre del archivo seleccionado

    constructor(
        public configuracion: Configuracion,
        public configuracionService: ConfiguracionService,
        public router: Router,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public i18nService: I18nService,
        public msgService: MessagesService
    ) {

        //Permite obtener si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_CONFIGURACION).subscribe(
            (permiso: boolean) => {
                this.escritura = permiso;
            });
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarConfiguracion();
    }

    //Método que se encarga de traer la configuración del sistema
    mostrarConfiguracion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.configuracionService.token = token;
                this.configuracionService.obtenerConfiguracion().subscribe(
                    (data) => {
                        this.configuracion = data;
                        this.politicas = this.configuracion.politicasSeguridad;
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    //Método para actualizar la configuración general del sistema
    actualizarConfiguracion() {
        this.subiendoDatos = true;
        this.msgService.showMessage(this.i18nService.getLabels("general-actualizando-datos"));
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.configuracionService.token = token;
                let sub = this.configuracionService.actualizarConfiguracion(this.configuracion).subscribe(
                    (data) => {
                        this.subiendoDatos = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.mostrarConfiguracion();
                    },
                    (error) => {
                        this.subiendoDatos = false;
                        this.manejaError(error)
                    },
                    () => { sub.unsubscribe(); }
                );
            }
        );
    }

    //Obtiene las imagenes que sube el usuario
    subirImagen() {
        this.imagen = document.getElementById("politicas");
        this.nombreArchivo = this.imagen.files[0].name;
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.politicas = reader.result;
            let urlBase64 = this.politicas.split(",");
            this.configuracion.politicasSeguridad = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}