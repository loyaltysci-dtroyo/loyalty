import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Configuracion, MedioPago } from '../../model/index';
import { ConfiguracionService, MedioPagoService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_CONFIGURACION } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { AppConfig } from '../../app.config';

@Component({
    moduleId: module.id,
    selector: 'configuracion-medios-component',
    templateUrl: 'configuracion-medios.component.html',
    providers: [Configuracion, MedioPago, ConfiguracionService, MedioPagoService],
})
/**
 * Jaylin Centeno
 * Componente que permite administrar los medios de pago de la configuracion
 */
export class ConfiguracionMediosComponent implements OnInit {

    public formPagos: FormGroup;
    public listaMedios: MedioPago[]; //Lista de medios de pago
    public listaVacia: boolean = true; //Permite verificar si la lista esta vacía
    public idMedio: string; //Contiene el id del medio de pago seleccionado
    public permisoConfig: boolean; //Permite verificar si el usuario tiene permisos (true/false)
    public totalRecords: number; //Total de medios en el sistema
    public cantidad: number = 10; //Cantidad de filas a mostrar en la tabla
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public nuevo: boolean = false; //Habilita el cuadro de dialogo para agregar un medio
    public subiendoImagen: boolean = false; //Utilizado para mostrar la carga de imagen
    public imagenMedio: any; //Represent 
    public imgTemporal: any; //Imagen temporal
    public imagen: any;
    public cargandoDatos: boolean = true
    public eliminar: boolean;

    constructor(
        public medioPago: MedioPago,
        public configuracion: Configuracion,
        public medioService: MedioPagoService,
        public configuracionService: ConfiguracionService,
        public router: Router,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Imagen por default
        this.imagenMedio = this.imgTemporal = `${AppConfig.DEFAULT_IMG_URL}`;
        this.formPagos = new FormGroup({
            'nombre': new FormControl('', Validators.required)
        })

        this.authGuard.canWrite(PERM_ESCR_CONFIGURACION).subscribe(
            (permiso: boolean) => {
                this.permisoConfig = permiso;
            });
    }

    /**
     * Método que se inicia al entrar por primera vez al componente
     */ 
    ngOnInit() {
        this.mostrarConfiguracion();
        this.obtenerListaMedios();
    }

    /**
     * Método llamado para la carga del datalist
     */
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerListaMedios();
    }

    confirmar(idMedio: string) {
        this.eliminar = true;
        this.idMedio = idMedio;
    }

    /**
     * Método que se encarga de traer la configuración del sistema
     */ 
    mostrarConfiguracion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.configuracionService.token = token;
                this.configuracionService.obtenerConfiguracion().subscribe(
                    (data) => {
                        this.configuracion = data;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Método para obtener los medio de pago disponible
     */ 
    obtenerListaMedios() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.medioService.token = token;
                this.medioService.obtenerListaMedios(this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaMedios = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaMedios.length > 0) {
                            this.listaVacia = false;
                        } else { this.listaVacia = true; }
                        this.cargandoDatos = false;

                    }, (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    /**
     * Método para insertar formas de pago
     */
    insertarFormaPago() {
        if (this.medioPago.nombre.trim() == '') {
            this.msgService.showMessage(this.i18nService.getLabels("warn-atributo-valor"));
            return;
        }
        if (this.medioPago.imagen == null) {
            this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-imagen"));
            return;
        }

        this.subiendoImagen = true;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.medioService.token = token;
                let sub = this.medioService.insertarMedioPago(this.medioPago).subscribe(
                    () => {
                        this.nuevo = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.obtenerListaMedios();
                        this.formPagos.reset();
                        this.imagenMedio = this.imgTemporal;

                    }, (error) => {
                        this.manejaError(error);
                        this.nuevo = false;
                        this.subiendoImagen = false;
                        this.formPagos.reset();
                        this.imagenMedio = this.imgTemporal;
                    },
                    () => { sub.unsubscribe(); this.subiendoImagen = false; }
                );
            }
        );

    }

    /**
     * Método para eliminar las formas de pago del sistema
     */ 
    eliminarFormasPago() {
        this.eliminar = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.medioService.token = token;
                this.medioService.eliminarMedio(this.idMedio).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.obtenerListaMedios();
                    }, (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    /**
     * Método utilizado para subir archivos
     */
    subirImagen() {
        this.imagen = document.getElementById("imagenMedio");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.imagenMedio = reader.result;
            let urlBase64 = this.imagenMedio.split(",");
            this.medioPago.imagen = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}