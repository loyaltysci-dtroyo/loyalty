export { ConfiguracionComponent } from './configuracion.component';
export { ConfiguracionGeneralComponent } from './configuracion-general.component';
export { ConfiguracionMediosComponent } from './configuracion-medios.component';
export { ConfiguracionPoliticaComponent } from './configuracion-politica.component';
export { ConfiguracionParamsComponent } from './configuracion-params.component';
export { ConfiguracionEvertecComponent } from './configuracion-evertec.component'