import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Configuracion } from '../../model/index';
import { ConfiguracionService, KeycloakService } from '../../service/index';
import { PERM_ESCR_CONFIGURACION } from '../common/auth.constants';
import * as Rutas from '../../utils/rutas';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'configuracion-component',
    templateUrl: 'configuracion.component.html',
    providers: [Configuracion, ConfiguracionService],
})

/**
 * Jaylin Centeno
 * Componente padre, permite administrar la configuración general del sistema
 */
export class ConfiguracionComponent {

    public general = Rutas.RT_CONFIGURACION_GENERAL;
    public medio = Rutas.RT_CONFIGURACION_MEDIOS;
    public politica = Rutas.RT_CONFIGURACION_POLITICA;
    public params = Rutas.RT_CONFIGURACION_PARAMS;
    public evertec = Rutas.RT_CONFIGURACION_EVERTEC;
    public permisoConfig: boolean;
    
    constructor(
        public configuracion: Configuracion,
        public configuracionService: ConfiguracionService,
        public router: Router,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public authGuard: AuthGuard,
    ) { 
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_CONFIGURACION).subscribe((permiso: boolean) => {
            this.permisoConfig = permiso;
        });
     }

    /**
     * Método que se inicia al entrar por primera vez al componente
     */ 
    ngOnInit() {
        this.mostrarConfiguracion();
    }

    /**
     * Método que se encarga de traer la configuración del sistema
     */ 
    mostrarConfiguracion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.configuracionService.token = token;
                this.configuracionService.obtenerConfiguracion().subscribe(
                    (data) => {
                        this.configuracion = data;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}