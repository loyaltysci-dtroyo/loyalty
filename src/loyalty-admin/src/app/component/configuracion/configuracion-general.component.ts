import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Configuracion, Ubicacion } from '../../model/index';
import { ConfiguracionService, KeycloakService, I18nService, UbicacionService } from '../../service/index';
import { PERM_ESCR_CONFIGURACION, PERM_LECT_UBICACIONES } from '../common/auth.constants';
import { SelectItem } from 'primeng/primeng';
import { Response } from '@angular/http';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'configuracion-general-component',
    templateUrl: 'configuracion-general.component.html',
    providers: [Configuracion, ConfiguracionService, Ubicacion, UbicacionService],
})
/**
 * Jaylin Centeno
 * Componente que permite administrar la configuracion general del sistema
 */ 
export class ConfiguracionGeneralComponent {

    public formGeneral: FormGroup;
    public logo: any; //Guarda la dirección de la imagen
    public imagen: any; //Contendra el archivo subido por el usuario
    public permisoConfig: boolean; //Permite verificar permiso de escritura (true/false)
    public lecturaUbics: boolean; //Permite verificar permiso de lectura (true/false)
    public subiendoDatos: boolean = false; 
    public cargandoDatos: boolean = true;
    public nombreUbicacion: string; //Guarda el nombre de la ubicacion origen del documento
    public ubicacionItems: SelectItem[] = []; //Contiene las ubicaciones disponibles para mostrar en el dropdown
    public dialogUbicacion: boolean; //Habilita el cuadro de dialgo para el cambio de ubicación

    constructor(
        public configuracion: Configuracion,
        public configuracionService: ConfiguracionService,
        public ubicacionTemp: Ubicacion,
        public i18nService: I18nService,
        public router: Router,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService
    ) {
        //Form group
        this.formGeneral = new FormGroup({
            'nombre': new FormControl(''),
            'logo': new FormControl(''),
            'sitioWeb': new FormControl(''),
            'emailDesde': new FormControl(''),
            'integracion': new FormControl(''),
            'ubicacion': new FormControl('')
        })
        //Permite saber si el usuario tiene permisos 
        this.authGuard.canWrite(PERM_ESCR_CONFIGURACION).subscribe((permiso: boolean) => {
            this.permisoConfig = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_UBICACIONES).subscribe((permiso: boolean) => {
            this.lecturaUbics = permiso;
        });
    }

    /**
     * Método que se inicia al entrar por primera vez al componente, solo se ejecuta una vez
     */ 
    ngOnInit() {
        this.mostrarConfiguracion();
    }

    /**
     * Método para obtener la ubicación seleccionada
     */
    ubicacionSeleccionada() {
        this.configuracion.ubicacionPrincipal = this.ubicacionTemp;
        this.dialogUbicacion = false;
        this.actualizarConfiguracion();
    }

    /**
     * Método que se encarga de traer la configuración del sistema
     */ 
    mostrarConfiguracion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.configuracionService.token = token;
                this.configuracionService.obtenerConfiguracion().subscribe(
                    (data) => {
                        this.configuracion = data;
                        this.logo = this.configuracion.logoEmpresa;
                        this.ubicacionTemp = this.configuracion.ubicacionPrincipal;
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Método para actualizar la configuración general del sistema
     */ 
    actualizarConfiguracion() {
        this.subiendoDatos = true;
        this.msgService.showMessage(this.i18nService.getLabels("general-actualizando-datos"));
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.configuracionService.token = token;
                let sub = this.configuracionService.actualizarConfiguracion(this.configuracion).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.mostrarConfiguracion();
                        this.subiendoDatos = false;
                    },
                    (error) => {
                        this.manejaError(error);
                        this.subiendoDatos = false;
                    },
                    () => { sub.unsubscribe(); this.subiendoDatos = false; }
                );
            }
        );
    }

    /**
     * Método utilizado para subir archivos
     */ 
    subirImagen() {
        this.imagen = document.getElementById("logo");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.logo = reader.result;
            let urlBase64 = this.logo.split(",");
            this.configuracion.logoEmpresa = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /** 
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}