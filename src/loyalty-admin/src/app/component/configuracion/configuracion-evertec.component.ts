import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Configuracion, Metrica } from '../../model/index';
import { ConfiguracionService, MetricaService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_CONFIGURACION, PERM_LECT_METRICAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import * as Rutas from '../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'configuracion-evertec-component',
    templateUrl: 'configuracion-evertec.component.html',
    providers: [Configuracion, ConfiguracionService, Metrica, MetricaService],
})
/**
 * Jaylin Centeno
 * Componente que permite administrar la configuracion de los parametros del sistema
 */
export class ConfiguracionEvertecComponent {

    public escritura: boolean; //Permite verificar permiso de escritura (true/false)
    public lecturaMetrica: boolean; //Permite verificar permiso de lectura de metrica(true/false)
    public cargandoDatos: boolean = true; //Contienen los valores para desplegar en el dropdown

    //--- Metrica ----//
    public metrica: Metrica;
    public listaMetricas: Metrica[]; //Lista de las métricas disponibles
    public listaBusqueda: string[] = [];
    public totalRecordsMetrica: number;
    public nombreMetrica: string;
    public displayDialogMetrica: boolean;
    public cantidadMetrica: number = 50; //Cantidad de filas a mostrar
    public filaMetrica: number = 0; //Desplazamiento de la primera fila
    public idMetrica: string; //Id de la metrica a remover
    public listaVacia: boolean = true; //Para verificar si la lista esta vacia
    public info: boolean; //Habilita el cuadro de dialogo para ver la info sobre métrica

    constructor(
        public metricaService: MetricaService,
        public configuracion: Configuracion,
        public configuracionService: ConfiguracionService,
        public router: Router,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {

        //Se verifica si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_CONFIGURACION).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_METRICAS).subscribe((permiso: boolean) => {
            this.lecturaMetrica = permiso;
            this.obtenerListaMetrica();
        });
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarConfiguracion();
        this.obtenerListaMetrica();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadDataMetrica(event: any) {
        this.filaMetrica = event.first;
        this.cantidadMetrica = event.rows;
        this.obtenerListaMetrica();
    }

    //guarda los datos de la metrica seleccionada
    seleccionarMetrica(metrica: Metrica) {
        this.metrica = metrica;
        this.nombreMetrica = metrica.nombre;
        this.displayDialogMetrica = false;
    }

    /**
     * Se obtiene la configuracion del sistema
     */
    mostrarConfiguracion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.configuracionService.token = token;
                this.configuracionService.obtenerConfiguracion().subscribe(
                    (data) => {
                        this.configuracion = data;
                        if (this.configuracion.idMetricaInicial != undefined) {
                            this.metrica = this.configuracion.idMetricaInicial;
                            this.nombreMetrica = this.metrica.nombre;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }
    /**
     * Se obtiene la lista de métricas del sistema
     */
    obtenerListaMetrica() {
        if (this.lecturaMetrica) {
            let estado = "P";
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.metricaService.token = token;
                    this.metricaService.getListaMetrica(estado).subscribe(
                        (data: Metrica[]) => {
                            this.listaMetricas = data;
                            if (this.listaMetricas.length > 0) {
                                this.listaVacia = false;
                            } else {
                                this.listaVacia = true;
                            }
                        },
                        (error) => this.manejaError(error)
                    );
                }
            );
        }
    }

    /**
     * Se actualiza la configuración del sistema
     */
    actualizarConfiguracion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.configuracionService.token = token;
                this.configuracion.idMetricaInicial = this.metrica;
                let sub = this.configuracionService.actualizarConfiguracion(this.configuracion).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.mostrarConfiguracion();
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); }
                );
            }
        );
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}