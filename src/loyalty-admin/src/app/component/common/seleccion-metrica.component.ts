import { Component, OnInit, Output, forwardRef, Input } from '@angular/core';
import { Metrica } from '../../model/';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator, AbstractControl, ValidationErrors } from '@angular/forms';
import { MetricaService, KeycloakService } from '../../service/';
import { ErrorHandlerService, MessagesService } from '../../utils/';
import { Response } from '@angular/http';

/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Componente encargado de actuar como selector de metricas, capaz de ser usado como un form control del api standard de angular
 * (validacion, ngmodel)
 */
@Component({
    moduleId: module.id,
    selector: 'seleccion-metrica',
    templateUrl: 'seleccion-metrica.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SeleccionMetricaComponent),
            multi: true,
        }, {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => SeleccionMetricaComponent),
            multi: true,
        }
    ]
})
export class SeleccionMetricaComponent implements OnInit, ControlValueAccessor, Validator {
    public listaMetricas: Metrica[];
    public metricaSeleccionado: Metrica;
    public listaMetricasSeleccionados: Metrica[];
    public estados: string[];
    public cantRows: number;
    public rowOffset: number;
    public cantTotalRegistros: number;
    //The internal data model
    private innerValue: any;
    public displayDialog = false;
    public terminosBusqueda;
    public listaPremios;
    @Input() selectionMode: string;

    //Placeholders for the callbacks which are later providesd
    //by the Control Value Accessor
    private onTouchedCallback: () => void = () => { };
    private onChangeCallback: (_: any) => void = () => { };
    constructor(
        private kc: KeycloakService,
        private msgService: MessagesService,
        private metricaService: MetricaService
    ) {
        this.innerValue = new Metrica();
        this.metricaSeleccionado = new Metrica();
        this.metricaSeleccionado.nombre = "";
        this.estados = ["P"];
        this.cantRows = 4;
        this.rowOffset = 0;
    }   

    ngOnInit() {
        if (!this.selectionMode) {
            this.selectionMode = "U";
        }
    
    }

    seleccionarMetricas() {

    }

    seleccionarMetrica(metrica: Metrica) {
        this.innerValue = metrica;
        this.onChangeCallback(this.innerValue);
        this.displayDialog = false;
    }
    loadLazy(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantRows = event.rows;
        this.getListaMetricas();
    }
    /**
 * Agrega una regla de metrica
 */
    getListaMetricas() {
        this.kc.getToken().then(
            (tkn) => {
                this.metricaService.token = tkn;
                let sub = this.metricaService.buscarMetricas(this.estados, this.cantRows, this.rowOffset).subscribe(
                    (response: Response) => {
                        this.listaMetricas = <Metrica[]>response.json();
                        this.cantTotalRegistros = +response.headers.get("Content-Range").split("/")[1];
                    },
                    (error: Response) => {
                        this.manejaError(error);
                    },
                    () => {
                        sub.unsubscribe();
                    }
                );
            }
        );
    }

    //get accessor
    get value(): any {
        return this.innerValue;
    };

    //set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
    }

    /** 
     * Escribe el valor inicial del elemento.
     * @param obj 
     */
    writeValue(obj: any): void {
        if (obj !== this.innerValue) {
            this.innerValue = obj;
        }
    }
    /**
     * Set the function to be called when the control receives a change event.
     * @param fn 
     */
    registerOnChange(fn: any): void {
        this.onChangeCallback = fn;
    }
    /**
     * Set the function to be called when the control receives a touch event.
     */
    registerOnTouched(fn: any): void {
        this.onTouchedCallback = fn;
    }
    /**
     * This function is called when the control status changes to or from "DISABLED". Depending on the value, it will enable or disable the appropriate DOM element.
     * @param isDisabled 
     */
    setDisabledState(isDisabled: boolean): void {

    }

    validate(c: AbstractControl): ValidationErrors | null {
        switch (this.selectionMode) {
            case 'U': {
                if (!(this.innerValue as Metrica[])) {
                    return {
                        noElementSelected: {
                            valid: false,
                        },
                    };
                }
                break;
            }
            case 'M': {
                if (!(this.innerValue as Metrica)) {
                    return {
                        noElementSelected: {
                            valid: false,
                        },
                    };
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    registerOnValidatorChange(fn: () => void): void {

    }

    /**
     * Maneja errores a nivel de componente
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}