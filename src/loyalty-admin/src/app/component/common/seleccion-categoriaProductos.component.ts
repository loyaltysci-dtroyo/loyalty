import { Component, OnInit, forwardRef, Input } from '@angular/core';
import { CategoriaProducto } from '../../model';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator, AbstractControl, ValidationErrors } from '@angular/forms';
import { CategoriaProductoService, KeycloakService } from '../../service';
import { ErrorHandlerService, MessagesService } from '../../utils';
import { Response } from '@angular/http';

/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Componente encargado de actuar como selector de categoriaProducto, capaz de ser usado como un form control del api standard de angular
 * (validacion, ngmodel)
 */
@Component({
    moduleId: module.id,
    selector: 'seleccion-categoriaProductos',
    templateUrl: 'seleccion-categoriaProductos.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SeleccionCategoriaProductosComponent),
            multi: true,
        }, {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => SeleccionCategoriaProductosComponent),
            multi: true,
        }
    ]
})
export class SeleccionCategoriaProductosComponent implements OnInit, ControlValueAccessor, Validator {
    public listaCategoriaProductos: CategoriaProducto[];
    public categoriaProductoSeleccionado: CategoriaProducto;
    public listaCategoriaProductosSeleccionados: CategoriaProducto[];
    public cantRows: number;
    public rowOffset: number;
    public cantTotalRegistros: number;
    //The internal data model
    private innerValue: any;
    public displayDialog = false;
    public terminosBusqueda;
    public busqueda: string = ""; //Representa los criterios de búsqueda
    public listaBusqueda: string[] = [];  //Contiene los criterios de bú
    @Input() selectionMode: string;

    //Placeholders for the callbacks which are later providesd
    //by the Control Value Accessor
    private onTouchedCallback: () => void = () => { };
    private onChangeCallback: (_: any) => void = () => { };
    constructor(
        private kc: KeycloakService,
        private msgService: MessagesService,
        private categoriaProductoService: CategoriaProductoService
    ) {
        this.innerValue = new CategoriaProducto();
        this.categoriaProductoSeleccionado = new CategoriaProducto();
        this.categoriaProductoSeleccionado.nombre = "";
        this.cantRows = 4;
        this.rowOffset = 0;
    }   

    ngOnInit() {
        if (!this.selectionMode) {
            this.selectionMode = "U";
        }
    
    }

    seleccionarCategoriaProductos() {

    }

    seleccionarCategoriaProducto(categoriaProducto: CategoriaProducto) {
        this.innerValue = categoriaProducto;
        this.onChangeCallback(this.innerValue);
        this.displayDialog = false;
    }
    loadLazy(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantRows = event.rows;
        this.getListaCategoriaProductos();
    }
    /**
 * Agrega una regla de categoriaProducto
 */
    getListaCategoriaProductos() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.kc.getToken().then(
            (tkn) => {
                this.categoriaProductoService.token = tkn;
                let sub = this.categoriaProductoService.obtenerListaCategorias(this.cantRows, this.rowOffset, this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaCategoriaProductos = <CategoriaProducto[]>response.json();
                        this.cantTotalRegistros = +response.headers.get("Content-Range").split("/")[1];
                    },
                    (error: Response) => {
                        this.manejaError(error);
                    },
                    () => {
                        sub.unsubscribe();
                    }
                );
            }
        );
    }

    //get accessor
    get value(): any {
        return this.innerValue;
    };

    //set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
    }

    /** 
     * Escribe el valor inicial del elemento.
     * @param obj 
     */
    writeValue(obj: any): void {
        if (obj !== this.innerValue) {
            this.innerValue = obj;
        }
    }
    /**
     * Set the function to be called when the control receives a change event.
     * @param fn 
     */
    registerOnChange(fn: any): void {
        this.onChangeCallback = fn;
    }
    /**
     * Set the function to be called when the control receives a touch event.
     */
    registerOnTouched(fn: any): void {
        this.onTouchedCallback = fn;
    }
    /**
     * This function is called when the control status changes to or from "DISABLED". Depending on the value, it will enable or disable the appropriate DOM element.
     * @param isDisabled 
     */
    setDisabledState(isDisabled: boolean): void {

    }

    validate(c: AbstractControl): ValidationErrors | null {
        switch (this.selectionMode) {
            case 'U': {
                if (!(this.innerValue as CategoriaProducto[])) {
                    return {
                        noElementSelected: {
                            valid: false,
                        },
                    };
                }
                break;
            }
            case 'M': {
                if (!(this.innerValue as CategoriaProducto)) {
                    return {
                        noElementSelected: {
                            valid: false,
                        },
                    };
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    registerOnValidatorChange(fn: () => void): void {

    }

    /**
     * Maneja errores a nivel de componente
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}