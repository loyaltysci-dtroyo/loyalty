import { Component, OnInit, Output, forwardRef, Input } from '@angular/core';
import { Promocion } from '../../model/';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator, AbstractControl, ValidationErrors } from '@angular/forms';
import { PromocionService, KeycloakService } from '../../service/';
import { ErrorHandlerService, MessagesService } from '../../utils/';
import { Response } from '@angular/http';

/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Componente encargado de actuar como selector de promociones, capaz de ser usado como un form control del api standard de angular
 * (validacion, ngmodel)
 */
@Component({
    moduleId: module.id,
    selector: 'seleccion-promociones',
    templateUrl: 'seleccion-promociones.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SeleccionPromocionesComponent),
            multi: true,
        }, {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => SeleccionPromocionesComponent),
            multi: true,
        }
    ]
})
export class SeleccionPromocionesComponent implements OnInit, ControlValueAccessor, Validator {
    public listaPromociones: Promocion[];
    public promocionSeleccionado: Promocion;
    public listaPromocionesSeleccionados: Promocion[];
    public estados: string[];
    public cantRows: number;
    public rowOffset: number;
    public cantTotalRegistros: number;
    //The internal data model
    private innerValue: any;
    public displayDialog = false;
    public terminosBusqueda;
    @Input() selectionMode: string;

    //Placeholders for the callbacks which are later providesd
    //by the Control Value Accessor
    private onTouchedCallback: () => void = () => { };
    private onChangeCallback: (_: any) => void = () => { };
    constructor(
        private kc: KeycloakService,
        private msgService: MessagesService,
        private promocionService: PromocionService
    ) {
        this.innerValue = new Promocion();
        this.promocionSeleccionado = new Promocion();
        this.promocionSeleccionado.nombre = "";
        this.estados[0] = "P";
        this.cantRows = 4;
        this.rowOffset = 0;
    }   

    ngOnInit() {
        if (!this.selectionMode) {
            this.selectionMode = "U";
        }
    
    }

    seleccionarPromociones() {

    }

    seleccionarPromocion(promocion: Promocion) {
        this.innerValue = promocion;
        this.onChangeCallback(this.innerValue);
        this.displayDialog = false;
    }
    loadLazy(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantRows = event.rows;
        this.getListaPromociones();
    }
    /**
 * Agrega una regla de promocion
 */
    getListaPromociones() {
        this.kc.getToken().then(
            (tkn) => {
                this.promocionService.token = tkn;
                let sub = this.promocionService.buscarPromociones(this.terminosBusqueda,this.estados, this.cantRows, this.rowOffset).subscribe(
                    (response: Response) => {
                        this.listaPromociones = <Promocion[]>response.json();
                        this.cantTotalRegistros = +response.headers.get("Content-Range").split("/")[1];
                    },
                    (error: Response) => {
                        this.manejaError(error);
                    },
                    () => {
                        sub.unsubscribe();
                    }
                );
            }
        );
    }

    //get accessor
    get value(): any {
        return this.innerValue;
    };

    //set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
    }

    /** 
     * Escribe el valor inicial del elemento.
     * @param obj 
     */
    writeValue(obj: any): void {
        if (obj !== this.innerValue) {
            this.innerValue = obj;
        }
    }
    /**
     * Set the function to be called when the control receives a change event.
     * @param fn 
     */
    registerOnChange(fn: any): void {
        this.onChangeCallback = fn;
    }
    /**
     * Set the function to be called when the control receives a touch event.
     */
    registerOnTouched(fn: any): void {
        this.onTouchedCallback = fn;
    }
    /**
     * This function is called when the control status changes to or from "DISABLED". Depending on the value, it will enable or disable the appropriate DOM element.
     * @param isDisabled 
     */
    setDisabledState(isDisabled: boolean): void {

    }

    validate(c: AbstractControl): ValidationErrors | null {
        switch (this.selectionMode) {
            case 'U': {
                if (!(this.innerValue as Promocion[])) {
                    return {
                        noElementSelected: {
                            valid: false,
                        },
                    };
                }
                break;
            }
            case 'M': {
                if (!(this.innerValue as Promocion)) {
                    return {
                        noElementSelected: {
                            valid: false,
                        },
                    };
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    registerOnValidatorChange(fn: () => void): void {

    }

    /**
     * Maneja errores a nivel de componente
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}