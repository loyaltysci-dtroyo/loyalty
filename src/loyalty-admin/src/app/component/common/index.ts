export { AuthGuard } from './auth.guard';
export { SeleccionPremiosComponent } from './seleccion-premios.component';
export { SeleccionCategoriaProductosComponent } from './seleccion-categoriaProductos.component'
export { SeleccionMiembrosComponent } from './seleccion-miembros.component';
export { SeleccionSegmentosComponent } from './seleccion-segmentos.component';
export { SeleccionUbicacionesComponent } from './seleccion-ubicaciones.component';
export { SeleccionMetricaComponent } from './seleccion-metrica.component';
export { SeleccionPromocionesComponent } from './seleccion-promociones.component';