
/**
 * Constantes que representan los recutsos de Escritura en el sistema, deben usarse en conjunto con el 
 * service de auth guard para pedir permisos de Escritura para un determunado recurso
 */
export const ANULAR_CERTIFICADO = "Anular Certificado";

export const PERM_ESCR_ATRIBUTOS = "Atributos Escritura";
export const PERM_LECT_ATRIBUTOS = "Atributos Lectura";

export const PERM_ESCR_PRODUCTO_ATRIBUTO = "Atributos Producto Escritura";
export const PERM_LECT_PRODUCTO_ATRIBUTO = "Atributos Producto Lectura";

export const PERM_ESCR_CAMPANAS = "Campanas Escritura";
export const PERM_LECT_CAMPANAS = "Campanas Lectura";

export const PERM_ESCR_CATEGORIAS = "Categorias Escritura"; //Misión
export const PERM_LECT_CATEGORIAS = "Categorias Lectura"; //Misión

export const PERM_ESCR_PREMIO_CATEGORIA = "Categorias Premio Escritura";
export const PERM_LECT_PREMIO_CATEGORIA = "Categorias Premio Lectura";

export const PERM_ESCR_CATEGORIAS_PRODUCTO = "Categorias Producto Escritura";
export const PERM_LECT_CATEGORIAS_PRODUCTO = "Categorias Producto Lectura";

export const PERM_ESCR_CATEGORIAS_PROMOCION = "Categorias Promocion Escritura";
export const PERM_LECT_CATEGORIAS_PROMOCION = "Categorias Promocion Lectura";

export const PERM_ESCR_CATEGORIAS_NOTICIA = "Categorias Noticia Escritura";
export const PERM_LECT_CATEGORIAS_NOTICIA = "Categorias Noticia Lectura";

export const PERM_ESCR_CONFIGURACION = "Configuracion General";

export const PERM_ESCR_DOCUMENTO = "Documentos Escritura";
export const PERM_LECT_DOCUMENTO = "Documentos Lectura";

export const PERM_ESCR_GRUPOS = "Grupos Escritura";
export const PERM_LECT_GRUPOS = "Grupos Lectura";

export const PERM_ESCR_INSIGNIAS = "Insignias Escritura";
export const PERM_LECT_INSIGNIAS = "Insignias Lectura";

export const PERM_ESCR_LEADERBOARDS = "Leaderboards Escritura";
export const PERM_LECT_LEADERBOARDS = "Leaderboards Lectura";

export const PERM_ESCR_METRICAS = "Metricas Escritura";
export const PERM_LECT_METRICAS = "Metricas Lectura";

export const PERM_ESCR_MIEMBROS = "Miembros Escritura";
export const PERM_LECT_MIEMBROS = "Miembros Lectura";

export const PERM_ESCR_MISIONES = "Misiones Escritura";
export const PERM_LECT_MISIONES = "Misiones Lectura";

export const PERM_ESCR_JUEGOS = "Juegos Escritura";
export const PERM_LECT_JUEGOS = "Juegos Lectura";

export const PERM_ESCR_PREFERENCIA = "Preferencias Escritura";
export const PERM_LECT_PREFERENCIA = "Preferencias Lectura";

export const PERM_ESCR_PREMIOS = "Premios Escritura";
export const PERM_LECT_PREMIOS = "Premios Lectura";

export const PERM_ESCR_CERTIFICADO_CATEGORIA = "Certificado Categoria Escritura";
export const PERM_LECT_CERTIFICADO_CATEGORIA = "Certificado Categoria Lectura";

export const PERM_ESCR_PRODUCTOS = "Productos Escritura";
export const PERM_LECT_PRODUCTOS = "Productos Lectura";

export const PERM_ESCR_PROMOCIONES = "Promociones Escritura";
export const PERM_LECT_PROMOCIONES = "Promociones Lectura";

export const PERM_ESCR_RECOMPENSAS = "Recompensas Escritura";
export const PERM_LECT_RECOMPENSAS = "Recompensas Lectura";

export const REPORTES = "Reportes";

export const PERM_ESCR_ROLES = "Roles Escritura";
export const PERM_LECT_ROLES = "Roles Lectura";

export const PERM_ESCR_SEGMENTOS = "Segmentos Escritura";
export const PERM_LECT_SEGMENTOS = "Segmento Lectura";
export const PERM_ADMIN_REGLAS_SEGMENTOS = "Segmentos Administracion Reglas";

export const PERM_ESCR_UBICACIONES = "Ubicaciones Escritura";
export const PERM_LECT_UBICACIONES = "Ubicaciones Lectura";

export const PERM_ESCR_USUARIOS = "Usuarios Escritura";
export const PERM_LECT_USUARIOS = "Usuarios Lectura";

export const PERM_ESCR_WORKFLOW = "Workflow Escritura";
export const PERM_LECT_WORKFLOW = "Workflow Escritura";

export const BITACORA = "Bitacora Lectura";

export const PERM_ESCR_NEWSFEED = "NewsFeed Escritura";
export const PERM_LECT_NEWSFEED = "NewsFeed Lectura";

export const PERM_APROBACION_MISION = "Gestión de Respuesta Misión";