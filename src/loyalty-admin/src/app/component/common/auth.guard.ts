import { MessagesService } from '../../utils/index';
import { KeycloakService } from '../../service/index';
import { Injectable, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from 'rxjs/Rx';
import * as Routes from '../../utils/rutas';

@Injectable()
/**
* Flecha Roja Technologies oct-2016
* Fernando Aguilar
* Encargada de realizar operaciones de autorización
* Dependencias: KeycloakService
*/
export class AuthGuard implements CanActivate {

    public RPT: any;//Es un JWT que contiene los permisos
    public resourcesAutorizados: string[];//array de permisos otorgados a un determinado usuario
    public mapCanActivate: Map<string, string>;//para permisos de lectura

    constructor(public keycloakService: KeycloakService) {
        this.resourcesAutorizados = [];
        this.inicializarMap();
    }
    /**Metodo que obtiene la lista de permisos que el usuario tiene, es usado por el componente raiz para determinar cuales elementos
     * mostrar u ocultar al usuario segun sus permisos, retorna un observable, en caso de exito retorna un map de string y booleano
     * el string representa el permiso y el booleano es true siempre que tenga permiso para acceder al mismo
     */
    getPermisos(): Observable<Map<string, boolean>> {
        this.inicializarMap();
        return Observable.create((observer:any) => {
            this.keycloakService.getRPT().then((rpt) => {
                this.RPT = rpt;
                //TODO: Remove when fix roles
                this.RPT.authorization.permissions.push({
                    resource_set_name: "Juegos Escritura"
                });
                this.RPT.authorization.permissions.push({
                    resource_set_name: "Juegos Lectura"
                });
                this.resourcesAutorizados = this.RPT.authorization.permissions.map((element:any) => { return element.resource_set_name });
                let m = new Map<string, boolean>();
                this.resourcesAutorizados.forEach((element) => {
                    m.set(element, true);
                });
                observer.next(m);
                observer.complete();
            });
        });
    }

    /**
     * Metodo que determina si un usario puede activar una ruta determiada
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        this.inicializarMap();//se inicializa el mapeo de rutas con nombre de resource
        //se crea un observable para que la clase cliente pueda obtener los datos de manera asincrona
        return Observable.create((observer:any) => {
            //se obtiene un resource party token del api de Keycloak
            this.keycloakService.getRPT().then((rpt) => {
                this.RPT = rpt;
                //se obtienen los datos relevantes, en este caso un listado de resources autorizados al usuario
                this.resourcesAutorizados = this.RPT.authorization.permissions.map((element:any) => { return element.resource_set_name });
                //por cada resource autorizado se verifica si el permiso esta mapeado a una de las rutas registradas en el map de rutas
                this.resourcesAutorizados.forEach(element => {
                    //  console.log(state.url, this.mapCanActivate.get(element), state.url.startsWith(this.mapCanActivate.get(element)))
                    if (state.url.startsWith(this.mapCanActivate.get(element))) {
                        //Acceso aprobado
                        observer.next(true);
                        observer.complete();
                    }
                });
                //acceso denegado
                observer.next(false);
                observer.complete();
            }).catch((err) => { observer.error(err) });
        });
    }

    /**
     * Metodo que evaluará si un usuario tiene permiso para editar datos en una determinada ruta en el sistema, es usado por
     * los componentes para verificar si se permite la acción de escritura de datos a un usuario determinado
     * permiso: una de las constantes de AuthGuard representando el recurso a eliminar
     */
    canWrite(permiso: string): Observable<boolean> {
        //se crea un observable para que la clase cliente pueda obtener los datos de manera asincrona
        return Observable.create((observer:any) => {
            //se obtiene un resource party token del api de Keycloak
            this.keycloakService.getRPT().then((rpt) => {
                this.RPT = rpt;
                //se obtienen los datos relevantes, en este caso un listado de resources autorizados al usuario
                this.resourcesAutorizados = this.RPT.authorization.permissions.map((element:any) => { return element.resource_set_name });
                //por cada resource autorizado se verifica si el permiso que entra por parametro es igual al resource autorizado
                this.resourcesAutorizados.forEach(element => {
                    if (permiso == element) {
                        // Acceso Aprobado
                        observer.next(true);
                        observer.complete();
                    }
                });
                // Acceso Denegado
                observer.next(false);
                observer.complete();
            });
        });
    }

    /**
     * Metodo encargado de mapear los ID de recurso con rutas que
     * en nuestro caso representan el recurso en el sevidor, son usadas por el servicio de autorización  para
     * determinar si el usuario puede ingresar a dicho componente
     */
    inicializarMap() {
        this.mapCanActivate = new Map<string, string>();
        this.mapCanActivate.set("Usuarios Lectura", Routes.RT_USUARIOS);
        this.mapCanActivate.set("Usuarios Escritura", Routes.RT_USUARIOS_INSERTAR);
        this.mapCanActivate.set("Miembros Lectura", Routes.RT_MIEMBROS);
        this.mapCanActivate.set("Miembros Escritura", Routes.RT_MIEMBROS_INSERTAR);
        this.mapCanActivate.set("Ubicaciones Lectura", Routes.RT_UBICACIONES);
        this.mapCanActivate.set("Ubicaciones Escritura", Routes.RT_UBICACIONES_INSERTAR);
        this.mapCanActivate.set("Metricas Lectura", Routes.RT_METRICAS);
        this.mapCanActivate.set("Metricas Escritura", Routes.RT_METRICAS_INSERTAR);
        this.mapCanActivate.set("Leaderboards Lectura", Routes.RT_LEADERBOARDS);
        this.mapCanActivate.set("Leaderboards Escritura", Routes.RT_LEADERBOARDS_INSERTAR);
        this.mapCanActivate.set("Segmento Lectura", Routes.RT_SEGMENTOS);
        this.mapCanActivate.set("Categorias Lectura", Routes.RT_CATEGORIAS_DETALLE);
        this.mapCanActivate.set("Categorias Escritura", Routes.RT_CATEGORIAS_INSERTAR);
        this.mapCanActivate.set("Promociones Escritura", Routes.RT_PROMOCIONES_INSERTAR);
        this.mapCanActivate.set("Promociones Lectura", Routes.RT_PROMOCIONES);
        this.mapCanActivate.set("Misiones Lectura", Routes.RT_MISIONES);
        this.mapCanActivate.set("Juegos Lectura", Routes.RT_JUEGOS);
        this.mapCanActivate.set("Productos Lectura", Routes.RT_PRODUCTOS);
        this.mapCanActivate.set("Productos Escritura", Routes.RT_PRODUCTOS_INSERTAR);
        this.mapCanActivate.set("Recompensas Lectura", Routes.RT_RECOMPENSAS);
        this.mapCanActivate.set("Campanas Lectura", Routes.RT_CAMPANAS);
        this.mapCanActivate.set("Campanas Escritura", Routes.RT_CAMPANAS_INSERTAR);
        this.mapCanActivate.set("Grupos Lectura", Routes.RT_GRUPOS);
        this.mapCanActivate.set("Grupos Escritura", Routes.RT_GRUPOS_INSERTAR);
        this.mapCanActivate.set("Atributos Lectura", Routes.RT_ATRIBUTOS);
        this.mapCanActivate.set("Insignias Lectura", Routes.RT_INSIGNIAS);
        this.mapCanActivate.set("Insignias Escritura", Routes.RT_INSIGNIAS_INSERTAR);
        this.mapCanActivate.set("Configuracion General", Routes.RT_CONFIGURACION);
        this.mapCanActivate.set("Premios Lectura", Routes.RT_PREMIOS);
        this.mapCanActivate.set("Premios Escritura", Routes.RT_PREMIOS_INSERTAR);
        this.mapCanActivate.set("Categorias Premio Escritura", Routes.RT_PREMIOS_CATEGORIA_INSERTAR);
        this.mapCanActivate.set("Categorias Premio Lectura", Routes.RT_PREMIOS_CATEGORIA_LISTA);
        this.mapCanActivate.set("Categorias Producto Escritura", Routes.RT_PRODUCTOS_CATEGORIA_INSERTAR);
        this.mapCanActivate.set("Categorias Producto Lectura", Routes.RT_PRODUCTOS_CATEGORIA_LISTA);
        this.mapCanActivate.set("Atributos Producto Lectura", Routes.RT_PRODUCTOS_ATRIBUTO);
        this.mapCanActivate.set("Atributos Producto Escritura", Routes.RT_PRODUCTOS_ATRIBUTO_INSERTAR);
        this.mapCanActivate.set("Workflow Lectura", Routes.RT_TAREA);
        this.mapCanActivate.set("Workflow Escritura", Routes.RT_TAREA_INSERTAR);
        this.mapCanActivate.set("Documentos Lectura", Routes.RT_INVENTARIO);
        this.mapCanActivate.set("Documentos Lectura", Routes.RT_INVENTARIO_PROVEEDOR);
        this.mapCanActivate.set("Documentos Lectura", Routes.RT_INVENTARIO_LISTA_DOCUMENTOS);
        this.mapCanActivate.set("Documentos Lectura", Routes.RT_INVENTARIO_HISTORICO);
        this.mapCanActivate.set("Documentos Escritura", Routes.RT_INVENTARIO_INSERTAR_DOCUMENTO);
        this.mapCanActivate.set("Bitacora Lectura", Routes.RT_BITACORA);
        this.mapCanActivate.set("NewsFeed Lectura", Routes.RT_NOTICIA);
        this.mapCanActivate.set("NewsFeed Escritura", Routes.RT_NOTICIA_INSERTAR);
        this.mapCanActivate.set("Categorías Noticia Escritura", Routes.RT_NOTICIA_CATEGORIA_INSERTAR);
        this.mapCanActivate.set("Categorías Noticia Lectura", Routes.RT_NOTICIA_CATEGORIA_LISTA);
        this.mapCanActivate.set("Gestión de Respuesta Misión", Routes.RT_MISIONES_DETALLE_APROBACION);
    }
}