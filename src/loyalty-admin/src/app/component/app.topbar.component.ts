import { MessagesService } from '../utils';
import { KeycloakService } from '../service';
import { Component, Inject, forwardRef, Input, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { Configuracion } from '../model/index';
import { ConfiguracionService } from '../service/index';
import { ErrorHandlerService } from '../utils/index';
@Component({
    selector: 'app-topbar',
    templateUrl: 'app.topbar.component.html'
})
export class AppTopBarComponent implements OnInit {
    // @Input() configuracion: Configuracion;
    public nombreEmpresa: string[];
    constructor(public app: AppComponent,
        public kc: KeycloakService,
        public configuracion: Configuracion,
        public msgService:MessagesService,
        public configuracionService: ConfiguracionService) {
        this.nombreEmpresa = [];

    }
    ngOnInit() {
        this.mostrarConfiguracion();
    }
    mostrarConfiguracion() {
        this.kc.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.configuracionService.token = token;
                this.configuracionService.obtenerConfiguracion().subscribe(
                    (data) => {
                        this.copyValuesOf(this.configuracion, data);
                        this.nombreEmpresa = this.configuracion.nombreEmpresa.split(" ");
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }
    /**
     * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
     * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al   
     * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
     * no siempre sea necesario llamar al api para actualizar los datos
     */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    logout(){
         this.kc.logout();
    }
    /** Muestra un error en caso de que los datos de usuario no se puedan recuperar del servidor */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}