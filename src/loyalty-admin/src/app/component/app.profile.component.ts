import { MessagesService } from '../utils';
import { KeycloakService } from '../service/keycloak.service';
import {
    Component, Input, OnInit, EventEmitter, ViewChild, trigger, state,
    transition, style, animate, Inject, forwardRef
} from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/primeng';
import { AppComponent } from '../app.component';
import { Usuario } from '../model/index';
import { UsuarioService } from '../service/index';
import { ErrorHandlerService } from '../utils/error-handler.service';
@Component({
    selector: 'inline-profile',
    providers: [Usuario, UsuarioService],
    templateUrl: 'app.profile.component.html',
    animations: [
        trigger('menu', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class InlineProfileComponent {
    active: boolean;
    constructor(
        public kc: KeycloakService, 
        public msgService: MessagesService, 
        public usuarioService: UsuarioService, 
        public usuario: Usuario
    ) {
    }

    ngOnInit() {
        /**
  * Carga de detalles del usuario para el componente principal, esto para desplegar
  * el nombre de usuario, y la foto del usuario en el menu
 */
        this.kc.getToken().then(
            (tkn) => {
                this.usuarioService.token = tkn;
                let sub = this.usuarioService.obtenerPerfilUsuario().subscribe(
                    (data) => {
                        this.usuario = data.json();
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); }
                );
            }
        );

    }
    onClick(event) {
        this.active = !this.active;
        event.preventDefault();
    }
    account() {
        this.kc.accountMAnagement();
    }
    logout() {
        this.kc.logout();
    }
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}