import { Component, OnInit } from '@angular/core';
import { Noticia } from '../../model/index';
import { NoticiaService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PERM_ESCR_NEWSFEED } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Response } from '@angular/http'
import * as Rutas from '../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'noticia-lista-component',
    templateUrl: 'noticia-lista.component.html',
    providers: [Noticia, NoticiaService]
})
/**
 * Jaylin Centeno
 * Componente que muestra la lista de los news feed
 */
export class NoticiaListaComponent implements OnInit{

    public escritura: boolean = true; //Permiso de escritura (true/false)
    public cargandoDatos: boolean = true; //Muestra la carga de datos

    // --- NewsFeed --- //
    public idNewsFeed: string; //Id del noticia seleccionado
    public listaNoticia: Noticia[]; //Lista de noticia existentes
    public listaVacia: boolean = true; //Para verificar si la lista esta vacia
    public cantidad: number = 10; //Cantidad de filas
    public totalRecords: number; //Total de noticia en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    // --- Búsqueda --- //
    public busqueda: string;
    public listaBusqueda: string[] = [];
    public avanzada: boolean = false;
    public estado: string[] = []; //Estado de miembro A = activo I = suspendido  

    constructor(
        public noticia: Noticia,
        public noticiaService: NoticiaService,
        public router: Router,
        public authGuard: AuthGuard,
        public i18nService: I18nService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_NEWSFEED).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    //Inicializacion
    ngOnInit() {
        this.busquedaNoticia();
    }

    /**
     * Método utilizado para la carga de datos de manera perezosa
     */
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
    }
    /**
     * Facilita la busqueda de lOS NewsFeed que cumplan con los parámetros proporcionados
     */
    busquedaNoticia() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.noticiaService.token = token;
                this.noticiaService.obtenerListaNoticias(this.estado, this.cantidad, this.filaDesplazamiento, this.busqueda).subscribe(
                    (response: Response) => {
                        this.cargandoDatos = false;
                        this.listaNoticia = response.json();
                        this.totalRecords = +response.headers.get("Content-Range").split("/")[1];
                        if (this.listaNoticia.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                    },
                    (error) => { this.manejaError(error); this.cargandoDatos = false; }
                );
            }
        );
    }

    /**
     * Método para ir a la pantalla de ingreso
     */ 
    nuevaNoticia() {
        this.router.navigate([Rutas.RT_NOTICIA_INSERTAR]);
    }
    /**
     * Redirecciona al detalle del noticia seleccionado
     */
    noticiaSeleccionada(idNewsFeed: string) {
        this.router.navigate([Rutas.RT_NOTICIA_DETALLE, idNewsFeed]);
    }
    /**
     * Metodo para manejar los errores que se provocan en las transacciones
     */ 
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}