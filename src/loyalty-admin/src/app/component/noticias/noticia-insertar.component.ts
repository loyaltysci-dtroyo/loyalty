import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Noticia } from '../../model/index';
import { NoticiaService, KeycloakService, I18nService } from '../../service/index';
import { AppConfig } from '../../app.config';
import { PERM_ESCR_NEWSFEED } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'noticia-insertar-component',
    templateUrl: 'noticia-insertar.component.html',
    providers: [Noticia, NoticiaService]
})
/**
 * Jaylin Centeno
 * Componente que permite definir una entidad de noticia
 */
export class NoticiaInsertarComponent {

    public formNoticia: FormGroup; //Formulario para insertar una noticia
    public imagen: any; //Obtiene el archivo subido por el usuario
    public imagenNoticia: any; //Muestra la imagen de la noticia 
    public escritura: boolean; //Permiso de escritura (true/false)
    public subiendoImagen: boolean = false; // Muestra el gif durante la carga

    constructor(
        public noticia: Noticia,
        public noticiaService: NoticiaService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        //Se obtiene true/false, indica si el usuario tiene permisos de escritura sobre noticia
        this.authGuard.canWrite(PERM_ESCR_NEWSFEED).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Definición del formulario de la noticia
        this.formNoticia = new FormGroup({
            'titulo': new FormControl('', Validators.required),
            'contenido': new FormControl('', Validators.required)

        });

        this.imagenNoticia = `${AppConfig.DEFAULT_IMG_URL}`;
    }

    /**
     * Permite insertar la definición de un noticia
     */
    agregarNoticia() {
        if (this.noticia.imagen != undefined) {
            this.subiendoImagen = true;
            this.msgService.showMessage(this.i18nService.getLabels("info-uploading"));
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.noticiaService.token = token;
                    let sub = this.noticiaService.insertarNoticia(this.noticia).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                            //Se redirecciona inmediatamente al detalle de la noticia
                            this.router.navigate([Rutas.RT_NOTICIA_DETALLE, data.text()]);
                        },
                        (error) => {
                            this.manejaError(error);
                            this.subiendoImagen = false;
                        },
                        () => { sub.unsubscribe(); this.subiendoImagen = false; }
                    );
                }
            );
        }else{
            this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-imagen"));
        }
    }
    /**
     * Obtiene el archivo subido por el usuario, y lo transforma a un base64
     */
    subirImagen() {
        this.imagen = document.getElementById('imagenNoticia');
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.imagenNoticia = reader.result;
            let urlBase64 = this.imagenNoticia.split(",");
            this.noticia.imagen = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }
    /**
     * Permete redireccionar a la lista de noticia
     */
    goBack() {
        this.router.navigate([Rutas.RT_NOTICIA]);
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}