import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Response } from '@angular/http';
import * as Rutas from '../../../utils/rutas';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { CategoriaNoticia, Noticia } from '../../../model/index';
import { CategoriaNoticiaService, NoticiaService, KeycloakService, I18nService } from '../../../service/index';
import { PERM_ESCR_CATEGORIAS_NOTICIA, PERM_ESCR_NEWSFEED } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'categoria-noticia-detalle-component',
    templateUrl: 'categoria-noticia-detalle.component.html',
    providers: [CategoriaNoticia, CategoriaNoticiaService, Noticia, NoticiaService]
})
/**
 * Jaylin Centeno
 * Componente utilizado para mostrar/manipular el detalle de la noticia
 */
export class CategoriaNoticiaDetalleComponent implements OnInit {

    public id: string; //Id de la categoría
    public items: MenuItem[]; //Representa los items del tabmenu
    public escrituraCateg: boolean; //verificación de permisos (true/false)
    public escrituraNoticia: boolean; //verificación de permisos (true/false)
    public detalle: boolean = true;
    public asociar: boolean = false;
    public disponible: boolean = false; //Muestra las noticias disponibles
    public asociada: boolean = true; //Muestra las noticias asosciadas

    public eliminar: boolean; //Habilita el cuadro de dialogo para eliminar la categoría
    public editar: boolean;

    // --------- Noticias ----------//
    public listaNoticias: Noticia[] = [];
    public listaNoticiasD: Noticia[] = [];
    public listaVacia: boolean;
    public estado: string;
    public accion: string;
    public totalRegistro: number;
    public cantidadD: number = 10;
    public filaDesplazamientoD: number = 0;
    public cantidadA: number = 10;
    public filaDesplazamientoA: number = 0;
    public idNoticia: string;

    // --------- Disponibles / Asociadas -----------//
    public listaAsociar: string[];
    public listaRemover: string[];
    public lista: string[] = []; //Lista con los id de los miembros seleccionados
    public listaSeleccion: Noticia[] = []; //Seleccionados
    public seleccion: string; //Representa el id de la noticia seleccionada

    // ---------- Imagen ---------- //
    public imagen: any; //Para manipular el archivo subido por el usuario
    public imagenNoticia: any; //Contendra el archivo subido por el usuario

    constructor(
        public categoria: CategoriaNoticia,
        public categoriaService: CategoriaNoticiaService,
        public noticiaServiece: NoticiaService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Se obtiene el id desde la ruta -->
        this.route.params.forEach((params) => { this.id = params['id'] });
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS_NOTICIA).subscribe((permiso: boolean) => {
            this.escrituraCateg = permiso;
        });
        this.authGuard.canWrite(PERM_ESCR_NEWSFEED).subscribe((permiso: boolean) => {
            this.escrituraNoticia = permiso;
        });

    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
        this.obtenerNoticiasAsociadas();
        this.obtenerNoticiasDisponibles();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadDataA(event) {
        this.filaDesplazamientoA = event.first;
        this.cantidadA = event.rows;
    }

    /**
     * Método para guardar el valor del miembro seleccionado
     */
    noticiaSeleccionada(elemento: string, idNoticia?: string) {
        this.accion = elemento;
        if (elemento == "noticia") {
            this.seleccion = idNoticia;
        }
        this.eliminar = true;
    }

    /**
     * Metodo para crear la lista con los id de las noticias que se van a asociar
     */
    crearListaAsociar() {
        this.lista = [];
        //Lista selección es la lista generada cuando un cliente se marca
        if (this.listaSeleccion.length > 0) {
            this.listaSeleccion.forEach(noticia => {
                this.lista = [...this.lista, noticia.idNoticia]
            })
        }
    }

    /**
     * Metodo para crear la lista con los id de las noticias a remover
     */
    crearListaRemover() {
        this.lista = [];
        //Lista selección es la lista generada cuando un cliente se marca
        if (this.listaSeleccion.length > 0) {
            this.listaSeleccion.forEach(noticia => {
                this.lista = [...this.lista, noticia.idNoticia]
            })
        }
    }

    /**
     * Se obtiene el detalle de la categoría
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.obtenerCategoriaId(this.id).subscribe(
                    (data: Response) => {
                        this.categoria = data.json();
                        this.imagenNoticia = this.categoria.imagen;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Noticias disponbles
     */
    obtenerNoticiasDisponibles() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                let sub = this.categoriaService.obtenerListaNoticiasPorCateg(this.id, 'D', ['A'], this.cantidadD, this.filaDesplazamientoD).subscribe(
                    (data: Response) => {
                        this.listaNoticiasD = data.json();
                        if (data.headers.get("Content-Range") != null) {
                            this.totalRegistro = + data.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaNoticiasD.length > 0) {
                            this.listaVacia = false;
                        } else this.listaVacia = true;

                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); }
                )
            }
        )
    }
    /**
     * Noticias asociadas
     */
    obtenerNoticiasAsociadas() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                let sub = this.categoriaService.obtenerListaNoticiasPorCateg(this.id, 'A', ['A'], this.cantidadA, this.filaDesplazamientoA).subscribe(
                    (data: Response) => {
                        this.listaNoticias = data.json();
                        if (data.headers.get("Content-Range") != null) {
                            this.totalRegistro = + data.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaNoticias.length > 0) {
                            this.listaVacia = false;
                        } else this.listaVacia = true;

                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); }
                )
            }
        )
    }


    /**
    * Modificar la categoría
    */
    modificarCategoria() {
        if (this.categoria.imagen != undefined) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.categoriaService.token = token;
                    let sub = this.categoriaService.modificarCategoria(this.categoria).subscribe(
                        (response: Response) => {
                            this.mostrarDetalle();
                            this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        },
                        (error) => { this.manejaError(error); }
                    )
                });
        } else {
            this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-imagen"));
        }
    }

    /**
     * Eliminar la categoría
     */
    removerCategoria() {
        this.eliminar = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.removerCategoriaId(this.id).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.goBack();
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    /**
     * Asociar la noticia con la categoría
     */
    agregarAsociacion(idNoticia: string) {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.asignarCategoria(this.id, idNoticia).subscribe(
                    (data) => {
                        let index = this.listaNoticiasD.findIndex(element => element.idNoticia == idNoticia);
                        this.listaNoticiasD.splice(index, 1);
                        this.listaNoticiasD = [... this.listaNoticiasD];
                        let index2 = this.listaSeleccion.findIndex(element => element.idNoticia == idNoticia);
                        this.listaSeleccion.splice(index2, 1);
                        this.obtenerNoticiasAsociadas();
                        this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    /**
     * Eliminar la noticia de la lista de asociados
     */
    removerAsociacion() {
        this.eliminar = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.removerAsignacionCategoria(this.id, this.seleccion).subscribe(
                    (data) => {
                        let index = this.listaNoticias.findIndex(element => element.idNoticia == this.seleccion);
                        this.listaNoticias.splice(index, 1);
                        this.listaNoticias = [... this.listaNoticias];
                        let index2 = this.listaSeleccion.findIndex(element => element.idNoticia == this.seleccion);
                        this.listaSeleccion.splice(index2, 1);
                        this.obtenerNoticiasDisponibles();
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    /**
     * Método para asociar una lista noticias
     */
    agregarListaNoticia() {
        //Se pregunta si se selecciono algun cliente
        if (this.listaSeleccion.length > 0) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.categoriaService.token = token;
                    this.crearListaAsociar();
                    this.categoriaService.asignarCategoriaListaNoticias(this.id, this.lista).subscribe(
                        (data) => {
                            this.obtenerNoticiasDisponibles();
                            this.obtenerNoticiasAsociadas()
                            this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-multiple'));
                            this.lista = [];
                            this.listaSeleccion = [];
                        },
                        (error) => this.manejaError(error)
                    );
                }
            );
        } else {
            this.msgService.showMessage(this.i18nService.getLabels('warn-cantidad'));
        }
    }

    /**
     * Método para remover noticias asociadas
     */
    removerListaNoticias() {
        this.eliminar = false;
        //Se pregunta si se selecciono algun cliente
        if (this.listaSeleccion.length > 0) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.categoriaService.token = token;
                    this.crearListaRemover();
                    this.categoriaService.removerAsignacionListaNoticia(this.id, this.lista).subscribe(
                        (data) => {
                            this.obtenerNoticiasDisponibles();
                            this.obtenerNoticiasAsociadas()
                            this.msgService.showMessage(this.i18nService.getLabels('general-exclusion-multiple'));
                            this.lista = [];
                            this.listaSeleccion = [];
                        },
                        (error) => this.manejaError(error)
                    );
                }
            );
        } else {
            this.msgService.showMessage(this.i18nService.getLabels('warn-cantidad'));
        }
    }

    /**
    * Método para subir la imagen del avatar del cliente
    */
    subirImagen() {
        this.imagen = document.getElementById("imagen");
        let reader = new FileReader();
        reader.addEventListener("load", (event: any) => {
            this.imagenNoticia = reader.result;
            let urlBase64 = this.imagenNoticia.split(",");
            this.categoria.imagen = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /**
     * Se redirecciona a la lista de categorías
     */
    goBack() {
        this.router.navigate([Rutas.RT_NOTICIA_CATEGORIA_LISTA]);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
