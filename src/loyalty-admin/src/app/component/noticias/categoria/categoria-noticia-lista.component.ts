import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CategoriaNoticia } from '../../../model/index';
import { CategoriaNoticiaService, KeycloakService, I18nService } from '../../../service/index';
import { Http, Response } from '@angular/http';
import { PERM_LECT_CATEGORIAS_NOTICIA, PERM_ESCR_CATEGORIAS_NOTICIA } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import * as Rutas from '../../../utils/rutas'
import { AppConfig } from '../../../app.config';

@Component({
    moduleId: module.id,
    selector: 'categoria-noticia-lista-component',
    templateUrl: 'categoria-noticia-lista.component.html',
    providers: [CategoriaNoticia, CategoriaNoticiaService]
})
/**
 * Jaylin Centeno
 * Componente que muestra la lista de categorías de noticias
 */
export class CategoriaNoticiaListaComponent implements OnInit {

    public eliminar: boolean; // Habilita el cuadro de dialogo para remover 
    public dialogInsertar: boolean; //Habilita el cuadro de dialogo para insertar
    public escritura: boolean; //Permite verificar permisos (true/false)
    public lectura: boolean; //Permite verificar permisos (true/false)
    public cargandoDatos: boolean = true; //Muestra la carga de datos
    public formCategoria: FormGroup; //Formulario para insertar una categoría

    // ------- Categoría -------- //
    public idCategoria: string; //Id de la categoría seleccionada
    public listaCategoria: CategoriaNoticia[]; //Lista de categorías
    public listaVacia: boolean = true; //Para verificar si la lista de obtenida esta vacía
    public cantidad: number = 10; //Cantidad de rows
    public totalRecords: number; //Total de categorías de premios en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera filaDesplazamiento

    // ---------- Imagen ---------- //
    public imagen: any; //Para manipular el archivo subido por el usuario
    public imagenNoticia: any; //Contendra el archivo subido por el usuario
    public imagenReserva: any;

    //---------- Búsqueda ---------//
    public busqueda: string = ""; //Palabras clave para la búsqueda
    public listaBusqueda: string[] = [];

    constructor(
        public categoria: CategoriaNoticia,
        public categoriaService: CategoriaNoticiaService,
        public router: Router,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura sobre este elemento
        this.authGuard.canWrite(PERM_LECT_CATEGORIAS_NOTICIA).subscribe(
            (permiso: boolean) => { this.lectura = permiso; });
        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS_NOTICIA).subscribe(
            (permiso: boolean) => { this.escritura = permiso; });
        this.imagenNoticia = this.imagenReserva = `${AppConfig.DEFAULT_IMG_URL}`;
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.busquedaCategorias();
    }

    /**
     * Método para mostrar el cuadro de dialogo para eliminar un elemento
     */
    confirmar(idCategoria: string) {
        this.idCategoria = idCategoria;
        this.eliminar = true;
    }

    /**
     * Método utilizado para la carga de los elementos del datatable
     * onLazyLoad, permite la carga de los elementos en segmentos
     */
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaCategorias();
    }

    /**
     * Búsqueda de las categorías de premio según palabras claves
     */
    busquedaCategorias() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.obtenerListaCategorias(this.cantidad, this.filaDesplazamiento, this.busqueda.trim()).subscribe(
                    (response: Response) => {
                        this.listaCategoria = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        //Se pregunta si la lista esta vacía
                        if (this.listaCategoria.length > 0) { this.listaVacia = false; }
                        else { this.listaVacia = true; }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Ingresar una nueva categoría
     */
    insertarCategoria() {
        if(this.categoria.imagen != undefined){
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                let sub = this.categoriaService.insertarCategoria(this.categoria).subscribe(
                    (response: Response) => {
                        this.dialogInsertar = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                        this.busquedaCategorias();
                        this.categoria = new CategoriaNoticia();
                        this.imagenNoticia = this.imagenReserva;
                    },
                    (error) =>{this.manejaError(error);}
                )
            });
        }else{
            this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-imagen"));
        }
    }

    /**
     * Permite remover la categoría seleccionada
     */
    removerCategoria() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.removerCategoriaId(this.idCategoria).subscribe(
                    (data) => {
                        this.eliminar = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.filaDesplazamiento = 0;
                        let index = this.listaCategoria.findIndex(element => element.idCategoria == this.idCategoria);
                        this.listaCategoria.splice(index, 1);
                        this.listaCategoria = [...this.listaCategoria];
                    },
                    (error) => {
                        this.eliminar = false;
                        this.manejaError(error)
                    }
                );
            }
        );
    }

    /**
     * Método para subir la imagen del avatar del cliente
     */
    subirImagen() {
        this.imagen = document.getElementById("imagen");
        let reader = new FileReader();
        reader.addEventListener("load", (event: any) => {
            this.imagenNoticia = reader.result;
            let urlBase64 = this.imagenNoticia.split(",");
            this.categoria.imagen = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }
    /**
     * Método para navegar hasta el detalle de la categoría seleccionada
     */
    detalleCategoria(idCategoria: string) {
        this.router.navigate([Rutas.RT_NOTICIA_CATEGORIA_DETALLE, idCategoria]);
    }
    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}