import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Noticia, Usuario, Miembro, NoticiaComentario } from '../../model/index';
import { NoticiaService, UsuarioService, MiembroService, KeycloakService, I18nService } from '../../service/index';
import { SelectItem } from 'primeng/primeng';
import { AppConfig } from '../../app.config';
import { PERM_ESCR_NEWSFEED } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Response } from '@angular/http';

@Component({
    moduleId: module.id,
    selector: 'noticia-detalle-component',
    templateUrl: 'noticia-detalle.component.html',
    providers: [Noticia, NoticiaService, Usuario, UsuarioService, Miembro, MiembroService, NoticiaComentario]
})

/**
 * Jaylin Centeno
 * Componente padre de noticia que permite administrar el estado y rutas a los demás componentes de la noticia
 */
export class NoticiaDetalleComponent implements OnInit {

    public id: string; //Id de la noticia, por parametros
    public escritura: boolean; //Guarda el estado del permiso (true/false)
    public displayCambioEstado: boolean; //Habilita el cuadro de dialogo para ejercer una acción sobre la noticia 
    public accion: string; //Usado para mostrar si se debe archivar/inactivar/eliminar la noticia
    public pregunta: boolean; //Guardar la pregunta según la acción seleccionada
    public cargandoInfo: boolean = true; //Muestra el gif de carga mientras se obtinen la iformación de noticia
    //---- Utilizados para escoger la clase (ngClass) según estado ---->
    public publicado: boolean; 
    public archivado: boolean;
    public borrador: boolean;
    public inactivo: boolean;

    // ---- Items del menú y sus respectivas rutas ----//
    public general = Rutas.RT_NOTICIA_DETALLE_GENERAL;
    public audiencia = Rutas.RT_NOTICIA_DETALLE_AUDIENCIA;

    constructor(
        public noticia: Noticia,
        public noticiaService: NoticiaService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService,
        public route: ActivatedRoute,
    ) {
        //Se obtiene el id de la noticia
        this.route.params.forEach((params: Params) => { this.id = params['id'] });
        //Se obtiene true/false que indica si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_NEWSFEED).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    /**
     * Llamado solo una vez al iniciarce el componente
     */
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Obtiene la definición de la noticia
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.noticiaService.token = token;
                let sub = this.noticiaService.obtenerNoticiaId(this.id).subscribe(
                    (data: Response) => {
                        this.cargandoInfo = false;
                        this.copyValuesOf(this.noticia, data.json());
                        switch (this.noticia.indEstado) {
                            case 'A': this.publicado = true;
                                break;
                            case 'B': this.archivado = true;
                                break;
                            case 'C': this.borrador = true;
                                break;
                            case 'D': this.inactivo = true;
                                break;
                        }
                    },
                    (error) => {this.manejaError(error); this.cargandoInfo = false;},
                    () => sub.unsubscribe()
                )
            }
        )
    }

    /**
     * Invoca el despliegue del cuadro de dialogo y la pregunta a mostrar según la acción
     */
    mostrarCambioEstado(accion) {
        this.accion = accion;
        this.pregunta = this.i18nService.getLabels("confirmacion-pregunta")[accion];
        this.displayCambioEstado = true;
    }


    /**
     * Remueve la noticia, siempre y cuando se encuentre en estado borrador
     */
    removerNoticia() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.noticiaService.token = token;
                let sub = this.noticiaService.removernoticiaId(this.id).subscribe(
                    (data) => {
                        this.displayCambioEstado = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.goBack();
                    },
                    (error) => { this.manejaError(error); this.displayCambioEstado = false; },
                    () => sub.unsubscribe()
                )
            }
        )
    }

    /**
     * Manejo del cambio de estado
     * A = publicado
     * B = archivado
     * C = borrador
     * D = inactivo
     */
    cambiarEstado(accion: string) {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.noticiaService.token = token;
                this.noticia.indEstado = accion;
                let sub = this.noticiaService.modificarNoticia(this.noticia).subscribe(
                    (data) => {
                        //Según la acción ejecutada y el estado de la noticia se escoge la clase y el msj a desplegar
                        if (this.publicado) {
                            if (accion == "D") {
                                this.inactivo = true;
                                this.msgService.showMessage(this.i18nService.getLabels("general-desactivar"));
                            }
                            else {
                                this.archivado = true;
                                this.msgService.showMessage(this.i18nService.getLabels("general-archivar"));
                            }
                            this.publicado = false;
                        } else if (this.borrador) {
                            this.publicado = true;
                            this.borrador = false;
                            this.msgService.showMessage(this.i18nService.getLabels("general-publicar"));
                        } else if (this.inactivo) {
                            this.publicado = true;
                            this.inactivo = false;
                            this.msgService.showMessage(this.i18nService.getLabels("general-publicar"));
                        }
                        this.mostrarDetalle();
                        this.displayCambioEstado = false;
                    },
                    (error) => { this.manejaError(error); this.displayCambioEstado = false;},
                    () => sub.unsubscribe()
                )
            }
        )

    }

    /**
     * Redirecciona a la lista de noticia
     */
    goBack() {
        this.router.navigate([Rutas.RT_NOTICIA]);
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}