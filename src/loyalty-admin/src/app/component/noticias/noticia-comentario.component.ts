import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { AppConfig } from '../../app.config';
import { Noticia, NoticiaComentario, Usuario } from '../../model/index';
import { NoticiaService, UsuarioService, KeycloakService, I18nService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';
import { PERM_ESCR_NEWSFEED } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';

@Component({
    selector: 'comentario-relacionado',
    templateUrl: 'noticia-comentario.component.html',
    providers: [NoticiaService, NoticiaComentario]
})

/**
 * Jaylin Centeno
 * Componente que permite manipular los comentarios de la noticia 
 */
export class NoticiaComentarioComponent implements OnChanges {
    //Usuario en sesión
    @Input()
    usuario: Usuario
    //Noticia asociada
    @Input()
    noticia: Noticia
    @Input()
    //Comentario seleccionado
    comentarioPricipal: NoticiaComentario;
    //Prueba
    @Output()
    change: EventEmitter<number> = new EventEmitter<number>();

    public escritura: boolean; //Guarda el estado del permiso (true/false)
    public estadoArchivado: boolean; //Según el estado, muestra el botón de edición

    // -------- Comentarios Principales-------- // 
    //En esta lista se guardan los comentarios que selecciona el usuario en orden jerarquico
    public listaComentarios: NoticiaComentario[] = []; 

    // -------- Manipuación de comentario ---------//
    public comentario: NoticiaComentario; //Utilizado en diferentes acciones
    public respuesta: string; // Guarda el contenido que tendrá el nuevo comentario
    public respuestaEdicion: string; //Guarda el contenido del comentario seleccionado a editar
    public vacia: boolean; // True = respuesta, false = vacio
    public publicando: boolean = false; //Usado para habilitar/deshabilitar el botón de publicar
    //public menuItems: any[]; //Muestra los items disponibles para el comentario (responder, borrar, editar)
    public item: any = ""; //Guarda el item seleccionado en el menú
    public displayEditarComentario: boolean; // Manipula el DOM para mostrar la edición de un comentario
    public modalRemover: boolean; //Habilita el cuadro de dialogo para eliminar un comentario

    // ------- Comentarios relacionados --------- //
    public listaRelacionados: NoticiaComentario[] = []; //Lista de los comentarios relacionados al comentario seleccionado como principal
    public relacionadosVacia: boolean = true; //True si la lista esta vacia
    public untilRelacionado: number; //Fecha de inicio para buscar los comentarios
    public sinceRelacionado: number; //Fecha del último comentario recibido

    //------ Infinite Scroll -------//
    // Se utilizan para el [ngClass] según la cantidad de comentarios en la lista  //Tamaño del div para infinite scroll
    public mayor: boolean;// height = 900
    public menor: boolean;// height = 600
    public scrollDown: boolean; // true : scrollup, false: scrolldown
    public listaScroll: NoticiaComentario[] = [];//Se guardan los comentarios obtenidos cada vez que se hace un llamado al scroll
    public listaCompleta: boolean; //mostrar los botones de carga de comentarios sin necesidad del scroll o info de no comentarios
    public tipoLlamado: string = "N"; //Permite manejar como se agregan los comentarios a la lista segun el tipo de llamado
    //I=inserción E=edición R=remover N=ninguno S=scroll
    public tipoScroll: string; // Para manejar si el scroll es hacia arriba/abajo
    /*Permite mostrar el botón del up para cargar los registros 
      en caso de ser la primera vez u ocurra algún fallo al realizar la transaccion*/
    public botonUp: boolean;
    /*Permite mostrar el botón del down para cargar los registros 
      en caso de ser la primera vez u ocurra algún fallo al realizar la transaccion*/
    public botonDown: boolean;
    public scrollVacio: boolean = false;//Misma funcion de lista completa
    public cargando: boolean = false; //Permite mostrar el gif de carga, dif de iniciando carga
    public editar: boolean = false; //True = muestra la edición del comentario; False = muestra la información
    public iniciandoCarga: boolean = true; //Permite mostrar el gif para carga de datos
    public comentarioSeleccionado: NoticiaComentario; //Utilizado para guardar un comentario especifico

    constructor(
        public noticiaService: NoticiaService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService,
        public route: ActivatedRoute,

    ) {
        this.comentario = new NoticiaComentario();
        //Se obtiene true/false que indica si el suario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_NEWSFEED).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

    }
    /**
     * Método llamado cada vez que hay un cambio en el comentario seleccionado
     */
    ngOnChanges(changes: SimpleChanges) {
        if (this.comentarioPricipal.idComentario != undefined) {
            this.untilRelacionado = undefined;
            this.sinceRelacionado = undefined;
            if (changes['comentarioPricipal']) {
                this.listaRelacionados = [];
                this.listaComentarios = [];
                this.listaComentarios = [...this.listaComentarios, this.comentarioPricipal]
                this.obtenerComentariosRelacionados();
            }
        }
    }

    /**
     * Método llamado por el evento scrollUp del infinite scroll
     */
    onScrollUp() {
        this.tipoLlamado = "S";
        this.tipoScroll = "up";
        this.untilRelacionado = this.listaRelacionados[0].fechaCreacion;
        this.sinceRelacionado = undefined;
        this.obtenerComentariosRelacionados();
    }

    /**
     * Método llamado por el evento scroll del infinite scroll 
     */
    onScroll() {
        this.botonDown = false;
        this.tipoLlamado = "S";
        this.tipoScroll = "down";
        this.sinceRelacionado = this.listaRelacionados[this.listaRelacionados.length - 1].fechaCreacion;
        this.untilRelacionado = undefined;
        this.obtenerComentariosRelacionados();
    }

    /**
     * Se obtiene los comentarios relacionados a la noticia
     */
    obtenerComentariosRelacionados() {
        if (!this.cargando) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.noticiaService.token = token;
                    this.cargando = true;
                    if (this.item == "CR" || this.item == "CP") {
                        this.comentarioSeleccionado = this.listaComentarios[this.listaComentarios.length - 1];
                    } else {
                        this.comentarioSeleccionado = this.comentarioPricipal
                    }
                    let sub = this.noticiaService.obtenerListaComentariosDeComentario(this.noticia.idNoticia, this.comentarioSeleccionado.idComentario, this.untilRelacionado, this.sinceRelacionado).subscribe(
                        (response: Response) => {
                            this.listaScroll = response.json();
                            //Si la lista contiene elementos, se verifica que tipo de llamado
                            if (this.listaScroll.length > 0) {
                                //Si es I=insertar o N=ninguno se guarda directamente en la lista de comentarios
                                if (this.tipoLlamado == "I" || this.tipoLlamado == "N") {
                                    //botonUp es true para que el usuario pueda cargar 
                                    //los métodos más antiguos sin tener que hacer el scrollUp
                                    this.botonUp = true;
                                    this.listaRelacionados = this.listaScroll;
                                    //Es el valor por defecto
                                    this.tipoLlamado = "N";
                                } else { //Ingresa por cualquier llamado al método (S=scroll, E=editar)
                                    //Se pregunta por el tipo de scroll en la lista
                                    if (this.tipoScroll == "down") {
                                        //los elementos se agregan al final de la lista
                                        this.listaScroll.forEach(element => {
                                            this.listaRelacionados = [...this.listaRelacionados, element];
                                        })
                                    } else {
                                        //se agregan al final de la lista
                                        this.listaScroll.forEach(element => {
                                            this.listaRelacionados = [element, ...this.listaRelacionados];
                                        })
                                    }
                                }
                                //Se compara el Tamaño de la lista con la cantidad de comentarios en la noticia
                                if (this.comentarioSeleccionado.cantComentarios == this.listaRelacionados.length) {
                                    //Muestra el msj que no hay mas elementos
                                    this.listaCompleta = true;
                                    this.scrollVacio = false;
                                }
                            } else { //Si la lista NO contiene elementos
                                this.scrollVacio = true;
                                //Se compara el Tamaño de la lista con la cantidad de comentarios en la noticia
                                if (this.comentarioSeleccionado.cantComentarios == this.listaRelacionados.length) {
                                    //Muestra el msj que no hay mas elementos
                                    this.listaCompleta = true;
                                }
                            }

                            //Se utiliza para mostrar las clases según el tamaño de la lista [ngClass]
                            if (this.listaRelacionados.length >= 10) {
                                this.mayor = true;
                                this.menor = false;
                                //this.posicionScroll('mayor');
                            } else {
                                this.menor = true;
                                this.mayor = false;
                                //this.posicionScroll('menor');
                            }
                            if (this.listaRelacionados.length > 0) {
                                this.relacionadosVacia = false;
                            } else {
                                this.relacionadosVacia = true;
                            }
                            this.iniciandoCarga = false;
                            this.cargando = false;
                        },
                        (error) => { this.manejaError(error); this.botonUp = true; this.botonDown = true; this.cargando = false; this.iniciandoCarga = false; },
                        () => sub.unsubscribe()
                    )
                }
            )
        }
    }

    /**
     * En caso de que el usuario quiera volver a un comentario principal
     * Se remueven todos los comentarios debajo de este en el array de los comentarios principales
     */
    clickAComentarioPrincipal() {
        let i = this.listaComentarios.findIndex(element => element.idComentario == this.comentario.idComentario);
        if (this.listaComentarios.length != 1) {
            this.listaComentarios.splice(i + 1);
            this.sinceRelacionado = undefined;
            this.untilRelacionado = undefined;
            this.listaComentarios = [...this.listaComentarios]
            this.listaRelacionados = [];
        }
        this.obtenerComentariosRelacionados();
    }

    /**
     * En caso de que el usuario quiera responder a un comentario que responde al comentario principal
     * Se agrega el comentario seleccionado al array con los comentarios principales
     */
    clickAComentarioReferente() {
        let i = this.listaComentarios.find(element => element.idComentario == this.comentario.idComentario);
        if (i == undefined) {
            this.listaComentarios = [...this.listaComentarios, this.comentario]
            this.listaRelacionados = [];
            this.sinceRelacionado = undefined;
            this.untilRelacionado = undefined;
            this.obtenerComentariosRelacionados();
        }
        
    }

    /**
     * Restaura los valores utilizados para manipular comentarios
     */
    limpiarValores(accion?: string) {
        this.comentario = new NoticiaComentario();
        this.displayEditarComentario = false;
        this.modalRemover = false;

        if (accion == "metodo") {
            this.respuesta = "";
            this.respuestaEdicion = "";
            if (this.tipoLlamado == "I") {
                this.sinceRelacionado = undefined;
                this.untilRelacionado = undefined;
            }
            //R=remover, no es necesario hacer el llamado en este caso
            if (this.tipoLlamado != 'R' && this.tipoLlamado != 'E') {
                this.obtenerComentariosRelacionados();
            }
        }
    }

    /**
      * Controla los dialogos a mostrar según el item seleccionado
      */
    mostrarModal(comentario: NoticiaComentario) {
        this.comentario = comentario;
        switch (this.item) {
            case "E":
                this.respuestaEdicion = this.comentario.contenido;
                this.editar = true;
                break;
            case "B":
                this.modalRemover = true;
                break;
            case "CR":
                this.clickAComentarioReferente();
                break;
            case "CP":
                this.clickAComentarioPrincipal();
                break;
            default:
                this.item = "";
                break;
        }
    }

    /**
     * Ingreso de un nuevo comentario
     */
    agregarComentario() {
        this.limpiarValores();
        this.publicando = true;
        if (this.respuesta != null && this.respuesta.trim() != "") {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.noticiaService.token = token;
                    this.comentario.contenido = this.respuesta;
                    this.comentario.comentarioReferente = this.comentarioSeleccionado;
                    let sub = this.noticiaService.insertarComentario(this.noticia.idNoticia, this.comentario).subscribe(
                        (data: Response) => {
                            let cantidad = data.json();
                            this.comentarioSeleccionado.cantComentarios = cantidad['cantComentarios'];
                            this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                            this.publicando = false;
                            this.tipoLlamado = "I";
                            this.limpiarValores('metodo');
                        },
                        (error) => { this.manejaError(error); this.limpiarValores('metodo'); this.publicando = false; },
                        () => sub.unsubscribe()
                    )
                }
            )
        } else {
            this.vacia = true;
            this.publicando = false;
        }
    }

    /**
     * Remover comentario de la noticia
     */
    removerComentario() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.noticiaService.token = token;
                let sub = this.noticiaService.removerComentario(this.noticia.idNoticia, this.comentario.idComentario).subscribe(
                    (data: Response) => {
                        this.tipoLlamado = "R";
                        this.comentarioSeleccionado.cantComentarios -= 1;
                        let i = this.listaRelacionados.findIndex(element => element.idComentario == this.comentario.idComentario);
                        this.listaRelacionados.splice(i, 1);
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.limpiarValores('metodo');
                    },
                    (error) => { this.manejaError(error); this.limpiarValores('metodo'); },
                    () => sub.unsubscribe()
                )
            }
        )
    }

    /**
     * Editar un comentario propio
     */
    editarComentario() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.noticiaService.token = token;
                let temp = this.comentario.contenido;
                this.comentario.contenido = this.respuestaEdicion;
                let sub = this.noticiaService.modificarComentario(this.noticia.idNoticia, this.comentario).subscribe(
                    (data: Response) => {
                        this.editar = false;
                        this.tipoLlamado = "E";
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        let i = this.listaRelacionados.findIndex(element => element.idComentario == this.comentario.idComentario);
                        this.comentario.numVersion += 1;
                        this.listaRelacionados[i] = this.comentario;
                        this.listaRelacionados = [... this.listaRelacionados];
                        this.limpiarValores('metodo');
                    },
                    (error) => { this.manejaError(error); this.limpiarValores('metodo'); this.editar = false; this.comentario.contenido = temp },
                    () => sub.unsubscribe()
                )
            }
        )
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}