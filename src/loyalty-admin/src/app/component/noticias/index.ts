export { NoticiaComponent } from './noticia.component';
export { NoticiaListaComponent } from './noticia-lista.component';
export { NoticiaInsertarComponent } from './noticia-insertar.component';
export { NoticiaDetalleComponent } from './noticia-detalle.component';
export { NoticiaDetalleGeneralComponent } from './noticia-detalle-general.component';
export { NoticiaSegmentoElegiblesComponent } from './noticia-segmentos.component';
export { CategoriaNoticiaListaComponent } from './categoria/categoria-noticia-lista.component';
export { CategoriaNoticiaDetalleComponent } from './categoria/categoria-noticia-detalle.component';
export { NoticiaComentarioComponent } from './noticia-comentario.component';