import { Component, OnInit, Renderer } from '@angular/core';
import { Response, Headers } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { Noticia, Segmento } from '../../model/index';
import { SegmentoService, NoticiaListaSegmentoService, NoticiaService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_NEWSFEED, PERM_ESCR_SEGMENTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'noticia-segmentos-component',
    templateUrl: 'noticia-segmentos.component.html',
    providers: [SegmentoService, Segmento, NoticiaListaSegmentoService, NoticiaService, Noticia]
})
/**
 * Jaylin Centeno
 * Componente que permite administrar los segmentos incluidos/excluidos de la noticia
 */
export class NoticiaSegmentoElegiblesComponent implements OnInit {

    public id: string; // Id de la noticia pasado por parametros
    public escrituraNoticia: boolean; //Permiso de noticia
    public escrituraSegmento: boolean; //Permiso de noticia
    public cargandoDatos: boolean = true;
    public eliminar: boolean; //Habilita el cuadro de dialogo para confirmar la eliminacion de segmento(s)
    public estadoArchivado: boolean = true; //Permite guardar si la noticia esta archivada o no
    public tipo: string; //Guarda el tipo de elemento a remover
    public pregunta: string; //Se guarda la pregunta segun el tipo 

    //--------- Segmentos --------- //
    public listaSegmentos: Segmento[]; // Lista con los segmentos disponibles en el sistema
    public listaVacia: boolean = true; // Valida que la lista de disponibles no esta vacía
    public indAccion: string = "I"; // Permite determinar la acción, ya sea incluir o excluir
    public elegible: string = "D"; // Indica cual es el elemento elegible a mostrar (Elegibles/Disponibles) 
    public totalRecords: number; // Total de segmentos en el sistema
    public cantidad: number = 10; // Cantidad de filas
    public filaDesplazamiento: number = 0; // Desplazamiento de la primera fila

    public listaSeleccion: Segmento[] = []; // Lista que contendra la seleccion de los segmentos, para asociar o remover
    public listaRemover: Segmento[] = [];
    public lista: string[] = []; // Lista que contendra el id de los segmentos, para agregar o remover 
    public itemsAccion: SelectItem[] = []; // Items del dropdown, muestra las acciones de incluir o excluir 
    public itemsElegibles: SelectItem[] = []; // Items del dropdown, muestra los elementos de elegibles 
    public idSegmento: string; // Contendra el id del segmento a eliminar     

    public busqueda: string; // Búsqueda de segmentos, guarda las palabras para realizar las búsquedas
    public listaBusqueda: string[] = []; //Palabras de búsqueda, ChipModule
    public avanzada: boolean = false; //Permite mostrar la búsqueda avanzada
    public tipos: string[] = []; //Indicador de tipo segmento estatico/dinamico 
    public cargaInicial: boolean = true;

    constructor(
        public noticia: Noticia,
        public listaNoticiaService: NoticiaListaSegmentoService,
        public noticiaService: NoticiaService,
        public segmentoService: SegmentoService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public router: Router,
        public msgService: MessagesService,
        public route: ActivatedRoute,
        public i18nService: I18nService
    ) {
        //Obtiene el id de la ruta padre
        this.route.parent.params.forEach((params: Params) => { this.id = params['id'] });
        //Lista de elementos predeterminada en el sistema
        this.itemsAccion = this.i18nService.getLabels("accion-elegibles-general");
        this.itemsElegibles = this.i18nService.getLabels("tipos-elegibles-general");
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_NEWSFEED).subscribe((permiso: boolean) => {
            this.escrituraNoticia = permiso;
        });
        this.authGuard.canWrite(PERM_ESCR_SEGMENTOS).subscribe((permiso: boolean) => {
            this.escrituraSegmento = permiso;
        });
        this.tipos = ['D', 'E']
    }

    /**
     * Método llamado solo al entrar por primera vez al componente
     */
    ngOnInit() {
        this.obtenerDetalle();
        this.obtenerSegmentos();
    }
    /**
     * Método que muestra un cuadro de dialogo para pedir una confirmación
     * Recibe una acción a realizar sobre el segmento
     */
    dialogoConfirmar(tipo: string, idSegmento?: string) {
        this.tipo = tipo;
        this.pregunta = this.i18nService.getLabels("confirmacion-pregunta")[tipo];
        this.idSegmento = idSegmento;
        this.eliminar = true;

    }

    /**
     * Permite la carga de los segmentos de manera perezosa 
     */
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerSegmentos();
    }

    /**
     * Obtiene la definición de la noticia
     */
    obtenerDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.noticiaService.token = token;
                let sub = this.noticiaService.obtenerNoticiaId(this.id).subscribe(
                    (data: Response) => {
                        this.noticia = data.json();
                        if (this.noticia.indEstado == 'B') {
                            this.estadoArchivado = true;
                        } else { this.estadoArchivado = false }
                        this.cargaInicial = false;
                    },
                    (error) => { this.manejaError(error); this.cargaInicial = false; },
                    () => sub.unsubscribe()
                )
            }
        )
    }
    /**
     * Método para obtener la lista de los segmentos según la acción
     * Si la acción es incluir o excluir se obtendran los segmentos que ya estan en los elegibles
     * Si la acción es disponible se obtendran los segmentos que no están en los elegibles
     */
    obtenerSegmentos() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.listaNoticiaService.token = token;
                this.listaNoticiaService.obtenerListaSegmentosNoticia(this.id, this.elegible, this.cantidad, this.filaDesplazamiento, this.busqueda, this.tipos).subscribe(
                    (response: Response) => {
                        this.listaSegmentos = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaSegmentos.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                )
            }
        );
    }

    /**
     * Se agrega un segmento a los elegibles de la noticia, 
     */
    agregarSegmento(idSegmento: string) {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.listaNoticiaService.token = token;
                let sub = this.listaNoticiaService.asignarSegmento(idSegmento, this.id, this.indAccion).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                        this.obtenerSegmentos();
                        if (this.listaSeleccion.length > 0) {
                            let index = this.listaSeleccion.findIndex(element => element.idSegmento == idSegmento);
                            this.listaSeleccion.splice(index, 1);
                        }
                    },
                    (error) => { this.manejaError(error); },
                );
            });
    }

    /**
     * Método para agregar la lista de segmentos a los elegibles de la noticia
     */ 
    agregarListaSegmentos() {
        if (this.listaSeleccion.length > 0) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.listaNoticiaService.token = token;
                    this.listaSeleccion.forEach(segmento => {
                        this.lista = [...this.lista, segmento.idSegmento]
                    })
                    this.listaNoticiaService.asignarListaSegmentosNoticia(this.id, this.lista, this.indAccion).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-multiple"));
                            this.listaSeleccion = [];
                            this.lista = [];
                            this.obtenerSegmentos();
                        },
                        (error) => { this.manejaError(error); },
                    );
                }
            );
        } else {
            this.msgService.showMessage(this.i18nService.getLabels("warn-cantidad"));
        }
    }
    /**
     * Método para remover de los elegibles una lista de los segmentos
     */
    removerListaSegmentos() {
        if (this.listaRemover.length > 0) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.listaNoticiaService.token = token;
                    this.listaRemover.forEach(segmento => {
                        this.lista = [...this.lista, segmento.idSegmento]
                    })
                    this.listaNoticiaService.removerAsignacionListaNoticia(this.id, this.lista).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-multiple"));
                            this.obtenerSegmentos();
                            this.listaRemover = [];
                            this.lista = [];
                            this.eliminar = false;
                        },
                        (error) => { this.manejaError(error); this.eliminar = false; },
                    );
                }
            );
        } else {
            this.msgService.showMessage(this.i18nService.getLabels("warn-cantidad"));
        }
    }
    /**
     * Método para eliminar un segmento de la lista de los elegibles
     */
    removerSegmento() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (tkn: string) => {
                this.listaNoticiaService.token = tkn;
                let sub = this.listaNoticiaService.removerAsignacioSegmento(this.id, this.idSegmento).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.eliminar = false;
                        this.obtenerSegmentos();
                        if (this.listaRemover.length > 0) {
                            let index = this.listaRemover.findIndex(element => element.idSegmento == this.idSegmento);
                            this.listaRemover.splice(index, 1);
                        }
                    },
                    (error) => { this.manejaError(error); this.eliminar = false; },
                    () => { sub.unsubscribe() }
                );
            });
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }


}