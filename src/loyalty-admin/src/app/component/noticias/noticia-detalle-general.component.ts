import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Noticia, NoticiaComentario, Usuario } from '../../model/index';
import { NoticiaService, UsuarioService, KeycloakService, I18nService } from '../../service/index';
import { SelectItem } from 'primeng/primeng';
import { AppConfig } from '../../app.config';
import { PERM_ESCR_NEWSFEED } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Response } from '@angular/http';

@Component({
    moduleId: module.id,
    selector: 'noticia-detalle-general-component',
    templateUrl: 'noticia-detalle-general.component.html',
    providers: [NoticiaService, NoticiaComentario]
})
/**
 * Jaylin Centeno
 * Componente que permite administrar los detalles de la noticia, incluyendo los comentarios (de primer nivel)
 */
export class NoticiaDetalleGeneralComponent implements OnInit {

    public id: string; //Id de la noticia, pasado por parametros
    public formNoticia: FormGroup; //Formulario utilizado para los detalles de la noticia
    public imagen: any; //Obtiene el archivo subido por el usuario
    public imgTemp: any; //Guarda la imagen original de la noticia temporalmente
    public imagenNoticia: any; //Guarda la imagen a mostrar en el template
    public escritura: boolean; //Guarda el estado del permiso (true/false)
    public subiendoImagen: boolean = false; // Carga de la imagen
    public estadoArchivado: boolean; //Según el estado, muestra el botón de edición
    public editar: boolean; //Muestra los componentes para la edición de la noticia

    // -------- Comentarios Noticia-------- // 
    public listaComentarios: NoticiaComentario[] = []; //Lista de los comentarios de la noticia
    public listaVacia: boolean; //True si la lista esta vacia
    public totalRegistros: number; //Cantidad de registros mostrados
    public until: number; //Fecha de inicio para buscar los comentarios 
    public since: number; //Fecha del último comentario recibido 

    // -------- Manipulación de comentario ---------//
    public nuevoComentario: boolean; //Manipula el DOM para mostrar la inserción de un comentario
    public respuesta: string; // Guarda el contenido de un comentario 
    public vacia: boolean; // True = respuesta, false = vacio
    public publicando: boolean = false; //Usado para habilitar/deshabilitar el botón de publicar
    public item: any = ""; //Guarda el item seleccionado en el menú
    public displayEditarComentario: boolean; // Habilita el cuadro de dialogo para la edición de un comentario
    public displayRemoverComentario: boolean; //Habilita el cuadro de dialogo para eliminar un comentario

    // ------- Comentarios relacionados --------- //
    public displayComentarioRelacionado: boolean; //Habilita el cuadro de dialogo para mostrar los comentario relacionados a otro comentario

    //------ Infinite Scroll -------//
    // Se utilizan para el [ngClass] según la cantidad de comentarios en la lista  //Tamaño del div para infinite scroll
    public mayor: boolean;// height = 900
    public menor: boolean;// height = 600
    public listaScroll: NoticiaComentario[] = [];//Se guardan los comentarios obtenidos cada vez que se hace un llamado al scroll
    public listaCompleta: boolean; //Permite mostrar los botones para pedir la carga de comentarios manualmente
    public tipoLlamado: string = "N"; /*Permite manejar como se agregan los comentarios a la lista segun el tipo de llamado
    I=inserción E=edición R=remover N=ninguno S=scroll*/
    public tipoScroll: string; // Para manejar si es un scroll tipo up/down
    /*Permite mostrar el botón del up para cargar los registros 
      en caso de ser la primera vez u ocurra algún fallo al realizar la transaccion*/
    public botonUp: boolean;
    /*Permite mostrar el botón del down para cargar los registros 
      en caso de ser la primera vez u ocurra algún fallo al realizar la transaccion*/
    public botonDown: boolean;
    public scrollVacio: boolean = false; //True = listaScroll es vacia; false = listaScroll con elementos 
    public cargando: boolean = false; //Permite mostrar la imagen de carga
    public cargaInicial: boolean = true; //utilizada solo para mostrar el gif para la carga inicial
    public autorComentario: boolean = false; //Utilizado para comprobar si el comentario pertenece al usuairo en sesión
    
    constructor(
        public noticia: Noticia,
        public noticiaService: NoticiaService,
        public comentario: NoticiaComentario,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService,
        public route: ActivatedRoute,
        public fb: FormBuilder,
        public fbComment: FormBuilder,
        public usuario: Usuario,
        public usuarioService: UsuarioService
    ) {
        //Se obtiene el id de la noticia
        this.route.parent.params.forEach((params: Params) => { this.id = params['id'] });
        //Se obtiene true/false que indica si el suario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_NEWSFEED).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        //Se usa la imagen por defecto del sistema
        this.imagenNoticia = `${AppConfig.DEFAULT_IMG_URL}`;
        //Formulario utilizado para edición 
        this.formNoticia = this.fb.group({
            'titulo': ['', Validators.compose([
                Validators.required,
                Validators.maxLength(100)
            ])],
            'contenido': ['', Validators.compose([
                Validators.required,
                Validators.maxLength(300)
            ])]
        });
    }

    /**
     * Llamado solo una vez al iniciarce el componente
     */
    ngOnInit() {
        this.mostrarDetalle();
        this.obtenerPerfilUsuario();
        this.obtenerComentarios();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Obtiene la definición de la noticia
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.noticiaService.token = token;
                let sub = this.noticiaService.obtenerNoticiaId(this.id).subscribe(
                    (data: Response) => {
                        this.copyValuesOf(this.noticia, data.json());
                        if (this.noticia.indEstado == 'B') {
                            this.estadoArchivado = true;
                        } else { this.estadoArchivado = false }
                        this.imagenNoticia = this.noticia.imagen;
                        this.imgTemp = this.noticia.imagen;
                        this.cargaInicial = false;
                    },
                    (error) => { this.manejaError(error); this.cargaInicial = false; },
                    () => sub.unsubscribe()
                )
            }
        )
    }

    /**
     * Obtiene el perfil del usuario en sesión
     */
    obtenerPerfilUsuario() {
        this.keycloakService.getToken().then(
            (token) => {
                this.usuarioService.token = token;
                let sub = this.usuarioService.obtenerPerfilUsuario().subscribe(
                    (data: Response) => {
                        this.usuario = data.json();
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); }
                );
            }
        );
    }

    /**
     * Habilita la edición de la noticia y asigna los valores al formulario
     */
    mostrarEdicion() {
        this.editar = true;
        this.formNoticia.patchValue({ titulo: this.noticia.titulo, contenido: this.noticia.contenido });
    }

    /**
     * Cancela la edición 
     */
    cancelarEdicion() {
        this.editar = false;
        this.imagenNoticia = this.noticia.imagen;
    }

    /**
     * Modifica los datos de la noticia 
     */
    modificarNoticia() {
        if (this.subirImagen) {
            this.msgService.showMessage(this.i18nService.getLabels("info-uploading"));
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.noticiaService.token = token;
                this.noticia.titulo = this.formNoticia.get('titulo').value;
                this.noticia.contenido = this.formNoticia.get('contenido').value;
                this.noticia.imagen = this.imgTemp;
                this.noticiaService.modificarNoticia(this.noticia).subscribe(
                    (data) => {
                        this.editar = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.formNoticia.reset();
                        this.mostrarDetalle();
                    }
                )
            }
        )

    }

    /**
     * Método llamado por el evento scrollUp del infinite scroll
     */
    onScrollUp() {
        this.tipoLlamado = "S";
        this.tipoScroll = "up";
        this.until = this.listaComentarios[0].fechaCreacion;
        this.since = undefined;
        this.obtenerComentarios();
    }

    /**
     * Método llamado por el evento scroll del infinite scroll 
     */
    onScroll() {
        this.botonDown = false;
        this.tipoLlamado = "S";
        this.tipoScroll = "down";
        this.since = this.listaComentarios[this.listaComentarios.length - 1].fechaCreacion;
        this.until = undefined;
        this.obtenerComentarios();
    }

    /**
     * Se obtiene los comentarios relacionados a la noticia
     */
    obtenerComentarios() {
        if (!this.cargando) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.noticiaService.token = token;
                    this.cargando = true;
                    let sub = this.noticiaService.obtenerListaComentariosNoticia(this.id, this.since, this.until).subscribe(
                        (response: Response) => {
                            this.listaScroll = response.json();
                            //Si la lista contiene elementos, se verifica que tipo de llamado
                            if (this.listaScroll.length > 0) {
                                //Si es I=insertar o N=ninguno se guarda directamente en la lista de comentarios
                                if (this.tipoLlamado == "I" || this.tipoLlamado == "N") {
                                    //botonUp es true para que el usuario pueda cargar 
                                    //los métodos más antiguos sin tener que hacer el scrollUp
                                    this.botonUp = true;
                                    this.listaComentarios = this.listaScroll;
                                    //Es el valor por defecto
                                    this.tipoLlamado = "N";
                                } else { //Ingresa por cualquier llamado al método (S=scroll, E=editar)
                                    //Se pregunta por el tipo de scroll en la lista
                                    if (this.tipoScroll == "down") {
                                        //los elementos van al final de la lista por se los más recientes
                                        this.listaScroll.forEach(element => {
                                            this.listaComentarios = [...this.listaComentarios, element];
                                        })
                                    } else {
                                        //los elementos val inicio de la lista ya que son los más antiguos
                                        this.listaScroll.forEach(element => {
                                            this.listaComentarios = [element, ...this.listaComentarios];
                                        })
                                    }
                                }
                                //Se compara el Tamaño de la lista con la cantidad de comentarios en la noticia
                                if (this.noticia.cantComentarios == this.listaComentarios.length) {
                                    //Muestra el msj que no hay mas elementos
                                    this.listaCompleta = true;
                                    this.scrollVacio = false;
                                }
                            } else { //Si la lista NO contiene elementos
                                this.scrollVacio = true;
                                //Se compara el Tamaño de la lista con la cantidad de comentarios en la noticia
                                if (this.noticia.cantComentarios == this.listaComentarios.length) {
                                    //Muestra el msj que no hay mas elementos
                                    this.listaCompleta = true;
                                }
                            }

                            //Se utiliza para mostrar las clases según el tamaño de la lista [ngClass]
                            if (this.listaComentarios.length >= 10) {
                                this.mayor = true;
                                this.menor = false;
                            } else {
                                this.menor = true;
                                this.mayor = false;
                            }
                            if (this.listaComentarios.length > 0) {
                                this.listaVacia = false;
                            } else {
                                this.listaVacia = true;
                            }
                            this.cargando = false;
                        },
                        (error) => { this.manejaError(error); this.botonUp = true; this.botonDown = true; this.cargando = false; },
                        () => sub.unsubscribe()
                    )
                }
            )
        }
    }

    /**
     * Restaura los valores utilizados para manipular comentarios
     */
    limpiarValores(accion?: string) {
        this.comentario = new NoticiaComentario();
        this.item = "";
        this.displayEditarComentario = false;
        this.displayRemoverComentario = false;
        this.displayComentarioRelacionado = false;

        if (accion == "metodo") {
            this.respuesta = "";
            if (this.tipoLlamado == "I") {
                this.since = undefined;
                this.until = undefined;
            }
            //R=remover, no es necesario hacer el llamado en este caso
            if (this.tipoLlamado != 'R' && this.tipoLlamado != 'E') {
                this.obtenerComentarios();
            }
        }
    }

    /**
     * Controla los dialogos a mostrar según el item seleccionado
     */
    mostrarModal(comentario: NoticiaComentario) {
        this.comentario = comentario;
        switch (this.item) {
            case "E":
                this.respuesta = this.comentario.contenido;
                this.displayEditarComentario = true;
                break;
            case "B":
                this.displayRemoverComentario = true;
                break;
            case "R":
                this.displayComentarioRelacionado = true;
                break;
            default:
                this.item = "";
                break;
        }
    }

    /**
     * Ingreso de un nuevo comentario
     */
    agregarComentario() {
        this.limpiarValores();
        this.publicando = true;
        if (this.respuesta != null && this.respuesta.trim() != "") {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.noticiaService.token = token;
                    this.comentario.contenido = this.respuesta;
                    let sub = this.noticiaService.insertarComentario(this.id, this.comentario).subscribe(
                        (data: Response) => {
                            let cantidad = data.json();
                            this.noticia.cantComentarios = cantidad['cantComentarios'];
                            this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                            this.publicando = false;
                            this.tipoLlamado = "I";
                            this.limpiarValores('metodo');
                        },
                        (error) => { this.manejaError(error); this.limpiarValores('metodo'); this.publicando = false; },
                        () => sub.unsubscribe()
                    )
                }
            )
        } else {
            this.vacia = true;
            this.publicando = false;
        }
    }

    /**
     * Remover comentario de la noticia
     */
    removerComentario() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.noticiaService.token = token;
                let sub = this.noticiaService.removerComentario(this.id, this.comentario.idComentario).subscribe(
                    (data: Response) => {
                        --this.noticia.cantComentarios;
                        let i = this.listaComentarios.findIndex(element => element.idComentario == this.comentario.idComentario);
                        this.listaComentarios.splice(i, 1);
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                        this.tipoLlamado = "R";
                        this.limpiarValores('metodo');
                    },
                    (error) => { this.manejaError(error); this.limpiarValores('metodo'); },
                    () => sub.unsubscribe()
                )
            }
        )
    }

    /**
     * Editar un comentario propio
     */
    editarComentario() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.noticiaService.token = token;
                this.comentario.contenido = this.respuesta;
                let sub = this.noticiaService.modificarComentario(this.id, this.comentario).subscribe(
                    (data: Response) => {
                        this.tipoLlamado = "E";
                        let i = this.listaComentarios.findIndex(element => element.idComentario == this.comentario.idComentario);
                        this.comentario.numVersion += 1;
                        this.listaComentarios[i] = this.comentario;
                        this.listaComentarios = [... this.listaComentarios];

                        this.limpiarValores('metodo');
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                    },
                    (error) => { this.manejaError(error); this.limpiarValores('metodo'); },
                    () => sub.unsubscribe()
                )
            }
        )
    }

    /**
     * Obtiene la imagen subida por el usuario, y lo transforma en un base64
     */
    subirImagen() {
        this.imagen = document.getElementById('imagenNoticia');
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.imagenNoticia = reader.result;
            let urlBase64 = this.imagenNoticia.split(",");
            this.imgTemp = urlBase64[1];
            this.subiendoImagen = true;
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /**
     * Permite redireccionar a la lista de noticia
     */
    goBack() {
        this.router.navigate([Rutas.RT_NOTICIA]);
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}