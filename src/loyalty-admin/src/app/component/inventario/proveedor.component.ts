import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Proveedor } from '../../model/index';
import { ProveedorService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_DOCUMENTO } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import * as Rutas from '../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'proveedor-component',
    templateUrl: 'proveedor.component.html',
    providers: [Proveedor, ProveedorService],
})

/**
 * Jaylin Centeno
 * Componente que permite administrar los proveedores*/
/*---------------------------------------------------------------------------------------------*/
export class ProveedorComponent {

    public accion: boolean = false; //Para verificar el ingreso/edición de un proveedor
    public accionTipo: string; //Representa el tipo de acción a realizar
    public remover: boolean = false; //Habilita el cuadro de dialogo para remover un proveedor
    public escritura: boolean; //Permite verificar permiso de escritura (true/false)
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public formProveedor: FormGroup; // Form group para validar el formulario de premio

    //  ------ Proveedores ------ //
    public listaProveedores: Proveedor[]; //Lista de los proveedores existentes
    public listaVacia: boolean; //Permite verificar si la lista de proveedores está vacía
    public idProveedor: string; //Id del proveedor seleccionado
    public cantidad: number = 10; //Cantidad de filas a mostrar en la tabla
    public totalRecords: number; //Total de proveedores en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la fila

    //  ------ Búsqueda ------  //
    public busqueda: string = ""; //Se guardán los críterios de búsqueda
    public listaBusqueda: string[] = [];
    public camposFiltro: string[] = []; // atributos de la busqueda avanzada
    public avanzada: boolean = false; //Permite activar/desactivar la búsqueda avanzada

    constructor(
        public proveedor: Proveedor,
        public proveedorService: ProveedorService,
        public router: Router,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Validación del formulario y datos por defecto
        this.formProveedor = new FormGroup({
            'codigoProveedor': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
            'direccion': new FormControl(''),
            'cedJuridica': new FormControl(''),
            'telefono': new FormControl('')
        });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_DOCUMENTO).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.busquedaProveedores();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaProveedores();
    }

    //Remover un provedor
    confirmarAccion(idProveedor: string) {
        this.idProveedor = idProveedor;
        this.remover = true;
    }

    //Cancelar la acción de remover
    cancelarAccion() {
        this.accion = false;
        this.formProveedor.reset();
        this.accionTipo = "insertar"
    }

    //Método que realiza una acción según la opción escogida
    accionProveedor() {
        if (this.accionTipo == "editar") {
            this.editarProveedor();
        } else {
            this.agregarProveedor();
        }
    }

    //Método que obtiene el proveedor seleccionado
    verDetalle(proveedor: Proveedor) {
        this.formProveedor.patchValue(proveedor);
        this.proveedor = proveedor;
        this.accionTipo = "editar";
        this.accion = true;
    }

    //Método encargado de realizar la búsqueda de miembros por palabras claves
    busquedaProveedores() {
        this.listaProveedores = [];
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.proveedorService.token = token || "";
                this.proveedorService.obtenerProveedores(this.listaBusqueda.join(" "), this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaProveedores = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1] + 1;
                        }
                        if (this.listaProveedores.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    //Método para agregar un proveedor
    agregarProveedor() {
        if (this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.proveedorService.token = token || "";
                    this.proveedor = this.formProveedor.value;
                    let sub = this.proveedorService.agregarProveedor(this.proveedor).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                            this.listaProveedores = [];
                            this.busquedaProveedores();
                            this.formProveedor.reset();
                            this.accion = false;
                        },
                        (error) => {
                            this.manejaError(error)
                        },
                        () => { sub.unsubscribe(); }
                    );
                }
            );
        }

    }

    editarProveedor() {
        if (this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.proveedorService.token = token || "";
                    this.proveedor.codigoProveedor = this.formProveedor.get('codigoProveedor').value;
                    this.proveedor.descripcion = this.formProveedor.get('descripcion').value;
                    this.proveedor.direccion = this.formProveedor.get('direccion').value;
                    this.proveedor.cedJuridica = this.formProveedor.get('cedJuridica').value;
                    this.proveedor.telefono = this.formProveedor.get('telefono').value;
                    let sub = this.proveedorService.editarProveedor(this.proveedor).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                            this.formProveedor.reset();
                            this.accion = false;
                            this.accionTipo = "insertar";
                        },
                        (error) => {
                            this.manejaError(error)
                        },
                        () => { sub.unsubscribe(); }
                    );
                }
            );
        }

    }

    eliminarProveedor() {
        if (this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.proveedorService.token = token || "";
                    let sub = this.proveedorService.eliminarProveedor(this.idProveedor).subscribe(
                        (data) => {
                            this.remover = false;
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                            this.busquedaProveedores();
                        },
                        (error) => {
                            this.manejaError(error)
                        },
                        () => { sub.unsubscribe(); }
                    );
                }
            );
        }

    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}