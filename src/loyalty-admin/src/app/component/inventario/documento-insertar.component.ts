import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Response, Headers } from '@angular/http';
import { DocumentoInventario, Ubicacion, Proveedor } from '../../model/index';
import { DocumentoInventarioService, UbicacionService, ProveedorService, KeycloakService, I18nService } from '../../service/index';
import * as Routes from '../../utils/rutas';
import { PERM_ESCR_DOCUMENTO, PERM_LECT_UBICACIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'documento-insertar-component',
    templateUrl: 'documento-insertar.component.html',
    providers: [DocumentoInventario, DocumentoInventarioService, Ubicacion, UbicacionService, Proveedor, ProveedorService],

})

/**
 * Componente utilizado para insertar documentos */
/*-------------------------------------------------------------*/
export class DocumentoInsertarComponent implements OnInit {

    public escritura: boolean; //Permite verificar permisos (true/false)
    public lecturaUbicacion: boolean; //Permite verificar si el usuario tiene permisos de lectura (true/false)
    public rango: string; //Rango de años a mostrar en el calendario
    public formDocumento: FormGroup;

    // ------ Ubicacion ------ //
    public origenUbic: Ubicacion;
    public destinoUbic: Ubicacion;

    // ------ Proveedor ------ //
    public proveedorItems: SelectItem[] = []; //Contiene las ubicaciones a mostrar en el dropdown
    public listaProveedores: Proveedor[]; //Lista de los proveedores disponibles
    public noProveedor: boolean = true; //Permite verificar si la lista de proveedores esta vacía

    // ----- Busqueda  ------ //
    public busqueda: string = ""; //Contiene los criterios de búsqueda para la ubicacion
    public listaBusqueda: string[];
    public itemsTipo: SelectItem[]; //Items del dropdown, muestra los tipos de documento 
    public diaDoc: Date; //Fecha del documento   

    constructor(
        public documento: DocumentoInventario,
        public ubicacionSearch: Ubicacion,
        public documentoService: DocumentoInventarioService,
        public ubicacionService: UbicacionService,
        public proveedorService: ProveedorService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_DOCUMENTO).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_UBICACIONES).subscribe((permiso: boolean) => {
            this.lecturaUbicacion = permiso;
        });

        //Validación del forms
        this.formDocumento = new FormGroup({
            'tipo': new FormControl('', Validators.required),
            'codigo': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
            'fecha': new FormControl('', Validators.required),
            'origen': new FormControl('', Validators.required),
            'destino': new FormControl(''),
            'proveedor': new FormControl('')

        });

        //Items del dropdown de tipo de documento
        this.itemsTipo = [];
        this.itemsTipo = this.i18nService.getLabels("documento-inventario-tipo");

        let anno = new Date();
        let annoActual = anno.getFullYear();
        this.rango = (annoActual - 67) + ':' + annoActual;
        this.documento.tipo = "C";
        this.diaDoc = new Date();
    }

    //Método que se ejecuta al entrar por primera vez al componente
    ngOnInit() {
        this.busquedaProveedores();
    }

    //Método encargado de realizar la búsqueda de miembros por palabras claves
    busquedaProveedores() {
        this.listaProveedores = [];
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.proveedorService.token = token || "";
                this.proveedorService.obtenerProveedores("", 50, 0).subscribe(
                    (response: Response) => {
                        this.listaProveedores = response.json();
                        if (this.listaProveedores.length > 0) {
                            this.noProveedor = false;
                            //this.proveedorItems = [...this.proveedorItems, this.i18nService.getLabels('general-default-select')];
                            this.listaProveedores.forEach(element => {
                                this.proveedorItems = [...this.proveedorItems, { value: element, label: element.descripcion }];
                            })
                        } else {
                            this.noProveedor = true;
                        }
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    //Método que se encarga de insertar productos
    insertarDocumento() {
        if (this.escritura) {
            if (this.documento.tipo == 'I') {
                if (this.destinoUbic == undefined) {
                    this.msgService.showMessage(this.i18nService.getLabels('warn-ubicacion-destino'));
                    return;
                } else {
                    if (this.origenUbic.idUbicacion == this.destinoUbic.idUbicacion) {
                        this.msgService.showMessage(this.i18nService.getLabels('warn-ubicacion-igual'));
                        return;
                    }
                }
            }
            if (this.origenUbic == undefined) {
                this.msgService.showMessage(this.i18nService.getLabels('warn-ubicacion-origen'));
                return;
            }
            if (this.documento.tipo == 'C' && this.documento.codProveedor == undefined) {
                this.msgService.showMessage(this.i18nService.getLabels('warn-proveedor'));
                return;
            }
            if (this.documento.tipo != 'C') {
                this.documento.codProveedor = undefined;
            }
            if (this.documento.tipo != 'I') {
                this.documento.ubicacionDestino = undefined;
            }
            if (this.documento.tipo == 'I') {
                this.documento.ubicacionDestino = this.destinoUbic;
            }
            this.documento.ubicacionOrigen = this.origenUbic;
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.documentoService.token = token;
                    let sub = this.documentoService.insertarDocumento(this.documento).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-insercion'));
                            let link = ['inventario/detalleDocumento', data];
                            this.router.navigate(link);
                        },
                        (error) => this.manejaError(error),
                        () => sub.unsubscribe()
                    );
                }
            );
        }
    }

    //Método que redirecciona al detalle del documento seleccionado
    //Recibe:el id del miembro
    goBack() {
        let link = ['inventario'];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
