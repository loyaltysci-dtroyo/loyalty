import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Response, Headers } from '@angular/http';
import { DocumentoInventario, DetalleDocumento, Premio } from '../../model/index';
import { DocumentoInventarioService, PremioService, KeycloakService, I18nService } from '../../service/index';
import * as Routes from '../../utils/rutas';
import { AuthGuard } from '../common/index';
import { PERM_ESCR_DOCUMENTO } from '../common/auth.constants';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'detalle-documento-component',
    templateUrl: 'detalle-documento.component.html',
    providers: [DetalleDocumento, Premio, PremioService]
})
/**
 * Jaylin Centeno
 * Componente en el cual se manejan los detalles del documento 
 */
export class DetalleDocumentoComponent implements OnInit {

    public escritura: boolean; //Permite verificar permisos de escritura (true/false)
    public confirmar: boolean = false; //Habilita el cuadro de dialog para borrar elementos
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public busqueda: string; // Guarda las palabras de los criterios de búsqueda
    public listaBusqueda: string[] = []; //Contiene los criterios de búsqueda
    public formDetalle: FormGroup; //Formulario para controlar la inserción/eliminación de detalles
    public entidadValida: boolean = true; //Indica si el premio buscado es válido
    public unidades: number; //Representa la cantidad de unidades de premio para los detalles
    public precio: number; //Precio del premio

    // ------ Detalles ------ 
    public listaDetalles: DetalleDocumento[]; //Detalles del documento
    public totalRecords: number = 0; //Cantidad total de los detalles
    public cantidad: number = 10; //Cantidad de filas a mostrar
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public listaVacia: boolean = true; //Permite verificar si la lista de detalles esta vacía
    public numDetalle: string; //Número del detalle a eliminar

    // ------ Premios ------- 
    public listaPremios: Premio[]; //Contiene los premio solicitados (por ubic, disp. sist.)
    public totalRecordsPremio: number; //Cantidad total de los premios 
    public cantidadPremio: number = 8; //Cantidad de filas a mostrar
    public filaPremio: number = 0; //Desplazamiento de la primera fila
    public premioVacio: boolean; //Permite verificar si lista de premios esta vacia
    public dialogPremios: boolean = false; //Habilita el cuadro de dialogo con los premio disponibles

    constructor(
        public premio: Premio,
        public premioService: PremioService,
        public detalle: DetalleDocumento,
        public detalleEditar: DetalleDocumento,
        public documento: DocumentoInventario,
        public documentoService: DocumentoInventarioService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        //Valida los permisos de escritura del usuario sobre este modulo
        this.authGuard.canWrite(PERM_ESCR_DOCUMENTO).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Formulario para ingreso/edición de detalles
        this.formDetalle = new FormGroup({
            'codigo': new FormControl(''),
            'precio': new FormControl(''),
            'unidades': new FormControl('', Validators.required),
        })
    }

    //Método que se ejecuta al entrar por primera vez al componente
    ngOnInit() {
        this.obtenerDetalles();
    }

    //Método que permite mostrar la confirmación para remover un detalle
    dialogoConfirmacion(id: string) {
        this.confirmar = true;
        this.numDetalle = id;
    }

    //Metodo para el lazyLoad en la tabla de detalles
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerDetalles();
    }

    //Metodo para el lazyLoad en la tabla de premio
    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadPremio(event: any) {
        this.filaPremio = event.first;
        this.cantidadPremio = event.rows;
        if (this.documento.tipo == 'C') {
            this.buscarPremios();
        } else {
            this.obtenerPremiosUbicacion();
        }
    }

    //Recibe el detalle seleccionado de la lista
    detalleSeleccionado(anytype: any) {
        this.detalleEditar = anytype;
    }

    //Método para obtener la lista de los detalles del documento
    obtenerDetalles() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.documentoService.token = token;
                this.documentoService.obtenerDetallesDocumento(this.documento.numDocumento, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaDetalles = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaDetalles.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /* Método que llama a la busqueda de premios 
     * según el tipo de documento, y habilita el cuadro de dialogo 
     * */
    mostrarPremios() {
        this.dialogPremios = true;
        if (this.documento.tipo == 'C') {
            this.buscarPremios();
        } else {
            this.obtenerPremiosUbicacion();
        }
    }

    /* Se encarga de realizar las busquedas de premios con criterios definidos
     * Se utiliza cuando el tipo de documento es compra
     * */
    buscarPremios() {
        //P = producto físico
        let tipo = ['P'];
        //P = publicado
        let estado = ['P'];
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.premioService.token = token;
                this.premioService.busquedaPremios(this.cantidadPremio, this.filaPremio, tipo, estado, this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaPremios = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecordsPremio = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaPremios.length > 0) {
                            this.premioVacio = false;
                        } else {
                            this.premioVacio = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    /*
    * Método para obtener la lista de los premios disponibles en una ubicación
    * Se utiliza cuando el tipo de documento es ajuste +/-, interubicación 
    */
    obtenerPremiosUbicacion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.documentoService.token = token;
                this.documentoService.obtenerPreimosPorUbicacion(this.documento.ubicacionOrigen.idUbicacion, this.cantidadPremio, this.filaPremio).subscribe(
                    (response: Response) => {
                        this.listaPremios = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecordsPremio = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaPremios.length > 0) {
                            this.premioVacio = false;
                        } else {
                            this.premioVacio = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    //Se guarda premio escogido para el detalle
    premioSeleccionado(premio: Premio) {
        this.premio = premio;
        this.entidadValida = true;
        this.dialogPremios = false;
        this.unidades = 1;
        this.precio = 0;
    }

    /* Al ingresar un código de premio, se verifica si este existe en el sistema
     * Si existe, retorna el detalle del premio
     *  */
    confirmarExistenciaPremio() {
        if (this.premio.codPremio != null && this.premio.codPremio != "") {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.premioService.token = token;
                    this.premioService.obtenerPremioPorId(this.premio.codPremio).subscribe(
                        (data) => {
                            this.premio = data;
                            this.entidadValida = true;
                        },
                        (error: Response) => {
                            //Si devuelve un 102 es que la entidad no existe
                            if (error.status == 102) {
                                this.msgService.showMessage(this.i18nService.getLabels('error-codigo-premio'));
                                this.entidadValida = false;
                                return;
                            } else {
                                this.manejaError(error);
                            }
                        }
                    );
                }
            );
        }
    }

    //Agregar un nuevo detalle
    agregarDetalle() {
        if (this.documento.tipo == 'C' && this.precio == null) {
            this.msgService.showMessage(this.i18nService.getLabels('warn-precio-premio'));
            return;
        }
        if (this.entidadValida) {
            this.detalle.precio = this.precio;
            this.detalle.codPremio = this.premio
            this.detalle.cantidad = this.unidades;
            this.detalle.numDocumento = this.documento;
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.documentoService.token = token;
                    let sub = this.documentoService.insertarDetalle(this.documento.numDocumento, this.detalle).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                            //Reset de los elementos usados
                            this.obtenerDetalles();
                            this.formDetalle.reset();
                            this.premio = new Premio();
                            this.detalle = new DetalleDocumento();
                        },
                        (error) => {
                            this.manejaError(error);
                        },
                        () => sub.unsubscribe()
                    );
                });
        } else {
            this.msgService.showMessage(this.i18nService.getLabels('warn-misiones-premio-juego'));
        }
    }

    //Editar detalle de documento
    editarDetalle() {
        //Validaciones requeridas cuando el precio es tipo C=compra
        if (this.documento.tipo == 'C') {
            if (this.formDetalle.get('precio').value != null && this.detalleEditar.precio != null) {
                this.detalleEditar.precio = this.formDetalle.get('precio').value;
            } else if (this.detalleEditar.precio == null && this.precio == null) {
                this.msgService.showMessage(this.i18nService.getLabels('warn-precio-premio'));
                return;
            }
        }
        //Se valida la cantidad de unidades de un producto
        if (this.formDetalle.get('unidades') && this.detalleEditar.cantidad != null) {
            this.detalleEditar.cantidad = this.formDetalle.get('unidades').value;
        } else if (this.detalleEditar.cantidad == null && this.unidades == null) {
            this.msgService.showMessage(this.i18nService.getLabels('warn-cantidad-premio'));
            return;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.documentoService.token = token;
                let sub = this.documentoService.editarDetalle(this.documento.numDocumento, this.detalleEditar).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                        this.obtenerDetalles();
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => sub.unsubscribe()
                );
            });
    }

    //Método para eliminar un detalle del documento
    eliminarDetalle() {
        this.confirmar = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.documentoService.token = token;
                this.documentoService.eliminarDetalle(this.documento.numDocumento, this.numDetalle).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                        this.removerDetalleLista();
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    //Método para remover los detalles de la lista
    removerDetalleLista() {
        let listaTemporal = this.listaDetalles;
        let posicion;
        if (this.listaDetalles.length >= 0) {
            let index = listaTemporal.findIndex(element => element.numDetalleDocumento == this.numDetalle);
            this.listaDetalles.splice(index, 1);
            this.listaDetalles = [...this.listaDetalles];
        }
        if (this.listaDetalles.length == 0) {
            this.listaVacia = true;
        }
    }

    //Redirecciona a la lista de documentos
    goBack() {
        let link = ['inventario'];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}