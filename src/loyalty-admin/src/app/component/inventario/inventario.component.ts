import {Component}  from '@angular/core';

@Component({
    moduleId: module.id,
    selector:'inventario-component',
    templateUrl:'inventario.component.html',
})

/**
 * Jaylin Centeno
 * Componente padre de los inventarios
 */
export class InventarioComponent  {
    constructor(){}
}