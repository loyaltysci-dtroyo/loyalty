import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { Response, Headers } from '@angular/http';
import { HistoricoMovimientos, Ubicacion, Miembro, Premio } from '../../model/index';
import { HistoricoService, MiembroService, UbicacionService, PremioService, KeycloakService, I18nService } from '../../service/index';
import * as Rutas from '../../utils/rutas';
import { PERM_ESCR_DOCUMENTO, PERM_LECT_UBICACIONES, PERM_LECT_MIEMBROS, PERM_LECT_PREMIOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'movimiento-historico-component',
    templateUrl: 'movimiento-historico.component.html',
    providers: [HistoricoMovimientos, HistoricoService, Premio, PremioService, Miembro, MiembroService, Ubicacion, UbicacionService],

})

/**
 * Jaylin Centeno
 * Componente desde el que se administran los movimientos historicos */
/*-------------------------------------------------------------*/
export class MovimientoHistoricoComponent implements OnInit {

    //Permite verificar si el usuario tiene permisos (true/false)
    public escritura: boolean; 
    public lecturaUbicacion: boolean;
    public lecturaMiembro: boolean;
    public lecturaPremio: boolean;
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public fechaInicio: Date;
    public itemsEstado: SelectItem[] = [];
    public dialogHistorico: boolean; //Habilita el cuadro de dialog para cerrar el mes
    public accion: string;
    public premioNombre: string;
    public miembroNombre: string;
    public premioCod: string;
    public ubicNombre: string;
    public imagen: string;
    public avatar: string;
    public estado: string[] = [];
    public mostrarEstado: boolean;

    // -------- Historicos ---------- //
    public listaHistoricos: HistoricoMovimientos[]; //Movimientos historicos del sistema
    public listaVacia: boolean; //Permite verificar si la lista de historicos esta vacía
    public totalRecords: number; //Total de documentos en el sistema
    public cantidad: number = 10; //Cantidad de filas a mostrar en la tabla
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public idHistorico: string; //Id del historico seleccionado

    // --------- Ubicaciones --------- //
    public ubicSelec: Ubicacion;

    // ---------- Búsqueda ---------- //
    public itemsTipo: SelectItem[]; //Contiene los tipos de documento 
    public busquedaAvanzada: boolean; //Permite activar/desactivar la búsqueda avanzada
    public busqueda: string; //Palabras clave de búsqueda
    public listaBusqueda: string[] = []; //Lista de las palabras ingresadas en el Chip 
    public tipo: string; //Almacena el indicador de tipo de documento para busqueda avanzada

    // -------- Premios ---------- // 
    public premio: Premio; //Premio seleccionado para la búsqueda

    // -------- Miembros ----------
    public displayDialog: boolean; //Muestra el modal de seleccion para el usuario
    public listaMiembros: Miembro[];
    public idMiembro: string;
    public nombre: string;
    public listaVaciaMiembro: boolean;
    public totalRecordsMiembro: number; //Total de premios en el sistema 
    public cantidadMiembro: number = 8; //Cantidad de filas a mostrar
    public filaMiembro: number = 0; //Desplazamiento de la primera fila

    constructor(
        public historico: HistoricoMovimientos,
        public ubicacionSearch: Ubicacion,
        public ubicacionService: UbicacionService,
        public miembroService: MiembroService,
        public premioService: PremioService,
        public historicoService: HistoricoService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Se obtienen los permisos
        this.authGuard.canWrite(PERM_ESCR_DOCUMENTO).subscribe((permiso: boolean) => {
            this.escritura = permiso; //Valida los permisos de escritura del usuario
        });
        this.authGuard.canWrite(PERM_LECT_UBICACIONES).subscribe((permiso: boolean) => {
            this.lecturaUbicacion = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_MIEMBROS).subscribe((permiso: boolean) => {
            this.lecturaMiembro = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_PREMIOS).subscribe((permiso: boolean) => {
            this.lecturaPremio = permiso;
        });
        this.itemsEstado = this.i18nService.getLabels("estado-redencion-premio");
        this.itemsTipo = this.i18nService.getLabels("documento-inventario-tipo");
    }

    //Método que se ejecuta al entrar por primera vez al componente
    ngOnInit() {
        this.busquedaHistoricos();
    }

    cambioEstado(historico: HistoricoMovimientos, accion: string) {
        this.historico = historico;
        this.miembroNombre = this.historico.idMiembro.nombre + " " + this.historico.idMiembro.apellido;
        this.premioNombre = this.historico.codPremio.nombre;
        this.premioCod = this.historico.codPremio.codPremio;
        this.ubicNombre = this.historico.ubicacionOrigen.nombre;
        this.imagen = this.historico.codPremio.imagenArte;
        this.avatar = this.historico.idMiembro.avatar;
        this.accion = accion;
        this.dialogHistorico = true;
    }

    editarHistorico() {
        this.dialogHistorico = false;
        if (this.accion == 'A') { // A = ANULAR
            this.historico.status = 'A';
        } else if (this.accion == 'R') { //R = REDIMIR
            this.historico.status = 'R';
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.historicoService.token = token;
                this.historicoService.editarHistorico(this.historico).subscribe(
                    (response: Response) => {
                        if (this.accion == 'A') { // A = ANULAR 
                            this.msgService.showMessage({ detail: "La redención se ha anulado.", severity: "info", summary: "" });
                        } else if (this.accion == 'R') { //R = REDIMIR
                            this.msgService.showMessage({ detail: "El premio ha sido redimido.", severity: "info", summary: "" });
                        }
                    },
                    (error) => {
                        this.manejaError(error);
                        this.historico.status = 'S';
                    }
                );
            });
    }

    cambioTipo() {
        if (this.tipo == 'R') {
            this.mostrarEstado = true;
        } else {
            this.mostrarEstado = false;
        }
    }

    //Obtiene los historicos de movimiento
    busquedaHistoricos(event?) {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.historicoService.token = token;
                let idPremio ="";
                let idUbicacion ="";
                if(this.premio != undefined){
                    idPremio = this.premio.idPremio;
                }
                if(this.ubicSelec != undefined){
                    idUbicacion = this.ubicSelec.idUbicacion;
                }
                this.historicoService.busquedaHistoricos(this.cantidad, this.filaDesplazamiento, this.tipo, this.estado,
                    this.idMiembro, idPremio, idUbicacion).subscribe(
                    (response: Response) => {
                        this.listaHistoricos = response.json();
                        if (this.listaHistoricos.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => {
                        this.manejaError(error);
                        this.cargandoDatos = false;
                    }
                    );
            });
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        //this.obtenerDocumentoPorTermino();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadDataMiembro(event:any){
        this.filaMiembro = event.first;
        this.cantidadMiembro = event.rows;
        this.busquedaMiembros();
    }

    //Mostrar busqueda avanzada
    toggleBusquedaAvanzada() {
        this.busquedaAvanzada = !this.busquedaAvanzada;
    }

    //Selección del miembro
    seleccionarMiembro(miembro: Miembro){
        this.idMiembro = miembro.idMiembro;
        this.nombre = miembro.nombre + ' ' + miembro.apellido;
        this.displayDialog = false;
    }

    // Método encargado de realizar la búsqueda de miembros por palabras claves
    busquedaMiembros() {
        if (this.lecturaMiembro) {
            let estado =["A"];
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.miembroService.token = token || "";
                    //this.filtros, this.generos, this.estadosCiviles, this.educaciones, this.indEmail, this.indSMS, this.indNotificacion,this.indHijos, this.tipoOrden, this.campoOrden
                    this.miembroService.busquedaMiembros(estado, this.filaMiembro, this.cantidadMiembro, this.listaBusqueda.join(" ")).subscribe(
                        (response: Response) => {
                            this.listaMiembros = response.json();
                            if (response.headers.get("Content-Range") != null) {
                                this.totalRecordsMiembro = + response.headers.get("Content-Range").split("/")[1];
                            }
                            if (this.listaMiembros.length > 0) {
                                this.listaVaciaMiembro = false;
                            } else {
                                this.listaVaciaMiembro = true;
                            }
                        },
                        (error) =>
                            this.manejaError(error)
                    );
                }
            );
        }
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
