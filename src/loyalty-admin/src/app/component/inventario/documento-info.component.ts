import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Response, Headers } from '@angular/http';
import { DocumentoInventario, Ubicacion, Proveedor } from '../../model/index';
import { DocumentoInventarioService, UbicacionService, ProveedorService, KeycloakService, I18nService } from '../../service/index';
import * as Routes from '../../utils/rutas';
import { AuthGuard } from '../common/index';
import { PERM_ESCR_DOCUMENTO, PERM_LECT_UBICACIONES } from '../common/auth.constants';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'documento-info-component',
    templateUrl: 'documento-info.component.html',
    providers: [DocumentoInventario, DocumentoInventarioService, Proveedor, ProveedorService, Ubicacion, UbicacionService]
})

/*Componente en el cual se listan todos los documentos existentes */
/*-------------------------------------------------------------*/
export class DocumentoInfoComponent implements OnInit {

    public editar: boolean = false; //Habilita el dialogo de edición del documento
    public escritura: boolean; //Valida el permiso de escritura sobre documento(true/false)
    public lectura: boolean; //Valida el permiso de lectura sobre ubicaciones(true/false)
    public confirmar: boolean = false; // Habilita el cuadro de diálogo de confirmacion
    public itemsTipo: SelectItem[]; //Muestra los tipos de documento 
    public cargandoDatos: boolean = true; //Permite saber si hay carga de datos

    public cerrado: boolean; //Estado del documento
    public abierto: boolean; //Estado del documento
    public fecha: Date; // Fecha del documento
    public nombreUbicacion: string; //Guarda el nombre de la ubicacion origen del documento
    public destino: string; //Guarda el nombre de la ubicacion origen del documento

    // --- Proveedores --- //
    public listaProveedores: Proveedor[]; //Lista de los proveedores disponibles
    public noProveedor: boolean = true; //Lista proveedor vacia
    public proveedorItems: SelectItem[] = []; //Se muestran las ubicaciones disponibles
    public cantidad: number = 50; //Cantidad de filas
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    constructor(
        public documento: DocumentoInventario,
        public documentoService: DocumentoInventarioService,
        public ubicacionService: UbicacionService,
        public proveedorService: ProveedorService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Se obtiene el parametro que tiene el id del documento
        this.route.params.forEach((params: Params) => { this.documento.numDocumento = params['id'] });

        //Permite saber si el usuario tiene permisos 
        this.authGuard.canWrite(PERM_ESCR_DOCUMENTO).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_UBICACIONES).subscribe((permiso: boolean) => {
            this.lectura = permiso;
        });

        //Items del dropdown de tipo de documento
        this.itemsTipo = [];
        this.itemsTipo = this.i18nService.getLabels("documento-inventario-tipo");
    }

    //Método que se ejecuta al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    //Método que permite mostrar la confirmación para remover un detalle o documento
    mostrarEdicion() {
        if (this.documento.tipo == "C") {
            this.busquedaProveedores();
        }
        this.editar = true;
    }

    //Método para obtener el detalle del documento
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.documentoService.token = token;
                this.documentoService.obtenerDocumentoPorId(this.documento.numDocumento).subscribe(
                    (data) => {
                        this.copyValuesOf(this.documento, data);
                        this.fecha = new Date(this.documento.fecha);
                        if (this.documento.indCerrado) {
                            this.cerrado = true;
                        } else {
                            this.abierto = true;
                        }
                    },
                    (error) => this.manejaError(error)
                );
            });
    }

    //Método encargado de realizar la búsqueda de miembros por palabras claves
    busquedaProveedores() {
        this.listaProveedores = [];
        let busqueda = ""
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.proveedorService.token = token || "";
                this.proveedorService.obtenerProveedores(busqueda, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaProveedores = response.json();
                        if (this.listaProveedores.length > 0) {
                            this.noProveedor = false;
                            //this.proveedorItems = [...this.proveedorItems, { value: "", label: "Seleccione un proveedor" }];
                            this.listaProveedores.forEach(element => {
                                this.proveedorItems = [...this.proveedorItems, { value: element, label: element.descripcion }];
                            })
                        } else {
                            this.noProveedor = true;
                        }
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    //Método que se encarga de editar un documento, siempre y cuando esté abierto
    editarDocumento() {
        if (this.documento.tipo == 'I' && this.documento.ubicacionDestino == null) {
            this.msgService.showMessage(this.i18nService.getLabels('warn-ubicacion-destino'));
            return;
        }
        if (this.documento.ubicacionOrigen == null) {
            this.msgService.showMessage(this.i18nService.getLabels('warn-ubicacion-origen'));
            return;
        }
        if (this.documento.tipo == "I") {
            if (this.documento.ubicacionOrigen.idUbicacion == this.documento.ubicacionDestino.idUbicacion) {
                this.msgService.showMessage(this.i18nService.getLabels('warn-ubicacion-igual'));
                return;
            }
        }
        if (this.documento.tipo == 'C' && this.documento.codProveedor == undefined) {
            this.msgService.showMessage(this.i18nService.getLabels('warn-proveedor'));
            return;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.documentoService.token = token;
                let sub = this.documentoService.editarDocumento(this.documento).subscribe(
                    (data) => {
                        this.mostrarDetalle();
                        this.editar = false;
                        this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                    },
                    (error) => this.manejaError(error),
                    () => sub.unsubscribe()
                );
            }
        );
    }

    //Método para eliminar el documento
    eliminarDocumento() {
        this.confirmar = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.documentoService.token = token;
                this.documentoService.eliminarDocumento(this.documento.numDocumento).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                        this.goBack();
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    //Método para cerrar un documento
    cerrarDocumento() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.documentoService.token = token;
                this.documentoService.cerrarDocumento(this.documento.numDocumento).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('documento-cerrado'));
                        this.cerrado = true;
                        this.abierto = false;
                        this.documento.indCerrado = true;
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    //Método que redirecciona a la lista de documentos
    //Recibe:el id del miembro
    goBack() {
        let link = ['inventario'];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}