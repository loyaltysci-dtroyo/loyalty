import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { Response, Headers } from '@angular/http';
import { DocumentoInventario, Ubicacion } from '../../model/index';
import { DocumentoInventarioService, UbicacionService, KeycloakService, I18nService } from '../../service/index';
import * as Rutas from '../../utils/rutas';
import { PERM_ESCR_DOCUMENTO, PERM_LECT_UBICACIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'documento-lista-component',
    templateUrl: 'documento-lista.component.html',
    providers: [DocumentoInventario, DocumentoInventarioService, Ubicacion, UbicacionService],

})

/*Componente en el cual se listan todos los documentos existentes */
/*-------------------------------------------------------------*/
export class DocumentoListaComponent implements OnInit {

    public escritura: boolean; //Permite verificar si el usuario tiene permisos de escritura (true/false)
    public lecturaUbicacion: boolean; //Permite verificar si el usuario tiene permisos de lectura (true/false)
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public insertar = Rutas.RT_INVENTARIO_INSERTAR_DOCUMENTO; //Ruta para mostar la inserción de documento
    public dialogCierre: boolean; //Habilita el cuadro de dialogo para cerrar el mes

    // -------- Documentos ---------- //
    public listaDocumento: DocumentoInventario[]; //Documentos del sistema
    public listaVacia: boolean; //Permite verificar si la lista de documentos esta vacía
    public totalRecords: number; //Total de documentos en el sistema
    public cantidad: number = 10; //Cantidad de filas a mostrar en la tabla
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public idDocumento: string; //Id del documento seleccionado en la lista

    // --------- Ubicaciones --------- //
    public ubicacion: Ubicacion;

    // ---------- Búsqueda ---------- //
    public itemsTipo: SelectItem[]; //Contiene los tipos de documento 
    public busquedaAvanzada: boolean; //Permite activar/desactivar la búsqueda avanzada
    public busqueda: string; //Palabras clave de búsqueda
    public listaBusqueda: string[] = []; //Lista de las palabras ingresadas en el Chip 
    public tipo: string[] = []; //Almacena los indicadores de tipo de documento para busqueda avanzada
    public tipoOrden: string = ""; //Representa el orden escogido para mostrar los documentos
    public estadoDoc: string = ""; //Representa el estado de los documentos a mostrar
    public campoOrden: string = ""; //Representa el campo por el cual se quiere hacer el ordenamiento

    constructor(
        public documento: DocumentoInventario,
        public ubicacionSearch: Ubicacion,
        public documentoService: DocumentoInventarioService,
        public ubicacionService: UbicacionService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos
        this.authGuard.canWrite(PERM_ESCR_DOCUMENTO).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_UBICACIONES).subscribe((permiso: boolean) => {
            this.lecturaUbicacion = permiso;
        });

        //Tipos de documento
        this.itemsTipo = [];
        this.itemsTipo = this.i18nService.getLabels("documento-inventario-tipo");
    }

    //Método que se ejecuta al entrar por primera vez al componente
    ngOnInit() {
        this.obtenerDocumentoPorTermino();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerDocumentoPorTermino();
    }

    toggleBusquedaAvanzada() {
        this.busquedaAvanzada = !this.busquedaAvanzada;
    }

    //Método que se ejecuta para realizar el cierre de mes
    cierreMes() {
        if (this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.documentoService.token = token;
                    this.documentoService.cerrarMes().subscribe(
                        (response: Response) => {
                            this.dialogCierre = false;
                            this.msgService.showMessage(this.i18nService.getLabels('cierre-mes'));
                        },
                        (error) => {
                            this.dialogCierre = false;
                            this.manejaError(error);
                        }
                    );
                }
            );
        }

    }

    //Método que se encarga de traer la lista de los documentos
    obtenerDocumentoPorTermino() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.documentoService.token = token;
                let idUbicacion;
                if(this.ubicacion != undefined){
                    idUbicacion = this.ubicacion.idUbicacion;
                }
                this.documentoService.obtenerDocumentosPorTerminos(this.cantidad, this.filaDesplazamiento, this.tipo, idUbicacion, this.estadoDoc, this.tipoOrden, this.campoOrden).subscribe(
                    (response: Response) => {
                        this.listaDocumento = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaDocumento.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => { this.manejaError(error); this.cargandoDatos = false; }

                );
            });
    }

    //Redirecciona al componente de inserción
    nuevo() {
        if (this.escritura) {
            let link = ['inventario/insertarDocumento'];
            this.router.navigate(link);
        }
    }

    //Método que redirecciona al detalle del documento seleccionado
    gotoDetail(idDocumento: string) {
        let link = ['inventario/detalleDocumento', idDocumento];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
