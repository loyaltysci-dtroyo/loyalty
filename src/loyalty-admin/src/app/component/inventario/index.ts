export { InventarioComponent } from './inventario.component';
export { DocumentoListaComponent } from './documento-lista.component';
export { DocumentoInfoComponent } from './documento-info.component';
export { ProveedorComponent } from './proveedor.component';
export { DocumentoInsertarComponent } from './documento-insertar.component';
export { DetalleDocumentoComponent } from './detalle-documento.component';
export { MovimientoHistoricoComponent } from './movimiento-historico.component';