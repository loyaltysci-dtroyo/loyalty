import { Component, OnInit, Renderer, ViewChild, ElementRef } from '@angular/core';
import { AtributoDinamico } from '../../model/index';
import { AtributoDinamicoService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Response, RequestOptionsArgs } from '@angular/http';
import { PERM_ESCR_ATRIBUTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'atributo-lista-component',
    templateUrl: 'atributo-lista.component.html',
    providers: [AtributoDinamico, AtributoDinamicoService]
})

/**
 * Jaylin Centeno
 * Componente hijo, se listan todos los atributos existentes
 */
export class AtributoListaComponent {

    public escritura: boolean; //Permite verificar permiso de escritura (true/false)
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public eliminar: boolean; //Habilita el cuadro de dialogo para remover

    // ------ Atributos dinámicos ------ //
    public listaAtributos: AtributoDinamico[]; //Lista de atributos existentes
    public listaVacia: boolean; //Para verificar si la lista esta vacia
    public cantidad: number = 10; //Cantidad de rows
    public totalRecords: number; //Total de miembros en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public idAtributo: string; //Id del atributo seleccionado

    // ------   Busqueda   ------ //
    public avanzada: boolean = false; //Para mostrar la búsqueda avanzada
    public busqueda: string = ""; //Palabras clave de búsqueda
    public listaBusqueda: string[] = [];
    public tipoAtributo: string[] = []; //Almacena los indicadores del tipo
    public estadoAtributo: string[] = []; //Almacena los indicadores del estado 

    constructor(
        public atributo: AtributoDinamico,
        public atributoService: AtributoDinamicoService,
        public renderer: Renderer,
        public router: Router,
        public msgService: MessagesService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_ATRIBUTOS).subscribe(
            (permiso: boolean) => {
                this.escritura = permiso;
            });

        this.estadoAtributo = ['P', 'D'];
        this.tipoAtributo = ['B', 'F', 'N', 'T'];
    }

    //Método que se ejecuta al entrar por primera vez al componente
    ngOnInit() {
        this.busquedaAtributos();
    }

    //Permite mostrar el dialogo de confirmación
    confirmar(atributo: AtributoDinamico) {
        this.eliminar = true;
        this.atributo = atributo;
        this.idAtributo = atributo.idAtributo;
    }

    //Redirecciona a la página de ingresar un nuevo atributo
    nuevoAtributo() {
        let link = ['atributosDinamicos/insertar'];
        this.router.navigate(link);
    }

    //Redirecciona al detalle del atributo
    atributoSeleccionado(idAtributo: string) {
        let link = ['atributosDinamicos/detalleAtributo', idAtributo];
        this.router.navigate(link);
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaAtributos();
    }

    //Método encargado de realizar la búsqueda de atributos
    busquedaAtributos() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.atributoService.token = token;
                this.atributoService.busquedaAtributosDinamicos(this.cantidad, this.filaDesplazamiento, this.estadoAtributo, this.tipoAtributo, this.busqueda).subscribe(
                    (response: Response) => {
                        //Se obtiene la lista de la respuesta enviada
                        this.listaAtributos = response.json();
                        //El content-range tiene la cantidad total de elementos 
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        //Se pregunta por el tamaño de la lista
                        if (this.listaAtributos.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => {
                        this.manejaError(error);
                        this.cargandoDatos = false;
                    }
                );
            }
        );
    }

    //Archiva/Remueve el atributo del sistema
    removerAtributo() {
        this.eliminar = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.atributoService.token = token;
                this.atributoService.eliminarAtributo(this.idAtributo).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                        this.busquedaAtributos();
                        this.cargandoDatos = true;
                        this.eliminar = false;
                    },
                    (error) => { this.manejaError(error); this.eliminar = false; }
                );
            }
        );
    }

    // Metodo para manejar los errores que se provocan en las transacciones
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}