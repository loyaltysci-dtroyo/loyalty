import { Component, OnInit } from '@angular/core';
import { AtributoDinamico } from '../../model/index';
import { AtributoDinamicoService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { PERM_ESCR_ATRIBUTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'atributo-insertar-component',
    templateUrl: 'atributo-insertar.component.html',
    providers: [AtributoDinamico, AtributoDinamicoService]
})

/**
 * Jaylin Centeno
 * Componente hijo, permite la inserción de un atributo
 */
export class AtributoInsertarComponent {

    // Form usado para validar y asignar valores por default
    public formAtributo: FormGroup;
    //Lista para Dropdown de tipos de datos
    public tipoItems: SelectItem[];
    //Representa al tipo de dato del atributo
    public tipoTemporal: string;
    //Guarda el valor del tipo de dato si es Date
    public fechaDefecto: Date;
    //Guarda el valor del tipo de dato si es booleano
    public boolDefecto: boolean = false;
    //Permite verificar permisos (true/false)
    public escritura: boolean;

    constructor(
        public atributo: AtributoDinamico,
        public atributoService: AtributoDinamicoService,
        public router: Router,
        public msgService: MessagesService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public i18nService: I18nService

    ) {
        //inicialización del form group
        this.formAtributo = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'nombreInterno': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
            'indTipoDato': new FormControl('', Validators.required),
            'indVisible': new FormControl('false', Validators.required),
            'valorDefecto': new FormControl(''),
            'fecha': new FormControl(''),
            'indRequerido': new FormControl('false', Validators.required),

        });

        this.tipoItems = [];
        //Se obtiene la lista definida de los tipos de items
        this.tipoItems = this.i18nService.getLabels("atributo-miembro-tipoItems");

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_ATRIBUTOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    //Llama al servicio para agregar un nuevo atributo
    agregarAtributo() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.atributoService.token = token;
                if (this.atributo.indTipoDato == "F") {
                    this.atributo.valorDefecto = this.fechaDefecto.getTime() + '';
                }
                else if (this.atributo.indTipoDato == "B") {
                    this.atributo.valorDefecto = this.boolDefecto + "";
                }
                this.atributoService.agregarAtributo(this.atributo).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                        let link = ['atributosDinamicos/detalleAtributo', data];
                        this.router.navigate(link);
                    },
                    (error) => this.manejaError(error)
                );
            });
    }

    establecerNombre() {
        if (this.atributo.nombre != "" && this.atributo.nombre != null) {
            let temp = this.atributo.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.atributo.nombreInterno = temp;
        }
    }

    //método para evaluar el cambio de valor por defecto
    //evita que se produzca un error a la hora del despliegue.
    cambioTipoDato() {
        this.atributo.valorDefecto = "";
        this.fechaDefecto = new Date();
    }

    //Metodo que permite regresar en la navegación
    goBack() {
        let link = ['atributosDinamicos'];
        this.router.navigate(link);
        //window.history.back();
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}