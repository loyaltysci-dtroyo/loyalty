import { Component, OnInit } from '@angular/core';
import { AtributoDinamico } from '../../model/index';
import { AtributoDinamicoService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { PERM_ESCR_ATRIBUTOS, PERM_LECT_ATRIBUTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'atributo-detalle-component',
    templateUrl: 'atributo-detalle.component.html',
    providers: [AtributoDinamico, AtributoDinamicoService]
})

/**
 * Jaylin Centeno
 * Componente hijo, permite ver el detalle de atributo dinámico 
 */
export class AtributoDetalleComponent implements OnInit {

    public id: string;  //Id del atributo recibido en la url
    public escritura: boolean; //Permite verificar permisos de escritura (true/false)
    public formAtributo: FormGroup; //Formulario del atributo
    public cargandoDatos: boolean = true; //Representa la carga de datos
    public eliminar: boolean; //Habilita el cuadro de dialogo para eliminar

    public tipoItems: SelectItem[]; //Lista con los tipos de datos
    public boolItem: SelectItem[]; //Lista con los datos booleanos
    public estado: string; //Guarda el estado del atributo
    public fecha: Date;  //Contendra el valor de la fecha cuando el atributo sea tipo fecha
    public nuevaFecha: Date; //Guarda el valor de fecha cuando cambia

    //Manejo de las clases para el elemento div con clase card box
    public publicado: boolean;
    public borrador: boolean;
    public archivado: boolean;

    public fechaTimestamp: number;

    constructor(
        public atributo: AtributoDinamico,
        public atributoService: AtributoDinamicoService,
        public authGuard: AuthGuard,
        public router: Router,
        public route: ActivatedRoute,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService

    ) {

        //Se obtiene el id del atributo
        this.route.params.forEach((params: Params) => { this.id = params['id'] });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_ATRIBUTOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //inicialización del form group
        this.formAtributo = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'nombreInterno': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
            'indTipoDato': new FormControl({ value: '', disabled: true }, Validators.required),
            'indVisible': new FormControl('', Validators.required),
            'valorDefecto': new FormControl('', Validators.required),
            'indRequerido': new FormControl('', Validators.required),

        });

        this.tipoItems = [];
        //Se obtiene la lista definida de los tipos
        this.tipoItems = this.i18nService.getLabels("atributo-miembro-tipoItems");

        this.boolItem = [];
        //Se obtiene la lista definida de los tipos booleanos
        this.boolItem = this.i18nService.getLabels("booleano-general");
    }

    //Se llama solo una vez (lifecycle hook)
    ngOnInit() {
        this.mostrarDetalle();
    }

    //Método para mostrar el detalle de la categoría
    mostrarDetalle() {
        //Se obtiene el token
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.atributoService.token = token;
                this.atributoService.obtenerAtributoId(this.id).subscribe(
                    (data) => {
                        this.atributo = data;
                        //ngClass: para seleccionar la clase
                        if (this.atributo.indEstado == 'A') {
                            this.archivado = true;
                        } else if (this.atributo.indEstado == 'P') {
                            this.publicado = true;
                        } else if (this.atributo.indEstado == 'D') {
                            this.borrador = true;
                        }
                        if (this.atributo.indTipoDato == 'F') {
                            if (this.atributo.valorDefecto.includes(' ') == false) {
                                //Parsea la fecha a timestamp
                                this.fechaTimestamp = parseInt(this.atributo.valorDefecto, 10);
                                this.fecha = new Date(this.fechaTimestamp);
                            } else {
                                this.fecha = new Date(this.atributo.valorDefecto);
                            }
                            this.estado = this.atributo.indEstado;
                        }
                        this.estado = this.atributo.indEstado;
                        this.cargandoDatos = false;
                    },
                    //Manejo en caso de error en la petición
                    (error) => { this.manejaError(error) }
                );
            });
    }

    //Método para archivar/remover el atributo actual
    removerAtributo() {
        //Se obtiene el token
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.atributoService.token = token;
                //Llamado al servicio
                this.atributoService.eliminarAtributo(this.id).subscribe(
                    (data) => {
                        if (this.estado == 'P') {
                            this.archivado = true;
                            this.atributo.indEstado = 'A';
                            this.publicado = this.borrador = false;
                            this.msgService.showMessage(this.i18nService.getLabels('general-archivar'));
                        } else {
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                            this.goBack();
                        }
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    //Método para publicar el atributo si este se encuentra en borrador
    publicarAtributo() {
        this.estado = this.atributo.indEstado;
        this.atributo.indEstado = 'P';
        //Se obtiene el token
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.atributoService.token = token;
                //Llamado al servicio
                this.atributoService.editarAtributo(this.atributo).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-publicar'));
                        if (this.estado == 'D') {
                            this.publicado = true;
                            this.archivado = this.borrador = false;
                        }
                        this.mostrarDetalle();
                    },
                    (error) => {
                        //En caso de error, vuelve al estado anterior
                        this.manejaError(error);
                        this.atributo.indEstado = 'D';
                        this.borrador = true;
                        this.archivado = this.publicado = false;
                    }
                );
            });
    }

    //Llamado en caso de que se de un evento para escoger una nueva fecha
    obtenerFecha(event: any) {
        this.nuevaFecha = event;
    }

    establecerNombre() {
        if (this.atributo.nombre != "" && this.atributo.nombre != null) {
            let temp = this.atributo.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.atributo.nombreInterno = temp;
        }
    }

    //Método para actualizar el atributo actual
    actualizarAtributo() {
        if (this.atributo.indTipoDato == 'F') {
            if(this.nuevaFecha != undefined){
                this.atributo.valorDefecto = this.nuevaFecha.getTime() + '';
            }
        }
        //Se obtiene el token
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.atributoService.token = token;
                //Llamado al servicio
                this.atributoService.editarAtributo(this.atributo).subscribe(
                    (data) => {
                        this.mostrarDetalle();
                        this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    //Método que permite regresar a la lista de atributos
    goBack() {
        let link = ['atributosDinamicos'];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}