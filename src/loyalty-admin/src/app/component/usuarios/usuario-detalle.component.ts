import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Usuario } from '../../model/index';
import { UsuarioService, KeycloakService, I18nService } from '../../service/index';
import { MenuItem } from 'primeng/primeng';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { PERM_ESCR_USUARIOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import * as Rutas from '../../utils/rutas';
/**
 * Flecha Roja Technologies
 * Componente encargado de mostrar el detalle del usuario y servir como padre para el resto de componentes de detalle
 * Autor: Fernando Aguilar
 */
@Component({
    moduleId: module.id,
    selector: 'usuario-detalle-component',
    templateUrl: 'usuario-detalle.component.html',
    providers: [UsuarioService, Usuario]
})
export class UsuarioDetalleComponent implements OnInit {
    public items: MenuItem[];
    public activeItem: MenuItem;
    public escritura: boolean; //Permite verificar permisos de escritura (true/false)
    public cargandoDatos: boolean = true; //Carga de datos
    public displayModalConfirm: boolean; //Habilita el cuadro de dialogo para confirmar la suspención
    public activo: boolean; //True = Activo False = Inactivo
    public inactivo: boolean; //True = Inactivo False = Activo

    constructor(
        public activatedRoute: ActivatedRoute,
        public usuario: Usuario,
        public kc: KeycloakService,
        public usuarioService: UsuarioService,
        public msgService: MessagesService,
        public router: Router,
        public authGuard: AuthGuard,
        public i18nservice: I18nService
    ) {
        this.activatedRoute.params.forEach((params: Params) => { this.usuario.idUsuario = params['id'] });

    }
    ngOnInit() {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_USUARIOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.obtenerUsuario();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Obtiene los datos del usuario seleccionado para desplegarlos a la hora de insertar
     * Emplea el dato de id de usuario obtenido a partir de parametros de la ruta
     */
    obtenerUsuario() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.usuarioService.token = tkn
                let sub = this.usuarioService.getUsuario(this.usuario.idUsuario).subscribe(
                    (data: Usuario) => {
                        this.copyValuesOf(this.usuario, data);
                        if (this.usuario.indEstado == 'B') {
                            this.activo = true;
                        } else { this.inactivo = true; }
                        this.cargandoDatos = false;
                    },
                    (error) => {
                        this.manejaError(error);
                        this.cargandoDatos = false;
                    },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**
     * Activa un usuario del sistema
     */
    activarUsuario() {
        this.kc.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.usuarioService.token = token || "";
                this.usuarioService.activarUsuario(this.usuario.idUsuario).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nservice.getLabels("general-activar"));
                        this.activo = true;
                        this.inactivo = false;
                        this.obtenerUsuario();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.activo = false;
                        this.inactivo = true;
                    }
                );
            });
    }

    /**
     * Desactiva un usuario del sistema, los usuarios no pueden ser eliminados debido a razones de auditoría, 
     * los registros de bitacora tienen referencias a los usuarios
     */
    desactivarUsuario() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.usuarioService.token = tkn;
                let sub = this.usuarioService.inactivarUsuario(this.usuario.idUsuario).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nservice.getLabels("general-desactivar"));
                        this.activo = false;
                        this.inactivo = true;
                        this.obtenerUsuario();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.activo = true;
                        this.inactivo = false;
                    },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    /**Encargado del manejo de errores del sistema */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    goBack() {
        //window.history.back();
        this.router.navigate([Rutas.RT_USUARIOS_LISTA_USUARIOS]);
    }
}

