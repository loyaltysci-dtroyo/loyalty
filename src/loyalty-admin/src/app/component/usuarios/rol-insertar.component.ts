import { I18nService } from '../../service';
import {RT_USUARIOS_LISTA_ROLES} from '../../utils/rutas';
import {ErrorHandlerService,MessagesService} from '../../utils/index';
import { Component, Renderer, OnInit,ViewChild,ElementRef}  from '@angular/core';
import { Rol } from '../../model/index';
import { RolService }  from '../../service/index';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import { Router } from '@angular/router';
import {Message } from'primeng/primeng';
import { KeycloakService } from '../../service/index';
import { PERM_ESCR_ROLES} from '../common/auth.constants'
import { AuthGuard } from '../common/index';
@Component({
    moduleId: module.id,
    selector: 'rol-insertar-component',
    templateUrl: 'rol-insertar.component.html',
    providers: [Rol, RolService]
})
/*
* Flecha Roja Technologies
* Autor: Fernando Aguilar
* Componente encargado de insertar roles en el sistema
*/
export class RolInsertarComponent implements OnInit {
  
    public formRoles: FormGroup;  //form group usado para efectos de validación y seteo de valores por default
    public modalmsg: string;//mensaje a mostrar en el cuerpo del modal
    public modalheader: string;//mensaje a mostrar en el header del modal
    public modalAccion: string; //accion a ejecutar luego del confirm
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    constructor(
            public rol: Rol, 
            public kc: KeycloakService,
            public renderer:Renderer,
            public rolService: RolService,
            public authGuard: AuthGuard,
            public router: Router,
            private i18nService:I18nService,
        public msgService:MessagesService) {

        //inicialización del form group
        this.formRoles = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required)
        });
        this.authGuard.canWrite(PERM_ESCR_ROLES).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }

    ngOnInit() {


    }

    /*
    *Metodo encargado de procesar el submit del form de insertar rol
    *Recbe: void
    *Retorna: void
    */
    agregarRol() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.rolService.token = tkn
        let sub = this.rolService.insertRol(this.rol).subscribe(
            (data) => {
                let link = [RT_USUARIOS_LISTA_ROLES];
                this.router.navigate(link);
                this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
            },
            (error) => {
                this.manejaError(error);
            },
            () => sub.unsubscribe()
        );});
    }
    /**Encargado del manejo de errores, despliega un mensaje de error al usuario */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    mostrarModalMensaje(modalHeader: string, modalMessage: string) {
       
    }
    goBack() {
        window.history.back();
    }
}