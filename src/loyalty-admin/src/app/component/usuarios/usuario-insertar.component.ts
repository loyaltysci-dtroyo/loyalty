import { I18nService } from '../../service';
import { Component } from '@angular/core';
import { Usuario } from '../../model/index';
import { UsuarioService, KeycloakService } from '../../service/index';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import * as Routes from '../../utils/rutas';
import { PERM_ESCR_USUARIOS } from '../common/auth.constants'
import { AuthGuard } from '../common/index';
import { AppConfig } from '../../app.config';

@Component({
    moduleId: module.id,
    selector: 'usuario-insertar-component',
    templateUrl: 'usuario-insertar.component.html',
    providers: [UsuarioService]
})
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Componente encargado de la insercion de usuarios en el sistema
 */
export class UsuarioInsertarComponent {
    public usuarioForm: FormGroup;// validaciones y valores por defecto del form
    public nombreValido: boolean;//usado para validar si el nombre publico es unico
    public file_src: any;//src del avatar del usuario
    public file: any;//imagen
    public cargando: boolean = false;//usado para mostrar una animacion al usuario 
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    constructor(
        public usuario: Usuario,
        public usuarioService: UsuarioService,
        public router: Router,
        public authGuard: AuthGuard,
        private i18nService:I18nService,
        public kc: KeycloakService, 
        public msgService: MessagesService
        ) {
        this.usuarioForm = new FormGroup({
            "nombrePublico": new FormControl('', Validators.required),
            "nombreUsuario": new FormControl('', Validators.required),
            "contrasena": new FormControl('', Validators.required),
            "correo": new FormControl('', [Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")])
        });
        this.file_src = `${AppConfig.DEFAULT_IMG_URL}`;;
         this.authGuard.canWrite(PERM_ESCR_USUARIOS).subscribe((result: boolean) => {
                this.tienePermisosEscritura = result;
            });
    }

    /**
     * Metodo encargado de insertar un usuario usando la capa de sevicios
     * Emplea los datos almacenados en el modelo por los controles del form
     */
    insertarUsuario() {
        if (this.nombreValido) {
            this.usuario.nombrePublico = this.usuario.nombrePublico.trim();
            this.usuario.nombreUsuario = this.usuario.nombreUsuario.trim();
            this.usuario.nombreUsuario = this.usuario.nombreUsuario.toLowerCase();
            this.usuario.correo = this.usuario.correo.trim();
            this.usuario.correo = this.usuario.correo.toLowerCase();
            this.cargando = true;
            this.msgService.showMessage(this.i18nService.getLabels("info-uploading"));
            this.kc.getToken().then((tkn:string) => {
                this.usuarioService.token = tkn;
                let sub = this.usuarioService.addUsuario(this.usuario).subscribe(
                    (data) => {
                        this.cargando = false;
                       this.msgService.showMessage(this.i18nService.getLabels("general-insercion"))
                        let link = [Routes.RT_USUARIOS_DETALLE_USUARIO, data];
                        this.router.navigate(link);
                    },
                    (error) => {
                        this.cargando = false;
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); }
                );
            });
        } else {
            this.msgService.showMessage(this.i18nService.getLabels("usuarios-error-username"));
        }
    }

    /*Metodo encargado de informar al usuario cuando ocurre algún tipo de error
    *Recibe:una instancia de error, que puede ser un ProgressEvent o un error de http
    */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /*Metodo encargado de validar si el nombre de usuario ya existe en el sistema, 
    *esto para evitar enviar datos erroneos al api
    */
    validarNombre() {
        if (this.usuario.nombreUsuario == null || this.usuario.nombreUsuario == "") {
            this.nombreValido = false;
            return;
        }
        this.kc.getToken().then((tkn:string) => {
            this.usuarioService.token = tkn;
            let sub = this.usuarioService.validarNombre(this.usuario.nombreUsuario).subscribe(
                (data: string) => {
                    this.nombreValido = (data.trim() == "false");
                },
                (error) => { },
                () => sub.unsubscribe()//evita memory leak
            );
        });
    }


    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {
        this.file = input.files[0];
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        let reader: any
        let target: EventTarget;
        reader = new FileReader(); // Crear un FileReader
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            img.src = event.target.result; // Obtener el (base64 de la imagen)
            this.usuario.avatar = img.src.split(',')[1];
            this.file_src = this.resize(img); // Redimensionar la imagen
        }, false);
        reader.readAsDataURL(input.files[0]);
    }
    /*
     * Metodo encargado de redimensionar una imagen
     * Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     * estan por defecto en 900*900
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");
        let width = img.width;
        let height = img.height;
        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        let dataUrl = canvas.toDataURL('image/jpeg'); // IMPORTANT: 'jpeg' NOT 'jpg'
        return dataUrl
    }
    /**
     *  Método que permite regresar en la navegación
     */
    goBack() {
        //window.history.back();
        let link = ['usuarios'];
        this.router.navigate(link);
    }
}
