import { I18nService } from '../../service';
import { ErrorHandlerService } from '../../utils/index';
import { MessagesService } from '../../utils/index';
import { Component, OnInit } from '@angular/core';
import { Rol, Usuario } from '../../model/index';
import { RolService } from '../../service/index';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import * as Rutas from '../../utils/rutas';
import { Response } from '@angular/http';
import { KeycloakService } from '../../service/index';
import { PERM_ESCR_ROLES, PERM_LECT_USUARIOS } from '../common/auth.constants'
import { AuthGuard } from '../common/index';
@Component({
    moduleId: module.id,
    selector: 'rol-detalle-component',
    templateUrl: 'rol-detalle.component.html',
    providers: [Rol, RolService]
})
/*
* Flecha Roja Technologies
* Autor:Fernando Aguilar 
* Clase encargada de representar el componente encargado de mostrar el detalle del rol seleccionado por el usuario
*/
export class RolDetalleComponent implements OnInit {
    public formRoles: FormGroup;
    public displayUsuariosPorRol: boolean;
    public usuariosPorRol: Usuario[];
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public filaDesplazamiento: number;
    public cantRegistros: number;
    public cantRegistrosTotal: number;
    constructor(
        public route: ActivatedRoute,
        public rol: Rol,
        private i18nService:I18nService,
        public kc: KeycloakService,
        public rolService: RolService,
        public authGuard: AuthGuard,
        public router: Router,
        public msgService: MessagesService) {
        this.route.params.forEach((params: Params) => { this.rol.nombre = params['id'] });
        this.formRoles = new FormGroup({
            'nombre': new FormControl({ value: '', disabled: true }, Validators.required),
            'descripcion': new FormControl('', Validators.required)
        });
        this.usuariosPorRol = [];

        this.authGuard.canWrite(PERM_ESCR_ROLES).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });

    }
    ngOnInit() {

    }

    /**Usado para cargar de manera perezosa los elementos de la lista de usuarios */
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantRegistros = event.rows;
        this.getUsuariosPorRol();
        //this.cargarUsuarios();
    }

    /**
     * Obtiene un rl del api para desplegarlo en el form de edicion
     */
    obtenerRol(nombreRol: string) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.rolService.token = tkn
                let sub = this.rolService.getRolPorNombre(this.rol.nombre).subscribe(
                    (data) => this.rol = data,
                    (error) => {
                        this.manejaError(error);
                    },
                    () => sub.unsubscribe()
                );
            });
    }
    /**
     * Muestra los usuarios por rol y esconde el form de detalle de rol
     */
    showUsuariosPorRol() {
        this.displayUsuariosPorRol = true;
        this.getUsuariosPorRol();
    }
    /**
     * Obtiene la lista de usuarios asociados al rol, utiliza el campo de busqueda para refinar los resultados
     * 
     */
    getUsuariosPorRol() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.rolService.token = tkn
                let sub = this.rolService.getUsuariosPorRol(this.rol.nombre, this.cantRegistros, this.filaDesplazamiento).subscribe(
                    (data: Response) => {
                        this.usuariosPorRol = data.json();
                        if (data.headers.get("Content-Range") != null) {
                            this.cantRegistrosTotal =+ data.headers.get("Content-Range").split("/")[1];
                        }
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe() }
                );
            });
    }

    /*
   *Metodo encargado de modificar los roles
   *Recbe: void
   *Retorna: void*/
    modificarRol() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.rolService.token = tkn
                let sub = this.rolService.editRol(this.rol).subscribe(
                    () => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"))
                        //this.obtenerRol(this.rol.nombre);
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => sub.unsubscribe()
                );
            });
    }
    /**Metodo encargado de navegar hacia le detalle del usuario seleccionado en la lista de usuarios por rol
     * 
     */
    gotoDetailUsuario(usuario: Usuario) {
        let link = [Rutas.RT_USUARIOS_DETALLE_USUARIO, usuario.idUsuario];
        this.router.navigate(link);
    }

    goBack() {
        this.router.navigate([Rutas.RT_ROLES_LISTA]);
        //window.history.back();
    }
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}