import {Component}  from '@angular/core';
import {Usuario} from '../../model/index';
@Component({
    moduleId: module.id,
    selector:'usuarios-component',
    templateUrl:'usuarios.component.html',
    providers:[Usuario]
})
/**
 * Componente padre del arbol de componentes de usuarios
 */
export class UsuariosComponent  {
    constructor(public usuario:Usuario){}
}