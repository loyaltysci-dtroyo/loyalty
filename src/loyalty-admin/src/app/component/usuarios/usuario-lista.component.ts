import { RT_USUARIOS_DETALLE_USUARIO } from '../../utils/rutas';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Component, Renderer, OnInit, ChangeDetectorRef } from '@angular/core';
import { Usuario } from '../../model/index';
import { UsuarioService, KeycloakService } from '../../service/index';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/primeng';
import { PERM_ESCR_USUARIOS } from '../common/auth.constants'
import { AuthGuard } from '../common/index';
@Component({
    moduleId: module.id,
    selector: 'usuario-insertar-component',
    templateUrl: 'usuario-lista.component.html',
    providers: [Usuario, UsuarioService, KeycloakService]
})
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Componente encargado de mostrar el listado de usuarios en el sistema, provee funcionalidades de busquedas 
 */
export class UsuarioListaComponent implements OnInit {
    public usuarioList: Usuario[];//lista de usuarios
    public usuarioEliminar: Usuario; //almacena temporalmente el usuario que sera eliminado
    public terminosBusqueda: string[] = [];
    public preferenciasBusqueda: string[] = ["B"];
    public displayModalConfirm: boolean;
    public items: MenuItem[];//items para el split button, usado para mostrar el form de busqueda avanzada
    public busquedaAvanzada: boolean;
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public rowOffset: number;
    public cantRegistros: number;
    public cantRegistrosTotal: number;
    constructor(
        public usuarioService: UsuarioService,
        public router: Router, public cdRef: ChangeDetectorRef,
        public kc: KeycloakService, public authGuard: AuthGuard,
        public msgService: MessagesService) {
        this.displayModalConfirm = false;
        this.cantRegistros = 10;
        this.rowOffset = 0;
        this.cantRegistrosTotal = 10
        // this.usuarioList=[];
    }

    ngOnInit() {
        this.authGuard.canWrite(PERM_ESCR_USUARIOS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.items = [
            { label: 'Avanzado', icon: 'ui-icon-build', command: () => this.toggleBusquedaAvanzada() }
        ];
        this.buscarUsuarios();
        //this.cargarUsuarios();
    }
    /**
     * Carga los usuarios del sistema
     */
    /*cargarUsuarios() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.usuarioService.token = tkn;
                let sub = this.usuarioService.getListaUsuarios(this.cantRegistros, this.rowOffset).subscribe(
                    (data) => {
                        this.usuarioList = <Usuario[]>data.json();
                        this.cantRegistrosTotal = data.headers.get("Content-Range").split("/")[1];
                        this.cdRef.detectChanges();
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe() }
                );
            });
    }*/

    toggleBusquedaAvanzada() {
        this.busquedaAvanzada = !this.busquedaAvanzada;
    }

    gotoDetail(usuario: Usuario) {
        let link = [RT_USUARIOS_DETALLE_USUARIO, usuario.idUsuario];
        this.router.navigate(link);
    }
    /**Encargado del manejo de errores a nivel del sistema, 
     * despliega un mensaje de error al usuario 
     */

    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /**Usado para cargar de manera perezosa los elementos de la lista de promociones */
    loadData(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantRegistros = event.rows;
        this.buscarUsuarios();
        //this.cargarUsuarios();
    }

    /* Metodo que muestra un modal especial para confirmar la eliminacion de un segmento
    * Retorna:void
    */
    mostrarModalEliminar(usuario: Usuario) {
        this.usuarioEliminar = usuario;
        this.displayModalConfirm = true;
    }
    /**
     * Desactiva un usuario del sistema, los usuarios no pueden ser eliminados debido a razones de auditoría, 
     * los registros de bitacora tienen referencias a los usuarios
     */
    eliminarUsuario() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.usuarioService.token = tkn;

                let sub = this.usuarioService.inactivarUsuario(this.usuarioEliminar.idUsuario).subscribe(
                    () => { this.buscarUsuarios(); },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**Metodo encargaod de buscar los usuarios en el sistema, emplea el termino de busqueda ingresado por el usuario asi como otros parametros para la paginacion, 
     * Parametros:  terminoBusqueda: el termino de busqueda que el usuario introdujo
     *              inactivos,activos: estados del usuario, los infiere a partir de los elementos seleccionados en la GUI
     *              cantRegistros: la cantidad de registros a solicitar por pagina
     *              rowOffset: offset de la pagina actual
     */
    buscarUsuarios() {
        //let itmB: string = this.preferenciasBusqueda.find((itm) => { return itm == "B" });
        //let itmI: string = this.preferenciasBusqueda.find((itm) => { return itm == "I" });
        //let activos: boolean = false, inactivos: boolean = false;
        //if (itmB) activos = true;
        //if (itmI) inactivos = true;
        //console.log(activos, inactivos)
        this.kc.getToken().then(
            (tkn: string) => {
                this.usuarioService.token = tkn;
                let sub = this.usuarioService.busqueda(this.terminosBusqueda, this.preferenciasBusqueda, this.cantRegistros, this.rowOffset).subscribe(
                    (data) => {
                        this.usuarioList = <Usuario[]>data.json();
                        this.cantRegistrosTotal = data.headers.get("Content-Range").split("/")[1];
                        this.cdRef.detectChanges();
                    },
                    (error) => { this.manejaError(error); },
                    () => sub.unsubscribe()
                );
            });
    }
}