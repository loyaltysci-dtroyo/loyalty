import { I18nService } from '../../service';
import * as Rutas from '../../utils/rutas';

import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { KeycloakService, RolService, UsuarioService } from '../../service/index';
import { Rol, Usuario } from '../../model/index';

import { AuthGuard } from '../common/index';
import { PERM_ESCR_USUARIOS } from '../common/auth.constants'
import { Router } from '@angular/router';
import { Response } from '@angular/http';
/**Flecha Roja Technologies
 * Autor:Fernando Aguilar 
 * Componente encargado de mostrar los roles asociados a un usuario, ademas de asociar y revocar roles a un usuario
 */
@Component({
    moduleId: module.id,
    selector: 'usuario-detalle-roles',
    templateUrl: 'usuario-detalle-roles.component.html',
    providers: [RolService, Rol]
})
export class UsuarioDetalleRolesComponent implements OnInit {

    public rolEliminar: Rol; //almacena temporalmente el usuario que sera eliminado
    public displayRoles: boolean;//true= se muestra el modal de asociacion de roles
    public roles: Rol[];//lista de roles para almacenar los roles asociados al usuario
    public displayConfirm: boolean;
    public rutaInsertar;//para el redirect a insertar rol
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public terminosBusqueda: string[] = [];
    public rolesAsociados: Rol[];
    public rowOffset: number;
    public totalRows: number;
    public rowsPerPage: number;

    public rowOffsetRoles: number;
    public rowsPerPageRoles: number;
    public totalRowsRoles:number;

    public rolesVacio: boolean;

    constructor(
        public usuario: Usuario,
        public kc: KeycloakService,
        public usuarioService: UsuarioService,
        public rolService: RolService,
        public authGuard: AuthGuard,
        public router: Router,
        private i18nService: I18nService,
        public msgService: MessagesService
    ) {
        this.rowOffset = 0;
        this.rowOffsetRoles = 0;
        this.rowsPerPageRoles = 10;
        this.rowsPerPage = 10;
        this.displayRoles = false;
        this.rutaInsertar = Rutas.RT_ROLES_INSERTAR;
    }

    ngOnInit() {

        this.authGuard.canWrite(PERM_ESCR_USUARIOS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.cargarUsuario();

    }
    loadData(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.rowsPerPage = event.rows;
        this.cargarRolesAsociados();
    }
    loadDataRoles(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffsetRoles = event.first;
        this.rowsPerPageRoles = event.rows;
        this.cargarRoles();
    }
    /**
     * Obtiene los roles disponibles para asociar al usuario en cuestion 
     */
    cargarRoles() {
        //se carga la lista con los valores que provee la API REST
        this.kc.getToken().then(
            (tkn: string) => {
                this.rolService.token = tkn;
                let sub = this.rolService.getRolesDisponiblesPorUsuario(this.usuario.idUsuario, this.terminosBusqueda, this.rowOffsetRoles, this.rowsPerPageRoles).subscribe(
                    (response: Response) => {
                        this.roles = response.json();
                        this.totalRowsRoles = + response.headers.get("Content-Range").split("/")[1];
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); }
                );
            }
        );
    }
    busquedaRolesAsociados() {
        this.rolesAsociados = this.usuario.roles.filter((element) => {
            let valid = true;
            this.terminosBusqueda.forEach(terminoBusqueda => {
                valid = element.nombre.includes(terminoBusqueda.trim());
            });
            return valid;
        });
    }
    mostrarModalRol() {
        this.cargarRoles();
        this.displayRoles = true;
    }
    /**
     * Obtiene los datos del usuario
     */
    cargarUsuario() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.usuarioService.token = tkn
                let sub = this.usuarioService.getUsuario(this.usuario.idUsuario).subscribe(
                    (data) => {
                        this.usuario = data;
                     this.cargarRolesAsociados();
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Despliega el modal para eliminar un rol
     */
    mostrarModalEliminarRol(rol: Rol) {
        this.rolEliminar = rol;
        this.displayConfirm = true;
    }
    /**
     * Obtiene los roles asociados al usuario en cuestion
     */
    cargarRolesAsociados() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.usuarioService.token = tkn
                let sub = this.usuarioService.getListaRolesAsociados(this.usuario, this.rowOffset, this.rowsPerPage).subscribe(
                    (response: Response) => {
                        this.usuario.roles = response.json();
                        if (response.headers.has("Content-Range")) {
                            this.totalRows = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if(this.usuario.roles.length > 0){
                            this.rolesVacio = false;
                        }else{
                            this.rolesVacio = true;
                        }
                        this.busquedaRolesAsociados();
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Maneja el error a nivel de este componente, despliega un mensaje al usuario para informarlo del error
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    /**
     * Metodo usado para confirmar la acion de eliminar un rol, es accionado por el dialog de confimacion de eliminar 
     * Elimina el rol seleccionado (almacenado en la variable rolEliminar) del sistema
     */
    confirmEliminarRol() {
        this.removerRol(this.rolEliminar);
    }
    /**
     * Encargado de actualizar un usuario en el sistema,
     * utiliza la variable global usuario, en la cual se almacenaron los datos de previo
     */
    actualizarUsuario() {
        this.kc.getToken().then((tkn: string) => {
            this.usuarioService.token = tkn
            let sub = this.usuarioService.updateUsuario(this.usuario).subscribe(
                () => {
                    this.cargarUsuario();
                },
                (error) => {
                    this.manejaError(error);
                },
                () => { sub.unsubscribe(); }
            );
        });
    }

    /**
    * Asocia un rol a un usuario, es invocado desde el dialog de selección de rol
    */
    agregarRol(rol: Rol) {
        this.kc.getToken().then((tkn: string) => {
            this.usuarioService.token = tkn
            let sub = this.usuarioService.agregarRol(this.usuario, rol).subscribe(
                () => {
                    this.cargarRoles();
                    this.cargarRolesAsociados();
                    this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                    this.displayConfirm = false;
                },
                (error) => {
                    this.manejaError(error);
                },
                () => { sub.unsubscribe(); }
            );
        });
    }
    /**
    * Revoca un rol a un usuario, es invocado desde el dialog de selección de rol
    */
    removerRol(rol: Rol) {
        this.kc.getToken().then((tkn: string) => {
            this.usuarioService.token = tkn
            let sub = this.usuarioService.removerRol(this.usuario, rol).subscribe(
                () => {
                    this.cargarRoles();
                    this.cargarRolesAsociados();
                    this.displayConfirm = false;
                    this.msgService.showMessage(this.i18nService.getLabels("general-exclusion-simple"));
                },
                (error) => {
                    this.manejaError(error);
                    this.displayConfirm = false;
                },
                () => { sub.unsubscribe(); }
            );
        });

    }
    /**
     * Navega hacia el detalle del rol que recibe como parametros
     */
    gotoDetalleRol(rol: Rol) {
        // console.log(rol);
        this.router.navigate([Rutas.RT_ROLES_DETALLE, rol.nombre]);
    }

}