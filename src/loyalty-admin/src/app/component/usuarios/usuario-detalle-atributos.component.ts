import { I18nService } from '../../service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../model/index';
import { UsuarioService, KeycloakService } from '../../service/index';
import { Subscription } from 'rxjs/Rx';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Message } from 'primeng/primeng';
import { PERM_ESCR_USUARIOS } from '../common/auth.constants'
import { AuthGuard } from '../common/index';
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Componente encargado de mostrar el detalle de atributos del usuario
 * 
 */
@Component({
    moduleId: module.id,
    selector: 'detalle-usuario-atributos',
    templateUrl: 'usuario-detalle-atributos.component.html'
})
export class UsuarioDetalleAtributosComponent implements OnInit {
    public usuarioForm: FormGroup;//formgroup usado para validaciones y valores por defecto de los componentes del form
    public nombreValido: boolean;
    public file_src: any;
    public file: any;
    public usuarioActivo: boolean;
    public uploading:boolean;
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema

    constructor(
        public usuario: Usuario,
        public usuarioService: UsuarioService,
        public router: Router,
        public kc: KeycloakService,
        private i18nService:I18nService,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public msgService: MessagesService
    ) {
        this.usuarioForm = new FormGroup({
            "nombrePublico": new FormControl('', Validators.required),
            "nombreUsuario": new FormControl({ value: '', disabled: true }, Validators.required),
            "correo": new FormControl('', Validators.required),
            "contrasena": new FormControl('')
        });
        this.uploading=false;
         this.authGuard.canWrite(PERM_ESCR_USUARIOS).subscribe((result: boolean) => {
                this.tienePermisosEscritura = result;
            });
    }

    ngOnInit() {
        this.obtenerUsuario();

    }
    /**
     * Obtiene los datos del usuario seleccionado para desplegarlos a la hora de insertar
     * Emplea el dato de id de usuario obtenido a partir de parametros de la ruta
     */
    obtenerUsuario() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.usuarioService.token = tkn
        let sub = this.usuarioService.getUsuario(this.usuario.idUsuario).subscribe(
            (data: Usuario) => {
                // console.log(this.usuario.idUsuario)
                this.usuario = data;
                this.usuarioActivo = !(this.usuario.indEstado == "I")
                this.file_src=this.usuario.avatar;
            },
            (error) => {
                this.manejaError(error);
            },
            () => { sub.unsubscribe(); }
        );});
    }
    /**
     * Modifica un usuario en el sitema, emplea los datos almacenados en la variable global usuario para realizar la actualizacion
     * 
     */
    modificarUsuario() {
        this.uploading = true;
        this.kc.getToken().then((tkn:string) => {
            this.usuarioService.token = tkn;
            let sub = this.usuarioService.updateUsuario(this.usuario).subscribe(
                () => {
                    this.uploading = false;
                   this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"))
                    this.obtenerUsuario();
                },
                (error) => {
                    this.manejaError(error);
                },
                () => { sub.unsubscribe(); }
            );
        });
    }
    /**Encargado del manejo de errores del sistema */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /**Informa al usuario sobre algun acontecimiento */
    mostrarModalMensaje(modalHeader: string, modalMessage: string) {
        this.msgService.showMessage({ severity: 'info', summary: modalHeader, detail: modalMessage });
    }
    toggleEstado() {
        this.usuario.indEstado = this.usuarioActivo ? "B" : "I";
    }
    siguiente() {
        this.modificarUsuario();
    }

    validarNombre(){
        //TODO
    }

    /**Llamado cuando el usuario cambia un archivo en el file upload */
    fileChange(input) {
        // se itera entre los archivos
        for (var i = 0; i < input.files.length; i++) {
            this.file = input.files[i];

            var img = document.createElement("img"); // Create an img element and add the image file data to it
            img.src = window.URL.createObjectURL(input.files[i]);

            let reader: any
            let target: EventTarget;

            reader = new FileReader();// Crear un FileReader

            reader.addEventListener("load", (event) => {  // Agregar un event listener para el cambio

                this.usuario.avatar = event.target.result.split(",")[1];
                // Obtener el (base64 de la imagen)
                img.src = event.target.result;
                // Redimensionar la imagen
                this.file_src = this.resize(img);
            }, false);

            reader.readAsDataURL(input.files[i]);

        }

    }
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");

        // console.log("Size Before: " + img.src.length + " bytes");

        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        let dataUrl = canvas.toDataURL('image/jpeg');// IMPORTANT: 'jpeg' NOT 'jpg'
        // console.log("Size After:  " + dataUrl.length + " bytes");
        return dataUrl
    }
}