import { I18nService } from '../../service';
import * as Rutas from '../../utils/rutas';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Component, ViewChild, ElementRef, Renderer, OnInit } from '@angular/core';
import { RolService, KeycloakService } from '../../service/index';
import { Rol } from '../../model/index'
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Message } from 'primeng/primeng';
import { Response } from '@angular/http'
import { PERM_ESCR_ROLES } from '../common/auth.constants'
import { AuthGuard } from '../common/index';
/*
* Flecha Roja Technologies
* Autor:Fernando Aguilar
* Componente encargado de representar el componente encargado de listar todos los roles
*/
@Component({
    moduleId: module.id,
    selector: 'rol-lista-component',
    templateUrl: 'rol-lista.component.html',
    providers: [Rol, RolService]
})
export class RolListaComponent implements OnInit{

    public roles: Rol[];  //array con la lista de roles correspondientes a la pagina actual de la vista
    public rolEliminar: Rol; //temporal para el rol a eliminar
    public displayConfirm: boolean;// true=muestra dalog de confirm para eliminar un rol
    public busqueda: string="";// string de busqueda
    public rutaInsertar = Rutas.RT_ROLES_INSERTAR;//Ruta dinamica para redirect a insertar
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public filaDesplazamiento: number=0;
    public cantRegistros: number=10;
    public cantRegistrosTotal: number;
    public vacio: boolean = false;

    constructor(
        public rol: Rol,
        public rolService: RolService,
        public router: Router,
        public renderer: Renderer,
        private i18nService:I18nService,
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        public msgService: MessagesService) {
        this.roles = [];
        this.authGuard.canWrite(PERM_ESCR_ROLES).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });

    }

    ngOnInit() {
        //this.cargarListaRoles();
        this.buscarRoles();
    }
    /**
     * Carga la lista de roles del sistema 
     */
    cargarListaRoles(){
        //se carga la lista con los valores que provee la API REST
        this.kc.getToken().catch((error) => this.manejaError(error)).then(
            (tkn:string) => {
                this.rolService.token = tkn;
                let sub = this.rolService.getListaRoles().subscribe(
                    (data) => {
                        this.roles = data
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => sub.unsubscribe()
                );
            }
        );
    }

    /**Usado para cargar de manera perezosa los elementos de la lista de promociones */
    loadData(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.filaDesplazamiento = event.first;
        this.cantRegistros = event.rows;
        this.buscarRoles();
        //this.cargarUsuarios();
    }
    /**
     * Metodo encargado de buscar entre los roles presentes en el api 
     */
    buscarRoles() {
        this.kc.getToken().catch((error) => this.manejaError(error)).then(
            (tkn:string) => {
                this.rolService.token = tkn
                let sub = this.rolService.busquedaRoles(this.busqueda, this.filaDesplazamiento, this.cantRegistros).subscribe(
                    (data: Response) => {
                        this.roles = data.json();
                        if (data.headers.get("Content-Range") != null) {
                            this.cantRegistrosTotal =+ data.headers.get("Content-Range").split("/")[1];
                        }
                        if(this.roles.length > 0){
                            this.vacio = false;
                        }else{
                            this.vacio = true;
                        }
                    }, (error) => {
                        this.manejaError(error);
                    },
                    () => {
                        sub.unsubscribe();
                    });
            });
    }

    /*
     * Metodo encargado de informar al usuario cuando ocurre algún tipo de error
     * Recibe:una instancia de error, que puede ser un ProgressEvent o un error de http
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    /* Metodo que muestra un modal especial para confirmar la eliminacion de un segmento
     * Retorna:void
     */
    mostrarModalEliminarRol(rol: Rol) {
        this.rolEliminar = rol;
        this.displayConfirm = true;
    }
    /**
      * Encargado de eliminar un rol del sistema
      * 
      */
    confirmEliminarRol() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.rolService.token = tkn
                let sub = this.rolService.deleteRol(this.rolEliminar).subscribe(
                    () => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                        this.displayConfirm=false;
                        this.cargarListaRoles();
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); });
            });
    }
    /**
     * Encargado de navegar hacia el detalle del rol 
     * Recibe: el rol hacia el cual debe navegar
     */
    gotoDetalle(rol: Rol) {
        let link = [Rutas.RT_ROLES_DETALLE, rol.nombre];
        this.router.navigate(link);
    }
}
