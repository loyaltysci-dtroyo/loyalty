import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Grupo } from '../../model/index';
import { GrupoService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_GRUPOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { AppConfig } from '../../app.config';

@Component({
    moduleId: module.id,
    selector: 'grupo-insertar-component',
    templateUrl: 'grupo-insertar.component.html',
    providers: [Grupo, GrupoService]
})

/**
 * Jaylin Centeno
 * Componente hijo de Grupo de miembros, permite insertar grupos
 */
export class GrupoInsertarComponent implements OnInit {

    public formGrupo: FormGroup; //form del grupo
    public imagen: any;
    public avatar: any;
    public subiendoImagen: boolean = false;
    public escritura: boolean; //Permite verificar permisos (true/false)

    constructor(
        public grupo: Grupo,
        public grupoService: GrupoService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        this.formGrupo = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required)
        });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_GRUPOS).subscribe(
            (permiso: boolean) => {
                this.escritura = permiso;
            });
    }

    //Se llama una sola vez
    ngOnInit() {
        //Imagen predeterminada
        this.avatar = `${AppConfig.DEFAULT_IMG_URL}`;
    }

    //Método para subir una imagen que representa el avatar del grupo
    subirImagen() {
        //Se obtiene el archivo subido
        this.imagen = document.getElementById("avatar");
        //se verifica que el usuario subiera un archivo
        if (this.imagen.files.length == 0) {
            this.msgService.showMessage(this.i18nService.getLabels('warn-misiones-juego-imagen'));
            return;
        }
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.avatar = reader.result;
            let urlBase64 = this.avatar.split(",");
            this.grupo.avatar = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    //Método para insertar un grupo
    insertarGrupo() {
        if (this.escritura) {
            this.grupo.indVisible = "I";
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.grupoService.token = token;
                    if (this.grupo.avatar != null) {
                        this.subiendoImagen = true;
                    }
                    this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
                    let sub = this.grupoService.insertarGrupo(this.grupo).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-insercion'));
                            let link = ['grupos/detalleGrupo', data];
                            this.router.navigate(link);
                        },
                        (error) => { this.manejarError(error) },
                        () => { sub.unsubscribe(); this.subiendoImagen = false; }
                    )
                }
            )
        }
    }

    //Método para regresar a la pantalla anterior
    goBack() {
        let link = ['grupos'];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}