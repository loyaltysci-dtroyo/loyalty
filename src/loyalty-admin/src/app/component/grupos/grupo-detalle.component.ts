import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Grupo } from '../../model/index';
import { GrupoMiembroService, GrupoService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_GRUPOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { AppConfig } from '../../app.config';

@Component({
    moduleId: module.id,
    selector: 'grupo-detalle-component',
    templateUrl: 'grupo-detalle.component.html',
    providers: [GrupoMiembroService, GrupoService]
})

/**
 * Jaylin Centeno
 * Componente hijo de Grupo de miembros,permite ver el detalle del grupo
 */
export class GrupoDetalleComponent implements OnInit {

    public escritura: boolean; //Permite verificar los permisos de escritura (true/false)
    public subiendoImagen: boolean = false; //Indica si existe una carga de imagen
    public cargandoDatos: boolean = true;
    public eliminar: boolean; //Habilita el cuadro de dialogo para remover

    public formGrupo: FormGroup;
    //Manejo de imagen
    public imagen: any;
    public avatar: any;
    public imagenCambio: boolean = false;

    constructor(

        public grupoMiembroService: GrupoMiembroService,
        public grupo: Grupo,
        public grupoService: GrupoService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        //Validación del forms
        this.formGrupo = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required)
        });

        //Optiene la imagen predetereminda del sistema
        this.avatar = `${AppConfig.DEFAULT_IMG_URL}`;

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_GRUPOS).subscribe(
            (permiso: boolean) => {
                this.escritura = permiso;
            });
    }

    //Inicia
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    //Método que llama al servicio para obtener el detalle del grupo
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.grupoService.token = token;
                this.grupoService.obtenerGrupoPorId(this.grupo.idGrupo).subscribe(
                    (data) => {
                        this.copyValuesOf(this.grupo, data);
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Método para subir una imagen que representa el avatar del grupo
    subirImagen() {
        this.imagen = document.getElementById("avatar");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.avatar = reader.result;
            let urlBase64 = this.avatar.split(",");
            this.grupo.avatar = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
        this.imagenCambio = true;
    }

    //Método para remover el grupo actual
    removerGrupo() {
        if (this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.grupoService.token = token;
                    this.grupoService.removerGrupo(this.grupo.idGrupo).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                            this.goBack();
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
        }

    }

    //Metodo para actualizar el grupo actual
    actualizarGrupo() {
        if (this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.grupoService.token = token;
                    this.subiendoImagen = true;
                    this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
                    let sub = this.grupoService.actualizarGrupo(this.grupo).subscribe(
                        (data) => {
                            this.mostrarDetalle();
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                        },
                        (error) => this.manejarError(error),
                        () => { sub.unsubscribe(); this.subiendoImagen = false; }
                    );
                }
            );
        }
    }

    // Metodo para volver a la página anterior
    goBack() {
        let link = ['/grupos/'];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}