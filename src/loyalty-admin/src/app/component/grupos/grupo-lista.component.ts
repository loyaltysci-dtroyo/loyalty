import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Grupo } from '../../model/index';
import { GrupoService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_GRUPOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { Response } from '@angular/http';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { AppConfig } from '../../app.config';

@Component({
    moduleId: module.id,
    selector: 'grupo-lista-component',
    templateUrl: 'grupo-lista.component.html',
    providers: [Grupo, GrupoService]
})
/**
 * Jaylin Centeno
 * Componente hijo de Grupo de miembros, permite ver la lista de grupos
 */
export class GrupoListaComponent implements OnInit {

    public avatar: any;
    public escritura: boolean;//Permite verificar permisos de escritura (true/false)
    public avanzada: boolean = false; //Para mostrar la búsqueda avanzada
    public eliminar: boolean;
    public cargandoDatos: boolean = true;
    public idGrupo: string; //Id del grupo seleccionado
    public listaVacia: boolean; //Permite verificar si la lista de grupos esta vacia
    public listaGrupos: Grupo[]; //Lista con los grupos disponibles 
    public totalRecords: number; //Total de miembros en el sistema
    public cantidad: number = 10; //Cantidad de rows
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public grupoVisible: boolean = true;
    public grupoInvisible: boolean = true;
    public visibilidad: string[] = ['V', 'I']; //Manejo de estado/visibilidad
    public busqueda: string = ""; //Palabras clave de búsqueda
    public listaBusqueda: string[] = [];

    constructor(
        public grupo: Grupo,
        public grupoService: GrupoService,
        public router: Router,
        public keycloakService: KeycloakService,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_GRUPOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    ngOnInit() {
        this.avatar = `${AppConfig.DEFAULT_IMG_URL}`;
        this.busquedaGrupos();
    }

    //Método para mostrar el dialogo de confirmación
    confirmar(idGrupo: string) {
        this.eliminar = true;
        this.idGrupo = idGrupo;
    }

    //Método que redirecciona a la página de insertar un grupo
    nuevoGrupo() {
        let link = ['grupos/insertar'];
        this.router.navigate(link);
    }

    //Método para mostrar la búsquda avanzada
    busquedaAvanzada() {
        this.avanzada = !this.avanzada;
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaGrupos();
    }

    //Redirecciona al grupo seleccionado
    grupoSeleccionado(grupoId: string) {
        this.idGrupo = grupoId;
        let link = ['grupos/detalleGrupo', grupoId];
        this.router.navigate(link);
    }

    //Metodo encargado de realizar la búsqueda de grupos por palabras claves
    busquedaGrupos() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.grupoService.token = token;
                this.grupoService.buscarGrupo(this.cantidad, this.filaDesplazamiento, this.busqueda, this.visibilidad).subscribe(
                    (response: Response) => {
                        this.listaGrupos = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaGrupos.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    //Metodo que se encarga remover un grupo
    removerGrupo() {
        if (this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.grupoService.token = token;
                    this.grupoService.removerGrupo(this.idGrupo).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                            this.busquedaGrupos();
                        },
                        (error) => {
                            this.manejaError(error)
                        }
                    );
                }
            );
        }
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}