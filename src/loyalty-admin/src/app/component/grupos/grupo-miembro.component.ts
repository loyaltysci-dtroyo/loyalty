import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Grupo, Miembro, GrupoMiembro } from '../../model/index';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { GrupoService, MiembroService, GrupoMiembroService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_GRUPOS, PERM_ESCR_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'grupo-miembro-component',
    templateUrl: 'grupo-miembro.component.html',
    providers: [GrupoService, Miembro, MiembroService, GrupoMiembro, GrupoMiembroService]
})

/*Esta clase representa al componente para ver, remover y asociar miembros*/
export class GrupoMiembroComponent implements OnInit {

    public escritura: boolean; //Permite verificar los permisos de escritura (true/false)
    public escrituraMiembro: boolean;
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public cargandoGrupos: boolean = true; //Utilizado para mostrar la carga de datos
    public eliminar: boolean; //Habilita el cuadro de dialogo para remover
    public accion: string; //Permite saber sobre que elemento se ejecuta la acción
    public estado: string = "A"; //Estado de los clientes

    // ------  Miembros del grupo  ------ //
    public listaMiembroGrupo: Miembro[] = []; //Lista de los miembros del grupo
    public listaVacia: boolean; //Para verificar existen miembros asociados al grupo
    public totalRecords: number; //Total de miembros en el grupo
    public cantidad: number = 10; //Cantidad de filas
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    // ------  Miembros en el sistema  ------ //
    public listaMiembros: Miembro[] = []; //Lista con los miembros del sistema (no estan en el grupo)
    public listaTotalVacia: boolean = true; //Para verificar si existen miembros en la aplicacion
    public totalRecordsMiembro: number;
    public cantidadMiembro: number = 10; //Cantidad de filas
    public filaDesplazMiembro: number = 0; //Desplazamiento de la primera fila

    // ------ Miembros para agregar/remover del grupo ------ //
    public lista: string[] = []; //Lista con los id de los miembros seleccionados
    public listaSeleccion: Miembro[] = []; //Seleccionados
    public listaRemover: Miembro[] = []; // Remover
    public seleccion: string; //Representa el id del miembro seleccionado

    public busqueda: string; //Palabras claves de búsqueda
    public listaBusqueda: string[] = []; //Palabras de búsqueda, ChipModule;

    public totalMiembros: number; //Representa la cantidad total de los miembros del grupo    

    constructor(
        public grupo: Grupo,
        public grupoService: GrupoService,
        public miembroService: MiembroService,
        public grupoMiembroService: GrupoMiembroService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        ;
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_GRUPOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escrituraMiembro = permiso;
        });
    }

    ngOnInit() {
        this.mostrarDetalle();
        this.obtenerMiembrosNoGrupo();
        this.obtenerMiembrosGrupo();
    }

    loadMiembroGrupo(event: any) {
        this.filaDesplazMiembro = event.first;
        this.cantidadMiembro = event.rows;
        this.obtenerMiembrosGrupo();
    }

    loadMiembroNoGrupo(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerMiembrosNoGrupo();
    }

    //Método para guardar el valor del miembro seleccionado
    miembroSeleccionado(elemento: string, idMiembro?: string) {
        this.accion = elemento;
        if (elemento == "miembro") {
            this.seleccion = idMiembro;
        }
        this.eliminar = true;
    }

    //metodo para mostrar el detalle del grupo
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.grupoService.token = token;
                this.grupoService.obtenerGrupoPorId(this.grupo.idGrupo).subscribe(
                    (data) => {
                        this.grupo = data;
                        this.obtenerMiembrosNoGrupo();
                        this.obtenerMiembrosGrupo();
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Método para obtener los miembros que pertenecen al grupo seleccionado
    obtenerMiembrosGrupo() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.grupoMiembroService.token = token;
                this.grupoMiembroService.obtenerMiembrosGrupo(this.grupo.idGrupo, this.cantidad, this.filaDesplazamiento, this.estado, this.busqueda, "A").subscribe(
                    (response: Response) => {
                        this.listaMiembroGrupo = response.json();
                        //Se recibe el total de las categorías disponibles
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecordsMiembro = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaMiembroGrupo.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                        this.cargandoGrupos = false;
                    },
                    (error) => { this.manejarError(error); this.cargandoGrupos = false; }
                )
            }
        );

    }

    //Metodo que se encarga de traer los clientes que no forman parte del grupo
    obtenerMiembrosNoGrupo() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.grupoMiembroService.token = token;
                this.grupoMiembroService.obtenerMiembrosGrupo(this.grupo.idGrupo, this.cantidadMiembro, this.filaDesplazMiembro, this.estado, this.busqueda, "D").subscribe(
                    (response: Response) => {
                        this.listaMiembros = response.json();
                        //Se recibe el total de las categorías disponibles
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaMiembros.length > 0) {
                            this.listaTotalVacia = false;
                        } else {
                            this.listaTotalVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => { this.manejarError(error); this.cargandoGrupos = false; }
                );
            });
    }

    //Metodo para crear la lista con los id de los clientes
    //que se agregaran al grupo
    crearListaPrueba() {
        //Lista selección es la lista generada cuando un cliente se marca
        if (this.listaSeleccion.length > 0) {
            this.listaSeleccion.forEach(miembro => {
                this.lista = [...this.lista, miembro.idMiembro]
            })
        }
    }

    //Metodo para crear la lista con los id de los clientes a remover
    crearListaRemover() {
        //Lista selección es la lista generada cuando un cliente se marca
        if (this.listaRemover.length > 0) {
            this.listaRemover.forEach(miembro => {
                this.lista = [...this.lista, miembro.idMiembro]
            })
        }
    }

    //Metodo para agregar un miembro al grupo
    agregarMiembroGrupo(idMiembro: string) {
        if (this.escritura && this.escrituraMiembro) {
            this.seleccion = idMiembro;
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.grupoMiembroService.token = token;
                    this.grupoMiembroService.agregarMiembro(idMiembro, this.grupo.idGrupo).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                            //llamado al metodo para agregar un miembro
                            this.meterMiembroLista("miembro");
                            if (this.listaSeleccion.length > 0) {
                                let index = this.listaSeleccion.findIndex(element => element.idMiembro == this.seleccion);
                                this.listaSeleccion.splice(index, 1);
                            }
                            this.obtenerMiembrosNoGrupo();
                            this.obtenerMiembrosGrupo()
                        },
                        (error) => this.manejarError(error)
                    );
                });
        }
    }

    //Método para asociar una lista de clientes al grupo 
    agregarListaMiembro() {
        if (this.escritura && this.escrituraMiembro) {
            //Se pregunta si se selecciono algun cliente
            if (this.listaSeleccion.length >= 1) {
                this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                    (token: string) => {
                        this.grupoMiembroService.token = token;
                        this.crearListaPrueba();
                        this.grupoMiembroService.agregarListaMiembro(this.lista, this.grupo.idGrupo).subscribe(
                            (data) => {
                                this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-multiple'));
                                this.meterMiembroLista("lista");
                                this.lista = [];
                                this.listaSeleccion = [];
                                this.obtenerMiembrosNoGrupo();
                                this.obtenerMiembrosGrupo()
                            },
                            (error) => this.manejarError(error)
                        );
                    }
                );
            } else {
                this.msgService.showMessage(this.i18nService.getLabels('warn-cantidad'));
            }
        }
    }

    //Método para remover a un miembro del grupo
    removerMiembro() {
        if (this.escritura && this.escrituraMiembro) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.grupoMiembroService.token = token;
                    this.grupoMiembroService.removerMiembro(this.seleccion, this.grupo.idGrupo).subscribe(
                        (data) => {
                            this.sacarMiembroListaGrupo("miembro");
                            this.msgService.showMessage(this.i18nService.getLabels('general-exclusion-simple"'));
                            if (this.listaRemover.length > 0) {
                                let index = this.listaRemover.findIndex(element => element.idMiembro == this.seleccion);
                                this.listaRemover.splice(index, 1);
                            }
                            this.obtenerMiembrosNoGrupo();
                            this.obtenerMiembrosGrupo()
                            this.eliminar = false;
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
        }
    }

    //Método para asociar una lista de clientes al grupo 
    removerListaMiembro() {
        if (this.escritura && this.escrituraMiembro) {
            //Se pregunta si se selecciono algun cliente
            if (this.listaRemover.length > 1) {
                this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                    (token: string) => {
                        this.grupoMiembroService.token = token;
                        this.crearListaRemover();
                        this.grupoMiembroService.removerListaMiembro(this.lista, this.grupo.idGrupo).subscribe(
                            (data) => {
                                this.msgService.showMessage(this.i18nService.getLabels('general-exclusion-multiple"'));
                                this.sacarMiembroListaGrupo("lista");
                                this.lista = [];
                                this.listaRemover = [];
                                this.eliminar = false;
                                this.obtenerMiembrosNoGrupo();
                                this.obtenerMiembrosGrupo()
                            },
                            (error) => this.manejarError(error)
                        );
                    }
                );
            } else {
                this.msgService.showMessage({ detail: "Debe seleccionar al menos 2 miembros", severity: "warn", summary: "" });
            }
        }
    }

    //Método para remover el row de miembro de la lista de los que no pertenecen al grupo
    meterMiembroLista(tipo: string) {
        let listaTemporal = this.listaMiembros;
        let posicion;
        if (this.listaMiembros.length >= 0) {
            if (tipo == "lista") {
                for (let x in listaTemporal) {
                    for (let y in this.lista) {
                        if (listaTemporal[x].idMiembro == this.lista[y]) {
                            posicion = parseInt(x);
                            this.listaMiembros.splice(posicion, 1);
                        }
                    }
                }
            } else {
                for (let x in listaTemporal) {
                    if (listaTemporal[x].idMiembro == this.seleccion) {
                        posicion = parseInt(x);
                        this.listaMiembros.splice(posicion, 1);
                        break;
                    }
                }
            }
            if (tipo == "lista") {
                this.totalMiembros += this.lista.length;
            } else {
                this.totalMiembros++;
            }
            this.obtenerMiembrosGrupo();
        }
    }

    //Método para sacar los miembros seleccionados de la lista de los que si pertenecen al grupo
    sacarMiembroListaGrupo(tipo: string) {
        let listaTemporal = this.listaMiembroGrupo;
        let posicion;
        if (this.listaMiembroGrupo.length >= 0) {
            if (tipo == "lista") {
                for (let x in listaTemporal) {
                    for (let y in this.lista) {
                        if (listaTemporal[x].idMiembro == this.lista[y]) {
                            posicion = parseInt(x);
                            this.listaMiembroGrupo.splice(posicion, 1);
                        }
                    }
                }
            } else {
                for (let x in listaTemporal) {
                    if (listaTemporal[x].idMiembro == this.seleccion) {
                        posicion = parseInt(x);
                        this.listaMiembroGrupo.splice(posicion, 1);
                        break;
                    }
                }
            }
        }
        if (tipo == "lista") {
            this.totalMiembros -= this.lista.length;
        } else {
            this.totalMiembros--;
        }
        this.obtenerMiembrosNoGrupo();
    }

    // Redirecciona al detalle del grupo
    goBack() {
        let link = ['grupos/detalleGrupo', this.grupo.idGrupo];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}