export { GrupoComponent } from './grupo.component';
export { GrupoInsertarComponent } from './grupo-insertar.component';
export { GrupoListaComponent } from './grupo-lista.component';
export { GrupoDetalleComponent } from './grupo-detalle.component';
export { GrupoMiembroComponent } from './grupo-miembro.component';
export { DetalleComponent } from './detalle.component';