import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Grupo } from '../../model/index';
import { GrupoMiembroService, GrupoService, KeycloakService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'detalle-component',
    templateUrl: 'detalle.component.html',
    providers: [GrupoMiembroService, Grupo, GrupoService]
})

/**
 * Jaylin Centeno
 * Componente hijo, permite manejar al grupo
 */ 
export class DetalleComponent {

    public puntaje: number = 0; //Puntaje del grupo
    public cargandoDatos: boolean = true; //Utilizado para mostrar el gif de carga de datos
    public general = Rutas.RT_GRUPOS_DETALLE_GENERAL; //Rutas
    public miembros = Rutas.RT_GRUPOS_DETALLE_MIEMBROS; //Rutas
    public cantidad:any;
    constructor(
        public grupoMiembroService: GrupoMiembroService,
        public grupo: Grupo,
        public grupoService: GrupoService,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService,
        public keycloakService: KeycloakService
    ) {
        //Representa el id del grupo obtenido de la url 
        this.route.params.forEach((params: Params) => { this.grupo.idGrupo = params['id'] });
    }

    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    //Método que llama al servicio para obtener el detalle del grupo
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.grupoService.token = token;
                this.grupoService.obtenerGrupoPorId(this.grupo.idGrupo).subscribe(
                    (data) => {
                        this.copyValuesOf(this.grupo, data);
                            this.obtenerCantidad();
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Método para obtener la cantidad de miembros de un grupo
    obtenerCantidad() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.grupoMiembroService.token = token;
                this.grupoMiembroService.cantidadMiembros(this.grupo.idGrupo).subscribe(
                    (data) => this.cantidad = data,
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    // Metodo para volver a la página anterior
    goBack() {
        let link = ['/grupos/'];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
