import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Miembro } from '../../model/index';
import { MiembroService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { AppConfig } from '../../app.config';

@Component({
    moduleId: module.id,
    selector: 'miembro-insertar-component',
    templateUrl: 'miembro-insertar.component.html',
    providers: [Miembro, MiembroService]
})
/**
 * Jaylin Centeno
 * Componente utilizado para la inserción de miembros
 */
export class MiembroInsertarComponent implements OnInit {

    public formMiembro: FormGroup; // Form group para validar el formulario de miembro
    public imagen: any; //Contendra el archivo subido por el usuario
    public avatar: any; //Guarda la dirección de la imagen
    public urlBase64: string[]; //Se guarda el array de la imagen
    public escritura: boolean; //Permite verificar permisos (true/false)
    public subiendoImagen: boolean = false; //Indica si existe una carga de imagen
    public nombreValido: boolean; //Validación del nombre usuario
    public correoValido: boolean; //Validación del correo

    constructor(
        public miembro: Miembro,
        public miembroService: MiembroService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Validacion del formulario y datos por defecto
        this.formMiembro = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'apellido': new FormControl('', Validators.required),
            'apellido2': new FormControl(''),
            'identificacion': new FormControl('', Validators.required),
            'correo': new FormControl('', Validators.required),
            'contrasena': new FormControl('', Validators.required),
            'nombreUsuario': new FormControl('', Validators.required)
        });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }
    ngOnInit() {
        this.avatar = `${AppConfig.DEFAULT_IMG_URL}`;
    }

    /**
     * Guarda la imagen subida por el usuario
     */
    subirImagen() {
        this.imagen = document.getElementById("avatar");
        let reader = new FileReader();
        reader.addEventListener("load", (event: any) => {
            this.avatar = reader.result;
            this.urlBase64 = this.avatar.split(",");
            this.miembro.avatar = this.urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /**
     * Permite insertar nuevo cliente
     */ 
    insertarMiembro() {
        if (this.nombreValido) {
            this.msgService.showMessage(this.i18nService.getLabels('usuarios-error-username'));
            return;
        }
        if (this.correoValido) {
            this.msgService.showMessage(this.i18nService.getLabels('usuario-correo'));
            return;
        }
        this.miembro.fechaIngreso = new Date();
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.miembro.indPolitica = false;
                this.miembroService.token = token || "";
                if (this.miembro.avatar != null) {
                    this.subiendoImagen = true;
                }
                let sub = this.miembroService.insertarMiembro(this.miembro).subscribe(
                    (data) => {
                        this.miembro.idMiembro = data;
                        this.msgService.showMessage(this.i18nService.getLabels('general-insercion'));
                        this.gotoDetail(this.miembro.idMiembro);
                    },
                    (error) => {
                        this.manejaError(error)
                    },
                    () => { sub.unsubscribe(); this.subiendoImagen = false; }
                );
            }
        )
    }

    /** 
     * Se valida la existencia del correo del miembro, este debe ser único
     */ 
    validarExistencia(elemento: string) {
        if (elemento == "correo") {
            if (this.miembro.correo == null || this.miembro.correo == "") {
                this.correoValido = false;
                return;
            }
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.miembroService.token = token;
                    let sub = this.miembroService.existenciaCorreo(this.miembro.correo.trim()).subscribe(
                        (data) => {
                            this.correoValido = data;
                            if (this.correoValido) {
                                this.msgService.showMessage(this.i18nService.getLabels('usuario-correo'));
                            }
                        },
                        (error) => { this.manejaError(error) },
                        () => { sub.unsubscribe() });
                });
        }

        if (elemento == "nombreUsuario") {
            if (this.miembro.nombreUsuario == null || this.miembro.nombreUsuario == "") {
                this.nombreValido = false;
                return;
            }
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.miembroService.token = token;
                    let sub = this.miembroService.existenciaNombreUsuario(this.miembro.nombreUsuario.trim()).subscribe(
                        (data) => {
                            this.nombreValido = data;
                            if (this.nombreValido) {
                                this.msgService.showMessage(this.i18nService.getLabels('usuarios-error-username'));
                            }
                        },
                        (error) => { this.manejaError(error) },
                        () => { sub.unsubscribe() });
                });
        }
    }

    /**
     * Método que redireccional al detalle del cliente
     */ 
    gotoDetail(idMiembro: string) {
        let link = ['/miembros/detalleMiembro', idMiembro];
        this.router.navigate(link);
    }

    /**
     * Método que permite regresar a la lista de miembros
     */ 
    goBack() {
        //window.history.back();
        let link = ['miembros'];
        this.router.navigate(link);
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
