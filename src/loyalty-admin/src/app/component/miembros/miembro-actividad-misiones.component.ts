import { ErrorHandlerService, MessagesService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { ActividadService, KeycloakService, I18nService } from '../../service/index';
import { Miembro, RegistroActividad } from '../../model/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { PERM_LECT_MISIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'miembro-actividad-misiones-component',
    templateUrl: 'miembro-actividad-misiones.component.html',
    providers: [Miembro, ActividadService]
})

/**
 * Componente que permite ver las actividades de misiones de un miembro
 */
export class MiembroActividadMisionesComponent implements OnInit {
    public listaActividades: any[];

    /**Atributos para sorting/busqueda */
    public indPeriodo: string = '1M';
    public periodos: SelectItem[] = [];
    public lectura: boolean; //Permiso de lectura sobre misiones

    constructor(
        public miembro: Miembro,
        public actividadService: ActividadService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public route: ActivatedRoute,
        public router: Router,
        public i18nService: I18nService,
        public authGuard: AuthGuard
    ) {
        //Permite saber si el usuario tiene permisos de lectura sobre misiones
        this.authGuard.canWrite(PERM_LECT_MISIONES).subscribe((permiso: boolean) => {
            this.lectura = permiso;
        });
    }

    ngOnInit() {
        //Obtiene los períodos por default del sistema
        this.periodos = this.i18nService.getLabels("general-actividad-periodos");
        this.route.parent.params.subscribe((params) => {
            if (params['id']) {
                this.miembro.idMiembro = params['id'];
                this.obtenerActividad();
            } 
        });
    }

    /**
     * Obtiene los registros de actividad para el miembro actual
     */
    obtenerActividad() {
        this.kc.getToken().then((tkn) => {
            this.actividadService.token = tkn;
            let sub = this.actividadService.getActividadesMisionPorMiembro(this.miembro.idMiembro, this.indPeriodo).subscribe(
                (response: RegistroActividad[]) => {
                    this.listaActividades = response;
                }, (error: Response) => {
                    this.handleError(error);
                }, () => {
                    sub.unsubscribe();
                }
            );
        }).catch()

    }
    /**
     * Redirige al administrador a la pantalla de dashboard para el elemento seleccionado
     */
    gotoDetail(idElemento: string) {
        this.router.navigate(['/misiones/detalleMision', idElemento]);
    }
    /**
     * Maneja el erro a nivel de componente
     */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}