import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthGuard } from '../common/index';
import { Miembro } from '../../model/index';
import { MiembroService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_MIEMBROS } from '../common/auth.constants';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import * as Routes from '../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'miembro-lista-component',
    templateUrl: 'miembro-lista.component.html',
    providers: [Miembro, MiembroService],
})

/**
 * Jaylin Centeno
 * Componente utilizado para mostrar la lista de miembros disponibles en el sistema
 */
export class MiembroListaComponent implements OnInit {

    public escritura: boolean; //Permiso de escritura (true/false)
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public avanzada: boolean = false; //Muestra la búsqueda avanzada
    public suspender: boolean; //Habilita el cuadro de dialogo para suspender al miembro
    public confirmar: boolean = false; //Habilita el cuadro de dialogo para confirmar la suspencón del miembro
    public pregunta: string; //Guarda la pregunta para el dialogo de confirmacion 

    public listaMiembros: Miembro[]; //Lista de miembros
    public listaVacia: boolean; //Para verificar si la lista de miembros esta vacia
    public totalRecords: number; //Total de miembros en el sistema
    public cantidad: number = 10; //Cantidad de rows
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    //  Parametros para busqueda avanzada
    public estado: string[] = []; //Estado de miembro A = activo I = suspendido  
    public listaBusqueda: string[] = [];
    public filtros: string[] = [];
    public generos: string[] = [];
    public estadosCiviles: string[] = [];
    public educaciones: string[] = [];
    public indEmail: boolean = undefined;
    public indSMS: boolean = undefined;
    public indNotificacion: boolean = undefined;
    public tipoOrden: string;
    public campoOrden: string;
    public indHijos: boolean;

    constructor(
        public miembro: Miembro,
        public miembroService: MiembroService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public i18nService: I18nService,
        public keycloakService: KeycloakService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.pregunta = this.i18nService.getLabels("confirmacion-pregunta")["D"];
    }

    /** Método que se ejecuta al entrar por primera vez al componente */
    ngOnInit() { 

        this.busquedaMiembros(); 
    }

    /** Carga los elementos de miembro en el dataTable */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaMiembros();
    }

    /**
     * Método que redirecciona o muestra la suspencion del miembro seleccionado segun la acción especificada.
     */
    miembroSeleccionado(miembro: Miembro, accion: string) {
        this.miembro = miembro;
        if (accion == "detalle") {
            let link = [Routes.RT_MIEMBROS_DETALLE, this.miembro.idMiembro];
            this.router.navigate(link);
        } else { this.suspender = true; }
    }

    /**
     * Método que se encarga de la búsqueda de miembros según los criterios definidos
     * Búsqueda, estado, fila desplazamiento, cantidad de filas, filtros, genero, estado civil, 
     * educacion, indicadores: email - sms - notificacion - hijos, tipo y campo de orden
     */
    busquedaMiembros() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.busquedaMiembros(this.estado, this.filaDesplazamiento, this.cantidad, this.listaBusqueda.join(" "), this.filtros, this.generos,
                    this.estadosCiviles, this.educaciones, this.indEmail, this.indSMS, this.indNotificacion, this.indHijos, this.tipoOrden, this.campoOrden).subscribe(
                    (response: Response) => {
                        this.listaMiembros = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaMiembros.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                    },
                    (error) => this.manejaError(error)
                    );
            }
        );
    }

    /**
     * Método para redireccionar a la página de inserción de un miembro
     */
    nuevoCliente() {
        this.router.navigate([Routes.RT_MIEMBROS_INSERTAR]);
    }

    /**
     * Método utilizado para suspender a un miembro
     */
    suspenderMiembro() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.suspenderMiembro(this.miembro.idMiembro, this.miembro.causaSuspension).subscribe(
                    (data) => {
                        this.busquedaMiembros();
                        this.confirmar = false;
                        this.msgService.showMessage(this.i18nService.getLabels("miembro-suspencion"));
                    },
                    (error) => { this.manejaError(error); this.confirmar = false }
                );
            }
        );

    }

    /**
     * Método que redirecciona al detalle del miembro seleccionado
     */
    gotoDetail(idMiembro: string) {
        let link = [Routes.RT_MIEMBROS_DETALLE, idMiembro];
        this.router.navigate(link);
    }

    /** Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
