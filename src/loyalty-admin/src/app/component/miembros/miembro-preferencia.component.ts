import { Component, OnInit } from '@angular/core';
import { RespuestaPreferencia, Preferencia, Miembro } from '../../model/index';
import { RespuestaPreferenciaService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { Response, Headers } from '@angular/http';
import * as Rutas from '../../utils/rutas';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'miembro-preferencia-component',
    templateUrl: 'miembro-preferencia.component.html',
    providers: [RespuestaPreferencia, RespuestaPreferenciaService, Preferencia, Miembro]
})

/**
 * Componente que muestra las preferencias del miembro
 */
export class MiembroPreferenciaComponent implements OnInit {

    public idMiembro: string; //Id del miembro pasado por la url
    public idPreferencia: string; //Id de la preferencia seleccionada
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos

    // ------  Respuestas  ------ //
    public listaRespuestas: any[]; //Lista con las repuestas del miembro
    public respuestaVacia: boolean = true; //Permite verificar si la lista esta vacia
    public cantidad: number = 10; //Cantidad de filas
    public totalRecords: number; //Total de respuesas del miembro
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    // ------   Busqueda   ------ //
    public busqueda: string; //Palabras clave de búsqueda
    public listaBusqueda: string[] = [];
    public tipo: string[] = []; //Tipos de respuesta (selec. unica = SU, resp. multip = RM, texto = TX)

    constructor(
        public respuesta: RespuestaPreferencia,
        public respuestaService: RespuestaPreferenciaService,
        public route: ActivatedRoute,
        public router: Router,
        public keycloakService: KeycloakService,
        public i18nService: I18nService, 
        public msgService: MessagesService
    ) {
        this.route.parent.params.subscribe(params => { this.idMiembro = params["id"]; });
    }

    ngOnInit() {
        this.busquedaRespuestasMiembro();
    }

    //Preferencia seleccionada
    preferenciaSeleccionada(idPreferencia: any) {
        this.idPreferencia = idPreferencia;
        let preferencia = Rutas.RT_PREFERENCIAS_DETALLE_PREFERENCIA;
        let link = [preferencia, this.idPreferencia];
        this.router.navigate(link);
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaRespuestasMiembro();
    }

    //Método para obtener las respuestas de preferencias relacionadas con el miembro
    busquedaRespuestasMiembro() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token) => {
                this.respuestaService.token = token || "";
                this.respuestaService.busquedaRespuestasMiembro(this.idMiembro, this.cantidad, this.filaDesplazamiento, this.tipo, this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaRespuestas = response.json();
                        if (this.listaRespuestas.length > 0) {
                            this.respuestaVacia = false;
                        }
                        else {
                            this.respuestaVacia = true;
                        }
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}