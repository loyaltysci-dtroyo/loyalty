import { ErrorHandlerService, MessagesService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { ActividadService, KeycloakService } from '../../service/index';
import { Miembro, RegistroActividad } from '../../model/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';

@Component({
    moduleId: module.id,
    selector: 'miembro-actividad-awards-component',
    templateUrl: 'miembro-actividad-awards.component.html',
    providers: [Miembro,ActividadService]
})

/**
 * Componente que muestra las actividades de premios del miembro 
 */
export class MiembroActividadAwardsComponent implements OnInit {
    public listaActividades: any[];

    /**Atributos para sorting/busqueda */
    public indPeriodo: string = '1M';
    public periodos: SelectItem[] = [
        { label: '1 mes', value: '1M' },
        { label: '2 meses', value: '2M' },
        { label: '3 meses', value: '3M' },
        { label: '6 meses', value: '6M' },
        { label: '1 año', value: '1Y' },
        { label: '2 años', value: '2Y' }];

    constructor(
        public miembro: Miembro,
        public actividadService: ActividadService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public route: ActivatedRoute,
        public router: Router) {

    }

    ngOnInit() {
        this.route.parent.params.subscribe((params) => {
            if (params['id']) {
                this.miembro.idMiembro = params['id'];
                this.obtenerActividad();
            } 
        });
    }

    /**
     * Obtiene los registros de actividad para el miembro actual
     */
    obtenerActividad() {
        this.kc.getToken().then((tkn) => {
            this.actividadService.token = tkn;
            let sub = this.actividadService.getActividadesPremiosPorMiembro(this.miembro.idMiembro, this.indPeriodo).subscribe(
                (response: RegistroActividad[]) => {
                    this.listaActividades = response;
                }, (response: Response) => {
                    this.handleError(response);
                }, () => {
                    sub.unsubscribe();
                }
            );
        }).catch()

    }
    /**
     * Redirige al administrador a la pantalla de dashboard para el elemento seleccionado
     */
    gotoDetail(idElemento: string) {
        // this.router.navigate(['/notificaciones/detalleNotificacion', idElemento]);
    }
    /**
     * Maneja el erro a nivel de componente
     */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}