import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Grupo, Miembro, GrupoMiembro } from '../../model/index';
import { GrupoService, MiembroService, GrupoMiembroService, KeycloakService, I18nService } from '../../service/index';
import { MenuItem } from 'primeng/primeng';
import { Response, Headers } from '@angular/http';
import { PERM_ESCR_MIEMBROS, PERM_ESCR_GRUPOS, PERM_LECT_GRUPOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'miembro-grupo-component',
    templateUrl: 'miembro-grupo.component.html',
    providers: [Grupo, GrupoService, MiembroService, GrupoMiembro, GrupoMiembroService]
})

/*Componente para ver, remover y asociar miembros a grupos */
export class MiembroGrupoComponent implements OnInit {

    public escritura: boolean; //Escritura sobre miembro, usado en asignar y remover grupos(true/false)
    public escrituraGrupo: boolean; //Escritura sobre grupo, usado en asignar y remover grupos(true/false)
    public lecturaGrupo: boolean; //Permite verificar permisos de lectura de grupos (true/false)    
    public asignar: boolean = false; //Mostrar / Ocultar información para asociar al grupo
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public eliminar: boolean; //Habilita el cuadro de dialogo para remover

    public listaGrupos: Grupo[]; //Lista con los grupos a los que el miembro esta asociado
    public listaVacia: boolean; //Para verificar existen grupos asociados al miembro
    public cantidadGrupos: number = 10; //Cantidad de filas
    public totalRecordsGrupos: number; //Total de miembros en el sistema
    public filaDesplazMiembro: number = 0; //Desplazamiento de la primera fila

    public listaTotal: Grupo[]; //Lista con los grupos disponibles del sistema
    public listaTotalVacia: boolean = true; //Para verificar si existen grupos en la aplicacion
    public cantidad: number = 10; //Cantidad de filas
    public totalRecords: number; //Total de miembros en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    public busqueda: string; //Palabras clave de búsqueda
    public listaBusqueda: string[] = [];

    constructor(
        public grupo: Grupo,
        public grupoService: GrupoService,
        public miembro: Miembro,
        public miembroService: MiembroService,
        public grupoMiembroService: GrupoMiembroService,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public router: Router,
        public route: ActivatedRoute,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_ESCR_GRUPOS).subscribe((permiso: boolean) => {
            this.escrituraGrupo = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_GRUPOS).subscribe((permiso: boolean) => {
            this.lecturaGrupo = permiso;
        });
    }

    ngOnInit() {
        this.obtenerGruposMiembro();
    }

    //Método para mostrar el dialogo de Información
    confirmar(idGrupo: string) {
        this.grupo.idGrupo = idGrupo;
        this.eliminar = true;
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadMiembroGrupo(event: any) {
        this.filaDesplazMiembro = event.first;
        this.cantidadGrupos = event.rows;
        this.obtenerGruposMiembro();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadMiembroNoGrupo(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerGrupos();
    }

    //Llama al metodo para obtener los grupos disponibles
    asignarGrupo() {
        if (!this.cargandoDatos) {
            this.cargandoDatos = true;
        }
        this.obtenerGrupos();
        this.asignar = true;
    }

    //Método para obtener los grupos a los que pertenece el miembro
    obtenerGruposMiembro() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        let tipo = "A";
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.obtenerGruposPorMiembro(this.miembro.idMiembro, this.cantidadGrupos, this.filaDesplazMiembro, tipo, this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaGrupos = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecordsGrupos = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaGrupos.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => { this.manejarError(error); this.cargandoDatos = false; }
                );
            }
        );
    }

    //Método para obtener los grupos en los que el miembro no esta
    obtenerGrupos() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        let tipo = "D";
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.obtenerGruposPorMiembro(this.miembro.idMiembro, this.cantidadGrupos, this.filaDesplazMiembro, tipo, this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaTotal = response.json();
                        //Se recibe el total de las categorías disponibles
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaTotal.length > 0) {
                            this.listaTotalVacia = false;
                        }
                        else {
                            this.listaTotalVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => { this.manejarError(error); this.cargandoDatos = false; }
                );
            }
        );
    }

    //Método para asociar un miembro a un grupo
    asocicarMiembroGrupo(grupo: Grupo) {
        if (this.escritura && this.escrituraGrupo) {
            this.grupo = grupo;
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token) => {
                    this.grupoMiembroService.token = token || "";
                    this.grupoMiembroService.agregarMiembro(this.miembro.idMiembro, this.grupo.idGrupo).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                            this.meterGrupoLista();
                            this.obtenerGrupos();
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
        }
    }

    //Método para remover la asignacion del miembro con un grupo
    removerGrupoMiembro() {
        if (this.escritura && this.escrituraGrupo) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token) => {
                    this.grupoMiembroService.token = token || "";
                    this.grupoMiembroService.removerMiembro(this.miembro.idMiembro, this.grupo.idGrupo).subscribe(
                        (data) => {
                            this.sacarGrupoListaMiembro();
                            this.msgService.showMessage(this.i18nService.getLabels('general-exclusion-simple'));
                            this.eliminar = false;
                            this.obtenerGruposMiembro();
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
        }
    }

    //Método para sacar a los grupos de los cuales el miembro ya no forma parte
    sacarGrupoListaMiembro() {
        let listaTemporal = this.listaGrupos;
        let posicion;
        if (this.listaGrupos.length >= 0) {
            posicion = this.listaGrupos.findIndex(element => element.idGrupo == this.grupo.idGrupo);
            this.listaGrupos.splice(posicion, 1);
            this.listaGrupos = [...this.listaGrupos];
        }
    }

    //Método que sacar de la lista los grupos que ya fueron agregados
    meterGrupoLista() {
        let listaTemporal = this.listaTotal;
        let posicion;
        if (this.listaTotal.length >= 0) {
            posicion = this.listaTotal.findIndex(element => element.idGrupo == this.grupo.idGrupo); 
            this.listaTotal.splice(posicion, 1);
            this.listaTotal = [...this.listaTotal];
        }
        this.obtenerGruposMiembro();
    }

    //redirecciona al detalle del grupo
    gotoGrupo(idGrupo: string) {
        let link = ['grupos/detalleGrupo/', idGrupo];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}