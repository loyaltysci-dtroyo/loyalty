import { AppConfig } from '../../app.config';
import { MessagesService, ErrorHandlerService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { Http, RequestOptionsArgs, Headers, Response } from '@angular/http';
import { Ubicacion, Miembro, Metrica } from '../../model/index';
import { MiembroService, MetricaService, UbicacionService, KeycloakService, I18nService, TransaccionesService } from '../../service/index';
import { Message, SelectItem } from "primeng/primeng";
import { PERM_LECT_METRICAS } from '../common/auth.constants';
import { ActivatedRoute } from "@angular/router";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthGuard } from '../common/index';
import { IDMetrica } from 'app/model/IDMetrica';
@Component({
    moduleId: module.id,
    selector: 'miembro-compras',
    templateUrl: 'miembro-compras.component.html',
    providers: [Miembro, Metrica, MetricaService, MiembroService, Ubicacion, UbicacionService,TransaccionesService]
})

/**
 * Componente que permite realizar compras del miembro
 */
export class MiembroComprasComponent implements OnInit {
    public headers: Headers;
    public URL: string;
    public resp: Response;
    public lecturaMetrica: boolean; //Permite verificar permisos de lectura sobre metrica(true/false)
    public data: any;
    public msgsSuccess: Message[];
    public msgsError: Message[];
    public formCompra: FormGroup;
    public metricaSeleccionada: Metrica;
    public request: {
        idCompra: string,
        fecha: Date,
        subtotal: number,
        impuesto: number,
        total: number,
        miembro: Miembro,
        ubicacion: Ubicacion,
        idMetrica: string,
        cantMetrica: number
    };
    private idMiembro: string;
    public busqueda: string; //Palabras clave de búsqueda
    public listaBusqueda: string[] = []; //Lista de las palabras ingresadas en el Chip 
    public listaTransacciones:any[];

    // --------- Métricas --------- //
    public listaMetricas: Metrica[] = []; // Métricas disponibles en el sistema
    public metricaVacia: boolean; //Permite verificar si la lista de métricas esta vacía
    public totalRecordsMetricas: number; //Total de premios disponibles
    public metricaItems: SelectItem[]; //Lista para el dropdown con las métricas disponibles 
    public cantidadMetricas: number = 5; //Cantidad de métricas
    public filaMetricas: number = 0; //Desplazamiento de la primera fila
    public displayDialogMetrica: boolean;
    public nombreMetrica: string;
    public metricaBusqueda: string[] = []; //Palabras de búsqueda, ChipModule

    constructor(
        private route: ActivatedRoute,
        private http: Http,
        private msgService: MessagesService,
        private ubicacionSearch: Ubicacion,
        private ubicacionService: UbicacionService,
        public metricaService: MetricaService,
        private miembroService: MiembroService,
        private keycloakService: KeycloakService,
        public authGuard: AuthGuard,
        private i18nService: I18nService,
        private transaccionService: TransaccionesService
    ) {
        this.msgsSuccess = [];
        this.msgsError = [];
        this.headers = new Headers();
        this.headers.append("Accept", "application/json");
        this.headers.append("Content-Type", "application/json");
        this.headers.append("Authorization", "");
        //POST /transaccion-compra
        this.URL = AppConfig.TRANS_API_ENDPOINT + "/transaccion-compra";

        //Validacion del formulario y datos por defecto
        this.formCompra = new FormGroup({
            'idCompra': new FormControl('', Validators.required),
            'ubicacion': new FormControl(''),
            'fecha': new FormControl('', Validators.required),
            'subtotal': new FormControl('', Validators.required),
            'impuesto': new FormControl('', Validators.required),
            'metrica': new FormControl(''),
            'total': new FormControl({value: '', disabled: true})
        });

        this.authGuard.canWrite(PERM_LECT_METRICAS).subscribe((result: boolean) => {
            this.lecturaMetrica = result;
        });
    }

    ngOnInit() {
        this.request = {
            idCompra: undefined,
            fecha: new Date(),
            subtotal: 0,
            impuesto: 0,
            total: 0,
            miembro: undefined,
            ubicacion: undefined,
            idMetrica: undefined,
            cantMetrica: 0
        };
        //Se obtiene el id del miembro 
        this.route.parent.params.subscribe((params) => {
            if (params["id"] && params["id"] != "") {
                this.idMiembro = params["id"];
            }
        });
    }

    //Calcula el total de la compra de un miembro
    calculoTotal() {
        this.request.total = this.request.subtotal + this.request.impuesto;
    }

    obtenerListadoCompras() {
        if (this.idMiembro) {
           this.keycloakService.getToken().then((tkn) => {
                let sub= this.transaccionService.obtenerListadoTransacciones(this.idMiembro).subscribe(
                    (response) => {
                        this.listaTransacciones=response;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    });
            });
        }
    }

    //Método encargado de realizar la búsqueda de métricas
    busquedaMetricas() {
        if (this.lecturaMetrica) {
            let busqueda = "";
            busqueda = this.listaBusqueda.join(" ")
            let estado = ["P"];
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.metricaService.token = token;
                    this.metricaService.buscarMetricas(estado, this.cantidadMetricas, this.filaMetricas, busqueda).subscribe(
                        (response: Response) => {
                            this.listaMetricas = response.json();
                            if (response.headers.get("Content-Range") != null) {
                                this.totalRecordsMetricas = + response.headers.get("Content-Range").split("/")[1];
                            }
                            /*this.metricaItems = [];
                            this.metricaItems=[...this.metricaItems,this.i18nService.getLabels("general-default-select")];*/
                            if (this.listaMetricas.length > 0) {
                                this.metricaVacia = false;
                                /*this.listaMetricas.forEach(metrica => {
                                    this.metricaItems=[...this.metricaItems,{ label: metrica.nombre, value: metrica }];
                                });*/
                            } else {
                                this.metricaVacia = true;
                            }
                        },
                        (error) => this.manejaError(error)
                    );
                });
        }
    }

    seleccionarMetrica(metrica: Metrica){
        this.request.idMetrica = metrica.idMetrica;
        this.nombreMetrica = metrica.nombre;
        this.displayDialogMetrica = false;
    }

    loadDataMetrica(event: any) {
        this.filaMetricas = event.first;
        this.cantidadMetricas = event.rows;
        this.busquedaMetricas();
    }

    registrarCompra() {
        if (this.idMiembro && this.idMiembro != "") {
            this.keycloakService.getToken().then((tkn) => {
                this.miembroService.token = tkn;
                this.miembroService.obtenerMiembroPorId(this.idMiembro).subscribe((response) => {
                    this.request.miembro = response;
                    console.log(this.request);
                    if (!this.request.ubicacion) { this.request.ubicacion = undefined }
                    this.headers.set("Authorization", "bearer " + tkn);
                    let sub = this.http.post(this.URL, JSON.stringify(this.request), { headers: this.headers }).subscribe(
                        (response: Response) => {
                            this.data = response.json();
                            this.resp = response;
                            this.msgsSuccess=[...this.msgsSuccess,{ detail: response.statusText + " " + response.text(), severity: "success", summary: response.status + "" }];
                            this.msgService.mostrarSuccess("Registrada", "La compra ha sido registrada");
                        },
                        (err: Response) => {
                            this.msgsError=[...this.msgsError,{ detail: err.statusText + " " + err.text(), severity: "error", summary: err.status + "" }];
                            this.msgService.mostrarError("Error", "Ha ocurrido un error")
                        },
                        () => {
                            sub.unsubscribe();
                        }
                    );
                }, (error) => { this.manejaError(error) });

            });
        }
    }
    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}