import { Component, OnInit } from '@angular/core';
import { Response, Headers } from '@angular/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Miembro, Segmento } from '../../model/index';
import { MiembroService, KeycloakService } from '../../service/index';
import { PERM_LECT_SEGMENTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'miembro-segmento-component',
    templateUrl: 'miembro-segmento.component.html',
    providers: [Miembro, Segmento, MiembroService]
})

/*componente que permite mostrar los segmentos en los que el miembro es elegible*/
/*-------------------------------------------------------------------------------------------------------*/
export class MiembroSegmentoComponent implements OnInit {


    public id: string; // Id del miembro pasado por la url
    public listaSegmentos: Segmento[] = []; // Lista de los segmentos
    public totalRecords: number; // Total de miembros en el sistema
    public cantidad: number = 0; // Cantidad de filas
    public filaDesplazamiento: number = 0; // Desplazamiento de la primera fila
    public listaVacia: boolean = true; // Para validar si la lista de segmentos viene vacía
    public lecturaSegmento: boolean; //Permite verificar permisos de lectura de segmentos (true/false)

    constructor(
        public miembroService: MiembroService,
        public router: Router,
        public route: ActivatedRoute,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public authGuard: AuthGuard

    ) {
        //Optiene los permisos de lectura sobre segmentos
        this.authGuard.canWrite(PERM_LECT_SEGMENTOS).subscribe((permiso: boolean) => {
            this.lecturaSegmento = permiso;
            this.obtenerListaSegmentos();
        });

        //Permite capturar el id del miembro que viene en la url
        this.route.parent.params.subscribe(params => { this.id = params["id"]; });
    }

    ngOnInit() {
        this.obtenerListaSegmentos()
    }

    /*
    * Método utilizado para la carga de datos (lazyLoad)
    * en la tabla que mostrará la lista de segmentos 
    */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerListaSegmentos();
    }

    /*
    * Método para obtener la lista de segmentos en los que el miembro es elegible
    * Envia el id del miembro, la cantidad y fila de desplazamiento
    */
    obtenerListaSegmentos() {
        if (this.lecturaSegmento) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.miembroService.token = token || "";
                    this.miembroService.obtenerSegmentosMiembro(this.id, this.cantidad, this.filaDesplazamiento).subscribe(
                        (response: Response) => {
                            if (response.headers.has("Content-Range")) {
                                this.listaSegmentos = response.json();
                                this.totalRecords = parseInt(response.headers.get("Content-Range").split("/")[1]);
                            }
                        },
                        (error) => {
                            this.manejarError(error)
                        }
                    );
                }
            );
        }
    }

    // Metodo para volver a la página anterior
    gotoDetail(idSegmento: string) {
        let link = ['segmentos/detalleSegmento/', idSegmento];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}