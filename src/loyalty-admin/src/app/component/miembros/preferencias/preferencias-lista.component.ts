import {RT_PREFERENCIAS_DETALLE_PREFERENCIA, RT_PREFERENCIAS_INSERTAR_PREFERENCIA} from '../../../utils/rutas';
import {Preferencia} from '../../../model/index';
import {PreferenciaService, KeycloakService} from '../../../service/index';
import {MessagesService, ErrorHandlerService} from "../../../utils/index";
import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Http} from '@angular/http';
import {MenuItem } from 'primeng/primeng';

@Component({
    moduleId: module.id,
    selector: 'preferencias-component',
    templateUrl: 'preferencias-lista.component.html',
    providers: [Preferencia, PreferenciaService]
})
/**
* Flecha Roja Technologies oct-2016
* Fernando Aguilar
* Servicio encargado de realizar operaciones de gestion de datos de promocion interactuando directamente con el API
*/
export class PreferenciasListaComponent implements OnInit {
    public preferencias: Preferencia[];
    public listaBusqueda: string[] = [];
    public terminoBusqueda: string;//contiene el termino de busqueda proveniente dle cmapo de texto
    public preferenciasBusqueda: string[];//contiene las preferencia de busquda ingresadas por el usuario en los checks de tipo
    public preferenciaEliminar: Preferencia;
    public displayModalConfirm: boolean = false;//muestra el modal de eliminar
    public displayBusquedaAvanzada: boolean;
    public itemsBusqueda: MenuItem[];
    public rtInsertarPreferencias = RT_PREFERENCIAS_INSERTAR_PREFERENCIA;

    constructor(
        public router: Router,
        public http: Http,
        public preferencia: Preferencia,
        public preferenciaService: PreferenciaService,
        public msgService: MessagesService, public keycloakService: KeycloakService
    ) {
        this.terminoBusqueda = "";
        this.preferencias = [];
        this.preferenciasBusqueda = ["SU", "RM", "TX"];
        this.itemsBusqueda = [];
        this.itemsBusqueda=[...this.itemsBusqueda,{ label: "Avanzado", command: () => { this.displayBusquedaAvanzada = !this.displayBusquedaAvanzada } }];
        this.preferencia.indTipoRespuesta;
    }

    ngOnInit() {
        this.buscarPreferencias();
    }
    /**
     * Busca las preferencias en el API basandodse en los atributos de busqueda introducidos por el usuario
     * Emplea: el arreglo de preferencias de busqueda y el termino de busueda introdcido
     */
    buscarPreferencias() {
        this.terminoBusqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.terminoBusqueda += element + " ";
            });
            this.terminoBusqueda = this.terminoBusqueda.trim();
        }
        this.keycloakService.getToken().then((tkn: string) => {
            this.preferenciaService.token = tkn;
            let sub = this.preferenciaService.busqueda(this.terminoBusqueda, this.preferenciasBusqueda).subscribe(
                (data: Preferencia[]) => {
                    this.preferencias = data;
                },
                (error) => { this.manejaError(error); },
                () => { sub.unsubscribe(); }
            );
        }).catch((error) => { this.manejaError(error) });


    }

    /**Obtiene todas las preferencias del API */
    getAllPreferencias() {
        this.keycloakService.getToken().then((tkn: string) => {
            this.preferenciaService.token = tkn;
            let sub = this.preferenciaService.getListaPreferencias().subscribe(
                (data: Preferencia[]) => {
                    this.preferencias = data;
                },
                (error) => { this.manejaError(error); },
                () => { sub.unsubscribe(); }
            );
        }).catch((error) => { this.manejaError(error) });

    }
    /**navega hacia el detalle de una preferencia en concreto */
    gotoDetail(preferencia: Preferencia) {
        let link = RT_PREFERENCIAS_DETALLE_PREFERENCIA;
        this.router.navigate([link, preferencia.idPreferencia]);
    }
    /**elimina la preferencia del sistema, es invocado por el modal de confirmacion */
    eliminarPreferencia() {
        this.keycloakService.getToken().then((tkn: string) => {
            this.preferenciaService.token = tkn;
            let sub = this.preferenciaService.deletePreferencia(this.preferenciaEliminar).subscribe(
                () => {
                    this.buscarPreferencias();
                    this.displayModalConfirm = false;
                },
                (error) => { this.manejaError(error); },
                () => { sub.unsubscribe(); }
            );
        }).catch((error) => { this.manejaError(error) });
    }

    mostrarModalEliminarPreferencia(prefrencia: Preferencia) {
        this.preferenciaEliminar = prefrencia;
        this.displayModalConfirm = true;
    }
    /**
     * Encargado de manejo de errores
     */
    manejaError(error:any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
