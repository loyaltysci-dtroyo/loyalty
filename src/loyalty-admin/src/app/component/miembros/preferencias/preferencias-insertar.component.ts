import {RT_PREFERENCIAS_LISTA_PREFERENCIAS} from '../../../utils/rutas';
import {ErrorHandlerService, MessagesService} from '../../../utils/index';
import {Preferencia} from '../../../model/index';
import {PreferenciaService, KeycloakService, I18nService} from '../../../service/index';
import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Http} from '@angular/http';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {SelectItem} from 'primeng/primeng';

@Component({
    moduleId: module.id,
    selector: 'preferencia-insertar',
    templateUrl: 'preferencias-insertar.component.html',
    providers: [Preferencia, PreferenciaService]
})
/**
 * Flecha Roja Technologies 21-oct-2016
 * Fernando Aguilar
 * Componente encargado de insertar preguntas para almacenar las preferencias del usuario
 */
export class PreferenciasInsertarComponent implements OnInit {

    public form: FormGroup;
    public respuestas: string[];//array con las respuestas qu ele usuario ha inctroducido
    public itemsTipoRespuesta: SelectItem[];
    public preferenciaInsertar: string;//preferencia a insertar en la lista solamente

    constructor(
        public router: Router,
        public http: Http,
        public preferencia: Preferencia,
        public preferenciaService: PreferenciaService,
        public msgService: MessagesService, 
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        this.respuestas = [];
        this.itemsTipoRespuesta = [];
        //Se obtiene los items de tipo respuesta predefinidos en el sistema
        this.itemsTipoRespuesta = this.i18nService.getLabels("respuesta-preferencia-tipo");
        this.form = new FormGroup({
            "pregunta": new FormControl("", Validators.required),
            "indRespuesta": new FormControl("", Validators.required)
        });
    }

    ngOnInit() {
        this.preferencia.indTipoRespuesta = "RM";
        this.respuestas = [];
    }

    /**ELiminar una respuesta a la preferencia de la lista de respuestas disponibles */
    eliminarRespuesta(resp: string) {
        let idx = this.respuestas.findIndex((element) => { return element == resp });
        if (idx >= 0) {
            this.respuestas.splice(idx, 1);
            this.respuestas = [...this.respuestas];
        }
    }
    /**
     * Encargado de isnertar la preferencia a traves del service que conecta con el API */
    insertarPreferencia() {
        if((this.preferencia.indTipoRespuesta=="RM"&&this.respuestas.length>=2||this.preferencia.indTipoRespuesta=="SU"&&this.respuestas.length>=2||this.preferencia.indTipoRespuesta=="TX")){
        this.keycloakService.getToken().then((tkn) => {
            this.preferenciaService.token = tkn;
            let resp: string = "";
            this.respuestas.forEach((element,i)=>{
                if (i == this.respuestas.length-1) {
                  resp += this.respuestas[i];
                } else {
                   resp += this.respuestas[i] + "\n";
                }
            })
            this.preferencia.respuestas = resp.trim();
            let sub = this.preferenciaService.addPreferencia(this.preferencia).subscribe(
                (data: Preferencia) => {
                    this.msgService.showMessage({ detail: "Se ha insertado la preferencia correctamente", severity: "info", summary: "Insertado" })
                    this.router.navigate([RT_PREFERENCIAS_LISTA_PREFERENCIAS]);
                },
                (error) => {
                    this.manejaError(error)
                },
                () => { sub.unsubscribe(); }
            );
        }).catch((error) => { this.manejaError(error) })
        }else{
            this.msgService.showMessage({detail:"Debes especificar al menos dos respuestas",severity:"error",summary:"Error"})
        }

    }
    /**
    * Inserta una respuesta en la lista de preferencias en caso de que se trate e una repsueta multiple 
    * */
    insertarRespuestaLista() { 
        this.respuestas=[...this.respuestas,this.preferenciaInsertar.trim()];
        this.preferenciaInsertar="";   
    }

    //Metodo que permite regresar en la navegación
    goBack() {
        let link = ['miembros/listaPreferencias'];
        this.router.navigate(link);
    }

    manejaError(error:any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}