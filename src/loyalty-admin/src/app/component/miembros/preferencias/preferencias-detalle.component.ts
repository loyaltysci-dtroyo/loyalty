import {RT_PREFERENCIAS_LISTA_PREFERENCIAS} from '../../../utils/rutas';
import {ErrorHandlerService, MessagesService} from '../../../utils/index';
import {Preferencia} from '../../../model/index';
import {PreferenciaService, KeycloakService, I18nService} from '../../../service/index';
import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Http} from '@angular/http';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {SelectItem} from 'primeng/primeng';
/**
 * Flecha Roja Technologies
 * Autor: Fernando Aguilar
 * Componente encargado de mostrar los atributos de la preferencia para que el usuario pueda revisarlos
 *  ademas permite editar la preferencia
 */
@Component({
    moduleId: module.id,
    selector: 'detalle-preferencia',
    templateUrl: 'preferencias-detalle.component.html',
    providers: [Preferencia, PreferenciaService]
})
export class DetallePreferenciaComponent implements OnInit {

    public form: FormGroup;
    public respuestas: string[];//array con las respuestas qu ele usuario ha inctroducido
    public itemsTipoRespuesta: SelectItem[];
    public preferenciaInsertar: string;//preferencia a insertar en la lista solamente

    constructor(public router: Router,
        public http: Http,
        public preferencia: Preferencia,
        public preferenciaService: PreferenciaService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService, 
        public i18nService: I18nService,
        public route: ActivatedRoute//ruta actual
    ) {
        this.route.params.forEach((params: Params) => { this.preferencia.idPreferencia = params['id'] });
        this.respuestas = [];
        this.itemsTipoRespuesta = [];
        //Se obtiene los items de tipo respuesta predefinidos en el sistema
        this.itemsTipoRespuesta= this.i18nService.getLabels('respuesta-preferencia-tipo');
        this.form = new FormGroup({
            "pregunta": new FormControl("", Validators.required),
            "indRespuesta": new FormControl("", Validators.required)
        });
    }

    ngOnInit() {
        this.obtenerDetallePreferencia();
    }

    /**Encargado de insertar los datos de preferencia a traves del service que conecta con el API */
    obtenerDetallePreferencia() {
        this.keycloakService.getToken().then((tkn: string) => {
            this.preferenciaService.token = tkn;
            let sub = this.preferenciaService.getPreferencia(this.preferencia.idPreferencia).subscribe(
                (data: Preferencia) => {
                    this.preferencia = data;
                    this.respuestas = this.preferencia.respuestas.split("\n");
                },
                (error) => {
                    this.manejaError(error);
                },
                () => {
                    sub.unsubscribe();
                }
            );
        }).catch((error) => { this.manejaError(error) });
    }
    /**Encargado de modificar los datos de preferencia a traves del service que conecta con el API */
    editarPreferencia() {
        this.keycloakService.getToken().then((tkn: string) => {
            this.preferenciaService.token = tkn;
            let resp: string = "";
            this.respuestas.forEach((element, i) => {
                if (i == this.respuestas.length - 1) {
                    resp += this.respuestas[i];
                } else {
                    resp += this.respuestas[i] + "\n";
                }
            })
            this.preferencia.respuestas = resp;
            let sub = this.preferenciaService.updatePreferencia(this.preferencia).subscribe(
                (data: Preferencia) => {
                    this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'))
                    this.obtenerDetallePreferencia();
                },
                (error) => {
                    this.manejaError(error);
                },
                () => {
                    sub.unsubscribe();
                }
            );
        }).catch((error) => { this.manejaError(error) });
    }
    /**
     * Encargado de manejo de errores
     */
    manejaError(error:any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
     /**ELiminar una respuesta a la preferencia de la lista de respuestas disponibles */
    eliminarRespuesta(resp: string) {
        let idx = this.respuestas.findIndex((element) => { return element == resp });
        if (idx >= 0) {
            this.respuestas.splice(idx, 1);
            this.respuestas = [...this.respuestas];
        }
    }
    /**
    * Inserta una respuesta en la lista de preferencias en caso de que se trate e una repsueta multiple 
    * */
    insertarRespuestaLista(respuesta: string) {
        this.preferenciaInsertar="";    
        this.respuestas=[...this.respuestas,respuesta];
    }
    //Metodo que permite regresar en la navegación
    goBack() {
        let link = ['miembros/listaPreferencias'];
        this.router.navigate(link);
    }
}