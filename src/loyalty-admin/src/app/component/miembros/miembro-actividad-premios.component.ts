import { ErrorHandlerService, MessagesService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { ActividadService, KeycloakService, I18nService } from '../../service/index';
import { Miembro, RegistroActividad } from '../../model/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { PERM_LECT_PREMIOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'miembro-actividad-premios-component',
    templateUrl: 'miembro-actividad-premios.component.html',
    providers: [Miembro, ActividadService]
})
/**
 * Componente que lista la actividad del miembro con los premios
 */
export class MiembroActividadPremiosComponent implements OnInit {

    public listaActividades: any[];
    public lectura: boolean;
    /**Atributos para sorting/busqueda */
    public indPeriodo: string = '1M';
    public periodos: SelectItem[] = [];

    constructor(
        public miembro: Miembro,
        public actividadService: ActividadService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public i18nService: I18nService
        ) {
            //Consulta los permisos de lectura sobre premios
        this.authGuard.canWrite(PERM_LECT_PREMIOS).subscribe((permiso: boolean) => {
            this.lectura = permiso;
            this.obtenerActividad();
        });
    }

    ngOnInit() {
        //Obtiene los períodos por default del sistema
        this.periodos = this.i18nService.getLabels("general-actividad-periodos");
        this.route.parent.params.subscribe((params) => {
            if (params['id']) {
                this.miembro.idMiembro = params['id'];
                this.obtenerActividad();
            }
        });
    }

    /**
     * Obtiene los registros de actividad para el miembro actual
     */
    obtenerActividad() {
        if(this.lectura){
            this.kc.getToken().then((tkn) => {
            this.actividadService.token = tkn;
            let sub = this.actividadService.getActividadesPremiosPorMiembro(this.miembro.idMiembro, this.indPeriodo).subscribe(
                (response: RegistroActividad[]) => {
                    this.listaActividades = response;
                }, (response: Response) => {
                    this.handleError(response);
                }, () => {
                    sub.unsubscribe();
                }
            );
        }).catch()
        }
    }
    /**
     * Redirige al administrador a la pantalla de dashboard para el elemento seleccionado
     */
    gotoDetail(idElemento: string) {
        this.router.navigate(['/premios/detallePremio', idElemento]);
    }
    /**
     * Maneja el erro a nivel de componente
     */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}