import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Miembro, Insignia, InsigniaNivel } from '../../model/index';
import { InsigniaService, MiembroService, InsigniaMiembroService, KeycloakService, I18nService } from '../../service/index';
import { MenuItem } from 'primeng/primeng';
import { Response, Headers } from '@angular/http';
import { PERM_ESCR_MIEMBROS, PERM_ESCR_INSIGNIAS, PERM_LECT_INSIGNIAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'miembro-insignia-component',
    templateUrl: 'miembro-insignia.component.html',
    providers: [Insignia, InsigniaNivel, InsigniaService, InsigniaMiembroService, MiembroService]
})

/*Componente para ver, remover y asociar miembros a una insignia*/
export class MiembroInsigniaComponent implements OnInit {

    public escritura: boolean; //Permite verificar permiso de escritura (true/false)
    public escrituraInsignia: boolean; //Permite verificar permiso de escritura (true/false)
    public lecturaInsignia: boolean; //Permite verificar permiso de lectura sobre insignias (true/false)
    public verNiveles: boolean = false; //Habilita el cuadro de dialogo para ver los niveles de una insignia
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public cargandoNivel: boolean = true; //Utilizado para mostrar la carga de datos
    public asignar: boolean = false; // Oculta/Muestar información de insignias disponibles 
    public eliminar: boolean; //Habilita el cuadro de dialogo para eliminar una insignia

    public listaNoAsignada: Insignia[] = []; //Lista de insignias del sistema no asignadas
    public listaTotalVacia: boolean = true; //Para verificar si la lista esta vacia
    public cantidadInsigia: number = 10; //Cantidad de filas
    public totalRecordsInsignia: number;
    public filaDesplazInsignia: number = 0; //Desplazamiento de la primera fila

    public listaInsigniasMiembro: Insignia[]; //Lista de insignias que tiene asignado un miembro
    public listaVacia: boolean = true; //Para verificar si la lista esta vacia
    public cantidad: number = 10; //Cantidad de filas
    public totalRecords: number; //Total de miembros en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    public listaNiveles: InsigniaNivel[] = []; //Lista de los niveles de una insignia
    public listaNivelVacia: boolean = true; //Verificar si la lista de niveles esta vacia

    public busqueda: string;
    public listaBusqueda: string[] = [];
    public tipoInsignia: string[] = ["C", "S"];

    constructor(
        public insignia: Insignia,
        public insigniaNivel: InsigniaNivel,
        public miembro: Miembro,
        public miembroService: MiembroService,
        public insigniaService: InsigniaService,
        public insigniaMiembroService: InsigniaMiembroService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_ESCR_INSIGNIAS).subscribe((permiso: boolean) => {
            this.escrituraInsignia = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_INSIGNIAS).subscribe((permiso: boolean) => {
            this.lecturaInsignia = permiso;
        });
    }

    ngOnInit() {
        this.obtenerInsigniasMiembro();
        //this.obtenerInsigniasNoAsignadas();
    }

    //Método para mostrar el dialogo de confirmacion
    confirmar(idInsignia: string) {
        this.insignia.idInsignia = idInsignia;
        this.eliminar = true;
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadInsigniasNoMiembro(event: any) {
        this.filaDesplazInsignia = event.first;
        this.cantidadInsigia = event.rows;
        this.obtenerInsigniasNoAsignadas();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadInsigniasMiembro(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerInsigniasMiembro();
    }

    //Se encarga de obtener las insignias no asignadas al miembro
    asignarInsignia() {
        if (!this.cargandoDatos) {
            this.cargandoDatos = true;
        }
        this.listaBusqueda = [];
        this.obtenerInsigniasNoAsignadas();
        this.asignar = true;
    }

    //Metodo para obtener las insignias del miembro
    obtenerInsigniasMiembro() {
        let estado = ["P"];
        let tipo = "A";
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.obtenerInsigniasMiembro(this.miembro.idMiembro, this.cantidad, this.filaDesplazamiento, estado, tipo, this.tipoInsignia, this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaInsigniasMiembro = response.json();
                        //Se recibe el total de los grupos del miembro
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaInsigniasMiembro.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => { this.manejarError(error); this.cargandoDatos = false;}
                )
            }
        );

    }

    //Metodo para obtener las insignias no asignadas al miembro
    obtenerInsigniasNoAsignadas() {
        let estado = ["P"];
        let tipo = "D";
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.obtenerInsigniasMiembro(this.miembro.idMiembro, this.cantidadInsigia, this.filaDesplazInsignia, estado, tipo, this.tipoInsignia, this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaNoAsignada = response.json();
                        //Se recibe el total de los grupos del miembro
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecordsInsignia = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaNoAsignada.length > 0) {
                            this.listaTotalVacia = false;
                        } else {
                            this.listaTotalVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => { this.manejarError(error); this.cargandoNivel = false; }
                )
            }
        );
    }

    //Permite obtener los niveles en los que esta el miembro
    obtenerNivelInsigniaMiembro(insignia: Insignia) {
        this.insignia = insignia;
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token) => {
                this.insigniaMiembroService.token = token || "";
                this.insigniaMiembroService.obtenerNivelesMiembro(this.miembro.idMiembro, this.insignia.idInsignia).subscribe(
                    (data) => {
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Mpetodo para asociar una insignia con un miembro
    agregarMiembroInsignia(insignia: Insignia) {
        if (this.escritura && this.escrituraInsignia) {
            this.insignia = insignia;
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token) => {
                    this.insigniaMiembroService.token = token || "";
                    if (this.insignia.tipo == 'C') {
                        this.insigniaMiembroService.asingnarInsigniaMiembro(this.miembro.idMiembro, this.insignia.idInsignia).subscribe(
                            (data) => {
                                this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                                this.sacarInsigniaNoMiembro();
                            },
                            (error) => this.manejarError(error)
                        );
                    } else {
                        this.obtenerListaNiveles();
                        this.verNiveles = true;
                    }
                }
            );
        }
    }

    //Método para asociar un nivel de insignia con el miembro
    asociarNivelMiembro(idNivel: string) {
        if (this.escritura && this.escrituraInsignia) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token) => {
                    this.insigniaMiembroService.token = token || "";
                    this.insigniaMiembroService.asingnarNivelMiembro(this.miembro.idMiembro, this.insignia.idInsignia, idNivel).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                            this.sacarInsigniaNoMiembro();
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
            this.verNiveles = false;
            this.obtenerListaNiveles();
        }
    }

    //Obtiene la lista de los niveles de una insingnia
    obtenerListaNiveles() {
        if (this.lecturaInsignia) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token) => {
                    this.insigniaService.token = token || "";
                    this.insigniaService.obtenerListaNiveles(this.insignia.idInsignia).subscribe(
                        (data) => {
                            this.listaNiveles = data;
                            if (this.listaNiveles.length > 0) {
                                this.listaNivelVacia = false;
                            }
                            else {
                                this.listaNivelVacia = true;
                            }
                            this.cargandoNivel = false;
                        },
                        (error) => { this.manejarError(error); this.cargandoNivel = false; }
                    );
                }
            );
        }
    }

    //Método que sacar de la lista de no asignadas las insignias que ya fueron agregadas
    sacarInsigniaNoMiembro() {
        let listaTemporal = this.listaNoAsignada;
        let posicion;
        if (this.listaNoAsignada.length >= 0) {
            posicion = this.listaNoAsignada.findIndex(element => element.idInsignia == this.insignia.idInsignia);
            this.listaNoAsignada.splice(posicion, 1);
            this.listaNoAsignada = [...this.listaNoAsignada];
        }
        if (this.filaDesplazamiento >= this.totalRecords) {
            this.filaDesplazamiento = 0;
        }
        if (this.listaNoAsignada.length == 0) {
            this.obtenerInsigniasNoAsignadas();
        }
        this.obtenerInsigniasMiembro();
    }

    //Método que saca de la lista de asignadas la insignia que ya no van a estar asociadas
    sacarInsigniaLista() {
        let listaTemporal = this.listaInsigniasMiembro;
        let posicion;
        if (this.listaInsigniasMiembro.length >= 0) {
            posicion = this.listaInsigniasMiembro.findIndex(elemento => elemento.idInsignia == this.insignia.idInsignia)
            this.listaInsigniasMiembro.splice(posicion, 1);
            this.listaInsigniasMiembro = [...this.listaInsigniasMiembro];           
        }
        if (this.filaDesplazInsignia >= this.totalRecordsInsignia) {
            this.filaDesplazamiento = 0;
        }
        if (this.listaInsigniasMiembro.length == 0) {
            this.obtenerInsigniasMiembro();
        }
        this.obtenerInsigniasNoAsignadas();
    }

    //Método para remover la asignacion de una insignia
    removerInsignia() {
        if (this.escrituraInsignia && this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token) => {
                    this.insigniaMiembroService.token = token || "";
                    this.insigniaMiembroService.removerNivelMiembro(this.miembro.idMiembro, this.insignia.idInsignia).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-exclusion-simple'));
                            this.sacarInsigniaLista();
                            this.eliminar = false;
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
        }
    }

    //Redirecciona al detalle de la insignia
    gotoInsignia(idInsignia: string) {
        let link = ['insignias/detalleInsignia/', idInsignia];
        this.router.navigate(link);
    }

    // Metodo para volver a la página anterior
    goBack() {
        let link = ['miembros/detalleMiembro/', this.miembro.idMiembro];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}