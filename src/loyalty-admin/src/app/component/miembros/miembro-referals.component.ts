import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Miembro } from '../../model/index';
import { Response } from '@angular/http';
import { MiembroService, KeycloakService } from '../../service/index';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'miembro-referals-component',
    templateUrl: 'miembro-referals.component.html',
    providers: [MiembroService]
})

/* Componente que muestra los miembros referidos de un miembro especifico */
/*-------------------------------------------------------------*/
export class MiembroReferalsComponent implements OnInit {

    public escritura: boolean; //Permite verificar permisos de escritura (true/false)
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos

    //Miembros
    public listaReferidos: Miembro[]; //Miembros que han sido referidos
    public listaVacia: boolean; //Permite verificar si la lista de ubicaciones esta vacía
    public cantidad: number = 10; //Cantidad de filas en la tabla
    public totalRecords: number; //Total de referidos
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public idReferido: string; //Id del miembro referido seleccionado

    constructor(
        public miembro: Miembro,
        public miembroService: MiembroService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService
    ) { }

    ngOnInit() {
        this.obtenerReferidos();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerReferidos();
    }

    /**
     * Obtener los referidos de este miembro
     */
    public obtenerReferidos() {
        this.keycloakService.getToken().catch((error) => { this.manejaError(error); this.cargandoDatos = false; }).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.obtenerReferals(this.miembro.idMiembro, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaReferidos = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaReferidos.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    }
                )
            });
    }

    //Redirecciona a los detalles del miembro
    public detalle(idMiembro){
        let link = ['/miembros/detalleMiembro', idMiembro];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}


