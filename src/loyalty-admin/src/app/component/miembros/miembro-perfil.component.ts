import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Miembro, Pais, } from '../../model/index';
import { MiembroService, PaisService, KeycloakService } from '../../service/index';
import { PERM_ESCR_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'miembro-perfil-component',
    templateUrl: 'miembro-perfil.component.html',
    providers: [MiembroService, Pais, PaisService]
})

/**
 * Jaylin Centeno
 * Componente que muestra el perfil de un miembro */
/*-------------------------------------------------------------*/
export class MiembroPerfilComponent implements OnInit, AfterViewInit {
    
    public listaPaises: Pais[]; //Lista de los países
    public vacio: boolean = false; //Permite el manejo de las suscripciones del cliente
    //Fechas
    public fecNac: Date;
    public fecExpira: Date = null;
    public fecSuspencion: Date = null;
    
    public escritura: boolean;//Permite verificar permisos de escritura (true/false)
    public cargandoDatos: boolean = true;

    constructor(
        public miembro: Miembro,
        public miembroService: MiembroService,
        public pais: Pais,
        public paisService: PaisService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService
    ) {

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    ngOnInit() {
        this.obtenerPaises();
    }

    ngAfterViewInit(){
        this.cargandoDatos = false;
    }

    //Método que redirige a la pantalla de editar miembro
    editar() {
        let link = ['/miembros/editarMiembro', this.miembro.idMiembro];
        this.router.navigate(link);
    }

    //Método para obtener la lista de países
    obtenerPaises() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.paisService.token = token || "";
                this.paisService.obtenerPaises().subscribe(
                    (data: Pais[]) => { this.listaPaises = data; },
                    (error) => this.manejaError(error)
                );
            });
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}


