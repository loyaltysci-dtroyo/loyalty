import { Component } from '@angular/core';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { AtributoDinamico } from '../../model/index';
import { ImportacionService, AtributoDinamicoService, KeycloakService, I18nService } from '../../service/index';
import * as Routes from '../../utils/rutas';
import { PERM_ESCR_MIEMBROS, PERM_ESCR_ATRIBUTOS, PERM_LECT_ATRIBUTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { SelectItem } from 'primeng/primeng';

@Component({
    moduleId: module.id,
    selector: 'miembro-importacion-component',
    templateUrl: 'miembro-importacion.component.html',
    providers: [ImportacionService, AtributoDinamico, AtributoDinamicoService],
})

/**
 * Jaylin Centeno
 * Importación de miembros y atributos dinámicos de miembros
 */
export class MiembroImportacionComponent {

    public escrituraMiembro: boolean; //Permite verificar permiso de escritura (true/false)
    public escrituraAtributo: boolean; //Permite verificar permiso de escritura (true/false)
    public lecturaAtributo: boolean; //Permite verificar permiso de lectura (true/false)
    public listaBase64: string; //Contiene el base64 del archivo subido
    public nombreArchivo: string = ""; //Para mostrar el nombre del archivo seleccionado
    public miembro: boolean; //Aplicar el color a la ruta
    public atributo: boolean; //Aplicar el color a la ruta
    public ayuda: boolean; //Aplicar el color a la ruta

    //Importacion de atributos
    public tipo: string = "E"; //Guarda el tipo de atributo E = estatico, D = dinamico
    public idAtributo: string; //Guarda el id del atributo seleccionado 
    public identificacion: string = "idMiembro"; //Guarda el tipo de identif. por el que se va a reconocer el miembro  
    public idItems: SelectItem[]; //Lista de los tipos de documento (uuid - doc. identif.)
    public tipoAtributoItem: SelectItem[]; //Lista con los tipos de atributo (estatico - dinámico)
    public atributosEstaticos: SelectItem[]; //Lista con los atributos estaticos del miembro
    public atributosDinamicos: SelectItem[] = []; // Lista con los atributos dinámicos del miembro
    public cantidad: number = 10; //Cantidad de rows
    public totalRecords: number; //Total de premios en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    constructor(
        public atributoService: AtributoDinamicoService,
        public importacionService: ImportacionService,
        public router: Router,
        public authGuard: AuthGuard,
        public i18nService: I18nService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escrituraMiembro = permiso;
        });
        this.authGuard.canWrite(PERM_ESCR_ATRIBUTOS).subscribe((permiso: boolean) => {
            this.escrituraAtributo = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_ATRIBUTOS).subscribe((permiso: boolean) => {
            this.lecturaAtributo = permiso;
        });

        this.miembro = true;
        this.idItems = [];
        this.tipoAtributoItem = [];
        //Se obtienen los valores predeterminados por el sismtea
        this.idItems = this.i18nService.getLabels("id-miembro-importacion");
        this.tipoAtributoItem = this.i18nService.getLabels("atributo-tipo");
        this.atributosEstaticos = this.i18nService.getLabels("atributos-estaticos");
    }

    //Es llamado cuando existe un cambio entre importación de miembros y atributos
    cambioArchivo() {
        if (!this.atributo) {
            //Reset de los atributos
            this.nombreArchivo = '';
            this.listaBase64 = null;
        }
    }

    //Se encarga de realizar la importación de los miembros subidos en el documento csv
    importarMiembros() {
        if (this.escrituraMiembro) {
            if (this.listaBase64 != "" && this.listaBase64 != undefined) {
                this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                    (token: string) => {
                        this.importacionService.token = token;
                        this.importacionService.importarMiembros(this.listaBase64).subscribe(
                            (data) => {
                                this.msgService.showMessage(this.i18nService.getLabels('procesando-importacion'));
                                this.listaBase64 = "";
                                this.nombreArchivo = "";
                            },
                            (error) => this.manejarError(error)
                        );
                    }
                );
            } else {
                this.msgService.showMessage(this.i18nService.getLabels('warn-cantidad'));
            }
        }
    }

    //Se encarga de la importación de los atributos de los miembros que venían en el archivo csv
    importarAtributos() {
        if (this.escrituraAtributo && this.escrituraMiembro) {
            if (this.listaBase64 != "" && this.listaBase64 != undefined) {
                this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                    (token: string) => {
                        this.importacionService.token = token;
                        this.importacionService.importarInfoMiembros(this.identificacion, this.idAtributo, this.tipo, this.listaBase64).subscribe(
                            (data) => {
                                this.msgService.showMessage(this.i18nService.getLabels('procesando-importacion'));
                                this.listaBase64 = "";
                                this.nombreArchivo = "";
                            },
                            (error) => this.manejarError(error)
                        );
                    }
                );
            } else {
                this.msgService.showMessage(this.i18nService.getLabels('warn-cantidad'));
            }
        }

    }

    //Se obtiene el archivo csv subido por el usuario y se convierte en base64
    subirArchivo() {
        let archivoCsv: any;
        let nuevoCsv: any;
        let urlBase64: string[];
        archivoCsv = document.getElementById("archivo"); //Se obtiene el archivo subido por el usuario
        this.nombreArchivo = archivoCsv.files[0].name;
        let reader = new FileReader();
        reader.readAsDataURL(archivoCsv.files[0]);
        reader.addEventListener("load", (event) => {
            let archivo = reader.result;
            urlBase64 = archivo.split(",");
            this.listaBase64 = urlBase64[1];
        }, false);
    }

    //Es utilizado cuando se cambia el tipo de atributo a recibir
    cambioAtributo() {
        if (this.tipo == "D") {
            this.atributosDinamicos = [];
            this.busquedaAtributos();
        }
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaAtributos();
    }

    /* *Metodo que obtiene los atributos dinamicos para que sean seleccionados */
    busquedaAtributos() {
        if (this.lecturaAtributo) {
            let estado = ['P'];
            this.keycloakService.getToken().then(
                (tkn: string) => {
                    this.atributoService.token = tkn;
                    let sub = this.atributoService.busquedaAtributosDinamicos(this.cantidad, this.filaDesplazamiento, estado).subscribe(
                        (response: Response) => {
                            let listaAtributos = response.json();
                            if (response.headers.get("Content-Range") != null) {
                                this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                            }
                            listaAtributos.forEach(element => {
                                this.atributosDinamicos = [...this.atributosDinamicos, { value: element.idAtributo, label: element.nombre }];
                            })
                        },
                        (error) => this.manejarError(error),
                        () => {
                            sub.unsubscribe();
                        }
                    );
                });
        }
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejarError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
