import { ErrorHandlerService, MessagesService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { ActividadService, KeycloakService, I18nService } from '../../service/index';
import { Miembro, RegistroActividad } from '../../model/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { PERM_LECT_METRICAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'miembro-acctividad-metrica',
    templateUrl: 'miembro-actividad-metrica.component.html',
    providers: [ActividadService]
})
export class MiembroActividadMetricaComponent implements OnInit {
    public listaActividades: any[];

    /**Atributos para sorting/busqueda */
    public indPeriodo: string = '1M';
    public periodos: SelectItem[] = [];
    public lecturaMetrica: boolean; //Permiso de lectura sobre metrrica

    constructor(
        public miembro: Miembro,
        public actividadService: ActividadService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public route: ActivatedRoute,
        public router: Router,
        public i18nService: I18nService,
        public authGuard: AuthGuard
    ) {
        //Permite saber si el usuario tiene permisos de lectura sobre miembro
        this.authGuard.canWrite(PERM_LECT_METRICAS).subscribe((permiso: boolean) => {
            this.lecturaMetrica = permiso;
        });
    }

    ngOnInit() {
        this.periodos = this.i18nService.getLabels("general-actividad-periodos");
        this.route.parent.params.subscribe((params) => {
            if (params['id']) {
                this.miembro.idMiembro = params['id'];
                this.obtenerActividad();
            } else {
                // this.router.navigate(['/miembros']);
            }
        });
    }

    /**
     * Obtiene los registros de actividad para el miembro actual
     */
    obtenerActividad() {
        this.kc.getToken().then((tkn) => {
            this.actividadService.token = tkn;
            let sub = this.actividadService.getActividadesMetricaPorMiembro(this.miembro.idMiembro, this.indPeriodo).subscribe(
                (response: RegistroActividad[]) => {
                    this.listaActividades = response;
                }, (response: Response) => {
                    this.handleError(response);
                }, () => {
                    sub.unsubscribe();
                }
            );
        }).catch()

    }
    /**
     * Redirige al administrador a la pantalla de dashboard para el elemento seleccionado
     */
    gotoDetail(idElemento: string) {
        this.router.navigate(['/metricas/metricaDetalle', idElemento]);
    }
    /**
     * Maneja el erro a nivel de componente
     */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}