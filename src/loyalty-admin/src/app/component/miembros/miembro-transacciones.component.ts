import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Miembro, Ubicacion, Premio, Producto, TransaccionCompra } from '../../model/index';
import { Response } from '@angular/http';
import { MiembroService, KeycloakService } from '../../service/index';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'miembro-transacciones-component',
    templateUrl: 'miembro-transacciones.component.html',
    providers: [MiembroService]
})

/* Componente que muestra las transacciones de un miembro*/
export class MiembroTransaccionesComponent implements OnInit {

    public escritura: boolean; //Permite verificar permisos de escritura (true/false)
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public detalle: TransaccionCompra;
    
    //Representa la compra de un premio
    public compraPremio: {
        idTransaccion: string,
        idPremio: any,
        codigoCertificado: string,
        valorEfectivo: number,
        transaccionCompra:any
    }

    //Representa la compra de un producto
    public compraProducto: {
        cantidad: number,
        valorUnitario: number,
        total: number,
        producto: any,
        transaccionCompra: any
    }

    //Representa la transacción, incluye una compra de premio/producto
    public transaccion: {
        idCompra: string,
        fecha: Date,
        subtotal: number,
        impuesto: number,
        total: number,
        miembro: any,
        ubicacion: any,
        compraPremio: any,
        compraProductos: any
    };

    //Miembros
    public listaTransacciones: TransaccionCompra[];
    public listaVacia: boolean; //Para verificar si la lista esta vacia
    public cantidad: number = 10; //Cantidad de filas en la tabla
    public totalRecords: number; //Total de referidos
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public idTransaccion: string; //Id del atributo seleccionado

    constructor(
        public miembro: Miembro,
        public miembroService: MiembroService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService
    ) { }

    ngOnInit() {
        this.obtenerTransacciones();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerTransacciones();
    }

    //Recibe el detalle seleccionado de la lista
    detalleSeleccionado(anytype: any) {
        this.detalle = anytype;
    }

    /**
     * Obtiene las transacciones de un miembro
     */
    public obtenerTransacciones() {
    this.keycloakService.getToken().catch((error) => {this.manejaError(error); this.cargandoDatos = false;}).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.obtenerTransacciones(this.miembro.idMiembro, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaTransacciones = response.json();
                         if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaTransacciones.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    }
                )
        });
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}


