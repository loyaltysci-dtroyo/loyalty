import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Miembro, MiembroNivel, Nivel, Metrica, DetalleMiembroNivel } from '../../model/index';
import { MiembroService, MiembroNivelService, NivelService, MetricaService, KeycloakService, I18nService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { PERM_ESCR_MIEMBROS, PERM_LECT_METRICAS, PERM_ESCR_METRICAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { Response, Headers } from '@angular/http';

@Component({
    moduleId: module.id,
    selector: 'miembro-nivel-component',
    templateUrl: 'miembro-nivel.component.html',
    providers: [Nivel, Metrica, MiembroService, MiembroNivel, NivelService,
        MetricaService, MiembroNivelService, DetalleMiembroNivel]
})

/**
 * Jaylin Centeno
 * Componente que permite manejar niveles según el miembro*/
export class MiembroNivelComponent implements OnInit {

    public escritura: boolean; //Escritura sobre miembro, usado en asignar y remover nivel (true/false)
    public escrituraMetrica: boolean; //Escritura sobre metrica, usado en asignar y remover nivel(true/false)
    public lecturaMetrica: boolean;  //Lectura sobre metrica
    public asignar: boolean = false; //Permite manejar la asignacion de los nivles, muestra/oculta la info
    public eliminar: boolean; //Habilita el cuadro de dialogo para remover una asignación
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos de las metricas relacionadas
    public cargandoNivel: boolean = false; //Utilizado para mostar la carga de datos en niveles

    public metricaMiembro: Metrica[] = []; // Lista de las metricas con las que el miembro esta asociados
    public listaVacia: boolean = true; //Permite verificar si la lista de metricaMiembro esta vacia
    public totalRecords: number; //Total de metricas relacionadas
    public cantidad: number = 10; //Cantidad de filas a mostrar
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    //--- Metrica ----//
    public metrica: Metrica;
    public listaMetricas: Metrica[]; //Lista de las métricas disponibles
    public listaBusqueda: string[] = [];
    public totalRecordsMetrica: number;
    public nombreMetrica: string;
    public displayDialogMetrica: boolean;
    public metricaVacio: boolean; //Maneja si las metricas estan vacías
    public metricaItems: SelectItem[]; //Contienen los valores de métrica para desplegar en el dropdown
    public cantidadMetrica: number = 50; //Cantidad de filas a mostrar
    public filaMetrica: number = 0; //Desplazamiento de la primera fila
    public idMetrica: string; //Id de la metrica a remover

    public nivelesDisponibles: Nivel[];//Lista de los niveles de una métrica seleccionada
    public nivelVacio: boolean = true; //Permite verificar si la lista de niveles esta vacia
    public nivelItems: SelectItem[]; //Contienen los valores de niveles para desplegar en el dropdown
    public idNivel: string; //Id del nivel seleccionado
    public metricaBusqueda;
    public totalRecordsMetricas;

    constructor(
        public miembro: Miembro,
        public metricaSeleccionada: Metrica,
        public nivelMiembro: DetalleMiembroNivel,
        public miembroService: MiembroService,
        public miembroNivelService: MiembroNivelService,
        public metricaService: MetricaService,
        public nivelService: NivelService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_ESCR_METRICAS).subscribe((permiso: boolean) => {
            this.escrituraMetrica = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_METRICAS).subscribe((permiso: boolean) => {
            this.lecturaMetrica = permiso;
            this.obtenerMetricas();
        });

    }

    ngOnInit() {
        this.obtenerMiembroNiveles();
    }

    //Método para mostrar el dialogo de Información
    confirmar(idMetrica: string) {
        this.idMetrica = idMetrica;
        this.eliminar = true;
    }
    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerMiembroNiveles();
    }

    //Permite manejar el cambio de métrica y hacer llamado al metodo para obtener niveles
    cambioMetrica() {
        this.cargandoNivel = true;
        this.obtenerNiveles();
    }

    //Permite cartar las metricas y mostrar el cuadro de dialogo para asignar las métricas
    asignarNivel() {
        this.obtenerMetricas();
        this.asignar = true;
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadDataMetrica(event: any) {
        this.filaMetrica = event.first;
        this.cantidadMetrica = event.rows;
        this.obtenerMetricas();
    }

    //Cuando se hace la seleccion de metrica
    seleccionarMetrica(metrica: Metrica) {
        this.cambioMetrica()
        this.metrica = metrica;
        this.nombreMetrica = metrica.nombre;
        this.displayDialogMetrica = false;
    }

    //Metodo que se encarga de traer la lista de los niveles asociados al miembro 
    obtenerMiembroNiveles() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.miembroNivelService.token = token || "";
                this.miembroNivelService.obtenerNivelesMiembro(this.miembro.idMiembro, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.metricaMiembro = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        let total = this.metricaMiembro.length;
                        if (this.metricaMiembro.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }


    //Método para asociar un nivel al miembro
    asociarNivel() {
        if (this.escrituraMetrica && this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.miembroNivelService.token = token || "";
                    this.miembroNivelService.asignarNivelMiembro(this.miembro.idMiembro, this.metrica.idMetrica, this.idNivel).subscribe(
                        (response: Response) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                            this.obtenerMiembroNiveles();
                            this.actualizarListaMetricas();
                            this.asignar = false;
                        },
                        (error) => {
                            this.manejaError(error);
                        }
                    );
                }
            );
        }
    }

    //Método para remover de la lista de métricas las métricas que ya fueron seleccionadas
    actualizarListaMetricas() {
        let listaTemporal = this.listaMetricas;
        let posicion;
        if (this.listaMetricas.length > 0) {
            for (let x in listaTemporal) {
                if (listaTemporal[x].idMetrica == this.metrica.idMetrica) {
                    posicion = parseInt(x);
                    this.listaMetricas.splice(posicion, 1);
                    break;
                }
            }
        }
    }

    //Método para remover una relación con el nivel 
    removerNivel() {
        if (this.escrituraMetrica && this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.miembroNivelService.token = token || "";
                    this.miembroNivelService.desasignarNivelMiembro(this.miembro.idMiembro, this.idMetrica).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-exclusion-simple'));
                            this.actualizarListaNiveles();
                            this.obtenerMiembroNiveles();
                            this.eliminar = false;
                        },
                        (error) => { this.manejaError(error) }
                    );
                }
            );
        }
    }

    //Método para sacar de la lista de los niveles que no se relacionan con la metrica
    actualizarListaNiveles() {
        if (this.metricaMiembro.length > 0) {
            let index = this.metricaMiembro.findIndex(element => element.idMetrica == this.idMetrica);
            this.metricaMiembro.splice(index, 1);
        }
    }

    //Método que obtienen la lista de todas las metricas
    obtenerMetricas() {
        if (this.lecturaMetrica) {
            let busqueda = "";
            busqueda = this.listaBusqueda.join(" ")
            let estado = ["P"];
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.metricaService.token = token || "";
                    this.metricaService.buscarMetricas(estado, this.cantidadMetrica, this.filaMetrica).subscribe(
                        (response: Response) => {
                            this.listaMetricas = response.json();
                            if (response.headers.get("Content-Range") != null) {
                                this.totalRecordsMetrica = + response.headers.get("Content-Range").split("/")[1];
                            }
                            if (this.listaMetricas.length > 0) {
                                this.metricaVacio = false;
                            } else {
                                this.metricaVacio = true;
                            }
                            this.cargandoDatos = false;
                        },
                        (error) => this.manejaError(error)
                    );
                }
            );
        }
    }


    //Método que obtiene la lista de todos los niveles asociados a la metrica seleccionada
    obtenerNiveles() {
        if (this.lecturaMetrica) {
            this.nivelItems = [];
            this.nivelItems = [...this.nivelItems, this.i18nService.getLabels('general-default-select')];
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.nivelService.token = token || "";
                    this.nivelService.obtenerListaNiveles(this.metrica.grupoNiveles.idGrupoNivel).subscribe(
                        (data) => {
                            this.nivelesDisponibles = data;
                            if (this.nivelesDisponibles.length > 0) {
                                this.nivelVacio = false;
                                this.nivelesDisponibles.forEach(nivel => {
                                    this.nivelItems = [...this.nivelItems, { label: nivel.nombre, value: nivel.idNivel }];
                                });
                            } else {
                                this.nivelVacio = true;
                            }
                            this.cargandoNivel = false;
                        },
                        (error) => this.manejaError(error)
                    );
                }
            );
        }
    }

    //Redirecciona al detalle de la metrica
    gotoMetrica(idMetrica: string) {
        let link = ['metricas/metricaDetalle/', idMetrica];
        this.router.navigate(link);
    }

    //Redirecciona al detalle del miembro
    goBack() {
        let link = ['miembros/detalleMiembro/', this.miembro.idMiembro];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
