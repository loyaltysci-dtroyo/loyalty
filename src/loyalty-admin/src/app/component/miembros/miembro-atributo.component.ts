import { Component, OnInit } from '@angular/core';
import { AtributoDinamico, Miembro } from '../../model/index';
import { AtributoDinamicoService, MiembroService, AtributoMiembroService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { PERM_ESCR_MIEMBROS, PERM_LECT_ATRIBUTOS, PERM_ESCR_ATRIBUTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { SelectItem } from 'primeng/primeng';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'miembro-atributo-component',
    templateUrl: 'miembro-atributo.component.html',
    providers: [AtributoDinamico, AtributoDinamicoService, AtributoMiembroService, MiembroService]
})

/**
 * Jaylin Centeno
 * Componente para ver y editar los atributos dinámicos del miembro*/
export class MiembroAtributoComponent implements OnInit {

    public escritura: boolean; //Permite verificar los permisos de escritura(true/false)
    public lecturaAtributo: boolean; //Permite verificar los permisos de lectura de atributos(true/false)
    public escrituraAtributo: boolean; //Permite verificar los permisos de escritura de atributos(true/false)
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public asignar: boolean = false; //Habilita el cuadro de dialogo para asignar un atributo

    public listaAtributosMiembro: AtributoDinamico[]; //Lista con los atributos relacionados
    public atributoVacia: boolean; //Permite verificar si la lista de atributos esta vacía
    public totalRecords: number; //Total de premios en el sistema
    public cantidad: number = 10; //Cantidad de filas
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    public numero: number = 0; //Guarda el valor númerico del atributo
    public texto: string = ""; //Guarda el valor de texto del atributo
    public fecha: Date; //Guarda el valor tipo fecha del atributo

    public estado: string[]; //Lista con los tipos de estado del atributo
    public boolItem: SelectItem[]; //Items true y false para el dropdown
    public tipoNull: boolean = false; //Permite verificar si el miembro a modificado el atributo
    public fechaTimestamp: number;

    constructor(
        public atributo: AtributoDinamico,
        public miembro: Miembro,
        public miembroService: MiembroService,
        public atributoService: AtributoDinamicoService,
        public atributoMiembroService: AtributoMiembroService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_ATRIBUTOS).subscribe((permiso: boolean) => {
            this.lecturaAtributo = permiso;
            this.obtenerAtributosMiembro();
        });
        this.authGuard.canWrite(PERM_ESCR_ATRIBUTOS).subscribe((permiso: boolean) => {
            this.escrituraAtributo = permiso;
        });

        this.estado = ['P']; //Por default se usa el estado publico
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.boolItem = [];
        this.boolItem = this.i18nService.getLabels("booleano-general");
        this.obtenerAtributosMiembro();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerAtributosMiembro();
    }

    //Es utilizado para comprobar el tipo de valor del miembro
    tipoValor(tipo: string) {
        if (tipo == null) {
            this.tipoNull = true;
        } else {
            this.tipoNull = false;
        }
    }

    //Método para guardar el valor del atributo seleccionado
    atributoSeleccionado(atributo: AtributoDinamico) {
        this.tipoValor(atributo.valorAtributoMiembro);
        this.atributo = atributo;
        //Si no es nulo, se le asignan los valores correspondientes a las propiedades a utilizar
        if (!this.tipoNull) {
            if (this.atributo.indTipoDato == 'F') {
                if (this.atributo.valorAtributoMiembro.includes(' ') == false) {
                    this.fechaTimestamp = parseInt(this.atributo.valorAtributoMiembro, 10);
                    this.fecha = new Date(this.fechaTimestamp);
                } else {
                    this.fecha = new Date(this.atributo.valorAtributoMiembro);
                }
            }
            if (this.atributo.indTipoDato == 'N') {
                this.numero = parseInt(this.atributo.valorAtributoMiembro);
            }
            if (this.atributo.indTipoDato == 'B' || this.atributo.indTipoDato == 'T') {
                this.texto = this.atributo.valorAtributoMiembro;
            }
        } else {
            //Si se le asignan los valores de valor por defecto a las propiedades a utilizar
            if (this.atributo.indTipoDato == 'F') {
                if (this.atributo.valorDefecto.includes(' ') == false) {
                    this.fechaTimestamp = parseInt(this.atributo.valorDefecto, 10);
                    this.fecha = new Date(this.fechaTimestamp);
                } else {
                    this.fecha = new Date(this.atributo.valorDefecto);
                }
            }
            if (this.atributo.indTipoDato == 'N') {
                this.numero = parseInt(this.atributo.valorDefecto);
            }
            if (this.atributo.indTipoDato == 'B' || this.atributo.indTipoDato == 'T') {
                this.texto = this.atributo.valorDefecto;
            }
        }
        this.asignar = true;
    }

    /*Método que pergunta por el tipo
    En caso de ser null quiere decir que no existe relacion con el atributo,
    de lo contrario se actualiza la relación*/
    asignacion() {
        if (this.tipoNull) {
            this.asignarAtributoMiembro();
        } else {
            this.actualizarAtributoMiembro();
        }
    }

    //Método para obtener los atributos relacionados con el miembro
    obtenerAtributosMiembro() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.obtenerAtributosPorMiembro(this.miembro.idMiembro, this.cantidad, this.filaDesplazamiento, this.estado).subscribe(
                    (response: Response) => {
                        //Se obtiene la lista de las categorías del premio
                        this.listaAtributosMiembro = response.json();
                        //Se recibe el total de las categorías disponibles
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaAtributosMiembro.length > 0) {
                            this.atributoVacia = false;
                        }
                        else {
                            this.atributoVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejarError(error)
                );
            });
    }


    //Método que permite asignar una atributo dinámico a un miembro
    asignarAtributoMiembro() {
        if (this.escritura && this.escrituraAtributo) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token) => {
                    this.atributoMiembroService.token = token || "";
                    if (this.atributo.indTipoDato == 'F') {
                        this.atributo.valorAtributoMiembro = this.fecha.getTime() + '';
                    }
                    if (this.atributo.indTipoDato == 'N') {
                        this.atributo.valorAtributoMiembro = this.numero + '';
                    }
                    if (this.atributo.indTipoDato == 'B' || this.atributo.indTipoDato == 'T') {
                        this.atributo.valorAtributoMiembro = this.texto;
                    }
                    this.atributoMiembroService.asignarAtributo(this.miembro.idMiembro, this.atributo).subscribe(
                        (data) => {
                            this.obtenerAtributosMiembro();
                            this.asignar = false;
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
        }
    }

    //Método que permite asignar una atributo dinámico a un miembro
    actualizarAtributoMiembro() {
        if (this.escritura && this.escrituraAtributo) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token) => {
                    this.atributoMiembroService.token = token || "";
                    if (this.atributo.indTipoDato == 'F') {
                        this.atributo.valorAtributoMiembro = this.fecha.getTime() + '';
                    }
                    if (this.atributo.indTipoDato == 'N') {
                        this.atributo.valorAtributoMiembro = this.numero + '';
                    }
                    if (this.atributo.indTipoDato == 'B' || this.atributo.indTipoDato == 'T') {
                        this.atributo.valorAtributoMiembro = this.texto;
                    }
                    this.atributoMiembroService.editarAtributo(this.miembro.idMiembro, this.atributo).subscribe(
                        (data) => {
                            this.obtenerAtributosMiembro();
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                            this.asignar = false;
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
        }
    }

    //Redirecciona al detalle del atributo
    gotoAtributo(idAtributo: string) {
        let link = ['atributosDinamicos/detalleAtributo/', idAtributo];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}