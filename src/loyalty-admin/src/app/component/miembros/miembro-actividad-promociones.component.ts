import { ErrorHandlerService, MessagesService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { ActividadService, KeycloakService, I18nService } from '../../service/index';
import { Miembro, RegistroActividad } from '../../model/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { PERM_LECT_PROMOCIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import * as Routes from '../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'miembro-actividad-promociones',
    templateUrl: 'miembro-actividad-promociones.component.html',
    providers: [ActividadService]
})

/**
 * Componente que muestra las actividades de promociones en las que ha participado el miembro
 */
export class MiembroActividadPromocionesComponent implements OnInit {

    public lecturaPromo: boolean; //Permite verificar los permisos de lectura de atributos(true/false)
    public listaActividades: any[];
    /**Atributos para sorting/busqueda */
    public indPeriodo: string = '1M';
    public periodos: SelectItem[] = [];

    constructor(
        public miembro: Miembro,
        public actividadService: ActividadService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public i18nService: I18nService,
        public router: Router
    ) {
        //Consulta los permisos de lectura sobre promociones
        this.authGuard.canWrite(PERM_LECT_PROMOCIONES).subscribe((permiso: boolean) => {
            this.lecturaPromo = permiso;
        });

    }

    //Es llamado al inicializarce el componente
    ngOnInit() {
        //Obtiene los períodos por default del sistema
        this.periodos = this.i18nService.getLabels("general-actividad-periodos");
        this.route.parent.params.subscribe((params) => {
            if (params['id']) {
                this.miembro.idMiembro = params['id'];
                this.obtenerActividad();
            } 
        });
    }

    /**
     * Obtiene los registros de actividad para el miembro actual
     */
    obtenerActividad() {
        if (this.lecturaPromo) {
            this.kc.getToken().then((tkn) => {
                this.actividadService.token = tkn;
                let sub = this.actividadService.getActividadesPromocionPorMiembro(this.miembro.idMiembro, this.indPeriodo).subscribe(
                    (response: RegistroActividad[]) => {
                        this.listaActividades = response;
                    }, (response: Response) => {
                        this.handleError(response);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            }).catch()
        }
    }
    /**
     * Redirige al administrador a la pantalla de dashboard para el elemento seleccionado
     */
    gotoDetail(idElemento: string) {
        this.router.navigate([Routes.RT_PROMOCIONES_DETALLE_PROMOCION, idElemento]);
    }
    /**
     * Maneja el erro a nivel de componente
     */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}