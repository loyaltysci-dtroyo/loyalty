import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Miembro, Pais } from '../../model/index';
import { MiembroService, PaisService, KeycloakService, I18nService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { PERM_ESCR_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'miembro-editar-component',
    templateUrl: 'miembro-editar.component.html',
    providers: [Miembro, MiembroService, Pais, PaisService]
})

/**
 * Jaylin Centeno
 * Componente que permite la edición de los miembros */
/*-------------------------------------------------------------*/
export class MiembroEditarComponent implements OnInit {

    //Id del miembro recibido en la url
    public id: string;
    //Lista de los países
    public listaPaises: Pais[];
    //Listas para Dropdown//
    public estadoCivilItems: SelectItem[];
    public generoItems: SelectItem[];
    public educacionItems: SelectItem[];
    public paisItems: SelectItem[];
    // Form group para validar el formulario de miembro
    public formMiembro: FormGroup;
    //Contendra el archivo subido por el usuario
    public imagen: any;
    //Guarda la dirección de la imagen
    public avatar: any;
    //Se guarda el array de la imagen
    public urlBase64: string[];
    //Permite verificar permisos (true/false)
    public escritura: boolean;
    //Rango de años
    public rango: string;
    //Fechas
    public fecNac: Date;
    public fecExpira: Date = null;
    public fecSuspencion: Date = null;
    public subiendoImagen: boolean = false;
    public confirmar: boolean;
    public correoInvalido: boolean;

    constructor(
        public miembro: Miembro,
        public miembroService: MiembroService,
        public pais: Pais,
        public paisService: PaisService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //El id de la metrica se inicializa con el valor introducido por la url ---- public router: Router,
        this.route.params.forEach((params: Params) => { this.miembro.idMiembro = params['id'] });

        //Validacion del formulario y datos por defecto
        this.formMiembro = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'apellido': new FormControl('', Validators.required),
            'apellido2': new FormControl(''),
            'identificacion': new FormControl('', Validators.required),
            'correo': new FormControl('', Validators.required),
            'contrasena': new FormControl('')
        });

        //Se obtienen los items definidos para el sistema
        this.estadoCivilItems = [];
        this.estadoCivilItems = this.i18nService.getLabels("miembro-estado-civil");

        this.generoItems = [];
        this.generoItems = this.i18nService.getLabels("miembro-genero");

        this.educacionItems = [];
        this.educacionItems = this.i18nService.getLabels("miembro-educacion");

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        let anno = new Date();
        let annoActual = anno.getFullYear() - 15;
        this.rango = (annoActual - 30) + ":" + annoActual;

    }

    ngOnInit() {
        this.mostrarDetalle();
        this.obtenerPaises();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    //Metodo que se encarga traer el detalle de la metrica
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.obtenerMiembroPorId(this.miembro.idMiembro).subscribe(
                    (data) => {
                        this.copyValuesOf(this.miembro, data);
                        this.avatar = this.miembro.avatar;
                        this.fecNac = new Date(this.miembro.fechaNacimiento);
                        this.fecExpira = new Date(this.miembro.fechaExpiracion);
                        this.fecSuspencion = new Date(this.miembro.fechaSuspension);
                    },
                    (error) => this.manejaError(error)
                );
            });
    }

    //Método para obtener la lista de países
    obtenerPaises() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.paisService.token = token || "";
                this.paisItems = [];
                this.paisItems=[...this.paisItems, this.i18nService.getLabels('general-default-select')];
                this.paisService.obtenerPaises().subscribe(
                    (data: Pais[]) => {
                        this.listaPaises = data;
                        if (this.listaPaises.length > 0) {
                            this.listaPaises.forEach(pais => {
                                this.paisItems=[...this.paisItems,{ label: pais.nombrePais, value: pais.alfa3 }];
                            });
                        }
                    },
                    (error) =>
                        this.manejaError(error)
                );
            });
    }

    //Metodo que se encarga de actualizar el miembro que se muestra en la vista
    actualizarMiembro() {
        if (this.correoInvalido) {
            this.msgService.showMessage(this.i18nService.getLabels('usuario-correo'));
            this.confirmar = false;
            return;
        }
        this.confirmar = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
                this.subiendoImagen = true;
                let sub = this.miembroService.editarMiembro(this.miembro).subscribe(
                    (data) => {
                        this.subiendoImagen = false;
                        this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                        this.mostrarDetalle();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.subiendoImagen = false;
                    },
                    () => { sub.unsubscribe(); this.subiendoImagen = false; }
                );
            }
        );
    }

    //Método encargado de validar la existencia del correo
    validarExistencia() {
        if (this.miembro.correo == null || this.miembro.correo == "") {
            this.correoInvalido = true;
            return;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.miembroService.token = token;
                let sub = this.miembroService.existenciaCorreo(this.miembro.correo.trim()).subscribe(
                    (data) => {
                        this.correoInvalido = data;
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe() });
            });
    }


    //Método para subir la imagen del avatar del cliente
    subirImagen() {
        //se obtiene el archivo subido
        this.imagen = document.getElementById("avatar");
        let reader = new FileReader();
        reader.addEventListener("load", (event: any) => {
            this.avatar = reader.result;
            this.urlBase64 = this.avatar.split(",");
            this.miembro.avatar = this.urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }


    // Redirecciona al detalle del miembro
    goBack() {
        let link = ['/miembros/detalleMiembro', this.miembro.idMiembro];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
