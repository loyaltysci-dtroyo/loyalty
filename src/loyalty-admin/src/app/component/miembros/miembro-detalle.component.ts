import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { Response } from '@angular/http';
import { Miembro, Configuracion, MiembroBalance } from '../../model/index';
import { MiembroService, ConfiguracionService, KeycloakService, I18nService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { PERM_ESCR_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import * as Rutas from '../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'miembro-detalle-component',
    templateUrl: 'miembro-detalle.component.html',
    providers: [Miembro, MiembroService, MiembroBalance, Configuracion, ConfiguracionService]
})

/**
 * Jaylin Centeno
 * Componente utilizado para mostrar el detalle de un miembro
 */
export class MiembroDetalleComponent implements OnInit {

    public escritura: boolean; //Permite verificar permisos de escritura (true/false)
    public suspender: boolean; //Usado para verificar la suspención
    public vacio: boolean = false; //Permite el manejo de las suscripciones del miembro// Estado del miembro
    public cargandoDatos: boolean = true; //Carga de datos
    public confirmar: boolean; //Habilita el cuadro de dialogo para confirmar la suspención
    public activo: boolean; //True = Activo False = Inactivo
    public inactivo: boolean; //True = Inactivo False = Activo
    public pregunta: boolean; //

    //Items del menú y sus respectivas rutas
    public perfil = Rutas.RT_MIEMBROS_DETALLE_PERFIL;
    public nivel = Rutas.RT_MIEMBROS_DETALLE_NIVEL;
    public atributo = Rutas.RT_MIEMBROS_DETALLE_ATRIBUTO;
    public grupo = Rutas.RT_MIEMBROS_DETALLE_GRUPO;
    public insignia = Rutas.RT_MIEMBROS_DETALLE_INSIGNIA;
    public preferencia = Rutas.RT_MIEMBROS_DETALLE_PREFERENCIA;
    public segmento = Rutas.RT_MIEMBROS_DETALLE_SEGMENTO;
    public misiones = Rutas.RT_MIEMBROS_DETALLE_MISIONES;
    public premios = Rutas.RT_MIEMBROS_DETALLE_PREMIOS;
    public rewards = Rutas.RT_MIEMBROS_DETALLE_AWARDS;
    public metrica = Rutas.RT_MIEMBROS_DETALLE_METRICA;
    public promociones = Rutas.RT_MIEMBROS_DETALLE_PROMOCIONES;
    public notificaciones = Rutas.RT_MIEMBROS_DETALLE_NOTIFICACIONES;
    public referals = Rutas.RT_MIEMBROS_DETALLE_REFERALS;
    public transacciones = Rutas.RT_MIEMBROS_DETALLE_TRANSACCION;
    public cuentas = Rutas.RT_MIEMBROS_DETALLE_CUENTAS;

    public idMetrica: string;
    constructor(
        public balance: MiembroBalance,
        public miembro: Miembro,
        public miembroService: MiembroService,
        public configuracion: Configuracion,
        public configuracionService: ConfiguracionService,
        public router: Router,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nservice: I18nService
    ) {
        //El id de la métrica se inicializa con el valor introducido por la url
        this.route.params.forEach((params: Params) => { this.miembro.idMiembro = params['id'] });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.pregunta = this.i18nservice.getLabels("confirmacion-pregunta")["D"];
    }
    ngOnInit() {
        this.mostrarDetalle();
        this.mostrarConfiguracion();
    }
    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
     * Obtiene el detalle del miembro por id
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.obtenerMiembroPorId(this.miembro.idMiembro).subscribe(
                    (data) => {
                        this.copyValuesOf(this.miembro, data);
                        if (!this.miembro.indContactoEmail && !this.miembro.indContactoNotificacion &&
                            !this.miembro.indContactoSms && !this.miembro.indContactoEstado) {
                            this.vacio = true;
                        } if (this.miembro.indEstadoMiembro == 'A') {
                            this.activo = true;
                        } else { this.inactivo = true; }
                        this.cargandoDatos = false;
                    },
                    (error) => { this.manejaError(error); this.cargandoDatos = false; }
                );
            });
    }

    /**
     * Método que se encarga de traer la configuración del sistema para obtener la información 
     * utilizada para cargar el balance de métrica del usuario
     */
    mostrarConfiguracion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.configuracionService.token = token;
                this.configuracionService.obtenerConfiguracion().subscribe(
                    (data) => {
                        this.configuracion = data;
                        if (this.configuracion.idMetricaInicial != null && this.configuracion.idMetricaInicial != undefined) {
                            this.idMetrica = this.configuracion.idMetricaInicial.idMetrica;
                            this.obtenerBalanceMetrica();
                        }
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }
    /**
     * Activa el miembro en caso de estar suspendido
     */
    activar() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.activarMiembro(this.miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nservice.getLabels("general-activar"));
                        this.activo = true;
                        this.inactivo = false;
                        this.mostrarDetalle();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.activo = false;
                        this.inactivo = true;
                    }
                );
            });
    }
    /**
     * Método utilizado para suspender a un miembro
     */
    suspenderMiembro() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.suspenderMiembro(this.miembro.idMiembro, this.miembro.causaSuspension).subscribe(
                    (data) => {
                        this.mostrarDetalle();
                        this.confirmar = false;
                        this.msgService.showMessage(this.i18nservice.getLabels("miembro-suspencion"));
                        this.inactivo = true;
                        this.activo = false;
                    },
                    (error) => { this.manejaError(error); this.confirmar = false;}
                );
            });
    }
    /**
     * Obtiene el balance de métrica principal del usuario: 
     * disponible - redimida - vencida - acumulada
     */
    obtenerBalanceMetrica() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token) => {
                this.miembroService.token = token || "";
                this.miembroService.obtenerBalanceMiembroMetrica(this.miembro.idMiembro, this.idMetrica).subscribe(
                    (response: Response) => {
                        this.balance = response.json();
                    },
                    (error) => {
                        this.manejaError(error)
                    }
                );
            });
    }
    /**
     * Método que redirige a la pantalla de editar miembro
     */
    editar() {
        this.router.navigate([Rutas.RT_MIEMBROS_EDITAR, this.miembro.idMiembro]);
    }
    /**
     *  Método que permite regresar en la navegación
     */
    goBack() {
       this.router.navigate([Rutas.RT_MIEMBROS_LISTA]);
    }
    /**
     *  Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
