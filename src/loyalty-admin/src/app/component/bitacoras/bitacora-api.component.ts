import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'bitacora-api-component',
    templateUrl: 'bitacora-api.component.html',
})
/**
 * Jaylin Centeno
 * Componente padre de bitacora
 */
export class BitacoraComponent{
    constructor(){}
}