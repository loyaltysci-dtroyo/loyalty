import { Component, OnInit } from '@angular/core';
import { AuthGuard } from '../common';
import { Router } from '@angular/router'
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { BITACORA } from '../common/auth.constants';
import { ErrorHandlerService, MessagesService } from '../../utils';
import { BitacoraService, KeycloakService, I18nService } from '../../service';

@Component({
    moduleId: module.id,
    selector: 'bitacora-api-administrativa',
    templateUrl: 'bitacora-api-administrativa.component.html',
    providers: [BitacoraService]
})
/**
 * Jaylin Centeno
 * Componente hijo, representa la bitacora del api Administrativo
 */
export class BitacoraApiAdministrativaComponent implements OnInit {

    // -- Api Administrativo -- //
    public listaAdmin: any[];
    public listaVacia: boolean; // True = lista vacia, False = lista con elementos
    public accion: string;
    public cantidad: number = 15; //Cantidad de filas
    public totalRecords: number; //Total de registros
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public permiso: boolean; //Permiso de lectura sobre bitacora
    public acccion: string; // Tipo de acción a escoger 
    public acciones: SelectItem[] = []; // Acciones disponibles
    public cargandoDatos: boolean = true;

    constructor(
        public router: Router,
        public authGuard: AuthGuard,
        public i18nService: I18nService,
        public bitacoraService: BitacoraService,
        public keycloakService: KeycloakService,
        public messageServices: MessagesService

    ) {
        //Se obtienen las acciones pre-definidas
        this.acciones = this.i18nService.getLabels("bitacora-accion");
    }

    ngOnInit() {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(BITACORA).subscribe((permiso: boolean) => {
            this.permiso = permiso;
            this.obtenerRegistroBitacora();
        });
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerRegistroBitacora();
    }

    //Obtiene el registro de la bitacora administrativa
    obtenerRegistroBitacora() {
        if (this.permiso) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.bitacoraService.token = token || "";
                    this.bitacoraService.obtenerBitacoraAdmin(this.accion, this.cantidad, this.filaDesplazamiento).subscribe(
                        (response: Response) => {
                            this.listaAdmin = response.json();
                            if (response.headers.get("Content-Range") != null) {
                                this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                            }
                            this.cargandoDatos = false;
                        },
                        (error) => { this.manejaError(error); this.cargandoDatos = false;}
                    )
                }
            )
        }
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejaError(error: any) {
        this.messageServices.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}