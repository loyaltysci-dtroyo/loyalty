import { Component, OnInit } from '@angular/core';
import { AuthGuard } from '../common';
import { Router } from '@angular/router'
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { BITACORA } from '../common/auth.constants';
import { BitacoraService, KeycloakService, I18nService } from '../../service';
import { ErrorHandlerService, MessagesService } from '../../utils';

@Component({
    moduleId: module.id,
    selector: 'bitacora-api-external',
    templateUrl: 'bitacora-api-external.component.html',
    providers: [BitacoraService]
})

/**
 * Jaylin Centeno
 * Componente hijo, representa la bitacora del api externo
 */
export class BitacoraApiExternalComponent implements OnInit {

    public bitacora: {
        fecha: Date,
        idEntrada: string,
        httpEstadoRespuesta: string,
        jsonRespuesta: any,
        jsonEntrada: any
    }

    public permiso: boolean; //Permiso de lectura sobre bitacora
    public indPeriodo: string = '1M';/**Atributos para sorting/busqueda */
    public periodos: SelectItem[] = [];
    public listaExternal: any[];
    public listaVaciaExternal: boolean; // True = lista vacia, False = lista con elementos
    public cargandoDatos: boolean = true;

    constructor(
        public router: Router,
        public authGuard: AuthGuard,
        public i18nService: I18nService,
        public bitacoraService: BitacoraService,
        public keycloakService: KeycloakService,
        public messageServices: MessagesService

    ) { }

    ngOnInit() {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(BITACORA).subscribe((permiso: boolean) => {
            this.permiso = permiso;
            this.obtenerRegistroBitacora();
        });
        this.periodos = this.i18nService.getLabels("general-actividad-periodos")
    }

    //Permite obtener los registro de la bitacora externa
    obtenerRegistroBitacora() {
        if (this.permiso) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.bitacoraService.token = token || "";
                    this.bitacoraService.obtenerBitacoraExternal(this.indPeriodo).subscribe(
                        (response: Response) => {
                            this.listaExternal = response.json();
                            this.cargandoDatos = false;
                        },
                        (error) => { this.manejaError(error); this.cargandoDatos = false; }
                    )
                }
            )
        }
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejaError(error: any) {
        this.messageServices.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}