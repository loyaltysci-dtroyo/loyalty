export { BitacoraComponent } from './bitacora-api.component';
export { BitacoraApiExternalComponent } from './bitacora-api-external.component';
export { BitacoraApiAdministrativaComponent } from './bitacora-api-administrativa.component';