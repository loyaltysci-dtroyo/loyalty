import { I18nService } from '../../../service';
import { ErrorHandlerService } from '../../../utils/index';
import * as Rutas from '../../../utils/rutas';
import { MessagesService } from '../../../utils/index'
import { QuienQuiereSerMillonarioService, PreguntaQuienQuiereSerMillonarioService } from '../../../service/index';
import { JuegoMillonario, PreguntaMillonario } from '../../../model/index';
import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { FormGroup } from '@angular/forms';
import { KeycloakService } from '../../../service/index';
import { PERM_ESCR_JUEGOS } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { Router } from '@angular/router';
@Component({
    moduleId: module.id,
    selector: 'quien-quiere-ser-millonario-detalle-component',
    templateUrl: 'quien-quiere-ser-millonario-detalle.component.html',
    providers: [JuegoMillonario, PreguntaMillonario, PreguntaQuienQuiereSerMillonarioService, QuienQuiereSerMillonarioService]
})
/**
 * Flecha Roja Technologies set-2016
 * Autor: Fernando Aguilar
 * Componente encargado de mostrar el menu y funcionar de padre para los componente de promocion
 * entre ellos insertar promocion, detalle de promocion y categorias de promocion asi como proveer
 * enrutamiento entre ellos, permite activar y desactivar promociones 
 */
export class QuienQuiereSerMillonarioDetalleComponent {
    public itemsCalendarizacion: SelectItem[];//items de menu para la calendarizacion de la promocion
    public form: FormGroup;//validaciones
    public rtCommon = Rutas.RT_JUEGO_MILLONARIO_DETALLE_COMMON;//rutas dinamicas
    public rtPreguntas = Rutas.RT_JUEGO_MILLONARIO_DETALLE_PREGUNTAS;//rutas dinamicas
    public isRecurrente: any;//flag de recurrencia de la promo true=recurrente
    public es: any; // locales para los calendar input en español
    public diasSeleccionados: string[];//dias seleccionados para recurrencia

    public minDate;//fecha minima a partir de la cual se permite al usuario definir la fecha inicial
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public displayModalEliminar: boolean = false;
    public displayModalEditar: boolean = false;
    public displayModalArchivar: boolean = false;

    public publicado: boolean;
    public borrador: boolean;
    public archivado: boolean;
    public inactivo: boolean;

    //Se guarda las fechas si es calendarizado
    public fecInicio: Date = null;
    public fecFin: Date = null;

    constructor(
        public juegoMillonario: JuegoMillonario,
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,//ruta actual
        public quienQuiereSerMillonarioService: QuienQuiereSerMillonarioService,
        public msgService: MessagesService,
        public router: Router,
        private i18nService: I18nService
    ) {
        this.diasSeleccionados = [];
        this.itemsCalendarizacion = [];
        this.route.params.forEach((params: Params) => { this.juegoMillonario.idJuego = params['id'] });
        //this.juegoMillonario.indSemanaRecurrencia = 1;
        this.itemsCalendarizacion = this.i18nService.getLabels('promociones-items-calendarizacion');
        this.minDate = new Date(); //fecha minima para la calendarizacion
        this.es = this.i18nService.getLabels("general-calendar");
        this.obtenerJuego();
        /*this.authGuard.canWrite(PERM_ESCR_JUEGOS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });*/

        this.tienePermisosEscritura = true;
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
     *Obtiene los datos de la promocion para desplegar el estado de la promocion y el nombr een la barra de encabezado
     */
    obtenerJuego() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.quienQuiereSerMillonarioService.token = tkn;
                let sub = this.quienQuiereSerMillonarioService.getJuego(this.juegoMillonario.idJuego).subscribe(
                    (data: JuegoMillonario) => {
                        this.copyValuesOf(this.juegoMillonario, data);
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); }
                )
            });
    }

    //Método que permite regresar en la navegación
    goBack() {
        let link = [Rutas.RT_JUEGO_MILLONARIO_LISTA];
        this.router.navigate(link);
    }

    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}