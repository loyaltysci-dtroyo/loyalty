import { I18nService } from '../../../service/i18n.service';
import * as Rutas from '../../../utils/rutas';

import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthGuard } from '../../common/index';
import { KeycloakService } from '../../../service/index';
import { PERM_ESCR_JUEGOS } from '../../common/auth.constants'
import { JuegoMillonario } from '../../../model/index';
import { QuienQuiereSerMillonarioService } from '../../../service/index';
import { SelectItem } from 'primeng/primeng';

/**
 * LoyaltySci
 * Autor: Kevin Ramírez
 * Componente encargado de mostrar los detalles de un tema, permite la edicion de los atributos generales del tema
 * si el usuario tiene permiso para escribir en este recurso
 */
@Component({
    moduleId: module.id,
    selector: 'quien-quiere-ser-millonario-detalle-common-component',
    templateUrl: 'quien-quiere-ser-millonario-detalle-common.component.html'
})
export class QuienQuiereSerMillonarioDetalleCommonComponent implements OnInit {
    public tags: string[];
    public form: FormGroup;//validaciones
    public itemsCodigo: SelectItem[] = [];//items de seleccion para 
    public nombreExiste: boolean;//true si el nombre interno de la promo es valido (unico)
    public tienePermisosEscritura: boolean = true;//true si el usuario tiene permisos para escrbir en este recurso del sistema

    constructor(
        public juegoMillonario: JuegoMillonario,
        private i18nService: I18nService,
        public quienQuiereSerMillonarioService: QuienQuiereSerMillonarioService,
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService) {
        this.form = new FormGroup({
            "nombre": new FormControl('', Validators.required),
            "descripcion": new FormControl('', Validators.required)
        });
        /*this.authGuard.canWrite(PERM_ESCR_JUEGOS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });*/
        this.tienePermisosEscritura = true;
    }
    ngOnInit() {
        this.obtenerJuego();//se obtienen los datos de la promocion 
    }

    /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Metodo encargado de guardar los detalles de la promocion en el sistema
     * Utiliza la instancia de promocion almacenada en this.promo
     */
    actualizarJuego() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.quienQuiereSerMillonarioService.token = tkn;
                let sub = this.quienQuiereSerMillonarioService.updateJuego(this.juegoMillonario).subscribe(
                    () => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.obtenerJuego();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Obtiene los detalles de la promocion en el sistema
     */
    obtenerJuego() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.quienQuiereSerMillonarioService.token = tkn;
                let sub = this.quienQuiereSerMillonarioService.getJuego(this.juegoMillonario.idJuego).subscribe(
                    (data: JuegoMillonario) => {
                        this.copyValuesOf(this.juegoMillonario, data);
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}