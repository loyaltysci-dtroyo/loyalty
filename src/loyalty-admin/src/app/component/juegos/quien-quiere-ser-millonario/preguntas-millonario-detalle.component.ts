import { Component, OnInit } from '@angular/core';
import { PreguntaMillonario } from '../../../model/index';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { KeycloakService, I18nService, PreguntaQuienQuiereSerMillonarioService } from '../../../service'
import { MessagesService, ErrorHandlerService } from "../../../utils/index";
import { SelectItem } from 'primeng/primeng';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as Routes from '../../../utils/rutas';
import { PERM_ESCR_JUEGOS } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { Response } from '@angular/http';
@Component({
    moduleId: module.id,
    selector: 'preguntas-millonario-detalle',
    templateUrl: 'preguntas-millonario-detalle.component.html',
    providers: [PreguntaMillonario, PreguntaQuienQuiereSerMillonarioService]
})
/**
 * Flecha Roja Technologies 24-oct-2016
 * Fernando Aguilar
 * Compoennte encargado de mostrar el detalle de leaderboards y de editar leaderboards
 */
export class PreguntasMillonarioDetalleComponent implements OnInit {
    public itemsPuntos: SelectItem[];//para el select de tipo de tabla
    public itemsRespuestas: SelectItem[];//para el select de tipo de tabla
    public form: FormGroup;//validacion y controles del form de editar leaderboard
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema

    constructor(
        public preguntaMillonario: PreguntaMillonario,
        public preguntaQuienQuiereSerMillonarioService: PreguntaQuienQuiereSerMillonarioService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public i18nService: I18nService
    ) {
        /**Verificacion de los permisos de escritura del usuario en sesion */
        /*this.authGuard.canWrite(PERM_ESCR_JUEGOS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = true;//result;
        });*/
        this.tienePermisosEscritura = true;

        this.route.params.forEach((params: Params) => { this.preguntaMillonario.idPregunta = params['id'] });
        
        this.itemsPuntos = this.i18nService.getLabels("preguntas-itemsPuntos");
        this.itemsRespuestas = this.i18nService.getLabels("preguntas-respuestas");
        this.form = new FormGroup({
            "pregunta": new FormControl("", Validators.required),
            "puntos": new FormControl("", Validators.required),
            "respuestaCorrecta": new FormControl(""),
            "respuesta1": new FormControl("", Validators.required),
            "respuesta2": new FormControl("", Validators.required),
            "respuesta3": new FormControl("", Validators.required),
            "respuesta4": new FormControl("", Validators.required)
        });
    }

    ngOnInit() {
        this.obtenerPregunta();//se obtienen los datos del leaderboard 
    }

        /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
   copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Obtiene el detalle de preguntas del juego
     * */
    obtenerPregunta() {
        this.kc.getToken().then((tkn: string) => {
            this.preguntaQuienQuiereSerMillonarioService.token = tkn;
            let sub = this.preguntaQuienQuiereSerMillonarioService.getPregunta(this.preguntaMillonario.idPregunta).subscribe(
                (data: PreguntaMillonario) => {
                    this.copyValuesOf(this.preguntaMillonario, data);
                },
                (error: any) => {
                    this.manejaError(error);
                },
                () => {
                    sub.unsubscribe();
                }
            );
        }).catch((error: any) => { this.manejaError(error) });
    }

    /**
     * Encargado de manejo de errores
     */
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    /**
     * Encargado de isnertar la leaderboard a traves del service que conecta con el API
     * Emplea la variable global llamada leaderboard para almacenar lso datos introducidos por el usuario
     *  */
    editarPregunta() {
        this.msgService.showMessage(this.i18nService.getLabels("general-actualizando-datos"));
        this.kc.getToken().then((tkn) => {
            this.preguntaQuienQuiereSerMillonarioService.token = tkn;
            let sub = this.preguntaQuienQuiereSerMillonarioService.updatePregunta(this.preguntaMillonario).subscribe(
                (data: PreguntaMillonario) => {
                    this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                    this.obtenerPregunta();
                },
                (error: any) => {
                    this.manejaError(error)
                },
                () => {
                    sub.unsubscribe();
                }
            );
                },
                (error: any) => this.manejaError(error)
            );
    }

    //Regresar a la lista
    goBack() {
        this.router.navigate([Routes.RT_JUEGO_MILLONARIO_DETALLE, this.preguntaMillonario.idJuego]);
    }
}