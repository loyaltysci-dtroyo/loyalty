import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { Router } from '@angular/router';
import { JuegoMillonario } from '../../../model/index';
import { QuienQuiereSerMillonarioService } from '../../../service/index';
import { MenuItem } from 'primeng/primeng';
import { Response } from '@angular/http';
import { KeycloakService } from '../../../service/index';
import * as Rutas from '../../../utils/rutas';
/**
 * Flecha Roja Technologies
 * Componente encargado de desplegar la lista de juegos y proveer controles de busqueda y paginacion 
 */

@Component({
    moduleId: module.id,
    selector: 'quien-quiere-ser-millonario-lista',
    templateUrl: 'quien-quiere-ser-millonario-lista.component.html',
    providers: [JuegoMillonario, QuienQuiereSerMillonarioService]
})
export class QuienQuiereSerMillonarioListaComponent implements OnInit {

    public listaJuegos: JuegoMillonario[];//lista de juegos 
    public promocionEliminar: JuegoMillonario;//variable para almacenar la mision a eliminar
    public terminosBusqueda: string[]=[];// termino de busqueda
    public displayModalEliminar: boolean = false;//true= se muestra el modal
    public displayModalMensaje: boolean = false;//true= se muestra el modal
    
    public displayModalConfirm: boolean;//se muestra el modal de confirmacion cuando este valor es true
    //public itemsBusqueda: MenuItem[] = [];//usado para poblar el split button de terminos avanzados de busqueda
    public busquedaAvanzada: boolean = false;//usado para desplegar el div de busqueda avanzada
    public rowOffset: number;
    public cantRegistros: number;//cantidad de registros que se va a solicitar al api
    public cantTotalRegistros: number;//cantidad total de registros en el sistema para los terminos de busqueda establecidos

    /**
     * Parametros para busqueda avanzada
     */
    public filtros:string[]=[];
    public tipoOrden:string;
    public campoOrden:string;
    constructor(
        public juegoMillonario: JuegoMillonario,
        public cdRef: ChangeDetectorRef,
        public router: Router,
        public kc: KeycloakService,
        public quienQuiereSerMillonarioService: QuienQuiereSerMillonarioService,
        public msgService: MessagesService) {

        /*this.itemsBusqueda = [
            { label: 'Avanzado', icon: 'ui-icon-build', command: () => { this.busquedaAvanzada = !this.busquedaAvanzada } }
        ];*/
    }
    ngOnInit() {
        this.cargarJuegos();
    }

    selectPromoEliminar(juegoMillonario: JuegoMillonario) {
        this.promocionEliminar = juegoMillonario;
    }

    /*Metodo que reririge al detalle del segmento que recibe por parámetros
   * Recibe: el segmento al cual redirigir al detalle
   */
    gotoDetail(juegoMillonario: JuegoMillonario) {
        let link = [Rutas.RT_JUEGO_MILLONARIO_DETALLE, juegoMillonario.idJuego];
        this.router.navigate(link);
    }

    /**Usado para cargar de manera perezosa los elementos de la lista de juegos */
    loadData(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantRegistros = event.rows;
        this.cargarJuegos();
    }

    /**Metodo encargado de cargar las promos de la base de datos
     * Utiliza el string de busqueda que fue seteado por el campo de busqueda,
     * en este caso se obtiene el response completo para efectos de obtener los headers para la paginacion
     */
    cargarJuegos() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.quienQuiereSerMillonarioService.token = tkn;
                let sub = this.quienQuiereSerMillonarioService.buscarJuegos(this.terminosBusqueda, this.rowOffset, this.cantRegistros, this.filtros, this.tipoOrden, this.campoOrden).subscribe(
                    (data: Response) => {
                        this.cantTotalRegistros = +data.headers.get("Content-Range").split("/")[1];
                        this.listaJuegos = <JuegoMillonario[]>data.json();//se obtiene los datos del body del request
                        this.cdRef.detectChanges();
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /*
      * Muestra un mensaje de error al usuario con los datos de la transaccion
      * Recibe: una instancia de request con la informacion de error
      * Retorna: un observable que proporciona información sobre la transaccion
      */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}