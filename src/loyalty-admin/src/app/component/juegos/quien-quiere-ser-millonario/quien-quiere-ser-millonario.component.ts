import { Component }  from '@angular/core'; 
/**
 * Flecha Roja Technologies
 * Componente padre de todos los componentes de mision, encargado de proveer dependencias a nivel de juegos 
 * TODO: cambio de preferencias en mision y cambio de atributos de perfil en mision
 */
@Component({
    moduleId:module.id,
    selector:'quien-quiere-ser-millonario-component',
    templateUrl:'quien-quiere-ser-millonario.component.html'
})
export class QuienQuiereSerMillonarioComponent  {
}