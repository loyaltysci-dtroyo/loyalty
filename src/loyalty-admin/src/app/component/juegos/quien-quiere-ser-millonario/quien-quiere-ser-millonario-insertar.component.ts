import { I18nService } from '../../../service/i18n.service';
import * as Rutas from '../../../utils/rutas';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { JuegoMillonario } from '../../../model/index';
import { MenuItem, Message, SelectItem } from 'primeng/primeng';
import { KeycloakService, QuienQuiereSerMillonarioService } from '../../../service/index';
import { PERM_ESCR_JUEGOS } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { AppConfig } from '../../../app.config';

/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente que permite la insercion de juegos al sistema, permite la definicion de datos basicos de la mision, los demas datos
 * deberan ser ingresados en el detalle de mision
 */
@Component({
    moduleId: module.id,
    selector: 'quien-quiere-ser-millonario-insertar',
    templateUrl: 'quien-quiere-ser-millonario-insertar.component.html',
    providers: [JuegoMillonario, QuienQuiereSerMillonarioService]
})
export class QuienQuiereSerMillonarioInsertarComponent implements OnInit {

    public form: FormGroup;//validaciones
    public nombreValido: boolean;//true si el nombre es valido
    public idJuegoInsertado: string;//id que retorna el api cuado la mision es insertada
    public tienePermisosEscritura: boolean = true;//true si el usuario tiene permisos para escrbir en este recurso del sistema

    constructor(public juegoMillonario: JuegoMillonario,
        public router: Router, public i18nService: I18nService,
        public quienQuiereSerMillonarioService: QuienQuiereSerMillonarioService,
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        public msgService: MessagesService) {
    }
    ngOnInit() {
        this.form = new FormGroup({
            "nombre": new FormControl('', [Validators.required]),
            "descripcion": new FormControl('')
        });
        /*this.authGuard.canWrite(PERM_ESCR_JUEGOS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });*/

        this.tienePermisosEscritura = true;
    }

    /** 
     * Inserta una promcion en el sistema, emplea los datos que contiene la variable global this.mision
     */
    insertarJuego() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.quienQuiereSerMillonarioService.token = tkn;//actualiza el token de authenticacion
                let sub = this.quienQuiereSerMillonarioService.addJuego(this.juegoMillonario).subscribe(
                    (response: Response) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.idJuegoInsertado = response.text();//obtiene el id de la mision insertada a partir del body del response
                        this.gotoDetail(this.idJuegoInsertado);
                    }, (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /*Metodo que reririge al detalle del segmento que recibe por parámetros
  * Recibe: el segmento al cual redirigir al detalle
  */
    gotoDetail(idJuego: string) {
        let link = [Rutas.RT_JUEGO_MILLONARIO_DETALLE, idJuego];
        this.router.navigate(link);
    }
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    confirm() {
        this.gotoDetail(this.idJuegoInsertado);
    }

    //Regresar a la lista
    goBack() {
        this.router.navigate([Rutas.RT_JUEGO_MILLONARIO_LISTA]);
    }
}