import { RT_JUEGO_MILLONARIO_DETALLE_INSERTAR_PREGUNTA, RT_JUEGO_MILLONARIO_DETALLE_PREGUNTA } from '../../../utils/rutas';
import { JuegoMillonario, PreguntaMillonario } from '../../../model/index';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { QuienQuiereSerMillonarioService, PreguntaQuienQuiereSerMillonarioService, KeycloakService, I18nService } from '../../../service/index'
import { MessagesService, ErrorHandlerService } from "../../../utils/index";
import { MenuItem } from 'primeng/primeng';
import { PERM_ESCR_JUEGOS } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { Response } from '@angular/http';
import * as Routes from '../../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'preguntas-millonario-lista',
    templateUrl: 'preguntas-millonario-lista.component.html'
})
/**
 * Kevin Ramírez
 * Componente encargado de listar y eliminar las preguntas de un juego millonario
 */
export class PreguntasMillonarioListaComponent implements OnInit {
    public preguntas: PreguntaMillonario[];
    public preguntaEliminar: PreguntaMillonario;
    public terminosBusqueda: string[];
    public displayBusquedaAvanzada: boolean;
    public displayModalEliminar: boolean;
    public rtInsertarPregunta = RT_JUEGO_MILLONARIO_DETALLE_INSERTAR_PREGUNTA;
    public escritura: boolean; //Permite verificar permiso de escritura (true/false)
    public questBorrar: string;
    public metricaBusqueda;
    public totalRecordsMetricas;
    public dialogInsertar: boolean = false;
    public puntosFaltantes: string[] = []; //Lista de puntos faltantes

    public form: FormGroup;
    public itemsPuntos: SelectItem[];//para el select de tipo de tabla
    public itemsRespuestas: SelectItem[];//para el select de tipo de tabla

    constructor(
        public juegoMillonario: JuegoMillonario,
        public preguntaMillonario: PreguntaMillonario,
        public kc: KeycloakService,
        public pregunta: PreguntaMillonario,
        public router: Router, 
        public quienQuiereSerMillonarioService: QuienQuiereSerMillonarioService,
        public preguntaQuienQuiereSerMillonarioService: PreguntaQuienQuiereSerMillonarioService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService,
        public authGuard: AuthGuard
    ) {
        /*this.authGuard.canWrite(PERM_ESCR_JUEGOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });*/
        this.escritura = true;
        this.displayModalEliminar = false;

        this.itemsPuntos = this.i18nService.getLabels("preguntas-itemsPuntos");
        this.itemsRespuestas = this.i18nService.getLabels("preguntas-respuestas");
        this.form = new FormGroup({
            "pregunta": new FormControl("", Validators.required),
            "puntos": new FormControl(this.itemsPuntos[2], Validators.required),
            "respuestaCorrecta": new FormControl(""),
            "respuesta1": new FormControl("", Validators.required),
            "respuesta2": new FormControl("", Validators.required),
            "respuesta3": new FormControl("", Validators.required),
            "respuesta4": new FormControl("", Validators.required)
        });
    }

    ngOnInit() {
        this.obtenerJuego();
        this.questBorrar = this.i18nService.getLabels("confirmacion-pregunta")["B"];
    }

    /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Obtiene los detalles de la promocion en el sistema
     */
    obtenerJuego() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.quienQuiereSerMillonarioService.token = tkn;
                let sub = this.quienQuiereSerMillonarioService.getJuego(this.juegoMillonario.idJuego).subscribe(
                    (data: JuegoMillonario) => {
                        this.copyValuesOf(this.juegoMillonario, data);
                        this.buscarPreguntas();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /**
     * Encargado de isnertar la leaderboard a traves del service que conecta con el API
     * Emplea la variable global llamada leaderboard para almacenar lso datos introducidos por el usuario
     *  */
    insertarPregunta() {
        this.kc.getToken().then((tkn) => {
            this.preguntaQuienQuiereSerMillonarioService.token = tkn;
            this.preguntaMillonario.idJuego = this.juegoMillonario.idJuego;
            let sub = this.preguntaQuienQuiereSerMillonarioService.addPregunta(this.preguntaMillonario).subscribe(
                (data: PreguntaMillonario) => {
                    this.cancelarInsertar();
                    this.buscarPreguntas();
                    this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                },
                (error) => {
                    this.manejaError(error);
                },
                () => {
                    sub.unsubscribe();
                }
            );

        }).catch((error: any) => { this.manejaError(error) });
    }

    cancelarInsertar() {
        this.dialogInsertar = false;
    }

    abrirDialogs(dialog: string) {
        if (dialog == "agregar") {
            this.preguntaMillonario = new PreguntaMillonario();
            this.preguntaMillonario.puntos = this.itemsPuntos[0].value;
            this.preguntaMillonario.respuestaCorrecta = this.itemsRespuestas[0].value;
            this.dialogInsertar = true;
            return;
        }
    }

    /**
     * Busca las leaderboards en el API basandodse en los atributos de busqueda introducidos por el usuario
     * Emplea: el arreglo de leaderboards de busqueda y el termino de busueda introdcido
     */
    buscarPreguntas() {
        this.keycloakService.getToken().then((tkn: string) => {
            this.preguntaQuienQuiereSerMillonarioService.token = tkn;
            let sub = this.preguntaQuienQuiereSerMillonarioService.buscarPreguntas(this.juegoMillonario).subscribe(
                (response: Response) => {
                    this.preguntas = <PreguntaMillonario[]>response.json();
                    const puntosObtenidos = this.preguntas.reduce((acumm, pregunta) => {
                        if (acumm.indexOf(pregunta.puntos) === -1) {
                            acumm.push(pregunta.puntos + '');
                        }
                        return acumm;
                    }, []);
                    this.puntosFaltantes = this.itemsPuntos.reduce((acumm, punto) => {
                        if (puntosObtenidos.indexOf(punto.value) === -1) {
                            acumm.push(punto.value);
                        }
                        return acumm;
                    }, []);
                },
                (error: any) => { this.manejaError(error);},
                () => { sub.unsubscribe(); }
            );
        }).catch((error: any) => { this.manejaError(error) });


    }
    /**navega hacia el detalle de una preferencia en concreto */
    gotoDetail(pregunta: PreguntaMillonario) {
        this.router.navigate([RT_JUEGO_MILLONARIO_DETALLE_PREGUNTA, pregunta.idPregunta]);
    }
    /**elimina la preferencia del sistema, es invocado por el modal de confirmacion */
    eliminarPregunta() {
        this.keycloakService.getToken().then((tkn: string) => {
            this.preguntaQuienQuiereSerMillonarioService.token = tkn;
            let sub = this.preguntaQuienQuiereSerMillonarioService.deletePregunta(this.preguntaEliminar).subscribe(
                () => {
                    this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                    this.buscarPreguntas();
                    this.displayModalEliminar = false;
                },
                (error: any) => { this.manejaError(error); },
                () => { sub.unsubscribe(); }
            );
        }).catch((error: any) => { this.manejaError(error) });
    }
    /**
     * Encargado de manejo de errores
     */
    manejaError(error: any) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /**
     * Muestra el modal de eliminar del leaderboard y ademas almacena el leaderboard a eliminara
     * para luego eliminarla una vez que el usuario confirma la accion
     */
    mostrarModalEliminarPregunta(preguntaMillonario: PreguntaMillonario) {
        this.preguntaEliminar = preguntaMillonario;
        this.displayModalEliminar = true;
    }
}
