export { AtributoComponent } from './atributos/atributo.component';
export { AtributoListaComponent } from './atributos/atributo-lista.component';
export { AtributoInsertarComponent } from './atributos/atributo-insertar.component';
export { AtributoDetalleComponent } from './atributos/atributo-detalle.component';

export { AuthGuard } from './common/auth.guard';

export { ConfiguracionComponent } from './configuracion/configuracion.component';
export { ConfiguracionGeneralComponent } from './configuracion/configuracion-general.component';
export { ConfiguracionMediosComponent } from './configuracion/configuracion-medios.component';
export { ConfiguracionPoliticaComponent } from './configuracion/configuracion-politica.component';
export { ConfiguracionParamsComponent } from './configuracion/configuracion-params.component';

export { DashboardComponent } from './dashboards/dashboard.component';
export { DashboardClienteComponent } from './dashboards/dashboard-cliente.component';
export { DashboardSegmentoComponent } from './dashboards/dashboard-segmentos.component';
export { DashVacioComponent } from './dashboards/dashboard-vacio.component';

export { GrupoComponent } from './grupos/grupo.component';
export { GrupoInsertarComponent } from './grupos/grupo-insertar.component';
export { GrupoListaComponent } from './grupos/grupo-lista.component';
export { GrupoDetalleComponent } from './grupos/grupo-detalle.component';
export { GrupoMiembroComponent } from './grupos/grupo-miembro.component';
export { DetalleComponent } from './grupos/detalle.component';

export { InsigniaComponent } from './insignias/insignias.component';
export { InsigniaListaComponent } from './insignias/insignias-lista.component';
export { InsigniaInsertarComponent } from './insignias/insignias-insertar.component';
export { InsigniaDetalleComponent } from './insignias/insignias-detalle.component';
export { InsigniaNivelListaComponent } from './insignias/insignias-nivel-lista.component';
export { InsigniaMiembroComponent } from './insignias/insignia-miembro.component';
export { InsigniaInformacionComponent } from './insignias/insignia-informacion.component';

export { WorkflowComponent } from "./workflows/workflow.component";
export { WorkflowDetalleComponent } from "./workflows/workflow-detalle.component";
export { WorkflowInsertarComponent } from "./workflows/workflow-insertar.component";
export { WorkflowListaComponent } from "./workflows/workflow-lista.component";
export { WorkflowDetalleCommonComponent } from "./workflows/workflow-detalle-common.component";
export { WorkflowDetalleDefinicionComponent } from "./workflows/workflow-detalle-definicion.component";

export { UsuariosComponent } from './usuarios/usuarios.component';
export { UsuarioListaComponent } from './usuarios/usuario-lista.component';
export { UsuarioInsertarComponent } from './usuarios/usuario-insertar.component';
export { UsuarioDetalleComponent } from './usuarios/usuario-detalle.component';
export { RolListaComponent } from './usuarios/rol-lista.component';
export { RolInsertarComponent } from './usuarios/rol-insertar.component';
export { RolDetalleComponent } from './usuarios/rol-detalle.component';
export { UsuarioDetalleAtributosComponent } from './usuarios/usuario-detalle-atributos.component';
export { UsuarioDetalleRolesComponent } from './usuarios/usuario-detalle-roles.component';

export { UbicacionesComponent } from './ubicaciones/ubicaciones.component';
export { UbicacionListaComponent } from './ubicaciones/ubicacion-lista.component';
export { UbicacionInsertarComponent } from './ubicaciones/ubicacion-insertar.component';
export { UbicacionDetalleComponent } from './ubicaciones/ubicacion-detalle.component';
export { UbicacionInformacionComponent } from './ubicaciones/ubicacion-informacion.component';
export { UbicacionEfectividadComponent } from './ubicaciones/ubicacion-efectividad.component';
export { UbicacionMapaComponent } from './ubicaciones/ubicacion-mapa.component';


export { SegmentosComponent } from './segmentos/segmentos.component';
export { DetalleSegmentoComponent } from './segmentos/segmentos-detalle.component';
export { InsertarSegmentoComponent } from './segmentos/segmentos-insertar.component';
export { ListaSegmentosComponent } from './segmentos/segmentos-lista.component';
export { SegmentosListaReglasComponent } from './segmentos/segmentos-lista-reglas.component';
export { SegmentoDetalleReglasComponent } from './segmentos/segmentos-detalle-reglas.component';
export { DetalleAtributosSegmentoComponent } from './segmentos/segmentos-detalle-atributos.component';
export { SegmentosDetalleMiembros } from './segmentos/segmentos-detalle-miembros.component';
export { SegmentosDetalleElegiblesComponent } from './segmentos/segmentos-detalle-elegibles.component';


export { PromocionesComponent } from './promociones/promociones.component'
export { DetallePromoComponent } from './promociones/detalle-promo.component'
export { DetallePromoAlcanceComponent } from './promociones/detalle-promo-alcance.component'
export { DetallePromoArteComponent } from './promociones/detalle-promo-arte.component'
export { DetallePromoCommonComponent } from './promociones/detalle-promo-common.component'
export { DetallePromoElegiblesComponent } from './promociones/detalle-promo-elegibles.component'
export { ListaPromoComponent } from './promociones/lista-promo.component'
export { InsertarPromoComponent } from './promociones/insertar-promo.component'
export { InsertarCategoriaPromoComponent } from './promociones/insertar-categoria.component'
export { ListaCategoriasPromoComponent } from './promociones/lista-categorias.component'
export { DetallePromoCategoriasComponent } from './promociones/detalle-promo-categorias.component';
export { DetalleCategoriaComponent } from './promociones/detalle-categoria.component';

export { ProductoComponent } from './productos/producto.component';
export { ProductoListaComponent } from './productos/producto-lista.component';
export { ProductoInsertarComponent } from './productos/producto-insertar.component';
export { ProductoDetalleComponent } from './productos/producto-detalle.component';
export { ProductoDisplayComponent } from './productos/producto-display.component';
export { ProductoLimiteComponent } from './productos/producto-limite.component';
export { ProductoCategoriaComponent } from './productos/producto-categoria.component';
export { ProductoElegiblesComponent } from './productos/producto-elegibles.component';
export { ProductoMiembroElegiblesComponent } from './productos/producto-elegible-miembros.component';
export { ProductoSegmentoElegiblesComponent } from './productos/producto-elegible-segmento.component';
export { ProductoInformacionComponent } from './productos/producto-informacion.component';
export { ProductoAtributoComponent } from './productos/producto-atributo.component';
export { CategoriaProductoComponent } from './productos/categoria/categoria-producto.component';
export { CategoriaProductoListaComponent } from './productos/categoria/categoria-producto-lista.component';
export { CategoriaProductoInsertarComponent } from './productos/categoria/categoria-producto-insertar.component';
export { CategoriaProductoDetalleComponent } from './productos/categoria/categoria-producto-detalle.component';
export { CategoriaProductoSubCategoriaComponent } from './productos/categoria/categoria-producto-subcategoria.component';
export { AtributoProductoComponent } from './productos/atributos/atributo-producto.component';
export { AtributoProductoListaComponent } from './productos/atributos/atributo-producto-lista.component';
export { AtributoProdcutoInsertarComponent } from './productos/atributos/atributo-producto-insertar.component';
export { AtributoProductoDetalleComponent } from './productos/atributos/atributo-producto-detalle.component';
export { ProductoCalendarizacionComponent } from './productos/producto-calendarizacion.component';
export { CertificadoCategoriaProducto } from './productos/categoria/certificado-categoria-producto.component';

export { PremioComponent } from './premio/premio.component';
export { PremioListaComponent } from './premio/premio-lista.component';
export { PremioInsertarComponent } from './premio/premio-insertar.component';
export { PremioDetalleComponent } from './premio/premio-detalle.component';
export { PremioInformacionComponent } from './premio/premio-informacion.component';
export { PremioElegiblesComponent } from './premio/premio-elegibles.component';
export { PremioMiembroElegiblesComponent } from './premio/premio-elegibles-miembros.component';
export { PremioSegmentoElegiblesComponent } from './premio/premio-elegibles-segmentos.component';
export { PremioDisplayComponent } from './premio/premio-display.component';
export { PremioLimiteComponent } from './premio/premio-limite.component';
export { PremioCategoriaComponent } from './premio/premio-categoria.component';
export { CategoriaPremioListaComponent } from './premio/categoria/categoria-premio-lista.component';
export { CategoriaPremioInsertarComponent } from './premio/categoria/categoria-premio-insertar.component';
export { CategoriaPremioDetalleComponent } from './premio/categoria/categoria-premio-detalle.component';
export { CategoriaPremioComponent } from './premio/categoria/categoria-premio.component';
export { CategoriaPremioAsignarComponent } from './premio/categoria/categoria-premio-asignar.component';
export { PremioCertificadoComponent } from './premio/certificado-premio.component';
export { UnidadMedidaComponent } from './premio/medidas/unidad-medida.component';

export {NotificacionesComponent} from './notificaciones/notificacion.component';
export {NotificacionesListaComponent} from './notificaciones/notificacion-lista.component';
export {NotificacionesInsertarComponent} from './notificaciones/notificacion-insertar.component';
export {NotificacionesDetalleComponent} from './notificaciones/notificacion-detalle.component';
export {NotificacionesDetalleArteComponent} from './notificaciones/notificacion-detalle-arte.component';
export {NotificacionesDetalleAtributosComponent} from './notificaciones/notificacion-detalle-atributos.component';
export {NotificacionesDetalleElegiblesComponent} from './notificaciones/notificacion-detalle-elegibles.component';
export {NotificacionesDetalleMiembrosComponent} from './notificaciones/elegibles/notificacion-detalle-miembros.component';
export {NotificacionesDetallePromoComponent} from './notificaciones/elegibles/notificacion-detalle-promo.component';
export {NotificacionesDetalleSegmentoComponent} from './notificaciones/elegibles/notificacion-detalle-segmento.component';
export {NotificacionDetalleDashboardComponent} from './notificaciones/notificacion-detalle-dashboard.component';

export { GrupoNivelListaComponent } from './nivel/grupo-nivel-lista.component';
export { GrupoNivelDetalleComponent } from './nivel/grupo-nivel-detalle.component';

export { MisionesComponent } from './misiones/misiones.component';
export { MisionesDetalleComponent } from './misiones/misiones-detalle.component';
export { MisionesDetalleCommmonComponent } from './misiones/misiones-detalle-common.component';
export { MisionesInsertarComponent } from './misiones/misiones-insertar.component';
export { MisionesListaComponent } from './misiones/misiones-lista.component';
export { MisionesContenidoComponent } from './misiones/tipos/misiones-contenido.component';
export { MisionesEncuestaComponent } from './misiones/tipos/misiones-encuesta.component';
export { MisionesPerfilComponent } from './misiones/tipos/misiones-perfil.component';
export { MisionesDetalleLimitesComponent } from './misiones/misiones-detalle-limites.component';
export { MisionesSubirContenidoComponent } from './misiones/tipos/misiones-subir-contenido.component';
export { MisionesActividadSocialComponent } from './misiones/tipos/misiones-actividad-social.component';
export { MisionesDetalleRetoComponent } from './misiones/misiones-detalle-reto.component';
export { MisionesDetalleElegiblesComponent } from './misiones/misiones-detalle-elegibles.component';
export { MisionesDetalleMiembrosComponent } from './misiones/elegibles/misiones-detalle-miembros.component';
export { MisionesDetalleSegmentosComponent } from './misiones/elegibles/misiones-detalle-segmentos.component';
export { MisionesDetalleUbicacionesComponent } from './misiones/elegibles/misiones-detalle-ubicaciones.component';
export { MisionesDetalleCategoriasComponent } from './misiones/misiones-detalle-categorias.component';
export { DetalleCategoriaMisionComponent } from './misiones/detalle-categoria.component';
export { InsertarCategoriaMisionComponent } from './misiones/insertar-categoria.component';
export { ListaCategoriasMisionComponent } from './misiones/lista-categorias.component';
export { MisionesDetalleArteComponent } from './misiones/misiones-detalle-arte.component';

export { MiembrosComponent } from './miembros/miembros.component';
export { MiembroListaComponent } from './miembros/miembro-lista.component';
export { MiembroInsertarComponent } from './miembros/miembro-insertar.component';
export { MiembroDetalleComponent } from './miembros/miembro-detalle.component';
export { MiembroEditarComponent } from './miembros/miembro-editar.component';
export { PreferenciasInsertarComponent } from './miembros/preferencias/preferencias-insertar.component';
export { DetallePreferenciaComponent } from './miembros/preferencias/preferencias-detalle.component';
export { PreferenciasListaComponent } from './miembros/preferencias/preferencias-lista.component';
export { MiembroNivelComponent } from './miembros/miembro-nivel.component';
export { MiembroGrupoComponent } from './miembros/miembro-grupo.component';
export { MiembroAtributoComponent } from './miembros/miembro-atributo.component';
export { MiembroInsigniaComponent } from './miembros/miembro-insignia.component';
export { MiembroPreferenciaComponent } from './miembros/miembro-preferencia.component';
export { MiembroPerfilComponent } from './miembros/miembro-perfil.component';
export { MiembroSegmentoComponent} from './miembros/miembro-segmento.component';
export { MiembroActividadAwardsComponent} from './miembros/miembro-actividad-awards.component';
export { MiembroActividadMisionesComponent} from './miembros/miembro-actividad-misiones.component';
export { MiembroActividadPremiosComponent} from './miembros/miembro-actividad-premios.component';
export { MiembroActividadNotificacionesComponent} from './miembros/miembro-actividad-notificaciones.component';
export { MiembroActividadMetricaComponent} from './miembros/miembro-actividad-metrica.component';
export { MiembroActividadPromocionesComponent} from './miembros/miembro-actividad-promociones.component';

export { MetricasComponent } from './metrica/metricas.component';
export { MetricaListaComponent } from './metrica/metrica-lista.component';
export { MetricaInsertarComponent } from './metrica/metrica-insertar.component';
export { MetricaDetalleComponent } from './metrica/metrica-detalle.component';

export { ReporteComponent } from './reportes/reportes.component';
