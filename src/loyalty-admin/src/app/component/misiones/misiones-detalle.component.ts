import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';
import { MessagesService } from '../../utils/index'
import { MisionService, MetricaService } from '../../service/index';
import { Mision, Metrica } from '../../model/index';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { FormGroup } from '@angular/forms';
import { KeycloakService } from '../../service/index';
import { PERM_ESCR_MISIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import * as Routes from '../../utils/rutas';
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente padre del arbol de componentes de misiones, provee dependencias en comun para todos los elementos
 * hijos, y permite la consulta de los datos de la mision, permite el cambio de estado de mision
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-detalle',
    templateUrl: 'misiones-detalle.component.html',
    providers: [Mision, MisionService, Metrica, MetricaService]
})
export class MisionesDetalleComponent implements OnInit {
    public itemsCalendarizacion: SelectItem[];//items de menu para la calendarizacion de la mision
    public form: FormGroup;//validaciones
    public isRecurrente: any;//flag de recurrencia de la mision true=recurrente
    public es: any; // locales para los calendar input en español
    public diasSeleccionados: string[];//dias seleccionados para recurrencia
    public displayModalEliminar: boolean;
    public displayModalArchivar: boolean;
    public minDate;//fecha minima a partir de la cual se permite al usuario definir la fecha inicial
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public publicado: boolean;
    public borrador: boolean;
    public archivado: boolean;
    public inactivo: boolean;
    public fecInicio: Date = null;
    public fecFin: Date = null;

    constructor(
        public mision: Mision,
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,//ruta actual
        public misionService: MisionService,
        public msgService: MessagesService,
        public router: Router,
        public i18nService: I18nService
    ) {

        this.displayModalArchivar = false;
        this.displayModalEliminar = false;
        this.diasSeleccionados = [];
        this.itemsCalendarizacion =  [];
        this.itemsCalendarizacion = this.i18nService.getLabels("tipo-efectividad-general")
        this.route.params.forEach((params: Params) => { this.mision.idMision = params['id'] });
        this.minDate = new Date(); //fecha minima para la calendarizacion
        this.es = this.i18nService.getLabels("general-calendar");
        this.obtenerMision();
        this.authGuard.canWrite(PERM_ESCR_MISIONES).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }

    ngOnInit() {
        // this.obtenerMision();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
     *Obtiene los datos de la mision para desplegar el estado de la mision y el nombr een la barra de encabezado
     */
    obtenerMision() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.getMision(this.mision.idMision).subscribe(
                    (data: Mision) => {
                        this.copyValuesOf(this.mision, data);
                        if (this.mision.indEstado == 'A') {
                            this.publicado = true;
                        } else if (this.mision.indEstado == 'C') {
                            this.borrador = true;
                        } else if (this.mision.indEstado == 'B') {
                            this.archivado = true;
                        } else {
                            this.inactivo = true;
                        }
                        if (this.mision.indDiaRecurrencia && this.mision.indDiaRecurrencia != "") {
                            /*for (let i = 0; i < this.mision.indDiaRecurrencia.length; i++) {
                                this.diasSeleccionados=[...this.diasSeleccionados,this.mision.indDiaRecurrencia[i]];
                            }*/
                             this.diasSeleccionados = this.mision.indDiaRecurrencia.split('');
                        }
                         if (this.mision.fechaInicio != null) {
                            this.fecInicio = new Date(this.mision.fechaInicio);
                        }
                        if (this.mision.fechaFin != null) {
                            this.fecFin = new Date(this.mision.fechaFin);
                        }
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); }
                )
            });
    }

    /**Vuelve a establecer valores por defecto cuando el usuario seleccion una opcion diferente en el select*/
    onChangeDropCalendarizacion(event) {
        if (event.value == "C") {
            this.mision.fechaInicio = new Date();
            this.mision.fechaFin = new Date();
            this.mision.indDiaRecurrencia = "";
            this.mision.indSemanaRecurrencia = 1;
        }
    }
    /**Vuelve a establecer valores por defecto cuando el usuario cambia el check de recurrencia*/
    onChangeRecurrencia() {
        if (!this.isRecurrente) {
            this.mision.indDiaRecurrencia = "";
            this.mision.indSemanaRecurrencia = 1;
        }
    }
    /**
     * TODO: ELiminar mision solo uando es borrador, archivar cuando es activo, en este mmento solo esta eliminando
     */
    eliminarMision() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.deleteMision(this.mision).subscribe(
                    (data) => {
                        this.displayModalEliminar = false;
                        // this.obtenerMision();
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                        this.router.navigate([Routes.RT_MISIONES]);

                    }, (error) => { this.manejaError(error) }
                );
            });
    }
    arhivarPromo() {
        this.mision.indEstado = "B";
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.updateMision(this.mision).subscribe(
                    (data) => {
                        this.obtenerMision();
                        this.msgService.showMessage(this.i18nService.getLabels("general-archivar"));
                    }, (error) => { this.manejaError(error) }
                );
            });
    }

    /**
     * Cancela la publicacion de la mision y la devuelve al estado de archivado
     */
    cancelarPublicacion() {
        this.mision.indEstado = "B";
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.updateMision(this.mision).subscribe(
                    (data) => {
                        this.displayModalArchivar = false;
                        this.obtenerMision();
                        this.borrador = true;
                        this.inactivo = this.publicado = this.archivado = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));

                    }, (error) => { this.manejaError(error) }
                );
            });
    }
    /**
         * Metodo invocado al publicar una mision
         * Establece el estado de la mision a activo y define el metadata para la activacion de la misma
         */
    activarMision() {
        let indDias = "";
        this.diasSeleccionados.forEach((element) => {
            indDias += element;
        });
        this.mision.indDiaRecurrencia = indDias;
        this.diasSeleccionados = []

        let indSemanas = "";

        if (this.mision.indCalendarizacion != "C") {
            this.mision.fechaInicio = null;
            this.mision.fechaFin = null;
            this.mision.indDiaRecurrencia = null;
            this.mision.indSemanaRecurrencia = null;
        }
        this.mision.indEstado = "A";
        if (this.mision.indDiaRecurrencia == "") { this.mision.indDiaRecurrencia = null }

        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.updateMision(this.mision).subscribe((data) => {
                    this.obtenerMision();
                    this.publicado = true;
                    this.inactivo = this.borrador = this.archivado = false;
                    this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"))
                },
                    (error) => {
                        this.obtenerMision();
                        this.manejaError(error)
                    });
                () => { sub.unsubscribe(); this.obtenerMision(); }
            });
    }

    actualizarMision() {
        let indDias = "";
        this.diasSeleccionados.forEach((element) => {
            indDias += element;
        });
        this.mision.indDiaRecurrencia = indDias;
        this.diasSeleccionados = []

        let indSemanas = "";

        if (this.mision.indCalendarizacion != "C") {
            this.mision.fechaInicio = null;
            this.mision.fechaFin = null;
            this.mision.indDiaRecurrencia = null;
            this.mision.indSemanaRecurrencia = null;
        }
        if (this.mision.indDiaRecurrencia == "") { this.mision.indDiaRecurrencia = null }

        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.updateMision(this.mision).subscribe((data) => {
                    this.obtenerMision();
                    this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"))
                },
                    (error) => {
                        this.obtenerMision();
                        this.manejaError(error)
                    });
                () => { sub.unsubscribe(); this.obtenerMision(); }
            });
    }

    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    goBack() {
        this.router.navigate([Routes.RT_MISIONES_LISTA]);
    }

}