import { I18nService } from '../../service/i18n.service';
import { RT_MISIONES_LISTA_CATEGORIAS } from '../../utils/rutas';

import { MessagesService, ErrorHandlerService } from '../../utils/index';
import { Router } from '@angular/router';
import { Component, ElementRef, OnInit, Renderer, ViewChild } from '@angular/core';
import { MisionService } from '../../service/index';
import { Categoria, Mision, Metrica } from '../../model/index';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { KeycloakService } from '../../service/index';
import { PERM_ESCR_CATEGORIAS, PERM_ESCR_PROMOCIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { AppConfig } from '../../app.config';
@Component({
    moduleId: module.id,
    selector: 'insertar-categoria-mision',
    templateUrl: 'insertar-categoria.component.html',
    providers: [Categoria, MisionService, Mision, Metrica]
})
export class InsertarCategoriaMisionComponent implements OnInit {
    public form: FormGroup;//validaciones
    public file_src: any;
    public file: any;
    public nombreValido: boolean//true cuando el nombre es valido
    public tienePermisosEscritura: boolean;
    constructor(
        public categoria: Categoria,
        public i18nService: I18nService,
        public misionService: MisionService,
        public router: Router,
        public authGuard: AuthGuard,
        public renderer: Renderer,
        public kc: KeycloakService,
        public msgService: MessagesService) {
        this.form = new FormGroup({
            "nombre": new FormControl('', Validators.required),
            "nombreInterno": new FormControl('', Validators.required),
            "descripcion": new FormControl('', Validators.required)
        });

        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.file_src = `${AppConfig.DEFAULT_IMG_URL}`;
    }

    mostrarModalEliminarCategoria(categoria) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.eliminarCategoria(categoria.idCategoria).subscribe(
                    (data) => { }, (error) => { this.manejaError(error) }, () => { sub.unsubscribe(); }
                );
            });
    }
    /*
   * Inserta una categoría
   * Recibe: nada, toma la instancia de categoria que fuen llenada por el form 
   * Retorna: void, se encarga de insertar y reireccionar a la pagina e lista de categorias
   */
    insertarCategoria() {
        if (this.nombreValido) {
            this.categoria.fechaRegistro = new Date();
            if(this.categoria.imagen != null && this.categoria.imagen != ''){
                this.categoria.imagen = this.categoria.imagen.split(",")[1];   
            }
            if(this.file){
                this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
            }
            this.kc.getToken().then(
                (tkn: string) => {
                    this.misionService.token = tkn;
                    let sub = this.misionService.insertarCategoria(this.categoria).subscribe(
                        () => {
                            this.msgService.showMessage(this.i18nService.getLabels("general-insercion"))
                            this.router.navigate([RT_MISIONES_LISTA_CATEGORIAS]);
                        }, (error) => {
                            this.manejaError(error);
                        },
                        () => { sub.unsubscribe(); }
                    );
                });
        } else {

        }
    }

    goBack() {
        this.router.navigate([RT_MISIONES_LISTA_CATEGORIAS]);
    }

    ngOnInit() { }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    establecerNombres() {
        if (this.categoria.nombre != "" && this.categoria.nombre != null) {
            let temp = this.categoria.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.categoria.nombreInterno = temp;
            this.verificarNombreInterno();
        }
    }

    verificarNombreInterno() {
        if (this.categoria.nombreInterno == null || this.categoria.nombreInterno == "") {
            this.nombreValido = false;
            return;
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.verificarNombreInternoCategoria(this.categoria.nombreInterno).subscribe(
                    (data) => {
                        this.nombreValido = data.text() == "false";
                        if (this.nombreValido == false) {
                            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
                        }
                    }, (error) => { this.manejaError(error) }, () => { sub.unsubscribe() }
                );
            });
    }

    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {

        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        // Crear un FileReader
        let reader: any
        let target: EventTarget;
        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            // Obtener el (base64 de la imagen)
            img.src = this.categoria.imagen = event.target.result;
            // Redimensionar la imagen
            this.file_src = this.resize(img);
        }, false);
        reader.readAsDataURL(input.files[0]);
    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");

        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");

        ctx.drawImage(img, 0, 0, width, height);

        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        return dataUrl
    }
}