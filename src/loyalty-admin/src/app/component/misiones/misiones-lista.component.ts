import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Router } from '@angular/router';
import { Mision, Metrica } from '../../model/index';
import { MisionService } from '../../service/index';
import { MenuItem } from 'primeng/primeng';
import { Response } from '@angular/http';
import { KeycloakService } from '../../service/index';
import * as Rutas from '../../utils/rutas';
/**
 * Flecha Roja Technologies
 * Componente encargado de desplegar la lista de misiones y proveer controles de busqueda y paginacion 
 */

@Component({
    moduleId: module.id,
    selector: 'misiones-lista',
    templateUrl: 'misiones-lista.component.html',
    providers: [Mision, MisionService, Metrica]
})
export class MisionesListaComponent implements OnInit {

    public listaMisiones: Mision[];//lista de misiones 
    public promocionEliminar: Mision;//variable para almacenar la mision a eliminar
    public terminosBusqueda: string[]=[];// termino de busqueda
    public displayModalEliminar: boolean = false;//true= se muestra el modal
    public displayModalMensaje: boolean = false;//true= se muestra el modal
    
    public displayModalConfirm: boolean;//se muestra el modal de confirmacion cuando este valor es true
    //public itemsBusqueda: MenuItem[] = [];//usado para poblar el split button de terminos avanzados de busqueda
    public busquedaAvanzada: boolean = false;//usado para desplegar el div de busqueda avanzada
    public rowOffset: number;
    public cantRegistros: number;//cantidad de registros que se va a solicitar al api
    public cantTotalRegistros: number;//cantidad total de registros en el sistema para los terminos de busqueda establecidos

    /**
     * Parametros para busqueda avanzada
     */
    public estados: string[] = ["A", "C"];//tipos de mision que se desplegaran
    public tipos:string[]=[];
    public aprobaciones:string[]=[];
    public filtros:string[]=[];
    public tipoOrden:string;
    public campoOrden:string;
    constructor(
        public mision: Mision,
        public cdRef: ChangeDetectorRef,
        public router: Router,
        public kc: KeycloakService,
        public misionService: MisionService,
        public msgService: MessagesService) {

        /*this.itemsBusqueda = [
            { label: 'Avanzado', icon: 'ui-icon-build', command: () => { this.busquedaAvanzada = !this.busquedaAvanzada } }
        ];*/
    }
    ngOnInit() {
        this.cargarMisiones();
    }

    selectPromoEliminar(mision: Mision) {
        this.promocionEliminar = mision;
    }

    /*Metodo que reririge al detalle del segmento que recibe por parámetros
   * Recibe: el segmento al cual redirigir al detalle
   */
    gotoDetail(mision: Mision) {
        let link = [Rutas.RT_MISIONES_DETALLE, mision.idMision];
        this.router.navigate(link);
    }

    /**Usado para cargar de manera perezosa los elementos de la lista de misiones */
    loadData(event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
        this.rowOffset = event.first;
        this.cantRegistros = event.rows;
        this.cargarMisiones();
    }

    /**Metodo encargado de cargar las promos de la base de datos
     * Utiliza el string de busqueda que fue seteado por el campo de busqueda,
     * en este caso se obtiene el response completo para efectos de obtener los headers para la paginacion
     */
    cargarMisiones() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.buscarMisiones(this.terminosBusqueda, this.estados, this.rowOffset, this.cantRegistros,this.tipos,this.aprobaciones,this.filtros, this.tipoOrden, this.campoOrden).subscribe(
                    (data: Response) => {
                        this.cantTotalRegistros = +data.headers.get("Content-Range").split("/")[1];
                        this.listaMisiones = <Mision[]>data.json();//se obtiene los datos del body del request
                        this.cdRef.detectChanges();
                    },
                    (error) => {
                        this.manejaError(error);
                    },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /*
      * Muestra un mensaje de error al usuario con los datos de la transaccion
      * Recibe: una instancia de request con la informacion de error
      * Retorna: un observable que proporciona información sobre la transaccion
      */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}