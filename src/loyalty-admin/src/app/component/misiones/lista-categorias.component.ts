import { I18nService } from '../../service/i18n.service';
import { Http, Response, Headers } from '@angular/http';
import { RT_CATEGORIAS_MISION_DETALLE, RT_CATEGORIAS_MISION_NSERTAR } from '../../utils/rutas';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Categoria, Mision, Metrica } from '../../model/index';
import { Component, ElementRef, OnInit, Renderer, ViewChild } from '@angular/core';
import { PromocionCategoriaService, MisionService } from '../../service/index';
import { Message } from 'primeng/primeng';
import { Router } from '@angular/router'
import { KeycloakService } from '../../service/index';
import { PERM_ESCR_CATEGORIAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Compoentne encargado de listar las categorias de la mision y permitir la navegacion hacia el detalle de las mismas
 */
@Component({
    moduleId: module.id,
    selector: 'lista-categorias-mision',
    templateUrl: 'lista-categorias.component.html',
    providers: [Categoria, MisionService, Mision, Metrica]
})
export class ListaCategoriasMisionComponent implements OnInit {
    public categorias: Categoria[];//lista de ctegorias
    public displayModalEliminar: boolean;//muesta el modal con el form para inertar
    public categoriaEliminar: Categoria;//almacena la categoria a eliminar
    public rutaInsertar = RT_CATEGORIAS_MISION_NSERTAR;//para efectos de forwarding
    public rowOffset: number; // Fila desplazamiento
    public cantRowPagina: number; // Cantidad de filas
    public cantRegistros: number; //
    public totalRecords: number;

    public escritura: boolean; //Permite verificar permisos (true/false)

    constructor(public categoria: Categoria,
        public kc: KeycloakService,
        public misionService: MisionService,
        public router: Router,
        public mision: Mision,
        public i18nService:I18nService,
        public msgService: MessagesService,
        public authGuard: AuthGuard,
    ) {
        this.categorias = [];
        this.cantRegistros = 10;
        this.rowOffset = 0;

        //Permite saber si el usuario tiene permisos de escritura sobre este elemento
        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS).subscribe(
            (permiso: boolean) => { this.escritura = permiso; });
    }

    /*Lazy load de la lista de categorias de promo*/
    loadData(event) {
        this.rowOffset = event.first;
        this.cantRowPagina = event.rows;
        this.getListaCategorias();
    }

    ngOnInit() {
        this.getListaCategorias();
    }
    /**
     * Obtiene todas las categorias del API
     * 
     */
    getListaCategorias() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.obtenerListaCategorias(this.rowOffset, this.cantRegistros).subscribe(
                    (data: Response) => {
                        this.categorias = data.json();
                        if(data.headers.get("Content-Range") != null){
                           this.totalRecords =+ data.headers.get("Content-Range").split("/")[1];
                        }
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Metodo encargado de manejar errores, recibe una instancia de response y la redirige al service de manejo
     * de errores
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /**
     * Metodo encargado de mistra un mensaje de confimacion al usuario paa el iminar la categoria
     *  */
    mostrarModalEliminarCategoria(categoria: Categoria) {
        this.displayModalEliminar = true;
        this.categoriaEliminar = categoria;
    }
    /**Metodo encagado de eliminar la categoria dela base de datos */
    eliminarCategoria() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.eliminarCategoria(this.categoriaEliminar.idCategoria).subscribe(
                    (data) => {
                        this.displayModalEliminar = false;
                        this.getListaCategorias();
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"))
                    },
                    (error) => { this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error)) },
                    () => { sub.unsubscribe() }
                );
            });
    }

    gotoDetail(categoria: Categoria) {
        let link = RT_CATEGORIAS_MISION_DETALLE;
        this.router.navigate([link, categoria.idCategoria]);
    }

}