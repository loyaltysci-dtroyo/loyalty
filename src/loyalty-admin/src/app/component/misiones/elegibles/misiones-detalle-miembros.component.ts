
import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { MenuItem, SelectItem } from 'primeng/primeng';
import { Miembro, Mision, Segmento, Ubicacion } from '../../../model/index';
import { MiembroService, KeycloakService, SegmentoService, MisionService } from '../../../service/index';
import { Response } from '@angular/http'
import { Router } from '@angular/router';
import { PERM_ESCR_PROMOCIONES } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de realizar mantenimiento de datos de los miembros elegibles de la mision, agrega los miembros de la lista de disponibles a las listas de incluir/excluir
 * y/o elimina los mismos de las listas
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-detalle-miembros',
    templateUrl: 'misiones-detalle-miembros.component.html'
})
export class MisionesDetalleMiembrosComponent implements OnChanges, OnInit {
    public tienePermisosEscritura = true;
    public listaMiembrosDisponibles: Miembro[];//miembros disponibles
    public listaMiembros: Miembro[];//lista de miembros elegibles para la mision
    public busquedaElegibles: string;//campo de busqueda para elegibles en el caso de miembros
    public busquedaDisponibles: string;//campo de busqueda para segmentos/usuarios/ubicaciones disponibles para agregar 
    public seleccMultipleAgregar: any[];//lista de miembros actualmente seleccionados en la seleccion multiple de agregar
    public seleccMultipleEliminar: any[];//lista de segmentos actualmente seleccionados en la seleccion multiple de eliminar
    public displayEliminar: boolean = false;//muestra el modal de eliminar

    public totalElegibles: number;
    public filaElegibles: number = 0;
    public cantidadElegibles: number = 10;
    public totalDisponibles: number;
    public filaDisponibles: number = 0;
    public cantidadDisponibles: number = 10;

    @Input('incluir') indIncluir: string;//indica si se debe trabajar con los miembros a incluir o excluir
    @Input('insertar') displayInsertar: boolean = false;//indica si se debe mostrar los miembros a insertar

    constructor(
        public mision: Mision, public msgService: MessagesService,
        public miembroService: MiembroService,
        public authGuard: AuthGuard,
        public kc: KeycloakService,
        public misionService: MisionService) {
    }

    ngOnInit() {
        this.displayInsertar = false;
        this.listarMiembrosElegibles();
    }

    /**
     * Usado para detectar cambios en los valores de los input del Componente
     * se usa para actualizar los listados dependiendo del valor y la variable
     * que fueron actualizados
     */
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        for (let propName in changes) {
            if (propName == "indIncluir") {
                if (this.displayInsertar == true) {
                    this.listarMiembrosDisponibles();
                }
                if (this.displayInsertar == false) {
                    this.listarMiembrosElegibles();
                }
            }
            if (propName == "displayInsertar") {
                let changedProp = changes[propName];
                if (changedProp.currentValue == true) {
                    this.listarMiembrosDisponibles();
                }
                if (changedProp.currentValue == false) {
                    this.listarMiembrosElegibles();
                }
            }

        }
    }

    loadDataElegibles(event) {
        this.filaElegibles = event.first;
        this.cantidadElegibles = event.rows;
        this.listarMiembrosElegibles();
    }

    loadDataDisponibles(event) {
        this.filaDisponibles = event.first;
        this.cantidadElegibles = event.rows;
        this.listarMiembrosDisponibles();
    }

    /**Lista los miembros asociados a una promocion, 
         * usa el indicador de inclusion directamente en el llamdo al service
         * para traer solo los miembros incluidos/excluidos */
    listarMiembrosElegibles() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.listarMiembrosElegiblesMision(
                    this.mision.idMision, this.indIncluir, this.cantidadElegibles, this.filaElegibles).subscribe(
                    (data: Response) => {
                        this.listaMiembros = data.json();
                        if (data.headers.get("Content-Range") != null) {
                            this.totalElegibles = + data.headers.get("Content-Range").split("/")[1];
                        }
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe() }
                    );
            });

    }

    //lista miembros disponibles para agregar
    listarMiembrosDisponibles() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.listarMiembrosElegiblesMision(this.mision.idMision, "D", this.cantidadDisponibles, this.filaDisponibles).subscribe(
                    (data: Response) => {
                        this.listaMiembrosDisponibles = data.json();
                        if (data.headers.get("Content-Range") != null) {
                            this.totalDisponibles = + data.headers.get("Content-Range").split("/")[1];
                        }
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    //-------------OPERACIONES DE MIEMBROS----------------
    /**
     * Incluye un miembro a la lista de elegibles para la mision
     * Recibe: el miembroa insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    incluirMiembro(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.incluirMiembro(this.mision.idMision, miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "El miembro ha sido incluido");
                        this.listarMiembrosDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
    * Incluye una lista de miembros a la lista d elgibles de la promcoion
    * Emplea la variable seleccionMultipleAgregar para tomar el listado de miembros que debe agregar
    * Llama al metodo manejaError en caso de excepcion
    */
    incluirListaMiembros(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let idMiembros = this.seleccMultipleAgregar.map((elem: Miembro) => { return elem.idMiembro });
                let sub = this.misionService.incluirListaMiembros(this.mision.idMision, idMiembros).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "Los miembros han sido incluidos");
                        this.listarMiembrosDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Excluye una lista de miembros de la lista de elegibles para la mision
     * Recibe: el miembro a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    excluirListaMiembros(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let idMiembros = this.seleccMultipleAgregar.map((elem: Miembro) => { return elem.idMiembro });
                let sub = this.misionService.excluirListaMiembros(this.mision.idMision, idMiembros).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "Los miembros han sido excluidos");
                        this.listarMiembrosDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Excluye una lista de miembros de la lista de elegibles para la mision
     * Recibe: el miembro a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarListaMiembro(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let idMiembros = this.seleccMultipleEliminar.map((elem: Miembro) => { return elem.idMiembro });
                let sub = this.misionService.eliminarListaMiembros(this.mision.idMision, idMiembros).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "Los miembros han sido eliminados");
                        this.listarMiembrosElegibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
    * Excluye un miembro de la lista de elegibles para la mision
    * Recibe: el miembro a excluir de la lista de elegibles
    * Llama al metodo manejaError en caso de excepcion
    */
    excluirMiembro(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.excluirMiembro(this.mision.idMision, miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "El miembro ha sido excluido");
                        this.listarMiembrosDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Elimina un miembro de la lista de elegibles para la mision
     * Recibe: el miembro a eliminar de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarMiembroListas(miembro: Miembro) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.eliminarMiembro(this.mision.idMision, miembro.idMiembro).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "El miembro ha sido eliminado");
                        this.listarMiembrosElegibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}