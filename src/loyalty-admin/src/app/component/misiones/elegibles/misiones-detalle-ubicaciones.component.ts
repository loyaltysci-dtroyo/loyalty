import { Component, OnInit, OnChanges, SimpleChange, Input } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { MenuItem, SelectItem } from 'primeng/primeng';
import { Miembro, Mision, Segmento, Ubicacion } from '../../../model/index';
import { MiembroService, KeycloakService, SegmentoService, MisionService } from '../../../service/index';

import { Router } from '@angular/router';
import { PERM_ESCR_PROMOCIONES } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componenete encargado de realizar mantenimientos de datos de las ubicaciones elegibles para las misiones
 * tiene un listado de ubicaciones a incluir/excluir y ubicaciones disponibles las cuales el usuario alterna
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-detalle-ubicaciones',
    templateUrl: 'misiones-detalle-ubicaciones.component.html'
})
export class MisionesDetalleUbicacionesComponent implements OnChanges, OnInit {
    public tienePermisosEscritura=true;
    public listaUbicaciones:Ubicacion[];
    public listaUbicacionesDisponibles: Ubicacion[];//ista de segmentos que no se encuentran en elegibles 
    public busquedaElegibles: string;//campo de busqueda para elegibles en el caso de miembros
    public busquedaDisponibles: string;//campo de busqueda para segmentos/usuarios/ubicaciones disponibles para agregar 
    public seleccMultipleAgregar: any[];//lista de miembros actualmente seleccionados en la seleccion multiple de agregar
    public seleccMultipleEliminar: any[];//lista de segmentos actualmente seleccionados en la seleccion multiple de eliminar
    public displayEliminar: boolean = false;//muestra el modal de eliminar
    @Input('incluir') indIncluir: string;//indica si se debe trabajar con los miembros a incluir o excluir
    @Input('insertar') displayInsertar: boolean;//indica si se debe trabajar con los elementos para insertar a incluidos/excluidos

    constructor(
        public mision: Mision,
        public msgService: MessagesService,
        public miembroService: MiembroService,
        public authGuard: AuthGuard,
        public kc: KeycloakService,
        public misionService: MisionService
    ) {

    }

    ngOnInit() {
          this.displayInsertar=false;

    }
/**
     * Usado para detectar cambios en los valores de los input del Componente
     * se usa para actualizar los listados dependiendo del valor y la variable
     * que fueron actualizados
     */
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        for (let propName in changes) {
            if (propName == "indIncluir") {

                if (this.displayInsertar == true) {
                    this.listarUbicacionesDisponibles();
                }
                if (this.displayInsertar == false) {
                    this.listarUbicacionesElegibles();
                }
            }
            if (propName == "displayInsertar") {
                let changedProp = changes[propName];
                if (changedProp.currentValue == true) {
                    this.listarUbicacionesDisponibles();
                }
                if (changedProp.currentValue == false) {
                    this.listarUbicacionesElegibles();
                }
            }

        }
    }
      /**Lista ubicaciones asociadas a una promocion, 
     * usa el indicador de inclusion directamente en el llamdo al service
     * para traer solo las ubicaciones incluidos/excluidos */
    listarUbicacionesElegibles() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.listarUbicacionesElegiblesMision(
                    this.mision.idMision, this.indIncluir).subscribe(
                    (data: Ubicacion[]) => {
                        this.listaUbicaciones = data;
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); }
                    );
            });
    }

    //lista las ubicaciones disponibles para agregar a la lista de elegibles
    listarUbicacionesDisponibles() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.listarUbicacionesElegiblesMision(
                    this.mision.idMision, "D").subscribe(
                    (data: Ubicacion[]) => {
                        this.listaUbicacionesDisponibles = data;
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); }
                    );
            });
    }

    //-------------OPERACIONES DE UBICACION----------------
    /**
     * Incluye una ubicaicon a la lista de elegibles para la mision
     * Recibe: el ubicaicon a insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    incluirUbicacion(ubicacion: Ubicacion) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.incluirUbicacion(this.mision.idMision, ubicacion.idUbicacion + "").subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "La ubicacion ha sido incluida");
                        this.listarUbicacionesDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });

    }
    /**
     * Excluye una ubicacion de la lista de elegibles para la mision
     * Recibe: la ubicacion a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    excluirUbicacion(ubicacion: Ubicacion) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.excluirUbicacion(this.mision.idMision, ubicacion.idUbicacion + "").subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "La mision ha sido excluida");
                        this.listarUbicacionesDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }


    /**
     * Elimina una ubicacion de la lista de elegibles para la mision
     * Recibe: la ubicacion a eliminar de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarUbicacionListas(ubicacion: Ubicacion) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.eliminarUbicacion(this.mision.idMision, ubicacion.idUbicacion + "").subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "La ubicacion ha sido eliminada");
                        this.listarUbicacionesElegibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }


    /**
     * Incluye una ubicaicon a la lista de elegibles para la mision
     * Recibe: el ubicaicon a insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    incluirListaUbicaciones(ubicacion: Ubicacion) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let idUbicaciones = this.seleccMultipleAgregar.map((elem: Ubicacion) => { return elem.idUbicacion });
                let sub = this.misionService.incluirListaUbicaciones(this.mision.idMision, idUbicaciones).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "Las ubicaciones han sido incluidas ");
                        this.listarUbicacionesDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });

    }
    /**
     * Excluye una ubicacion de la lista de elegibles para la mision
     * Recibe: la ubicacion a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    excluirListaUbicacion(ubicacion: Ubicacion) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let idUbicaciones = this.seleccMultipleAgregar.map((elem: Ubicacion) => { return elem.idUbicacion });
                let sub = this.misionService.excluirListaUbicaciones(this.mision.idMision, idUbicaciones).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "La ubicacion ha sido incluida");
                        this.listarUbicacionesDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }


    /**
     * Elimina una ubicacion de la lista de elegibles para la mision
     * Recibe: la ubicacion a eliminar de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarListaUbicacionListas(ubicacion: Ubicacion) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let idUbicaciones = this.seleccMultipleEliminar.map((elem: Ubicacion) => { return elem.idUbicacion });
                let sub = this.misionService.eliminarListaUbicaciones(this.mision.idMision, idUbicaciones).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "Las uicaciones han sido eliminadas");
                        this.listarUbicacionesElegibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /*
    * Muestra un mensaje de error al usuario con los datos de la transaccion
    * Recibe: una instancia de request con la informacion de error
    * Retorna: un observable que proporciona información sobre la transaccion
    */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}