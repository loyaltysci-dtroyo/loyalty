
import { Component, OnInit, OnChanges, SimpleChange, Input } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { MenuItem, SelectItem } from 'primeng/primeng';
import { Miembro, Mision, Segmento, Ubicacion } from '../../../model/index';
import { MiembroService, KeycloakService, SegmentoService, MisionService } from '../../../service/index';

import { Router } from '@angular/router';
import { PERM_ESCR_PROMOCIONES } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
/**
 * Flecha Roja technologies
 * Fernando Aguilar Morales
 * Componente encargado de realizar el mantenimiento de datos de los segmentos que participan como elegibles
 * de la mision, contiene listados de segmentos que se consideran elegibles, a incluir o excluit y segmentos disponibles.
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-detalle-segmentos',
    templateUrl: 'misiones-detalle-segmentos.component.html'
})
export class MisionesDetalleSegmentosComponent implements OnChanges, OnInit {
    public tienePermisosEscritura = true;
    public listaSegmentos: Segmento[];
    public listaSegmentosDisponibles: Segmento[];//ista de segmentos que no se encuentran en elegibles 
    public busquedaElegibles: string;//campo de busqueda para elegibles en el caso de miembros
    public busquedaDisponibles: string;//campo de busqueda para segmentos/usuarios/ubicaciones disponibles para agregar 
    public seleccMultipleAgregar: any[];//lista de miembros actualmente seleccionados en la seleccion multiple de agregar
    public seleccMultipleEliminar: any[];//lista de segmentos actualmente seleccionados en la seleccion multiple de eliminar
    public displayEliminar: boolean = false;//muestra el modal de eliminar
    @Input('incluir') indIncluir: string = "I";//indica si se debe trabajar con los segmentos elegibles de incluir/excluir
    @Input('insertar') displayInsertar: boolean = false;//indica si se debe mostrar los elementos a insertar

    constructor(
        public mision: Mision,
        public msgService: MessagesService,
        public miembroService: MiembroService,
        public authGuard: AuthGuard,
        public kc: KeycloakService,
        public misionService: MisionService
    ) {
        this.seleccMultipleAgregar=[];
        this.seleccMultipleEliminar=[];
    }

    ngOnInit() {
        this.displayInsertar = false;
        this.listarSegmentosElegibles();
    }

    /**
     * Usado para detectar cambios en los valores de los input del Componente
     * se usa para actualizar los listados dependiendo del valor y la variable
     * que fueron actualizados
     */
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        for (let propName in changes) {
            if (propName == "indIncluir") {

                if (this.displayInsertar == true) {
                    this.listarSegmentosDisponibles();
                }
                if (this.displayInsertar == false) {
                    this.listarSegmentosElegibles();
                }
            }
            if (propName == "displayInsertar") {
                let changedProp = changes[propName];
                if (changedProp.currentValue == true) {
                    this.listarSegmentosDisponibles();
                }
                if (changedProp.currentValue == false) {
                    this.listarSegmentosElegibles();
                }
            }

        }
    }
    //lista segmentos disponibles para agregar a la lista de elegibles
    listarSegmentosDisponibles() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.listarSegmentosElegiblesMision(this.mision.idMision, "D").subscribe(
                    (data: Segmento[]) => {
                        this.listaSegmentosDisponibles = data;//se asignan los datos provenientes del api al listado de segmentos
                    },
                    (error) => {
                        this.manejaError(error);//se informa al usuario si hubo algun error
                    },
                    () => {
                        sub.unsubscribe(); //evita memory leak
                    }
                );
            });
    }
    /**Lista los segmentos asociados a una mision, 
     * usa el indicador de inclusion directamente en el llamdo al service
     * para traer solo los segmentos incluidos/excluidos */
    listarSegmentosElegibles() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.listarSegmentosElegiblesMision(
                    this.mision.idMision, this.indIncluir).subscribe(
                    (data: Segmento[]) => {
                        this.listaSegmentos = data;
                    },
                    (error) => { this.manejaError(error) },
                    () => { sub.unsubscribe(); }
                    );
            });
    }

    //-------------OPERACIONES DE SEGMENTOS----------------
    /**
     * Incluye un segmento a la lista de elegibles para la mision
     * Recibe: el segmento a insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    incluirSegmento(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.incluirSegmento(this.mision.idMision, segmento.idSegmento).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "El segmento ha sido incluido");
                        this.listarSegmentosDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Excluye un segmento de la lista de elegibles para la mision
     * Recibe: el segmento a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    excluirSegmento(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.excluirSegmento(this.mision.idMision, segmento.idSegmento).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "El semgmento ha sido excluido");
                        this.listarSegmentosDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Elimina un segmento de la lista de elegibles para la mision
     * Recibe: El segmento a eliminar de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarSegmento(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.eliminarSegmento(this.mision.idMision, segmento.idSegmento).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "El segmetno ha sido eliminado");
                        this.listarSegmentosElegibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() } 
                );
            });
    }

    /**
     * Incluye un segmento a la lista de elegibles para la mision
     * Recibe: el segmento a insertar a la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    incluirListaSegmentos(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let idSegmentos = this.seleccMultipleAgregar.map((elem: Segmento) => { return elem.idSegmento });
                let sub = this.misionService.incluirListaSegmentos(this.mision.idMision, idSegmentos).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "La lista de segmentos ha sido incluida");
                        this.listarSegmentosDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Excluye un segmento de la lista de elegibles para la mision
     * Recibe: el segmento a excluir de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    excluirListaSegmentos(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let idSegmentos = this.seleccMultipleAgregar.map((elem: Segmento) => { return elem.idSegmento });
                let sub = this.misionService.excluirListaSegmentos(this.mision.idMision, idSegmentos).subscribe(
                    (data) => {
                        this.msgService.mostrarInfo("", "La lista de segmentos ha sido incluida");
                        this.listarSegmentosDisponibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /**
     * Elimina un segmento de la lista de elegibles para la mision
     * Recibe: El segmento a eliminar de la lista de elegibles
     * Llama al metodo manejaError en caso de excepcion
     */
    eliminarListaSegmento(segmento: Segmento) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let idSegmentos = this.seleccMultipleAgregar.map((elem: Segmento) => { return elem.idSegmento });
                let sub = this.misionService.eliminarListaSegmentos(this.mision.idMision, idSegmentos).subscribe(
                    (data) => {
                       this.msgService.mostrarInfo("", "El segmento ha sido eliminado");
                       this.listarSegmentosElegibles();
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }
    /*
    * Muestra un mensaje de error al usuario con los datos de la transaccion
    * Recibe: una instancia de request con la informacion de error
    * Retorna: un observable que proporciona información sobre la transaccion
    */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }


}