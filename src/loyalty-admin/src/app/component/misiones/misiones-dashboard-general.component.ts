import { EstadisticaMisionGeneral } from '../../model/estadistica-mision-general';
import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MetricaService, EstadisticaService } from '../../service/index';
import { Metrica } from '../../model/index';
import { KeycloakService } from '../../service/index'; import { Component, OnInit } from '@angular/core';
import { PERM_LECT_MISIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'misiones-dashboard-general',
    templateUrl: 'misiones-dashboard-general.component.html',
    providers: [EstadisticaService]
})

export class MisionesDashboardGeneralComponent implements OnInit {
    public statsMisionGeneral: EstadisticaMisionGeneral;

    public statsData: [{
        idMision: string,
        nombreMision: string,
        cantRespuestas: number,
        cantRespuestasAprobadas: number,
        cantRespuestasRechazadas: number,
        cantMiembrosElegibles: number,
        cantVistasUnicasElegibles: number,
        cantVistasTotales: number,
        periodos: [
            {
                mes: string,
                cantRespuestas: number,
                cantRespuestasAprobadas: number,
                cantRespuestasRechazadas: number,
                cantElegiblesPromedio: number,
                cantVistasTotales: number,
                cantVistasUnicas: number
            },
            {
                mes: string,
                cantRespuestas: number,
                cantRespuestasAprobadas: number,
                cantRespuestasRechazadas: number,
                cantElegiblesPromedio: number,
                cantVistasTotales: number,
                cantVistasUnicas: number
            },
            {
                mes: string,
                cantRespuestas: number,
                cantRespuestasAprobadas: number,
                cantRespuestasRechazadas: number,
                cantElegiblesPromedio: number,
                cantVistasTotales: number,
                cantVistasUnicas: number
            },
            {
                mes: string,
                cantRespuestas: number,
                cantRespuestasAprobadas: number,
                cantRespuestasRechazadas: number,
                cantElegiblesPromedio: number,
                cantVistasTotales: number,
                cantVistasUnicas: number
            }
        ]
    }];
    public chartCantElegiblesPromedio: any;
    public chartCantRespuestas: any;
    public chartCantRespuestasAprobadas: any;
    public chartCantRespuestasRechazadas: any;
    public chartCantVistasTotales: any;
    public chartCantVistasUnicas: any;
    public options: any;
    public lectura: boolean; //Permisos de lectura sobre misiones
    public cargandoDatos: boolean = true;

    constructor(
        public kc: KeycloakService,
        private i18nService: I18nService,
        public estadisticaService: EstadisticaService,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService,
        public authGuard: AuthGuard
    ) {
        this.options = {
            legend: {
                position: 'bottom'
            },
            //maintainAspectRatio: false
        };
        this.authGuard.canWrite(PERM_LECT_MISIONES).subscribe((permiso: boolean) => {
            this.lectura = permiso;
            this.obtenerDatosEstadistica();
        });
    }

    ngOnInit() {
    }

    // this.statsData.periodos.map((element=>element.mes))
    poblarGraficos() {
        this.chartCantElegiblesPromedio = {
            labels: this.statsData[0].periodos.map((element => element.mes)),
            datasets: this.statsData.map((element) => {
                return {
                    label: element.nombreMision,
                    data: element.periodos.map((element) => { return element.cantElegiblesPromedio }),
                    fill: false,
                    borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                }
            })
        }
        this.chartCantRespuestas = {
            labels: this.statsData[0].periodos.map((element => element.mes)),
            datasets: this.statsData.map((element) => {
                return {
                    label: element.nombreMision,
                    data: element.periodos.map((element) => { return element.cantRespuestas }),
                    fill: false,
                    borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                }
            })
        }
        this.chartCantRespuestasAprobadas = {
            labels: this.statsData[0].periodos.map((element => element.mes)),
            datasets: this.statsData.map((element) => {
                return {
                    label: element.nombreMision,
                    data: element.periodos.map((element) => { return element.cantRespuestasAprobadas }),
                    fill: false,
                    borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                }
            })
        }
        this.chartCantRespuestasRechazadas = {
            labels: this.statsData[0].periodos.map((element => element.mes)),
            datasets: this.statsData.map((element) => {
                return {
                    label: element.nombreMision,
                    data: element.periodos.map((element) => { return element.cantRespuestasRechazadas }),
                    fill: false,
                    borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                }
            })
        }
        this.chartCantVistasTotales = {
            labels: this.statsData[0].periodos.map((element => element.mes)),
            datasets: this.statsData.map((element) => {
                return {
                    label: element.nombreMision,
                    data: element.periodos.map((element) => { return element.cantVistasTotales }),
                    fill: false,
                    borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                }
            })
        }
        this.chartCantVistasUnicas = {
            labels: this.statsData[0].periodos.map((element => element.mes)),
            datasets: this.statsData.map((element) => {
                return {
                    label: element.nombreMision,
                    data: element.periodos.map((element) => { return element.cantVistasUnicas }),
                    fill: false,
                    borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                }
            })
        }
    }

    obtenerDatosEstadistica() {
        if (this.lectura) {
            this.kc.getToken().then(
                (tkn: string) => {
                    this.estadisticaService.token = tkn;
                    let sub = this.estadisticaService.getEstadisticasMisiones().subscribe(
                        (responseData: any) => {
                            this.statsData = responseData;
                            this.poblarGraficos();
                            this.cargandoDatos = false;
                        }, (error) => {
                            this.manejaError(error);
                            this.cargandoDatos = false;
                        }, () => {
                            sub.unsubscribe();
                        }
                    );
                    let subs = this.estadisticaService.getEstadisticasMisionGeneral().subscribe(
                        (responseData: EstadisticaMisionGeneral) => {
                            this.statsMisionGeneral = responseData;
                            this.poblarGraficos();
                            this.cargandoDatos = false;
                        }, (error) => {
                            this.statsMisionGeneral = new EstadisticaMisionGeneral();
                            this.cargandoDatos = false;
                        }, () => {
                            subs.unsubscribe();
                        }
                    );
                });
        }else{ this.cargandoDatos = false; }
    }

    selectData(event) {

    }
    /**
   * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
   * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
   * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
   * no siempre sea necesario llamar al api para actualizar los datos
   */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
