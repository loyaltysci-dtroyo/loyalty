import { I18nService } from '../../service/i18n.service';
import { Mision, Metrica } from "../../model/index";
import { MisionService, MetricaService, KeycloakService } from "../../service/index";
import { ErrorHandlerService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';
import { MessagesService } from '../../utils/index';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PERM_ESCR_MISIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de actualizar el arte de la mision, 
 * es decir habilita el usuario para cambiar la imagen general de la mision
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-detalle-arte',
    templateUrl: 'misiones-detalle-arte.component.html'
})
export class MisionesDetalleArteComponent implements OnInit {

    public form: FormGroup;//validaciones
    public nombreValido: boolean;//true si el nombre interno de la Mision es valido (unico)
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    //public itemsMetrica: SelectItem[] = [];//items para poblar el select de metricas para la recompensad de mision
    public itemsAprobacion: SelectItem[] = [];//items para poblar el select de tipos de aprobacion para la mision
    public itemsTipoMision: SelectItem[] = []//items de seelect para poblar el drop de tipo de mision; perfil,ver contenido o encuesta
    public file_src: string;
    public file: any;
    public subiendoImagen: boolean = false;
    constructor(public mision: Mision,
        public i18nService: I18nService,
        public misionService: MisionService,
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        public route: ActivatedRoute, public metricaService: MetricaService,
        public router: Router,
        public msgService: MessagesService) {
        this.form = new FormGroup({
            "encabezado": new FormControl(''),
            "sub-encabezado": new FormControl(''),
            "detalle": new FormControl('')
        });
        this.authGuard.canWrite(PERM_ESCR_MISIONES).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }
    ngOnInit() {
        //poblado de los items de metrica
        //this.poblarMetricas();
        //poblacion de los items de aprobacion
        this.itemsAprobacion = this.i18nService.getLabels("misiones-itemsAprobacion");
        //poblar los items de tipo de mision
        this.itemsTipoMision = this.i18nService.getLabels("misiones-itemsTipoMision");
        this.authGuard.canWrite(PERM_ESCR_MISIONES).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.obtenerMision();//se obtienen los datos de la mision 
    }

    /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
    * Metodo que permite cargar la lista de metricas del sistema y las usa para poblar el dropdown de metricas
    */
    /*poblarMetricas() {
        this.itemsMetrica = [];
        this.itemsMetrica=[...this.itemsMetrica, this.i18nService.getLabels("general-default-select")];
        this.kc.getToken().then(
            (tkn: string) => {
                this.metricaService.token = tkn;
                let sub = this.metricaService.getListaMetricas().subscribe(
                    (metricas: Metrica[]) => {
                        metricas.forEach(metr => {
                            this.itemsMetrica=[...this.itemsMetrica,{ value: metr.idMetrica, label: metr.nombreInterno }];
                        });
                    },
                    (error) => this.manejaError(error),
                    () => { sub.unsubscribe(); }
                );
            });
    }*/

    /**
     * Metodo encargado de guardar los detalles de la mision en el sistema
     * Utiliza la instancia de mision almacenada en this.Mision
     */
    actualizarMision() {
        this.subiendoImagen = true;
        this.msgService.showMessage({ detail: "Ésta acción puede tardar unos segundos...", severity: "info", summary: "" });
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.updateMision(this.mision).subscribe(
                    () => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.subiendoImagen = false;
                        this.obtenerMision();
                    }, (error) => {
                        this.subiendoImagen = false;
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Obtiene los detalles de la mision en el sistema
     */
    obtenerMision() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.getMision(this.mision.idMision).subscribe(
                    (data: Mision) => {
                        this.copyValuesOf(this.mision, data);
                        this.file_src = this.mision.imagen;
                        // this.Mision = data;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {
        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            // Obtener el (base64 de la imagen)
            img.src = event.target.result;
            this.mision.imagen = event.target.result;
            this.mision.imagen = this.mision.imagen.split(",")[1];
            // Redimensionar la imagen
            this.file_src = this.resize(img);
        }, false);

        reader.readAsDataURL(input.files[0]);
    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");

        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        return dataUrl
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
