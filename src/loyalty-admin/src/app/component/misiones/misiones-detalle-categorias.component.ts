import { Component, OnInit } from '@angular/core';
import { Categoria,Mision } from '../../model/index';
import {MisionService,PromocionCategoriaService,KeycloakService}from '../../service/index';
import {MessagesService,ErrorHandlerService} from '../../utils/index';
import {AuthGuard} from '../common/index';
import {Router} from '@angular/router';
import {PERM_ESCR_PROMOCIONES} from '../common/auth.constants';
import { Http, Response, Headers } from '@angular/http';
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de realiar mantenimiento de datos de las categorias de la mision
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-detalle-categorias',
    templateUrl: 'misiones-detalle-categorias.component.html',
    providers:[MisionService,Categoria]
})
export class MisionesDetalleCategoriasComponent implements OnInit {
    public categoriasDisponibles: Categoria[];//Lista de categorias que pueden ser insertadas en la Mision
    public categorias: Categoria[];//lista de categorias que la Mision tiene asignadas actualmente
    public displayInsertar = false;
    public tienePermisosEscritura:boolean;//Indica si el usuario tiene permiso para escribir en el componente actual
    public rowOffset:number;
    public cantRegistros:number;
    constructor(
        public categoria: Categoria,
        public misionService: MisionService,
        public authGuard:AuthGuard,
        public mision: Mision, 
        public kc: KeycloakService,
        public router: Router,
        public msgService: MessagesService
    ) {
        this.rowOffset=0;
        this.cantRegistros=10;
        this.categorias = [];
        this.categoriasDisponibles = [];
    }

    ngOnInit() {
        this.listarCategoriasAsociadas();
         if (this.mision.indEstado == "A" || this.mision.indEstado == "B") {
            this.tienePermisosEscritura = false;
        } else {
            this.authGuard.canWrite(PERM_ESCR_PROMOCIONES).subscribe((result: boolean) => {
                this.tienePermisosEscritura = result;
            });
        }
    }


    /**
     * Lista las categorias asociadas a la Mision
     */
    listarCategoriasAsociadas() {
        this.categorias = []
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token=tkn;
                let sub = this.misionService.obtenerCategoriasAsociadas(this.mision.idMision,this.rowOffset,this.cantRegistros).subscribe(
                    (data: Categoria[]) => {
                        this.categorias = data
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Lista las categorias disponibles en el API
     */
    listarcategoriasDisponibles() {
        this.categoriasDisponibles = [];
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token=tkn;
                let sub = this.misionService.obtenerListaCategorias(this.rowOffset,this.cantRegistros).subscribe(
                    (data: Response) => {
                        let temporal = data.json();
                        let existe;
                        temporal.forEach((categoriaData) => {
                            existe = this.categorias.find((categoria) => { return categoria.idCategoria == categoriaData.idCategoria });
                            if (existe == null || existe == undefined) {
                                this.categoriasDisponibles=[...this.categoriasDisponibles,categoriaData];
                            }
                        })

                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Asocia una categoría a la Mision
     */
    asociarCategoria(categoria: Categoria) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token=tkn;
                let sub = this.misionService.asignarCategoriaMision(categoria.idCategoria, this.mision.idMision).subscribe(
                    () => {
                        this.msgService.showMessage({ severity: "info", summary: "", detail: "La categoria ha sido agregada" });
                        this.listarCategoriasAsociadas();
                        this.displayInsertar = false;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                )
            });
    }
    /**
     * Revoca la asociación entre la categoria y la Mision
     */
    revocarCategoria(categoria: Categoria) {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.removerCategoriaMision(categoria.idCategoria, this.mision.idMision).subscribe(
                    () => {
                        this.msgService.showMessage({ severity: "info", summary: "", detail: "La categoria ha sido removida" });
                        this.listarCategoriasAsociadas();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                )
            });
    }
    /**Metodo que muestr el modal de categorias disponibles y carga la lista de categorias disponibles */
    displayModalInsertar() {
        this.displayInsertar = true;
        this.listarcategoriasDisponibles();
    }

    /**
     * Encargado de manejo de errores
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}