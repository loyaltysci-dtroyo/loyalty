import { I18nService } from '../../service/i18n.service';
import { Component, OnInit } from '@angular/core';
import { SelectItem } from "primeng/primeng";
import { MiembroService, UbicacionService, SegmentoService } from "../../service/index";
import { Miembro } from "../../model/index";
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de deter minar los elegibles para la mision,para lo cual se slelecciona el componente de elegibles
 * segun sea necesario, este componente elige el tipo de elegible que se trabajara y carga el componente necesario, el resto de
 * la logica se ejecuta en el componente respectivo  
 * 
 * Este componente controla los demas componentes, tiene los indicadores que los componentes hijos toman como parametros para funcionar
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-detalle-elegibles',
    templateUrl: 'misiones-detalle-elegibles.component.html',
    providers: [Miembro, MiembroService, UbicacionService, SegmentoService]
})
export class MisionesDetalleElegiblesComponent implements OnInit {
    public itemsIndExclusion: SelectItem[];
    public itemsIndComponente: SelectItem[];//items de select para el componente actual
    public indComponente: string;//indicador para seleccionar entre el componente de la mision
    public indExclusion: string;//indicador para seleccionar entre la inclusion o exclusion de elementos 
    public indInsertar: boolean;//indicador para seleccionar si inserta o se elimina 
    public displayInsertar: boolean;
    constructor(public i18nService: I18nService) {
        this.itemsIndComponente = [];
        this.itemsIndExclusion = [];
        this.indComponente = "S";
        this.indExclusion = "I";
        this.indInsertar = false;

        this.itemsIndExclusion = this.i18nService.getLabels("general-itemsIndExclusion");

        this.itemsIndComponente = this.i18nService.getLabels("misiones-elegibles-itemsIndComponente");
    }
    ngOnInit() {

    }
}