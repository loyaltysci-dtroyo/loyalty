import { Component }  from '@angular/core'; 
/**
 * Flecha Roja Technologies
 * Componente padre de todos los componentes de mision, encargado de proveer dependencias a nivel de misiones 
 * TODO: cambio de preferencias en mision y cambio de atributos de perfil en mision
 */
@Component({
    moduleId:module.id,
    selector:'misiones-component',
    templateUrl:'misiones.component.html'
})
export class MisionesComponent  {
}