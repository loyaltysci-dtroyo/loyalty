import { I18nService } from '../../service/i18n.service';
import { Http, Response, Headers } from '@angular/http';
import { RT_CATEGORIAS_DETALLE, RT_CATEGORIAS_INSERTAR } from '../../utils/rutas';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Categoria, Mision, Metrica } from '../../model/index';
import { Component, OnInit } from '@angular/core';
import { MisionService } from '../../service/index';
import { Message } from 'primeng/primeng';
import { Router } from '@angular/router'
import { KeycloakService } from '../../service/index';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { AuthGuard } from '../common/index';
import { PERM_ESCR_CATEGORIAS, PERM_ESCR_MISIONES, PERM_LECT_MISIONES } from '../common/auth.constants';
import * as Routes from '../../utils/rutas';
@Component({
    moduleId: module.id,
    selector: 'detalle-categoria-mision',
    templateUrl: 'detalle-categoria.component.html',
    providers: [Categoria, MisionService, Mision, Metrica]
})
/**
 * Flecha Roja Technoolgies
 * Fernando Aguilar 
 * Componente encargado de mostrar el detalle de una categoria de mision y permitir la edicion de la misma
 */
export class DetalleCategoriaMisionComponent implements OnInit {

    public tienePermisosEscritura: boolean;
    public escrituraMision: boolean;
    public misiones: Mision[];
    public displayModalEliminar: boolean = false;
    public misionEliminar: Mision;
    public displayMisiones: boolean = false;
    public form: FormGroup;//validaciones
    public file_src: any;//campo del source de la imagen, contiene el url de la imagen
    public file: any;
    public rowOffset: number;
    public cantRegistros: number;
    public totalRecords: number;
    public nombreValido: boolean//true cuando el nombre es valido

    constructor(public mision: Mision,
        public i18nService: I18nService,
        public categoria: Categoria,
        public categoriaService: MisionService,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public router: Router, public kc: KeycloakService,
        public msgService: MessagesService) {
        this.misiones = [];
        this.rowOffset = 0;
        this.cantRegistros = 10;
    }

    ngOnInit() {
        this.getCategoria();
        this.listarMisiones();
        this.route.params.forEach((params: Params) => { this.categoria.idCategoria = params['id'] });
        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.authGuard.canWrite(PERM_ESCR_MISIONES).subscribe((result: boolean) => {
            this.escrituraMision = result;
        });
        this.form = new FormGroup({
            "nombre": new FormControl('', Validators.required),
            "nombreInterno": new FormControl('', Validators.required),
            "descripcion": new FormControl('', Validators.required)
        });
    }

    loadData(event) {
        this.rowOffset = event.first;
        this.cantRegistros = event.rows;
        this.listarMisiones();
    }
    /**
     * Obtiene una determinada categoria del api y almacena dicha categoria en la variable de categoria correspondiente
     * 
     */
    getCategoria() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.categoriaService.token = tkn;
                let sub = this.categoriaService.obtenerCategoria(this.categoria.idCategoria).subscribe(
                    (categoria) => {
                        this.categoria = categoria;
                        this.file_src = this.categoria.imagen//actualizo el url de la imagen
                    },
                    (error) => {
                        this.manejaError(error)//en caso de error se reenvia el error a la capa de manejo de errores
                    },
                    () => {
                        sub.unsubscribe();//evita memory leak
                    }
                );
            }
        );
    }
    /**
    * Edita los valores de la cateogria, emplea la variable this.categoria para la insercion
    */
    editarCatetoria() {
        if (this.nombreValido) {
            this.kc.getToken().then(
                (tkn: string) => {
                    this.categoriaService.token = tkn;
                    //this.categoria.imagen = this.categoria.imagen.split(",")[1];//se almacenan el valor de la imagen, el split es necesario debido al formato del base64 generado
                    let sub = this.categoriaService.modificarCategoria(this.categoria).subscribe(
                        (data) => {
                            this.getCategoria();//actualiza el valor de la categoria
                            this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        },
                        (error) => {
                            this.manejaError(error) //en caso de error se reenvia el error a la capa de manejo de errores
                        },
                        () => {
                            sub.unsubscribe();//evita memory leak
                        });
                });
        } else {
            this.msgService.showMessage({ detail: "El nombre interno ya existe. Digite un nuevo nombre interno.", severity: "warn", summary: "" });
        }
    }

    establecerNombres() {
        if (this.categoria.nombre != "" && this.categoria.nombre != null) {
            let temp = this.categoria.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.categoria.nombreInterno = temp;
            this.verificarNombreInterno();
        }
    }

    verificarNombreInterno() {
        if (this.categoria.nombreInterno == null || this.categoria.nombreInterno == "") {
            this.nombreValido = false;
            return;
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.categoriaService.token = tkn;
                let sub = this.categoriaService.verificarNombreInternoCategoria(this.categoria.nombreInterno).subscribe(
                    (data) => {
                        this.nombreValido = data.text() == "false";
                    }, (error) => { this.manejaError(error) }, () => { sub.unsubscribe() }
                );
            });
    }

    /**
    * Revoca la asociación entre la categoria y la Mision
    */
    eliminarCategoria() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.categoriaService.token = tkn;
                let sub = this.categoriaService.removerCategoriaMision(this.categoria.idCategoria, this.misionEliminar.idMision).subscribe(
                    () => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                        this.listarMisiones();//se listan las misiones asociadas a esta categoria nuevamente
                    }, (error) => {
                        this.manejaError(error);//en caso de error se reenvia el error a la capa de manejo de errores
                    }, () => {
                        sub.unsubscribe();//evita memory leak
                    }
                )
            });
    }
    /**
     * Metodo encargado de listar las misiones asociadas a la categoria en cuestion 
     * */
    listarMisiones() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.categoriaService.token = tkn;
                let sub = this.categoriaService.obtenerMisionesPorCategoria(this.categoria.idCategoria, this.rowOffset, this.cantRegistros).subscribe(
                    (data: Response) => {
                        this.misiones = data.json();
                        if(data.headers.get("Content-Range") != null){
                           this.totalRecords =+ data.headers.get("Content-Range").split("/")[1];
                        }
                    }, (error) => {
                        this.manejaError(error);//en caso de error se reenvia el error a la capa de manejo de errores
                    }, () => {
                        sub.unsubscribe();//evita memory leak
                    }
                )
            });
    }
    /**
    * Encargado de manejo de errores
    */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /*
     * Metodo encargado de mostrar un modal de confirmacion antes de revcar una Mision de la lista d emisiones asociadas a la CategoriaPromocion,
     * es invocado desde el template de este componente
    */
    mostrarModalEliminarCateg(mision: Mision) {
        this.misionEliminar = mision;
        this.displayModalEliminar = true;
    }
    /**
     * Navega hacia el detalle de la Mision, este Metodo
   *  Es invocado desde el template de este componente 
     */
    gotoDetailMision(mision) {
        let link = [Routes.RT_MISIONES_DETALLE, mision.idMision];
        this.router.navigate(link);
    }
    /* Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form, este Metodo
    *  Es invocado desde el template de este componente 
    */
    fileChange(input) {

        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            // Obtener el (base64 de la imagen)
            img.src = this.categoria.imagen = event.target.result;
            // Redimensionar la imagen
            this.file_src = this.resize(img);
            let urlBase64 = this.file_src.split(",");
            this.categoria.imagen = urlBase64[1];

        }, false);

        reader.readAsDataURL(input.files[0]);


    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");

        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");

        ctx.drawImage(img, 0, 0, width, height);

        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        return dataUrl
    }

    //Regresar a la lista
    goBack() {
        this.router.navigate([Routes.RT_MISIONES_LISTA_CATEGORIAS]);
    }
}