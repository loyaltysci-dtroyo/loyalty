import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MisionItemAprobacion, Pais } from '../../model/index';
import { KeycloakService, MisionService, I18nService, PaisService } from '../../service';
import { ErrorHandlerService, MessagesService } from '../../utils';
import { AuthGuard } from '../common/index';
import { PERM_ESCR_MISIONES, PERM_APROBACION_MISION } from '../common/auth.constants';

import { Mision } from '../../model/index'

@Component({
    moduleId: module.id,
    selector: 'misiones-detalle-aprobacion',
    templateUrl: 'misiones-detalle-aprobacion.component.html',
    providers: [Mision, MisionService, Pais, PaisService]
})
/**
 * Encargado de realizar lasaprobaciones de mision para las misiones de 
 * aprobacion manual
 */
export class MisionesDetalleAprobacionComponent implements OnInit {

    public cargandoDatos: boolean = true;
    public escritura: boolean;//Permite verificar permisos (true/false)
    public gestion: boolean;//Permiso de aprobacion de mision
    public accion: string; //Guarda el tipo de accion a realizar sobre una metricas
    public misionEncuesta: boolean;

    //Respuestas
    public respuestasPendientes: MisionItemAprobacion[] = [];
    public listaVacia: boolean; //Verifica si la lista de metricas esta vacia
    public cantidad: number = 10; //Cantidad de filas
    public totalRecords: number; //Total de productos en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public idRespuesta: string; //Id de la respuesta seleccionadamisionEncuesta
    //Terminos de busqueda
    public estado: string; // GANADO("G") FALLIDO("F") PENDIENTE("P")
    public tipoOrden: string;
    public itemsEstado: string[];

    public respuestaImagen: any;
    public imagen: any;
    public tipo: string;

    public respuestaContenido: any; //Guarda los datos de la respuesta de subir contenido
    public modalContenido: boolean; //Habilita el modal para mostrar el contenido de la respuesta
    public estadoRespuesta: string; //Guarda el estado (G=gano, F=fallo) de la respuesta seleccionada

    //Aprobación de la misión
    public aprobacion: {
        'idMiembro': string,
        'indEstado': string,
        'fecha': Date
    };

    constructor(
        public mision: Mision,
        public misionService: MisionService,
        public i18nService: I18nService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        public route: ActivatedRoute,
        public paisService: PaisService
    ) {
        this.aprobacion = {
            idMiembro: '',
            indEstado: '',
            fecha: new Date()
        }

        //Representa el id del grupo pasado por la url 
        this.route.parent.params.forEach((params: Params) => { this.mision.idMision = params['id'] });

        this.authGuard.canWrite(PERM_ESCR_MISIONES).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_APROBACION_MISION).subscribe((permiso: boolean) => {
            this.gestion = permiso;
        });
        this.itemsEstado = [];
        this.itemsEstado = this.i18nService.getLabels("mision-aprobacion-estado");
    }

    ngOnInit() {
        this.obtenerMision();
        this.getRespuestasPendientes();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.getRespuestasPendientes();
    }

    respuestaSeleccionada(respuesta: MisionItemAprobacion, estado: string) {
        this.aprobacion.fecha = respuesta.fecha;
        this.aprobacion.idMiembro = respuesta.miembro.idMiembro;
        this.aprobacion.indEstado = this.estadoRespuesta = estado;
        this.enviarAprobacion();
    }

    /**
    *Obtiene los datos de la mision para desplegar el estado de la mision y el nombr een la barra de encabezado
    */
    obtenerMision() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.getMision(this.mision.idMision).subscribe(
                    (data: Mision) => {
                        this.copyValuesOf(this.mision, data);
                        if (this.mision.indAprovacion == "A") {
                            this.estado = "G";
                        } else {
                            this.estado = "P"
                        }
                    },
                    (error) => { this.handleError(error) },
                    () => { sub.unsubscribe(); }
                )
            });
    }

    /**Vuelve a establecer valores por defecto cuando el usuario seleccion una opcion diferente en el select*/
    onChangeDropCalendarizacion(event) {
        if (event.value == "C") {
            this.mision.fechaInicio = new Date();
            this.mision.fechaFin = new Date();
            this.mision.indDiaRecurrencia = "";
            this.mision.indSemanaRecurrencia = 1;
        }
    }

    mostrarContenido(event: any) {
        this.respuestaContenido = event;
        this.modalContenido = true;

    }

    /**
     * Obtiene las respuestas pendientes 
     */
    getRespuestasPendientes() {
        if (this.mision.indAprovacion == "A" && this.estado == "P") {
            return;
        }
        this.kc.getToken().then((tkn) => {
            this.misionService.token = tkn;
            let sub = this.misionService.getRespuestasPendientes(this.mision.idMision, this.estado, this.cantidad, this.filaDesplazamiento).subscribe(
                (response: Response) => {
                    this.respuestasPendientes = response.json();
                    if (response.headers.has("Content-Range")) {
                        this.totalRecords = +response.headers.get("Content-Range").split("/")[1];
                    }
                    if (this.respuestasPendientes.length > 0) {
                        this.listaVacia = false;
                    } else {
                        this.listaVacia = true;
                    }
                }, (error: Response) => {
                    this.handleError(error);
                }, () => {
                    sub.unsubscribe();
                });
        });
    }
    /**
     * Envia una aprobacion/reprobacion para una mision particular
     */
    enviarAprobacion() {
        let sub = this.misionService.enviarAprobacion(this.aprobacion, this.mision.idMision).subscribe(
            (response: Response) => {
                this.getRespuestasPendientes();
                if (this.estadoRespuesta == 'G') {
                    this.msgService.showMessage(this.i18nService.getLabels("respuesta-mision-aprobar"));
                } else {
                    this.msgService.showMessage(this.i18nService.getLabels("respuesta-mision-rechazar"));
                }
            }, (error: Response) => {
                this.handleError(error);
            }, () => {
                sub.unsubscribe();
            });
    }
    /**
     * Maneja el error a nivel de componente
     */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error))
    }
}