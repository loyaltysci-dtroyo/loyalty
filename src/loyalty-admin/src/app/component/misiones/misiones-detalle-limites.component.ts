import { I18nService } from '../../service/i18n.service';
import { Component, OnInit } from '@angular/core';
import { KeycloakService } from '../../service/index';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MisionService } from '../../service/index';
import { Mision } from '../../model/index';
import { SelectItem } from 'primeng/primeng';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { PERM_ESCR_PROMOCIONES } from '../common/auth.constants'
import { AuthGuard } from '../common/index';
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de definir 
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-detalle-limites',
    templateUrl: 'misiones-detalle-limites.component.html'
})
export class MisionesDetalleLimitesComponent implements OnInit {
    public itemsPeriodo: SelectItem[] = [];
    public form: FormGroup;
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema

    constructor(
        public i18nService:I18nService,
        public mision: Mision,
        public kc: KeycloakService,
        public misionService: MisionService,
        public authGuard: AuthGuard,
        public router: Router,
        public msgService: MessagesService) {
        this.form = new FormGroup({
             "indRespuesta": new FormControl(false),
            "cantTotal": new FormControl(Validators.required),
            "cantTotalMiembro": new FormControl(Validators.required),
            "indIntervaloTotal": new FormControl(Validators.required),
            "indIntervaloMiembro": new FormControl(Validators.required),
            "indIntervaloRespuesta": new FormControl(Validators.required),
            "cantIntervalo": new FormControl("")
        });
       
        this.mision.indIntervaloMimebro = "A";
        this.mision.indIntervalo = "A";
        this.mision.indIntervaloTotal = "A";

        this.itemsPeriodo=this.i18nService.getLabels("general-limites-periodo");

        if (this.mision.indEstado == "BO" || this.mision.indEstado == "AR") {
            this.tienePermisosEscritura = false;
        } else {
            this.authGuard.canWrite(PERM_ESCR_PROMOCIONES).subscribe((result: boolean) => {
                this.tienePermisosEscritura = result;
            });
        }
    }

    ngOnInit() {
        this.obtenerMision();
    }
    /**
     * Metodo encargado de obtener los datos de la mision seleccionada
     */
    obtenerMision() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.getMision(this.mision.idMision).subscribe(
                    (data: Mision) => {
                        this.copyValuesOf(this.mision, data);
                        if(!this.mision.cantTotal) {this.mision.cantTotal=0;}
                        if(!this.mision.cantIntervalo) {this.mision.cantIntervalo = 0;}
                        if(!this.mision.indIntervaloMimebro){this.mision.indIntervaloMimebro = "A";}
                        if(!this.mision.indIntervalo){this.mision.indIntervalo = "A";}
                        if(!this.mision.indIntervaloTotal){this.mision.indIntervaloTotal = "A";}

                        // this.mision = data;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
    * Metodo encarao de guardar los detalld e ls mision en el sistema
    * Utiliza la instancia de mision almacenada en this.mision
    */
    actualizarMision() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                if (this.form.touched) {
                    let sub = this.misionService.updateMision(this.mision).subscribe(() => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.obtenerMision();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    });
                }
            });
    }
    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}