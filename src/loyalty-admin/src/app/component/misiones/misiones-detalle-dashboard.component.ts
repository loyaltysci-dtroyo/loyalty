import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MisionService, EstadisticaService } from '../../service/index';
import { Mision } from '../../model/index';
import { KeycloakService } from '../../service/index';

@Component({
    moduleId: module.id,
    selector: 'misiones-detalle-dashboard',
    templateUrl: 'misiones-detalle-dashboard.component.html',
    providers:[EstadisticaService]
})

export class MisionesDetalleDashboardComponent implements OnInit {
     public statsData: {
       idMision: string,
		nombreMision: string,
		cantRespuestas: number,
		cantRespuestasAprobadas: number,
		cantRespuestasRechazadas: number,
		cantMiembrosElegibles: number,
		cantVistasUnicasElegibles: number,
		cantVistasTotales: number,
		periodos: [
			{
				mes: string,
				cantRespuestas: number,
				cantRespuestasAprobadas: number,
				cantRespuestasRechazadas: number,
				cantElegiblesPromedio: number,
				cantVistasTotales: number,
				cantVistasUnicas: number
			},
			{
				mes: string,
				cantRespuestas: number,
				cantRespuestasAprobadas: number,
				cantRespuestasRechazadas: number,
				cantElegiblesPromedio: number,
				cantVistasTotales: number,
				cantVistasUnicas: number
			},
			{
				mes: string,
				cantRespuestas: number,
				cantRespuestasAprobadas: number,
				cantRespuestasRechazadas: number,
				cantElegiblesPromedio: number,
				cantVistasTotales: number,
				cantVistasUnicas: number
			},
			{
				mes: string,
				cantRespuestas: number,
				cantRespuestasAprobadas: number,
				cantRespuestasRechazadas: number,
				cantElegiblesPromedio: number,
				cantVistasTotales: number,
				cantVistasUnicas: number
			}
		]
    };
    public chartVistasData: any;
    public chartElegibilidadData: any;
    public options: any;

    constructor(public mision: Mision,
     public kc: KeycloakService,
        public misionService: MisionService,
        private i18nService:I18nService,
        public estadisticaService: EstadisticaService,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService) { 

        }

    ngOnInit() {
        this.obtenerPromo();
        this.obtenerDatosEstadistica();
    }

    // this.statsData.periodos.map((element=>element.mes))
    poblarGraficos() {
        this.statsData.periodos.reverse();
        this.chartElegibilidadData = {
            labels: this.statsData.periodos.map((element => element.mes)),
            datasets: [
                {
                    label: this.i18nService.getLabels("misiones-dashboard-cant-respuestas") ,
                    data: this.statsData.periodos.map((element => element.cantRespuestas)),
                    fill: false,
                    borderColor: '#4bc0c0'
                },
                {
                    label: this.i18nService.getLabels("misiones-dashboard-cant-respuestas-aprobadas"),
                    data: this.statsData.periodos.map((element => element.cantRespuestasAprobadas)),
                    fill: false,
                    borderColor: '#afc0c0'
                },
                {
                    label: this.i18nService.getLabels("misiones-dashboard-cant-respuestas-rechazadas"),
                    data: this.statsData.periodos.map((element => element.cantRespuestasRechazadas)),
                    fill: false,
                    borderColor: '#f3c0c0'
                }
            ]
        }
         this.chartVistasData = {
            labels: this.statsData.periodos.map((element => element.mes)),
            datasets: [
                {
                    label: this.i18nService.getLabels("misiones-dashboard-cant-elegibles-promedio"),
                    data: this.statsData.periodos.map((element => element.cantElegiblesPromedio)),
                    fill: false,
                    borderColor: '#f3c0c0'
                },
                {
                    label: this.i18nService.getLabels("misiones-dashboard-cant-cant-vistas-totales"),
                    data: this.statsData.periodos.map((element => element.cantVistasTotales)),
                    fill: false,
                    borderColor: '#f3c0c0'
                },
                {
                    label: this.i18nService.getLabels("misiones-dashboard-cant-vistas-unicas"),
                    data: this.statsData.periodos.map((element => element.cantVistasUnicas)),
                    fill: false,
                    borderColor: '#f3c0c0'
                }
            ]
        }
        
    }

    obtenerDatosEstadistica() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.estadisticaService.token = tkn;
                let sub = this.estadisticaService.getEstadisticasMision(this.mision.idMision).subscribe(
                    (responseData: any) => {
                        this.statsData = responseData;
                        this.poblarGraficos();
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /**Obtiene una mision a traves del API */
    obtenerPromo() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.getMision(this.mision.idMision).subscribe(
                    (chartData: Mision) => {
                        this.copyValuesOf(this.mision, chartData);
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    selectData(event) {

    }
    /**
   * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
   * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
   * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
   * no siempre sea necesario llamar al api para actualizar los datos
   */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}