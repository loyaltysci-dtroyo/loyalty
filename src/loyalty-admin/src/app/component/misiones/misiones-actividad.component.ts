import { ErrorHandlerService, MessagesService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { ActividadService, KeycloakService, I18nService } from '../../service/index';
import { Miembro, RegistroActividad } from '../../model/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { PERM_LECT_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'misiones-actividad',
    templateUrl: 'misiones-actividad.component.html',
    providers: [ActividadService]
})

export class MisionesActividadComponent implements OnInit {

    public indPeriodo: string = '1M';/**Atributos para sorting/busqueda */
    public periodos: SelectItem[] = [];
    public listaActividades: any[];
    public lecturaMiembro: boolean; //Permiso de lectura sobre miembro

    constructor(
        public i18nService: I18nService,
        public actividadService: ActividadService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard
    ) {
        this.authGuard.canWrite(PERM_LECT_MIEMBROS).subscribe((permiso: boolean) => {
            this.lecturaMiembro = permiso;
        });
    }

    ngOnInit() {
        this.periodos = this.i18nService.getLabels("general-actividad-periodos")
        this.obtenerActividad();
    }

    /**
     * Obtiene los registros de actividad para el miembro actual
     */
    obtenerActividad() {
        this.kc.getToken().then((tkn) => {
            this.actividadService.token = tkn;
            let sub = this.actividadService.getActividadesMisiones(this.indPeriodo).subscribe(
                (response: RegistroActividad[]) => {
                    this.listaActividades = response;
                }, (response: Response) => {
                    this.handleError(response);
                }, () => {
                    sub.unsubscribe();
                }
            );
        }).catch()
    }
    /**
     * Redirige al administrador al detalle del miembro
     */
    gotoMiembro(idElemento: string) {
        this.router.navigate(['/miembros/detalleMiembro/', idElemento]);
    }
    /**
    * Redirige al administrador a la pantalla de dashboard para el elemento seleccionado
    */
    gotoMision(idElemento: string) {
        this.router.navigate(['/misiones/detalleMision', idElemento]);
    }
    /**
     * Maneja el erro a nivel de componente
     */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}