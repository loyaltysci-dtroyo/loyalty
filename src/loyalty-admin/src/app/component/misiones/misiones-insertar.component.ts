import { I18nService } from '../../service/i18n.service';
import * as Rutas from '../../utils/rutas';
import { RT_PROMOCIONES_DETALLE_COMMON, RT_PROMOCIONES_DETALLE_PROMOCION } from '../../utils/rutas';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Mision, Metrica } from '../../model/index';
import { MenuItem, Message, SelectItem } from 'primeng/primeng';
import { KeycloakService, MisionService, MetricaService } from '../../service/index';
import { PERM_ESCR_PRODUCTOS, PERM_ESCR_PROMOCIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { AppConfig } from '../../app.config';

/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente que permite la insercion de misiones al sistema, permite la definicion de datos basicos de la mision, los demas datos
 * deberan ser ingresados en el detalle de mision
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-insertar',
    templateUrl: 'misiones-insertar.component.html',
    providers: [Mision, MisionService, Metrica, MetricaService]
})
export class MisionesInsertarComponent implements OnInit {

    public form: FormGroup;//validaciones
    public nombreValido: boolean;//true si el nombre es valido
    public idMisionInsertada: string;//id que retorna el api cuado la mision es insertada
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public itemsMetrica: SelectItem[] = [];//items para poblar el select de metricas para la recompensad de mision
    public itemsAprobacion: SelectItem[] = [];//items para poblar el select de tipos de aprobacion para la mision
    public itemsTipoMision: SelectItem[] = []//items de seelect para poblar el drop de tipo de mision; perfil,ver contenido o encuesta
    public file_src: string;
    public file: any;
    public tags: string[];

    constructor(public mision: Mision,
        public router: Router, public i18nService: I18nService,
        public metricaService: MetricaService,
        public kc: KeycloakService,
        public misionService: MisionService,
        public authGuard: AuthGuard,
        public msgService: MessagesService) {
    }
    ngOnInit() {
        this.file_src = `${AppConfig.DEFAULT_IMG_URL}`;
        this.form = new FormGroup({
            "nombre": new FormControl('', [Validators.required]),
            "descripcion": new FormControl(''),
            "tags": new FormControl(''),
            "metrica": new FormControl('', Validators.required),
            "cantMetrica": new FormControl('', Validators.required),
            "aprobacion": new FormControl({value:'', disabled: false}, Validators.required),
            "tipoMision": new FormControl('', Validators.required),
            "indRespuesta": new FormControl(false)
        });
        //poblado de los items de metrica
        this.poblarMetricas();
        //poblacion de los items de aprobacion
        this.itemsAprobacion = this.i18nService.getLabels("misiones-itemsAprobacion");
        //poblar los items de tipo de mision
        this.itemsTipoMision = this.i18nService.getLabels("misiones-itemsTipoMision");
        this.authGuard.canWrite(PERM_ESCR_PROMOCIONES).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.tags = [];
        this.mision.indRespuesta = false;
        this.mision.indAprovacion = "A";
        this.mision.indTipoMision = "E";
    }

    verificarTipo() {
        if (this.mision.indTipoMision == 'J') {
            this.mision.indAprovacion = "A";
            this.form.controls["aprobacion"].disable();
        }else{
            this.form.controls["aprobacion"].enable();
        }
    }
    /**
     * Metodo que permite cargar la lista de metricas del sistema y las usa para poblar el dropdown de metricas
     */
    poblarMetricas() {
        let estado = ['P'];
        this.itemsMetrica = [];
        this.itemsMetrica=[...this.itemsMetrica,this.i18nService.getLabels("general-default-select")];
        this.kc.getToken().then(
            (tkn: string) => {
                this.metricaService.token = tkn;
                let sub = this.metricaService.buscarMetricas(estado,50).subscribe(
                    (response: Response) => {
                        let metricas = response.json();
                        metricas.forEach(metr => {
                            this.itemsMetrica=[...this.itemsMetrica,{ value: metr.idMetrica, label: metr.nombreInterno }];
                        });
                    },
                    (error) => this.manejaError(error),
                    () => { sub.unsubscribe(); }
                );
            });

    }
    /** 
     * Inserta una promcion en el sistema, emplea los datos que contiene la variable global this.mision
     */
    insertarMision() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;//actualiza el token de authenticacion
                if (this.mision.indRespuesta == true) {
                    this.mision.indIntervaloMimebro = this.mision.indIntervaloTotal = this.mision.indIntervalo = "A";
                    this.mision.cantIntervalo = this.mision.cantTotal = this.mision.cantTotalMiembro = 0;
                }
                this.mision.tags = this.tags.join("\n");
                let sub = this.misionService.addMision(this.mision).subscribe(
                    (response: Response) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.idMisionInsertada = response.text();//obtiene el id de la mision insertada a partir del body del response
                        this.gotoDetail(this.idMisionInsertada);
                    }, (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /*Metodo que reririge al detalle del segmento que recibe por parámetros
  * Recibe: el segmento al cual redirigir al detalle
  */
    gotoDetail(idMision: string) {
        let link = [Rutas.RT_MISIONES_DETALLE, idMision];
        this.router.navigate(link);
    }
    // validarNombre() {
    //     this.kc.getToken().then(
    //         (tkn:string) => {
    //             this.misionService.token = tkn;
    //             let sub = this.misionService.validarNombre(this.mision.nombre).subscribe(
    //                 (existeNombre: string) => {
    //                     this.nombreValido = (existeNombre == "false");
    //                 }, (error) => { this.manejaError(error)
    //                 }, () => { sub.unsubscribe(); }
    //             );
    //         });
    // }
    /*
     * Muestra un mensaje de error al usuario con los datos de la transaccion
     * Recibe: una instancia de request con la informacion de error
     * Retorna: un observable que proporciona información sobre la transaccion
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {
        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            // Obtener el (base64 de la imagen)

            img.src = event.target.result
            this.mision.imagen = event.target.result.split(",")[1];
            // Redimensionar la imagen
            this.file_src = this.resize(img);
        }, false);

        reader.readAsDataURL(input.files[0]);
    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");
        // console.log("Size Before: " + img.src.length + " bytes");
        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        // console.log("Size After:  " + dataUrl.length + " bytes");
        return dataUrl
    }

    confirm() {
        this.gotoDetail(this.idMisionInsertada);
    }

    //Regresar a la lista
    goBack() {
        this.router.navigate([Rutas.RT_MISIONES_LISTA]);
    }
}