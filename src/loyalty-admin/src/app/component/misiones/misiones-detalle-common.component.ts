import { I18nService } from '../../service/i18n.service';
import { Mision, Metrica } from "../../model/index";
import { MisionService, MetricaService, KeycloakService } from "../../service/index";
import { ErrorHandlerService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';
import { MessagesService } from '../../utils/index';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PERM_ESCR_MISIONES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { Response } from '@angular/http';

/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de funcionar como padre del arbol de componentes, permite el enrutamiento y provee dependencias al arbol de componentes de misiones
 * 
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-detalle-common',
    templateUrl: 'misiones-detalle-common.component.html'

})
export class MisionesDetalleCommmonComponent implements OnInit {

    public form: FormGroup;//validaciones
    public nombreValido: boolean;//true si el nombre interno de la Mision es valido (unico)
    public tienePermisosEscritura: boolean;//true si el usuario tiene permisos para escrbir en este recurso del sistema
    public itemsMetrica: SelectItem[] = [];//items para poblar el select de metricas para la recompensad de mision
    public itemsAprobacion: SelectItem[] = [];//items para poblar el select de tipos de aprobacion para la mision
    public itemsTipoMision: SelectItem[] = []//items de seelect para poblar el drop de tipo de mision; perfil,ver contenido o encuesta
    public file_src: string;
    public file: any;
    public tags: string[];
    public guardando: boolean = false;

    constructor(public mision: Mision,
        public i18nService: I18nService,
        public misionService: MisionService,
        public kc: KeycloakService,
        public authGuard: AuthGuard,
        public route: ActivatedRoute, public metricaService: MetricaService,
        public router: Router,
        public msgService: MessagesService) {
        this.form = new FormGroup({
            "nombre": new FormControl('', [Validators.required]),
            "descripcion": new FormControl('', Validators.required),
            "tags": new FormControl(''),
            "metrica": new FormControl('', Validators.required),
            "cantMetrica": new FormControl('', Validators.required),
            "aprobacion": new FormControl('', Validators.required),
            "tipoMision": new FormControl({ value: '', disabled: true }),
            "indRespuesta": new FormControl(false)
        });

        this.authGuard.canWrite(PERM_ESCR_MISIONES).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
    }
    ngOnInit() {

        //poblacion de los items de aprobacion
        this.itemsAprobacion = this.i18nService.getLabels("misiones-itemsAprobacion");
        //poblar los items de tipo de mision
        this.itemsTipoMision = this.i18nService.getLabels("misiones-itemsTipoMision");

        this.authGuard.canWrite(PERM_ESCR_MISIONES).subscribe((result: boolean) => {
            this.tienePermisosEscritura = result;
        });
        this.poblarMetricas();
        this.obtenerMision();//se obtienen los datos de la mision 
    }

    /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
    * Metodo que permite cargar la lista de metricas del sistema y las usa para poblar el dropdown de metricas
    */
    poblarMetricas() {
        let estado = ['P'];
        this.itemsMetrica = [];
        this.itemsMetrica=[...this.itemsMetrica,this.i18nService.getLabels("general-default-select")];
        this.kc.getToken().then(
            (tkn: string) => {
                this.metricaService.token = tkn;
                let sub = this.metricaService.buscarMetricas(estado,50).subscribe(
                    (response: Response) => {
                        let metricas = response.json();
                        metricas.forEach(metr => {
                            this.itemsMetrica=[...this.itemsMetrica,{ value: metr.idMetrica, label: metr.nombreInterno }];
                        });
                    },
                    (error) => this.manejaError(error),
                    () => { sub.unsubscribe(); }
                );
            });

    }

    /**
     * Metodo encargado de guardar los detalles de la mision en el sistema
     * Utiliza la instancia de mision almacenada en this.Mision
     */
    actualizarMision() {
        this.guardando = true;
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                if (this.mision.tags) {
                    this.mision.tags = this.tags.join("\n");
                }
                let sub = this.misionService.updateMision(this.mision).subscribe(
                    () => {
                        this.obtenerMision();
                        this.guardando = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                    }, (error) => {
                        this.guardando = false;
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Obtiene los detalles de la mision en el sistema
     */
    obtenerMision() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.getMision(this.mision.idMision).subscribe(
                    (data: Mision) => {
                        this.copyValuesOf(this.mision, data);
                        this.file_src = this.mision.imagen;
                        if (this.mision.tags) {
                            this.tags = this.mision.tags.split("\n");
                        }
                        if(this.mision.indTipoMision == "J"){
                            this.form.controls["aprobacion"].disable();
                        }
                        //poblado de los items de metrica
                        this.poblarMetricas();
                        // this.Mision = data;
                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {
        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            // Obtener el (base64 de la imagen)
            img.src = event.target.result;
            this.mision.imagen = event.target.result;
            this.mision.imagen = this.mision.imagen.split(",")[1];
            // Redimensionar la imagen
            this.file_src = this.resize(img);
        }, false);

        reader.readAsDataURL(input.files[0]);
    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");
        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        // console.log("Size After:  " + dataUrl.length + " bytes");
        return dataUrl
    }

    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

    /* 
     * Metodo que muestra un mensaje al usuario para informarlo sobre algun acontecimiento
     * Recibe: modalHeader: el mensaje que mostrará en el header del modal 
     *         modalMessage: el mensaje que mostrara en el cuerpo del modal
     */
    mostrarModalMensaje(modalHeader: string, modalMessage: string) {
        this.msgService.showMessage({ severity: "info", detail: modalMessage, summary: modalHeader })
    }
}