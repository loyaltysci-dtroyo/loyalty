import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { KeycloakService, QuienQuiereSerMillonarioService, MisionService, ConfiguracionService, PremioService, MetricaService, I18nService } from '../../../service/index';
import { Mision, PremioMisionJuego, MisionJuego, Premio, Metrica, Configuracion, JuegoMillonario } from '../../../model/index';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng'
import { PERM_ESCR_MISIONES, PERM_LECT_PREMIOS, PERM_LECT_METRICAS } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { AppConfig } from '../../../app.config';

@Component({
    moduleId: module.id,
    selector: 'misiones-juego',
    templateUrl: 'misiones-juego.component.html',
    providers: [PremioMisionJuego, JuegoMillonario, QuienQuiereSerMillonarioService, Configuracion, ConfiguracionService, MisionJuego, Metrica, Premio, PremioService, MetricaService]
})

//Representa el componente que permite configurar las misiones de juego y sus respectivos premios
export class MisionesJuegoComponent implements OnInit {

    public escrituraMision: boolean; //Permite verificar permisos (true/false)
    public lecturaPremio: boolean; //Permite verificar permisos de lectura sobre premio(true/false)
    public lecturaMetrica: boolean; //Permite verificar permisos de lectura sobre metrica(true/false)
    public exists: boolean; //Se utiliza para validar si el juego existe, true= existe
    public cargandoDatos: boolean; //Utilizado para mostrar la carga de datos
    public verModal: boolean; //Habilita el cuadro de dialogo para insertar/editar un premio de juego
    public accion: string //Permite determinar la acción a seguir insertar/editar  un premio de juego
    public cambioEstado: boolean; //Permite validar si la misión cambio el estado
    public guardandoDatos: boolean = false; //Utilizado para mostrar la carga de datos del juego
    public tipoTemporal: string; //Guarda el tipo de juego temporalmente

    // ------- Listas dropdowns ------- //
    public tipoItems: SelectItem[]; //Tipos de juegos disponibles
    public temasMillonario: SelectItem[];
    public estrategiaItems: SelectItem[]; //Estrategía a uilizar para determinar la obtención del premio 
    public tipoPremioItems: SelectItem[]; //Tipos de premios, M=métrica, P=premio, G=agradecimiento
    public itemsRespuesta: SelectItem[]; //Intervalo de tiempo (semana, mes, siempre, hora, minuto, día)

    // --------- Métricas --------- //
    public listaMetricas: Metrica[] = []; // Métricas disponibles en el sistema
    public metricaVacia: boolean; //Permite verificar si la lista de métricas esta vacía
    public totalRecordsMetricas: number; //Total de premios disponibles
    public metricaItems: SelectItem[]; //Lista para el dropdown con las métricas disponibles 
    public cantidadMetricas: number = 5; //Cantidad de métricas
    public filaMetricas: number = 0; //Desplazamiento de la primera fila
    public displayDialogMetrica: boolean;
    public nombreMetrica: string;
    public metricaBusqueda: string[] = []; //Palabras de búsqueda, ChipModule

    // --------- Premios Misión Juego ---------- //
    public listaPremiosJuego: PremioMisionJuego[] = []; //Lista con todos los premios que pertenecen a la misión
    public listaVacia: boolean; //Permite verificar si la lista de premios juego esta vacía
    public totalRecords: number; //Total de premios de misión de juego en el sistema
    public cantidad: number = 10; //Cantidad de filas a mostrar en la tabla
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public formPremioJuego: FormGroup; //Form de validación
    public cantMayor: boolean; //Usado en la validación de cantidad de premios del juego según el tipo de misión
    public idMisionJuego: string; //Representa el id del premio mision juego seleccionado para eliminar
    public dialogEliminar: boolean; //Habilita el cuadro de dialogo para confirmar la eliminacion del premio mision juego
    public respuesta: boolean; //Permite manejar si el premio mision juego acepta respuestas
    public acciones: boolean; //Permite controlar las acciones de editar/eliminar un premio juego

    // ----------- Premios ----------- //
    public listaPremios: Premio[] = []; //Lista con todos los premios de la ubicación central
    public premioVacio: boolean; //Permite verificar si la lista de premios de ubic. central esta vacía
    public premioItems: Premio[]; //Lista para el dropdown con los premios
    public totalRecordsPremios: number; //Total de premios en la ubic. centralpublic cantidadPremio: number = 10; //Cantidad de filas a mostrar en la tabla
    public cantidadPremio: number = 10; //Cantidad de filas a mostrar en la tabla
    public filaDesplazamientoPremio: number = 0; //Desplazamiento de la primera fila
    public displayDialog: boolean;
    public nombre: string;
    // ---------- Imagen ---------- //
    public imagen: any; //Contendra el archivo subido por el usuario
    public avatar: any; //Guarda la dirección de la imagen
    public urlBase64: string[]; //Se guarda el array de la imagen

    // ------ Búsqueda ------ //
    public busqueda: string; //Palabras claves de búsqueda
    public listaBusqueda: string[] = []; //Palabras de búsqueda, ChipModule

    constructor(
        public i18nService: I18nService,
        public quienQuiereSerMillonarioService: QuienQuiereSerMillonarioService,
        public mision: Mision,
        public juego: MisionJuego,
        public premio: Premio,
        public configuracion: Configuracion,
        public premioService: PremioService,
        public metricaService: MetricaService,
        public metrica: Metrica,
        public premioJuego: PremioMisionJuego,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public misionService: MisionService,
        public keycloakService: KeycloakService,
        public configuracionService: ConfiguracionService
    ) {
        this.authGuard.canWrite(PERM_ESCR_MISIONES).subscribe((result: boolean) => {
            this.escrituraMision = result;
        });
        this.authGuard.canWrite(PERM_LECT_PREMIOS).subscribe((result: boolean) => {
            this.lecturaPremio = result;
        });
        this.authGuard.canWrite(PERM_LECT_METRICAS).subscribe((result: boolean) => {
            this.lecturaMetrica = result;
        });
        this.avatar = `${AppConfig.DEFAULT_IMG_URL}`;
    }

    ngOnInit() {
        if (this.mision.indEstado == 'A' || this.mision.indEstado == 'B') {
            this.cambioEstado = true;
            this.cantMayor = true;
            this.acciones = true;
        } else {
            this.cambioEstado = false;
            this.cantMayor = false;
            this.acciones = false;
        }

        this.formPremioJuego = new FormGroup({
            'indTipoPremio': new FormControl('', Validators.required),
            'cantMetrica': new FormControl(''),
            'probabilidad': new FormControl('1'),
            'indRespuesta': new FormControl('', Validators.required),
            'cantInterTotal': new FormControl(''),
            'indInterTotal': new FormControl(''),
            'cantInterMiembro': new FormControl(''),
            'indInterMiembro': new FormControl(''),
            'metrica': new FormControl(''),
            'idPremio': new FormControl('')
        });

        this.juego = new MisionJuego();
        this.tipoItems = this.i18nService.getLabels("misiones-juego-itemstipo");
        this.estrategiaItems = this.i18nService.getLabels("misiones-juego-estrategiaItems");
        this.tipoPremioItems = this.i18nService.getLabels("misiones-juego-tipoPremioItems");
        this.itemsRespuesta = this.i18nService.getLabels("misiones-juego-intervalos");
        this.juego.estrategia = "R";
        this.juego.tipo = "R";
        this.poblarTemas();
        this.obtenerJuego();
        this.obtenerConfiguracion();
    }

    /**
    * Metodo que permite cargar la lista de metricas del sistema y las usa para poblar el dropdown de metricas
    */
   poblarTemas() {
    this.temasMillonario = [];
    this.temasMillonario=[...this.temasMillonario,this.i18nService.getLabels("general-default-select")];
    this.keycloakService.getToken().then(
        (tkn: string) => {
            this.quienQuiereSerMillonarioService.token = tkn;
            let sub = this.quienQuiereSerMillonarioService.buscarJuegos(null, -1, -1).subscribe(
                (data: Response) => {
                    let juegos = data.json();
                    juegos.forEach(juego => {
                        if (juego.complete) {
                            this.temasMillonario = [
                                ...this.temasMillonario,
                                {
                                    value: juego.idJuego,
                                    label: juego.nombre
                                }
                            ];
                        }
                    });
                },
                (error) => this.manejaError(error),
                () => { sub.unsubscribe(); }
            );
        });

}

    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerPremiosMisionJuego();
    }

    loadDataPremio(event: any) {
        this.filaDesplazamientoPremio = event.first;
        this.cantidadPremio = event.rows;
        this.buscarPremios();
    }

    loadDataMetrica(event: any) {
        this.filaMetricas = event.first;
        this.cantidadMetricas = event.rows;
        this.busquedaMetricas();
    }

    seleccionarPremio(premio: Premio){
        this.premioJuego.idPremio = premio;
        this.nombre = premio.nombre;
        this.displayDialog = false;
    }

    seleccionarMetrica(metrica: Metrica){
        this.premioJuego.metrica = metrica;
        this.nombreMetrica = metrica.nombre;
        this.displayDialogMetrica = false;
    }

    confirmar(idMisionJuego: string) {
        this.idMisionJuego = idMisionJuego;
        this.dialogEliminar = true;
    }

    //Método que realiza una acción dependiendo si el juego existe o no
    validarExistencia() {
        if (this.exists) {
            this.actualizarJuego();
        } else {
            this.insertarJuego();
        }
    }

    seleccionPremio(premio: PremioMisionJuego) {
        this.premioJuego = new PremioMisionJuego();
        this.busquedaMetricas();
        this.buscarPremios();
        this.premioJuego = premio;
        /*this.premioJuego.indRespuesta = premio.indRespuesta;
        this.premioJuego.indTipoPremio = premio.indTipoPremio;
        /*this.formPremioJuego.patchValue(
            {
                indTipoPremio: this.premioJuego.indTipoPremio, cantMetrica: this.premioJuego.cantMetrica,
                probabilidad: this.premioJuego.probabilidad,
                cantInterTotal: this.premioJuego.cantInterTotal, indInterTotal: this.premioJuego.indInterTotal,
                cantInterMiembro: this.premioJuego.cantInterMiembro, indInterMiembro: this.premioJuego.indInterMiembro,
            })*/
        this.verModal = true;
        this.accion = "editar";
    }

    accionPremioJuego() {
        if (this.accion == "editar") {
            this.modificarPremioJuegoMision();
        } else {
            this.insertarPremioJuegoMision();
        }
    }

    cancelarAccion() {
        this.verModal = false;
        this.accion = "insertar"
    }

    nuevoPremio() {
        //this.formPremioJuego.reset();
        this.premioJuego = new PremioMisionJuego();
        this.busquedaMetricas();
        this.buscarPremios();
        if (this.mision.indEstado == 'A' || this.mision.indEstado == 'B') {
            this.cambioEstado = true;
            this.cantMayor = true;
            this.acciones = true;
            this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-cambio"));
            return;
        } else {
            this.cambioEstado = false;
            this.acciones = false;
        }
        this.accion = "insertar"
        this.verModal = true;
        this.premioJuego.indTipoPremio = "P";
    }

    //Se obtiene la información del juego
    obtenerJuego() {
        this.keycloakService.getToken().then(
            (token: string) => {
                this.misionService.token = token;
                let sub = this.misionService.obtenerJuegoPorId(this.mision.idMision).subscribe(
                    (data: MisionJuego) => {
                        this.juego = data;
                        this.exists = true;
                        this.obtenerPremiosMisionJuego();
                        if (this.juego.tipo == "O") {
                            this.avatar = this.juego.imagen;
                        }
                        this.tipoTemporal = this.juego.tipo;
                    }, (error: Response) => {
                        //Si devuelve un 404 es que el juego no existe
                        if (error.status == 404) {
                            this.exists = false;
                            this.cargandoDatos = false;
                            this.mision.juego = new MisionJuego();
                            return;
                        }
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    //Permite insertar el juego
    insertarJuego() {
        this.juego.idMision = this.mision.idMision;
        if (this.juego.tipo == null) {
            this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-tipo"));
            return;
        }
        if (this.juego.estrategia == null) {
            this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-estrategia"));
            return;
        }
        if (this.juego.tipo == "O" && this.juego.imagen == null) {
            this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-imagen"));
            return;
        }
        if (this.juego.tipo == "O") {
            if (this.totalRecords > 1) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-rompecabeza"));
                return;
            }
            if (this.juego.tiempo < 10 || this.juego.tiempo == null) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-tiempo"));
                return;
            }
            this.msgService.showMessage({ detail: "Esta acción puede tardar unos segundos...", severity: "info", summary: "" });
        }
        if (this.juego.tipo != "O") {
            this.juego.imagen == null;
            this.juego.tiempo = null;
        }
        if (this.juego.tipo == "M" && this.juego.idJuegoMillonario == null) {
            return;
        }
        this.guardandoDatos = true;
        this.keycloakService.getToken().then(
            (token: string) => {
                this.misionService.token = token;//actualiza el token de authenticacion
                let sub = this.misionService.agregarJuego(this.mision.idMision, this.juego).subscribe(
                    (response: Response) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.juego.idMision = this.mision.idMision;
                        this.exists = true;
                        this.guardandoDatos = false;
                        this.obtenerJuego();
                    }, (error) => { this.manejaError(error); this.guardandoDatos = false; },
                    () => { sub.unsubscribe(); this.guardandoDatos = false; }
                );
            });
    }

    //Actualiza los datos del juego
    actualizarJuego() {
        if (this.juego.tipo == null) {
            this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-tipo"));
            return;
        }
        if (this.juego.estrategia == null) {
            this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-estrategia"));
            return;
        }
        if (this.juego.tipo == "O" && this.juego.imagen == null) {
            this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-imagen"));
            return;
        }
        if (this.juego.tipo != "O") {
            this.juego.imagen = null;
            this.juego.tiempo = null;
        }

        if (this.juego.tipo == "O") {
            if (this.totalRecords > 1) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-rompecabeza"));
                this.juego.tipo = this.tipoTemporal;
                return;
            }
            if (this.juego.tiempo < 10 || this.juego.tiempo == null) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-tiempo"));
                return;
            }
            this.juego.imagenesRompecabezas = "";
            this.msgService.showMessage({ detail: "Esta acción puede tardar unos segundos...", severity: "info", summary: "" });
        }

        if (this.juego.tipo == "A") {
            if (this.totalRecords > 6) {
                this.juego.tipo = this.tipoTemporal;
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-raspadita"));
                return;
            }
        }
        this.guardandoDatos = true;
        this.keycloakService.getToken().then(
            (token: string) => {
                this.misionService.token = token;//actualiza el token de authenticacion
                let sub = this.misionService.actualizarJuego(this.mision.idMision, this.juego).subscribe(
                    (response: Response) => {
                        this.obtenerJuego();
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.guardandoDatos = false;
                    }, (error) => { this.manejaError(error); this.guardandoDatos = false; },
                    () => { sub.unsubscribe(); this.guardandoDatos = false; }
                );
            });
    }

    //Obtiene los premios de la misión de juego
    obtenerPremiosMisionJuego() {
        if (this.exists) {
            this.keycloakService.getToken().then(
                (token: string) => {
                    this.misionService.token = token;//actualiza el token de authenticacion
                    let sub = this.misionService.obtenerPremiosMisionJuego(this.mision.idMision, this.cantidad, this.filaDesplazamiento).subscribe(
                        (response: Response) => {
                            this.listaPremiosJuego = response.json();
                            if (response.headers.get("Content-Range") != null) {
                                this.totalRecords = + response.headers.get("Content-Range").split("/")[1] + 1;
                            }
                            if (this.listaPremiosJuego.length > 0) {
                                if (this.mision.indEstado == 'A' || this.mision.indEstado == 'B') {
                                    this.cantMayor = true;
                                } else {
                                    if (this.juego.tipo == "A") {
                                        if (this.totalRecords == 6) {
                                            this.cantMayor = true;
                                        } else {
                                            this.cantMayor = false;
                                        }
                                    } else if (this.juego.tipo == "O") {
                                        if (this.totalRecords >= 1) {
                                            this.cantMayor = true;
                                        } else {
                                            this.cantMayor = false;
                                        }
                                    } else if (this.juego.tipo == "R") {
                                        if (this.totalRecords == 7) {
                                            this.cantMayor = true;
                                        } else {
                                            this.cantMayor = false;
                                        }
                                    }
                                }
                                this.listaVacia = false;
                            } else {
                                this.listaVacia = true;
                            }
                            this.cargandoDatos = false;
                        }, (error) => { this.manejaError(error); this.cargandoDatos = false; },
                        () => { sub.unsubscribe(); }
                    );
                });
        } else {
            this.cargandoDatos = false;
            this.listaVacia = true;
        }
    }

    //Obtiene un premio especifico por id
    obtenerPremioPorId() {
        let idMisionJuego = "";
        this.keycloakService.getToken().then(
            (token: string) => {
                this.misionService.token = token;//actualiza el token de authenticacion
                let sub = this.misionService.obtenerPremioJuegoPorId(this.mision.idMision, idMisionJuego).subscribe(
                    (response: Response) => {
                        this.premioJuego = response.json();
                    }, (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    //Inserta un nuevo premio de la mision juego
    insertarPremioJuegoMision() {
        this.premioJuego.idJuego = this.juego;
        if (this.juego.estrategia == 'P') {
            if (this.premioJuego.probabilidad == null) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-probabilidad"));
                return;
            }
        } else {
            this.premioJuego.probabilidad = 1;
        }
        if (this.premioJuego.indTipoPremio == "P") {
            if (this.premioJuego.idPremio == undefined || this.premioJuego.idPremio == null) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-premio-juego"));
                return;
            } else {
                this.premioJuego.metrica = undefined;
                this.premioJuego.cantMetrica = undefined;
            }
        }
        if (this.premioJuego.indTipoPremio == "M") {
            if (this.premioJuego.metrica == undefined || this.premioJuego.metrica == null) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-premio-juego"));
                return;
            } else {
                this.premioJuego.idPremio = undefined;
            }
            if (this.premioJuego.cantMetrica == null || this.premioJuego.cantMetrica == undefined) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-metrica-juego"));
                return;
            }
        }
        if (this.premioJuego.indTipoPremio == "G") {
            this.premioJuego.idPremio = undefined;
            this.premioJuego.metrica = undefined;
            this.premioJuego.cantMetrica = undefined;
        }

        if (!this.premioJuego.indRespuesta) {
            this.premioJuego.cantInterTotal = undefined;
            this.premioJuego.indInterTotal = undefined;
            this.premioJuego.cantInterMiembro = undefined;
            this.premioJuego.indInterMiembro = undefined;
        }
        this.keycloakService.getToken().then(
            (token: string) => {
                this.misionService.token = token;//actualiza el token de authenticacion
                let sub = this.misionService.registrarPremioJuego(this.mision.idMision, this.premioJuego).subscribe(
                    (response: Response) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.exists = true;
                        this.obtenerPremiosMisionJuego();
                        this.verModal = false;
                        this.nombre = "";
                        this.nombreMetrica = "";
                    }, (error) => { this.manejaError(error); this.verModal = false; },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    //Modifica la información de un premio
    modificarPremioJuegoMision() {
        this.premioJuego.idJuego = this.juego;
        if (this.juego.estrategia == 'P') {
            if (this.premioJuego.probabilidad == null) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-juego-probabilidad"));
            }
        } else {
            this.premioJuego.probabilidad = 1;
        }
        if (this.premioJuego.indTipoPremio == "P") {
            if (this.premioJuego.idPremio == undefined || this.premioJuego.idPremio == null) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-premio-juego"));
                return;
            } else {
                this.premioJuego.metrica = undefined;
                this.premioJuego.cantMetrica = undefined;
            }
        }
        if (this.premioJuego.indTipoPremio == "M") {
            if (this.premioJuego.metrica == undefined || this.premioJuego.metrica == null) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-premio-juego"));
                return;
            } else {
                this.premioJuego.idPremio = undefined;
            }
            if (this.premioJuego.cantMetrica == null || this.premioJuego.cantMetrica == undefined) {
                this.msgService.showMessage(this.i18nService.getLabels("warn-misiones-metrica-juego"));
                return;
            }
        }

        if (this.premioJuego.indTipoPremio == "G") {
            this.premioJuego.idPremio = undefined;
            this.premioJuego.metrica = undefined;
            this.premioJuego.cantMetrica = undefined;
        }

        if (!this.premioJuego.indRespuesta) {
            this.premioJuego.cantInterTotal = undefined;
            this.premioJuego.indInterTotal = undefined;
            this.premioJuego.cantInterMiembro = undefined;
            this.premioJuego.indInterMiembro = undefined;
        }
        this.keycloakService.getToken().then(
            (token: string) => {
                this.misionService.token = token;//actualiza el token de authenticacion
                let sub = this.misionService.editarPremioJuego(this.mision.idMision, this.premioJuego).subscribe(
                    (response: Response) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        //this.formPremioJuego.reset();
                        this.accion = "insertar";
                        this.verModal = false;
                    }, (error) => { this.manejaError(error); this.verModal = false; },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    sacarPremiosDeLista() {
        let listaTemporal = this.listaPremiosJuego;
        if (this.listaPremiosJuego.length > 0) {
            let index = this.listaPremiosJuego.findIndex(element => element.idMisionJuego == this.idMisionJuego);
            this.listaPremiosJuego.splice(index, 1);
            this.listaPremiosJuego =[...this.listaPremiosJuego];
        }
    }

    //Elimina un premio
    eliminarPremioJuegoMision() {
        this.keycloakService.getToken().then(
            (token: string) => {
                this.misionService.token = token;//actualiza el token de authenticacion
                let sub = this.misionService.removerPremioJuegoMision(this.mision.idMision, this.idMisionJuego).subscribe(
                    (response: Response) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                        this.dialogEliminar = false;
                        this.sacarPremiosDeLista();
                    }, (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    cambioTipoPremio() {
        if (this.premioJuego.indTipoPremio == 'P') {
            this.buscarPremios();
        } else {
            this.busquedaMetricas();
        }
    }

    obtenerConfiguracion() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.configuracionService.token = token;
                this.configuracionService.obtenerConfiguracion().subscribe(
                    (response: Response) => {
                        this.configuracion = response.json();
                    })
            })
    }


    /*Método encargado de realizar la búsqueda de premios en la ubicacion central, obtiene todos los premios 
    *de la ubicación central*/
    buscarPremios(event?) {
        if (this.lecturaPremio) {
            let busqueda = "";
            busqueda = this.listaBusqueda.join(" ")
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.premioService.token = token;
                    if (this.configuracion.integracionInventarioPremio != undefined && this.configuracion.integracionInventarioPremio == true) {
                        
                        this.premioService.obtenerPremiosUbicacionCentral(busqueda).subscribe(
                            (response: Response) => {
                                this.listaPremios = response.json();
                                if (response.headers.get("Content-Range") != null) {
                                    this.totalRecordsPremios = + response.headers.get("Content-Range").split("/")[1];
                                }
                                this.premioItems = [];
                                if (this.listaPremios.length > 0) {
                                    this.premioVacio = false;
                                    //this.listaPremios.forEach(premio => { this.premioItems.push(premio); });
                                    //this.listaPremios.forEach(premio => { this.premioItems=[...this.premioItems, this.premio]});
                                } else { this.premioVacio = true; }
                            },
                            (error) => this.manejaError(error)
                        );
                    } else {
                        let estadoPremio = ['P'];
                        let tipoPremio = ['C'];
                        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                            (token: string) => {
                                this.premioService.token = token;
                                this.premioService.busquedaPremios(this.cantidadPremio, this.filaDesplazamientoPremio, tipoPremio, estadoPremio, busqueda).subscribe(
                                    (response: Response) => {
                                        this.listaPremios = response.json();
                                        if (response.headers.get("Content-Range") != null) {
                                            this.totalRecordsPremios = + response.headers.get("Content-Range").split("/")[1];
                                        }
                                        this.premioItems = [];
                                        if (this.listaPremios.length > 0) {
                                            this.premioVacio = false;
                                        } else { this.premioVacio = true; }
                                        this.cargandoDatos = false;
                                    },
                                    (error) => {
                                        this.manejaError(error);
                                        this.cargandoDatos = false;
                                    }
                                );
                            });
                    }
                });
        }
    }

    //Método encargado de realizar la búsqueda de métricas
    busquedaMetricas() {
        if (this.lecturaMetrica) {
            let busqueda = "";
            busqueda = this.listaBusqueda.join(" ")
            let estado = ["P"];
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.metricaService.token = token;
                    this.metricaService.buscarMetricas(estado, this.cantidadMetricas, this.filaMetricas, busqueda).subscribe(
                        (response: Response) => {
                            this.listaMetricas = response.json();
                            if (response.headers.get("Content-Range") != null) {
                                this.totalRecordsMetricas = + response.headers.get("Content-Range").split("/")[1];
                            }
                            /*this.metricaItems = [];
                            this.metricaItems=[...this.metricaItems,this.i18nService.getLabels("general-default-select")];*/
                            if (this.listaMetricas.length > 0) {
                                this.metricaVacia = false;
                                /*this.listaMetricas.forEach(metrica => {
                                    this.metricaItems=[...this.metricaItems,{ label: metrica.nombre, value: metrica }];
                                });*/
                            } else {
                                this.metricaVacia = true;
                            }
                        },
                        (error) => this.manejaError(error)
                    );
                });
        }
    }

    //Método para subir la imagen del avatar del cliente
    subirImagen() {
        this.imagen = document.getElementById("avatar");
        let reader = new FileReader();
        reader.addEventListener("load", (event: any) => {
            this.avatar = reader.result;
            this.urlBase64 = this.avatar.split(",");
            this.juego.imagen = this.urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /*
  * Muestra un mensaje de error al usuario con los datos de la transaccion
  * Recibe: una instancia de request con la informacion de error
  * Retorna: un observable que proporciona información sobre la transaccion
  */
    manejaError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}