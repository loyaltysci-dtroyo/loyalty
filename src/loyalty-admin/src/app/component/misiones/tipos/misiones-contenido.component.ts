import { I18nService } from '../../../service';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { KeycloakService, MisionService } from '../../../service/index';
import { Mision, MisionVerContenido } from '../../../model/index';

import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng'

/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de realizar mantenimiento de datos para el reto de las misiones cuando el tipo de mision es ver contenido, 
 * determina los elementos necesarios para ingresar un reto de visualizacion de contenido en el sistema
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-contenido',
    templateUrl: 'misiones-contenido.component.html'
})
export class MisionesContenidoComponent implements OnInit {
    public itemsTipo: SelectItem[];
    public form: FormGroup;
    public file: any;
    public file_src: any;
    constructor(
        public i18nService: I18nService,
        public mision: Mision,
        public msgService: MessagesService,
        public misionService: MisionService,
        public kc: KeycloakService
    ) {

    }

    ngOnInit() {
        this.mision.misionVerContenido = new MisionVerContenido();
        //se obtienen los datos del contenido de la mision
        this.obtenerContenido();
        this.itemsTipo = this.i18nService.getLabels("misiones-contenido-itemsTipo");
        this.form = new FormGroup({
            "tipo": new FormControl('', Validators.required),
            "url": new FormControl(''),
            "texto": new FormControl('', Validators.required),
        });
    }
    /**
     * Obtiene los datos de el reto de ver contenido
     */
    obtenerContenido() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.obtenerContenido(this.mision.idMision).subscribe(
                    (data: MisionVerContenido) => {
                        this.mision.misionVerContenido = data;
                        this.file_src = this.mision.misionVerContenido.url;
                    }, (error: Response) => {
                        if (error.status == 404) {
                            this.mision.misionVerContenido = new MisionVerContenido();
                            this.mision.misionVerContenido.indTipo = 'U';
                            return;
                        }

                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Crea un contenido especifico para la mision de ver contenido en caso que el contenido no exista (nueva mision)
     * para lo cual llama al metodo de actualizarContenido, el cual sirve a su vez para editar y crear contenido
     */
    crearContenido() {
        this.actualizarContenido();
    }

    /**
     * Actualiza los datos del reto de la mision de tipo ver contenido
     */
    actualizarContenido() {
        if (this.mision.misionVerContenido.url == "" || this.mision.misionVerContenido.url == undefined) {
            if (this.mision.misionVerContenido.indTipo == "U" || this.mision.misionVerContenido.indTipo == "V") { 
                this.msgService.showMessage({ detail: "Debe ingresar una url para continuar.", severity: "warn", summary: "" });
            }
            if (this.mision.misionVerContenido.indTipo == "I") {
                this.msgService.showMessage({ detail: "Debe seleccionar una imagen para continuar.", severity: "warn", summary: "" });
            }
            return;
        }
        try {
            //let a = new URL(this.mision.misionVerContenido.url);
            this.kc.getToken().then(
                (tkn: string) => {
                    this.misionService.token = tkn;//actualiza el token de authenticacion
                    let sub = this.misionService.actualizarContenido(this.mision.idMision, this.mision.misionVerContenido).subscribe(
                        (response: Response) => {
                            this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                            this.obtenerContenido();
                        }, (error) => { this.manejaError(error); },
                        () => { sub.unsubscribe(); }
                    );
                });
        } catch (e) { this.msgService.showMessage(this.i18nService.getLabels("error-url-invalido")); }
    }

    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {
        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            // Obtener el (base64 de la imagen)
            img.src = event.target.result;
            this.mision.misionVerContenido.url = event.target.result;
            this.mision.misionVerContenido.url = this.mision.misionVerContenido.url.split(",")[1];
            // Redimensionar la imagen
            this.file_src = this.resize(img);
        }, false);

        reader.readAsDataURL(input.files[0]);
    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");

        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        return dataUrl;
    }

    /*
  * Muestra un mensaje de error al usuario con los datos de la transaccion
  * Recibe: una instancia de request con la informacion de error
  * Retorna: un observable que proporciona información sobre la transaccion
  */
    manejaError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}