import { I18nService } from '../../../service';
import { MessagesService, ErrorHandlerService } from '../../../utils/index';
import { KeycloakService, MisionService, AtributoDinamicoService } from '../../../service/index';
import { Mision, AtributoDinamico, MisionAtrPerfil } from '../../../model/index';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { Observable } from 'rxjs/Rx';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de realizar mantenimiento de dats de los retos de perfil
 * un reto de perfil es un reto en el cual elmimbro debe actualizar su perfil
 * en este componente se definen los metadatos necesarios para definir los elementos que debe actualizar el miembro
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-perfil',
    templateUrl: 'misiones-perfil.component.html',
    providers: [AtributoDinamicoService, AtributoDinamico, MisionAtrPerfil]
})
export class MisionesPerfilComponent implements OnInit {
    public displayInsertar: boolean;
    public listaAtributos: any[];
    public atributoEliminar: MisionAtrPerfil;
    public form: FormGroup;
    public seleccMultipleAtributos: any[];
    public displayEliminar: boolean;
    constructor(
        public i18nService: I18nService,
        public mision: Mision,
        public msgService: MessagesService,
        public misionService: MisionService,
        public kc: KeycloakService,
        public atributoInsertar: MisionAtrPerfil,
        public atributoService: AtributoDinamicoService
    ) {
        this.form = new FormGroup({
            "selectAtributo": new FormControl('', [Validators.required]),
            "requerido": new FormControl('', [Validators.required]),
            "selectAtributoDinamico": new FormControl('')
        });
    }

    ngOnInit() {

        this.listaAtributos = [];
        this.obtenerAtributosMisionPerfil();
        this.mision.misionPerfilAtributoList = [];
    }
    /**
     * Obtiene los atributos que conforman el reto de actualizar perfil
     */
    obtenerAtributosMisionPerfil() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.obtenerAtributosMisionPerfil(this.mision.idMision).subscribe(
                    (data: MisionAtrPerfil[]) => {
                        this.mision.misionPerfilAtributoList = data;
                        //this.mapeoSelectAtrubutos();

                    }, (error) => {
                        this.manejaError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Encargado de filtrar la lista de atributos que se obtienen del api para evitar mostrar todos los elementos al cliente
     */
    filtradoListaAtributos() {
        this.mision.misionPerfilAtributoList.forEach(element => {
            this.listaAtributos.forEach((attr, idx) => {
                if (attr.indAtributoDinamico && (element.idAtributoDinamico == attr.idAtributoDinamico)) {
                    this.listaAtributos.splice(idx, 1);
                }
                if (attr.indAtributo != 'R' && (element.indAtributo == attr.indAtributo)) {
                    this.listaAtributos.splice(idx, 1);
                }
            })

        })
    }
    /**
     * Agrega un atributo a la lista de atributos que el miembro debe actualizar del perfil
     */
    agregarAtributo(misionAtrPerfil: any) {
        let nuevoAtributo: MisionAtrPerfil = new MisionAtrPerfil();
        if (misionAtrPerfil.indRequerido == undefined) {
            nuevoAtributo.indRequerido = false;
        }

        nuevoAtributo.idAtributoDinamico = misionAtrPerfil.idAtributoDinamico;

        nuevoAtributo.indRequerido = misionAtrPerfil.indRequerido;
        nuevoAtributo.indAtributo = misionAtrPerfil.indAtributo;
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;//actualiza el token de authenticacion
                let sub = this.misionService.crearAtributoMisionPerfil(this.mision.idMision, nuevoAtributo).subscribe(
                    (response: Response) => {
                        this.obtenerAtributosMisionPerfil();
                        // this.mapeoSelectAtrubutosDinamicos();
                        this.displayInsertar = false;
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                    }, (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**
     * Remueve un atributo de la lista de atributos que el miembro debe actualizar de su perfil
     */
    eliminarAtributo() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;//actualiza el token de authenticacion
                let sub = this.misionService.removerAtributoMisionPerfil(this.mision.idMision, this.atributoEliminar).subscribe(
                    (response: Response) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                        this.obtenerAtributosMisionPerfil();
                        this.displayEliminar = false;
                    }, (error) => {
                        this.manejaError(error);
                        this.displayEliminar = false;
                    },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    /**
     * Agrega una lista de atributos a la lista de atributos de miembro 
     * para actualizar en la mision
     */
    agregarListaAtributos() {
        // let listaAtributosInsertar: MisionAtrPerfil[] = [];
        // this.seleccMultipleAtributos.forEach(element => {
        //     let atributoTemp: MisionAtrPerfil = new MisionAtrPerfil();
        //     atributoTemp.indAtributo = element.indAtributo;
        //     if (atributoTemp.indAtributo == "R") {
        //         atributoTemp.idAtributoDinamico = element.idAtributoDinamico;
        //     }
        //     atributoTemp.indRequerido = element.indRequerido;
        //     listaAtributosInsertar.push(atributoTemp);
        // });
        // this.kc.getToken().then(
        //     (tkn:string) => {
        //         this.misionService.token = tkn;//actualiza el token de authenticacion
        //         let sub = this.misionService.(this.mision.idMision, misionAtrPerfil).subscribe(
        //             (response: Response) => {
        //                 this.msgService.mostrarInfo("Insertado", "El atributo ha sido insertado");
        //             }, (error) => { this.manejaError(error); },
        //             () => { sub.unsubscribe(); }
        //         );
        //     });
    }

    /**
     * Mapea los valores del select para los items de select de atributos
     */
    mapeoSelectAtrubutos() {
        this.listaAtributos = this.i18nService.getLabels("misiones-perfil-atributos");
    }
    mapeoSelectAtrubutosDinamicos() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.atributoService.token = tkn;
                //let sub = this.atributoService.busquedaAtributos("", "T N B").subscribe(
                let sub = this.atributoService.busquedaAtributosDinamicos(20, 0, ['P'], ["T", "N", "B"]).subscribe(
                    (data: Response) => {
                        this.listaAtributos = [];
                        let lista = data.json();
                        this.mapeoSelectAtrubutos();
                        lista.forEach((element) => {
                            this.listaAtributos = [...this.listaAtributos, { indAtributo: 'R', indRequerido: false, idAtributoDinamico: element.idAtributo, label: element.nombre }];
                        });
                        this.filtradoListaAtributos();
                        this.displayInsertar = true;
                    },
                    (error) => { this.manejaError(error) },
                    () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /*
  * Muestra un mensaje de error al usuario con los datos de la transaccion
  * Recibe: una instancia de request con la informacion de error
  * Retorna: un observable que proporciona información sobre la transaccion
  */
    manejaError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /**
     * Establece cual elemento se va a eliminar y muestraa el modal de eliminar elemento
     */
    mostrarModalEliminarAtributo(atrEliminar: MisionAtrPerfil) {
        this.atributoEliminar = atrEliminar;
        this.displayEliminar = true;
    }

}