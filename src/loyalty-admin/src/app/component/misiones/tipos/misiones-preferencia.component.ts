import { Mision, Preferencia } from '../../../model';
import { ErrorHandlerService, MessagesService } from '../../../utils';
import { KeycloakService, MisionService } from '../../../service';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
@Component({
    moduleId: module.id,
    selector: 'misiones-preferencia',
    templateUrl: 'misiones-preferencia.component.html'
})

export class MisionesPreferenciaComponent implements OnInit {
    public displayInsertar: boolean;
    public listaPreferencias: Preferencia[];
    public listaPreferenciasAsociadas: Preferencia[]
    
    constructor(public mision: Mision,
        private misionService: MisionService,
        private kc: KeycloakService,
        private msgService: MessagesService) {

    }

    ngOnInit() {
        this.obtenerPreferenciasAsociadas();
    }

    obtenerPreferenciasAsociadas() {
        this.kc.getToken().then((tkn) => {
            this.misionService.token=tkn;
            let sub = this.misionService.obtenerListaPreferenciasAsociadas(this.mision.idMision).subscribe(
                (data: Preferencia[]) => {
                    this.listaPreferenciasAsociadas = data;
                  
                }, (error: Response) => {
                    this.handleError(error);
                }, () => {
                    sub.unsubscribe();
                }
            );

        });
    }
    obtenerPreferenciasDisponibles() {
        this.kc.getToken().then((tkn) => {
            this.misionService.token=tkn;
            let sub = this.misionService.obtenerListaPreferenciasDisponibles(this.mision.idMision).subscribe(
                (data: Preferencia[]) => {
                    this.listaPreferencias = data;
                      this.displayInsertar=true;
                }, (error: Response) => {
                    this.handleError(error);
                }, () => {
                    sub.unsubscribe();
                }
            );
        });
    }

    agregarPreferencias(preferencias:Preferencia[]) {
         this.kc.getToken().then((tkn) => {
            this.misionService.token=tkn;
            let sub = this.misionService.asociarPreferencias(preferencias.map((element:Preferencia)=>{return element.idPreferencia;}),this.mision.idMision).subscribe(
                (data: any) => {
                    this.obtenerPreferenciasAsociadas();
                    this.displayInsertar=false;
                }, (error: Response) => {
                    this.handleError(error);
                }, () => {
                    sub.unsubscribe();
                }
            );
        });
    }

    removerPreferencias(preferencias:Preferencia[]) {
         this.kc.getToken().then((tkn) => {
            this.misionService.token=tkn;
            let sub = this.misionService.removerPreferencias(preferencias.map((element:Preferencia)=>{return element.idPreferencia;}),this.mision.idMision).subscribe(
                (data: any) => {
                    this.obtenerPreferenciasAsociadas();
                    this.displayInsertar=false;
                }, (error: Response) => {
                    this.handleError(error);
                }, () => {
                    sub.unsubscribe();
                }
            );
        });
    }

    actualizarReto() {
        let sub=this.misionService.actualizarRetoPreferencia().subscribe(
                (data: Preferencia[]) => {
                    this.listaPreferenciasAsociadas = data;
                }, (error: Response) => {
                    this.handleError(error);
                }, () => {
                    sub.unsubscribe();
                }
            );
    }
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}