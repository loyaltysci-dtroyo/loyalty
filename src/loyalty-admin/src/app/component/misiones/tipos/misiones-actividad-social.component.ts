import { I18nService } from '../../../service';

import { AuthGuard } from '../../common/index';
import { MisionService, KeycloakService } from '../../../service/index';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Mision, MisionSocial } from '../../../model/index';
import { SelectItem } from 'primeng/primeng';
import { MessagesService, ErrorHandlerService } from '../../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'misiones-actividad-social',
    templateUrl: 'misiones-actividad-social.component.html'
})
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de registrar los metadatos necesarios para la definicion del reto de actividad social
 * el cual consiste en actividades que se realizaran en redes sociales como share o post
 */
export class MisionesActividadSocialComponent implements OnInit {
    public optionsTiposActividad: SelectItem[];
    public formMSG: FormGroup;
    public formGRAL: FormGroup;
    public formELC: FormGroup;
    public formMG: FormGroup;
    public displayInsertar: boolean;
    public file_src: string;
    public file: any;
    public itemsRedSocial: SelectItem[];
    public redSocial: string = "F";
    public facePng: string = "../../../resources/img/facebook.png";
    public twitterPng: string = "../../../resources/img/twitter.png";
    public instagramPng: string = "../../../resources/img/instagram.png";
    constructor(
        public mision: Mision,
        public i18nService:I18nService,
        public misionService: MisionService,
        public authGuard: AuthGuard,
        public kc: KeycloakService,
        public msgService: MessagesService
    ) {
    }

    ngOnInit() {
        this.itemsRedSocial = [{value: 'F', label: 'Facebook'}, {value: 'T', label: 'Twitter'}, {value: 'I', label: 'Instagram'}]
        this.obtenerMision();
        this.mapeoSelect();
        this.formGRAL = new FormGroup({
            "tipo": new FormControl("", Validators.required)
        });
        this.formELC = new FormGroup({
            "urlObjetivo": new FormControl("", [Validators.required]),
            "mensaje": new FormControl("", Validators.required),
            "urlTitulo": new FormControl("", [Validators.required])
        });
        this.formMSG = new FormGroup({
            "mensaje": new FormControl("", Validators.required),
        });
        this.formMG = new FormGroup({
            "urlObjetivo": new FormControl("", [Validators.required]),
        });
    }

    /**
     * Encargado de obtener una mision del API, carga los detos de la mision en la instancia local de mision
     */
    public obtenerMision() {
        this.kc.getToken().then((tkn: string) => { this.misionService.token = tkn });
        let sub = this.misionService.getMision(this.mision.idMision).subscribe(
            (data: Mision) => {
                this.mision = data;
                this.obtenerReto();
            }, (error: Response) => {
                this.manejaError(error);
            }, () => {
                sub.unsubscribe();
            }
        );
    }
    /**
     * Encargado de obtener llos metadatos del reto del API, carga los datos en el atributo de mision social en la insancia de mision
     */
    public obtenerReto() {
        this.kc.getToken().then((tkn: string) => { this.misionService.token = tkn });
        let sub = this.misionService.obtenerRetoRedSocial(this.mision.idMision).subscribe(
            (data: MisionSocial) => {
                this.mision.misionSocial = data;
                this.file_src = this.mision.misionSocial.urlImagen;
                if(this.mision.misionSocial.indTipo == "L" || this.mision.misionSocial.indTipo == "G" || this.mision.misionSocial.indTipo == "M"){
                    this.redSocial = "F"
                }
            }, (error: Response) => {
                if (error.status == 404) {
                    
                        this.mision.misionSocial = new MisionSocial();
                   
                } else {
                    this.manejaError(error);
                }
            }, () => {
                sub.unsubscribe();
            }
        );
    }
    /**
     * Encargado de actualizar los metadatos del reto de la mision en el API
     */
    public actualizarReto() {
        this.kc.getToken().then((tkn: string) => { this.misionService.token = tkn });
        if (this.mision.misionSocial.urlImagen.split(",").length > 1) {
            this.mision.misionSocial.urlImagen = this.mision.misionSocial.urlImagen.split(",")[1];
        }
        let sub = this.misionService.modificarRetoRedSocial(this.mision.idMision, this.mision.misionSocial).subscribe(
            (data: MisionSocial) => {
                // this.mision.misionSocial = data;
                this.msgService.showMessage(this.i18nService.getLabels("general-insercion"))
                this.obtenerReto();
            }, (error: Response) => {
                this.manejaError(error);
            }, () => {
                sub.unsubscribe();
            }
        );
    }

    crearReto() {
        this.kc.getToken().then((tkn: string) => { this.misionService.token = tkn });
        if (this.mision.misionSocial.urlImagen.split(",").length > 1) {
            this.mision.misionSocial.urlImagen = this.mision.misionSocial.urlImagen.split(",")[1];
        }
        let sub = this.misionService.crearRetoRedSocial(this.mision.idMision, this.mision.misionSocial).subscribe(
            (data: MisionSocial) => {
                this.obtenerReto();
            }, (error: Response) => {
                this.manejaError(error);
            }, () => {
                sub.unsubscribe();
            }
        );
    }

    /**
     * Encargado de inicializar los datos de los select
     */
    public mapeoSelect() {
        this.optionsTiposActividad =this.i18nService.getLabels("misiones-actividad-social-optionsTiposActividad");
    }
    /**
     * Maneja los errores, en concreto redirige la excepcion al service de mensajeria tomando como parametro el manejo de error que realiza el service de manejo de errores
     */
    public manejaError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
    /* Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form, este Metodo
       *  Es invocado desde el template de este componente 
       */
    fileChange(input) {

        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            // Obtener el (base64 de la imagen)
            img.src = this.mision.misionSocial.urlImagen = event.target.result;
            // Redimensionar la imagen
            this.file_src = this.resize(img);
        }, false);

        reader.readAsDataURL(input.files[0]);


    }
    /*Metodo encargado de redimensionar una imagen
     Recibe: la imagen a redimensionar y las dimensiones, que en este caso 
     estan por defecto en 900*900
     Retorna: un elemento img con la imagen redimensionada
     */
    resize(img, MAX_WIDTH: number = 900, MAX_HEIGHT: number = 900) {
        let canvas = document.createElement("canvas");

        // console.log("Size Before: " + img.src.length + " bytes");

        let width = img.width;
        let height = img.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        } else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");

        ctx.drawImage(img, 0, 0, width, height);

        let dataUrl = canvas.toDataURL('image/jpeg');
        // IMPORTANT: 'jpeg' NOT 'jpg'
        // console.log("Size After:  " + dataUrl.length + " bytes");
        return dataUrl
    }



}