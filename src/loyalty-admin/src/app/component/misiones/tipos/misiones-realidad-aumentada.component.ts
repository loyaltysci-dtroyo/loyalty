import { I18nService } from '../../../service';
import { AuthGuard } from '../../common/index';
import { MisionService, KeycloakService } from '../../../service/index';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Mision } from '../../../model/index';
import { SelectItem } from 'primeng/primeng';
import { MessagesService, ErrorHandlerService } from '../../../utils/index';
import { MisionRealidadAumentada } from 'app/model/mision-realidad-aumentada';

@Component({
    selector: 'misiones-realidad-aumentada',
    templateUrl: 'misiones-realidad-aumentada.component.html'
})

export class MisionesRealidadAumentada implements OnInit {
    constructor(
        public mision: Mision,
        public i18nService:I18nService,
        public misionService: MisionService,
        public authGuard: AuthGuard,
        public kc: KeycloakService,
        public msgService: MessagesService
    ) { }

    ngOnInit() {
        this.obtenerMision();
    }

     /**
     * Encargado de obtener una mision del API, carga los detos de la mision en la instancia local de mision
     */
    public obtenerMision() {
        this.kc.getToken().then((tkn: string) => { this.misionService.token = tkn });
        let sub = this.misionService.getMision(this.mision.idMision).subscribe(
            (data: Mision) => {
                this.mision = data;
                this.obtenerReto();
            }, (error: Response) => {
                this.manejaError(error);
            }, () => {
                sub.unsubscribe();
            }
        );
    }
    /**
     * Encargado de obtener llos metadatos del reto del API, carga los datos en el atributo de mision social en la insancia de mision
     */
    public obtenerReto() {
        this.kc.getToken().then((tkn: string) => { this.misionService.token = tkn });
        let sub = this.misionService.obtenerRetoRealidadAumentada(this.mision.idMision).subscribe(
            (data: MisionRealidadAumentada) => {
                this.mision.misionRealidadAumentada = data;
            }, (error: Response) => {
                if (error.status == 404) {
                        this.mision.misionRealidadAumentada = new MisionRealidadAumentada();
                        console.log(this.mision.misionRealidadAumentada);
                } else {
                    this.manejaError(error);
                }
            }, () => {
                sub.unsubscribe();
            }
        );
    }
    /**
     * Encargado de actualizar los metadatos del reto de la mision en el API
     */
    public actualizarReto() {
        this.kc.getToken().then((tkn: string) => { this.misionService.token = tkn });
        
        let sub = this.misionService.modificarRetoRealidadAumentada(this.mision.idMision, this.mision.misionRealidadAumentada).subscribe(
            (data: MisionRealidadAumentada) => {
                this.msgService.showMessage(this.i18nService.getLabels("general-insercion"))
                this.obtenerReto();
            }, (error: Response) => {
                this.manejaError(error);
            }, () => {
                sub.unsubscribe();
            }
        );
    }

    /**
     * Maneja los errores, en concreto redirige la excepcion al service de mensajeria tomando como parametro el manejo de error que realiza el service de manejo de errores
     */
    public manejaError(error: Response) {
        console.log(error);
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }


}