import { I18nService } from '../../../service';
import { MessagesService, ErrorHandlerService } from '../../../utils/index';
import { KeycloakService, MisionService } from '../../../service/index';
import { Mision, MisionEncuestaPregunta } from '../../../model/index';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { FormGroup, AbstractControl, ValidatorFn, FormControl, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { ViewChild } from '@angular/core';
import { AppConfig } from '../../../app.config';
/**
 * Flecha Roja Technologies
 * Fernnado Aguilar
 * Comonente encargado de realizar mantenimiento de datos de las misiones de encuesta en el sistema
 * una mision de encuesta puede ser una mision de pregunta de opinion o de un conjunto de preferencias(preferencia)
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-encuesta',
    templateUrl: 'misiones-encuesta.component.html',
    providers: [MisionEncuestaPregunta]
})
export class MisionesEncuestaComponent implements OnInit {

    public displayInsertar: boolean;//despliega el modal de insertar 
    public displayEditar: boolean;//despliega el modal de editar preguntas   
    public displayEliminar: boolean;//despliega el modal de eliminar preguntas  
    public itemsTipoRespuesta: SelectItem[];//tipos de respuesta 
    public itemsRespuestaCorrectaMultiple: SelectItem[];
    public itemsTipoPregunta: SelectItem[];// tipos de pregunta, pregutna o calificacion
    public respuestaTemp: string;//para efectos de insertar una nueva respuesta
    public itemsRespuestaCorrecta: SelectItem[];
    public itemsRespuestaCorrectaCalificacion: SelectItem[];
    public formGral: FormGroup;
    public formC: FormGroup;
    public formP: FormGroup;
    public listaRespuestasInsertar: string[] = [];
    public listaRespuestasEditar: string[] = [];
    public listaRespuestasCorrectas: string[] = [];
    public indRespuestaCorrectaCalificacion: boolean;//true= la calificacion a insertar admite respuesta correcta
    public file_src: string;
    public file: any;
    public subiendoImagen: boolean;
    public imgDefault: string;
    public accion: boolean = false;
    public rating: number;

    @ViewChild('inp') inputImg: any;

    constructor(
        public i18nService: I18nService,
        public preguntaInsertar: MisionEncuestaPregunta,//pregunta temporal para insertar
        public preguntaEditar: MisionEncuestaPregunta,//temporal para almacenar la pregunta a editar
        public preguntaEliminar: MisionEncuestaPregunta,//temporal para almacenar la pregunta a eliminar
        public mision: Mision,//contiene los valores de mision
        public msgService: MessagesService,
        public misionService: MisionService,
        public kc: KeycloakService,
    ) {
        this.subiendoImagen = false;
        this.indRespuestaCorrectaCalificacion = false;
    }

    ngOnInit() {

        this.listaRespuestasCorrectas = [];
        this.listaRespuestasInsertar = [];
        this.listaRespuestasEditar = [];

        //se obtienen los datos del contenido de la mision
        this.obtenerPreguntasEncuesta();
        this.formGral = new FormGroup({
            "pregunta": new FormControl('', [Validators.required]),
            "tipoPregunta": new FormControl('', [Validators.required]),
        });
        this.formP = new FormGroup({
            "indRespuesta": new FormControl('', Validators.required),
            "indRespuestaCorrecta": new FormControl('')
        });
        this.formC = new FormGroup({
            "indComentarios": new FormControl('', Validators.required),
            "indRespuesta": new FormControl('', Validators.required),
            "indRespuestaCorrecta": new FormControl('')
        });
        this.displayEditar = false;
        this.displayInsertar = false;
        this.displayEliminar = false;
        this.itemsRespuestaCorrectaCalificacion = this.i18nService.getLabels("misiones-contenido-itemsRespuestaCorrectaCalificacion");
        this.itemsTipoPregunta = this.i18nService.getLabels("misiones-contenido-itemsTipoPregunta");
        this.itemsTipoRespuesta = this.i18nService.getLabels("misiones-contenido-itemsTipoRespuesta");
        this.itemsRespuestaCorrecta = this.i18nService.getLabels("misiones-contenido-itemsRespuestaCorrecta");
        this.itemsRespuestaCorrectaMultiple = this.i18nService.getLabels("misiones-contenido-itemsRespuestaCorrectaMultiple");
        this.preguntaInsertar.imagen = undefined;
        this.preguntaEditar.imagen = undefined;
        this.file_src = `${AppConfig.DEFAULT_IMG_URL}`;
    }

    resetFields() {
        this.inputImg.nativeElement.value = "";
        this.preguntaInsertar = new MisionEncuestaPregunta();
        this.preguntaEditar = new MisionEncuestaPregunta();
        this.listaRespuestasCorrectas = [];
        this.listaRespuestasInsertar = [];
        this.listaRespuestasEditar = [];
        this.preguntaInsertar.imagen = undefined;
        this.preguntaEditar.imagen = undefined;
        this.file_src = `${AppConfig.DEFAULT_IMG_URL}`;
    }
    /**
     * Obtiene las preguntas/calificaciones del cuestionario previamente asociadas, si no hy preguntas
     * retorna un listado vacio
     */
    obtenerPreguntasEncuesta() {
        this.kc.getToken().then((tkn) => {
            this.misionService.token = tkn;
            let subs = this.misionService.obtenerPreguntasEncuesta(this.mision.idMision).subscribe(
                (response: Response) => {
                    this.mision.misionEncuestaPreguntaList = <MisionEncuestaPregunta[]>response.json();
                    // console.log(this.mision.misionEncuestaPreguntaList);
                    if (this.mision.indEstado == 'B' || this.mision.indEstado == 'A') {
                        this.accion = true;
                    }
                }, (error) => {
                    this.manejaError(error);
                });
        });
    }
    /**Muestra un modal para que el usuario pueda editar una pregunta */
    displayModalEditar(pregunta: MisionEncuestaPregunta) {
        if (pregunta.indTipoPregunta == 'E' && (pregunta.indTipoRespuesta == "RM" || pregunta.indTipoRespuesta == "RU")) {
            this.listaRespuestasEditar = pregunta.respuestas.split("\n");
        }

        this.listaRespuestasEditar.splice(this.listaRespuestasEditar.length, 1);
        this.preguntaEditar = pregunta;
        if (this.preguntaEditar.indTipoPregunta == 'C') {
            if (this.preguntaEditar.respuestasCorrectas) {
                this.indRespuestaCorrectaCalificacion = true;
            }
            this.rating = parseInt(this.preguntaEditar.respuestasCorrectas);
        }
        try {
            if (this.preguntaInsertar.indTipoPregunta == "E") {
                this.preguntaEditar.respuestasCorrectas.split("\n").forEach((element) => {
                    this.listaRespuestasCorrectas = [...this.listaRespuestasCorrectas, this.listaRespuestasEditar[Number.parseInt(element) - 1]];
                })
            }

        } catch (Err) {
            console.log("Error al parsear el indice de respuesta");
        }

        this.file_src = pregunta.imagen;
        this.displayEditar = true;
    }

    mostrarModalEliminarPregunta(pregunta: MisionEncuestaPregunta) {
        this.preguntaEliminar = pregunta;
        this.displayEliminar = true;
    }
    /**Inserta una respuesta en la lista de respuestas */
    insertarRespuestaLista(respuesta: string) {
        this.respuestaTemp = "";
        if (respuesta.trim().length > 0) {
            this.listaRespuestasInsertar = [...this.listaRespuestasInsertar, respuesta];
        }
    }
    /**Elimina una respuesta de la lista de respuestas */
    eliminarRespuesta(respuesta) {
        this.respuestaTemp = "";
        let index = this.listaRespuestasInsertar.findIndex(element => respuesta == element);
        this.listaRespuestasInsertar.splice(index, 1);
        this.listaRespuestasInsertar = [... this.listaRespuestasInsertar];

    }
    /**Elimina una respuesta de la lista de respuestas */
    eliminarRespuestaEditar(respuesta) {
        this.respuestaTemp = "";
        let index = this.listaRespuestasEditar.findIndex(element => respuesta == element);
        this.listaRespuestasEditar.splice(index, 1);
        this.listaRespuestasEditar = [...this.listaRespuestasEditar];
    }
    /**Inserta una respuesta en la lista de respuestas */
    insertarRespuestaListaEditar(respuesta: string) {
        this.respuestaTemp = "";
        if (respuesta.trim().length > 0) {
            this.listaRespuestasEditar = [...this.listaRespuestasEditar, respuesta];
        }
    }
    /**
     * Inserta una pregunta en el sistema asociada a esta miforEachsion
     */
    insertarPregunta() {
        this.preguntaInsertar.respuestas = "";
        this.preguntaInsertar.respuestasCorrectas = ""
        if (this.preguntaInsertar.indTipoPregunta == "E" && (this.preguntaInsertar.indTipoRespuesta == "RM" || this.preguntaInsertar.indTipoRespuesta == "RU") && this.listaRespuestasInsertar.length >0) { //&& this.listaRespuestasInsertar.length < 2
            /*this.msgService.showMessage(this.i18nService.getLabels("warn-atributo-respuesta"));
            return;
        } else {*/
            this.preguntaInsertar.respuestas = this.listaRespuestasInsertar.join("\n");
            this.preguntaInsertar.respuestasCorrectas = this.listaRespuestasCorrectas.map(
                (element) => {
                    return this.preguntaInsertar.respuestas
                        .split("\n")
                        .findIndex((itm) => { return element == itm }) + 1;
                }).join("\n");
        }
        if (this.preguntaInsertar.indTipoPregunta == 'E' && (this.preguntaInsertar.indTipoRespuesta == "RT" || this.preguntaInsertar.indTipoRespuesta == "RN")) {
            this.preguntaInsertar.indRespuestaCorrecta = "T";
            this.preguntaInsertar.respuestas = undefined;
            this.preguntaInsertar.respuestasCorrectas = undefined;
        }
        //si es encuesta y es de tipo respuesta multiple o respuesta unica y tiene mas de una respuesta, o bien es calificacion
        if (this.preguntaInsertar.indTipoPregunta == "C") {
            if (this.indRespuestaCorrectaCalificacion) {
                this.preguntaInsertar.respuestasCorrectas = this.rating + "";
            } else {
                this.preguntaInsertar.indRespuestaCorrecta = 'T';
            }
            this.preguntaInsertar.respuestas = undefined;
        }
        if (this.preguntaInsertar.indTipoPregunta == "E") {
            this.preguntaInsertar.indComentario = false;
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.subiendoImagen = true;
                this.msgService.showMessage(this.i18nService.getLabels("info-uploading"));
                this.misionService.token = tkn;//actualiza el token de autenticacion
                let sub = this.misionService.crearPreguntaEncuesta(this.mision.idMision, this.preguntaInsertar).subscribe(
                    (response: Response) => {
                        this.resetFields();
                        this.obtenerPreguntasEncuesta();
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.displayInsertar = false;
                        this.subiendoImagen = false;
                        this.rating = 0;
                        this.listaRespuestasCorrectas = [];
                    }, (error) => { this.manejaError(error); this.subiendoImagen = false; },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    /**
    * Edita una pregunta en el sistema asociada a esta mision 
    */
    editarPregunta() {
        this.preguntaEditar.respuestas = "";
        this.preguntaEditar.respuestasCorrectas = "";
        if (this.preguntaEditar.indTipoPregunta == "E" && (this.preguntaEditar.indTipoRespuesta == "RM" || this.preguntaEditar.indTipoRespuesta == "RU") && this.listaRespuestasEditar.length > 0) {
            /*this.msgService.showMessage(this.i18nService.getLabels("warn-atributo-respuesta"));
            return;
        } else {*/
            this.preguntaEditar.respuestas = this.listaRespuestasEditar.join("\n");
            this.preguntaEditar.respuestasCorrectas = this.listaRespuestasCorrectas.map(
                (element) => {
                    return this.preguntaEditar.respuestas.split("\n").findIndex(
                        (itm) => { return element == itm }) + 1
                }
            ).join("\n");
        }
        if (this.preguntaEditar.indTipoPregunta == 'E' && (this.preguntaEditar.indTipoRespuesta == "RT" || this.preguntaEditar.indTipoRespuesta == "RN")) {
            this.preguntaEditar.indRespuestaCorrecta = "T";
            this.preguntaEditar.respuestas = undefined;
            this.preguntaEditar.respuestasCorrectas = undefined;
        }
        if (this.preguntaEditar.indTipoPregunta == "C") {
            if (this.indRespuestaCorrectaCalificacion) {
                this.preguntaEditar.respuestasCorrectas = this.rating + "";
            } else {
                this.preguntaEditar.indRespuestaCorrecta = 'T';
            }
            this.preguntaEditar.respuestas = undefined;
        }
        if (this.preguntaEditar.indTipoPregunta == "E") {
            this.preguntaEditar.indComentario = false;
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;//actualiza el token de autenticacion
                let sub = this.misionService.editarPreguntasEncuesta(this.mision.idMision, this.preguntaEditar).subscribe(
                    (response: Response) => {
                        this.resetFields();
                        this.obtenerPreguntasEncuesta();
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.displayEditar = false;
                        this.rating = 0;
                        this.listaRespuestasCorrectas = [];
                    }, (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }
    // Metodo que sera llamado cada vez que el usaurio seleccione una nueva imagen desde el form
    fileChange(input) {
        this.file = input.files[0];
        // Create an img element and add the image file data to it
        var img = document.createElement("img");
        img.src = window.URL.createObjectURL(input.files[0]);
        // Crear un FileReader
        let reader: any
        let target: EventTarget;

        reader = new FileReader();
        // Agregar un event listener para el cambio
        reader.addEventListener("load", (event) => {
            // Obtener el (base64 de la imagen)
            img.src = event.target.result;
            // Redimensionar la imagen
            this.file_src = event.target.result;
            this.preguntaInsertar.imagen = img.src.split(",")[1];
            this.preguntaEditar.imagen = img.src.split(",")[1];
        }, false);

        reader.readAsDataURL(input.files[0]);
    }
    /**
     * Elimina una pregunta en el sistema asociada a esta mision
     */
    eliminarPregunta() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.misionService.token = tkn;//actualiza el token de authenticacion
                let sub = this.misionService.eliminarPreguntasEncuesta(this.mision.idMision, this.preguntaEliminar.idPregunta).subscribe(
                    (response: Response) => {
                        this.obtenerPreguntasEncuesta();
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                        this.displayEliminar = false;
                    }, (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe(); }
                );
            });
    }

    /*
    * Muestra un mensaje de error al usuario con los datos de la transaccion
    * Recibe: una instancia de request con la informacion de error
    * Retorna: un observable que proporciona información sobre la transaccion
    */
    manejaError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}