import { I18nService } from '../../../service/i18n.service';
import { AuthGuard } from '../../common/index';
import { MisionService, KeycloakService } from '../../../service/index';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Mision, MisionSubirContenido } from '../../../model/index';
import { SelectItem } from 'primeng/primeng';
import { MessagesService, ErrorHandlerService } from '../../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'misiones-subir-contenido',
    templateUrl: 'misiones-subir-contenido.component.html'
})
/**
 * Componente encargado de registrar el reto para las misiones de actualizacion de contenido
 */
export class MisionesSubirContenidoComponent implements OnInit {
    public optionsTiposContenido: SelectItem[];
    public form: FormGroup;
    public displayInsertar: boolean;
    constructor(
        public i18nService: I18nService,
        public mision: Mision,
        public misionService: MisionService,
        public authGuard: AuthGuard,
        public kc: KeycloakService,
        public msgService: MessagesService
    ) {

    }

    ngOnInit() {
        this.obtenerMision();
        this.optionsTiposContenido = this.i18nService.getLabels("misiones-contenido-optionsTiposContenido");
        this.form = new FormGroup({
             "tipo":new FormControl("",Validators.required),
             "texto":new FormControl("",Validators.required)
        });
    }
    /**
     * Obtiene los datos de la mision de la base de datos
     */
    public obtenerMision() {
        let sub = this.misionService.getMision(this.mision.idMision).subscribe(
            (data: Mision) => {
                this.mision = data;
                this.obtenerReto();
            }, (error: Response) => {
                //this.manejaError(error);
            }, () => {
                sub.unsubscribe();
            }
        );
    }
    /**
     * Obtiene los metadatos del reto de subir contenido de la base de datos, en caso de que la definicion del reto no exista
     * en el API retoornara un  404 por lo tanto es necesario verificar este codigo de error y crear la definicion
     */
    public obtenerReto() {
        let sub = this.misionService.obtenerRetoSubirContenido(this.mision.idMision).subscribe(
            (data: MisionSubirContenido) => {
                this.mision.misionSubirContenido = data;
            }, (error: Response) => {
                if (error.status == 404) {
                    this.mision.misionSubirContenido= new MisionSubirContenido();
                } else {
                    this.manejaError(error);
                }
            }, () => {
                sub.unsubscribe();
            }
        );
    }
    /**
     * Actualiza la informacion del reto en el sistema
     */
    public actualizarReto() {

        let sub = this.misionService.actualizarRetoSubirContenido(this.mision.idMision, this.mision.misionSubirContenido).subscribe(
            (data: any) => {
                this.obtenerReto();
                this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
            }, (error: Response) => {
               // this.manejaError(error);
            }, () => {
                sub.unsubscribe();
            }
        );
    }
    /**
     * Poblado de los selects de 
     */
    public mapeoSelect() {
        this.optionsTiposContenido = this.i18nService.getLabels("misiones-contenido-optionsTiposContenido");
    }
    /**
     * Metodo encargado de manejar el error,  notifica al service de manejo de errores para que informe al usuario
     */
    public manejaError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}