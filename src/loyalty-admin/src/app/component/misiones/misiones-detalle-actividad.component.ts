import { I18nService } from '../../service/i18n.service';
import { ErrorHandlerService, MessagesService } from '../../utils';
import { Component, OnInit } from '@angular/core';
import { ActividadService, KeycloakService } from '../../service/index';
import { Mision, RegistroActividad } from '../../model/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';
import { PERM_LECT_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'misiones-detalle-actividad',
    templateUrl: 'misiones-detalle-actividad.component.html',
    providers:[ActividadService]
})

export class MisionesDetalleActividadComponent implements OnInit {
   public listaActividades: any[];
    /**Atributos para sorting/busqueda */
    public indPeriodo: string = '1M';
    public periodos: SelectItem[]=[];
    public lecturaMiembro: boolean; //Permiso de lectura sobre miembro

    constructor(
        public i18nService:I18nService,
        public mision: Mision,
        public actividadService: ActividadService,
        public msgService: MessagesService,
        public kc: KeycloakService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard
        ) {
            this.authGuard.canWrite(PERM_LECT_MIEMBROS).subscribe((permiso: boolean) => {
            this.lecturaMiembro = permiso;
        });
    }

    ngOnInit() {
        this.periodos=this.i18nService.getLabels("general-actividad-periodos")
        this.route.parent.params.subscribe((params) => {
            if (params['id']) {
                this.mision.idMision = params['id'];
                this.obtenerActividad();
            } else {
                // this.router.navigate(['/misions']);
            }
        });
    }

    /**
     * Obtiene los registros de actividad para el mision actual
     */
    obtenerActividad() {
        this.kc.getToken().then((tkn) => {
            this.actividadService.token = tkn;
            let sub = this.actividadService.getActividadesMision(this.mision.idMision,this.indPeriodo).subscribe(
                (response: RegistroActividad[]) => {
                    this.listaActividades = response;
                }, (error: Response) => {
                    this.handleError(error);
                }, () => {
                    sub.unsubscribe();
                }
            );
        }).catch();
    }
    /**
     * Redirige al administrador a la pantalla de dashboard para el elemento seleccionado
     */
    gotoDetail(idElemento: string) {
        this.router.navigate(['/miembros/detalleMiembro', idElemento]);
    }
    /**
     * Maneja el erro a nivel de componente
     */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}