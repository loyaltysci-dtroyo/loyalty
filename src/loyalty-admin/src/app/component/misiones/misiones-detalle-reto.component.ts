import { MessagesService } from '../../utils/index';
import { MisionService, KeycloakService } from '../../service/index';
import { Mision } from '../../model/index';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService } from "../../utils/index";
import {SelectItem} from 'primeng/primeng'
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Componente encargado de cargar los componentes de mentenimiento de datos de el reto de la mision,
 * es decir las acciones que los miembros deben hacer par a que sea retribuido el monto de la metrica 
 * seleccionada en la configuracion
 * 
 * Este componente alterna entre los componentes de reto dependiendo de el tipo de mision
 * determinado en los ajustes generales de la mision. Por lo tanto este componente no contiene
 * logica del mantenimiento de datos como tal sino que aloja el componente que realmente hace el 
 * mantenimiento de datos, el cual, puede variar en tiempo de ejecucion dependiendo del tipo de mision, 
 * los componentes de reto si se especializan en realizar ese mantenimiento 
 */
@Component({
    moduleId: module.id,
    selector: 'misiones-detalle-reto',
    templateUrl: 'misiones-detalle-reto.component.html'
})
export class MisionesDetalleRetoComponent implements OnInit {
  
    constructor(
        public mision: Mision,
        public kc: KeycloakService,
        public misionService: MisionService,
        public msgService: MessagesService
    ) { 

     
    }

    ngOnInit() {
        this.obtenerMision();
    }
    /**
     * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
     * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
     * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
     * no siempre sea necesario llamar al api para actualizar los datos
     */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }
    /**
     * Obtiene los metadatos de la mision del API, para lo cual usa la instancia de mision que provee la inyeccion de dependencias 
     */
    obtenerMision() {
        this.kc.getToken().then(
            (tkn:string) => {
                this.misionService.token = tkn;
                let sub = this.misionService.getMision(this.mision.idMision).subscribe(
                    (msn: Mision) => {
                        this.copyValuesOf(this.mision, msn);
                    },
                    (error) => this.manejaError(error),
                    () => { sub.unsubscribe(); }
                );
            });
    }

    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}