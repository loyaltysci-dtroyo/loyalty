import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Response, Headers } from '@angular/http';
import { ConfirmationService, SelectItem, MenuItem } from 'primeng/primeng';
import { DocumentoInventario, Ubicacion, Premio, CategoriaProducto } from '../../model/index';
import { DocumentoInventarioService, UbicacionService, PremioService, CategoriaProductoService, ReporteService, KeycloakService, I18nService } from '../../service/index';
import { REPORTES } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'reporte-component',
    templateUrl: 'reportes.component.html',
    providers: [DocumentoInventario, Ubicacion, Premio, UbicacionService, PremioService,
        CategoriaProducto, CategoriaProductoService, DocumentoInventarioService, ReporteService, ConfirmationService]
})
/**
 * Jaylin Centeno
 * Componente para la generación de reportes del sistema
 */
export class ReporteComponent {

    public escritura: boolean;//Permite verificar permisos (true/false)
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public mostrarReporte: boolean; //Habilita el cuadro de dialogo para ingresar los parámetros del reporte
    public busqueda: string = ""; //Representa los criterios de búsqueda
    public listaBusqueda: string[];  //Contiene los criterios de búsqueda ingresados en el Chip
    public dialogDocumento: boolean; //Habilita el cuadro de dialogo del reporte de documentos
    public dialogExistencia: boolean; //Habilita el cuadro de dialogo del reporte de existencias
    public dialogMovimiento: boolean; //Habilita el cuadro de dialogo del reporte de movimiento historico
    public dialogCarrito: boolean; //Habilita el cuadro de dialogo del reporte de carritos
    public avanzada: boolean; //Muestra la busqueda avanzada
    public formExistencia: FormGroup; //Form para controlar el reporte de existencias
    public formMovimiento: FormGroup; //Form para controlar el reporte de movimientos
    public formCarrito: FormGroup; //Form para controlar el reporte de movimientos
    public codigo:any;
    // ------- Existencias de premios -------- //
    public annoMax: number; //Año actual
    public annoMin: number; //Año minimo
    public mes: number; //Mes por el que se desea buscar 
    public periodo: number; //Representa el año

    // ------- Movimientos historicos -------- //
    public fechaInicio: Date; // Representa la fecha inicial del rango de busqueda
    public fechaFin: Date; // Representa la fecha final del rango de busqueda
    public tipoMovimiento: string; //Guarda el tipo de movimiento C=compra, R= redencion, I=interubicacion, A=ajuste más, M=ajuste menos
    public tiposItems: SelectItem[]; //Lista con los tipos de documento 

    //-------- Doc. inventario ----------//
    public docSelec: DocumentoInventario; //Documento seleccionado
    public idDocumento: string; //Id del documento seleccionado
    public listaDocumento: DocumentoInventario[]; //Documentos del sistema
    public listaVacia: boolean = false; //Permite verificar si la lista de documentos esta vacía
    public totalRecords: number; //Total de documentos en el sistema
    public cantidad: number = 5; //Cantidad de filas a mostrar en la tabla
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public documentoItems: SelectItem[] = []; //Lista para el dropdown con los documentos disponibles (abiertos/cerrados)
    public tipo: string[] = []; //Almacena los indicadores de tipo de documento para busqueda avanzada
    public estadoDoc: string = ""; //Representa el estado de los documentos a mostrar

    // --------- Ubicacion -------- //
    public listaUbicaciones: Ubicacion[]; //Ubicaciones del sistema
    public listaVaciaUbic: boolean = true; //Permite verificar si la lista de ubicaciones esta vacía
    public totalRecordsUbic: number; //Total de documentos en el sistema
    public cantidadUbic: number = 10; //Cantidad de filas a mostrar en la tabla
    public filaDesplazamientoUbic: number = 0; //Desplazamiento de la primera fila
    public idUbicacion: string; //Id de la ubicación por la que se desea filtrar la búsqueda 
    public ubicacion: Ubicacion;
    public ubicacionItems: SelectItem[] = []; //Lista para el dropdown con las ubicaciones disponibles 

    // -------- Premios ---------- // 
    public listaPremios: Premio[]; //Premios del sistema
    public listaVaciaPremio: boolean; //Permite verificar si lista de premios esta vacia
    public totalRecordsPremio: number; //Total de premios en el sistema 
    public cantidadPremio: number = 8; //Cantidad de filas a mostrar
    public filaPremio: number = 0; //Desplazamiento de la primera fila
    public idPremio: string; // Id del premio por la que se desea filtrar la búsqueda 
    public premio: Premio;
    public premioItems: SelectItem[] = []; //Lista para el dropdown con los premios disponibles

    // -------- CategoriaProductos ---------- // 
    public listaCategoriaProducto: CategoriaProducto[]; //Categorias de productos del sistema
    public listaVaciaCategoriaProducto: boolean; //Permite verificar si lista de categoriaProducto esta vacia
    public totalRecordsCategoriaProducto: number; //Total de CategoriaProductos en el sistema 
    public cantidadCategoriaProducto: number = 8; //Cantidad de filas a mostrar
    public filaCategoriaProducto: number = 0; //Desplazamiento de la primera fila
    public idCategoriaProducto: string; // Id del CategoriaProducto por la que se desea filtrar la búsqueda 
    public categoriaProducto: CategoriaProducto;
    public categoriaProductoItems: SelectItem[] = []; //Lista para el dropdown con los CategoriaProductos disponibles

    //------- Reportes -----------//
    public reporteCreado: boolean; //Habilita el cuadro de dialogo para generar el reporte
    public reporteGenerado: any; //Guarda el archivo 
    public cargando: boolean = true;;
    public repoA: boolean;
    public repoB: boolean;
    public repoC: boolean;
    public repoD: boolean;
    public repoE: boolean;

    constructor(
        public premioService: PremioService,
        public categoriaProductoService:CategoriaProductoService,
        public ubicacionService: UbicacionService,
        public documentoService: DocumentoInventarioService,
        public reporteService: ReporteService,
        public keycloakService: KeycloakService,
        public confirmationService: ConfirmationService,
        public authGuard: AuthGuard,
        public i18nService: I18nService,
        public msgService: MessagesService,
    ) {

        //Obtiene si el usuario tiene permisos para utilizar este recurso
        this.authGuard.canWrite(REPORTES).subscribe((permiso: boolean) => {
            this.escritura = permiso; //Valida los permisos de escritura del usuario
            this.cargando = false;
        });

        //Se obtiene los tipos de documento de invetario disponibles
        this.tiposItems = [];
        this.tiposItems = this.i18nService.getLabels("documento-inventario-tipo");

        let anno = new Date();
        this.annoMax = anno.getFullYear();
        this.annoMin = anno.getFullYear() - 40;

        //Formularios utilizados en la generación de reportes
        this.formExistencia = new FormGroup({
            'periodo': new FormControl('', Validators.required),
            'mes': new FormControl('', Validators.required)
        });

        this.formMovimiento = new FormGroup({
            'fechaInicio': new FormControl('', Validators.required),
            'fechaFin': new FormControl('', Validators.required),
            'tipo': new FormControl('')
        })

        this.formCarrito = new FormGroup({
            'fechaInicio': new FormControl('', Validators.required),
            'fechaFin': new FormControl('', Validators.required)
        })
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerDocumentos();
    }

    busquedaAvanzada() {
        this.avanzada = !this.avanzada;
    }

    /* Llama al método busqueda de documentos
     * Parámetros: cantidad, fila, tipo, estado, busqueda
     */
    obtenerDocumentos() {
        this.documentoItems = [];
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.documentoService.token = token;
                let idUbicacion = '';
                if (this.avanzada) {
                    if (this.ubicacion != undefined) {
                        idUbicacion = this.ubicacion.idUbicacion;
                    }
                }
                this.documentoService.obtenerDocumentosPorTerminos(this.cantidad, this.filaDesplazamiento, this.tipo, idUbicacion, this.estadoDoc).subscribe(
                    (response: Response) => {
                        this.listaDocumento = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaDocumento.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) =>
                        this.manejaError(error)
                );
            });
    }

    //Utilizado para descargar los reportes generados
    descargarReporte() {
        document.body.appendChild(this.reporteGenerado);
        this.reporteGenerado.click();
        this.repoA = false;
        this.repoB = false;
        this.repoC = false;
        this.repoD = false;
        this.repoE = false;
    }

    //Se llama al cancelar la generación de reportes, se setean los datos a default 
    cancelarReporte() {
        this.dialogDocumento = false;
        this.dialogExistencia = false;
        this.dialogMovimiento = false;
        this.dialogCarrito = false;
        if (this.ubicacion != undefined) {
            this.ubicacion = new Ubicacion();
        }
        if (this.premio != undefined) {
            this.premio = new Premio();
        }
        if (this.categoriaProducto != undefined) {
            this.categoriaProducto = new CategoriaProducto();
        }
    }

    /* 
     * Llama al método para generar el catálogo de premios en inventario
     */
    reporteCatalogoPremios() {
        this.cargando = true;
        this.repoA = true;
        this.repoB = this.repoC = this.repoD = this.repoE = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.reporteService.token = token;
                this.reporteService.reporteCatalogoPremios().subscribe(
                    (response: Response) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-reporte-creacion"));
                        let fecha = new Date()
                        this.reporteGenerado = document.createElement('a');
                        this.reporteGenerado.setAttribute('href', encodeURI("data:application/pdf;base64," + response.text()));
                        this.reporteGenerado.setAttribute('download', "reporte_catalogo_" + fecha.toLocaleDateString() + ".pdf");
                        this.cargando = false;
                    },
                    (error) => { this.manejaError(error); this.repoA = false; this.cargando = false; }
                );
            }
        );
    }
    
    onRowSelect(event) {
       console.log(event);
    }


    /**
     * Llama al método para obtener el pdf de un documento de inventario con sus detalles
     * Parámetros: id del documento
     */
    reporteDocumentoInventario() {
        this.cargando = true;
        this.repoB = true;
        this.repoA = this.repoC = this.repoD = this.repoE = false;
        this.dialogDocumento = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.reporteService.token = token;
                this.idDocumento = this.docSelec.numDocumento;
                this.reporteService.reporteDocumentoInventario(this.idDocumento).subscribe(
                    (response: Response) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-reporte-creacion"));
                        this.idDocumento = undefined;
                        if (this.ubicacion != undefined) {
                            this.ubicacion = new Ubicacion();
                        }
                        if (this.premio != undefined) {
                            this.premio = new Premio();
                        }
                        this.avanzada = false;
                        this.estadoDoc = "";
                        this.tipo = [];
                        this.reporteGenerado = document.createElement('a');
                        this.reporteGenerado.setAttribute('href', encodeURI("data:application/pdf;base64," + response.text()));
                        this.reporteGenerado.setAttribute('download', "doc_inventario_" + this.idDocumento + ".pdf");
                        this.cargando = false;
                    },
                    (error) => { this.manejaError(error); this.repoB = false; }
                );
            });
    }

    /**
     * Llama al método para obtener las existencias de uno o varios premios
     * Parámetros: periodo, mes, id de ubicación y el id de premio
     */
    reporteExistencias() {
        this.cargando = true;
        this.repoC = true;
        this.repoB = this.repoA = this.repoD = this.repoE = false;
        if (this.periodo > this.annoMax || this.periodo < this.annoMin) {
            this.msgService.showMessage(this.i18nService.getLabels("warn-repoert-periodo"));
            return;
        }
        if (this.mes > 12 || this.mes < 1) {
            this.msgService.showMessage(this.i18nService.getLabels("warn-repoert-mes"));
            return;
        }
        this.dialogExistencia = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.reporteService.token = token;
                let idUbicacion = '';
                let idPremio = '';
                if (this.ubicacion != undefined) {
                    idUbicacion = this.ubicacion.idUbicacion;
                }
                if (this.premio != undefined) {
                    idPremio = this.premio.idPremio;
                }
                this.reporteService.reporteExistenciasPremio(this.periodo, this.mes, idUbicacion, idPremio).subscribe(
                    (response: Response) => {
                        let fecha = new Date();
                        let nombre = fecha.toLocaleDateString();
                        this.msgService.showMessage(this.i18nService.getLabels("general-reporte-creacion"));
                        this.reporteGenerado = document.createElement('a');
                        this.reporteGenerado.setAttribute('href', encodeURI("data:application/pdf;base64," + response.text()));
                        if (this.premio != undefined) {
                            nombre += "_" + this.premio.idPremio;
                            this.premio = new Premio();
                        }
                        if (this.ubicacion != undefined) {
                            nombre += "_" + this.ubicacion.idUbicacion;
                            this.ubicacion = new Ubicacion();
                        }
                        this.reporteGenerado.setAttribute('download', nombre + "_existencia.pdf");
                        this.cargando = false;
                        this.periodo = this.annoMax;
                        this.mes = 1;
                    },

                    (error) => { this.manejaError(error); this.repoC = false; this.cargando = false; }
                );
            }
        );
    }

    /**
     * Llama al método para obtener el reporte de los movimientos historicos
     * Parámetros: id del documento
     */
    reporteMovimientoHistorico() {
        this.cargando = true;
        this.repoD = true;
        this.repoB = this.repoC = this.repoA = this.repoE = false;
        this.dialogMovimiento = false;
        let inicio = this.fechaInicio.getTime();
        let fin = this.fechaFin.getTime();
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.reporteService.token = token;
                let idUbicacion = '';
                let idPremio = '';
                if (this.ubicacion != undefined) {
                    idUbicacion = this.ubicacion.idUbicacion;
                }
                if (this.premio != undefined) {
                    idPremio = this.premio.idPremio;
                }
                this.reporteService.reporteMovimientos(inicio, fin, this.tipoMovimiento, idUbicacion, idPremio).subscribe(
                    (response: Response) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-reporte-creacion"));
                        if (this.ubicacion != undefined) {
                            this.ubicacion = new Ubicacion();
                        }
                        if (this.premio != undefined) {
                            this.premio = new Premio();
                        }
                        this.tipoMovimiento = undefined;
                        this.fechaFin = new Date();
                        this.fechaInicio = new Date();
                        let fecha = new Date();
                        this.reporteGenerado = document.createElement('a');
                        this.reporteGenerado.setAttribute('href', encodeURI("data:application/pdf;base64," + response.text()));
                        this.reporteGenerado.setAttribute('download', "carritos_" + fecha.toLocaleDateString() + ".pdf");
                        this.cargando = false;
                    },
                    (error) => { this.manejaError(error); this.repoD = false; }
                );
            });
    }


    /**
     * Llama al método para obtener el reporte de los carritos
     * Parámetros: id del documento
     */
    reporteCarritosHistorico() {
        this.cargando = true;
        this.repoE = true;
        this.repoB = this.repoC = this.repoA = this.repoD = false;
        this.dialogCarrito = false;
        let inicio = this.fechaInicio.getTime();
        let fin = this.fechaFin.getTime();
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.reporteService.token = token;
                let idCategoria = '';
                if (this.categoriaProducto != undefined) {
                    idCategoria = this.categoriaProducto.idCategoria;
                }
                this.reporteService.reporteCarritos(inicio, fin, idCategoria).subscribe(
                    (response: Response) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-reporte-creacion"));
                        if (this.categoriaProducto != undefined) {
                            this.categoriaProducto = new CategoriaProducto();
                        }
                        this.fechaFin = new Date();
                        this.fechaInicio = new Date();
                        let fecha = new Date();
                        this.reporteGenerado = document.createElement('a');
                        this.reporteGenerado.setAttribute('href', encodeURI("data:application/pdf;base64," + response.text()));
                        this.reporteGenerado.setAttribute('download', "historico_" + fecha.toLocaleDateString() + ".pdf");
                        this.cargando = false;
                    },
                    (error) => { this.manejaError(error); this.repoE = false; }
                );
            });
    }

    /**
     * Permite abrir un de dialgo según el reporte seleccionado
     * LLama los métodos requeridos para cargar los parámetros a enviar
     */
    abrirDialogs(reporte: string) {
        if (reporte == "documento") {
            this.obtenerDocumentos();
            this.dialogDocumento = true;
            this.reporteCreado = false;
            this.reporteGenerado = null;
            return;
        }

        if (reporte == "existencia") {
            this.dialogExistencia = true;
            this.reporteCreado = false;
            this.reporteGenerado = null;
            return;
        }

        if (reporte == "movimiento") {
            this.dialogMovimiento = true;
            this.reporteCreado = false;
            this.reporteGenerado = null;
            return;
        }

        if (reporte == "carrito") {
            this.dialogCarrito = true;
            this.reporteCreado = false;
            this.reporteGenerado = null;
            return;
        }
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}