import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Insignia } from '../../model/index';
import { InsigniaService, InsigniaMiembroService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_INSIGNIAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import * as Rutas from '../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'insignia-detalle-component',
    templateUrl: 'insignias-detalle.component.html',
    providers: [Insignia, InsigniaMiembroService, InsigniaService]
})

/**
 * Jaylin Centeno
 * Componente padre que permite administrar el uso de la insignia
*/
export class InsigniaDetalleComponent implements OnInit {

    //Evalua el estado de la insignia
    public estado: string;
    public accion: string;
    public publicado: boolean;
    public borrador: boolean;
    public archivado: boolean;
    //Permite verificar permisos (true/false)
    public escritura: boolean;
    //Rutas de los componentes 
    public informacion = Rutas.RT_INSIGNIAS_INFORMACION;
    public asignacion = Rutas.RT_INSIGNIAS_MIEMBRO;
    public niveles = Rutas.RT_INSIGNIAS_NIVEL_LISTA;
    public cargandoDatos: boolean = true;
    public eliminar: boolean;

    constructor(
        public insignia: Insignia,
        public insigniaService: InsigniaService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Representa el id del grupo pasado por la url 
        this.route.params.forEach((params: Params) => { this.insignia.idInsignia = params['id'] });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_INSIGNIAS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    //Inicia
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    //Método que llama al servicio para obtener el detalle de la insignia
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.insigniaService.token = token;
                this.insigniaService.obtenerInsigniaPorId(this.insignia.idInsignia).subscribe(
                    (data) => {
                        this.copyValuesOf(this.insignia, data);
                        if (this.insignia.estado == 'P') {
                            this.publicado = true;
                        } else if (this.insignia.estado == 'B') {
                            this.borrador = true;
                        } else {
                            this.archivado = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => {
                        this.manejarError(error);
                        this.cargandoDatos = false;
                    }
                );
            }
        );
    }

    //Método para remover el grupo actual
    removerInsignia() {
        this.eliminar = false;
        this.estado = this.insignia.estado;
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.insigniaService.token = token;
                this.insigniaService.removerInsignia(this.insignia.idInsignia).subscribe(
                    (data) => {
                        if (this.estado == 'P') {
                            //Se modifican valores
                            this.archivado = true;
                            this.insignia.estado = "A";
                            this.publicado = this.borrador = false;
                            this.msgService.showMessage(this.i18nService.getLabels('general-archivar'));
                            this.mostrarDetalle();
                        } else {
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                            this.goBack();
                        }

                    },
                    (error) => this.manejarError(error)
                );
            }
        );

    }

    //Metodo para publicar la inisignia cuando esta en estado borrador
    publicarInsignia() {
        this.insignia.estado = 'P';
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.insigniaService.token = token;
                let sub = this.insigniaService.actualizarInsignia(this.insignia).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-publicar'));
                        this.publicado = true;
                        this.archivado = this.borrador = false;
                        this.mostrarDetalle();
                    },
                    (error) => {
                        this.manejarError(error);
                        this.publicado = false;
                        this.borrador = true;
                    },
                    () => { sub.unsubscribe(); }
                );
            }
        );
    }


    // Redirecciona a la pantalla de lista de insignias
    goBack() {
        let link = ['/insignias/'];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}