import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Insignia } from '../../model/index';
import { InsigniaService, KeycloakService, I18nService } from '../../service/index';
import { SelectItem } from 'primeng/primeng';
import { AppConfig } from '../../app.config';
import { PERM_ESCR_INSIGNIAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'insignias-insertar-component',
    templateUrl: 'insignias-insertar.component.html',
    providers: [Insignia, InsigniaService]
})

/**
 * Jaylin Centeno
 * Componente que permite insertar una nueva insignia
*/
export class InsigniaInsertarComponent {

    public formInsignia: FormGroup;
    public imagen: any;
    public imagenInsignia: any;
    public tipoItems: SelectItem[] = []; //Tipos de insignia S=status C= collector
    public escritura: boolean; //Permite verificar permisos (true/false)
    public nombreExiste: boolean; //Validación del nombre interno
    public subiendoImagen: boolean = false;

    constructor(
        public insignia: Insignia,
        public insigniaService: InsigniaService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_INSIGNIAS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Inicialización form
        this.formInsignia = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'nombreInterno': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
            'tipo': new FormControl('C', Validators.required)

        });
        this.imagenInsignia = `${AppConfig.DEFAULT_IMG_URL}`;
        //Lista definida de los tipos de insignia
        this.tipoItems = this.i18nService.getLabels("insignia-tipo");
    }

    /**
     * Se establece el nombre interno de la insignia a partir del nombre
     */
    establecerNombres() {
        if (this.insignia.nombreInsignia != "" && this.insignia.nombreInsignia != null) {
            let temp = this.insignia.nombreInsignia.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.insignia.nombreInterno = temp;
            this.validarNombre();
        }
    }

    /**
     * Se valida la existencia del nombre en la base de datos
     */
    validarNombre() {
        if (this.insignia.nombreInterno == null && this.insignia.nombreInterno == "") {
            this.nombreExiste = true;
            return;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.insigniaService.token = token;
                this.insigniaService.verificarNombreInterno(this.insignia.nombreInterno).subscribe(
                    (data) => {
                        this.nombreExiste = data;
                        if (this.nombreExiste) {
                            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
                        }
                    }
                );
            }
        );
    }

    /**
     * Inserción de la nueva insignia
     */
    agregarInsignia() {
        if (this.nombreExiste) {
            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
        } else {
            this.subiendoImagen = true;
            this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.insigniaService.token = token;
                    let sub = this.insigniaService.insertarInsignia(this.insignia).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-insercion'));
                            let link = ['insignias/detalleInsignia', data];
                            this.router.navigate(link);
                        },
                        (error) => {
                            this.manejaError(error);
                            this.subiendoImagen = false;
                        },
                        () => { sub.unsubscribe(); this.subiendoImagen = false; }
                    );
                }
            );
        }
    }

    /**
     * Permite guardar la imagen subida por el usuario
     */
    subirImagen() {
        this.imagen = document.getElementById('imagenInsignia');
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.imagenInsignia = reader.result;
            let urlBase64 = this.imagenInsignia.split(",");
            this.insignia.imagen = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    //Redirecciona a la lista de insignias
    goBack() {
        let link = ['insignias'];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}