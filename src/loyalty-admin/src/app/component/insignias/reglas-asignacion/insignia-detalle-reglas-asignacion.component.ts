import { ReglaAsignacionBadgeService } from '../../../service/regla-asignacion-badge.service';
import { ReglaAsignacionBadge, Metrica, Nivel, GrupoNivel } from '../../../model/index';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { KeycloakService, I18nService, NivelService } from '../../../service/index';
import { AuthGuard } from '../../common/index';
import { MessagesService, ErrorHandlerService } from '../../../utils/index';
import { PERM_ESCR_INSIGNIAS } from '../../common/auth.constants';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';

@Component({
    moduleId: module.id,
    selector: 'insignia-detalle-reglas-asignacion',
    templateUrl: 'insignia-detalle-reglas-asignacion.component.html',
    providers: [ReglaAsignacionBadge, ReglaAsignacionBadgeService, NivelService, Nivel]
})

/**
 * Componente que maneja las reglas de asignación de una insignia
 */
export class InsigniaDetalleReglasAsignacionComponent implements OnInit {

    public regla: ReglaAsignacionBadge;
    private idBadge: string;
    public permisoEscritura: boolean;
    public listaReglas: ReglaAsignacionBadge[];
    public metrica: Metrica;
    public grupoSeleccionado: GrupoNivel;
    public displayModalEliminar: boolean;
    public itemsGruposTier: SelectItem[];
    public itemsNiveles: SelectItem[];
    public itemsOperador: SelectItem[];
    public itemsOperadoresRegla: SelectItem[];
    public itemsTiposRegla: SelectItem[];
    public itemsCalendarizacion: SelectItem[];
    public itemsFechaIndUltimo: SelectItem[];
    public itemsIndTipoValorComparacion: SelectItem[];
    public displayModal: boolean;
    public indAccion: string;
    public fechaInicio: Date;
    public fechaFin: Date;
    constructor(
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        private activatedroute: ActivatedRoute,
        public kc: KeycloakService,
        private i18nService: I18nService,
        private reglaAsignacionBadgeService: ReglaAsignacionBadgeService,
        private nivelService: NivelService
    ) {

    }

    ngOnInit() {
        this.displayModal = false;
        this.itemsOperadoresRegla = [];
        //Valores por default determinados en el sistema
        this.itemsOperadoresRegla=[...this.itemsOperadoresRegla,{ "label": "AND", "value": "A" }, { "label": "OR", "value": "O" }];
        this.itemsCalendarizacion = this.i18nService.getLabels("segmentos-listaPeriodoTiempo");
        this.itemsTiposRegla = this.i18nService.getLabels("reglas-asignacion-badge-tipo-regla");
        this.itemsOperador = this.i18nService.getLabels("segmentos-operadoresNumericos");
        this.itemsFechaIndUltimo = this.i18nService.getLabels("reglas-asignacion-premio-fecha-calendarizacion");
        this.itemsIndTipoValorComparacion = this.i18nService.getLabels("misiones-itemsTipoMision");
    
        this.regla = new ReglaAsignacionBadge();
        this.obtenerGruposTier();
        //Se obtiene el id de la insignia   
        this.activatedroute.parent.params.subscribe((params: { [key: string]: any }) => {
            this.idBadge = params["id"];
        });
        // Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_INSIGNIAS).subscribe((permiso: boolean) => {
            this.permisoEscritura = permiso;
        });
        this.obtenerReglasAsignacion();
        
        this.setFechas();
    }

    //Setea las fechas de inicio y fin para poder ser utilizadas en la asignación
    setFechas() {
        if (this.regla.fechaInicio == null && (this.regla.fechaIndTipo == "D" || this.regla.fechaIndTipo == "E")) {
            this.regla.fechaInicio = new Date();
        }
        if(this.regla.fechaFin == null && (this.regla.fechaIndTipo == "H" || this.regla.fechaIndTipo == "E")){
            this.regla.fechaFin = new Date();
        }
    }

    //Llamado para remover los valores del grupo de niveles seleccionado
    resetGrupoSeleccionado() {
        //Es usado cuando la regla es tipo ascendio nivel 
        this.grupoSeleccionado = undefined;
    }

    //Se obtiene los grupos de nivel disponibles
    obtenerGruposTier() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.nivelService.token = tkn;
                let sub = this.nivelService.obtenertListaGrupoNivel().subscribe(
                    (data: GrupoNivel[]) => {
                        this.itemsGruposTier = data.map((element: GrupoNivel) => { return { label: element.nombre, value: element.idGrupoNivel } });
                        this.itemsGruposTier=[...this.itemsGruposTier,{ label: "Ninguno", value: undefined }];
                    },
                    (error) => {
                        this.handleError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    //Permite obtener la lista de tiers de un grupo de nivel
    obtenerListaTier(event) {
        this.kc.getToken().then(
            (tkn: string) => {
                this.nivelService.token = tkn;
                let sub = this.nivelService.obtenerListaNiveles(event.value ? event.value : event).subscribe(
                    (data: Nivel[]) => {
                        this.itemsNiveles = data.map((element: Nivel) => { return { label: element.nombre, value: element.idNivel } });
                    },
                    (error) => {
                        this.handleError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    /**
     * Obtiene la lista de reglas de asignacion de premio
     */
    obtenerReglasAsignacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaAsignacionBadgeService.token = tkn;
                let sub = this.reglaAsignacionBadgeService.getReglasAsignacionBadge(this.idBadge).subscribe(
                    (data) => {
                        this.listaReglas = data;
                        this.displayModalEliminar = false;

                    },
                    (error) => {
                        this.handleError(error);
                        this.displayModalEliminar = false;
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * inserta una regla de asignacion de premio
     */
    insertarReglasAsignacion(regla: ReglaAsignacionBadge) {
        if (this.regla.indTipoRegla == 'C') {
            this.regla.indOperadorComparacion = ReglaAsignacionBadge.MAYOR;
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaAsignacionBadgeService.token = tkn;
                let sub = this.reglaAsignacionBadgeService.createReglaAsignacionBadge(this.idBadge, this.regla).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-insercion"));
                        this.regla = new ReglaAsignacionBadge();
                        this.obtenerReglasAsignacion();
                        this.displayModal = false;
                        this.regla = new ReglaAsignacionBadge();
                    },
                    (error) => {
                        this.regla = new ReglaAsignacionBadge();
                        this.obtenerReglasAsignacion();
                        this.handleError(error);
                        this.displayModal = false;
                        this.regla = new ReglaAsignacionBadge();
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }
    /**
     * Actualiza una relga de asignacion de premio
     */
    actualizarReglaAsignacion(regla: ReglaAsignacionBadge) {
        if(regla.fechaIndTipo == "D" || regla.fechaIndTipo == "E"){
            this.regla.fechaInicio = this.fechaInicio;
        }
        if(regla.fechaIndTipo == "H" || regla.fechaIndTipo == "E"){
            this.regla.fechaFin = this.fechaFin;
        }
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaAsignacionBadgeService.token = tkn;
                let sub = this.reglaAsignacionBadgeService.updateReglaAsignacionBadge(this.idBadge, this.regla).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.regla = new ReglaAsignacionBadge();
                        this.obtenerReglasAsignacion();
                        this.displayModal = false;
                        this.regla = new ReglaAsignacionBadge();
                    },
                    (error) => {
                        this.regla = new ReglaAsignacionBadge();
                        this.obtenerReglasAsignacion();
                        this.handleError(error);
                        this.displayModal = false;
                        this.regla = new ReglaAsignacionBadge();
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });

    }
    /**
    * Actualiza una relga de asignacion de premio
    */
    eliminarReglaAsignacion() {
        this.kc.getToken().then(
            (tkn: string) => {
                this.reglaAsignacionBadgeService.token = tkn;
                let sub = this.reglaAsignacionBadgeService.deleteReglaAsignacionBadge(this.idBadge, this.regla.idRegla).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                        this.obtenerReglasAsignacion();
                        this.regla = new ReglaAsignacionBadge();
                    },
                    (error) => {
                        this.handleError(error);
                    }, () => {
                        sub.unsubscribe();
                    }
                );
            });
    }

    //Muestra el modal con la regla a remover
    mostrarModalEliminar(regla: ReglaAsignacionBadge) {
        this.regla = regla;
        this.displayModalEliminar = true;
    }
    //Muestra el modal para insertar una nueva regla
    mostrarModalInsertar() {
        this.indAccion = "I";
        this.regla = new ReglaAsignacionBadge();
        this.displayModal = true;
    }
    //Muestra el modal para editar una regla de asignación 
    mostrarModalEditar(regla: ReglaAsignacionBadge) {
        this.regla = regla;
        this.indAccion = "E";
        if(this.regla.fechaIndTipo == "D" || this.regla.fechaIndTipo == "E"){
            this.fechaInicio = new Date(this.regla.fechaInicio);
        }
        if(this.regla.fechaIndTipo == "H" || this.regla.fechaIndTipo == "E"){
            this.fechaFin = new Date(this.regla.fechaFin);
        }
        if (regla.indTipoRegla == 'C') {
            this.kc.getToken().then(
                (tkn: string) => {
                    this.nivelService.token = tkn;
                    this.nivelService.obtenertNivel(this.regla.valorComparacion)
                        .subscribe(
                        (nivel: Nivel) => {
                            this.grupoSeleccionado = nivel.grupoNiveles; 
                            this.obtenerListaTier(this.grupoSeleccionado.idGrupoNivel);
                        }
                        );
                });
        }
        this.displayModal = true;
    }
    /**
        * Maneja el erro a nivel de componente
        */
    handleError(error: Response) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}