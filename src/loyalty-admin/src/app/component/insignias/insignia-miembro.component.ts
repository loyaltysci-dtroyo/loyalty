import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Miembro, Insignia, InsigniaNivel } from '../../model/index';
import { InsigniaService, MiembroService, InsigniaMiembroService, KeycloakService, I18nService } from '../../service/index';
import { MenuItem } from 'primeng/primeng';
import { Response, Headers } from '@angular/http';
import { PERM_ESCR_INSIGNIAS, PERM_ESCR_MIEMBROS, PERM_LECT_MIEMBROS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'insignia-miembro-component',
    templateUrl: 'insignia-miembro.component.html',
    providers: [Insignia, InsigniaNivel, Miembro, MiembroService]
})

/**
 * Componente utilizado para la asignación de insignias/niveles a los miembros
*/
export class InsigniaMiembroComponent implements OnInit {

    public id: string; //Id de la insignia pasada por la url
    public escrituraInsignia: boolean; //Permiso de escritura sobre insignia (true/false)
    public escrituraMiembro: boolean; //Permiso de escritura sobre miembro (true/false)
    public lecturaMiembro: boolean; //Permiso de escritura sobre miembro (true/false)
    public verNiveles: boolean = false; // Habilita el cuadro de dialogo para ver los niveles de la insignia
    public cargandoDatos: boolean = true; // Carga de datos
    public cargandoTotal: boolean = true; // Carga de datos
    public eliminar: boolean; // Habilita el cuadro de dialog para remover asignación

    // --- Miembros Disponibles --- //
    public listaMiembro: Miembro[] = []; // Miembros no la insignia
    public listaTotalVacia: boolean = true; // Valida si la lista de miembros no insignia esta vacía
    public cantidad: number = 10; //Cantidad de filas
    public totalRecords: number; //Total de miembros disponibles 
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    // --- Miembros Asignados --- //
    public listaMiembroInsignia: Miembro[] = []; // Miembros con la insignia
    public listaInsigniaVacia: boolean = false; // Valida si la lista de miembros con insignia esta vacía
    public cantidadMiembro: number = 10; //Cantidad de filas
    public totalRecordsMiembro: number; // Total de miembros asignados
    public filaDesplazMiembro: number = 0; //Desplazamiento de la primera fila

    public listaNiveles: InsigniaNivel[] = []; // Lista de los niveles de insignia
    public listaVacia: boolean = true; //Para verificar si la lista de niveles esta vacia

    public idNivel: string; // Id del nivel de insignia seleccionado 
    public idMiembro: string; // Id del miembro seleccionado

    // --- Búsqueda ---  //
    public busqueda: string;
    public listaBusqueda: string[] = []; //Palabras de búsqueda, ChipModule

    constructor(
        public insignia: Insignia,
        public insigniaNivel: InsigniaNivel,
        public miembro: Miembro,
        public miembroService: MiembroService,
        public insigniaService: InsigniaService,
        public insigniaMiembroService: InsigniaMiembroService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Representa el id del grupo pasado por la url 
        this.route.parent.params.subscribe(params => { this.id = params["id"]; });

        //Permite saber si el usuario tiene permisos de escrituraInsignia
        this.authGuard.canWrite(PERM_ESCR_INSIGNIAS).subscribe((permiso: boolean) => {
            this.escrituraInsignia = permiso;
        });
        this.authGuard.canWrite(PERM_ESCR_MIEMBROS).subscribe((permiso: boolean) => {
            this.escrituraMiembro = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_MIEMBROS).subscribe((permiso: boolean) => {
            this.lecturaMiembro = permiso;
        });
    }

    //Lifecyle hook, se llama solo una vez
    ngOnInit() {
        this.detalleInsignia();
        this.obtenerMiembrosConInsignia();
        this.obtenerMiembrosNoInsignia()
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadMiembroInsignia(event: any) {
        this.filaDesplazMiembro = event.first;
        this.cantidadMiembro = event.rows;
        this.obtenerMiembrosConInsignia();
    }

    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadMiembroNoInsignia(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerMiembrosNoInsignia();
    }

    //Método para guardar el valor del miembro seleccionado
    confirmacion(idMiembro: string) {
        this.idMiembro = idMiembro;
        this.eliminar = true;
    }

    //Metodo que se encarga de traer los clientes que tienen la insignia asociada
    obtenerMiembrosConInsignia() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.insigniaService.token = token;
                this.insigniaService.obtenerMiembrosParaInsignia(this.id, this.cantidadMiembro, this.filaDesplazMiembro, 'A', this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaMiembroInsignia = response.json();
                        //Se recibe el total de las categorías disponibles
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecordsMiembro = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaMiembroInsignia.length > 0) {
                            this.listaInsigniaVacia = false;
                        } else {
                            this.listaInsigniaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) =>
                        this.manejarError(error)
                );
            }
        );

    }

    //Metodo que se encarga de traer los clientes que no tiene insignia
    obtenerMiembrosNoInsignia() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.insigniaService.token = token;
                this.insigniaService.obtenerMiembrosParaInsignia(this.id, this.cantidad, this.filaDesplazamiento, 'D', this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaMiembro = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaMiembro.length > 0) {
                            this.listaTotalVacia = false;
                        } else {
                            this.listaTotalVacia = true;
                        }
                        this.cargandoTotal = false;
                    },
                    (error) =>
                        this.manejarError(error)
                );
            });
    }

    //Método que llama al servicio para obtener el detalle de la insignia
    detalleInsignia() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.insigniaService.token = token;
                this.insigniaService.obtenerInsigniaPorId(this.id).subscribe(
                    (data) => {
                        this.insignia = data;
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Metodo que permite asingar al miembro una insignia
    agregarMiembroInsignia(idMiembro: string) {
        if (this.escrituraInsignia && this.escrituraMiembro) {
            if (this.insignia.tipo == 'C') {
                this.idMiembro = idMiembro;
                this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                    (token: string) => {
                        this.insigniaMiembroService.token = token;

                        this.insigniaMiembroService.asingnarInsigniaMiembro(idMiembro, this.id).subscribe(
                            (data) => {
                                this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                                this.sacarMiembroNoInsignia();
                            },
                            (error) => this.manejarError(error)
                        );
                    }
                );
            } else {
                this.obtenerListaNiveles();
                this.verNiveles = true;
            }
        } 
    }

    //En caso de que la insignia sea status, se utiliza este método para asigniar un nivel de la insignia
    asociarNivelMiembro(idNivel: string) {
        if (this.escrituraInsignia && this.escrituraMiembro) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.insigniaMiembroService.token = token;
                    this.insigniaMiembroService.asingnarNivelMiembro(this.idMiembro, this.id, idNivel).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                            this.sacarMiembroNoInsignia();
                            this.verNiveles = false;
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
        }
    }

    //Lista de los niveles de una insignia tipo status
    obtenerListaNiveles() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.insigniaService.token = token;
                this.insigniaService.obtenerListaNiveles(this.id).subscribe(
                    (data) => {
                        this.listaNiveles = data;
                        if (this.listaNiveles.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Método que sacar de la lista los miembros que ya fueron agregados
    sacarMiembroNoInsignia() {

        let listaTemporal = this.listaMiembro;
        let posicion;
        let index = this.listaMiembro.forEach(element => {
            element.idMiembro == this.idMiembro;
        })
        this.listaMiembro.splice(posicion, 1);
        this.listaMiembro = [...this.listaMiembro];
        this.obtenerMiembrosConInsignia();
    }

    //Método que sacar de la lista los miembros que ya fueron agregados
    sacarMiembroLista() {
        let listaTemporal = this.listaMiembroInsignia;
        if (this.listaMiembroInsignia.length >= 0) {
            let index = this.listaMiembroInsignia.findIndex(element => element.idMiembro == this.idMiembro);
            this.listaMiembroInsignia.splice(index, 1);
            this.listaMiembroInsignia = [...this.listaMiembroInsignia];
        }
        this.obtenerMiembrosNoInsignia();
    }

    //Método para remover a un miembro del grupo
    removerMiembro() {
        if (this.escrituraInsignia && this.escrituraMiembro) {
            this.eliminar = false;
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.insigniaMiembroService.token = token;
                    this.insigniaMiembroService.removerNivelMiembro(this.idMiembro, this.id).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));

                            this.sacarMiembroLista();
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
        }
    }

    //Redirecciona al detalle de un miembro seleccionado
    gotoMiembro(idMiembro: string) {
        let link = ['miembros/detalleMiembro/', idMiembro];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}