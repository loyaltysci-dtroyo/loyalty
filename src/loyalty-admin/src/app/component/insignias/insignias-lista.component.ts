import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Insignia } from '../../model/index';
import { InsigniaService, KeycloakService, I18nService } from '../../service/index';
import { Response, RequestOptionsArgs } from '@angular/http';
import { PERM_ESCR_INSIGNIAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'insignias-lista-component',
    templateUrl: 'insignias-lista.component.html',
    providers: [Insignia, InsigniaService]
})

/**
 * Jaylin Centeno
 * Ccomponente con la lista de insignias
 */ 
export class InsigniaListaComponent implements OnInit {

    public escritura: boolean; //Permite verificar permisos de escritura(true/false)
    public cargandoDatos: boolean = true; //Utilizado para mostrar la carga de datos
    public eliminar: boolean; //Habilita el cuadro de dialogo para remover

    //Insignias
    public idInsignia: string; //Id del atributo seleccionado
    public listaInsignias: Insignia[]; //Lista de insignias
    public listaVacia: boolean = true; //Para verificar si la lista de insignias esta vacia
    public cantidad: number = 10; //Cantidad de rows
    public totalRecords: number; //Total de miembros en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public publicada: boolean;

    //Busquedas
    public busqueda: string = ""; //Palabras clave de búsqueda
    public listaBusqueda: string[] = [];
    public busquedaAvanzada: boolean = false;
    public tipoInsignia: string[] = [];
    public estadoInsignia: string[] = [];
    public tipoOrden: string = "";
    public campoOrden: string = "";
    public filtro: string[] = [];

    constructor(
        public insigniaService: InsigniaService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_INSIGNIAS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        //Opciones default
        this.estadoInsignia = ['P', 'B'];
        this.tipoInsignia = ['C', 'S'];
    }

    ngOnInit() {
        this.busquedaInsignias();
    }

    //redirecciona al detalle de la insignia seleccionada
    insigniaSeleccionada(idInsignia: string) {
        let link = ['insignias/detalleInsignia', idInsignia];
        this.router.navigate(link);
    }

    //Método para mostrar el dialogo de confirmación
    confirmar(idInsignia: string, estado: string) {
        if (estado == "P") {
            this.publicada = true;
        } else {
            this.publicada = false;
        }
        this.eliminar = true;
        this.idInsignia = idInsignia;
    }

    //Método que redirecciona a la página de insertar un grupo
    nuevaInsignia() {
        let link = ['insignias/insertarInsignia'];
        this.router.navigate(link);
    }
    
    /**
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaInsignias();
    }

    //Metodo encargado de realizar la búsqueda de insignias 
    busquedaInsignias() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.insigniaService.token = token;
                this.insigniaService.buscarInsigniasTemp(this.estadoInsignia, this.cantidad, this.filaDesplazamiento, this.tipoInsignia, this.busqueda, this.filtro, this.tipoOrden, this.campoOrden).subscribe(
                    (response: Response) => {
                        this.listaInsignias = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaInsignias.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    //Metodo que se encarga remover un grupo
    removerInsignia() {
        if (this.escritura) {
            this.eliminar = false;
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.insigniaService.token = token;
                    this.insigniaService.removerInsignia(this.idInsignia).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                            this.busquedaInsignias();

                        },
                        (error) => {
                            this.manejaError(error)
                        }
                    );
                }
            );
        }
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}