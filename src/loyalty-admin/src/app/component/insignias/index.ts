export { InsigniaComponent } from './insignias.component';
export { InsigniaListaComponent } from './insignias-lista.component';
export { InsigniaInsertarComponent } from './insignias-insertar.component';
export { InsigniaDetalleComponent } from './insignias-detalle.component';
export { InsigniaNivelListaComponent } from './insignias-nivel-lista.component';
export { InsigniaMiembroComponent } from './insignia-miembro.component';
export { InsigniaInformacionComponent } from './insignia-informacion.component';
export { InsigniaDetalleReglasAsignacionComponent } from './reglas-asignacion/insignia-detalle-reglas-asignacion.component';