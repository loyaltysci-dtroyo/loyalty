import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Insignia } from '../../model/index';
import { InsigniaService, InsigniaMiembroService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_INSIGNIAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { AppConfig } from '../../app.config';

@Component({
    moduleId: module.id,
    selector: 'insignia-informacion-component',
    templateUrl: 'insignia-informacion.component.html',
    providers: [InsigniaMiembroService, InsigniaService]
})

/**
 * Jaylin Centeno
 * Componente utilizado para mostrar la información de la insignia
 */
export class InsigniaInformacionComponent implements OnInit {

    public subiendoImagen: boolean = false;
    public cargandoDatos: boolean = true;
    public cambioImagen: boolean;
    public escritura: boolean; //Permite verificar permisos (true/false)
    public formInsignia: FormGroup;
    public imagen: any;
    public imagenInsignia: any;
    public tipoItems: SelectItem[];
    public nombreTemporal: string; //Guarda el nombre de la insignia
    public nombreExiste: boolean; //Validación del nombre interno
    public actualizando: boolean = false;

    constructor(
        public insignia: Insignia,
        public insigniaService: InsigniaService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_INSIGNIAS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Validación del forms
        this.formInsignia = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'nombreInterno': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
            'tipo': new FormControl(''),
        });

        this.tipoItems = [];
        //Obtiene los tipos de insignia predeterminados
        this.tipoItems = this.i18nService.getLabels("insignia-tipo");
        this.imagenInsignia = `${AppConfig.DEFAULT_IMG_URL}`;
    }

    //Inicia
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    //Método que llama al servicio para obtener el detalle del grupo
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.insigniaService.token = token;
                this.insigniaService.obtenerInsigniaPorId(this.insignia.idInsignia).subscribe(
                    (data) => {
                        this.copyValuesOf(this.insignia, data);
                        this.nombreTemporal = this.insignia.nombreInterno;
                        this.imagenInsignia = this.insignia.imagen;
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Se establecen los nombres de las insignias
    establecerNombres() {
        if (this.insignia.nombreInsignia != "" && this.insignia.nombreInsignia != null) {
            let temp = this.insignia.nombreInsignia.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.insignia.nombreInterno = temp;
            this.validarNombre();
        }
    }

    //Método encargado de validar la existencia del nombre interno de una métrica
    validarNombre() {
        if (this.insignia.nombreInterno == "" || this.insignia.nombreInterno == null) {
            this.nombreExiste = false;
            return;
        }
        //Si el nombre sigue siendo el mismo no se llama al servicio
        if (this.insignia.nombreInterno != this.nombreTemporal) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token) => {
                    this.insigniaService.token = token || "";
                    this.insigniaService.verificarNombreInterno(this.insignia.nombreInterno).subscribe(
                        (data) => {
                            this.nombreExiste = data;
                            if (this.nombreExiste) {
                                this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
                                return;
                            }
                        },
                        (error) => {
                            this.manejarError(error);
                        }
                    );
                }
            );
        }
    }
    //Método para subir una imagen
    subirImagen() {
        this.imagen = document.getElementById("imagen");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.imagenInsignia = reader.result;
            let urlBase64 = this.imagenInsignia.split(",");
            this.insignia.imagen = urlBase64[1];
            this.cambioImagen = true;
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    //Metodo para actualizar el grupo actual
    actualizarInsignia() {
        this.actualizando = true;
        if (this.nombreExiste) {
            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
            return;
        } else {
            if (this.cambioImagen) {
                this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
            }
            this.subiendoImagen = true;
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.insigniaService.token = token;
                    let sub = this.insigniaService.actualizarInsignia(this.insignia).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                            this.mostrarDetalle();
                            this.actualizando = false;
                        },
                        (error) => {
                            this.manejarError(error);
                            this.subiendoImagen = false;
                            this.actualizando = false;
                        },
                        () => { sub.unsubscribe(); this.subiendoImagen = false; this.actualizando = false; }
                    );
                }
            );
        }
    }

    // Metodo para volver a la página anterior
    goBack() {
        let link = ['/insignias/'];
        this.router.navigate(link);
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}