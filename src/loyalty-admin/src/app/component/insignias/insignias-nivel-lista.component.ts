import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Insignia, InsigniaNivel } from '../../model/index';
import { InsigniaService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_INSIGNIAS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { AppConfig } from '../../app.config';

@Component({
    moduleId: module.id,
    selector: 'insignias-nivel-lista-component',
    templateUrl: 'insignias-nivel-lista.component.html',
    providers: [Insignia, InsigniaNivel, InsigniaService]
})

/**
 * Jaylin Centeno
 * Componente hijo, se muestran los niveles de la insignia
 */
export class InsigniaNivelListaComponent implements OnInit {

    public id: string; //Id de la insignia recibido en la url
    public accion: string; //Guarda el tipo de acción (insertar/editar)
    public subiendoImagen: boolean = false; //Indicar si existe una carga de imagen
    public cargandoDatos: boolean = true; //Indicar si existe una carga de datos
    public editar: boolean; //Habilita el cuadro de dialogo para editar
    public eliminar: boolean; //Habilita el cuadro de dialogo para eliminar
    public escritura: boolean; //Permite verificar permisos (true/false)
    public formNivel: FormGroup;

    //Niveles
    public listaNiveles: InsigniaNivel[]; //Niveles de la insignia
    public listaVacia: boolean = true; //Verificar si la lista de niveles viene vacia
    public idNivel: string; //Id del nivel a eliminar 
    public busqueda: string = ""; //Palabras clave de búsqueda

    //Imagenes de la insignia
    public imagenAgregar: any;
    public imagen: any;
    public urlBase64: any;
    public cambioImagen: boolean;

    //Existencia del nombre interno
    public nombreTemporal: string; //Permite el manejo del nombre interno del nivel
    public nombreExiste: boolean; //Validación del nombre interno

    constructor(
        public insignia: Insignia,
        public nivel: InsigniaNivel,
        public insigniaService: InsigniaService,
        public fb: FormBuilder,
        public router: Router,
        public route: ActivatedRoute,
        public msgService: MessagesService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_INSIGNIAS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        //El id del nivel se inicializa con el valor introducido por la url
        this.route.parent.params.forEach((params: Params) => { this.id = params['id'] });

        //Validacion del form
        this.formNivel = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
            'nombreInterno': new FormControl('', Validators.required)
        });

        this.imagenAgregar = `${AppConfig.DEFAULT_IMG_URL}`;;
    }

    //Lifecycle hook, se inicializa al completarse la carga del componente
    ngOnInit() {
        this.mostrarDetalle();
        this.obtenerListaNiveles();
    }

    //Método para mostrar el dialogo de confirmación
    confirmar(idNivel: string) {
        this.eliminar = true;
        this.idNivel = idNivel;
    }

    //Método que llama al servicio para obtener el detalle del grupo
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.insigniaService.token = token;
                this.insigniaService.obtenerInsigniaPorId(this.id).subscribe(
                    (data) => {
                        this.insignia = data;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    //Utilizado para ver el detalle del nivel seleccionado
    nivelSeleccionado(nivel: InsigniaNivel) {
        this.nivel = nivel;
        //Se guardan los valores del nivel en el formNivel
        this.formNivel.patchValue({ nombre: this.nivel.nombreNivel, descripcion: this.nivel.descripcion, nombreInterno: this.nivel.nombreInterno });
        this.imagenAgregar = this.nivel.imagen;
        this.nombreTemporal = this.nivel.nombreInterno;
        this.accion = "editar";
        this.editar = true;
    }

    //Según la acción realizada se actualiza o agrega un nivel
    accionNivel() {
        if (this.accion == "editar") {
            this.actualizarNivel();
        } else {
            this.agregarNivel();
        }
    }

    //Cancela la acción de editar/insertar un nivel
    cancelarAccion() {
        this.editar = false;
        this.formNivel.reset();
        this.accion = "insertar"
    }

    //Se obtienen los niveles que tiene una insignia
    obtenerListaNiveles() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.insigniaService.token = token;
                this.insigniaService.obtenerListaNiveles(this.id).subscribe(
                    (data) => {
                        this.listaNiveles = data;
                        if (this.listaNiveles.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    //Establece el nombre interno a partir del nombre dado al nivel
    establecerNombres() {
        if (this.formNivel.get('nombre').value != "" || this.formNivel.get('nombre').value != null) {
            this.nivel.nombreNivel = this.formNivel.get('nombre').value;
            let temp = this.nivel.nombreNivel.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.nivel.nombreInterno = temp;
            this.formNivel.get('nombreInterno').setValue(this.nivel.nombreInterno);
            this.validarNombre();

        }
    }

    //Método encargado de validar la existencia del nombre interno que debe ser único
    validarNombre() {
        if (this.nivel.nombreInterno == null || this.nivel.nombreInterno == "") {
            this.nombreExiste = true;
            return;
        }//Entra solo si el nombre temporal es diferente al nombre interno definido anteriormente
        if (this.nombreTemporal != this.formNivel.get('nombreInterno').value) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.insigniaService.token = token;
                    this.insigniaService.verificarNivelNombreInterno(this.formNivel.get('nombreInterno').value).subscribe(
                        (data) => {
                            this.nombreExiste = data;
                            if (this.nombreExiste) {
                                this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
                            }
                        }
                    );
                }
            );
        }

    }

    /**
     * Permite actualizar el nivel seleccionado
     */
    actualizarNivel() {
        if (this.escritura) {
            //Se obtienen los valores del form
            this.nivel.nombreNivel = this.formNivel.get('nombre').value;
            this.nivel.descripcion = this.formNivel.get('descripcion').value;
            this.nivel.nombreInterno = this.formNivel.get('nombreInterno').value;
            this.nivel.idInsignia = this.insignia;
            if (this.cambioImagen) {
                this.nivel.imagen = this.urlBase64[1];
            }
            if (this.nombreExiste) {
                this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
            } else {
                if (this.nivel.imagen != null) {
                    this.subiendoImagen = true;
                    this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
                    this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                        (token: string) => {
                            this.insigniaService.token = token;
                            let sub = this.insigniaService.actualizarNivel(this.nivel, this.id).subscribe(
                                (data) => {
                                    this.obtenerListaNiveles();
                                    //Reset de las propiedades utilizadas
                                    this.editar = false;
                                    this.accion = "insertar";
                                    this.formNivel.reset();
                                    this.urlBase64 = "";
                                    this.imagenAgregar = `${AppConfig.DEFAULT_IMG_URL}`;;
                                    this.msgService.showMessage(this.i18nService.getLabels('Los datos se han actualizado correctamente'));
                                },
                                (error) => {
                                    this.manejaError(error);
                                    this.subiendoImagen = false;
                                },
                                () => { sub.unsubscribe(); this.subiendoImagen = false; }
                            );
                        }
                    );
                } else {
                    this.msgService.showMessage(this.i18nService.getLabels('warn-misiones-juego-imagen'));
                }
            }
        }
    }

    //Método para agregar un nivel 
    agregarNivel() {
        if (this.escritura) {
            this.nivel.nombreNivel = this.formNivel.get('nombre').value;
            this.nivel.descripcion = this.formNivel.get('descripcion').value;
            this.nivel.nombreInterno = this.formNivel.get('nombreInterno').value;
            this.nivel.idInsignia = this.insignia;
            if (this.urlBase64 != null) {
                this.nivel.imagen = this.urlBase64[1];
            }
            if (this.nombreExiste) {
                this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
            } else {
                if (this.nivel.imagen != null) {
                    this.subiendoImagen = true;
                    this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
                    this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                        (token: string) => {
                            this.insigniaService.token = token;
                            let sub = this.insigniaService.insertarNivel(this.nivel, this.id).subscribe(
                                (data) => {
                                    //Reset a los valores default
                                    this.editar = false;
                                    this.formNivel.reset();
                                    this.accion = "insertar";
                                    this.imagenAgregar = `${AppConfig.DEFAULT_IMG_URL}`;;
                                    this.msgService.showMessage(this.i18nService.getLabels('general-insercion'));
                                    this.obtenerListaNiveles();
                                    this.urlBase64 = "";
                                    this.cambioImagen = false;
                                },
                                (error) => {
                                    this.manejaError(error);
                                    this.subiendoImagen = false;
                                    this.cambioImagen = false;
                                },
                                () => { sub.unsubscribe(); this.subiendoImagen = false; }
                            )
                        }
                    );
                } else {
                    this.msgService.showMessage(this.i18nService.getLabels('warn-misiones-juego-imagen'));
                }
            }
        }
    }

    /**
     * Permite guardar la imagen que el usuario subio
     */
    subirImagen() {
        this.imagen = document.getElementById("imagenAgregar");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.imagenAgregar = reader.result;
            this.urlBase64 = this.imagenAgregar.split(",");
            this.cambioImagen = true;
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    //Metodo que se encarga remover un grupo
    removerNivel() {
        if (this.escritura) {
            this.eliminar = false;
            this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.insigniaService.token = token;
                    this.insigniaService.removerNivel(this.id, this.idNivel).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion'));
                            this.obtenerListaNiveles();
                        },
                        (error) => {
                            this.manejaError(error)
                        }
                    );
                }
            );
        }
    }

    /* Metodo para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}