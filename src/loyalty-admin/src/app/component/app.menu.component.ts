import { I18nService } from '../service';
import {
    Component, Input, OnInit, EventEmitter, ViewChild, trigger,
    state, transition, style, animate, Inject, forwardRef
} from '@angular/core';
import { AuthGuard } from './common/index';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/primeng';
import { AppComponent } from '../app.component';
import { Usuario } from '../model/index';

@Component({
    selector: 'app-menu',
    template: `
        <ul app-submenu [item]="model" root="true" class="ultima-menu ultima-main-menu clearfix" [reset]="reset" visible="true"></ul>
    `
})
export class AppMenuComponent implements OnInit {
    public mapPermisos: Map<string, boolean>;
    @Input() reset: boolean;
    model: any[];

    constructor(
        public app: AppComponent,
        private i18nService: I18nService,
        public authGuard: AuthGuard,
        public usuario: Usuario) { }

    ngOnInit() {
        this.mapPermisos = new Map<string, boolean>();
        this.model = [];
        this.authGuard.getPermisos().subscribe(
            (data: Map<string, boolean>) => {
                this.mapPermisos = data;
                console.log(this.mapPermisos)
                this.model.push({
                    label: 'Dashboards', icon: 'dashboard',
                    items: [
                        { label: this.i18nService.getLabels("menu")["dashboard-miembros"], icon: 'person', routerLink: ['/dashboard/miembros'] },
                        { label: this.i18nService.getLabels("menu")["dashboard-segmentos"], icon: 'group', routerLink: ['/dashboard/segmentos'] },
                        { label: this.i18nService.getLabels("menu")["dashboard-general-metrica"], icon: 'group', routerLink: ['/dashboard/metricas'] },
                        { label: this.i18nService.getLabels("menu")["dashboard-general-mision"], icon: 'group', routerLink: ['/dashboard/misiones'] },
                        { label: this.i18nService.getLabels("menu")["dashboard-general-notificacion"], icon: 'group', routerLink: ['/dashboard/notificaciones'] },
                        { label: this.i18nService.getLabels("menu")["dashboard-general-premio"], icon: 'group', routerLink: ['/dashboard/premios'] },
                        { label: this.i18nService.getLabels("menu")["dashboard-general-promocion"], icon: 'group', routerLink: ['/dashboard/promociones'] }
                    ]
                });

                let administracionPrograma = [];
                if (this.mapPermisos.get('Ubicaciones Lectura')) {
                    administracionPrograma.push({ label: this.i18nService.getLabels("menu")["ubicaciones"], icon: 'map', routerLink: ['/ubicaciones'] });
                }
                // Metricas
                if (this.mapPermisos.get('Metricas Lectura')) {
                    administracionPrograma.push({ label: this.i18nService.getLabels("menu")["metricas"], icon: 'list', routerLink: ['/metricas'] });
                    administracionPrograma.push({ label: this.i18nService.getLabels("menu")["grupos-tiers"], icon: 'question_answer', routerLink: ['/metricas/gruposNiveles'] });
                }

                this.model = [...this.model, { label: this.i18nService.getLabels("menu")["adm-programa"], icon: 'settings', items: administracionPrograma }];
                // Usuarios
                if (this.mapPermisos.get('Usuarios Lectura') || this.mapPermisos.get('Roles Lectura')) {
                    let itemsModel = [];
                    if (this.mapPermisos.get('Usuarios Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["listado-usuarios"], icon: 'list', routerLink: ['/usuarios/listaUsuarios'] });
                    }
                    if (this.mapPermisos.get('Roles Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["listado-roles"], icon: 'security', routerLink: ['/usuarios/listaRoles'] });
                    }
                    this.model.push(

                        { label: this.i18nService.getLabels("menu")["usuarios"], icon: 'person_outline', items: itemsModel }
                    );
                }
                // Miembros
                if (this.mapPermisos.get('Miembros Lectura') || this.mapPermisos.get('Atributos Lectura') || this.mapPermisos.get('Grupos Lectura')) {
                    let itemsModel = [];
                    if (this.mapPermisos.get('Miembros Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["listado-miembros"], icon: 'list', routerLink: ['/miembros'] });
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["preferencias"], icon: 'question_answer', routerLink: ['/miembros/listaPreferencias'] });
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["importacion"], icon: 'insert_drive_file', routerLink: ['/miembros/importacion'] });
                    }
                    if (this.mapPermisos.get('Atributos Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["atributos-dinamicos"], icon: 'playlist_add', routerLink: ['/atributosDinamicos'] });
                    }
                    if (this.mapPermisos.get('Grupos Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["grupos-miembro"], icon: 'people_outline', routerLink: ['/grupos'] });
                    }
                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["miembros"], icon: 'person', items: itemsModel }];
                }
                // Leaderboards
                if (this.mapPermisos.get('Leaderboards Lectura')) {
                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["leaderboards"], icon: 'flag', routerLink: ['/leaderboards'] }];
                }
                // Segmentos
                if (this.mapPermisos.get('Segmento Lectura')) {
                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["segmentos"], icon: 'list', routerLink: ['/segmentos'] }];
                }
                // Promociones
                if (this.mapPermisos.get('Promociones Lectura')) {
                    let itemsModel = [];
                    if (this.mapPermisos.get('Promociones Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["listado-promociones"], icon: 'list', routerLink: ['/promociones/listaPromos'] });
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["actividad-promociones"], icon: 'list', routerLink: ['/promociones/actividad'] });
                        if (this.mapPermisos.get('Categorias Lectura')) {
                            itemsModel.push({ label: this.i18nService.getLabels("menu")["listado-categorias"], icon: 'list', routerLink: ['/promociones/listaCategorias'] });
                        }
                    }
                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["promociones"], icon: 'attach_money', items: itemsModel }];
                }
                // Insignias
                if (this.mapPermisos.get('Insignias Lectura')) {
                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["insignias"], icon: '  ', routerLink: ['/insignias'] }];
                }
                // Premios
                if (this.mapPermisos.get('Premios Lectura') || this.mapPermisos.get('Categorias Premio Lectura')) {
                    let itemsModel = [];
                    if (this.mapPermisos.get('Premios Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["premios"], icon: 'list', routerLink: ['/premios/'] });
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["actividad-premios"], icon: 'list', routerLink: ['/premios/actividad'] });
                    }
                    if (this.mapPermisos.get('Categorias Premio Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["categorias-premio"], icon: 'list', routerLink: ['/premios/listaCategoriasPremio'] });
                    }
                    //Medidas
                    itemsModel.push({ label: this.i18nService.getLabels("menu")["unidades-medida"], icon: 'list', routerLink: ['/premios/unidadMedida'] });
                    this.model = [...this.model, { label: 'Premios', icon: 'star', items: itemsModel }];
                }

                // Campanas
                if (this.mapPermisos.get('Campanas Lectura')) {
                    let itemsModel = [];
                    itemsModel.push({ label: this.i18nService.getLabels("menu")["notificaciones-listado"], icon: 'list', routerLink: ['/notificaciones/'] });
                    itemsModel.push({ label: this.i18nService.getLabels("menu")["actividad-notificaciones"], icon: 'list', routerLink: ['/notificaciones/actividad'] });
                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["notificaciones"], icon: 'email', items: itemsModel }];
                }
                // Misiones
                if (this.mapPermisos.get('Misiones Lectura') || this.mapPermisos.get('Categorias Escritura')) {
                    let itemsModel = [];
                    if (this.mapPermisos.get('Misiones Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["listado-misiones"], icon: 'list', routerLink: ['/misiones/'] });
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["actividad-misiones"], icon: 'list', routerLink: ['/misiones/actividad'] });
                        //itemsModel.push({ label: this.i18nService.getLabels("menu")["actividad-misiones"], icon: 'list', routerLink: ['/misiones/aprobacion'] });
                    }
                    if (this.mapPermisos.get('Categorias Escritura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["categorias-mision"], icon: 'list', routerLink: ['/misiones/listaCategorias'] });
                    }
                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["misiones"], icon: 'local_activity', items: itemsModel }];
                }
                // Juegos
                if (this.mapPermisos.get('Juegos Lectura')) {
                    let itemsModel = [];
                    itemsModel.push({ label: this.i18nService.getLabels("menu")["quien-quiere-ser-millonario"], icon: 'star', routerLink: ['/juegos/'] });
                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["juegos"], icon: 'list', items: itemsModel }];
                }
                // Productos
                if (this.mapPermisos.get('Productos Lectura') || this.mapPermisos.get('Categorias Producto Lectura') || this.mapPermisos.get('Atributos Producto Lectura')) {
                    let itemsModel = [];
                    if (this.mapPermisos.get('Productos Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["listado-productos"], icon: 'list', routerLink: ['/productos/'] });

                    }
                    if (this.mapPermisos.get('Categorias Producto Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["categorias-productos"], icon: 'list', routerLink: ['/productos/listaCategoriasProducto'] });
                    }
                    if (this.mapPermisos.get('Atributos Producto Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["atributos-producto"], icon: 'playlist_add', routerLink: ['/productos/listaAtributosProducto'] });
                    }
                    this.model = [...this.model, { label: 'Productos', icon: 'attach_money', items: itemsModel }];
                }
                //Workflows
                if (this.mapPermisos.get('Workflow Lectura')) {
                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["workflow"], icon: 'timer', routerLink: ['/workflows'] }];
                }
                // Inventarios
                if (this.mapPermisos.get('Documentos Lectura')) {
                    let itemsModel = [];
                    itemsModel.push({ label: this.i18nService.getLabels("menu")["documentos-listado"], icon: 'list', routerLink: ['/inventario/'] });
                    itemsModel.push({ label: this.i18nService.getLabels("menu")["proveedores-listado"], icon: 'group', routerLink: ['/inventario/proveedor'] });
                    itemsModel.push({ label: this.i18nService.getLabels("menu")["historico"], icon: 'list', routerLink: ['/inventario/historico'] });

                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["inventario-premio"], icon: 'list', items: itemsModel }];
                }
                //Reportes
                if (this.mapPermisos.get('Reportes')) {
                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["reportes"], icon: 'flag', routerLink: ['/reportes'] }];
                }
                // Bitacoras
                if (this.mapPermisos.get('Bitacora Lectura')) {
                    let itemsModel = [];
                    itemsModel.push({ label: this.i18nService.getLabels("menu")["apiExternal"], icon: 'list', routerLink: ['/bitacoras/apiExternal'] });
                    itemsModel.push({ label: this.i18nService.getLabels("menu")["apiAdministrativo"], icon: 'list', routerLink: ['/bitacoras/apiAdministrativa'] });
                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["bitacora"], icon: 'list', items: itemsModel }];
                }
                //Noticias
                if (this.mapPermisos.get('NewsFeed Lectura') || this.mapPermisos.get('Categorías Noticia Lectura')) {
                    let itemsModel = [];
                    if (this.mapPermisos.get('NewsFeed Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["listaNoticias"], icon: 'list', routerLink: ['/noticia'] });
                    }
                    if (this.mapPermisos.get('Categorias Noticia Lectura')) {
                        itemsModel.push({ label: this.i18nService.getLabels("menu")["categoriasNoticia"], icon: 'list', routerLink: ['/noticia/listaCategoriasNoticia'] });
                    }
                    this.model = [...this.model, { label: this.i18nService.getLabels("menu")["noticia"], icon: 'attach_money', items: itemsModel }];
                }
            }
        );
    }

    changeTheme(theme) {
        let themeLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('theme-css');
        let layoutLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('layout-css');

        themeLink.href = 'assets/theme/theme-' + theme + '.css';
        layoutLink.href = 'assets/layout/css/layout-' + theme + '.css';
    }
}

@Component({
    selector: '[app-submenu]',
    template: `
        <ng-template ngFor let-child let-i="index" [ngForOf]="(root ? item : item.items)">
            <li [ngClass]="{'active-menuitem': isActive(i)}" *ngIf="child.visible === false ? false : true">
                <a [href]="child.url||'#'" (click)="itemClick($event,child,i)" class="ripplelink"
                 *ngIf="!child.routerLink" [attr.tabindex]="!visible ? '-1' : null">
                    <i class="material-icons">{{child.icon}}</i>
                    <span>{{child.label}}</span>
                    <i class="material-icons" *ngIf="child.items">keyboard_arrow_down</i>
                </a>

                <a (click)="itemClick($event,child,i)" class="ripplelink" *ngIf="child.routerLink"
                    [routerLink]="child.routerLink" routerLinkActive="active-menuitem-routerlink"
                     [routerLinkActiveOptions]="{exact: true}" [attr.tabindex]="!visible ? '-1' : null">
                    <i class="material-icons">{{child.icon}}</i>
                    <span>{{child.label}}</span>
                    <i class="material-icons" *ngIf="child.items">keyboard_arrow_down</i>
                </a>
                <ul app-submenu [item]="child" *ngIf="child.items" [@children]="isActive(i) ? 
                'visible' : 'hidden'" [visible]="isActive(i)" [reset]="reset"></ul>
            </li>
        </ng-template>
    `,
    animations: [
        trigger('children', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class AppSubMenuComponent {

    @Input() item: MenuItem;
    @Input() root: boolean;
    @Input() visible: boolean;

    _reset: boolean;
    activeIndex: number;

    constructor(
        @Inject(forwardRef(() => AppComponent))
        public app: AppComponent,
        public router: Router,
        public location: Location) { }

    itemClick(event: Event, item: MenuItem, index: number) {
        //avoid processing disabled items
        if (item.disabled) {
            event.preventDefault();
            return true;
        }

        //activate current item and deactivate active sibling if any
        this.activeIndex = (this.activeIndex === index) ? null : index;

        //execute command
        if (item.command) {
            item.command({ originalEvent: event, item: item });
        }

        //prevent hash change
        if (item.items || (!item.url && !item.routerLink)) {
            event.preventDefault();
        }

        //hide menu
        if (!item.items) {
            if (this.app.isHorizontal())
                this.app.resetMenu = true;
            else
                this.app.resetMenu = false;

            this.app.overlayMenuActive = false;
            this.app.staticMenuMobileActive = false;
        }
    }

    isActive(index: number): boolean {
        return this.activeIndex === index;
    }

    @Input() get reset(): boolean {
        return this._reset;
    }

    set reset(val: boolean) {
        this._reset = val;

        if (this._reset && this.app.isHorizontal()) {
            this.activeIndex = null;
        }
    }
}