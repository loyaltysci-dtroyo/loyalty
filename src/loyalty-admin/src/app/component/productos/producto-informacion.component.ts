import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Response } from '@angular/http';
import { ConfirmationService, SelectItem, MenuItem } from 'primeng/primeng';
import { Producto, Ubicacion } from '../../model/index';
import { ProductoService, UbicacionService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_PRODUCTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'producto-informacion-component',
    templateUrl: 'producto-informacion.component.html',
    providers: [Ubicacion, UbicacionService, ConfirmationService]
})

/**
 * Jaylin Centeno
 * Componente que permite mostrar el detalle de información del producto
 */
export class ProductoInformacionComponent implements OnInit {

    public formProducto: FormGroup; // Form group para validar el formulario de producto
    public estado: string = "P"; //Contendra el estado de las ubicaciones
    public escritura: boolean; //Permite verificar permisos (true/false)
    public nombreInterno: string; //Nombre interno actual del producto
    public listaTags: string[] = [];
    public nombreExiste: boolean; //Validación del nombre interno
    public cargandoDatos: boolean = true;

    constructor(
        public producto: Producto,
        public productoService: ProductoService,
        public ubicacion: Ubicacion,
        public ubicacionService: UbicacionService,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public confirmationService: ConfirmationService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PRODUCTOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Validacion del formulario y datos por defecto
        this.formProducto = new FormGroup({
            'codProducto' : new FormControl('', Validators.required),
            'nombre': new FormControl('', Validators.required),
            'nombreInterno': new FormControl('', Validators.required),
            'precio': new FormControl('', Validators.required),
            'saldoInicial': new FormControl(''),
            'saldoDisponible': new FormControl(''),
            'indImpuesto': new FormControl('', Validators.required),
            'indEntrega': new FormControl('', Validators.required),
            'montoEntrega': new FormControl(''),
            'ubicacion': new FormControl(''),
            'descripcion': new FormControl('', Validators.required),
            'sku': new FormControl(''),
            'tags': new FormControl('')
        });
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
    * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
    * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
    * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
    * no siempre sea necesario llamar al api para actualizar los datos
    */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    /**
     * Método que se encarga traer el detalle del producto
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                this.productoService.obtenerProductoPorId(this.producto.idProducto).subscribe(
                    (data) => {
                        this.copyValuesOf(this.producto, data);
                        if (this.producto.tags != null) {
                            this.listaTags = this.producto.tags.split(" ")
                        }
                        this.cargandoDatos = false;
                        this.nombreInterno = this.producto.nombreInterno;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Establece el nombre interno a partir del nombre del producto
     */
    establecerNombres() {
        if (this.producto.nombre != "" && this.producto.nombre != null) {
            let temp = this.producto.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.producto.nombreInterno = temp;
            this.nombreInterno = this.producto.nombreInterno;
            this.validarNombreInterno();
        }
    }

    /**
     * Método encargado de validar la existencia del nombre interno de un producto
     */
    validarNombreInterno() {
        if (this.producto.nombreInterno == null || this.producto.nombreInterno.trim() == "") {
            this.nombreExiste = true;
            return;
        }
        if (this.producto.nombreInterno != this.nombreInterno) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.productoService.token = token;
                    this.productoService.verificarNombreInterno(this.producto.nombreInterno.trim()).subscribe(
                        (data) => {
                            this.nombreExiste = data;
                            if (this.nombreExiste) {
                                this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
                            }
                        }
                    );
                }
            );
        }
    }

    //Método que se encarga de actualizar la informacion del producto
    actualizarProducto() {
        if (this.nombreExiste) {
            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
        } else {
            //Si el usuario ingresa un monto de entrega, pero cambia el indEntrega
            if (this.producto.indEntrega == "U") {
                this.producto.montoEntrega = null;
            } else {
                this.producto.idUbicacion = null;
            }
            if (this.listaTags.length > 0) {
                let tag = "";
                this.listaTags.forEach(element => {
                    tag += element + " ";
                });
                this.producto.tags = tag.trim();
            }
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.productoService.token = token;
                    this.productoService.editarProducto(this.producto).subscribe(
                        (data) => {
                            this.mostrarDetalle();
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                        },
                        (error) => {
                            this.manejaError(error)
                        }
                    );
                }
            );
        }
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
