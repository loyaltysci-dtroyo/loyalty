import { Component, OnInit } from '@angular/core';
import { AtributoDinamicoProducto, ProductoAtributo, ProductoAtributoPk, Producto } from '../../model/index';
import { AtributoDinamicoProductoService, ProductoService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { PERM_ESCR_PRODUCTO_ATRIBUTO } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'producto-atributo-component',
    templateUrl: 'producto-atributo.component.html',
    providers: [AtributoDinamicoProducto, AtributoDinamicoProductoService, ProductoAtributo]
})

/**
 * Componente para ver y editar los atributos dinámicos del Producto
 */
export class ProductoAtributoComponent implements OnInit {

    //Id del producto pasado por la url
    public id: string;
    //Id del atributo seleccionado
    public idAtributo: string;
    //Lista con todos los atributos existentes
    public listaAtributos: AtributoDinamicoProducto[] = [];
    //Validar la lista de atributos no este vacía
    public listaAtribVacia: boolean = true;
    //Lista con los atributos relacionados
    public listaAtributosProducto: AtributoDinamicoProducto[] = [];
    //Validar lista relacionada vacia
    public listaVacia: boolean = true;
    public valorDefecto: boolean = true;
    //Guardara el valor que le asigne el usuario
    public valor: string;
    //Palabras clave de búsqueda
    public busqueda: string;
    //Cantidad total de entidades 
    public totalRecords: number;
    //Cantidad de filas
    public cantidad: number = 10;
    //Desplazamiento de la primera fila
    public filaDesplazamiento: number = 0;
    //Guarda las respuestas cuando el tipo de dato es Multiple
    public listaRespuesta: any[] = [];
    //Contiene la lista de respuestas seleccionadas
    public listaSeleccion: any[] = [];
    //Contiene la respuesta cuando es numérico o texto
    public respuesta: string = "";
    //Contiene el tipo de acción a realizar sobre el atributo seleccionado
    public accion: string;
    //Permite mostrar el cuadro de dialogo para editar el valor del atributo
    public editar: boolean = false;
    //Permite verificar permisos (true/false)
    public escritura: boolean;
    //Permite saber si hay carga de datos
    public cargandoDatos: boolean = true;
    public eliminar: boolean;

    constructor(
        public atributo: AtributoDinamicoProducto,
        public atributoService: AtributoDinamicoProductoService,
        public producto: Producto,
        public productoService: ProductoService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.obtenerAtributosProducto();
        this.obtenerAtributosNoProducto();
        // Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PRODUCTO_ATRIBUTO).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerAtributosProducto();
    }

    /**
     * Método para guardar el valor del atributo seleccionado
     * @param atributo 
     * @param accion 
     */
    atributoSeleccionado(atributo: AtributoDinamicoProducto, accion: string) {
        this.accion = accion;
        this.atributo = atributo;
        if (accion == "remover") {
            this.eliminar = true;
        } else {
            //Entra si el atributo es de respuesta multiple
            if (this.atributo.indTipo == "M") {
                this.listaSeleccion = [];
                this.listaRespuesta = [];
                let temp = this.atributo.valorOpciones.split("&");
                temp.forEach(element => {
                    this.listaRespuesta.push({ label: element, value: element });
                });
                if (this.atributo.valorAtributo != undefined) {

                    let temp2 = this.atributo.valorAtributo.split("&");
                    temp2.forEach(element => {
                        this.listaSeleccion.push(element);
                    });
                }
            } else {
                this.respuesta = this.atributo.valorOpciones;
            }
            this.editar = true;
        }
    }

    /**
     * Se llama cuando se desea asignar o actualizar un atributo según la acción
     */
    manejoDeAtributo() {
        this.editar = false;
        if (this.accion == "asignar") {
            this.asignarAtributoProducto();
        } else {
            this.actualizarAtributoProducto();
        }
    }

    /**
     * Método para obtener los atributos relacionados con el producto
     */
    obtenerAtributosProducto() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.productoService.token = token || "";
                this.productoService.obtenerAtributosPorProducto(this.producto.idProducto, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        //Se obtiene la lista de las categorías del premio
                        this.listaAtributosProducto = response.json();
                        //Se recibe el total de las categorías disponibles
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaAtributosProducto.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    /**
     * Método para obtener los atributos que no están relacionados con el producto
     */
    obtenerAtributosNoProducto() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.productoService.token = token || "";
                this.productoService.obtenerAtributosNoProducto(this.producto.idProducto, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        //Se obtiene la lista de las categorías del premio
                        this.listaAtributos = response.json();
                        //Se recibe el total de las categorías disponibles
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaAtributos.length > 0) {
                            this.listaAtribVacia = false;
                        }
                        else {
                            this.listaAtribVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Método que sacar de la lista los atributos que fueron asignados
    sacarAtributo() {
        let listaTemporal = this.listaAtributos;
        let posicion;
        posicion = this.listaAtributos.findIndex(element => element.idAtributo == this.atributo.idAtributo)
        this.listaAtributos.splice(posicion, 1);
        this.listaAtributos = [...this.listaAtributos];
        this.obtenerAtributosProducto();
    }

    //Método para sacar los atributos de los asignados
    sacarAtributoAsignado() {
        let listaTemporal = this.listaAtributosProducto;
        let posicion;
        posicion = this.listaAtributosProducto.findIndex(element => element.idAtributo == this.atributo.idAtributo)
        this.listaAtributosProducto.splice(posicion, 1);
        this.listaAtributosProducto = [...this.listaAtributosProducto];
        this.obtenerAtributosNoProducto();
    }

    //Método que permite asignar una atributo dinámico a un producto
    asignarAtributoProducto() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.atributoService.token = token || "";
                if (this.atributo.indTipo == "M") {
                    this.respuesta = "";
                    for (let i = 0; i < this.listaSeleccion.length; i++) {
                        if (i == (this.listaSeleccion.length - 1)) {
                            this.respuesta += this.listaSeleccion[i];
                        } else {
                            this.respuesta += this.listaSeleccion[i] + "&";
                        }
                    }
                }
                this.atributoService.asignarAtributoProducto(this.producto.idProducto, this.atributo.idAtributo, this.respuesta).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                        this.sacarAtributo();
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Método que permite actualizar el valor del atributo dinámico
    actualizarAtributoProducto() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.atributoService.token = token || "";
                if (this.atributo.indTipo == "M") {
                    this.respuesta = "";
                    for (let i = 0; i < this.listaSeleccion.length; i++) {
                        if (i == (this.listaSeleccion.length - 1)) {
                            this.respuesta += this.listaSeleccion[i];
                        } else {
                            this.respuesta += this.listaSeleccion[i]+ "&";
                        }
                    }
                }
                this.atributoService.actualizarAtributoProducto(this.producto.idProducto, this.atributo.idAtributo, this.respuesta).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                        this.obtenerAtributosProducto();
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Método para remover la asociación del atributo dinámico con el producto
    removerAsociacionAtributo() {
        this.eliminar = false;
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.atributoService.token = token || "";
                this.atributoService.removerAsignacionAtributo(this.producto.idProducto, this.atributo.idAtributo).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-exclusion-simple'));
                        this.sacarAtributoAsignado();
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}