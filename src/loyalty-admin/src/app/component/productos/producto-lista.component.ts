import { Component, OnInit, ViewChild, Renderer, ElementRef, OnChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Message, ConfirmDialogModule, DataListModule, ConfirmationService, TooltipModule, InputTextareaModule, ToolbarModule } from 'primeng/primeng';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Producto, CategoriaProducto } from '../../model/index';
import { ProductoService, CategoriaProductoService, KeycloakService, I18nService } from '../../service/index';
import * as Routes from '../../utils/rutas';
import { PERM_ESCR_PRODUCTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'producto-lista-component',
    templateUrl: 'producto-lista.component.html',
    providers: [Producto, ProductoService, CategoriaProducto, CategoriaProductoService, ConfirmationService],

})

/**
 * Jaylin Centeno
 * Componente de Lista de productos
 */
export class ProductoListaComponent implements OnInit {

    public escritura: boolean; //Permite verificar permisos (true/false)
    public cargandoDatos : boolean = true; //Utilizado para mostrar la carga de datos
    public publicada: boolean; //Maneja el tipo de estado del producto seleccionado
    public eliminar: boolean; //Habilita el cuadro de dialogo 

    // --- Productos --- //
    public idProducto: string; //Contendra el id del producto seleccionado
    public listaProductos: Producto[]; //Lista de los productos del sistema
    public listaVacia: boolean = true; //Para verificar si la lista de productos esta vacía
    public cantidad: number = 10; //Cantidad de filas
    public totalRecords: number; //Total de productos en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    
    // --- Búsqueda --- //
    public busqueda: string; //Palabras clave de búsqueda
    public listaBusqueda: string[] = [];
    public avanzada: boolean = false; //Permite activar la búsqueda avanzada
    public estado: string[] = []; //Estado A = archivado, B = borrador P = publicado I = inactivo
    
    constructor(
        public producto: Producto,
        public productoService: ProductoService,
        public categoria: CategoriaProductoService,
        public i18nService: I18nService,
        public router: Router,
        public renderer: Renderer,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public confirmationService: ConfirmationService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PRODUCTOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    //Método que se ejecuta al entrar por primera vez al componente
    ngOnInit() {
        this.busquedaProductos();
    }

    /**
     * Método para mostrar el dialogo de confirmación
     * @param idProducto 
     * @param estado 
     */
    confirmar(idProducto: string, estado: string) {
        if (estado == "P") {
            this.publicada = true;
        } else {
            this.publicada = false;
        }
        this.eliminar = true;
        this.idProducto = idProducto;
    }

    /**
     * 
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     * @param event 
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.busquedaProductos();
    }

    /**
     * Método que se encarga de traer la lista de los productos
     */
    busquedaProductos() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                this.productoService.busquedaProducto(this.estado, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaProductos = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaProductos.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    /**
     * Redirecciona a la página para insertar un producto
     */
    nuevoProducto() {
        let link = ['productos/insertarProducto'];
        this.router.navigate(link);
    }

    /**
     * Permite remover un producto
     */
    removerProducto() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                this.productoService.eliminarProducto(this.idProducto).subscribe(
                    (data) => {
                        if (this.producto.indEstado == 'P') {
                            this.msgService.showMessage(this.i18nService.getLabels('general-archivar'));
                        } else {
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                        }
                        this.eliminar = false;
                        this.limpiarLista();
                    },
                    (error) =>{ this.manejaError(error); this.eliminar = false;}
                );
            }
        );
    }

    /**
     * limpia la lista despues de remover el producto
     */
    limpiarLista(){
        let index =  this.listaProductos.findIndex( element => element.idProducto == this.idProducto)
        this.listaProductos.splice(index, 1);
        this.listaProductos = [...this.listaProductos];
    }

    /**
     * Método que redirecciona al detalle del producto seleccionado
     * @param idProducto 
     */
    gotoDetail(idProducto: string) {
        let link = ['productos/detalleProducto', idProducto];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
