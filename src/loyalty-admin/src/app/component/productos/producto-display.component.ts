import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {
    Message, ConfirmDialogModule, ConfirmationService, TooltipModule, DropdownModule,
    SelectItem, FileUploadModule, RadioButtonModule
} from 'primeng/primeng';

import { Producto } from '../../model/index';
import { ProductoService, KeycloakService, I18nService } from '../../service/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { PERM_ESCR_PRODUCTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { read } from 'fs';

@Component({
    moduleId: module.id,
    selector: 'producto-display.component',
    templateUrl: 'producto-display.component.html',
})

/**
 * Jaylin Centeno
 * Componente utilizado para el display de los prodcutos
 */
export class ProductoDisplayComponent implements OnInit {

    public id: string; //Id del producto
    public formDisplay: FormGroup; // Form group para validar el formulario de producto
    public imagen: any; //Contendra el archivo subido por el usuario
    public avatar: any; //Guarda la dirección de la imagen
    public urlBase64: string[]; //Se guarda el array de la imagen
    public escritura: boolean; //Permite verificar permisos (true/false)
    public subiendoImagen: boolean = false;
    public cargandoDatos : boolean = true;

    constructor(
        public producto: Producto,
        public productoService: ProductoService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PRODUCTOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Validacion del formulario y datos por defecto
        this.formDisplay = new FormGroup({
            'encabezado': new FormControl('', Validators.required),
            'subEncabezado': new FormControl(''),
            'detalle': new FormControl('', Validators.required)
        });
    }

    //Método que se ejecuta al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
                objDestino[property] = objFuente[property];
        }
    }

    /**
     * Método que se encarga traer el detalle del producto
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.productoService.token = token;
                this.productoService.obtenerProductoPorId(this.producto.idProducto).subscribe(
                    (data) => {
                        this.copyValuesOf(this.producto, data);
                        this.avatar = this.producto.imagenArte;
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Método que se encarga de actualizar la informacion del producto
     */
    actualizarProducto() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.productoService.token = token;
                if(this.producto.imagenArte != null){
                    this.subiendoImagen = true;
                }
                this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
                let sub = this.productoService.editarProducto(this.producto).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                        this.mostrarDetalle();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.subiendoImagen = false;
                    },
                    () => { sub.unsubscribe(); this.subiendoImagen = false; }
                );
            }
        );
    }

    /**
     * Obtiene la imagen subida por el usuario y la pasa a base64
     */
    subirImagen() {
        this.imagen = document.getElementById("avatar");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.avatar = reader.result;
            let urlBase64 = this.avatar.split(",");
            this.producto.imagenArte = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}