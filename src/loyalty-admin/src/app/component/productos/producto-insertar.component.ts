import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import {
    Message, ConfirmDialogModule, ConfirmationService, TooltipModule,
    DropdownModule, SelectItem, FileUploadModule, RadioButtonModule, ListboxModule
} from 'primeng/primeng';
import { Producto, Ubicacion } from '../../model/index';
import { ProductoService, UbicacionService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_PRODUCTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'producto-insertar-component',
    templateUrl: 'producto-insertar.component.html',
    providers: [Producto, ProductoService, Ubicacion, UbicacionService]

})

/**
 * Jaylin Centeno
 * Componente que permite insertar un Producto
 */
export class ProductoInsertarComponent implements OnInit {

    public formProducto: FormGroup; // Form group para validar el formulario de producto
    public escritura: boolean; //Permite verificar permisos (true/false)    
    public ubicacionSeleccionada: Ubicacion; //Ubicacion seleccionada
    public nombreValido: boolean; //Validación del nombre interno

    constructor(
        public producto: Producto,
        public productoService: ProductoService,
        public router: Router,
        public msgService: MessagesService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PRODUCTOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Validación del formulario y datos por defecto
        this.formProducto = new FormGroup({
            'codProducto' : new FormControl('', Validators.required),
            'nombre': new FormControl('', Validators.required),
            'nombreInterno': new FormControl('', Validators.required),
            'descripcion': new FormControl('', Validators.required),
            'precio': new FormControl('', Validators.required),
            'indImpuesto': new FormControl('true', Validators.required),
            'indEntrega': new FormControl('', Validators.required),
            'montoEntrega': new FormControl(''),
            'ubicacion': new FormControl(''),
        });
    }

    ngOnInit() {
    }

    /**
     * Establece el nombre interno utilizando el nombre del producto
     */
    establecerNombres() {
        if (this.producto.nombre != "" || this.producto.nombre != null) {
            let temp = this.producto.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.producto.nombreInterno = temp;
        }
    }

    /**
     * Método encargado de validar la existencia del nombre interno de un producto una vez establecido
     */
    validarNombreInterno() {
        if (this.producto.nombreInterno == null || this.producto.nombreInterno.trim() == "" || this.producto.nombre.trim() == "" || this.producto.nombre == null) {
            this.nombreValido = false;
            return;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                this.productoService.verificarNombreInterno(this.producto.nombreInterno.trim()).subscribe(
                    (data) => {
                        this.nombreValido = data;
                    }
                );
            }
        );
    }

    /**
     * Método que se encarga de insertar productos
     */
    insertarProducto() {
        if (this.nombreValido) {
            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
            return;
        } else {
            if (this.producto.indEntrega == "D") {
                this.producto.idUbicacion == null
            } else {
                this.producto.montoEntrega == null
                this.producto.idUbicacion = this.ubicacionSeleccionada;
            }
            if ((this.producto.indEntrega == "D" && this.producto.montoEntrega != null) ||
                (this.producto.indEntrega == "U" && this.producto.idUbicacion != null)) {
                this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                    (token: string) => {
                        this.productoService.token = token;
                        this.productoService.insertarProducto(this.producto).subscribe(
                            (data) => {
                                this.producto.idProducto = data;
                                /**Redirecciona al detalle del producto */
                                let link = ['productos/detalleProducto', this.producto.idProducto];
                                this.router.navigate(link);
                            },
                            (error) => {
                                this.manejaError(error);
                            }
                        );
                    }
                );
            } else {
                this.msgService.showMessage(this.i18nService.getLabels('warn-dato-valido'));
            }
        }
    }

    /**
     * Redirecciona a la lista de productos
     */
    goBack() {
        let link = ['productos'];
        this.router.navigate(link);
    }

    // Método para manejar los errores que se provocan en las transacciones
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}
