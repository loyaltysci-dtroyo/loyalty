import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Response } from '@angular/http'
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ConfirmationService, SelectItem, MenuItem } from 'primeng/primeng';
import { Producto, Metrica, Nivel } from '../../model/index';
import { ProductoService, MetricaService, NivelService, KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_PRODUCTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { elementAt } from 'rxjs/operator/elementAt';

@Component({
    moduleId: module.id,
    selector: 'producto-limite-component',
    templateUrl: 'producto-limite.component.html',
    providers: [Producto, ProductoService, Metrica, MetricaService, Nivel, NivelService, ConfirmationService]
})

/**
 * Jaylin Centeno
 * Componente utilizado para asignar límites a los productos
 */
export class ProductoLimiteComponent implements OnInit {


    public id: string; //Id del producto
    public escritura: boolean; //Permite verificar permisos (true/false)    
    public formLimite: FormGroup; // Form group para validar el formulario de producto    
    public estado: string; //Contendra los estados del producto    
    public itemsRespuesta: SelectItem[] = []; //Contiene los elementos del intervalo de respuesta    
    public nivelItems: SelectItem[] = []; //Contiene los niveles a mostrar en el dropdownlist
    public listaNiveles: Nivel[]; //Lista de niveles
    public nivelVacio: boolean = true; //Validar si la lista esta vacia
    public niveles: boolean = false; //Mostrar el modal de niveles

    //--- Metrica ----//
    public metrica: Metrica;
    public listaMetricas: Metrica[]; //Lista de las métricas disponibles
    public listaVacia: boolean = true; //Verificar si la lista esta vacía
    public listaBusqueda: string[] = [];
    public totalRecordsMetrica: number;
    public cantidadMetricas: number = 5;
    public filaMetricas: number = 0;
    public nombreMetrica: string;
    public displayDialogMetrica: boolean;

    constructor(
        public producto: Producto,
        public productoService: ProductoService,
        public metricaService: MetricaService,
        public nivelService: NivelService,
        public route: ActivatedRoute,
        public router: Router,
        public msgService: MessagesService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public confirmationService: ConfirmationService,
        public i18nService: I18nService
    ) {
        //El id del producto se inicializa con el valor introducido por la url
        this.route.parent.params.forEach((params: Params) => { this.id = params['id'] });
        //Se obtiene los permisos para escritura de productos
        this.authGuard.canWrite(PERM_ESCR_PRODUCTOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Validacion del formulario y datos por defecto 
        this.formLimite = new FormGroup({
            'limiteIndRespuesta': new FormControl('true', Validators.required),
            "limiteCantTotal": new FormControl(''),
            "limiteIndIntervaloTotal": new FormControl(''),
            "limiteCantTotalMiembro": new FormControl(''),
            "limiteIndIntervaloMiembro": new FormControl(''),
            "limiteIndIntervaloRespuesta": new FormControl(''),
            "limiteCantInterRespuesta": new FormControl(''),
            "cantMinMetrica": new FormControl(''),
            "idMetrica": new FormControl(''),
            "idNivelMinimo": new FormControl('')
        });
        //Lista predeterminada con los limites por período
        this.itemsRespuesta.push({ value: null, label: '--' });
        this.i18nService.getLabels("general-limites-periodo").forEach(
            element => {
                this.itemsRespuesta.push(element);
            }
        )
    }

    //Método que se ejecuta al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
        this.obtenerListaMetrica();
    }

    /**
     * 
     * Instead of loading the entire data, small chunks of data is loaded by invoking onLazyLoad callback 
     * everytime paging, sorting and filtering happens. 
     * @param event 
     */
    loadDataMetrica(event: any) {
        this.filaMetricas = event.first;
        this.cantidadMetricas = event.rows;
        this.obtenerListaMetrica();
    }

    /**
     * Llamado al seleccionar la métrica
     * @param metrica 
     */
    seleccionarMetrica(metrica: Metrica) {
        this.metrica = metrica;
        this.nombreMetrica = metrica.nombre;
        this.displayDialogMetrica = false;
        this.producto.idMetrica = metrica;
        this.cambioMetrica();
    }

    //Método que se encarga traer el detalle del producto
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                this.productoService.obtenerProductoPorId(this.id).subscribe(
                    (data) => {
                        this.producto = data;
                        this.nombreMetrica = this.producto.idMetrica.nombre;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Método que se encarga de traer las metricas existentes
     */
    obtenerListaMetrica() {
        let busqueda = "";
        busqueda = this.listaBusqueda.join(" ")
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.metricaService.token = token;
                //Se carga la lista con los valores que provee el servicio
                this.metricaService.buscarMetricas(['P'], this.cantidadMetricas, this.filaMetricas, busqueda).subscribe(
                    (response: Response) => {
                        this.listaMetricas = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecordsMetrica = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaMetricas.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /**
     * Llamado cuando hay un cambio en métrica
     */
    cambioMetrica() {
        this.niveles = true;
        this.obtenerNiveles();
    }

    /**
     * Método que obtiene la lista de todos los niveles asociados a la metrica seleccionada
     */
    obtenerNiveles() {
        this.nivelItems = [];
        this.nivelItems = [...this.nivelItems, this.i18nService.getLabels('general-default-select')];
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.nivelService.token = token || "";
                this.nivelService.obtenerListaNiveles(this.producto.idMetrica.grupoNiveles.idGrupoNivel).subscribe(
                    (data) => {
                        this.listaNiveles = data;
                        if (this.listaNiveles.length > 0) {
                            this.nivelVacio = false;
                            this.listaNiveles.forEach(nivel => {
                                this.nivelItems = [...this.nivelItems, { label: nivel.nombre, value: nivel }];
                            });
                        } else {
                            this.nivelVacio = true;
                        }
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    //Método que se encarga de actualizar la informacion del producto
    actualizarProducto() {
        if (this.producto.limiteIndRespuesta) {
            if (this.producto.limiteCantTotalMiembro > 1 && this.producto.limiteIndIntervaloRespuesta == null) {
                this.msgService.showMessage(this.i18nService.getLabels('warn-dato-valido'));
                return;
            }
            if (this.producto.limiteCantTotal != null && this.producto.limiteIndIntervaloTotal != null && this.producto.limiteCantTotalMiembro != null && this.producto.limiteIndIntervaloMiembro != null && this.producto.limiteCantInterRespuesta != null) {

                this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                    (token: string) => {
                        this.productoService.token = token;
                        this.productoService.editarProducto(this.producto).subscribe(
                            (data) => {
                                this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                                this.mostrarDetalle();
                            },
                            (error) => {
                                this.manejaError(error)
                            }
                        );
                    }
                );

            } else {
                this.msgService.showMessage(this.i18nService.getLabels('warn-dato-valido'));
            };
        } else {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.productoService.token = token;
                    this.productoService.editarProducto(this.producto).subscribe(
                        (data) => {
                            this.mostrarDetalle();
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                        },
                        (error) => {
                            this.manejaError(error)
                        }
                    );
                }
            );
        }
    }

    /**
     * Manejo de error en la respuesta del observable
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}