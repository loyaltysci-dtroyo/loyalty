import { Component, OnInit } from '@angular/core';
import { AtributoDinamicoProducto } from '../../../model/index';
import { AtributoDinamicoProductoService, KeycloakService, I18nService } from '../../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Response, RequestOptionsArgs } from '@angular/http';
import { PERM_ESCR_PRODUCTO_ATRIBUTO } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import * as Rutas from '../../../utils/rutas';

@Component({
    moduleId: module.id,
    selector: 'atributo-producto-lista-component',
    templateUrl: 'atributo-producto-lista.component.html',
    providers: [AtributoDinamicoProducto, AtributoDinamicoProductoService]
})
/**
 * Jaylin Centeno
 * Componente en el cual se listan todos los atributos de producto
 * Se puede ir al detalle o archivar/remover
 */
export class AtributoProductoListaComponent {

    public escritura: boolean; //Permiso de escritura sobre atributo
    public cargandoDatos: boolean = true; //Muestra la carga de datos
    public eliminar: boolean; //Habilita el cuadro de dialogo 
    public idAtributo: string; //Id del atributo seleccionado
    public estado: string; // Estado del atributo seleccionado
    public questBorrar: string; //Guarda la pregunta que se muestra al borrar
    public questArchivar: string; //Guarda la pregunta que se muestra al archivar

    // ------  Atributos  -------  //
    public listaAtributos: AtributoDinamicoProducto[]; //Array que contendra la lista de atributos existentes
    public listaVacia: boolean; //Para verificar si la lista esta vacia    
    public cantidad: number = 10; //Cantidad de rows
    public totalRecords: number; //Total de miembros en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    // ------   Busqueda   ------ //
    public avanzada: boolean = false; //Para mostrar la búsqueda avanzada
    public busqueda: string = ""; //Palabras clave de búsqueda
    public listaBusqueda: string[] = [];
    public tipoAtributo: string[] = []; //Almacena los indicadores del tipo
    public estadoAtributo: string[] = ['P', 'B']; //Almacena los indicadores del estado 

    constructor(
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public i18nService: I18nService,
        public keycloakService: KeycloakService,
        public atributo: AtributoDinamicoProducto,
        public atributoService: AtributoDinamicoProductoService,

    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PRODUCTO_ATRIBUTO).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }
    //Método que se ejecuta al entrar por primera vez al componente
    ngOnInit() {
        this.busquedaAtributos();
        this.questBorrar = this.i18nService.getLabels("confirmacion-pregunta")["B"];
        this.questArchivar = this.i18nService.getLabels("confirmacion-pregunta")["A"];
    }
    /**
     * Método utilizado para la carga de datos de manera perezosa
     */
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
    }
    /**
     * Facilita la búsqueda de atributos que cumplan con los parámetros proporcionados
     * Estado = publicado, borrador o archivado 
     * Y los terminos de busqueda 
     */
    busquedaAtributos() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.atributoService.token = token;
                this.atributoService.busquedaAtributosProducto(this.estadoAtributo, this.cantidad, this.filaDesplazamiento, this.tipoAtributo, this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaAtributos = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaAtributos.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => { this.manejaError(error); this.cargandoDatos = false;}
                );
            }
        );
    }
    /** 
     * Método para mostrar el dialogo de confirmación
     */
    dialogoConfirmar(atributo: AtributoDinamicoProducto) {
        this.idAtributo = atributo.idAtributo;
        this.estado = atributo.indEstado;
        this.eliminar = true;
    }
    /**
     * Permite ir a la pantalla de ingreso de atributos
     */
    nuevoAtributo() {
        let link = ['productos/insertarAtributoProducto'];
        this.router.navigate(link);
    }
    /**
     * Redirecciona al detalle de la ubicación seleccionada
     */
    atributoSeleccionado(idAtributo: string) {
        let link = ['productos/detalleAtributoProducto', idAtributo];
        this.router.navigate(link);
    }
    /**
     * Remueve o archiva una atributo dinámico según el estado
     */
    removerAtributo() {
        if (this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.atributoService.token = token;
                    this.atributoService.eliminarAtributo(this.idAtributo).subscribe(
                        (data) => {
                            if(this.atributo.indEstado == "B"){
                                this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                            }else{
                                this.msgService.showMessage(this.i18nService.getLabels("general-archivar"));
                            }
                            this.eliminar = false;
                            this.busquedaAtributos();
                        },
                        (error) => {this.manejaError(error); this.eliminar = false;}
                    );
                }
            );
        }
    }
    /**
     * Metodo para manejar los errores que se provocan en las transacciones
     */ 
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}