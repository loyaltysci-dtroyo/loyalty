import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AtributoDinamicoProducto } from '../../../model/index';
import { AtributoDinamicoProductoService, KeycloakService, I18nService } from '../../../service/index';
import { SelectItem } from 'primeng/primeng';
import { ValidationService, ControlMessagesComponent } from '../../../utils/index';
import { PERM_ESCR_PRODUCTO_ATRIBUTO } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { AppConfig } from '../../../app.config';

@Component({
    moduleId: module.id,
    selector: 'atributo-producto-insertar-component',
    templateUrl: 'atributo-producto-insertar.component.html',
    providers: [AtributoDinamicoProducto, AtributoDinamicoProductoService, ControlMessagesComponent]
})

/**
 * Jaylin Centeno
 * Componente para realizar la inserción de un atributo de producto
 */
export class AtributoProdcutoInsertarComponent implements OnInit {

    public escritura: boolean; //Permiso de escritura
    public subiendoImagen: boolean = false; //Carga de la imagen
    public formAtributo: FormGroup; // Form usado para validar y asignar valores por default
    public tipoItems: SelectItem[]; //Lista para Dropdown de tipos de datos
    public tipoTemporal: string; //Representa al tipo de dato del atributo
    public imagen: any; //Contendra el archivo subido por el usuario
    public imgAtributo: any;
    public respuesta: string; //Guarda la respuesta cuando es multiple
    public listaRespuesta: string[] = []; //Guarda las respuestas cuando el tipo de dato es Multiple
    public temporal: string = ""; //Guarda la lista de respuestas

    constructor(
        public atributo: AtributoDinamicoProducto,
        public atributoService: AtributoDinamicoProductoService,
        public router: Router,
        public fb: FormBuilder,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public validationService: ValidationService,
        public i18nService: I18nService
    ) { }

    ngOnInit() {
        this.buildForm();
        this.imgAtributo = `${AppConfig.DEFAULT_IMG_URL}`;;

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PRODUCTO_ATRIBUTO).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        //Se llena el Dropdown que muestra los tipos de atributo 
        this.tipoItems = [];
        this.tipoItems = this.i18nService.getLabels("atributo-producto-tipoItems");

    }
    /**
     * Método para construir el formulario
     */
    buildForm(): void {
        this.formAtributo = this.fb.group({
            'nombre': [this.atributo.nombre, Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(150)
            ])],
            'nombreInterno': [this.atributo.nombreInterno, Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(150)
            ])],
            'indTipo': [this.atributo.indTipo, Validators.required],
            'indRequerido': [this.atributo.indRequerido, Validators.required],
            'valorNumero': ['', Validators.pattern('[0-9]*')],
            'valorTexto': ['', Validators.maxLength(300)],
            'valorMultiple': ['', Validators.maxLength(300)]
        });
    }

    /**
     * Llamado al ingresar una nueva respuesta
     * Solo se utiliza para opciones de respuesta multiple
     */
    nuevaRespuesta() {
        this.respuesta = this.respuesta.trim();
        if (this.respuesta != null && this.respuesta != '') {
            this.listaRespuesta = [...this.listaRespuesta, this.respuesta];
            this.respuesta = "";
        }
    }

    /**
     * Remover una respuesta de la lista de respuestas
     */
    removerRespuesta(respuesta: string) {
        let index = this.listaRespuesta.findIndex((elemento) => { return elemento == respuesta });
        if (index >= 0) {
            this.listaRespuesta.splice(index, 1);
            this.listaRespuesta = [... this.listaRespuesta];
        }
    }
    /**
     * Permite establecer el nombre interno del atributo
     */
    establecerNombre() {
        if (this.formAtributo.get('nombre').value != "" || this.formAtributo.get('nombre').value != null) {
            let temp = this.formAtributo.get('nombre').value.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.atributo.nombreInterno = temp;
            this.formAtributo.patchValue({ nombreInterno: temp });

        }
    }

    /**
     * Método que permite insertar un nuevo atributo de producto
     */
    agregarAtributo() {
        if (this.escritura) {
            this.atributo.indRequerido = this.formAtributo.get('indRequerido').value;
            this.atributo.nombre = this.formAtributo.get('nombre').value;
            this.atributo.indTipo = this.formAtributo.get('indTipo').value;
            if (this.listaRespuesta.length > 1 && this.formAtributo.get('indTipo').value == "M" || this.formAtributo.get('indTipo').value == "T" && this.formAtributo.get('valorTexto').value != ""
                || this.formAtributo.get('indTipo').value == "N" && this.formAtributo.get('valorNumero').value != "") {

                if (this.atributo.imagen != null) {
                    this.msgService.showMessage(this.i18nService.getLabels('info-uploading'));
                }
                this.subiendoImagen = true;
                this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                    (token: string) => {
                        this.atributoService.token = token;

                        if (this.formAtributo.get('indTipo').value == "M") {
                            for (let i = 0; i < this.listaRespuesta.length; i++) {
                                if (i == (this.listaRespuesta.length - 1)) {
                                    this.temporal += this.listaRespuesta[i];
                                } else {
                                    this.temporal += this.listaRespuesta[i] + "&";
                                }
                            }
                            this.atributo.valorOpciones = this.temporal.trim();
                        } else if (this.formAtributo.get('indTipo').value == "T") {
                            this.atributo.valorOpciones = this.formAtributo.get('valorTexto').value;
                        } else {
                            this.atributo.valorOpciones = this.formAtributo.get('valorNumero').value;
                        }
                        let sub = this.atributoService.agregarAtributo(this.atributo).subscribe(
                            (data) => {
                                this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                                let link = ['productos/detalleAtributoProducto', data];
                                this.router.navigate(link);
                            },
                            (error) => { this.manejaError(error); this.subiendoImagen = false; sub.unsubscribe(); },
                            () => { sub.unsubscribe(); this.subiendoImagen = false; }
                        );
                    });
            } else {
                if (this.listaRespuesta.length > 1 && this.formAtributo.get('indTipo').value == "M") {
                    this.msgService.showMessage(this.i18nService.getLabels('warn-atributo-respuesta'));
                } else {
                    this.msgService.showMessage(this.i18nService.getLabels('warn-atributo-valor'));
                }
            }
        }
    }

    /**
     * Método para evaluar el cambio de valor por defecto
     * evita que se produzca un error a la hora del despliegue.
     */
    cambioTipoDato() {
        if (this.formAtributo.get('indTipo').value != this.tipoTemporal) {
            this.formAtributo.patchValue({ valorOpciones: "" });
            this.tipoTemporal = this.formAtributo.get('indTipo').value;
        }
    }

    /**
     * Método para obtener y guardar la imagen seleccionada por el usuario
     */
    subirImagen() {
        //se obtiene el elemento con el id de imgAtributo (donde esta el archivo)
        this.imagen = document.getElementById("imgAtributo");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.imgAtributo = reader.result;
            let urlBase64 = this.imgAtributo.split(",");
            this.atributo.imagen = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /**
     * Metodo que permite regresar en la navegación
     */
    goBack() {
        let link = ['productos/listaAtributosProducto'];
        this.router.navigate(link);
    }

    /**
     *  Metodo para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}