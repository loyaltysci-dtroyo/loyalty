import { Component, OnInit } from '@angular/core';
import { AtributoDinamicoProducto } from '../../../model/index';
import { AtributoDinamicoProductoService, KeycloakService, I18nService } from '../../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { PERM_ESCR_PRODUCTO_ATRIBUTO } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'atributo-producto-detalle-component',
    templateUrl: 'atributo-producto-detalle.component.html',
    providers: [AtributoDinamicoProducto, AtributoDinamicoProductoService]
})
/**
 * Jaylin Centeno
 * Componente utilizado para ver el detalle del atributo de producto
 */
export class AtributoProductoDetalleComponent implements OnInit {

    public id: string; //Id del atributo recibido en la url
    public escritura: boolean; //Permiso de usario para escribir
    public cargandoDatos: boolean = true; //Muestra la carga de datos
    public eliminar: boolean; //Habilita el cuadro de dialogo para eliminar
    public formAtributo: FormGroup; // Form usado para validar y asignar valores por default
    public estado: string; //Representa el estado del atributo
    public tipoTemporal: string; //Representa al tipo de dato del atributo
    public respuesta: string; //Guarda la respuesta cuando es multiple
    public listaRespuesta: string[] = []; //Guarda las respuestas cuando el tipo de dato es Multiple
    public imagen: any;
    public imgAtributo: any = null;
    public subiendoDatos: boolean;
    // Estados del atributo
    public archivado: boolean;
    public borrador: boolean;
    public publicado: boolean;
    public questBorrar: string; //Guarda la pregunta que se muestra al borrar
    public questArchivar: string; //Guarda la pregunta que se muestra al archivar

    constructor(
        public atributo: AtributoDinamicoProducto,
        public atributoService: AtributoDinamicoProductoService,
        public authGuard: AuthGuard,
        public router: Router,
        public route: ActivatedRoute,
        public i18nService: I18nService,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
    ) {

        //Representa el id del atributo pasado por la url 
        this.route.params.forEach((params: Params) => { this.id = params['id'] });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PRODUCTO_ATRIBUTO).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //inicialización del form group
        this.formAtributo = new FormGroup({
            'nombre': new FormControl('', Validators.required),
            'nombreInterno': new FormControl('', Validators.required),
            'descripcion': new FormControl(''),
            'indRequerido': new FormControl('N', Validators.required),
            'valor': new FormControl('')
        });
    }

    ngOnInit() {
        this.mostrarDetalle();
        this.questBorrar = this.i18nService.getLabels("confirmacion-pregunta")["B"];
        this.questArchivar = this.i18nService.getLabels("confirmacion-pregunta")["A"];
    }

    /**
     * Obtiene la definición del atributo de producto
     */
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.atributoService.token = token;
                this.atributoService.obtenerAtributoId(this.id).subscribe(
                    (data) => {
                        this.atributo = data;
                        this.estado = this.atributo.indEstado;
                        this.tipoTemporal = this.atributo.indTipo;
                        this.imgAtributo = this.atributo.imagen;

                        if (this.atributo.indEstado == 'P') {
                            this.publicado = true;
                        } else if (this.atributo.indEstado == 'A') {
                            this.archivado = true;
                        } else {
                            this.borrador = true;
                        }
                        if (this.atributo.indTipo == "M") {
                            this.listaRespuesta = this.atributo.valorOpciones.split("&");
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => { this.manejaError(error); this.cargandoDatos = false; }
                );
            }
        );
    }

    /**
     * Método para obtener y guardar la imagen seleccionada por el usuario
     */
    subirImagen() {
        this.imagen = document.getElementById("imgAtributo");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.imgAtributo = reader.result;
            let urlBase64 = this.imgAtributo.split(",");
            this.atributo.imagen = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /**
     * Método para evaluar el cambio del tipo de dato
     * evita que se produzca un error a la hora del despliegue.
     */
    cambioTipoDato() {
        if (this.atributo.indTipo != this.tipoTemporal) {
            this.atributo.valorOpciones = "";
            this.tipoTemporal = this.atributo.indTipo;
        }
    }

    /**
     * LLamado al ingresar una nueva respuesta 
     * Solo se utiliza para opciones de respuesta multiple
     */
    nuevaRespuesta() {
        this.respuesta = this.respuesta.trim();
        if (this.respuesta != null && this.respuesta != '') {
            this.listaRespuesta = [...this.listaRespuesta, this.respuesta];
            this.respuesta = "";
        }
    }

    /**
     * Remover una respuesta de la lista de respuestas
     */
    removerRespuesta(respuesta: string) {
        let index = this.listaRespuesta.findIndex((elemento) => { return elemento == respuesta });
        if (index >= 0) {
            this.listaRespuesta.splice(index, 1);
            this.listaRespuesta = [... this.listaRespuesta];
        }
    }
    /**
     * Método para desactivar el atributo actual
     */
    removerAtributo() {
        this.eliminar = false;;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.atributoService.token = token;
                this.atributoService.eliminarAtributo(this.id).subscribe(
                    (data) => {
                        if (this.estado == 'P') {
                            this.archivado = true;
                            this.atributo.indEstado = 'A';
                            this.publicado = this.borrador = false;
                            this.msgService.showMessage(this.i18nService.getLabels("general-archivar"));
                            this.mostrarDetalle();
                        } else {
                            this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion-simple"));
                            this.goBack();
                        }
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }
    /**
     * Permite establecer el nombre interno del atributo
     */
    establecerNombre() {
        if (this.atributo.nombre != null && this.atributo.nombre != "") {
            let temp = this.atributo.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.atributo.nombreInterno = temp;
        }
    }
    /**
     * Método para actualizar el atributo actual
     */
    actualizarAtributo() {
        this.atributo.valorOpciones.trim();
        if (this.listaRespuesta.length > 1 && this.atributo.indTipo == "M" || (this.atributo.indTipo == "T" && this.atributo.valorOpciones != null || this.atributo.valorOpciones != "")
            || (this.atributo.indTipo == "N" && this.atributo.valorOpciones != null || this.atributo.valorOpciones != "")) {
            this.subiendoDatos = true;
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    if (this.imgAtributo != null) {
                        this.msgService.showMessage(this.i18nService.getLabels("info-uploading"));
                    }
                    this.atributoService.token = token;
                    if (this.atributo.indTipo == "M") {
                        this.respuesta = "";
                        for (let i = 0; i < this.listaRespuesta.length; i++) {
                            if (i == (this.listaRespuesta.length - 1)) {
                                this.respuesta += this.listaRespuesta[i];
                            } else {
                                this.respuesta += this.listaRespuesta[i] + "&";
                            }
                        }
                        this.atributo.valorOpciones = this.respuesta.trim();
                        this.respuesta = "";
                    }
                    this.atributoService.editarAtributo(this.atributo).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                            this.mostrarDetalle();
                            this.subiendoDatos = false;
                        },
                        (error) => { this.manejaError(error); this.subiendoDatos = false; }
                    );
                }
            );
        } else {
            if (this.listaRespuesta.length > 1 && this.formAtributo.get('indTipo').value == "M") {
                this.msgService.showMessage(this.i18nService.getLabels('warn-atributo-respuesta'));
            } else {
                this.msgService.showMessage(this.i18nService.getLabels('warn-atributo-valor'));
            }
        }
    }

    /**
     * Método para publicar el atributo
     */
    publicarAtributo() {
        this.estado = this.atributo.indEstado;
        this.atributo.indEstado = 'P';
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.atributoService.token = token;
                this.atributoService.editarAtributo(this.atributo).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-publicar'));
                        this.mostrarDetalle();
                        if (this.estado == 'B') {
                            this.publicado = true;
                            this.archivado = this.borrador = false;
                        }
                    },
                    (error) => {
                        this.manejaError(error);
                        this.atributo.indEstado = 'B';
                        this.borrador = true;
                        this.archivado = this.publicado = false;
                    }
                );
            });
    }

    /**
     * Método que permite regresar a la lista de atributos
     */
    goBack() {
        let link = ['productos/listaAtributosProducto'];
        this.router.navigate(link);
        //window.history.back();
    }

    /**
     * Método para manejar los errores que se provocan en las transacciones
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}