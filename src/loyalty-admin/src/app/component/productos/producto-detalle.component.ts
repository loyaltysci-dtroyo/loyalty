import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Rutas from '../../utils/rutas';
import { SelectItem, TabViewModule, MenuItem } from 'primeng/primeng';

import { Producto } from '../../model/index';
import { ProductoService, KeycloakService, I18nService } from '../../service/index';

import { PERM_ESCR_PRODUCTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'producto-detalle-component',
    templateUrl: 'producto-detalle.component.html',
    providers: [Producto, ProductoService]
})

/** 
 * Jaylin Centeno
 * Componente que permite mostrar los detalles del producto
 */
export class ProductoDetalleComponent implements OnInit {

    public id: string; //Id del producto pasado por parametros
    public estado: string; //Contendra los estados del producto
    public items: MenuItem[]; //Representa la lista con los items del menú 
    //Items del menú
    public detalle = Rutas.RT_PRODUCTOS_DETALLE_INFORMACION;
    public elegibles = Rutas.RT_PRODUCTOS_DETALLE_ELEGIBLES;
    public limites = Rutas.RT_PRODUCTOS_DETALLE_LIMITE;
    public display = Rutas.RT_PRODUCTOS_DETALLE_DISPLAY;
    public redimidos = Rutas.RT_PRODUCTOS_DETALLE;
    public categorias = Rutas.RT_PRODUCTOS_DETALLE_CATEGORIA;
    public atributos = Rutas.RT_PRODUCTOS_DETALLE_ATRIBUTO;
    public calendarizacion = Rutas.RT_PRODUCTOS_DETALLE_CALENDARIZACION;
    public escritura: boolean; //Permite verificar permisos (true/false)
    public cargandoDatos: boolean = true;
    public publicado: boolean;
    public borrador: boolean;
    public archivado: boolean;
    //Lista para Dropdown//
    public efectividadItem: SelectItem[];
    public fecInicio: Date;
    public fecFin: Date;
    public dias: SelectItem[];
    public diasSelec: string[] = [];
    public today: Date = new Date();
    public esRecurrente: boolean = false;
    public formEfectividad: FormGroup;
    public eliminar: boolean;
    public accion: string;

    constructor(
        public producto: Producto,
        public productoService: ProductoService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //El id del producto se inicializa con el valor introducido por la url
        this.route.params.forEach((params) => { this.producto.idProducto = params['id'] });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PRODUCTOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        //Validacion del formulario y datos por defecto 
        this.formEfectividad = new FormGroup({
            'indCalendarizacion': new FormControl('true', Validators.required),
            "fechaInicio": new FormControl(''),
            "fechaFin": new FormControl(''),
            "diaRecurrencia": new FormControl(''),
            "semanaRecurrencia": new FormControl(''),
        });
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
        this.efectividadItem = [];
        this.efectividadItem = this.i18nService.getLabels("tipo-efectividad-general");

    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
            objDestino[property] = objFuente[property];
        }
    }

    dialogoEliminar(accion: string) {
        this.accion = accion;
        this.eliminar = true;
    }

    //Método que se encarga traer el detalle del producto
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                let sub = this.productoService.obtenerProductoPorId(this.producto.idProducto).subscribe(
                    (data) => {
                        this.copyValuesOf(this.producto, data);
                        if (this.producto.indEstado == 'P') {
                            this.publicado = true;
                        } else if (this.producto.indEstado == 'B') {
                            this.borrador = true;
                        } else {
                            this.archivado = true;
                        }
                        if (this.producto.efectividadIndCalendarizado == 'C') {
                            this.fecFin = new Date(this.producto.efectividadFechaFin);
                            this.fecInicio = new Date(this.producto.efectividadFechaInicio);
                            if(this.producto.efectividadIndDias || this.producto.efectividadIndSemana){
                                this.esRecurrente = true;
                            }
                            if (this.producto.efectividadIndDias != null && this.producto.efectividadIndDias != "") {
                                this.diasSelec = this.producto.efectividadIndDias.split('');
                            }
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error),
                    () => { sub.unsubscribe(); }
                );
            }
        );
    }

    //Método que se encarga de publicar, si este se encuentra en estado de borrador
    publicarProducto() {
        this.mostrarDetalle();
        //Si es calendarizado se pregunta por los datos requeridos
        if (this.producto.efectividadIndCalendarizado == 'C') {
            if (this.esRecurrente && this.diasSelec != null) {
                this.producto.efectividadIndDias = "";
                let diasS = ""
                this.diasSelec.forEach((element) => {
                    diasS += element;
                });
                this.producto.efectividadIndDias = diasS;
            }
            if (!this.esRecurrente) {
                this.producto.efectividadIndSemana = null;
                this.producto.efectividadIndDias = null;
            }
        } else {
            //Si no es calendarizado todos los datos se vuelven nulos
            this.producto.efectividadFechaFin = null;
            this.producto.efectividadFechaInicio = null;
            this.producto.efectividadIndSemana = null;
            this.producto.efectividadIndDias = null;
        }
        this.producto.indEstado = "P";
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                this.productoService.editarProducto(this.producto).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-publicar"));
                        this.publicado = true;
                        this.archivado = this.borrador = false;
                        this.mostrarDetalle();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.producto.indEstado = this.estado;
                        this.publicado = false;
                        this.borrador = true;
                    }
                );
            }
        );
    }

    //Método que se encarga de actualizar, si estese encuentra en estado de borrador
    actualizarProducto() {
        //Si es calendarizado se pregunta por los datos requeridos
        if (this.producto.efectividadIndCalendarizado == 'C') {
            if (this.esRecurrente && this.diasSelec != null) {
                this.producto.efectividadIndDias = "";
                for (let i = 0; i < this.diasSelec.length; i++) {
                    this.producto.efectividadIndDias += this.diasSelec[i];
                }
            }
            if (!this.esRecurrente) {
                this.producto.efectividadIndSemana = null;
                this.producto.efectividadIndDias = null;
            }
        } else {
            this.producto.efectividadFechaFin = null;
            this.producto.efectividadFechaInicio = null;
            this.producto.efectividadIndSemana = null;
            this.producto.efectividadIndDias = null;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                this.productoService.editarProducto(this.producto).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels("general-actualizacion"));
                        this.publicado = true;
                        this.archivado = this.borrador = false;
                        this.mostrarDetalle();
                    },
                    (error) => {
                        this.manejaError(error);
                        this.producto.indEstado = this.estado;
                        this.publicado = false;
                        this.borrador = true;
                    }
                );
            }
        );
    }

    //Método para remover un producto
    removerProducto() {
        this.estado = this.producto.indEstado;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                this.productoService.eliminarProducto(this.producto.idProducto).subscribe(
                    (data) => {
                        if (this.estado == 'P') {
                            this.archivado = true;
                            this.producto.indEstado = "A";
                            this.publicado = false;
                            this.msgService.showMessage(this.i18nService.getLabels("general-archivar"));
                        } else {
                            this.msgService.showMessage(this.i18nService.getLabels("general-eliminacion"));
                            this.goBack();
                        }
                    },
                    (error) => {
                        this.manejaError(error);
                        this.producto.indEstado = this.estado;
                        if (this.estado == 'P') {
                            this.publicado = true;
                            this.archivado = false;
                        } else {
                            this.borrador = true;
                        }
                    }
                );
            }
        );
    }

    // Método para regresar a la lista de productos
    goBack() {
        let link = ['productos'];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
