import { Component, OnInit, Renderer } from '@angular/core';
import { Response, Headers } from '@angular/http';
import { ErrorHandlerService, MessagesService } from '../../utils/index';
import { SelectItem } from 'primeng/primeng';
import { Producto, Segmento } from '../../model/index';
import { SegmentoService, ProductoService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_PRODUCTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';

@Component({
    moduleId: module.id,
    selector: 'producto-elegible-segmento-component',
    templateUrl: 'producto-elegible-segmento.component.html',
    providers: [SegmentoService, Segmento]
})

/**
 * Jaylin Centeno 
 * Componente que permite mostrar el detalle de los segmentos elegibles del producto
 */
export class ProductoSegmentoElegiblesComponent implements OnInit {

    public id: string; //Contiene el id del producto
    public cargandoDatos: boolean = true; //Permite saber si hay carga de datos
    public valor: string; //Para guardar el valor del indicador, si hay carga o no
    public eliminar: boolean;
    public escritura: boolean; //Permite verificar permisos (true/false)

    //-----Segmentos-------//
    public listaSegmentosDisponibles: Segmento[] = []; //Lista con los segmentos disponibles en el sistema
    public listaVacia: boolean = true; //Valida que la lista de disponibles no esta vacía
    public totalRecords: number; //Total de segmentos en el sistema
    public cantidad: number = 0; //Cantidad de filas
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public indAccion: string = 'D'; //Permite determinar la acción, ya sea incluir o excluir
    public elegible: string = "I"; //Indica cual es el elemento elegible a mostrar (Elegibles/Disponibles)

    public listaSegmentosElegibles: Segmento[] = []; //Lista con los segmentos asociados a un producto
    public listaVaciaEleg: boolean = true; //Valida que la lista de elegibles no este vacía
    public listaSeleccion: Segmento[] = []; //Lista que contendra la seleccion de los segmentos, para asociar o remover
    public listaRemover: Segmento[] = [];
    public lista: string[] = []; //Lista que contendra el id de los segmentos, para agregar o remover
    public itemsAccion: SelectItem[] = []; //Items del dropdown, muestra las acciones de incluir o excluir 
    public itemsElegibles: SelectItem[] = []; //Items del dropdown, muestra los elementos de elegibles
    public idSegmento: string; //Contendra el id del segmento a eliminar   

    public busqueda: string; //Búsqueda de segmentos, guarda las palabras para realizar las búsquedas
    public listaBusqueda: string[] = [];

    constructor(
        public producto: Producto,
        public productoService: ProductoService,
        public segmentoService: SegmentoService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public router: Router,
        public route: ActivatedRoute,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Se obtienen las listas predeterminadas en el sistema
        this.itemsElegibles = this.i18nService.getLabels("accion-elegibles-general");
        this.itemsAccion = this.i18nService.getLabels("tipos-elegibles-general");
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PRODUCTOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.obtenerSegmentosDisponibles();
    }

    /**
     * Método que muestra un cuadro de dialogo para pedir una confirmación
     * @param idSegmento 
     */
    dialogoConfirmar(idSegmento: string) {
        this.idSegmento = idSegmento;
        this.eliminar = true;
    }

    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerSegmentosDisponibles();
    }

    /**
     * Método para obtener la lista de los segmentos según la acción
     * Si la acción es incluir o excluir se obtendran los segmentos que ya estan en los elegibles
     * Si la acción es disponibles se obtendran los segmentos que no están en los elegibles
     */
    obtenerSegmentosDisponibles() {
        if (this.valor != this.indAccion) {
            this.cargandoDatos = true;
            this.valor = this.indAccion;
        }
        let busqueda = "";
        busqueda = this.listaBusqueda.join(" ")

        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                this.productoService.obtenerSegmentoElegiblesProducto(this.producto.idProducto, this.indAccion, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        if (this.indAccion == "D") {
                            this.listaSegmentosDisponibles = response.json();
                            if (this.listaSegmentosDisponibles.length > 0) {
                                this.listaVacia = false;
                            }
                            else {
                                this.listaVacia = true;
                            }
                            this.cargandoDatos = false;
                        } else {
                            this.listaSegmentosElegibles = response.json();
                            if (this.listaSegmentosElegibles.length > 0) {
                                this.listaVaciaEleg = false;
                            }
                            else {
                                this.listaVaciaEleg = true;
                            }
                            this.cargandoDatos = false;
                        }

                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                    },
                    (error) => this.manejaError(error)
                )
            }
        );
    }

    /**
     * Método para agregar un segmento a los elegibles del producto, 
     * Ya sea para incluirlo o excluirlo de los elegibles
     * @param idSegmento 
     */
    agregarSegmento(idSegmento: string) {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                let sub = this.productoService.agregarSegmento(this.producto.idProducto, idSegmento, this.elegible).subscribe(
                    (data) => {
                        if (this.elegible == "E") {
                            this.msgService.showMessage(this.i18nService.getLabels('general-exclusion-simple'));
                        } else {
                            this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                        }
                        this.obtenerSegmentosDisponibles();
                        if (this.listaSeleccion.length > 0) {
                            let index = this.listaSeleccion.findIndex(element => element.idSegmento == idSegmento);
                            this.listaSeleccion.splice(index, 1);
                        }
                    },
                    (error) => { this.manejaError(error); },
                );
            });
    }

    /**
     * Método para agregar la lista de segmentos a los elegibles de los productos 
     * Ya sea para incluir o excluir la lista de los elegibles
     */
    agregarListaSegmentos() {
        if (this.listaSeleccion.length >= 1) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.productoService.token = token;
                    this.listaSeleccion.forEach(segmento => {
                        this.lista = [...this.lista, segmento.idSegmento]
                    })
                    this.productoService.agregarListaSegmentos(this.producto.idProducto, this.lista, this.elegible).subscribe(
                        (data) => {
                            if (this.elegible == "E") {
                                this.msgService.showMessage(this.i18nService.getLabels('general-exclusion-multiple'));
                            } else {
                                this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-multiple'));
                            }
                            this.listaSeleccion = [];
                            this.lista = [];
                            this.obtenerSegmentosDisponibles();
                        },
                        (error) => { this.manejaError(error); },
                    );
                }
            );
        } else {
            this.msgService.showMessage(this.i18nService.getLabels('warn-cantidad'));
        }
    }

    /**
     * Permite remover una lista de segmentos de los incluidos/excluidos
     */
    removerListaSegmentos() {
        if (this.listaRemover.length >= 1) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.productoService.token = token;
                    this.listaRemover.forEach(segmento => {
                        this.lista = [...this.lista, segmento.idSegmento]
                    })
                    this.productoService.eliminarListaSegmentos(this.producto.idProducto, this.lista).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-multiple'));
                            this.obtenerSegmentosDisponibles();
                            this.listaRemover = [];
                            this.lista = [];
                        },
                        (error) => { this.manejaError(error); },
                    );
                }
            );
        } else {
            this.msgService.showMessage(this.i18nService.getLabels('warn-cantidad'));
        }
    }

    /**
     * Método para eliminar un segmento de la lista de los elegibles de un producto
     */
    removerSegmento() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (tkn) => {
                this.productoService.token = tkn;
                let sub = this.productoService.eliminarSegmento(this.producto.idProducto, this.idSegmento).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                        this.obtenerSegmentosDisponibles();
                        if (this.listaRemover.length > 0) {
                            let index = this.listaRemover.findIndex(element => element.idSegmento == this.idSegmento);
                            this.listaRemover.splice(index, 1);
                        }
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }

    /**
     * Redirecciona al componente padre de elegibles
     */
    goBack() {
        let ruta = "productos/detalleProducto/" + this.producto.idProducto + "/elegibles";
        let link = [ruta]
        this.router.navigate(link);
    }

    /**
     * Manejo de errores en las respuestas
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}