import {Component}  from '@angular/core';

@Component({
    moduleId: module.id,
    selector:'producto-component',
    templateUrl:'producto.component.html',
})

/**
 * Jaylin Centeno 
 * Componente padre de Producto
 */
export class ProductoComponent  {
    constructor(){}
}