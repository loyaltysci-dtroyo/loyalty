import { Component, OnInit, Renderer, ViewChild, ElementRef } from '@angular/core';
import { Producto } from '../../model/index';
import { ProductoService, KeycloakService, I18nService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { PERM_ESCR_PRODUCTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

declare var google: any;
@Component({
    moduleId: module.id,
    selector: 'producto-calendarizacion-component',
    templateUrl: 'producto-calendarizacion.component.html'

})

/**
 * Componente que permite la calendarización del producto
 */
export class ProductoCalendarizacionComponent implements OnInit {

    //Id del producto
    public id: string;
    //Lista para Dropdown//
    public efectividadItem: SelectItem[];
    public estado: string;
    //Permite verificar permisos (true/false)
    public escritura: boolean = true;
    public fecInicio: Date;
    public fecFin: Date;
    public dias: SelectItem[];
    public diasSelec: string[];
    public today: Date = new Date();
    public esRecurrente: boolean = false;
    public formEfectividad: FormGroup;
    //Permite saber si hay carga de datos
    public cargandoDatos: boolean = true;

    constructor(
        public producto: Producto,
        public productoService: ProductoService,
        public router: Router,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public route: ActivatedRoute,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_PRODUCTOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });

        //Validacion del formulario y datos por defecto 
        this.formEfectividad = new FormGroup({
            'indCalendarizacion': new FormControl('true', Validators.required),
            "fechaInicio": new FormControl(''),
            "fechaFin": new FormControl(''),
            "diaRecurrencia": new FormControl(''),
            "semanaRecurrencia": new FormControl(''),
        });
    }

    //Inicializacion
    ngOnInit() {

        this.mostrarDetalle();
        this.efectividadItem = [];
        //Listas predeterminadas en el sistema
        this.efectividadItem = this.i18nService.getLabels('tipo-efectividad-general')
        this.dias = [];
        this.dias = this.i18nService.getLabels('dias-efectividad-general');
    }

    /**
      * Copia los valores de cualquier objeto fuente en cualquier objeto destino, es usado por los metodos de obtener datos para 
      * setear los valores de la instancia compartida entre los elementos hijos del padre de componentes, esto permite mantener un unico puntero al 
      * elemento y asi los datos se actualizan inmediatamente (mas notorio en relaciones padre-hijo) y tambien para que al iterar entre ellos
      * no siempre sea necesario llamar al api para actualizar los datos
      */
    copyValuesOf(objDestino, objFuente) {
        for (var property in objFuente) {
                objDestino[property] = objFuente[property];
        }
    }

    /**
     * Obtiene el detalle del producto
     */
    mostrarDetalle() {

        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                this.productoService.obtenerProductoPorId(this.producto.idProducto).subscribe(
                    (data) => {
                        this.copyValuesOf(this.producto, data)
                        if (this.producto.efectividadIndCalendarizado == 'C') {
                            this.fecFin = new Date(this.producto.efectividadFechaFin);
                            this.fecInicio = new Date(this.producto.efectividadFechaInicio);
                            if (this.producto.efectividadIndSemana != null || this.producto.efectividadIndDias != null) {
                                if (this.producto.efectividadIndDias != null) {
                                    this.diasSelec = this.producto.efectividadIndDias.split('');
                                }
                                this.esRecurrente = true;
                            }
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }

    /*Método que se encarga de actualizar la efectividad del producto
    *Se utiliza solo cuando la efectividad es Calendarizada muse*/
    actualizarProducto() {
        if (this.producto.efectividadIndCalendarizado == 'C') {
            if (this.esRecurrente && this.diasSelec != null) {
                this.producto.efectividadIndDias = "";
                for (let i = 0; i < this.diasSelec.length; i++) {
                    this.producto.efectividadIndDias += this.diasSelec[i];
                }
            }
            if (!this.esRecurrente) {
                this.producto.efectividadIndSemana = null;
                this.producto.efectividadIndDias = null;
            }
        } else {
            this.producto.efectividadFechaFin = null;
            this.producto.efectividadFechaInicio = null;
            this.producto.efectividadIndSemana = null;
            this.producto.efectividadIndDias = null;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                this.productoService.editarProducto(this.producto).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                    },
                    (error) => {
                        this.manejaError(error);
                    });
            });
    }

    // Método para volver a la página anterior
    goBack() {
        let link = ['producto'];
        this.router.navigate(link);
    }

    // Metodo para manejar los errores que se provocan en las transacciones
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error)); 
    }
}