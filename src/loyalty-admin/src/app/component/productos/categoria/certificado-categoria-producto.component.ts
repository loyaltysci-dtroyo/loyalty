import { Component, OnInit } from '@angular/core';
import { CertificadoCategoria, CategoriaProducto, Configuracion } from '../../../model/index';
import { CategoriaProductoService, ConfiguracionService, KeycloakService, ImportacionService, I18nService } from '../../../service/index';
import { Response } from '@angular/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PERM_ESCR_CATEGORIAS_PRODUCTO, PERM_LECT_MIEMBROS, PERM_ESCR_CONFIGURACION } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'certificado-categoria-producto-component',
    templateUrl: 'certificado-categoria-producto.component.html',
    providers: [CertificadoCategoria, ConfiguracionService, ImportacionService]
})

/**
 * Jaylin Centeno
 * Componente para nsertar,ver, remover y asociar certificados  
 */
export class CertificadoCategoriaProducto implements OnInit {

    public id: string; //Id de la categoría pasado por la url
    public escritura: boolean; //Verifica permiso de escritura (true/false)
    public permisoAnular: boolean; //Verifica el permiso para anular certificados (true/false)
    public lecturaMiembro: boolean; // Verifica el permiso de lectura de miembro (true/false)
    public permisoConfig: boolean; //Verifica si tiene acceso a los datos de la configuracion
    public estado: string = "D"; //Estado del certificado D = disponible A = anulardo O = ocupado
    public configValida: boolean = true; //Validación que la configuración general permite la creación de certificados 

    //  --- Generar ---  //
    public nuevo: boolean; //Habilita el cuadro de dialogo para ingresar un certificado 
    public eliminar: boolean; //Habilita el cuadro de dialogo para remover 
    public generar: boolean; //Habilita el cuadro de dialogo para la generación de certificados
    public importar: boolean; //Habilita el cuadro de dialogo para importar un archivo
    public cantCertificados: number; //Cantidad de certificados a generar
    public generandoCertificados: boolean = false; //Indica la generación de certificados

    //  ---  Certificados --  //
    public listaCertificados: CertificadoCategoria[] = []; //Certificados de la categoria producto
    public listaVacia: boolean = true; //Permite verificar si la lista de certificados esta vacia
    public cantidad: number = 10; //Cantidad de filas a mostrar
    public totalRecords: number; //Total de de certificados en el sistema 
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila 
    public numCertificado: string; //Contiene el código que se quiere ingresar/remover/anular
    public estadoCertif: string; //Estado del certificado seleccionado

    //  ---- Imagen ---  //
    public archivo: any; //Representa el archivo que contiene los certificados sube el usuario con los 
    public listaBase64: string; //Contine el base64 resultado de la subida del archivo
    public fileName: string; //Nombre del archivo subido
    public archivoNuevo: boolean; //Muestra el nombre del archivo o un msj default

    public entroMetodo: boolean=false;

    constructor(
        public categoriaService: CategoriaProductoService,
        public categoriaProducto: CategoriaProducto,
        public categoriaProductoService: CategoriaProductoService,
        public router: Router,
        public route: ActivatedRoute,
        public configuracion: Configuracion,
        public configuracionService: ConfiguracionService,
        public importacionService: ImportacionService,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public keycloakService: KeycloakService,
        public i18nService: I18nService
    ) {
        //Representa el id del grupo pasado por la url 
        this.route.parent.params.forEach((params: Params) => { this.id = params['id'] });
        //Permite saber si el usuario tiene permisos
        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS_PRODUCTO).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        this.authGuard.canWrite(PERM_LECT_MIEMBROS).subscribe((permiso: boolean) => {
            this.lecturaMiembro = permiso;
        });
        this.authGuard.canWrite(PERM_ESCR_CONFIGURACION).subscribe((permiso: boolean) => {
            this.permisoConfig = permiso;
            this.obtenerConfiguracionGeneral();
        });
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
        this.obtenerCertificadosCategoria();
        this.obtenerConfiguracionGeneral();
    }

    //Método para mostrar el detalle de la categoría
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.obtenerCategoriaPorId(this.id).subscribe(
                    (data) => {
                        Object.keys(data).forEach(key => {
                            this.categoriaProducto[key] = data[key]
                        })
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Método para guardar el id del certificado seleccionado
    confirmar(certificado: CertificadoCategoria) {
        this.numCertificado = certificado.numCertificado;
        this.estadoCertif = certificado.estado;
        this.eliminar = true;
    }

    /**
     * Redirecciona al detalle del miembro
     * @param idMiembro 
     */
    gotoMiembro(idMiembro: string) {
        let link = ['miembros/detalleMiembro', idMiembro];
        this.router.navigate(link);
    }

    /**
     * Permite habilitar el cuadro de dialogo para generar certificados 
     * @param accion 
     */
    cargaCertificados(accion: string) {
        if (!this.generandoCertificados || !this.entroMetodo) {
            switch (accion) {
                case "nuevo":
                        this.nuevo = true;
                        this.generandoCertificados = true;
                    break;
                case "importar":
                        this.importar = true;
                        this.generandoCertificados = true;
                    break;
                case "generar":
                        this.generar = true;
                        this.generandoCertificados = true;
                    break;
                default:
                    this.generandoCertificados = false;
                    break;
            }
        } else {
            this.msgService.showMessage(this.i18nService.getLabels('reporte-espera-generar'));
        }

    }

    /**
     * Método que se encarga de traer la configuración del sistema
     */
    obtenerConfiguracionGeneral() {
        if (this.permisoConfig) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.configuracionService.token = token;
                    this.configuracionService.obtenerConfiguracion().subscribe(
                        (data) => {
                            this.configuracion = data;
                            if (this.configuracion.mes == undefined || this.configuracion.periodo == undefined || this.configuracion.ubicacionPrincipal == undefined) {
                                this.configValida = !this.configValida;
                            }
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
        }
    }

    /**
     * Método utilizado para la carga de los elementos del datatable
     * onLazyLoad, permite la carga de los elementos de forma peresoza 
     * @param event 
     */
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerCertificadosCategoria();
    }

    //Método para obtener los certificados asignados al premio
    obtenerCertificadosCategoria() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.categoriaProductoService.token = token;
                this.categoriaProductoService.obtenerListaCertificados(this.categoriaProducto.idCategoria).subscribe(
                    (response: Response) => {
                        //Se obtiene la lista de los certificados del premio
                        this.listaCertificados = response.json();
                        //Se recibe el total de los certificados disponibles
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaCertificados.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                    },
                    (error) => this.manejarError(error)
                )
            }
        );
    }

    //Método para insertar un certificado de premio
    agregarCertificado() {
        if (this.configValida && this.escritura) {
            this.entroMetodo = true;
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.categoriaProductoService.token = token;
                    this.categoriaProductoService.insertarCertificado(this.categoriaProducto.idCategoria, this.numCertificado).subscribe(
                        (data) => {
                            this.nuevo = true;
                            this.generandoCertificados = true;
                            this.numCertificado = "";
                            this.entroMetodo = false;
                            this.msgService.showMessage(this.i18nService.getLabels('general-insercion'));
                            this.obtenerCertificadosCategoria();
                        },
                        (error) => { this.manejarError(error); this.nuevo = false; this.generandoCertificados = false; this.entroMetodo = false;}
                    )
                });
        } else {
            this.msgService.showMessage({ detail: "Debe verificar la definición del período, mes y/o ubicación principal en la configuración del sistema para continuar.", severity: "warn", summary: "" });
        }
    }

    /**
     * Método para generar un badge de certificados de premio
     */
/*    generarCertificados() {
        if (this.cantCertificados <= 0) {
            this.msgService.showMessage({ detail: "La cantidad de certificados debe ser mayor a 0", severity: "warn", summary: "" });
        }
        if (this.configValida && this.escritura) {
            this.entroMetodo = true;
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.categoriaProductoService.token = token;
                    this.categoriaProductoService.generarCertificados(this.premio.idPremio, this.cantCertificados).subscribe(
                        (data) => {
                            this.entroMetodo = false;
                            this.generar = false;
                            this.generandoCertificados = false;
                            this.cantCertificados = 0;
                            this.msgService.showMessage({ detail: "Procesando los datos.", severity: "info", summary: "" });
                            this.obtenerCertificadoPremio();
                        },
                        (error) => { this.manejarError(error); this.generar = false; this.generandoCertificados = false; this.entroMetodo = false;}
                    )
                });
        } else {
            this.msgService.showMessage({ detail: "Debe verificar la definición del período, mes y/o ubicación principal en la configuración del sistema para continuar.", severity: "warn", summary: "" });
        }
    }
*/
    /**
     * Se encarga de importar un archivo de certificados
     */
    importarCertificados() {
        if (this.listaBase64 != "" && this.listaBase64 != undefined && this.escritura && this.configValida) {
            this.entroMetodo = true;
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.importacionService.token = token;
                    this.importacionService.importarCertificadosCategoriaProducto(this.categoriaProducto.idCategoria, this.listaBase64).subscribe(
                        (data) => {
                            this.entroMetodo = false;
                            this.importar = false;
                            this.generandoCertificados = false;
                            this.msgService.showMessage({ detail: "La importación de certificados esta siendo procesada.", severity: "success", summary: "" });
                            this.obtenerCertificadosCategoria();
                            this.listaBase64 = "";
                        },
                        (error) => { this.manejarError(error); this.importar = false; this.generandoCertificados = false; this.entroMetodo = false;}
                    );
                }
            );
        } else {
            this.msgService.showMessage({ detail: "Debe seleccionar un archivo para continuar.", severity: "warn", summary: "" });
        }
    }

    //Método para remover un certificado de un premio
    /*removerCertificadoPremio() {
        if (this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.categoriaProductoService.token = token;
                    this.categoriaProductoService.eliminarCertificado(this.premio.idPremio, this.codigoCertificado).subscribe(
                        (data) => {
                            this.eliminar = false;
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                            this.obtenerCertificadoPremio();
                        },
                        (error) => { this.manejarError(error); this.eliminar = false; }
                    );
                }
            );
        }
    }
    */
   
    //Método para subir un archivo csv con los codigos
    subirArchivo() {
        let archivoCsv: any;
        let nuevoCsv: any;
        let urlBase64: string[];
        archivoCsv = document.getElementById("archivo"); //Se obtiene el archivo subido por el usuario
        this.fileName = archivoCsv.files[0].name;
        let reader = new FileReader();
        reader.readAsDataURL(archivoCsv.files[0]);
        reader.addEventListener("load", (event) => {
            this.archivo = reader.result;
            urlBase64 = this.archivo.split(",");
            this.listaBase64 = urlBase64[1];
            this.archivoNuevo = true;
        }, false);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}