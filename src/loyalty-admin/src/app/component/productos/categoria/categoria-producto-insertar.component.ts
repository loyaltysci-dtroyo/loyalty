import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CategoriaProducto, Producto } from '../../../model/index';
import { CategoriaProductoService, KeycloakService, I18nService } from '../../../service/index';
import { ValidationService, ControlMessagesComponent } from '../../../utils/index';
import { PERM_ESCR_CATEGORIAS_PRODUCTO } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { AppConfig } from '../../../app.config';

@Component({
    moduleId: module.id,
    selector: 'categoria-producto-insertar-component',
    templateUrl: 'categoria-producto-insertar.component.html',
    providers: [CategoriaProducto, Producto, CategoriaProductoService, ControlMessagesComponent]
})

/**
 * Componente que permite insertar categorías de producto
 */
export class CategoriaProductoInsertarComponent implements OnInit {

    //Formulario
    public formCategoria: FormGroup;
    //Contendra el archivo subido por el usuario
    public imagen: any;
    //Guarda la dirección de la imagen
    public avatar: any;
    //Se guarda el array de la imagen
    public urlBase64: string[];
    //Permite verificar permisos (true/false)
    public escritura: boolean;
    //Validación del nombre interno
    public nombreExiste: boolean;
    //Indica si existe una carga de imagen
    public subiendoImagen: boolean = false;

    constructor(
        public categoria: CategoriaProducto,
        public categoriaService: CategoriaProductoService,
        public router: Router,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public fb: FormBuilder,
        public validationService: ValidationService,
        public msgService: MessagesService,
        public i18nService: I18nService

    ) { }

    ngOnInit() {
        this.buildForm();
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS_PRODUCTO).subscribe(
            (permiso: boolean) => {
                this.escritura = permiso;
            }
        );
        this.avatar = `${AppConfig.DEFAULT_IMG_URL}`;;
    }

    buildForm(): void {
        this.formCategoria = this.fb.group({
            'nombre': [this.categoria.nombre, Validators.compose([
                Validators.required,
                Validators.minLength(4),
                Validators.maxLength(50)
            ])],
            'nombreInterno': [this.categoria.nombreInterno, Validators.compose([
                Validators.required,
                Validators.minLength(4),
                Validators.maxLength(50)
            ])],
            'descripcion': [this.categoria.descripcion, Validators.compose([
                Validators.required,
                Validators.maxLength(100)
            ])],
            'loginId': [this.categoria.loginId, Validators.compose([
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(300)
            ])]
        });
    }

    //Método para insertar una categoría
    insertarCategoria() {
        if (this.nombreExiste) {
            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
        } else {
            this.categoria = this.formCategoria.value;
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token) => {
                    this.categoriaService.token = token;
                    if (this.urlBase64 != null) {
                        this.categoria.imagen = this.urlBase64[1];
                    }
                    this.subiendoImagen = true;
                    this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));

                    let sub = this.categoriaService.insertarCategoriaProducto(this.categoria).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                            let link = ['productos/detalleCategProducto', data];
                            this.router.navigate(link);
                        },
                        (error) => {
                            this.manejaError(error);
                            this.subiendoImagen = false;
                        },
                        () => { sub.unsubscribe(); this.subiendoImagen = false; }
                    );
                }
            );
        }

    }

    /**
     * Establece el nombre interno a partir del nombre de la categoría
     */
    establecerNombres() {
        if (this.formCategoria.get('nombre').value != null && this.formCategoria.get('nombre').value != "") {
            this.categoria.nombre = this.formCategoria.get('nombre').value;
            let temp = this.categoria.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.categoria.nombreInterno = temp;
            this.formCategoria.get('nombreInterno').setValue(this.categoria.nombreInterno);
            this.validarNombreInterno();
        }
    }

    /**
     * Método encargado de validar la existencia del nombre interno de una categoría
     */
    validarNombreInterno() {
        if (this.formCategoria.get('nombreInterno').value == "" || this.formCategoria.get('nombreInterno').value == null || !this.formCategoria.get('nombreInterno').valid) {
            this.nombreExiste = true;
            return;
        }
        this.categoria.nombreInterno = this.formCategoria.get('nombreInterno').value;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.verificarNombre(this.categoria.nombreInterno).subscribe(
                    (data) => {
                        this.nombreExiste = data;
                        if (this.nombreExiste) {
                            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
                        }
                    }
                );
            }
        );
    }

    /**
     * Obtiene la imagen subida por el usuario y la pasa a base64
     */
    subirImagen() {
        //se obtiene el archivo subido
        this.imagen = document.getElementById("avatar");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.avatar = reader.result;
            this.urlBase64 = this.avatar.split(",");

        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /**
     * Redirecciona a la lista de categorías de producto|
     */
    goBack() {
        let link = ['productos/listaCategoriasProducto'];
        this.router.navigate(link);
    }


    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}