import { Component, Renderer, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CategoriaProducto, SubCategoriaProducto } from '../../../model/index';
import { CategoriaProductoService, SubCategoriaProductoService, KeycloakService, I18nService } from '../../../service/index';
import { Http, Response, Headers } from '@angular/http';
import { ValidationService, ControlMessagesComponent } from '../../../utils/index';
import { PERM_ESCR_CATEGORIAS_PRODUCTO } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';
import { AppConfig } from '../../../app.config';

@Component({
    moduleId: module.id,
    selector: 'categoria-producto-subcategoria-component',
    templateUrl: 'categoria-producto-subcategoria.component.html',
    providers: [CategoriaProducto, SubCategoriaProducto, CategoriaProductoService, SubCategoriaProductoService, ControlMessagesComponent]
})

/**
 * Componente para ver, remover y asociar categorías y subcategorias
 */
export class CategoriaProductoSubCategoriaComponent implements OnInit {


    public id: string; //Id de la categoría pasado por la url
    public accion: string; //Guarda el tipo de acción (insertar / editar)
    public cargandoDatos: boolean = true; //Indicar si existe una carga de datos
    public editar: boolean = false; //Habilita el cuadro de dialogo para editar
    public escritura: boolean; //Permite verificar permisos (true/false)
    public formSubcategoria: FormGroup;
    public eliminar: boolean;

    // Subcategoria
    public listaSubcategorias: SubCategoriaProducto[] = []; //Subcategorias de una categoría
    public listaVacia: boolean = true; //Verifica si la lista de subcategorias viene vacía
    public cantidad: number = 10; //Cantidad de rows
    public totalRecords: number; //Total de de subcategorias en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public idSubcategoria: string; //Id del subcategoria seleccionado

    //Busqueda
    public busqueda: string = ""; //Palabras clave para la búsqueda
    public listaBusqueda: string[] = [];

    //Imagen
    public imagen: any;
    public imagenAgregar: any;
    public avatar2: any;
    public urlBase64: any;
    public cambioImagen: boolean;

    //Existencia del nombre interno
    public nombreInterno: string; //Permite guardar el nombre interno de la subcategoría
    public nombreExiste: boolean;

    //Indica si existe una carga de imagen
    public subiendoImagen: boolean = false;

    constructor(
        public subcategoriaE: SubCategoriaProducto,
        public subCategoriaService: SubCategoriaProductoService,
        public categoria: CategoriaProducto,
        public categoriaService: CategoriaProductoService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public renderer: Renderer,
        public fb: FormBuilder,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Representa el id del grupo pasado por la url 
        this.route.parent.params.forEach((params: Params) => { this.id = params['id'] });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS_PRODUCTO).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
        //Imagen por default
        this.imagenAgregar = this.avatar2 = `${AppConfig.DEFAULT_IMG_URL}`;;
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
        this.obtenerSubCategorias();
        this.buildForm();
    }

    buildForm(): void {
        this.formSubcategoria = this.fb.group({
            'nombre': ['', Validators.compose([
                Validators.required,
                Validators.minLength(4),
                Validators.maxLength(50)
            ])],
            'nombreInterno': ['', Validators.compose([
                Validators.required,
                Validators.minLength(4),
                Validators.maxLength(50)
            ])],
            'descripcion': ['', Validators.compose([
                Validators.required,
                Validators.maxLength(100)
            ])]
        });
    }

    //Método que muestra un cuadro de dialogo para pedir una confirmación
    //Recibe el id de la categoría a eliminar
    remover(idSubcategoria: string) {
        this.idSubcategoria = idSubcategoria;
        this.eliminar = true;
    }

    //Método que obtiene la subcategoria seleccionado
    subcategoriaSeleccionada(subcategoria: SubCategoriaProducto) {
        this.subcategoriaE = subcategoria;
        this.formSubcategoria.patchValue({ nombre: this.subcategoriaE.nombre, descripcion: this.subcategoriaE.descripcion, nombreInterno: this.subcategoriaE.nombreInterno });
        this.imagenAgregar = this.subcategoriaE.imagen;
        this.nombreInterno = this.subcategoriaE.nombreInterno;
        this.accion = "editar";
        this.editar = true;
    }

    accionSubcategoria() {
        if (this.accion == "editar") {
            this.actualizarSubcategoria();
        } else {
            this.agregarSubcategoria();
        }
    }

    cancelarAccion() {
        this.editar = false;
        this.formSubcategoria.reset();
        this.accion = "insertar"
    }

    //Método para mostrar el detalle de la categoría
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.obtenerCategoriaPorId(this.id).subscribe(
                    (data) => {
                        this.categoria = data;
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Método utilizado para la carga de los elementos del datatable
    //onLazyLoad, permite la carga de los elementos en segmentos
    //paginación
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerSubCategorias();
    }

    //Método para obtener las subcategorias que pertenecen a la categoría seleccionado
    obtenerSubCategorias() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
        }
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.subCategoriaService.token = token;
                this.subCategoriaService.busquedaSubCategorias(this.id, this.cantidad, this.filaDesplazamiento, this.busqueda.trim()).subscribe(
                    (response: Response) => {
                        //Se obtiene la lista de las subcategorias disponibles
                        this.listaSubcategorias = response.json();
                        //Se recibe el total de las categorías disponibles
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaSubcategorias.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejarError(error)
                )
            }
        );
    }

    //Método encargado de validar la existencia del nombre interno de una subcategoria
    validarNombreInterno() {
        if (this.formSubcategoria.get('nombreInterno').value == "" || this.formSubcategoria.get('nombreInterno').value == null) {
            return;
        } else {
            if (this.nombreInterno != this.formSubcategoria.get('nombreInterno').value) {
                this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                    (token: string) => {
                        this.subCategoriaService.token = token;
                        this.subCategoriaService.verificarNombre(this.id, this.formSubcategoria.get('nombreInterno').value).subscribe(
                            (data) => {
                                this.nombreExiste = data;
                            },
                            (error) =>
                                this.manejarError(error)
                        );
                    }
                );
            }
        }
    }

    establecerNombres() {
        if (this.formSubcategoria.get('nombre').value != "" || this.formSubcategoria.get('nombre').value != null) {
            this.subcategoriaE.nombre = this.formSubcategoria.get('nombre').value;
            let temp = this.subcategoriaE.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.subcategoriaE.nombreInterno = temp;
            this.formSubcategoria.get('nombreInterno').setValue(this.subcategoriaE.nombreInterno);
            this.validarNombreInterno();
        }
    }

    //Método para insertar una nueva subcategoría
    agregarSubcategoria() {
        this.subcategoriaE = this.formSubcategoria.value;
        if (this.urlBase64 != null) {
            this.subcategoriaE.imagen = this.urlBase64[1];
        }
        this.subcategoriaE.idCategoria = this.categoria;
        if (this.nombreExiste) {
            this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
        } else {
            this.subiendoImagen = true;
            this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token) => {
                    this.subCategoriaService.token = token;
                    let sub = this.subCategoriaService.insertarSubCategoria(this.id, this.subcategoriaE).subscribe(
                        (data) => {
                            this.editar = false;
                            this.obtenerSubCategorias();
                            this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
                            this.formSubcategoria.reset();
                            this.imagenAgregar = this.avatar2;
                            this.accion = "insertar";
                            this.urlBase64 = "";
                            this.cambioImagen = false;
                        },
                        (error) => {
                            this.manejarError(error);
                            this.subiendoImagen = false;
                            this.cambioImagen = false;
                        },
                        () => { sub.unsubscribe(); this.subiendoImagen = false; this.cambioImagen = false;}

                    );
                }
            );
        }
    }

    //Método para editar una subcategoría
    actualizarSubcategoria() {
        this.subcategoriaE.nombre = this.formSubcategoria.get('nombre').value;
        this.subcategoriaE.descripcion = this.formSubcategoria.get('descripcion').value;
        this.subcategoriaE.nombreInterno = this.formSubcategoria.get('nombreInterno').value;
        this.subcategoriaE.idCategoria = this.categoria;
        if (this.cambioImagen) {
            this.subcategoriaE.imagen = this.urlBase64[1];
        }
        console.log(this.subcategoriaE);
        if (this.nombreExiste) {
            this.msgService.showMessage(this.i18nService.getLabels("general-inclusion-simple"));
        } else {
            this.subiendoImagen = true;
            this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.subCategoriaService.token = token;
                    let sub = this.subCategoriaService.editarSubCategoria(this.id, this.subcategoriaE).subscribe(
                        (data) => {
                            this.obtenerSubCategorias();
                            this.editar = false;
                            this.accion = "insertar";
                            this.formSubcategoria.reset();
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                            this.imagenAgregar = this.avatar2;
                            this.urlBase64 = "";
                        },
                        (error) => {
                            this.manejarError(error);
                            this.subiendoImagen = false;
                        },
                        () => { sub.unsubscribe(); this.subiendoImagen = false; }
                    );
                }
            );

        }
    }

    //Método para remover la asignación de una categoría a un subcategoria
    removerCategoriaProducto() {
        if (this.escritura) {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token: string) => {
                    this.subCategoriaService.token = token;
                    this.subCategoriaService.eliminarSubCategoria(this.id, this.idSubcategoria).subscribe(
                        (data) => {
                            this.sacarSubCategorias();
                            this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                            this.obtenerSubCategorias();
                        },
                        (error) => this.manejarError(error)
                    );
                }
            );
        }
    }

    //Método que remueve de la tabla de subcategorias con categoría las subcategorias seleccionados
    // para removerles la categoría 
    sacarSubCategorias() {
        if (this.listaSubcategorias.length >= 0) {
            let index = this.listaSubcategorias.findIndex(element => element.idSubcategoria == this.idSubcategoria)
            this.listaSubcategorias.splice(index, 1);
        }
    }

    //Método para subir una imagen que representa el avatar del grupo
    subirImagen() {
        this.imagen = document.getElementById("imagenAgregar");
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            this.imagenAgregar = reader.result;
            this.urlBase64 = this.imagenAgregar.split(",");
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}