import { Component, Renderer, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CategoriaProducto, Producto } from '../../../model/index';
import { CategoriaProductoService, KeycloakService, I18nService } from '../../../service/index';
import { PERM_ESCR_CATEGORIAS_PRODUCTO } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ValidationService, ControlMessagesComponent } from '../../../utils/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'categoria-producto-detalle-component',
    templateUrl: 'categoria-producto-detalle.component.html',
    providers: [Producto, CategoriaProducto, CategoriaProductoService]
})

/**
 * Componente que permite ver/manipular la información de la categoría
 */
export class CategoriaProductoDetalleComponent implements OnInit {

    //Id de la categoría recibido en la url
    public id: string;
    //Formulario
    public formCategoria: FormGroup;
    //Contendra el archivo subido por el usuario
    public imagen: any;
    //Contendra la dirección
    public avatar: any;
    //Nombre interno actual de la categoría
    public nombreActual: string;
    //Permite verificar permisos (true/false)
    public escritura: boolean;
    //Validación del nombre interno
    public nombreExiste: boolean;
    //Manejar la edición del nombre interno
    public interno: boolean = true;
    //Indica si existe una carga de imagen
    public subiendoImagen: boolean = false;
    //Permite saber si se están cargando datos
    public cargandoDatos : boolean = true;

    constructor(
        public categoria: CategoriaProducto,
        public categoriaService: CategoriaProductoService,
        public route: ActivatedRoute,
        public router: Router,
        public authGuard: AuthGuard,
        public renderer: Renderer,
        public keycloakService: KeycloakService,
        public fb: FormBuilder,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Representa el id del grupo pasado por la url 
        this.route.parent.params.forEach((params: Params) => { this.id = params['id'] });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS_PRODUCTO).subscribe(
            (permiso: boolean) => {
                this.escritura = permiso;
            });
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.buildForm();
        this.mostrarDetalle();
    }

    //Construcción del form
    buildForm(): void {
        this.formCategoria = this.fb.group({
            'nombre': ['', Validators.compose([
                Validators.required,
                Validators.minLength(4),
                Validators.maxLength(50)
            ])],
            'nombreInterno': ['', Validators.compose([
                Validators.required,
                Validators.minLength(4),
                Validators.maxLength(50)
            ])],
            'descripcion': ['', Validators.compose([
                Validators.required,
                Validators.maxLength(100)
            ])],
            'loginId': ['', Validators.compose([
                Validators.required,
                Validators.maxLength(300)
            ])]
        });
    }

    //Método que llama al servicio para obtener el detalle de la categoría
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.categoriaService.token = token;
                this.categoriaService.obtenerCategoriaPorId(this.id).subscribe(
                    (data) => {
                        this.categoria = data;
                        this.avatar = this.categoria.imagen;
                        this.nombreActual = this.categoria.nombreInterno;
                        this.formCategoria.patchValue({ nombre: this.categoria.nombre, descripcion: this.categoria.descripcion, nombreInterno: this.categoria.nombreInterno, loginId: this.categoria.loginId });
                        this.cargandoDatos = false; 
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    /**
     * Obtiene la imagen subida por el usuario y la pasa a base64
     */
    subirImagen() {
        //se obtiene el archivo subido
        this.imagen = document.getElementById("avatar");
        let reader  = new FileReader();
        reader.addEventListener("load", (event) => {
            this.avatar = reader.result;
            let urlBase64 = this.avatar.split(",");
            this.categoria.imagen = urlBase64[1];
        }, false);
        reader.readAsDataURL(this.imagen.files[0]);
    }

    /**
     * Establece el nombre interno a partir del nombre de la categoría
     */
    establecerNombres() {
        if (this.formCategoria.get('nombre').value != "" || this.formCategoria.get('nombre').value != null) {
            this.categoria.nombre = this.formCategoria.get('nombre').value;
            let temp = this.categoria.nombre.trim();
            temp = temp.toLowerCase();
            temp = temp.replace(new RegExp(" ", 'g'), "_");
            temp = temp.replace(/[^_^a-zA-Z0-9 ]/g, "");
            this.categoria.nombreInterno = temp;
            this.formCategoria.get('nombreInterno').setValue(this.categoria.nombreInterno);
        }
    }

    /**
     * Método encargado de validar la existencia del nombre interno de una categoría
     */
    validarNombre() {
        if (this.formCategoria.get('nombreInterno').value == "" || this.formCategoria.get('nombreInterno').value == null) {
            return;
        } else {
            if (this.nombreActual != this.formCategoria.get('nombreInterno').value) {
                this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                    (token) => {
                        this.categoriaService.token = token;
                        this.categoriaService.verificarNombre(this.categoria.nombreInterno).subscribe(
                            (data) => {
                                this.nombreExiste = data;
                            },
                            (error) => {
                                this.manejarError(error);
                            }
                        );
                    }
                );
            }
        }
    }

    /**
     * Método para actualizar la categoría
     */
    actualizarCategoria() {
        if (this.nombreExiste) {
            this.msgService.showMessage(this.i18nService.getLabels('error-nombre-interno'));
        } else {
            this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
                (token) => {
                    this.categoriaService.token = token;
                    this.categoria.nombre = this.formCategoria.get('nombre').value;
                    this.categoria.descripcion = this.formCategoria.get('descripcion').value;
                    this.categoria.nombreInterno = this.formCategoria.get('nombreInterno').value;
                    this.categoria.loginId = this.formCategoria.get('loginId').value;
                    this.subiendoImagen = true;
                    this.msgService.showMessage(this.i18nService.getLabels('general-actualizando-datos'));
                    let sub = this.categoriaService.editarCategoriaProducto(this.categoria).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nService.getLabels('general-actualizacion'));
                            this.mostrarDetalle();
                        },
                        (error) => {
                            this.manejarError(error);
                            this.subiendoImagen = false;
                        },
                        () => { sub.unsubscribe(); this.subiendoImagen = false; }
                    );
                }
            );
        }
    }

    /**
     * Redirecciona a la lista de categorías
     */
    goBack() {
        let link = ['productos/listaCategoriasProducto'];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}