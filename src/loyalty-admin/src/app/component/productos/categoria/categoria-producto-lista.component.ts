import { Component, Renderer, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CategoriaProducto, Producto } from '../../../model/index';
import { CategoriaProductoService, KeycloakService, I18nService } from '../../../service/index';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { PERM_ESCR_CATEGORIAS_PRODUCTO } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'categoria-producto-lista-component',
    templateUrl: 'categoria-producto-lista.component.html',
    providers: [CategoriaProducto, Producto, CategoriaProductoService]
})

/**
 * Componente que muestra la lista de categorías de un producto
 */
export class CategoriaProductoListaComponent implements OnInit {

    public escritura: boolean; //Permite verificar permisos (true/false)
    public cargandoDatos: boolean = true;
    public eliminar: boolean;
    
    public idCategoria: string; //Id de la categoría seleccionada
    public listaCategoria: CategoriaProducto[]; //Lista de categorías
    public listaVacia: boolean = true; //Para verificar si la lista de obtenida esta vacía
    public cantidad: number = 10; //Cantidad de rows
    public totalRecords: number; //Total de categorías de prodcutos en el sistema
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila

    public busqueda: string = ""; //Palabras clave para la búsqueda
    public listaBusqueda: string[] = [];
    
    constructor(
        public categoria: CategoriaProducto,
        public categoriaService: CategoriaProductoService,
        public router: Router,
        public renderer: Renderer,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura sobre este elemento
        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS_PRODUCTO).subscribe(
            (permiso: boolean) => { this.escritura = permiso; });
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.obtenerListaCategorias();
    }

    //Método que muestra un cuadro de dialogo para pedir una confirmación
    //Recibe el id de la categoría a eliminar
    dialogoConfirmacion(idCategoria: string) {
        this.idCategoria = idCategoria;
        this.eliminar = true;
    }

    //Método utilizado para la carga de los elementos del datatable
    //onLazyLoad, permite la carga de los elementos en segmentos
    //paginación
    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerListaCategorias();
    }

    //Método que obtiene las categorías existentes en la aplicación
    obtenerListaCategorias() {
        this.busqueda = "";
        if (this.listaBusqueda.length > 0) {
            this.listaBusqueda.forEach(element => {
                this.busqueda += element + " ";
            });
            this.busqueda = this.busqueda.trim();
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.categoriaService.token = token;
                this.categoriaService.obtenerListaCategorias(this.cantidad, this.filaDesplazamiento, this.busqueda).subscribe(
                    (response: Response) => {
                        this.listaCategoria = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            //Se recibe el total de las categorías disponibles
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        //Se pregunta si la lista esta vacía
                        if (this.listaCategoria.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => {this.manejaError(error); this.cargandoDatos = false;}
                );
            }
        );
    }

    //Método para navegar hasta el detalle de la categoría
    //Recibe el id de la categoría seleccionada
    detalleCategoria(idCategoria: string) {
        let link = ['productos/detalleCategProducto', idCategoria];
        this.router.navigate(link);
    }

    //Método para navegar a la pantalla de inserción de categoría
    insertarCategoria() {
        let link = ['productos/insertarCategProducto'];
        this.router.navigate(link);
    }

    //Método para remover una categoría
    removerCategoria() {
        this.eliminar = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.categoriaService.token = token;
                this.categoriaService.eliminarCategoriaProducto(this.idCategoria).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                        this.filaDesplazamiento = 0;
                        this.obtenerListaCategorias();
                    },
                    (error) => {
                        this.manejaError(error)
                    }
                );
            }
        );
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}