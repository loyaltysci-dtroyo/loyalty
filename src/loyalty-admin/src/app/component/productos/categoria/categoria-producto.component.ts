import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Rutas from '../../../utils/rutas';

import { CategoriaProducto } from '../../../model/index';
import { CategoriaProductoService, KeycloakService, I18nService } from '../../../service/index';

import { PERM_ESCR_CATEGORIAS_PRODUCTO } from '../../common/auth.constants';
import { AuthGuard } from '../../common/index';
import { ErrorHandlerService, MessagesService } from '../../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'categoria-producto-component',
    templateUrl: 'categoria-producto.component.html',
    providers: [CategoriaProducto, CategoriaProductoService]
})

/**
 * Componente que permite mostrar la pantalla inicial de las categorias de producto
 */
export class CategoriaProductoComponent implements OnInit {

    //Id de la categoría de producto
    public id: string;
    //Contendra la ruta para ver el detalle de la categoría del producto
    public informacion = Rutas.RT_PRODUCTOS_CATEGORIA_INFORMACION;
    //Contendra la ruta para ver las subcategorías que contiene esa categoría
    public subcategorias = Rutas.RT_PRODUCTOS_CATEGORIA_SUBCATEGORIA;
    //Contendra la ruta para ver los certificados que contiene la categoría
    public certificadosCategoriaProducto = Rutas.RT_PRODUCTOS_CATEGORIA_CERTIFICADOS_CATEGORIA_PRODUCTO
    //Permite verificar permisos (true/false)
    public escritura: boolean;
    public eliminar: boolean;

    constructor(
        public categoria: CategoriaProducto,
        public categoriaService: CategoriaProductoService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public msgService: MessagesService,
        public i18nService: I18nService
    ) {
        //Representa el id de la categoría pasado por la url 
        this.route.params.forEach((params) => { this.id = params['id'] });

        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS_PRODUCTO).subscribe(
            (permiso: boolean) => {
                this.escritura = permiso;
            }
        );
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.mostrarDetalle();
    }

    //Método que se encarga traer el detalle de la categoría
    mostrarDetalle() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.categoriaService.token = token;
                this.categoriaService.obtenerCategoriaPorId(this.id).subscribe(
                    (data) => {
                        console.log(data)
                        //this.categoria = data;
                        //this.categoria.idCategoria = data.idCategoria;
                        Object.keys(data).forEach(key => {
                            this.categoria[key] = data[key]
                        })
                    },
                    (error) => this.manejaError(error)
                );
            }
        );
    }
    //Método para remover una categoría de producto
    removerCategoriaProducto() {
        this.eliminar = false;
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token:string) => {
                this.categoriaService.token = token;
                this.categoriaService.eliminarCategoriaProducto(this.id).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-eliminacion-simple'));
                        this.goBack();
                    },
                    (error) =>
                        this.manejaError(error)
                );
            }
        );
    }

    /**
     * Redirecciona a la lista de categorías de producto
     */
    goBack() {
        let link = ['productos/listaCategoriasProducto'];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }
}
