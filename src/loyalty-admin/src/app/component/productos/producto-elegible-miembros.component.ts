import { Component, OnInit, Renderer } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { Response } from '@angular/http';
import { Miembro, Producto } from '../../model/index';
import { MiembroService, ProductoService } from '../../service/index';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { KeycloakService, I18nService } from '../../service/index';
import { PERM_ESCR_PRODUCTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'producto-elegible-miembro-component',
    templateUrl: 'producto-elegible-miembros.component.html',
    providers: [MiembroService, Miembro]
})

/**
 * Jaylin Centeno
 * Componente que muestra el detalle de los miembros elegibles del producto
 */
export class ProductoMiembroElegiblesComponent implements OnInit {

    public id: string; //Contiene el id del producto
    public listaMiembrosDisponibles: Miembro[]; //Lista con los miembros disponibles en el sistema
    public listaVacia: boolean = true; //Valida que la lista de disponibles no esta vacía
    public indAccion: string = 'D'; //Permite determinar la acción, ya sea incluir o excluir
    public elegible: string = "I"; //Indica cual es el elemento elegible a mostrar
    public busqueda: string; //Búsqueda de miembros, guarda las palabras para realizar las búsquedas
    public listaSeleccion: Miembro[] = []; //Lista que contendra la seleccion de los miembros, para asociar o remover
    public listaRemover: Miembro[] = [];
    public lista: string[] = []; //Lista que contendra el id de los miembros, para asociar o remover del grupo
    public itemsAccion: SelectItem[] = []; //Items del dropdown, muestra las acciones de incluir o excluir 
    public itemsElegibles: SelectItem[] = []; //Items del dropdown, muestra los elementos de elegibles
    public idMiembro: string; //Contendra el id del miembro a eliminar
    public escritura: boolean; //Permite verificar permisos (true/false)
    public totalRecords: number; //Total de miembros en el sistema
    public cantidad: number = 0; //Cantidad de filas
    public filaDesplazamiento: number = 0; //Desplazamiento de la primera fila
    public cargandoDatos: boolean = true; //Permite saber si hay carga de datos
    public valor: string; //Para guardar el valor del indicador, si hay carga o no
    public eliminar: boolean;

    constructor(
        public producto: Producto,
        public miembroService: MiembroService,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public productoService: ProductoService,
        public router: Router,
        public msgService: MessagesService,
        public route: ActivatedRoute,
        public i18nservice: I18nService
    ) {
        //Se obtienen las listas predeterminadas en el sistema 
        this.itemsElegibles = this.i18nservice.getLabels("accion-elegibles-general");
        this.itemsAccion = this.i18nservice.getLabels("tipos-elegibles-general");
        //Permite comprobar los permisos de escritura del usuario
        this.authGuard.canWrite(PERM_ESCR_PRODUCTOS).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    /**
     * Método que se inicia al entrar por primera vez al componente
     */
    ngOnInit() {
        this.obtenerMiembrosDisponibles();
    }

    /**
     * Método que muestra un cuadro de dialogo para pedir una confirmación
     * @param idMiembro 
     */
    dialogoConfirmar(idMiembro: string) {
        this.idMiembro = idMiembro;
        this.eliminar = true;
    }

    loadData(event) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerMiembrosDisponibles();
    }

    /**
     * Método para obtener la lista de los miembros según la acción
     * Si la acción es incluir o excluir se obtendran los miembros que ya estan en los elegibles
     * Si la acción es disponibles se obtendran los miembros que no están en los elegibles
     */
    obtenerMiembrosDisponibles() {
        if (this.valor != this.indAccion) {
            this.cargandoDatos = true;
            this.valor = this.indAccion;
        }
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                this.productoService.obtenerMiembrosElegiblesProducto(this.producto.idProducto, this.indAccion, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaMiembrosDisponibles = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaMiembrosDisponibles.length > 0) {
                            this.listaVacia = false;
                        } else {
                            this.listaVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejaError(error)
                )
            }
        );
    }

    /**
     * Método para agregar un miembro a los elegibles del producto, 
     * Ya sea para incluirlo o excluirlo de los elegibles
     * @param idMiembro 
     */
    agregarMiembro(idMiembro: string) {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (token: string) => {
                this.productoService.token = token;
                let sub = this.productoService.agregarMiembro(this.producto.idProducto, idMiembro, this.elegible).subscribe(
                    (data) => {
                        if (this.elegible == "E") {
                            this.msgService.showMessage(this.i18nservice.getLabels("general-exclusion-simple"));
                        } else {
                            this.msgService.showMessage(this.i18nservice.getLabels("general-inclusion-simple"));
                        }
                        this.obtenerMiembrosDisponibles();
                        if (this.listaSeleccion.length > 0) {
                            let index = this.listaSeleccion.findIndex(element => element.idMiembro == idMiembro);
                            this.listaSeleccion.splice(index, 1);
                        }
                    },
                    (error) => { this.manejaError(error); },
                );
            });
    }

    /**
     * Método para agregar la lista de miembros a los elegibles de los productos 
     * Ya sea para incluir o excluir la lista de los elegibles
     */
    agregarListaMiembros() {
        if (this.listaSeleccion.length > 1) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.productoService.token = token;
                    this.listaSeleccion.forEach(miembro => {
                        this.lista = [...this.lista, miembro.idMiembro]
                    })
                    this.productoService.agregarListaMiembros(this.producto.idProducto, this.lista, this.elegible).subscribe(
                        (data) => {
                            if (this.elegible == "E") {
                                this.msgService.showMessage(this.i18nservice.getLabels("general-exclusion-multiple"));
                            } else {
                                this.msgService.showMessage(this.i18nservice.getLabels("general-inclusion-multiple"));
                            }
                            this.listaSeleccion = [];
                            this.lista = [];
                            this.obtenerMiembrosDisponibles();
                        },
                        (error) => { this.manejaError(error); },
                    );
                }
            );
        } else {
            this.msgService.showMessage(this.i18nservice.getLabels('warn-cantidad'));
        }

    }

    /**
     * Permite remover una lista de miembros de los incluidos/excluidos
     */
    removerListaMiembros() {
        if (this.listaRemover.length >= 1) {
            this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
                (token: string) => {
                    this.productoService.token = token;
                    this.listaRemover.forEach(miembro => {
                        this.lista = [...this.lista, miembro.idMiembro]
                    })
                    this.productoService.eliminarListaMiembros(this.producto.idProducto, this.lista).subscribe(
                        (data) => {
                            this.msgService.showMessage(this.i18nservice.getLabels('general-eliminacion-multiple'));
                            this.obtenerMiembrosDisponibles();
                            this.listaRemover = [];
                            this.lista = [];
                        },
                        (error) => { this.manejaError(error); },
                    );
                }
            );
        } else {
            this.msgService.showMessage(this.i18nservice.getLabels('warn-cantidad'));
        }
    }

    /**
     * Método para eliminar un miembro de la lista de los elegibles de un producto
     */
    removerMiembro() {
        this.keycloakService.getToken().catch((error) => this.manejaError(error)).then(
            (tkn) => {
                this.productoService.token = tkn;
                let sub = this.productoService.eliminarMiembro(this.producto.idProducto, this.idMiembro).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nservice.getLabels('general-eliminacion-simple'));
                        this.obtenerMiembrosDisponibles();
                        if (this.listaRemover.length > 0) {
                            let index = this.listaRemover.findIndex(element => element.idMiembro == this.idMiembro);
                            this.listaRemover.splice(index, 1);
                        }
                    },
                    (error) => { this.manejaError(error); },
                    () => { sub.unsubscribe() }
                );
            });
    }

    /**
     * Redirección a componente padre de los elegibles
     */
    goBack() {
        let ruta = "productos/detalleProducto/" + this.producto.idProducto + "/elegibles";
        let link = [ruta]
        this.router.navigate(link);
    }

    /**
     * Manejo de error en las respuestas 
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }


}