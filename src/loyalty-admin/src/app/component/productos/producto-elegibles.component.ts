import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { Producto } from '../../model/index';
import { ProductoService, KeycloakService,I18nService } from '../../service/index';
import { PERM_ESCR_PRODUCTOS } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'producto-elegibles-component',
    templateUrl: 'producto-elegibles.component.html',
})

/**
 * Jaylin Centeno
 * Componente que permite ir a los distintos componentes para los elegibles del producto
 */
export class ProductoElegiblesComponent {

    //Contiene el id del producto
    public id: string;
    //Indica cual es el elemento elegible a mostrar
    public elegible: string;
    //Items del dropdown, muestra los elementos de elegibles
    public itemsElegibles: SelectItem[];

    constructor(
        public producto: Producto,
        public authGuard: AuthGuard,
        public keycloakService: KeycloakService,
        public productoService: ProductoService,
        public router: Router,
        public msgService: MessagesService,
        public route: ActivatedRoute,
        public i18nService: I18nService
    ) {
        this.itemsElegibles = [];
        this.itemsElegibles = this.i18nService.getLabels("elegibles-general");
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
    }

    /**
     * Método que muestra la definición de elegibles según el elementos seleccionado
     */
    definicionElegibles() {
        let ruta = "productos/detalleProducto/" + this.producto.idProducto ;
        if (this.elegible == 'M') {
            ruta += "/miembros";
        }
        else if (this.elegible == 'S') {
            ruta += "/segmentos";
        }
        let link = [ruta];
        this.router.navigate(link);
    }

    /**
     * Maneja el error en las transacciones
     * @param error 
     */
    manejaError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }


}