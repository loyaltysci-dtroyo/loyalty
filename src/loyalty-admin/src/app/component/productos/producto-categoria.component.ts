import { Component, Renderer, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CategoriaProducto, Producto, SubCategoriaProducto, ProductoCategSubCategoria } from '../../model/index';
import { CategoriaProductoService, ProductoService, SubCategoriaProductoService, KeycloakService, I18nService } from '../../service/index';
import {
    Message, ConfirmDialogModule, ConfirmationService, TooltipModule, InputTextareaModule, PickListModule,
    TabViewModule, MenuItem, DataTableModule, SharedModule
} from 'primeng/primeng';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { PERM_ESCR_CATEGORIAS_PRODUCTO } from '../common/auth.constants';
import { AuthGuard } from '../common/index';
import { ErrorHandlerService, MessagesService } from '../../utils/index';

@Component({
    moduleId: module.id,
    selector: 'producto-categoria-component',
    templateUrl: 'producto-categoria.component.html',
    providers: [CategoriaProducto, SubCategoriaProducto, SubCategoriaProductoService, CategoriaProducto, CategoriaProductoService, ConfirmationService]
})

/**
 * Jaylin Centeno
 * Componente para ver, remover y asociar productos a categorías
 */
export class ProductoCategoriaComponent implements OnInit {

    public escritura: boolean; //Permite verificar permisos (true/false)
    public mostrarSubCat: boolean = false;//Desplegar el modal con las subcategorías disponibles
    public asignar: boolean = false; //Permite mostrar las categorías disponibles
    public cargandoDatos: boolean = true; //Permite saber si hay carga de datos
    public subActiva: boolean = false; //Permite saber si se activo la busqueda de subcategorías
    public eliminar:boolean;
    
    public idCategoria: string; //Id de la categoría seleccionada para asignar/remover
    public idSubcategoria: string; //Id de la subcategoría seleccionada para asignar/remover
    public lista: any[] = []; //Lista con las categorías y subcategorias de cada una
    public totalRecords: number; //Se usa para todas las lista, dice el total    
    
    // -- Lista con las subcategorias del producto --  //
    public listaSubCatProducto: ProductoCategSubCategoria[] = []; //Lista de las subcategorías que tiene el producto
    public listaSubCatProdVacia: boolean = true; //Para verificar que la lista de subcategorias de producto no esta vacía
    public cantidad: number; //Cantidad de filas
    public filaDesplazamiento: number; //Desplazamiento de la primera fila

    //  -- Categorías no ligadas al producto, van a aparecer hasta que todas sus subcategorias esten asignadas  --  //
    public listaTotalCategorias: CategoriaProducto[] = []; //Lista con las categorías existentes
    public listaTotalVacia: boolean = true; //Para verificar si existen categorías en la aplicacion
    public cantidadSubDisp: number = 10; //Cantidad de filas
    public filaDesplazamientoSubDis: number = 0; //Desplazamiento de la primera fila

    //  -- Subcategorías disponibles por categoria seleccionada --  //
    public listaSubCategorias: SubCategoriaProducto[] = []; //Lista de las subcategorías del producto
    public listaVacia: boolean = true; //Para verificar existen categorías asignadas a productos

    constructor(
        public producto: Producto,
        public productoService: ProductoService,
        public categoria: CategoriaProducto,
        public categoriaService: CategoriaProductoService,
        public subCategoria: SubCategoriaProducto,
        public subCategoriaService: SubCategoriaProductoService,
        public router: Router,
        public route: ActivatedRoute,
        public authGuard: AuthGuard,
        public msgService: MessagesService,
        public renderer: Renderer,
        public keycloakService: KeycloakService,
        public confirmationService: ConfirmationService,
        public i18nService: I18nService
    ) {
        //Permite saber si el usuario tiene permisos de escritura
        this.authGuard.canWrite(PERM_ESCR_CATEGORIAS_PRODUCTO).subscribe((permiso: boolean) => {
            this.escritura = permiso;
        });
    }

    //Método que se inicia al entrar por primera vez al componente
    ngOnInit() {
        this.filaDesplazamiento = 0;
        this.cantidad = 10;
        this.obtenerSubCategoriasDeProducto();
    }


    //Método utilizado para la carga de los elementos del datatable de subcategorias del producto
    //onLazyLoad, permite la carga de los elementos en segmentos
    //paginación
    loadData(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerSubCategoriasDeProducto();
    }

    //Método utilizado para la carga de los elementos del datatable de las subcategorias disponbiles
    //onLazyLoad, permite la carga de los elementos en segmentos
    //paginación
    loadDataSubcategoria(event: any) {
        this.filaDesplazamientoSubDis = event.first;
        this.cantidadSubDisp = event.rows;
        this.obtenerSubCategoriasDisponibles();
    }

    //Método utilizado para la carga de los elementos del datatable de categorias no asignadas
    //onLazyLoad, permite la carga de los elementos en segmentos
    //paginación
    loadDataCategoria(event: any) {
        this.filaDesplazamiento = event.first;
        this.cantidad = event.rows;
        this.obtenerNoCategorias();
    }

    //Método utilizado para mostrar las subcategorías disponibles en un cuadro de dialogo
    categoriaSeleccionada(idCategoria: string) {
        this.idCategoria = idCategoria;
        this.subActiva = true;
        this.cargandoDatos = true;
        this.obtenerSubCategoriasDisponibles();
        this.mostrarSubCat = true;
    }

    //Según la acción se llama al método correspondiente
    subcategoriaSeleccionada(subcategoria: SubCategoriaProducto, accion: string) {
        this.subCategoria = subcategoria;
        this.subActiva = false;
        if(accion == "asignar"){
            this.asignarSubCategoriaProducto();
        }else{
            this.removerSubCategoriaProducto();
        }
        
    }

    asignarCategoria() {
        this.asignar = true;
        this.cargandoDatos = true;
        this.obtenerNoCategorias();
    }

    //Método para obtener las subcategorías asignadas al producto
    obtenerSubCategoriasDeProducto() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.subCategoriaService.token = token;
                this.subCategoriaService.obtenerSubCategoriaProducto(this.producto.idProducto, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaSubCatProducto = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaSubCatProducto.length > 0) {
                            this.listaSubCatProdVacia = false;
                        }
                        else {
                            this.listaSubCatProdVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejarError(error)
                )
            }
        );
    }

    //Método para obtener las subcategorías disponibles NO asignadas al producto
    obtenerSubCategoriasDisponibles(){
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.subCategoriaService.token = token;
                this.subCategoriaService.obtenerSubCategoriasDisponibles(this.producto.idProducto, this.idCategoria, this.cantidadSubDisp, this.filaDesplazamientoSubDis).subscribe(
                    (response: Response) => {
                        //Se obtiene la lista de las subcategorías disponibles para el producto
                        this.listaSubCategorias = response.json();
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaSubCategorias.length > 0) {
                            this.listaVacia = false;
                        }
                        else {
                            this.listaVacia = true;
                        }
                    },
                    (error) => this.manejarError(error)
                )
            }
        );
        
    }

    //Método para obtener las categorías que no estan ligadas al producto
    obtenerNoCategorias() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.subCategoriaService.token = token;
                this.subCategoriaService.obtenerNoSubCategoriaProducto(this.producto.idProducto, this.cantidad, this.filaDesplazamiento).subscribe(
                    (response: Response) => {
                        this.listaTotalCategorias = []
                        this.lista = response.json();
                        for (let x in this.lista) {
                            this.listaTotalCategorias=[...this.listaTotalCategorias,this.lista[x][0]];
                        }
                        if (response.headers.get("Content-Range") != null) {
                            this.totalRecords = + response.headers.get("Content-Range").split("/")[1];
                        }
                        if (this.listaTotalCategorias.length > 0) {
                            this.listaTotalVacia = false;
                        }
                        else {
                            this.listaTotalVacia = true;
                        }
                        this.cargandoDatos = false;
                    },
                    (error) => this.manejarError(error)
                )
            }
        );
    }

    //Método que permite asignar la categoría y subcategoria a un producto
    asignarSubCategoriaProducto() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.subCategoriaService.token = token;
                this.subCategoriaService.asignarSubCategoriaProducto(this.idCategoria, this.subCategoria.idSubcategoria, this.producto.idProducto).subscribe(
                    (data) => {
                        this.mostrarSubCat = false;
                        this.msgService.showMessage(this.i18nService.getLabels('general-inclusion-simple'));
                        this.moverSubCategoriaNoAsignada()
                    },
                    (error) => this.manejarError(error)
                );
            });
    }

    //Método para remover la asignación de una categoría y subcategoría a un producto
    removerSubCategoriaProducto() {
        this.keycloakService.getToken().catch((error) => this.manejarError(error)).then(
            (token: string) => {
                this.subCategoriaService.token = token;
                this.subCategoriaService.removerAsignacionSubCategoria(this.idCategoria, this.subCategoria.idSubcategoria, this.producto.idProducto).subscribe(
                    (data) => {
                        this.msgService.showMessage(this.i18nService.getLabels('general-exclusion-simple'));
                        this.obtenerSubCategoriasDeProducto();
                    },
                    (error) => this.manejarError(error)
                );
            }
        );
    }

    //Método que remueve de la tabla de categorías no asignasdas la categoría seleccionada
    sacarCategoriaNoAsignada() {
        let listaTemporal = this.listaTotalCategorias;
        if (this.listaTotalCategorias.length > 0) {
            let index = this.listaTotalCategorias.findIndex(element => element.idCategoria == this.idCategoria);
            this.listaTotalCategorias.splice(index, 1);
            this.obtenerSubCategoriasDeProducto();
        }
    }

    //Método que remueve de la tabla las subcategorías no asignadas de la lista
    moverSubCategoriaNoAsignada() {
        let listaTemporal = this.listaSubCategorias;
        if (this.listaSubCategorias.length > 0) {
            let index = this.listaSubCategorias.findIndex(element => element.idSubcategoria == this.subCategoria.idSubcategoria);
            this.listaSubCategorias.splice(index, 1);
            this.obtenerSubCategoriasDeProducto();
        }
        if (this.listaSubCategorias.length == 0) {
            this.mostrarSubCat = false;
            this.sacarCategoriaNoAsignada();
        }
    }

    //Método que remueve las subcategorias de la tabla de subcategorias asignadas al producto
    moverSubCategoriaAsignada(){
        let listaTemporal = this.listaSubCatProducto;
        if (this.listaSubCatProducto.length > 0) {
            let index = this.listaSubCatProducto.findIndex(element => element.subcategoriaProducto.idSubcategoria == this.subCategoria.idSubcategoria);
            this.listaSubCatProducto.splice(index, 1);
            this.obtenerSubCategoriasDeProducto();
        }
    }

    /**
     * Redirecciona al detalle de la categoría 
     */ 
    goBack() {
        let link = ['productos/detalleCategoria', this.producto.idProducto];
        this.router.navigate(link);
    }

    /* Método para manejar los errores que se provocan en las transacciones*/
    manejarError(error) {
        this.msgService.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
    }

}