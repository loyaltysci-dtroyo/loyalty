import './polyfills';
import { AppModule } from './app.module';
import { KeycloakService } from './service/index';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';

/**
 * Flecha Roja Technologies
 * Actualización: 26-octubre-2016
 * Autor: Fernando Aguilar 
 * Configuracion del archivo boot para el arranque de el proyecto
 */
// se condiciona el inicio de la aplicacion si el servicio de keycloak retorna un llamado de success
KeycloakService.init().then(
    (status) => {
//         if (status = "ok") {
            enableProdMode();
            doBootstrap()
        // }
    },
    () => {
        // window.location.reload();
    }
);
// //workaround horible para el bug de deteccion del bootstrap de angular cLI ***remover cuando arreglen el bug*****
function doBootstrap() {
  platformBrowserDynamic().bootstrapModule(AppModule);
}


