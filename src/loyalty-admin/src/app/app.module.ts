import { SeleccionPremiosComponent } from './component/common/seleccion-premios.component';
import { SeleccionCategoriaProductosComponent } from './component/common/seleccion-categoriaProductos.component';
import { DashboardModule } from './module/dashboard.module';
import { ErrorHandlerService } from './utils';
import { I18nService } from './service/i18n.service';
import { WorkflowModule } from './module/workflow.module';
import { PromocionesModule } from './module/promociones.module';
import { ProductosModule } from './module/productos.module';
import { PremiosModule } from './module/premios.module';
import { OtrosModule } from './module/otros.module';
import { NotificacionesModule } from './module/notificaciones.module';
import { MiembrosModule } from './module/miembros.module';
import { MetricasModule } from './module/metricas.module';
import { InventariosModule } from './module/inventarios.module';
import { InsigniasModule } from './module/insignias.module';
import { GruposModule } from './module/grupos.module';
import { ConfigModule } from './module/config.module';
import { UsuariosModule } from './module/usuarios.module';
import { SegmentosModule } from './module/segmentos.module';
import { UbicacionesModule } from './module/ubicaciones.module';
import { LeaderboardsModule } from './module/leaderboards.module';
import { GeneralModule } from './module/general.module';
import { MisionesModule } from './module/misiones.module';
import { AppTopBarComponent } from './component/app.topbar.component';
import { AppMenuComponent, AppSubMenuComponent } from './component/app.menu.component';
import { InlineProfileComponent } from './component/app.profile.component';
import { AppFooter } from './component/app.footer.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthGuard } from './component/common/auth.guard';
import { AppRoutingModule } from './app.routing.module';
import { KeycloakService } from './service/index';
import { ValidationService } from './utils/index';
import { AppComponent } from './app.component';
import { ErrorHandler } from '@angular/core';
import { BreadcrumbComponent } from './utils/breadcrumb-component';
import { ReportesModule } from './module/reportes.module';
import { TrabajoInternoComponent } from './component/configuracion/trabajos-internos.component';
import { BitacoraModule } from './module/bitacora.module';
import { NoticiasModule} from './module/noticias.module';
import { JuegosModule } from './module/juegos.module'

@NgModule({
    imports: [
        BrowserModule, 
        BrowserAnimationsModule,
        AppRoutingModule,
        GeneralModule,
        MisionesModule,
        ConfigModule,
        GruposModule,
        InsigniasModule,
        InventariosModule,
        LeaderboardsModule,
        MetricasModule,
        MiembrosModule,
        NotificacionesModule,
        OtrosModule,
        PremiosModule,
        ProductosModule,
        PromocionesModule,
        SegmentosModule,
        UbicacionesModule,
        UsuariosModule,
        WorkflowModule,
        DashboardModule,
        ReportesModule,
        BitacoraModule,
        NoticiasModule,
        JuegosModule
    ],
    declarations: [
        //Componentes del container principal
        AppComponent,
        AppFooter,
        AppTopBarComponent,
        InlineProfileComponent,
        AppMenuComponent,
        AppSubMenuComponent,
        //Genericos
        TrabajoInternoComponent,
        //UTILS 
        BreadcrumbComponent
    ],
    providers: [I18nService,KeycloakService, AuthGuard, ValidationService, { provide: ErrorHandler, useClass: ErrorHandlerService }],
    bootstrap: [AppComponent]
})
export class AppModule { }