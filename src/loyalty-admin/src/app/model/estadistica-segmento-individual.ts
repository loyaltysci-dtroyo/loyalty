import { Injectable } from '@angular/core';
import { Segmento } from './';
@Injectable()
export class EstadisticaSegmentoIndividual {
    public segmento: Segmento;
    public miembrosNuevos: number;
    constructor() { }
}