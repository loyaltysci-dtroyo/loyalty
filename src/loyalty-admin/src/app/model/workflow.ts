import { Injectable } from '@angular/core';

@Injectable()
export class Workflow {
    public idWorkflow: string;
    public nombreWorkflow: string;
    public nombreInternoWorkflow: string;
    public indDisparador: string;
    public indEstado: string;
    public numVersion: number;
    public usuarioCreacion: string;
    public usuarioModificacion: string;
    public fechaCreacion: Date;
    public fechaModificacion: Date;
    public referenciaDisparador: string;
    public isWorkflowMisionEspec:boolean;
    constructor() {
        this.indDisparador="A";
        // this.indEstado="B";
     }
}