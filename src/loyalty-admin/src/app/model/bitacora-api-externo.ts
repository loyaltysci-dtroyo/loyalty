import { Injectable} from '@angular/core'

@Injectable()

export class BitacoraApiExternal{

    public fecha: Date;
    public idEntrada: string;
    public httpEstadoRespuesta: string;
    public jsonRespuesta: any;
    public jsonEntrada: any;
    constructor(){}
}