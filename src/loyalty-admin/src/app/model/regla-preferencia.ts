import { Preferencia } from './preferencia';
import { Injectable } from '@angular/core';

@Injectable()
export class ReglaPreferencia {
    idRegla: string;
    indOperador: string;
    valorComparacion: string;
    fechaCreacion: Date;
    usuarioCreacion: string;
    preferencia: Preferencia
}