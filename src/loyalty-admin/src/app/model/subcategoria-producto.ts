import { Injectable } from '@angular/core';
import { CategoriaProducto} from './index';
@Injectable()

/*Esta clase representa a la subcategoría de productos*/
export class SubCategoriaProducto {

    //atributos
    public numVersion: number;
    public idSubcategoria: string;
    public nombre: string;//max 50
    public nombreInterno: string; //max 50
    public imagen: string;
    public descripcion: string; //max 100, opt
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public fechaModificacion: Date;
    public usuarioModificacion: string;
    public idCategoria: CategoriaProducto;

    constructor() { }
}