import { Injectable } from '@angular/core';
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Entidad(service) encargada de representar la tabla de posiciones de los miembros
 */
@Injectable()
export class TablaPosiciones{
    public idTabla: number;//id de caracter unico en el sistema
    public nombre: string;//nombre  de caracter no unico 
    public indTipo: string;//tipo, indica si la tabla es grupal, individual o por equipos
    public imagen: string;
    public idMetrica: number;//metrica a la cual esta asociada la tabla
    public indFormato: string;//indica si se debe desplegar la tabla en la app movil como nombre o solamente iniciales
    public descripcion: string;
    public indGrupalFormaCalculo: string;//indica si el puntaje de la tabla se debe calcular de manera promediada o sumada
    public fechaCreacion: Date;
    public usuarioCreacion: number;
    public fechaModificacion: Date;
    public usuarioModificacion: number;
    public numVersion: number;
    
    constructor(){
        
    }
}