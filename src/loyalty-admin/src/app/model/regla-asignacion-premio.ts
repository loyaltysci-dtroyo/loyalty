import { Injectable } from '@angular/core';
import { Nivel } from './index';
@Injectable()
export class ReglaAsignacionPremio {

    public static TIPO_REGLA_FRECUENCIA_COMPRA = 'A';
    public static TIPO_REGLA_MONTO_COMPRA = 'B';
    public static TIPO_REGLA_ACUMULADO_COMPRA = 'C';

    public static IND_ULTIMO_DIA = 'D';
    public static IND_ULTIMO_MES = 'M';
    public static IND_ULTIMO_ANO = 'A';

    public static IND_FECHA_ULTIMO = 'B';
    public static IND_FECHA_ANTERIOR = 'C';
    public static IND_FECHA_DESDE = 'D';
    public static IND_FECHA_HASTA = 'H';
    public static IND_FECHA_ENTRE = 'E';
    public static IND_FECHA_MES_ACTUAL = 'F';
    public static IND_FECHA_ANO_ACTUAL = 'G';

    public static IGUAL = 'V';
    public static MAYOR = 'W';
    public static MAYOR_IGUAL = 'X';
    public static MENOR = 'Y';
    public static MENOR_IGUAL = 'Z';

    tier: Nivel;
    numVersion: 1;
    idRegla: string;
    indTipoRegla: string;
    indOperador: string;
    valorComparacion: number;
    fechaInicio: Date;
    fechaFinal: Date;
    fechaIndUltimo: string;
    fechaCantidad: number;
    fechaIndTipo: string;
    usuarioCreacion: string;
    usuarioModificacion: string;
    indOperadorRegla: string;

    constructor() {
        this.indTipoRegla=ReglaAsignacionPremio.TIPO_REGLA_FRECUENCIA_COMPRA;
        this.indOperadorRegla="A";
        this.indOperador=ReglaAsignacionPremio.IGUAL;
        this.fechaIndTipo=ReglaAsignacionPremio.IND_FECHA_ULTIMO;
        // this.fechaIndUltimo="0";
        this.valorComparacion=0;
        this.tier= new Nivel();
        this.fechaIndUltimo = ReglaAsignacionPremio.IND_ULTIMO_DIA;
        this.fechaInicio= new Date();
        this.fechaFinal= new Date();
    }

}