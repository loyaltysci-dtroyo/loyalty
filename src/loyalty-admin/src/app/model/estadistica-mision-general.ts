import { Injectable } from '@angular/core';

@Injectable()
export class EstadisticaMisionGeneral {
    public id: number;
    public cantMisionesEncuesta: number;
    public cantMisionesJuego: number;
    public cantMisionesPerfil: number;
    public cantMisionesPreferencia: number;
    public cantMisionesVerContenido: number;
    public cantMisionesSubirContenido: number;
    public cantMisionesSocial: number;
    constructor() {
        this.id=0;
        this.cantMisionesEncuesta=0;
        this.cantMisionesJuego=0;
        this.cantMisionesPerfil=0;
        this.cantMisionesPreferencia=0;
        this.cantMisionesVerContenido=0;
        this.cantMisionesSubirContenido=0;
        this.cantMisionesSocial=0;
    }
}