import { Injectable } from '@angular/core';
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Clase(service) encargada de representar las preguntas que permitiran determinar las preferencias de los miembros 
 */
@Injectable()
export class Preferencia{
    static TIPO_PREF_RESP_MULTIPLE="RM";
    static TIPO_PREF_RESP_UNICA="SU";
    static TIPO_PREF_TEXTO="TX";

    //atributos
    public idPreferencia: string;
    public pregunta: string;
    public respuestas: string;
    public indTipoRespuesta: string;
    public fechaCreacion: Date;
    public usuarioCreacion: number;
    public fechaModificacion: Date;
    public usuarioModificacion: number;
    public numVersion: number;
    constructor(){

    }
}