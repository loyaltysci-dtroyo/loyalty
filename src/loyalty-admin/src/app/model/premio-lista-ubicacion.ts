import { Injectable } from '@angular/core';
import { Premio, Ubicacion } from './index';

@Injectable()

/*Esta clase representa la lista de las ubicaciones elegibles para los premios*/
export class PremioListaUbicacion {

    //atributos
    public usuarioCreacion: string;
    public fechaCreacion: Date;
    public ubicacion: Ubicacion;
    public premio: Premio;
    public indTipo: string;

    constructor(){}
}