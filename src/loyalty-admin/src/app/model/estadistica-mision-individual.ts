import { Injectable } from '@angular/core';

@Injectable()
export class EstadisticaMisionIndividual {
    public idMiembro:string;
    public cantMisionesEncuesta:number;
    public cantMisionesJuego:number;
    public cantMisionesPerfil:number;
    public cantMisionesPreferencia:number;
    public cantMisionesVerContenido:number;
    public cantMisionesSubirContenido:number;
    public cantMisionesSocial:number;
    constructor() { }
}