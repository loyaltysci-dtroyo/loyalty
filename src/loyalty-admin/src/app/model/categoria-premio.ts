import { Injectable } from '@angular/core';

@Injectable()

/*Esta clase representa a las categorías de premios*/
export class CategoriaPremio{

    //atributos
    public idCategoria: string;
    public nombre: string; // max 100
    public imagen: string;
    public descripcion: string; //max 200, opt
    public usuarioCreacion: string;
    public fechaCreacion: Date;
    public usuarioModificacion: string;
    public fechaModificacion: Date;
    public numVersion: number;
    public nombreInterno: string; // max 50

    constructor(){}
}