import { Metrica, AtributoDinamico, Nivel, ListaReglas, MetricaNivelMetrica } from './index';
import { Injectable } from '@angular/core';
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Entidad(service) encargada
 */
@Injectable()
export class ReglaMiembroAtb {
    public nombre: string;//nombre de la regla, no es de caracter unico
    public idRegla: string;
    public indAtributo: string;//indicador del atributo sobre el cual la regla ejerce, de la lista de incidadores de atributos
    public indOperador: string;//operador de comparacion de la rela, puede variar dependiendo de si la regla es sobr eun atributo numerico o texto
    public valorComparacion;//valor cobre el cual se compara el atributo
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public idLista: ListaReglas;
    public atributoDinamico: AtributoDinamico;//en caso de que la regla referencie u atributo dinamico este debe ser especificaado
    public metrica: Metrica;//metrca en caso de que la regla sea de nivel de metrica
    public nivelMetrica: Nivel;//nivel de la metrica en caso de que la regla sea de nivel de metrica
    constructor() {
        this.atributoDinamico = new AtributoDinamico();
        this.nivelMetrica = new Nivel();
        this.metrica = new Metrica();
    }
}