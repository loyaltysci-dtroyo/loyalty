//Reglas de otorgacion de premio basados en cantidades de compra

import { Injectable } from '@angular/core';
import { Premio } from './';
@Injectable()
export class ReglaPremioCompra {
    public idRegla: string;
    public premio: Premio;
    public montoInicial: number;
    private numVersion: number;
    constructor() { 
        
    }
}