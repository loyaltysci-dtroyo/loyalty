import { Injectable } from '@angular/core';

@Injectable()
export class ReglaAsignacionBadge { 
    public static TIPO_REGLA_FRECUENCIA_COMPRA = 'A';
    public static TIPO_REGLA_MONTO_COMPRA = 'B';
    public static TIPO_REGLA_ACUMULADO_COMPRA = 'C';

    public static IND_ULTIMO_DIA = 'D';
    public static IND_ULTIMO_MES = 'M';
    public static IND_ULTIMO_ANO = 'A';

    public static IND_FECHA_ULTIMO = 'B';
    public static IND_FECHA_ANTERIOR = 'C';
    public static IND_FECHA_DESDE = 'D';
    public static IND_FECHA_HASTA = 'H';
    public static IND_FECHA_ENTRE = 'E';
    public static IND_FECHA_MES_ACTUAL = 'F';
    public static IND_FECHA_ANO_ACTUAL = 'G';

    public static IGUAL = 'V';
    public static MAYOR = 'W';
    public static MAYOR_IGUAL = 'X';
    public static MENOR = 'Y';
    public static MENOR_IGUAL = 'Z';

    idRegla: string
    indTipoRegla: string;
    indOperadorComparacion: string;
    valorComparacion: string;
    fechaInicio: Date;
    fechaFin: Date;     
    fechaIndUltimo: string;
    fechaCantidad: string;
    fechaIndTipo: string;
    usuarioCreacion: string;
    usuarioModificacion: string;
    fechaCreacion: Date;
    fechaModificacion: Date;
    referencia: string;
    indOperadorRegla: string;
    indTipoValorComparacion:string;
    idTier: string

    constructor() {
        this.indTipoRegla = ReglaAsignacionBadge.TIPO_REGLA_FRECUENCIA_COMPRA;
        this.indOperadorRegla = "A";
        this.indOperadorComparacion = ReglaAsignacionBadge.IGUAL;
        this.fechaIndTipo = ReglaAsignacionBadge.IND_FECHA_ULTIMO;
        this.fechaIndUltimo = ReglaAsignacionBadge.IND_ULTIMO_DIA;
        this.fechaInicio = new Date();
        this.fechaFin = new Date();

    }

}