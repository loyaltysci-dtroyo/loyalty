import { IDMetrica } from './IDMetrica';
import { Injectable } from '@angular/core';
import { Metrica, Nivel } from './index';
@Injectable()
/**
 * Flecha Roja Technologies
 * Clase que representa una union entre una metrica y un nivel de metrica, usado especificamente 
 * para itroducir reglas de atributo de miembro (fue necesaria debido a una restriccion de back-end)
 * 
 */
export class MetricaNivelMetrica {
    public idMetricaNivelMetrica:IDMetrica;
    constructor() { 
        this.idMetricaNivelMetrica= new IDMetrica();
    }
}


