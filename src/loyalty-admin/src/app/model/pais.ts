import { Injectable } from '@angular/core';
@Injectable()

/* Esta clase representa los países utilizados para la dirección del cliente */
export class Pais{

    public idPais: number;
    public alfa2: string;
    public alfa3: string;
    public codigoNumero: number;
    public nombrePais: string;
    
    constructor(){}
}