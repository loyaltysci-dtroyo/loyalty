import { Injectable } from '@angular/core';

@Injectable()

/**
 * Autor: Jaylin Centeno
 */
export class TrabajoInterno {

    public idTrabajo: string; //Identificación autogenerada por el sistema.
    public indEstado: string; //Indica el estado del trabajo P=procesando, F=fallido, C=completado
    public resultado: string; //Se muestra el resultado del trabajo intero
    public indTipo: string; //Indica el tipo de trabajo, N=notificación, E=exportar, I=importar
    public fechaCreacion: Date; //Fecha en la que se creó el trabajo.
    public fechaActualizacion: Date; //Fecha en la que se completo el trabajo.
    public porcentajeCompletado: number; //Representa la cantidad de trabajo completado
    
    constructor(){}
}