import { Injectable } from '@angular/core';

@Injectable()

/**
 * Autor: Jaylin Centeno
 * Esta entidad representa los atributos dinámicos del producto
 */
export class AtributoDinamicoProducto{

    public idAtributo: string;//Identificación autogenerada por el sistema, no llenar.
    public imagen: string; //Representa al atributo 
    public nombre: string;// Nombre con el que se presenta, max 150
    public descripcion: string;//Descripción del atributo, max 500, opcional    
    public indTipo: string; //Tipo de dato N:numerico,T:string,F:fecha,B:bool
    public valorOpciones: string; //Guarda las opciones de respuesta para el atributo, max 500 opcional
    public indRequerido: string; //Indica si el atributo es requerido
    public indEstado: string; //Estado B: borrador, A: archivado P: publicado
    public fechaCreacion: Date; // Fecha en la que se creo, no llenar.
    public usuarioCreacion: string; // Usuario que creo el atributo, no llenar.
    public fechaModificacion: Date; // Fecha de la última modificación, no llenar.
    public usuarioModificacion: string; // Último usuario que modifico el atributo, no llenar.
    public numVersion: number; // Se guarda el número de version , no llenar.
    public valorAtributo: string; //Guarda el valor asignado por default o escogido de atributo
    public nombreInterno: string; //Nombre para manejo en el sistema
    constructor(){}
}