import { Injectable } from '@angular/core';

@Injectable()

/*Esta clase representa al objeto grupo*/
export class Grupo{

    //atributos
    public idGrupo: string;
    public nombre: string;//max 50
    public descripcion: string;//max 100
    public avatar: string;//opcional
    public indVisible: string;//caracter
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public fechaModificacion: Date;
    public usuarioModificacion: string;
    public numVersion: number;
    public rankingMiembros: number;

    constructor(){
        this.indVisible = "I";
    }
}