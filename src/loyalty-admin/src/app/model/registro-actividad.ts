
import { Injectable } from '@angular/core';

@Injectable()
export class RegistroActividad {
    fecha: Date;
    indTipo: string;
    idElemento: string;
    nombreElemento: string;
    nombreInternoElemento: string;
    idMiembro: string;
    nombreMiembro: string
    constructor() { }
}
