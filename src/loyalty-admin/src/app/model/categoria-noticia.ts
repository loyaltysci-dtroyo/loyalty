import { Injectable } from '@angular/core';

@Injectable()

/**
 * Autor: Jaylin Centeno
 * Esta entidad representa las categorías de las noticias (pueden tener más de una)
 */
export class CategoriaNoticia{

    public idCategoria: string;//Identificación autogenerada por el sistema, no llenar.
    public nombre: string;// Nombre de la categoría, max 100
    public imagen: string; // Imagen que representa la categoría 
    public fechaCreacion: Date; // Fecha en la que se creo, no llenar.
    public usuarioCreacion: string; // Usuario que creo la categoría, no llenar. 
    public fechaModificacion: Date; // Fecha de la última modificación, no llenar.
    public usuarioModificacion: string;// Último usuario en modificar la categoría, no llenar.
    public numVersion: number; // Se maneja el número de version de la categoría, ++, no llenar.

    constructor(){}

}