import { Injectable } from '@angular/core';

@Injectable()
export class WorkflowNodo {
    public idNodo: string;
    public nombreNodo: string;
    public indTipoTarea: string;
    public referencia: string;
    public numVersion: number;
    public idSucesor: WorkflowNodo;
    public fechaCreacion: Date;
    public fechaModificacion: Date;
    public usuarioCreacion: string;
    public usuarioModificacion: string;
    public cantReferencia: number;
    public isNodoMisionEspecial:boolean;
    constructor() { 
        this.indTipoTarea="E";
    }
}