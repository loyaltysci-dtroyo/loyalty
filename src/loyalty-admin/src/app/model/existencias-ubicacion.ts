import { Injectable } from '@angular/core';
import { Ubicacion, Premio } from './index';

@Injectable()

/*Esta clase representa las existencias de un premio en una ubicacón*/
export class ExistenciaUbicacion {

    public saldoInicial: number; //Representa el saldo de inicial de un premio al cerrar el mes
    public entradas: number; //Representan las entradas que ha tenido el premio en una ubicacón
    public salidas: number; //Representa las salidas que se han realizado de ese premio (redencion, ajuste menos, inter ubicacón)
    public premio: Premio; 
    public ubicacion: Ubicacion;

    constructor() { }
}