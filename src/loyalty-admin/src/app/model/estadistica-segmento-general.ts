import { Injectable } from '@angular/core';

@Injectable()
export class EstadisticaSegmentoGeneral {
    public id: number;
    public cantMiembrosSegmentados: number;
    public cantMiembrosNoSegmentados: number;
    constructor() { }
}