import { Injectable } from '@angular/core';
import { SubCategoriaPk, CategoriaProducto, Producto, SubCategoriaProducto } from './index';

@Injectable()

/*Esta clase representa a las subcategorías de productos*/
export class ProductoCategSubCategoria{

    //atributos
    public productoCategSubcategPK: SubCategoriaPk;
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public idCategoria: CategoriaProducto;
    public producto: Producto;
    public subcategoriaProducto: SubCategoriaProducto;
        
    constructor(){}
}