import { Injectable } from '@angular/core';

@Injectable()
export class PreguntaMillonario {
    public idPregunta: string;
    public idJuego: string;
    public pregunta: string;
    public respuesta1: string;
    public respuesta2: string;
    public respuesta3: string;
    public respuesta4: string;
    public respuestaCorrecta: number;
    public puntos: number;
    public fechaCreacion: Date;//seteado en el api, ignorar
    public usuarioCreacion: string;//seteado en el api, ignorar
    public fechaModificacion: Date;//seteado en el api, ignorar
    public usuarioModificacion: string;//seteado en el api, ignorar
    public numVersion: number;//numero de version de la tabla

    constructor(){}
}  