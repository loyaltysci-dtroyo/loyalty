import { Injectable } from '@angular/core';

@Injectable()

/*Esta clase representa a las categorías de productos*/
export class CategoriaProducto{

    //atributos
    public idCategoria: string;
    public nombre: string; // max 50
    public nombreInterno: string; // max 50
    public imagen: string;
    public descripcion: string; //max 100, opt
    public usuarioCreacion: string;
    public fechaCreacion: Date;
    public usuarioModificacion: string;
    public fechaModificacion: Date;
    public numVersion: number;
    public loginId: string;

    constructor(){}
}