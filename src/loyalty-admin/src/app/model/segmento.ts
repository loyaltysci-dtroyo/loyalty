import {Usuario, ListaReglas, Miembro } from './index';
import { Injectable } from '@angular/core';
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Entidad(service) encargada de reprsentar un segmento en el sistema
 */
@Injectable()
export class Segmento{
     public idSegmento:string;
     public nombre:string;//npmbre de caracter no unico para el segmento
     public nombreDespliegue:string;//nombre unico del segmento en el sistema
     public descripcion:string;//descrpcion del segmento
     public indEstatico:string;//indica si el segmento es estatico o dinamico
     public frecuenciaActualizacion:number;//frecuencia de actualizacion, en caso de que el segmento sea dinamico
     public ultimaActualizacion:Date;//ultima vez que se actualizaron los metadatos del segmento
     public indEstado:string;//indicador del estado del segmento, que opuede ser archivado, borrador o activo
     public fechaPublicacion:Date;
     public fechaArchivado:Date;//cuando se archiva, este campo contiene la fecha de archivado 
     public tags:string;//campo auxiliar para poner tags
     public fechaCreacion:Date;
     public usuarioCreacion:string;
     public fechaModificacion:Date;
     public usuarioModificacion:string;
     public numVersion:number;
     public listaReglasList:ListaReglas[];//llista de grupos de reglas sel segmento
     public listaMiembrosList:Miembro[];//lista de miembros a incluir en el sistema
    constructor(){
        this.indEstatico="D";
        this.indEstado="B";
        this.listaReglasList=[];
    }
}