import { Injectable } from '@angular/core';
import { AtributoDinamicoProducto, Producto, ProductoAtributoPk } from './index';

@Injectable()

/*Esta clase representa la relación de un producto y un atributos*/
export class ProductoAtributo{

    //atributos
    public productoAtributoPK: ProductoAtributoPk;
    public valor: string; //max 300
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public atributoDinamicoProducto: AtributoDinamicoProducto;
    public producto: Producto;
        
    constructor(){}
}