import { Injectable } from '@angular/core';

@Injectable()

/**
 * Autor: Jaylin Centeno
 * Esta entidad representa las noticias 
 */
export class Noticia{

    public idNoticia: string;//Identificación autogenerada por el sistema, no llenar.
    public titulo: string;// Título que tendrá la noticia, max 100
    public contenido: string;// Contenido (información) de la noticia, max 300
    public imagen: number; // Imagen representativa de la noticia
    public indEstado: string; // Indicador para el estado A=publicado B=archivado C=borrador D=inactivo
    public fechaCreacion: Date; // Fecha en la que se creo la noticia, no llenar.
    public fechaModificacion: Date; // Fecha de la última modificación, no llenar.
    public fechaPublicacion: Date; // Fecha en la que se publico la noticia, no llenar.
    public indAutorAdmin: boolean; //Usado para saber si el autor fue un admin o no
    public idAutor: string; //Id del admin/miembro que creo/modifico la noticia
    public numVersion: number; // Se maneja el número de version de la noticia, no llenar.
    public cantComentarios: number; //Cantidad de comentario por noticia
    public cantMarcas: number; //Cantidad de marcas(me gusta) para la noticia
    public nombreAutor: string; //Nombre del autor de la noticia
    public avatarAutor: string; //Avatar del autor de la noticia

    constructor(){}

}