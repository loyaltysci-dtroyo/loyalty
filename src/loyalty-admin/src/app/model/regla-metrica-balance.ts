import { Injectable } from '@angular/core';
import { ListaReglas, Metrica  } from './index';
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Entidad que representa la regla de balance de metrica en el sistema
 */
@Injectable()
export class ReglaMetricaBalance {
    public nombre:string;//nombre de la regla
    public idRegla:string;//id unico para la regla
    public indOperador:string;//indicador de operador, de una lista de operadores numericos
    public indBalance:string;//usado para determinar si se debe tomar en cuenta el balance total de expirar/ganar/redimir metrica
    public valorComparacion:string;//valor con el cual comparar el balance de la metrica que tiene el usuario en ese momento
    public fechaCreacion:Date;
    public usuarioCreacion:string;
    public idLista:ListaReglas;//referencia la lista de reglas a la cual pertenece esta regla
    public idMetrica:Metrica;//referencia la metrica a la cual esta asociada la regla
    
    constructor() { 
        this.idMetrica= new Metrica();
        
    }
}