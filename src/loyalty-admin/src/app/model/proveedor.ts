import { Injectable } from '@angular/core';

@Injectable()

/* Esta clase representa a los proveedores, utilizado para el manejo de inventarios de premio */
export class Proveedor {

    //Atributos
    public idProveedor: string; //Identificación autogenerada por el sistema, manejo interno, no se llena.
    public codigoProveedor: string; //Código ingresado por el usuario, utlizado en el despliegue, max 40
    public descripcion: string; //Representa el nombre/descripción del proveedor, max 100
    public direccion: string; //Dirección física, max 200, opcional
    public telefono: string; //Teléfono, max 20, opcional
    public cedJuridica: string;//Cédula juridica del proveedor, max 30, opcional
    public fechaCreacion: Date; // Fecha en la que se ingreso el proveedor, no llenar.
    public usuarioCreacion: string; // Usuario que creo el proveedor, no llenar.
    public usuarioModificacion: string; // Último usuario que modifico el proveedor, no llenar.
    public fechaModificacion: Date; // Fecha de la última modificación, no llenar.
    public numVersion: number; // Se maneja el número de version del proveedor, no llenar.
    
    constructor(){}
} 