import { Injectable } from '@angular/core';

@Injectable()

/*Esta clase representa a la llave primaria compuesta de la subcategoría de productos*/
export class SubCategoriaPk{

    //atributos
    public idProducto: string;
    public idSubcategoria: string;

    constructor(){}
}