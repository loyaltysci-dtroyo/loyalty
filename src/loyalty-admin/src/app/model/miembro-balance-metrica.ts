import { Injectable } from '@angular/core';
import { Miembro, Metrica } from './index';
@Injectable()

export class MiembroBalance{

    public progresoActual: number; //
    public totalAcumulado: number; //
    public disponible: number; //
    public vencido: number; //
    public redimido: number; //
    public numVersion: number; //
    public metrica: Metrica; //
    public miembro: Miembro; //
    constructor(){}
}
