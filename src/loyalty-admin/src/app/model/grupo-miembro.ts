import { Injectable } from '@angular/core';
import { Grupo, Miembro } from './index';
@Injectable()

/*Esta clase representa la asociación entre miembros y grupos*/
export class GrupoMiembro{

    //atributos
    /*public grupoMiembroPK:{
        idMiembro: string,
        idGrupo: string
};*/
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public idGrupo: Grupo;
    public idMiembro: Miembro;

    constructor(){}
}