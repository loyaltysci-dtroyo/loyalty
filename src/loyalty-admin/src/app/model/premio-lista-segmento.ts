import { Injectable } from '@angular/core';
import { Premio, Segmento } from './index';

@Injectable()

/*Esta clase representa la lista de los segmentos elegibles para los premios*/
export class PremioListaSegmento {

    //atributos
    public usuarioCreacion: string;
    public fechaCreacion: Date;
    public segmento: Segmento;
    public premio: Premio;
    public indTipo: string;

    constructor(){}
}