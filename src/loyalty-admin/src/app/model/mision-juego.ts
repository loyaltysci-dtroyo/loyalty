import { Injectable } from '@angular/core';

@Injectable()

/*Esta clase representa la entidad de juego que será utilizada en mision*/
export class MisionJuego {

    public tipo: string; //Idenficador para definir el tipo de juego R=ruleta, M=Juego millonario, A=raspadita,O=Rompecabezas Identificación autogenerada para el juego, no se llena.
    public estrategia: string; //Idenficador para definir el tipo de estrategía, P=probabilidad, R=random
    public fechaCreacion: Date; //Fecha en la que se creo el juego, no llenar.
    public fechaModificacion: Date; //Fecha de la última modificación, no llenar.
    public usuarioCreacion: string; //Usuario que creo el juego, no llenar.
    public usuarioModificacion: string; //Último usuario que modifico el juego, no llenar.
    public idMision: string; //Representa el id de la misión a la que pertenece el juego
    public numVersion: number; // Número de version del juego, no llenar.
    public imagen: string; //Imagen a desplegar solo cuando el tipo es rompecabeza, opt 
    public tiempo: number; //Cantidad de tiempo limite para armar el rompecabezas
    public imagenesRompecabezas: string; // Contiene las imagenes creadas a partir de la imagen prinicpal para el rompecabezas
    public idInstanciaMillonario: string; //Id de la instancia quien quiere ser millonario
    public idJuegoMillonario: string; //Id del juego quien quiere ser millonario
    constructor() { }
}