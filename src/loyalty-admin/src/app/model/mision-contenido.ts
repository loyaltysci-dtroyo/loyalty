import { Injectable } from '@angular/core';
import {Mision} from "./index";
@Injectable()
export class MisionVerContenido {
    public idMision:string;
    public indTipo:string;
    public url:string;
    public texto:string;
    public fechaCreacion:Date;
    public fechaModificacion:Date;
    public usuarioCreacion:string;
    public usuarioModificacion:string;
    public numVersion:number;
    constructor() { }
}