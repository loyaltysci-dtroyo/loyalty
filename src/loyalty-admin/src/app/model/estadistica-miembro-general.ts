import { Injectable } from '@angular/core';

@Injectable()
export class EstadisticaMiembroGeneral {
    public id:number;
    public cantMiembrosEdad1825:number;
    public cantMiembrosEdad2535:number;
    public cantMiembrosEdad3545:number;
    public cantMiembrosEdad4555:number;
    public cantMiembrosEdadMayor55:number;
    constructor() { }
}