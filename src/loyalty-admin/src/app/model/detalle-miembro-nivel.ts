import { Injectable } from '@angular/core';

@Injectable()

/* Esta clase representa el detalle de los miembros y nivel*/
export class DetalleMiembroNivel{

    public nivel: string;
    public idNivel: string;
    public metricaId: string;   
    public metricaInicial: number;
    public metricaNombre: string;

    constructor(){}
}