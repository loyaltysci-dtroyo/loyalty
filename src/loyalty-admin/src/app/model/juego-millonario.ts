import { Injectable } from '@angular/core';
/**Flecha Roja Technologies
 * Fernando Aguilar
 * Clase que representa un objeto mision, 
 */

@Injectable()
export class JuegoMillonario {
    public idJuego: string;
    public nombre: string;
    public descripcion: string;
    public complete: number;
    public fechaCreacion: Date;//seteado en el api, ignorar
    public usuarioCreacion: string;//seteado en el api, ignorar
    public fechaModificacion: Date;//seteado en el api, ignorar
    public usuarioModificacion: string;//seteado en el api, ignorar
    public numVersion: number;//numero de version de la tabla
    constructor() {}
}  