import { Injectable } from '@angular/core';
import { CategoriaPromocion } from './index';
@Injectable()
/**modelo para categorias de promocion, 
 * no confundir con otras categorias 
 * */
export class Categoria {

    public idCategoria:string;
    public nombre:string;
    public nombreInterno:string;
    public imagen:string;
    public descripcion:string;
    public fechaRegistro:Date;
    public fechaCreacion:Date;
    public usuarioCreacion:string;
    public fechaModificacion:Date;
    public usuarioModificacion:string;
    public numVersion:number;
    public categoriaPromocionList:CategoriaPromocion[];

    constructor() { }
}