import { ReglaAvanzada } from './regla-avanzada';
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Segmento,
        ReglaMiembroAtb,
        ReglaMetricaCambio,
        ReglaMetricaBalance } from './index';

@Injectable()
/**
 * Representa la lista de reglas del segmento, es una manera de agrupar reglas, toda reglas
 * debe pertnecer a una lista de reglas
 */
export class ListaReglas {
        public idLista: string;
        public indOperador: string;
        public orden: number;
        public fechaCreacion: Date;
        public usuarioCreacion: string;
        public fechaModificacion: Date;
        public usuarioModificacion: string;
        public numVersion: number;
        // public reglaMiembroAtbList: ReglaMiembroAtb[];
        // public reglaMetricaCambioList: ReglaMetricaCambio[];
        //public reglaMetricaBalanceList: ReglaMetricaBalance[];
        // public reglaAvanzadaList:ReglaAvanzada[];
        public nombre: string;

        constructor() {
                this.indOperador="O";
                //this.idSegmento = new Segmento();
                //this.reglaMetricaBalanceList = [];
                // this.reglaMetricaCambioList = [];
                // this.reglaMiembroAtbList = [];
        }

}

