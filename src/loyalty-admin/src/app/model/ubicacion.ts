import { Injectable } from '@angular/core';

@Injectable()

/*
 *Autor: Jaylin Centeno
 *Esta entidad representa las ubicaciones/locaciones utilizadas por la empresa/compañía en el sistema
 */
export class Ubicacion {

    //atributos
    public idUbicacion: string; // Identificación autogenerada para la ubicación, no llenar.
    public nombre: string; // Nombre de la ubicacion, 50 max
    public nombreDespliegue: string; // Nombre para manejo interno del sistema, 50 max
    public indEstado: string; // Representa el estado del producto, D=draft, P=publicada activa, I=publicada inactiva, A=archivado
    public indDirPais: string; // Guarda el iso para identificar el país, 3 max, opt   
    public indDirEstado: string; // Estado en el que se ubica la locación, 100 max, opt
    public indDirCiudad: string; // Ciudad en la que se ubica la locacion, 100 max
    public direccion: string; // Dirección exacta de la ubicación, 250 max
    public dirProyeccion: string; // Este campo aun no esta definido
    public horarioAtencion: string; // Horario en el que la locación esta disponible, 150 max, opt
    public telefono: string; // Teléfono de la ubicación, 25 max, opt
    public indCalendarizacion: string; // Indica el tipo de calendarización/efectividad, R=recurrente, C=calendarizado, P=permanente
    public fechaInicio: Date; // Fecha en que inicia la aplicación de la efectividad, opt
    public fechaFin: Date; // Fecha en que termina la efectividad, opt
    public indDiaRecurrencia: string; // Maneja los días en que se repite la efectividad, 7 max, opt
    public indSemanaRecurrencia: number; // Permite saber las semanas en que se aplica la efectividad cuando es recurrente
    public rangoDeteccion: number; // Este campo aun no esta definido
    public fechaCreacion: Date; // Fecha en la que se creo el premio, no llenar.
    public usuarioCreacion: string; // Usuario que creo el premio, no llenar.
    public fechaModificacion: Date; // Fecha de la última modificación, no llenar.
    public usuarioModificacion: string; // Último usuario que modifico el premio, no llenar.
    public dirLat: number; // Representa la latitud específica en la que se encuentra la ubicación
    public dirLng: number; // Representa la longitud específica en la que se encuentra la ubicación
    public numVersion: number; // Se maneja el número de versión del premio, no llenar.
    public codigoInterno: string; // Es el código interno, permite identificar la ubicación de forma externa. Obligatorio

    constructor (){
        this.indDiaRecurrencia = "";
    }

}