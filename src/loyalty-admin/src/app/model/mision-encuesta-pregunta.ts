import { Injectable } from '@angular/core';
/**
 * Flecha Roja Technologies
 * Fernando Aguilar 
 * 5-dic-2016
 * Clase encarada de representar un item de pregunta o calificacion para una encuesta
 * empelada en las misiones cuando la actividad de la mision es encuesta
 */

@Injectable()
export class MisionEncuestaPregunta {

    static readonly TIP_PREG_ENCUESTA = 'E';
    static readonly TIP_PREG_CALIFICACION='C';

    static readonly TIP_RESP_SELECCION_MULTIPLE="RM";
    static readonly TIP_RESP_SELECCION_UNICA="RU";
    static readonly TIP_RESP_RESPUESTA_ABIERTA_TEXTO="RT";
    static readonly TIP_RESP_RESPUESTA_ABIERTA_NUMERICO="RN";
    
    static readonly RESP_CORR_TODAS='T';
    static readonly RESP_CORR_ES='U';
    static readonly RESP_CORR_CUALQUIERA_DE='C';
    static readonly RESP_CORR_SON='G';
    static readonly RESP_CORR_MENOR_IGUAL='D';
    static readonly RESP_CORR_IGUAL='E';
    static readonly RESP_CORR_MAYOR_IGUAL='F';


    public idPregunta: string;
    public indTipoPregunta: string;
    public pregunta: string;
    public respuestas: string;
    public indTipoRespuesta: string;//M=multiple,U=unica,A=abierta,indica si es seleccion unica multiple o abierta
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public fechaModificacion: Date;
    public usuarioModificacion: String;
    public numVersion: number;
    public imagen: string;
    public indComentario: boolean;//para calificacion, determina si se puede adjuntar un comentario a la calificacion
    public comentarios: string;
    public indRespuestaCorrecta: string;// T=Todas, U=Valor Unico C=Cualquiera de
    public respuestasCorrectas: string;
    constructor() {
        this.indTipoPregunta = "E";
        this.indComentario = true;
        this.indTipoRespuesta = "RM";
        this.indRespuestaCorrecta = "T";
    }


    
}