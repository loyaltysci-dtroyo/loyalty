import {Promocion, Segmento} from './index';
import { Injectable } from '@angular/core'
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Clase(service) encargada de representar la lista de segmentos eleibles de una Promocion
 */
@Injectable()
export class PromocionListaSegmento {
    public indTipo:boolean;//indicador de tipo incluir o excluir
    public fechaCreacion:Date;
    public usuarioCreacion:string;
    public promocion:Promocion;
    public segmento:Segmento;
    constructor() { }
}