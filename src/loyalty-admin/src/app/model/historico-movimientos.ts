import { Injectable } from '@angular/core';
import { Miembro, Premio, Ubicacion } from './index';

@Injectable()

/*Esta clase representa la entidad de Historico de Movimientos*/
export class HistoricoMovimientos{

    //atributos
    public idHistorico: string; //Identificación autogenerada por el sistema, no llenar.
    public codPremio: Premio; //Representa el código del premio que tiene un registro de movimiento
    public idMiembro: Miembro; //Representa al miembro en caso de que el historico sea por redención, de lo contrario queda vacío, opcional
    public cantidad: number; //Es la cantidad de ese premio que fue movido
    public tipoMovimiento: string; //Es el tipo de movimiento: R=redimido, I=inter ubicación, C=compra, A=ajuste más, M=ajuste menos.
    public fecha: Date; //Fecha de creación del historico
    public ubicacionOrigen: Ubicacion; //Id de la ubicación en la que se origina el movimiento
    public ubicacionDestino: Ubicacion; //Id de la ubicación de destino a la que se envían los premios, en caso de ser inter ubicación, opcional
    public status: string; //Estado del historico en caso de que sea por redención, S=red. sin retirar, R=red. retirado, A=anulado, opcional
    public numDocumento: number; //Representa el documento del cual se creo el registro de movimiento
    public precioPromedio: number; //Precio promedio del premio
    constructor(){ }
}