import { AtributoDinamico } from './index';
import { Injectable } from '@angular/core';

@Injectable()
export class MisionAtrPerfil {
    public idAtributo: string;
    public indRequerido: boolean;//indica si el atributo es requerido para actualizar
    public indAtributo: string;//indica el atributo, mismos valores de reglas de segmento
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public fechaModificacion: Date;
    public idAtributoDinamico: AtributoDinamico;//en caso de que el atributo sea un atributo dinamico
    public numVersion: number;
    constructor() {
        this.indRequerido = true;
        this.fechaCreacion=new Date();
        this.fechaModificacion= new Date();
    }
}