import { Injectable } from '@angular/core';
import { Miembro } from './index';
import { Nivel } from './index';
import { Metrica } from './index';
@Injectable()

/* Esta clase representa la asociación del miembro con los niveles */
export class MiembroNivel{

    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public metrica: Metrica;
    public miembro: Miembro;
    public nivel: Nivel;

    constructor(){}
}