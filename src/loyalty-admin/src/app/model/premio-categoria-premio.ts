import { Injectable } from '@angular/core';
import { CategoriaPremio, Premio } from './index';

@Injectable()

/*Esta clase representa a las relacion entre categoría y premios*/
export class PremioCategoriaPremio{

    //atributos
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public categoriaPremio: CategoriaPremio;
    public premio: Premio;

    constructor(){}
}