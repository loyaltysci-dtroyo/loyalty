import { Injectable } from '@angular/core';
import { Metrica,Grupo} from './index';
@Injectable()
export class Leaderboard {
    public idTabla: string;
    public nombre: string;//npmbre de caracter no unico
    public indTipo: string;//grupal o usuario
    public imagen: string;
    public indFormato: string;//nombre o nombre e iniciales
    public descripcion: string;
    public indGrupalFormaCalculo: string;//forma de calcular el puntaje de la tabla, suma o promedio
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public fechaModificacion: Date;
    public usuarioModificacion: string;
    public numVersion: number;
    public idMetrica: Metrica;
    public idGrupo:Grupo
    //dependencias: metrica - sobre la cual se calcula la tabla, grupo - en caso de que sea grupal
    constructor() {
        this.indFormato = "N";
        this.indGrupalFormaCalculo = "S";
        this.indTipo = "G";
    }
}