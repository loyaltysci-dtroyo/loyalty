import { Injectable } from '@angular/core';
import { Promocion } from './index';
@Injectable()
export class Notificacion {
    public idNotificacion: string;
    public nombre: string;
    public nombreInterno: string;
    public texto: string;//contenido del texto que se enviara en la notificacion
    public indTipo: string;//E=email P=push
    public indEvento: string;//M=manual C=calendarizado T=trigger
    public indObjetivo: string;//M=mision P=promocion R=recompensa
    public indEstado: string;// B=borrador, P=publicado (encolado en el server), E=ejecutado (enviado a los miembros), A=Archivado(cuando se calendariza y es cancelado) 
    public fechaEjecucion: Date;//fecha en que se ejecuta la notificacion en caso de calendarizarla
    public usuarioCreacion: string;
    public usuarioModificacion: string;
    public fechaCreacion: Date;
    public fechaModificacion: Date;
    public numVersion: number;
    public encabezadoArte: string;//encabezado para el arte en caso de que sea una notificaion push
    public imagenArte: string;//imagen para el arte en caso de que sea notificacion push
    public promocion: Promocion//promocion objeivo 
    constructor(
    ) {

    }
}