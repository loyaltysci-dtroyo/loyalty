import { Injectable } from '@angular/core';
import { MiembroNivel } from './index';
@Injectable()

/**
 * Autor: Jaylin Centeno
 * Esta entidad representa a los miembros del sistema
 */
export class Miembro{

    public idMiembro: string; //Identificación autogenerada por el sistema, no llenar.
    public docIdentificacion: string; //Número de dentificacion del usuario;
    public fechaIngreso: Date; //Fecha en la que ingreso al sistema;
    public indEstadoMiembro: string; //Representa el estado del miembro, A=activo, I=inactivo
    public fechaExpiracion: Date; //Fecha de expiración del usuario
    public direccion: string; //Direccion de residencia
    public ciudadResidencia: string; //Ciudad de residencia
    public estadoResidencia: string; //Estado de residencia
    public paisResidencia: string; //País de residencia;
    public codigoPostal: string; //Código postal del miembro
    public telefonoMovil: string; //Teléfono para contactar
    public fechaNacimiento: Date; 
    public indGenero: string; //Genero de la persona, M=masculino F=femenino
    public indContactoEmail: boolean; //
    public indContactoSms: boolean; //
    public indContactoNotificacion: boolean; //
    public indContactoEstado: boolean; //
    public indEstadoCivil: string; //
    public frecuenciaCompra: string; // 
    public fechaSuspension: Date; //
    public causaSuspension: string; //
    public indEducacion: string; //
    public ingresoEconomico: number; //
    public indHijos: boolean; //
    public comentarios: string; //
    public indPolitica: boolean; //Indicador que valida si acepta las politicas
    public fechaPolitica: Date; //Fecha en la que acepto la politica
    public usuarioCreacion: string; // Usuario que creo al miembro, no llenar.
    public fechaCreacion: Date; // Fecha en la que se creo el miembro, no llenar.
    public usuarioModificacion: string; // Último usuario que modifico el miembro, no llenar.
    public fechaModificacion: Date; // Fecha de la última modificación, no llenar.
    public numVersion: number; //Se maneja el número de version del miembro, no llenar.
    public avatar: string; //
    public nombre:string; //
    public apellido:string;//
    public apellido2: string; //
    public correo:string; //
    public contrasena:string; //
    public nombreUsuario: string; //
    public totalAcumulado: number; //
    public miembroNivelList: MiembroNivel[];
    public indMiembroSistema: boolean; //Indicador 
    
    constructor(){
    }
}