import { Injectable } from '@angular/core';

@Injectable()
export class MisionRealidadAumentada {
    public  idMision: string;
    public idBlippar: number;
    public fechaCreacion: Date;
    public fechaModificacion: Date;
    public usuarioCreacion: string;
    public usuarioModificacion: string;
    public numVersion: number;
    
    constructor() { }
}