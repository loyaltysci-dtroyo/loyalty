import { Injectable } from '@angular/core';
import {Rol, Usuario} from './index';
@Injectable()
export class UsuarioRol {
    public fechaCreacion:Date;
    public usuarioCreacion:string;
    constructor(public rol: Rol, public usuario: Usuario) {
        
     }
     
}