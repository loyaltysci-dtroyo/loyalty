import { Injectable } from '@angular/core';
import { Miembro, Ubicacion} from './index';

@Injectable()
export class TransaccionCompra{

    public idCompra: string; // Identificación de la compra
    public idTransaccion: string; //Identificación de la transacción
    public fecha: Date; //Fecha en la que se realizo la compra
    public subtotal: number; 
    public impuesto: number;
    public total: number;
    public miembro: Miembro; //Miembro que realizo la compra
    public ubicacion: Ubicacion; //Ubicación en la que se realizo la compra
    public compraPremio: any;
    public compraProductos: any[];

    constructor(){}
}