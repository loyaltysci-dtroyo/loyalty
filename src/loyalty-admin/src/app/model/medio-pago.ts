import { Injectable } from '@angular/core';
import { Configuracion } from './index';
@Injectable()

/*Entidad de medios de pago*/
export class MedioPago{

    public idTipo: string; //Identificación autogenerada por el sistema.
    public nombre: string; //Nombre del medio de pago.
    public imagen: string; //Imagen representativa del medio.
    public idConfiguracion: string; //Identificacion de la configuración general.
    public fechaCreacion: Date; //Usuario que creo el medio de pago.
    public usuarioCreacion: string; //// Fecha de creación del usuario.
    
    constructor(){}
}
