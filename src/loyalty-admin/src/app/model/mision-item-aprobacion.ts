import { Injectable } from '@angular/core';
import { Miembro } from './';
@Injectable()
export class MisionItemAprobacion {

    public miembro: Miembro;
    public fecha: Date;
    public respuestaEncuesta: [{
        detallePregunta: {
            idPregunta: string;
            indTipoPregunta: string;
            pregunta: string;
            respuestas: string;
            indTipoRespuesta: string;
            imagen: string;
            indComentario: false;
            comentarios: string;
            indRespuestaCorrecta: string;
            respuestasCorrectas: string;
            fechaCreacion: Date;
            usuarioCreacion: string;
            fechaModificacion: Date;
            usuarioModificacion: string;
            numVersion: 0
        };
        respuesta: string;
        comentarios: string
    }];
    respuestaActualizarPerfil: [{
        detalleAtributo: {
            idAtributo: string;
            indRequerido: false;
            indAtributo: string;
            fechaCreacion: Date;
            usuarioCreacion: string;
            fechaModificacion: Date;
            usuarioModificacion: string;
            numVersion: 0;
            idAtributoDinamico: {
                idAtributo: string;
                nombre: string;
                descripcion: string;
                indTipoDato: string;
                valorDefecto: string;
                fechaCreacion: Date;
                usuarioCreacion: string;
                fechaModificacion: Date;
                usuarioModificacion: string;
                indEstado: string;
                indVisible: false;
                indRequerido: false;
                numVersion: 0;
                valorAtributoMiembro: string
            }
        };
        respuesta: string
    }];
    respuestaSubirContenido: {
        detalleSubirContenido: {
            idMision: string;
            indTipo: string;
            texto: string;
            fechaCreacion: Date;
            fechaModificacion: Date;
            usuarioCreacion: string;
            usuarioModificacion: string;
            numVersion: 0
        }
    };
    respuestaRedSocial: {
        detalleRedSocial: {
            idMision: string;
            indTipo: string;
            mensaje: string;
            tituloUrl: string;
            urlObjectivo: string;
            urlImagen: string;
            fechaCreacion: Date;
            fechaModificacion: Date;
            usuarioCreacion: string;
            usuarioModificacion: string;
            numVersion: 0
        }
    };
    respuestaVerContenido: {
        detalleVerContenido: {
            idMision: string;
            indTipo: string;
            url: string;
            texto: string;
            fechaCreacion: Date;
            fechaModificacion: Date;
            usuarioCreacion: string;
            usuarioModificacion: string;
            numVersion: 0
        }
    }

    constructor() { }
}