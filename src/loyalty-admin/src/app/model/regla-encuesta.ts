import { Injectable } from '@angular/core';
import { MisionEncuestaPregunta } from './';
@Injectable()

export class ReglaEncuesta {

    public idRegla: string;
    public indOperador: string;
    public valorComparacion: string;
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public pregunta: MisionEncuestaPregunta;
    public idLista: string;

    constructor() { }
}