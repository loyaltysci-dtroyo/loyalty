import { Injectable } from '@angular/core';
import { Insignia } from './index';
@Injectable()

export class InsigniaNivel {

    //atributos
    public idNivel: string;
    public nombreNivel: string; //max 50
    public descripcion: string; //max 100
    public nombreInterno: string;// max 50
    public imagen: string;
    public idInsignia: Insignia;
    public usuarioCreacion: string;
    public fechaCreacion: Date;
    public usuarioModificacion: string;
    public fechaModificacion: Date;//opt
    public numVersion: number;//opt
    
    constructor() { }
}