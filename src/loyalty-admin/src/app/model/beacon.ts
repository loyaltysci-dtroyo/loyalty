import { NotificacionesDetalleMiembrosComponent } from '../component/notificaciones';
import { Injectable } from '@angular/core';

@Injectable()
export class Beacon {
   
   public nombre : string;
   public UUID : string;
   public minor : string;
   public major : string;

   constructor(){
       
   }
   public getMajor() : string {
       return this.major;
   }
   public setMajor(v : string) {
       this.major = v;
   }
   
   public getMinor() : string {
       return this.minor;
   }
   public setMinor(v : string) {
       this.minor = v;
   }
   
   public getUUID() : string {
       return this.UUID;
   }
   public setUUID(v : string) {
       this.UUID = v;
   }
   
   public getNombre() : string {
       return this.nombre;
   }
   public setNombre(v : string) {
       this.nombre = v;
   }
   
    

}