import { Injectable } from '@angular/core';
@Injectable()

/*Esta clase representa la tabla de grupos niveles, que permitira la agrupación de los niveles*/
export class GrupoNivel{

    //atributos
    //public fechaCreacion: Date;
    //public usuarioCreacion: string;
    public idGrupoNivel: string;
    public nombre: string;

    constructor(){}
}