import { CategoriaPromocion, PromocionListaMiembro, PromocionListaSegmento, PromocionListaUbicacion } from './index'; 
import { Miembro, Segmento, Ubicacion } from './index'; 

import { Injectable } from '@angular/core'; 

/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Clase(service) encargada de representar una entidad que representa una promocion en el sistema
 */
@Injectable()
export class Promocion {
    public idPromocion: string; // id unico para la promocion
    public nombre: string;
    public nombreInterno: string; // nomre interno de caracter unico para la promocion
    public descripcion: string;
    public tags: string; // los tags que se decidan agregar a la promo
    public indTipoAccion: string; // indica el tipo de accion que se tomara, url o codigo QR
    public urlOrCodigo: string; // el valor del codigo
    public indEstado: string; // estado de la entidad, que pued eser publicado, borrador o archivado
    public fechaCreacion: Date; // fecha de creacion de la entidad
    public fechaPublicacion: Date; // fecha de publicacion de la promocion en caso de que esta sea publicada
    public fechaArchivado: Date; // fecha en que se archivo la promocion en caso de que este archivada
    public usuarioCreacion: string; // usuario que creo la promocion 
    public fechaModificacion: string; // fecha en que se mmodfco la promocion por ultima vez
    public usuarioModificacion: string; // usuario que modifico la entidad por ultia vez
    public numVersion: number; // numero de version de la entidad
    public promocionListaMiembroList: Miembro[]; // lista de miembros elegibles para la promcion
    public promocionListaUbicacionList: Ubicacion[]; // lista de ubicaciones elegibles para la promcion
    public categoriaPromocionList: CategoriaPromocion[]; // lista de categorias asociadas para la promcion
    public promocionListaSegmentoList: Segmento[]; // lista de segmentos elegibles para la promocion
    public indRespuesta: boolean; // indicador de sirecibe o no respuesta
    public cantTotal: number; // limite de respuestas totales 
    public indIntervaloTotal: string; // intervalo de respuestas global
    public cantTotalMiembro: number; // limite de respuestas por miembro
    public indIntervaloMiembro: string; // intervalo de respuestas permitido por miembro en general
    public indIntervaloRespuesta: string; // frecuencia permitida entre respuestas, dentro del limite de frecuencias
    public encabezadoArte: string; // encabezado del arte que se desplegara en la app movil
    public subencabezadoArte: string; // sub encabezado del arte para la app
    public detalleArte: string; // detalle del arte para la app
    public imagenArte: string; // imagen en base 64
    public indCalendarizacion: string; // indica el tipo de calendarizacion, permanente, calendarizada
    public fechaInicio: Date; //  para la calendarizacion
    public fechaFin: Date; // para la calendarizacion de la promocion
    public indDiaRecurrencia: string; // para la calendarizacion de la promocion, en caso de ser recurrente indica los dias de recurrencia de la misma
    public indSemanaRecurrencia: number; // para la calendarzacion de la promocion, indica las semanas de recurrencia de la misma
    constructor() {
        
    }
}
