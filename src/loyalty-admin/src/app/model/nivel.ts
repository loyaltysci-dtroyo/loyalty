import { Injectable } from '@angular/core';
import { Metrica, GrupoNivel } from './index';

@Injectable()

/**
 * Autor: Jaylin Centeno
 * Esta entidad representa los niveles de las métricas, están agrupados
 */
export class Nivel{

    public idNivel: string;//Identificación autogenerada por el sistema, no llenar.
    public nombre: string;//// Nombre con el que se presenta, max 50 
    public descripcion: string;// Descripción del nivel
    public metricaInicial: number; // Valor inicial de la métrica 
    public fechaCreacion: Date; // Fecha en la que se creo el nivel, no llenar.
    public usuarioCreacion: string; // Usuario que creo el nivel, no llenar. 
    public fechaModificacion: Date; // Fecha de la última modificación, no llenar.
    public usuarioModificacion: string;// Último usuario en modificar el nivel, no llenar.
    public numVersion: number; // Se maneja el número de version del premio, no llenar.
    public grupoNiveles: GrupoNivel; //Grupo al que pertence el nivel
    public multiplicador: number; // Valor multiplicador para cada nivel 

    constructor(){
        this.grupoNiveles = new GrupoNivel();
    }

}