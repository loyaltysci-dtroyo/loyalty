import { Injectable } from '@angular/core';
import { Ubicacion, Proveedor } from './index';

@Injectable()

/* Esta clase representa la entidad de documento, utilizado para el manejo de inventarios de premio */
export class DocumentoInventario {

    //Atributos
    public numDocumento: string; //Identificación autogenerada para el juego, no se llena.
    public fecha: Date; //Fecha de creación del documento
    public tipo: string; //Indicador del tipo de documento, C=compra R=redencion A=ajuste más M=ajuste menos I=inter ubicación
    public ubicacionOrigen: Ubicacion; //Representa la ubicación de origen, es donde ocurre la acción para la creación del documento
    public ubicacionDestino: Ubicacion; //Representa la ubicaciónpublic usuarioCreacion: string; // Usuario que creo el detalle, no llenar.
    public fechaCreacion: Date; // Fecha en la que se creo el documento, no llenar.
    public usuarioCreacion: string; // Usuario que creo el documento, no llenar.
    public usuarioModificacion: string; // Último usuario que modifico el documento, no llenar.
    public fechaModificacion: Date; // Fecha de la última modificación, no llenar.
    public numVersion: number; // Se maneja el número de version del inventario, no llenar.
    public indCerrado: boolean; // Permite indicar si el documento se ha cerrado.
    public descripcion: string; //Dsscripción del documento, max 200, opcional
    public codigoDocumento: string; //Este código representa el número de la factura, max 40
    public codProveedor: Proveedor;//Codigo del proveedor, usado para documento tipo compra
    constructor(){}
} 