import { Injectable } from '@angular/core';
import { DocumentoInventario, Premio, Miembro } from './index';

@Injectable()

/* Esta clase representa la entidad de detalle de documento, utilizado para el manejo de inventarios de premio */
export class DetalleDocumento {

    //Atributos
    public numDetalleDocumento: string; //Identificador autogenerado para el detalle del documento
    public cantidad: number; //Representa la cantidad de premios que va en el detalle
    public status: string; //Indicador del estado del premio en caso de redención, E=entregado P=pendiente, opcional
    public usuarioCreacion: string; // Usuario que creo el detalle, no llenar.
    public fechaCreacion: Date; // Fecha en la que se creo el detalle, no llenar.
    public usuarioModificacion: string; // Último usuario que modifico el detalle, no llenar.
    public fechaModificacion: Date; // Fecha de la última modificación, no llenar.
    public numVersion: number; // Se maneja el número de version del premio, no llenar.
    public precio: number; //Representa el precio del premio
    public promedio: number; //Promedio del precio de acuerdo a la cantidad y el precio de los premios
    public numDocumento: DocumentoInventario; //Representa la ubicación de origen, es donde ocurre la acción para la creación del documentopublic numDocumento: string; //Identificador del documento al que pertenece el detalle
    public codPremio: Premio; //Representa la ubicación
    public idMiembro: Miembro; //Representa la identificación del miembro, se utiliza en caso de una redencion, opcional
    constructor(){}
} 