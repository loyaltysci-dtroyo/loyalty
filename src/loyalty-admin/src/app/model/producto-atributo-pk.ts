import { Injectable } from '@angular/core';
@Injectable()

/*Esta clase representa la clave primaria de ProductoAtributo*/
export class ProductoAtributoPk{

    //atributos
    public idProducto: string;
    public idAtributo: string;
        
    constructor(){}
}