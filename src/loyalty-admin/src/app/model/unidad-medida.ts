import { Injectable } from '@angular/core';

@Injectable()

/*
 *Autor: Jaylin Centeno
 *Esta entidad representa las unidades de medida para ser utilizada en premios
 */
export class UnidadMedida {

    public idUnidad: string;// Identificación autogenerada para la unidad
    public descripcion: string; // Descripción de la unidad de medida, max 100
    public fechaCrecacion: string; // Fecha en la que se creo la unidad
    public usuarioCreacion: string; // Usuario que creo la unidad
    public fechaModificacion: string; // Fecha en que la unidad se modifico
    public usuarioModificacion: string; //Usuario que modifico la unidad
    public numVersion: number; // Se maneja el número de version de la unidad

    constructor(){}

}