import { Injectable } from '@angular/core';

@Injectable()
/**
 * Flecha Roja Technologies
 * Fernando Aguilar Morales
 * clase encargada de representar los metadatos para la el reto de la mision de subir contenido
 * 
 */
export class MisionSubirContenido {
    public idMision:string;
    public indTipo:string;
    public texto:string;
    public fechaCreacion:Date;
    public fechaModificacion:Date;
    public usuarioCreacion:string;
    public usuarioModificacion:string;
    public numVersion:number;
    
    constructor() { }
}