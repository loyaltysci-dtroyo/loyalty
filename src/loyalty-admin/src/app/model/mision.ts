import { MisionSubirContenido } from './mision-subir-contenido';
import { MisionSocial } from './mision-social';
import { Injectable } from '@angular/core';
import { Metrica, Segmento, Ubicacion, Miembro, MisionVerContenido, MisionAtrPerfil, MisionEncuestaPregunta, MisionJuego, MisionRealidadAumentada} from './index';
/**Flecha Roja Technologies
 * Fernando Aguilar
 * Clase que representa un objeto mision, 
 */

const TIPOS_ENCUESTA = 'E';
const TIPOS_PERFIL = 'P';
const TIPOS_VER_CONTENIDO = 'V';
const TIPOS_SUBIR_CONTENIDO = 'S';
const TIPOS_RED_SOCIAL = 'R';
const TIPOS_JUEGO_PREMIO = 'J';

@Injectable()
export class Mision {
    public idMision: string;
    public indEstado: string;//borrador=C, publicado=A, archivado=B
    public nombre: string;
    public descripcion: string;
    public cantMetrica: number;//cantidad de metrica de recompensa
    public indAprovacion: string;//requiere aprobacion de un admin para otorgar la metrica de promo (???)
    public fechaCreacion: Date;//seteado en el api, ignorar
    public usuarioCreacion: string;//seteado en el api, ignorar
    public fechaModificacion: Date;//seteado en el api, ignorar
    public usuarioModificacion: string;//seteado en el api, ignorar
    public fechaPublicacion: Date;//seteado en el api
    public tags: string;//campo opional
    public indRespuesta: boolean;//determina si la mision permite o no respuestas
    public cantTotal: number;//para la configuracion de respuestas a nivel global, determina la cantidad de respuestas a nivel global
    public indTipoMision: string;// indica el tipo de mision, E=Encuesta P=Perfil, V=Ver contenido
    public numVersion: number;//numero de version de la tabla   
    public imagen: string;//imagen de la mision
    public encabezadoArte: string;
    public subEncabezadoArte: string;
    public textoArte: string;
    //limites
    public indIntervaloTotal: string;//para la configuracion de respuesta de promo, determina la frecuencia de respuesta global
    public cantTotalMiembro: number;//para la configuracion de respuestas, determina la cantidad de respuestas por miembro
    public indIntervaloMimebro: string;//intervalo de respuestas por miembro, determina la frecuencia de respuestas por miembro
    public indIntervalo: string;//indicador de frecuencia de respuestas, determina la frecuencia a pesar de los parametros definidos
    public cantIntervalo: number;
    //Calendarización
    public indCalendarizacion: string; //indicador de calendarizacion, P = permanente C = calendarizado 
    public fechaInicio: Date;
    public fechaFin: Date;
    public indDiaRecurrencia: string;
    public indSemanaRecurrencia: number;
    /**Tipos de misiones */
    public misionVerContenido: MisionVerContenido;//contiene info de contenido de mision en caso de ser una mision de contenido
    public misionEncuestaPreguntaList: MisionEncuestaPregunta[];
    public misionPerfilAtributoList: MisionAtrPerfil[];//lista de elementos de atribuos que deben actualizarse en caso de una mision de actualizacion de perfil
    public misionSocial: MisionSocial;
    public misionSubirContenido: MisionSubirContenido;
    public juego: MisionJuego;

    public misionRealidadAumentada:MisionRealidadAumentada;
    /**Elegibles de mision */
    public misionListaMiembroList: Miembro[];//lista de miembros elegibles de la mision
    public misionListaUbicacionList: Ubicacion[];//lista de ubicaciones elegibles de la mision
    public misionListaSegmentoList: Segmento[];//lista de segmentos elegibles de la mision
    public idMetrica: Metrica//indica la metrica para recompensar al usuariio


    constructor(
    ) {
        this.idMetrica = new Metrica();
    }
}  