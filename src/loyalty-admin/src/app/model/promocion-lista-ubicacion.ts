import {Promocion, Ubicacion} from './index';
import { Injectable } from '@angular/core';
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Clase(service) encargada de representar la lista de ubicaciones de una Promocion
 */
@Injectable()
export class PromocionListaUbicacion {
    public indTipo:boolean;
    public fechaCreacion:Date;
    public usuarioCreacion:string;
    public promocion:Promocion;
    public ubicacion:Ubicacion;
    constructor() { }
}
   
