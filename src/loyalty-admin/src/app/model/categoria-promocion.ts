import { Injectable } from '@angular/core';
import {Categoria, Promocion} from './index';
@Injectable()
export class CategoriaPromocion {
    public fechaCreacion:Date;
    public usuarioCreacion:string;
   
    constructor( public categoria:Categoria,
                 public promocion:Promocion) { 

    }
}