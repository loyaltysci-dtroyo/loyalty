import { Injectable } from '@angular/core';
import { Metrica, Ubicacion } from './index';
@Injectable()

/**Representa la entidad de configuración del sistema */
export class Configuracion {

    public id: string; //Identificación autogenerada por el sistema, no llenar.
    public nombreEmpresa: string; // Nombre que representa a la empresa, max 50
    public logoEmpresa: string; //Logo para mostrar en el sitio
    public idMetricaInicial: Metrica; //Métrica principal que se va utilizar en el sistema
    public idMetricaSecundaria: Metrica; //Métrica principal que se va utilizar en el sistema
    public msjEmailDesde: string; //Email que se va a utilizar para el envío de mensajes, max 100
    public usuarioModificacion: string; //Último usuario que modificó la configuración, no llenar.
    public fechaModificacion: Date; //Fecha de la última modificación, no llenar.
    public sitioWeb: string; //Guarda la dirección del sitio web de la empresa,max 300, opc
    public bonoEntrada: number; //Bono de entrada para el miembro que se registra, opc
    public porcentajeCompra: number; //De 1 a 100, opc
    public porcentajeImpuesto: number; //Es el impuesto que se le va a aplicar al miembro por el total de las compras, opc
    public cantDiasEntregaPedido: number; //Cantidad de días hábiles para las entregas, opc
    public politicasSeguridad: string;//Políticas de la empresa, max 500, opc
    public numVersion: number; //Se maneja el número de version del premio, no llenar
    public ubicacionPrincipal: Ubicacion; //Ubicación que se va a utilizar como central para redimir premios, opc
    public periodo: number; //Año, se modifica una sola vez
    public mes: number; //Mes, se modifica una sola vez
    public bonoReferencia: number; //Bono que se le da al miembro por referenciar a otro
    public integracionInventarioPremio: boolean; //Boolean
    public xFpSecuencia: number;
    public idProvider: string;
    public keyEvertec: string;
    public ivEvertec: string;
    public currencyCod: string;
    public usuarioEver: string;
    public passwordEver: string;
    public invoiceSeq: number;

    constructor() { }
}