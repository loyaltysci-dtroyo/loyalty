import { Injectable } from '@angular/core';
import { CategoriaProducto, Metrica, Ubicacion, Nivel } from './index';

@Injectable()

/*Esta clase representa los productos*/
export class Producto {

    //atributos
    public idProducto: string; //Identificación autogenerada para el producto, no llenar.
    public nombre: string; // Nombre con el que se presenta, max 100
    public nombreInterno: string; // Nombre interno para uso en el sistema, max 100
    public descripcion: string; // Descripción del producto, max 300, opt
    public sku: string; // Código para los productos, max 100, opt
    public precio: number; //Valor del producto en efectivo
    public montoEntrega: number; // Monto a cobrar por cada entrega, opt
    public tags: string;//Etiquetas, max 300, opt
    public indEstado: string; // Representa el estado del producto, B=borrador, P=publicado, A=archivado
    public efectividadIndCalendarizado: string; // Indica el tipo de efectividad, C=calendarizado, P=permanente
    public efectividadFechaInicio: Date; // Fecha en que inicia la aplicación de la efectividad, opt
    public efectividadFechaFin: Date; // Fecha en que termina la efectividad, opt
    public efectividadIndDias: string; // Maneja los días en que se repite la efectividad, 7 max, opt
    public efectividadIndSemana: number; // Permite saber las semanas en que se aplica la efectividad 
    public limiteCantTotal: number; // Indica la cantidad global de respuestas aceptadas, tiene que ver con la cantidad disponible, opt
    public limiteIndIntervaloTotal: string; // Indica el intervalo de respuesta global para el producto, opt
    public limiteCantTotalMiembro: number; // Cantidad total de respuestas por cliente, opt
    public limiteIndIntervaloMiembro: string; // Indica el intervalo de respuesta global para el miembro, opt
    public limiteIndIntervaloRespuesta: string; // Indica la frecuencia de los intervalos de respuesta para el miembro, si la cantTotalMiembro > 1, opt
    public fechaPublicacion: Date; // Fecha en la que se publico el producto, opt
    public fechaArchivado: Date; // Fecha en la que se archivo, opt, no llenar.
    public fechaCreacion: Date;  // Fecha en la que se creo el producto, no llenar.
    public usuarioCreacion: string;  // Usuario que creo el producto, no llenar.
    public fechaModificacion: Date;  // Fecha en la que se creo el producto, no llenar.
    public usuarioModificacion: string;  // Último usuario que modifico el producto, no llenar.
    public imagenArte: string; // Imagen que representa al producto, opt
    public encabezadoArte: string; // Representa el título del arte, opt 150
    public subencabezadoArte: string; // Representa el subtitulo, opt 300
    public detalleArte: string; // Descripción, opt 500
    public indImpuesto: boolean; // Permite saber si el producto esta excento de impuestos
    public limiteIndRespuesta: boolean; // Indica si el producto permite respuestas, opt
    public numVersion: number; // Se maneja el número de version del producto, no llenar.
    public limiteCantInterRespuesta: number; // Valor que indica el intervalo entre respuestas, opt
    public cantMinMetrica: number; // Cantidad mínima de métrica requerida para comprar el producto opt
    public idMetrica: Metrica; // Representa la métrica utilizada por el producto, opt
    public idNivelMinimo: Nivel; // Representa el nivel mínimo en el que el miembro debe estar para comprar el producto, opt
    public indEntrega: string; // Indica si el producto requiere entrega en una ubicación
    public idUbicacion: Ubicacion; // Representa la ubicación, en caso de ser necesaria para una entrega, opt
    public codProducto: string; //Representa el codigo interno del producto
    public saldoInicial: number; //Saldo inicial de productos
    public saldoDisponible: number; //Saldo disponible de productos
    constructor(){}
}