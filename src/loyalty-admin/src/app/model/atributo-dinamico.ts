import { Injectable } from '@angular/core';
import { Preferencia } from './index';

@Injectable()
 
/* Esta clase representa al atributos dinámico asociados a un miembro */
export class AtributoDinamico{

    //atributos
    public idAtributo: string; //Identificación autogenerada por el sistema, no llenar.
    public nombre: string; //Nombre del atributo dinámico, max 50 
    public descripcion: string; //Descripción del atributo, max 100
    public indTipoDato: string; //Indicador de tipo de dato, puede ser N=numerico T=texto F=fecha B=booleano
    public valorDefecto: string; //Valor por defecto que se le dará al atributo, max 50
    public fechaCreacion: Date; //Fecha en la que se creo el atributo, no se llena.
    public usuarioCreacion: string; //Usuario que creo el atributo, no llenar.
    public fechaModificacion: Date; //Fecha de la última modificación, no llenar.
    public usuarioModificacion: string; //Último usuario en modificar, no llenar.
    public indVisible: boolean; //Indicador de visibilidad, TRUE=visible para la app FALSE=invisible para la app  
    public indRequerido: boolean; //Indicador de requerido, TRUE = si FALSE = no
    public numVersion: number; //Se maneja el número de version del atributo, lo maneja el sistema.
    public indEstado: string; //Representa el estado del atributo, D=borrador, P=publicado, A=archivado
    public valorAtributoMiembro:string; //Valor ingredado por el miembro, max 50
    public nombreInterno: string; //Nombre interno para uso en el sistema

    constructor(){
        this.indRequerido = false;
        this.indVisible = false;
    }
}