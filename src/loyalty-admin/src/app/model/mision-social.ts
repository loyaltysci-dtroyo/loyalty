import { Injectable } from '@angular/core';
/**
 * Flecha Roja Technologies
 * Fernando Aguilar
 * Clase encargada de representar los metadatos necesarios para definir un reto social para Mision
 */
@Injectable()
export class MisionSocial {

    public idMision:string;
    public indTipo:string;
    public mensaje:string;
    public tituloUrl:string;
    public urlObjectivo:string;
    public urlImagen:string;
    public fechaCreacio:Date;
    public fechaModificacion:Date;
    public usuarioCreacion:string;
    public usuarioModificacion:string;
    public numVersion:number;

    constructor() { 
        this.indTipo="L";
        this.mensaje=this.tituloUrl=this.urlImagen=this.urlObjectivo="";
    }
}