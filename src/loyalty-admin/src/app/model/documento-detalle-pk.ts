import { Injectable } from '@angular/core';
import { Ubicacion } from './index';

@Injectable()

/* Esta clase representa la entidad de detalle de documento, utilizado para el manejo de inventarios de premio */
export class DetalleDocumentoPK {

    //Atributos
    public numDocumento: string; //Identificación del documento.
    public numDetalleDocumento: string; //Identificador autogenerado.

    constructor(){}
} 