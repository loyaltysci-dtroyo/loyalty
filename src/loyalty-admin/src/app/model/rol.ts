import {Injectable} from '@angular/core';
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Entidad(service) encargada de representar un rol en el sistema
 */
@Injectable()
export class Rol {
    public id:string;//id del rol, de caractr unico para el sistema
    public nombre:string;//nombre del rol
    public descripcion:string;//desscripcion del rol
    public containerId:string;//actualmente en desuso, indica el contenedor para el cual el rool es efectivo

    constructor(){

    }
}