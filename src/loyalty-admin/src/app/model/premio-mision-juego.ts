import { Injectable } from '@angular/core';
import { MisionJuego, Premio, Metrica }  from'./index';

@Injectable()

/*Esta clase representa los premios que serán usados en mision*/
export class PremioMisionJuego {

    public idMisionJuego: string; //Identificación autogenerada para el premio de mision juego, no se llena.
    public indTipoPremio: string; //Idenficador para definir el tipo de premio, P=premio, M=métrica, G=gracias.
    public probabilidad: number; //Probabilidad que tiene el miembro de ganar el premio
    public indRespuesta: boolean; //Indica si la misión acepta respuestas
    public cantInterTotal: number; // Indica la cantidad total de respuestas aceptadas, opt
    public indInterTotal: string; // Indica el intervalo de respuesta total, opt
    public cantInterMiembro: number; // Indica la cantidad total de respuestas aceptadas por miembro, opt
    public indInterMiembro: string; // Indica el intervalo de respuesta total para el miembro, opt
    public cantMetrica: number; // Cantidad de métrica que ganará el usuario, solo si el tipo de premio es métrica
    public usuarioCreacion: string; // Usuario que creo la misión juego, no llenar.
    public fechaCreacion: Date; // Fecha en la que se creo la misión juego, no llenar.
    public usuarioModificacion: string; // Último usuario que modifico la misión juego, no llenar.
    public fechaModificacion: Date; // Fecha de la última modificación, no llenar.
    public numVersion: number; // Número de version del juego, no llenar.
    public idJuego: MisionJuego; // Representa el juego al que pertenece el premio, no llenar.
    public idPremio: Premio; // Premio que gana el miembro, solo si el tipo de premio es premio.
    public metrica: Metrica; //Premio que gana el miembro, solo si el tipo de premio es métrica
    constructor() { }
}