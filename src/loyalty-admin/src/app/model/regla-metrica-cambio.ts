import { Injectable } from '@angular/core';
import { ListaReglas, Metrica } from './index';
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Clase(service) encargada de representar una regla de cambio de metrica de una promocion
 */
@Injectable()
export class ReglaMetricaCambio {

    public nombre:string;
    public idRegla:string;
    public indCambio:string;//1 caracter, ganar expirar o redimir
    public indTipo:string;//1 caracter, usado para almacenar el periodo, ej permanente,mes actual,etc
    public indValor:string;//dia mes o anno
    public fechaInicio:Date;
    public fechaFinal:Date;
    public fechaCreacion:Date;
    public usuarioCreacion:string;
    public idLista:ListaReglas;
    public idMetrica:Metrica;
    public indOperador:string;//operao
    public valorComparacion:string;

    constructor() {
        this.idMetrica=new Metrica();

    }
}