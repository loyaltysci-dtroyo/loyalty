import { Injectable } from '@angular/core';
import { GrupoNivel } from './index';
@Injectable()

/*Esta clase representa al objeto Métrica*/
export class Metrica{

    public idMetrica: string;
    public nombre: string;// max 50
    public medida: string;// max 10
    public nombreInterno: string;// max 50
    public indExpiracion: boolean;
    public diasVencimiento: number;
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public fechaModificacion: Date;
    public usuarioModificacion: string;
    public numVersion: number;
    public indEstado: string;
    public grupoNiveles: GrupoNivel;

    constructor(){
        this.diasVencimiento = 0;
        this.indExpiracion = false;
        this.grupoNiveles = new GrupoNivel();
    }
}