import { Injectable } from '@angular/core';
import { CategoriaPremio, Metrica, UnidadMedida } from './index';

@Injectable()

/**
 * Autor: Jaylin Centeno
 * Esta entidad representa los premios utilizados en el sistema
 */
export class Premio {

    public idPremio: string; //Identificación autogenerada por el sistema, no llenar.
    public nombre: string; // Nombre con el que se presenta, max 150
    public nombreInterno: string; // Nombre interno para uso en el sistema, max 150
    public indTipoPremio: string; // Indicador de tipo de premio, puede ser C=certificado, P=producto físico
    public descripcion: string; // Descripción del premio, max 500, opt
    public valorMoneda: number; // Valor del premio en métrica
    public valorEfectivo: number; //Valor del premio en efectivo, puede requerirse para redimir el premio, opt
    public trackingCode: string; //max 100, opt
    public indEstado: string; // Representa el estado del premio, B=borrador, P=publicado, A=archivado
    public fechaPublicacion: Date; // Fecha en la que se publico el premio, opt
    public fechaArchivado: Date;// Fecha en la que se archivo, opt, no llenar.
    public usuarioCreacion: string; // Usuario que creo el premio, no llenar.
    public fechaCreacion: Date; // Fecha en la que se creo el premio, no llenar.
    public usuarioModificacion: string; // Último usuario que modifico el premio, no llenar.
    public fechaModificacion: Date; // Fecha de la última modificación, no llenar.
    public idMetrica: Metrica; // Representa la métrica utilizada para la redención del premio
    public sku: string; // Código para los premios, max 100, opt
    public cantTotal: number; // Indica la cantidad global de respuestas aceptadas, tiene que ver con la cantidad de premios disponibles, opt
    public indIntervaloTotal: string; // Indica el intervalo de respuesta global para el premio, opt
    public cantIntervaloRespuesta:number; // Valor que indica el intervalo entre respuestas, opt
    public cantMinAcumulado: number; // Cantidad mínima de métrica requerida (acumulada) para redimir el premio, opt
    public cantTotalMiembro: number; // Cantidad total de respuestas por cliente, opt
    public indIntervaloMiembro: string; // Indica el intervalo de respuesta global para el miembro, opt
    public indIntervaloRespuesta: string; // Indica la frecuencia de los intervalos de respuesta para el miembro, si la cantTotalMiembro > 1, opt
    public imagenArte: string; // Imagen que representa el premio
    public encabezadoArte: string; // Representa el título del arte, opt 150
    public subencabezadoArte: string; // Representa el subtitulo, opt 300
    public detalleArte: string; // Descripción, opt 500
    public efectividadIndCalendarizado: string; // Indica el tipo de efectividad, C=calendarizado, P=permanente
    public efectividadFechaInicio: Date; // Fecha en que inicia la aplicación de la efectividad, opt
    public efectividadFechaFin: Date; // Fecha en que termina la efectividad, opt
    public efectividadIndDias: string; // Maneja los días en que se repite la efectividad, 7 max, opt
    public efectividadIndSemana: number; // Permite saber las semanas en que se aplica la efectividad 
    public indEnvio: boolean; // Indica si el premio requiere envio
    public numVersion: number; // Se maneja el número de version del premio, no llenar.
    public indRespuesta: boolean; // Indica si el premio permite respuestas, opt
    public unidadMedida: UnidadMedida; // Representa la unidad de medida que tendrá el premio, opc
    public precioPromedio: number; //Representa el precio promedio del premio
    public codPremio: string; //Es el código interno que será usado por los usuarios, max 40
    public totalExistencias: number; //Es la cantidad total de existencias del premio en la ubic. general y demás
    public cantDiasVencimiento: number;
    constructor(){}
}