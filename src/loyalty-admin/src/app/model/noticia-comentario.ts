import { Injectable } from '@angular/core';
import { Noticia } from './index';

@Injectable()

/**
 * Autor: Jaylin Centeno
 * Esta entidad representa los comentarios de las noticias 
 */
export class NoticiaComentario{

    public idComentario: string;//Identificación autogenerada por el sistema, no llenar.
    public contenido: string;// Contenido del comentario, max 300
    public fechaCreacion: number; // Fecha en la que se creo el comentario, no llenar.
    public fechaModificacion: number; // Fecha de la última modificación, no llenar.
    public indAutorAdmin: boolean; //Usado para saber si el autor fue un admin o no
    public idAutor: string; //Id del admin/miembro que creo/modifico la noticia
    public noticia: Noticia; //Noticia a la que pertenece
    public comentarioReferente: NoticiaComentario; //Permite saber si este comentario refiere a otro en la noticia
    public numVersion: number; // Se maneja el número de version de la noticia, no llenar.
    public cantComentarios: number; //Cantidad de comentarios que responden
    public nombreAutor: string; //Nombre del autor del comentario
    public avatarAutor: string; //Avatar del autor del comentario

    constructor(){}

}