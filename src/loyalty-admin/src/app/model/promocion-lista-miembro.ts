import {Promocion, Miembro} from './index';
import { Injectable } from '@angular/core';
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Clase(service) encargada de representar la lista de miembros elegibles de una Promocion
 */
@Injectable()
export class PromocionListaMiembro {
    public indTipo:boolean;//indicador de tipo incluir o excluir
    public fechaCreacion:Date;
    public usuarioCreacion:string;
    public miembro:Miembro;
    public promocion:Promocion;
    constructor() { }
}