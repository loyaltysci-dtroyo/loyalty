import {UsuarioRol, Rol } from './index';
import { Injectable } from '@angular/core';
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar
 * Entidad(service) encargada de representar un usuario en el sistema
 */
@Injectable()
export class Usuario {
    public  idUsuario:string;
    public  indEstado:string;//activo o inactivo
    public  nombrePublico:string;//noombre y apellidos
    public  avatar:string;//la imagen
    public  fechaCreacion:Date;
    public  usuarioCreacion:string;
    public  fechaModificacion:Date;
    public  usuarioModificacion:string;
    public  numVersion:number;
    public  contrasena:string;
    public  nombreUsuario:string;//nombre de usuario, de caracter unico
    public  correo:string;
    public  roles:Rol[];//lista de roles asignados al usuario

    constructor() {
        this.fechaModificacion= new Date();
        this.fechaCreacion= new Date();
    }

}
