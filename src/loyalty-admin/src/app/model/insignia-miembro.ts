import { Injectable} from '@angular/core';
import { Miembro, Insignia, InsigniaNivel } from './index';

@Injectable()

export class InsigniaMiembro {

    //atributos
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public insignias: Insignia;
    public miembro: Miembro;
    public idNivel: InsigniaNivel;

    constructor(){}
}