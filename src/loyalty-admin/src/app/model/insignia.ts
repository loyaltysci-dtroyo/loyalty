import { Injectable } from '@angular/core';

@Injectable()

export class Insignia {

    //atributos
    public idInsignia: string;
    public nombreInsignia: string; //max 50
    public descripcion: string; //max 100
    public tipo: string;
    public imagen: string;
    public estado: string;
    public fechaPublicacion: Date;//opt
    public fechaArchivado: Date;//opt
    public fechaCreacion: Date;
    public usuarioCreacion: string;
    public fechaModificacion: Date;
    public usuarioModificacion: string;
    public numVersion: number;
    public nombreInterno: string; //max 50

    constructor() { }
}