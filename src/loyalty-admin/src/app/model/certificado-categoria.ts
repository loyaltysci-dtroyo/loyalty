import { Injectable } from '@angular/core';
import { Metrica, UnidadMedida, CategoriaProducto } from './index';

@Injectable()

/**
 * Autor: Jaylin Centeno
 * Esta entidad representa los premios utilizados en el sistema
 */
export class CertificadoCategoria {
    public idCertificado: string; //Identificación autogenerada por el sistema, no llenar.
    public categoriaProducto: CategoriaProducto; // Categoria a la que pertenece el certificado
    public numCertificado: string; // Número de certificado
    public estado: string; // Indicador de tipo de certificado: D=Disponible o A=asignado
    public fechaExpiracion: Date; // Fecha de expiración del certificado (generada automática por el sistema), no llenar
    constructor(){}
}