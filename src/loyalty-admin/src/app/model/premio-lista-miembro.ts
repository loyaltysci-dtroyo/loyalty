import { Injectable } from '@angular/core';
import { Premio, Miembro } from './index';

@Injectable()

/*Esta clase representa la lista de los miembros elegibles para los premios*/
export class PremioListaMiembro {

    //atributos
    public usuarioCreacion: string;
    public fechaCreacion: Date;
    public miembro: Miembro;
    public premio: Premio;
    public indTipo: string;

    constructor(){}
}