import { Injectable } from '@angular/core';

@Injectable()
export class ReglaAvanzada {

    public idRegla:string;
    public indTipoRegla:string;
    public referencia:string;
    public indOperador:string;
    public valorComparacion0:string;
    public valorComparacion1:string;
    public fechaInicio:Date;
    public fechaFinal:Date;
    public fechaIndUltimo:string;
    public fechaCantidad:number;
    public usuarioCreacion:string;
    public fechaCreacion:Date;
    public nombre:string;
    public nombreReferencia:string;
   
    constructor() {
     Object.defineProperty(this.nombreReferencia, 'transient', {value: 'static', writable: true});
    }
}