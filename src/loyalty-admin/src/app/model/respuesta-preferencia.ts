import { Injectable } from '@angular/core';
import { Miembro, Preferencia } from './index';

@Injectable()
/**
 * Autor: Jaylin Centeno López 
 * Representa la respuesta a una preferencia por parte del miembro 
 * */
export class RespuestaPreferencia {

    public respuesta: string; //Respuesta de la preferencia
    public fechaCreacion: Date; //Fecha en la que se creo esta respuesta
    public miembro: Miembro; //Miembro que contesto
    public preferencia: Preferencia; //Preferencia que se respondio

}