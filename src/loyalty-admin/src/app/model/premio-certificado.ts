import { Injectable } from '@angular/core';
import { Premio, Miembro, CodigoCertificadoPk } from './index';

@Injectable()

/*Esta clase representa el certificado de premio*/
export class PremioCertificado {

    //atributos
    public codigoCertificadoPK: CodigoCertificadoPk;
    public indEstado: string; //Estados del certificado: D=disponible, O=ocupado, A=anulado.
    public premio: Premio; //Premio al que pertenece
    public fechaCreacion: Date; //Fecha en la que se creo el premio, no llenar.
    public usuarioCreacion: string; //Usuario que creo el certificado, no llenar.
    public nombreMiembro: string; //Miembro que tiene asignado el certificado
    public fechaOtorgue: Date; //Fecha en la que se otorgo
    public idMiembro: string; //Id del miembro al que se le entrego
    public indEstadoPremioMiembro: string; // Indicador del estado del premio cuando es ocupado por un miembro (redimido sin retirar, redimido retirado)
    constructor(){}
}