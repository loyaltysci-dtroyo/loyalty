import { Injectable } from '@angular/core';

@Injectable()

/* Esta clase representa el detalle de la respuesta, preferencia y miembro*/
export class DetalleRespuestaMiembro{

    public respuesta: string;
    public idPreferencia: string;
    public pregunta: string;

    constructor(){}
}