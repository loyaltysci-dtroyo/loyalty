import { Component, Injectable } from '@angular/core';
import { Message } from 'primeng/primeng';
@Injectable()
/**
 * Flecha Roja Technologies
 * Autor:Fernando Aguilar 
 * 
 * Servicio de mensajes
 * Despliega mensajes pero aun mas importante se encarga que estos mensajes persistan 
 * a pesar de que el usuario cambie de Component (debido a que el service es consumido por el
 * Application Component), de otra manera se cortaría el mensaje al cambiar de componente
 * (por ejemplo en redirecciones de tipo insertar entidad -> lista de entidad)
 */
export class MessagesService {

    public msgs: Message[];
    public life: number;
    public sticky = "false";

    constructor() {
        this.msgs = [];
        this.life = 3000;
    }

    showMessage(msg: Message) {
        this.msgs.push(msg);
        if(this.msgs.length>=3){
            this.msgs=this.msgs.slice(this.msgs.length-1,1);
        }   
        this.msgs = [...this.msgs]
        
        // setTimeout(()=>{ this.msgs.msgs=this.msgs.msgs.slice(this.msgs.msgs.length-1,1);}, 3000);
 //()=>{console.log("timer");this.msgs=this.msgs.splice(this.msgs.length-1,1);
   
       
    }
    /**
     * Muestra un growl de info
     */
    mostrarInfo(header: string, body: string) {
         if(this.msgs.length>=3){
            this.msgs.slice(this.msgs.length,1)
        }
        this.msgs.push({ severity: 'info', summary: header, detail: body });
    }
    /**
     * Muestra un growl de error 
     */
    mostrarError(header: string, body: string) {
         if(this.msgs.length>=3){
            this.msgs.slice(this.msgs.length,1)
        }
        this.msgs.push({ severity: 'error', summary: header, detail: body });
    }
    /**
     * Muestra un growl de warning
     */
    mostrarWarning(header: string, body: string) {
         if(this.msgs.length>=3){
            this.msgs.slice(this.msgs.length,1)
        }
        this.msgs.push({ severity: 'warn', summary: header, detail: body });
    }
    /**
     * Muestra un growl de success
     */
    mostrarSuccess(header: string, body: string) {
         if(this.msgs.length>=3){
            this.msgs.slice(this.msgs.length,1)
        }
        this.msgs.push({ severity: 'success', summary: header, detail: body });
    }
    /**
        * Limpia los mensajes existentes
        */
    clearMessages() {
        this.msgs = []
    }

}