/**
 * Flecha Roja Technologies
 * Ultima edicion: 01-dic-2017
 * Autor: Fernando Aguilar (fernando.aguilar@flecharoja.com) / Jaylin Centeno López
 * Descripcion: Mapeo de las rutas de la aplicacion, estas rutas son usadas en el resto de componentes y servicios 
 * para diversas razones como redirecciones, permisos e identificacion de ruta actual, si alguna ruta 
 * cambia en la aplicacion considere como necesario actualizar este archivo
 */


//USUARIOS
export const RT_USUARIOS = "/usuarios/";
export const RT_USUARIOS_INSERTAR = "/usuarios/insertarUsuario";
export const RT_USUARIOS_LISTA_USUARIOS = "/usuarios/listaUsuarios";
export const RT_USUARIOS_LISTA_ROLES = "/usuarios/listaRoles";
export const RT_USUARIOS_DETALLE_USUARIO = "/usuarios/detalleUsuario";

export const RT_ROLES_INSERTAR = "/usuarios/insertarRoles";
export const RT_ROLES_LISTA = "/usuarios/listaRoles";
export const RT_ROLES_DETALLE = "/usuarios/detalleRol";

//MIEMBBROS
export const RT_MIEMBROS = "/miembros";
export const RT_MIEMBROS_INSERTAR = "/miembros/insertarMiembro";
export const RT_MIEMBROS_DETALLE = "/miembros/detalleMiembro/";
export const RT_MIEMBROS_DETALLE_PERFIL = "perfil";
export const RT_MIEMBROS_DETALLE_NIVEL = "niveles";
export const RT_MIEMBROS_DETALLE_GRUPO = "grupos";
export const RT_MIEMBROS_DETALLE_ATRIBUTO = "atributosDinamicos";
export const RT_MIEMBROS_DETALLE_INSIGNIA = "insignias";
export const RT_MIEMBROS_DETALLE_PREFERENCIA = "preferencias";
export const RT_MIEMBROS_DETALLE_SEGMENTO = "segmentos";
export const RT_MIEMBROS_DETALLE_AWARDS = "awards";
export const RT_MIEMBROS_DETALLE_PREMIOS = "premios";
export const RT_MIEMBROS_DETALLE_MISIONES = "misiones";
export const RT_MIEMBROS_DETALLE_PROMOCIONES = "promociones";
export const RT_MIEMBROS_DETALLE_NOTIFICACIONES = "notificaciones";
export const RT_MIEMBROS_DETALLE_METRICA = "metrica";
export const RT_MIEMBROS_DETALLE_REFERALS = "referals";
export const RT_MIEMBROS_DETALLE_TRANSACCION = "transacciones";
export const RT_MIEMBROS_DETALLE_CUENTAS = "cuentas";
export const RT_MIEMBROS_EDITAR = "/miembros/editarMiembro";
export const RT_MIEMBROS_LISTA = "/miembros/listaMiembros";
export const RT_MIEMBROS_PREFERENCIAS_DETALLE = "/miembros/detallePreferencias";
export const RT_MIEMBROS_PREFERENCIAS_LISTA = "/miembros/listaPreferencias";
export const RT_MIEMBROS_PREFERENCIAS_INSERTAR = "/miembros/insertarPreferencia";

//PREFERENCIAS DE MIEMBRO
export const RT_PREFERENCIAS_LISTA_PREFERENCIAS = "/miembros/listaPreferencias";
export const RT_PREFERENCIAS_DETALLE_PREFERENCIA = "/miembros/detallePreferencias";
export const RT_PREFERENCIAS_INSERTAR_PREFERENCIA = "/miembros/insertarPreferencia";

//UBICACIONES

export const RT_UBICACIONES = "/ubicaciones";
export const RT_UBICACIONES_LISTA = "/ubicaciones/listaUbicaciones";
export const RT_UBICACIONES_INSERTAR = "/ubicaciones/insertarUbicacion";
export const RT_UBICACIONES_DETALLE = "/ubicaciones/detalleUbicacion/";
export const RT_UBICACIONES_DETALLE_INFORMACION = "informacion";
export const RT_UBICACIONES_DETALLE_EFECTIVIDAD = "efectividad";
export const RT_UBICACIONES_DETALLE_MAPA = "mapa";

//METRICAS
export const RT_METRICAS = "/metricas/";
export const RT_METRICAS_LISTA_METRICAS = "/metricas/metricaLista";
export const RT_METRICAS_INSERTAR = "/metricas/insertarMetrica";
export const RT_METRICAS_DETALLE = "/metricas/metricaDetalle";
export const RT_METRICAS_DETALLE_NIVEL = "/metricas/detalleNivel";
export const RT_METRICAS_GRUPONIVEL = "/metricas/gruposNiveles";
export const RT_METRICAS_GRUPODETALLE = "/metricas/detalle";

//LEADERBOARDS
export const RT_LEADERBOARDS = "/leaderboards/";
export const RT_LEADERBOARDS_LISTA = "/leaderboards/listaLeaderboards";
export const RT_LEADERBOARDS_DETALLE = "/leaderboards/detalleLeaderboard";
export const RT_LEADERBOARDS_INSERTAR = "/leaderboards/insertarLeaderboards";

//SEGMENTOS
export const RT_SEGMENTOS = "/segmentos";
export const RT_SEGMENTOS_INSERTAR = "/segmentos/insertarSegmento";
export const RT_SEGMENTOS_LISTA_SEGMENTOS = "/segmentos/listaSegmentos";
export const RT_SEGMENTOS_DETALLE_SEGMENTO = "/segmentos/detalleSegmento/";
export const RT_SEGMENTOS_DETALLE_ATRIBUTOS = "/atributos";
export const RT_SEGMENTOS_DETALLE_LISTA_REGLAS = "/listaReglas";
export const RT_SEGMENTOS_DETALLE_DETALLE_REGLAS = "/detalleReglas/";
export const RT_SEGMENTOS_DETALLE_MIEMBROS = "/miembros";
export const RT_SEGMENTOS_DETALLE_ELEGIBLES = "/elegibles";
export const RT_SEGMENTOS_DETALLE_IMPORTAR = "/import-export";

//PROMOCIONES
export const RT_PROMOCIONES = "/promociones/";
export const RT_PROMOCIONES_LISTA_PROMOCIONES = "/promociones/listaPromos";
export const RT_PROMOCIONES_ACTIVIDAD = "/promociones/actividad";
export const RT_PROMOCIONES_INSERTAR = "/promociones/insertarPromo";
export const RT_PROMOCIONES_DETALLE_PROMOCION = "/promociones/detallePromo/";
export const RT_PROMOCIONES_DETALLE_ALCANCE = "alcance";
export const RT_PROMOCIONES_DETALLE_ARTE = "arte";
export const RT_PROMOCIONES_DETALLE_CATEGORIAS = "categorias";
export const RT_PROMOCIONES_DETALLE_COMMON = "common";
export const RT_PROMOCIONES_DETALLE_ELEGIBLES = "elegibles";
export const RT_PROMOCIONES_DETALLE_ACTIVIDAD = "actividad";

//CATEGORIAS DE PROMO
export const RT_CATEGORIAS_INSERTAR = "/promociones/insertarCategoria";
export const RT_CATEGORIAS_DETALLE = "/promociones/detalleCategorias";
export const RT_PROMOCIONES_LISTA_CATEGORIAS = "/promociones/listaCategorias";

//MISIONES
export const RT_MISIONES = "/misiones/";
export const RT_MISIONES_LISTA = "/misiones/listaMisiones";
export const RT_MISIONES_INSERTAR = "/misiones/insertarMision";
export const RT_MISIONES_DETALLE = "/misiones/detalleMision";
export const RT_MISIONES_DETALLE_COMMON = "/common/";
export const RT_MISIONES_DETALLE_ELEGIBLES = "/elegibles/";
export const RT_MISIONES_DETALLE_RETO = "/reto/";
export const RT_MISIONES_DETALLE_CATEGORIAS = "/categorias/";
export const RT_MISIONES_DETALLE_ACTIVIDAD = "/actividades/";
export const RT_MISIONES_DETALLE_APROBACION = "/aprobacion/";

//JUEGOS
export const RT_JUEGOS = "/juegos/";
export const RT_JUEGO_MILLONARIO_LISTA = "/juegos/listaJuegosMillonario";
export const RT_JUEGO_MILLONARIO_DETALLE = "/juegos/detalleJuegoMillonario";
export const RT_JUEGO_MILLONARIO_DETALLE_PREGUNTA = "/juegos/detallePregunta";
export const RT_JUEGO_MILLONARIO_DETALLE_COMMON = "/common/";
export const RT_JUEGO_MILLONARIO_DETALLE_PREGUNTAS = "/preguntas/";
export const RT_JUEGO_MILLONARIO_DETALLE_INSERTAR_PREGUNTA = "/insertarPreguntaMillonario/";

//CATEGORIAS DE MISION
export const RT_CATEGORIAS_MISION_NSERTAR = "/misiones/insertarCategoria";
export const RT_CATEGORIAS_MISION_DETALLE = "/misiones/detalleCategorias";
export const RT_MISIONES_LISTA_CATEGORIAS = "/misiones/listaCategorias";


//PRODUTOS
export const RT_PRODUCTOS = "/productos/";
export const RT_PRODUCTOS_LISTA = "/productos/listaProductos";
export const RT_PRODUCTOS_INSERTAR = "/productos/insertarProducto";
export const RT_PRODUCTOS_DETALLE = "/productos/detalleProducto/";
export const RT_PRODUCTOS_DETALLE_INFORMACION = "informacion";
export const RT_PRODUCTOS_DETALLE_ELEGIBLES = "elegibles";
export const RT_PRODUCTOS_DETALLE_DISPLAY = "display";
export const RT_PRODUCTOS_DETALLE_LIMITE = "limite";
export const RT_PRODUCTOS_DETALLE_CALENDARIZACION = "calendarizacion";
export const RT_PRODUCTOS_DETALLE_CATEGORIA = "categorias";
export const RT_PRODUCTOS_DETALLE_ATRIBUTO = "atributos";
export const RT_PRODUCTOS_CATEGORIA_LISTA ="/productos/listaCategoriasProducto";
export const RT_PRODUCTOS_CATEGORIA_INSERTAR = "/productos/insertarCategProducto";
export const RT_PRODUCTOS_CATEGORIA_DETALLE ="/productos/detalleCategProducto/";
export const RT_PRODUCTOS_CATEGORIA_INFORMACION = "informacion";
export const RT_PRODUCTOS_CATEGORIA_SUBCATEGORIA = "subcategorias";
export const RT_PRODUCTOS_CATEGORIA_CERTIFICADOS_CATEGORIA_PRODUCTO = "certificadosCategoriaProducto"
export const RT_PRODUCTOS_ATRIBUTO = "/productos/atributos";
export const RT_PRODUCTOS_ATRIBUTO_INSERTAR = "/productos/insertarAtributo";

//RECOMPENSAS
export const RT_RECOMPENSAS = "/recompensas/";

//CAMPANAS 
export const RT_CAMPANAS = "/notificaciones";
export const RT_CAMPANAS_INSERTAR = "/notificaciones/insertarNotificaciones";
export const RT_CAMPANAS_LISTA = "/notificaciones/listaNotificaciones";
export const RT_CAMPANAS_DETALLE = "/notificaciones/detalleNotificacion";
export const RT_CAMPANAS_DETALLE_COMMON = "/common";
export const RT_CAMPANAS_DETALLE_ARTE = "/arte";
export const RT_CAMPANAS_DETALLE_ELEGIBLES = "/elegibles";

//GRUPOS
export const RT_GRUPOS = "/grupos";
export const RT_GRUPOS_INSERTAR = "/grupos/insertar";
export const RT_GRUPOS_DETALLE = "/grupos/detalleGrupo/";
export const RT_GRUPOS_DETALLE_GENERAL = "general";
export const RT_GRUPOS_DETALLE_MIEMBROS = "miembros";

//ATRIBUTOS
export const RT_ATRIBUTOS = "/atributos";

//INSIGNIAS
export const RT_INSIGNIAS = "/insignias";
export const RT_INSIGNIAS_LISTA = "/insignias/listaInsignias";
export const RT_INSIGNIAS_INSERTAR = "/insignias/insertarInsignia";
export const RT_INSIGNIAS_DETALLE = "/insignias/detalleInsignia/";
export const RT_INSIGNIAS_NIVEL_LISTA = "listaNivel";
export const RT_INSIGNIAS_MIEMBRO = "asignarMiembro";
export const RT_INSIGNIAS_INFORMACION = "informacion";

//ATRIBUTOS
export const RT_ATRIBUTOSDINAMICOS = "/atributosDinamicos";
export const RT_ATRIBUTOSDINAMICOS_LISTA = "/atributosDinamicos/listaAtributos";
export const RT_ATRIBUTOSDINAMICOS_INSERTAR = "/atributosDinamicos/insertar";
export const RT_ATRIBUTOSDINAMICOS_DETALLE = "/atributosDinamicos/detalleAtributo";

//CONFIGURACION DEL SISTEMA
export const RT_CONFIGURACION = "/configuracionSistema/";
export const RT_CONFIGURACION_GENERAL = "general";
export const RT_CONFIGURACION_MEDIOS = "medios";
export const RT_CONFIGURACION_POLITICA = "politica";
export const RT_CONFIGURACION_PARAMS = "parametros";
export const RT_CONFIGURACION_EVERTEC = "evertec";

//PREMIOS
export const RT_PREMIOS = "/premios";
export const RT_PREMIOS_INSERTAR = "/premios/insertarPremio";
export const RT_PREMIOS_DETALLE = "/premios/detallePremio/";
export const RT_PREMIOS_DETALLE_REQUERIDO = "requerido";
export const RT_PREMIOS_DETALLE_DISPLAY = "display";
export const RT_PREMIOS_DETALLE_ELEGIBLES = "elegibles";
export const RT_PREMIOS_DETALLE_REDIMIDO = "redimido";
export const RT_PREMIOS_DETALLE_LIMITE = "limite";
export const RT_PREMIOS_DETALLE_CATEGORIA = "categorias";
export const RT_PREMIOS_DETALLE_CERTIFICADO = "certificados";
export const RT_PREMIOS_CATEGORIA_LISTA = "/premios/listaCategoriasPremio";
export const RT_PREMIOS_CATEGORIA_INSERTAR = "premios/insertarCategoria";
export const RT_PREMIOS_CATEGORIA_DETALLE = "premios/detalleCategoria";
export const RT_PREMIOS_CATEGORIA_INFORMACION = "informacion"
export const RT_PREMIOS_CATEGORIA_ASINGAR = "asignar";

//TAREAS PROGRAMADAS
export const RT_TAREA_INSERTAR ="/workflows/insertarWorkflow";
export const RT_TAREA_DETALLE = "/workflows/detalleWorkflow";
export const RT_TAREA_LISTA = "/workflows/listaWorkflow";
export const RT_TAREA = "/workflows";

//INVENTARIO PREMIOS
export const RT_INVENTARIO = "/inventario";
export const RT_INVENTARIO_LISTA_DOCUMENTOS = "/listaDocumentos";
export const RT_INVENTARIO_DOCUMENTO = "/detalleDocumento";
export const RT_INVENTARIO_DETALLES = "/detalles";
export const RT_INVENTARIO_INSERTAR_DOCUMENTO = "/insertarDocumento";
export const RT_INVENTARIO_PROVEEDOR = "/proveedor";
export const RT_INVENTARIO_HISTORICO = "/historico";

//REPORTES
export const RT_REPORTES = "/reportes";

//BITACORAS
export const RT_BITACORA = "/bitacoras";
export const RT_BITACORA_API_ADMIN = "/apiAdministrativa";
export const RT_BITACORA_API_EXTERNAL = "/apiExternal";

//NOTICIAS
export const RT_NOTICIA = "/noticia";
export const RT_NOTICIA_LISTA = "/noticia/listaNoticia";
export const RT_NOTICIA_INSERTAR = "/noticia/insertarNoticia";
export const RT_NOTICIA_DETALLE = "/noticia/detalleNoticia";
export const RT_NOTICIA_DETALLE_GENERAL = "general";
export const RT_NOTICIA_DETALLE_AUDIENCIA = "audiencia";
export const RT_NOTICIA_CATEGORIA = "/noticia/categoriaNoticia";
export const RT_NOTICIA_CATEGORIA_LISTA = "/noticia/listaCategoriasNoticia";
export const RT_NOTICIA_CATEGORIA_INSERTAR = "noticia/insertarCategoriaNoticia";
export const RT_NOTICIA_CATEGORIA_ASINGAR = "asignarCategoriaNoticia";
export const RT_NOTICIA_CATEGORIA_DETALLE = "noticia/detalleCategoriaNoticia";



