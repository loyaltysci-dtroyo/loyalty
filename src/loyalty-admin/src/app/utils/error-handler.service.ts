import { Response } from '@angular/http';
import { Injectable, ErrorHandler } from '@angular/core';
@Injectable()
/**
 * Flecha Roja Technologies 30-set-2016
 * Fernando Aguilar
 * Service encargado de retornar mensajes de error a partir de 
 * responses con errores (el campo de error en los observables)
 */
export class ErrorHandlerService implements ErrorHandler {

    constructor() {

    }


    handleError(error: any): void {
        console.log(error);
    }
    /*
   * Muestra un mensaje de error al usuario con los datos de la transaccion
   * Recibe: una instancia de request con la informacion de error
   * Retorna: un observable que proporciona información sobre la transaccion
   */
    static manejaErrorGrowl(error: Response) {
        try {
            if (error instanceof ProgressEvent) {
                return { severity: 'error', summary: "Error de conexión", detail: "Verifica tu conectividad a la red e intentalo de nuevo", life: "7000" };
            }
            let err = error.json();
            if (err) {
                return { severity: 'error', summary: "Error", detail: err.message, life: "5000" }
            } else {
                return { severity: 'error', summary: "Error", detail: "Error desconocido", life: "5000" }
            }
        } catch (e) {
            return { severity: 'error', summary: "Error", detail: "Ha ocurrido un error al procesar la transacción", life: "7000" }
        }
    }

}