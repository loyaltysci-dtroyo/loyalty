import { I18nService } from '../service';
import * as Routes from './rutas';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuItem } from 'primeng/primeng';
import { Router, ActivatedRoute, Event, NavigationEnd, NavigationError } from '@angular/router';

@Component({
    selector: 'breadcrumb-component',
    template: `
<p-breadcrumb [model]="items"></p-breadcrumb>`
})
/**
 * Flecha Roja Technologies 14-oct-2016
 * Autor: Fernando Aguilar
 * Componente encargado de renderizar un breadcrumb que cambia dinámicamente 
 * basandose en el estado del router
 */
export class BreadcrumbComponent implements OnInit, OnDestroy {

    public items: MenuItem[];
    public nombres: Map<string, string>;

    constructor(private i18nService:I18nService,public router: Router, public activatedRoute: ActivatedRoute) {

        this.nombres = new Map<string, string>();
    }
    /**
     * Construye una ruta a partir de los nombres de componente previamente establecidos en
     * el map
     */
      ngOnInit() {
          this.setLabels();
        this.router.routerState.snapshot.url;
        /**Se subscribe a los eventos del router por que de otra manera (ejemplo el router snapshot) no habria forma
         * de detectar cuando una ruta cambia
         */
        this.router.events.subscribe(
            (event: Event) => {
                /**Si el tipo de evento es un final  de navegacion */
                if (event instanceof NavigationEnd) {
                    /**Tomamos el url despues de que se aplican los redirects */
                    let URL = event.urlAfterRedirects;
                    this.items = [];
                    // let path = "";
                    //console.log("URL: ", URL);
                    /**Se secciona el url actual para determinar los fragmentos de la ubicacion */
                    URL.split('/').forEach((fragment: string) => {
                        // se prueba el fragmento para ver si coincide un un uuid
                        // let regexp = new RegExp('^[0-9a-f]{8}-?[0-9a-f]{4}-?[0-5][0-9a-f]{3}-?[089ab][0-9a-f]{3}-?[0-9a-f]{12}')
                        // console.log(fragment,  regexp.test(fragment));
                        /**Obtenemos el nombre del fragmento del mpapa de nombres  */
                        let label = this.nombres.get(fragment);
                        if (label != undefined && label != "") {
                            // path += "/" + fragment;//actualizo el path
                            if (this.items.length == 0) {
                                this.items.push({ label: label, routerLink: [fragment + ""] });
                            } else {
                                this.items.push({ label: label });
                            }
                        }
                    });
                }
            }, () => { }, () => { }
        );
    }

    setLabels(){
        //Mapeo de url y nombres de componentes
        this.nombres.set('dashboard', this.i18nService.getLabels("breadcrumbs")["dashboard"]);
        this.nombres.set('pruebasCompra', this.i18nService.getLabels("breadcrumbs")["compras"]);

        //USUARIOS
        this.nombres.set('usuarios', this.i18nService.getLabels("breadcrumbs")["usuarios"]);
        this.nombres.set('insertarUsuario', this.i18nService.getLabels("breadcrumbs")["insertarUsuario"]);
        this.nombres.set('listaUsuarios', this.i18nService.getLabels("breadcrumbs")["listaUsuarios"]);
        this.nombres.set('detalleUsuario', this.i18nService.getLabels("breadcrumbs")["detalleUsuario"]);
        this.nombres.set('detalleAtributos', this.i18nService.getLabels("breadcrumbs")["detalleAtributos"]);
        this.nombres.set('detalleRoles', this.i18nService.getLabels("breadcrumbs")["detalleRoles"]);
        this.nombres.set('insertarRoles', this.i18nService.getLabels("breadcrumbs")["insertarRoles"]);
        this.nombres.set('listaRoles', this.i18nService.getLabels("breadcrumbs")["listaRoles"]);
        this.nombres.set('detalleRol', this.i18nService.getLabels("breadcrumbs")["detalleRol"]);

        //MIEMBROS
        this.nombres.set('tipoMiembro', this.i18nService.getLabels("breadcrumbs")["tipoMiembro"]);
        this.nombres.set('miembros', this.i18nService.getLabels("breadcrumbs")["miembros"]);
        this.nombres.set('insertarMiembro', this.i18nService.getLabels("breadcrumbs")["insertarMiembro"]);
        this.nombres.set('detalleMiembro', this.i18nService.getLabels("breadcrumbs")["detalleMiembro"]);
        this.nombres.set('perfil', this.i18nService.getLabels("breadcrumbs")["perfil"]);
        this.nombres.set('niveles', this.i18nService.getLabels("breadcrumbs")["niveles"]);
        this.nombres.set('grupos', this.i18nService.getLabels("breadcrumbs")["grupos"]);
        this.nombres.set('atributosDinamicos', this.i18nService.getLabels("breadcrumbs")["atributosDinamicos"]);
        this.nombres.set('insignias', this.i18nService.getLabels("breadcrumbs")["insignias"]);
        this.nombres.set('preferencias', this.i18nService.getLabels("breadcrumbs")["preferencias"]);
        this.nombres.set('segmentos', this.i18nService.getLabels("breadcrumbs")["segmentos"]);
        this.nombres.set('redenciones', this.i18nService.getLabels("breadcrumbs")["redenciones"]);
        this.nombres.set('premios', this.i18nService.getLabels("breadcrumbs")["premios"]);
        this.nombres.set('retos', this.i18nService.getLabels("breadcrumbs")["retos"]);
        this.nombres.set('editarMiembro', this.i18nService.getLabels("breadcrumbs")["editarMiembro"]);
        this.nombres.set('listaMiembros', this.i18nService.getLabels("breadcrumbs")["listaMiembros"]);

        //UBICACIONES
        this.nombres.set('ubicaciones', this.i18nService.getLabels("breadcrumbs")["ubicaciones"]);
        this.nombres.set('listaUbicaciones', this.i18nService.getLabels("breadcrumbs")["listaUbicaciones"]);
        this.nombres.set('insertarUbicacion', this.i18nService.getLabels("breadcrumbs")["insertarUbicacion"]);
        this.nombres.set('detalleUbicacion', this.i18nService.getLabels("breadcrumbs")["detalleUbicacion"]);
        this.nombres.set('efectividad',this.i18nService.getLabels("breadcrumbs")["efectividad"]);
        this.nombres.set('mapa', this.i18nService.getLabels("breadcrumbs")["mapa"]);

        //MÉTRICAS
        this.nombres.set('metricas', this.i18nService.getLabels("breadcrumbs")["metricas"]);
        this.nombres.set('insertarMetrica', this.i18nService.getLabels("breadcrumbs")["insertarMetrica"]);
        this.nombres.set('metricaLista', this.i18nService.getLabels("breadcrumbs")["metricaLista"]);
        this.nombres.set('metricaDetalle', this.i18nService.getLabels("breadcrumbs")["metricaDetalle"]);
        this.nombres.set('detalleNivel', this.i18nService.getLabels("breadcrumbs")["detalleNivel"]);
        this.nombres.set('gruposNivel ', this.i18nService.getLabels("breadcrumbs")["gruposNivel"]);
        this.nombres.set('detalle', this.i18nService.getLabels("breadcrumbs")["detalle"]);

        //LEADERBOARDS
        this.nombres.set('leaderboards', this.i18nService.getLabels("breadcrumbs")["leaderboards"]);
        this.nombres.set('listaLeaderboards', this.i18nService.getLabels("breadcrumbs")["listaLeaderboards"]);
        this.nombres.set('detalleLeaderboard', this.i18nService.getLabels("breadcrumbs")["detalleLeaderboard"]);
        this.nombres.set('insertarLeaderboards', this.i18nService.getLabels("breadcrumbs")["insertarLeaderboards"]);

        //SEGMENTOS
        this.nombres.set('segmentos', this.i18nService.getLabels("breadcrumbs")["segmentos"]);
        this.nombres.set('insertarSegmento', this.i18nService.getLabels("breadcrumbs")["insertarSegmento"]);
        this.nombres.set('listaSegmentos', this.i18nService.getLabels("breadcrumbs")["listaSegmentos"]);
        this.nombres.set('detalleSegmento', this.i18nService.getLabels("breadcrumbs")["detalleSegmento"]);
        this.nombres.set('atributos', this.i18nService.getLabels("breadcrumbs")["atributos"]);
        this.nombres.set('reglas', this.i18nService.getLabels("breadcrumbs")["reglas"]);
        this.nombres.set('detalleReglas', this.i18nService.getLabels("breadcrumbs")["detalleReglas"]);
        this.nombres.set('miembros', this.i18nService.getLabels("breadcrumbs")["miembros"]);
        this.nombres.set('elegibles', this.i18nService.getLabels("breadcrumbs")["elegibles"]);

        //PROMOCIONES
        this.nombres.set('promociones', this.i18nService.getLabels("breadcrumbs")["promociones"]);
        this.nombres.set('listaPromos', this.i18nService.getLabels("breadcrumbs")["listaPromos"]);
        this.nombres.set('insertarPromo', this.i18nService.getLabels("breadcrumbs")["insertarPromo"]);
        this.nombres.set('insertarCategoria', this.i18nService.getLabels("breadcrumbs")["insertarCategoria"]);
        this.nombres.set('listaCategorias', this.i18nService.getLabels("breadcrumbs")["listaCategorias"]);
        this.nombres.set('detallePromo', this.i18nService.getLabels("breadcrumbs")["detallePromo"]);
        this.nombres.set('common', this.i18nService.getLabels("breadcrumbs")["common"]);
        this.nombres.set('arte', this.i18nService.getLabels("breadcrumbs")["arte"]);
        this.nombres.set('alcance', this.i18nService.getLabels("breadcrumbs")["alcance"]);
        this.nombres.set('elegibles', this.i18nService.getLabels("breadcrumbs")["elegibles"]);
        this.nombres.set('categorias', this.i18nService.getLabels("breadcrumbs")["categorias"]);
        this.nombres.set('detalleCategorias', this.i18nService.getLabels("breadcrumbs")["detalleCategorias"]);
        this.nombres.set('insertarCategoria', this.i18nService.getLabels("breadcrumbs")["insertarCategoria"]);

        //MISIONES
        this.nombres.set('misiones', this.i18nService.getLabels("breadcrumbs")["misiones"]);
        this.nombres.set('insertarMision', this.i18nService.getLabels("breadcrumbs")["insertarMision"]);
        this.nombres.set('detalleMision', this.i18nService.getLabels("breadcrumbs")["detalleMision"]);
        this.nombres.set('listaMisiones', this.i18nService.getLabels("breadcrumbs")["listaMisiones"]);

        //PRODUCTOS
        this.nombres.set('productos', this.i18nService.getLabels("breadcrumbs")["productos"]);
        this.nombres.set("detalleProducto",this.i18nService.getLabels("breadcrumbs")["detalleProducto"]);
        this.nombres.set('listaProductos', this.i18nService.getLabels("breadcrumbs")["listaProductos"]);
        this.nombres.set('insertarProducto', this.i18nService.getLabels("breadcrumbs")["insertarProducto"]);
        this.nombres.set('informacion', this.i18nService.getLabels("breadcrumbs")["informacion"]);
        this.nombres.set("elegibles",this.i18nService.getLabels("breadcrumbs")["elegibles"]);
        this.nombres.set("display",this.i18nService.getLabels("breadcrumbs")["display"]);
        this.nombres.set('listaCategoriasProducto', this.i18nService.getLabels("breadcrumbs")["listaCategoriasProducto"]);
        this.nombres.set('insertarCategProducto', this.i18nService.getLabels("breadcrumbs")["insertarCategProducto"]);
        this.nombres.set('informacion', this.i18nService.getLabels("breadcrumbs")["informacion"]);
        this.nombres.set('subcategorias', this.i18nService.getLabels("breadcrumbs")["subcategorias"]);
        this.nombres.set('calendarizacion', this.i18nService.getLabels("breadcrumbs")["calendarizacion"]);

        //RECOMPENSAS
        this.nombres.set('recompensas', this.i18nService.getLabels("breadcrumbs")["recompensas"]);

        //CAMPAÑAS
        this.nombres.set('notificaciones', this.i18nService.getLabels("breadcrumbs")["notificaciones"]);
        this.nombres.set('listaNotificaciones', this.i18nService.getLabels("breadcrumbs")["listaNotificaciones"]);
        this.nombres.set('insertarNotificaciones', this.i18nService.getLabels("breadcrumbs")["insertarNotificaciones"]);
        this.nombres.set('detalleCampana', this.i18nService.getLabels("breadcrumbs")["detalleCampana"]);

        //GRUPOS
        this.nombres.set('grupos', this.i18nService.getLabels("breadcrumbs")["grupos"]);
        this.nombres.set('insertar', this.i18nService.getLabels("breadcrumbs")["insertar"]);
        this.nombres.set('detalleGrupo', this.i18nService.getLabels("breadcrumbs")["detalleGrupo"]);
        this.nombres.set('miembros', this.i18nService.getLabels("breadcrumbs")["miembros"]);

        //PREFERENCIAS
        this.nombres.set("preferencias", this.i18nService.getLabels("breadcrumbs")["preferencias"]);
        this.nombres.set("listaPreferencias", this.i18nService.getLabels("breadcrumbs")["listaPreferencias"]);
        this.nombres.set("insertarPreferencia", this.i18nService.getLabels("breadcrumbs")["insertarPreferencia"]);
        this.nombres.set("detallePreferencias", this.i18nService.getLabels("breadcrumbs")["detallePreferencias"]);

        //INSIGNIAS
        this.nombres.set("insignias", this.i18nService.getLabels("breadcrumbs")["insignias"]);
        this.nombres.set("listaInsignias", this.i18nService.getLabels("breadcrumbs")["listaInsignias"]);  
        this.nombres.set("insertarInsignia", this.i18nService.getLabels("breadcrumbs")["insertarInsignia"]);
        this.nombres.set("detalleInsignia", this.i18nService.getLabels("breadcrumbs")["detalleInsignia"]);
        this.nombres.set("listaNivel", this.i18nService.getLabels("breadcrumbs")["listaNivel"]);
        this.nombres.set("asignarMiembro", this.i18nService.getLabels("breadcrumbs")["asignarMiembro"]);
        this.nombres.set("informacion", this.i18nService.getLabels("breadcrumbs")["informacion"]);

        //ATRIBUTOS DINAMICOS
        this.nombres.set("atributosDinamicos", this.i18nService.getLabels("breadcrumbs")["atributosDinamicos"]);
        this.nombres.set("listaAtributos", this.i18nService.getLabels("breadcrumbs")["listaAtributos"]);  
        this.nombres.set("insertar", this.i18nService.getLabels("breadcrumbs")["insertar"]);
        this.nombres.set("detalleAtributo", this.i18nService.getLabels("breadcrumbs")["detalleAtributo"]);

        //CONFIGURACIÓN DEL SISTEMA
        this.nombres.set("configuracionSistema",this.i18nService.getLabels("breadcrumbs")["configuracionSistema"])
        this.nombres.set("general", this.i18nService.getLabels("breadcrumbs")["general"]);
        this.nombres.set("medios", this.i18nService.getLabels("breadcrumbs")["medios"]);
        this.nombres.set("politica", this.i18nService.getLabels("breadcrumbs")["politica"]);
        this.nombres.set("parametros", this.i18nService.getLabels("breadcrumbs")["parametros"]);

        //PREMIOS
        this.nombres.set("premios", this.i18nService.getLabels("breadcrumbs")["premios"]);
        this.nombres.set("detallePremio", this.i18nService.getLabels("breadcrumbs")["detallePremio"]);
        this.nombres.set("requerido", this.i18nService.getLabels("breadcrumbs")["requerido"]);
        this.nombres.set("insertarPremio", this.i18nService.getLabels("breadcrumbs")["insertarPremio"]);
        this.nombres.set("display", this.i18nService.getLabels("breadcrumbs")["display"]);
        this.nombres.set("elegibles", this.i18nService.getLabels("breadcrumbs")["elegibles"]);
        this.nombres.set("redimido", this.i18nService.getLabels("breadcrumbs")["redimido"]);
        this.nombres.set("limite", this.i18nService.getLabels("breadcrumbs")["limite"]);
        this.nombres.set("categorias", this.i18nService.getLabels("breadcrumbs")["categorias"]);
        this.nombres.set("insertarCategoria", this.i18nService.getLabels("breadcrumbs")["insertarCategoria"]);
        this.nombres.set("listaCategoriasPremio", this.i18nService.getLabels("breadcrumbs")["listaCategoriasPremio"]);
        this.nombres.set("detalleCategoria", this.i18nService.getLabels("breadcrumbs")["detalleCategoria"]);
        this.nombres.set("asignar",this.i18nService.getLabels("breadcrumbs")["asignar"]);
        this.nombres.set("informacion", this.i18nService.getLabels("breadcrumbs")["informacion"]);
        this.nombres.set("certificados", this.i18nService.getLabels("breadcrumbs")["certificados"]);

        //WORKFLOWS
        this.nombres.set("workflows", this.i18nService.getLabels("breadcrumbs")["workflows"]);
        this.nombres.set("listaWorkflow", this.i18nService.getLabels("breadcrumbs")["listaWorkflow"]);
        this.nombres.set("insertarWorkflow", this.i18nService.getLabels("breadcrumbs")["insertarWorkflow"]);
        this.nombres.set("detalleWorkflow", this.i18nService.getLabels("breadcrumbs")["detalleWorkflow"]);

        //INVENTARIO PREMIO
        this.nombres.set("inventario", this.i18nService.getLabels("breadcrumbs")["inventario"]);
        this.nombres.set("listaDocumentos", this.i18nService.getLabels("breadcrumbs")["listaDocumentos"]);
        this.nombres.set("insertarDocumento", this.i18nService.getLabels("breadcrumbs")["insertarDocumento"]);
        this.nombres.set("infoDocumento", this.i18nService.getLabels("breadcrumbs")["infoDocumento"]);
        this.nombres.set("proveedor",this.i18nService.getLabels("breadcrumbs")["proveedor"])

    }

    ngOnDestroy() {

    }
}