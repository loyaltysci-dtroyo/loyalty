import { Component, Injectable } from '@angular/core';
@Injectable()

export class ValidationService {

    //atributos
    //Validacion del nombre interno de una entidad
    public elementoValido: boolean;

    constructor() { }

    getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
        let config = {
            'required': 'El campo es requerido.',
            'maxlength': `El campo no puede tener más de ${validatorValue.requiredLength} caracteres.`,
            'minlength': `El campo debe tener al menos ${validatorValue.requiredLength} caracteres.`,
            'pattern': 'Solo se aceptan numeros.'
        };

        return config[validatorName];
    }


    creditCardValidator(control) {
        // Visa, MasterCard, American Express, Diners Club, Discover, JCB
        if (control.value.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)) {
            return null;
        } else {
            return { 'invalidCreditCard': true };
        }
    }

    emailValidator(control) {
        // RFC 2822 compliant regex
        if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
        } else {
            return { 'invalidEmailAddress': true };
        }
    }

    passwordValidator(control) {
        // {6,100}           - Assert password is between 6 and 100 characters
        // (?=.*[0-9])       - Assert a string has at least one number
        if (control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/)) {
            return null;
        } else {
            return { 'invalidPassword': true };
        }
    }
}


