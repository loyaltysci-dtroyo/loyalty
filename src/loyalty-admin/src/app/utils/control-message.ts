import { Component, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ValidationService } from './validation.service';

@Component({
  selector: 'control-messages',
  template: `<small *ngIf="errorMessage != null"> {{ errorMessage }} </small>`
})
export class ControlMessagesComponent {

  @Input() control: FormControl;

  constructor( public validationService: ValidationService) { }

  get errorMessage() {
    for (let propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        /*if(this.tipoDato == "nombreInterno"){
          this.validarElemento();
        }*/
        return this.validationService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
      }
    }
    return null;
  }

}

