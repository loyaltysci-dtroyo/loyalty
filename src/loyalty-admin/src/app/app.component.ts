import { AppConfig } from './app.config';
import { I18nService } from './service/i18n.service';
import { ErrorHandlerService, MessagesService } from './utils/index';
import { AuthGuard } from './component/common/index';
import { Usuario, Configuracion } from './model/index';
import { KeycloakService, UsuarioService, ConfiguracionService } from './service/index';
import { Component, AfterViewInit, ElementRef, Renderer, OnInit, ViewChild } from '@angular/core';
import * as Routes from './utils/rutas';

enum MenuOrientation {
  STATIC,
  OVERLAY,
  HORIZONTAL
};
declare var jQuery: any;
@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  providers: [Usuario, UsuarioService, Configuracion, ConfiguracionService, MessagesService, AppConfig]

})
export class AppComponent implements OnInit, AfterViewInit {

  public nombre: string[];
  layoutCompact = true;
  layoutMode: MenuOrientation = MenuOrientation.STATIC;
  darkMenu = false;
  profileMode = 'inline';
  rotateMenuButton: boolean;
  topbarMenuActive: boolean;
  overlayMenuActive: boolean;
  staticMenuDesktopInactive: boolean;
  staticMenuMobileActive: boolean;
  layoutContainer: HTMLDivElement;
  layoutMenuScroller: HTMLDivElement;
  menuClick: boolean;
  topbarItemClick: boolean;
  activeTopbarItem: any;
  documentClickListener: Function;
  resetMenu: boolean;
  @ViewChild('layoutContainer') layourContainerViewChild: ElementRef;
  @ViewChild('layoutMenuScroller') layoutMenuScrollerViewChild: ElementRef;

  constructor(public el: ElementRef,
    public kc: KeycloakService,
    public usuario: Usuario,
    public usuarioService: UsuarioService,
    public msgs: MessagesService,
    public renderer: Renderer,
    public i18nService: I18nService,
   ) {

  }

  ngOnInit() {
    this.i18nService.initialize();
  
  }

  ngAfterViewInit() {
    this.layoutContainer = <HTMLDivElement>this.layourContainerViewChild.nativeElement;
    this.layoutMenuScroller = <HTMLDivElement>this.layoutMenuScrollerViewChild.nativeElement;

    // hides the horizontal submenus or top menu if outside is clicked
    this.documentClickListener = this.renderer.listenGlobal('body', 'click', (event) => {
      if (!this.topbarItemClick) {
        this.activeTopbarItem = null;
        this.topbarMenuActive = false;
      }

      if (!this.menuClick && this.isHorizontal()) {
        this.resetMenu = true;
      }
      
      this.topbarItemClick = false;
      this.menuClick = false;
    });

    setTimeout(() => {
      jQuery(this.layoutMenuScroller).nanoScroller({ flash: true });
    }, 10);
  }

  accountManagement() {
    window.location.href = this.kc.accountMAnagement();
  }
  logout() {
    this.kc.logout();
  }

  /** Muestra un error en caso de que los datos de usuario no se puedan recuperar del servidor */
  manejaError(error) {
    this.msgs.showMessage(ErrorHandlerService.manejaErrorGrowl(error));
  }

  /**
   * Cambia de temas y estilos visuales
   */
  changeTheme(event, theme) {
    let themeLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('theme-css');
    let layoutLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('layout-css');
    themeLink.href = 'resources/theme/theme-' + theme + '.css';
    layoutLink.href = 'resources/layout/css/layout-' + theme + '.css';
    event.preventDefault();
  }

  onMenuButtonClick(event) {
    this.rotateMenuButton = !this.rotateMenuButton;
    this.topbarMenuActive = false;

    if (this.layoutMode === MenuOrientation.OVERLAY) {
      this.overlayMenuActive = !this.overlayMenuActive;
    } else {
      if (this.isDesktop()) {
        this.staticMenuDesktopInactive = !this.staticMenuDesktopInactive;
      } else {
        this.staticMenuMobileActive = !this.staticMenuMobileActive;
      }
    }

    event.preventDefault();
  }

  onMenuClick($event) {
    this.menuClick = true;
    this.resetMenu = false;

    if (!this.isHorizontal()) {
      setTimeout(() => {
        jQuery(this.layoutMenuScroller).nanoScroller();
      }, 500);
    }
  }

  onTopbarMenuButtonClick(event) {
    this.topbarItemClick = true;
    this.topbarMenuActive = !this.topbarMenuActive;

    if (this.overlayMenuActive || this.staticMenuMobileActive) {
      this.rotateMenuButton = false;
      this.overlayMenuActive = false;
      this.staticMenuMobileActive = false;
    }

    event.preventDefault();
  }

  onTopbarItemClick(event, item) {
    this.topbarItemClick = true;

    if (this.activeTopbarItem === item) {
      this.activeTopbarItem = null;
    }
    else {
      this.activeTopbarItem = item;
    }

    event.preventDefault();
  }

  isTablet() {
    let width = window.innerWidth;
    return width <= 1024 && width > 640;
  }

  isDesktop() {
    return window.innerWidth > 1024;
  }

  isMobile() {
    return window.innerWidth <= 640;
  }

  isOverlay() {
    return this.layoutMode === MenuOrientation.OVERLAY;
  }

  isHorizontal() {
    return this.layoutMode === MenuOrientation.HORIZONTAL;
  }

  changeToStaticMenu() {
    this.layoutMode = MenuOrientation.STATIC;
  }

  changeToOverlayMenu() {
    this.layoutMode = MenuOrientation.OVERLAY;
  }

  changeToHorizontalMenu() {
    this.layoutMode = MenuOrientation.HORIZONTAL;
  }

  ngOnDestroy() {
    if (this.documentClickListener) {
      this.documentClickListener();
    }

    jQuery(this.layoutMenuScroller).nanoScroller({ flash: true });
  }

}
