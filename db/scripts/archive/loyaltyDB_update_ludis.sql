-- TABLAS CREADAS 

-- -----------------------------------------------------
-- Table `loyalty`.`REGLA_ASIGNACION_PREMIO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`REGLA_ASIGNACION_PREMIO` (
  `ID_REGLA` VARCHAR(40) NOT NULL,
  `IND_TIPO_REGLA` VARCHAR(1) NOT NULL,
  `IND_OPERADOR` VARCHAR(1) DEFAULT NULL,
  `VALOR_COMPARACION` VARCHAR(40)  DEFAULT NULL,
  `FECHA_INICIO` DATETIME DEFAULT NULL,
  `FECHA_FINAL` DATETIME DEFAULT NULL,
  `FECHA_IND_ULTIMO` VARCHAR(1) DEFAULT NULL,
  `FECHA_CANTIDAD` VARCHAR(1) DEFAULT NULL,
  `FECHA_IND_TIPO` VARCHAR(1) NOT NULL,
  `USUARIO_CREACION` VARCHAR(40) NOT NULL,
  `USUARIO_MODIFICACION` VARCHAR(40) DEFAULT NULL,
  `FECHA_CREACION` DATETIME NOT NULL,
  `FECHA_MODIFICACION` DATETIME NOT NULL,
  `PREMIO` VARCHAR(45) DEFAULT NULL,
  `IND_OPERADOR_REGLA` VARCHAR(5) NOT NULL,
  `TIER` VARCHAR(40) DEFAULT NULL,
  `NUM_VERSION` INT(11) NOT NULL,
  PRIMARY KEY (`ID_REGLA`),
  KEY `FK_REGLA_ASIG_PREMIO_PREM_idx` (`PREMIO`),
  KEY `FK_REGLA_ASIG_PREMIO_TIER_idx` (`TIER`),
  CONSTRAINT `FK_REGLA_ASIG_PREMIO_PREM` FOREIGN KEY (`PREMIO`) REFERENCES `PREMIO` (`ID_PREMIO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_REGLA_ASIG_PREMIO_TIER` FOREIGN KEY (`TIER`) REFERENCES `NIVEL_METRICA` (`ID_NIVEL`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
-- -----------------------------------------------------
-- Table `loyalty`.`ESTADISTICA_MIEMBRO_GENERAL`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`ESTADISTICA_MIEMBRO_GENERAL` (
  `ID` INT(11) NOT NULL,
  `CANT_MIEMBROS_EDAD_18_25` INT(11) DEFAULT NULL,
  `CANT_MIEMBROS_EDAD_25_35` INT(11) DEFAULT NULL,
  `CANT_MIEMBROS_EDAD_35_45` INT(11) DEFAULT NULL,
  `CANT_MIEMBROS_EDAD_45_55` INT(11) DEFAULT NULL,
  `CANT_MIEMBROS_EDAD_MAYOR_55` INT(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
);

-- -----------------------------------------------------
-- Table `loyalty`.`ESTADISTICA_MISION_GENERAL`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`ESTADISTICA_MISION_GENERAL` (
  `CANT_MISIONES_ENCUESTA` INT(11) DEFAULT NULL,
  `CANT_MISIONES_JUEGO` INT(11) DEFAULT NULL,
  `CANT_MISIONES_PERFIL` INT(11) DEFAULT NULL,
  `CANT_MISIONES_PREFERENCIA` INT(11) DEFAULT NULL,
  `CANT_MISIONES_VER_CONTENIDO` INT(11) DEFAULT NULL,
  `CANT_MISIONES_SUBIR_CONTENIDO` INT(11) DEFAULT NULL,
  `CANT_MISIONES_SOCIAL` INT(11) DEFAULT NULL,
  `ID` INT(11) NOT NULL,
  PRIMARY KEY (`ID`)
);
-- -----------------------------------------------------
-- Table `loyalty`.`ESTADISTICA_MISION_INDIVIDUAL`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`ESTADISTICA_MISION_INDIVIDUAL` (
  `ID_MIEMBRO` varchar(40) NOT NULL,
  `CANT_MISIONES_ENCUESTA` INT(11) DEFAULT NULL,
  `CANT_MISIONES_JUEGO` INT(11) DEFAULT NULL,
  `CANT_MISIONES_PERFIL` INT(11) DEFAULT NULL,
  `CANT_MISIONES_PREFERENCIA` INT(11) DEFAULT NULL,
  `CANT_MISIONES_VER_CONTENIDO` INT(11) DEFAULT NULL,
  `CANT_MISIONES_SUBIR_CONTENIDO` INT(11) DEFAULT NULL,
  `CANT_MISIONES_SOCIAL` INT(11) DEFAULT NULL,
  PRIMARY KEY (`ID_MIEMBRO`)
); 
-- -----------------------------------------------------
-- Table `loyalty`.`ESTADISTICA_SEGMENTO_GENERAL`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`ESTADISTICA_SEGMENTO_GENERAL` (
  `CANT_MIEMBROS_SEGMENTADOS` INT(11) DEFAULT NULL,
  `CANT_MIEMBROS_NO_SEGMENTADOS` INT(11) DEFAULT NULL,
  `ID` INT(11) NOT NULL,
  PRIMARY KEY (`ID`)
);
-- -----------------------------------------------------
-- Table `loyalty`.`ESTADISTICA_SEGMENTO_INDIVIDUAL`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`ESTADISTICA_SEGMENTO_INDIVIDUAL` (
  `ID_SEGMENTO` varchar(40) NOT NULL,
  `MIEMBROS_NUEVOS` INT(11) DEFAULT NULL,
  PRIMARY KEY (`ID_SEGMENTO`)
);
-- -----------------------------------------------------
-- Table `loyalty`.`APUESTA_LIGA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`APUESTA_LIGA` (
  `ID_LIGA` VARCHAR(40) NOT NULL,
  `DESCRIPCION` VARCHAR(100) NOT NULL,
  `IMAGEN` VARCHAR(200) NOT NULL,
  `FECHA_CREACION` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `USUARIO_CREACION` VARCHAR(40) NOT NULL,
  `FECHA_MODIFICACION` TIMESTAMP NULL DEFAULT NULL,
  `USUARIO_MODIFICACION` VARCHAR(40) DEFAULT NULL,
  `NUM_VERSION` BIGINT NOT NULL,
  CONSTRAINT `APUESTA_LIGA_PK` PRIMARY KEY (`ID_LIGA`)
);
-- -----------------------------------------------------
-- Table `loyalty`.`APUESTA_TORNEO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`APUESTA_TORNEO` (
  `ID_TORNEO` VARCHAR(40) NOT NULL,
  `DESCRIPCION` VARCHAR(100) NOT NULL,
  `ID_LIGA` VARCHAR(40) NOT NULL,
  `FECHA_CREACION` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `USUARIO_CREACION` VARCHAR(40) NOT NULL,
  `FECHA_MODIFICACION` TIMESTAMP NULL DEFAULT NULL,
  `USUARIO_MODIFICACION` VARCHAR(40) NOT NULL,
  `NUM_VERSION` BIGINT NOT NULL,
  CONSTRAINT `APUESTA_TORNEO_PK` PRIMARY KEY (`ID_TORNEO`)
);
-- -----------------------------------------------------
-- Table `loyalty`.`APUESTA_JORNADA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`APUESTA_JORNADA` (
  `ID_JORNADA` VARCHAR(40) NOT NULL,
  `DESCRIPCION` VARCHAR(100) NOT NULL,
  `ID_TORNEO` VARCHAR(40) NOT NULL,
  `FECHA_CREACION` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `USUARIO_CREACION` VARCHAR(40) NOT NULL,
  `FECHA_MODIFICACION` TIMESTAMP NULL DEFAULT NULL,
  `USUARIO_MODIFICACION` VARCHAR(40) NOT NULL,
  `NUM_VERSION` BIGINT NOT NULL,
  CONSTRAINT `APUESTA_JORNADA_PK` PRIMARY KEY (`ID_JORNADA`)
);
-- -----------------------------------------------------
-- Table `loyalty`.`APUESTA_EQUIPO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`APUESTA_EQUIPO` (
  `ID_EQUIPO` VARCHAR(40) NOT NULL,
  `DESCRIPCION` VARCHAR(100) NOT NULL,
  `IMAGEN` VARCHAR(200) NOT NULL,
  `FECHA_CREACION` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `USUARIO_CREACION` VARCHAR(40) NOT NULL,
  `FECHA_MODIFICACION` TIMESTAMP NULL DEFAULT NULL,
  `USUARIO_MODIFICACION` VARCHAR(40) NOT NULL,
  `NUM_VERSION` BIGINT NOT NULL,
  CONSTRAINT `APUESTA_EQUIPO_PK` PRIMARY KEY (`ID_EQUIPO`)
);
-- -----------------------------------------------------
-- Table `loyalty`.`APUESTA_JUEGO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`APUESTA_JUEGO` (
  `ID_JUEGO` VARCHAR(40) NOT NULL,
  `FECHA_LIMITE` TIMESTAMP NULL DEFAULT NULL,
  `ID_EQUIPO1` VARCHAR(40) NOT NULL,
  `ID_EQUIPO2` VARCHAR(40) NOT NULL,
  `ID_JORNADA` VARCHAR(40) NOT NULL,
  `RESULTADO` VARCHAR(7) DEFAULT NULL,
  `FECHA_CREACION` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `USUARIO_CREACION` VARCHAR(40) NOT NULL,
  `FECHA_MODIFICACION` TIMESTAMP NULL DEFAULT NULL,
  `USUARIO_MODIFICACION` VARCHAR(40) NOT NULL,
  `NUM_VERSION` BIGINT NOT NULL,
  CONSTRAINT `APUESTA_JUEGO_PK` PRIMARY KEY (`ID_JUEGO`)
);
-- -----------------------------------------------------
-- Table `loyalty`.`APUESTA_MIEMBRO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`APUESTA_MIEMBRO` (
  `ID_JUEGO` VARCHAR(40) NOT NULL,
  `ID_MIEMBRO` VARCHAR(40) NOT NULL,
  `METRICA_APOSTADA` DOUBLE NOT NULL,
  `PRONOSTICO` INT NOT NULL,
  `IND_ESTADO` CHAR(1) NOT NULL,
  `FECHA_CREACION` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT `APUESTA_MIEMBRO_PK` PRIMARY KEY (`ID_JUEGO`, `ID_MIEMBRO`)
);

-- -----------------------------------------------------
-- Table `loyalty`.`MIEMBRO_PROMOCION_REGALO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loyalty`.`MIEMBRO_PROMOCION_REGALO` (
  `ID_PROMOCION` VARCHAR(40) NOT NULL,
  `ID_MIEMBRO` VARCHAR(40) NOT NULL,
  `FECHA` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `GANADO` CHAR(1) NOT NULL,
  PRIMARY KEY (`ID_PROMOCION`, `ID_MIEMBRO`)
);

ALTER TABLE `loyalty`.`REGION` 
ADD COLUMN `COD_INTERNO` VARCHAR(40) NOT NULL AFTER `NOMBRE_REGION`;

ALTER TABLE `loyalty`.`PROMOCION` 
  ADD COLUMN `PREMIO` VARCHAR(45) DEFAULT NULL AFTER `IND_SEMANA_RECURRENCIA`;
  
ALTER TABLE `loyalty`.`PROMOCION` 
  ADD CONSTRAINT `FK_PROMO_PREMIO` 
  FOREIGN KEY (`PREMIO`) REFERENCES `loyalty`.`PREMIO` (`ID_PREMIO`);

ALTER TABLE `loyalty`.`APUESTA_TORNEO`
  ADD CONSTRAINT `FK_APUESTA_TORNEO`
  FOREIGN KEY (`ID_LIGA`) REFERENCES `loyalty`.`APUESTA_LIGA` (`ID_LIGA`);
  
ALTER TABLE `loyalty`.`APUESTA_JORNADA`
  ADD CONSTRAINT `FK_APUESTA_JORNADA`
  FOREIGN KEY (`ID_TORNEO`) REFERENCES `loyalty`.`APUESTA_TORNEO` (`ID_TORNEO`);

ALTER TABLE `loyalty`.`APUESTA_JUEGO` 
  ADD CONSTRAINT `FK_APUESTA_JUEGO_EQUIPO1` 
  FOREIGN KEY (`ID_EQUIPO1`) REFERENCES `loyalty`.`APUESTA_EQUIPO`(`ID_EQUIPO`),
  
  ADD CONSTRAINT `FK_APUESTA_JUEGO_EQUIPO2` 
  FOREIGN KEY (`ID_EQUIPO2`) REFERENCES `loyalty`.`APUESTA_EQUIPO`(`ID_EQUIPO`),
  
  ADD CONSTRAINT `FK_APUESTA_JUEGO_JORNADA` 
  FOREIGN KEY (`ID_JORNADA`) REFERENCES `loyalty`.`APUESTA_JORNADA`(`ID_JORNADA`);

ALTER TABLE `loyalty`.`APUESTA_MIEMBRO` 
  ADD CONSTRAINT `FK_APUESTA_MIEMBRO` 
  FOREIGN KEY (`ID_MIEMBRO`) REFERENCES `loyalty`.`MIEMBRO`(`ID_MIEMBRO`),
  
  ADD CONSTRAINT `FK_APUESTA_MIEMBRO_JUEGO` 
  FOREIGN KEY (`ID_JUEGO`) REFERENCES `loyalty`.`APUESTA_JUEGO`(`ID_JUEGO`);

ALTER TABLE `loyalty`.`MIEMBRO_PROMOCION_REGALO` 
  ADD CONSTRAINT `FK_MIEMBRO_PROMO_REGALO`
  FOREIGN KEY (`ID_MIEMBRO`) REFERENCES `loyalty`.`MIEMBRO` (`ID_MIEMBRO`);

ALTER TABLE `loyalty`.`MIEMBRO_PROMOCION_REGALO` 
  ADD CONSTRAINT `FK_PROMO_PROMO_REGALO` 
  FOREIGN KEY (`ID_PROMOCION`) REFERENCES `loyalty`.`PROMOCION` (`ID_PROMOCION`);

ALTER TABLE `loyalty`.`PREMIO_MIEMBRO` 
ADD COLUMN `FECHA_EXPIRACION` VARCHAR(45) NULL AFTER `CODIGO_CERTIFICADO`;

ALTER TABLE `loyalty`.`PREMIO` 
ADD COLUMN `COMERCIO` VARCHAR(40) NULL AFTER `TOTAL_EXISTENCIAS`;

ALTER TABLE `loyalty`.`PREMIO` 
ADD COLUMN `CANT_DIAS_VENCIMIENTO` INT(2) NULL AFTER `COMERCIO`;

ALTER TABLE `loyalty`.`PREMIO` 
ADD CONSTRAINT `FK_PREMIO_COMERCIO`
  FOREIGN KEY (`COMERCIO`) REFERENCES `loyalty`.`REGION` (`ID_REGION`);
  
-- CORE

-- ALTER para las columnas tipo TIMESTAMP
ALTER TABLE `loyalty`.`CATEGORIA_MISION` CHANGE COLUMN FECHA_REGISTRO FECHA_REGISTRO TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `loyalty`.`CATEGORIA_PROMOCION` CHANGE COLUMN FECHA_REGISTRO FECHA_REGISTRO TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `loyalty`.`DOCUMENTO_INVENTARIO` CHANGE COLUMN FECHA FECHA TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `loyalty`.`INSTANCIA_NOTIFICACION` CHANGE COLUMN FECHA_ENVIO FECHA_ENVIO TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `loyalty`.`NOTIFICACION` CHANGE COLUMN FECHA_EJECUCION FECHA_EJECUCION TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `loyalty`.`PREMIO` CHANGE COLUMN FECHA_PUBLICACION FECHA_PUBLICACION TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `loyalty`.`PRODUCTO` CHANGE COLUMN EFECTIVIDAD_FECHA_INICIO EFECTIVIDAD_FECHA_INICIO TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `loyalty`.`PRODUCTO` CHANGE COLUMN EFECTIVIDAD_FECHA_FIN EFECTIVIDAD_FECHA_FIN TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `loyalty`.`PRODUCTO` CHANGE COLUMN FECHA_PUBLICACION FECHA_PUBLICACION TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `loyalty`.`REGLA_AVANZADA` CHANGE COLUMN FECHA_FINAL FECHA_FINAL TIMESTAMP NULL DEFAULT NULL; 
ALTER TABLE `loyalty`.`REGLA_AVANZADA` CHANGE COLUMN FECHA_INICIO FECHA_INICIO TIMESTAMP NULL DEFAULT NULL; 

-- ALTER para la FECHA_MODIFICACION
ALTER TABLE `loyalty`.`ATRIBUTO_DINAMICO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`ATRIBUTO_DINAMICO_PRODUCTO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`CATEGORIA_MISION` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`CATEGORIA_PREMIO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`CATEGORIA_PRODUCTO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`CATEGORIA_PROMOCION` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`CONFIGURACION_GENERAL` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`DOCUMENTO_DETALLE` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`DOCUMENTO_INVENTARIO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`GRUPO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`GRUPO_NIVELES` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`INSIGNIAS` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`LISTA_REGLAS` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`METRICA` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`MIEMBRO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`MISION` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`MISION_ENCUESTA_PREGUNTA` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`MISION_JUEGO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`MISION_PERFIL_ATRIBUTO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`MISION_RED_SOCIAL` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`MISION_SUBIR_CONTENIDO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`MISION_VER_CONTENIDO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`NIVELES_INSIGNIAS` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`NIVEL_METRICA` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`NOTIFICACION` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`PREFERENCIA` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`PREMIO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`PREMIO_MISION_JUEGO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`PRODUCTO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`PROMOCION` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`PROVEEDOR` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`REGION` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`REGLA_ASIGNACION_PREMIO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`SEGMENTO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`SUBCATEGORIA_PRODUCTO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`TABLA_POSICIONES` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`UBICACION` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`UNIDADES_MEDIDA` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`USUARIO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`WORKFLOW` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`WORKFLOW_NODO` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;
ALTER TABLE `loyalty`.`ZONA` CHANGE COLUMN FECHA_MODIFICACION FECHA_MODIFICACION TIMESTAMP NULL;

-- ALTER para las columnas de FECHA_CREACION

ALTER TABLE `loyalty`.`ATRIBUTO_DINAMICO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`ATRIBUTO_DINAMICO_PRODUCTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`CATEGORIA_MISION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`CATEGORIA_PREMIO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`CATEGORIA_PRODUCTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`CATEGORIA_PROMOCION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`CODIGO_CERTIFICADO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`DOCUMENTO_DETALLE` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`DOCUMENTO_INVENTARIO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`GRUPO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`GRUPO_MIEMBRO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`GRUPO_NIVELES` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`INSIGNIAS` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`INSTANCIA_NOTIFICACION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`LISTA_REGLAS` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MEDIOS_PAGO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`METRICA` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MIEMBRO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MIEMBRO_ATRIBUTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MIEMBRO_INSIGNIA_NIVEL` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MIEMBRO_METRICA_NIVEL` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MISION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MISION_CATEGORIA_MISION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MISION_ENCUESTA_PREGUNTA` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MISION_JUEGO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MISION_LISTA_MIEMBRO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MISION_LISTA_SEGMENTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MISION_LISTA_UBICACION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MISION_PERFIL_ATRIBUTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MISION_PREFERENCIA` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MISION_RED_SOCIAL` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MISION_SUBIR_CONTENIDO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`MISION_VER_CONTENIDO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`NIVELES_INSIGNIAS` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`NIVEL_METRICA` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`NOTIFICACION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`NOTIFICACION_LISTA_MIEMBRO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`NOTIFICACION_LISTA_SEGMENTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PREFERENCIA` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PREMIO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PREMIO_CATEGORIA_PREMIO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PREMIO_LISTA_MIEMBRO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PREMIO_LISTA_SEGMENTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PREMIO_LISTA_UBICACION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PREMIO_MISION_JUEGO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PRODUCTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PRODUCTO_ATRIBUTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PRODUCTO_CATEG_SUBCATEG` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PRODUCTO_LISTA_MIEMBRO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PRODUCTO_LISTA_SEGMENTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PRODUCTO_LISTA_UBICACION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PROMOCION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PROMOCION_CATEGORIA_PROMO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PROMOCION_LISTA_MIEMBRO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PROMOCION_LISTA_SEGMENTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PROMOCION_LISTA_UBICACION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`PROVEEDOR` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`REGION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`REGION_UBICACION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`REGLA_ASIGNACION_PREMIO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`REGLA_AVANZADA` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`REGLA_ENCUESTA` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`REGLA_METRICA_BALANCE` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`REGLA_METRICA_CAMBIO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`REGLA_MIEMBRO_ATB` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`REGLA_PREFERENCIA` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`RESPUESTA_PREFERENCIA` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`SEGMENTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`SEGMENTO_LISTA_MIEMBRO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`SUBCATEGORIA_PRODUCTO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`TABLA_POSICIONES` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`TRABAJOS_INTERNOS` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`UBICACION` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`UNIDADES_MEDIDA` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`USUARIO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`WORKFLOW` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`WORKFLOW_NODO` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `loyalty`.`ZONA` CHANGE COLUMN FECHA_CREACION FECHA_CREACION TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
