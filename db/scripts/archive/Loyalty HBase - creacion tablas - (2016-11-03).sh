if [ -z ${HBASE_HOME+x} ];
then
	echo "HBASE_HOME no esta establecido";
else
	#nota: no se verifica la existencia de las tablas y solo se intenta crearlas
	echo "create 'ELIGIBLES_SEGMENTO','DATA','MIEMBROS'" | $HBASE_HOME/bin/hbase shell;
	echo "create 'BILLETERA_MIEMBRO','DATA'" | $HBASE_HOME/bin/hbase shell;
	echo "create 'REGISTRO_METRICA','DATA','DETALLES'" | $HBASE_HOME/bin/hbase shell;
	echo "create 'TABLA_POSICIONES','MIEMBRO','GRUPO'" | $HBASE_HOME/bin/hbase shell;
fi

