

ALTER TABLE `loyalty`.`MIEMBRO` 
  ADD COLUMN `IND_POLITICA` CHAR(1) DEFAULT NULL AFTER `COMENTARIOS`;
  
ALTER TABLE `loyalty`.`MIEMBRO` 
  ADD COLUMN `FECHA_POLITICA` TIMESTAMP NULL DEFAULT NULL AFTER `IND_POLITICA`;
  

ALTER TABLE `loyalty`.`CONFIGURACION_GENERAL` 
  DROP COLUMN `MSJ_IND_PROTOCOLO_SMTP`;
 
-- ALTER TABLE `loyalty`.`NIVEL_METRICA` 
  -- ADD COLUMN `MULTIPLICADOR` DOUBLE DEFAULT NULL AFTER `METRICA_INICIAL`;
